package com.pradera.report.generation.executor.accounting;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.type.AccountingConciliationType;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.accounting.service.AccountingGeneratorReportServiceBean;
import com.pradera.report.generation.executor.accounting.service.AccountingReportServiceBean;
import com.pradera.report.generation.executor.accounting.to.AccountingMatrixDetailTo;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.pradera.report.generation.executor.accounting.to.OperationBalanceTo;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSquaringReportPDF.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="AccountingSquaringReportPDF")
public class AccountingSquaringReportPDF extends GenericReport{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;

	/** The accounting generator report service bean. */
	@Inject
	private AccountingGeneratorReportServiceBean accountingGeneratorReportServiceBean;
	

	/** The accounting report service bean. */
	@Inject
	AccountingReportServiceBean 	accountingReportServiceBean;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The tag squaring. */
	private static String TAG_SQUARING       					= "SQUARING";
	
	/** The tag account. */
	private static String TAG_ACCOUNT       					= "CUENTA";
	
	/** The tag date time process. */
	private static String TAG_DATE_TIME_PROCESS       			= "DATE_TIME_PROCESS";
	
	/** The tag date system. */
	private static String TAG_DATE_SYSTEM       				= "DATE_SYSTEM";
	
	/** The tag tc usd. */
	private static String TAG_TC_USD       						= "TC_USD";
	
	/** The tag tc ufv. */
	private static String TAG_TC_UFV       						= "TC_UFV";
	
	/** The tag tc ecu. */
	private static String TAG_TC_ECU       						= "TC_ECU";


	/** The tag label balance. */
	private static String TAG_LABEL_BALANCE       				= "LABEL_BALANCE";
	
	/** The tag account fixed income. */
	private static String TAG_ACCOUNT_FIXED_INCOME       		= "ACCOUNT_FIXED_INCOME";
	
	/** The tag account variable income. */
	private static String TAG_ACCOUNT_VARIABLE_INCOME       	= "ACCOUNT_VARIABLE_INCOME";
	
	/** The tag account mixed income. */
	private static String TAG_ACCOUNT_MIXED_INCOME       		= "ACCOUNT_MIXED_INCOME";
	
	/** The tag cartera bob. */
	private static String TAG_CARTERA_BOB       				= "CARTERA_BOB";
	
	/** The tag cartera usd. */
	private static String TAG_CARTERA_USD       				= "CARTERA_USD";
	
	/** The tag cartera mv. */
	private static String TAG_CARTERA_MV       					= "CARTERA_MV";
	
	/** The tag cartera ufv. */
	private static String TAG_CARTERA_UFV       				= "CARTERA_UFV";
	
	/** The tag cartera euro. */
	private static String TAG_CARTERA_EURO       				= "CARTERA_EURO";
	
	/** The tag cartera total bs. */
	private static String TAG_CARTERA_TOTAL_BS       			= "CARTERA_TOTAL_BS";
	
	/** The tag account bob fixed income. */
	private static String TAG_ACCOUNT_BOB_FIXED_INCOME       	= "ACCOUNT_BOB_FIXED_INCOME";
	
	/** The tag account usd fixed income. */
	private static String TAG_ACCOUNT_USD_FIXED_INCOME       	= "ACCOUNT_USD_FIXED_INCOME";
	
	/** The tag account mv fixed income. */
	private static String TAG_ACCOUNT_MV_FIXED_INCOME       	= "ACCOUNT_MV_FIXED_INCOME";
	
	/** The tag account ufv fixed income. */
	private static String TAG_ACCOUNT_UFV_FIXED_INCOME       	= "ACCOUNT_UFV_FIXED_INCOME";
	
	/** The tag account euro fixed income. */
	private static String TAG_ACCOUNT_EURO_FIXED_INCOME       	= "ACCOUNT_EURO_FIXED_INCOME";

	/** The tag account bob variable income. */
	private static String TAG_ACCOUNT_BOB_VARIABLE_INCOME       = "ACCOUNT_BOB_VARIABLE_INCOME";
	
	/** The tag account usd variable income. */
	private static String TAG_ACCOUNT_USD_VARIABLE_INCOME       = "ACCOUNT_USD_VARIABLE_INCOME";
	
	/** The tag account mv variable income. */
	private static String TAG_ACCOUNT_MV_VARIABLE_INCOME       	= "ACCOUNT_MV_VARIABLE_INCOME";
	
	/** The tag account ufv variable income. */
	private static String TAG_ACCOUNT_UFV_VARIABLE_INCOME       = "ACCOUNT_UFV_VARIABLE_INCOME";
	
	/** The tag account euro variable income. */
	private static String TAG_ACCOUNT_EURO_VARIABLE_INCOME      = "ACCOUNT_EURO_VARIABLE_INCOME";

	/** The tag account bob mixed income. */
	private static String TAG_ACCOUNT_BOB_MIXED_INCOME       	= "ACCOUNT_BOB_MIXED_INCOME";
	
	/** The tag account usd mixed income. */
	private static String TAG_ACCOUNT_USD_MIXED_INCOME       	= "ACCOUNT_USD_MIXED_INCOME";
	
	/** The tag account mv mixed income. */
	private static String TAG_ACCOUNT_MV_MIXED_INCOME       	= "ACCOUNT_MV_MIXED_INCOME";
	
	/** The tag account ufv mixed income. */
	private static String TAG_ACCOUNT_UFV_MIXED_INCOME       	= "ACCOUNT_UFV_MIXED_INCOME";
	
	/** The tag account euro mixed income. */
	private static String TAG_ACCOUNT_EURO_MIXED_INCOME       	= "ACCOUNT_EURO_MIXED_INCOME";
	
	/** The tag account total bob. */
	private static String TAG_ACCOUNT_TOTAL_BOB       			= "ACCOUNT_TOTAL_BOB";
	
	/** The tag account remainder. */
	private static String TAG_ACCOUNT_REMAINDER    				= "ACCOUNT_REMAINDER";
	
	/** The tag total cartera total bs. */
	private static String TAG_TOTAL_CARTERA_TOTAL_BS    		= "TOTAL_CARTERA_TOTAL_BS";
	
	/** The tag total accounting total bs. */
	private static String TAG_TOTAL_ACCOUNTING_TOTAL_BS    		= "TOTAL_ACCOUNTING_TOTAL_BS";
	
	/** The tag total accounting remainder total. */
	private static String TAG_TOTAL_ACCOUNTING_REMAINDER_TOTAL  = "TOTAL_ACCOUNTING_REMAINDER_TOTAL";
	
	
	
	/**
	 * Instantiates a new accounting squaring report pdf.
	 */
	public AccountingSquaringReportPDF() {
		
	}
	
	/** The list accounting matrix detail. */
	List<AccountingMatrixDetailTo>	listAccountingMatrixDetail;
	
	/** The list accounting account. */
	List<AccountingAccount>	listAccountingAccount;
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
		AccountingProcessTo accountingProcessTo=new AccountingProcessTo();
		StringBuilder sbParametersReport = new StringBuilder();
		Map<String, Object> 			parameters = new HashMap<String, Object>();
		boolean existData = false;
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {



			if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_ACCOUNTING_PK)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setIdAccountingProcessPk(Long.parseLong(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
			else if(loggerDetail.getFilterName().equals(ReportConstant.BRANCH)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setProcessType(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
			else if(loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setExecutionDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}else if(loggerDetail.getFilterName().equals(GeneralConstants.REPORT_BY_PROCESS)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setReportByProcess(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
		}
		
		accountingProcessTo.setReportByProcess(BooleanType.YES.getCode());
		accountingProcessTo.setStartType(AccountingStartType.CONTINUE.getCode());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			
			parameters=this.accountingGeneratorReportServiceBean.findSquaringOperations(accountingProcessTo);
			
			existData = true;
		
		
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;
				xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 * */
			xmlsw.writeStartElement(ReportConstant.REPORT);
			
			
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			
			createHeaderReport(xmlsw, reportLogger);
	
			if(existData) {
				
				createBodyReport(xmlsw,reportLogger,parameters,accountingProcessTo.getExecutionDate());
	
			}
			else{
				createEmptyReport(xmlsw);
			}
			

			createTagString(xmlsw,ReportConstant.REPORT_ID,reportLogger.getReport().getIdReportPk());
			
			
			//time finish and parameters
			String hourEnd = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),
					ReportConstant.DATE_FORMAT_TIME_SECOND);
			createTagString(xmlsw, ReportConstant.END_HOUR, hourEnd);
			//parameters
			String parametersFilter = Validations.validateIsNotNullAndNotEmpty(sbParametersReport) ? sbParametersReport.substring(0, sbParametersReport.toString().length() -2) : GeneralConstants.EMPTY_STRING;
			createTagString(xmlsw, ReportConstant.PARAMETERS_TAG_REPORT, parametersFilter);
			//close report tag
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
			/**FILE**/
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baos;
	}
	

	/**
	 * Creates the body report.
	 *
	 * @param xmlsw the xmlsw
	 * @param reportLogger the report logger
	 * @param parameters the parameters
	 * @throws XMLStreamException the XML stream exception
	 * @throws ServiceException the service exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw, ReportLogger reportLogger,Map<String, Object> parameters, Date date)  throws XMLStreamException, ServiceException{
		
		int iDes=0,iRep=0,iFis=0,iBloq=0,iBloqO=0,iNoCol=0;
		int maxDes=0,maxRep=0,maxFis=0,maxBloq=0,maxBloqO=0,maxNoCol=0;

		BigDecimal dailyExchangeUSD=accountingReportServiceBean.processExchange(parameters,CurrencyType.USD.getCode(),
	    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
		BigDecimal dailyExchangeUFV=accountingReportServiceBean.processExchange(parameters,CurrencyType.UFV.getCode(),
	    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
		BigDecimal dailyExchangeECU=accountingReportServiceBean.processExchange(parameters,CurrencyType.EU.getCode(),
	    		CurrencyType.PYG.getCode(),DailyExchangeRoleType.BUY.getCode());
		
		String strStartHour = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),ReportConstant.DATE_FORMAT_TIME_SECOND);
		
		/**
		 * Cartera
		 */
        Map<String,OperationBalanceTo> paramOperation= (Map<String, OperationBalanceTo>) parameters.get(GeneralConstants.SQUARING_OPERATIONS);
        OperationBalanceTo 	balanceAvailable=paramOperation.get(GeneralConstants.DESMATERIALIZADO);
		OperationBalanceTo 	balanceReporting=paramOperation.get(GeneralConstants.REPORTANTE);
		OperationBalanceTo 	balanceBlock=paramOperation.get(GeneralConstants.BLOQUEOS);
		OperationBalanceTo 	balanceOtherBlock=paramOperation.get(GeneralConstants.BLOQUEO_OTROS);
		OperationBalanceTo 	balancePhysical=paramOperation.get(GeneralConstants.FISICOS);
		OperationBalanceTo 	balanceNotPlaced=paramOperation.get(GeneralConstants.NOT_PLACED);
		
		

		BigDecimal totalAmountOperation=new BigDecimal(0);
		BigDecimal amountOperAvailable=balanceAvailable.getAmountToBOB();
		BigDecimal amountOperReporting=balanceReporting.getAmountToBOB();
		BigDecimal amountOperBlock=balanceBlock.getAmountToBOB();
		BigDecimal amountOperOtherBlock=balanceOtherBlock.getAmountToBOB();
		BigDecimal amountOperPhysical=balancePhysical.getAmountToBOB();
		BigDecimal amountOperNotPlaced=balanceNotPlaced.getAmountToBOB();
		
		BigDecimal totalAmountAccounting=new BigDecimal(0);
		BigDecimal amountAccountingAvailable=new BigDecimal(0);
		BigDecimal amountAccountingReporting=new BigDecimal(0);
		BigDecimal amountAccountingBlock=new BigDecimal(0);
		BigDecimal amountAccountingOtherBlock=new BigDecimal(0);
		BigDecimal amountAccountingPhysical=new BigDecimal(0);
		BigDecimal amountAccountingNotPlaced=new BigDecimal(0);	
		
		BigDecimal totalRemainder=new BigDecimal(0);
		BigDecimal amountRemainderAvailable=new BigDecimal(0);
		BigDecimal amountRemainderReporting=new BigDecimal(0);
		BigDecimal amountRemainderBlock=new BigDecimal(0);
		BigDecimal amountRemainderOtherBlock=new BigDecimal(0);
		BigDecimal amountRemainderPhysical=new BigDecimal(0);
		BigDecimal amountRemainderNotPlaced=new BigDecimal(0);
		
		List<AccountingAccount> listAccountingAccount=
				(List<AccountingAccount>) parameters.get(GeneralConstants.SQUARING_ACCOUNTING);
		AccountingProcess accountingProcess=(AccountingProcess) parameters.get(GeneralConstants.ACCOUNTING_PROCESS);
		
		createTagString(xmlsw, TAG_DATE_TIME_PROCESS ,  CommonsUtilities.convertDatetoString(accountingProcess.getExecutionDate()).concat(GeneralConstants.BLANK_SPACE).concat(strStartHour) );
		createTagString(xmlsw, TAG_DATE_SYSTEM ,    	CommonsUtilities.convertDatetoString(reportLogger.getRegistryDate()));
		createTagString(xmlsw, TAG_TC_USD ,    			CommonsUtilities.getFormatOfAmount(dailyExchangeUSD, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		createTagString(xmlsw, TAG_TC_UFV ,    			CommonsUtilities.getFormatOfAmount(dailyExchangeUFV, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		createTagString(xmlsw, TAG_TC_ECU ,    			CommonsUtilities.getFormatOfAmount(dailyExchangeECU, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		
		xmlsw.writeStartElement(TAG_SQUARING);
		
		/**
		 * Fill Maximum Indicator By PortFolio  
		 */
		for (AccountingAccount accountingAccount : listAccountingAccount) {
			
        	if(AccountingConciliationType.DEMATERIALIZED_BALANCE_AVAILABLE.getCode().equals(accountingAccount.getPortFolio())){
 
        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxDes++;
        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxDes++;
        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxDes++;
        		}
        		
        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_REPORTED.getCode().equals(accountingAccount.getPortFolio())){
        		
        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxRep++;
        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxRep++;
        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxRep++;	
        		}
        		
        		
        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_LOCK.getCode().equals(accountingAccount.getPortFolio())){
        		
        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxBloq++;
        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxBloq++;
        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxBloq++;
        		}
        		
        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_LOCK_OTHER.getCode().equals(accountingAccount.getPortFolio())){
        		
        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxBloqO++;
        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxBloqO++;
        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxBloqO++;
        		}
        		                
        	}else if(AccountingConciliationType.PHYSICAL_BALANCE.getCode().equals(accountingAccount.getPortFolio())){
        		
        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxFis++;
        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxFis++;
        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxFis++;
        		}
        		
        	}else if(AccountingConciliationType.SECURITIES_OUTSTANDING_PLACE.getCode().equals(accountingAccount.getPortFolio())){
        		
        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxNoCol++;
        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxNoCol++;
        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
        			maxNoCol++;
        		}
        		
        	}
		}
	
		
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingAccount)){
			
			for (AccountingAccount accountingAccount : listAccountingAccount) {
				
	        	if(AccountingConciliationType.DEMATERIALIZED_BALANCE_AVAILABLE.getCode().equals(accountingAccount.getPortFolio())){
	        		
	        		if(iDes==0){
	        			xmlsw.writeStartElement(TAG_ACCOUNT);
	        			createTagString(xmlsw, TAG_LABEL_BALANCE ,    GeneralConstants.DESMATERIALIZADO);
	            		createTagString(xmlsw, TAG_CARTERA_BOB ,      CommonsUtilities.getFormatOfAmount(balanceAvailable.getBalanceBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_USD ,      CommonsUtilities.getFormatOfAmount(balanceAvailable.getBalanceUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_MV  ,      CommonsUtilities.getFormatOfAmount(balanceAvailable.getBalanceMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_UFV ,      CommonsUtilities.getFormatOfAmount(balanceAvailable.getBalanceUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_EURO ,     CommonsUtilities.getFormatOfAmount(balanceAvailable.getBalanceECU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS , CommonsUtilities.getFormatOfAmount(amountOperAvailable, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperAvailable, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		
	        		//11
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_FIXED_INCOME,CommonsUtilities.getFormatOfAmount( accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			
		     	        amountAccountingAvailable=CommonsUtilities.addTwoDecimal(amountAccountingAvailable, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//12	
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
			     	    amountAccountingAvailable=CommonsUtilities.addTwoDecimal(amountAccountingAvailable, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//13
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_MIXED_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingAvailable=CommonsUtilities.addTwoDecimal(amountAccountingAvailable, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		iDes++;
	        		
	        		if(iDes==maxDes){
	        			/**
	        			 * Label
	        			 */
	        			createTagString(xmlsw, TAG_ACCOUNT_FIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IF_AVAILABLE);
	        			createTagString(xmlsw, TAG_ACCOUNT_VARIABLE_INCOME, ReportConstant.LABEL_ACCOUNT_IV_AVAILABLE);
	        			createTagString(xmlsw, TAG_ACCOUNT_MIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IM_AVAILABLE);
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_TOTAL_BOB, CommonsUtilities.getFormatOfAmount(amountAccountingAvailable, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		        		amountRemainderAvailable=CommonsUtilities.subtractTwoDecimal(amountOperAvailable, amountAccountingAvailable, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderAvailable, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                createTagString(xmlsw, TAG_ACCOUNT_REMAINDER, CommonsUtilities.getFormatOfAmount(amountRemainderAvailable, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                
		                xmlsw.writeEndElement();
	        		}
	        		
	        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_REPORTED.getCode().equals(accountingAccount.getPortFolio())){
	        		
	        		if(iRep==0){
	        			
	        			xmlsw.writeStartElement(TAG_ACCOUNT);
	        			createTagString(xmlsw, TAG_LABEL_BALANCE ,    GeneralConstants.REPORTADO);
	            		createTagString(xmlsw, TAG_CARTERA_BOB ,      CommonsUtilities.getFormatOfAmount(balanceReporting.getBalanceBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_USD ,      CommonsUtilities.getFormatOfAmount(balanceReporting.getBalanceUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_MV  ,      CommonsUtilities.getFormatOfAmount(balanceReporting.getBalanceMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_UFV ,      CommonsUtilities.getFormatOfAmount(balanceReporting.getBalanceUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_EURO ,     CommonsUtilities.getFormatOfAmount(balanceReporting.getBalanceECU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS , CommonsUtilities.getFormatOfAmount(amountOperReporting, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperReporting, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        			
	        		
	        		//14
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_FIXED_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingReporting=CommonsUtilities.addTwoDecimal(amountAccountingReporting, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//15
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_VARIABLE_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingReporting=CommonsUtilities.addTwoDecimal(amountAccountingReporting, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//16
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_MIXED_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingReporting=CommonsUtilities.addTwoDecimal(amountAccountingReporting, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		iRep++;
	        		
	        		if(iRep==maxRep){
	        			createTagString(xmlsw, TAG_ACCOUNT_FIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IF_REPOTED);
	        			createTagString(xmlsw, TAG_ACCOUNT_VARIABLE_INCOME, ReportConstant.LABEL_ACCOUNT_IV_REPOTED);
	        			createTagString(xmlsw, TAG_ACCOUNT_MIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IM_REPOTED);
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_TOTAL_BOB, CommonsUtilities.getFormatOfAmount(amountAccountingReporting, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                amountRemainderReporting=CommonsUtilities.subtractTwoDecimal(amountOperReporting, amountAccountingReporting, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderReporting, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                createTagString(xmlsw, TAG_ACCOUNT_REMAINDER, CommonsUtilities.getFormatOfAmount(amountRemainderReporting, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                
		                xmlsw.writeEndElement();
	        		}
	        		
	                
	        		
	        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_LOCK.getCode().equals(accountingAccount.getPortFolio())){
	        		
	        		if(iBloq==0){
	        			
	        			xmlsw.writeStartElement(TAG_ACCOUNT);
	        			createTagString(xmlsw, TAG_LABEL_BALANCE ,    GeneralConstants.BLOQUEOS);
	            		createTagString(xmlsw, TAG_CARTERA_BOB ,      CommonsUtilities.getFormatOfAmount(balanceBlock.getBalanceBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_USD ,      CommonsUtilities.getFormatOfAmount(balanceBlock.getBalanceUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_MV  ,      CommonsUtilities.getFormatOfAmount(balanceBlock.getBalanceMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_UFV ,      CommonsUtilities.getFormatOfAmount(balanceBlock.getBalanceUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_EURO ,     CommonsUtilities.getFormatOfAmount(balanceBlock.getBalanceECU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS , CommonsUtilities.getFormatOfAmount(amountOperBlock, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		
	        		
	        		//17
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_FIXED_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingBlock=CommonsUtilities.addTwoDecimal(amountAccountingBlock, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//18
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_VARIABLE_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingBlock=CommonsUtilities.addTwoDecimal(amountAccountingBlock, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//19
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_MIXED_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingBlock=CommonsUtilities.addTwoDecimal(amountAccountingBlock, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        			
	        		}
	        		iBloq++;
	        		
	        		if(iBloq==maxBloq){
	        			createTagString(xmlsw, TAG_ACCOUNT_FIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IF_LOCK);
	        			createTagString(xmlsw, TAG_ACCOUNT_VARIABLE_INCOME, ReportConstant.LABEL_ACCOUNT_IV_LOCK);
	        			createTagString(xmlsw, TAG_ACCOUNT_MIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IM_LOCK);
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_TOTAL_BOB, CommonsUtilities.getFormatOfAmount(amountAccountingBlock, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                amountRemainderBlock=CommonsUtilities.subtractTwoDecimal(amountOperBlock, amountAccountingBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                createTagString(xmlsw, TAG_ACCOUNT_REMAINDER, CommonsUtilities.getFormatOfAmount(amountRemainderBlock, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                
		                xmlsw.writeEndElement();
	        		}
	        		
	                
	        		
	        	}else if(AccountingConciliationType.DEMATERIALIZED_BALANCE_LOCK_OTHER.getCode().equals(accountingAccount.getPortFolio())){
	        		
	        		if(iBloqO==0){
	        			
	        			xmlsw.writeStartElement(TAG_ACCOUNT);
	        			createTagString(xmlsw, TAG_LABEL_BALANCE ,    GeneralConstants.BLOQUEO_OTROS);
	            		createTagString(xmlsw, TAG_CARTERA_BOB ,      CommonsUtilities.getFormatOfAmount(balanceOtherBlock.getBalanceBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_USD ,      CommonsUtilities.getFormatOfAmount(balanceOtherBlock.getBalanceUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_MV  ,      CommonsUtilities.getFormatOfAmount(balanceOtherBlock.getBalanceMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_UFV ,      CommonsUtilities.getFormatOfAmount(balanceOtherBlock.getBalanceUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_EURO ,     CommonsUtilities.getFormatOfAmount(balanceOtherBlock.getBalanceECU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS , CommonsUtilities.getFormatOfAmount(amountOperOtherBlock, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperOtherBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		
	        		
	        		//20
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_FIXED_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingOtherBlock=CommonsUtilities.addTwoDecimal(amountAccountingOtherBlock, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//21
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingOtherBlock=CommonsUtilities.addTwoDecimal(amountAccountingOtherBlock, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//22
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingOtherBlock=CommonsUtilities.addTwoDecimal(amountAccountingOtherBlock, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        			
	        		}
	        		iBloqO++;
	        		
	        		if(iBloqO==maxBloqO){
	        			createTagString(xmlsw, TAG_ACCOUNT_FIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IF_OTHER_LOCK);
	        			createTagString(xmlsw, TAG_ACCOUNT_VARIABLE_INCOME, ReportConstant.LABEL_ACCOUNT_IV_OTHER_LOCK);
	        			createTagString(xmlsw, TAG_ACCOUNT_MIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IM_OTHER_LOCK);
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_TOTAL_BOB, CommonsUtilities.getFormatOfAmount(amountAccountingOtherBlock, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		        		amountRemainderOtherBlock=CommonsUtilities.subtractTwoDecimal(amountOperOtherBlock, amountAccountingOtherBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderOtherBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                createTagString(xmlsw, TAG_ACCOUNT_REMAINDER, CommonsUtilities.getFormatOfAmount(amountRemainderOtherBlock, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                
		                xmlsw.writeEndElement();
	        		}
	        		
	                
	        	}else if(AccountingConciliationType.PHYSICAL_BALANCE.getCode().equals(accountingAccount.getPortFolio())){
	        		
	        		if(iFis==0){
	        			
	        			xmlsw.writeStartElement(TAG_ACCOUNT);
	        			createTagString(xmlsw, TAG_LABEL_BALANCE ,    GeneralConstants.FISICOS);
	            		createTagString(xmlsw, TAG_CARTERA_BOB ,      CommonsUtilities.getFormatOfAmount(balancePhysical.getBalanceBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_USD ,      CommonsUtilities.getFormatOfAmount(balancePhysical.getBalanceUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_MV  ,      CommonsUtilities.getFormatOfAmount(balancePhysical.getBalanceMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_UFV ,      CommonsUtilities.getFormatOfAmount(balancePhysical.getBalanceUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_EURO ,     CommonsUtilities.getFormatOfAmount(balancePhysical.getBalanceECU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS , CommonsUtilities.getFormatOfAmount(amountOperPhysical, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	            		totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperPhysical, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		
	        		
	        		//23
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingPhysical=CommonsUtilities.addTwoDecimal(amountAccountingPhysical, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//24
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingPhysical=CommonsUtilities.addTwoDecimal(amountAccountingPhysical, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        			
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			amountAccountingPhysical=CommonsUtilities.addTwoDecimal(amountAccountingPhysical, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		iFis++;
	        		
	        		if(iFis==maxFis){
	        			createTagString(xmlsw, TAG_ACCOUNT_FIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IF_PHYSICAL);
	        			createTagString(xmlsw, TAG_ACCOUNT_VARIABLE_INCOME, ReportConstant.LABEL_ACCOUNT_IV_PHYSICAL);
	        			createTagString(xmlsw, TAG_ACCOUNT_MIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IM_PHYSICAL);
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_TOTAL_BOB, CommonsUtilities.getFormatOfAmount(amountAccountingPhysical, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		        		amountRemainderPhysical=CommonsUtilities.subtractTwoDecimal(amountOperPhysical, amountAccountingPhysical, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderPhysical, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                createTagString(xmlsw, TAG_ACCOUNT_REMAINDER, CommonsUtilities.getFormatOfAmount(amountRemainderPhysical, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                
		                xmlsw.writeEndElement();
	        		}
	        		
	        	}else if(AccountingConciliationType.SECURITIES_OUTSTANDING_PLACE.getCode().equals(accountingAccount.getPortFolio())){
	        		
	        		if(iNoCol==0){
	        			//no colocados cartera
	        			xmlsw.writeStartElement(TAG_ACCOUNT);
	        			if(date.before(CommonsUtilities.currentDate())) {
	        				BigDecimal sumNotPlaced = BigDecimal.ZERO;
	        				BigDecimal sumNotPacedBOB = BigDecimal.ZERO;
	        				BigDecimal sumNotPlacedUSD = BigDecimal.ZERO;
	        				BigDecimal sumNotPlacedMVL = BigDecimal.ZERO;
	        				BigDecimal sumNotPlacedUFV = BigDecimal.ZERO;
	        				BigDecimal sumNotPlacedEU = BigDecimal.ZERO;
	        				
		        			for (AccountingAccount accountingAccountAux : listAccountingAccount) {
		        				if(AccountingConciliationType.SECURITIES_OUTSTANDING_PLACE.getCode().equals(accountingAccountAux.getPortFolio())){
		        					if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccountAux.getInstrumentType())){
		        	        			sumNotPlaced=CommonsUtilities.addTwoDecimal(sumNotPlaced, accountingAccountAux.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			
		        	        			sumNotPacedBOB=CommonsUtilities.addTwoDecimal(sumNotPacedBOB, accountingAccountAux.getAmountBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedUSD=CommonsUtilities.addTwoDecimal(sumNotPlacedUSD, accountingAccountAux.getAmountUSD(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedMVL=CommonsUtilities.addTwoDecimal(sumNotPlacedMVL, accountingAccountAux.getAmountMVL(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedUFV=CommonsUtilities.addTwoDecimal(sumNotPlacedUFV, accountingAccountAux.getAmountUFV(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedEU=CommonsUtilities.addTwoDecimal(sumNotPlacedEU, accountingAccountAux.getAmountEU(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccountAux.getInstrumentType())){
		        	        			sumNotPlaced=CommonsUtilities.addTwoDecimal(sumNotPlaced, accountingAccountAux.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			
		        	        			sumNotPacedBOB=CommonsUtilities.addTwoDecimal(sumNotPacedBOB, accountingAccountAux.getAmountBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedUSD=CommonsUtilities.addTwoDecimal(sumNotPlacedUSD, accountingAccountAux.getAmountUSD(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedMVL=CommonsUtilities.addTwoDecimal(sumNotPlacedMVL, accountingAccountAux.getAmountMVL(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedUFV=CommonsUtilities.addTwoDecimal(sumNotPlacedUFV, accountingAccountAux.getAmountUFV(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedEU=CommonsUtilities.addTwoDecimal(sumNotPlacedEU, accountingAccountAux.getAmountEU(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccountAux.getInstrumentType())){
		        	        			sumNotPlaced=CommonsUtilities.addTwoDecimal(sumNotPlaced, accountingAccountAux.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			
		        	        			sumNotPacedBOB=CommonsUtilities.addTwoDecimal(sumNotPacedBOB, accountingAccount.getAmountBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedUSD=CommonsUtilities.addTwoDecimal(sumNotPlacedUSD, accountingAccountAux.getAmountUSD(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedMVL=CommonsUtilities.addTwoDecimal(sumNotPlacedMVL, accountingAccountAux.getAmountMVL(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedUFV=CommonsUtilities.addTwoDecimal(sumNotPlacedUFV, accountingAccountAux.getAmountUFV(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        			sumNotPlacedEU=CommonsUtilities.addTwoDecimal(sumNotPlacedEU, accountingAccountAux.getAmountEU(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		        	        		}
		        				}
							}
	        				createTagString(xmlsw, TAG_LABEL_BALANCE ,    GeneralConstants.NOT_PLACED);
		            		createTagString(xmlsw, TAG_CARTERA_BOB ,      CommonsUtilities.getFormatOfAmount(sumNotPacedBOB, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_USD ,      CommonsUtilities.getFormatOfAmount(sumNotPlacedUSD, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_MV  ,      CommonsUtilities.getFormatOfAmount(sumNotPlacedMVL, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_UFV ,      CommonsUtilities.getFormatOfAmount(sumNotPlacedUFV, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_EURO ,     CommonsUtilities.getFormatOfAmount(sumNotPlacedEU, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS , CommonsUtilities.getFormatOfAmount(sumNotPlaced, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		amountOperNotPlaced = sumNotPlaced;
		            		totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperNotPlaced, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        			}else {
	        				createTagString(xmlsw, TAG_LABEL_BALANCE ,    GeneralConstants.NOT_PLACED);
		            		createTagString(xmlsw, TAG_CARTERA_BOB ,      CommonsUtilities.getFormatOfAmount(balanceNotPlaced.getBalanceBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_USD ,      CommonsUtilities.getFormatOfAmount(balanceNotPlaced.getBalanceUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_MV  ,      CommonsUtilities.getFormatOfAmount(balanceNotPlaced.getBalanceMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_UFV ,      CommonsUtilities.getFormatOfAmount(balanceNotPlaced.getBalanceUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_EURO ,     CommonsUtilities.getFormatOfAmount(balanceNotPlaced.getBalanceECU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS , CommonsUtilities.getFormatOfAmount(amountOperNotPlaced, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		            		totalAmountOperation=CommonsUtilities.addTwoDecimal(totalAmountOperation, amountOperNotPlaced, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        			}
	        		}
	        		
	        		//Contabilidad
	        		//25
	        		if(InstrumentType.FIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_FIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingNotPlaced=CommonsUtilities.addTwoDecimal(amountAccountingNotPlaced, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//26
	        		}else if(InstrumentType.VARIABLE_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_VARIABLE_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingNotPlaced=CommonsUtilities.addTwoDecimal(amountAccountingNotPlaced, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		//27
	        		}else if(InstrumentType.MIXED_INCOME.getCode().equals(accountingAccount.getInstrumentType())){
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_BOB_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountBOB(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_USD_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUSD(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_MV_MIXED_INCOME,  CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountMVL(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_UFV_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountUFV(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
	        			createTagString(xmlsw, TAG_ACCOUNT_EURO_MIXED_INCOME, CommonsUtilities.getFormatOfAmount(accountingAccount.getAmountEU(), 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		     	        amountAccountingNotPlaced=CommonsUtilities.addTwoDecimal(amountAccountingNotPlaced, accountingAccount.getAmountToBOB(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        		}
	        		iNoCol++;
	        		
	        		if(iNoCol==maxNoCol){
	        			createTagString(xmlsw, TAG_ACCOUNT_FIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IF_NOT_PLACED);
	        			createTagString(xmlsw, TAG_ACCOUNT_VARIABLE_INCOME, ReportConstant.LABEL_ACCOUNT_IV_NOT_PLACED);
	        			createTagString(xmlsw, TAG_ACCOUNT_MIXED_INCOME, ReportConstant.LABEL_ACCOUNT_IM_NOT_PLACED);
	        			
	        			createTagString(xmlsw, TAG_ACCOUNT_TOTAL_BOB, CommonsUtilities.getFormatOfAmount(amountAccountingNotPlaced, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		        		amountRemainderNotPlaced=CommonsUtilities.subtractTwoDecimal(amountOperNotPlaced, amountAccountingNotPlaced, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                totalRemainder=CommonsUtilities.addTwoDecimal(totalRemainder, amountRemainderNotPlaced, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		                createTagString(xmlsw, TAG_ACCOUNT_REMAINDER, CommonsUtilities.getFormatOfAmount(amountRemainderNotPlaced, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		                
		                
		                
		                xmlsw.writeEndElement();
	        		}
	        		
	                
	        		
	        	}

			}
        	

	        /**
	         * Sum To Total Amount Accounting
	         */
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingAvailable, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingReporting, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingOtherBlock, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingPhysical, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
	        totalAmountAccounting=CommonsUtilities.addTwoDecimal(totalAmountAccounting, amountAccountingNotPlaced, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
		}
		
		
		
		createTagString(xmlsw, TAG_TOTAL_CARTERA_TOTAL_BS, CommonsUtilities.getFormatOfAmount(totalAmountOperation, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		createTagString(xmlsw, TAG_TOTAL_ACCOUNTING_TOTAL_BS, CommonsUtilities.getFormatOfAmount(totalAmountAccounting, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		createTagString(xmlsw, TAG_TOTAL_ACCOUNTING_REMAINDER_TOTAL, CommonsUtilities.getFormatOfAmount(totalRemainder, 2, 25, GeneralConstants.FORMAT_AMOUNT_SECURITIES, true));
		
		
		xmlsw.writeEndElement();
		

	}
	
	

	/**
	 * Creates the empty report.
	 *
	 * @param xmlsw the xmlsw
	 * @throws XMLStreamException the XML stream exception
	 */
	private void createEmptyReport(XMLStreamWriter xmlsw) throws XMLStreamException {
		String empty="---";
		String zero="0";
		xmlsw.writeStartElement(TAG_SQUARING);
		xmlsw.writeStartElement(TAG_ACCOUNT);
		createTagString(xmlsw, TAG_LABEL_BALANCE ,      				 empty);
		createTagString(xmlsw, TAG_ACCOUNT_FIXED_INCOME ,      			 empty);
		createTagString(xmlsw, TAG_ACCOUNT_VARIABLE_INCOME ,      		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_MIXED_INCOME ,      		 	 empty);
		createTagString(xmlsw, TAG_CARTERA_BOB ,      					 empty);
		createTagString(xmlsw, TAG_CARTERA_USD ,      					 empty);
		createTagString(xmlsw, TAG_CARTERA_MV  ,     					 empty);
		createTagString(xmlsw, TAG_CARTERA_UFV ,      					 empty);
		createTagString(xmlsw, TAG_CARTERA_EURO ,      					 empty);
		createTagString(xmlsw, TAG_CARTERA_TOTAL_BS ,      			 	 empty);
		createTagString(xmlsw, TAG_ACCOUNT_BOB_FIXED_INCOME,       		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_USD_FIXED_INCOME,       		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_MV_FIXED_INCOME,       		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_UFV_FIXED_INCOME,       		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_EURO_FIXED_INCOME,       	 empty);

		createTagString(xmlsw, TAG_ACCOUNT_BOB_VARIABLE_INCOME,      	 empty);
		createTagString(xmlsw, TAG_ACCOUNT_USD_VARIABLE_INCOME,      	 empty);
		createTagString(xmlsw, TAG_ACCOUNT_MV_VARIABLE_INCOME,       	 empty);
		createTagString(xmlsw, TAG_ACCOUNT_UFV_VARIABLE_INCOME,      	 empty);
		createTagString(xmlsw, TAG_ACCOUNT_EURO_VARIABLE_INCOME,      	 empty);
		
		createTagString(xmlsw, TAG_ACCOUNT_BOB_MIXED_INCOME,       		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_USD_MIXED_INCOME,       		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_MV_MIXED_INCOME,       		 empty);
		createTagString(xmlsw, TAG_ACCOUNT_UFV_MIXED_INCOME,       	 	 empty);
		createTagString(xmlsw, TAG_ACCOUNT_EURO_MIXED_INCOME,       	 empty);
		
		createTagString(xmlsw, TAG_ACCOUNT_TOTAL_BOB,       	 empty);
		createTagString(xmlsw, TAG_ACCOUNT_REMAINDER,   	 empty);
		
		xmlsw.writeEndElement();
		
		createTagString(xmlsw, TAG_TOTAL_CARTERA_TOTAL_BS, empty);
		createTagString(xmlsw, TAG_TOTAL_ACCOUNTING_TOTAL_BS, empty);
		createTagString(xmlsw, TAG_TOTAL_ACCOUNTING_REMAINDER_TOTAL, empty);
		
		xmlsw.writeEndElement();
		
	}

}
