package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


@ReportProcess(name="CorporativeReportMarginOperationReport")
public class CorporativeReportMarginOperationReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static int ACCOUNT_NUMBER=0;
	private static int HOLDER_TYPE=1;
	private static int HOLDER_RNT=2;
	private static int FULL_NAME=3;
	private static int ALTERNATE_CODE=4;
	private static int PARTICIPANT=5;
	private static int ID_PARTICIPANT=6;
	private static int TOTAL_BALANCE=7;
	private static int REPORTING_BALANCE=8;
	private static int MARGIN_BALANCE=9;
	private static int INTEREST_TOTAL_BALANCE=10;
	private static int INTEREST_REPORT_BALANCE=11;
	private static int INTEREST_MARGIN_BALANCE=12;
	
	/*Constants for the header corporativeOperation
	 * 
	 */
	
	private static int ISSUER       = 0;
	private static int ISIN         = 1;
	private static int NOMINAL      = 2;
	private static int TAX          = 3;
	private static int INTEREST     = 4;
	private static int ROUND        = 5;
	private static int ISSUANCE     = 6;
	private static int CUPON_NUMBER = 7;
	private static int REGISTRY_DATE  = 8;
	private static int CUTOFF_DATE    = 9;
	private static int DELIVERY_DATE  = 10;
	private static int ALTERNATIVE_CODE=11;
	private static int CURRENCY=12;
	private static int COUPON_BEGIN_DATE=13;
	private static int COUPING_END_DATE=14;

	@Inject
	private CorporativeReportServiceBean corporativeReportServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {


		CorporativeOperationTO corporativeOperationTO = this.getReportToFromReportLogger(reportLogger);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		try{

 
			List<Object[]> list = this.corporativeReportServiceBean.searchBalanceByCorporativeForReportAndMargin(corporativeOperationTO);
            Object[] tempObjects = this.corporativeReportServiceBean.searchCorporativeOperationByFilter(corporativeOperationTO);

			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(arrayOutputStream));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			//Create all the headers of the reports for no result statement
			createTagString(xmlsw, "issuer", tempObjects[ISSUER]);
			createTagString(xmlsw, "isin_code", tempObjects[ISIN]);
			createTagString(xmlsw, "issuance", tempObjects[ISSUANCE]);
			
			createTagDecimal(xmlsw, "nominal", tempObjects[NOMINAL]);
			DecimalFormat format = new DecimalFormat("###.0000");
			DecimalFormat interestFormat = new DecimalFormat("0.0000");
			Double interest =Double.parseDouble(tempObjects[INTEREST].toString());
			Double tax =Double.parseDouble(tempObjects[TAX].toString());
			
			String strInterest = interestFormat.format(interest);
			String strTax = format.format(tax);
			Double decimalTax = new Double(tax);
			decimalTax = decimalTax*100;
			
			//BigDecimal result = decimalTax.multiply(new BigDecimal(100));
			if(tax.equals(0.0)){
				createTagString(xmlsw, "tax", "0%");

			}
			else{
				createTagString(xmlsw, "tax", format.format(decimalTax)+"%");

			}
			//strInterest = strInterest + "%";
			String temp = strInterest+"%";
			createTagString(xmlsw, "interest", temp);
			
			 
			createTagString(xmlsw, "round", tempObjects[ROUND]);
			createTagString(xmlsw, "coupon", tempObjects[CUPON_NUMBER]);
			createTagString(xmlsw, "cutoff_date", tempObjects[CUTOFF_DATE]);
			createTagString(xmlsw, "delivery_date", tempObjects[DELIVERY_DATE]);
			createTagString(xmlsw, "registry_date", tempObjects[REGISTRY_DATE]);
			String state = CorporateProcessStateType.get(corporativeOperationTO.getState()).getValue();
			createTagString(xmlsw, "corporate_state", state);
			createTagString(xmlsw, "alternate_code", tempObjects[ALTERNATIVE_CODE]);
			createTagString(xmlsw, "currency", tempObjects[CURRENCY]);
			createTagString(xmlsw, "cupon_begin_date", tempObjects[COUPON_BEGIN_DATE].toString());
			createTagString(xmlsw, "cupon_expiration_date", tempObjects[COUPING_END_DATE].toString());

		 
			if(Validations.validateListIsNotNullAndNotEmpty(list)){
				/** Creating the body **/
				createBodyReport(xmlsw, list);
			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
			xmlsw.close();
		}catch(XMLStreamException e){
			e.printStackTrace();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return arrayOutputStream;

	}
	
	public boolean validateReportOrMarginBalance(Object[] objects){
		BigDecimal margin = new BigDecimal(objects[MARGIN_BALANCE].toString());
		BigDecimal report = new BigDecimal(objects[REPORTING_BALANCE].toString());
		if(margin.compareTo(BigDecimal.ONE)==BooleanType.YES.getCode() || report.compareTo(BigDecimal.ONE)==BooleanType.YES.getCode()){
			return true;
		}
		
		return false;
	}


	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> listOperations){
		try {
			for(Object[] temp: listOperations){
				if(validateReportOrMarginBalance(temp)){

				xmlsw.writeStartElement("operation");
				Integer personType=Integer.parseInt(temp[HOLDER_TYPE].toString());
				String strPersonType="";
				if(PersonType.NATURAL.getCode().equals(personType)){
					strPersonType = "N";
				}
				else if(PersonType.JURIDIC.getCode().equals(personType)){
					strPersonType = "J";
				}
				createTagString(xmlsw, "person_type",    strPersonType);
				 
				createTagString(xmlsw, "cuenta",         temp[ACCOUNT_NUMBER]);
				createTagString(xmlsw, "holder",         temp[FULL_NAME]);
				createTagString(xmlsw, "rnt", temp[HOLDER_RNT]);
				//createTagString(xmlsw, "alternate_code", temp[ALTERNATE_CODE]);
				createTagString(xmlsw, ReportConstant.PARTICIPANT_PARAM, temp[PARTICIPANT]);
				createTagString(xmlsw, "participant_pk",   temp[ID_PARTICIPANT]);
				createTagDecimal(xmlsw, "total_balance",    temp[TOTAL_BALANCE]);
				createTagDecimal(xmlsw, "report_balance",   temp[REPORTING_BALANCE]);
				createTagDecimal(xmlsw, "margin_balance",   temp[MARGIN_BALANCE]);
				createTagDecimal(xmlsw, "i_total_balance",  temp[INTEREST_TOTAL_BALANCE]);
				createTagDecimal(xmlsw, "i_report_balance", temp[INTEREST_REPORT_BALANCE]);
				createTagDecimal(xmlsw, "i_margin_balance", temp[INTEREST_MARGIN_BALANCE]);
				xmlsw.writeEndElement();
			}
			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Object[] findCorporativeOperationByFilter(CorporativeOperationTO filter){
		return corporativeReportServiceBean.searchCorporativeOperationByFilter(filter);		
 	}
	
	
	public CorporativeOperationTO getReportToFromReportLogger(ReportLogger reportLogger){
		CorporativeOperationTO corporativeOperationTO = new CorporativeOperationTO();
		for(ReportLoggerDetail detail:reportLogger.getReportLoggerDetails()){
			if(ReportConstant.CORP_PROCESS_ID.equals(detail.getFilterName())){
				corporativeOperationTO.setId(detail.getFilterValue()!=null?Long.parseLong(detail.getFilterValue()):null);
			}
			else if(ReportConstant.CUTOFF_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setCutOffDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}else if(ReportConstant.DELIVERY_DATE.equals(detail.getFilterName())){
				corporativeOperationTO.setDeliveryDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue()));
			}
			else if(ReportConstant.CORPORATE_EVENT_TYPE.equals(detail.getFilterName())){
				corporativeOperationTO.setCorporativeEventType(Integer.parseInt(detail.getFilterValue()));
			}
			else if(ReportConstant.CODE_ISIN.equals(detail.getFilterName())){
				corporativeOperationTO.setSourceIsinCode(detail.getFilterValue());
			}
			else if(ReportConstant.DEST_CODE_ISIN.equals(detail.getFilterName()))
			{
				if(detail.getFilterValue()!=null){
					corporativeOperationTO.setTargetIsinCode(detail.getFilterValue());
				}
			}
			else if(ReportConstant.STATE_PARAM.equals(detail.getFilterName())){
				corporativeOperationTO.setState(Integer.parseInt(detail.getFilterValue()));
			}
		}
		
		return corporativeOperationTO;
	}

}
