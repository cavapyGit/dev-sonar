package com.pradera.report.generation.executor.accounting;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.accounting.service.AccountingGeneratorReportServiceBean;
import com.pradera.report.generation.executor.accounting.to.AccountingMatrixDetailTo;
import com.pradera.report.generation.executor.accounting.to.AccountingProcessTo;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSquaringReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name="AccountingSquaringReport")
public class AccountingSquaringReport extends GenericReport{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The accounting poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean accountingPoiGeneratorServiceBean;

	/** The accounting generator report service bean. */
	@Inject
	private AccountingGeneratorReportServiceBean accountingGeneratorReportServiceBean;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/**
	 * Instantiates a new accounting squaring report.
	 */
	public AccountingSquaringReport() {
		
	}
	
	/** The list accounting matrix detail. */
	List<AccountingMatrixDetailTo>	listAccountingMatrixDetail;
	
	/** The list accounting account. */
	List<AccountingAccount>	listAccountingAccount;
	
	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		
		AccountingProcessTo accountingProcessTo=new AccountingProcessTo();
		StringBuilder sbParametersReport = new StringBuilder();
		Map<String, Object> 			parameters = new HashMap<String, Object>();
		boolean existData = false;
		for(ReportLoggerDetail loggerDetail : reportLogger.getReportLoggerDetails()) {

			if(loggerDetail.getFilterName().equals(ReportConstant.PROCESS_ACCOUNTING_PK)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setIdAccountingProcessPk(Long.parseLong(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
			else if(loggerDetail.getFilterName().equals(ReportConstant.BRANCH)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setProcessType(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
			else if(loggerDetail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setExecutionDate(CommonsUtilities.convertStringtoDate(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
			else if(loggerDetail.getFilterName().equals(GeneralConstants.REPORT_BY_PROCESS)){
				
				if (StringUtils.isNotBlank(loggerDetail.getFilterValue())) {
					accountingProcessTo.setReportByProcess(Integer.parseInt(loggerDetail.getFilterValue()));
					sbParametersReport.append(loggerDetail.getFilterValue()).append(GeneralConstants.STR_COMMA);
				}
			}
		}
		accountingProcessTo.setReportByProcess(BooleanType.YES.getCode());
		accountingProcessTo.setStartType(AccountingStartType.CONTINUE.getCode());
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		byte[] arrayByteExcel = new byte[16384];
		try {
			parameters=this.accountingGeneratorReportServiceBean.findSquaringOperations(accountingProcessTo);
			
//		if(!listAccountingAccount.isEmpty()){
			
			existData = true;
			
//		}
		
		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		XMLStreamWriter xmlsw;
			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
		xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
		//root tag of report
		/**
		 * report
		 * */
		xmlsw.writeStartElement(ReportConstant.REPORT);
		
		
		/**
		 * Create to header report
		 * start_hour
		 * report_title
		 * mnemonic_report
		 * clasification_report
		 * generation_date
		 * user_name
		 * mnemonic_entity
		 * **/
		
		createHeaderReport(xmlsw, reportLogger);

		if(existData) {
			
			/**GENERATE FILE EXCEL ***/
			arrayByteExcel=accountingPoiGeneratorServiceBean.writeFileExcelAccountingSquare(parameters,reportLogger,accountingProcessTo);

		}
		else{
			arrayByteExcel=accountingPoiGeneratorServiceBean.writeFileExcelAccountingSquare(parameters,reportLogger,accountingProcessTo);
		}
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
//		setArrayByte(arrayByteExcel);
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		createTagString(xmlsw,ReportConstant.REPORT_ID,reportLogger.getReport().getIdReportPk());
		
		
		//time finish and parameters
		String hourEnd = CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),
				ReportConstant.DATE_FORMAT_TIME_SECOND);
		createTagString(xmlsw, ReportConstant.END_HOUR, hourEnd);
		//parameters
		String parametersFilter = Validations.validateIsNotNullAndNotEmpty(sbParametersReport) ? sbParametersReport.substring(0, sbParametersReport.toString().length() -2) : GeneralConstants.EMPTY_STRING;
		createTagString(xmlsw, ReportConstant.PARAMETERS_TAG_REPORT, parametersFilter);
		//close report tag
		xmlsw.writeEndElement();
		//close document
		xmlsw.writeEndDocument();
		xmlsw.flush();
        xmlsw.close();
		/**FILE**/
		
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baos;
	}
}
