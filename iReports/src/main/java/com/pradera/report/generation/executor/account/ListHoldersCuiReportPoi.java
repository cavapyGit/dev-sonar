package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.issuances.to.ListHoldersCuiReportTO;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "ListHoldersCuiReportPoi")
public class ListHoldersCuiReportPoi extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private AccountReportServiceBean accountReportServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		ListHoldersCuiReportTO listHoldersCuiReportTo = new ListHoldersCuiReportTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parameters = new HashMap<>();
		for (int i = 0; i < listaLogger.size(); i++) {

			if (listaLogger.get(i).getFilterName().equals("doc_type"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setDocType(Integer.parseInt(listaLogger.get(i).getFilterValue()));

			if (listaLogger.get(i).getFilterName().equals("participant"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setParticipant(Integer.parseInt(listaLogger.get(i).getFilterValue()));
			
			if (listaLogger.get(i).getFilterName().equals("doc_number"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setDocNumber(listaLogger.get(i).getFilterValue());
			
			if (listaLogger.get(i).getFilterName().equals("state_holder"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setStateHolder(Integer.parseInt(listaLogger.get(i).getFilterValue()));
			
			if (listaLogger.get(i).getFilterName().equals("securities_balance"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setSecuritiesBalance(listaLogger.get(i).getFilterValue());
			
			if (listaLogger.get(i).getFilterName().equals("type_person"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setTypePerson(Integer.parseInt(listaLogger.get(i).getFilterValue()));
			
			if (listaLogger.get(i).getFilterName().equals("economic_activity"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setEconomicActivity(Integer.parseInt(listaLogger.get(i).getFilterValue()));
			
			if (listaLogger.get(i).getFilterName().equals("date_initial"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setDateInitial(listaLogger.get(i).getFilterValue());

			if (listaLogger.get(i).getFilterName().equals("date_end"))
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
					listHoldersCuiReportTo.setDateEnd(listaLogger.get(i).getFilterValue());

		}
		
		// Checking if the doc_number field not is empty and null
		if (listHoldersCuiReportTo.getDocNumber() != null
				&& !listHoldersCuiReportTo.getDocNumber().trim().equals("")) {
			listHoldersCuiReportTo.setDateInitial(null);
			listHoldersCuiReportTo.setDateEnd(null);
		}
				
		String strQueryFormated = accountReportServiceBean.getQueryListHoldersCui(listHoldersCuiReportTo);
		List<Object[]> lstObject = accountReportServiceBean.getQueryListByClass(strQueryFormated);
		parameters.put("lstObjects", lstObject);
		parameters.put("dateInitial", listHoldersCuiReportTo.getDateInitial());
		parameters.put("dateEnd", listHoldersCuiReportTo.getDateEnd());
		
		/**GENERATE FILE EXCEL ***/
		arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelListHoldersCui(parameters, reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);
		
		return baos;
	}

}
