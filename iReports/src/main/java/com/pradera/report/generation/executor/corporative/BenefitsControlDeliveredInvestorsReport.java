package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.swift.to.SwiftConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcessResultTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
					 
@ReportProcess(name="BenefitsControlDeliveredInvestorsReport")
public class BenefitsControlDeliveredInvestorsReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	PraderaLogger log;
	@EJB
	CorporativeReportServiceBean corporativeReportServiceBean;
	@EJB
	private ParameterServiceBean parameterService;
	private static int ID_CORPORATIVE_PROCESS  = 0;
	private static int ID_PARTICIPANT		   = 1;
	private static int PARTICIPANT_MNEMONIC	   = 2;
	private static int ID_HOLDER_ACCOUNT	   = 3;
	private static int ACCOUNT_NUMBER		   = 4;
	private static int BENEFIT_TYPE			   = 5;
	private static int ISIN_CODE			   = 6;	
	private static int DELIVERY_FACTOR		   = 7;
	private static int TOTAL_BALANCE		   = 8;
	private static int DELIVERY_DATE		   = 9;	
	private static int CURRENCY				   = 10;
	private static int IND_ORIGIN_TARGET	   = 11;
	private static int REGISTRY_DATE	   	   = 12;
	private static int CUSTODY_COM	   		   = 13;
	private static int TAX	   				   = 14;
	private static int PENALITY				   = 15;
	private static int ID_HOLDER			   = 16;
	private static int FULL_NAME		   	   = 17;
	private static int DOCUMENT_TYPE   		   = 18;
	private static int DOCUMENT_NUMBER		   = 19;
	private static int LEGAL_ADDRESS		   = 20;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		CorporativeProcessResultTO corporativeProcessResultTO = this.reportLoggerCorporativeProcessResultTO(reportLogger);
				
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		try{
			List<Object[]> objLst = this.corporativeReportServiceBean.getBenefitsControlDeliveredInvestorsInfo(corporativeProcessResultTO);
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(arrayOutputStream));
			//open document file
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");
			//root tag of report
			/**
			 * report
			 **/
			xmlsw.writeStartElement(ReportConstant.REPORT);
			/**
			 * Create to header report
			 * start_hour
			 * report_title
			 * mnemonic_report
			 * clasification_report
			 * generation_date
			 * user_name
			 * mnemonic_entity
			 * **/
			createHeaderReport(xmlsw, reportLogger);
			String initalDateStr = CommonsUtilities.convertDateToString(corporativeProcessResultTO.getInitialDate(),ReportConstant.DATE_FORMAT_STANDAR);
			String endDateStr = CommonsUtilities.convertDateToString(corporativeProcessResultTO.getFinalDate(),ReportConstant.DATE_FORMAT_STANDAR);
			String fromTo = "DESDE: " + initalDateStr + " HASTA: " + endDateStr;
			
			createTagString(xmlsw, "fromTo", fromTo);
			
			xmlsw.writeStartElement("CorporativeProcessResult");
			if(Validations.validateListIsNotNullAndNotEmpty(objLst)){
				/** Creating the body **/
				createBodyReport(xmlsw, objLst);
			}else{
				createTagString(xmlsw,"validation","NO SE ENCONTRARON REGISTROS");
			}
			//END REPORT TAG
			xmlsw.writeEndElement();
			//close document
			xmlsw.writeEndDocument();
			xmlsw.flush();
	        xmlsw.close();
		}catch(ServiceException | XMLStreamException e){
			log.error(e.getMessage());
		}
		return arrayOutputStream;
	}
	
	
	public void createBodyReport(XMLStreamWriter xmlsw,List<Object[]> objLst)  throws XMLStreamException, ServiceException{
		//xmlsw.writeStartElement("rows"); //START ROWS TAG
		
		Map<Long,Double>totalAmountMap = new HashMap<Long,Double>();
		Map<Long,Long>totalActionsMap = new HashMap<Long,Long>();
		String emptyString="---";
		String zero="0";
		Double totalAmountDOP = new Double(0);
		Double totalAmountUSD = new Double(0);
		Long totalActions = new Long(0);
		//BY ROW
		for(Object[] corporativeProcessResult : objLst){
			Long idHolderAccountPk = new Long(corporativeProcessResult[ID_HOLDER_ACCOUNT].toString());
			//Holder holder = this.corporativeReportServiceBean.searchHolders(idHolderAccountPk).get(0);	
			BigDecimal neto= new BigDecimal(0), totalBalance= new BigDecimal(0), custCom= new BigDecimal(0),
					   tax = new BigDecimal(0), penality=new BigDecimal(0);
			boolean totBalanceNull=true;
			String currency=SwiftConstants.CURRENCY_PESOS_DOMINICANOS;
			xmlsw.writeStartElement("row"); //START row ELEMENT
			if(corporativeProcessResult[FULL_NAME]!=null)
				createTagString(xmlsw,"RNT_NAME",corporativeProcessResult[FULL_NAME]);
			else
				createTagString(xmlsw,"RNT_NAME",emptyString);
			if(corporativeProcessResult[ID_HOLDER]!=null)
				createTagString(xmlsw,"RNT_CODE",corporativeProcessResult[ID_HOLDER]);
			else
				createTagString(xmlsw,"RNT_CODE",emptyString);
			if(corporativeProcessResult[LEGAL_ADDRESS]!=null)
				createTagString(xmlsw,"RNT_LEGAL_ADDRESS", corporativeProcessResult[LEGAL_ADDRESS]);
			else
				createTagString(xmlsw,"RNT_LEGAL_ADDRESS",emptyString);
			Integer cedulaId=86;
			Integer rnc=386;
			Integer dio=390;
			if(corporativeProcessResult[DOCUMENT_TYPE]!=null && corporativeProcessResult[DOCUMENT_NUMBER]!=null){
				if(cedulaId==corporativeProcessResult[DOCUMENT_TYPE]){
					createTagString(xmlsw,"RNT_DOCUMENT","CI" + " " + corporativeProcessResult[DOCUMENT_NUMBER]);
				}else if(rnc.intValue()==((Integer)corporativeProcessResult[DOCUMENT_TYPE]).intValue()){
					createTagString(xmlsw,"RNT_DOCUMENT","RNC" + " " + corporativeProcessResult[DOCUMENT_NUMBER]);
				}else if(dio==corporativeProcessResult[DOCUMENT_TYPE]){
					createTagString(xmlsw,"RNT_DOCUMENT","DIO" + " " + corporativeProcessResult[DOCUMENT_NUMBER]);
				}else{
					createTagString(xmlsw,"RNT_DOCUMENT", DocumentType.get(Integer.valueOf(corporativeProcessResult[DOCUMENT_TYPE].toString())).getValue() + 
														" " + corporativeProcessResult[DOCUMENT_NUMBER]);
				}
			}else{
				createTagString(xmlsw,"RNT_DOCUMENT",emptyString);
			}
		
				if(corporativeProcessResult[ISIN_CODE]!=null)
					createTagString(xmlsw,"ISIN_CODE",corporativeProcessResult[ISIN_CODE]);
				else
					createTagString(xmlsw,"ISIN_CODE",emptyString);
				
				if(corporativeProcessResult[DELIVERY_DATE]!=null)
					createTagString(xmlsw,"DELIVERY_DATE", CommonsUtilities.convertDatetoString((Date)corporativeProcessResult[DELIVERY_DATE]));
				else
					createTagString(xmlsw,"DELIVERY_DATE",emptyString);
				if(corporativeProcessResult[REGISTRY_DATE]!=null)
					createTagString(xmlsw,"REGISTRY_DATE", CommonsUtilities.convertDatetoString((Date)corporativeProcessResult[REGISTRY_DATE]));
				else
					createTagString(xmlsw,"REGISTRY_DATE",emptyString);
				if(corporativeProcessResult[ACCOUNT_NUMBER]!=null)
					createTagString(xmlsw,"ACCOUNT_NUMBER",corporativeProcessResult[ACCOUNT_NUMBER]);
				else
					createTagString(xmlsw,"ACCOUNT_NUMBER",emptyString);
				if(corporativeProcessResult[DELIVERY_FACTOR]!=null)
					createTagString(xmlsw,"DELIVERY_FACTOR",((BigDecimal)corporativeProcessResult[DELIVERY_FACTOR]).doubleValue() + "%");
				else
					createTagString(xmlsw,"DELIVERY_FACTOR",emptyString);
				if(corporativeProcessResult[BENEFIT_TYPE]!=null)
					createTagString(xmlsw,"BENEFIT_TYPE",ImportanceEventType.get((Integer)corporativeProcessResult[BENEFIT_TYPE]).getValue());
				else
					createTagString(xmlsw,"BENEFIT_TYPE",emptyString);
				if(corporativeProcessResult[PARTICIPANT_MNEMONIC]!=null && corporativeProcessResult[ID_PARTICIPANT]!=null)
					createTagString(xmlsw,"PART_MNEMONIC_ID",corporativeProcessResult[PARTICIPANT_MNEMONIC].toString() + " - " + corporativeProcessResult[ID_PARTICIPANT]);
				else
					createTagString(xmlsw,"PART_MNEMONIC_ID",emptyString);		
				
				//ACTIONS
				if(ImportanceEventType.ACTION_DIVIDENDS.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE])
				|| ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE])
				|| ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE])
				&& (corporativeProcessResult[IND_ORIGIN_TARGET].toString().equals(ReportConstant.CORPORATIVE_BALANCE_ORIGIN))){
					//CREATING ACTION BALANCE TAG
					if(corporativeProcessResult[TOTAL_BALANCE]!=null){
						createTagString(xmlsw,"ACTIONS_BALANCE", corporativeProcessResult[TOTAL_BALANCE]);
						totBalanceNull=false;
					}else
						createTagString(xmlsw,"ACTIONS_BALANCE", zero);
					//CREATING NORMAL AMOUNT BALANCE TAG
					createTagString(xmlsw,"NORMAL_AMOUNT", zero);
										
					//VALIDATING IF TOTAL ACTION MAP IS EMPTY AND TOTAL_BALANCE SHOULD BE NOT NULL
					if(!totBalanceNull){
						if(totalActionsMap.isEmpty()){
							totalActions =((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).longValue();
							totalActionsMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalActions);//ADDING ID HOLDER AND TOTAL ACTIONS					
						}else if(Validations.validateIsNotNull(totalActionsMap.get(corporativeProcessResult[ID_HOLDER]))){
							totalActions += totalActionsMap.get(corporativeProcessResult[ID_HOLDER]);//GETTING THE TOTAL ACTIONS OF THE HOLDER
							totalActions += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).longValue();//ADDING TOTAL ACTIONS AND TOTAL ACTIONS OF THE ACCOUNT
							totalActionsMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalActions);	//MODIFIYING THE HOLDER IN THE MAP
						}else{
							totalActions += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).longValue();
							totalActionsMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalActions);	
						}
					}
				//AMOUNT
				}else if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE])
					  || ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE]) 						  
					  || ImportanceEventType.REMANENT_PAYMENT.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE])
					  || ImportanceEventType.INTEREST_PAYMENT.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE])
					  || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals((Integer)corporativeProcessResult[BENEFIT_TYPE])
					  && (corporativeProcessResult[IND_ORIGIN_TARGET].toString().equals(ReportConstant.CORPORATIVE_BALANCE_DESTINY))){
					//CREATING NORMAL AMOUNT BALANCE TAG
					if(corporativeProcessResult[CURRENCY]!=null)
						currency=CurrencyType.get((Integer)corporativeProcessResult[CURRENCY]).getCodeIso();
					if(corporativeProcessResult[TOTAL_BALANCE]!=null){
						totBalanceNull=false;
						totalBalance=(BigDecimal)corporativeProcessResult[TOTAL_BALANCE];
						String decimalMonto=formatDecimal(corporativeProcessResult[TOTAL_BALANCE]);
						createTagString(xmlsw,"NORMAL_AMOUNT", currency + " " + decimalMonto);
					}else
						createTagDecimal(xmlsw,"NORMAL_AMOUNT", currency + " " + totalBalance);
					//CREATING ACTION BALANCE TAG
					createTagString(xmlsw,"ACTIONS_BALANCE", zero);
					if(!totBalanceNull){
						//depending of the currency store total_balance
						if(SwiftConstants.CURRENCY_PESOS_DOMINICANOS.equals(currency)){
							if(totalAmountMap.isEmpty()){
								totalAmountDOP += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).doubleValue();
								totalAmountMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalAmountDOP);					
							}else if(Validations.validateIsNotNull(totalAmountMap.get(corporativeProcessResult[ID_HOLDER]))){
								totalAmountDOP += totalAmountMap.get(corporativeProcessResult[ID_HOLDER]);
								totalAmountDOP += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).doubleValue();
								totalAmountMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalAmountDOP);	
							}else{
								totalAmountDOP += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).doubleValue();
								totalAmountMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalAmountDOP);	
							}	
						}else if(SwiftConstants.CURRENCY_DOLLARS.equals(currency)){
							if(totalAmountMap.isEmpty()){
								totalAmountUSD += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).doubleValue();
								totalAmountMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalAmountUSD);					
							}else if(Validations.validateIsNotNull(totalAmountMap.get(corporativeProcessResult[ID_HOLDER]))){
								totalAmountUSD += totalAmountMap.get(corporativeProcessResult[ID_HOLDER]);
								totalAmountUSD += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).doubleValue();
								totalAmountMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalAmountUSD);	
							}else{
								totalAmountUSD += ((BigDecimal)(corporativeProcessResult[TOTAL_BALANCE])).doubleValue();
								totalAmountMap.put(Long.valueOf(corporativeProcessResult[ID_HOLDER].toString()),totalAmountUSD);	
							}
						}
					}				
				}else{
					createTagDecimal(xmlsw,"NORMAL_AMOUNT", totalBalance);
					createTagString(xmlsw,"ACTIONS_BALANCE", zero);
				}
				if(totalActions>0)
					createTagString(xmlsw,"ACTIONS_TOTAL_BALANCE", totalActions.toString());
				else
					createTagString(xmlsw,"ACTIONS_TOTAL_BALANCE", totalActions);
				createTagDecimal(xmlsw,"NORMAL_TOTAL_BALANCE_DOP", totalAmountDOP);
				createTagDecimal(xmlsw,"NORMAL_TOTAL_BALANCE_USD", totalAmountUSD);
				if(corporativeProcessResult[CUSTODY_COM]!=null){
					custCom=(BigDecimal)corporativeProcessResult[CUSTODY_COM];
					createTagDecimal(xmlsw,"CUSTODY_COM", custCom);
				}else
					createTagDecimal(xmlsw,"CUSTODY_COM", custCom);
				if(corporativeProcessResult[TAX]!=null){
					tax=(BigDecimal)corporativeProcessResult[TAX];
					createTagDecimal(xmlsw,"TAX", tax);
				}else
					createTagDecimal(xmlsw,"TAX", tax);
				if(corporativeProcessResult[PENALITY]!=null){
					penality=(BigDecimal)corporativeProcessResult[PENALITY];
					createTagDecimal(xmlsw,"PENALITY", penality);
				}else
					createTagDecimal(xmlsw,"PENALITY", penality);
				//getting neto
				neto = totalBalance.subtract(custCom).subtract(tax).add(penality);
				createTagDecimal(xmlsw,"TOTAL_NETO", neto);

			xmlsw.writeEndElement();	//END ROW TAG
		}	
		//xmlsw.writeEndElement();	//END ROWS TAG
	}
	
	public CorporativeProcessResultTO reportLoggerCorporativeProcessResultTO(ReportLogger reportLogger){
		CorporativeProcessResultTO corporativeProcessResultTO = new CorporativeProcessResultTO();		
		for (ReportLoggerDetail detail : reportLogger.getReportLoggerDetails()) {
			if( detail.getFilterName().equals(ReportConstant.RNT_CODE_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeProcessResultTO.setRntCode(new Long(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.PARTICIPANT_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeProcessResultTO.setParticipant(new Long(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.ISIN_CODE_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeProcessResultTO.setIsinCode((detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.DATE_INITIAL_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeProcessResultTO.setInitialDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.DATE_END_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeProcessResultTO.setFinalDate(CommonsUtilities.convertStringtoDate(detail.getFilterValue().toString()));
				}
			}else if( detail.getFilterName().equals(ReportConstant.ACCOUNT_NUMBER_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeProcessResultTO.setAccountNumber(new Integer(detail.getFilterValue()));
				}
			}
			else if(detail.getFilterName().equals(ReportConstant.BENEFIT_TYPE_PARAM) ){
				if(Validations.validateIsNotNull(detail.getFilterValue())){
					corporativeProcessResultTO.setBenefitType(new Integer(detail.getFilterValue()));
				}
			}
		}
		//IF BENEFIT TYPE IS NULL THEN I CHOOSE EVERY BENEFIT TYPE
		if(Validations.validateIsNull(corporativeProcessResultTO.getBenefitType())){
			List<Integer>idBenefitsLst = new ArrayList<Integer>();
			idBenefitsLst.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
			idBenefitsLst.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
			idBenefitsLst.add(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode());
			idBenefitsLst.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
			idBenefitsLst.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
			idBenefitsLst.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
			idBenefitsLst.add(ImportanceEventType.REMANENT_PAYMENT.getCode());
			idBenefitsLst.add(ImportanceEventType.INTEREST_PAYMENT.getCode());	
			corporativeProcessResultTO.setBenefitList(idBenefitsLst);
		}
		return corporativeProcessResultTO;
	}
}
