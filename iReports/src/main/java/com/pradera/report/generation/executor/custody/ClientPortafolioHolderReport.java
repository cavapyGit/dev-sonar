package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.to.ClientPortafolioTO;
import com.pradera.report.generation.executor.securities.service.IssuerListReportServiceBean;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ClientPortafolioHolderReport")
public class ClientPortafolioHolderReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	@EJB
	private CustodyReportServiceBean custodyReportService;
	@EJB
	private ReportUtilServiceBean reportUtilServiceBean;
	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	@EJB
	private IssuerListReportServiceBean issuerServiceBean;
	@Inject
	private PraderaLogger log;

	private ClientPortafolioTO clientPortafolioTO;
	
	public ClientPortafolioHolderReport() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		String title=null;
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		clientPortafolioTO = new ClientPortafolioTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("participant")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdParticipant(Long.valueOf(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setParticipantDesc(listaLogger.get(i).getFilterDescription());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("cui_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setCui(new Long(listaLogger.get(i).getFilterValue()).longValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_class")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setSecurityClass(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("currency")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdCurrency(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("security_code")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdSecurityCode(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("portafolio_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setPortafolioType(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("available_balance")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setShowAvailableBalance(Integer.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					
					clientPortafolioTO.setDateMarketFact(CommonsUtilities.convertStringtoDate(listaLogger.get(i).getFilterValue()));
					clientPortafolioTO.setDateMarketFactString(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("account_holder")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setHolderAccountPk(Long.valueOf(listaLogger.get(i).getFilterValue()));
				}
			}
			if(listaLogger.get(i).getFilterName().equals("issuer")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setIdIssuer(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("institution_type")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					clientPortafolioTO.setInstitutionType(listaLogger.get(i).getFilterValue());
				}
			}
		}
		//VALIDAMOS SI ES TIPO PARTICIPANTE INVERSIONISTA
		clientPortafolioTO.setIsParticipantInvestor(false);
		if(clientPortafolioTO.getInstitutionType()!=null && Integer.valueOf(clientPortafolioTO.getInstitutionType()).equals(InstitutionType.PARTICIPANT_INVESTOR.getCode())){
			clientPortafolioTO.setIsParticipantInvestor(true);
		}
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		try{
			custodyReportService.getExchangeRate(clientPortafolioTO.getDateMarketFactString());
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + clientPortafolioTO.getDateMarketFactString());
			}
			log.error(e.getMessage());
		}
		
		Long idStkCalcProcess= null;
		String holderAccountNumber = GeneralConstants.EMPTY_STRING;
		String CUI = GeneralConstants.EMPTY_STRING;
		String issuerDescription = GeneralConstants.EMPTY_STRING;
		String securityClassDesc = GeneralConstants.EMPTY_STRING;

			ParameterTableTO  filter = new ParameterTableTO();
			Map<Integer,String> currencyTypeShort = new HashMap<Integer, String>();
			Map<Integer,String> currencyType = new HashMap<Integer, String>();
			Map<Integer,String> securityClassShort = new HashMap<Integer, String>();
			Map<Integer,String> securityClass = new HashMap<Integer, String>();
			try {
				filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
					currencyTypeShort.put(param.getParameterTablePk(), param.getText1());
					currencyType.put(param.getParameterTablePk(), param.getParameterName());
				}
				filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
					securityClassShort.put(param.getParameterTablePk(), param.getText1());
					securityClass.put(param.getParameterTablePk(), param.getParameterName());
				}
				//PARA LA CUENTA TITULAR
				if(clientPortafolioTO.getHolderAccountPk() != null)
					holderAccountNumber = accountComponentServiceBean.find(clientPortafolioTO.getHolderAccountPk(),HolderAccount.class).getAccountNumber().toString();
				else
					holderAccountNumber = "Todos";
				//PARA EL CUI DEL TITULAR
				if(clientPortafolioTO.getCui() != null)
					CUI = clientPortafolioTO.getCui().toString();
				else
					CUI = "Todos";
				//EMISOR
				if(clientPortafolioTO.getIdIssuer() != null){
					Issuer iss = issuerServiceBean.find(Issuer.class, clientPortafolioTO.getIdIssuer());
					issuerDescription = iss.getMnemonic() + " - " + iss.getBusinessName();
				}else{
					issuerDescription = "Todos";
				}
				//CLASE DE VALOR
				if(clientPortafolioTO.getSecurityClass() != null){
					ParameterTable parameterClass = parameterService.getParameterTableById(Integer.parseInt(clientPortafolioTO.getSecurityClass()));
					securityClassDesc=parameterClass.getParameterName();
				}else{
					securityClassDesc = "Todos";
				}
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			String strQuery = null;
			String strSubTotalQuery = null;
			
			if(clientPortafolioTO.getPortafolioType().equals(IssuanceType.DEMATERIALIZED.getCode())){
				title="RELACION DE TENENCIA DE VALORES DESMATERIALIZADOS POR TITULAR";
				strQuery = custodyReportService.getQueryClientPortafolio(clientPortafolioTO, idStkCalcProcess);
				strSubTotalQuery = custodyReportService.getSubTotalQueryClientPortafolio(clientPortafolioTO, idStkCalcProcess);
			}else{
				title="RELACION DE TENENCIA DE VALORES FISICOS POR TITULAR";
				strQuery = custodyReportService.getQueryClientPortafolio(clientPortafolioTO);
				strSubTotalQuery = custodyReportService.getSubTotalQueryClientPortafolio(clientPortafolioTO);
			}
			
			parametersRequired.put("report_title_customize", title);
			parametersRequired.put("str_query", strQuery);
			parametersRequired.put("str_subTotalQuery", strSubTotalQuery);
			parametersRequired.put("p_securityClass", securityClassShort);
			parametersRequired.put("p_securityClassDescription", securityClass);
			parametersRequired.put("p_participant_desc", clientPortafolioTO.getParticipantDesc());
			parametersRequired.put("p_currency", currencyTypeShort);
			parametersRequired.put("p_description", currencyType);
			parametersRequired.put("p_currency_desc", currencyType.get(clientPortafolioTO.getIdCurrency()));
			parametersRequired.put("p_account_holder", holderAccountNumber);
			parametersRequired.put("p_CUI", CUI);
			parametersRequired.put("p_issuerDescription", issuerDescription);
			parametersRequired.put("p_securityClassDesc", securityClassDesc);
			parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
	}
	
}
