package com.pradera.report.generation.api;

import java.util.List;
import java.util.Map;

import com.pradera.model.report.ReportLogger;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ReportGenerationFramework.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/08/2014
 */
public interface ReportGenerationFramework {
	
	/**
	 * Sets the report logger.
	 *
	 * @param reportLogger the new report logger
	 */
	public void setReportLogger(ReportLogger reportLogger);
	
	/**
	 * Sets the report query.
	 *
	 * @param reportQuery the new report query
	 */
	public void setReportQuery(String reportQuery);
	
	/**
	 * Sets the sub report query.
	 *
	 * @param subReportQuery the new sub report query
	 */
	public void setSubReportQuery(String subReportQuery);
	
	/**
	 * Sets the template report url.
	 *
	 * @param templateReportUrl the new template report url
	 */
	public void setTemplateReportUrl(String templateReportUrl);
	
	/**
	 * Sets the xml data source for report.
	 *
	 * @param xmlDataSource the new xml data source
	 */
	public void setXmlDataSource(String xmlDataSource);
	
	/**
	 * Sets the xml sub data source for subreport.
	 *
	 * @param subXmlDataSource the new xml sub data source
	 */
	public void setXmlSubDataSource(String subXmlDataSource);
	
	/**
	 * Generate reports.
	 */
	public void generateReports(ReportLogger reportLogger);
	
	/**
	 * Sets the custom jasper parameters.
	 *
	 * @param customJasperParameters the custom jasper parameters
	 */
	public void setCustomJasperParameters(Map<String,Object> customJasperParameters);
	
	/**
	 * Generate reports. to accounting
	 */
	public void generateReportsByFile(byte[] arrayByte);
	
	/**
	 * Generate reports. to accounting
	 */
	public void generateReportsByListFile(List<byte[]> listArrayByte);

}
