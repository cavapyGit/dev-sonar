package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.Date;

public class AccreditationOperationTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idAccreditationOperationPk;
	
	private Integer certificationType;

	private Date expirationDate;
    
	private Long holder;

	private Long participant;

	private String observations;

	private Integer petitionerType;

	private Date registerDate;

    private Integer wayOfPayment;

    private Integer indDelivered;

    private Integer indGenerated;

    private Integer destinationCertificate;

    private Integer idSignatureCertificateFk;

    private Integer idRejectMotive;

    private String rejectMotiveOther;

   	private Date deliveryDate;

   	private Date generatedDate;

    private Date initialDate;

    private Date finalDate;
    
	private Boolean isGenerated;

	private Integer accreditationState;

	private Long idBlockEntity;

	private String blockEntityName;

	private String securityCode;
	
	private Long idReportPk;

	public Long getIdAccreditationOperationPk() {
		return idAccreditationOperationPk;
	}

	public void setIdAccreditationOperationPk(Long idAccreditationOperationPk) {
		this.idAccreditationOperationPk = idAccreditationOperationPk;
	}

	public Integer getCertificationType() {
		return certificationType;
	}

	public void setCertificationType(Integer certificationType) {
		this.certificationType = certificationType;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Integer getPetitionerType() {
		return petitionerType;
	}

	public void setPetitionerType(Integer petitionerType) {
		this.petitionerType = petitionerType;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public Long getHolder() {
		return holder;
	}

	public void setHolder(Long holder) {
		this.holder = holder;
	}

	public Long getParticipant() {
		return participant;
	}

	public void setParticipant(Long participant) {
		this.participant = participant;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Integer getWayOfPayment() {
		return wayOfPayment;
	}

	public void setWayOfPayment(Integer wayOfPayment) {
		this.wayOfPayment = wayOfPayment;
	}

	public Integer getIndDelivered() {
		return indDelivered;
	}

	public void setIndDelivered(Integer indDelivered) {
		this.indDelivered = indDelivered;
	}

	public Integer getIndGenerated() {
		return indGenerated;
	}

	public void setIndGenerated(Integer indGenerated) {
		this.indGenerated = indGenerated;
	}

	public Integer getDestinationCertificate() {
		return destinationCertificate;
	}

	public void setDestinationCertificate(Integer destinationCertificate) {
		this.destinationCertificate = destinationCertificate;
	}

	public Integer getIdSignatureCertificateFk() {
		return idSignatureCertificateFk;
	}

	public void setIdSignatureCertificateFk(Integer idSignatureCertificateFk) {
		this.idSignatureCertificateFk = idSignatureCertificateFk;
	}

	public Integer getIdRejectMotive() {
		return idRejectMotive;
	}

	public void setIdRejectMotive(Integer idRejectMotive) {
		this.idRejectMotive = idRejectMotive;
	}

	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getGeneratedDate() {
		return generatedDate;
	}

	public void setGeneratedDate(Date generatedDate) {
		this.generatedDate = generatedDate;
	}

	public Boolean getIsGenerated() {
		return isGenerated;
	}

	public void setIsGenerated(Boolean isGenerated) {
		this.isGenerated = isGenerated;
	}

	public Integer getAccreditationState() {
		return accreditationState;
	}

	public void setAccreditationState(Integer accreditationState) {
		this.accreditationState = accreditationState;
	}

	public Long getIdBlockEntity() {
		return idBlockEntity;
	}

	public void setIdBlockEntity(Long idBlockEntity) {
		this.idBlockEntity = idBlockEntity;
	}

	public String getBlockEntityName() {
		return blockEntityName;
	}

	public void setBlockEntityName(String blockEntityName) {
		this.blockEntityName = blockEntityName;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public Long getIdReportPk() {
		return idReportPk;
	}

	public void setIdReportPk(Long idReportPk) {
		this.idReportPk = idReportPk;
	}
	
	
}
