package com.pradera.report.generation.executor.issuances;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.issuances.to.PaymentChronogramTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;

@ReportProcess(name="PaymentsScheduleReport")
public class PaymentsScheduleReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private PaymentChronogramBySecurity paymentChronogramService;
	@Inject
	private PraderaLogger log;
	@EJB
	private ParameterServiceBean parameterService;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		PaymentChronogramTO couponsTO = new PaymentChronogramTO();

		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("state_issuer")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					couponsTO.setStrState(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("activity_economic")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					couponsTO.setStrEconomicActivity(listaLogger.get(i).getFilterValue().toString());
				}
			}
		}
	 
		String strQuery= paymentChronogramService.getPaymentChronogram(couponsTO);
//		String strQueryFormatted = accountReportService.getQueryFormatForJasper(pdcTO, strQuery, idStkCalcProcess);
//		List<PaymentChronogramTO> lstSameCoupons= joinCoupons(lstCoupons);

		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClassMnemonic = new HashMap<Integer, String>();
		Map<Integer,String> secClassDesc = new HashMap<Integer, String>();
		Map<Integer,String> secCurrency = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassMnemonic.put(param.getParameterTablePk(), param.getText1());
			}
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClassDesc.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}

		parametersRequired.put("str_Query", strQuery);
		
		
		return parametersRequired;
	}
	
	//joining the same coupon
	public List<PaymentChronogramTO> joinCoupons(List<PaymentChronogramTO> lstCoupons){
		List<PaymentChronogramTO> lstSameCoupons= new ArrayList<PaymentChronogramTO>();
		Date lastDate= null;
		String lastSecurity=null;
		PaymentChronogramTO ccTO= null;
		int contador=0;
		int pos=0;
		for (int i = 0; i < lstCoupons.size(); i++) {
			ccTO= new PaymentChronogramTO();
			PaymentChronogramTO coupons= lstCoupons.get(i);
			ccTO.setCustodian(coupons.getCustodian());
			ccTO.setIdSecurityOnly(coupons.getIdSecurityOnly());
			ccTO.setRegistryDt(coupons.getRegistryDt());
			ccTO.setSecurityClass(coupons.getSecurityClass());
			if(lastDate==null && lastSecurity==null){//first time
				lastSecurity = coupons.getIdSecurityOnly();
				if(coupons.getCouponType().equals("INT")){
					lastDate = coupons.getExpirationDtInt();
					ccTO.setExpirationDtInt(coupons.getExpirationDtInt());
					ccTO.setCouponNumberInt(coupons.getCouponNumberInt());
					ccTO.setInterestFactor(coupons.getInterestFactor());
					ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
				}else{
					lastDate = coupons.getExpirationDtAmort();
					ccTO.setExpirationDtAmort(coupons.getExpirationDtAmort());
					ccTO.setCouponNumberAmort(coupons.getCouponNumberAmort());
					ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
					ccTO.setInterestFactor(coupons.getInterestFactor());
				}
				ccTO.setTotalCoupon(ccTO.getTotalCoupon());
				lstSameCoupons.add(ccTO);
			}else{
				if(lastSecurity.equals(coupons.getIdSecurityOnly()) && 
						(lastDate.equals(coupons.getExpirationDtInt()) || lastDate.equals(coupons.getExpirationDtAmort()))){//replace
					pos=i-1-contador;
					lstSameCoupons.get(pos).setExpirationDtInt(coupons.getExpirationDtInt());
					lstSameCoupons.get(pos).setCouponNumberInt(coupons.getCouponNumberInt());
					if(lstSameCoupons.get(pos).getInterestFactor().intValue()==0){
						lstSameCoupons.get(pos).setInterestFactor(coupons.getInterestFactor());
					}
					lstSameCoupons.get(pos).setExpirationDtAmort(null);//interest is mandatory
					lstSameCoupons.get(pos).setCouponNumberAmort(null);//interest is mandatory
					if(lstSameCoupons.get(pos).getAmortizationFactor().intValue()==0){
						lstSameCoupons.get(pos).setAmortizationFactor(coupons.getAmortizationFactor());
					}	
					lstSameCoupons.get(pos).setTotalCoupon(lstSameCoupons.get(pos).getAmortizationFactor().add(lstSameCoupons.get(pos).getInterestFactor()));
					contador++;
				}else{//add
					lastSecurity = coupons.getIdSecurityOnly();
					if(coupons.getCouponType().equals("INT")){
						lastDate = coupons.getExpirationDtInt();
						ccTO.setExpirationDtInt(coupons.getExpirationDtInt());
						ccTO.setCouponNumberInt(coupons.getCouponNumberInt());
						ccTO.setInterestFactor(coupons.getInterestFactor());
						ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
					}else{
						lastDate = coupons.getExpirationDtAmort();
						ccTO.setExpirationDtAmort(coupons.getExpirationDtAmort());
						ccTO.setCouponNumberAmort(coupons.getCouponNumberAmort());
						ccTO.setAmortizationFactor(coupons.getAmortizationFactor());
						ccTO.setInterestFactor(coupons.getInterestFactor());
					}
					ccTO.setTotalCoupon(ccTO.getTotalCoupon());
					lstSameCoupons.add(ccTO);
				}
			}
		}
		return lstSameCoupons;
	}

}
