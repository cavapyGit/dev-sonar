package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.component.business.to.VaultControlTO;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.executor.custody.service.VaultControlReportServiceBean;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="VaultControlReport")
public class VaultControlReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	
	@Inject  PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private VaultControlReportServiceBean vaultControlReportServiceBean;
	
	@EJB
	private CustodyReportServiceBean custodyReportServiceBean;

	public VaultControlReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {

	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		VaultControlTO vaultControlTO = new VaultControlTO();
		
		for(int i=0; i<listaLogger.size(); i++) {
			
			/*if(listaLogger.get(i).getFilterName().equals("p_income_initial")) {
				if(listaLogger.get(i).getFilterValue()!=null) {
					String fechaIngreso = listaLogger.get(i).getFilterValue();
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

					try {
						Date fechaIngresoDate = formatter.parse(fechaIngreso);
						String fechaIngresoDateStr = formatter2.format(fechaIngresoDate).concat(" 00:00:00");
						vaultControlTO.setFechaIngresoInicial(fechaIngresoDateStr);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("p_income_final")) {
				if(listaLogger.get(i).getFilterValue()!=null) {
					String fechaIngreso = listaLogger.get(i).getFilterValue();
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

					try {
						Date fechaIngresoDate = formatter.parse(fechaIngreso);
						String fechaIngresoDateStr = formatter2.format(fechaIngresoDate).concat(" 00:00:00");
						vaultControlTO.setFechaIngresoFinal(fechaIngresoDateStr);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
				}
			}			
			
			if(listaLogger.get(i).getFilterName().equals("p_retirement_initial")) {
				if(listaLogger.get(i).getFilterValue()!=null) {
					String fechaRetiro = listaLogger.get(i).getFilterValue();
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

					try {
						Date fechaRetiroDate = formatter.parse(fechaRetiro);
						String fechaRetiroStr = formatter2.format(fechaRetiroDate).concat(" 00:00:00");
						vaultControlTO.setFechaRetiroInicial(fechaRetiroStr);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("p_retirement_final")) {
				if(listaLogger.get(i).getFilterValue()!=null) {
					String fechaRetiro = listaLogger.get(i).getFilterValue();
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

					try {
						Date fechaRetiroDate = formatter.parse(fechaRetiro);
						String fechaRetiroStr = formatter2.format(fechaRetiroDate).concat(" 00:00:00");
						vaultControlTO.setFechaRetiroFinal(fechaRetiroStr);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
				}
			}*/			
			
			if(listaLogger.get(i).getFilterName().equals("NIVEL")) {
				if(listaLogger.get(i).getFilterValue()!=null) {
					vaultControlTO.setNivel(Integer.parseInt(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("pNivel", listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("CAJA")) {
				if(listaLogger.get(i).getFilterValue()!=null) {
					vaultControlTO.setCaja(Integer.parseInt(listaLogger.get(i).getFilterValue()));
					parametersRequired.put("pCaja", listaLogger.get(i).getFilterValue());
				}
			}
			
			if(listaLogger.get(i).getFilterName().equals("ARMARIO")) {
				if(listaLogger.get(i).getFilterValue()!=null) {
					vaultControlTO.setArmario(listaLogger.get(i).getFilterValue());
					parametersRequired.put("pArmario", listaLogger.get(i).getFilterValue());
				}
			}
			
			
		}
		
		String strQuery = vaultControlReportServiceBean.queryVaultControlReport(vaultControlTO);
		String strQueryFormatted = vaultControlReportServiceBean.queryFormatForJasper(vaultControlTO, strQuery);
		
		parametersRequired.put("str_query", strQueryFormatted);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		
		return parametersRequired;
		
	}

}
