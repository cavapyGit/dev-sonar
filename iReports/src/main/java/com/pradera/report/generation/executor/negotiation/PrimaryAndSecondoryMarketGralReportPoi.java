package com.pradera.report.generation.executor.negotiation;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.BillingServiceQueryServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.negotiation.service.NegotiationReportServiceBean;
import com.pradera.report.generation.executor.negotiation.to.PrimaryAndSecondaryMarketGralTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CommissionsAnnotationAccountReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@ReportProcess(name = "PrimaryAndSecondoryMarketGralReportPoi")
public class PrimaryAndSecondoryMarketGralReportPoi extends GenericReport {
 
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private BillingServiceQueryServiceBean billingServiceQueryServiceBean;
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The Report poi generator service bean. */
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	@EJB
	private NegotiationReportServiceBean negotiationReportServiceBean;
	
	
	//TODO varialbes del bean
	private static final Integer PRIMARY_MARKET=1;
	private static final Integer SECONDARY_MARKET=2;
	
	@EJB
	private PaymentChronogramBySecurity paymentChronogramService;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/**
	 * Instantiates a new commissions annotation account report.
	 */
	public PrimaryAndSecondoryMarketGralReportPoi() {	
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] arrayByteExcel = new byte[16384];
		
		PrimaryAndSecondaryMarketGralTO primaryAndSecondaryMarketTO=new PrimaryAndSecondaryMarketGralTO();
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		Map<String,Object> parametersRequired = new HashMap<>();
		
		for (int i = 0; i < listaLogger.size(); i++){
			
			if(listaLogger.get(i).getFilterName().equals("p_participant"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setIdParticipant(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_cui"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setCui(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_currency"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setCurrency(listaLogger.get(i).getFilterValue());
			
			
			if(listaLogger.get(i).getFilterName().equals("issuer"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setIdIssuer(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("security_class"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setSecurityClass(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("security_code"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setIdSecurity(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_type_market"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setTypeInformation(Integer.parseInt(listaLogger.get(i).getFilterValue()));
			
			if(listaLogger.get(i).getFilterName().equals("modality"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setModality(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_initial_date"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setInitialDate(listaLogger.get(i).getFilterValue());
			
			if(listaLogger.get(i).getFilterName().equals("p_final_date"))
			    if(Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue()))
			    	primaryAndSecondaryMarketTO.setFinalDate(listaLogger.get(i).getFilterValue());
			
		}
		
		String strQueryFormated;
		//verificando el tipo de mercado que se selecciono
		if (primaryAndSecondaryMarketTO.getTypeInformation().equals(PRIMARY_MARKET))
			strQueryFormated = negotiationReportServiceBean.getQueryPrimaryMarketGral(primaryAndSecondaryMarketTO);
		else
			strQueryFormated = negotiationReportServiceBean.getQuerySecondayMarketGral(primaryAndSecondaryMarketTO);
		
		List<Object[]> lstObject = negotiationReportServiceBean.getQueryListByClass(strQueryFormated);
		parametersRequired.put("lstObjects", lstObject);
				
		/**GENERATE FILE EXCEL ***/
		if (primaryAndSecondaryMarketTO.getTypeInformation().equals(PRIMARY_MARKET))
			arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelPrimaryyMarketGral1179(parametersRequired, reportLogger);
		else
			arrayByteExcel = reportPoiGeneratorServiceBean.writeFileExcelSecondaryyMarketGral1179(parametersRequired, reportLogger);
		
		/**SETTING ARRAY BYTE TO GENERIC REPORT FOR PERSIST**/
		List<byte[]> bytes = new ArrayList<>();
		bytes.add(arrayByteExcel);
		setListByte(bytes);

		return baos;
	}
	
}