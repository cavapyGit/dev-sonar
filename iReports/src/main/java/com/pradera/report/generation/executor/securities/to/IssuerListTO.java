package com.pradera.report.generation.executor.securities.to;

import java.io.Serializable;
import java.util.Date;

public class IssuerListTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The id issuer pk. */
	private String idIssuerPk;

	/** The mnemonic. */
	private String mnemonic;

	/** The economic sector. */
	private Integer economicSector;

	/** The initial date. */
	private Date initialDate;

	/** The final date. */
	private Date finalDate;

	public IssuerListTO() {
		// TODO Auto-generated constructor stub
	}

	/** Getters and Setters*/
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public Integer getEconomicSector() {
		return economicSector;
	}

	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
}
