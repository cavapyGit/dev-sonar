package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ChangeOwnershipSecuritiesReport")
public class ChangeOwnershipSecuritiesReport extends GenericReport{
private static final long serialVersionUID = 1L;

@Inject  PraderaLogger log;

@EJB
private ParameterServiceBean parameterService;

public ChangeOwnershipSecuritiesReport() {
}
	
@Override
public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addParametersQueryReport() { }

@Override
public Map<String, Object> getCustomJasperParameters() {
	Map<String,Object> parametersRequired = new HashMap<>();
	ParameterTableTO  filter = new ParameterTableTO();
	Map<Integer,String> motive = new HashMap<Integer, String>();
	Map<Integer,String> state = new HashMap<Integer, String>();
	Map<Integer,String> balance_type = new HashMap<Integer, String>();
	Map<Integer,String> affectation_type = new HashMap<Integer, String>();
	Map<Integer,String> level_type = new HashMap<Integer, String>();
	
	try {
		
		filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_MOTIVE_TYPE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			motive.put(param.getParameterTablePk(), param.getParameterName());
		}

		filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_STATUS_TYPE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			state.put(param.getParameterTablePk(), param.getDescription());
		}
		filter.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_BALANCE_TYPE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			balance_type.put(param.getParameterTablePk(), param.getDescription());
		}
		filter.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			affectation_type.put(param.getParameterTablePk(), param.getDescription());
		}
		filter.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			level_type.put(param.getParameterTablePk(), param.getDescription());
		}
		
	}catch(Exception ex){
		log.error(ex.getMessage());
		ex.printStackTrace();
		throw new RuntimeException();
	}
	parametersRequired.put("p_motive", motive);
	parametersRequired.put("p_state", state);
	parametersRequired.put("p_balance_type", balance_type);
	parametersRequired.put("p_affectation_type", affectation_type);
	parametersRequired.put("p_level_type", level_type);
	parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
	// TODO Auto-generated method stub
	return parametersRequired;
	}

}
