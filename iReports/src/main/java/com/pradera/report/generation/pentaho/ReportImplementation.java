package com.pradera.report.generation.pentaho;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

import com.pradera.model.report.type.ReportType;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface ReportImplementation.
 * Implementation Bean or Query pentaho generation
 * @author PraderaTechnologies.
 * @version 1.0 , 03/07/2013
 */
@Qualifier
@Target({ TYPE, METHOD, PARAMETER, FIELD })
@Retention(RUNTIME)
@Documented
public @interface ReportImplementation {
	
	/**
	 * Type.
	 *
	 * @return the report type
	 */
	ReportType type();
}
