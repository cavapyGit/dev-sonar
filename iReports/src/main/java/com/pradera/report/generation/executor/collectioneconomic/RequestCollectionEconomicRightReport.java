package com.pradera.report.generation.executor.collectioneconomic;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportUser;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.collectioneconomic.service.CollectionReportServiceBean;
import com.pradera.report.generation.executor.collectioneconomic.to.RequestCollectionTO;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.poi.ReportPoiGeneratorServiceBean;

@ReportProcess(name = "RequestCollectionEconomicRightReport")
public class RequestCollectionEconomicRightReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6961869241848582229L;
	
	@Inject
	private PraderaLogger log;
	
	/** The report generation facade. */
	@EJB
	private ReportGenerationLocalServiceFacade reportGenerationFacade;
	
private CollectionReportServiceBean  collectionReportServiceBean;
	
	@Inject
	private ReportPoiGeneratorServiceBean reportPoiGeneratorServiceBean;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	private ReportUser reportUser;

	private RequestCollectionTO collectionTO;
	
	@EJB
	private CustodyReportServiceBean custodyReportService;
	
	Map<String, Object> parametersRequired = new HashMap<>();
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {
	}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		List<ReportLoggerDetail> loggerDetails = getReportLogger().getReportLoggerDetails();

		//variables para setear en los parametros del reporte (299)
		String idParticipantPk=null;
		String idHolderCui=null;
		String idSecurityCode=null;
		String dueDate=null;
		String paymentDate=null;
		String requestNumber=null;
		String initialDate=null;
		String finalDate=null;

		collectionTO = new RequestCollectionTO();
		for (ReportLoggerDetail r : loggerDetails) {
			if (r.getFilterName().equals("reportFormats")&&r.getFilterValue()!=null){
				collectionTO.setReportType(Integer.parseInt(r.getFilterValue()));
			}
			
			if (r.getFilterName().equals("sp")&&r.getFilterValue()!=null){
				idParticipantPk = r.getFilterValue();
				collectionTO.setIdParticipantPk(Long.parseLong(idParticipantPk));
			}
			if (r.getFilterName().equals("param_cui")&&r.getFilterValue() != null){
				idHolderCui = r.getFilterValue();
				collectionTO.setIdHoderPk(Long.parseLong(idHolderCui));
			}
			if (r.getFilterName().equals("security_code")&&r.getFilterValue() != null){
				idSecurityCode = r.getFilterValue();
				collectionTO.setIdSecurityCodePk(idSecurityCode);
			}
			if (r.getFilterName().equals("param_duedate")&&r.getFilterValue() != null){
				dueDate = r.getFilterValue();
				collectionTO.setDueDate(dueDate);
			}
			if (r.getFilterName().equals("param_paymentdate")&& r.getFilterValue() != null){
				paymentDate = r.getFilterValue();
				collectionTO.setPaymentDate(paymentDate);
			}
			if (r.getFilterName().equals("request_number")&&r.getFilterValue() != null){
				requestNumber = r.getFilterValue();
				collectionTO.setIdReqCollectionEconomicPk(Long.parseLong(requestNumber));
			}					
			if (r.getFilterName().equals("date_initial")&&r.getFilterValue() != null){
				initialDate = r.getFilterValue();
				collectionTO.setInitialDate(initialDate);
			}
			if (r.getFilterName().equals("date_end")&&r.getFilterValue() != null){
				finalDate = r.getFilterValue();
				collectionTO.setFinalDate(finalDate);
			}
		}
		//Obteniendo los parametros para el reporte 299 de COBRO DE DERECHOS ECONOMICOS
		parametersRequired.put("sp", idParticipantPk);
		parametersRequired.put("param_cui", idHolderCui);
		parametersRequired.put("security_code", idSecurityCode);
		parametersRequired.put("param_duedate", dueDate);
		parametersRequired.put("param_paymentdate", paymentDate);
		parametersRequired.put("request_number", requestNumber);
		parametersRequired.put("date_initial", initialDate);
		parametersRequired.put("date_end", finalDate );
		
		//EXCEL
		if(collectionTO.getReportType().equals(ReportFormatType.POI.getCode())){
			Map<String,String> newMap = new HashMap<>();
			Map<String,String> parameterDetails = new HashMap<String, String>();
			for (Map.Entry<String, Object> entry : parametersRequired.entrySet()) 
			       if(entry.getValue() instanceof String)
			            newMap.put(entry.getKey(), (String) entry.getValue());
			//String name = getReportLogger().getReport().getReportBeanName();
			try {
				reportGenerationFacade.saveReportExecution(308L, newMap, parameterDetails, reportUser);
				custodyReportService.deleteRegistreReport(getReportLogger().getIdReportLoggerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			generateData(getReportLogger());
			return null;
		}
		
		return parametersRequired;
	}

}
