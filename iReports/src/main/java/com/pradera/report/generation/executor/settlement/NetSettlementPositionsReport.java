package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.to.PositionsHolderParticipantTO;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.GenericSettlementOperationTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name="NetSettlementPositionsReport")
public class NetSettlementPositionsReport extends GenericReport{
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PraderaLogger log;
	
	@EJB
	private SettlementReportServiceBean settlementReportServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		GenericSettlementOperationTO gfto = new GenericSettlementOperationTO();
		
		for (int i = 0; i < listaLogger.size(); i++) {
			if(listaLogger.get(i).getFilterName().equals("settlement_date")){
				if (Validations.validateIsNotNullAndNotEmpty(listaLogger.get(i).getFilterValue())){
					gfto.setSettlementDate(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("mechanism")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setMechanism(listaLogger.get(i).getFilterValue().toString());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("settlement_schema")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setSettlementSchema(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("settlement_type")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setSettlementType(listaLogger.get(i).getFilterValue());
				}
			}
			if(listaLogger.get(i).getFilterName().equals("id_participant")){
				if (listaLogger.get(i).getFilterValue()!=null && !listaLogger.get(i).getFilterValue().equals("0")){
					gfto.setBuyerParticipant(listaLogger.get(i).getFilterValue());
				}
			}
		}
		
		String strQuery = settlementReportServiceBean.getNetSettlementPositionsQuery(gfto); 
		
		Map<String,Object> parametersRequired = new HashMap<>();
//		ParameterTableTO  filter = new ParameterTableTO();
//		Map<Integer,String> secSchema = new HashMap<Integer, String>();
//		Map<Integer,String> secType = new HashMap<Integer, String>();
//		try {
//			for(SettlementSchemaType param : SettlementSchemaType.list) {
//				secSchema.put(param.getCode(), param.getValue());
//			}
//			for(SettlementType param : SettlementType.list) {
//				secType.put(param.getCode(), param.getMnemonic());
//			}
//		}catch(Exception ex){
//			log.error(ex.getMessage());
//			ex.printStackTrace();
//			throw new RuntimeException();
//		}
		
		parametersRequired.put("strQuery", strQuery);
//		parametersRequired.put("pSchema", secSchema);
//		parametersRequired.put("pType", secType);
		parametersRequired.put("settlement_date", gfto.getSettlementDate());
		
		return parametersRequired;
	}
	
	

}
