package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.services.remote.stockcalculation.StockCalculationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.account.service.AccountReportServiceBean;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.GenericCorporativesTO;
import com.pradera.report.generation.executor.securities.service.PaymentChronogramBySecurity;
import com.pradera.report.generation.executor.settlement.service.SettlementReportServiceBean;
import com.pradera.report.generation.executor.settlement.to.GenericSettlementOperationTO;
import com.pradera.report.generation.executor.to.GenericsFiltersTO;

@ReportProcess(name = "PayOfAmortizationOnHAccountToParticipantsReport")
public class PayOfAmortizationOnHAccountToParticipantsReport extends GenericReport{
	private static final long serialVersionUID = 1L;

	@EJB
	private ParameterServiceBean parameterService;
	
	@EJB
	private CorporativeReportServiceBean corporativeReportServiceBean;

	@Inject
	private PraderaLogger log;

	public PayOfAmortizationOnHAccountToParticipantsReport() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {

		GenericCorporativesTO gfto = new GenericCorporativesTO();

		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("process_id")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setProcessId(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_initial")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setCutDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("date_end")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setRegistryDate(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("event_type")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setEventType(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("security_class_key")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setSecurities(param.getFilterValue().toString());
				}
			}
			if(param.getFilterName().equals("state")){
				if (param.getFilterValue()!=null && !param.getFilterValue().equals("0")){
					gfto.setState(param.getFilterValue().toString());
				}
			}
		}
		
		String strQuery = corporativeReportServiceBean.getQueryPayOfAmortizationOnHAccountToParticipantsReport(gfto);  
		 
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> secClass = new HashMap<Integer, String>();
		Map<Integer,String> secCurrency = new HashMap<Integer, String>();
		try {
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secClass.put(param.getParameterTablePk(), param.getDescription());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				secCurrency.put(param.getParameterTablePk(), param.getParameterName());
			}
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		parametersRequired.put("str_Query", strQuery);
		parametersRequired.put("mSecClass", secClass);
		parametersRequired.put("mCurrency", secCurrency);
		parametersRequired.put("initialDt", gfto.getCutDate());
		parametersRequired.put("finalDt", gfto.getRegistryDate());
		
		return parametersRequired;
		
		
	}

}
