package com.pradera.report.generation.executor.custody;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;

@ReportProcess(name = "ReportBeanEntriesVoluntary")
public class ReportBeanEntriesVoluntary extends GenericReport {

	private static final long serialVersionUID = 1L;
	//filtros en BD
	private final String ACCOUNT_HOLDER="account_holder";
	private final String NUMBER_TITLE="number_title";
	private final String ISSUER="issuer";
	private final String KEY_VALUE="key_value";
	private final String STATE="state";
	private final String CUI="cui";
	private final String PARTICIPANT="participant";
	private final String START_DATE="start_date";
	private final String FINISCH_DATE="finish_date";
	private final String CLASS_SECURITY="class_security";
	
	//filtro en *.jasper
	private Map<String, Object> map= new HashMap<String, Object>();
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream arrayOutputStream=new  ByteArrayOutputStream();
		return arrayOutputStream;
	}
	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}
		for (ReportLoggerDetail reportLoggerDetail : listaLogger) {
			switch (reportLoggerDetail.getFilterName()){
			case ACCOUNT_HOLDER:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(ACCOUNT_HOLDER,reportLoggerDetail.getFilterValue());
			break;
			case NUMBER_TITLE:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(NUMBER_TITLE, reportLoggerDetail.getFilterValue());
			break;
			case ISSUER:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(ISSUER, reportLoggerDetail.getFilterValue());
			break;
			case KEY_VALUE:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(KEY_VALUE, reportLoggerDetail.getFilterValue());
			break;
			case STATE:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(STATE, reportLoggerDetail.getFilterValue());
			break;
			case CUI:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(CUI, reportLoggerDetail.getFilterValue());
			break;
			case PARTICIPANT:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(PARTICIPANT, reportLoggerDetail.getFilterValue());
			break;
			case START_DATE:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(START_DATE, reportLoggerDetail.getFilterValue());
			break;
			case FINISCH_DATE:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(FINISCH_DATE, reportLoggerDetail.getFilterValue());
			break;
			case CLASS_SECURITY:
				if(Validations.validateIsNotNullAndNotEmpty(reportLoggerDetail.getFilterValue()))
					map.put(CLASS_SECURITY, reportLoggerDetail.getFilterValue());
			break;
			default:
			break;
			}
		}
	}
	@Override
	public Map<String, Object> getCustomJasperParameters(){
		Iterator it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry e = (Map.Entry)it.next();
			System.out.println(e.getKey() + ":" + e.getValue());
		}
		return map;
	}
}
