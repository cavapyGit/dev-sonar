package com.pradera.report.generation.executor.settlement;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.custody.service.CustodyReportServiceBean;

@ReportProcess(name = "CuadreLogbookReport")
public class CuadreLogbookReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;
	@Inject private PraderaLogger log;
	@EJB CustodyReportServiceBean custodyReportServiceBean;
	
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		String participantDesc = "";
		String date = null;

		for (ReportLoggerDetail detail : listaLogger) {
			if(detail.getFilterName().equals(ReportConstant.PARTICIPANT_PK_PARAM)){
				if (Validations.validateIsNotNullAndNotEmpty(detail.getFilterValue())){
					participantDesc = detail.getFilterDescription();
				}else{
					participantDesc = "TODOS";
				}
			}
			if(detail.getFilterName().equals(ReportConstant.DATE_PARAM)){
				if (Validations.validateIsNotNullAndNotEmpty(detail.getFilterValue())){
					date = detail.getFilterValue();
				}
			}
		}
		
		//VALIDAR TIPO DE CAMBIO A LA FECHA DE CORTE
		try{
			custodyReportServiceBean.getExchangeRate(date);
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.NOT_EXCHANGE_RATE)){
				throw new RuntimeErrorException(null, ReportConstant.ERROR_EXCHANGE_RATE + " " + date);
			}
			log.error(e.getMessage());
		}
		
		Map<String,Object> parametersRequired = new HashMap<String,Object>();
		parametersRequired.put("participant_description", participantDesc);
		
		return parametersRequired;
	}
	
}
