package com.pradera.report.generation.executor.issuances.to;

import java.io.Serializable;

public class CouponsDetachmentTO implements Serializable{
	private static final long serialVersionUID = 1L;

	private String requestNumber;
	private String requestState;
	private String dateChangeState;
	private String participantCode;
	private String idHolderAccountPk; 
	private String cui;
	private String securityClass;
	private String idSecurityCodePk;
	
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getRequestState() {
		return requestState;
	}
	public void setRequestState(String requestState) {
		this.requestState = requestState;
	}
	public String getDateChangeState() {
		return dateChangeState;
	}
	public void setDateChangeState(String dateChangeState) {
		this.dateChangeState = dateChangeState;
	}
	public String getParticipantCode() {
		return participantCode;
	}
	public void setParticipantCode(String participantCode) {
		this.participantCode = participantCode;
	}
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public String getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(String idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
}
