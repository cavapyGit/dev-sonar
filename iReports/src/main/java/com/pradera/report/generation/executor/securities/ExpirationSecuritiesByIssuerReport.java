package com.pradera.report.generation.executor.securities;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name = "ExpirationSecuritiesByIssuerReport")
public class ExpirationSecuritiesByIssuerReport extends GenericReport{
	
	private static final long serialVersionUID = 1L;

	@EJB private ParameterServiceBean parameterService;
	@Inject PraderaLogger log;
	@EJB ParticipantServiceBean participantServiceBean;

	public ExpirationSecuritiesByIssuerReport() {}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		return null;
	}

	@Override
	public void addParametersQueryReport() {}

	@Override
	public Map<String, Object> getCustomJasperParameters() {
		Map<String, Object> parametersRequired = new HashMap<>();
		String indAutomatic = null;
		for(ReportLoggerDetail param: getReportLogger().getReportLoggerDetails()){
			if(param.getFilterName().equals("ind_automatic")){
				if (param.getFilterValue()!=null){
					indAutomatic = param.getFilterValue().toString();
				}
			}
		}
		
		parametersRequired.put("indAutomatic", indAutomatic);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());

		return parametersRequired;
	}

}
