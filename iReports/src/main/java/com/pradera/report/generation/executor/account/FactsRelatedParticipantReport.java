package com.pradera.report.generation.executor.account;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.ReportLoggerDetail;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.util.view.UtilReportConstants;

@ReportProcess(name="FactsRelatedParticipantReport")
public class FactsRelatedParticipantReport extends GenericReport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	@Inject  PraderaLogger log;
	
	@EJB
	GeneralParametersFacade generalParametersFacade;

	public FactsRelatedParticipantReport() {
	}

	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void addParametersQueryReport() {
		List<ReportLoggerDetail> listaLogger = getReportLogger().getReportLoggerDetails();
		if (listaLogger == null) {
			listaLogger = new ArrayList<ReportLoggerDetail>();
		}

		ReportLoggerDetail loggerDetail = new ReportLoggerDetail();
		
//		loggerDetail.setFilterName("balance_ind");
//		String value = valueParameter(listaLogger,"balance_ind");
//		if (value!=null){
//			loggerDetail.setFilterValue(value);
//		}else{
//			loggerDetail.setFilterValue("1,0");
//		}
//		listaLogger.add(loggerDetail);
		
	}
	
	@Override
	public Map<String, Object> getCustomJasperParameters() {
		
		Map<String,Object> parametersRequired = new HashMap<>();
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> personType = new HashMap<Integer, String>();
		Map<Integer,String> personDocumentType = new HashMap<Integer, String>();
		Map<Integer,String> stateHolder = new HashMap<Integer, String>();
		Map<Integer,String> geographicLocation = new HashMap<Integer, String>();
		Map<Integer,String> economicActivity = new HashMap<Integer, String>();
		Map<Integer,String> juridicClass = new HashMap<Integer, String>();
		Map<Integer,String> typeHolder = new HashMap<Integer, String>();
		Map<Integer,String> currency = new HashMap<Integer, String>();
		Map<Integer,String> holderAccountStatus = new HashMap<Integer, String>();
		Map<Integer,String> holderAccountBankType = new HashMap<Integer, String>();
		
		try {
			filter.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				personType.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			filter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				personDocumentType.put(param.getParameterTablePk(), param.getIndicator1());
			}
			
			filter.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				stateHolder.put(param.getParameterTablePk(), param.getDescription());
			}
			
			 GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			 geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
			 geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
			 List<GeographicLocation> listGeographicLocation = generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);
			 for(GeographicLocation lista :listGeographicLocation){
				 geographicLocation.put(lista.getIdGeographicLocationPk(), lista.getName());
			 }
			 
			 filter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
			 for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				 economicActivity.put(param.getParameterTablePk(), param.getIndicator1());
			 }
			 filter.setMasterTableFk(MasterTableType.T_CLASE_JURIDICO_TITULAR.getCode());
			 for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				 juridicClass.put(param.getParameterTablePk(), param.getParameterName());
			 }
			 
			 filter.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
			 for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				 typeHolder.put(param.getParameterTablePk(), param.getDescription());
			 }
			 
			 filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			 for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				 currency.put(param.getParameterTablePk(), param.getParameterName());
			 }
			 
			 filter.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
			 for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				 holderAccountStatus.put(param.getParameterTablePk(), param.getDescription());
			 }
			 
			 filter.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());
			 for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				 holderAccountBankType.put(param.getParameterTablePk(), param.getDescription());
			 }
			 
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		parametersRequired.put("p_personType", personType);
		parametersRequired.put("p_personDocumentType", personDocumentType);
		parametersRequired.put("p_stateHolder", stateHolder);
		parametersRequired.put("p_geographicLocation", geographicLocation);
		parametersRequired.put("p_economicActivity", economicActivity);
		parametersRequired.put("p_juridicClass", juridicClass);
		parametersRequired.put("p_typeHolder", typeHolder);
		parametersRequired.put("p_currency", currency);
		parametersRequired.put("p_holderAccountStatus", holderAccountStatus);
		parametersRequired.put("p_holderAccountBankType", holderAccountBankType);
		parametersRequired.put("logo_path", UtilReportConstants.readLogoReport());
		return parametersRequired;
	}
	
	private String valueParameter(List<ReportLoggerDetail> listaLogger,String parameter){
		String value = null;
		for (int i=0;i<listaLogger.size();i++){
			if (listaLogger.get(i).getFilterName()!=null){
				if (listaLogger.get(i).getFilterName().equals(parameter)){
					if (listaLogger.get(i).getFilterValue()!=null){
						value=listaLogger.get(i).getFilterValue();
						i=listaLogger.size();
					}
				}
			}			
		}
		return value;
	}

}
