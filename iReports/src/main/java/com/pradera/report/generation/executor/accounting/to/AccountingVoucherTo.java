package com.pradera.report.generation.executor.accounting.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingVoucherTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountingVoucherTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The voucher number. */
	private BigDecimal 	voucherNumber;//NUMCBTE
	
	/** The gloss. */
	private String 		gloss;//GLOSA
	
	/** The daily exchange. */
	private BigDecimal 	dailyExchange;//TIPOCAMBIOCAB
	
	/** The date. */
	private String 		date;//FECHA
	
	/** The type. */
	private String 		type;//TIPO
	
	/** The list accounting voucher registry to. */
	private List<AccountingVoucherRegistryTo> listAccountingVoucherRegistryTo;


	/**
	 * Instantiates a new accounting voucher to.
	 */
	public AccountingVoucherTo() {
		
		listAccountingVoucherRegistryTo=new ArrayList<AccountingVoucherRegistryTo>();
		
	}

	/**
	 * Instantiates a new accounting voucher to.
	 *
	 * @param voucherNumber the voucher number
	 * @param gloss the gloss
	 * @param dailyExchange the daily exchange
	 * @param date the date
	 * @param type the type
	 * @param listAccountingVoucherRegistryTo the list accounting voucher registry to
	 */
	public AccountingVoucherTo(BigDecimal voucherNumber, String gloss,
			BigDecimal dailyExchange, String date, String type,
			List<AccountingVoucherRegistryTo> listAccountingVoucherRegistryTo) {
		this.voucherNumber = voucherNumber;
		this.gloss = gloss;
		this.dailyExchange = dailyExchange;
		this.date = date;
		this.type = type;
		this.listAccountingVoucherRegistryTo = listAccountingVoucherRegistryTo;
	}

	/**
	 * Gets the voucher number.
	 *
	 * @return the voucherNumber
	 */
	public BigDecimal getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * Sets the voucher number.
	 *
	 * @param voucherNumber the voucherNumber to set
	 */
	public void setVoucherNumber(BigDecimal voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	/**
	 * Gets the gloss.
	 *
	 * @return the gloss
	 */
	public String getGloss() {
		return gloss;
	}

	/**
	 * Sets the gloss.
	 *
	 * @param gloss the gloss to set
	 */
	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	/**
	 * Gets the daily exchange.
	 *
	 * @return the dailyExchange
	 */
	public BigDecimal getDailyExchange() {
		return dailyExchange;
	}

	/**
	 * Sets the daily exchange.
	 *
	 * @param dailyExchange the dailyExchange to set
	 */
	public void setDailyExchange(BigDecimal dailyExchange) {
		this.dailyExchange = dailyExchange;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the list accounting voucher registry to.
	 *
	 * @return the listAccountingVoucherRegistryTo
	 */
	public List<AccountingVoucherRegistryTo> getListAccountingVoucherRegistryTo() {
		return listAccountingVoucherRegistryTo;
	}

	/**
	 * Sets the list accounting voucher registry to.
	 *
	 * @param listAccountingVoucherRegistryTo the listAccountingVoucherRegistryTo to set
	 */
	public void setListAccountingVoucherRegistryTo(
			List<AccountingVoucherRegistryTo> listAccountingVoucherRegistryTo) {
		this.listAccountingVoucherRegistryTo = listAccountingVoucherRegistryTo;
	}
	
	
		
}
