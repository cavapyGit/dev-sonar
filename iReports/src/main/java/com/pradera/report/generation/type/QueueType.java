package com.pradera.report.generation.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum QueueType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/06/2013
 */
public enum QueueType {
	
	/** The one. */
	ONE(Integer.valueOf(1)),
	
	/** The two. */
	TWO(Integer.valueOf(2)),
	
	/** The three. */
	THREE(Integer.valueOf(3));
	
	/** The Constant lookup. */
	public static final Map<Integer, QueueType> lookup = new HashMap<Integer, QueueType>();
	static {
		for (QueueType s : EnumSet.allOf(QueueType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	/** The code. */
	private Integer code;

	/**
	 * Instantiates a new queue type.
	 *
	 * @param code the code
	 */
	private QueueType(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the queue type
	 */
	public static QueueType get(Integer code){
		return lookup.get(code);
	}
}
