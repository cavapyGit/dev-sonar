package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AnnotationOperationTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idParticipantPk;
	private BigDecimal accountNumber;
	private Long rnt;
	private String idIsinCode;
	private Integer state;
	private Date initialDate;
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public BigDecimal getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(BigDecimal accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Long getRnt() {
		return rnt;
	}
	public void setRnt(Long rnt) {
		this.rnt = rnt;
	}
	public String getIdIsinCode() {
		return idIsinCode;
	}
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	private Date finalDate;
}
