package com.pradera.report.generation.executor.corporative;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportLogger;
import com.pradera.report.generation.ReportProcess;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.executor.corporative.service.CorporativeReportServiceBean;
import com.pradera.report.generation.executor.corporative.to.CorporativeProcesInterestTO;
import com.pradera.report.generation.executor.corporative.to.StockCalculationProcessTO;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InterestAmortizationBlockedReport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/09/2015
 */
@ReportProcess(name="InterestAmortizationBlockedReport")
public class InterestAmortizationBlockedReport extends GenericReport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6505767022470569721L;
	
	/** The parameter service. */
	@EJB
	ParameterServiceBean parameterService;
	
	/** The corporative report service. */
	@EJB
	CorporativeReportServiceBean corporativeReportService;
	
	/** The participant service. */
	@EJB
	ParticipantServiceBean participantService;
	
	/** The corporative to. */
	private CorporativeProcesInterestTO corporativeTo;
	
	/** The participante pk. */
	private static Integer PARTICIPANTE_PK = new Integer(0);
	
	/** The participante mnemonic. */
	private static Integer PARTICIPANTE_MNEMONIC = new Integer(1);
	
	/** The participante desc. */
	private static Integer PARTICIPANTE_DESC = new Integer(2);
	
	/** The account pk. */
	private static Integer ACCOUNT_PK = new Integer(3);
	
	/** The account number. */
	private static Integer ACCOUNT_NUMBER = new Integer(4);
	
	/** The holders name. */
	private static Integer HOLDERS_NAME = new Integer(5);
	
	/** The stock pawn. */
	private static Integer STOCK_PAWN = new Integer(6);
	
	/** The stock ban. */
	private static Integer STOCK_BAN = new Integer(7);
	
	/** The stock other. */
	private static Integer STOCK_OTHER = new Integer(8);
	
	/** The stock total. */
	private static Integer STOCK_TOTAL = new Integer(9);
	
	/** The process pawn. */
	private static Integer PROCESS_PAWN = new Integer(10);
	
	/** The process ban. */
	private static Integer PROCESS_BAN = new Integer(11);
	
	/** The process other. */
	private static Integer PROCESS_OTHER = new Integer(12);
	
	/** The process total. */
	private static Integer PROCESS_TOTAL = new Integer(13);
	
	/** The block doc number. */
	private static Integer BLOCK_DOC_NUMBER = new Integer(0);
	
	/** The block cite number. */
	private static Integer BLOCK_CITE_NUMBER = new Integer(1);
	
	/** The block type. */
	private static Integer BLOCK_TYPE = new Integer(2);
	
	/** The block balance. */
	private static Integer BLOCK_BALANCE = new Integer(3);
	
	/** The block process. */
	private static Integer BLOCK_PROCESS = new Integer(4);
	
	/** The block entity. */
	private static Integer BLOCK_ENTITY = new Integer(5);
	
	/** The block origin destiny. */
	private static Integer BLOCK_ORIGIN_DESTINY = new Integer(6);
	
	/** The block result pk. */
	private static Integer BLOCK_RESULT_PK = new Integer(7);

	/**
	 * Instantiates a new interest amortization blocked report.
	 */
	public InterestAmortizationBlockedReport() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.pradera.report.generation.executor.GenericReport#generateData(com.pradera.model.report.ReportLogger)
	 */
	@Override
	public ByteArrayOutputStream generateData(ReportLogger reportLogger) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		corporativeTo = getCorporateParameters(reportLogger);
		List<Object[]> processInformation = corporativeReportService.getCorporativePaymentHeader(corporativeTo);
		try{
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));
			xmlsw.writeStartDocument(ReportConstant.ENCODING_XML, "1.0");//<XML>
			xmlsw.writeStartElement(ReportConstant.REPORT);//<report>
			createHeaderReport( xmlsw, reportLogger );//header
			if(!processInformation.isEmpty()){
				Map<Integer,String> processStateMap = new HashMap<>();
				Map<Integer,String> currencyMap = new HashMap<>();
				Map<Integer,String> currencyMapAux = new HashMap<>();
				for (ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.CURRENCY.getCode())){
					currencyMap.put(param.getParameterTablePk(), param.getParameterName());
					currencyMapAux.put(param.getParameterTablePk(), param.getText1());
				}
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode())){
					processStateMap.put(param.getParameterTablePk(), param.getParameterName());
				}
				createSubHeaderCorporativeReport(xmlsw,corporativeTo, processInformation.get(0), processStateMap, currencyMap);
				StockCalculationProcessTO stockCalculationTO = new StockCalculationProcessTO();
				stockCalculationTO.setSecurity(processInformation.get(0)[GenericReport.SECURITY_CODE].toString());
				stockCalculationTO.setCutOffDate((Date)processInformation.get(0)[GenericReport.CUTTOF_DATE]);
				stockCalculationTO.setRegistryDate((Date)processInformation.get(0)[GenericReport.REGISTRY_DATE]);
				stockCalculationTO.setStockClass(StockClassType.NORMAL.getCode());
				stockCalculationTO.setStockType((StockType.CORPORATIVE.getCode()));
				
				Long stockCalculationProcess = corporativeReportService.getStockCalculationNumber(stockCalculationTO);
				corporativeTo.setStockId(stockCalculationProcess);
				if(corporativeTo.getProcessId()==null){
					corporativeTo.setProcessId((Long)processInformation.get(0)[GenericReport.CORPORATIVE_OPERATION]);
				}
				xmlsw.writeStartElement("holders");
				createBodyReport(xmlsw,corporativeTo,new Integer(processInformation.get(0)[GenericReport.CORPORATIVE_TYPE].toString()),currencyMapAux,processInformation.get(0));
				xmlsw.writeEndElement();
			} else {
				xmlsw.writeStartElement("holders");
				xmlsw.writeEndElement();
				
			}
			xmlsw.writeEndElement();//</report>
			xmlsw.writeEndDocument();//</XML>
			xmlsw.flush();
	        xmlsw.close();
		}catch(XMLStreamException exm){
			exm.printStackTrace();
		} catch (ServiceException ex) {
			// TODO: handle exception
		}
		return baos;
	}
	
	/**
	 * Creates the body report.
	 *
	 * @param xmlsw the xmlsw
	 * @param corporativeProcessInformation the corporative process information
	 * @param corporativeType the corporative type
	 * @param currencyMap the currency map
	 * @param corporativeInformation the corporative information
	 * @throws XMLStreamException the XML stream exception
	 * @throws ServiceException the service exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw,CorporativeProcesInterestTO corporativeProcessInformation,Integer corporativeType
			,Map<Integer,String> currencyMap,Object[] corporativeInformation) throws XMLStreamException,ServiceException{
		List<Object[]> processControlInformation = corporativeReportService.getStockBlockedProcess(corporativeProcessInformation,corporativeInformation);
		if(!processControlInformation.isEmpty()){
			Map<Integer,String> blockedTypeMap = new HashMap<>();
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(MasterTableType.TYPE_AFFECTATION.getCode())){
				blockedTypeMap.put(param.getParameterTablePk(), param.getParameterName());
			}
			Integer processType = new Integer(corporativeInformation[CORPORATIVE_TYPE].toString());
			for(Object[] row : processControlInformation){
				xmlsw.writeStartElement("holder");
					createTagString(xmlsw, "participantCode", row[PARTICIPANTE_PK]);
					createTagString(xmlsw, "participantDesc", row[PARTICIPANTE_MNEMONIC]+" - "+row[PARTICIPANTE_DESC]);
					createTagString(xmlsw, "accountPk", row[ACCOUNT_PK]);
					createTagString(xmlsw, "accountNumber", row[ACCOUNT_NUMBER]);
					createTagCDataString(xmlsw, "holderName", row[HOLDERS_NAME]);
					createTagString(xmlsw, "stockPawn", row[STOCK_PAWN]);
					createTagString(xmlsw, "stockBan", row[STOCK_BAN]);
					createTagString(xmlsw, "stockOther", row[STOCK_OTHER]);
					createTagString(xmlsw, "stockTotal", row[STOCK_TOTAL]);
					if(processType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode()) || 
							processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
						createTagInteger(xmlsw, "processPawn", row[PROCESS_PAWN]);
						createTagInteger(xmlsw, "processBan", row[PROCESS_BAN]);
						createTagInteger(xmlsw, "processOther", row[PROCESS_OTHER]);
						createTagInteger(xmlsw, "processTotal", row[PROCESS_TOTAL]);
					}else{
						createTagDecimal(xmlsw, "processPawn", row[PROCESS_PAWN]);
						createTagDecimal(xmlsw, "processBan", row[PROCESS_BAN]);
						createTagDecimal(xmlsw, "processOther", row[PROCESS_OTHER]);
						createTagDecimal(xmlsw, "processTotal",row[PROCESS_TOTAL]);
					}					
					xmlsw.writeStartElement("blockeds");
					List<Object[]> blocks = corporativeReportService.getStockDetailBalanceBlockedProcess(corporativeProcessInformation.getProcessId(),
							new Long(row[ACCOUNT_PK].toString()), corporativeType);
					
					if(Validations.validateListIsNotNullAndNotEmpty(blocks)) {
						boolean blValidateExchange = validateExchange(blocks);
						
						for(Object[] block : blocks){
							xmlsw.writeStartElement("blocked");
							xmlsw.writeAttribute("id", row[PARTICIPANTE_PK]+" - "+row[ACCOUNT_PK]);
								createTagString(xmlsw, "blockNumber", block[BLOCK_DOC_NUMBER]);
								createTagString(xmlsw, "blockCite", block[BLOCK_CITE_NUMBER]);
								createTagString(xmlsw, "blockType", blockedTypeMap.get(new Integer(block[BLOCK_TYPE].toString())));								
								createTagString(xmlsw, "blockBalance", block[BLOCK_BALANCE]);
								if(blValidateExchange){
									if(new Integer(block[BLOCK_ORIGIN_DESTINY].toString()).equals(GeneralConstants.TWO_VALUE_INTEGER)){
										createTagString(xmlsw, ReportConstant.CURRENCY, CurrencyType.PYG.getCodeIso());
									}else{										
										if(processType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode()) || 
												processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
											createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY_AUX].toString())));
										}else{
											createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY].toString())));
										}	
									}
								}else{
									if(processType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode()) || 
											processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
										createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY_AUX].toString())));
									}else{
										createTagString(xmlsw, ReportConstant.CURRENCY, currencyMap.get(new Integer(corporativeInformation[CURRENCY].toString())));
									}	
								}								
								if(processType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode()) || 
										processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
									createTagInteger(xmlsw, "blockAmortization",block[BLOCK_PROCESS]);
								}else{
									createTagDecimal(xmlsw, "blockAmortization",block[BLOCK_PROCESS]);
								}								
								createTagString(xmlsw, "blockEntity", block[BLOCK_ENTITY]);
								if(corporativeType.equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
									createTagString(xmlsw, "corporativeType", "A");
								}else if(corporativeType.equals(ImportanceEventType.INTEREST_PAYMENT.getCode())){
									createTagString(xmlsw, "corporativeType", "I");
								}else if(corporativeType.equals(ImportanceEventType.CASH_DIVIDENDS.getCode())){
									createTagString(xmlsw, "corporativeType", "CD");
								}else if(corporativeType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
									createTagString(xmlsw, "corporativeType", "AD");
								}else if(corporativeType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
									createTagString(xmlsw, "corporativeType", "PS");
								}
							xmlsw.writeEndElement();
						}
					}	
					xmlsw.writeEndElement();
				xmlsw.writeEndElement();
			}
		}
	}
	
	/**
	 * Validate exchange.
	 *
	 * @param blocks the blocks
	 * @return true, if successful
	 */
	public boolean validateExchange(List<Object[]> blocks){
		boolean validate = false;
		List<Long> idResult = new ArrayList<Long>();
		for(Object[] block : blocks){
			idResult.add(new Long(block[BLOCK_RESULT_PK].toString()));
		}
		validate = corporativeReportService.validateExchange(idResult);
		return validate;
	}
}
