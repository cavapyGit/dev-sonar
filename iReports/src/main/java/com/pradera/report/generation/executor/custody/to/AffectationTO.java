package com.pradera.report.generation.executor.custody.to;

import java.io.Serializable;
import java.util.Date;

public class AffectationTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long documentNumber;
	private Integer custodyOperationtState;
	private Integer blockRequestState;
	private Integer blockType;
	private Long idHolderPk;
	private Long idBlockEntityPk;
	private String blockNumber;
	private Long idBlockRequestPk;
	private Long idUnblockRequestPk;
	private Date initialDate;
	private Date endDate;
	private Integer state;
	private Long idParticipant;
	
	
	public Integer getBlockRequestState() {
		return blockRequestState;
	}
	public void setBlockRequestState(Integer blockRequestState) {
		this.blockRequestState = blockRequestState;
	}
	public Integer getBlockType() {
		return blockType;
	}
	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Long getIdBlockEntityPk() {
		return idBlockEntityPk;
	}
	public void setIdBlockEntityPk(Long idBlockEntityPk) {
		this.idBlockEntityPk = idBlockEntityPk;
	}
	public String getBlockNumber() {
		return blockNumber;
	}
	public void setBlockNumber(String blockNumber) {
		this.blockNumber = blockNumber;
	}
	public Long getIdBlockRequestPk() {
		return idBlockRequestPk;
	}
	public void setIdBlockRequestPk(Long idBlockRequestPk) {
		this.idBlockRequestPk = idBlockRequestPk;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Long getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(Long documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Integer getCustodyOperationtState() {
		return custodyOperationtState;
	}
	public void setCustodyOperationtState(Integer custodyOperationtState) {
		this.custodyOperationtState = custodyOperationtState;
	}
	public Long getIdUnblockRequestPk() {
		return idUnblockRequestPk;
	}
	public void setIdUnblockRequestPk(Long idUnblockRequestPk) {
		this.idUnblockRequestPk = idUnblockRequestPk;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Long getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
}
