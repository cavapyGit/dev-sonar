package com.pradera.report.generation;
/*
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.descriptor.api.Descriptors;
import org.jboss.shrinkwrap.descriptor.api.beans10.BeansDescriptor;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.report.ReportLogger;
import com.pradera.model.report.type.ReportType;
import com.pradera.report.ReportContext;
import com.pradera.report.generation.executor.GenericReport;
import com.pradera.report.generation.executor.ReportConstant;
import com.pradera.report.generation.facade.ReportGenerationLocalServiceFacade;
import com.pradera.report.generation.pentaho.ReportGenerator;
import com.pradera.report.generation.pentaho.ReportImplementation;
import com.pradera.report.generation.service.ReportUtilServiceBean;
import com.pradera.report.management.facade.ReportMgmtServiceFacade;
*/
//@RunWith(Arquillian.class)
public class GenerationReportTest {
	
//	@Inject
//	PraderaLogger log;
//	
//	@EJB
//	ReportGenerationLocalServiceFacade reportGenerationServiceFacade;
//	
//	@Inject @ReportImplementation(type=ReportType.BEAN)
//	ReportGenerator reportGenerator;
//	
//	@EJB
//	ReportUtilServiceBean reportUtilService;
//	
////	@Inject @ReportProcess(name="ParticipantListReport")
////	GenericReport reportParticipant;
//	
//	@EJB
//	ReportMgmtServiceFacade reportMgmtFacade;
//	
//	/** The dao service. */
//	@EJB
//	CrudDaoServiceBean daoService;
//	/*
//	
//	@Deployment
//  	public static WebArchive createTestArchive() {
//  		File[] libs = Maven.resolver()
//                  .loadPomFromFile("pom.xml")
//                  .importRuntimeDependencies()
//                  .asFile();
//  		 BeansDescriptor beans = Descriptors.create(BeansDescriptor.class)
//  				 .getOrCreateAlternatives().clazz("com.pradera.report.generation.jasper.ReportGeneratorJasper").up()
//  				 .getOrCreateAlternatives().clazz("com.pradera.report.generation.jasper.BeanReportGeneratorJasper").up()
//  				 .getOrCreateAlternatives().clazz("com.pradera.report.generation.jasper.QueryReportGeneratorJasper").up()
//  				 .getOrCreateInterceptors().clazz("com.pradera.commons.interceptores.PerformanceInterceptor").up()
//  				 .getOrCreateInterceptors().clazz("com.pradera.core.framework.extension.transaction.jta.TransactionalInterceptor").up();
//  				 
//  		
//  		WebArchive war =
//  		
//  		ShrinkWrap.create(WebArchive.class, "iReports.war")
//  				.addAsLibraries(libs)
//  				.addPackages(true, ReportContext.class.getPackage())
//  				//.addPackages(true, StockCalculationServiceBean.class.getPackage())
//  				.addAsManifestResource("MANIFEST.MF")
//  				.addAsWebInfResource(new StringAsset(beans.exportAsString()), "beans.xml")
//  				//logo edv
//  				.addAsResource(new File("src/main/resources/edv-logo.jpg"),"edv-logo.jpg")
//  				//reports templates
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/RelacionValoresEyE.jasper"),"reportsJasper/securities/RelacionValoresEyE.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/RelacionValoresRF.jasper"),"reportsJasper/securities/RelacionValoresRF.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/RelacionValoresRV.jasper"),"reportsJasper/securities/RelacionValoresRV.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/RegValSistAnot.jasper"),"reportsJasper/securities/RegValSistAnot.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/RelValSectoryClase.jasper"),"reportsJasper/securities/RelValSectoryClase.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/ListaEjecRespEmisores.jasper"),"reportsJasper/securities/ListaEjecRespEmisores.jasper");
//  				.addAsResource(new File("src/main/resources/reportsJasper/securities/EmpresasVinculos.jasper"),"reportsJasper/securities/EmpresasVinculos.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/IndividualEmpresas.jasper"),"reportsJasper/securities/IndividualEmpresas.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/RelacionEmpresas.jasper"),"reportsJasper/securities/RelacionEmpresas.jasper");
//  				//.addAsResource(new File("src/main/resources/reportsJasper/securities/TitulosFechaVencimiento.jasper"),"reportsJasper/securities/TitulosFechaVencimiento.jasper");
//  				
//  				System.out.println(war.toString((Formatters.VERBOSE)));
//  		return war;
//  	}
//
////	@Test
//	public void testGenerateReport() {
//		LoggerUser loggerUser = new LoggerUser();
//		loggerUser.setIpAddress("192.168.1.56");
//		loggerUser.setUserName("ADMIN");
//		loggerUser.setIdUserSessionPk(1l);
//		loggerUser.setSessionId("sessssxkxkdd");
//		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
//		//Praderafilter in each request get current UserAcction
//		UserAcctions userAcctions = new UserAcctions();
//		userAcctions.setIdPrivilegeAdd(1);
//		userAcctions.setIdPrivilegeRegister(1);
//		loggerUser.setUserAction(userAcctions);
//		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
//		try{
//		Map<String,String> parameter = new HashMap<String, String>();
////		parameter.put("participant_class", "30");
//		reportGenerationServiceFacade.saveReportExecution(new Long(2), parameter, null, null);
////		sendReportOne.sendMessage(null);
////		clientJMSOne.sendMessage(null);
//		log.info("reporte lanzado esperando generacion");
//		log.info("si salio del metodo debio haber enviado el mensaje");
//		log.info("terminando test");
//		}catch(Exception sex){
//			log.error(sex.getMessage());
//		}
//		
//	}
//	
////	@Test
//	/***
//	 * Usado para probar la generacion de reportes del tipo query
//	 * sin un bean que implemente su procesado 
//	 **/
//	public void testGenerationQueryPdf() {
//		LoggerUser loggerUser = new LoggerUser();
//		loggerUser.setIpAddress("192.168.1.56");
//		loggerUser.setUserName("rcaso");
//		loggerUser.setIdUserSessionPk(1l);
//		loggerUser.setSessionId("sessssxkxkdd");
//		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
//		//Praderafilter in each request get current UserAcction
//		UserAcctions userAcctions = new UserAcctions();
//		userAcctions.setIdPrivilegeAdd(1);
//		userAcctions.setIdPrivilegeRegister(1);
//		loggerUser.setUserAction(userAcctions);
//		loggerUser.setIdPrivilegeOfSystem(1);
//		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
//		Map<String,Object> parameter = new HashMap<String, Object>();
//		parameter.put("reportLogger", new Long(423));
//		ReportLogger reportLogger = reportUtilService.findEntityByNamedQuery(ReportLogger.class,
//				ReportLogger.REPORTLOGGER__FIND_DETAILS, parameter);
//		reportGenerator.setReportLogger(reportLogger);
//		reportGenerator.setXmlDataSource(reportLogger.getReport().getDatasourceXml());
//		reportGenerator.setTemplateReportUrl(reportLogger.getReport().getTemplateName());
//		reportGenerator.generateReports(reportLogger);
//		log.info("termino de generar");
//	}
//	
////	@Test
//	/**
//	 * Usado para probar la generacion del xml
//	 * de un reporte especifico
//	 * **/
//	public void testGenerationXmlBean() {
//		LoggerUser loggerUser = new LoggerUser();
//		loggerUser.setIpAddress("192.168.1.56");
//		loggerUser.setUserName("rcaso");
//		loggerUser.setIdUserSessionPk(1l);
//		loggerUser.setSessionId("sessssxkxkdd");
//		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
//		//Praderafilter in each request get current UserAcction
//		UserAcctions userAcctions = new UserAcctions();
//		userAcctions.setIdPrivilegeAdd(1);
//		userAcctions.setIdPrivilegeRegister(1);
//		loggerUser.setUserAction(userAcctions);
//		loggerUser.setIdPrivilegeOfSystem(1);
//		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
//		Map<String,Object> parameter = new HashMap<String, Object>();
//		parameter.put("reportLogger", new Long(403));
//		ReportLogger reportLogger = reportUtilService.findEntityByNamedQuery(ReportLogger.class,
//				ReportLogger.REPORTLOGGER__FIND_DETAILS, parameter);
////		reportParticipant.setReportLogger(reportLogger);
////		log.info(reportParticipant.generateData(reportLogger).toString());
//		log.info("reporte procesado");
//	}
//	
//	
//	//@Test
//	public void loadReportTest(){
//		LoggerUser loggerUser = new LoggerUser();
//		loggerUser.setIpAddress("192.168.1.56");
//		loggerUser.setUserName("rcaso");
//		loggerUser.setIdUserSessionPk(1l);
//		loggerUser.setSessionId("sessssxkxkdd");
//		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
//		//Praderafilter in each request get current UserAcction
//		UserAcctions userAcctions = new UserAcctions();
//		userAcctions.setIdPrivilegeAdd(1);
//		userAcctions.setIdPrivilegeRegister(1);
//		loggerUser.setUserAction(userAcctions);
//		loggerUser.setIdPrivilegeOfSystem(1);
//		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
//		UserAccountSession userAccount = new UserAccountSession();
//		try {
//		reportMgmtFacade.loadReporte(new Long(3),userAccount,"es");
//		} catch(Exception ex){
//			ex.printStackTrace();
//		}
//		log.info("reporte cargado");
//	}
//	
//	@Test
//	public void testGenerationConsolidateXmlBean() {
//		LoggerUser loggerUser = new LoggerUser();
//		loggerUser.setIpAddress("192.168.1.56");
//		//loggerUser.setIpAddress("127.0.0.1");
//		loggerUser.setUserName("EDVOH");
//		loggerUser.setIdUserSessionPk(1l);
//		loggerUser.setSessionId("SESSIONID");
//		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
//		//Praderafilter in each request get current UserAcction
//		UserAcctions userAcctions = new UserAcctions();
//		userAcctions.setIdPrivilegeAdd(1);
//		userAcctions.setIdPrivilegeRegister(1);
//		loggerUser.setUserAction(userAcctions);
//		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
//		try{
//			Map<String,String> parameter = new HashMap<String, String>();
//			parameter.put(ReportConstant.DATE_INITIAL_PARAM,"01/10/2013");
//			//parameter.put("date","23/05/2014");
//			
//			//reportGenerationServiceFacade.saveReportExecution(new Long(65), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(66), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(67), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(68), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(74), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(77), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(79), parameter, null, null);
//			reportGenerationServiceFacade.saveReportExecution(new Long(80), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(81), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(83), parameter, null, null);
//			//reportGenerationServiceFacade.saveReportExecution(new Long(91), parameter, null, null);
//			
//	//		sendReportOne.sendMessage(null);
//	//		clientJMSOne.sendMessage(null);
//			log.info("reporte lanzado esperando generacion");
//			log.info("si salio del metodo debio haber enviado el mensaje");
//			log.info("terminando test");
//		}catch(Exception sex){
//			log.error(sex.getMessage());
//		}
//	}
//	
////	@Test
//	public void testCorrelativesRequestOperations(){
//		log.info("iterando por los correlativos");
//		for(RequestCorrelativeType correlative : RequestCorrelativeType.list){
//			log.info("operacion "+correlative.toString());
//			log.info("valor de operacion "+daoService.getNextCorrelativeOperations(correlative));
//		}
//		log.info("termino de iterar");
//	}

}
