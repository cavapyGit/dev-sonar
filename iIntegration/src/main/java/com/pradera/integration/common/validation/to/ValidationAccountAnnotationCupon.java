package com.pradera.integration.common.validation.to;

import java.io.Serializable;

public class ValidationAccountAnnotationCupon implements  Serializable {
	
	private static final long serialVersionUID = 1L;
	
	protected Integer lineNumber;

	private String certificateNumber;//certificado
	private String serialNumber;//serie
	private String nroCupon;//nro cupon
	private String beginingDate;//Fecha Inicio
	private String expirationDate;//Fecha Vencimiento
	private String paymentDate;//Fecha Pago
	private String interestRate;//tasa
	private String cuponAmount;//monto Cupon
	private String mnemonicIssuer;
	private String securityCurrency;
	
	public Integer getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getNroCupon() {
		return nroCupon;
	}
	public void setNroCupon(String nroCupon) {
		this.nroCupon = nroCupon;
	}
	public String getBeginingDate() {
		return beginingDate;
	}
	public void setBeginingDate(String beginingDate) {
		this.beginingDate = beginingDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}
	public String getCuponAmount() {
		return cuponAmount;
	}
	public void setCuponAmount(String cuponAmount) {
		this.cuponAmount = cuponAmount;
	}
	public String getMnemonicIssuer() {
		return mnemonicIssuer;
	}
	public void setMnemonicIssuer(String mnemonicIssuer) {
		this.mnemonicIssuer = mnemonicIssuer;
	}
	public String getSecurityCurrency() {
		return securityCurrency;
	}
	public void setSecurityCurrency(String securityCurrency) {
		this.securityCurrency = securityCurrency;
	}
}
