package com.pradera.integration.common.validation.to;

import java.io.Serializable;
import java.util.List;

public class ValidationAccountAnnotationOperation implements  Serializable {
	
	private static final long serialVersionUID = 1L;
	
	protected Integer lineNumber;

	private String certificateNumber;//certificado
	private String serialNumber;//serie
	private String mnemonicIssuer;//codigo emisor
	private String securityClass="CDA";//CDA o cupon Default
	private String securityNominalValue;
	private String totalBalance= "1";// cantidad - en CDA 1 por Default
	private String securityCurrency = "127"; // moneda
	private String motive = "INMOVILIZACION";//motivo
	private String observation;//observacion
	private String branchOffice = "Default";//sucursal
	private String securityInterestType;//tipo de tasa
	private String securityInterestRate;//Tasa
	private String expeditionDate;//fecha emision
	private String securityExpirationDate;//fecha vencimiento
	private String periodicity;//peridiocidad
	private String indElectronicCupon = "0";//sus cupones son electronico	
	private String participantSeller = "";
	private String amountRate = "";	//precio sucio
	private String taxExoneration = "";//exoneracion de impuesto
	//private String motiveDescription = "0";//sus cupones son electronico	

	private String holderAccountNumber;//numero de cuenta
	private String participant;//participante

	private String idSecurityCodePk;
	private String certificateFrom;
	private String certificateTo;
	private String holderAccountNumberBenefi;//numero de cuenta beneficiario
	private String holderAccountNumberDebtor;//numero de cuenta deudor
	
	private List<ValidationAccountAnnotationCupon> validationAccountAnnotationCupons;
	
	public Integer getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getMnemonicIssuer() {
		return mnemonicIssuer;
	}
	public void setMnemonicIssuer(String mnemonicIssuer) {
		this.mnemonicIssuer = mnemonicIssuer;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getSecurityNominalValue() {
		return securityNominalValue;
	}
	public void setSecurityNominalValue(String securityNominalValue) {
		this.securityNominalValue = securityNominalValue;
	}
	public String getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(String totalBalance) {
		this.totalBalance = totalBalance;
	}
	public String getSecurityCurrency() {
		return securityCurrency;
	}
	public void setSecurityCurrency(String securityCurrency) {
		this.securityCurrency = securityCurrency;
	}
	public String getMotive() {
		return motive;
	}
	public void setMotive(String motive) {
		this.motive = motive;
	}
	public String getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}
	public String getSecurityInterestType() {
		return securityInterestType;
	}
	public void setSecurityInterestType(String securityInterestType) {
		this.securityInterestType = securityInterestType;
	}
	public String getSecurityInterestRate() {
		return securityInterestRate;
	}
	public void setSecurityInterestRate(String securityInterestRate) {
		this.securityInterestRate = securityInterestRate;
	}
	public String getExpeditionDate() {
		return expeditionDate;
	}
	public void setExpeditionDate(String expeditionDate) {
		this.expeditionDate = expeditionDate;
	}
	public String getSecurityExpirationDate() {
		return securityExpirationDate;
	}
	public void setSecurityExpirationDate(String securityExpirationDate) {
		this.securityExpirationDate = securityExpirationDate;
	}
	public String getPeriodicity() {
		return periodicity;
	}
	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}
	public String getIndElectronicCupon() {
		return indElectronicCupon;
	}
	public void setIndElectronicCupon(String indElectronicCupon) {
		this.indElectronicCupon = indElectronicCupon;
	}
	public String getHolderAccountNumber() {
		return holderAccountNumber;
	}
	public void setHolderAccountNumber(String holderAccountNumber) {
		this.holderAccountNumber = holderAccountNumber;
	}
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public List<ValidationAccountAnnotationCupon> getValidationAccountAnnotationCupons() {
		return validationAccountAnnotationCupons;
	}
	public void setValidationAccountAnnotationCupons(
			List<ValidationAccountAnnotationCupon> validationAccountAnnotationCupons) {
		this.validationAccountAnnotationCupons = validationAccountAnnotationCupons;
	}
	public String getParticipantSeller() {
		return participantSeller;
	}
	public void setParticipantSeller(String participantSeller) {
		this.participantSeller = participantSeller;
	}
	public String getAmountRate() {
		return amountRate;
	}
	public void setAmountRate(String amountRate) {
		this.amountRate = amountRate;
	}
	public String getTaxExoneration() {
		return taxExoneration;
	}
	public void setTaxExoneration(String taxExoneration) {
		this.taxExoneration = taxExoneration;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public String getCertificateFrom() {
		return certificateFrom;
	}
	public void setCertificateFrom(String certificateFrom) {
		this.certificateFrom = certificateFrom;
	}
	public String getCertificateTo() {
		return certificateTo;
	}
	public void setCertificateTo(String certificateTo) {
		this.certificateTo = certificateTo;
	}
	public String getHolderAccountNumberBenefi() {
		return holderAccountNumberBenefi;
	}
	public void setHolderAccountNumberBenefi(String holderAccountNumberBenefi) {
		this.holderAccountNumberBenefi = holderAccountNumberBenefi;
	}
	public String getHolderAccountNumberDebtor() {
		return holderAccountNumberDebtor;
	}
	public void setHolderAccountNumberDebtor(String holderAccountNumberDebtor) {
		this.holderAccountNumberDebtor = holderAccountNumberDebtor;
	}
}
