package com.pradera.integration.common.validation.to;

import java.io.Serializable;
import java.util.Date;

public class OperationInterfaceTO extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long operationNumber;
	private String operationCode;
	private Date operationDate;
	private String obligationNumber;
	private String idSecurityCode; // not mapped
	private Long idCustodyOperation; // not mapped
	private Long idCorporativeOperation; // not mapped
	private Long idTradeOperation; // not mapped
	private Long idParticipant; // not mapped
	private Long idInterfaceProcess; // not mapped
	private Long idInterfaceTransaction; // not mapped
	private Long idFundsOperation; // not mapped
	
	private Integer transactionState;
	
	public Long getIdParticipant() {
		return idParticipant;
	}
	private String entityNemonic;
	
	public Long getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	public String getObligationNumber() {
		return obligationNumber;
	}
	public void setObligationNumber(String obligationNumber) {
		this.obligationNumber = obligationNumber;
	}
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	
	public Long getIdCustodyOperation() {
		return idCustodyOperation;
	}
	public void setIdCustodyOperation(Long idCustodyOperation) {
		this.idCustodyOperation = idCustodyOperation;
	}
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	public String getOperationCode() {
		return operationCode;
	}
	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}
	public Date getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	public String getEntityNemonic() {
		return entityNemonic;
	}
	public void setEntityNemonic(String entityNemonic) {
		this.entityNemonic = entityNemonic;
	}
	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}
	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}
	public Long getIdCorporativeOperation() {
		return idCorporativeOperation;
	}
	public void setIdCorporativeOperation(Long idCorporativeOperation) {
		this.idCorporativeOperation = idCorporativeOperation;
	}
	public Long getIdTradeOperation() {
		return idTradeOperation;
	}
	public void setIdTradeOperation(Long idTradeOperation) {
		this.idTradeOperation = idTradeOperation;
	}
	
	public Integer getTransactionState() {
		return transactionState;
	}
	
	public void setTransactionState(Integer transactionState) {
		this.transactionState = transactionState;
	}
	public Long getIdFundsOperation() {
		return idFundsOperation;
	}
	public void setIdFundsOperation(Long idFundsOperation) {
		this.idFundsOperation = idFundsOperation;
	}
	public Long getIdInterfaceTransaction() {
		return idInterfaceTransaction;
	}
	public void setIdInterfaceTransaction(Long idInterfaceTransaction) {
		this.idInterfaceTransaction = idInterfaceTransaction;
	}
	
}
