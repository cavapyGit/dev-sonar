package com.pradera.integration.common.validation.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ValidationProcessTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ValidationOperationType> lstOperations; // operations from XML
	private List<RecordValidationType> lstValidations;
	private List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperation;
	private List<ValidationAccountAnnotationCupon> lstAccountAnnotationCupon;
	private int acceptedOps ;
	private int rejectedOps ;
	private int totalCount;
	
	
	public List<ValidationOperationType> getAcceptedOperations(){
		List<ValidationOperationType> accepted = new ArrayList<ValidationOperationType>(0);
		for (ValidationOperationType operation : lstOperations) {
			if(!operation.isHasError()){
				accepted.add(operation);
			}
		}
		return accepted;
	}
	
	public void countOperations (){
		for (ValidationOperationType operation : lstOperations) {
			if(operation.isHasError()){
				rejectedOps++;
			}else{
				acceptedOps++;
			}
		}
	}
	
	public ValidationProcessTO() {
		super();
		lstOperations =  new ArrayList<ValidationOperationType>(0);
		lstValidations = new ArrayList<RecordValidationType>(0);
	}
	
	
	public ValidationProcessTO(List<RecordValidationType> lstErrors,
			List<ValidationOperationType> lstAccOperations,
			List<ValidationOperationType> lstRejOperations) {
		super();
		//this.bcrdOperationHeader = bcrdOperationHeader;
		this.lstValidations = lstErrors;
	}
	/**
	 * @return the lstErrors
	 */
	public List<RecordValidationType> getLstValidations() {
		return lstValidations;
	}
	/**
	 * @param lstErrors the lstErrors to set
	 */
	public void setLstValidations(List<RecordValidationType> lstErrors) {
		this.lstValidations = lstErrors;
	}

	/**
	 * @return the rejectedOps
	 */
	public int getRejectedOps() {
		return rejectedOps;
	}


	/**
	 * @param rejectedOps the rejectedOps to set
	 */
	public void setRejectedOps(int rejectedOps) {
		this.rejectedOps = rejectedOps;
	}


	/**
	 * @return the acceptedOps
	 */
	public int getAcceptedOps() {
		return acceptedOps;
	}


	/**
	 * @param acceptedOps the acceptedOps to set
	 */
	public void setAcceptedOps(int acceptedOps) {
		this.acceptedOps = acceptedOps;
	}

	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<ValidationOperationType> getLstOperations() {
		return lstOperations;
	}

	public void setLstOperations(List<ValidationOperationType> lstOperations) {
		this.lstOperations = lstOperations;
	}

	public List<ValidationAccountAnnotationOperation> getLstAccountAnnotationOperation() {
		return lstAccountAnnotationOperation;
	}

	public void setLstAccountAnnotationOperation(List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperation) {
		this.lstAccountAnnotationOperation = lstAccountAnnotationOperation;
	}

	public List<ValidationAccountAnnotationCupon> getLstAccountAnnotationCupon() {
		return lstAccountAnnotationCupon;
	}

	public void setLstAccountAnnotationCupon(List<ValidationAccountAnnotationCupon> lstAccountAnnotationCupon) {
		this.lstAccountAnnotationCupon = lstAccountAnnotationCupon;
	}


}
