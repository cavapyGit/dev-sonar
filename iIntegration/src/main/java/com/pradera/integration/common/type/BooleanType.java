package com.pradera.integration.common.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum BooleanType.
 * This Enum is used for Indicators. 0 for No - false and 1 for YES - true
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/02/2013
 */
public enum BooleanType {
	
	/** The no. */
	NO(Integer.valueOf(0),Integer.valueOf(0),false,"NO"),
	
	/** The yes. */
	YES(Integer.valueOf(1),Integer.valueOf(1),true,"SI");
	
	/** The code. */
	private Integer code;
	
	/** The binary value. */
	private Integer binaryValue;
	
	/** The boolean value. */
	private boolean booleanValue;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<BooleanType> list = new ArrayList<BooleanType>();
	
	/** The Constant lookup. */
	public static final Map<Boolean, BooleanType> lookup = new HashMap<Boolean, BooleanType>();
	
	public static final Map<Integer, BooleanType> lookupInteger = new HashMap<Integer, BooleanType>();
	
	static {
        for (BooleanType d : BooleanType.values()){
            lookup.put(d.getBooleanValue(), d);
            lookupInteger.put(d.getCode(), d);
        	list.add(d);
        }
    }

	/**
	 * Instantiates a new boolean type.
	 *
	 * @param code the code
	 * @param binaryValue the binary value
	 * @param booleanValue the boolean value
	 * @param value the value
	 */
	private BooleanType(Integer code,Integer binaryValue, boolean booleanValue, String value) {
		this.code=code;
		this.binaryValue = binaryValue;
		this.booleanValue = booleanValue;
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the binary value.
	 *
	 * @return the binary value
	 */
	public Integer getBinaryValue() {
		return binaryValue;
	}

	/**
	 * Sets the binary value.
	 *
	 * @param binaryValue the new binary value
	 */
	public void setBinaryValue(Integer binaryValue) {
		this.binaryValue = binaryValue;
	}

	/**
	 * Gets the boolean value.
	 *
	 * @return the boolean value
	 */
	public boolean getBooleanValue() {
		return booleanValue;
	}

	/**
	 * Sets the boolean value.
	 *
	 * @param booleanValue the new boolean value
	 */
	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the boolean type
	 */
	public static BooleanType get(Boolean booleanValue) {
		return lookup.get(booleanValue);
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the boolean type
	 */
	public static BooleanType get(Integer code) {
		return lookupInteger.get(code);
	}
}