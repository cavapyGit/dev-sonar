package com.pradera.integration.common.validation.to;

import java.io.Serializable;
import java.util.Locale;

public class ValidationOperationType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer index;
	
	private Integer lineNumber;

	private Locale locale = new Locale("es");
	
	private Long idFileMechanismPk;
	
	private Long idFileParticipantPk;
	
	private boolean hasError;
	
	private boolean dpfInterface;
	
	private boolean earlyPaymentNoDpf;
	
	public ValidationOperationType() {
		super();
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Long getIdFileMechanismPk() {
		return idFileMechanismPk;
	}

	public void setIdFileMechanismPk(Long idFileMechanismPk) {
		this.idFileMechanismPk = idFileMechanismPk;
	}

	public Long getIdFileParticipantPk() {
		return idFileParticipantPk;
	}

	public void setIdFileParticipantPk(Long idFileParticipantPk) {
		this.idFileParticipantPk = idFileParticipantPk;
	}

	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public boolean isDpfInterface() {
		return dpfInterface;
	}

	public void setDpfInterface(boolean dpfInterface) {
		this.dpfInterface = dpfInterface;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public boolean isEarlyPaymentNoDpf() {
		return earlyPaymentNoDpf;
	}

	public void setEarlyPaymentNoDpf(boolean earlyPaymentNoDpf) {
		this.earlyPaymentNoDpf = earlyPaymentNoDpf;
	}
}
