package com.pradera.integration.common.validation.to;

import java.io.Serializable;
import java.util.Date;

public class InterfaceFileTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String interfaceName;
	
	private Long idExtensionFileType;
	
	private Long idExternalInterface;
	
	private Date processDate;
	
	private byte[] interfaceFile;

	private String relativeServiceUrl;
	
	
	public InterfaceFileTO() {
		super();
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public Long getIdExtensionFileType() {
		return idExtensionFileType;
	}

	public void setIdExtensionFileType(Long idExtensionFileType) {
		this.idExtensionFileType = idExtensionFileType;
	}

	public Long getIdExternalInterface() {
		return idExternalInterface;
	}

	public void setIdExternalInterface(Long idExternalInterface) {
		this.idExternalInterface = idExternalInterface;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public byte[] getInterfaceFile() {
		return interfaceFile;
	}

	public void setInterfaceFile(byte[] interfaceFile) {
		this.interfaceFile = interfaceFile;
	}

	public String getRelativeServiceUrl() {
		return relativeServiceUrl;
	}

	public void setRelativeServiceUrl(String relativeServiceUrl) {
		this.relativeServiceUrl = relativeServiceUrl;
	}
	
	
}
