package com.pradera.integration.common.validation.to;

import java.io.Serializable;
import java.util.List;

public class RecordValidationType implements Comparable<RecordValidationType>, Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;
	private static final String EMPTY_STRING = "";
	
	protected Integer lineNumber;
	protected Integer recordNumber;
	protected String field;
	protected String errorCode;
	protected String errorDescription;
	protected String errorSimpleDetail;
	protected String fieldText;
	protected ValidationOperationType operationRef;
	protected ValidationOperationType securityRef;
	protected String emptyProp = EMPTY_STRING;
	protected List<Long> errorProcess;
	
	public RecordValidationType(ValidationOperationType operationRef,String field) {
		super();
		this.field = field;
		this.operationRef = operationRef;
	}
	
	public RecordValidationType() {
		super();
	}
	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}
	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}
	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	/**
	 * @return the lineNumber
	 */
	public Integer getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return the recordNumber
	 */
	public Integer getRecordNumber() {
		return recordNumber;
	}
	/**
	 * @param recordNumber the recordNumber to set
	 */
	public void setRecordNumber(Integer recordNumber) {
		this.recordNumber = recordNumber;
	}

	/**
	 * @return the emptyProp
	 */
	public String getEmptyProp() {
		return emptyProp;
	}
	/**
	 * @param emptyProp the emptyProp to set
	 */
	public void setEmptyProp(String emptyProp) {
		this.emptyProp = emptyProp;
	}
	
	@Override
	public int compareTo(RecordValidationType o) {
		return lineNumber - o.getLineNumber();
	}
	
	public ValidationOperationType getOperationRef() {
		return operationRef;
	}
	public void setOperationRef(ValidationOperationType operationRef) {
		this.operationRef = operationRef;
	}
	
	public ValidationOperationType getSecurityRef() {
		return securityRef;
	}

	public void setSecurityRef(ValidationOperationType securityRef) {
		this.securityRef = securityRef;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorSimpleDetail() {
		return errorSimpleDetail;
	}

	public void setErrorSimpleDetail(String errorSimpleDetail) {
		this.errorSimpleDetail = errorSimpleDetail;
	}

	public String getFieldText() {
		return fieldText;
	}

	public void setFieldText(String fieldText) {
		this.fieldText = fieldText;
	}

	public List<Long> getErrorProcess() {
		return errorProcess;
	}

	public void setErrorProcess(List<Long> errorProcess) {
		this.errorProcess = errorProcess;
	}

	@Override
	public RecordValidationType clone() throws CloneNotSupportedException {
		return (RecordValidationType) super.clone();
	}

}
