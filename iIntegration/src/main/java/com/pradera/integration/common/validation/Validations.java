package com.pradera.integration.common.validation;

import java.math.BigDecimal;
import java.util.List;

import com.pradera.integration.usersession.UserAcctions;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class Validations.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/12/2012
 */
public class Validations {

	/**
	 * Validate is null.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateIsNull(Object o) {
		if (o == null) {
			return true;
		}
		return false;
	}

	/**
	 * Validate is empty.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateIsEmpty(Object o) {
		String d = o.toString();
		if (d.trim().length() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Validate is not empty.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateIsNotEmpty(Object o) {
		return !validateIsEmpty(o);
	}

	/**
	 * Validate is not null.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateIsNotNull(Object o) {
		return !validateIsNull(o);
	}

	/**
	 * Validate is null or empty.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateIsNullOrEmpty(Object o) {
		return (validateIsNull(o) || validateIsEmpty(o));
	}

	/**
	 * Validate is not null and not empty.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateIsNotNullAndNotEmpty(Object o) {
		return (validateIsNotNull(o) && validateIsNotEmpty(o));
	}
	
	/**
	 * Validate list is not null and not empty.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateListIsNullOrEmpty(List<?> o) {
		return (validateIsNull(o) || !validateIsPositiveNumber(o.size()));
	}

	/**
	 * Validate is positive number.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public static boolean validateIsPositiveNumber(Integer num) {
		return num > 0;
	}
	/**
	 * Validate is either zero or positive number.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public static boolean validateZeroOrPositiveNumber(Integer num) {
		return num >= 0;
	}
	/**
	 * Validate is positive number.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public static boolean validateIsPositiveNumber(Long num) {
		return num > 0;
	}
	
	public static boolean validateIsPositiveNumber(BigDecimal num) {
		return num.compareTo(BigDecimal.ZERO)>0;
	}
	
	/**
	 * Checks for acces privileges.
	 *
	 * @param userAcctions the user acctions
	 * @return true, if successful
	 */
	public static boolean hasPrivilegesOnPage(UserAcctions userAcctions){
		if(Validations.validateIsNull(userAcctions)){
			return false;
		} else {
			//Verify if the user have at least one privilege
			return userAcctions.isReview()
					|| userAcctions.isRegister()
					|| userAcctions.isModify()
					|| userAcctions.isConfirm()
					|| userAcctions.isDelete()
					|| userAcctions.isBlock()
					|| userAcctions.isUnblock()
					|| userAcctions.isReject()
					|| userAcctions.isSearch()
					|| userAcctions.isAdd()
					|| userAcctions.isApprove()
					|| userAcctions.isAnnular()
					|| userAcctions.isAdjustments()
				    || userAcctions.isApply()
				    || userAcctions.isAuthorize()
				    || userAcctions.isConfirmDepositary()
				    || userAcctions.isConfirmIssuer()
				    || userAcctions.isGenerate()
				    || userAcctions.isExecute()
				    || userAcctions.isPreliminary()
				    || userAcctions.isDefinitive()
				    || userAcctions.isUpdate()
				    || userAcctions.isDeliver()
				    || userAcctions.isCertify()
				    || userAcctions.isConfirmAnnular()
				    || userAcctions.isConfirmBlock()
				    || userAcctions.isConfirmDelete()
				    || userAcctions.isConfirmUnblock()
				    || userAcctions.isRejectAnnular()
				    || userAcctions.isRejectBlock()
				    || userAcctions.isRejectDelete()
				    || userAcctions.isRejectModify()
				    || userAcctions.isRejectUnblock();
		}
	}
	
	/**
	 * Validate is null or not positive.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public static boolean validateIsNullOrNotPositive(Long num){
		return (validateIsNull(num) || !validateIsPositiveNumber(num));
	}
	
	/**
	 * Validate is not null and positive.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public static boolean validateIsNotNullAndPositive(Long num){
		return (validateIsNotNull(num) && validateIsPositiveNumber(num));
	}
	
	public static boolean validateIsNotNullAndPositive(BigDecimal num){
		return (validateIsNotNull(num) && validateIsPositiveNumber(num));
	}
	/**
	 * Validate is null or not positive.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public static boolean validateIsNullOrNotPositive(Integer num){
		return (validateIsNull(num) || !validateIsPositiveNumber(num));
	}
	
	/**
	 * Validate is not null and positive.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public static boolean validateIsNotNullAndPositive(Integer num){
		return (validateIsNotNull(num) && validateIsPositiveNumber(num));
	}
	
	/**
	 * Validate list is not null and not empty.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	public static boolean validateListIsNotNullAndNotEmpty(List<?> o) {
		return (validateIsNotNull(o) && validateIsPositiveNumber(o.size()));
	}
	
	public static boolean validateOldAndNewObjectChanged(Object obj1, Object obj2){
		if(Validations.validateIsNotNull(obj1) &&  Validations.validateIsNotNull(obj2)){
			if(!obj1.equals(obj2)){
				return true;
			}
		} else if((Validations.validateIsNotNull(obj1) && Validations.validateIsNull(obj2) || 
				Validations.validateIsNull(obj1) && Validations.validateIsNotNull(obj2))){
			return true;
		}
		
		return false;
	}
	
	public static boolean validateIsNumeric(String s) {  
	    return s.matches("[-+]?\\d*\\.?\\d+");  
	} 
}