package com.pradera.integration.common.validation.to;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

import com.pradera.integration.component.business.ComponentConstant;

public class ProcessFileTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Locale locale;
	private Long idProcessFilePk;
	private Long sequenceNumber;
	private String description;
	private String mechanismName;
	private Long idNegotiationMechanismPk;
	private Long idParticipantPk;
	private String fileName;
	private Date processDate;
	private Integer totalOperations = ComponentConstant.ZERO;
	private Integer acceptedOperations = ComponentConstant.ZERO;
	private Integer rejectedOperations = ComponentConstant.ZERO;
	private Integer indAutomaticProcess;
	private Integer processType;
	private String receptionTypeDescription;
	private String rootTag;
	private Long idBusinessProcess;
	private Long idExternalInterface;
	private String interfaceName;
	
	private boolean isProcessed;
	private File tempProcessFile;
	private File originalTempProcessFile;
	private byte[] processFile;
	private InputStream processInputStream;
	private String streamFileDir;
	private String streamResponseFileDir;
	private String processStateDescription;
	
	private boolean isDpfInterface;
	
	private ValidationProcessTO validationProcessTO;
	
	public ProcessFileTO(){
		
	}
	
	public ProcessFileTO(Long idNegotiationMechanismPk) {
		super();
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}



	public ProcessFileTO(Date processDate) {
		super();
		this.processDate = processDate;
	}
	/**
	 * @return the sequenceNumber
	 */
	public Long getSequenceNumber() {
		return sequenceNumber;
	}
	/**
	 * @param sequenceNumber the sequenceNumber to set
	 */
	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	/**
	 * @return the mechanismDescription
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param mechanismDescription the mechanismDescription to set
	 */
	public void setDescription(String mechanismDescription) {
		this.description = mechanismDescription;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the mechanismName
	 */
	public String getMechanismName() {
		return mechanismName;
	}
	/**
	 * @param mechanismName the mechanismName to set
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}
	/**
	 * @return the idMcnProcessFilePk
	 */
	public Long getIdProcessFilePk() {
		return idProcessFilePk;
	}
	/**
	 * @param idMcnProcessFilePk the idMcnProcessFilePk to set
	 */
	public void setIdProcessFilePk(Long idMcnProcessFilePk) {
		this.idProcessFilePk = idMcnProcessFilePk;
	}
	/**
	 * @return the idNegotiationMechanismPk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	/**
	 * @param idNegotiationMechanismPk the idNegotiationMechanismPk to set
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}
	/**
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	/**
	 * @return the totalOperations
	 */
	public Integer getTotalOperations() {
		return totalOperations;
	}
	/**
	 * @param totalOperations the totalOperations to set
	 */
	public void setTotalOperations(Integer totalOperations) {
		this.totalOperations = totalOperations;
	}
	/**
	 * @return the acceptedOperations
	 */
	public Integer getAcceptedOperations() {
		return acceptedOperations;
	}
	/**
	 * @param acceptedOperations the acceptedOperations to set
	 */
	public void setAcceptedOperations(Integer acceptedOperations) {
		this.acceptedOperations = acceptedOperations;
	}
	/**
	 * @return the rejectedOperations
	 */
	public Integer getRejectedOperations() {
		return rejectedOperations;
	}
	/**
	 * @param rejectedOperations the rejectedOperations to set
	 */
	public void setRejectedOperations(Integer rejectedOperations) {
		this.rejectedOperations = rejectedOperations;
	}
	/**
	 * @return the streamFileDir
	 */
	public String getStreamFileDir() {
		return streamFileDir;
	}
	/**
	 * @param streamFileDir the streamFileDir to set
	 */
	public void setStreamFileDir(String streamFileDir) {
		this.streamFileDir = streamFileDir;
	}
	/**
	 * @return the tempMcnFile
	 */
	public File getTempProcessFile() {
		return tempProcessFile;
	}
	/**
	 * @param tempMcnFile the tempMcnFile to set
	 */
	public void setTempProcessFile(File tempMcnFile) {
		this.tempProcessFile = tempMcnFile;
	}
	/**
	 * @return the mcnFile
	 */
	public byte[] getProcessFile() {
		return processFile;
	}
	/**
	 * @param mcnFile the mcnFile to set
	 */
	public void setProcessFile(byte[] mcnFile) {
		this.processFile = mcnFile;
	}

	/**
	 * @return the indAutomaticProcess
	 */
	public Integer getIndAutomaticProcess() {
		return indAutomaticProcess;
	}
	/**
	 * @param indAutomaticProcess the indAutomaticProcess to set
	 */
	public void setIndAutomaticProcess(Integer indAutomaticProcess) {
		this.indAutomaticProcess = indAutomaticProcess;
	}
	/**
	 * @return the processType
	 */
	public Integer getProcessType() {
		return processType;
	}
	/**
	 * @param processType the processType to set
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}
	/**
	 * @param locale the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * @return the rootTag
	 */
	public String getRootTag() {
		return rootTag;
	}

	/**
	 * @param rootTag the rootTag to set
	 */
	public void setRootTag(String rootTag) {
		this.rootTag = rootTag;
	}

	/**
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public ValidationProcessTO getValidationProcessTO() {
		return validationProcessTO;
	}

	public void setValidationProcessTO(ValidationProcessTO validationProcessTO) {
		this.validationProcessTO = validationProcessTO;
	}

	public String getReceptionTypeDescription() {
		return receptionTypeDescription;
	}

	public void setReceptionTypeDescription(String receptionTypeDescription) {
		this.receptionTypeDescription = receptionTypeDescription;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}

	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}

	public String getStreamResponseFileDir() {
		return streamResponseFileDir;
	}

	public void setStreamResponseFileDir(String streamResponseFileDir) {
		this.streamResponseFileDir = streamResponseFileDir;
	}

	public Long getIdExternalInterface() {
		return idExternalInterface;
	}

	public void setIdExternalInterface(Long idExternalInterface) {
		this.idExternalInterface = idExternalInterface;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getProcessStateDescription() {
		return processStateDescription;
	}

	public void setProcessStateDescription(String processStateDescription) {
		this.processStateDescription = processStateDescription;
	}

	/**
	 * @return the isDpfInterface
	 */
	public boolean isDpfInterface() {
		return isDpfInterface;
	}

	/**
	 * @param isDpfInterface the isDpfInterface to set
	 */
	public void setDpfInterface(boolean isDpfInterface) {
		this.isDpfInterface = isDpfInterface;
	}

	public InputStream getProcessInputStream() {
		return processInputStream;
	}

	public void setProcessInputStream(InputStream processInputStream) {
		this.processInputStream = processInputStream;
	}

	public File getOriginalTempProcessFile() {
		return originalTempProcessFile;
	}

	public void setOriginalTempProcessFile(File originalTempProcessFile) {
		this.originalTempProcessFile = originalTempProcessFile;
	}

}
