package com.pradera.integration.common.validation.to;

import java.io.Serializable;

public class ResponseDefaultTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String securityClassDesc;
	private String securitySerial;
	private String errorCode;
	private String errorDescription;
	private String messageReceive;
	
	
	public ResponseDefaultTO() {
		super();
	}

	public String getSecurityClassDesc() {
		return securityClassDesc;
	}

	public void setSecurityClassDesc(String securityClassDesc) {
		this.securityClassDesc = securityClassDesc;
	}

	public String getSecuritySerial() {
		return securitySerial;
	}

	public void setSecuritySerial(String securitySerial) {
		this.securitySerial = securitySerial;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getMessageReceive() {
		return messageReceive;
	}

	public void setMessageReceive(String messageReceive) {
		this.messageReceive = messageReceive;
	}
	
}
