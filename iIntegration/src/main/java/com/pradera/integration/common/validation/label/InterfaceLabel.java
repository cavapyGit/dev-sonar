package com.pradera.integration.common.validation.label;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceLabel {
	
	// labels para EMISIONES directas del BCB 
	OPERACIONES("operaciones"),
	NRO_OPERACION("operationNumber"),
	OPERACION("operationCode"),
	FECHA_OPERACION("operationDate"),
	ENTIDAD("entityNemonic"),
	
	TITULO_VALOR("securityObjectTO"),
	CLASE_VALOR("securityClass"),
	SERIE("securitySerial"),
	EMISOR("emisor"),
	TIPO_RENTA("tipoRenta"),
	TIPO_EMISION("tipoEmision"),
	MONEDA("moneda"),
	VALOR_NOMINAL("valorNominal"),
	
	FECHA_EMISION("fechaEmision"),
	FECHA_VENCIMIENTO("fechaVencimiento"),
	TASA_INTERES("tasaInteres"),
	DIAS_CALENDARIO("diasCalendario"),
	TIPO_INTERES("tipoInteres"),
	NRO_CUPONES("nroCupones"),
	FECHA_VCTO_PRIMER_CUPON("fechaVctoPrimerCupon"),
	PERIODICIDAD("periodicidad"),
	MODALIDAD_PAGO("modalidadPago"),
	TIPO_PAGO("tipoPago"),
	PERIODICIDAD_PAGO("periodicidadPago"),
	FACTOR_PAGO("factorPago"),
	IND_REDENCION_ANT("indRedencionAnt"),
	MONTO_MINIMO("montoMinimo"),
	MONTO_MAXIMO("montoMaximo"),
	DESPRENDIMIENTO_CUPON("desprendimientoCupon"),
	NEG_MER_SEC("negMerSec"),
	LIBRE_IMPUESTOS("libreImpuestos"),
	NRO_CUPON("nroCupon"),
	FECHA_VCTO("fechaVcto"),
	CUPON("listCouponEmitionTO"),
	CUPONES("seccCupones"),
	
	//labels para COLOCACIONES directas del BCB
	//OPERACION
	NRO_OBLIGACION("obligationNumber"),
	NUMERO_DE_CUENTA("numeroCuenta"),
	NUMERO_CUENTA("accountNumber"),
	TIPO_CUENTA("accountType"),
	CANT_VAL("stockQuantity"),
	PRECIO("price"),
	DEPARTAMENTO("departament"),
	MUNICIPIO("municipality"),
	
	//TITULAR JURIDICO
	SECC_TITULAR_JURIDICO("juridicAccountTO"),
	TITULAR_NATURAL("listNaturalAccountBcbTO"),
	TIPO_DOCUMENTO("documentType"),
	NUM_DOCUMENTO("documentNumber"),
	RAZON_SOCIAL("razonSocial"),
	CLASE_TITULAR("claseTitular"),
	SECTOR_ECONOMICO("sectorEconomico"),
	ACTIVIDAD_ECONOMICA("actividadEconomica"),
	NUM_TELEFONO("numberPhone"),
	NUM_CELULAR("numberPhoneMovil"),
	NUM_FAX("numberFax"),
	EMAIL("email"),
	PAIS_RESIDENCIA("residenceCountry"),
	PROVINCIA("province"),
	DIR_LEGAL("dirLegal"),
	
	//TITULAR NATURAL
	SECC_TITULARES_NATURALES("seccTitularesNaturales"),
	COD_DUPLICADO("duplicateCod"),
	EXPEDIDO("expended"),
	PRIMER_APELLIDO("firstName"),
	SEGUNDO_APELLIDO("lastName"),
	APELLIDO_CASADA("marriedName"),
	NOMBRES("name"),
	TIPO_PRIORIDAD("priorityType"),
	SEXO("sexo"),
	FECHA_NACIMIENTO("dateBirth"),
	NACIONALIDAD("nationality"),
	NUM_OFICINA("numberPhoneOfice"),
	IND_RESIDENTE("residentInd"),
	DPTO_LEGAL("legalDpto"),
	PROVINCIA_LEGAL("legalProvince"),
	CIUDAD_LEGAL("legalCity"),
	DIRECCION_LEGAL("legalAddress"),
	SEG_NACIONALIDAD("secondNationality"),
	SEGUNDO_TIPO_DOC("secondTypeDoc"),
	SEGUNDO_NUM_DOC("secondNumerDoc"),
	IND_INCAPACIDAD("indUnderAge"),
	ESTADO_CIVIL("maritalStatus"),
	
	
	//Representante Legal
	SECC_REPRESENTANTE("listlegalRepresentativeTO"),
	SEGUNDA_NACIONALIDAD("secondNationality"),
	CANTIDAD("cantidad"),
	
	//emisiones tgn
	SECC_EMISION("emitionsTgnTO"),
	DESC_EMISION("descEmition"),
	MONTO_EMISION("amountEmition"),
	TIPO_INSTRUMENTO("instrumentType"),
	TIPO_VALOR("secuirtyType"),
	FECHA_RESOL_ASFI("dateResolAsfi"),
	NUM_RESOLUCION("numResolution"),
	NEM_EMISOR("nemIssuer"),
	PAIS_EMISION("countryIssuer"),
	INDEMI_COLPRI("indemiColpri"),
	
	//Extrabursatil
	CUENTA_ORIGEN("cuentaOrigen"),
	TASA("tasa"),
	FECHA_HECHOMERCADO("fechaHechoMercado"),
	CANT_VALORES("cantValores"),
	CUENTA_DESTINO("cuentaDestino"),
	SECC_CUENTA_ORIGEN("accountMovementOrigenTO"),
	SECC_VALOR_TRANS("tituloValorTransferTO"),
	SECC_CUENTA_DESTINO("accountMovementDestinTO")
	;
	
	
	
	InterfaceLabel(String name){
		this.name=name;
	}
	
	String name;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static final List<InterfaceLabel> list = new ArrayList<InterfaceLabel>();
	
	/** The Constant lookup. */
	public static final Map<String, InterfaceLabel> lookup = new HashMap<String, InterfaceLabel>();
	static {
		for (InterfaceLabel s : EnumSet.allOf(InterfaceLabel.class)) {
			list.add(s);
			lookup.put(s.getName(), s);
		}
	}
	
	public static InterfaceLabel get(String name) {
		return lookup.get(name);
	}
}