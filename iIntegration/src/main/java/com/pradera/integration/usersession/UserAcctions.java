package com.pradera.integration.usersession;

import java.io.Serializable;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserAcctions.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/02/2013
 */
public class UserAcctions implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6885342572804537437L;

	/** The register. */
	private boolean register;
	
	/** The modify. */
	private boolean modify;
	
	/** The confirm. */
	private boolean confirm;
	
	/** The reject. */
	private boolean reject;
	
	/** The block. */
	private boolean block;
	
	/** The unblock. */
	private boolean unblock;
	
	/** The delete. */
	private boolean delete;
	
	/** The search. */
	private boolean search;
	
	/** The add. */
	private boolean add;
	
	/** The Annular. */
	private boolean annular;
	
	/** The approve. */
	private boolean approve;
	
	/** The review. */
	private boolean review;
	
	/** The apply. */
	private boolean apply;
	
	/** The authorize. */
	private boolean authorize;
	
	/** The cancel. */
	private boolean cancel;
	
	/** The settlement. */
	private boolean settlement;
	
	/** The activate. */
	private boolean activate;
	
	/** The desactivate. */
	private boolean deactivate;
	
	/** The confirm depositary. */
	private boolean confirmDepositary;
	
	/** The confirm issuer. */
	private boolean confirmIssuer;
	
	/** The preliminary. */
	private boolean preliminary;
	
	/** The definitive. */
	private boolean definitive;
	
	/** The adjustments. */
	private boolean adjustments;
	
	/** The generate. */
	private boolean generate;
	
	/** The execute. */
	private boolean execute;
	
	/** The update. */
	private boolean update;
	
	/** The deliver. */
	private boolean deliver;
	
	/** The appeal. */
	private boolean appeal;
	
	/** The certify. */
	private boolean certify;
	
	/** The confirm block. */
	private boolean confirmBlock;
	
	/** The reject block. */
	private boolean rejectBlock;
	
	/** The confirm unblock. */
	private boolean confirmUnblock;
	
	/** The reject unblock. */
	private boolean rejectUnblock;
	
	/** The confirm delete. */
	private boolean confirmDelete;
	
	/** The reject delete. */
	private boolean rejectDelete;
	
	/** The confirm annular. */
	private boolean confirmAnnular;
	
	/** The reject annular. */
	private boolean rejectAnnular;
	
	/** The confirm modify. */
	private boolean confirmModify;
	
	/** The reject modify. */
	private boolean rejectModify;
	
	//privileges of system
	
	/** The id privilege register. */
	private Integer idPrivilegeRegister;
	
	/** The id privilege modify. */
	private Integer idPrivilegeModify;
	
	/** The id privilege confirm. */
	private Integer idPrivilegeConfirm;
	
	/** The id privilege reject. */
	private Integer idPrivilegeReject;
	
	/** The id privilege block. */
	private Integer idPrivilegeBlock;
	
	/** The id privilege unblock. */
	private Integer idPrivilegeUnblock;
	
	/** The id privilege delete. */
	private Integer idPrivilegeDelete;
	
	/** The id privilege search. */
	private Integer idPrivilegeSearch;
	
	/** The id privilege add. */
	private Integer idPrivilegeAdd;
	
	/** The id privilege Annular. */
	private Integer idPrivilegeAnnular;
	
	/** The id privilege approve. */
	private Integer idPrivilegeApprove;
	
	/** The id privilege review. */
	private Integer idPrivilegeReview;
	
	/** The id privilege apply. */
	private Integer idPrivilegeApply;
	
	/** The id privilege authorize. */
	private Integer idPrivilegeAuthorize;
	
	/** The id privilige cancel. */
	private Integer idPrivilegeCancel;
	
	/** The id privilege settlement. */
	private Integer idPrivilegeSettlement;
	
	/** The id privilege activate. */
	private Integer idPrivilegeActivate;
	
	/** The id privilege desactivate. */
	private Integer idPrivilegeDeactivate;
	
	/** The id privilege confirm depositary. */
	private Integer idPrivilegeConfirmDepositary;
	
	/** The id privilege confirm issuer. */
	private Integer idPrivilegeConfirmIssuer;
	
	/** The id privilege preliminary. */
	private Integer idPrivilegePreliminary;
	
	/** The id privilege definitive. */
	private Integer idPrivilegeDefinitive;
	
	/** The id privilege adjustments. */
	private Integer idPrivilegeAdjustments;
	
	/** The id privilege generate. */
	private Integer idPrivilegeGenerate;
	
	/** The id privilege execute. */
	private Integer idPrivilegeExecute;
	
	/** The id privilege update. */
	private Integer idPrivilegeUpdate;
	
	/** The id privilege deliver. */
	private Integer idPrivilegeDeliver;
	
	/** The id privilege appeal. */
	private Integer idPrivilegeAppeal;
	
	/** The id privilege certify. */
	private Integer idPrivilegeCertify;
	
	/** The id privilege confirm block. */
	private Integer idPrivilegeConfirmBlock;
	
	/** The id privilege reject block. */
	private Integer idPrivilegeRejectBlock;
	
	/** The id privilege confirm unblock. */
	private Integer idPrivilegeConfirmUnblock;
	
	/** The id privilege reject unblock. */
	private Integer idPrivilegeRejectUnblock;
	
	/** The id privilege confirm delete. */
	private Integer idPrivilegeConfirmDelete;
	
	/** The id privilege reject delete. */
	private Integer idPrivilegeRejectDelete;
	
	/** The id privilege confirm annular. */
	private Integer idPrivilegeConfirmAnnular;
	
	/** The id privilege reject annular. */
	private Integer idPrivilegeRejectAnnular;
	
	/** The id privilege confirm modify. */
	private Integer idPrivilegeConfirmModify;
	
	/** The id privilege reject modify. */
	private Integer idPrivilegeRejectModify;
	
	/**
	 * Gets the id privilege annular.
	 *
	 * @return the id privilege annular
	 */
	public Integer getIdPrivilegeAnnular() {
		return idPrivilegeAnnular;
	}

	/**
	 * Sets the id privilege annular.
	 *
	 * @param idPrivilegeAnnular the new id privilege annular
	 */
	public void setIdPrivilegeAnnular(Integer idPrivilegeAnnular) {
		this.idPrivilegeAnnular = idPrivilegeAnnular;
	}

	/**
	 * Instantiates a new user acctions.
	 */
	public UserAcctions() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Checks if is register.
	 *
	 * @return true, if is register
	 */
	public boolean isRegister() {
		return register;
	}

	/**
	 * Sets the register.
	 *
	 * @param register the new register
	 */
	public void setRegister(boolean register) {
		this.register = register;
	}

	/**
	 * Checks if is modify.
	 *
	 * @return true, if is modify
	 */
	public boolean isModify() {
		return modify;
	}

	/**
	 * Sets the modify.
	 *
	 * @param modify the new modify
	 */
	public void setModify(boolean modify) {
		this.modify = modify;
	}

	/**
	 * Checks if is confirm.
	 *
	 * @return true, if is confirm
	 */
	public boolean isConfirm() {
		return confirm;
	}

	/**
	 * Sets the confirm.
	 *
	 * @param confirm the new confirm
	 */
	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}	

	/**
	 * Checks if is reject.
	 *
	 * @return true, if is reject
	 */
	public boolean isReject() {
		return reject;
	}

	/**
	 * Sets the reject.
	 *
	 * @param reject the new reject
	 */
	public void setReject(boolean reject) {
		this.reject = reject;
	}

	/**
	 * Checks if is block.
	 *
	 * @return true, if is block
	 */
	public boolean isBlock() {
		return block;
	}

	/**
	 * Sets the block.
	 *
	 * @param block the new block
	 */
	public void setBlock(boolean block) {
		this.block = block;
	}

	/**
	 * Checks if is unblock.
	 *
	 * @return true, if is unblock
	 */
	public boolean isUnblock() {
		return unblock;
	}

	/**
	 * Sets the unblock.
	 *
	 * @param unblock the new unblock
	 */
	public void setUnblock(boolean unblock) {
		this.unblock = unblock;
	}

	/**
	 * Checks if is delete.
	 *
	 * @return true, if is delete
	 */
	public boolean isDelete() {
		return delete;
	}

	/**
	 * Sets the delete.
	 *
	 * @param delete the new delete
	 */
	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	/**
	 * Checks if is search.
	 *
	 * @return true, if is search
	 */
	public boolean isSearch() {
		return search;
	}

	/**
	 * Sets the search.
	 *
	 * @param search the new search
	 */
	public void setSearch(boolean search) {
		this.search = search;
	}

	/**
	 * Checks if is adds the.
	 *
	 * @return true, if is adds the
	 */
	public boolean isAdd() {
		return add;
	}

	/**
	 * Checks if is annular.
	 *
	 * @return true, if is annular
	 */
	public boolean isAnnular() {
		return annular;
	}

	/**
	 * Sets the annular.
	 *
	 * @param annular the new annular
	 */
	public void setAnnular(boolean annular) {
		this.annular = annular;
	}

	/**
	 * Sets the adds the.
	 *
	 * @param add the new adds the
	 */
	public void setAdd(boolean add) {
		this.add = add;
	}

	/**
	 * Gets the id privilege register.
	 *
	 * @return the id privilege register
	 */
	public Integer getIdPrivilegeRegister() {
		return idPrivilegeRegister;
	}

	/**
	 * Sets the id privilege register.
	 *
	 * @param idPrivilegeRegister the new id privilege register
	 */
	public void setIdPrivilegeRegister(Integer idPrivilegeRegister) {
		this.idPrivilegeRegister = idPrivilegeRegister;
	}

	/**
	 * Gets the id privilege modify.
	 *
	 * @return the id privilege modify
	 */
	public Integer getIdPrivilegeModify() {
		return idPrivilegeModify;
	}

	/**
	 * Sets the id privilege modify.
	 *
	 * @param idPrivilegeModify the new id privilege modify
	 */
	public void setIdPrivilegeModify(Integer idPrivilegeModify) {
		this.idPrivilegeModify = idPrivilegeModify;
	}

	/**
	 * Gets the id privilege confirm.
	 *
	 * @return the id privilege confirm
	 */
	public Integer getIdPrivilegeConfirm() {
		return idPrivilegeConfirm;
	}

	/**
	 * Sets the id privilege confirm.
	 *
	 * @param idPrivilegeConfirm the new id privilege confirm
	 */
	public void setIdPrivilegeConfirm(Integer idPrivilegeConfirm) {
		this.idPrivilegeConfirm = idPrivilegeConfirm;
	}

	/**
	 * Gets the id privilege reject.
	 *
	 * @return the id privilege reject
	 */
	public Integer getIdPrivilegeReject() {
		return idPrivilegeReject;
	}

	/**
	 * Sets the id privilege reject.
	 *
	 * @param idPrivilegeReject the new id privilege reject
	 */
	public void setIdPrivilegeReject(Integer idPrivilegeReject) {
		this.idPrivilegeReject = idPrivilegeReject;
	}

	/**
	 * Gets the id privilege block.
	 *
	 * @return the id privilege block
	 */
	public Integer getIdPrivilegeBlock() {
		return idPrivilegeBlock;
	}

	/**
	 * Sets the id privilege block.
	 *
	 * @param idPrivilegeBlock the new id privilege block
	 */
	public void setIdPrivilegeBlock(Integer idPrivilegeBlock) {
		this.idPrivilegeBlock = idPrivilegeBlock;
	}

	/**
	 * Gets the id privilege unblock.
	 *
	 * @return the id privilege unblock
	 */
	public Integer getIdPrivilegeUnblock() {
		return idPrivilegeUnblock;
	}

	/**
	 * Sets the id privilege unblock.
	 *
	 * @param idPrivilegeUnblock the new id privilege unblock
	 */
	public void setIdPrivilegeUnblock(Integer idPrivilegeUnblock) {
		this.idPrivilegeUnblock = idPrivilegeUnblock;
	}

	/**
	 * Gets the id privilege delete.
	 *
	 * @return the id privilege delete
	 */
	public Integer getIdPrivilegeDelete() {
		return idPrivilegeDelete;
	}

	/**
	 * Sets the id privilege delete.
	 *
	 * @param idPrivilegeDelete the new id privilege delete
	 */
	public void setIdPrivilegeDelete(Integer idPrivilegeDelete) {
		this.idPrivilegeDelete = idPrivilegeDelete;
	}

	/**
	 * Gets the id privilege search.
	 *
	 * @return the id privilege search
	 */
	public Integer getIdPrivilegeSearch() {
		return idPrivilegeSearch;
	}

	/**
	 * Sets the id privilege search.
	 *
	 * @param idPrivilegeSearch the new id privilege search
	 */
	public void setIdPrivilegeSearch(Integer idPrivilegeSearch) {
		this.idPrivilegeSearch = idPrivilegeSearch;
	}

	/**
	 * Gets the id privilege add.
	 *
	 * @return the id privilege add
	 */
	public Integer getIdPrivilegeAdd() {
		return idPrivilegeAdd;
	}

	/**
	 * Sets the id privilege add.
	 *
	 * @param idPrivilegeAdd the new id privilege add
	 */
	public void setIdPrivilegeAdd(Integer idPrivilegeAdd) {
		this.idPrivilegeAdd = idPrivilegeAdd;
	}
	
	

	/**
	 * Checks if is approve.
	 *
	 * @return true, if is approve
	 */
	public boolean isApprove() {
		return approve;
	}

	/**
	 * Sets the approve.
	 *
	 * @param approve the new approve
	 */
	public void setApprove(boolean approve) {
		this.approve = approve;
	}

	/**
	 * Gets the id privilege approve.
	 *
	 * @return the id privilege approve
	 */
	public Integer getIdPrivilegeApprove() {
		return idPrivilegeApprove;
	}

	/**
	 * Sets the id privilege approve.
	 *
	 * @param idPrivilegeApprove the new id privilege approve
	 */
	public void setIdPrivilegeApprove(Integer idPrivilegeApprove) {
		this.idPrivilegeApprove = idPrivilegeApprove;
	}
	
	
	
	
	/**
	 * Checks if is review.
	 *
	 * @return true, if is review
	 */
	public boolean isReview() {
		return review;
	}

	/**
	 * Sets the review.
	 *
	 * @param review the new review
	 */
	public void setReview(boolean review) {
		this.review = review;
	}

	/**
	 * Gets the id privilege review.
	 *
	 * @return the id privilege review
	 */
	public Integer getIdPrivilegeReview() {
		return idPrivilegeReview;
	}

	/**
	 * Sets the id privilege review.
	 *
	 * @param idPrivilegeReview the new id privilege review
	 */
	public void setIdPrivilegeReview(Integer idPrivilegeReview) {
		this.idPrivilegeReview = idPrivilegeReview;
	}
	
	

	/**
	 * Gets the id privilege apply.
	 *
	 * @return the id privilege apply
	 */
	public Integer getIdPrivilegeApply() {
		return idPrivilegeApply;
	}

	/**
	 * Sets the id privilege apply.
	 *
	 * @param idPrivilegeApply the new id privilege apply
	 */
	public void setIdPrivilegeApply(Integer idPrivilegeApply) {
		this.idPrivilegeApply = idPrivilegeApply;
	}

	/**
	 * Gets the id privilege authorize.
	 *
	 * @return the id privilege authorize
	 */
	public Integer getIdPrivilegeAuthorize() {
		return idPrivilegeAuthorize;
	}

	/**
	 * Sets the id privilege authorize.
	 *
	 * @param idPrivilegeAuthorize the new id privilege authorize
	 */
	public void setIdPrivilegeAuthorize(Integer idPrivilegeAuthorize) {
		this.idPrivilegeAuthorize = idPrivilegeAuthorize;
	}
	
	/**
	 * Checks if is apply.
	 *
	 * @return true, if is apply
	 */
	public boolean isApply() {
		return apply;
	}

	/**
	 * Sets the apply.
	 *
	 * @param apply the new apply
	 */
	public void setApply(boolean apply) {
		this.apply = apply;
	}

	/**
	 * Checks if is authorize.
	 *
	 * @return true, if is authorize
	 */
	public boolean isAuthorize() {
		return authorize;
	}

	/**
	 * Sets the authorize.
	 *
	 * @param authorize the new authorize
	 */
	public void setAuthorize(boolean authorize) {
		this.authorize = authorize;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "UserAcctions [register=" + register + ", modify=" + modify
				+ ", confirm=" + confirm + ", reject=" + reject + ", block="
				+ block + ", unblock=" + unblock + ", delete=" + delete
				+ ", search=" + search + ", add=" + add + ", annular="
				+ annular + ", approve=" + approve + ", review=" + review + "]";
	}

	/**
	 * Checks if is cancel.
	 *
	 * @return true, if is cancel
	 */
	public boolean isCancel() {
		return cancel;
	}

	/**
	 * Sets the cancel.
	 *
	 * @param cancel the new cancel
	 */
	public void setCancel(boolean cancel) {
		this.cancel = cancel;
	}

	/**
	 * Checks if is settlement.
	 *
	 * @return true, if is settlement
	 */
	public boolean isSettlement() {
		return settlement;
	}

	/**
	 * Sets the settlement.
	 *
	 * @param settlement the new settlement
	 */
	public void setSettlement(boolean settlement) {
		this.settlement = settlement;
	}

	/**
	 * Gets the id privilege cancel.
	 *
	 * @return the id privilige cancel
	 */
	public Integer getIdPrivilegeCancel() {
		return idPrivilegeCancel;
	}

	/**
	 * Sets the id privilege cancel.
	 *
	 * @param idPriviligeCancel the new id privilige cancel
	 */
	public void setIdPrivilegeCancel(Integer idPriviligeCancel) {
		this.idPrivilegeCancel = idPriviligeCancel;
	}

	/**
	 * Gets the id privilege settlement.
	 *
	 * @return the id privilege settlement
	 */
	public Integer getIdPrivilegeSettlement() {
		return idPrivilegeSettlement;
	}

	/**
	 * Sets the id privilege settlement.
	 *
	 * @param idPrivilegeSettlement the new id privilege settlement
	 */
	public void setIdPrivilegeSettlement(Integer idPrivilegeSettlement) {
		this.idPrivilegeSettlement = idPrivilegeSettlement;
	}

	/**
	 * Checks if is activate.
	 *
	 * @return true, if is activate
	 */
	public boolean isActivate() {
		return activate;
	}

	/**
	 * Sets the activate.
	 *
	 * @param activate the new activate
	 */
	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	/**
	 * Checks if is deactivate.
	 *
	 * @return true, if is deactivate
	 */
	public boolean isDeactivate() {
		return deactivate;
	}

	/**
	 * Sets the deactivate.
	 *
	 * @param deactivate the new deactivate
	 */
	public void setDeactivate(boolean deactivate) {
		this.deactivate = deactivate;
	}

	/**
	 * Gets the id privilege activate.
	 *
	 * @return the id privilege activate
	 */
	public Integer getIdPrivilegeActivate() {
		return idPrivilegeActivate;
	}

	/**
	 * Sets the id privilege activate.
	 *
	 * @param idPrivilegeActivate the new id privilege activate
	 */
	public void setIdPrivilegeActivate(Integer idPrivilegeActivate) {
		this.idPrivilegeActivate = idPrivilegeActivate;
	}

	/**
	 * Gets the id privilege deactivate.
	 *
	 * @return the id privilege deactivate
	 */
	public Integer getIdPrivilegeDeactivate() {
		return idPrivilegeDeactivate;
	}

	/**
	 * Sets the id privilege deactivate.
	 *
	 * @param idPrivilegeDeactivate the new id privilege deactivate
	 */
	public void setIdPrivilegeDeactivate(Integer idPrivilegeDeactivate) {
		this.idPrivilegeDeactivate = idPrivilegeDeactivate;
	}

	/**
	 * Checks if is confirm depositary.
	 *
	 * @return true, if is confirm depositary
	 */
	public boolean isConfirmDepositary() {
		return confirmDepositary;
	}

	/**
	 * Sets the confirm depositary.
	 *
	 * @param confirmDepositary the new confirm depositary
	 */
	public void setConfirmDepositary(boolean confirmDepositary) {
		this.confirmDepositary = confirmDepositary;
	}

	/**
	 * Checks if is confirm issuer.
	 *
	 * @return true, if is confirm issuer
	 */
	public boolean isConfirmIssuer() {
		return confirmIssuer;
	}

	/**
	 * Sets the confirm issuer.
	 *
	 * @param confirmIssuer the new confirm issuer
	 */
	public void setConfirmIssuer(boolean confirmIssuer) {
		this.confirmIssuer = confirmIssuer;
	}

	/**
	 * Checks if is preliminary.
	 *
	 * @return true, if is preliminary
	 */
	public boolean isPreliminary() {
		return preliminary;
	}

	/**
	 * Sets the preliminary.
	 *
	 * @param preliminary the new preliminary
	 */
	public void setPreliminary(boolean preliminary) {
		this.preliminary = preliminary;
	}

	/**
	 * Checks if is definitive.
	 *
	 * @return true, if is definitive
	 */
	public boolean isDefinitive() {
		return definitive;
	}

	/**
	 * Sets the definitive.
	 *
	 * @param definitive the new definitive
	 */
	public void setDefinitive(boolean definitive) {
		this.definitive = definitive;
	}

	/**
	 * Checks if is adjustments.
	 *
	 * @return true, if is adjustments
	 */
	public boolean isAdjustments() {
		return adjustments;
	}

	/**
	 * Sets the adjustments.
	 *
	 * @param adjustments the new adjustments
	 */
	public void setAdjustments(boolean adjustments) {
		this.adjustments = adjustments;
	}

	/**
	 * Gets the id privilege confirm depositary.
	 *
	 * @return the id privilege confirm depositary
	 */
	public Integer getIdPrivilegeConfirmDepositary() {
		return idPrivilegeConfirmDepositary;
	}

	/**
	 * Sets the id privilege confirm depositary.
	 *
	 * @param idPrivilegeConfirmDepositary the new id privilege confirm depositary
	 */
	public void setIdPrivilegeConfirmDepositary(Integer idPrivilegeConfirmDepositary) {
		this.idPrivilegeConfirmDepositary = idPrivilegeConfirmDepositary;
	}

	/**
	 * Gets the id privilege confirm issuer.
	 *
	 * @return the id privilege confirm issuer
	 */
	public Integer getIdPrivilegeConfirmIssuer() {
		return idPrivilegeConfirmIssuer;
	}

	/**
	 * Sets the id privilege confirm issuer.
	 *
	 * @param idPrivilegeConfirmIssuer the new id privilege confirm issuer
	 */
	public void setIdPrivilegeConfirmIssuer(Integer idPrivilegeConfirmIssuer) {
		this.idPrivilegeConfirmIssuer = idPrivilegeConfirmIssuer;
	}

	/**
	 * Gets the id privilege preliminary.
	 *
	 * @return the id privilege preliminary
	 */
	public Integer getIdPrivilegePreliminary() {
		return idPrivilegePreliminary;
	}

	/**
	 * Sets the id privilege preliminary.
	 *
	 * @param idPrivilegePreliminary the new id privilege preliminary
	 */
	public void setIdPrivilegePreliminary(Integer idPrivilegePreliminary) {
		this.idPrivilegePreliminary = idPrivilegePreliminary;
	}

	/**
	 * Gets the id privilege definitive.
	 *
	 * @return the id privilege definitive
	 */
	public Integer getIdPrivilegeDefinitive() {
		return idPrivilegeDefinitive;
	}

	/**
	 * Sets the id privilege definitive.
	 *
	 * @param idPrivilegeDefinitive the new id privilege definitive
	 */
	public void setIdPrivilegeDefinitive(Integer idPrivilegeDefinitive) {
		this.idPrivilegeDefinitive = idPrivilegeDefinitive;
	}

	/**
	 * Gets the id privilege adjustments.
	 *
	 * @return the id privilege adjustments
	 */
	public Integer getIdPrivilegeAdjustments() {
		return idPrivilegeAdjustments;
	}

	/**
	 * Sets the id privilege adjustments.
	 *
	 * @param idPrivilegeAdjustments the new id privilege adjustments
	 */
	public void setIdPrivilegeAdjustments(Integer idPrivilegeAdjustments) {
		this.idPrivilegeAdjustments = idPrivilegeAdjustments;
	}

	/**
	 * Checks if is generate.
	 *
	 * @return true, if is generate
	 */
	public boolean isGenerate() {
		return generate;
	}

	/**
	 * Sets the generate.
	 *
	 * @param generate the new generate
	 */
	public void setGenerate(boolean generate) {
		this.generate = generate;
	}

	/**
	 * Checks if is execute.
	 *
	 * @return true, if is execute
	 */
	public boolean isExecute() {
		return execute;
	}

	/**
	 * Sets the execute.
	 *
	 * @param execute the new execute
	 */
	public void setExecute(boolean execute) {
		this.execute = execute;
	}

	/**
	 * Gets the id privilege generate.
	 *
	 * @return the id privilege generate
	 */
	public Integer getIdPrivilegeGenerate() {
		return idPrivilegeGenerate;
	}

	/**
	 * Sets the id privilege generate.
	 *
	 * @param idPrivilegeGenerate the new id privilege generate
	 */
	public void setIdPrivilegeGenerate(Integer idPrivilegeGenerate) {
		this.idPrivilegeGenerate = idPrivilegeGenerate;
	}

	/**
	 * Gets the id privilege execute.
	 *
	 * @return the id privilege execute
	 */
	public Integer getIdPrivilegeExecute() {
		return idPrivilegeExecute;
	}

	/**
	 * Sets the id privilege execute.
	 *
	 * @param idPrivilegeExecute the new id privilege execute
	 */
	public void setIdPrivilegeExecute(Integer idPrivilegeExecute) {
		this.idPrivilegeExecute = idPrivilegeExecute;
	}

	/**
	 * Checks if is update.
	 *
	 * @return true, if is update
	 */
	public boolean isUpdate() {
		return update;
	}

	/**
	 * Sets the update.
	 *
	 * @param update the new update
	 */
	public void setUpdate(boolean update) {
		this.update = update;
	}

	/**
	 * Gets the id privilege update.
	 *
	 * @return the id privilege update
	 */
	public Integer getIdPrivilegeUpdate() {
		return idPrivilegeUpdate;
	}

	/**
	 * Sets the id privilege update.
	 *
	 * @param idPrivilegeUpdate the new id privilege update
	 */
	public void setIdPrivilegeUpdate(Integer idPrivilegeUpdate) {
		this.idPrivilegeUpdate = idPrivilegeUpdate;
	}

	/**
	 * Checks if is deliver.
	 *
	 * @return true, if is deliver
	 */
	public boolean isDeliver() {
		return deliver;
	}

	/**
	 * Sets the deliver.
	 *
	 * @param deliver the new deliver
	 */
	public void setDeliver(boolean deliver) {
		this.deliver = deliver;
	}

	/**
	 * Gets the id privilege deliver.
	 *
	 * @return the id privilege deliver
	 */
	public Integer getIdPrivilegeDeliver() {
		return idPrivilegeDeliver;
	}

	/**
	 * Sets the id privilege deliver.
	 *
	 * @param idPrivilegeDeliver the new id privilege deliver
	 */
	public void setIdPrivilegeDeliver(Integer idPrivilegeDeliver) {
		this.idPrivilegeDeliver = idPrivilegeDeliver;
	}

	/**
	 * Checks if is appeal.
	 *
	 * @return true, if is appeal
	 */
	public boolean isAppeal() {
		return appeal;
	}

	/**
	 * Sets the appeal.
	 *
	 * @param appeal the new appeal
	 */
	public void setAppeal(boolean appeal) {
		this.appeal = appeal;
	}

	/**
	 * Gets the id privilege appeal.
	 *
	 * @return the id privilege appeal
	 */
	public Integer getIdPrivilegeAppeal() {
		return idPrivilegeAppeal;
	}

	/**
	 * Sets the id privilege appeal.
	 *
	 * @param idPrivilegeAppeal the new id privilege appeal
	 */
	public void setIdPrivilegeAppeal(Integer idPrivilegeAppeal) {
		this.idPrivilegeAppeal = idPrivilegeAppeal;
	}

	/**
	 * Checks if is certify.
	 *
	 * @return true, if is certify
	 */
	public boolean isCertify() {
		return certify;
	}

	/**
	 * Sets the certify.
	 *
	 * @param certify the new certify
	 */
	public void setCertify(boolean certify) {
		this.certify = certify;
	}

	/**
	 * Gets the id privilege certify.
	 *
	 * @return the id privilege certify
	 */
	public Integer getIdPrivilegeCertify() {
		return idPrivilegeCertify;
	}

	/**
	 * Sets the id privilege certify.
	 *
	 * @param idPrivilegeCertify the new id privilege certify
	 */
	public void setIdPrivilegeCertify(Integer idPrivilegeCertify) {
		this.idPrivilegeCertify = idPrivilegeCertify;
	}

	/**
	 * Checks if is confirm block.
	 *
	 * @return true, if is confirm block
	 */
	public boolean isConfirmBlock() {
		return confirmBlock;
	}

	/**
	 * Sets the confirm block.
	 *
	 * @param confirmBlock the new confirm block
	 */
	public void setConfirmBlock(boolean confirmBlock) {
		this.confirmBlock = confirmBlock;
	}

	/**
	 * Checks if is reject block.
	 *
	 * @return true, if is reject block
	 */
	public boolean isRejectBlock() {
		return rejectBlock;
	}

	/**
	 * Sets the reject block.
	 *
	 * @param rejectBlock the new reject block
	 */
	public void setRejectBlock(boolean rejectBlock) {
		this.rejectBlock = rejectBlock;
	}

	/**
	 * Checks if is confirm unblock.
	 *
	 * @return true, if is confirm unblock
	 */
	public boolean isConfirmUnblock() {
		return confirmUnblock;
	}

	/**
	 * Sets the confirm unblock.
	 *
	 * @param confirmUnblock the new confirm unblock
	 */
	public void setConfirmUnblock(boolean confirmUnblock) {
		this.confirmUnblock = confirmUnblock;
	}

	/**
	 * Checks if is reject unblock.
	 *
	 * @return true, if is reject unblock
	 */
	public boolean isRejectUnblock() {
		return rejectUnblock;
	}

	/**
	 * Sets the reject unblock.
	 *
	 * @param rejectUnblock the new reject unblock
	 */
	public void setRejectUnblock(boolean rejectUnblock) {
		this.rejectUnblock = rejectUnblock;
	}

	/**
	 * Checks if is confirm delete.
	 *
	 * @return true, if is confirm delete
	 */
	public boolean isConfirmDelete() {
		return confirmDelete;
	}

	/**
	 * Sets the confirm delete.
	 *
	 * @param confirmDelete the new confirm delete
	 */
	public void setConfirmDelete(boolean confirmDelete) {
		this.confirmDelete = confirmDelete;
	}

	/**
	 * Checks if is reject delete.
	 *
	 * @return true, if is reject delete
	 */
	public boolean isRejectDelete() {
		return rejectDelete;
	}

	/**
	 * Sets the reject delete.
	 *
	 * @param rejectDelete the new reject delete
	 */
	public void setRejectDelete(boolean rejectDelete) {
		this.rejectDelete = rejectDelete;
	}

	/**
	 * Checks if is confirm annular.
	 *
	 * @return true, if is confirm annular
	 */
	public boolean isConfirmAnnular() {
		return confirmAnnular;
	}

	/**
	 * Sets the confirm annular.
	 *
	 * @param confirmAnnular the new confirm annular
	 */
	public void setConfirmAnnular(boolean confirmAnnular) {
		this.confirmAnnular = confirmAnnular;
	}

	/**
	 * Checks if is reject annular.
	 *
	 * @return true, if is reject annular
	 */
	public boolean isRejectAnnular() {
		return rejectAnnular;
	}

	/**
	 * Sets the reject annular.
	 *
	 * @param rejectAnnular the new reject annular
	 */
	public void setRejectAnnular(boolean rejectAnnular) {
		this.rejectAnnular = rejectAnnular;
	}

	/**
	 * Gets the id privilege confirm block.
	 *
	 * @return the id privilege confirm block
	 */
	public Integer getIdPrivilegeConfirmBlock() {
		return idPrivilegeConfirmBlock;
	}

	/**
	 * Sets the id privilege confirm block.
	 *
	 * @param idPrivilegeConfirmBlock the new id privilege confirm block
	 */
	public void setIdPrivilegeConfirmBlock(Integer idPrivilegeConfirmBlock) {
		this.idPrivilegeConfirmBlock = idPrivilegeConfirmBlock;
	}

	/**
	 * Gets the id privilege reject block.
	 *
	 * @return the id privilege reject block
	 */
	public Integer getIdPrivilegeRejectBlock() {
		return idPrivilegeRejectBlock;
	}

	/**
	 * Sets the id privilege reject block.
	 *
	 * @param idPrivilegeRejectBlock the new id privilege reject block
	 */
	public void setIdPrivilegeRejectBlock(Integer idPrivilegeRejectBlock) {
		this.idPrivilegeRejectBlock = idPrivilegeRejectBlock;
	}

	/**
	 * Gets the id privilege confirm unblock.
	 *
	 * @return the id privilege confirm unblock
	 */
	public Integer getIdPrivilegeConfirmUnblock() {
		return idPrivilegeConfirmUnblock;
	}

	/**
	 * Sets the id privilege confirm unblock.
	 *
	 * @param idPrivilegeConfirmUnblock the new id privilege confirm unblock
	 */
	public void setIdPrivilegeConfirmUnblock(Integer idPrivilegeConfirmUnblock) {
		this.idPrivilegeConfirmUnblock = idPrivilegeConfirmUnblock;
	}

	/**
	 * Gets the id privilege reject unblock.
	 *
	 * @return the id privilege reject unblock
	 */
	public Integer getIdPrivilegeRejectUnblock() {
		return idPrivilegeRejectUnblock;
	}

	/**
	 * Sets the id privilege reject unblock.
	 *
	 * @param idPrivilegeRejectUnblock the new id privilege reject unblock
	 */
	public void setIdPrivilegeRejectUnblock(Integer idPrivilegeRejectUnblock) {
		this.idPrivilegeRejectUnblock = idPrivilegeRejectUnblock;
	}

	/**
	 * Gets the id privilege confirm delete.
	 *
	 * @return the id privilege confirm delete
	 */
	public Integer getIdPrivilegeConfirmDelete() {
		return idPrivilegeConfirmDelete;
	}

	/**
	 * Sets the id privilege confirm delete.
	 *
	 * @param idPrivilegeConfirmDelete the new id privilege confirm delete
	 */
	public void setIdPrivilegeConfirmDelete(Integer idPrivilegeConfirmDelete) {
		this.idPrivilegeConfirmDelete = idPrivilegeConfirmDelete;
	}

	/**
	 * Gets the id privilege reject delete.
	 *
	 * @return the id privilege reject delete
	 */
	public Integer getIdPrivilegeRejectDelete() {
		return idPrivilegeRejectDelete;
	}

	/**
	 * Sets the id privilege reject delete.
	 *
	 * @param idPrivilegeRejectDelete the new id privilege reject delete
	 */
	public void setIdPrivilegeRejectDelete(Integer idPrivilegeRejectDelete) {
		this.idPrivilegeRejectDelete = idPrivilegeRejectDelete;
	}

	/**
	 * Gets the id privilege confirm annular.
	 *
	 * @return the id privilege confirm annular
	 */
	public Integer getIdPrivilegeConfirmAnnular() {
		return idPrivilegeConfirmAnnular;
	}

	/**
	 * Sets the id privilege confirm annular.
	 *
	 * @param idPrivilegeConfirmAnnular the new id privilege confirm annular
	 */
	public void setIdPrivilegeConfirmAnnular(Integer idPrivilegeConfirmAnnular) {
		this.idPrivilegeConfirmAnnular = idPrivilegeConfirmAnnular;
	}

	/**
	 * Gets the id privilege reject annular.
	 *
	 * @return the id privilege reject annular
	 */
	public Integer getIdPrivilegeRejectAnnular() {
		return idPrivilegeRejectAnnular;
	}

	/**
	 * Sets the id privilege reject annular.
	 *
	 * @param idPrivilegeRejectAnnular the new id privilege reject annular
	 */
	public void setIdPrivilegeRejectAnnular(Integer idPrivilegeRejectAnnular) {
		this.idPrivilegeRejectAnnular = idPrivilegeRejectAnnular;
	}

	/**
	 * Checks if is confirm modify.
	 *
	 * @return true, if is confirm modify
	 */
	public boolean isConfirmModify() {
		return confirmModify;
	}

	/**
	 * Sets the confirm modify.
	 *
	 * @param confirmModify the new confirm modify
	 */
	public void setConfirmModify(boolean confirmModify) {
		this.confirmModify = confirmModify;
	}

	/**
	 * Checks if is reject modify.
	 *
	 * @return true, if is reject modify
	 */
	public boolean isRejectModify() {
		return rejectModify;
	}

	/**
	 * Sets the reject modify.
	 *
	 * @param rejectModify the new reject modify
	 */
	public void setRejectModify(boolean rejectModify) {
		this.rejectModify = rejectModify;
	}

	/**
	 * Gets the id privilege confirm modify.
	 *
	 * @return the id privilege confirm modify
	 */
	public Integer getIdPrivilegeConfirmModify() {
		return idPrivilegeConfirmModify;
	}

	/**
	 * Sets the id privilege confirm modify.
	 *
	 * @param idPrivilegeConfirmModify the new id privilege confirm modify
	 */
	public void setIdPrivilegeConfirmModify(Integer idPrivilegeConfirmModify) {
		this.idPrivilegeConfirmModify = idPrivilegeConfirmModify;
	}

	/**
	 * Gets the id privilege reject modify.
	 *
	 * @return the id privilege reject modify
	 */
	public Integer getIdPrivilegeRejectModify() {
		return idPrivilegeRejectModify;
	}

	/**
	 * Sets the id privilege reject modify.
	 *
	 * @param idPrivilegeRejectModify the new id privilege reject modify
	 */
	public void setIdPrivilegeRejectModify(Integer idPrivilegeRejectModify) {
		this.idPrivilegeRejectModify = idPrivilegeRejectModify;
	}	
	
	
		
}
