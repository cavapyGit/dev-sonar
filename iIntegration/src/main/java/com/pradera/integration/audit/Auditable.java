package com.pradera.integration.audit;

import java.util.List;
import java.util.Map;

import com.pradera.integration.contextholder.LoggerUser;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface Auditable.
 * using for audit field
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
public interface Auditable {
	
	/**
	 * Sets the audit.
	 *
	 * @param loggerUser the new audit
	 */
	public void setAudit(LoggerUser loggerUser);
	
	/**
	 * Gets the list for audit.
	 * using for setting audit fields in relations oneToMany
	 * @return the list for audit
	 */
	public Map<String,List<? extends Auditable>> getListForAudit(); 
}
