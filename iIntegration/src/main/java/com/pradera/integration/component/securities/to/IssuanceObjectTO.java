package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class IssuanceObjectTO extends ValidationOperationType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String issuanceDescription;
	private Date issuanceDate;
	private Date expirationDate;
	private Integer currency;
	private BigDecimal issuanceAmount;
	private Integer instrumentType;
	private String instrumentTypeDesc;
	private Integer securityType;
	private String securityTypeDesc;
	private Integer securityClass;
	private String securityClassDesc;
	private Date resolutionDate;
	private String resolutionNumber;
	private String issuerMnemonic;
	private String codeCountryLocation; //lugar emision (pais)
	private Integer indPrimaryPlacement;
	private String codeGeographicLocation; // lugar emision (dpto)
	
	public String getIssuanceDescription() {
		return issuanceDescription;
	}
	public void setIssuanceDescription(String issuanceDescription) {
		this.issuanceDescription = issuanceDescription;
	}
	public Date getIssuanceDate() {
		return issuanceDate;
	}
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public BigDecimal getIssuanceAmount() {
		return issuanceAmount;
	}
	public void setIssuanceAmount(BigDecimal issuanceAmount) {
		this.issuanceAmount = issuanceAmount;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public Integer getSecurityType() {
		return securityType;
	}
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Date getResolutionDate() {
		return resolutionDate;
	}
	public void setResolutionDate(Date resolutionDate) {
		this.resolutionDate = resolutionDate;
	}
	public String getResolutionNumber() {
		return resolutionNumber;
	}
	public void setResolutionNumber(String resolutionNumber) {
		this.resolutionNumber = resolutionNumber;
	}
	
	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}
	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}
	public Integer getIndPrimaryPlacement() {
		return indPrimaryPlacement;
	}
	public void setIndPrimaryPlacement(Integer indPrimaryPlacement) {
		this.indPrimaryPlacement = indPrimaryPlacement;
	}
	public String getCodeGeographicLocation() {
		return codeGeographicLocation;
	}
	public void setCodeGeographicLocation(String codeGeographicLocation) {
		this.codeGeographicLocation = codeGeographicLocation;
	}
	public String getCodeCountryLocation() {
		return codeCountryLocation;
	}
	public void setCodeCountryLocation(String codeCountryLocation) {
		this.codeCountryLocation = codeCountryLocation;
	}
	public String getInstrumentTypeDesc() {
		return instrumentTypeDesc;
	}
	public void setInstrumentTypeDesc(String instrumentTypeDesc) {
		this.instrumentTypeDesc = instrumentTypeDesc;
	}
	public String getSecurityTypeDesc() {
		return securityTypeDesc;
	}
	public void setSecurityTypeDesc(String securityTypeDesc) {
		this.securityTypeDesc = securityTypeDesc;
	}
	public String getSecurityClassDesc() {
		return securityClassDesc;
	}
	public void setSecurityClassDesc(String securityClassDesc) {
		this.securityClassDesc = securityClassDesc;
	}
	


}
