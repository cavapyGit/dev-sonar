package com.pradera.integration.component.settlements.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class SecuritiesTransferRegisterTO extends OperationInterfaceTO implements Serializable  {

	private static final long serialVersionUID = 1L;

	private String transferType;
	private SecurityObjectTO securityObjectTO;
	private HolderAccountObjectTO holderAccountObjectTO;
	
	
	public SecuritiesTransferRegisterTO() {
		super();
	}

	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}

	public HolderAccountObjectTO getHolderAccountObjectTO() {
		return holderAccountObjectTO;
	}

	public void setHolderAccountObjectTO(HolderAccountObjectTO holderAccountObjectTO) {
		this.holderAccountObjectTO = holderAccountObjectTO;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	
	
}
