package com.pradera.integration.component.custody.service;

import java.util.List;

import javax.ejb.Remote;

import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.to.QueryParametersConsultCdpf;
import com.pradera.integration.component.custody.to.AccountBalanceQueryTO;
import com.pradera.integration.component.custody.to.AccountEntryQueryTO;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.AuthorizedSignatureQueryTO;

import com.pradera.integration.component.custody.to.PhysicalCertificateQueryTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.custody.to.SecuritiesRenewalQueryTO;
import com.pradera.integration.component.custody.to.SecurityEntryQueryTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentQueryTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ExecutorComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
@Remote
public interface CustodyRemoteService {
	
	
	/**
	 * Creates the account entry operation.
	 *
	 * @param accountEntryRegisterTO the account entry register to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object createAccountEntryOperation(AccountEntryRegisterTO accountEntryRegisterTO) throws ServiceException;
	
	/**
	 * Creates the account entry operation tx.
	 *
	 * @param loggerUser the logger user
	 * @param accountEntryRegisterTO the account entry register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType createAccountEntryOperationTx(LoggerUser loggerUser, AccountEntryRegisterTO accountEntryRegisterTO) throws ServiceException;
		
	/**
	 * Creates the early payment operation.
	 *
	 * @param stockEarlyPaymentRegisterTO the stock early payment register to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object createEarlyPaymentOperation(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException;
	
	/**
	 * Creates the early payment operation tx.
	 *
	 * @param loggerUser the logger user
	 * @param stockEarlyPaymentRegisterTO the stock early payment register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType createEarlyPaymentOperationTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException;

	/**
	 * Renew securities operation tx.
	 *
	 * @param loggerUser the logger user
	 * @param stockEarlyPaymentRegisterTO the stock early payment register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType renewSecuritiesOperationTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException;
	
	/**
	 * Reverse account entry operation tx.
	 *
	 * @param loggerUser the logger user
	 * @param reverseRegisterTO the reverse register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType reverseAccountEntryOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException;
		
	/**
	 * Reverse early payment operation tx.
	 *
	 * @param loggerUser the logger user
	 * @param reverseRegisterTO the reverse register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType reverseEarlyPaymentOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException;
	
	/**
	 * issue 1239: Metodo para realizar reversion de una Cancelacion Antificapada. No Aplica a DPFs
	 * @param loggerUser
	 * @param reverseRegisterTO
	 * @return
	 * @throws ServiceException
	 */
	public RecordValidationType revreverseEarlyPaymentNoDpfTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException;
	
	/**
	 * Reverse renew securities operation tx.
	 *
	 * @param loggerUser the logger user
	 * @param reverseRegisterTO the reverse register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType reverseRenewSecuritiesOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException;
	
	/**
	 * Gets the list dpf holder account balance.
	 *
	 * @param accountBalanceQueryTO the account balance query to
	 * @return the list dpf holder account balance
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getListDpfHolderAccountBalance(AccountBalanceQueryTO accountBalanceQueryTO) throws ServiceException;
	
	/**
	 * Gets the list dpf account entry operation.
	 *
	 * @param accountEntryQueryTO the account entry query to
	 * @return the list dpf account entry operation
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getListDpfAccountEntryOperation(AccountEntryQueryTO accountEntryQueryTO) throws ServiceException;
	
	/**
	 * Gets the list dpf securities renewal operation.
	 *
	 * @param securitiesRenewalQueryTO the securities renewal query to
	 * @return the list dpf securities renewal operation
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getListDpfSecuritiesRenewalOperation(SecuritiesRenewalQueryTO securitiesRenewalQueryTO) throws ServiceException;
	
	/**
	 * Gets the list dpf early payment operation.
	 *
	 * @param stockEarlyPaymentQueryTO the stock early payment query to
	 * @return the list dpf early payment operation
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getListDpfEarlyPaymentOperation(StockEarlyPaymentQueryTO stockEarlyPaymentQueryTO) throws ServiceException;
	
	/**
	 * Creates the early payment operation bcb tgn tx.
	 *
	 * @param loggerUser the logger user
	 * @param stockEarlyPaymentRegisterTO the stock early payment register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType createEarlyPaymentOperationBcbTgnTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException;
	
	/**
	 * issue 1239: replica del metodo createEarlyPaymentOperationBcbTgnTx, se realizan modificacion de acuerdo a requermientos del issue
	 * @param loggerUser
	 * @param stockEarlyPaymentRegisterTO
	 * @return
	 * @throws ServiceException
	 */
	public RecordValidationType createEarlyPaymentNoDPFOperationTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException;
	
	/**
	 * Gets the list bbx btx securities query operation.
	 *
	 * @param securityEntryQueryTO the security entry query to
	 * @return the list bbx btx securities query operation
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getListBbxBtxSecuritiesQueryOperation(SecurityEntryQueryTO securityEntryQueryTO) throws ServiceException;
	
	/**
	 * Gets the list registered dpf.
	 *
	 * @param queryParametersConsultCdpf the query parameters consult cdpf
	 * @return the list registered dpf
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getListRegisteredDpf(QueryParametersConsultCdpf queryParametersConsultCdpf) throws ServiceException;
	
	/**
	 * Gets the list Confirmed Physical Certificates
	 * @param physicalCertificateQueryTO
	 * @return
	 * @throws ServiceException
	 */
	public List<RecordValidationType> getListPhysicalCertificate(PhysicalCertificateQueryTO physicalCertificateQueryTO)throws ServiceException;
	
	
	
	/***
	 * 
	 * @param authorizedSignatureQueryTO
	 * @return
	 * @throws ServiceException
	 */
	public List<RecordValidationType> getAuthorizedSignatureList(AuthorizedSignatureQueryTO authorizedSignatureQueryTO) throws ServiceException;

	/**
	 * Gets List Institutions
	 * @return
	 * @throws ServiceException
	 */
	public List<RecordValidationType> getInstitutionList() throws ServiceException;	
		

	
	/**
	 * Creates the account entry  new structure operation tx.
	 *
	 * @param loggerUser the logger user
	 * @param accountEntryRegisterTO the account entry register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType createAccountEntryNewOperationTx(LoggerUser loggerUser,AccountEntryRegisterTO accountEntryRegisterTO) throws ServiceException;
	
	/**
	 * Reverse registration placement operation tx.
	 * 
	 * @param loggerUser the logger user
	 * @param reverseRegisterTO the reverse register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType reverseRegistrationPlacementOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException;
}
