package com.pradera.integration.component.business.to;

import java.math.BigDecimal;
import java.util.Date;

public class PrimaryPlacementComponentTO {
	
	private String idSecurityCode;
	
	private Long idPlacementParticipant;
	
	private BigDecimal stockQuantity;
	
	private Date updateDate;
	
	private long mode;
	
	private boolean validateOpenPlacement = true;

	public long getMode() {
		return mode;
	}

	public void setMode(long mode) {
		this.mode = mode;
	}

	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public boolean isValidateOpenPlacement() {
		return validateOpenPlacement;
	}

	public void setValidateOpenPlacement(boolean validateOpenPlacement) {
		this.validateOpenPlacement = validateOpenPlacement;
	}

	public Long getIdPlacementParticipant() {
		return idPlacementParticipant;
	}

	public void setIdPlacementParticipant(Long idPlacementParticipant) {
		this.idPlacementParticipant = idPlacementParticipant;
	}

	//public BigDecimal getCurrentNominalValue() {
	//	return currentNominalValue;
	//}

	//public void setCurrentNominalValue(BigDecimal currentNominalValue) {
	//	this.currentNominalValue = currentNominalValue;
	//}

	
	
	
	
}
