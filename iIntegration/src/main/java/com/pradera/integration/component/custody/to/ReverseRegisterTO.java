package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class ReverseRegisterTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long reverseOperationNumber;
	private Long idHolder;
	private String motive;
	private Integer accountNumber;
	private SecurityObjectTO securityObjectTO;
	private HolderAccountObjectTO holderAccountObjectTO;
	
	
	public ReverseRegisterTO() {
		super();
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}



	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}



	/**
	 * @return the reverseOperationNumber
	 */
	public Long getReverseOperationNumber() {
		return reverseOperationNumber;
	}

	/**
	 * @param reverseOperationNumber the reverseOperationNumber to set
	 */
	public void setReverseOperationNumber(Long reverseOperationNumber) {
		this.reverseOperationNumber = reverseOperationNumber;
	}

	public Long getIdHolder() {
		return idHolder;
	}

	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}

	/**
	 * @return the holderAccountObjectTO
	 */
	public HolderAccountObjectTO getHolderAccountObjectTO() {
		return holderAccountObjectTO;
	}

	/**
	 * @param holderAccountObjectTO the holderAccountObjectTO to set
	 */
	public void setHolderAccountObjectTO(HolderAccountObjectTO holderAccountObjectTO) {
		this.holderAccountObjectTO = holderAccountObjectTO;
	}
	
}
