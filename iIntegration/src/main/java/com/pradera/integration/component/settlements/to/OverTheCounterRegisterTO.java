package com.pradera.integration.component.settlements.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class OverTheCounterRegisterTO extends OperationInterfaceTO implements Serializable  {

	private static final long serialVersionUID = 1L;

	private SecurityObjectTO securityObjectTO;
	private SecuritiesTransferObjectTO securitiesTransferObjectTO;
	private String securityCode;
	
	
	public OverTheCounterRegisterTO() {
		super();
	}

	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}

	public SecuritiesTransferObjectTO getSecuritiesTransferObjectTO() {
		return securitiesTransferObjectTO;
	}

	public void setSecuritiesTransferObjectTO(
			SecuritiesTransferObjectTO securitiesTransferObjectTO) {
		this.securitiesTransferObjectTO = securitiesTransferObjectTO;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
}
