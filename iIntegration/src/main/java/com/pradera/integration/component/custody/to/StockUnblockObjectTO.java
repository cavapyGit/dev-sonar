package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class StockUnblockObjectTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String affectationCode;
	private String referenceDocument;
	private String observations;
	private LegalInformationObjetTO legalInformationObjetTO;
	
	/**
	 * @return the affectationCode
	 */
	public String getAffectationCode() {
		return affectationCode;
	}
	/**
	 * @param affectationCode the affectationCode to set
	 */
	public void setAffectationCode(String affectationCode) {
		this.affectationCode = affectationCode;
	}
	/**
	 * @return the referenceDocument
	 */
	public String getReferenceDocument() {
		return referenceDocument;
	}
	/**
	 * @param referenceDocument the referenceDocument to set
	 */
	public void setReferenceDocument(String referenceDocument) {
		this.referenceDocument = referenceDocument;
	}
	/**
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}
	/**
	 * @param observations the observations to set
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}
	/**
	 * @return the legalInformationObjetTO
	 */
	public LegalInformationObjetTO getLegalInformationObjetTO() {
		return legalInformationObjetTO;
	}
	/**
	 * @param legalInformationObjetTO the legalInformationObjetTO to set
	 */
	public void setLegalInformationObjetTO(
			LegalInformationObjetTO legalInformationObjetTO) {
		this.legalInformationObjetTO = legalInformationObjetTO;
	}
	
}
