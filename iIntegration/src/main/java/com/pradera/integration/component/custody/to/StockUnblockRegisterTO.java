package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class StockUnblockRegisterTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer accountNumber;
	private String documentNumber;
	private SecurityObjectTO securityObjectTO;
	private StockUnblockObjectTO stockUnblockObjectTO;
	
	/**
	 * @return the accountNumber
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the documentNumber
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	/**
	 * @param documentNumber the documentNumber to set
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	/**
	 * @return the securityObjectTO
	 */
	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}
	/**
	 * @param securityObjectTO the securityObjectTO to set
	 */
	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}
	/**
	 * @return the stockUnblockObjectTO
	 */
	public StockUnblockObjectTO getStockUnblockObjectTO() {
		return stockUnblockObjectTO;
	}
	/**
	 * @param stockUnblockObjectTO the stockUnblockObjectTO to set
	 */
	public void setStockUnblockObjectTO(StockUnblockObjectTO stockUnblockObjectTO) {
		this.stockUnblockObjectTO = stockUnblockObjectTO;
	}
	
}
