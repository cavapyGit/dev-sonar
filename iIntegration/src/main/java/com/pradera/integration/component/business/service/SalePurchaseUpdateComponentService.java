package com.pradera.integration.component.business.service;

import javax.ejb.Local;

import com.pradera.integration.component.business.to.AccountOperationTO;
import com.pradera.integration.exception.ServiceException;

@Local
public interface SalePurchaseUpdateComponentService {
	
	public void blockSalePurchaseBalances(AccountOperationTO objAccountOperationTO, Long idBusinessProcess,Integer indMarketFact) throws ServiceException;
	
}
