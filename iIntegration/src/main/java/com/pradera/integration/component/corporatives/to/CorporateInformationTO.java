package com.pradera.integration.component.corporatives.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class CorporateInformationTO extends OperationInterfaceTO implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	private SecurityObjectTO securityObjectTO;
	private String errorCode;
	private String errorDescription;

	
	public CorporateInformationTO() {
		super();
	}

	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	
	public CorporateInformationTO clone() throws CloneNotSupportedException {
        return (CorporateInformationTO) super.clone();
    }
}
