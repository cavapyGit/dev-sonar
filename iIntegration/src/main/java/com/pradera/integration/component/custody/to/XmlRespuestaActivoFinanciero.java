package com.pradera.integration.component.custody.to;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * La Paz - Bolivia
 *  - JBPM
 * 17/09/2020 - 10:44 AM
 * Created by: equinajo
 */
@XmlRootElement(name = "RESP_SOLICITUD")
public class XmlRespuestaActivoFinanciero implements Serializable {

    private static final long serialVersionUID = 1L;

    private String codSolicitud;
    private String codResp;
    private String descResp;

    public XmlRespuestaActivoFinanciero(){
        super();
    }
    
    @XmlElement(name = "COD_SOLICITUD")
    public String getCodSolicitud() {
		return codSolicitud;
	}

	public void setCodSolicitud(String codSolicitud) {
		this.codSolicitud = codSolicitud;
	}

	@XmlElement(name = "COD_RESP")
	public String getCodResp() {
		return codResp;
	}

	public void setCodResp(String codResp) {
		this.codResp = codResp;
	}

	@XmlElement(name = "DESC_RESP")
	public String getDescResp() {
		return descResp;
	}

	public void setDescResp(String descResp) {
		this.descResp = descResp;
	}

	@Override
	public String toString() {
		return "XmlResponseActivoFinanciero [codSolicitud=" + codSolicitud
				+ ", codResp=" + codResp + ", descResp=" + descResp + "]";
	}

}
