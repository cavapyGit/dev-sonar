package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class FundsComponentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
public class FundsComponentTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id business process. */
	private Long idBusinessProcess; //executed ID business process 
	
	/** The id funds operation type. */
	private Long idFundsOperationType; //ID funds operation type
	
	/** The id funds operation. */
	private Long idFundsOperation; //ID funds operation
	
	/** The id institution cash account. */
	private Long idInstitutionCashAccount; //Institution cash account
	
	/** The cash amount. */
	private BigDecimal cashAmount; //cash amount for the movement
	
	/** The id guarantee operation. */
	private Long idGuaranteeOperation; //funds guarantee
	
	/** The id operation state. */
	private Integer operationState;
	
	/**
	 * Instantiates a new funds component to.
	 */
	public FundsComponentTO() {
		super();
	}

	/**
	 * Gets the id business process.
	 *
	 * @return the id business process
	 */
	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}

	/**
	 * Sets the id business process.
	 *
	 * @param idBusinessProcess the new id business process
	 */
	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}

	/**
	 * Gets the id funds operation type.
	 *
	 * @return the id funds operation type
	 */
	public Long getIdFundsOperationType() {
		return idFundsOperationType;
	}

	/**
	 * Sets the id funds operation type.
	 *
	 * @param idFundsOperationType the new id funds operation type
	 */
	public void setIdFundsOperationType(Long idFundsOperationType) {
		this.idFundsOperationType = idFundsOperationType;
	}

	/**
	 * Gets the id funds operation.
	 *
	 * @return the id funds operation
	 */
	public Long getIdFundsOperation() {
		return idFundsOperation;
	}

	/**
	 * Sets the id funds operation.
	 *
	 * @param idFundsOperation the new id funds operation
	 */
	public void setIdFundsOperation(Long idFundsOperation) {
		this.idFundsOperation = idFundsOperation;
	}

	/**
	 * Gets the id institution cash account.
	 *
	 * @return the id institution cash account
	 */
	public Long getIdInstitutionCashAccount() {
		return idInstitutionCashAccount;
	}

	/**
	 * Sets the id institution cash account.
	 *
	 * @param idInstitutionCashAccount the new id institution cash account
	 */
	public void setIdInstitutionCashAccount(Long idInstitutionCashAccount) {
		this.idInstitutionCashAccount = idInstitutionCashAccount;
	}

	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	/**
	 * Gets the id guarantee operation.
	 *
	 * @return the id guarantee operation
	 */
	public Long getIdGuaranteeOperation() {
		return idGuaranteeOperation;
	}

	/**
	 * Sets the id guarantee operation.
	 *
	 * @param idGuaranteeOperation the new id guarantee operation
	 */
	public void setIdGuaranteeOperation(Long idGuaranteeOperation) {
		this.idGuaranteeOperation = idGuaranteeOperation;
	}
	
	
	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}
	
}
