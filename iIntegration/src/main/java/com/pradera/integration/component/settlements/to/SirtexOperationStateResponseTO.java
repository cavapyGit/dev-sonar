package com.pradera.integration.component.settlements.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SirtexOperationStateResponseTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer queryNumber;
	private String participant;
	private Date operationDate;
	private String security;
	private Integer quantity;
	private String code;
	private String descCode;
		
	private Long idInterfaceProcess;
	
	public SirtexOperationStateResponseTO() {
		super();
	}

	public Integer getQueryNumber() {
		return queryNumber;
	}

	public void setQueryNumber(Integer queryNumber) {
		this.queryNumber = queryNumber;
	}

	public String getParticipant() {
		return participant;
	}

	public void setParticipant(String participant) {
		this.participant = participant;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescCode() {
		return descCode;
	}

	public void setDescCode(String descCode) {
		this.descCode = descCode;
	}

	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}

	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}

	
	
	
}
