package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.component.business.to.OperationQueryTO;

/**
 * 
 * Filtros para la busqueda de los titulos fisicos
 * para el flujo de bovedilla
 * Issue 1109
 * 
 * @author jquino
 *
 */
public class PhysicalCertificateQueryTO extends OperationQueryTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The participant pk. */
	private String participantMnemonic;
	
	/** The issuer pk. */
	private String issuerMnemonic;
	
	/** The holder pk. */
	private Long idHolderPk;
	
	/** The security code pk. */
	private String idSecurityCodePk;
	
	/** The request number. */
	private Long operationNumberCertificate;
	
	/** The certificate number. */
	private Long certificateNumber;
	
	/** The security Type */
	private String tipo;
	
	

	public PhysicalCertificateQueryTO() {
		super();
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getParticipantMnemonic() {
		return participantMnemonic;
	}

	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}

	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}

	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	public Long getOperationNumberCertificate() {
		return operationNumberCertificate;
	}

	public void setOperationNumberCertificate(Long operationNumberCertificate) {
		this.operationNumberCertificate = operationNumberCertificate;
	}

	public Long getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
