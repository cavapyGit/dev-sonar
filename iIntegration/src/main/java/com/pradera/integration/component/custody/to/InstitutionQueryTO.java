package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

/**
 * Datos de Ingreso de la Consulta
 * @author jquino
 *
 */
public class InstitutionQueryTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	public InstitutionQueryTO() {
		super();
 	}

}
