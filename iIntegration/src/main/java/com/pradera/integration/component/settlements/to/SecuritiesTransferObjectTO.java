package com.pradera.integration.component.settlements.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.component.accounts.to.HolderAccountObjectSourceTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.securities.to.SecurityObjectRteTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class SecuritiesTransferObjectTO extends ValidationOperationType implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long idSourceHolder;
	private Long idTargetHolder;
	private HolderAccountObjectSourceTO sourceHolderAccount;
	private HolderAccountObjectTO targetHolderAccount;
	private SecurityObjectRteTO securityObjectRteTO;
	
	
	public SecuritiesTransferObjectTO() {
		super();
	}

	public Long getIdSourceHolder() {
		return idSourceHolder;
	}

	public void setIdSourceHolder(Long idSourceHolder) {
		this.idSourceHolder = idSourceHolder;
	}

	public Long getIdTargetHolder() {
		return idTargetHolder;
	}

	public void setIdTargetHolder(Long idTargetHolder) {
		this.idTargetHolder = idTargetHolder;
	}

	public HolderAccountObjectTO getTargetHolderAccount() {
		return targetHolderAccount;
	}

	public void setTargetHolderAccount(HolderAccountObjectTO targetHolderAccount) {
		this.targetHolderAccount = targetHolderAccount;
	}

	public HolderAccountObjectSourceTO getSourceHolderAccount() {
		return sourceHolderAccount;
	}

	public void setSourceHolderAccount(
			HolderAccountObjectSourceTO sourceHolderAccount) {
		this.sourceHolderAccount = sourceHolderAccount;
	}

	public SecurityObjectRteTO getSecurityObjectRteTO() {
		return securityObjectRteTO;
	}

	public void setSecurityObjectRteTO(SecurityObjectRteTO securityObjectRteTO) {
		this.securityObjectRteTO = securityObjectRteTO;
	}
	
	
	
	
}
