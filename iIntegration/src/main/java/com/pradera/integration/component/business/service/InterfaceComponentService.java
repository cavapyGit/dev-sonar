package com.pradera.integration.component.business.service;

import java.util.List;

import javax.ejb.Local;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.generalparameters.to.ServiceStatusQueryTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface InterfaceComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Local
public interface InterfaceComponentService {
	
	/**
	 * Save external interface reception tx.
	 *
	 * @param processFileTO the process file to
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @return the long
	 */
	public Long saveExternalInterfaceReceptionTx(ProcessFileTO processFileTO,  Integer processType, LoggerUser loggerUser);
	
	/**
	 * Save external interface response file tx.
	 *
	 * @param idInterfaceReception the id interface reception
	 * @param lstOperations the lst operations
	 * @param mappingResponse the mapping response
	 * @param rootTag the root tag
	 * @param recordTag the record tag
	 * @param processState the process state
	 * @param loggerUser the logger user
	 * @return the string
	 */
	public String saveExternalInterfaceResponseFileTx(Long idInterfaceReception, List<RecordValidationType> lstOperations, 
						String mappingResponse, String rootTag, String recordTag, Integer processState, LoggerUser loggerUser);
	
	/**
	 * Save external interface upload file tx.
	 *
	 * @param idInterfaceProcess the id interface process
	 * @param lstOperations the lst operations
	 * @param mappingResponse the mapping response
	 * @param rootTag the root tag
	 * @param recordTag the record tag
	 * @param processState the process state
	 * @param loggerUser the logger user
	 * @return the string
	 */
	public String saveExternalInterfaceUploadFileTx(Long idInterfaceProcess, List<RecordValidationType> lstOperations, 
						String mappingResponse, String rootTag, String recordTag, Integer processState, LoggerUser loggerUser);

	/**
	 * Update external interface state.
	 *
	 * @param idInterfaceReception the id interface reception
	 * @param processState the process state
	 * @param xmlFile the xml file
	 * @param loggerUser the logger user
	 */
	public void updateExternalInterfaceState(Long idInterfaceReception, Integer processState, byte[] xmlFile, LoggerUser loggerUser);
	
	/**
	 * Update external interface state tx.
	 *
	 * @param idInterfaceReception the id interface reception
	 * @param processState the process state
	 */
	public void updateExternalInterfaceStateTx(Long idInterfaceReception, Integer processState);
	
	/**
	 * Save interface transaction.
	 *
	 * @param operationInterfaceTO the operation interface to
	 * @param transactionState the transaction state
	 * @param loggerUser the logger user
	 * @return the long
	 */
	public Long saveInterfaceTransaction(OperationInterfaceTO operationInterfaceTO, Integer transactionState, LoggerUser loggerUser);
	
	/**
	 * Save interface transaction tx.
	 *
	 * @param operationInterfaceTO the operation interface to
	 * @param transactionState the transaction state
	 * @param loggerUser the logger user
	 * @return the long
	 */
	public Long saveInterfaceTransactionTx(OperationInterfaceTO operationInterfaceTO, Integer transactionState, LoggerUser loggerUser);
	
	/**
	 * Update interface process response file.
	 *
	 * @param idInterfaceProcess the id interface process
	 * @param processState the process state
	 * @param xmlFile the xml file
	 * @param loggerUser the logger user
	 */
	public void updateInterfaceProcessResponseFile(Long idInterfaceProcess, Integer processState, byte[] xmlFile, LoggerUser loggerUser);
	
	/**
	 * Update interface process upload file.
	 * @param idInterfaceProcess
	 * @param processState
	 * @param xmlFile
	 * @param loggerUser
	 */
	public void updateInterfaceProcessUploadFile(Long idInterfaceProcess, Integer processState, byte[] xmlFile, LoggerUser loggerUser);
	
	/**
	 * Gets the service status.
	 *
	 * @param objServiceStatusQueryTO the obj service status query to
	 * @param loggerUser the logger user
	 * @return the service status
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getServiceStatus(ServiceStatusQueryTO objServiceStatusQueryTO, LoggerUser loggerUser) throws ServiceException ;
	
	/**
	 * Update funds interface transaction.
	 *
	 * @param idInterfaceTransaction the id interface transaction
	 * @param idFundsOperation the id funds operation
	 */
	public void updateFundsInterfaceTransaction(Long idInterfaceTransaction, Long idFundsOperation);
	
	/**
	 * Save external interface response file tx.
	 *
	 * @param lstOperations the lst operations
	 * @param mappingResponse the mapping response
	 * @param rootTag the root tag
	 * @param recordTag the record tag
	 * @return the string
	 */
	public String saveExternalInterfaceResponseFileTx(List<RecordValidationType> lstOperations, String mappingResponse, String rootTag, String recordTag);
	
}
