package com.pradera.integration.component.accounts.to;

import java.io.Serializable;

public class LegalRepresentativeObjectTO extends PersonObjectTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idLegalRepresentative;
	
	private Integer personType;
	
	private Integer representativeClass;
	
	public LegalRepresentativeObjectTO(){
		super();
	}
	
	public LegalRepresentativeObjectTO(PersonObjectTO personObjectTO){
		super(personObjectTO);
	}
	
	public Long getIdLegalRepresentative() {
		return idLegalRepresentative;
	}
	public void setIdLegalRepresentative(Long idLegalRepresentative) {
		this.idLegalRepresentative = idLegalRepresentative;
	}

	public Integer getPersonType() {
		return personType;
	}

	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	public Integer getRepresentativeClass() {
		return representativeClass;
	}

	public void setRepresentativeClass(Integer representativeClass) {
		this.representativeClass = representativeClass;
	}
	
}
