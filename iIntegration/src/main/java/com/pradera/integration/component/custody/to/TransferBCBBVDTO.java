package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class TransferBCBBVDTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String	numeroRegistro;	//NROREGISTRO
	private String  claveValor;		//CLAVE_VALOR
	private String 	cedulaVendedor;	//CEDULA_VENDEDOR
	private String 	nombreVendedor;	//NOMBRE_VENDEDOR
	private String 	cedulaComprador;//CEDULA_COMPRADOR
	private String 	nombreComprador;//NOMBRE_COMPRADOR
	private String 	cantidad;		//CANTIDAD
	private String 	fecha;			//FECHA
	
	/**
	 * @return the numeroRegistro
	 */
	public String getNumeroRegistro() {
		return numeroRegistro;
	}
	/**
	 * @param numeroRegistro the numeroRegistro to set
	 */
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	/**
	 * @return the claveValor
	 */
	public String getClaveValor() {
		return claveValor;
	}
	/**
	 * @param claveValor the claveValor to set
	 */
	public void setClaveValor(String claveValor) {
		this.claveValor = claveValor;
	}
	/**
	 * @return the cedulaVendedor
	 */
	public String getCedulaVendedor() {
		return cedulaVendedor;
	}
	/**
	 * @param cedulaVendedor the cedulaVendedor to set
	 */
	public void setCedulaVendedor(String cedulaVendedor) {
		this.cedulaVendedor = cedulaVendedor;
	}
	/**
	 * @return the nombreVendedor
	 */
	public String getNombreVendedor() {
		return nombreVendedor;
	}
	/**
	 * @param nombreVendedor the nombreVendedor to set
	 */
	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}
	/**
	 * @return the cedulaComprador
	 */
	public String getCedulaComprador() {
		return cedulaComprador;
	}
	/**
	 * @param cedulaComprador the cedulaComprador to set
	 */
	public void setCedulaComprador(String cedulaComprador) {
		this.cedulaComprador = cedulaComprador;
	}
	/**
	 * @return the nombreComprador
	 */
	public String getNombreComprador() {
		return nombreComprador;
	}
	/**
	 * @param nombreComprador the nombreComprador to set
	 */
	public void setNombreComprador(String nombreComprador) {
		this.nombreComprador = nombreComprador;
	}
	/**
	 * @return the cantidad
	 */
	public String getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
		
}
