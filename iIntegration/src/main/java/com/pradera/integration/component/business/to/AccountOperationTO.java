
package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountOperationTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idMechanismOperation;
	private Long idHolderAccountOperation;
	private Long idSettlementAccountOperation;
	private Long idSettlementOperation;
	private Integer idRole;
	private Integer operationPart;
	private Long idParticipant;
	private Long idHolderAccount;
	private String idSecurityCode;
	private Long idMechanism;
	private Long idModality;
	private BigDecimal totalStockQuantity;
	private BigDecimal stockQuantity;
	private Integer indInchain;
	private BigDecimal chainedQuantity;
	private Long idSwapOperation;
	private BigDecimal blockQuantity;
	private BigDecimal cashPrice;
	private Integer instrumentType;
	private Integer indReportingBalance;
	
	
	public AccountOperationTO() {
		super();
	}

	public BigDecimal getBlockQuantity() {
		return blockQuantity;
	}

	public void setBlockQuantity(BigDecimal blockQuantity) {
		this.blockQuantity = blockQuantity;
	}

	public Integer getIndInchain() {
		return indInchain;
	}


	public void setIndInchain(Integer indInchain) {
		this.indInchain = indInchain;
	}


	public BigDecimal getChainedQuantity() {
		return chainedQuantity;
	}


	public void setChainedQuantity(BigDecimal chainedQuantity) {
		this.chainedQuantity = chainedQuantity;
	}


	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}


	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}


	public Long getIdHolderAccountOperation() {
		return idHolderAccountOperation;
	}


	public void setIdHolderAccountOperation(Long idHolderAccountOperation) {
		this.idHolderAccountOperation = idHolderAccountOperation;
	}


	public Long getIdParticipant() {
		return idParticipant;
	}


	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}


	public Long getIdHolderAccount() {
		return idHolderAccount;
	}


	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}


	public String getIdSecurityCode() {
		return idSecurityCode;
	}


	public void setIdSecurityCode(String idIsinCode) {
		this.idSecurityCode = idIsinCode;
	}


	public Long getIdMechanism() {
		return idMechanism;
	}


	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}


	public Long getIdModality() {
		return idModality;
	}


	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}


	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}


	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}


	public Integer getIdRole() {
		return idRole;
	}


	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}


	public Long getIdSwapOperation() {
		return idSwapOperation;
	}


	public void setIdSwapOperation(Long idSwapOperation) {
		this.idSwapOperation = idSwapOperation;
	}


	public Integer getOperationPart() {
		return operationPart;
	}


	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public Long getIdSettlementAccountOperation() {
		return idSettlementAccountOperation;
	}

	public void setIdSettlementAccountOperation(Long idSettlementAccountOperation) {
		this.idSettlementAccountOperation = idSettlementAccountOperation;
	}

	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}

	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}

	public BigDecimal getTotalStockQuantity() {
		return totalStockQuantity;
	}

	public void setTotalStockQuantity(BigDecimal totalStockQuantity) {
		this.totalStockQuantity = totalStockQuantity;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	public Integer getIndReportingBalance() {
		return indReportingBalance;
	}

	public void setIndReportingBalance(Integer indReportingBalance) {
		this.indReportingBalance = indReportingBalance;
	}


}
