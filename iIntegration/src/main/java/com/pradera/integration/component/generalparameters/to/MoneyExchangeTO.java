package com.pradera.integration.component.generalparameters.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class MoneyExchangeTO extends OperationInterfaceTO  implements Serializable  {

	private static final long serialVersionUID = 1L;

	private int currencyCode;
	private int errorCode;
	private String strDate;
	private BigDecimal value;
	
	
	public MoneyExchangeTO() {
		super();
	}

	public int getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(int currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getStrDate() {
		return strDate;
	}

	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	
	
	
}
