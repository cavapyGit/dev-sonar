package com.pradera.integration.component.accounts.service;

import java.util.List;

import javax.ejb.Remote;

import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.to.CuiQueryTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ExecutorComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
@Remote
public interface AccountsRemoteService {
	
	/**
	 * Creates the holder remote service.
	 *
	 * @param loggerUser the logger user
	 * @param holderAccountObjectTO the holder account object to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	Object createHolderRemoteService(LoggerUser loggerUser, HolderAccountObjectTO holderAccountObjectTO) throws ServiceException;
	
	/**
	 * Delete holder remote service.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<RecordValidationType> deleteHolderRemoteService(ReverseRegisterTO reverseRegisterTO) throws ServiceException;
	
	/**
	 * Creates the holder account remote service.
	 *
	 * @param loggerUser the logger user
	 * @param holderAccountObjectTO the holder account object to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	Object createHolderAccountRemoteService(LoggerUser loggerUser, HolderAccountObjectTO holderAccountObjectTO) throws ServiceException;

	/**
	 * Creates the holder new remote service.
	 *
	 * @param loggerUser the logger user
	 * @param holderAccountObjectTO the holder account object to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	Object createHolderNewRemoteService(LoggerUser loggerUser, HolderAccountObjectTO holderAccountObjectTO) throws ServiceException;
	
	List<RecordValidationType> getListCuiQuery(CuiQueryTO cuiQueryTO) throws ServiceException;
	
}
