package com.pradera.integration.component.securities.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class GenerationFileDetailCouponTRCRTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String 	nroCupon;//nro_cupon
	private String 	fechaVenci;//fecha_venci
	private String 	amortizaK;//amortiza_k
	private String 	tasaCuponDif;//tasa_cupon_dif
	
	
	public GenerationFileDetailCouponTRCRTO(){
		
	}

	

	/**
	 * @param nroCupon
	 * @param fechaVenci
	 * @param amortizaK
	 * @param tasaCuponDif
	 */
	public GenerationFileDetailCouponTRCRTO(String nroCupon,
			String fechaVenci, String amortizaK, String tasaCuponDif) {
		this.nroCupon = nroCupon;
		this.fechaVenci = fechaVenci;
		this.amortizaK = amortizaK;
		this.tasaCuponDif = tasaCuponDif;
	}
	
	/**
	 * 
	 * @param generationFileSecurityTRCRTO
	 */
	public GenerationFileDetailCouponTRCRTO(GenerationFileSecurityTRCRTO generationFileSecurityTRCRTO) {
		this.nroCupon = generationFileSecurityTRCRTO.getNroCupon();
		this.fechaVenci = generationFileSecurityTRCRTO.getFechaVenci();
		this.amortizaK = generationFileSecurityTRCRTO.getAmortizaK();
		this.tasaCuponDif = generationFileSecurityTRCRTO.getTasaCuponDif();
	}



	/**
	 * @return the nroCupon
	 */
	public String getNroCupon() {
		return nroCupon;
	}


	/**
	 * @param nroCupon the nroCupon to set
	 */
	public void setNroCupon(String nroCupon) {
		this.nroCupon = nroCupon;
	}


	/**
	 * @return the fechaVenci
	 */
	public String getFechaVenci() {
		return fechaVenci;
	}


	/**
	 * @param fechaVenci the fechaVenci to set
	 */
	public void setFechaVenci(String fechaVenci) {
		this.fechaVenci = fechaVenci;
	}


	/**
	 * @return the amortizaK
	 */
	public String getAmortizaK() {
		return amortizaK;
	}


	/**
	 * @param amortizaK the amortizaK to set
	 */
	public void setAmortizaK(String amortizaK) {
		this.amortizaK = amortizaK;
	}


	/**
	 * @return the tasaCuponDif
	 */
	public String getTasaCuponDif() {
		return tasaCuponDif;
	}


	/**
	 * @param tasaCuponDif the tasaCuponDif to set
	 */
	public void setTasaCuponDif(String tasaCuponDif) {
		this.tasaCuponDif = tasaCuponDif;
	}

	
}
