package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class HolderAccountBalanceTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
public class HolderAccountBalanceTO implements Serializable{
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant. */
	private Long idParticipant; 
	
	/** The id holder account. */
	private Long idHolderAccount;
	
	/** The id securities. */
	private String idSecurityCode;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity; //Stock quantity to move in the account balance
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The id guarantee operation. */
	private Long idGuaranteeOperation;
	
	/** The market lock. */
	private Boolean marketLock;
	
	/** The lst market fact accounts. */
	private List<MarketFactAccountTO> lstMarketFactAccounts;
	
	private Object holderAccountBalance;
	
	private boolean normalQuantity; //this is used when it belongs a chain holder operation

	private BigDecimal cashPrice;// si no se ingresa, se tomara el VN por defecto como el precio
	
	private BigDecimal operationPrice;// si no se ingresa, se tomara el VN por defecto como el precio
	
	/**
	 * Instantiates a new holder account balance to.
	 */
	public HolderAccountBalanceTO() {
		super();
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the id holder account.
	 *
	 * @return the id holder account
	 */
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}

	/**
	 * Sets the id holder account.
	 *
	 * @param idHolderAccount the new id holder account
	 */
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}

	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the id isin code.
	 *
	 * @return the id isin code
	 */
	/*public String getIdIsinCode() {
		return idIsinCode;
	}*/

	/**
	 * Sets the id isin code.
	 *
	 * @param idIsinCode the new id isin code
	 */
	/*public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}*/

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the id guarantee operation.
	 *
	 * @return the id guarantee operation
	 */
	public Long getIdGuaranteeOperation() {
		return idGuaranteeOperation;
	}

	/**
	 * Sets the id guarantee operation.
	 *
	 * @param idGuaranteeOperation the new id guarantee operation
	 */
	public void setIdGuaranteeOperation(Long idGuaranteeOperation) {
		this.idGuaranteeOperation = idGuaranteeOperation;
	}

	/**
	 * Gets the lst market fact accounts.
	 *
	 * @return the lst market fact accounts
	 */
	public List<MarketFactAccountTO> getLstMarketFactAccounts() {
		return lstMarketFactAccounts;
	}

	/**
	 * Sets the lst market fact accounts.
	 *
	 * @param lstMarketFactAccounts the new lst market fact accounts
	 */
	public void setLstMarketFactAccounts(
			List<MarketFactAccountTO> lstMarketFactAccounts) {
		this.lstMarketFactAccounts = lstMarketFactAccounts;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	@Override
	public String toString() {
		return "HolderAccountBalanceTO [idParticipant=" + idParticipant
				+ ", idHolderAccount=" + idHolderAccount + ", idSecurityCode="
				+ idSecurityCode + ", stockQuantity=" + stockQuantity
				+ ", normalQuantity=" + normalQuantity + "]";
	}

	
	
	public Boolean getMarketLock() {
		return marketLock;
	}

	public void setMarketLock(Boolean marketLock) {
		this.marketLock = marketLock;
	}

	public Object getHolderAccountBalance() {
		return holderAccountBalance;
	}

	public void setHolderAccountBalance(Object holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	public boolean isNormalQuantity() {
		return normalQuantity;
	}

	public void setNormalQuantity(boolean normalQuantity) {
		this.normalQuantity = normalQuantity;
	}

	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}
	
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}

	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}
	
}