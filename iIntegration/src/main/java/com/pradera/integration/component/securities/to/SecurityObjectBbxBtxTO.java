package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SecurityObjectBbxBtxTO extends ValidationOperationType implements Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer securityClass;
	private String securitySerial;
	private Date issuanceDate;
	private Date expirationDate;
	private String currencyMnemonic;
	private BigDecimal interestRate;
	
	private List<PlacementObjectBbxBtxTO> listPlacementObjectBbxBtxTO;

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getSecuritySerial() {
		return securitySerial;
	}

	public void setSecuritySerial(String securitySerial) {
		this.securitySerial = securitySerial;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCurrencyMnemonic() {
		return currencyMnemonic;
	}

	public void setCurrencyMnemonic(String currencyMnemonic) {
		this.currencyMnemonic = currencyMnemonic;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public List<PlacementObjectBbxBtxTO> getListPlacementObjectBbxBtxTO() {
		return listPlacementObjectBbxBtxTO;
	}

	public void setListPlacementObjectBbxBtxTO(
			List<PlacementObjectBbxBtxTO> listPlacementObjectBbxBtxTO) {
		this.listPlacementObjectBbxBtxTO = listPlacementObjectBbxBtxTO;
	}
	
	
	
}
