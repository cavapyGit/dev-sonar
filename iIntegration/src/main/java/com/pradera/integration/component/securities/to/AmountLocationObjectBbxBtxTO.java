package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class AmountLocationObjectBbxBtxTO extends ValidationOperationType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nroObligationNumber;
	private BigDecimal cantVal;
	private BigDecimal amount;
	private Integer department;
	private Integer province;
	private Integer municipality;
	
	public String getNroObligationNumber() {
		return nroObligationNumber;
	}
	public void setNroObligationNumber(String nroObligationNumber) {
		this.nroObligationNumber = nroObligationNumber;
	}
	public BigDecimal getCantVal() {
		return cantVal;
	}
	public void setCantVal(BigDecimal cantVal) {
		this.cantVal = cantVal;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getDepartment() {
		return department;
	}
	public void setDepartment(Integer department) {
		this.department = department;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getMunicipality() {
		return municipality;
	}
	public void setMunicipality(Integer municipality) {
		this.municipality = municipality;
	}
	
	
	
}
