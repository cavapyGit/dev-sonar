package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class DetailCustodianAgbSafiFundsTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String	cCustodio;						//cCustodio
	private String 	fInforme;						//fInforme
	private String 	nemonicoAgbSafi;				//AGB/SAFI
	private String  fondo;							//Fondo
	private String 	instrumento;					//Instrumento
	private String 	serie;							//Serie
	private String 	codValoracion;					//Cód. Valoración
	private String 	qValoresCustodia;				//qValoresEnCustodia
	private String 	pTasaRendimientoEquivalente;	//pTasaRendimientoEquivalente
	private String 	mPrecioEquivalente;				//mPrecioEquivalente
	private String 	tCustodia;						//tCustodia
	private String 	cReporto;						//cReporto
	
	/**
	 * @return the cCustodio
	 */
	public String getcCustodio() {
		return cCustodio;
	}
	/**
	 * @param cCustodio the cCustodio to set
	 */
	public void setcCustodio(String cCustodio) {
		this.cCustodio = cCustodio;
	}
	/**
	 * @return the fInforme
	 */
	public String getfInforme() {
		return fInforme;
	}
	/**
	 * @param fInforme the fInforme to set
	 */
	public void setfInforme(String fInforme) {
		this.fInforme = fInforme;
	}
	/**
	 * @return the nemonicoAgbSafi
	 */
	public String getNemonicoAgbSafi() {
		return nemonicoAgbSafi;
	}
	/**
	 * @param nemonicoAgbSafi the nemonicoAgbSafi to set
	 */
	public void setNemonicoAgbSafi(String nemonicoAgbSafi) {
		this.nemonicoAgbSafi = nemonicoAgbSafi;
	}
	/**
	 * @return the fondo
	 */
	public String getFondo() {
		return fondo;
	}
	/**
	 * @param fondo the fondo to set
	 */
	public void setFondo(String fondo) {
		this.fondo = fondo;
	}
	/**
	 * @return the instrumento
	 */
	public String getInstrumento() {
		return instrumento;
	}
	/**
	 * @param instrumento the instrumento to set
	 */
	public void setInstrumento(String instrumento) {
		this.instrumento = instrumento;
	}
	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}
	/**
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}
	/**
	 * @return the codValoracion
	 */
	public String getCodValoracion() {
		return codValoracion;
	}
	/**
	 * @param codValoracion the codValoracion to set
	 */
	public void setCodValoracion(String codValoracion) {
		this.codValoracion = codValoracion;
	}
	/**
	 * @return the qValoresCustodia
	 */
	public String getqValoresCustodia() {
		return qValoresCustodia;
	}
	/**
	 * @param qValoresCustodia the qValoresCustodia to set
	 */
	public void setqValoresCustodia(String qValoresCustodia) {
		this.qValoresCustodia = qValoresCustodia;
	}
	/**
	 * @return the pTasaRendimientoEquivalente
	 */
	public String getpTasaRendimientoEquivalente() {
		return pTasaRendimientoEquivalente;
	}
	/**
	 * @param pTasaRendimientoEquivalente the pTasaRendimientoEquivalente to set
	 */
	public void setpTasaRendimientoEquivalente(String pTasaRendimientoEquivalente) {
		this.pTasaRendimientoEquivalente = pTasaRendimientoEquivalente;
	}
	/**
	 * @return the mPrecioEquivalente
	 */
	public String getmPrecioEquivalente() {
		return mPrecioEquivalente;
	}
	/**
	 * @param mPrecioEquivalente the mPrecioEquivalente to set
	 */
	public void setmPrecioEquivalente(String mPrecioEquivalente) {
		this.mPrecioEquivalente = mPrecioEquivalente;
	}
	/**
	 * @return the tCustodia
	 */
	public String gettCustodia() {
		return tCustodia;
	}
	/**
	 * @param tCustodia the tCustodia to set
	 */
	public void settCustodia(String tCustodia) {
		this.tCustodia = tCustodia;
	}
	/**
	 * @return the cReporto
	 */
	public String getcReporto() {
		return cReporto;
	}
	/**
	 * @param cReporto the cReporto to set
	 */
	public void setcReporto(String cReporto) {
		this.cReporto = cReporto;
	}
			
}
