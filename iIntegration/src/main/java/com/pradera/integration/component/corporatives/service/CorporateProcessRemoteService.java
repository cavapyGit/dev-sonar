package com.pradera.integration.component.corporatives.service;

import java.util.List;

import javax.ejb.Remote;

import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.corporatives.to.CorporateInformationTO;
import com.pradera.integration.component.corporatives.to.InterestPaymentQueryTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ExecutorComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
@Remote
public interface CorporateProcessRemoteService {
	
	/**
	 * Pay interest coupon.
	 *
	 * @param corporateInformationTO the corporate information to
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<RecordValidationType> payInterestCoupon(CorporateInformationTO corporateInformationTO) throws Exception;
	
	/**
	 * Reverse payment interest coupon.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> reversePaymentInterestCoupon(ReverseRegisterTO reverseRegisterTO) throws ServiceException;
	
	/**
	 * Gets the list dpf interest payment coupon.
	 *
	 * @param interestPaymentQueryTO the interest payment query to
	 * @return the list dpf interest payment coupon
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> getListDpfInterestPaymentCoupon(InterestPaymentQueryTO interestPaymentQueryTO) throws ServiceException;
	
	/**
	 * Execute expired securities dpf.
	 *
	 * @param loggerUser the logger user
	 * @param isSecurityCodePk the is security code pk
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object executeExpiredSecuritiesDpf(LoggerUser loggerUser, String isSecurityCodePk) throws ServiceException;
}
