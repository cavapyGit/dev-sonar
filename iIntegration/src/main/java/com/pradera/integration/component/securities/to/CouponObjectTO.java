package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class CouponObjectTO extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer couponNumber;
	private Date expirationDate;
	private BigDecimal interestRate;
	
	private Long operationNumber;
	private String securitySerial;
	
	
	public Integer getCouponNumber() {
		return couponNumber;
	}
	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public BigDecimal getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}
	public Long getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	public String getSecuritySerial() {
		return securitySerial;
	}
	public void setSecuritySerial(String securitySerial) {
		this.securitySerial = securitySerial;
	}
	
	
}
