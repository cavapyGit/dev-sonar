package com.pradera.integration.component.funds.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class FundsApiRegisterTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String transferReference;
	
	
	public String getTransferReference() {
		return transferReference;
	}


	public void setTransferReference(String transferReference) {
		this.transferReference = transferReference;
	}


	public FundsApiRegisterTO() {
		super();
	}

	

}
