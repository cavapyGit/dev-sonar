package com.pradera.integration.component.settlements.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SirtexOperationCouponTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer couponNumber;
	private String timeOfLife;
	private Date expirationDate;
	private Integer quantity;
	private Integer quantityAccepted;
	private Long idInterfaceProcess;
	
	public SirtexOperationCouponTO() {
		super();
	}

	public Integer getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getTimeOfLife() {
		return timeOfLife;
	}

	public void setTimeOfLife(String timeOfLife) {
		this.timeOfLife = timeOfLife;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}

	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}

	public Integer getQuantityAccepted() {
		return quantityAccepted;
	}

	public void setQuantityAccepted(Integer quantityAccepted) {
		this.quantityAccepted = quantityAccepted;
	}
	
	
	
}
