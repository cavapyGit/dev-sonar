package com.pradera.integration.component.business.service;

import javax.ejb.Local;

import com.pradera.integration.component.business.to.SplitCouponOperationTO;
import com.pradera.integration.exception.ServiceException;

@Local
public interface SplitCouponComponentService {
	
	public void confirmSplitCouponProcess(SplitCouponOperationTO splitCouponOperationTO) throws ServiceException;
	
}
