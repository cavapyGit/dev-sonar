package com.pradera.integration.component.corporatives.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.component.business.to.OperationQueryTO;
import com.pradera.integration.component.securities.to.CouponObjectTO;

public class InterestPaymentQueryTO extends OperationQueryTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<CouponObjectTO> lstCouponObjectTO;
	
	
	public InterestPaymentQueryTO() {
		super();
	}

	public InterestPaymentQueryTO(OperationQueryTO operationQueryTO) {
		setQueryNumber(operationQueryTO.getQueryNumber());
		setOperationCode(operationQueryTO.getOperationCode());
		setEntityNemonic(operationQueryTO.getEntityNemonic());
		setOperationDate(operationQueryTO.getOperationDate());
		setOperationNumber(operationQueryTO.getQueryNumber());
		setSecurityClassDesc(operationQueryTO.getSecurityClassDesc());
		setIdInterfaceProcess(operationQueryTO.getIdInterfaceProcess());
	}
	
	public List<CouponObjectTO> getLstCouponObjectTO() {
		return lstCouponObjectTO;
	}

	public void setLstCouponObjectTO(List<CouponObjectTO> lstCouponObjectTO) {
		this.lstCouponObjectTO = lstCouponObjectTO;
	}

		
}
