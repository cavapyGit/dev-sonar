package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class CouponFeatureTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private String 		cCustodio;
	private String 		fInformacion;
	private String 		cTipoInstrumento;
	private String  	tClaveInstrumento;
	private BigDecimal 	nCupon;
	private String 		fVencimiento;
	private BigDecimal 	mAmortizacionCapital;
	private BigDecimal 	mInteres;
	private BigDecimal 	mTotalCupon;
	/**
	 * @return the cCustodio
	 */
	public String getcCustodio() {
		return cCustodio;
	}
	/**
	 * @param cCustodio the cCustodio to set
	 */
	public void setcCustodio(String cCustodio) {
		this.cCustodio = cCustodio;
	}
	/**
	 * @return the fInformacion
	 */
	public String getfInformacion() {
		return fInformacion;
	}
	/**
	 * @param fInformacion the fInformacion to set
	 */
	public void setfInformacion(String fInformacion) {
		this.fInformacion = fInformacion;
	}
	/**
	 * @return the cTipoInstrumento
	 */
	public String getcTipoInstrumento() {
		return cTipoInstrumento;
	}
	/**
	 * @param cTipoInstrumento the cTipoInstrumento to set
	 */
	public void setcTipoInstrumento(String cTipoInstrumento) {
		this.cTipoInstrumento = cTipoInstrumento;
	}
	/**
	 * @return the tClaveInstrumento
	 */
	public String gettClaveInstrumento() {
		return tClaveInstrumento;
	}
	/**
	 * @param tClaveInstrumento the tClaveInstrumento to set
	 */
	public void settClaveInstrumento(String tClaveInstrumento) {
		this.tClaveInstrumento = tClaveInstrumento;
	}
	/**
	 * @return the nCupon
	 */
	public BigDecimal getnCupon() {
		return nCupon;
	}
	/**
	 * @param nCupon the nCupon to set
	 */
	public void setnCupon(BigDecimal nCupon) {
		this.nCupon = nCupon;
	}
	/**
	 * @return the fVencimiento
	 */
	public String getfVencimiento() {
		return fVencimiento;
	}
	/**
	 * @param fVencimiento the fVencimiento to set
	 */
	public void setfVencimiento(String fVencimiento) {
		this.fVencimiento = fVencimiento;
	}
	/**
	 * @return the mAmortizacionCapital
	 */
	public BigDecimal getmAmortizacionCapital() {
		return mAmortizacionCapital;
	}
	/**
	 * @param mAmortizacionCapital the mAmortizacionCapital to set
	 */
	public void setmAmortizacionCapital(BigDecimal mAmortizacionCapital) {
		this.mAmortizacionCapital = mAmortizacionCapital;
	}
	/**
	 * @return the mInteres
	 */
	public BigDecimal getmInteres() {
		return mInteres;
	}
	/**
	 * @param mInteres the mInteres to set
	 */
	public void setmInteres(BigDecimal mInteres) {
		this.mInteres = mInteres;
	}
	/**
	 * @return the mTotalCupon
	 */
	public BigDecimal getmTotalCupon() {
		return mTotalCupon;
	}
	/**
	 * @param mTotalCupon the mTotalCupon to set
	 */
	public void setmTotalCupon(BigDecimal mTotalCupon) {
		this.mTotalCupon = mTotalCupon;
	}
	
	
	
		
}
