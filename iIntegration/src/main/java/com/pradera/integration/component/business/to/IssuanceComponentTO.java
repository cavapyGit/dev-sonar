package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.component.business.ComponentConstant;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class IssuanceComponentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
public class IssuanceComponentTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id business process. */
	private Long idBusinessProcess; //executed ID business process 
	
	/** The id operation type. */
	private Long idOperationType; //ID operation type
	
	/** The id issuance operation. */
	private Long idIssuanceOperation; //ID issuance operation
	
	/** The id issuance. */
	private String idIssuance; // ID Issuance
	
	/** The cash amount. */
	private BigDecimal cashAmount; // cash amount for the issuance operation
	
	/** The ind source target. */
	private Long indSourceTarget = ComponentConstant.SOURCE; //default value
	
	
	/**
	 * Instantiates a new issuance component to.
	 */
	public IssuanceComponentTO() {
		super();
	}


	/**
	 * Gets the id business process.
	 *
	 * @return the id business process
	 */
	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}


	/**
	 * Sets the id business process.
	 *
	 * @param idBusinessProcess the new id business process
	 */
	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}


	/**
	 * Gets the id operation type.
	 *
	 * @return the id operation type
	 */
	public Long getIdOperationType() {
		return idOperationType;
	}


	/**
	 * Sets the id operation type.
	 *
	 * @param idOperationType the new id operation type
	 */
	public void setIdOperationType(Long idOperationType) {
		this.idOperationType = idOperationType;
	}


	/**
	 * Gets the id issuance operation.
	 *
	 * @return the id issuance operation
	 */
	public Long getIdIssuanceOperation() {
		return idIssuanceOperation;
	}


	/**
	 * Sets the id issuance operation.
	 *
	 * @param idIssuanceOperation the new id issuance operation
	 */
	public void setIdIssuanceOperation(Long idIssuanceOperation) {
		this.idIssuanceOperation = idIssuanceOperation;
	}


	/**
	 * Gets the id issuance.
	 *
	 * @return the id issuance
	 */
	public String getIdIssuance() {
		return idIssuance;
	}


	/**
	 * Sets the id issuance.
	 *
	 * @param idIssuance the new id issuance
	 */
	public void setIdIssuance(String idIssuance) {
		this.idIssuance = idIssuance;
	}


	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}


	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}


	/**
	 * Gets the ind source target.
	 *
	 * @return the ind source target
	 */
	public Long getIndSourceTarget() {
		return indSourceTarget;
	}


	/**
	 * Sets the ind source target.
	 *
	 * @param indSourceTarget the new ind source target
	 */
	public void setIndSourceTarget(Long indSourceTarget) {
		this.indSourceTarget = indSourceTarget;
	}
	
	
	

}
