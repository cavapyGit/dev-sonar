package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class MarketFactAccountTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
public class MarketFactAccountTO  implements Serializable, Cloneable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The market date. */
	private Date marketDate;
	
	/** The quantity. */
	private BigDecimal quantity;
	
	private BigDecimal chainedQuantity;
	
	/** The valuator amount. */
	private BigDecimal valuatorAmount;
	
	/** The valuator amount local currency. */
	private BigDecimal valuatorAmountOther;
	
	/** The ind active. */
	private Integer indActive;
	
	private Object objHolderMarketFactBalance;
	

	/**
	 * Instantiates a new market fact account to.
	 */
	public MarketFactAccountTO() {
		super();
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getValuatorAmount() {
		return valuatorAmount;
	}

	public void setValuatorAmount(BigDecimal valuatorAmount) {
		this.valuatorAmount = valuatorAmount;
	}

	public BigDecimal getValuatorAmountOther() {
		return valuatorAmountOther;
	}

	public void setValuatorAmountOther(BigDecimal valuatorAmountOther) {
		this.valuatorAmountOther = valuatorAmountOther;
	}

	public Integer getIndActive() {
		return indActive;
	}

	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}

	public BigDecimal getChainedQuantity() {
		return chainedQuantity;
	}

	public void setChainedQuantity(BigDecimal chainedQuantity) {
		this.chainedQuantity = chainedQuantity;
	}
	
	public MarketFactAccountTO clone() throws CloneNotSupportedException {
        return (MarketFactAccountTO) super.clone();
    }

	public Object getObjHolderMarketFactBalance() {
		return objHolderMarketFactBalance;
	}

	public void setObjHolderMarketFactBalance(Object objHolderMarketFactBalance) {
		this.objHolderMarketFactBalance = objHolderMarketFactBalance;
	}
}