package com.pradera.integration.component.accounts.to;

import java.io.Serializable;
import java.util.List;

public class NaturalHolderObjectTO extends PersonObjectTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idHolder;
	private Integer indRepresentative;
	private Integer civilStatus;
	private Integer indMinor;
	private Integer indDisabled;
	private String signType;
	private String holderTypeDescription;
	
	//Holder Accounts
	private  List<HolderAccountObjectTO> lstHolderAccountTOs;
	
	private List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjects;
	
	public Long getIdHolder() {
		return idHolder;
	}
	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}
	public Integer getIndRepresentative() {
		return indRepresentative;
	}
	public void setIndRepresentative(Integer indRepresentative) {
		this.indRepresentative = indRepresentative;
	}
	public Integer getCivilStatus() {
		return civilStatus;
	}
	public void setCivilStatus(Integer civilStatus) {
		this.civilStatus = civilStatus;
	}
	public Integer getIndMinor() {
		return indMinor;
	}
	public void setIndMinor(Integer indMinor) {
		this.indMinor = indMinor;
	}
	public Integer getIndDisabled() {
		return indDisabled;
	}
	public void setIndDisabled(Integer indDisabled) {
		this.indDisabled = indDisabled;
	}
	public String getSignType() {
		return signType;
	}
	public void setSignType(String signType) {
		this.signType = signType;
	}
	public List<HolderAccountObjectTO> getLstHolderAccountTOs() {
		return lstHolderAccountTOs;
	}
	public void setLstHolderAccountTOs(
			List<HolderAccountObjectTO> lstHolderAccountTOs) {
		this.lstHolderAccountTOs = lstHolderAccountTOs;
	}
	public String getHolderTypeDescription() {
		return holderTypeDescription;
	}
	public void setHolderTypeDescription(String holderTypeDescription) {
		this.holderTypeDescription = holderTypeDescription;
	}

	public List<LegalRepresentativeObjectTO> getLstLegalRepresentativeObjects() {
		return lstLegalRepresentativeObjects;
	}
	public void setLstLegalRepresentativeObjects(List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjects) {
		this.lstLegalRepresentativeObjects = lstLegalRepresentativeObjects;
	}
}
