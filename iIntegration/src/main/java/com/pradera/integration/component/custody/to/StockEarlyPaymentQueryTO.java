package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.component.business.to.OperationQueryTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class StockEarlyPaymentQueryTO extends OperationQueryTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SecurityObjectTO> lstSecurityObjectTO;

	
	public StockEarlyPaymentQueryTO() {
		super();
	}

	public StockEarlyPaymentQueryTO(OperationQueryTO operationQueryTO) {
		setQueryNumber(operationQueryTO.getQueryNumber());
		setOperationCode(operationQueryTO.getOperationCode());
		setEntityNemonic(operationQueryTO.getEntityNemonic());
		setOperationDate(operationQueryTO.getOperationDate());
		setOperationNumber(operationQueryTO.getQueryNumber());
		setSecurityClassDesc(operationQueryTO.getSecurityClassDesc());
		setIdInterfaceProcess(operationQueryTO.getIdInterfaceProcess());
	}
	
	public List<SecurityObjectTO> getLstSecurityObjectTO() {
		return lstSecurityObjectTO;
	}

	public void setLstSecurityObjectTO(List<SecurityObjectTO> lstSecurityObjectTO) {
		this.lstSecurityObjectTO = lstSecurityObjectTO;
	}
	
	
}
