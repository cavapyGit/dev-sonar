package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.business.to.OperationQueryTO;

public class AccountEntryQueryTO extends OperationQueryTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<HolderAccountObjectTO> lstHolderAccountObjectTO;
	
	
	public AccountEntryQueryTO() {
		super();
	}
	
	public AccountEntryQueryTO(OperationQueryTO operationQueryTO) {
		setQueryNumber(operationQueryTO.getQueryNumber());
		setOperationCode(operationQueryTO.getOperationCode());
		setEntityNemonic(operationQueryTO.getEntityNemonic());
		setOperationDate(operationQueryTO.getOperationDate());
		setOperationNumber(operationQueryTO.getQueryNumber());
		setSecurityClassDesc(operationQueryTO.getSecurityClassDesc());
		setIdInterfaceProcess(operationQueryTO.getIdInterfaceProcess());
	}
	
	
	public List<HolderAccountObjectTO> getLstHolderAccountObjectTO() {
		return lstHolderAccountObjectTO;
	}
	public void setLstHolderAccountObjectTO(
			List<HolderAccountObjectTO> lstHolderAccountObjectTO) {
		this.lstHolderAccountObjectTO = lstHolderAccountObjectTO;
	}
	
	
}
