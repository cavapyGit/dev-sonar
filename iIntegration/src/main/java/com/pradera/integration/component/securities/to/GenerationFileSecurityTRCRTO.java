package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GenerationFileSecurityTRCRTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
public class GenerationFileSecurityTRCRTO extends OperationInterfaceTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant ZERO_STRING. */
	private static final String ZERO_STRING="0";
	
	/** The id security code pk. */
	private String 		idSecurityCodePk;
	
	/** The instrument type. */
	private String 		instrumentType;//codinst
	
	/** The serie. */
	private String 		serie;//serie
	
	/** The emisor. */
	private String 		emisor;//emisor
	
	/** The registry date. */
	private String  	registryDate;//fecha_emision
	
	/** The expiration date. */
	private String 		expirationDate;//fecha_venci
	
	/** The currency. */
	private String 		currency;//moneda
	
	/** The valor emision. */
	private String 		valorEmision;//valor_emision
	
	/** The valor nominal. */
	private String 		valorNominal;//valorNominal
	
	/** The tasa rend. */
	private String 		tasaRend;//tasa_rend
	
	/** The valuacion. */
	private String 		valuacion;//valuacion
	
	/** The nro cupones. */
	private String 		nroCupones;//nro_cupones
	
	/** The plazo amortiza. */
	private String 		plazoAmortiza;//plazo_amortiza
	
	/** The prepago. */
	private String 		prepago;//prepago
	
	/** The subordinado. */
	private String 		subordinado;//subordinado
	
	/** The tipo bono. */
	private String 		tipoBono;//tipo_bono
	
	/** The tipo tasa. */
	private String  	tipoTasa;//tipo_tasa
	
	/** The tasa dif. */
	private String 		tasaDif;//tasa_dif
	
	/** The neg fci. */
	private String 		negFci;//neg_fci
	
	/** The sub producto. */
	private String 		subProducto;//subproducto
	
	/** The tasa pen. */
	private String 		tasaPen;//tasa_pen
	
	/** The list generation file detail coupon. */
	List<GenerationFileDetailCouponTRCRTO> listGenerationFileDetailCoupon;
	
	/** Regularized field. */
	private String 		nroCupon;//nro_cupon
	
	/** The fecha venci. */
	private String 		fechaVenci;//fecha_venci
	
	/** The amortiza k. */
	private String 		amortizaK;//amortiza_k
	
	/** The tasa cupon dif. */
	private String 		tasaCuponDif;//tasa_cupon_dif
		
	/**
	 * Instantiates a new generation file security trcrto.
	 */
	public GenerationFileSecurityTRCRTO(){
		listGenerationFileDetailCoupon= new ArrayList<>();
	}
	
	/**
	 * Instantiates a new generation file security trcrto.
	 *
	 * @param generationFileSecurityTRCRTO the generation file security trcrto
	 */
	public GenerationFileSecurityTRCRTO(GenerationFileSecurityTRCRTO generationFileSecurityTRCRTO){
		
		this.idSecurityCodePk = generationFileSecurityTRCRTO.getIdSecurityCodePk();
		this.instrumentType = generationFileSecurityTRCRTO.getInstrumentType();
		this.serie = generationFileSecurityTRCRTO.getSerie();
		this.emisor = generationFileSecurityTRCRTO.getEmisor();
		this.registryDate = generationFileSecurityTRCRTO.getRegistryDate();
		this.expirationDate = generationFileSecurityTRCRTO.getExpirationDate();
		this.currency = generationFileSecurityTRCRTO.getCurrency();
		this.valorEmision = generationFileSecurityTRCRTO.getValorEmision();
		this.valorNominal = generationFileSecurityTRCRTO.getValorNominal();
		this.tasaRend = generationFileSecurityTRCRTO.getTasaRend();
		this.valuacion = generationFileSecurityTRCRTO.getValuacion();
		this.nroCupones = generationFileSecurityTRCRTO.getNroCupones();
		this.plazoAmortiza = generationFileSecurityTRCRTO.getPlazoAmortiza();
		this.prepago = generationFileSecurityTRCRTO.getPrepago();
		this.subordinado = generationFileSecurityTRCRTO.getSubordinado();
		this.tipoBono = generationFileSecurityTRCRTO.getTipoBono();
		this.tipoTasa = generationFileSecurityTRCRTO.getTipoTasa();
		this.tasaDif = generationFileSecurityTRCRTO.getTasaDif();
		this.negFci = generationFileSecurityTRCRTO.getNegFci();
		this.subProducto = generationFileSecurityTRCRTO.getSubProducto();
		this.tasaPen = generationFileSecurityTRCRTO.getTasaPen();
		
		this.nroCupon = generationFileSecurityTRCRTO.getNroCupon();
		this.fechaVenci = generationFileSecurityTRCRTO.getFechaVenci();
		this.amortizaK = generationFileSecurityTRCRTO.getAmortizaK();
		this.tasaCuponDif = generationFileSecurityTRCRTO.getTasaCuponDif();
		
		listGenerationFileDetailCoupon=new ArrayList<>();
		if(!ZERO_STRING.equals(nroCupones)){
			addNewCouponDetail(generationFileSecurityTRCRTO);
		}
		
	}
	

	/**
	 * Instantiates a new generation file security trcrto.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param instrumentType the instrument type
	 * @param serie the serie
	 * @param emisor the emisor
	 * @param registryDate the registry date
	 * @param expirationDate the expiration date
	 * @param currency the currency
	 * @param valorEmision the valor emision
	 * @param valorNominal the valor nominal
	 * @param tasaRend the tasa rend
	 * @param valuacion the valuacion
	 * @param nroCupones the nro cupones
	 * @param plazoAmortiza the plazo amortiza
	 * @param prepago the prepago
	 * @param subordinado the subordinado
	 * @param tipoBono the tipo bono
	 * @param tipoTasa the tipo tasa
	 * @param tasaDif the tasa dif
	 * @param negFci the neg fci
	 * @param subProducto the sub producto
	 * @param tasaPen the tasa pen
	 * @param nroCupon the nro cupon
	 * @param fechaVenci the fecha venci
	 * @param amortizaK the amortiza k
	 * @param tasaCuponDif the tasa cupon dif
	 */
	public GenerationFileSecurityTRCRTO(String idSecurityCodePk,
			String instrumentType, String serie, String emisor,
			String registryDate, String expirationDate, String currency,
			String valorEmision, String valorNominal,
			String tasaRend, String valuacion, String nroCupones,
			String plazoAmortiza, String prepago, String subordinado,
			String tipoBono, String tipoTasa, String tasaDif, String negFci,
			String subProducto, String tasaPen, String nroCupon,
			String fechaVenci, String amortizaK, String tasaCuponDif) {
		this.idSecurityCodePk = idSecurityCodePk;
		this.instrumentType = instrumentType;
		this.serie = serie;
		this.emisor = emisor;
		this.registryDate = registryDate;
		this.expirationDate = expirationDate;
		this.currency = currency;
		this.valorEmision = valorEmision;
		this.valorNominal = valorNominal;
		this.tasaRend = tasaRend;
		this.valuacion = valuacion;
		this.nroCupones = nroCupones;
		this.plazoAmortiza = plazoAmortiza;
		this.prepago = prepago;
		this.subordinado = subordinado;
		this.tipoBono = tipoBono;
		this.tipoTasa = tipoTasa;
		this.tasaDif = tasaDif;
		this.negFci = negFci;
		this.subProducto = subProducto;
		this.tasaPen = tasaPen;
		this.nroCupon = nroCupon;
		this.fechaVenci = fechaVenci;
		this.amortizaK = amortizaK;
		this.tasaCuponDif = tasaCuponDif;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrumentType
	 */
	public String getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the serie.
	 *
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}

	/**
	 * Sets the serie.
	 *
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}

	/**
	 * Gets the emisor.
	 *
	 * @return the emisor
	 */
	public String getEmisor() {
		return emisor;
	}

	/**
	 * Sets the emisor.
	 *
	 * @param emisor the emisor to set
	 */
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registryDate
	 */
	public String getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expirationDate
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Gets the valor emision.
	 *
	 * @return the valorEmision
	 */
	public String getValorEmision() {
		return valorEmision;
	}

	/**
	 * Sets the valor emision.
	 *
	 * @param valorEmision the valorEmision to set
	 */
	public void setValorEmision(String valorEmision) {
		this.valorEmision = valorEmision;
	}

	/**
	 * Gets the valor nominal.
	 *
	 * @return the valorNominal
	 */
	public String getValorNominal() {
		return valorNominal;
	}

	/**
	 * Sets the valor nominal.
	 *
	 * @param valorNominal the valorNominal to set
	 */
	public void setValorNominal(String valorNominal) {
		this.valorNominal = valorNominal;
	}

	/**
	 * Gets the tasa rend.
	 *
	 * @return the tasaRend
	 */
	public String getTasaRend() {
		return tasaRend;
	}

	/**
	 * Sets the tasa rend.
	 *
	 * @param tasaRend the tasaRend to set
	 */
	public void setTasaRend(String tasaRend) {
		this.tasaRend = tasaRend;
	}

	/**
	 * Gets the valuacion.
	 *
	 * @return the valuacion
	 */
	public String getValuacion() {
		return valuacion;
	}

	/**
	 * Sets the valuacion.
	 *
	 * @param valuacion the valuacion to set
	 */
	public void setValuacion(String valuacion) {
		this.valuacion = valuacion;
	}

	/**
	 * Gets the nro cupones.
	 *
	 * @return the nroCupones
	 */
	public String getNroCupones() {
		return nroCupones;
	}

	/**
	 * Sets the nro cupones.
	 *
	 * @param nroCupones the nroCupones to set
	 */
	public void setNroCupones(String nroCupones) {
		this.nroCupones = nroCupones;
	}

	/**
	 * Gets the plazo amortiza.
	 *
	 * @return the plazoAmortiza
	 */
	public String getPlazoAmortiza() {
		return plazoAmortiza;
	}

	/**
	 * Sets the plazo amortiza.
	 *
	 * @param plazoAmortiza the plazoAmortiza to set
	 */
	public void setPlazoAmortiza(String plazoAmortiza) {
		this.plazoAmortiza = plazoAmortiza;
	}

	/**
	 * Gets the prepago.
	 *
	 * @return the prepago
	 */
	public String getPrepago() {
		return prepago;
	}

	/**
	 * Sets the prepago.
	 *
	 * @param prepago the prepago to set
	 */
	public void setPrepago(String prepago) {
		this.prepago = prepago;
	}

	/**
	 * Gets the subordinado.
	 *
	 * @return the subordinado
	 */
	public String getSubordinado() {
		return subordinado;
	}

	/**
	 * Sets the subordinado.
	 *
	 * @param subordinado the subordinado to set
	 */
	public void setSubordinado(String subordinado) {
		this.subordinado = subordinado;
	}

	/**
	 * Gets the tipo bono.
	 *
	 * @return the tipoBono
	 */
	public String getTipoBono() {
		return tipoBono;
	}

	/**
	 * Sets the tipo bono.
	 *
	 * @param tipoBono the tipoBono to set
	 */
	public void setTipoBono(String tipoBono) {
		this.tipoBono = tipoBono;
	}

	/**
	 * Gets the tipo tasa.
	 *
	 * @return the tipoTasa
	 */
	public String getTipoTasa() {
		return tipoTasa;
	}

	/**
	 * Sets the tipo tasa.
	 *
	 * @param tipoTasa the tipoTasa to set
	 */
	public void setTipoTasa(String tipoTasa) {
		this.tipoTasa = tipoTasa;
	}

	/**
	 * Gets the tasa dif.
	 *
	 * @return the tasaDif
	 */
	public String getTasaDif() {
		return tasaDif;
	}

	/**
	 * Sets the tasa dif.
	 *
	 * @param tasaDif the tasaDif to set
	 */
	public void setTasaDif(String tasaDif) {
		this.tasaDif = tasaDif;
	}

	/**
	 * Gets the neg fci.
	 *
	 * @return the negFci
	 */
	public String getNegFci() {
		return negFci;
	}

	/**
	 * Sets the neg fci.
	 *
	 * @param negFci the negFci to set
	 */
	public void setNegFci(String negFci) {
		this.negFci = negFci;
	}

	/**
	 * Gets the sub producto.
	 *
	 * @return the subProducto
	 */
	public String getSubProducto() {
		return subProducto;
	}

	/**
	 * Sets the sub producto.
	 *
	 * @param subProducto the subProducto to set
	 */
	public void setSubProducto(String subProducto) {
		this.subProducto = subProducto;
	}



	/**
	 * Gets the tasa pen.
	 *
	 * @return the tasaPen
	 */
	public String getTasaPen() {
		return tasaPen;
	}

	/**
	 * Sets the tasa pen.
	 *
	 * @param tasaPen the tasaPen to set
	 */
	public void setTasaPen(String tasaPen) {
		this.tasaPen = tasaPen;
	}

	/**
	 * Gets the list generation file detail coupon.
	 *
	 * @return the listGenerationFileDetailCoupon
	 */
	public List<GenerationFileDetailCouponTRCRTO> getListGenerationFileDetailCoupon() {
		return listGenerationFileDetailCoupon;
	}

	/**
	 * Sets the list generation file detail coupon.
	 *
	 * @param listGenerationFileDetailCoupon the listGenerationFileDetailCoupon to set
	 */
	public void setListGenerationFileDetailCoupon(
			List<GenerationFileDetailCouponTRCRTO> listGenerationFileDetailCoupon) {
		this.listGenerationFileDetailCoupon = listGenerationFileDetailCoupon;
	}

	
	/**
	 * *.
	 *
	 * @return the nro cupon
	 */
	/**
	 * @return the nroCupon
	 */
	public String getNroCupon() {
		return nroCupon;
	}

	/**
	 * Sets the nro cupon.
	 *
	 * @param nroCupon the nroCupon to set
	 */
	public void setNroCupon(String nroCupon) {
		this.nroCupon = nroCupon;
	}

	/**
	 * Gets the fecha venci.
	 *
	 * @return the fechaVenci
	 */
	public String getFechaVenci() {
		return fechaVenci;
	}

	/**
	 * Sets the fecha venci.
	 *
	 * @param fechaVenci the fechaVenci to set
	 */
	public void setFechaVenci(String fechaVenci) {
		this.fechaVenci = fechaVenci;
	}

	/**
	 * Gets the amortiza k.
	 *
	 * @return the amortizaK
	 */
	public String getAmortizaK() {
		return amortizaK;
	}

	/**
	 * Sets the amortiza k.
	 *
	 * @param amortizaK the amortizaK to set
	 */
	public void setAmortizaK(String amortizaK) {
		this.amortizaK = amortizaK;
	}

	/**
	 * Gets the tasa cupon dif.
	 *
	 * @return the tasaCuponDif
	 */
	public String getTasaCuponDif() {
		return tasaCuponDif;
	}

	/**
	 * Sets the tasa cupon dif.
	 *
	 * @param tasaCuponDif the tasaCuponDif to set
	 */
	public void setTasaCuponDif(String tasaCuponDif) {
		this.tasaCuponDif = tasaCuponDif;
	}
	
	/**
	 * Adds the new coupon detail.
	 *
	 * @param generationFileSecurityTRCRTO the generation file security trcrto
	 */
	public void addNewCouponDetail(GenerationFileSecurityTRCRTO generationFileSecurityTRCRTO ){
		GenerationFileDetailCouponTRCRTO generationFileDetailCouponTRCRTO=
				new GenerationFileDetailCouponTRCRTO(generationFileSecurityTRCRTO);
		
		this.listGenerationFileDetailCoupon.add(generationFileDetailCouponTRCRTO);
		
	}
	
}
