package com.pradera.integration.component.settlements.service;

import javax.ejb.Remote;

import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.component.settlements.to.OverTheCounterRegisterTO;
import com.pradera.integration.component.settlements.to.SirtexOperationConsultResponseTO;
import com.pradera.integration.component.settlements.to.SirtexOperationConsultTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface SettlementRemoteService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
@Remote
public interface SettlementRemoteService {
	
	/**
	 * Register ot coperation.
	 *
	 * @param overTheCounterRegisterTO the over the counter register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType registerOTCoperation(OverTheCounterRegisterTO overTheCounterRegisterTO) throws ServiceException;
	
	/**
	 * Deposit of funds lip.
	 *
	 * @param fundsTransferRegisterTO the funds transfer register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType depositOfFundsLip(FundsTransferRegisterTO fundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException;
	
	/**
	 * Consult Sirtex Operation.
	 *
	 * @param overTheCounterRegisterTO the over the counter register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType sirtexOperationConsult(SirtexOperationConsultTO sirtexOperationTO) throws ServiceException;
	
	
	/**
	 * Review Sirtex Operation.
	 *
	 * @param overTheCounterRegisterTO the over the counter register to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType sirtexOperationReview(SirtexOperationConsultResponseTO sirtexOperationTO, String operation) throws ServiceException;
	
	
	
	
	
}
