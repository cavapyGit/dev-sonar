package com.pradera.integration.component.accounts.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class PersonObjectTO extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String documentTypeCode;
	private Integer documentType;
	private String documentTypeDescription;
	private String documentNumber;
	private String duplicateCod;
	private String expended;
	private String documentSource;
	
	private String firstLastName;
	private String secondLastName;
	private String marriedLastName;
	private String name;
	private String fullName;
	private Integer sex;
	private Date birthDate;
	
	private String nationalityCode;
	private Integer nationality;
	private String homePhoneNumber;
	private String officePhoneNumber;
	private String mobileNumber;
	private String faxNumber;
	private String email;
	private Integer indResidence;
	
	private String legalResidenceCountryCode;
	private Integer legalResidenceCountry;
	private String legalDepartmentCode;
	private Integer legalDepartment;
	private String legalProvinceCode;
	private Integer legalProvince;
	private String legalDistrictCode;
	private Integer legalDistrict;
	private String legalAddress;
	private String secondNationalityCode;
	private Integer secondNationality;
	private Integer secondDocumentType;
	private String secondDocumentTypeCode;
	private String secondDocumentNumber;
	private Integer indCanNegotiate;
	private Integer documentIssuanceDate;
	
	private Date registryDate;
	private String stateDescription;	

	private Integer indFatca;
	private Integer indPep;
	private Integer pepMotive;
	private Integer pepRole;
	
	private Integer rowNumber;
		
	private Integer pepGrade;
	private String pepName;
	private Date pepBeginningPeriod;
	private Date pepEndingPeriod;
			
	public PersonObjectTO() {
		super();
	}
	
	public PersonObjectTO(PersonObjectTO personObjectTO) {
		super();
		this.documentTypeCode = personObjectTO.getDocumentTypeCode();
		this.documentType = personObjectTO.getDocumentType();
		this.documentNumber = personObjectTO.getDocumentNumber();
		this.duplicateCod = personObjectTO.getDuplicateCod();
		this.expended = personObjectTO.getExpended();
		this.documentSource = personObjectTO.getDocumentSource();
		this.firstLastName = personObjectTO.getFirstLastName();
		this.secondLastName = personObjectTO.getSecondLastName();
		this.marriedLastName = personObjectTO.getMarriedLastName();
		this.name = personObjectTO.getName();
		this.fullName = personObjectTO.getFullName();
		this.sex = personObjectTO.getSex();
		this.birthDate = personObjectTO.getBirthDate();
		this.nationality = personObjectTO.getNationality();
		this.nationalityCode = personObjectTO.getNationalityCode();
		this.homePhoneNumber = personObjectTO.getHomePhoneNumber();
		this.officePhoneNumber = personObjectTO.getOfficePhoneNumber();
		this.mobileNumber = personObjectTO.getMobileNumber();
		this.faxNumber = personObjectTO.getFaxNumber();
		this.email = personObjectTO.getEmail();
		this.indResidence = personObjectTO.getIndResidence();
		this.legalResidenceCountry = personObjectTO.getLegalResidenceCountry();
		this.legalResidenceCountryCode = personObjectTO.getLegalResidenceCountryCode();
		this.legalDepartment = personObjectTO.getLegalDepartment();
		this.legalDepartmentCode = personObjectTO.getLegalDepartmentCode();
		this.legalProvince = personObjectTO.getLegalProvince();
		this.legalProvinceCode = personObjectTO.getLegalProvinceCode();
		this.legalDistrict = personObjectTO.getLegalDistrict();
		this.legalDistrictCode = personObjectTO.getLegalDistrictCode();
		this.legalAddress = personObjectTO.getLegalAddress();
		this.secondNationality = personObjectTO.getSecondNationality();
		this.secondNationalityCode = personObjectTO.getSecondNationalityCode();
		this.secondDocumentType = personObjectTO.getSecondDocumentType();
		this.secondDocumentTypeCode = personObjectTO.getSecondDocumentTypeCode();
		this.secondDocumentNumber = personObjectTO.getSecondDocumentNumber();
	}
	public Integer getDocumentType() {
		return documentType;
	}
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getDuplicateCod() {
		return duplicateCod;
	}
	public void setDuplicateCod(String duplicateCod) {
		this.duplicateCod = duplicateCod;
	}
	public String getDocumentSource() {
		return documentSource;
	}
	public void setDocumentSource(String documentSource) {
		this.documentSource = documentSource;
	}
	public String getFirstLastName() {
		return firstLastName;
	}
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public String getMarriedLastName() {
		return marriedLastName;
	}
	public void setMarriedLastName(String marriedLastName) {
		this.marriedLastName = marriedLastName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}
	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}
	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getIndResidence() {
		return indResidence;
	}
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}
	public Integer getLegalDepartment() {
		return legalDepartment;
	}
	public void setLegalDepartment(Integer legalDepartment) {
		this.legalDepartment = legalDepartment;
	}
	public String getLegalDepartmentCode() {
		return legalDepartmentCode;
	}
	public void setLegalDepartmentCode(String legalDepartmentCode) {
		this.legalDepartmentCode = legalDepartmentCode;
	}
	public Integer getLegalProvince() {
		return legalProvince;
	}
	public void setLegalProvince(Integer legalProvince) {
		this.legalProvince = legalProvince;
	}
	public Integer getLegalDistrict() {
		return legalDistrict;
	}
	public void setLegalDistrict(Integer legalDistrict) {
		this.legalDistrict = legalDistrict;
	}
	public String getLegalAddress() {
		return legalAddress;
	}
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}
	public Integer getSecondDocumentType() {
		return secondDocumentType;
	}
	public void setSecondDocumentType(Integer secondDocumentType) {
		this.secondDocumentType = secondDocumentType;
	}
	public String getSecondDocumentTypeCode() {
		return secondDocumentTypeCode;
	}
	public void setSecondDocumentTypeCode(String secondDocumentTypeCode) {
		this.secondDocumentTypeCode = secondDocumentTypeCode;
	}
	public String getSecondDocumentNumber() {
		return secondDocumentNumber;
	}
	public void setSecondDocumentNumber(String secondDocumentNumber) {
		this.secondDocumentNumber = secondDocumentNumber;
	}
	public String getDocumentTypeCode() {
		return documentTypeCode;
	}
	public void setDocumentTypeCode(String documentTypeCode) {
		this.documentTypeCode = documentTypeCode;
	}
	public String getExpended() {
		return expended;
	}
	public void setExpended(String expended) {
		this.expended = expended;
	}

	public String getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	public Integer getNationality() {
		return nationality;
	}

	public String getLegalResidenceCountryCode() {
		return legalResidenceCountryCode;
	}

	public void setLegalResidenceCountryCode(String legalResidenceCountryCode) {
		this.legalResidenceCountryCode = legalResidenceCountryCode;
	}

	public Integer getLegalResidenceCountry() {
		return legalResidenceCountry;
	}

	public void setLegalResidenceCountry(Integer legalResidenceCountry) {
		this.legalResidenceCountry = legalResidenceCountry;
	}

	public String getLegalProvinceCode() {
		return legalProvinceCode;
	}

	public void setLegalProvinceCode(String legalProvinceCode) {
		this.legalProvinceCode = legalProvinceCode;
	}

	public String getLegalDistrictCode() {
		return legalDistrictCode;
	}

	public void setLegalDistrictCode(String legalDistrictCode) {
		this.legalDistrictCode = legalDistrictCode;
	}

	public String getSecondNationalityCode() {
		return secondNationalityCode;
	}

	public void setSecondNationalityCode(String secondNationalityCode) {
		this.secondNationalityCode = secondNationalityCode;
	}

	public Integer getSecondNationality() {
		return secondNationality;
	}

	public void setSecondNationality(Integer secondNationality) {
		this.secondNationality = secondNationality;
	}

	public Integer getIndCanNegotiate() {
		return indCanNegotiate;
	}

	public void setIndCanNegotiate(Integer indCanNegotiate) {
		this.indCanNegotiate = indCanNegotiate;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getStateDescription() {
		return stateDescription;
	}

	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	public Integer getDocumentIssuanceDate() {
		return documentIssuanceDate;
	}

	public void setDocumentIssuanceDate(Integer documentIssuanceDate) {
		this.documentIssuanceDate = documentIssuanceDate;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Integer getIndFatca() {
		return indFatca;
	}

	public void setIndFatca(Integer indFatca) {
		this.indFatca = indFatca;
	}

	public Integer getIndPep() {
		return indPep;
	}

	public void setIndPep(Integer indPep) {
		this.indPep = indPep;
	}

	public Integer getPepMotive() {
		return pepMotive;
	}

	public void setPepMotive(Integer pepMotive) {
		this.pepMotive = pepMotive;
	}

	public Integer getPepRole() {
		return pepRole;
	}

	public void setPepRole(Integer pepRole) {
		this.pepRole = pepRole;
	}

	public Integer getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}

	public String getPepName() {
		return pepName;
	}

	public void setPepName(String pepName) {
		this.pepName = pepName;
	}

	public Date getPepBeginningPeriod() {
		return pepBeginningPeriod;
	}

	public void setPepBeginningPeriod(Date pepBeginningPeriod) {
		this.pepBeginningPeriod = pepBeginningPeriod;
	}

	public Date getPepEndingPeriod() {
		return pepEndingPeriod;
	}

	public void setPepEndingPeriod(Date pepEndingPeriod) {
		this.pepEndingPeriod = pepEndingPeriod;
	}

	public Integer getPepGrade() {
		return pepGrade;
	}

	public void setPepGrade(Integer pepGrade) {
		this.pepGrade = pepGrade;
	}
	
}
