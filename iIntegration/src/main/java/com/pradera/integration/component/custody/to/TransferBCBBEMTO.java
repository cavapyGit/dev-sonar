package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class TransferBCBBEMTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String	numeroRegistro;	//NROREGISTRO
	private String  claveValor;		//CLAVE_VALOR
	private String 	cuentaVendedor;	//CTA_VENDEDOR
	private String 	cuentaComprador;//CTA_COMPRADOR
	private String 	cantidad;		//CANTIDAD
	private String 	fecha;			//FECHA
	
	/**
	 * @return the numeroRegistro
	 */
	public String getNumeroRegistro() {
		return numeroRegistro;
	}
	/**
	 * @param numeroRegistro the numeroRegistro to set
	 */
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	/**
	 * @return the claveValor
	 */
	public String getClaveValor() {
		return claveValor;
	}
	/**
	 * @param claveValor the claveValor to set
	 */
	public void setClaveValor(String claveValor) {
		this.claveValor = claveValor;
	}
	/**
	 * @return the cuentaVendedor
	 */
	public String getCuentaVendedor() {
		return cuentaVendedor;
	}
	/**
	 * @param cuentaVendedor the cuentaVendedor to set
	 */
	public void setCuentaVendedor(String cuentaVendedor) {
		this.cuentaVendedor = cuentaVendedor;
	}
	/**
	 * @return the cuentaComprador
	 */
	public String getCuentaComprador() {
		return cuentaComprador;
	}
	/**
	 * @param cuentaComprador the cuentaComprador to set
	 */
	public void setCuentaComprador(String cuentaComprador) {
		this.cuentaComprador = cuentaComprador;
	}
	/**
	 * @return the cantidad
	 */
	public String getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
		
}
