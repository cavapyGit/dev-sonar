package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

/**
 * Datos de Ingreso de la Consulta
 * @author jquino
 *
 */
public class AuthorizedSignatureTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long idAuthorizedSignaturePk;
	private String fullName;
	private String documentNumber;
	
	private String signatureType;
	private String signatureImg;
	private String signatureExtension;

	public AuthorizedSignatureTO() {
		super();
 	}

	public Long getIdAuthorizedSignaturePk() {
		return idAuthorizedSignaturePk;
	}

	public void setIdAuthorizedSignaturePk(Long idAuthorizedSignaturePk) {
		this.idAuthorizedSignaturePk = idAuthorizedSignaturePk;
	}

//	public String getFirstLastName() {
//		return firstLastName;
//	}
//
//	public void setFirstLastName(String firstLastName) {
//		this.firstLastName = firstLastName;
//	}
//
//	public String getSecondLastName() {
//		return secondLastName;
//	}
//
//	public void setSecondLastName(String secondLastName) {
//		this.secondLastName = secondLastName;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getSignatureType() {
		return signatureType;
	}

	public void setSignatureType(String signatureType) {
		this.signatureType = signatureType;
	}

	public String getSignatureExtension() {
		return signatureExtension;
	}

	public void setSignatureExtension(String signatureExtension) {
		this.signatureExtension = signatureExtension;
	}

	public String getSignatureImg() {
		return signatureImg;
	}

	public void setSignatureImg(String signatureImg) {
		this.signatureImg = signatureImg;
	}
	
}
