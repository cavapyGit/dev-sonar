package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.util.Date;

public class VaultControlTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String boveda;
	
	private String armario;
	
	private Integer nivel;
	
	private Integer caja;
	
	private String folio;
	
	private Integer indiceCaja;
	
	private Byte[] certificado;
	
	private String valor;
	
	private Date fechaIngreso;
	
	private Date fechaRetiro;
	
	private Integer estado;
	
	private String usuarioRegistrante;
	
	private Date fechaRegistro;
	
	private String usuarioModificante;
	
	private Date fechaModificacion;
	
	private String ip;
	
	private String app;
	
	private String idSecurityCodeFk;
	
	private String fechaIngresoInicial;
	
	private String fechaIngresoFinal;
	
	private String fechaRetiroInicial;
	
	private String fechaRetiroFinal;
	
	private Integer indCoupon;
	
	private Integer couponNumber;
	
	private String idPendingSecurityCodePk;
	
	private Long idPhysicalCertificateFk;
	
	private Integer securityClass;
	
	private Date expirationDate;
	
	public VaultControlTO() {
		super();
	}
	
	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getBoveda() {
		return boveda;
	}

	public void setBoveda(String boveda) {
		this.boveda = boveda;
	}

	public String getArmario() {
		return armario;
	}

	public void setArmario(String armario) {
		this.armario = armario;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public Integer getCaja() {
		return caja;
	}

	public void setCaja(Integer caja) {
		this.caja = caja;
	}

	public Integer getIndiceCaja() {
		return indiceCaja;
	}

	public void setIndiceCaja(Integer indiceCaja) {
		this.indiceCaja = indiceCaja;
	}

	public Byte[] getCertificado() {
		return certificado;
	}

	public void setCertificado(Byte[] certificado) {
		this.certificado = certificado;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaRetiro() {
		return fechaRetiro;
	}

	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getUsuarioRegistrante() {
		return usuarioRegistrante;
	}

	public void setUsuarioRegistrante(String usuarioRegistrante) {
		this.usuarioRegistrante = usuarioRegistrante;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioModificante() {
		return usuarioModificante;
	}

	public void setUsuarioModificante(String usuarioModificante) {
		this.usuarioModificante = usuarioModificante;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}


	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}


	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}


	public String getFechaIngresoInicial() {
		return fechaIngresoInicial;
	}


	public void setFechaIngresoInicial(String fechaIngresoInicial) {
		this.fechaIngresoInicial = fechaIngresoInicial;
	}

	public String getFechaIngresoFinal() {
		return fechaIngresoFinal;
	}

	public void setFechaIngresoFinal(String fechaIngresoFinal) {
		this.fechaIngresoFinal = fechaIngresoFinal;
	}

	public String getFechaRetiroInicial() {
		return fechaRetiroInicial;
	}

	public void setFechaRetiroInicial(String fechaRetiroInicial) {
		this.fechaRetiroInicial = fechaRetiroInicial;
	}

	public String getFechaRetiroFinal() {
		return fechaRetiroFinal;
	}

	public void setFechaRetiroFinal(String fechaRetiroFinal) {
		this.fechaRetiroFinal = fechaRetiroFinal;
	}


	public String getFolio() {
		return folio;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public Integer getIndCoupon() {
		return indCoupon;
	}


	public void setIndCoupon(Integer indCoupon) {
		this.indCoupon = indCoupon;
	}


	public Integer getCouponNumber() {
		return couponNumber;
	}


	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}


	public String getIdPendingSecurityCodePk() {
		return idPendingSecurityCodePk;
	}


	public void setIdPendingSecurityCodePk(String idPendingSecurityCodePk) {
		this.idPendingSecurityCodePk = idPendingSecurityCodePk;
	}


	public Long getIdPhysicalCertificateFk() {
		return idPhysicalCertificateFk;
	}


	public void setIdPhysicalCertificateFk(Long idPhysicalCertificateFk) {
		this.idPhysicalCertificateFk = idPhysicalCertificateFk;
	}


	public Integer getSecurityClass() {
		return securityClass;
	}


	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}


	public Date getExpirationDate() {
		return expirationDate;
	}


	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

}
