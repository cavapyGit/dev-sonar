package com.pradera.integration.component.securities.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.securities.to.IssuanceRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ExecutorComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
@Remote
public interface SecuritiesRemoteService {
	
	/**
	 * Creates the securities remote service.
	 *
	 * @param loggerUser the logger user
	 * @param securityObjectTO the security object to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	Object createSecuritiesRemoteService(LoggerUser loggerUser, SecurityObjectTO securityObjectTO) throws ServiceException;
	
	/**
	 * Delete securities remote service.
	 *
	 * @param securityObjectTO the security object to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<RecordValidationType> deleteSecuritiesRemoteService(SecurityObjectTO securityObjectTO) throws ServiceException;
	
	/**
	 * Gets the automatic valuator class pk.
	 *
	 * @param securityCodePk the security code pk
	 * @return the automatic valuator class pk
	 * @throws ServiceException the service exception
	 */
	String getAutomaticValuatorClassPk(String securityCodePk) throws ServiceException;
	
	/**
	 * Creates the issuance security tx.
	 *
	 * @param issuanceRegisterTO the issuance register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	RecordValidationType createIssuanceSecurityTx(IssuanceRegisterTO issuanceRegisterTO, LoggerUser loggerUser) throws ServiceException;
	
	/**
	 * Functionalities to add or remove neither operations or annotations related with a placement segment.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	void addOperationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException;
	
	/**
	 * Functionalities to add or remove neither operations or annotations related with a placement segment.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	void substractOperationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException;
	
	/**
	 * Functionalities to add or remove neither operations or annotations related with a placement segment.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	void addAnnotationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException;
	
	/**
	 * Functionalities to add or remove neither operations or annotations related with a placement segment.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	void substractAnnotationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException;
	
}
