package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.component.business.ComponentConstant;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SecuritiesComponentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
public class SecuritiesComponentTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id business process. */
	private Long idBusinessProcess; //executed ID business process 
	
	/** The id operation type. */
	private Long idOperationType; //ID operation type
	
	/** The id securities operation. */
	private Long idSecuritiesOperation; //ID securities operation
	
	/** The id isin code. */
	private String idSecurityCode; //ISIN code
	
	/** The dematerialized balance. */
	private BigDecimal dematerializedBalance; //dematerialized balance to move
	
	/** The physical balance. */
	private BigDecimal physicalBalance; //physical balance to move
	
	private BigDecimal placedBalance;
	
	private BigDecimal circulationBalance;
	
	private BigDecimal shareBalance;
	
	private boolean earlyPayment;
	
	private boolean reverseEarlyPayment;
	
	private boolean securitiesRenew;
	
	private boolean securitiesSuspended; 
	
	/** The ind source target. */
	private Long indSourceTarget = ComponentConstant.SOURCE; //default value

	// flag que indica el llamado al componente ISSUANCE en los eventos corporativos 
	// valor por defecto true
	private boolean issuanceExecute = Boolean.TRUE;
	
	/**
	 * Instantiates a new securities component to.
	 */
	public SecuritiesComponentTO() {
		super();
	}


	/**
	 * Gets the id business process.
	 *
	 * @return the id business process
	 */
	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}


	/**
	 * Sets the id business process.
	 *
	 * @param idBusinessProcess the new id business process
	 */
	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}


	/**
	 * Gets the id operation type.
	 *
	 * @return the id operation type
	 */
	public Long getIdOperationType() {
		return idOperationType;
	}


	/**
	 * Sets the id operation type.
	 *
	 * @param idOperationType the new id operation type
	 */
	public void setIdOperationType(Long idOperationType) {
		this.idOperationType = idOperationType;
	}


	/**
	 * Gets the id securities operation.
	 *
	 * @return the id securities operation
	 */
	public Long getIdSecuritiesOperation() {
		return idSecuritiesOperation;
	}


	/**
	 * Sets the id securities operation.
	 *
	 * @param idSecuritiesOperation the new id securities operation
	 */
	public void setIdSecuritiesOperation(Long idSecuritiesOperation) {
		this.idSecuritiesOperation = idSecuritiesOperation;
	}


	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}


	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}


	/**
	 * Gets the dematerialized balance.
	 *
	 * @return the dematerialized balance
	 */
	public BigDecimal getDematerializedBalance() {
		return dematerializedBalance;
	}


	/**
	 * Sets the dematerialized balance.
	 *
	 * @param dematerializedBalance the new dematerialized balance
	 */
	public void setDematerializedBalance(BigDecimal dematerializedBalance) {
		this.dematerializedBalance = dematerializedBalance;
	}


	/**
	 * Gets the physical balance.
	 *
	 * @return the physical balance
	 */
	public BigDecimal getPhysicalBalance() {
		return physicalBalance;
	}


	/**
	 * Sets the physical balance.
	 *
	 * @param physicalBalance the new physical balance
	 */
	public void setPhysicalBalance(BigDecimal physicalBalance) {
		this.physicalBalance = physicalBalance;
	}


	/**
	 * Gets the ind source target.
	 *
	 * @return the ind source target
	 */
	public Long getIndSourceTarget() {
		return indSourceTarget;
	}


	/**
	 * Sets the ind source target.
	 *
	 * @param indSourceTarget the new ind source target
	 */
	public void setIndSourceTarget(Long indSourceTarget) {
		this.indSourceTarget = indSourceTarget;
	}


	public BigDecimal getPlacedBalance() {
		return placedBalance;
	}


	public void setPlacedBalance(BigDecimal placedBalance) {
		this.placedBalance = placedBalance;
	}


	public BigDecimal getCirculationBalance() {
		return circulationBalance;
	}


	public void setCirculationBalance(BigDecimal circulationBalance) {
		this.circulationBalance = circulationBalance;
	}


	public BigDecimal getShareBalance() {
		return shareBalance;
	}


	public void setShareBalance(BigDecimal shareBalance) {
		this.shareBalance = shareBalance;
	}


	public boolean isEarlyPayment() {
		return earlyPayment;
	}


	public void setEarlyPayment(boolean redeemWhenShareBalanceZero) {
		this.earlyPayment = redeemWhenShareBalanceZero;
	}


	/**
	 * @return the reverseEarlyPayment
	 */
	public boolean isReverseEarlyPayment() {
		return reverseEarlyPayment;
	}


	/**
	 * @param reverseEarlyPayment the reverseEarlyPayment to set
	 */
	public void setReverseEarlyPayment(boolean reverseEarlyPayment) {
		this.reverseEarlyPayment = reverseEarlyPayment;
	}


	/**
	 * @return the securitiesRenew
	 */
	public boolean isSecuritiesRenew() {
		return securitiesRenew;
	}


	/**
	 * @param securitiesRenew the securitiesRenew to set
	 */
	public void setSecuritiesRenew(boolean securitiesRenew) {
		this.securitiesRenew = securitiesRenew;
	}


	/**
	 * @return the securitiesSuspended
	 */
	public boolean isSecuritiesSuspended() {
		return securitiesSuspended;
	}


	/**
	 * @param securitiesSuspended the securitiesSuspended to set
	 */
	public void setSecuritiesSuspended(boolean securitiesSuspended) {
		this.securitiesSuspended = securitiesSuspended;
	}


	public boolean isIssuanceExecute() {
		return issuanceExecute;
	}


	public void setIssuanceExecute(boolean issuanceExecute) {
		this.issuanceExecute = issuanceExecute;
	}

}