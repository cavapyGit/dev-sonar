package com.pradera.integration.component.settlements.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SirtexOperationConsultResponseTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer queryNumber;
	private String operationType;
	private Date operationDate;
	
	List<SirtexOperationResponseTO> sirtexOperations;
	List<SirtexOperationStateResponseTO> sirtexStateOperations;
	
	private Long idInterfaceProcess;
	
	public SirtexOperationConsultResponseTO() {
		super();
	}

	public Integer getQueryNumber() {
		return queryNumber;
	}

	public void setQueryNumber(Integer queryNumber) {
		this.queryNumber = queryNumber;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}

	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}

	public List<SirtexOperationResponseTO> getSirtexOperations() {
		return sirtexOperations;
	}

	public void setSirtexOperations(List<SirtexOperationResponseTO> sirtexOperations) {
		this.sirtexOperations = sirtexOperations;
	}

	public List<SirtexOperationStateResponseTO> getSirtexStateOperations() {
		return sirtexStateOperations;
	}

	public void setSirtexStateOperations(
			List<SirtexOperationStateResponseTO> sirtexStateOperations) {
		this.sirtexStateOperations = sirtexStateOperations;
	}
	
	
}
