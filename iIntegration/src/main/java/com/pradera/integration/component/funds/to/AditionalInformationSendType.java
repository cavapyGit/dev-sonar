package com.pradera.integration.component.funds.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class AditionalInformationSendType extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	
	private String value;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
