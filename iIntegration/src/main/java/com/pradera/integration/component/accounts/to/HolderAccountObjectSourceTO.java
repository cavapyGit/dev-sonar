package com.pradera.integration.component.accounts.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class HolderAccountObjectSourceTO extends ValidationOperationType implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String INDIVIDUAL_ACCOUNT_TYPE = "IND";
	public static final String OWNERSHIP_ACCOUNT_TYPE = "MAN";
	
	private boolean createObject;
	private Long idHolderAccount;
	private Long idParticipant;
	private Integer accountNumber;
	private Integer accountGroup;
	private Integer accountType; // parameter table
	private String accountTypeCode; // Ind(individual) or Man(ownership)
	private Integer holderType;  // from parameter table
	private Integer holderTypeAlt; // 1 natural 2 juridic (used for dpf)
	private String alternateCode;
	private String description;
	private String conditions;
	private boolean validateAccount;		
	
	
	public void setChildrenIndicators(){
		if(Validations.validateListIsNotNullAndNotEmpty(lstNaturalHolderObjectTOs)){
			for (NaturalHolderObjectTO naturalHolderObjectTO : lstNaturalHolderObjectTOs) {
				naturalHolderObjectTO.setDpfInterface(isDpfInterface());
			}
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeObjectTOs)){
			for (LegalRepresentativeObjectTO legalRepresentativeObjectTO : lstLegalRepresentativeObjectTOs) {
				legalRepresentativeObjectTO.setDpfInterface(isDpfInterface());
			}
		}
		if(Validations.validateIsNotNull(juridicHolderObjectTO)){
			juridicHolderObjectTO.setDpfInterface(isDpfInterface());
		}
	}
	
	List<NaturalHolderObjectTO> lstNaturalHolderObjectTOs;
	JuridicHolderObjectTO juridicHolderObjectTO;
	List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjectTOs;
	
	private Long operationNumber; //operation number which it was created 
	private SecurityObjectTO securityObjectTO; //holder account balance query
	private String blockCode; //holder account balance query
	private String indReported; //holder account balance query
	
	
	public HolderAccountObjectSourceTO() {
		super();
	}

	public Long getIdHolderAccount() {
		return idHolderAccount;
	}

	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}

	public Long getIdParticipant() {
		return idParticipant;
	}

	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getAccountGroup() {
		return accountGroup;
	}

	public void setAccountGroup(Integer accountGroup) {
		this.accountGroup = accountGroup;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public String getAlternateCode() {
		return alternateCode;
	}

	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<NaturalHolderObjectTO> getLstNaturalHolderObjectTOs() {
		return lstNaturalHolderObjectTOs;
	}

	public void setLstNaturalHolderObjectTOs(
			List<NaturalHolderObjectTO> lstNaturalHolderObjectTOs) {
		this.lstNaturalHolderObjectTOs = lstNaturalHolderObjectTOs;
	}

	public JuridicHolderObjectTO getJuridicHolderObjectTO() {
		return juridicHolderObjectTO;
	}

	public void setJuridicHolderObjectTO(JuridicHolderObjectTO juridicHolderObjectTO) {
		this.juridicHolderObjectTO = juridicHolderObjectTO;
	}

	public List<LegalRepresentativeObjectTO> getLstLegalRepresentativeObjectTOs() {
		return lstLegalRepresentativeObjectTOs;
	}

	public void setLstLegalRepresentativeObjectTOs(
			List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjectTOs) {
		this.lstLegalRepresentativeObjectTOs = lstLegalRepresentativeObjectTOs;
	}

	public Integer getHolderType() {
		return holderType;
	}

	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}

	public boolean isCreateObject() {
		return createObject;
	}

	public void setCreateObject(boolean createObject) {
		this.createObject = createObject;
	}

	public String getAccountTypeCode() {
		return accountTypeCode;
	}

	public void setAccountTypeCode(String accountTypeCode) {
		this.accountTypeCode = accountTypeCode;
	}

	public Integer getHolderTypeAlt() {
		return holderTypeAlt;
	}

	public void setHolderTypeAlt(Integer holderTypeAlt) {
		this.holderTypeAlt = holderTypeAlt;
	}

	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}

	public String getBlockCode() {
		return blockCode;
	}

	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}

	public String getIndReported() {
		return indReported;
	}

	public void setIndReported(String indReported) {
		this.indReported = indReported;
	}

	public Long getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public boolean isValidateAccount() {
		return validateAccount;
	}

	public void setValidateAccount(boolean validateAccount) {
		this.validateAccount = validateAccount;
	}
	
}
