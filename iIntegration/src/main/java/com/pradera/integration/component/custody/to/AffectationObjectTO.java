package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.util.Date;

public class AffectationObjectTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String affectationCode;
	private Integer lockingDays;
	private String referenceDocument;
	private Integer documentType;
	private Date receiptDate;
	private Long idHolder;
	private String descriptionHolder;
	private LegalInformationObjetTO legalInformationObjetTO;
	
	/**
	 * @return the affectationCode
	 */
	public String getAffectationCode() {
		return affectationCode;
	}
	/**
	 * @param affectationCode the affectationCode to set
	 */
	public void setAffectationCode(String affectationCode) {
		this.affectationCode = affectationCode;
	}
	/**
	 * @return the lockingDays
	 */
	public Integer getLockingDays() {
		return lockingDays;
	}
	/**
	 * @param lockingDays the lockingDays to set
	 */
	public void setLockingDays(Integer lockingDays) {
		this.lockingDays = lockingDays;
	}
	/**
	 * @return the referenceDocument
	 */
	public String getReferenceDocument() {
		return referenceDocument;
	}
	/**
	 * @param referenceDocument the referenceDocument to set
	 */
	public void setReferenceDocument(String referenceDocument) {
		this.referenceDocument = referenceDocument;
	}
	/**
	 * @return the documentType
	 */
	public Integer getDocumentType() {
		return documentType;
	}
	/**
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	/**
	 * @return the receiptDate
	 */
	public Date getReceiptDate() {
		return receiptDate;
	}
	/**
	 * @param receiptDate the receiptDate to set
	 */
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	/**
	 * @return the idHolder
	 */
	public Long getIdHolder() {
		return idHolder;
	}
	/**
	 * @param idHolder the idHolder to set
	 */
	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}
	/**
	 * @return the descriptionHolder
	 */
	public String getDescriptionHolder() {
		return descriptionHolder;
	}
	/**
	 * @param descriptionHolder the descriptionHolder to set
	 */
	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}
	/**
	 * @return the legalInformationObjetTO
	 */
	public LegalInformationObjetTO getLegalInformationObjetTO() {
		return legalInformationObjetTO;
	}
	/**
	 * @param legalInformationObjetTO the legalInformationObjetTO to set
	 */
	public void setLegalInformationObjetTO(
			LegalInformationObjetTO legalInformationObjetTO) {
		this.legalInformationObjetTO = legalInformationObjetTO;
	}
	
}
