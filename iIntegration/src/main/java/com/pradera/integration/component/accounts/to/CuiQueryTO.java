package com.pradera.integration.component.accounts.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;

/**
 * Datos de Ingreso de la Consulta
 * @author jquino
 *
 */
public class CuiQueryTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long queryNumber;
	private String operationType;
	private Date operationDate;
	
	private Integer holdersType; //1:Natural 2:Juridico
	
	//Juridico
	private String juridicDocumentNumber;
	private String razonSocial;
	
	//Natural
	private String naturalDocumentNumber;
	private String duplicateCode;
	private String documentSource;
	private String firstLastName;
	private String secondLastName;
	private String name;
	private String fullName;
	
	
	public CuiQueryTO() {
		super();
	}

	
	public Integer getHoldersType() {
		return holdersType;
	}
	public void setHoldersType(Integer holdersType) {
		this.holdersType = holdersType;
	}
	public String getJuridicDocumentNumber() {
		return juridicDocumentNumber;
	}
	public void setJuridicDocumentNumber(String juridicDocumentNumber) {
		this.juridicDocumentNumber = juridicDocumentNumber;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getNaturalDocumentNumber() {
		return naturalDocumentNumber;
	}
	public void setNaturalDocumentNumber(String naturalDocumentNumber) {
		this.naturalDocumentNumber = naturalDocumentNumber;
	}
	public String getDuplicateCode() {
		return duplicateCode;
	}
	public void setDuplicateCode(String duplicateCode) {
		this.duplicateCode = duplicateCode;
	}
	public String getDocumentSource() {
		return documentSource;
	}
	public void setDocumentSource(String documentSource) {
		this.documentSource = documentSource;
	}
	public String getFirstLastName() {
		return firstLastName;
	}
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public Date getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	public Long getQueryNumber() {
		return queryNumber;
	}
	public void setQueryNumber(Long queryNumber) {
		this.queryNumber = queryNumber;
	}
	public boolean isNaturalHolder(){
		if(this.holdersType == 1) return true;
		else return false;
	}
	public boolean isJuridicHolder(){
		if(this.holdersType == 2) return true;
		else return false;
	}
}
