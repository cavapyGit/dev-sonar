package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;

/**
 * Datos de Ingreso de la Consulta
 * @author jquino
 *
 */
public class AuthorizedSignatureQueryTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private NaturalHolderObjectTO naturalHolderObjectTO;
	private JuridicHolderObjectTO juridicHolderObjectTO;
	
	public AuthorizedSignatureQueryTO() {
		super();
 	}

	public NaturalHolderObjectTO getNaturalHolderObjectTO() {
		return naturalHolderObjectTO;
	}

	public void setNaturalHolderObjectTO(NaturalHolderObjectTO naturalHolderObjectTO) {
		this.naturalHolderObjectTO = naturalHolderObjectTO;
	}

	public JuridicHolderObjectTO getJuridicHolderObjectTO() {
		return juridicHolderObjectTO;
	}

	public void setJuridicHolderObjectTO(JuridicHolderObjectTO juridicHolderObjectTO) {
		this.juridicHolderObjectTO = juridicHolderObjectTO;
	}

}
