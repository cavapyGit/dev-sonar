package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class AccountEntryRegisterTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer motive;
	private BigDecimal stockQuantity;
	private BigDecimal price;
	private Integer departament;
	private Integer municipality;
	private Integer province;
	private String restrictions; 
	
	private MarketFactAccountTO marketFactAccountTO;
	private HolderAccountObjectTO holderAccountObjectTO;
	private SecurityObjectTO securityObjectTO;
	
	public Integer getMotive() {
		return motive;
	}
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getDepartament() {
		return departament;
	}
	public void setDepartament(Integer departament) {
		this.departament = departament;
	}
	public Integer getMunicipality() {
		return municipality;
	}
	public void setMunicipality(Integer municipality) {
		this.municipality = municipality;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	
	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}
	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}
	public HolderAccountObjectTO getHolderAccountObjectTO() {
		return holderAccountObjectTO;
	}
	public void setHolderAccountObjectTO(HolderAccountObjectTO holderAccountObjectTO) {
		this.holderAccountObjectTO = holderAccountObjectTO;
	}
	public String getRestrictions() {
		return restrictions;
	}
	public void setRestrictions(String restrictions) {
		this.restrictions = restrictions;
	}
	public MarketFactAccountTO getMarketFactAccountTO() {
		return marketFactAccountTO;
	}
	public void setMarketFactAccountTO(MarketFactAccountTO marketFactAccountTO) {
		this.marketFactAccountTO = marketFactAccountTO;
	}
	
}
