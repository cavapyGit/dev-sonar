package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class DetailCustodianInsuranceTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String 		claseValor;					//Clase de Valor
	private String  	claveValor;					//Clave de Valor (Pizarra)
	private BigDecimal 	tasaRelevanteValoracion;	//Tasa Relevante de Valoración
	private BigDecimal 	precioNominalMonedaOriginal;//Precio Nominal en Moneda Original
	private BigDecimal 	precioNominalBolivianos;	//Precio Nominal en Bolivianos
	private BigDecimal 	precioMercadoMonedaOriginal;//Precio Mercado en Moneda Original
	private BigDecimal 	precioMercadoBolivianos;	//Precio Mercado en Bolivianos
	private BigDecimal	cantidadTotalValores;		//Cantidad total de Valores
	private BigDecimal 	montoTotalMonedaOriginal;	//Monto Total en Moneda Original
	private String 		codigoCustodia;				//Código de Custodia
	/**
	 * @return the claseValor
	 */
	public String getClaseValor() {
		return claseValor;
	}
	/**
	 * @param claseValor the claseValor to set
	 */
	public void setClaseValor(String claseValor) {
		this.claseValor = claseValor;
	}
	/**
	 * @return the claveValor
	 */
	public String getClaveValor() {
		return claveValor;
	}
	/**
	 * @param claveValor the claveValor to set
	 */
	public void setClaveValor(String claveValor) {
		this.claveValor = claveValor;
	}
	/**
	 * @return the tasaRelevanteValoracion
	 */
	public BigDecimal getTasaRelevanteValoracion() {
		return tasaRelevanteValoracion;
	}
	/**
	 * @param tasaRelevanteValoracion the tasaRelevanteValoracion to set
	 */
	public void setTasaRelevanteValoracion(BigDecimal tasaRelevanteValoracion) {
		this.tasaRelevanteValoracion = tasaRelevanteValoracion;
	}
	/**
	 * @return the precioNominalMonedaOriginal
	 */
	public BigDecimal getPrecioNominalMonedaOriginal() {
		return precioNominalMonedaOriginal;
	}
	/**
	 * @param precioNominalMonedaOriginal the precioNominalMonedaOriginal to set
	 */
	public void setPrecioNominalMonedaOriginal(
			BigDecimal precioNominalMonedaOriginal) {
		this.precioNominalMonedaOriginal = precioNominalMonedaOriginal;
	}
	/**
	 * @return the precioNominalBolivianos
	 */
	public BigDecimal getPrecioNominalBolivianos() {
		return precioNominalBolivianos;
	}
	/**
	 * @param precioNominalBolivianos the precioNominalBolivianos to set
	 */
	public void setPrecioNominalBolivianos(BigDecimal precioNominalBolivianos) {
		this.precioNominalBolivianos = precioNominalBolivianos;
	}
	/**
	 * @return the precioMercadoMonedaOriginal
	 */
	public BigDecimal getPrecioMercadoMonedaOriginal() {
		return precioMercadoMonedaOriginal;
	}
	/**
	 * @param precioMercadoMonedaOriginal the precioMercadoMonedaOriginal to set
	 */
	public void setPrecioMercadoMonedaOriginal(
			BigDecimal precioMercadoMonedaOriginal) {
		this.precioMercadoMonedaOriginal = precioMercadoMonedaOriginal;
	}
	/**
	 * @return the precioMercadoBolivianos
	 */
	public BigDecimal getPrecioMercadoBolivianos() {
		return precioMercadoBolivianos;
	}
	/**
	 * @param precioMercadoBolivianos the precioMercadoBolivianos to set
	 */
	public void setPrecioMercadoBolivianos(BigDecimal precioMercadoBolivianos) {
		this.precioMercadoBolivianos = precioMercadoBolivianos;
	}
	/**
	 * @return the cantidadTotalValores
	 */
	public BigDecimal getCantidadTotalValores() {
		return cantidadTotalValores;
	}
	/**
	 * @param cantidadTotalValores the cantidadTotalValores to set
	 */
	public void setCantidadTotalValores(BigDecimal cantidadTotalValores) {
		this.cantidadTotalValores = cantidadTotalValores;
	}
	/**
	 * @return the montoTotalMonedaOriginal
	 */
	public BigDecimal getMontoTotalMonedaOriginal() {
		return montoTotalMonedaOriginal;
	}
	/**
	 * @param montoTotalMonedaOriginal the montoTotalMonedaOriginal to set
	 */
	public void setMontoTotalMonedaOriginal(BigDecimal montoTotalMonedaOriginal) {
		this.montoTotalMonedaOriginal = montoTotalMonedaOriginal;
	}
	/**
	 * @return the codigoCustodia
	 */
	public String getCodigoCustodia() {
		return codigoCustodia;
	}
	/**
	 * @param codigoCustodia the codigoCustodia to set
	 */
	public void setCodigoCustodia(String codigoCustodia) {
		this.codigoCustodia = codigoCustodia;
	}
	
		
}
