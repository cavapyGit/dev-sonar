package com.pradera.integration.component.securities.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class SecurityNegotiationTO extends OperationInterfaceTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String transactionDate;
	private String instrument; 
	private String serie;
	private String alternativeCode;
	private String issuanceDate;
	private String expirationDate;
	private String issuanceRate;
	private String cui;
	private String documentNumber;
	private String resident;
	private String stockQuantity;
	private String operation;
	private String holderName;
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getAlternativeCode() {
		return alternativeCode;
	}
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	public String getIssuanceDate() {
		return issuanceDate;
	}
	public void setIssuanceDate(String issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getIssuanceRate() {
		return issuanceRate;
	}
	public void setIssuanceRate(String issuanceRate) {
		this.issuanceRate = issuanceRate;
	}
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getResident() {
		return resident;
	}
	public void setResident(String resident) {
		this.resident = resident;
	}
	public String getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(String stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
}