package com.pradera.integration.component.funds.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class FundsConsultRegisterTO extends OperationInterfaceTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String transferType;
	private AutomaticSendType automaticSendTypeTO;
	
	
	public FundsConsultRegisterTO() {
		super();
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public AutomaticSendType getAutomaticSendTypeTO() {
		return automaticSendTypeTO;
	}

	public void setAutomaticSendTypeTO(AutomaticSendType automaticSendTypeTO) {
		this.automaticSendTypeTO = automaticSendTypeTO;
	}
}
