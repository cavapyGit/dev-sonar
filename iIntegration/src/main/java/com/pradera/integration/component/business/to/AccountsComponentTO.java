package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.type.BooleanType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AccountsComponentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
public class AccountsComponentTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id business process. */
	private Long idBusinessProcess; //executed ID business process 
	
	/** The id operation type. */
	private Long idOperationType; //ID operation type 
	
	/** The id trade operation. */
	private Long idTradeOperation; //ID trade operation (Mechanism operation, International operation)
	
	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The id custody operation. */
	private Long idCustodyOperation; //ID custody operation (block operation, unblock operation, securities transfer, ...)
	
	/** The id ref custody operation. */
	private Long idRefCustodyOperation; //ID referential custody operation (block operation, unblock operation, securities transfer, ...)
	
	/** The id corporative operation. */
	private Long idCorporativeOperation; //ID corporative operation
	
	/** The id guarantee operation. */
	private Long idGuaranteeOperation; //ID guarantee operation
	
	/** The lst source accounts. */
	private List<HolderAccountBalanceTO> lstSourceAccounts; //List of source holder accounts
	
	/** The lst target accounts. */
	private List<HolderAccountBalanceTO> lstTargetAccounts; //List of target holder accounts
	
	/** The operation part. */
	private Integer operationPart; //operation part used only for negotiation operations
	
	/** The ind market fact. */
	private Integer indMarketFact; //indicator to know if the component affect the sub market fact account
	
	/**
	 * Instantiates a new accounts component to.
	 */
	public AccountsComponentTO() {
		super();
		this.indMarketFact= BooleanType.NO.getCode(); //default
	}


	/**
	 * Gets the id business process.
	 *
	 * @return the id business process
	 */
	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}


	/**
	 * Sets the id business process.
	 *
	 * @param idBusinessProcess the new id business process
	 */
	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}


	/**
	 * Gets the id operation type.
	 *
	 * @return the id operation type
	 */
	public Long getIdOperationType() {
		return idOperationType;
	}


	/**
	 * Sets the id operation type.
	 *
	 * @param idOperationType the new id operation type
	 */
	public void setIdOperationType(Long idOperationType) {
		this.idOperationType = idOperationType;
	}


	/**
	 * Gets the id trade operation.
	 *
	 * @return the id trade operation
	 */
	public Long getIdTradeOperation() {
		return idTradeOperation;
	}


	/**
	 * Sets the id trade operation.
	 *
	 * @param idTradeOperation the new id trade operation
	 */
	public void setIdTradeOperation(Long idTradeOperation) {
		this.idTradeOperation = idTradeOperation;
	}


	/**
	 * Gets the id custody operation.
	 *
	 * @return the id custody operation
	 */
	public Long getIdCustodyOperation() {
		return idCustodyOperation;
	}


	/**
	 * Sets the id custody operation.
	 *
	 * @param idCustodyOperation the new id custody operation
	 */
	public void setIdCustodyOperation(Long idCustodyOperation) {
		this.idCustodyOperation = idCustodyOperation;
	}


	/**
	 * Gets the id corporative operation.
	 *
	 * @return the id corporative operation
	 */
	public Long getIdCorporativeOperation() {
		return idCorporativeOperation;
	}


	/**
	 * Sets the id corporative operation.
	 *
	 * @param idCorporativeOperation the new id corporative operation
	 */
	public void setIdCorporativeOperation(Long idCorporativeOperation) {
		this.idCorporativeOperation = idCorporativeOperation;
	}


	/**
	 * Gets the id guarantee operation.
	 *
	 * @return the id guarantee operation
	 */
	public Long getIdGuaranteeOperation() {
		return idGuaranteeOperation;
	}


	/**
	 * Sets the id guarantee operation.
	 *
	 * @param idGuaranteeOperation the new id guarantee operation
	 */
	public void setIdGuaranteeOperation(Long idGuaranteeOperation) {
		this.idGuaranteeOperation = idGuaranteeOperation;
	}


	/**
	 * Gets the lst source accounts.
	 *
	 * @return the lst source accounts
	 */
	public List<HolderAccountBalanceTO> getLstSourceAccounts() {
		return lstSourceAccounts;
	}


	/**
	 * Sets the lst source accounts.
	 *
	 * @param lstSourceAccounts the new lst source accounts
	 */
	public void setLstSourceAccounts(List<HolderAccountBalanceTO> lstSourceAccounts) {
		this.lstSourceAccounts = lstSourceAccounts;
	}


	/**
	 * Gets the lst target accounts.
	 *
	 * @return the lst target accounts
	 */
	public List<HolderAccountBalanceTO> getLstTargetAccounts() {
		return lstTargetAccounts;
	}


	/**
	 * Sets the lst target accounts.
	 *
	 * @param lstTargetAccounts the new lst target accounts
	 */
	public void setLstTargetAccounts(List<HolderAccountBalanceTO> lstTargetAccounts) {
		this.lstTargetAccounts = lstTargetAccounts;
	}


	/**
	 * Gets the id ref custody operation.
	 *
	 * @return the id ref custody operation
	 */
	public Long getIdRefCustodyOperation() {
		return idRefCustodyOperation;
	}


	/**
	 * Sets the id ref custody operation.
	 *
	 * @param idRefCustodyOperation the new id ref custody operation
	 */
	public void setIdRefCustodyOperation(Long idRefCustodyOperation) {
		this.idRefCustodyOperation = idRefCustodyOperation;
	}


	/**
	 * Gets the ind market fact.
	 *
	 * @return the ind market fact
	 */
	public Integer getIndMarketFact() {
		return indMarketFact;
	}


	/**
	 * Sets the ind market fact.
	 *
	 * @param indMarketFact the ind market fact
	 */
	public void setIndMarketFact(Integer indMarketFact) {
		this.indMarketFact = indMarketFact;
	}


	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}


	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}


	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}


	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}


	@Override
	public String toString() {
		return "AccountsComponentTO [idBusinessProcess=" + idBusinessProcess + ", idOperationType=" + idOperationType
				+ ", idTradeOperation=" + idTradeOperation + ", idSettlementOperation=" + idSettlementOperation
				+ ", idCustodyOperation=" + idCustodyOperation + ", idRefCustodyOperation=" + idRefCustodyOperation
				+ ", idCorporativeOperation=" + idCorporativeOperation + ", idGuaranteeOperation="
				+ idGuaranteeOperation + ", lstSourceAccounts=" + lstSourceAccounts + ", lstTargetAccounts="
				+ lstTargetAccounts + ", operationPart=" + operationPart + ", indMarketFact=" + indMarketFact + "]";
	}

}