package com.pradera.integration.component.business.to;

import java.io.Serializable;

public class SplitCouponOperationTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private Long idSplitCouponOperationPk;
	private Integer indMarketFact;
	
	public Long getIdSplitCouponOperationPk() {
		return idSplitCouponOperationPk;
	}
	public void setIdSplitCouponOperationPk(Long idSplitCouponOperationPk) {
		this.idSplitCouponOperationPk = idSplitCouponOperationPk;
	}
	public Integer getIndMarketFact() {
		return indMarketFact;
	}
	public void setIndMarketFact(Integer indMarketFact) {
		this.indMarketFact = indMarketFact;
	}
}
