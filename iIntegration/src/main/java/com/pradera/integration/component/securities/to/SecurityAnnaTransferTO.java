package com.pradera.integration.component.securities.to;


import java.io.Serializable;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SecurityAnnaTransferTO extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String 		idIsinCode;
	private String 		securityState;
	private String 		instrument;
	private String 		description;
	private String 		cfiCode;
	private String  	fisin;
	private String  	preliminaryTerms;
	private String	    nominalValue;
	private String 		currency;
	private String 		smallDenomination;
	private String		conversionRatio;
	private String		expirationDate;
	private String		exercisePrice;
	private String		exerciseCurrPrice;
	private String		underlying;
	private String 		interestType;
	private String	    interestRate;
	private String		firstPaymentDateSmall;
	private String		frequency;
	private String		firstPaymentDate;
	private String		comments;
	private String		mic;
	private String		lei;
	private String		fmn;
	private String		fml;
	private String		lei2;
	private String 		depositary;
	private String 		issuerName;
	private String		issuerLei;
	private String 		mnemonicIssuer;
	private String		supranacional;
	private String		address_1;
	private String		address_2;
	private String		addressState;
	private String		postCode;
	private String		city;
	private String		country;
	private String		c_38;
	private String		c_39;
	private String		c_40;
	private String		c_41;
	private String		c_42;
	private String		c_43;
	private String		idSecurityCodePk;
	
	
	
	public String getIdIsinCode() {
		return idIsinCode;
	}
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}
	public String getSecurityState() {
		return securityState;
	}
	public void setSecurityState(String securityState) {
		this.securityState = securityState;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCfiCode() {
		return cfiCode;
	}
	public void setCfiCode(String cfiCode) {
		this.cfiCode = cfiCode;
	}
	public String getNominalValue() {
		return nominalValue;
	}
	public void setNominalValue(String nominalValue) {
		this.nominalValue = nominalValue;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSmallDenomination() {
		return smallDenomination;
	}
	public void setSmallDenomination(String smallDenomination) {
		this.smallDenomination = smallDenomination;
	}
	public String getConversionRatio() {
		return conversionRatio;
	}
	public void setConversionRatio(String conversionRatio) {
		this.conversionRatio = conversionRatio;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getExercisePrice() {
		return exercisePrice;
	}
	public void setExercisePrice(String exercisePrice) {
		this.exercisePrice = exercisePrice;
	}
	public String getExerciseCurrPrice() {
		return exerciseCurrPrice;
	}
	public void setExerciseCurrPrice(String exerciseCurrPrice) {
		this.exerciseCurrPrice = exerciseCurrPrice;
	}
	public String getUnderlying() {
		return underlying;
	}
	public void setUnderlying(String underlying) {
		this.underlying = underlying;
	}
	public String getInterestType() {
		return interestType;
	}
	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}
	public String getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}
	public String getFirstPaymentDateSmall() {
		return firstPaymentDateSmall;
	}
	public void setFirstPaymentDateSmall(String firstPaymentDateSmall) {
		this.firstPaymentDateSmall = firstPaymentDateSmall;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getFirstPaymentDate() {
		return firstPaymentDate;
	}
	public void setFirstPaymentDate(String firstPaymentDate) {
		this.firstPaymentDate = firstPaymentDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getMic() {
		return mic;
	}
	public void setMic(String mic) {
		this.mic = mic;
	}
	public String getLei() {
		return lei;
	}
	public void setLei(String lei) {
		this.lei = lei;
	}
	public String getFmn() {
		return fmn;
	}
	public void setFmn(String fmn) {
		this.fmn = fmn;
	}
	public String getFml() {
		return fml;
	}
	public void setFml(String fml) {
		this.fml = fml;
	}
	public String getLei2() {
		return lei2;
	}
	public void setLei2(String lei2) {
		this.lei2 = lei2;
	}
	public String getDepositary() {
		return depositary;
	}
	public void setDepositary(String depositary) {
		this.depositary = depositary;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getIssuerLei() {
		return issuerLei;
	}
	public void setIssuerLei(String issuerLei) {
		this.issuerLei = issuerLei;
	}
	public String getMnemonicIssuer() {
		return mnemonicIssuer;
	}
	public void setMnemonicIssuer(String mnemonicIssuer) {
		this.mnemonicIssuer = mnemonicIssuer;
	}
	public String getSupranacional() {
		return supranacional;
	}
	public void setSupranacional(String supranacional) {
		this.supranacional = supranacional;
	}
	public String getAddress_1() {
		return address_1;
	}
	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}
	public String getAddress_2() {
		return address_2;
	}
	public void setAddress_2(String address_2) {
		this.address_2 = address_2;
	}
	public String getAddressState() {
		return addressState;
	}
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getC_38() {
		return c_38;
	}
	public void setC_38(String c_38) {
		this.c_38 = c_38;
	}
	public String getC_39() {
		return c_39;
	}
	public void setC_39(String c_39) {
		this.c_39 = c_39;
	}
	public String getC_40() {
		return c_40;
	}
	public void setC_40(String c_40) {
		this.c_40 = c_40;
	}
	public String getC_41() {
		return c_41;
	}
	public void setC_41(String c_41) {
		this.c_41 = c_41;
	}
	public String getC_42() {
		return c_42;
	}
	public void setC_42(String c_42) {
		this.c_42 = c_42;
	}
	public String getC_43() {
		return c_43;
	}
	public void setC_43(String c_43) {
		this.c_43 = c_43;
	}
	/**
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	/**
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	/**
	 * @return the fisin
	 */
	public String getFisin() {
		return fisin;
	}
	/**
	 * @param fisin the fisin to set
	 */
	public void setFisin(String fisin) {
		this.fisin = fisin;
	}
	/**
	 * @return the preliminaryTerms
	 */
	public String getPreliminaryTerms() {
		return preliminaryTerms;
	}
	/**
	 * @param preliminaryTerms the preliminaryTerms to set
	 */
	public void setPreliminaryTerms(String preliminaryTerms) {
		this.preliminaryTerms = preliminaryTerms;
	}
	
}