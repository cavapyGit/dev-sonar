package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.component.securities.to.CouponObjectTO;

public class SecurityConsultationTO implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cserie;
	private String codInst;
	private String codEm;
	private String monedaGen;
	private String moneda;
	private String metodoValuacion;
	private Date fechaEmision;
	private String plazo;
	private Date fechaVenci;
	private BigDecimal trem;
	private BigDecimal tDesc;
	private Long plazoAmortiza;
	private Long nroCupones;
	private BigDecimal valorEmision;
	private BigDecimal valorNominal;
	private String agenteRegistrador;
	private String status;
	private Long diasRedimidos;
	private Long tipoGenericoValor;
	private String lCupNegociable;
	private String lFciNeg;
	private String prepago;
	private String lSubor;
	private String lTasaFija;
	private String tipoBono;
	private String ltasaDif;
	private Long tir;
	private List<CouponObjectTO> lcupones;
	
	public SecurityConsultationTO() {
		lcupones=new ArrayList<>();
	}

	public String getCserie() {
		return cserie;
	}

	public void setCserie(String cserie) {
		this.cserie = cserie;
	}

	public String getCodInst() {
		return codInst;
	}

	public void setCodInst(String codInst) {
		this.codInst = codInst;
	}

	public String getCodEm() {
		return codEm;
	}

	public void setCodEm(String codEm) {
		this.codEm = codEm;
	}

	public String getMonedaGen() {
		return monedaGen;
	}

	public void setMonedaGen(String monedaGen) {
		this.monedaGen = monedaGen;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getMetodoValuacion() {
		return metodoValuacion;
	}

	public void setMetodoValuacion(String metodoValuacion) {
		this.metodoValuacion = metodoValuacion;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	public Date getFechaVenci() {
		return fechaVenci;
	}

	public void setFechaVenci(Date fechaVenci) {
		this.fechaVenci = fechaVenci;
	}

	public BigDecimal getTrem() {
		return trem;
	}

	public void setTrem(BigDecimal trem) {
		this.trem = trem;
	}

	public BigDecimal gettDesc() {
		return tDesc;
	}

	public void settDesc(BigDecimal tDesc) {
		this.tDesc = tDesc;
	}

	public Long getPlazoAmortiza() {
		return plazoAmortiza;
	}

	public void setPlazoAmortiza(Long plazoAmortiza) {
		this.plazoAmortiza = plazoAmortiza;
	}

	public Long getNroCupones() {
		return nroCupones;
	}

	public void setNroCupones(Long nroCupones) {
		this.nroCupones = nroCupones;
	}

	public BigDecimal getValorEmision() {
		return valorEmision;
	}

	public void setValorEmision(BigDecimal valorEmision) {
		this.valorEmision = valorEmision;
	}

	public BigDecimal getValorNominal() {
		return valorNominal;
	}

	public void setValorNominal(BigDecimal valorNominal) {
		this.valorNominal = valorNominal;
	}

	public String getAgenteRegistrador() {
		return agenteRegistrador;
	}

	public void setAgenteRegistrador(String agenteRegistrador) {
		this.agenteRegistrador = agenteRegistrador;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getDiasRedimidos() {
		return diasRedimidos;
	}

	public void setDiasRedimidos(Long diasRedimidos) {
		this.diasRedimidos = diasRedimidos;
	}

	public Long getTipoGenericoValor() {
		return tipoGenericoValor;
	}

	public void setTipoGenericoValor(Long tipoGenericoValor) {
		this.tipoGenericoValor = tipoGenericoValor;
	}

	public String getlCupNegociable() {
		return lCupNegociable;
	}

	public void setlCupNegociable(String lCupNegociable) {
		this.lCupNegociable = lCupNegociable;
	}

	public String getlFciNeg() {
		return lFciNeg;
	}

	public void setlFciNeg(String lFciNeg) {
		this.lFciNeg = lFciNeg;
	}

	public String getPrepago() {
		return prepago;
	}

	public void setPrepago(String prepago) {
		this.prepago = prepago;
	}

	public String getlSubor() {
		return lSubor;
	}

	public void setlSubor(String lSubor) {
		this.lSubor = lSubor;
	}

	public String getlTasaFija() {
		return lTasaFija;
	}

	public void setlTasaFija(String lTasaFija) {
		this.lTasaFija = lTasaFija;
	}

	public String getTipoBono() {
		return tipoBono;
	}

	public void setTipoBono(String tipoBono) {
		this.tipoBono = tipoBono;
	}

	public String getLtasaDif() {
		return ltasaDif;
	}

	public void setLtasaDif(String ltasaDif) {
		this.ltasaDif = ltasaDif;
	}

	public Long getTir() {
		return tir;
	}

	public void setTir(Long tir) {
		this.tir = tir;
	}

	public List<CouponObjectTO> getLcupones() {
		return lcupones;
	}

	public void setLcupones(List<CouponObjectTO> lcupones) {
		this.lcupones = lcupones;
	}
}
