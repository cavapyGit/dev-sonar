package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SecurityObjectTO extends ValidationOperationType implements Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idSecurityCode;
	private Integer securityClass;
	private String securitySerial;
	private BigDecimal stockQuantity;
	private BigDecimal nominalValue;
	private Date issuanceDate;
	private Date expirationDate;
	private BigDecimal interestRate;
	private Integer calendarDays;
	private Integer interestType; //1526 /1527 por cupon - al vencimiento (tipo)
	private Integer interestPaymentModality; // modalidad
	private Integer numberCoupons;
	private Date couponFirstExpirationDate;
	private Integer periodicity;
	private Integer capitalPaymentModality; // modalidad pago
	private Integer amortizationType; // tipo pago
	private Integer amortizationPeriodicity; //periodicidad pago
	private BigDecimal amortizationFactor;
	private Integer indEarlyRedemption;
	private BigDecimal minimumInvesment;
	private BigDecimal maximumInvesment;
	private Integer indSplitCoupon;
	private Integer indTraderSecondary;
	private Integer indTaxExempt;
	private Integer indSubordinated;
	private Integer indPrepayable;
	private String indPrepayableCode;
	private String instrumentTypeMnemonic;
	private Integer instrumentType;
	private String securityClassDesc;
	private Integer securitySource;
	private String currencyMnemonic;
	private String alternativeCode;
	private String securitySerialPrevious;
	private Integer currencySource;
	private BigDecimal passiveInterestRate;
	
	private Integer indexed;
	private Integer codeVerifier;
	private String descriptionAux;
	
	//otros
	private Integer securityType;
	private Integer indFCI;
	private Integer indExtendedTerm;
	private Integer yield;
	private Integer periodicityDays;
	private Integer amortizationPeriodicityDays;
	private BigDecimal yieldRate;
	private BigDecimal discountRate;
	private String restrictions;
	
	//Issuance
	private String issuerMnemonic;
	private String issuanceDescription;
	private Integer currency;
	private Integer issuanceCreditRatingScales;
	private String issuanceResolutionNumber;
	private Date issuanceResolutionDate;
	private String codeGeographicLocation; 		//Ciudad de emision
	private Integer issuanceRegulatorReport;
	private Integer idGeographicLocation;		//Ciudad de emision
	
	private String paymentLocation;
	private Integer couponNumberRef;
	private List<CouponObjectTO> lstCouponObjectTOs;
	private List<AmortizationObjectTO> lstAmortizationObjectTOs;
	
	private Long operationNumber; //operation number which it was generated
	
	//Stock Affectation
	private String unblockRequirement;
	
	//CUI
	private Long cuiNumber;
	
	// CUENTA
	private Integer accountNumber;
	
	private String notTraded;
	
	// Serie que se esta fraccionando
	private String fractionableSecurity;
	
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public String getSecuritySerial() {
		return securitySerial;
	}
	public void setSecuritySerial(String securitySerial) {
		this.securitySerial = securitySerial;
	}
	public BigDecimal getNominalValue() {
		return nominalValue;
	}
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}
	public Date getIssuanceDate() {
		return issuanceDate;
	}
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public BigDecimal getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}
	public Integer getCalendarDays() {
		return calendarDays;
	}
	public void setCalendarDays(Integer calendarDays) {
		this.calendarDays = calendarDays;
	}
	public Integer getInterestType() {
		return interestType;
	}
	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}
	public Integer getInterestPaymentModality() {
		return interestPaymentModality;
	}
	public void setInterestPaymentModality(Integer interestPaymentModality) {
		this.interestPaymentModality = interestPaymentModality;
	}
	public Integer getNumberCoupons() {
		return numberCoupons;
	}
	public void setNumberCoupons(Integer numberCoupons) {
		this.numberCoupons = numberCoupons;
	}
	public Date getCouponFirstExpirationDate() {
		return couponFirstExpirationDate;
	}
	public void setCouponFirstExpirationDate(Date couponFirstExpirationDate) {
		this.couponFirstExpirationDate = couponFirstExpirationDate;
	}
	public Integer getPeriodicity() {
		return periodicity;
	}
	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
	}
	public Integer getCapitalPaymentModality() {
		return capitalPaymentModality;
	}
	public void setCapitalPaymentModality(Integer capitalPaymentModality) {
		this.capitalPaymentModality = capitalPaymentModality;
	}
	public Integer getAmortizationType() {
		return amortizationType;
	}
	public void setAmortizationType(Integer amortizationType) {
		this.amortizationType = amortizationType;
	}
	public Integer getAmortizationPeriodicity() {
		return amortizationPeriodicity;
	}
	public void setAmortizationPeriodicity(Integer amortizationPeriodicity) {
		this.amortizationPeriodicity = amortizationPeriodicity;
	}
	public BigDecimal getAmortizationFactor() {
		return amortizationFactor;
	}
	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}
	public Integer getIndEarlyRedemption() {
		return indEarlyRedemption;
	}
	public void setIndEarlyRedemption(Integer indEarlyRedemption) {
		this.indEarlyRedemption = indEarlyRedemption;
	}
	public BigDecimal getMinimumInvesment() {
		return minimumInvesment;
	}
	public void setMinimumInvesment(BigDecimal minimumInvesment) {
		this.minimumInvesment = minimumInvesment;
	}
	public BigDecimal getMaximumInvesment() {
		return maximumInvesment;
	}
	public void setMaximumInvesment(BigDecimal maximumInvesment) {
		this.maximumInvesment = maximumInvesment;
	}
	public Integer getIndSplitCoupon() {
		return indSplitCoupon;
	}
	public void setIndSplitCoupon(Integer indSplitCoupon) {
		this.indSplitCoupon = indSplitCoupon;
	}
	public Integer getIndTraderSecondary() {
		return indTraderSecondary;
	}
	public void setIndTraderSecondary(Integer indTraderSecondary) {
		this.indTraderSecondary = indTraderSecondary;
	}
	public Integer getIndTaxExempt() {
		return indTaxExempt;
	}
	public void setIndTaxExempt(Integer indTaxExempt) {
		this.indTaxExempt = indTaxExempt;
	}
	public Integer getIndSubordinated() {
		return indSubordinated;
	}
	public void setIndSubordinated(Integer indSubordinated) {
		this.indSubordinated = indSubordinated;
	}
	public Integer getIndPrepayable() {
		return indPrepayable;
	}
	public void setIndPrepayable(Integer indPrepayable) {
		this.indPrepayable = indPrepayable;
	}
	public List<CouponObjectTO> getLstCouponObjectTOs() {
		return lstCouponObjectTOs;
	}
	public void setLstCouponObjectTOs(List<CouponObjectTO> lstCouponObjectTOs) {
		this.lstCouponObjectTOs = lstCouponObjectTOs;
	}
	public List<AmortizationObjectTO> getLstAmortizationObjectTOs() {
		return lstAmortizationObjectTOs;
	}
	public void setLstAmortizationObjectTOs(
			List<AmortizationObjectTO> lstAmortizationObjectTOs) {
		this.lstAmortizationObjectTOs = lstAmortizationObjectTOs;
	}
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Integer getSecurityType() {
		return securityType;
	}
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}
	public String getCodeGeographicLocation() {
		return codeGeographicLocation;
	}
	public void setCodeGeographicLocation(String codeGeographicLocation) {
		this.codeGeographicLocation = codeGeographicLocation;
	}
	public String getIssuanceDescription() {
		return issuanceDescription;
	}
	public void setIssuanceDescription(String issuanceDescription) {
		this.issuanceDescription = issuanceDescription;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Integer getIssuanceCreditRatingScales() {
		return issuanceCreditRatingScales;
	}
	public void setIssuanceCreditRatingScales(Integer issuanceCreditRatingScales) {
		this.issuanceCreditRatingScales = issuanceCreditRatingScales;
	}
	public String getIssuanceResolutionNumber() {
		return issuanceResolutionNumber;
	}
	public void setIssuanceResolutionNumber(String issuanceResolutionNumber) {
		this.issuanceResolutionNumber = issuanceResolutionNumber;
	}
	public Date getIssuanceResolutionDate() {
		return issuanceResolutionDate;
	}
	public void setIssuanceResolutionDate(Date issuanceResolutionDate) {
		this.issuanceResolutionDate = issuanceResolutionDate;
	}
	public Integer getIssuanceRegulatorReport() {
		return issuanceRegulatorReport;
	}
	public void setIssuanceRegulatorReport(Integer issuanceRegulatorReport) {
		this.issuanceRegulatorReport = issuanceRegulatorReport;
	}
	public Integer getIndFCI() {
		return indFCI;
	}
	public void setIndFCI(Integer indFCI) {
		this.indFCI = indFCI;
	}
	public Integer getIndExtendedTerm() {
		return indExtendedTerm;
	}
	public void setIndExtendedTerm(Integer indExtendedTerm) {
		this.indExtendedTerm = indExtendedTerm;
	}
	public Integer getYield() {
		return yield;
	}
	public void setYield(Integer yield) {
		this.yield = yield;
	}
	public Integer getPeriodicityDays() {
		return periodicityDays;
	}
	public void setPeriodicityDays(Integer periodicityDays) {
		this.periodicityDays = periodicityDays;
	}
	public Integer getAmortizationPeriodicityDays() {
		return amortizationPeriodicityDays;
	}
	public void setAmortizationPeriodicityDays(Integer amortizationPeriodicityDays) {
		this.amortizationPeriodicityDays = amortizationPeriodicityDays;
	}
	public BigDecimal getYieldRate() {
		return yieldRate;
	}
	public void setYieldRate(BigDecimal yieldRate) {
		this.yieldRate = yieldRate;
	}
	public BigDecimal getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public String getSecurityClassDesc() {
		return securityClassDesc;
	}
	public void setSecurityClassDesc(String securityClassDesc) {
		this.securityClassDesc = securityClassDesc;
	}
	public Integer getSecuritySource() {
		return securitySource;
	}
	public void setSecuritySource(Integer securitySource) {
		this.securitySource = securitySource;
	}
	
	public String getInstrumentTypeMnemonic() {
		return instrumentTypeMnemonic;
	}
	
	public void setInstrumentTypeMnemonic(String instrumentTypeMnemonic) {
		this.instrumentTypeMnemonic = instrumentTypeMnemonic;
	}
	
	public String getCurrencyMnemonic() {
		return currencyMnemonic;
	}
	
	public void setCurrencyMnemonic(String currencyMnemonic) {
		this.currencyMnemonic = currencyMnemonic;
	}
	public Integer getIndexed() {
		return indexed;
	}
	public void setIndexed(Integer indexed) {
		this.indexed = indexed;
	}
	
	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}
	
	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}
	public Integer getCodeVerifier() {
		return codeVerifier;
	}
	public void setCodeVerifier(Integer codeVerifier) {
		this.codeVerifier = codeVerifier;
	}
	public String getAlternativeCode() {
		return alternativeCode;
	}
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	public String getPaymentLocation() {
		return paymentLocation;
	}
	public void setPaymentLocation(String paymentLocation) {
		this.paymentLocation = paymentLocation;
	}
	public String getDescriptionAux() {
		return descriptionAux;
	}
	public void setDescriptionAux(String descriptionAux) {
		this.descriptionAux = descriptionAux;
	}
	public String getSecuritySerialPrevious() {
		return securitySerialPrevious;
	}
	public void setSecuritySerialPrevious(String securitySerialPrevious) {
		this.securitySerialPrevious = securitySerialPrevious;
	}
	public Integer getCouponNumberRef() {
		return couponNumberRef;
	}
	public void setCouponNumberRef(Integer couponNumberRef) {
		this.couponNumberRef = couponNumberRef;
	}
	
	/**
	 * @return the idGeographicLocation
	 */
	public Integer getIdGeographicLocation() {
		return idGeographicLocation;
	}
	
	/**
	 * @param idGeographicLocation the idGeographicLocation to set
	 */
	public void setIdGeographicLocation(Integer idGeographicLocation) {
		this.idGeographicLocation = idGeographicLocation;
	}
	public Long getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	public String getIndPrepayableCode() {
		return indPrepayableCode;
	}
	public void setIndPrepayableCode(String indPrepayableCode) {
		this.indPrepayableCode = indPrepayableCode;
	}
	
	/**
	 * @return the unblockRequirement
	 */
	public String getUnblockRequirement() {
		return unblockRequirement;
	}
	/**
	 * @param unblockRequirement the unblockRequirement to set
	 */
	public void setUnblockRequirement(String unblockRequirement) {
		this.unblockRequirement = unblockRequirement;
	}

	public SecurityObjectTO clone() throws CloneNotSupportedException {
        return (SecurityObjectTO) super.clone();
    }
	/**
	 * @return the currencySource
	 */
	public Integer getCurrencySource() {
		return currencySource;
	}
	/**
	 * @param currencySource the currencySource to set
	 */
	public void setCurrencySource(Integer currencySource) {
		this.currencySource = currencySource;
	}
	/**
	 * @return the passiveInterestRate
	 */
	public BigDecimal getPassiveInterestRate() {
		return passiveInterestRate;
	}
	/**
	 * @param passiveInterestRate the passiveInterestRate to set
	 */
	public void setPassiveInterestRate(BigDecimal passiveInterestRate) {
		this.passiveInterestRate = passiveInterestRate;
	}
	/**
	 * @return the cuiNumber
	 */
	public Long getCuiNumber() {
		return cuiNumber;
	}
	/**
	 * @param cuiNumber the cuiNumber to set
	 */
	public void setCuiNumber(Long cuiNumber) {
		this.cuiNumber = cuiNumber;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the restrictions
	 */
	public String getRestrictions() {
		return restrictions;
	}
	/**
	 * @param restrictions the restrictions to set
	 */
	public void setRestrictions(String restrictions) {
		this.restrictions = restrictions;
	}
	/**
	 * @return the notTraded
	 */
	public String getNotTraded() {
		return notTraded;
	}
	/**
	 * @param notTraded the notTraded to set
	 */
	public void setNotTraded(String notTraded) {
		this.notTraded = notTraded;
	}
	public String getFractionableSecurity() {
		return fractionableSecurity;
	}
	public void setFractionableSecurity(String fractionableSecurity) {
		this.fractionableSecurity = fractionableSecurity;
	}
	
}
