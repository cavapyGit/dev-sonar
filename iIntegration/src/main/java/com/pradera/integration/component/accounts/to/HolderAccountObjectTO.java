package com.pradera.integration.component.accounts.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountObjectTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/09/2015
 */
public class HolderAccountObjectTO extends ValidationOperationType implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant INDIVIDUAL_ACCOUNT_TYPE. */
	public static final String INDIVIDUAL_ACCOUNT_TYPE = "IND";
	
	/** The Constant OWNERSHIP_ACCOUNT_TYPE. */
	public static final String OWNERSHIP_ACCOUNT_TYPE = "MAN";
	
	/** The create object. */
	private boolean createObject;
	
	/** The id holder account. */
	private Long idHolderAccount;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The account group. */
	private Integer accountGroup;
	
	/** The account type. */
	private Integer accountType; // parameter table
	
	/** The account type code. */
	private String accountTypeCode; // Ind(individual) or Man(ownership)
	
	/** The holder type. */
	private Integer holderType;  // from parameter table
	
	/** The holder type alt. */
	private Integer holderTypeAlt; // 1 natural 2 juridic (used for dpf)
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The description. */
	private String description;
	
	/** The conditions. */
	private String conditions;
	
	/** The validate account. */
	private boolean validateAccount;		
	
	/** The lst natural holder object t os. */
	List<NaturalHolderObjectTO> lstNaturalHolderObjectTOs;
	
	/** The juridic holder object to. */
	JuridicHolderObjectTO juridicHolderObjectTO;
	
	/** The lst legal representative object t os. */
	List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjectTOs;
	
	List<BankAccountObjectTO> lstBankAccountObjectTOs;
	
	/** The operation number. */
	private Long operationNumber; //operation number which it was created 
	
	/** The security object to. */
	private SecurityObjectTO securityObjectTO; //holder account balance query
	
	/** The block code. */
	private String blockCode; //holder account balance query
	
	/** The ind reported. */
	private String indReported; //holder account balance query
	
	/** The lst id holder pk. */
	private List<Long> lstIdHolderPk;
	
	
	/** Cui Query */
	
	private String participantMnemonic; 			//CUENTA_MATRIZ_MNEMONICO
	private Long idParticipantPk; 					//CUENTA_MATRIZ
	private String accountTypeDescription; 			//TIPO_CUENTA
	private String holderAccountStateDescription; 	//ESTADO_CUENTA
	private Date holderAccountRegistryDate;			//FECHA_REGISTRO_CUENTA
	private String holderTypeDescription; 			//TIPO_TITULAR
	
	private Integer stockCode;
	
	private Integer rowNumber;
	
	private String relatedName;
	private Integer indPdd;
	/**
	 * Instantiates a new holder account object to.
	 */
	public HolderAccountObjectTO() {
		super();
	}
	
	/**
	 * Sets the children indicators.
	 */
	public void setChildrenIndicators(){
		if(Validations.validateListIsNotNullAndNotEmpty(lstNaturalHolderObjectTOs)){
			for (NaturalHolderObjectTO naturalHolderObjectTO : lstNaturalHolderObjectTOs) {
				naturalHolderObjectTO.setDpfInterface(isDpfInterface());
			}
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeObjectTOs)){
			for (LegalRepresentativeObjectTO legalRepresentativeObjectTO : lstLegalRepresentativeObjectTOs) {
				legalRepresentativeObjectTO.setDpfInterface(isDpfInterface());
			}
		}
		if(Validations.validateIsNotNull(juridicHolderObjectTO)){
			juridicHolderObjectTO.setDpfInterface(isDpfInterface());
		}
	}

	/**
	 * Gets the id holder account.
	 *
	 * @return the id holder account
	 */
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}

	/**
	 * Sets the id holder account.
	 *
	 * @param idHolderAccount the new id holder account
	 */
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the account group.
	 *
	 * @return the account group
	 */
	public Integer getAccountGroup() {
		return accountGroup;
	}

	/**
	 * Sets the account group.
	 *
	 * @param accountGroup the new account group
	 */
	public void setAccountGroup(Integer accountGroup) {
		this.accountGroup = accountGroup;
	}

	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public Integer getAccountType() {
		return accountType;
	}

	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	/**
	 * Gets the alternate code.
	 *
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return alternateCode;
	}

	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the lst natural holder object t os.
	 *
	 * @return the lst natural holder object t os
	 */
	public List<NaturalHolderObjectTO> getLstNaturalHolderObjectTOs() {
		return lstNaturalHolderObjectTOs;
	}

	/**
	 * Sets the lst natural holder object t os.
	 *
	 * @param lstNaturalHolderObjectTOs the new lst natural holder object t os
	 */
	public void setLstNaturalHolderObjectTOs(
			List<NaturalHolderObjectTO> lstNaturalHolderObjectTOs) {
		this.lstNaturalHolderObjectTOs = lstNaturalHolderObjectTOs;
	}

	/**
	 * Gets the juridic holder object to.
	 *
	 * @return the juridic holder object to
	 */
	public JuridicHolderObjectTO getJuridicHolderObjectTO() {
		return juridicHolderObjectTO;
	}

	/**
	 * Sets the juridic holder object to.
	 *
	 * @param juridicHolderObjectTO the new juridic holder object to
	 */
	public void setJuridicHolderObjectTO(JuridicHolderObjectTO juridicHolderObjectTO) {
		this.juridicHolderObjectTO = juridicHolderObjectTO;
	}

	/**
	 * Gets the lst legal representative object t os.
	 *
	 * @return the lst legal representative object t os
	 */
	public List<LegalRepresentativeObjectTO> getLstLegalRepresentativeObjectTOs() {
		return lstLegalRepresentativeObjectTOs;
	}

	/**
	 * Sets the lst legal representative object t os.
	 *
	 * @param lstLegalRepresentativeObjectTOs the new lst legal representative object t os
	 */
	public void setLstLegalRepresentativeObjectTOs(
			List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjectTOs) {
		this.lstLegalRepresentativeObjectTOs = lstLegalRepresentativeObjectTOs;
	}

	/**
	 * Gets the holder type.
	 *
	 * @return the holder type
	 */
	public Integer getHolderType() {
		return holderType;
	}

	/**
	 * Sets the holder type.
	 *
	 * @param holderType the new holder type
	 */
	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}

	/**
	 * Checks if is creates the object.
	 *
	 * @return true, if is creates the object
	 */
	public boolean isCreateObject() {
		return createObject;
	}

	/**
	 * Sets the creates the object.
	 *
	 * @param createObject the new creates the object
	 */
	public void setCreateObject(boolean createObject) {
		this.createObject = createObject;
	}

	/**
	 * Gets the account type code.
	 *
	 * @return the account type code
	 */
	public String getAccountTypeCode() {
		return accountTypeCode;
	}

	/**
	 * Sets the account type code.
	 *
	 * @param accountTypeCode the new account type code
	 */
	public void setAccountTypeCode(String accountTypeCode) {
		this.accountTypeCode = accountTypeCode;
	}

	/**
	 * Gets the holder type alt.
	 *
	 * @return the holder type alt
	 */
	public Integer getHolderTypeAlt() {
		return holderTypeAlt;
	}

	/**
	 * Sets the holder type alt.
	 *
	 * @param holderTypeAlt the new holder type alt
	 */
	public void setHolderTypeAlt(Integer holderTypeAlt) {
		this.holderTypeAlt = holderTypeAlt;
	}

	/**
	 * Gets the security object to.
	 *
	 * @return the security object to
	 */
	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	/**
	 * Sets the security object to.
	 *
	 * @param securityObjectTO the new security object to
	 */
	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}

	/**
	 * Gets the block code.
	 *
	 * @return the block code
	 */
	public String getBlockCode() {
		return blockCode;
	}

	/**
	 * Sets the block code.
	 *
	 * @param blockCode the new block code
	 */
	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}

	/**
	 * Gets the ind reported.
	 *
	 * @return the ind reported
	 */
	public String getIndReported() {
		return indReported;
	}

	/**
	 * Sets the ind reported.
	 *
	 * @param indReported the new ind reported
	 */
	public void setIndReported(String indReported) {
		this.indReported = indReported;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the conditions.
	 *
	 * @return the conditions
	 */
	public String getConditions() {
		return conditions;
	}

	/**
	 * Sets the conditions.
	 *
	 * @param conditions the new conditions
	 */
	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	/**
	 * Checks if is validate account.
	 *
	 * @return true, if is validate account
	 */
	public boolean isValidateAccount() {
		return validateAccount;
	}

	/**
	 * Sets the validate account.
	 *
	 * @param validateAccount the new validate account
	 */
	public void setValidateAccount(boolean validateAccount) {
		this.validateAccount = validateAccount;
	}

	/**
	 * Gets the lst id holder pk.
	 *
	 * @return the lstIdHolderPk
	 */
	public List<Long> getLstIdHolderPk() {
		return lstIdHolderPk;
	}

	/**
	 * Sets the lst id holder pk.
	 *
	 * @param lstIdHolderPk the lstIdHolderPk to set
	 */
	public void setLstIdHolderPk(List<Long> lstIdHolderPk) {
		this.lstIdHolderPk = lstIdHolderPk;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public String getParticipantMnemonic() {
		return participantMnemonic;
	}

	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}

	public String getAccountTypeDescription() {
		return accountTypeDescription;
	}

	public void setAccountTypeDescription(String accountTypeDescription) {
		this.accountTypeDescription = accountTypeDescription;
	}

	public String getHolderAccountStateDescription() {
		return holderAccountStateDescription;
	}

	public void setHolderAccountStateDescription(
			String holderAccountStateDescription) {
		this.holderAccountStateDescription = holderAccountStateDescription;
	}

	public Date getHolderAccountRegistryDate() {
		return holderAccountRegistryDate;
	}

	public void setHolderAccountRegistryDate(Date holderAccountRegistryDate) {
		this.holderAccountRegistryDate = holderAccountRegistryDate;
	}

	public String getHolderTypeDescription() {
		return holderTypeDescription;
	}

	public void setHolderTypeDescription(String holderTypeDescription) {
		this.holderTypeDescription = holderTypeDescription;
	}

	public List<BankAccountObjectTO> getLstBankAccountObjectTOs() {
		return lstBankAccountObjectTOs;
	}

	public void setLstBankAccountObjectTOs(List<BankAccountObjectTO> lstBankAccountObjectTOs) {
		this.lstBankAccountObjectTOs = lstBankAccountObjectTOs;
	}

	public Integer getStockCode() {
		return stockCode;
	}

	public void setStockCode(Integer stockCode) {
		this.stockCode = stockCode;
	}

	public Integer getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}

	public String getRelatedName() {
		return relatedName;
	}

	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	public Integer getIndPdd() {
		return indPdd;
	}

	public void setIndPdd(Integer indPdd) {
		this.indPdd = indPdd;
	}
	
	
	
}
