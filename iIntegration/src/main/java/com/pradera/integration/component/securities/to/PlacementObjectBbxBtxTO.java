package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;

public class PlacementObjectBbxBtxTO extends ValidationOperationType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	List<NaturalHolderObjectTO> listAccountObjectBbXBtxTO;
	List<HolderAccountObjectBbXBtxTO> listAccountObjectBbXBtxTO;
	AmountLocationObjectBbxBtxTO amountLocationObjectBbxBtxTO;
	
	public List<HolderAccountObjectBbXBtxTO> getListAccountObjectBbXBtxTO() {
		return listAccountObjectBbXBtxTO;
	}
	public void setListAccountObjectBbXBtxTO(
			List<HolderAccountObjectBbXBtxTO> listAccountObjectBbXBtxTO) {
		this.listAccountObjectBbXBtxTO = listAccountObjectBbXBtxTO;
	}
	public AmountLocationObjectBbxBtxTO getAmountLocationObjectBbxBtxTO() {
		return amountLocationObjectBbxBtxTO;
	}
	public void setAmountLocationObjectBbxBtxTO(
			AmountLocationObjectBbxBtxTO amountLocationObjectBbxBtxTO) {
		this.amountLocationObjectBbxBtxTO = amountLocationObjectBbxBtxTO;
	}
	
}
