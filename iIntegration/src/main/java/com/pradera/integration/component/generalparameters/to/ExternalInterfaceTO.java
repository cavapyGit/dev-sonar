package com.pradera.integration.component.generalparameters.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class ExternalInterfaceTO extends ValidationOperationType implements Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idExtInterface;
	
	private String description;
	
	private String interfaceName;
	
	private Integer extInterState;
	
	private String stateDescription;

	/**
	 * @return the idExtInterface
	 */
	public Long getIdExtInterface() {
		return idExtInterface;
	}

	/**
	 * @param idExtInterface the idExtInterface to set
	 */
	public void setIdExtInterface(Long idExtInterface) {
		this.idExtInterface = idExtInterface;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the interfaceName
	 */
	public String getInterfaceName() {
		return interfaceName;
	}

	/**
	 * @param interfaceName the interfaceName to set
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	/**
	 * @return the extInterState
	 */
	public Integer getExtInterState() {
		return extInterState;
	}

	/**
	 * @param extInterState the extInterState to set
	 */
	public void setExtInterState(Integer extInterState) {
		this.extInterState = extInterState;
	}

	/**
	 * @return the stateDescription
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * @param stateDescription the stateDescription to set
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	

}
