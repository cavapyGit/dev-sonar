package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class BlockDocumentTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String 		action;
	private String 		instrument;
	private String 		serie;
	private String 		serieAlter;
	private BigDecimal 	cui;
	private String 		documentNumber;
	private String 		documentDate;
	private String 		blockCode;
	private BigDecimal 	documentRequestPk;
	private BigDecimal 	stockQuantity;
	private String 		holderName;
	
	
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the instrument
	 */
	public String getInstrument() {
		return instrument;
	}
	/**
	 * @param instrument the instrument to set
	 */
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}
	/**
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}
	/**
	 * @return the serieAlter
	 */
	public String getSerieAlter() {
		return serieAlter;
	}
	/**
	 * @param serieAlter the serieAlter to set
	 */
	public void setSerieAlter(String serieAlter) {
		this.serieAlter = serieAlter;
	}
	/**
	 * @return the cui
	 */
	public BigDecimal getCui() {
		return cui;
	}
	/**
	 * @param cui the cui to set
	 */
	public void setCui(BigDecimal cui) {
		this.cui = cui;
	}
	/**
	 * @return the documentNumber
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	/**
	 * @param documentNumber the documentNumber to set
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	/**
	 * @return the documentDate
	 */
	public String getDocumentDate() {
		return documentDate;
	}
	/**
	 * @param documentDate the documentDate to set
	 */
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	/**
	 * @return the blockCode
	 */
	public String getBlockCode() {
		return blockCode;
	}
	/**
	 * @param blockCode the blockCode to set
	 */
	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}
	/**
	 * @return the documentRequestPk
	 */
	public BigDecimal getDocumentRequestPk() {
		return documentRequestPk;
	}
	/**
	 * @param documentRequestPk the documentRequestPk to set
	 */
	public void setDocumentRequestPk(BigDecimal documentRequestPk) {
		this.documentRequestPk = documentRequestPk;
	}
	/**
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	/**
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}
	/**
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	
	
	
	
}