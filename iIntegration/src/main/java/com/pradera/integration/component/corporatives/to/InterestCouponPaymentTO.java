package com.pradera.integration.component.corporatives.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class InterestCouponPaymentTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<CorporateInformationTO> lstCorporateInformationTO;

	
	public InterestCouponPaymentTO() {
		super();
	}

	public List<CorporateInformationTO> getLstCorporateInformationTO() {
		return lstCorporateInformationTO;
	}

	public void setLstCorporateInformationTO(
			List<CorporateInformationTO> lstCorporateInformationTO) {
		this.lstCorporateInformationTO = lstCorporateInformationTO;
	}
	
	
}
