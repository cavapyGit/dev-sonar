package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class QueryParametersConsultCdpf extends OperationInterfaceTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codInst;
	private String cSerie;
	private String codEm;
	private Date fechaEmi;
	private List<SecurityConsultationTO> consultedSecurities;
	private boolean error;
	public QueryParametersConsultCdpf() {
		super();
		consultedSecurities=new ArrayList<>();
		error=false;
	}
	
	public QueryParametersConsultCdpf(QueryParametersConsultCdpf queryParametersConsultCdpf) {
		super();
		this.codInst = queryParametersConsultCdpf.getCodInst();
		this.cSerie = queryParametersConsultCdpf.getcSerie();
		this.codEm = queryParametersConsultCdpf.getCodEm();
		this.fechaEmi =queryParametersConsultCdpf.getFechaEmi();
		consultedSecurities=new ArrayList<>();
	}
	public String getCodInst() {
		return codInst;
	}
	public void setCodInst(String codInst) {
		this.codInst = codInst;
	}
	public String getcSerie() {
		return cSerie;
	}
	public void setcSerie(String cSerie) {
		this.cSerie = cSerie;
	}
	public String getCodEm() {
		return codEm;
	}
	public void setCodEm(String codEm) {
		this.codEm = codEm;
	}
	public Date getFechaEmi() {
		return fechaEmi;
	}
	public void setFechaEmi(Date fechaEmi) {
		this.fechaEmi = fechaEmi;
	}
	public List<SecurityConsultationTO> getConsultedSecurities() {
		return consultedSecurities;
	}

	public void setConsultedSecurities(
			List<SecurityConsultationTO> consultedSecurities) {
		this.consultedSecurities = consultedSecurities;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	@Override
	public String toString(){
		return "\nCOD:"+codInst+"\nSERIE"+cSerie+"\nCOD-EM:"+codEm+"\nFECHA:"+fechaEmi;
	}
}
