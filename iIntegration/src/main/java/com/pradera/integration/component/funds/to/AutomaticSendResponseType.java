package com.pradera.integration.component.funds.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class AutomaticSendResponseType extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6981880515024444209L;
	private String requestNumber;
	private String responseCode;
	private String description;
	private String messageReceive;
	
	
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMessageReceive() {
		return messageReceive;
	}
	public void setMessageReceive(String messageReceive) {
		this.messageReceive = messageReceive;
	}

	
	
}
