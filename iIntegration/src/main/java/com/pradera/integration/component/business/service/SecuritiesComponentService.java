package com.pradera.integration.component.business.service;

import javax.ejb.Local;

import com.pradera.integration.component.business.to.PrimaryPlacementComponentTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface SecuritiesComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/03/2014
 */
@Local
public interface SecuritiesComponentService {

	/**
	 * Execute securities component.
	 *
	 * @param objSecuritiesComponentTO the obj securities component to
	 * @throws ServiceException the service exception
	 */
	public void executeSecuritiesComponent(SecuritiesComponentTO objSecuritiesComponentTO) throws ServiceException;

	public void updatePrimaryPlacement(PrimaryPlacementComponentTO primaryPlacementComponentTO) throws ServiceException;
}
