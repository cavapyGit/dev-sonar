package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

/**
 * 
 * Respuesta de la consulta de titulos fisicos
 * para el flujo de bovedilla
 * Issue 1109
 * 
 * @author jquino
 *
 */
public class PhysicalCertificateResponseTO extends OperationInterfaceTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	String requestType;
	String motive;
	String userRequest;
	String ipRequest;
	List<PhysicalCertificateTO> listPhysicalCertificate;
	Long idInterfaceProcess;

	public PhysicalCertificateResponseTO() {
		super();
	}

	public List<PhysicalCertificateTO> getListPhysicalCertificate() {
		return listPhysicalCertificate;
	}

	public void setListPhysicarCertificate(
			List<PhysicalCertificateTO> listPhysicalCertificate) {
		this.listPhysicalCertificate = listPhysicalCertificate;
	}

	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}

	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}

	public void setListPhysicalCertificate(
			List<PhysicalCertificateTO> listPhysicalCertificate) {
		this.listPhysicalCertificate = listPhysicalCertificate;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

	public String getUserRequest() {
		return userRequest;
	}

	public void setUserRequest(String userRequest) {
		this.userRequest = userRequest;
	}

	public String getIpRequest() {
		return ipRequest;
	}

	public void setIpRequest(String ipRequest) {
		this.ipRequest = ipRequest;
	}
	
}
