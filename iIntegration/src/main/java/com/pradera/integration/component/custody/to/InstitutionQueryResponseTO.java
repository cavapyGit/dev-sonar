package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;

public class InstitutionQueryResponseTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;

	List<JuridicHolderObjectTO> lstJuridicHolderObjectTOs;
	
	private Long idInterfaceProcess;
	
	public InstitutionQueryResponseTO() {
		super();
	}
	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}
	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}
	public List<JuridicHolderObjectTO> getLstJuridicHolderObjectTOs() {
		return lstJuridicHolderObjectTOs;
	}
	public void setLstJuridicHolderObjectTOs(List<JuridicHolderObjectTO> lstJuridicHolderObjectTOs) {
		this.lstJuridicHolderObjectTOs = lstJuridicHolderObjectTOs;
	}
}
