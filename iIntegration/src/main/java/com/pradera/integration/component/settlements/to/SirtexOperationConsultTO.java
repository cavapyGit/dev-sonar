package com.pradera.integration.component.settlements.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SirtexOperationConsultTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer queryNumber;
	private String operationType;
	private Date operationDate;
	private String participant;
	private Integer modality;
	private Integer dateType;
	private Integer state;
	private Date initialDate;
	private Date endDate;
	private Long idInterfaceProcess;
	
	public SirtexOperationConsultTO() {
		super();
	}

	public Integer getQueryNumber() {
		return queryNumber;
	}

	public void setQueryNumber(Integer queryNumber) {
		this.queryNumber = queryNumber;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getParticipant() {
		return participant;
	}

	public void setParticipant(String participant) {
		this.participant = participant;
	}

	public Integer getModality() {
		return modality;
	}

	public void setModality(Integer modality) {
		this.modality = modality;
	}

	public Integer getDateType() {
		return dateType;
	}

	public void setDateType(Integer dateType) {
		this.dateType = dateType;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}

	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}
	
	
	
	
}
