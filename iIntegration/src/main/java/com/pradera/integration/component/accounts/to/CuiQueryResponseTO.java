package com.pradera.integration.component.accounts.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;

public class CuiQueryResponseTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer queryNumber;
	private String operationType;
	private Date operationDates;
	
	//Lista de titulares Naturales
	List<NaturalHolderObjectTO> lstNaturalHolderObjectTOs;
	
	//Lista de titulares Juridicos
	
	List<JuridicHolderObjectTO> lstJuridicHolderObjectTOs;
	
	private Long idInterfaceProcess;
	
	
	public CuiQueryResponseTO() {
		super();
	}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}
	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}
	public Integer getQueryNumber() {
		return queryNumber;
	}
	public void setQueryNumber(Integer queryNumber) {
		this.queryNumber = queryNumber;
	}
	public Date getOperationDates() {
		return operationDates;
	}
	public void setOperationDates(Date operationDates) {
		this.operationDates = operationDates;
	}
	public List<NaturalHolderObjectTO> getLstNaturalHolderObjectTOs() {
		return lstNaturalHolderObjectTOs;
	}
	public void setLstNaturalHolderObjectTOs(
			List<NaturalHolderObjectTO> lstNaturalHolderObjectTOs) {
		this.lstNaturalHolderObjectTOs = lstNaturalHolderObjectTOs;
	}
	public List<JuridicHolderObjectTO> getLstJuridicHolderObjectTOs() {
		return lstJuridicHolderObjectTOs;
	}
	public void setLstJuridicHolderObjectTOs(
			List<JuridicHolderObjectTO> lstJuridicHolderObjectTOs) {
		this.lstJuridicHolderObjectTOs = lstJuridicHolderObjectTOs;
	}
}
