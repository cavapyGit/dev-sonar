package com.pradera.integration.component.generalparameters.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.component.business.to.OperationQueryTO;

public class ServiceStatusQueryTO extends OperationQueryTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ExternalInterfaceTO> lsExternalInterfaceTO;

	public ServiceStatusQueryTO() {
		super();
	}
	
	public ServiceStatusQueryTO(OperationQueryTO objOperationQueryTO) {
		setOperationCode(objOperationQueryTO.getOperationCode());
		setEntityNemonic(objOperationQueryTO.getEntityNemonic());
		setOperationDate(objOperationQueryTO.getOperationDate());
		setIdInterfaceProcess(objOperationQueryTO.getIdInterfaceProcess());
	}

	/**
	 * @return the lsExternalInterfaceTO
	 */
	public List<ExternalInterfaceTO> getLsExternalInterfaceTO() {
		return lsExternalInterfaceTO;
	}

	/**
	 * @param lsExternalInterfaceTO the lsExternalInterfaceTO to set
	 */
	public void setLsExternalInterfaceTO(
			List<ExternalInterfaceTO> lsExternalInterfaceTO) {
		this.lsExternalInterfaceTO = lsExternalInterfaceTO;
	}
	
	
}
