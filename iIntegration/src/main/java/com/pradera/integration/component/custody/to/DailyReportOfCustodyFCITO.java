package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class DailyReportOfCustodyFCITO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String 	claseValor;					//Clase de Valor
	private String  claveValor;					//Clave de Valor (Pizarra)
	private String 	tasaRelevanteValoracion;	//Tasa Relevante de Valoración
	private String 	precioNominalMonedaOriginal;//Precio Nominal en Moneda Original
	private String 	precioNominalBolivianos;	//Precio Nominal en Bolivianos
	private String 	precioMercadoMonedaOriginal;//Precio Mercado en Moneda Original
	private String 	precioMercadoBolivianos;	//Precio Mercado en Bolivianos
	private String	cantidadTotalValores;		//Cantidad total de Valores
	private String 	montoTotalMonedaOriginal;	//Monto Total en Moneda Original
	private String 	codigoCustodia;				//Código de Custodia
	
	/**
	 * @return the claseValor
	 */
	public String getClaseValor() {
		return claseValor;
	}
	/**
	 * @param claseValor the claseValor to set
	 */
	public void setClaseValor(String claseValor) {
		this.claseValor = claseValor;
	}
	/**
	 * @return the claveValor
	 */
	public String getClaveValor() {
		return claveValor;
	}
	/**
	 * @param claveValor the claveValor to set
	 */
	public void setClaveValor(String claveValor) {
		this.claveValor = claveValor;
	}
	/**
	 * @return the tasaRelevanteValoracion
	 */
	public String getTasaRelevanteValoracion() {
		return tasaRelevanteValoracion;
	}
	/**
	 * @param tasaRelevanteValoracion the tasaRelevanteValoracion to set
	 */
	public void setTasaRelevanteValoracion(String tasaRelevanteValoracion) {
		this.tasaRelevanteValoracion = tasaRelevanteValoracion;
	}
	/**
	 * @return the precioNominalMonedaOriginal
	 */
	public String getPrecioNominalMonedaOriginal() {
		return precioNominalMonedaOriginal;
	}
	/**
	 * @param precioNominalMonedaOriginal the precioNominalMonedaOriginal to set
	 */
	public void setPrecioNominalMonedaOriginal(String precioNominalMonedaOriginal) {
		this.precioNominalMonedaOriginal = precioNominalMonedaOriginal;
	}
	/**
	 * @return the precioNominalBolivianos
	 */
	public String getPrecioNominalBolivianos() {
		return precioNominalBolivianos;
	}
	/**
	 * @param precioNominalBolivianos the precioNominalBolivianos to set
	 */
	public void setPrecioNominalBolivianos(String precioNominalBolivianos) {
		this.precioNominalBolivianos = precioNominalBolivianos;
	}
	/**
	 * @return the precioMercadoMonedaOriginal
	 */
	public String getPrecioMercadoMonedaOriginal() {
		return precioMercadoMonedaOriginal;
	}
	/**
	 * @param precioMercadoMonedaOriginal the precioMercadoMonedaOriginal to set
	 */
	public void setPrecioMercadoMonedaOriginal(String precioMercadoMonedaOriginal) {
		this.precioMercadoMonedaOriginal = precioMercadoMonedaOriginal;
	}
	/**
	 * @return the precioMercadoBolivianos
	 */
	public String getPrecioMercadoBolivianos() {
		return precioMercadoBolivianos;
	}
	/**
	 * @param precioMercadoBolivianos the precioMercadoBolivianos to set
	 */
	public void setPrecioMercadoBolivianos(String precioMercadoBolivianos) {
		this.precioMercadoBolivianos = precioMercadoBolivianos;
	}
	/**
	 * @return the cantidadTotalValores
	 */
	public String getCantidadTotalValores() {
		return cantidadTotalValores;
	}
	/**
	 * @param cantidadTotalValores the cantidadTotalValores to set
	 */
	public void setCantidadTotalValores(String cantidadTotalValores) {
		this.cantidadTotalValores = cantidadTotalValores;
	}
	/**
	 * @return the montoTotalMonedaOriginal
	 */
	public String getMontoTotalMonedaOriginal() {
		return montoTotalMonedaOriginal;
	}
	/**
	 * @param montoTotalMonedaOriginal the montoTotalMonedaOriginal to set
	 */
	public void setMontoTotalMonedaOriginal(String montoTotalMonedaOriginal) {
		this.montoTotalMonedaOriginal = montoTotalMonedaOriginal;
	}
	/**
	 * @return the codigoCustodia
	 */
	public String getCodigoCustodia() {
		return codigoCustodia;
	}
	/**
	 * @param codigoCustodia the codigoCustodia to set
	 */
	public void setCodigoCustodia(String codigoCustodia) {
		this.codigoCustodia = codigoCustodia;
	}
	
}
