package com.pradera.integration.component.custody.to;

import java.io.Serializable;

public class LegalInformationObjetTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer courtCode;
	private String jobNumber;
	private String record;
	private String applicant;
	private String motive;
	private String observations;
	
	/**
	 * @return the courtCode
	 */
	public Integer getCourtCode() {
		return courtCode;
	}
	/**
	 * @param courtCode the courtCode to set
	 */
	public void setCourtCode(Integer courtCode) {
		this.courtCode = courtCode;
	}
	/**
	 * @return the jobNumber
	 */
	public String getJobNumber() {
		return jobNumber;
	}
	/**
	 * @param jobNumber the jobNumber to set
	 */
	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}
	/**
	 * @return the record
	 */
	public String getRecord() {
		return record;
	}
	/**
	 * @param record the record to set
	 */
	public void setRecord(String record) {
		this.record = record;
	}
	/**
	 * @return the applicant
	 */
	public String getApplicant() {
		return applicant;
	}
	/**
	 * @param applicant the applicant to set
	 */
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}
	/**
	 * @return the motive
	 */
	public String getMotive() {
		return motive;
	}
	/**
	 * @param motive the motive to set
	 */
	public void setMotive(String motive) {
		this.motive = motive;
	}
	/**
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}
	/**
	 * @param observations the observations to set
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}

}
