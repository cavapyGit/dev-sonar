package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class SecondaryAccountEntryRegisterTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String transferType;
	private SecurityObjectTO securityObjectTO;
	private HolderAccountObjectTO holderAccountObjectTO;
	/**
	 * @return the transferType
	 */
	public String getTransferType() {
		return transferType;
	}
	/**
	 * @param transferType the transferType to set
	 */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	/**
	 * @return the securityObjectTO
	 */
	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}
	/**
	 * @param securityObjectTO the securityObjectTO to set
	 */
	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}
	/**
	 * @return the holderAccountObjectTO
	 */
	public HolderAccountObjectTO getHolderAccountObjectTO() {
		return holderAccountObjectTO;
	}
	/**
	 * @param holderAccountObjectTO the holderAccountObjectTO to set
	 */
	public void setHolderAccountObjectTO(HolderAccountObjectTO holderAccountObjectTO) {
		this.holderAccountObjectTO = holderAccountObjectTO;
	}
	
	
	
	
	
	
	
	
	

}
