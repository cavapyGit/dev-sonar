package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class SubProductIssuanceWithCouponTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String 	cCustodio;//CUSTODIO
	private String 	fInformacion;//FECHA INFORMACION
	private String 	cTipoInstrumento;//TIPO INSTRUMENTO
	private String  tClaveInstrumento;//CLAVE INSTRUMENTO

	private String 	cCupDesprendidos;//CUPONES DESPRENDIDOS
	private String 	cCupVigentes;//CUPONES VIGENTES
	
	
	/**
	 * @return the cCustodio
	 */
	public String getcCustodio() {

		return cCustodio;
	}
	/**
	 * @param cCustodio the cCustodio to set
	 */
	public void setcCustodio(String cCustodio) {
		this.cCustodio = cCustodio;
	}
	/**
	 * @return the fInformacion
	 */
	public String getfInformacion() {

		return fInformacion;
	}
	/**
	 * @param fInformacion the fInformacion to set
	 */
	public void setfInformacion(String fInformacion) {
		this.fInformacion = fInformacion;
	}
	/**
	 * @return the cTipoInstrumento
	 */
	public String getcTipoInstrumento() {

		return cTipoInstrumento;
	}
	/**
	 * @param cTipoInstrumento the cTipoInstrumento to set
	 */
	public void setcTipoInstrumento(String cTipoInstrumento) {
		this.cTipoInstrumento = cTipoInstrumento;
	}
	/**
	 * @return the tClaveInstrumento
	 */
	public String gettClaveInstrumento() {

		return tClaveInstrumento;
	}
	/**
	 * @param tClaveInstrumento the tClaveInstrumento to set
	 */
	public void settClaveInstrumento(String tClaveInstrumento) {
		this.tClaveInstrumento = tClaveInstrumento;
	}
	/**
	 * @return the cCupDesprendidos
	 */
	public String getcCupDesprendidos() {

		if(cCupDesprendidos==null){
			return "0";
		}
		return cCupDesprendidos;
	}
	/**
	 * @param cCupDesprendidos the cCupDesprendidos to set
	 */
	public void setcCupDesprendidos(String cCupDesprendidos) {
		this.cCupDesprendidos = cCupDesprendidos;
	}
	/**
	 * @return the cCupVigentes
	 */
	public String getcCupVigentes() {

		if(cCupVigentes==null){
			return "0";
		}
		return cCupVigentes;
	}
	/**
	 * @param cCupVigentes the cCupVigentes to set
	 */
	public void setcCupVigentes(String cCupVigentes) {
		this.cCupVigentes = cCupVigentes;
	}

	
	}
