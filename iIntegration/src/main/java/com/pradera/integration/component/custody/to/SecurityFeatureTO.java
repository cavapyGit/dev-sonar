package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class SecurityFeatureTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String 		cTipoInstrumento;//INSTRUMENTO
	private String 		tClaveInstrumento;//SERIE
	private String 		cEmisor;
	private String  	cCustodio;
	private String 		fInformacion;
	private String 		fRegistroCustodio;
	private String 		cEntidad;
	private String 		cMoneda;
	private BigDecimal 	mValorNominal;
	private String 		fEmision;
	private String 		fVencimiento;
	private BigDecimal 	qValores;
	private BigDecimal 	pTasaRendimiento;
	private BigDecimal 	pTasaDescuento;
	private BigDecimal 	qCupones;
	private BigDecimal 	cTipoAmortizacion;
	private BigDecimal 	tPeriodicidad;
	private String 		tModalidadNegocia;
	private String 		tMesaDinero;
	/**
	 * @return the cTipoInstrumento
	 */
	public String getcTipoInstrumento() {
		return cTipoInstrumento;
	}
	/**
	 * @param cTipoInstrumento the cTipoInstrumento to set
	 */
	public void setcTipoInstrumento(String cTipoInstrumento) {
		this.cTipoInstrumento = cTipoInstrumento;
	}
	/**
	 * @return the tClaveInstrumento
	 */
	public String gettClaveInstrumento() {
		return tClaveInstrumento;
	}
	/**
	 * @param tClaveInstrumento the tClaveInstrumento to set
	 */
	public void settClaveInstrumento(String tClaveInstrumento) {
		this.tClaveInstrumento = tClaveInstrumento;
	}
	/**
	 * @return the cEmisor
	 */
	public String getcEmisor() {
		return cEmisor;
	}
	/**
	 * @param cEmisor the cEmisor to set
	 */
	public void setcEmisor(String cEmisor) {
		this.cEmisor = cEmisor;
	}
	/**
	 * @return the cCustodio
	 */
	public String getcCustodio() {
		return cCustodio;
	}
	/**
	 * @param cCustodio the cCustodio to set
	 */
	public void setcCustodio(String cCustodio) {
		this.cCustodio = cCustodio;
	}
	/**
	 * @return the fInformacion
	 */
	public String getfInformacion() {
		return fInformacion;
	}
	/**
	 * @param fInformacion the fInformacion to set
	 */
	public void setfInformacion(String fInformacion) {
		this.fInformacion = fInformacion;
	}
	/**
	 * @return the fRegistroCustodio
	 */
	public String getfRegistroCustodio() {
		return fRegistroCustodio;
	}
	/**
	 * @param fRegistroCustodio the fRegistroCustodio to set
	 */
	public void setfRegistroCustodio(String fRegistroCustodio) {
		this.fRegistroCustodio = fRegistroCustodio;
	}
	/**
	 * @return the cEntidad
	 */
	public String getcEntidad() {
		return cEntidad;
	}
	/**
	 * @param cEntidad the cEntidad to set
	 */
	public void setcEntidad(String cEntidad) {
		this.cEntidad = cEntidad;
	}
	/**
	 * @return the cMoneda
	 */
	public String getcMoneda() {
		return cMoneda;
	}
	/**
	 * @param cMoneda the cMoneda to set
	 */
	public void setcMoneda(String cMoneda) {
		this.cMoneda = cMoneda;
	}
	/**
	 * @return the mValorNominal
	 */
	public BigDecimal getmValorNominal() {
		return mValorNominal;
	}
	/**
	 * @param mValorNominal the mValorNominal to set
	 */
	public void setmValorNominal(BigDecimal mValorNominal) {
		this.mValorNominal = mValorNominal;
	}
	/**
	 * @return the fEmision
	 */
	public String getfEmision() {
		return fEmision;
	}
	/**
	 * @param fEmision the fEmision to set
	 */
	public void setfEmision(String fEmision) {
		this.fEmision = fEmision;
	}
	/**
	 * @return the fVencimiento
	 */
	public String getfVencimiento() {
		return fVencimiento;
	}
	/**
	 * @param fVencimiento the fVencimiento to set
	 */
	public void setfVencimiento(String fVencimiento) {
		this.fVencimiento = fVencimiento;
	}
	/**
	 * @return the qValores
	 */
	public BigDecimal getqValores() {
		return qValores;
	}
	/**
	 * @param qValores the qValores to set
	 */
	public void setqValores(BigDecimal qValores) {
		this.qValores = qValores;
	}
	/**
	 * @return the pTasaRendimiento
	 */
	public BigDecimal getpTasaRendimiento() {
		return pTasaRendimiento;
	}
	/**
	 * @param pTasaRendimiento the pTasaRendimiento to set
	 */
	public void setpTasaRendimiento(BigDecimal pTasaRendimiento) {
		this.pTasaRendimiento = pTasaRendimiento;
	}
	/**
	 * @return the pTasaDescuento
	 */
	public BigDecimal getpTasaDescuento() {
		return pTasaDescuento;
	}
	/**
	 * @param pTasaDescuento the pTasaDescuento to set
	 */
	public void setpTasaDescuento(BigDecimal pTasaDescuento) {
		this.pTasaDescuento = pTasaDescuento;
	}
	/**
	 * @return the qCupones
	 */
	public BigDecimal getqCupones() {
		return qCupones;
	}
	/**
	 * @param qCupones the qCupones to set
	 */
	public void setqCupones(BigDecimal qCupones) {
		this.qCupones = qCupones;
	}
	/**
	 * @return the cTipoAmortizacion
	 */
	public BigDecimal getcTipoAmortizacion() {
		return cTipoAmortizacion;
	}
	/**
	 * @param cTipoAmortizacion the cTipoAmortizacion to set
	 */
	public void setcTipoAmortizacion(BigDecimal cTipoAmortizacion) {
		this.cTipoAmortizacion = cTipoAmortizacion;
	}
	/**
	 * @return the tPeriodicidad
	 */
	public BigDecimal gettPeriodicidad() {
		return tPeriodicidad;
	}
	/**
	 * @param tPeriodicidad the tPeriodicidad to set
	 */
	public void settPeriodicidad(BigDecimal tPeriodicidad) {
		this.tPeriodicidad = tPeriodicidad;
	}
	/**
	 * @return the tModalidadNegocia
	 */
	public String gettModalidadNegocia() {
		return tModalidadNegocia;
	}
	/**
	 * @param tModalidadNegocia the tModalidadNegocia to set
	 */
	public void settModalidadNegocia(String tModalidadNegocia) {
		this.tModalidadNegocia = tModalidadNegocia;
	}
	/**
	 * @return the tMesaDinero
	 */
	public String gettMesaDinero() {
		return tMesaDinero;
	}
	/**
	 * @param tMesaDinero the tMesaDinero to set
	 */
	public void settMesaDinero(String tMesaDinero) {
		this.tMesaDinero = tMesaDinero;
	}
	
	
}
