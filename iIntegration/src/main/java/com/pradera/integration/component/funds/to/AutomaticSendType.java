package com.pradera.integration.component.funds.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class AutomaticSendType extends ValidationOperationType implements Serializable{

	private static final long serialVersionUID = 1L;

	private String codTypeOperation;
	
	private Date date;
	
	private String correlativeNumber;
	
	private String originParticipantCode;
	
	private String originBankAccountCode;
	
	private String originCurrency;
	
	private String originDepartment;
	
	private String targetParticipantCode;
	
	private String targetBankAccountCode;
	
	private String targetCurrency;
	
	private String targetDepartment;
	
	private BigDecimal amount;
	
	private String settlementType;
	
	private String priority;
	
	private String creditEntity;
	
	private String textDescription;
	
	private String userCode;
	
	private List<AditionalInformationSendType> lstAditionalInformationSendType;
	
	private Long numberRequest;
	
	private String originParticipantDesc;
	
	private String targetParticipantDesc;
	
	private String descTypeOperation;
	
	private BigDecimal retirementAmount;
	
	private BigDecimal actualBalance;
	
	private BigDecimal previousBalance;
	
	private String hour;
	
	private Integer indRecepFunds;
	
	private String codeParticipant;
	
	private String mnemonicParticipant;

	public String getCodTypeOperation() {
		return codTypeOperation;
	}

	public void setCodTypeOperation(String codTypeOperation) {
		this.codTypeOperation = codTypeOperation;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCorrelativeNumber() {
		return correlativeNumber;
	}

	public void setCorrelativeNumber(String correlativeNumber) {
		this.correlativeNumber = correlativeNumber;
	}

	public String getOriginParticipantCode() {
		return originParticipantCode;
	}

	public void setOriginParticipantCode(String originParticipantCode) {
		this.originParticipantCode = originParticipantCode;
	}

	public String getOriginBankAccountCode() {
		return originBankAccountCode;
	}

	public void setOriginBankAccountCode(String originBankAccountCode) {
		this.originBankAccountCode = originBankAccountCode;
	}

	public String getOriginCurrency() {
		return originCurrency;
	}

	public void setOriginCurrency(String originCurrency) {
		this.originCurrency = originCurrency;
	}

	public String getOriginDepartment() {
		return originDepartment;
	}

	public void setOriginDepartment(String originDepartment) {
		this.originDepartment = originDepartment;
	}

	public String getTargetParticipantCode() {
		return targetParticipantCode;
	}

	public void setTargetParticipantCode(String targetParticipantCode) {
		this.targetParticipantCode = targetParticipantCode;
	}

	public String getTargetBankAccountCode() {
		return targetBankAccountCode;
	}

	public void setTargetBankAccountCode(String targetBankAccountCode) {
		this.targetBankAccountCode = targetBankAccountCode;
	}

	public String getTargetCurrency() {
		return targetCurrency;
	}

	public void setTargetCurrency(String targetCurrency) {
		this.targetCurrency = targetCurrency;
	}

	public String getTargetDepartment() {
		return targetDepartment;
	}

	public void setTargetDepartment(String targetDepartment) {
		this.targetDepartment = targetDepartment;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(String settlementType) {
		this.settlementType = settlementType;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getCreditEntity() {
		return creditEntity;
	}

	public void setCreditEntity(String creditEntity) {
		this.creditEntity = creditEntity;
	}

	public String getTextDescription() {
		return textDescription;
	}

	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public List<AditionalInformationSendType> getLstAditionalInformationSendType() {
		return lstAditionalInformationSendType;
	}

	public void setLstAditionalInformationSendType(
			List<AditionalInformationSendType> lstAditionalInformationSendType) {
		this.lstAditionalInformationSendType = lstAditionalInformationSendType;
	}

	/**
	 * @return the numberRequest
	 */
	public Long getNumberRequest() {
		return numberRequest;
	}

	/**
	 * @param numberRequest the numberRequest to set
	 */
	public void setNumberRequest(Long numberRequest) {
		this.numberRequest = numberRequest;
	}

	/**
	 * @return the originParticipantDesc
	 */
	public String getOriginParticipantDesc() {
		return originParticipantDesc;
	}

	/**
	 * @param originParticipantDesc the originParticipantDesc to set
	 */
	public void setOriginParticipantDesc(String originParticipantDesc) {
		this.originParticipantDesc = originParticipantDesc;
	}

	/**
	 * @return the targetParticipantDesc
	 */
	public String getTargetParticipantDesc() {
		return targetParticipantDesc;
	}

	/**
	 * @param targetParticipantDesc the targetParticipantDesc to set
	 */
	public void setTargetParticipantDesc(String targetParticipantDesc) {
		this.targetParticipantDesc = targetParticipantDesc;
	}

	/**
	 * @return the descTypeOperation
	 */
	public String getDescTypeOperation() {
		return descTypeOperation;
	}

	/**
	 * @param descTypeOperation the descTypeOperation to set
	 */
	public void setDescTypeOperation(String descTypeOperation) {
		this.descTypeOperation = descTypeOperation;
	}

	/**
	 * @return the retirementAmount
	 */
	public BigDecimal getRetirementAmount() {
		return retirementAmount;
	}

	/**
	 * @param retirementAmount the retirementAmount to set
	 */
	public void setRetirementAmount(BigDecimal retirementAmount) {
		this.retirementAmount = retirementAmount;
	}

	/**
	 * @return the actualBalance
	 */
	public BigDecimal getActualBalance() {
		return actualBalance;
	}

	/**
	 * @param actualBalance the actualBalance to set
	 */
	public void setActualBalance(BigDecimal actualBalance) {
		this.actualBalance = actualBalance;
	}

	/**
	 * @return the previousBalance
	 */
	public BigDecimal getPreviousBalance() {
		return previousBalance;
	}

	/**
	 * @param previousBalance the previousBalance to set
	 */
	public void setPreviousBalance(BigDecimal previousBalance) {
		this.previousBalance = previousBalance;
	}

	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}

	/**
	 * @param hour the hour to set
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}

	/**
	 * @return the indRecepFunds
	 */
	public Integer getIndRecepFunds() {
		return indRecepFunds;
	}

	/**
	 * @param indRecepFunds the indRecepFunds to set
	 */
	public void setIndRecepFunds(Integer indRecepFunds) {
		this.indRecepFunds = indRecepFunds;
	}

	/**
	 * @return the codeParticipant
	 */
	public String getCodeParticipant() {
		return codeParticipant;
	}

	/**
	 * @param codeParticipant the codeParticipant to set
	 */
	public void setCodeParticipant(String codeParticipant) {
		this.codeParticipant = codeParticipant;
	}

	/**
	 * @return the mnemonicParticipant
	 */
	public String getMnemonicParticipant() {
		return mnemonicParticipant;
	}

	/**
	 * @param mnemonicParticipant the mnemonicParticipant to set
	 */
	public void setMnemonicParticipant(String mnemonicParticipant) {
		this.mnemonicParticipant = mnemonicParticipant;
	}
	
}
