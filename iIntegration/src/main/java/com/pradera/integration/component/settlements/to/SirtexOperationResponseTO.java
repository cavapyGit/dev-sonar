package com.pradera.integration.component.settlements.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;

public class SirtexOperationResponseTO extends OperationInterfaceTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer operationID;
	private Date operationDate;
	private String participant;
	private String security;
	private Date issuanceDate;
	private Date expirationDate;
	private Integer modality;
	private String operationRate;
	private Integer timeOfLife;
	private Integer quantity;
	private Integer quantityAccept;
	List<SirtexOperationCouponTO> sirCouponTOs;
	private Long idInterfaceProcess;
	
	public SirtexOperationResponseTO() {
		super();
		sirCouponTOs = new ArrayList<>();
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getParticipant() {
		return participant;
	}

	public void setParticipant(String participant) {
		this.participant = participant;
	}

	public Integer getModality() {
		return modality;
	}

	public void setModality(Integer modality) {
		this.modality = modality;
	}
	
	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getOperationRate() {
		return operationRate;
	}

	public void setOperationRate(String operationRate) {
		this.operationRate = operationRate;
	}

	public Integer getTimeOfLife() {
		return timeOfLife;
	}

	public void setTimeOfLife(Integer timeOfLife) {
		this.timeOfLife = timeOfLife;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public List<SirtexOperationCouponTO> getSirCouponTOs() {
		return sirCouponTOs;
	}

	public void setSirCouponTOs(List<SirtexOperationCouponTO> sirCouponTOs) {
		this.sirCouponTOs = sirCouponTOs;
	}

	public Long getIdInterfaceProcess() {
		return idInterfaceProcess;
	}

	public void setIdInterfaceProcess(Long idInterfaceProcess) {
		this.idInterfaceProcess = idInterfaceProcess;
	}

	public Integer getOperationID() {
		return operationID;
	}

	public void setOperationID(Integer operationID) {
		this.operationID = operationID;
	}

	public Integer getQuantityAccept() {
		return quantityAccept;
	}

	public void setQuantityAccept(Integer quantityAccept) {
		this.quantityAccept = quantityAccept;
	}
	
	
	
}
