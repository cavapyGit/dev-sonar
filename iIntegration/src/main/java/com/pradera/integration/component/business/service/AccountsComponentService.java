package com.pradera.integration.component.business.service;

import javax.ejb.Local;

import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.exception.ServiceException;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ExecutorComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
@Local
public interface AccountsComponentService {
	
	/**
	 * Execute accounts component.
	 *
	 * @param objAccountComponent the obj account component
	 * @throws ServiceException the service exception
	 */
	public void executeAccountsComponent(AccountsComponentTO objAccountComponent) throws ServiceException;
	
}
