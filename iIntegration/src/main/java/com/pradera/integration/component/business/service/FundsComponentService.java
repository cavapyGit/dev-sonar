package com.pradera.integration.component.business.service;

import javax.ejb.Local;

import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface FundsComponentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/03/2014
 */
@Local
public interface FundsComponentService {
	/**
	 * Execute funds component.
	 *
	 * @param objFundsComponentTO the obj funds component to
	 * @throws ServiceException the service exception
	 */
	public void executeFundsComponent(FundsComponentTO objFundsComponentTO) throws ServiceException;
}
