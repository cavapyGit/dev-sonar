package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Filtros para la busqueda de los titulos fisicos
 * para el flujo de bovedilla
 * Issue 1109
 * 
 * @author jquino
 *
 */
public class PhysicalCertificateTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Numero de Solicitud del deposito de titulos fisicos. */
	private Long operationNumberCertificate;
	
	/** Numero de titulo fisico. */
	private Long certificateNumber;
	
	/** Emisor del titulo fisico. */
	private String idIssuerPk;

	/** Mnemonico del Emisor del titulo fisico. */
	private String issuerMnemonic;
	
	/** Fecha de la solicitud del deposito de titulos fisicos. */
	private String registryDate;
	
	/** Clase de valor del titulo fisico. */
	private Integer securityClass;
	
	/** Descripcion de la Clase de valor del titulo fisico. */
	private String securityClassDesc;
	
	/** Clase Clave de Valor del titulo fisico. */
	private String idSecurityCodePk;
	
	/** Valor Nominal del titulo fisico. */
	private BigDecimal nominalValue;
	
	/** Moneda del titulo fisico. */
	private Integer currency;
	
	/** Descripcion Moneda del titulo fisico. */
	private String currencyDesc;
		
	/** Participante. */
	private Long idParticipantPk;
	
	/** Descripcion Participante. */
	private String participantDesc;
	
	/** CUI. */
	private String idHolderPk;
	
	/** Fecha de Emision del titulo. */
	private String issueDate;
	
	/** Fecha de vencimiento del titulo fisico. */
	private String expirationDate;
	
	/** Titulo aplicado. */
	private Integer indApplied;
	
	/** Estado de la solicitud de deposito. */
	private Integer state;
	
	/** Descripcion del Estado de la solicitud de deposito. */
	private String stateDesc;
	
	/** Fecha de Confirmacion */
	private String stateDate;
	
	/** Fecha Confirmacion */
	private String confDate;
	
	/** Tipo */
	private String tipo;
	
	
	public PhysicalCertificateTO() {
		super();
	}
	
	


	

	public PhysicalCertificateTO(Long operationNumberCertificate,
			Long certificateNumber, String idIssuerPk, String issuerMnemonic,
			String registryDate, Integer securityClass,
			String securityClassDesc, String idSecurityCodePk,
			BigDecimal nominalValue, Integer currency, String currencyDesc,
			Long idParticipantPk, String participantDesc, String idHolderPk,
			String issueDate, String expirationDate, Integer indApplied,
			Integer state, String stateDesc, String stateDate) {
		super();
		this.operationNumberCertificate = operationNumberCertificate;
		this.certificateNumber = certificateNumber;
		this.idIssuerPk = idIssuerPk;
		this.issuerMnemonic = issuerMnemonic;
		this.registryDate = registryDate;
		this.securityClass = securityClass;
		this.securityClassDesc = securityClassDesc;
		this.idSecurityCodePk = idSecurityCodePk;
		this.nominalValue = nominalValue;
		this.currency = currency;
		this.currencyDesc = currencyDesc;
		this.idParticipantPk = idParticipantPk;
		this.participantDesc = participantDesc;
		this.idHolderPk = idHolderPk;
		this.issueDate = issueDate;
		this.expirationDate = expirationDate;
		this.indApplied = indApplied;
		this.state = state;
		this.stateDesc = stateDesc;
		this.stateDate = stateDate;
	}






	public Long getOperationNumberCertificate() {
		return operationNumberCertificate;
	}

	public void setOperationNumberCertificate(Long operationNumberCertificate) {
		this.operationNumberCertificate = operationNumberCertificate;
	}




	public Long getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}

	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}

	public String getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getSecurityClassDesc() {
		return securityClassDesc;
	}

	public void setSecurityClassDesc(String securityClassDesc) {
		this.securityClassDesc = securityClassDesc;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public String getParticipantDesc() {
		return participantDesc;
	}

	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getIndApplied() {
		return indApplied;
	}

	public void setIndApplied(Integer indApplied) {
		this.indApplied = indApplied;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public String getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(String idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public String getStateDate() {
		return stateDate;
	}

	public void setStateDate(String stateDate) {
		this.stateDate = stateDate;
	}

	public String getConfDate() {
		return confDate;
	}

	public void setConfDate(String confDate) {
		this.confDate = confDate;
	}

	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
