package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class AccountBalanceQueryTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long queryNumber;
	private HolderAccountObjectTO holderAccountObjectTO;
	private SecurityObjectTO securityObjectTO; //just get the securities information - it is not used at response file
	
	
	public AccountBalanceQueryTO() {
		super();
	}

	public Long getQueryNumber() {
		return queryNumber;
	}

	public void setQueryNumber(Long queryNumber) {
		this.queryNumber = queryNumber;
	}

	public HolderAccountObjectTO getHolderAccountObjectTO() {
		return holderAccountObjectTO;
	}

	public void setHolderAccountObjectTO(HolderAccountObjectTO holderAccountObjectTO) {
		this.holderAccountObjectTO = holderAccountObjectTO;
	}

	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}
	
}
