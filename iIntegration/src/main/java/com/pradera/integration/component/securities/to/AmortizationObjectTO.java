package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class AmortizationObjectTO extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer paymentNumber;
	private Date paymentDate;
	private BigDecimal paymentFactor;
	
	public Integer getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(Integer paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public BigDecimal getPaymentFactor() {
		return paymentFactor;
	}
	public void setPaymentFactor(BigDecimal paymentFactor) {
		this.paymentFactor = paymentFactor;
	}
	
	
}
