package com.pradera.integration.component.business;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ComponentConstant.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2014
 */
public class ComponentConstant {

	/** * Issuance balance type **. */
	public final static int ISSUE_AMOUNT = 1001;
	
	/** The Constant SEGMENT_AMOUNT. */
	public final static int SEGMENT_AMOUNT = 1002;
	
	/** The Constant PLACED_AMOUNT. */
	public final static int PLACED_AMOUNT = 1003;
	
	/** The Constant AMORTIZED_AMOUNT. */
	public final static int AMORTIZED_AMOUNT = 1004;
	
	/** The Constant CIRCULATION_AMOUNT. */
	public final static int CIRCULATION_AMOUNT = 1005;
	
	/** * Securities balance type **. */
	public final static int SHARE_BALANCE = 2001;
	
	/** The Constant PLACED_BALANCE. */
	public final static int PLACED_BALANCE = 2002;
	
	/** The Constant CIRCULATION_BALANCE. */
	public final static int CIRCULATION_BALANCE = 2003;
	
	/** The Constant PHYSICAL_BALANCE. */
	public final static int PHYSICAL_BALANCE = 2004;
	
	/** The Constant DEMATERIALIZED_BALANCE. */
	public final static int DEMATERIALIZED_BALANCE = 2005;
	
	/** * Account Balance types **. */ 
	public final static int TOTAL_BALANCE = 3001; 
	
	/** The Constant AVAILABLE_BALANCE. */
	public final static int AVAILABLE_BALANCE = 3002;
	
	/** The Constant TRANSIT_BALANCE. */
	public final static int TRANSIT_BALANCE = 3003;
	
	/** The Constant PAWN_BALANCE. */
	public final static int PAWN_BALANCE = 3004;
	
	/** The Constant BAN_BALANCE. */
	public final static int BAN_BALANCE = 3005;
	
	/** The Constant OTHERS_BALANCE. */
	public final static int OTHERS_BALANCE = 3006;
	
	/** The Constant RESERVE_BALANCE. */
	public final static int RESERVE_BALANCE = 3007;
	
	/** The Constant OPPOSITION_BALANCE. */
	public final static int OPPOSITION_BALANCE = 3008;
	
	/** The Constant ACCREDITATION_BALANCE. */
	public final static int ACCREDITATION_BALANCE = 3009;
	
	/** The Constant REPORTING_BALANCE. */
	public final static int REPORTING_BALANCE = 3010;
	
	/** The Constant REPORTED_BALANCE. */
	public final static int REPORTED_BALANCE = 3011;
	
	/** The Constant MARGIN_BALANCE. */
	public final static int MARGIN_BALANCE = 3012;
	
	/** The Constant PURCHASE_BALANCE. */
	public final static int PURCHASE_BALANCE = 3013;
	
	/** The Constant SALE_BALANCE. */
	public final static int SALE_BALANCE = 3014;
	
	/** The Constant LENDER_BALANCE. */
	public final static int LENDER_BALANCE = 3015;
	
	/** The Constant LOANABLE_BALANCE. */
	public final static int LOANABLE_BALANCE = 3016;
	
	/** The Constant BORROWER_BALANCE. */
	public final static int BORROWER_BALANCE = 3017;
	
	/** The Constant GRANTED_BALANCE. */
	public final static int GRANTED_BALANCE = 3018;
	
	/** The Constant GRANT_BALANCE. */
	public final static int GRANT_BALANCE = 3019;
	
	/** * Funds amount types **. */
	public final static int TOTAL_AMOUNT = 4001;
	
	/** The Constant AVAILABLE_AMOUNT. */
	public final static int AVAILABLE_AMOUNT = 4002;
	
	/** The Constant DEPOSIT_AMOUNT. */
	public final static int DEPOSIT_AMOUNT = 4003;
	
	/** The Constant RETIREMENT_AMOUNT. */
	public final static int RETIREMENT_AMOUNT = 4004;
	
	/** The Constant MARGIN_AMOUNT. */
	public final static int MARGIN_AMOUNT = 4005;
	
	/** * Guarantee balance type**. */
	public final static int TOTAL_GUARANTEE = 5001;
	
	/** The Constant PRINCIPAL_GUARANTEE. */
	public final static int PRINCIPAL_GUARANTEE = 5002;
	
	/** The Constant MARGIN_GUARANTEE. */
	public final static int MARGIN_GUARANTEE = 5003;
	
	/** The Constant PRINCIPAL_DIVIDENDS_GUARANTEE. */
	public final static int PRINCIPAL_DIVIDENDS_GUARANTEE = 5004;
	
	/** The Constant MARGIN_DIVIDENDS_GUARANTEE. */
	public final static int MARGIN_DIVIDENDS_GUARANTEE = 5005;
	
	/** The Constant INTEREST_MARGIN_GUARANTEE. */
	public final static int INTEREST_MARGIN_GUARANTEE = 5006;
	
	/** * balance behavior **. */
	public final static long SUM = 1;
	
	/** The Constant SUBTRACTION. */
	public final static long SUBTRACTION = 2;
	
	/** * role position **. */
	public final static Integer PURCHARSE_ROLE = new Integer(1);
	
	public final static Integer SALE_ROLE = new Integer(2);
	
	public static final Integer CASH_PART = new Integer(1);
	
	public static final Integer TERM_PART = new Integer(2);
	
	/** * other constants **. */
	public static final Long SOURCE = new Long(1);
	
	/** The Constant TARGET. */
	public static final Long TARGET = new Long(2);
	
	/** The Constant ONE. */
	public static final Integer ONE = new Integer(1);
	
	/** The Constant ZERO. */
	public static final Integer ZERO = new Integer(0);
	
	public static final String MECHANISM = "MECANISMO";
	
	public static final String MODALITY = "MODALITY";
	
	public static final String SETTLEMENT_SCHEMA = "SETTLEMENT_SCHEMA";
	
	/** The Constant CUSTODY_OPERATION. */
	public static final String CUSTODY_OPERATION = "CUSTODY_OPERATION";
	
	/** The Constant REF_CUSTODY_OPERATION. */
	public static final String REF_CUSTODY_OPERATION = "REF_CUSTODY_OPERATION";
	
	/** The Constant TRADE_OPERATION. */
	public static final String TRADE_OPERATION = "TRADE_OPERATION";
	
	public static final String SETTLEMENT_OPERATION = "SETTLEMENT_OPERATION";
	
	/** The Constant CORPORATIVE_OPERATION. */
	public static final String CORPORATIVE_OPERATION = "CORPORATIVE_OPERATION";
	
	/** The Constant GUARANTEE_OPERATION. */
	public static final String GUARANTEE_OPERATION = "GUARANTEE_OPERATION";
	
	/** The Constant COUPON_GRANT_OPERATION. */
	public static final String COUPON_GRANT_OPERATION = "COUPON_GRANT_OPERATION ";
	
	/** The Constant COUPON_TRANSFER_OPERATION. */
	public static final String COUPON_TRANSFER_OPERATION = "COUPON_TRANSFER_OPERATION";
	
	/** *  **. */
	public static final String HOLDER_ACCOUNT= "HOLDER_ACCOUNT";
	
	/** The Constant PARTICIPANT. */
	public static final String PARTICIPANT= "PARTICIPANT";
	
	/** The Constant SECURITIES. */
	public static final String SECURITIES= "SECURITIES";
	
	/** The Constant STOCK_QUANTITY. */
	public static final String STOCK_QUANTITY= "STOCK_QUANTITY";
	
	/** The Constant INITIAL_DATE. */
	public static final String INITIAL_DATE= "INITIAL_DATE";
	
	/** The Constant FINAL_DATE. */
	public static final String FINAL_DATE= "FINAL_DATE";
	
	public static final String NOT_ACTIVE = "NA";
	
	/** The Constant STOCK_CALCULATION_PK. */
	public static final String STOCK_CALCULATION_PK= "STOCK_CALCULATION_PK";
	
	public static final String INTERFACE_EARLY_PAYMENT_DPF = "RAN";
	
	public static final String INTERFACE_ACCOUNT_ENTRY = "RCO";

	public static final String INTERFACE_ACCOUNT_ENTRY_MASSIVE = "RCOM";
	
	public static final String INTERFACE_ACCOUNT_ENTRY_WEB = "RCOW";

	public static final String INTERFACE_ISUANCE = "RVA";
	
	public static final String INTERFACE_ISUANCE_SECURITIES = "RVAL";
	
	public static final String INTERFACE_ISUANCE_BBXBTX = "RVAW";
	
	public static final String INTERFACE_ACCOUNT_ENTRY_DPF = "REG";
	
	public static final String INTERFACE_INTEREST_PAYMENT_PCU = "PCU";
	
	public static final String INTERFACE_REVERSE_INTEREST_PAYMENT_RPC = "RPC";
	
	public static final String INTERFACE_BBV_OPERATIONS = "OPBBV";
	
	public static final String INTERFACE_BBV_ASSIGNMENTS = "ASIGBBV";
	
	public static final String INTERFACE_EARLY_PAYMENT = "REA";
	
	public static final String INTERFACE_EARLY_PAYMENT_BCBTGN = "REAW";
	
	public static final String INTERFACE_EARLY_PAYMENT_NO_DPF = "RCA";
	
	public static final String INTERFACE_RENEW_SECURITIES_DPF = "REN";
	
	public static final String INTERFACE_REVERSE_EARLY_PAYMENT_DPF = "RRA";
	
	public static final String INTERFACE_REVERSE_EARLY_PAYMENT_NO_DPF = "RPA";
	
	public static final String INTERFACE_REVERSE_RENEW_SECURITIES_DPF = "RRE";
	
	public static final String INTERFACE_REVERSE_ACCOUNT_ENTRY_DPF = "RRG";
	
	public static final String INTERFACE_SINGLE_QUERY_BALANCE_CIV = "CIV";
	
	public static final String INTERFACE_CUSTODY_OPERATION_QUERY_CON = "CON";
	
	public static final String INTERFACE_SECURITY_QUERY_CONW = "CONW";

	public static final String INTERFACE_SECURITIES_REGISTER = "RVA";
	
	public static final String INTERFACE_ACCOUNT_ENTRY_QUERY = "CRG";
	
	public static final String INTERFACE_SECURITIES_RENEWAL_QUERY = "CRE";
	
	public static final String INTERFACE_EARLY_PAYMENT_QUERY = "CRA";
	
	public static final String INTERFACE_INTEREST_PAYMENT_QUERY = "CPC";
	
	public static final String INTERFACE_OTC_OPERATION_DPF = "TRA";
	
	public static final String INTERFACE_OTC_OPERATION_WEB_BCB = "RTE";
	
	public static final String INTERFACE_STOCK_BLOCKING_DPF = "BLQ";
	
	public static final String INTERFACE_STOCK_UNBLOCKING_DPF = "DBQ";
	
	public static final String INTERFACE_SECURITIES_TRANSFER_DPF = "TRV";
	
	public static final String INTERFACE_TRANSFER_DPF_LAST_HOLDER = "TUT";
	
	public static final String INTERFACE_SECONDARY_ACCOUNT_ENTRY_DPF = "ACS";
	
	public static final String INTERFACE_BCB_LIP_SEND = "SLIP";
	
	public static final String MOTIVE_SECONDARY_ACCOUNT_ENTRY = "DES";
	
	public static final String INTERFACE_QUERY_SERVICE_STATUS_DPF = "ESE";
	
	public static final String INTERFACE_DEPOSIT_OPERATION_LIP = "F01";
	
	public static final String INTERFACE_BCB_LIP_EXTRACT= "S01";
	
	public static final String INTERFACE_BCB_LIP_BALANCE = "S02";
	
	public static final String INTERFACE_BCB_SECURITY_ENTRY_QUERY = "CONW";
	
	public static final String INTERFACE_BCB_BTX_SECURITY_ENTRY_QUERY = "CVE";
	
	public static final String MONEY_EXCHANGE_SEND = "MES";
	
	public static final String INTERFACE_SIRTEX_OPERATION_CONSULT = "CSX";
	
	public static final String INTERFACE_SIRTEX_OPERATION_REVIEW = "ROS";
	
	public static final String INTERFACE_SIRTEX_OPERATION_REJECT = "ROX";
	
	public static final String INTERFACE_SIRTEX_OPERATION_CONFIRM = "COS";

	public static final String INTERFACE_SECURITY_PLACEMENT_RCO = "RCO";
	
	public static final String INTERFACE_EARLY_PAYMENT_OF_PLACEMENT = "RRC";
	public static final String SENDING_ACCOUNTING_INTERFACE = "REG-AST";
	
	public static final String SIMBOL_TO_REPLACE = "\\[";
	
	public static final String OPEN_CLASP = "[";
	
	public static final String CLASP_CLOSURE = "]";
	
	

	public static final String REG_DPF_NAME = "accountentry_dpf.xml";
	public static final String RAN_DPF_NAME = "earlyfile_dpf.xml";
	public static final String RAN_WSBCBTGN_NAME = "earlyfile_bcbtgn.xml";
	public static final String PCU_DPF_NAME = "pcufile_dpf.xml";
	public static final String RPC_DPF_NAME = "rpcfile_dpf.xml";
	public static final String CIV_DPF_NAME = "civfile_dpf.xml";
	public static final String CON_DPF_NAME = "confile_dpf.xml";
	public static final String CON_BBXBTX_NAME = "confile_bbxbtx.xml";
	public static final String RCO_BBX_NAME = "accountentryplacement.xml";	
	public static final String REN_DPF_NAME = "renewsecfile_dpf.xml";
	public static final String RRA_DPF_NAME = "reverseearlyfile_dpf.xml";
	public static final String RPA_NO_DPF_NAME = "revert_earlypayment_no_dpf.xml.xml";
	public static final String RRE_DPF_NAME = "reverserenewsecfile_dpf.xml";
	public static final String TUT_DPF_NALE = "tutfile_dpf.xml";
	public static final String TRV_DPF_NAME = "trvfile_dpf.xml";
	public static final String BLQ_DPF_NAME = "blqfile_dpf.xml";
	public static final String DBQ_DPF_NAME = "dbqfile_dpf.xml";
	public static final String ACS_DPF_NAME = "acsfile_dpf.xml";
	public static final String TRA_DPF_NAME = "trafile_dpf.xml";
	public static final String RTE_DPF_NAME = "rtefile_dpf.xml";
	public static final String ESE_DPF_NAME = "servicestatus_dpf.xml";
	public static final String RRG_DPF_NAME = "reverseaccountryyfile_dpf.xml";
	public static final String LIP_SEND_NAME = "sendfile_lip.xml";
	public static final String LIP_DEP_NAME = "depositfile_lip.xml";
	public static final String LIP_EXTRACT_NAME = "extractfile_lip.xml";
	public static final String RCO_DPF_NAME = "accountentry.xml"; 
	public static final String RVA_DPF_NAME = "issuancerva_dpf.xml";
	public static final String MONEY_EXCHANGE_NAME = "moneyexchangesend.xml";
	public static final String SIRTEX_OPERATION_CON = "csxquery.xml";
	public static final String SIRTEX_OPERATION_REV = "rosquery.xml";
	public static final String SIRTEX_OPERATION_REJ = "roxquery.xml";
	public static final String SIRTEX_OPERATION_COS = "cosquery.xml";
	public static final String RRC_BBXBTX_NAME = " reverseregisterplacement_no_dpf.xml";
	
	public static final String INTERFACE_SIRTEX_TAG = "CONS_SIRTEX";
	public static final String INTERFACE_SIRTEX_STATE_TAG = "ESTADO_SIRTEX";
	public static final String INTERFACE_SIRTEX_OPERATION_TAG = "OPERACIONES";
	public static final String INTERFACE_TAG = "root";
	public static final String DPF_TAG = "raiz";
	public static final String INTERFACE_TAG_REVERT_CAN_ANTICIPADA_RESPONSE = "RESP_REV_CANC_ANTICIPADA";
	public static final String INTERFACE_TAG_ISSUANCE_BCB = "REG_VALOR";
	public static final String INTERFACE_TAG_ISSUANCE_RESPONSE = "RESP_VALOR";
	public static final String INTERFACE_TAG_STOCK_EARLY_BCBTGN = "REG_RED_ANTICIPADA";
	public static final String INTERFACE_TAG_STOCK_EARLY_NO_DPF = "RESP_CANC_ANTICIPADA";
	public static final String INTERFACE_RECORD_ISSUANCE_BCB = "OPERACION";
	public static final String INTERFACE_RECORD_STOCK_EARLY_BCBTGN = "SECC_OPERACION";
	public static final String INTERFACE_TAG_EARLY_PAYMENT = "REG_RED_ANTICIPADA";
	public static final String OPERATIONS_RECORD_TAG = "operaciones";
	public static final String OPERATION_RECORD_TAG = "operacion";
	public static final String QUERY_RECORD_TAG = "consulta";
	public static final String INTERFACE_TAG_ROWSET = "rowset";
	public static final String INTERFACE_TAG_ROW = "row";
	public static final String MONEY_EXCHANGE_TAG_ROW = "edv";
	
	public static final String TAG_BCB_RTE = "REG_TRANSFERENCIA";
	public static final String OPERATION_TAG_BCB = "OPERACION";
	public static final String REG_PLACEMENT_TAG = "REG_COLOCACION";
	public static final String REG_PLACEMENT_TAG_RESPONSE = "RESP_COLOCACION";
	public static final String REV_PLACEMENT_TAG_RESPONSE = "RESP_REV_COL";
	public static final String OPERATION_PLACEMENT_TAG = "OPERACION";

	public static final String BBX_BTX_TAG = "CON_VENCIMIENTOS";
	public static final String QUERY_BBX_BTX_RECORD_TAG = "SECC_OPERACION";


	public static final String INTERFACE_TAG_ISSUANCES = "emisiones";
	public static final String INTERFACE_TAG_ISSUANCE = "emision";
	public static final String WAY_INTERFACE_FILE = "schema/edv/generationFileSecuritiesTRCR.xml";
	public static final String INTERFACE_LIP_ROW = "transferencia";
	public static final String DEPOSIT_LIP_ROW = "deposito";
	public static final String EXTRACT_LIP_ROW = "extracto";
	public static final String EXTRACT_LIP_RESP = "respuesta";
	
	
	public static final String CPC_SCHEMA_RESPONSE = "schema/edvresponse/interestpaymentquery_response.xml";
	public static final String CRA_SCHEMA_RESPONSE = "schema/edvresponse/earlypaymentquerydpf_reponse.xml";
	public static final String CRE_SCHEMA_RESPONSE = "schema/edvresponse/securitiesrenewalquerydpf_reponse.xml";
	public static final String CRG_SCHEMA_RESPONSE = "schema/edvresponse/accountentryquerydpf_reponse.xml";
	public static final String BBX_BTX_SCHEMA_RESPONSE = "schema/edvresponse/accountentryquerybbxbtx_reponse.xml";
	public static final String BBX_BTX_SCHEMA_RESPONSE_INCORRECT = "schema/edvresponse/acocuntentrybbxbtx_response_incorrect.xml";
	
	public static final String AFFECTATION_CODE_DPF_ECA = "ECA";
	public static final String AFFECTATION_CODE_DPF_REJ = "REJ";
	public static final String AFFECTATION_CODE_DPF_PRE = "PRE";
	
	// LIP
	
	public static final String LIP_BCB_OPERATION_TYPE_E02 = "E02";
	public static final String LIP_BCB_OPERATION_TYPE_E03 = "E03";
	public static final String LIP_BCB_OPERATION_TYPE_E73 = "E73";
	public static final String LIP_BCB_OPERATION_TYPE_E74 = "E74";
	public static final String LIP_BCB_OPERATION_TYPE_M12T = "M12T";
	
	//CONSULTAS CDPF 
	public static final String INTERFACE_CUSTODY_OPERATION_QUERY_CONCDF = "CONCDF";
	public static final String CUSTODY_OPERATION_OBEJET_RESPONSE = "schema/edvresponse/queryDpfResponse.xml";
	public static final String CDPF_SCHEMA_QUERY_CONCDF = "save_file.xml";
	public static final String TAG_CONSULT_DPF="valores";
	public static final String CUSTODY_OPERATION_OBEJET_RESPONSE_ERROR="schema/edvresponse/queryDpfResponseIncorrect.xml";
	
	//Cobros de Derechos Economicos
	public static final String TRANSFERS_WITH_OPEN_GLOSS = "E35";
	
	public static final String TRANSFERS_WITH_OPEN_GLOSS_DEPOSIT = "E77";
	
	public static final String TRANSFER_ACCOUNTING_FUNDS = "COIN01";
	
	//CONSULTA DE CUI PARA SISTEMAS INTERNOS
	
	public static final String INTERFACE_ACCOUNT_OPERATION_CUI_QUERY = "CCC";
	public static final String CUI_QUERY_SCHEMA = "cuiquery.xml";
	public static final String CUI_QUERY_SCHEMA_RESPONSE = "schema/edvresponse/cuiquery_response.xml";
	public static final String CUI_QUERY_SCHEMA_RESPONSE_INCORRECT = "schema/edvresponse/cuiquery_response_incorrect.xml";
	public static final String CUI_QUERY_TAG = "CONS_CTAS_CUI";
	public static final String CUI_QUERY_TAG_RESPONSE = "CONSS_CTAS_CUI";
	public static final String CUI_QUERY_RECORD_TAG = "CONSULTA";
	public static final String CUI_QUERY_RECORD_TAG_RESPONSE = "CONSULTA";
	
	//CONSULTA DE FIRMAS AUTORIZADAS PARA SISTEMAS INTERNOS
	public static final String INTERFACE_CUSTODY_OPERATION_AUTHORIZED_SIGNATURE_QUERY = "CFA";
	public static final String AUTHORIZED_SIGNATURE_SCHEMA = "authorizedsignaturequery.xml";
	public static final String AUTHORIZED_SIGNATURE_SCHEMA_RESPONSE = "schema/edvresponse/authorizedsignaturequery_response.xml";
	public static final String AUTHORIZED_SIGNATURE_SCHEMA_RESPONSE_INCORRECT = "schema/edvresponse/authorizedsignaturequery_response_incorrect.xml";
	public static final String AUTHORIZED_SIGNATURE_TAG = "CONS_AUTHORIZED_SIGNATURE";
	public static final String AUTHORIZED_SIGNATURE_TAG_RESPONSE = "CONS_AUTHORIZED_SIGNATURE";
	public static final String AUTHORIZED_SIGNATURE_RECORD_TAG = "CONSULTA";
	public static final String AUTHORIZED_SIGNATURE_RECORD_TAG_RESPONSE = "CONSULTA";
	
	//CONSULTA DE INSTITUCIONES
	public static final String INTERFACE_INSTITUTION_QUERY = "CIN";
	public static final String INSTITUTION_QUERY_SCHEMA = "institutionquery.xml";
	public static final String INSTITUTION_QUERY_SCHEMA_RESPONSE = "schema/edvresponse/institutionquery_response.xml";
	public static final String INSTITUTION_QUERY_SCHEMA_RESPONSE_INCORRECT = "schema/edvresponse/institutionquery_response_incorrect.xml";
	public static final String INSTITUTION_QUERY_TAG = "CONS_INSTITUTION";
	public static final String INSTITUTION_QUERY_TAG_RESPONSE = "CONS_INSTITUTION";
	public static final String INSTITUTION_QUERY_RECORD_TAG = "CONSULTA";
	public static final String INSTITUTION_QUERY_RECORD_TAG_RESPONSE = "CONSULTA";
	
	//CONSULTA DE TITULOS FISICOS FLUJO BOVEDILLA ISSUE 1210
	
	public static final String INTERFACE_PHYSICAL_CERTIFICATE_QUERY = "CTF";
	public static final String PHYSICAL_CERTIFICATE_QUERY_SCHEMA = "physicalcertificatequery.xml";
	public static final String PHYSICAL_CERTIFICATE_QUERY_RESPONSE = "schema/edvresponse/physicalcertificatequery_response.xml";
	public static final String PHYSICAL_CERTIFICATE_QUERY_SCHEMA_RESPONSE_INCORRECT = "schema/edvresponse/physicalccertificatequery_response_incorrect.xml";
	public static final String PHYSICAL_CERTIFICATE_QUERY_TAG = "SOLICITUD_ACTIVO_FINANCIERO";
	public static final String PHYSICAL_CERTIFICATE_QUERY_TAG_RESPONSE = "SOLICITUD_ACTIVO_FINANCIERO";
	public static final String PHYSICAL_CERTIFICATE_QUERY_RECORD_TAG = "CONSULTA";
	public static final String PHYSICAL_CERTIFICATE_QUERY_RECORD_TAG_RESPONSE = "CONSULTA";

	public static final String MASSIVE_ASSIGNMENT_PREFIX = "BVA";
	public static final String MASSIVE_ASSIGNMENT_SUFFIX = "_AS";
	
	public static final String MASSIVE_MCN_PREFIX = "BVA";
	public static final String MASSIVE_MCN_SUFFIX = "_OP";
	
	public static final String INTERFACE_HOLDER_MASSIVE_FOLDER = "HMF";
	public static final String MASSIVE_HOLDER_MASSIVE_PREFIX = "AMT";
	public static final String MASSIVE_HOLDER_MASSIVE_SUFFIX = "_CPY";
}
