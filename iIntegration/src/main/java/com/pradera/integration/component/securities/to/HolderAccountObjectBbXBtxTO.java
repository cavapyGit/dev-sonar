package com.pradera.integration.component.securities.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.ValidationOperationType;

public class HolderAccountObjectBbXBtxTO extends ValidationOperationType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer accountNumber;
	private Long cui;
	private Integer accountType;
	private String documentType;
	private String documentNumber;
	private String expended;
	private Integer indResidence;
	
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Long getCui() {
		return cui;
	}
	public void setCui(Long cui) {
		this.cui = cui;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getExpended() {
		return expended;
	}
	public void setExpended(String expended) {
		this.expended = expended;
	}
	public Integer getIndResidence() {
		return indResidence;
	}
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}
	
	
	
}
