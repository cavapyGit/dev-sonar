package com.pradera.integration.component.custody.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class StockEarlyPaymentRegisterTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String motive;
	private Integer redemptionType;
	private String redemptionTypeCode;
	
	private HolderAccountObjectTO holderAccountObjectTO;
	private SecurityObjectTO securityObjectTO;
	
	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}
	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}
	public String getMotive() {
		return motive;
	}
	public void setMotive(String motive) {
		this.motive = motive;
	}
	public HolderAccountObjectTO getHolderAccountObjectTO() {
		return holderAccountObjectTO;
	}
	public void setHolderAccountObjectTO(HolderAccountObjectTO holderAccountObjectTO) {
		this.holderAccountObjectTO = holderAccountObjectTO;
	}
	public Integer getRedemptionType() {
		return redemptionType;
	}
	public void setRedemptionType(Integer redemptionType) {
		this.redemptionType = redemptionType;
	}
	public String getRedemptionTypeCode() {
		return redemptionTypeCode;
	}
	public void setRedemptionTypeCode(String redemptionTypeCode) {
		this.redemptionTypeCode = redemptionTypeCode;
	}
}
