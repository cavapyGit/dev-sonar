package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.business.to.OperationQueryTO;
import com.pradera.integration.component.securities.to.SecurityObjectBbxBtxTO;

public class SecurityEntryQueryTO extends OperationQueryTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SecurityObjectBbxBtxTO> listSecurityObjectBbxBtxTO;
	
	
	
	public SecurityEntryQueryTO() {
		super();
	}
	
	public SecurityEntryQueryTO(OperationQueryTO operationQueryTO) {
		setQueryNumber(operationQueryTO.getQueryNumber());
		setOperationCode(operationQueryTO.getOperationCode());
		setEntityNemonic(operationQueryTO.getEntityNemonic());
		setOperationDate(operationQueryTO.getOperationDate());
		setOperationNumber(operationQueryTO.getQueryNumber());
		setSecurityClass(operationQueryTO.getSecurityClass());
		setIdInterfaceProcess(operationQueryTO.getIdInterfaceProcess());
		
		// Esta linea fue adicionada para BBX y BTX
		setFinalDate(operationQueryTO.getFinalDate());
		setInitialDate(operationQueryTO.getInitialDate());
	}

	public List<SecurityObjectBbxBtxTO> getListSecurityObjectBbxBtxTO() {
		return listSecurityObjectBbxBtxTO;
	}

	public void setListSecurityObjectBbxBtxTO(
			List<SecurityObjectBbxBtxTO> listSecurityObjectBbxBtxTO) {
		this.listSecurityObjectBbxBtxTO = listSecurityObjectBbxBtxTO;
	}
	
}
