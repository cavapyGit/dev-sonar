package com.pradera.integration.component.business.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;

public class OperationQueryTO extends OperationInterfaceTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long queryNumber;
	private Integer securityClass;
	private String securityClassDesc;
	private SecurityObjectTO securityObjectTO;
	
	private Date initialDate;
	private Date finalDate;

	
	public OperationQueryTO() {
		super();
	}

	public SecurityObjectTO getSecurityObjectTO() {
		return securityObjectTO;
	}

	public void setSecurityObjectTO(SecurityObjectTO securityObjectTO) {
		this.securityObjectTO = securityObjectTO;
	}

	public Long getQueryNumber() {
		return queryNumber;
	}

	public void setQueryNumber(Long queryNumber) {
		this.queryNumber = queryNumber;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getSecurityClassDesc() {
		return securityClassDesc;
	}

	public void setSecurityClassDesc(String securityClassDesc) {
		this.securityClassDesc = securityClassDesc;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	
	
}
