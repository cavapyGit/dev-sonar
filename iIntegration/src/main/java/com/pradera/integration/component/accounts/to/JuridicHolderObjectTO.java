package com.pradera.integration.component.accounts.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.component.custody.to.AuthorizedSignatureTO;

public class JuridicHolderObjectTO extends PersonObjectTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idHolder;
	private String fullName; // razon social
	private String mnemonic; //mnemonico
	private Integer juridicClass; // clase juridico
	private Integer economicSector;
	private Integer economicActivity;
	private String officePhoneNumber;
	private String mobileNumber;
	private String faxNumber;
	private String email;
//	private String legalResidenceCountry;
	private Integer legalDepartment;
	private String legalDepartmentCode;
	private Integer legalProvince;
	private Integer legalDistrict; // municipio
	private String legalAddress;
	private String legalAddress2;
//	private String nationality;
	private Integer indResidence;
	private String holderTypeDescription;
	private Integer indFatca;
	
	private List<AuthorizedSignatureTO> lstAuthorizedSignature; //lista de firmas autorizadas
	
	//Holder Accounts
	private  List<HolderAccountObjectTO> lstHolderAccountTOs;
	
	private List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjects;
		
	public Long getIdHolder() {
		return idHolder;
	}
	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getJuridicClass() {
		return juridicClass;
	}
	public void setJuridicClass(Integer juridicClass) {
		this.juridicClass = juridicClass;
	}
	public Integer getEconomicSector() {
		return economicSector;
	}
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}
	public Integer getEconomicActivity() {
		return economicActivity;
	}
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}
	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}
	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getLegalDepartment() {
		return legalDepartment;
	}
	public void setLegalDepartment(Integer legalDepartment) {
		this.legalDepartment = legalDepartment;
	}
	public String getLegalDepartmentCode() {
		return legalDepartmentCode;
	}
	public void setLegalDepartmentCode(String legalDepartmentCode) {
		this.legalDepartmentCode = legalDepartmentCode;
	}
	public Integer getLegalProvince() {
		return legalProvince;
	}
	public void setLegalProvince(Integer legalProvince) {
		this.legalProvince = legalProvince;
	}
	public Integer getLegalDistrict() {
		return legalDistrict;
	}
	public void setLegalDistrict(Integer legalDistrict) {
		this.legalDistrict = legalDistrict;
	}
	public String getLegalAddress() {
		return legalAddress;
	}
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}
	public String getLegalAddress2() {
		return legalAddress2;
	}
	public void setLegalAddress2(String legalAddress2) {
		this.legalAddress2 = legalAddress2;
	}
	public Integer getIndResidence() {
		return indResidence;
	}
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}
	public List<HolderAccountObjectTO> getLstHolderAccountTOs() {
		return lstHolderAccountTOs;
	}
	public void setLstHolderAccountTOs(
			List<HolderAccountObjectTO> lstHolderAccountTOs) {
		this.lstHolderAccountTOs = lstHolderAccountTOs;
	}
	public String getHolderTypeDescription() {
		return holderTypeDescription;
	}
	public void setHolderTypeDescription(String holderTypeDescription) {
		this.holderTypeDescription = holderTypeDescription;
	}
	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	public List<AuthorizedSignatureTO> getLstAuthorizedSignature() {
		return lstAuthorizedSignature;
	}
	public void setLstAuthorizedSignature(List<AuthorizedSignatureTO> lstAuthorizedSignature) {
		this.lstAuthorizedSignature = lstAuthorizedSignature;
	}
	public List<LegalRepresentativeObjectTO> getLstLegalRepresentativeObjects() {
		return lstLegalRepresentativeObjects;
	}
	public void setLstLegalRepresentativeObjects(List<LegalRepresentativeObjectTO> lstLegalRepresentativeObjects) {
		this.lstLegalRepresentativeObjects = lstLegalRepresentativeObjects;
	}
	public Integer getIndFatca() {
		return indFatca;
	}
	public void setIndFatca(Integer indFatca) {
		this.indFatca = indFatca;
	}
	
	
}