package com.pradera.integration.component.custody.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class DematerializationSecuritiesByIssuerTO extends OperationInterfaceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer		index;//
	private String 		instrument;//INSTRUMENTO
	private String 		idSecurityCode;//SERIE
	private String 		securityAlter;//SERIE_ALTER
	private String  	currencyCode;//MONEDA
	private Long 		idHolderFk;//COD_CUI
	private String 		indResidence;
	private String 		nitHolder;
	
	private Long 		numTitle;//NUM_TITULOS
	private Long 		quantiy;//CANTIDAD
	
	private BigDecimal 	rate;//TASA
	private BigDecimal 	unitPrice;//PRECIO_UNI
	private String 		operationCode;
	private String 		holderName;
	
	private Date 		dateDesmaterialization;//FECHA_DES
	private String		dateDes;


	/**
	 * @return the instrument
	 */
	public String getInstrument() {
		return instrument;
	}

	/**
	 * @param instrument the instrument to set
	 */
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	/**
	 * @return the idSecurityCode
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * @param idSecurityCode the idSecurityCode to set
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	/**
	 * @return the securityAlter
	 */
	public String getSecurityAlter() {
		return securityAlter;
	}

	/**
	 * @param securityAlter the securityAlter to set
	 */
	public void setSecurityAlter(String securityAlter) {
		this.securityAlter = securityAlter;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the idHolderFk
	 */
	public Long getIdHolderFk() {
		return idHolderFk;
	}

	/**
	 * @param idHolderFk the idHolderFk to set
	 */
	public void setIdHolderFk(Long idHolderFk) {
		this.idHolderFk = idHolderFk;
	}

	/**
	 * @return the indResidence
	 */
	public String getIndResidence() {
		return indResidence;
	}

	/**
	 * @param indResidence the indResidence to set
	 */
	public void setIndResidence(String indResidence) {
		this.indResidence = indResidence;
	}

	/**
	 * @return the nitHolder
	 */
	public String getNitHolder() {
		return nitHolder;
	}

	/**
	 * @param nitHolder the nitHolder to set
	 */
	public void setNitHolder(String nitHolder) {
		this.nitHolder = nitHolder;
	}

	/**
	 * @return the numTitle
	 */
	public Long getNumTitle() {
		return numTitle;
	}

	/**
	 * @param numTitle the numTitle to set
	 */
	public void setNumTitle(Long numTitle) {
		this.numTitle = numTitle;
	}

	/**
	 * @return the quantiy
	 */
	public Long getQuantiy() {
		return quantiy;
	}

	/**
	 * @param quantiy the quantiy to set
	 */
	public void setQuantiy(Long quantiy) {
		this.quantiy = quantiy;
	}

	/**
	 * @return the rate
	 */
	public BigDecimal getRate() {
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	/**
	 * @return the unitPrice
	 */
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the operationCode
	 */
	public String getOperationCode() {
		return operationCode;
	}

	/**
	 * @param operationCode the operationCode to set
	 */
	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return the dateDesmaterialization
	 */
	public Date getDateDesmaterialization() {
		return dateDesmaterialization;
	}

	/**
	 * @param dateDesmaterialization the dateDesmaterialization to set
	 */
	public void setDateDesmaterialization(Date dateDesmaterialization) {
		this.dateDesmaterialization = dateDesmaterialization;
	}

	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}


	/**
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * @return the dateDes
	 */
	public String getDateDes() {
		return dateDes;
	}

	/**
	 * @param dateDes the dateDes to set
	 */
	public void setDateDes(String dateDes) {
		this.dateDes = dateDes;
	}


	
	
	}
