package com.pradera.integration.component.custody.to;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import java.io.Serializable;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlActivoFinanciero implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "NRO_SOLICITUD")
	private long nroSolicitud;
	@XmlElement(name = "NRO_TITULO")
	private long nroTitulo;
	@XmlElement(name = "EMISOR")
	private String emisor;
	@XmlElement(name = "FECHA_SOLICITUD")
	private String fechaSolicitud;
	@XmlElement(name = "CLASE_VALOR")
	private String claseValor;
	@XmlElement(name = "CLASE_CLAVE_VALOR")
	private String claseClaveValor;
	@XmlElement(name = "VALOR_NOMINAL")
	private BigDecimal valorNominal;
	@XmlElement(name = "MONEDA")
	private String moneda;
	@XmlElement(name = "PARTICIPANTE")
	private String participante;
	@XmlElement(name = "CUI")
	private String cui;
	@XmlElement(name = "FECHA_EMISION")
	private String fechaEmision;
	@XmlElement(name = "FECHA_VENCIMIENTO")
	private String fechaVencimiento;
	@XmlElement(name = "FECHA_CONFIRMACION")
	private String fechaConfirmacion;
	@XmlElement(name = "ESTADO")
	private String estado;
	@XmlElement(name = "TITULO_APLICADO")
	private int tituloAplicado;
	@XmlElement(name = "TIPO")
	private String tipo;

	public XmlActivoFinanciero() {
		super();
	}

	public Long getNroSolicitud() {
		return nroSolicitud;
	}

	public void setNroSolicitud(Long nroSolicitud) {
		this.nroSolicitud = nroSolicitud;
	}

	public Long getNroTitulo() {
		return nroTitulo;
	}

	public void setNroTitulo(Long nroTitulo) {
		this.nroTitulo = nroTitulo;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public String getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public String getClaseValor() {
		return claseValor;
	}

	public void setClaseValor(String claseValor) {
		this.claseValor = claseValor;
	}

	public String getClaseClaveValor() {
		return claseClaveValor;
	}

	public void setClaseClaveValor(String claseClaveValor) {
		this.claseClaveValor = claseClaveValor;
	}

	public BigDecimal getValorNominal() {
		return valorNominal;
	}

	public void setValorNominal(BigDecimal valorNominal) {
		this.valorNominal = valorNominal;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getParticipante() {
		return participante;
	}

	public void setParticipante(String participante) {
		this.participante = participante;
	}

	public String getCui() {
		return cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getTituloAplicado() {
		return tituloAplicado;
	}

	public void setTituloAplicado(int tituloAplicado) {
		this.tituloAplicado = tituloAplicado;
	}

	public String getFechaConfirmacion() {
		return fechaConfirmacion;
	}

	public void setFechaConfirmacion(String fechaConfirmacion) {
		this.fechaConfirmacion = fechaConfirmacion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setNroSolicitud(long nroSolicitud) {
		this.nroSolicitud = nroSolicitud;
	}

	public void setNroTitulo(long nroTitulo) {
		this.nroTitulo = nroTitulo;
	}

	@Override
	public String toString() {
		return "XmlActivoFinanciero{" + "nroSolicitud=" + nroSolicitud + ", nroTitulo=" + nroTitulo + ", emisor='" + emisor + '\'' + ", fechaSolicitud='" + fechaSolicitud + '\'' + ", claseValor='"
				+ claseValor + '\'' + ", claseClaveValor='" + claseClaveValor + '\'' + ", valorNominal=" + valorNominal + ", moneda='" + moneda + '\'' + ", participante='" + participante + '\''
				+ ", cui='" + cui + '\'' + ", fechaEmision='" + fechaEmision + '\'' + ", fechaVencimiento='" + fechaVencimiento + '\'' + ", fechaConfirmacion='" + fechaConfirmacion + '\''
				+ ", estado='" + estado + '\'' + ", tituloAplicado=" + tituloAplicado + '}';
	}
}