package com.pradera.integration.component.business.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.pradera.integration.component.business.to.VaultControlTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

@Local
public interface VaultControlService{
	
	//public VaultControlTO getBestPosition(Integer cupones, Date fechaVencimiento, Integer securityClass, LoggerUser loggerUser) throws ServiceException;
	
	public void insertCertificates(Integer cupones, Date fechaVencimiento, List<VaultControlTO> certificates, LoggerUser loggerUser) throws ServiceException;
	
	public void deleteCertificates(String idSecurityCodePk) throws ServiceException;
	
	public void deleteCertificates(Long idPhysicalCertificateFk) throws ServiceException;
	
}
