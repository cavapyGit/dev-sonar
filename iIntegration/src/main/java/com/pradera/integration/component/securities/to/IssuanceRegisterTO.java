package com.pradera.integration.component.securities.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.validation.to.OperationInterfaceTO;

public class IssuanceRegisterTO extends OperationInterfaceTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IssuanceObjectTO issuanceObjectTO;
	private List<SecurityObjectTO> lstSecurityObjectTO;
	private SecurityObjectTO securityObjectRef; // not mapped, only for response
	
	public IssuanceObjectTO getIssuanceObjectTO() {
		return issuanceObjectTO;
	}
	public void setIssuanceObjectTO(IssuanceObjectTO issuanceObjectTO) {
		this.issuanceObjectTO = issuanceObjectTO;
	}
	public List<SecurityObjectTO> getLstSecurityObjectTO() {
		return lstSecurityObjectTO;
	}
	public void setLstSecurityObjectTO(List<SecurityObjectTO> lstSecurityObjectTO) {
		this.lstSecurityObjectTO = lstSecurityObjectTO;
	}
	public SecurityObjectTO getSecurityObjectRef() {
		return securityObjectRef;
	}
	public void setSecurityObjectRef(SecurityObjectTO securityObjectRef) {
		this.securityObjectRef = securityObjectRef;
	}
	
	

}
