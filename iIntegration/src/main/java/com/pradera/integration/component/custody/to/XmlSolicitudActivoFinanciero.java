package com.pradera.integration.component.custody.to;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author jquino
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SOLICITUD_ACTIVO_FINANCIERO")
public class XmlSolicitudActivoFinanciero implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "USUARIO_SOLICITANTE")
	private String usuarioSolicitante;
	@XmlElement(name = "TIPO_SOLICITUD")
	private String tipoSolicitud;
	@XmlElement(name = "MOTIVO")
	private String motivo;
	@XmlElement(name = "IP_ALTA")
	private String ipAlta;
	@XmlElementWrapper(name = "SECC_ACTIVOS_FINANCIEROS")
	@XmlElement(name = "ACTIVO_FINANCIERO")
	private List<XmlActivoFinanciero> listActivoFinanciero = null;

	public XmlSolicitudActivoFinanciero() {
		super();
	}
	
	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}
	
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}
	
	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getIpAlta() {
		return ipAlta;
	}

	public void setIpAlta(String ipAlta) {
		this.ipAlta = ipAlta;
	}

	public List<XmlActivoFinanciero> getListActivoFinanciero() {
		return listActivoFinanciero;
	}

	public void setListActivoFinanciero(
			List<XmlActivoFinanciero> listActivoFinanciero) {
		this.listActivoFinanciero = listActivoFinanciero;
	}

	@Override
	public String toString() {
		return "XmlSolicitudActivoFinanciero{" + "fechaSolicitud='"
				 + '\'' + ", usuarioSolicitante='"
				+ usuarioSolicitante + '\'' + ", tipoSolicitud='"
				+ tipoSolicitud + '\'' + ", motivo='" + motivo + '\''
				+ ", ipAlta='" + ipAlta + '\'' + ", listActivoFinanciero="
				+ listActivoFinanciero + '}';
	}
}
