package com.pradera.integration.contextholder;

import java.util.Calendar;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class LoggerUserConstants.
 */
public class LoggerUserConstants {
	
	/** The Constant ID_PRIVILEGE_OF_SYSTEM. */
	public static final Integer ID_PRIVILEGE_OF_SYSTEM = Integer.valueOf(69);
	
	/** The Constant ID_PRIVILEGE_OF_SYSTEM_AUX. */
	public static final Integer ID_PRIVILEGE_OF_SYSTEM_AUX = Integer.valueOf(169);
	
	/** The Constant IP_ADDRESS. */
	public final static String IP_ADDRESS = "192.168.100.41";
	
	/** The Constant USER_NAME. */
	public final static String USER_NAME = "WEBSERVICES";
	
	/**
	 * Current date time.
	 *
	 * @return the date
	 */
	public static Date currentDateTime(){
		Calendar calCurrentDate = Calendar.getInstance();
		return calCurrentDate.getTime();
	}
	
	/**
	 * Gets the logger user.
	 *
	 * @return the logger user
	 */
	public static LoggerUser getLoggerUser() {
		LoggerUser objLoggerUser = new LoggerUser();		
		objLoggerUser.setIdPrivilegeOfSystem(ID_PRIVILEGE_OF_SYSTEM_AUX);		
		objLoggerUser.setAuditTime(currentDateTime());
		objLoggerUser.setIpAddress(IP_ADDRESS);		
		objLoggerUser.setUserName(USER_NAME);
		return objLoggerUser;
	}

}
