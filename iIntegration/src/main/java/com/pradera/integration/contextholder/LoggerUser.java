package com.pradera.integration.contextholder;

import java.io.Serializable;
import java.util.Date;

import com.pradera.integration.usersession.UserAcctions;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class LoggerUser.com.pradera.commons.contextholder.interceptor
 * context holder pattern 
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
public class LoggerUser implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5450764215413251185L;
	
	/** The user name. */
	private String userName;
	
	/** The application name. */
	private String applicationName;
	
	/** The ip address. */
	private String ipAddress;
	
	/** The resource uri. */
	private String resourceUri;
	
	/** The session id. */
	private String sessionId;
	
	/** The id user session pk. */
	private Long idUserSessionPk;
	
	/** The id privilege of system. */
	private Integer idPrivilegeOfSystem;
	
	/** The audit time. */
	private Date auditTime;
	
	/** The user action. */
	private UserAcctions userAction;
	
	private Long idBusinessProcess;
	
	private Integer currentSystemId;

	/**
	 * Instantiates a new logger user.
	 */
	public LoggerUser() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}



	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}



	/**
	 * Gets the application name.
	 *
	 * @return the application name
	 */
	public String getApplicationName() {
		return applicationName;
	}



	/**
	 * Sets the application name.
	 *
	 * @param applicationName the new application name
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}



	/**
	 * Gets the ip address.
	 *
	 * @return the ip address
	 */
	public String getIpAddress() {
		return ipAddress;
	}



	/**
	 * Sets the ip address.
	 *
	 * @param ipAddress the new ip address
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}



	/**
	 * Gets the resource uri.
	 *
	 * @return the resource uri
	 */
	public String getResourceUri() {
		return resourceUri;
	}



	/**
	 * Sets the resource uri.
	 *
	 * @param resourceUri the new resource uri
	 */
	public void setResourceUri(String resourceUri) {
		this.resourceUri = resourceUri;
	}



	/**
	 * Gets the session id.
	 *
	 * @return the session id
	 */
	public String getSessionId() {
		return sessionId;
	}



	/**
	 * Sets the session id.
	 *
	 * @param sessionId the new session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}



	/**
	 * Gets the id user session pk.
	 *
	 * @return the id user session pk
	 */
	public Long getIdUserSessionPk() {
		return idUserSessionPk;
	}



	/**
	 * Sets the id user session pk.
	 *
	 * @param idUserSessionPk the new id user session pk
	 */
	public void setIdUserSessionPk(Long idUserSessionPk) {
		this.idUserSessionPk = idUserSessionPk;
	}



	/**
	 * Gets the id privilege of system.
	 *
	 * @return the id privilege of system
	 */
	public Integer getIdPrivilegeOfSystem() {
		if (idPrivilegeOfSystem==null)idPrivilegeOfSystem=1;
		return idPrivilegeOfSystem;
	}



	/**
	 * Sets the id privilege of system.
	 *
	 * @param idPrivilegeOfSystem the new id privilege of system
	 */
	public void setIdPrivilegeOfSystem(Integer idPrivilegeOfSystem) {
		this.idPrivilegeOfSystem = idPrivilegeOfSystem;
	}



	/**
	 * Gets the audit time.
	 *
	 * @return the audit time
	 */
	public Date getAuditTime() {
		return auditTime;
	}



	/**
	 * Sets the audit time.
	 *
	 * @param auditTime the new audit time
	 */
	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}



	/**
	 * Gets the user action.
	 *
	 * @return the user action
	 */
	public UserAcctions getUserAction() {
		return userAction;
	}



	/**
	 * Sets the user action.
	 *
	 * @param userAction the new user action
	 */
	public void setUserAction(UserAcctions userAction) {
		this.userAction = userAction;
	}



	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}



	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}



	public Integer getCurrentSystemId() {
		return currentSystemId;
	}



	public void setCurrentSystemId(Integer currentSystemId) {
		this.currentSystemId = currentSystemId;
	}
	
	

}
