package com.pradera.integration.exception;

import javax.ejb.ApplicationException;

import com.pradera.integration.exception.type.ErrorServiceType;

@ApplicationException(rollback=true)
public class CustomWebServiceException extends Exception{

	/** The error service. */
	private ErrorServiceType errorService;

	/** The params. */
	private Object[] params;
	
	private Integer httpStatusCode;
	
	public CustomWebServiceException(ErrorServiceType errorType, Object[] params, Integer httpStatusCode) {
		super(errorType.getMessage());
		this.errorService = errorType;
		this.params = params;
		this.httpStatusCode = httpStatusCode;
	}

	public ErrorServiceType getErrorService() {
		return errorService;
	}

	public void setErrorService(ErrorServiceType errorService) {
		this.errorService = errorService;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

	public Integer getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(Integer httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

}
