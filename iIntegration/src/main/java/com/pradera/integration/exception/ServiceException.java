package com.pradera.integration.exception;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.ApplicationException;

import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.exception.type.ErrorServiceType;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ServiceException.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/03/2014
 */
@ApplicationException(rollback=true)
public class ServiceException extends Exception {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7683492789845578577L;
	
	/** The error service. */
	private ErrorServiceType errorService;

	/** The params. */
	private Object[] params;
	
	private List<Object> concatenatedParams;
	
	private RecordValidationType recordValidationType;
	
	/**
	 * Instancia un nuevo service exception.
	 */
	public ServiceException() {
		concatenatedParams = new ArrayList<Object>(0);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * Instantiates a new service exception.
	 *
	 * @param errorType the error type
	 */
	public ServiceException(ErrorServiceType errorType) {
		super(errorType.getMessage());
		this.errorService = errorType;
		this.concatenatedParams = new ArrayList<Object>(0);
	}
	
	
	/**
	 * Instantiates a new service exception.
	 *
	 * @param errorType the error type
	 * @param params the params
	 */
	public ServiceException(ErrorServiceType errorType, Object[] params) {
		super(errorType.getMessage());
		this.errorService = errorType;
		this.params= params;
		this.concatenatedParams = new ArrayList<Object>(0);
	}
	
	/**
	 * Instantiates a new service exception.
	 *
	 * @param errorType the error type
	 * @param message the message
	 */
	public ServiceException(ErrorServiceType errorType, String message) {
		super(message);
		this.errorService = errorType;
		this.concatenatedParams = new ArrayList<Object>(0);
	}


	/**
	 * Instantiates a new service exception.
	 *
	 * @param errorType the error type
	 * @param cause the cause
	 */
	public ServiceException(ErrorServiceType errorType, Throwable cause) {
		super(errorType.getMessage(), cause);
		this.errorService = errorType;
		this.concatenatedParams = new ArrayList<Object>(0);
	}
	
	public ServiceException(RecordValidationType recordValidationType) {
		this.recordValidationType = recordValidationType;
	}

	
	/**
	 * Obtiene error service.
	 *
	 * @return error service
	 */
	public ErrorServiceType getErrorService() {
		return errorService;
	}

	/**
	 * Establece el error service.
	 *
	 * @param errorService el new error service
	 */
	public void setErrorService(ErrorServiceType errorService) {
		this.errorService = errorService;
	}


	/**
	 * Gets the params.
	 *
	 * @return the params
	 */
	public Object[] getParams() {
		return params;
	}


	/**
	 * Sets the params.
	 *
	 * @param params the new params
	 */
	public void setParams(Object[] params) {
		this.params = params;
	}


	public List<Object> getConcatenatedParams() {
		return concatenatedParams;
	}


	public void setConcatenatedParams(List<Object> concatenatedParams) {
		this.concatenatedParams = concatenatedParams;
	}


	public RecordValidationType getRecordValidationType() {
		return recordValidationType;
	}


	public void setRecordValidationType(RecordValidationType recordValidationType) {
		this.recordValidationType = recordValidationType;
	}


}
