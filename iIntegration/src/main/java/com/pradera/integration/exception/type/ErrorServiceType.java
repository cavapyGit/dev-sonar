package com.pradera.integration.exception.type;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum ErrorServiceType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/03/2014
 */
public enum ErrorServiceType{
	
	/** The user not logged. */
	USER_NOT_LOGGED("service.user.not.logged.menssage"),
	
	/** The concurrency lock update. */
	CONCURRENCY_LOCK_UPDATE("persistence.concurrency.lock.general.message"),

	/** The negative balance. */
	NEGATIVE_BALANCE("business.negative.balance"),
	
	/** The business process nofound. */
	BUSINESS_PROCESS_NOFOUND("business.process.nofound"),
	
	/** The operation type nofound. */
	OPERATION_TYPE_NOFOUND("operation.type.nofound"),
	
	/** The movement type nofound. */
	MOVEMENT_TYPE_NOFOUND("movement.type.nofound"),
	
	/** The securities movement type nofound. */
	SECURITIES_MOVEMENT_TYPE_NOFOUND("securities.movement.type.nofound"),
	
	/** The issuance movement type nofound. */
	ISSUANCE_MOVEMENT_TYPE_NOFOUND("issuance.movement.type.nofound"),
	
	/** The negative balance. */
	NO_MARKET_RATE_PRICE_FOUND("notMarketRatePriceFound"),
	
	/** The both market rate price. */
	BOTH_MARKET_RATE_PRICE("bothMarketRatePrice"),
	
	/** The multiple market rate price found. */
	MULTIPLE_MARKET_RATE_PRICE_FOUND("multipleMarketRatePriceFound"),
	
	/** The negative balance. */
	NO_RESULTS_FOUND("notResultFound"),
	
	/** The report not registered. */
	REPORT_NOT_REGISTERED("report.not.registered"),
	
	/** The report not executed. */
	REPORT_NOT_EXECUTED("report.not.executed"),
	
	/** The participant block. */
	PARTICIPANT_BLOCK("participant.error.blocked"),
	
	/** The assignment account holder no accounts pastipant trade. */
	ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE("msg.assignment.account.holder.trade.error.accounts"),
	
	/** The assignment account holder no accounts pastipant incharge. */
	ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE("msg.assignment.account.holder.incharge.error.accounts"),
	
	/** The participant block param. */
	PARTICIPANT_BLOCK_PARAM("participant.error.blocked.param"),
	
	/** The issuance block. */
	ISSUANCE_BLOCK("issuance.error.blocked"),
	
	/** The issuer block. */
	ISSUER_BLOCK("issuer.error.blocked"),
	
	/** The issuer block param. */
	ISSUER_BLOCK_PARAM("issuer.error.blocked.param"),
	
	/** The security block. */
	SECURITY_BLOCK("security.error.blocked"),
	
	/** The security code block. */
	SECURITY_CODE_BLOCK("security.code.error.blocked"),
		   
	/** The mechanism operation state. */
	MECHANISM_OPERATION_STATE("mechanism.operation.state"),
	
	/** The mechanism operation ref numb. */
	MECHANISM_OPERATION_REF_NUMB("mechanism.operation.ref.numb"),
	
	/** The otc operation state. */
	OTC_OPERATION_STATE("otc.operation.state"),
	
	/** The placement without balance. */
	PLACEMENT_WITHOUT_BALANCE("primary.placement.without.balance"),
	
	/** The placement securites negative physical balance. */
	PLACEMENT_SECURITES_NEGATIVE_PHYSICAL_BALANCE("placement.securities.negative.physical.balance"),
	
	/** The concurrency error process. */
	CONCURRENCY_ERROR_PROCESS("concurrency.error.process"),
	
	/** The participant balance not valid. */
	PARTICIPANT_BALANCE_NOT_VALID("participant.balance.notvalid"),
	
	/** The holder account not registered. */
	HOLDER_ACCOUNT_NOT_REGISTERED("error.account.not.registered"),
	
	/** The holder not registered. */
	HOLDER_NOT_REGISTERED("error.holder.not.registered"),
	
	/** The holder without balances. */
	HOLDER_WITHOUT_BALANCES("holder.without.balances"),
	
	/** The holder without holderaccounts. */
	HOLDER_WITHOUT_HOLDERACCOUNTS("holder.without.holderaccounts"),
	
	/** The holder dont select holderaccounts. */
	HOLDER_DONT_SELECT_HOLDERACCOUNTS("holder.dont.select.holderaccounts"),
	
	/** The enforce dont select blockentity. */
	ENFORCE_DONT_SELECT_BLOCKENTITY("enforce.dont.select.blockentity"),
	
	/** The enforce operation cui. */
	ENFORCE_OPERATION_CUI("enforce.operation.cui"),
	
	/** The rectification request exists. */
	RECTIFICATION_REQUEST_EXISTS("rectification.request.exists"),
	
	/** The rectification ownership exchange sucesion not valid. */
	RECTIFICATION_OWNERSHIP_EXCHANGE_SUCESION_NOT_VALID("rectification.ownership.exchange.sucesion.invalid"),
	
	/** The rectification retirement by reversion not valid. */
	RECTIFICATION_RETIREMENT_BY_REVERSION_NOT_VALID("rectification.retirement.reversion.invalid"),
	
	/** The rectification custody operation not confirmed. */
	RECTIFICATION_CUSTODY_OPERATION_NOT_CONFIRMED("rectification.custody.operation.not.confirmed"),
	
	/** The rectification custody operation not found. */
	RECTIFICATION_CUSTODY_OPERATION_NOT_FOUND("rectification.custody.operation.not.found"),
	
	/** The rectification custody operation block invalid. */
	RECTIFICATION_CUSTODY_OPERATION_BLOCK_INVALID("rectification.custody.operation.block.invalid"),
	
	/** The holder account security investor. */
	HOLDER_ACCOUNT_SECURITY_NOT_INVESTOR("error.account.security.notinvestor"),
	
	/** The holder account security investor. */
	HOLDER_ACCOUNT_SECURITY_INVESTOR("error.account.security.investor"),
	
	/** The holder account range amounts. */
	HOLDER_ACCOUNT_RANGE_AMOUNTS("error.account.range.amounts"),
	
	/** The holder account range max amounts. */
	HOLDER_ACCOUNT_RANGE_MAX_AMOUNTS("error.account.range.max.amounts"),
	
	/** The holder account block. */
	HOLDER_ACCOUNT_BLOCK("holderaccount.error.blocked"),
	
	/** The holder account no registered. */
	HOLDER_ACCOUNT_NO_REGISTERED("holderaccount.error.no.regitered"),
	
	/** The holder account block param. */
	HOLDER_ACCOUNT_BLOCK_PARAM("holderaccount.error.blocked.param"),
	
	/** The holder account closed param. */
	HOLDER_ACCOUNT_CLOSED_PARAM("holderaccount.error.closed.param"),
	
	/** The holder with balances. */
	HOLDER_ACCOUNT_WITH_BALANCES("holderaccount.with.balances"),
	
	/** The holder block. */
	HOLDER_BLOCK("holder.error.blocked"),
	
	/** The holder not exist for emisor. */
	HOLDER_NOT_EXIST_FOR_EMISOR("placementsegment.validation.holder.not.exist"),
	
	/** The holder block param. */
	HOLDER_BLOCK_PARAM("holder.error.blocked.param"),
	
	/** The participant mechanism block. */
	PARTICIPANT_MECHANISM_BLOCK("participant.mechanism.error.blocked"),
	
	/** The participant otc mechanism block. */
	PARTICIPANT_OTC_MECHANISM_BLOCK("participant.otc.mechanism.error.blocked"),
	
	/** The security mechanism block. */
	SECURITY_MECHANISM_BLOCK("security.mechanism.error.blocked"),

	/** The assignment process is closed *. */
	ASSIGNMENT_PROCESS_CLOSED("assignmentprocess.error.closed"),
	
	/** The assignment process unexpected state. */
	ASSIGNMENT_PROCESS_UNEXPECTED_STATE("assignmentprocess.error.notExpectedState"),
	
	/** The inconsistency data. */
	INCONSISTENCY_DATA("inconsistence.data"),
	
	/** The trade operation coupon notexist. */
	TRADE_OPERATION_COUPON_NOTEXIST("trade.opeartion.coupon.notexist"),
	
	/** The trade operation capital less coupons. */
	TRADE_OPERATION_CAPITAL_LESS_COUPONS("trade.operation.capital.less.coupons"),
	
	/** The trade operation coupon not pendient. */
	TRADE_OPERATION_COUPON_NOT_PENDIENT("trade.opeartion.coupon.not.pendient"),
	
	/** The participant without accounts. */
	PARTICIPANT_WITHOUT_ACCOUNTS("participant.without.accounts"),
	
	/** The placement segment part sec opened. */
	PLACEMENT_SEGMENT_PART_SEC_OPENED("placement.segment.participant.security.opened"),
	
	/** The cash account notexists. */
	CASH_ACCOUNT_NOTEXISTS("cashaccount.notexists"),
	
	/** The cash account seller notexists. */
	CASH_ACCOUNT_SELLER_NOTEXISTS("error.seller.cashaccount.notexists"),
	
	/** The block operation not blocked. */
	BLOCK_OPERATION_NOT_BLOCKED("blockoperation.error.not.blocked"),
	
	/** The block operation not finded. */
	BLOCK_OPERATION_NOT_FINDED("blockoperation.error.not.finded"),
	
	/** The block operation reblocks exceed total. */
	BLOCK_OPERATION_REBLOCKS_EXCEED_TOTAL("blockoperation.error.reblocks.exceed.total"),
	
	/** The block operation exists securities transfer operation. */
	BLOCK_OPERATION_EXISTS_SECURITIES_TRANSFER_OPERATION("blockoperation.exists.securitiestransfer.operation"),
	
	/** The block operation exists change ownerchip operation. */
	BLOCK_OPERATION_EXISTS_CHANGE_OWNERCHIP_OPERATION("blockoperation.exists.changeownership.operation"),
	
	/** The block operation exists unblock operation. */
	BLOCK_OPERATION_EXISTS_UNBLOCK_OPERATION("blockoperation.exists.unblock.operation"),
	
	/** The block operation exists enforce operation. */
	BLOCK_OPERATION_EXISTS_ENFORCE_OPERATION("blockoperation.exists.enforce.operation"),
	
	/** The block operation exists withdrawal operation. */
	BLOCK_OPERATION_EXISTS_WITHDRAWAL_OPERATION("blockoperation.exists.withdrawal.operation"),
	
	/** The unblock operation not enough amount. */
	UNBLOCK_OPERATION_NOT_ENOUGH_AMOUNT("unblockoperation.not.enough.amount"),
	
	/** The block request state modify. */
	BLOCK_REQUEST_STATE_MODIFY("error.blockrequest.state.modify"),
	
	/** The unblock request state modify. */
	UNBLOCK_REQUEST_STATE_MODIFY("error.unblockrequest.state.modify"),	
	
	/** The participant holder account. */
	PARTICIPANT_HOLDER_ACCOUNT("participant.holder.account.notexists"),

	PARTICIPANT_IN_CHARGE_HOLDER_ACCOUNT("participant.holder.account.in.charge.participant.notexists"),
	
	/** The primary placement not exist. */
	PRIMARY_PLACEMENT_NOT_EXIST("primary.placement.not.exist"),
	
	/** The primary placement not open. */
	PRIMARY_PLACEMENT_NOT_OPEN("primary.placement.not.open"),
	
	/** The primary placement participant incorrect. */
	PRIMARY_PLACEMENT_PARTICIPANT_INCORRECT("primary.placement.participant.not.exist"),
	
	/** The primary placement account not exist. */
	PRIMARY_PLACEMENT_ACCOUNT_NOT_EXIST("primary.placement.account.not.exist"),
	
	/** The primary placement account not exist. */
	BALANCE_WITHOUT_MARKETFACT("balance.without.marketfact"),
	
	/** Error exits operations accreditation. */
	ERROR_EXITS_OPERATIONS_ACREDITATIONS("error.exits.operations.acreditations"),
	
	/** The marketfact ui error. */
	MARKETFACT_UI_ERROR("marketfact.ui.error"),
	
	/** The settlement days out of range. */
	SETTLEMENT_DAYS_OUT_OF_RANGE("negotiation.settlement.days.out.of.range"),
	
	/** The securities not compilances. */
	SECURITIES_NOT_COMPILANCES("securities.not.compilances"),
	
	/** The settlement not working day. */
	SETTLEMENT_NOT_WORKING_DAY("negotiation.settlement.not.working.day"),

	/** The participant mech operation blocked stocks. */
	PARTICIPANT_MECH_OPERATION_BLOCKED_STOCKS("participant.mechoperation.blockedstocks"),
	
	/** The participant mech operation nofounded. */
	PARTICIPANT_MECH_OPERATION_NOFOUNDED("participant.mechoperation.nofounded"),
	
	/** The participant swap operation nofounded. */
	PARTICIPANT_SWAP_OPERATION_NOFOUNDED("participant.swapoperation.nofounded"),
	
	/** The securitie in corpotative operation. */
	SECURITIE_IN_CORPOTATIVE_OPERATION("securitie.in.corporative.operation"),
	
	/** The asset laundering process massive file. */
	ASSET_LAUNDERING_PROCESS_MASSIVE_FILE("error.process.massive.file"),
	
	/** The settlement remove operation. */
	SETTLEMENT_REMOVE_OPERATION("settlement.removeoperation"),
	
	/** The settlement remove operation settled. */
	SETTLEMENT_REMOVE_OPERATION_SETTLED("settlement.removeoperation.settled"),
	
	/** The settlement process modify operation. */
	SETTLEMENT_PROCESS_MODIFY_OPERATION("settlement.process.modify.operation"),
	
	/** The settlement process modify modality. */
	SETTLEMENT_PROCESS_MODIFY_MODALITY("settlement.process.modify.modality"),
	
	/** The settlement process max exec schedule. */
	SETTLEMENT_PROCESS_MAX_EXEC_SCHEDULE("settlement.process.max.executions.schedule"),
	
	/** The settlement process pending schedule. */
	SETTLEMENT_PROCESS_PENDING_SCHEDULE("settlement.process.pending.schedule"),
	
	/** The local country exist. */
	LOCAL_COUNTRY_EXIST("error.local.country.exist"),
	
	/** The corporative process payment remanent exist. */
	CORPORATIVE_PROCESS_PAYMENT_REMANENT_EXIST("importance.event.payment.remanent.exists"),
	
	/** The assignment holder exist. */
	ASSIGNMENT_HOLDER_EXIST("error.assignment.holder.exist"),
	
	/** The holder account incharge state modify. */
	HOLDER_ACCOUNT_INCHARGE_STATE_MODIFY("error.holder.account.incharge.state.modify"),
	
	/** The participant assignment closed. */
	PARTICIPANT_ASSIGNMENT_CLOSED("error.participant.assignment.closed"),
	
	/** The assignment request confirmed. */
	ASSIGNMENT_REQUEST_CONFIRMED("error.assignment.request.confirmed"),
	
	/** The assignment holder account modified. */
	ASSIGNMENT_HOLDER_ACCOUNT_MODIFIED("error.assignment.holderaccount.modified"),
	
	INCHARGE_ASSIGNMENT_ACCOUNT_MASSIVE("incharge.assignment.account.massive"),
	
	/** The assignment account role exists. */
	ASSIGNMENT_ACCOUNT_ROLE_EXISTS("error.assignment.account.role.exist"),
	
	/** The assignment account exists. */
	ASSIGNMENT_ACCOUNT_EXISTS("error.assignment.account.account.exist"),
	
	/** The assignment exceeded quantity. */
	ASSIGNMENT_EXCEEDED_QUANTITY("error.assignment.exceeded.quantity"),
	
	/** The assignment market fact inconsistencies continue. */
	ASSIGNMENT_MARKET_FACT_INCONSISTENCIES_CONTINUE("error.assignment.marketfact.inconsistencies.continue"),
	
	/** The assignment market fact inconsistencies. */
	ASSIGNMENT_MARKET_FACT_INCONSISTENCIES("error.assignment.marketfact.inconsistencies"),
	
	/** The assignment market fact inconsistencies incharge. */
	ASSIGNMENT_MARKET_FACT_INCONSISTENCIES_INCHARGE("error.assignment.marketfact.inconsistencies.incharge"),
	
	/** The assignment pending operations remain. */
	ASSIGNMENT_PENDING_OPERATIONS_REMAIN("error.assignment.pending.operations.remain"),
	
	/** The assignment incomplete securities incharge. */
	ASSIGNMENT_PENDING_INCOMPLETE_SECURITIES("error.assignment.incomplete.securities"),
	
	/** The assignment pending operations remain continue. */
	ASSIGNMENT_PENDING_OPERATIONS_REMAIN_CONTINUE("error.assignment.pending.operations.remain.continue"),
	
	/** The reassignment request exists. */
	REASSIGNMENT_REQUEST_EXISTS("error.reassignment.request.exists"),
	
	/** The reassignment request not changes market fact. */
	REASSIGNMENT_REQUEST_NOT_CANGES_MARKETFACT("error.reassignment.not.changes.marketfact"),
	
	/** The chained operations holder exists as chain. */
	CHAINED_OPERATIONS_HOLDER_EXISTS_AS_CHAIN("chainedoperation.holder.exists.as.chain"),
	
	/** The chained operations operation exists as chain. */
	CHAINED_OPERATIONS_OPERATION_EXISTS_AS_CHAIN("chainedoperation.operation.exists.as.chain"),
	
	/** The block entity off. */
	BLOCK_ENTITY_OFF("blockentity.error.off"),
	
	/** The bank blocked error. */
	BANK_BLOCKED_ERROR("error.bank.blocked"),
	
	/** The begin end day reporting marketfact. */
	BEGIN_END_DAY_REPORTING_MARKETFACT("error.begin.end.day.reporting.marketfact"),
	
	/** The begin end day process end error. */
	BEGIN_END_DAY_PROCESS_END_ERROR("error.begin.end.day.process.end"),
	
	/** The begin end day not exist bank account. */
	BEGIN_END_DAY_NOT_EXIST_BANK_ACCOUNT("error.begin.end.day.notexist.bank.account"),
	
	/** The begin end day exist amount error. */
	BEGIN_END_DAY_EXIST_AMOUNT_ERROR("error.begin.end.day.exist.amount"),
	
	/** The central bank not exist. */
	CENTRAL_BANK_NOT_EXIST("error.central.bank.not.exist"),
	
	/** The cash account related error. */
	CASH_ACCOUNT_RELATED_ERROR("error.cash.account.related"),
	/** The cash account active error. */
	CASH_ACCOUNT_ACTIVE_ERROR("error.cash.account.active"),
	
	/** The cash account register state error. */
	CASH_ACCOUNT_REGISTER_STATE_ERROR("error.cash.account.register.state"),
	
	/** The cash account approve exist. */
	CASH_ACCOUNT_APPROVE_EXIST("error.cash.account.approve.exist"),
	
	/** The cash account active state error. */
	CASH_ACCOUNT_ACTIVE_STATE_ERROR("error.cash.account.active.state"),
	
	/** The cash account deactive error. */
	CASH_ACCOUNT_DEACTIVE_ERROR("error.cash.account.deactive"),
	
	/** The central cash account not active. */
	CENTRAL_CASH_ACCOUNT_NOT_ACTIVE("error.central.cash.account.not.active"),
	
	/** BEGIN MANUAL DEPOSIT FUNDS OPERATION *. */
	MANUAL_DEPOSIT_FUNDS_REQUEST_STATE_MODIFY("error.fundsoperation.state.modify"),

	/** The error allocation benefit process. */
	ERROR_ALLOCATION_BENEFIT_PROCESS("error.allocation.benefit.process"),

	/** The manual deposit funds operation payed. */
	MANUAL_DEPOSIT_FUNDS_OPERATION_PAYED("error.fundsoperation.operation.payed"),
	
	/** The manual deposit funds day not open. */
	MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN("error.fundsoperation.day.not.open"),
	
	/** The manual deposit funds day not open. */
	FUNDS_BANK_ACCOUNT_EXIST("error.funds.bank.account.exist"),
	
	/** The funds bank account currency exist. */
	FUNDS_BANK_ACCOUNT_CURRENCY_EXIST("error.funds.bank.account.currency.exist"),
	
	/** The funds web services not access. */
	FUNDS_WEB_SERVICES_NOT_ACCESS("error.funds.webservices.not.access"),
	
	FUNDS_OPERATION_STOCK_REFERENCE_NULL("error.funds.operation.stock.reference.null"),
	
	/** END MANUAL DEPOSIT FUNDS OPERATION *. */
	HOLDER_EXIST("holder.exist"),
	
	/** The holder not exist. */
	HOLDER_NOT_EXIST("holder.not.exist"),
	
	/** The settlement sanction exists operation. */
	SETTLEMENT_SANCTION_EXISTS_OPERATION("settlement.sanction.operation.exists"),
	
	/** The settlement date request state modify. */
	SETTLEMENT_DATE_REQUEST_STATE_MODIFY("settlement.request.date.state.modify"),
	
	/** The settlement cash account not exist. */
	SETTLEMENT_CASH_ACCOUNT_NOT_EXIST("settlementdaterquest.cashaccount.notexist"),
	
	/** The settlement date request invalid participant. */
	SETTLEMENT_DATE_REQUEST_INVALID_PARTICIPANT("settlement.request.date.invalid.participant"),
	
	/** The settlement request currency exchange duplicate. */
	SETTLEMENT_REQUEST_CURRENCY_EXCHANGE_DUPLICATE("settlement.request.currencyExchange.duplicate"),
	
	/** The settlement request currency exchange reversion duplicate. */
	SETTLEMENT_REQUEST_CURRENCY_EXCHANGE_REVERSION_DUPLICATE("settlement.request.currencyExchange.reversion.duplicate"),
	
	/** The settlement date request anticipation duplicate. */
	SETTLEMENT_DATE_REQUEST_ANTICIPATION_DUPLICATE("settlement.request.date.anticipation.duplicate"),
	
	/** The settlement date request duplicate. */
	SETTLEMENT_DATE_REQUEST_DUPLICATE("settlement.request.date.duplicate"),
	
	/** The settlement process state incorrect. */
	SETTLEMENT_PROCESS_STATE_INCORRECT("settlement.process.state.incorrect"),
	
	/** The settlement process closed. */
	SETTLEMENT_PROCESS_CLOSED("settlement.process.closed"),
	
	/** The settlement process not finish. */
	SETTLEMENT_PROCESS_NOT_FINISH("settlement.process.not.finish"),
	
	/** The settlement process not executed finished. */
	SETTLEMENT_PROCESS_NOT_EXECUTED_FINISHED("settlement.process.not.executed.finished"),
	
	/** The settlement process not executed reinstant. */
	SETTLEMENT_PROCESS_NOT_EXECUTED_REINSTANT("settlement.process.not.reinstant"),
	
	/** The settlement process schedule finished. */
	SETTLEMENT_PROCESS_SCHEDULE_FINISHED("settlement.process.schedule.finished"),
	
	/** The settlement process not valid request. */
	SETTLEMENT_PROCESS_NOT_VALID_REQUEST("settlement.process.not.valid.request"),
	
	/** The settlement process day not start. */
	SETTLEMENT_PROCESS_DAY_NOT_START("settlement.process.day.not.start"),
	
	/** The settlement process day close. */
	SETTLEMENT_PROCESS_DAY_CLOSE("settlement.process.day.close"),
	
	/** The settlement process not available. */
	SETTLEMENT_PROCESS_NOT_AVAILABLE("settlement.process.not.available"),
	
	/** The settlement process final started. */
	SETTLEMENT_PROCESS_FINAL_STARTED("settlement.process.final.started"),
	
	/** The settlement net process final started. */
	SETTLEMENT_NET_PROCESS_FINAL_STARTED("settlement.net.process.final.started"),
	
	/** The settlement process exchange rate. */
	SETTLEMENT_PROCESS_EXCHANGE_RATE("settlement.process.exchange.rate"),
	
	/** The settlement schedule extension not permitted. */
	SETTLEMENT_SCHEDULE_EXTENSION_NOT_PERMITTED("settlement.schedule.extension.not.permitted"),
	
	/** The settlement schedule settlement not permitted. */
	SETTLEMENT_SCHEDULE_SETTLEMENT_NOT_PERMITTED("settlement.schedule.settlement.not.permitted"),
	
	/** The settlement schedule not found. */
	SETTLEMENT_SCHEDULE_NOT_FOUND("settlement.schedule.not.found"),
	
	SETTLEMENT_SCHEDULE_MELID_NOT_PERMITTED("settlement.schedule.melid.not.permitted"),
	
	/** The settlement schedule more than one. */
	SETTLEMENT_SCHEDULE_MORE_THAN_ONE("settlement.schedule.more.one"),
	
	/** The settlement chain operations. */
	SETTLEMENT_CHAIN_OPERATIONS("settlement.chain.operations"),
	
	/** The settlement chain operation state. */
	SETTLEMENT_CHAIN_OPERATION_STATE("settlement.chain.operation.state"),
	
	/** The accreditation operation state confirmed. */
	ACCREDITATION_OPERATION_STATE_CONFIRMED("accreditation.operation.state.confirmed"),
	
	/** The accreditation operation state rejected. */
	ACCREDITATION_OPERATION_STATE_REJECTED("accreditation.operation.state.rejected"),
	
	/** The accreditation operation state unblocked. */
	ACCREDITATION_OPERATION_STATE_UNBLOCKED("accreditation.operation.state.unblocked"),
	
	//SOLICITUDES DE VENCIMIENTO DE CAT
	
	/** The unblock accreditation operation state confirmed. */
	UNBLOCK_ACCREDITATION_OPERATION_STATE_CONFIRMED("accreditation.operation.state.confirmed"),
	
	/** The unblock accreditation operation state registered. */
	UNBLOCK_ACCREDITATION_OPERATION_STATE_REGISTERED("accreditation.operation.state.registered"),
	
	/** The unblock accreditation operation state rejected. */
	UNBLOCK_ACCREDITATION_OPERATION_STATE_REJECTED("accreditation.operation.state.rejected"),
	
	/** The unblock accreditation operation state annulled. */
	UNBLOCK_ACCREDITATION_OPERATION_STATE_ANNULLED("accreditation.operation.state.annulled"),
	
	/** The unblock accreditation operation state approved. */
	UNBLOCK_ACCREDITATION_OPERATION_STATE_APPROVED("accreditation.operation.state.approved"),
	
	/** The unblock accreditation save concurrency. */
	UNBLOCK_ACCREDITATION_SAVE_CONCURRENCY("unblock.accreditation.save.concurrency"),
	
	//SOLICITUDES DE VENCIMIENTO DE CAT
	
	/** The mechanism reference number exists. */
	MECHANISM_REFERENCE_NUMBER_EXISTS("negotiation.referenceNumber.exists"),
	
	/** The mechanism ballot sequential exists. */
	MECHANISM_BALLOT_SEQUENTIAL_EXISTS("negotiation.ballotSequential.exists"),
	
	/** The operation secundary exist report. */
	OPERATION_SECUNDARY_EXIST_REPORT("operation.secundary.exist.report"),
	
	/** The holder account swap notexists. */
	HOLDER_ACCOUNT_SWAP_NOTEXISTS ("holderaccount.swap.notexists"),
	
	/** The external interface query incorrect. */
	EXTERNAL_INTERFACE_QUERY_INCORRECT("external.interface.query.incorrect"),
	
	/** The external interface query column empty. */
	EXTERNAL_INTERFACE_QUERY_COLUMN_EMPTY("external.interface.query.column.empty"),
	
	/** The external interface template incorrect state. */
	EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE("external.interface.template.incorrect.state"),
	
	/** The template incorrect state. */
	TEMPLATE_INCORRECT_STATE("template.incorrect.state"),
	
	/** The swift process ftp connection problem. */
	SWIFT_PROCESS_FTP_CONNECTION_PROBLEM("swift.process.ftp.connection"),
	
	/** The swift process ftp upload temporal dir problem. */
	SWIFT_PROCESS_FTP_UPLOAD_TEMPORAL_DIR_PROBLEM("swift.process.ftp.save.file.temporal.dir"), 
	
	/** The change ownership state modified. */
	CHANGE_OWNERSHIP_STATE_MODIFIED("error.changeownership.state.modify"),
	
	/** The business process notification type error. */
	BUSINESS_PROCESS_NOTIFICATION_TYPE_ERROR("error.business.process.notification.type"),
	
	/** The security isin repeated. */
	SECURITY_ISIN_REPEATED("security.isin.repeated"),
	
	/** The security code repeated. */
	SECURITY_CODE_REPEATED("security.code.repeated"),
	
	SECURITY_COUNTRY_NOT_MATCH_ENCODE("security.country.not.match.encoder"),
	
	/** The security quote isin exist date. */
	SECURITY_QUOTE_ISIN_EXIST_DATE("security.quote.isin.exist.date"),
	
	/** The security quote isin state. */
	SECURITY_QUOTE_ISIN_STATE("security.quote.security.state"),
	
	/** The security cfi code no generate. */
	SECURITY_CFI_CODE_NO_GENERATE("security.cfi.code.no.generate"),
	
	/** The security cfi datos insuficientes */

   SAFETY_CFI_CODE_INSUFFICIENT_DATA("security.cfi.code.data.insufficient"),
     
   /** No se pudo generar la descripsion corta del valor */

   SECURITY_SHORT_CFI_CODE_NO_GENERATE("security.short.cfi.code.data.insufficient"),
   
	/** The security quote issuance state. */
	SECURITY_QUOTE_ISSUANCE_STATE("security.quote.issuance.state"),
	
	/** The security quote issuer state. */
	SECURITY_QUOTE_ISSUER_STATE("security.quote.issuer.state"),
	
	/** The security quote security type. */
	SECURITY_QUOTE_SECURITY_TYPE("security.quote.security.type"),
	
	/** The security quote issuance type. */
	SECURITY_QUOTE_ISSUANCE_TYPE("security.quote.issuance.type"),
	
	/** The security quote security issuance. */
	SECURITY_QUOTE_SECURITY_ISSUANCE("security.quote.security.issuance"),
	
	/** The security quote issuer holder. */
	SECURITY_QUOTE_ISSUER_HOLDER("security.quote.issuer.holder"),
	
	/** The security quote issuer holder state. */
	SECURITY_QUOTE_ISSUER_HOLDER_STATE("security.quote.issuer.holder.state"),
	
	/** The security quote state. */
	SECURITY_QUOTE_STATE("security.quote.state"),
	
	/** The security code incorrect format. */
	SECURITY_CODE_INCORRECT_FORMAT("security.code.incorrect.format"),
	
	/** The security code incorrect when search instrument and security class. */
	SECURITY_CODE_INCORRECT_INSTRUMENT_CLASS("security.code.incorrect.instrument.class"),
	
	/** The security code format issuer. */
	SECURITY_CODE_FORMAT_ISSUER("security.code.format.issuer"),
	
	/** The security code format currency. */
	SECURITY_CODE_FORMAT_CURRENCY("security.code.format.currency"),
	
	/** The security code format numeric. */
	SECURITY_CODE_FORMAT_NUMERIC("security.code.format.numeric"),
	
	/** The security code format term. */
	SECURITY_CODE_FORMAT_TERM("security.code.format.term"),
	
	/** The security code format year. */
	SECURITY_CODE_FORMAT_YEAR("security.code.format.year"),
	
	/** The security code format week. */
	SECURITY_CODE_FORMAT_WEEK("security.code.format.week"),
	
	/** The security code format dash. */
	SECURITY_CODE_FORMAT_DASH("security.code.format.dash"),
	
	/** The security code format alphanumeric. */
	SECURITY_CODE_FORMAT_ALPHANUMERIC("security.code.format.alphanumeric"),
	
	/** The security code format length. */
	SECURITY_CODE_FORMAT_LENGTH("security.code.format.length"),
	
	/** The security excel with formule. */
	SECURITY_EXCEL_WITH_FORMULE("security.excel.with.formule"),
	
	/** Financial indicator not found. */
	FINANCIAL_INDICATOR_NOT_FOUND("financial.indicator.not.found"),
	
	/** Financial indicator not unique res. */
	FINANCIAL_INDICATOR_NOT_UNIQUE_RES("financial.indicator.not.unique.res"),

	/** hours not valid. */
	HOURS_NOT_VALID("hours.no.valid"),
	
	/** The external interface template state. */
	EXTERNAL_INTERFACE_INCORRECT_STATE("external.interface.incorrect.state"), 
	
	/** The security already on corporative operation. */
	SECURITY_ALREADY_ON_CORPORATIVE_OPERATION("security.already.on.corporative.operation.ori"),
	
	/** The security already on corporative operation des. */
	SECURITY_ALREADY_ON_CORPORATIVE_OPERATION_DES("security.already.on.corporative.operation.des"),
	
	/** The holder not registered state. */
	HOLDER_NOT_REGISTERED_STATE("holder.not.registered.state"),
	
	/** The billing service code error. */
	BILLING_SERVICE_CREATE_ERROR("billing.service.save.error"),
	
	/** The billing service code error. */
	BILLING_SERVICE_CODE_ERROR("billing.service.code.error"),
	
	/** The billing service query grammar. */
	BILLING_SERVICE_QUERY_GRAMMAR("billing.service.query.grammar"),
	
	/** The billing service no exist rate actived. */
	BILLING_SERVICE_NO_EXIST_RATE_ACTIVED("billing.service.no.exist.rate.actived"),
	
	/** The billing service no exist rate. */
	BILLING_SERVICE_NO_EXIST_RATE("billing.service.no.exist.rate"),
	
	/** The interface error send file. */
	INTERFACE_ERROR_SEND_FILE("interface.error.send.file"),
	
	/** The processed service not unique in period. */
	PROCESSED_SERVICE_NOT_UNIQUE_IN_PERIOD("processed.service.not.unique.in.period"),
	
	/** The found no exchange. */
	FOUND_NO_EXCHANGE("found.no.change"),
	
	/** The billing process exchange rate. */
	BILLING_PROCESS_EXCHANGE_RATE("billing.process.exchange.rate"),
	
	/** The holder request exist. */
	HOLDER_REQUEST_EXIST("holder.request.exist"),
	
	/*SPECIAL PAYMENT*/
	/** The special payment detail exist. */
	SPECIAL_PAYMENT_DETAIL_EXIST("error.specialpayment.detail.exist"),
	
	/** The special payment header payed. */
	SPECIAL_PAYMENT_HEADER_PAYED("error.specialpayment.header.payed"),
	
	/** The special payment cash account not exist. */
	SPECIAL_PAYMENT_CASH_ACCOUNT_NOT_EXIST("error.specialpayment.cash.account.not.exist"),
	
	/** The special payment insufficient cash. */
	SPECIAL_PAYMENT_INSUFFICIENT_CASH("error.specialpayment.insufficient.cash"),
	
	/** The special payment header state modified. */
	SPECIAL_PAYMENT_HEADER_STATE_MODIFIED("error.specialpayment.header.state.modified"),
	
	/** The special payment devolution cash account not exist. */
	SPECIAL_PAYMENT_DEVOLUTION_CASH_ACCOUNT_NOT_EXIST("error.specialpayment.cash.devolution.account.not.exist"),
	/*SPECIAL PAYMENT*/
	
	/* ALLOCATION PROCESS */
	/** The allocation process holder account error. */
	ALLOCATION_PROCESS_HOLDER_ACCOUNT_ERROR("allocation.holderaccount.error"),
	
	/* PAYMENT SCHEDULE START */
	/** The payment cronogram error int req pending. */
	PAYMENT_CRONOGRAM_ERROR_INT_REQ_PENDING("payment.cronogram.error.int.req.pending"),
	
	/** The payment cronogram error amo req pending. */
	PAYMENT_CRONOGRAM_ERROR_AMO_REQ_PENDING("payment.cronogram.error.amo.req.pending"),
	
	/** The payment cronogram factamort incomplete. */
	PAYMENT_CRONOGRAM_FACTAMORT_INCOMPLETE("payment.cronogram.factarmort.incomplete"),
	/* PAYMENT SCHEDULE START */
	/** The not exchange rate. */
	NOT_EXCHANGE_RATE("not.exchange.rate"),
	
	/** The stock calculation process not exist. */
	STOCK_CALCULATION_PROCESS_NOT_EXIST("stockcalculationprocess.not.exist"),
	
	/** The blockentity exist. */
	BLOCKENTITY_EXIST("blockentity.entity.exist"),
	
	/** The blockentity exist holder. */
	BLOCKENTITY_EXIST_HOLDER("blockentity.holder.entity.exist"),
	
	/** The exist holder. */
	EXIST_HOLDER("blockentity.holder.exist"),
	
	/** The exist holder headler. */
	EXIST_HOLDER_HEADLER("blockentity.holder.exist.headler"),
	
	/** The blockentity exist headler. */
	BLOCKENTITY_EXIST_HEADLER("blockentity.entity.exist.headler"),
	
	/** The blockentity without holder. */
	BLOCKENTITY_WITHOUT_HOLDER("blockentity.without.holder"),
	
	/** The blockentity instinfo notexist. */
	BLOCKENTITY_INSTINFO_NOTEXIST("blockentity.instinfo.not.exist"),
	
	/** The blockentity deactive affec. */
	BLOCKENTITY_DEACTIVE_AFFEC("blockentity.desactive.affectation.exist"),
	
	/** The split coupon request modified state. */
	SPLIT_COUPON_REQUEST_MODIFIED_STATE("error.split.coupon.request.modified.state"),
	
	/** The split coupon request invalid confirm. */
	SPLIT_COUPON_REQUEST_INVALID_CONFIRM("error.split.coupon.request.invalid.confirm"),
	
	/** The split coupon invalid detachment. */
	SPLIT_COUPON_INVALID_DETACHMENT("error.split.coupon.invalid.detachment"),
	
	/** The split coupon invalid marketfact. */
	SPLIT_COUPON_INVALID_MARKETFACT("error.split.coupon.invalid.markefact"),
	
	/** The account annotation operation modified state. */
	ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE("error.account.annotation.operation.modified.state"),
	
	/** The account annotation operation dpf exist. */
	ACCOUNT_ANNOTATION_OPERATION_DPF_EXIST("error.account.annotation.operation.dpf.exist"),
	
	/** The certificate physical not save concurrency. */
	CERTIFICATE_PHYSICAL_NOT_SAVE_CONCURRENCY("error.certificate.physical.not.been.save"),
	
	/** The account annotation operation physical less than quantity. */
	ACCOUNT_ANNOTATION_OPERATION_PHYSICAL_LESS_THAN_QUANTITY("error.account.annotation.operation.physical.less.than.quantity"),
	
	/** The audit process rules exist. */
	AUDIT_PROCESS_RULES_EXIST("error.audit.process.rules.exist"),
	
	/** The security block exchange rate. */
	SECURITY_BLOCK_EXCHANGE_RATE("security.block.exchange.rate"),

	/** The security block exchange rate param. */
	SECURITY_BLOCK_EXCHANGE_RATE_PARAM("security.block.exchange.rate.param"),
	
	/** The security block exchange rate not sel. */
	SECURITY_BLOCK_EXCHANGE_RATE_NOT_SEL("security.block.exchange.rate.not.sel"),
	
	/** The blockoperation cash without cover. */
	BLOCKOPERATION_CASH_WITHOUT_COVER("blockoperation.cash.without.cover"),
	
	/** The enforce operation quantity. */
	ENFORCE_OPERATION_QUANTITY("enforce.operation.quantity"),
	
	/** The enforce operation file. */
	ENFORCE_OPERATION_FILE("enforce.operation.file"),
	
	/** The enforce operation account creditor. */
	ENFORCE_OPERATION_ACCOUNT_CREDITOR("enforce.operation.account.creditor"),
	
	/** The usersession incorrect action. */
	USERSESSION_INCORRECT_ACTION("usersession.incorrect.action"),
	
	/** The neg modality dont exist. */
	NEG_MODALITY_DONT_EXIST("neg.modality.dont.exist"),
	
	/** The neg operation secreport sell. */
	NEG_OPERATION_SECREPORT_SELL("neg.operation.secreport.sell"),
	
	/** The neg operation modality sec instrument. */
	NEG_OPERATION_MODALITY_SEC_INSTRUMENT("neg.operation.modality.sec.instrument"),
	
	/** The neg operation participant without balance. */
	NEG_OPERATION_PARTICIPANT_WITHOUT_BALANCE("neg.operation.participant.without.balance"),
	
	/** The neg operation securitycode mechanism. */
	NEG_OPERATION_SECURITYCODE_MECHANISM("neg.operation.securitycode.mechanism"),
	
	/** The neg operation securitycode private */
	NEG_OPERATION_SECURITYCODE_PRIVATE("neg.operation.securitycode.private"),
	
	/** The neg operation securitycode private authorized*/
	NEG_OPERATION_SECURITYCODE_PRIVATE_AUTHORIZED("neg.operation.securitycode.private.authorized"),
	
	/** neg operation securitycode own account*/
	NEG_OPERATION_SECURITYCODE_OWN_ACCOUNT("neg.operation.securitycode.own.account"),
	
	/** The neg operation primary placement. */
	NEG_OPERATION_PRIMARY_PLACEMENT("neg.operation.primary.placement"),
	
	/** The neg operation security swap exist. */
	NEG_OPERATION_SECURITY_SWAP_EXIST("neg.operation.security.swap.exist"),
	
	/** The neg operation primaryplac block. */
	NEG_OPERATION_PRIMARYPLAC_BLOCK("neg.operation.primaryplac.block"),
	
	/** The neg operation primaryplac participant. */
	NEG_OPERATION_PRIMARYPLAC_PARTICIPANT("neg.operation.primaryplac.participant"),
	
	/** The neg operation primaryplac account empty. */
	NEG_OPERATION_PRIMARYPLAC_ACCOUNT_EMPTY("neg.operation.primaryplac.account.empty"),
	
	/** The neg operation primaryplac account block. */
	NEG_OPERATION_PRIMARYPLAC_ACCOUNT_BLOCK("neg.operation.primaryplac.account.block"),
	
	/** The neg operation primaryplac account null. */
	NEG_OPERATION_PRIMARYPLAC_ACCOUNT_NULL("neg.operation.primaryplac.account.null"),
	
	/** The neg operation primaryplac stockquantity. */
	NEG_OPERATION_PRIMARYPLAC_STOCKQUANTITY("neg.operation.primaryplac.stockquantity"),
	
	/** The neg operation validation circulation. */
	NEG_OPERATION_VALIDATION_CIRCULATION("neg.operation.validation.circulation"),
	
	/** The neg operation validation reposecundary. */
	NEG_OPERATION_VALIDATION_REPOSECUNDARY("neg.operation.validation.reposecundary"),
	
	/** The neg operation validation cleanamount gather. */
	NEG_OPERATION_VALIDATION_CLEANAMOUNT_GATHER("neg.operation.validation.cleanamount.gather"),
	
	/** The neg operation modality inconsistent. */
	NEG_OPERATION_MODALITY_INCONSISTENT("neg.operation.modality.inconsistent"),
	
	/** The neg operation modality settlement days. */
	NEG_OPERATION_MODALITY_SETTLEMENT_DAYS("neg.operation.modality.settlement.days"),
	
	/** The neg operation modality settdate notworking. */
	NEG_OPERATION_MODALITY_SETTDATE_NOTWORKING("neg.operation.modality.settlementdate.notworking"),
	
	/** The neg operation settlementdate security venc. */
	NEG_OPERATION_SETTLEMENTDATE_SECURITY_VENC("neg.operation.settlementdate.securityvenc"),
	
	/** The neg operation settlementdate termdate. */
	NEG_OPERATION_SETTLEMENTDATE_TERMDATE("neg.operation.settlementdate.termdate"),
	
	/** The neg operation settlementdate splitpayment. */
	NEG_OPERATION_SETTLEMENTDATE_SPLITPAYMENT("neg.operation.settlementdate.splitpayment"),
	
	/** The neg operation validation holder duplicated. */
	NEG_OPERATION_VALIDATION_HOLDER_DUPLICATED("neg.operation.validation.holder.duplicated"),
	
	/** The neg operation validation holder account duplicated. */
	NEG_OPERATION_VALIDATION_HOLDERACCOUNT_DUPLICATED("neg.operation.validation.holderaccount.duplicated"),
	
	/** The neg operation validation quantity operation. */
	NEG_OPERATION_VALIDATION_QUANTITY_OPERATION("neg.operation.validation.quantity.operation"),
	
	/** The neg operation validation quantity less. */
	NEG_OPERATION_VALIDATION_QUANTITY_LESS("neg.operation.validation.quantity.operation.less"),
	
	/** The neg operation validation quantity sum. */
	NEG_OPERATION_VALIDATION_QUANTITY_SUM("neg.operation.validation.quantity.sum"),
	
	/** The neg operation validation quantity incharge. */
	NEG_OPERATION_VALIDATION_QUANTITY_INCHARGE("neg.operation.validation.incharge"),
	
	/** The neg operation validation participant assig. */
	NEG_OPERATION_VALIDATION_PARTICIPANT_ASSIG("neg.operation.validation.participant.assignation"),
	
	/** The neg operation validation secundary report empty. */
	NEG_OPERATION_VALIDATION_SECUNDARY_REPORT_EMPTY("neg.operation.validation.sec.repo.empty"),
	
	/** The neg operation validation secundary report quantity. */
	NEG_OPERATION_VALIDATION_SECUNDARY_REPORT_QUANTITY("neg.operation.validation.sec.repo.quantity"),
	
	/** The neg operation validation marketfact. */
	NEG_OPERATION_VALIDATION_MARKETFACT("neg.operation.validation.marketfact"),
	
	/** The neg operation validation marketfact quant sum. */
	NEG_OPERATION_VALIDATION_MARKETFACT_QUANT_SUM("neg.operation.validation.marketfact.quantity.sum"),
	
	/** The neg operation validation account quant sell. */
	NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_SELL("neg.operation.validation.holderaccount.sell"),
	
	/** The neg operation validation account quant sell empty. */
	NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_SELL_EMPTY("neg.operation.validation.holderaccount.sell.empty"),

	/** The neg operation validation account quant buy. */
	NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY("neg.operation.validation.holderaccount.buy"),
	
	/** The neg operation validation account quant buy empty. */
	NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY("neg.operation.validation.holderaccount.buy.empty"),
	
	/** The neg operation validation marketfact incomplete. */
	NEG_OPERATION_VALIDATION_MARKETFACT_INCOMPLETE("neg.operation.validation.marketfact.incomplete"),
	
	/** The neg operation validation termamount less. */
	NEG_OPERATION_VALIDATION_TERMAMOUNT_LESS("neg.operation.validation.term.amount.less"),
	
	/** The neg operation validation supplied amount. */
	NEG_OPERATION_VALIDATION_SUPPLIED_AMOUNT("neg.operation.validation.supplied.amount"),
	
	NEG_OPERATION_VALIDATION_WARRANTY_CAUSE_EMPTY("neg.operation.validation.warranty.cause.empty"),

	NEG_OPERATION_VALIDATION_TERM_INTEREST_RATE("neg.operation.validation.term.interest.rate"),

	NEG_OPERATION_VALIDATION_SETTLEMENT_TERM_DAY("neg.operation.validation.settlement.term.day"),

	NEG_OPERATION_VALIDATION_TERM_PRICE("neg.operation.validation.term.price"),

	NEG_OPERATION_VALIDATION_TERM_AMOUNT("neg.operation.validation.term.amount"),
	
	/** The trade operation acc marketfact different. */
	TRADE_OPERATION_ACC_MARKETFACT_DIFFERENT("trade.operation.acc.marketfact.different"),
	
	/** The trade operation acepted quantity. */
	TRADE_OPERATION_ACEPTED_QUANTITY("trade.operation.acepted.quantity"),
	
	/** The trade operation settlement error. */
	TRADE_OPERATION_SETTLEMENT_ERROR("trade.operation.settlement.error"),
	
	/** The trade operation accounts incorrect. */
	TRADE_OPERATION_ACCOUNTS_INCORRECT("trade.operation.accounts.incorrect"),
	
	/** The trade operation split quantity. */
	TRADE_OPERATION_SPLIT_QUANTITY("trade.operation.split.quantity"),
	
	/** The trade operation split interest coupon. */
	TRADE_OPERATION_SPLIT_INTEREST_COUPON("trade.operation.split.interest.coupon"),
	
	/** The trade operation participant incorrect action. */
	TRADE_OPERATION_PARTICIPANT_INCORRECT_ACTION("trade.operation.participant.incorrect.action"),
	
	/** The trade operation quantity less zero. */
	TRADE_OPERATION_QUANTITY_LESS_ZERO("trade.operation.quantity.less.zero"),
	
	/** The trade operation settlement term. */
	TRADE_OPERATION_SETTLEMENT_TERM("trade.operation.settlement.term"),
	
	/** The trade operation settlement term date. */
	TRADE_OPERATION_SETTLEMENT_TERM_DATE("trade.operation.settlement.term.date"),
	
	/** The valuator config range min. */
	VALUATOR_CONFIG_RANGE_MIN("error.valuator.config.range.min"),
	
	VALUATOR_MARKETFACT_INFO_EMPTY("error.valuator.marketfact.info.empty"),
	
	/** The valuator liveoperation custody. */
	VALUATOR_LIVEOPERATION_CUSTODY("error.valuator.liveoperation.custody"),
	
	/** The valuator liveoperation operation. */
	VALUATOR_LIVEOPERATION_OPERATION("error.valuator.liveoperation.reporting"),
	
	/** The valuator execution incorrect. */
	VALUATOR_EXECUTION_INCORRECT("error.valuator.execution.incorrect"),
	
	/** The valuator security balance transit. */
	VALUATOR_SECURITY_BALANCE_TRANSIT("valuator.security.balance.transit"),
	
	/** The valuator security balance sale. */
	VALUATOR_SECURITY_BALANCE_SALE("valuator.security.balance.sale"),
	
	/** The valuator security balance empty. */
	VALUATOR_SECURITY_BALANCE_EMPTY("valuator.security.balance.empty"),
	
	/** The valuator security balance purchase. */
	VALUATOR_SECURITY_BALANCE_PURCHASE("valuator.security.balance.purchase"),
	
	/** The valuator files not loaded. */
	VALUATOR_FILES_NOT_LOADED("valuator.files.not.loaded"),
	
	/** The valuator files incompleted. */
	VALUATOR_FILES_INCOMPLETED("valuator.files.incompleted"),
	
	/** The valuator begin day not created. */
	VALUATOR_BEGIN_DAY_NOT_CREATED("valuator.beginday.not.created"),
	
	/** The valuator begin day not closed. */
	VALUATOR_BEGIN_DAY_NOT_CLOSED("valuator.beginday.not.closed"),
	
	/** The valuator inconsistencies operation. */
	VALUATOR_INCONSISTENCIES_OPERATION("valuator.inconsistencies.operation"),
	
	/** The valuator config range max. */
	VALUATOR_CONFIG_RANGE_MAX("error.valuator.config.range.max"),
	
	/** The valuator config range dif. */
	VALUATOR_CONFIG_RANGE_DIF("error.valuator.config.range.dif"),
	
	/** The valuator config range less than. */
	VALUATOR_CONFIG_RANGE_LESS_THAN("error.valuator.config.range.less.than"),
	
	/** The valuator config range unselected. */
	VALUATOR_CONFIG_RANGE_UNSELECTED("error.valuator.config.range.unselected"),
	
	/** The valuator config range value. */
	VALUATOR_CONFIG_RANGE_VALUE("error.valuator.config.range.value"),
	
	/** The valuator config range if exists. */
	VALUATOR_CONFIG_RANGE_EXISTS("error.valuator.config.range.exists"),
	
	/** The valuator simulation is empty values balance list rate. */
	VALUATOR_SIMULATION_EMPTY_VALUES_BALANCE_LIST_RATE("error.valuator.simulation.empty.values.balance.list.rate"),
	
	/** The valuator simulation is empty values code list rate. */
	VALUATOR_SIMULATION_EMPTY_VALUES_CODE_LIST_RATE("error.valuator.simulation.empty.values.code.list.rate"),
	
	/** The valuator simulation is empty list rate. */
	VALUATOR_SIMULATION_EMPTY_LIST_RATE("error.valuator.simulation.empty.list.rate"),
	
	/** The valuator simulation unselected. */
	VALUATOR_SIMULATION_UNSELECTED("error.valuator.simulation.unselected"),
	
	/** The valuator security without rate. */
	VALUATOR_SECURITY_WITHOUT_RATE("valuator.security.witout.rate"),
	
	/** The valuator simulation unselected. */
	VALUATOR_SIMULATION_PART_CUI_SECURITY_UNSELECTED("error.valuator.simulation.part.cui.security.unselected"),
	
	/** The security inconsistent. */
	SECURITY_INCONSISTENT("neg.operation.security.inconsistent"),

	/** The security dont have split. */
	SECURITY_DONT_HAVE_SPLIT("security.dont.have.split"),
	
	/** The exchange rate dont exist. */
	EXCHANGE_RATE_DONT_EXIST("settlement.process.exchange.rate"),
	
	/** The exchange rate dont exist usd. */
	EXCHANGE_RATE_DONT_EXIST_USD("settlement.process.exchange.rate.usd"),
	
	/** The exchange rate dont exist ufv. */
	EXCHANGE_RATE_DONT_EXIST_UFV("settlement.process.exchange.rate.ufv"),
	
	/** The exchange rate dont exist dmv. */
	EXCHANGE_RATE_DONT_EXIST_DMV("settlement.process.exchange.rate.dmv"),
	
	/** The enforce operation details. */
	ENFORCE_OPERATION_DETAILS("enforce.operation.details"),
	
	/** The enforce operation detail selected. */
	ENFORCE_OPERATION_DETAIL_SELECTED("enforce.operation.details.selected"),
	
	/** The stock calculation process exist. */
	STOCK_CALCULATION_PROCESS_EXIST("stockcalculationprocess.exist.running.equals"),
	
	/** The corporate event state no valid. */
	CORPORATE_EVENT_STATE_NO_VALID("corporateevent.state.notvalid"),
	
	/** The corporate event not validate. */
	CORPORATE_EVENT_NOT_VALIDATE("alt.corporateevent.notvalid"),
	
	/** The msg corporate event convertibily bone action validate. */
	MSG_CORPORATE_EVENT_CONVERTIBILY_BONE_ACTION_VALIDATE("alt.corporateevent.convertibily.bone.action.notvalid"),
	
	/** The corporate event amortization not validate. */
	CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE("alt.corporateevent.amortization.notvalid"),
	
	/** The corporate event delivery date less. */
	CORPORATE_EVENT_DELIVERY_DATE_LESS("corporateevent.deliverydate.less"),
	
	/** The corporate event not stock calculation. */
	CORPORATE_EVENT_NOT_STOCK_CALCULATION("corporateevent.not.stock"),
	
	/** The corporate event not balance stock calculation. */
	CORPORATE_EVENT_NOT_BALANCE_STOCK_CALCULATION("corporateevent.not.balance.stock"),
	
	/** The corporate event icon balance stock calculation. */
	CORPORATE_EVENT_ICON_BALANCE_STOCK_CALCULATION("corporateevent.incon.balance.stock"),
	
	/** The corporate event security not confirm. */
	CORPORATE_EVENT_SECURITY_NOT_CONFIRM("corporateevent.security.not.confirm"),
	
	/** The corporate event security not balance confirm. */
	CORPORATE_EVENT_SECURITY_NOT_BALANCE_CONFIRM("corporateevent.security.not.balance.confirm"),
	
	/** The corporate event exist settlement operations. */
	CORPORATE_EVENT_EXIST_SETTLEMENT_OPERATIONS("corporateevent.exists.settlement.operations"),
	
	/** The corporate event exist transit balance. */
	CORPORATE_EVENT_EXIST_TRANSIT_BALANCE("corporateevent.exists.transit.balance"),
	
	/** The corporate event not confirmation issuer. */
	CORPORATE_EVENT_NOT_CONFIRMATION_ISSUER("corporateevent.not.confirmation.issue"),
	
	/** The corporate event exist block guaranties. */
	CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES("corporateevent.exist.block.guarantees"),
	
	/** The corporate event not available balance. */
	CORPORATE_EVENT_NOT_AVAILABLE_BALANCE("corporateevent.not.available.balance"),
	
	/** The corporate event exist confirmation depositary. */
	CORPORATE_EVENT_EXIST_CONFIRMATION_DEPOSITARY("corporateevent.exist.confirmation.depositary"),
	
	/** The corporate event not exist confirmation depositary. */
	CORPORATE_EVENT_NOT_EXIST_CONFIRMATION_DEPOSITARY("corporateevent.not.exist.confirmation.depositary"),
	
	/** The corporate event exist adjustments in preliminar. */
	CORPORATE_EVENT_EXIST_ADJUSTMENTS_IN_PRELIMINAR("corporateevent.exist.adjustments.no.considerate.preliminar"),
	
	/** The corporate event issuer amount not equal. */
	CORPORATE_EVENT_ISSUER_AMOUNT_NOT_EQUAL("corporateevent.issuer.amount.not.equal"),
	
	/** The corporate event not exist confirmation definitive. */
	CORPORATE_EVENT_NOT_EXIST_CONFIRMATION_DEFINITIVE("corporateevent.not.exist.confirmation.definitive"),
	
	/** The corporate event paid for error data. */
	CORPORATE_EVENT_PAID_FOR_ERROR_DATA("corporateevent.paid.for.error"),
	
	/** The corporate event delivery date more. */
	CORPORATE_EVENT_DELIVERY_DATE_MORE("corporateevent.deliverydate.more"),
	
	/** The corporate event not executed today. */
	CORPORATE_EVENT_NOT_EXECUTED_TODAY("corporateevent.not.executed.today"),
	
	/** The corporate event exist block guaranties definitive. */
	CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES_DEFINITIVE("corporateevent.exist.block.guaranties.definitive"),
	
	/** The corporate event exist accreditation balance. */
	CORPORATE_EVENT_EXIST_ACCREDITATION_BALANCE("corporateevent.exist.accreditation.balance"),

	
	/** The Billing Service source Info no result data**. */
	BILLING_SERVICE_RECEIPT_SOURCE_INFO_NO_RESULT("billing.service.receipt.source.info.no.result"),
	
	/** The billing service source wrong structured. */
	BILLING_SERVICE_SOURCE_WRONG_STRUCTURED("billing.service.source.wrong.structured"),
	
	/** The billing service source value format. */
	BILLING_SERVICE_SOURCE_VALUE_FORMAT("billing.service.source.value.format"),
	
	/** The billing service source visibility format. */
	BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT("billing.service.source.visibility.format"),
	
	/** The billing service source level format. */
	BILLING_SERVICE_SOURCE_LEVEL_FORMAT("billing.service.source.level.format"),
	
	/** The billing service source position format. */
	BILLING_SERVICE_SOURCE_POSITION_FORMAT("billing.service.source.position.format"),
	
	/** The billing service source type format. */
	BILLING_SERVICE_SOURCE_TYPE_FORMAT("billing.service.source.type.format"),
	
	/** The Not Found Associated Code. */
	BILLING_SERVICE_NOT_FOUND_ASSOCIATED_CODE("billing.service.not.found.associated.code"),
	/** The Not Found Associated Code. */
	BILLING_SERVICE_OTHER_ERROR("billing.service.other.error.type"),
	
	/** Security class valuator exist. */
	VALUATOR_SECURITY_CLASS_EXIST("valuator.security.class.exist"),
	
	/** The valuator securities class empty. */
	VALUATOR_SECURITIES_CLASS_EMPTY("valuator.securities.class.empty"),
	
	/** The valuator config class security state. */
	VALUATOR_CONFIG_CLASS_SECURITY_STATE("valuator.config.class.security.state"),
	
	/** MARKETFACT_FILE. */
	VALUATOR_CORE_MARKETFACT_FILE_EXIST("valuator.core.marketfact.file.exist"),
	
	/** The valuator core marketfact file format invalid. */
	VALUATOR_CORE_MARKETFACT_FILE_FORMAT_INVALID("valuator.core.marketfact.file.format.invalid"),
	
	/** The valuator core marketfact file date invalid. */
	VALUATOR_CORE_MARKETFACT_FILE_DATE_INVALID("valuator.core.marketfact.file.date.invalid"),
	
	/** The valuator core process empty coupons. */
	VALUATOR_CORE_PROCESS_EMPTY_COUPONS("valuator.core.process.empty.coupons"),
	
	/** The valuator core process market rate inconsistente. */
	VALUATOR_CORE_PROCESS_MARKET_RATE_INCONSISTENTE("valuator.core.market.rate.inconsistente"),
	
	/** The valuator core process negative days. */
	VALUATOR_CORE_PROCESS_NEGATIVE_DAYS("valuator.core.process.negative.days"),
	
	/** The valuator core process error te. */
	VALUATOR_CORE_PROCESS_ERROR_TE("valuator.core.process.error.te"),
	
	/** The valuator core process error price. */
	VALUATOR_CORE_PROCESS_ERROR_PRICE("valuator.core.process.error.price"),
	
	/** The valuator core process empty valuatorclass. */
	VALUATOR_CORE_PROCESS_EMPTY_VALUATORCLASS("valuator.core.process.empty.valuatorclass"),
	
	/** The valuator core process incorrect security. */
	VALUATOR_CORE_PROCESS_INCORRECT_SECURITY("valuator.core.process.incorrect.security"),
	
	/** The valuator core process error valuator code. */
	VALUATOR_CORE_PROCESS_ERROR_VALUATOR_CODE("valuator.core.process.error.valuator.code"),
	
	/** Error type by formulate Accounting*. */
	DIVISION_BY_ZERO("division.by.zero"),
	
	/** The missing operand. */
	MISSING_OPERAND("missing.operand"),
	
	/** The missing open parenthesis. */
	MISSING_OPEN_PARENTHESIS("missing.open.parenthesis"),
	
	/** The zero elevated zero. */
	ZERO_ELEVATED_ZERO("zero.elevated.zero.undefined"),
	
	/** The negative exponent. */
	NEGATIVE_EXPONENT("negative.exponent"),
	
	/** The unbalanced parenthesis. */
	UNBALANCED_PARENTHESIS("unbalanced.parenthesis"),
	
	/** The parse error. */
	PARSE_ERROR("parse.error"),
	
	/** The accounting receipt code error. */
	ACCOUNTING_RECEIPT_CODE_ERROR("accounting.receipt.code.error"),

	/** The trade request term days. */
	TRADE_REQUEST_TERM_DAYS("trade.request.term.days"),
	
	/** The trade request term days max. */
	TRADE_REQUEST_TERM_DAYS_MAX("trade.request.term.days.max"),
	
	/** The trade request stock quantity. */
	TRADE_REQUEST_STOCK_QUANTITY("trade.request.stock.quantity"),
	
	/** The accounting source Info no result data**. */
	ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT("accounting.receipt.source.info.no.result"),
	
	/** The accounting source wrong structured. */
	ACCOUNTING_SOURCE_WRONG_STRUCTURED("accounting.source.wrong.structured"),
	
	/** The accounting source value format. */
	ACCOUNTING_SOURCE_VALUE_FORMAT("accounting.source.value.format"),
	
	/** The accounting source visibility format. */
	ACCOUNTING_SOURCE_VISIBILITY_FORMAT("accounting.source.visibility.format"),
	
	/** The accounting source position format. */
	ACCOUNTING_SOURCE_POSITION_FORMAT("accounting.source.position.format"),
	
	/** The accounting source type format. */
	ACCOUNTING_SOURCE_TYPE_FORMAT("accounting.source.type.format"),
	
	/** The Not Found Associated Code. */
	ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE("accounting.not.found.associated.code"),
	/** The Not Found Associated Code. */
	ACCOUNTING_OTHER_ERROR("accounting.other.error.type"),
	/** The Not Found Associated Code. */
	ACCOUNTING_NOT_SOURCE_ERROR("accounting.not.source.error"),
	
	/** The type de associate sent by parameter in Query. */
	ACCOUNTING_SOURCE_TYPE_ASSOCIATE("accounting.source.type.associate"),
	/** The Accounting query grammar. */
	ACCOUNTING_QUERY_GRAMMAR("accounting.query.grammar"),
	
	/** The sirtex withdrawal state error. */
	SIRTEX_WITHDRAWAL_STATE_ERROR("sirtex.withdrawal.state.error"),
	
	/** The web service error sending. */
	WEB_SERVICE_ERROR_SENDING("web.service.error.sending"),
	
	/** The web service error response file. */
	WEB_SERVICE_ERROR_RESPONSE_FILE("web.service.error.response.file"),
	
	/** The web service error process. */
	WEB_SERVICE_ERROR_PROCESS("web.service.error.process"),
	
	/** The web service error notexits bank account edb. */
	WEB_SERVICE_ERROR_NOTEXITS_BANK_ACCOUNT_EDB("web.service.error.notexits.bank.account.edb"),
	
	/** The web service error reception participant. */
	WEB_SERVICE_ERROR_RECEPTION_PARTICIPANT("web.service.error.reception.participant"),
	
	/** The web service error notexits correlative. */
	WEB_SERVICE_ERROR_NOTEXITS_CORRELATIVE("web.service.error.notexits.correlative"),
	
	/** The reject incharges not confirmed. */
	REJECT_INCHARGES_NOT_CONFIRMED("reject.incharges.not.confirmed"),
	
	/** The complete holder account assignment. */
	COMPLETE_HOLDER_ACCOUNT_ASSIGNMENT("complete.holder.account.assignment"),
	
	/** The confirm recorded assignments. */
	CONFIRM_RECORDED_ASSIGNMENTS("confirm.recorded.assigments"),
	
	/** The synchronize settlement account marketfact. */
	SYNCHRONIZE_SETTLEMENT_ACCOUNT_MARKETFACT("synchronize.settlement.account.marketfact"),
	
	/** The close participant assignments. */
	CLOSE_PARTICIPANT_ASSIGNMENTS("close.participant.assignments"),
	
	/** The close assignment process. */
	CLOSE_ASSIGNMENT_PROCESS("close.assignment.process"),
	
	/** The reject all currency settlement request. */
	REJECT_ALL_CURRENCY_SETTLEMENT_REQUEST("reject.all.currency.settlement.request"),
	
	/** The send dpfbbv error insert. */
	SEND_DPFBBV_ERROR_INSERT("send.dpfbbv.error.insert"),
	
	/** The error validate max records. */
	ERROR_VALIDATE_MAX_RECORDS("error.validate.max.records"),
	
	/** Error de monto exedido*/
	EXCEEDED_AMOUNT_ALERT("error.funds.operation.amount.exceeded"),
	
	
	ERROR_CHAINED_QUANTITY_INVALID("error.chained.quantity.invalid"),
	
	/** Restriction operation otc. */
	ERRROR_MESSAGE_RESTRICTION_OPERATION_OTC("error.message.restriction.operation.otc"),
	
	/** Restriction operation otc. */
	ERROR_INSTITUTION_SECURITY_ALREADY_EXISTS("error.institution.security.already.exists"),
	
	INSTITUTION_SECURITY_NOT_FOUND("institution.security.not.found"),

	INSTITUTION_SECURITY_MORE_THAN_ONE_FOUND("institution.security.more.than.one.found"),
	
	ERROR_SECURITY_EXPIRATION_DATE_IS_TODAY("neg.operation.expiration.date.today"),
	;

	/** The message. */
	private String message;
	
	
	/**
	 * Instantiates a new error service type.
	 *
	 * @param message the message
	 */
	private ErrorServiceType(String message) {
		this.setMessage(message);
	}
	
	

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	
}