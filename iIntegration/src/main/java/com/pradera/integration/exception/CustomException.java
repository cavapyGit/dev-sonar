package com.pradera.integration.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class CustomException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CustomException(String message) {
		super(message);
	}

}
