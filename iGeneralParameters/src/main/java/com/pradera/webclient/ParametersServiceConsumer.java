package com.pradera.webclient;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.generalparameters.dailyexchanges.facade.DailyExchangesFacade;
import com.pradera.generalparameters.dailyexchanges.service.DailyExchangesService;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.generalparameters.to.MoneyExchangeTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.RequestDailyExchangeRates;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.CurrencyWSType;
import com.pradera.model.generalparameter.type.DailyExchangeRatesType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;

import bo.com.edv.www.ServiciosClienteBCBProxy;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementServiceConsumer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class ParametersServiceConsumer  extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The daily exchange facade. */
	@EJB
	DailyExchangesFacade dailyExchangeFacade ;
	
	/** The daily exchanges service. */
	@EJB
	DailyExchangesService dailyExchangesService;
	
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/**  User Information. */
	@Inject
	private UserInfo userInfo;
	
	/** The request daily exchange rates. */
	private RequestDailyExchangeRates requestDailyExchangeRates;
		
	/** The server configuration. */		
	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	Properties applicationConfiguration;
	
	
	/** The str services consumer . */
	String exmServicesConsumerBCB = GeneralConstants.EMPTY_STRING;
	
	// issue 1216: no esta mapeado en MasterTableType debido a que es un parametro que solo para el webservice de esta clase
	public static final Integer MASTER_TABLE_WEBSERVICE_EXCHANGE_RATE = 1206;
	public static final Integer END_POINT = 2506;
	public static final Integer NAME_SPACE = 2507;
	public static final Integer METHOD = 2508;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		exmServicesConsumerBCB = applicationConfiguration.getProperty("bcb.exchange.money.consumer");
	}
	
	private List<ParameterTable> lstParametersWebServiceExchangeRateBCB() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MASTER_TABLE_WEBSERVICE_EXCHANGE_RATE);
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	/**
	 * metodo para obtener el XML de respuesta de los webService tipoCambio BCB
	 * @param codIndicador
	 * @param codMoneda
	 * @param fecha
	 * @return
	 */
	public String obtenerTipoCambio(String codIndicador, String codMoneda, String fecha) {
		
		List<ParameterTable> lstParametersWebService=lstParametersWebServiceExchangeRateBCB();
		String endPoint="", nameSpace="", methodName="";
		for (ParameterTable paramWebService : lstParametersWebService) {
			if (paramWebService.getParameterTablePk().equals(END_POINT))
				endPoint = paramWebService.getDescription();
			if (paramWebService.getParameterTablePk().equals(NAME_SPACE))
				nameSpace = paramWebService.getDescription();
			if (paramWebService.getParameterTablePk().equals(METHOD))
				methodName = paramWebService.getDescription();
		}
		
		log.info("END_POINT: "+endPoint);
		log.info("NAME_SPACE: "+nameSpace);
		log.info("METHOD_NAME: "+methodName);

		try {
			// construyendo el servicio
			Service service = new Service();
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(new java.net.URL(endPoint));
			// seteando el parametros de entrada
			call.addParameter("codIndicador", XMLType.XSD_STRING, ParameterMode.IN);
			call.addParameter("codMoneda", XMLType.XSD_STRING, ParameterMode.IN);
			call.addParameter("fecha", XMLType.XSD_STRING, ParameterMode.IN);
			// formato de retorno del resultado (xml)
			call.setReturnType(XMLType.XSD_STRING);
			// tiempo de espera de respuesta del servico (20 segundos)
			call.setTimeout(20000);
			return (String) call.invoke(nameSpace, methodName, new Object[] { codIndicador, codMoneda, fecha });
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	/**
	 * Send currencycode and date to bcb.
	 *
	 * @param moneyExchangeTO 
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 * ws MES
	 */
	public Integer sendExchangeMoney(MoneyExchangeTO moneyExchangeTO, LoggerUser loggerUser) throws ServiceException{
		ServiciosClienteBCBProxy objServiciosClienteBCBProxy = new ServiciosClienteBCBProxy(exmServicesConsumerBCB);				
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		BigDecimal comp=null; Integer moneyExchange = null;	
		BigDecimal vent=null;	
		ProcessFileTO objProcessFileTO = null;
		String strXmlReception=null;
		byte [] arrXmlSend=null;
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.MONEY_EXCHANGE_SEND, null, ComponentConstant.MONEY_EXCHANGE_NAME, Boolean.FALSE);
			objProcessFileTO.setRootTag(ComponentConstant.INTERFACE_TAG_ROWSET);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
					interfaceComponentServiceBean.get().updateExternalInterfaceState(idInterfaceProcess, processState, arrXmlSend, loggerUser);
					log.info(":::::::: INICIO CONSUMO DE SERVICO AL WEBSERVICE BCB::::::::");					
					try {

						// antes se estaba intentando traer el servicio desde el bus
//						strXmlReception = objServiciosClienteBCBProxy.obtenerTipoCambio(moneyExchangeTO.getCurrencyCode(),moneyExchangeTO.getStrDate());
						
						// esto es de prueba, se puede descomentar para desarrollo
//						ConsumerWebService cws=new ConsumerWebService();
//						strXmlReception=cws.obtenerTipoCambio("1", String.valueOf(moneyExchangeTO.getCurrencyCode()), moneyExchangeTO.getStrDate());
						
						strXmlReception=obtenerTipoCambio("1", String.valueOf(moneyExchangeTO.getCurrencyCode()), moneyExchangeTO.getStrDate());
						
						log.error(":::::::: Tipo Cambio xml de respuesta:"+strXmlReception);
					}catch (Exception e) {
						e.printStackTrace();
						log.info("::: Se ha producido un error al consumir lo webservice BCB de tipo de cambio: "+e.getMessage());
					}
					if(Validations.validateIsNotNullAndNotEmpty(strXmlReception)) {
						moneyExchangeTO=parameterServiceBean.getResponseExchangeMoney(strXmlReception, objProcessFileTO);
						if (Validations.validateIsNotNullAndNotEmpty(moneyExchangeTO)) {
							if(moneyExchangeTO.getValue()!=null) {
								comp=moneyExchangeTO.getValue();
							}
						}
					}
					if(Validations.validateIsNotNullAndNotEmpty(moneyExchangeTO)) {
						if(moneyExchangeTO.getCurrencyCode()==CurrencyWSType.USDC.getCode()) { //si es dolar compra
							
							// antes se estaba intentando traer el servicio desde el bus
//							strXmlReception = objServiciosClienteBCBProxy.obtenerTipoCambio(CurrencyWSType.USDV.getCode(),moneyExchangeTO.getStrDate());
							
							// esto es de prueba, se puede descomentar para desarrollo
//							ConsumerWebService cws=new ConsumerWebService();
//							strXmlReception=cws.obtenerTipoCambio("1", String.valueOf(moneyExchangeTO.getCurrencyCode()), moneyExchangeTO.getStrDate());
							
							strXmlReception=obtenerTipoCambio("1", String.valueOf(CurrencyWSType.USDV.getCode()), moneyExchangeTO.getStrDate());
							
							if(Validations.validateIsNotNullAndNotEmpty(strXmlReception)) {
								moneyExchangeTO=parameterServiceBean.getResponseExchangeMoney(strXmlReception, objProcessFileTO);
								if(moneyExchangeTO.getValue()!=null) {
									vent=moneyExchangeTO.getValue();
									moneyExchangeTO.setCurrencyCode(CurrencyWSType.USDC.getCode());
								}
							}
						}
					} 
					if(comp!=null){
						moneyExchange = beforeReg(moneyExchangeTO.getCurrencyCode(), moneyExchangeTO.getStrDate(), comp, vent, loggerUser);
						
						// verificando si el dia siguiente es es sabado, domingo o feriado
						// si fuera el caso se asigna los tipos de cambio pra esos dias
						if (!CurrencyWSType.UFV.getCode().equals(moneyExchangeTO.getCurrencyCode())) {
							DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
							Date calendarDate = format.parse(moneyExchangeTO.getStrDate());
							calendarDate = CommonsUtilities.addDate(calendarDate, 1);
							while (holidayQueryServiceBean.isNonWorkingDayServiceBean(calendarDate, true)){
								moneyExchange = beforeReg(moneyExchangeTO.getCurrencyCode(), format.format(calendarDate), comp, vent, loggerUser);
								calendarDate = CommonsUtilities.addDate(calendarDate, 1);
							}
						}
					}
					
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																						processState, strXmlReception.getBytes(), loggerUser);		
					return moneyExchange;	
			}
			
		} catch (Exception ex) {			
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
		return moneyExchange;
	}
	
	
	public Integer beforeReg(Integer codMoneda, String strDate, BigDecimal valorCompra, BigDecimal valorVenta, LoggerUser loggerUser) 
			throws ServiceException, ParseException{

		requestDailyExchangeRates = new RequestDailyExchangeRates();
//		DailyExchangeRates dailyExchangeRates = new DailyExchangeRates();
//		DailyExchangeRates dailyExchangeRatesDMV = new DailyExchangeRates();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date regDate = format.parse(strDate);
		
		requestDailyExchangeRates.setIdSourceinformation(InformationSourceType.BCRD.getCode());
		requestDailyExchangeRates.setExchangeState(DailyExchangeRatesType.REGISTER.getCode());
		requestDailyExchangeRates.setRegistryUser(loggerUser.getUserName());
//		requestDailyExchangeRates.setConfirmUser(loggerUser.getUserName());
		requestDailyExchangeRates.setIndReferencePrice(GeneralConstants.ONE_VALUE_INTEGER);
//		requestDailyExchangeRates.setConfirmDate(CommonsUtilities.currentDate());
		requestDailyExchangeRates.setRegistryDate(CommonsUtilities.currentDate());
		requestDailyExchangeRates.setDateRate(regDate);

//		dailyExchangeRates.setDateRate(requestDailyExchangeRates.getDateRate());
//		dailyExchangeRates.setIdSourceinformation(requestDailyExchangeRates.getIdSourceinformation());
//		dailyExchangeRates.setIndReferencePrice(requestDailyExchangeRates.getIndReferencePrice());
//		dailyExchangeRates.setAudit(loggerUser);

		if((codMoneda.equals(CurrencyWSType.UFV.getCode()))||(codMoneda.equals(CurrencyWSType.EU.getCode()))){
			requestDailyExchangeRates.setAveragePrice(valorCompra);
			requestDailyExchangeRates.setBuyPrice(valorCompra);
			requestDailyExchangeRates.setSellPrice(valorCompra);
			requestDailyExchangeRates.setReferencePrice(valorCompra);
			
//			dailyExchangeRates.setAveragePrice(requestDailyExchangeRates.getAveragePrice());
//			dailyExchangeRates.setBuyPrice(requestDailyExchangeRates.getBuyPrice());
//			dailyExchangeRates.setReferencePrice(requestDailyExchangeRates.getReferencePrice());
//			dailyExchangeRates.setSellPrice(requestDailyExchangeRates.getSellPrice());
			
			}else{
				if(codMoneda.equals(CurrencyWSType.USDC.getCode())){
					BigDecimal divide=new BigDecimal(2);
					BigDecimal averagePrice=(valorCompra.add(valorVenta)).divide(divide);
					requestDailyExchangeRates.setAveragePrice(averagePrice);
					requestDailyExchangeRates.setBuyPrice(valorCompra);
					requestDailyExchangeRates.setSellPrice(valorVenta);
					requestDailyExchangeRates.setReferencePrice(valorCompra);
					requestDailyExchangeRates.setIdCurrency(CurrencyType.USD.getCode());
					
//					dailyExchangeRates.setAveragePrice(requestDailyExchangeRates.getAveragePrice());
//					dailyExchangeRates.setBuyPrice(requestDailyExchangeRates.getBuyPrice());
//					dailyExchangeRates.setReferencePrice(requestDailyExchangeRates.getReferencePrice());
//					dailyExchangeRates.setSellPrice(requestDailyExchangeRates.getSellPrice());
//					dailyExchangeRates.setIdCurrency(CurrencyType.USD.getCode());
//					
//					dailyExchangeRatesDMV.setAveragePrice(requestDailyExchangeRates.getAveragePrice());
//					dailyExchangeRatesDMV.setBuyPrice(requestDailyExchangeRates.getBuyPrice());
//					dailyExchangeRatesDMV.setDateRate(requestDailyExchangeRates.getDateRate());
//					dailyExchangeRatesDMV.setIdSourceinformation(requestDailyExchangeRates.getIdSourceinformation());
//					dailyExchangeRatesDMV.setIndReferencePrice(requestDailyExchangeRates.getIndReferencePrice());
//					dailyExchangeRatesDMV.setReferencePrice(requestDailyExchangeRates.getReferencePrice());
//					dailyExchangeRatesDMV.setSellPrice(requestDailyExchangeRates.getSellPrice());
//					dailyExchangeRatesDMV.setIdCurrency(CurrencyType.DMV.getCode());
//					dailyExchangeRatesDMV.setAudit(loggerUser);
					}	
				}
		if(codMoneda.equals(CurrencyWSType.EU.getCode())){
			requestDailyExchangeRates.setIdCurrency(CurrencyType.EU.getCode());
//			dailyExchangeRates.setIdCurrency(CurrencyType.EU.getCode());
		}
		if(codMoneda.equals(CurrencyWSType.UFV.getCode())){
			requestDailyExchangeRates.setIdCurrency(CurrencyType.UFV.getCode());
//			dailyExchangeRates.setIdCurrency(CurrencyType.UFV.getCode());
		}
		
		Long reqDailyExchangeRate =  dailyExchangeFacade.searchFiltersData(requestDailyExchangeRates);
		if(reqDailyExchangeRate != 0){
			requestDailyExchangeRates.setDateRate(CommonsUtilities.currentDate());
			log.info("ESTE DIA YA FUE REGISTRADO: "+ strDate + " Moneda: " + requestDailyExchangeRates.getIdCurrency() + " : 430 USD, 1304 EUR, 1734 UFV");
			return 0;
		}else{
//			dailyExchangeFacade.registerDailyExchangesFacade(requestDailyExchangeRates);
//			dailyExchangeFacade.registerDailyExchangeWebService(requestDailyExchangeRates);
			
			dailyExchangesService.registerDailyExchangesFacade(requestDailyExchangeRates);
//			dailyExchangesService.registerDailyExchanges(dailyExchangeRates);
//			if(!(dailyExchangeRatesDMV.getIdCurrency()==null)){
//				dailyExchangesService.registerDailyExchanges(dailyExchangeRatesDMV);
//			}
			return codMoneda;
		}
	}
}
