package com.pradera.webclient;

import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;

public class ConsumerWebService {

	private String END_POINT = "http://ws01.bcb.gob.bo:8080/ServiciosBCB/indicadores";
	// targetNamespace
	private final static String NAME_SPACE = "http://ws.bcb.gob.bo";

	// Nombre metodo
	private final static String METHOD_NAME = "obtenerIndicadorXML";

	public String obtenerTipoCambio(String codIndicador, String codMoneda, String fecha) {

		try {
			// construyendo el servicio
			Service service = new Service();
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(new java.net.URL(END_POINT));
			// seteando el parametros de entrada
			call.addParameter("codIndicador", XMLType.XSD_STRING, ParameterMode.IN);
			call.addParameter("codMoneda", XMLType.XSD_STRING, ParameterMode.IN);
			call.addParameter("fecha", XMLType.XSD_STRING, ParameterMode.IN);
			// formato de retorno del resultado (xml)
			call.setReturnType(XMLType.XSD_STRING);
			// tiempo de espera de respuesta del servico (20 segundos)
			call.setTimeout(20000);
			return (String) call.invoke(NAME_SPACE, METHOD_NAME, new Object[] { codIndicador, codMoneda, fecha });
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
