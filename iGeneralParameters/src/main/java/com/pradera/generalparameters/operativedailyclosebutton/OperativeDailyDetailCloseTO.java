package com.pradera.generalparameters.operativedailyclosebutton;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OperativeDailyDetailCloseTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer processState;
	private Date beginTime;
	private Date registryTime;
	private Date finishTime;
	private Date processDate;
	private Integer percentage;
	private Long eventCode;
	private Integer executionOrder;
	private Integer executionCount;
	private Integer hasError;
	private String errorDetail;
	private Integer indShowDetail;
	private Integer indRetryManual;
	private String eventName;
	private Double rowQuantity;

	private List<OperativeDailyDetailCloseTO> executionList;

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public Long getEventCode() {
		return eventCode;
	}

	public void setEventCode(Long eventCode) {
		this.eventCode = eventCode;
	}

	public Integer getExecutionOrder() {
		return executionOrder;
	}

	public void setExecutionOrder(Integer executionOrder) {
		this.executionOrder = executionOrder;
	}

	public Integer getExecutionCount() {
		return executionCount;
	}

	public void setExecutionCount(Integer executionCount) {
		this.executionCount = executionCount;
	}

	public Integer getHasError() {
		return hasError;
	}

	public void setHasError(Integer hasError) {
		this.hasError = hasError;
	}

	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	public Integer getIndShowDetail() {
		return indShowDetail;
	}

	public void setIndShowDetail(Integer indShowDetail) {
		this.indShowDetail = indShowDetail;
	}

	public Integer getIndRetryManual() {
		return indRetryManual;
	}

	public void setIndRetryManual(Integer indRetryManual) {
		this.indRetryManual = indRetryManual;
	}

	public List<OperativeDailyDetailCloseTO> getExecutionList() {
		return executionList;
	}

	public void setExecutionList(List<OperativeDailyDetailCloseTO> executionList) {
		this.executionList = executionList;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Double getRowQuantity() {
		return rowQuantity;
	}

	public void setRowQuantity(Double rowQuantity) {
		this.rowQuantity = rowQuantity;
	}

	public Date getRegistryTime() {
		return registryTime;
	}

	public void setRegistryTime(Date registryTime) {
		this.registryTime = registryTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventCode == null) ? 0 : eventCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperativeDailyDetailCloseTO other = (OperativeDailyDetailCloseTO) obj;
		if (eventCode == null) {
			if (other.eventCode != null)
				return false;
		} else if (!eventCode.equals(other.eventCode))
			return false;
		return true;
	}
}