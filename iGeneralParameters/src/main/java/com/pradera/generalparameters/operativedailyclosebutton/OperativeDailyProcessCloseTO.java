package com.pradera.generalparameters.operativedailyclosebutton;

import java.util.Date;
import java.util.List;

public class OperativeDailyProcessCloseTO {

	private Integer processState;
	private Date beginTime;
	private Date finishTime;
	private Date processDate;
	private Integer percentage;
	private List<OperativeDailyDetailCloseTO> dailyDetailList;
	public Integer getProcessState() {
		return processState;
	}
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	public List<OperativeDailyDetailCloseTO> getDailyDetailList() {
		return dailyDetailList;
	}
	public void setDailyDetailList(List<OperativeDailyDetailCloseTO> dailyDetailList) {
		this.dailyDetailList = dailyDetailList;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
}