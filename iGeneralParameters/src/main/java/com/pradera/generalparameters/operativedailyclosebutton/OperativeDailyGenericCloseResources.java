package com.pradera.generalparameters.operativedailyclosebutton;

import static com.pradera.model.operativedaily.type.OperativeDailyState.FINALIZED;
import static com.pradera.model.operativedaily.type.OperativeDailyState.PROCESSING;
import static com.pradera.model.operativedaily.type.OperativeDailyState.WAITING;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import com.pradera.commons.processes.scheduler.SynchronizeBatchEvent;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.operativedaily.OperativeDailyDetail;
import com.pradera.model.operativedaily.OperativeDailyLogger;
import com.pradera.model.operativedaily.OperativeDailyProcess;

@Stateless
public class OperativeDailyGenericCloseResources extends CrudDaoServiceBean{

	/**
	 * Gets the operative daily process.
	 *
	 * @param processDate the process date
	 * @return the operative daily process
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyProcessCloseTO getOperativeDailyProcess(Date processDate) throws ServiceException{
		
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select Distinct proc From OperativeDailyProcess proc	");
		querySql.append("Inner Join Fetch proc.operativeDailyDetails det 		");
//		querySql.append("Left Outer Join Fetch  det.operativeDailyLoggers logg 	");
		querySql.append("Where Trunc(proc.processDate) = :proccDate				");
//		querySql.append("Where Trunc(proc.processDate) = :proccDate	And			");
//		querySql.append("	   logg.finishTime Is Not Null						");
		querySql.append("Order By det.executionOrder							");
		
		Map<String,Object> parameters = new HashMap<>();
		parameters.put("proccDate", processDate);

		try{
			return populateOperativeProcess((OperativeDailyProcess) findObjectByQueryString(querySql.toString(), parameters));
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Populate operative process.
	 *
	 * @param operativeProcess the operative process
	 * @return the operative daily process to
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyProcessCloseTO populateOperativeProcess(OperativeDailyProcess operativeProcess) throws ServiceException{
		if(operativeProcess!=null){
			OperativeDailyProcessCloseTO operativeProcessTO = new OperativeDailyProcessCloseTO();
			operativeProcessTO.setBeginTime(operativeProcess.getBeginTime());
			operativeProcessTO.setProcessState(operativeProcess.getProcessState());
			operativeProcessTO.setProcessDate(operativeProcess.getProcessDate());
			operativeProcessTO.setPercentage(ComponentConstant.ZERO);
			operativeProcessTO.setDailyDetailList(new ArrayList<OperativeDailyDetailCloseTO>());
			
			for(OperativeDailyDetail operativeDetail: operativeProcess.getOperativeDailyDetails()){
				Integer executionCount = operativeDetail.getExecutionsCount();
				OperativeDailyDetailCloseTO operativeDetailTO = new OperativeDailyDetailCloseTO();
				operativeDetailTO.setExecutionCount(executionCount);
				operativeDetailTO.setRegistryTime(operativeDetail.getBeginTime());
				operativeDetailTO.setFinishTime(operativeDetail.getFinishTime());
				operativeDetailTO.setRowQuantity(operativeDetail.getRowQuantity());
				operativeDetailTO.setProcessState(operativeDetail.getProcessState());
				operativeDetailTO.setExecutionOrder(operativeDetail.getExecutionOrder());
				operativeDetailTO.setExecutionCount(operativeDetail.getExecutionsCount());
				operativeDetailTO.setEventCode(operativeDetail.getIdOperationDailyDetailPk());
				operativeDetailTO.setEventName(operativeDetail.getOperativeDailySetup().getEventName());
				operativeDetailTO.setBeginTime(executionCount.equals(ComponentConstant.ZERO)?null:operativeDetail.getBeginTime());
				
				operativeDetailTO.setExecutionList(new ArrayList<OperativeDailyDetailCloseTO>());
				operativeProcessTO.getDailyDetailList().add(operativeDetailTO);
				
				if(operativeDetail.getOperativeDailyLoggers()!=null || !operativeDetail.getOperativeDailyLoggers().isEmpty()){
					List<OperativeDailyDetailCloseTO> executionList = new ArrayList<OperativeDailyDetailCloseTO>();
					for(OperativeDailyLogger executionLogger: operativeDetail.getOperativeDailyLoggers()){
						OperativeDailyDetailCloseTO operativeLogger = new OperativeDailyDetailCloseTO();
						
						operativeLogger.setBeginTime(executionLogger.getBeginTime());
						operativeLogger.setFinishTime(executionLogger.getFinishTime());
						if(executionLogger.getErrorDetail()!=null && !executionLogger.getErrorDetail().isEmpty()){
							operativeLogger.setErrorDetail(executionLogger.getErrorDetail().toString());
							operativeLogger.setHasError(BooleanType.YES.getCode());
						}else{
							operativeLogger.setHasError(BooleanType.NO.getCode());
						}
						if(operativeLogger.getFinishTime()!=null){
							operativeLogger.setProcessState(FINALIZED.getCode());
						}else{
							operativeLogger.setProcessState(PROCESSING.getCode());
						}
						
						executionList.add(operativeLogger);
					}
					Collections.sort(executionList,COMPARE_BY_TIME);
					operativeDetailTO.setExecutionList(executionList);
				}
			}
			return operativeProcessTO;
		}else{
			return null;
		}
	}
	
	/**
	 * Update process execution.
	 *
	 * @param event the event
	 * @param processOperative the process operative
	 * @return the operative daily process to
	 * @throws ServiceException the service exception
	 */
	public OperativeDailyProcessCloseTO updateProcessExecution(SynchronizeBatchEvent event, OperativeDailyProcessCloseTO processOperative) throws ServiceException{
		Date finishTime = null;
		Long eventCode = event.getEventTypeCode();
		Integer percentage = event.getPercentage();
		Double rowQuantity = event.getRowQuantity().doubleValue();
		Boolean isBegining = event.getIndBegin() == BooleanType.YES.getCode();
		Boolean isFinishing = event.getIndFinish() == BooleanType.YES.getCode();
		String errorDetail = event.getErrorDetail();
		Boolean isPercetage = event.getPercentage() != ComponentConstant.ZERO && !(isBegining || isFinishing);
		Date beginTime = CommonsUtilities.convertStringtoDate(event.getBeginTime(), CommonsUtilities.DATETIME_PATTERN);
		if(event.getFinishTime()!=null){
			finishTime = CommonsUtilities.convertStringtoDate(event.getFinishTime(), CommonsUtilities.DATETIME_PATTERN);
		}
		
		firstIteration:
		for(OperativeDailyDetailCloseTO operativeDetailTO: processOperative.getDailyDetailList()){
			if(operativeDetailTO.getEventCode().equals(eventCode)){
				if(operativeDetailTO.getExecutionList().isEmpty() || (isBegining)){
					boolean haveLoggerProcesing = false;
					for(OperativeDailyDetailCloseTO loggerTO: operativeDetailTO.getExecutionList()){
						if(loggerTO.getFinishTime()==null){
							haveLoggerProcesing = true;break;
						}
					}
					
					if(!haveLoggerProcesing){
						operativeDetailTO.getExecutionList().add(getOperativeDetailLogger(errorDetail, beginTime, null, percentage, rowQuantity,null));
					}
					
					if(operativeDetailTO.getBeginTime()==null){
						operativeDetailTO.setBeginTime(beginTime);
					}
					operativeDetailTO.setProcessState(PROCESSING.getCode());
					processOperative.setProcessState(PROCESSING.getCode());
				}else{
					for(OperativeDailyDetailCloseTO logger: operativeDetailTO.getExecutionList()){
						if(logger.getFinishTime()==null){
							if(isFinishing){
								logger = getOperativeDetailLogger(errorDetail, null, finishTime, percentage, rowQuantity,logger);
								operativeDetailTO.setProcessState(errorDetail.isEmpty()?FINALIZED.getCode():WAITING.getCode());
								
								if(operativeDetailTO.getProcessState().equals(FINALIZED.getCode())){
									operativeDetailTO.setFinishTime(finishTime);
								}else{
									processOperative.setProcessState(WAITING.getCode());
								}
							}else if(isPercetage){
								logger = getOperativeDetailLogger(errorDetail, null, finishTime, percentage, rowQuantity,logger);
							}
							continue firstIteration;
						}
					}
				}
			}
			operativeDetailTO.setExecutionCount(operativeDetailTO.getExecutionList().size());
			Collections.sort(operativeDetailTO.getExecutionList(),COMPARE_BY_TIME);
		}
		
		if(event.getIsAllFinish().equals(BooleanType.YES.getCode())){
			processOperative.setProcessState(FINALIZED.getCode());
		}
		
		return processOperative;
	}
	
	/**
	 * Gets the operative detail logger.
	 *
	 * @param error the error
	 * @param begTime the beg time
	 * @param finDate the fin date
	 * @param percentage the percentage
	 * @param rowQuantit the row quantit
	 * @param logg the logg
	 * @return the operative detail logger
	 */
	public OperativeDailyDetailCloseTO getOperativeDetailLogger(String error, Date begTime, Date finDate, Integer percentage, Double rowQuantit, OperativeDailyDetailCloseTO logg){
		OperativeDailyDetailCloseTO operativeLogger = (logg!=null?logg:new OperativeDailyDetailCloseTO());
		
		if(begTime!=null){
			operativeLogger.setBeginTime(begTime);
			operativeLogger.setProcessState(PROCESSING.getCode());
		}
		
		if(finDate!=null){
			operativeLogger.setFinishTime(finDate);
			operativeLogger.setProcessState(FINALIZED.getCode());
		}
		
		if(percentage!=null){
			operativeLogger.setPercentage(percentage);
		}
		
		if(error!=null && !error.isEmpty()){
			operativeLogger.setErrorDetail(error);
			operativeLogger.setHasError(BooleanType.YES.getCode());
		}else{
			operativeLogger.setHasError(BooleanType.NO.getCode());
		}
		
		if(rowQuantit!=null){
			operativeLogger.setRowQuantity(rowQuantit);
		}
		
		return operativeLogger;
	}
	
	/** The compare by time. */
	public static Comparator<OperativeDailyDetailCloseTO> COMPARE_BY_TIME = new Comparator<OperativeDailyDetailCloseTO>() {
		 public int compare(OperativeDailyDetailCloseTO o1, OperativeDailyDetailCloseTO o2) {
				/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
		        if ( o1.getBeginTime().before(o2.getBeginTime())) { return -1; }
				return 1;
			}
	    };
	
}