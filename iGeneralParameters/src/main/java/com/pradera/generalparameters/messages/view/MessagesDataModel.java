package com.pradera.generalparameters.messages.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.notification.NotificationLogger;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MessagesDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/10/2013
 */
public class MessagesDataModel extends ListDataModel<NotificationLogger> implements Serializable, SelectableDataModel<NotificationLogger>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7637065202823781101L;



	/**
	 * Default Constructor.
	 */
	public MessagesDataModel(){
		
	}
	
	/**
	 * Parameterized constructor for the MessagesDataModel.
	 *
	 * @param data the data
	 */
	public MessagesDataModel(List<NotificationLogger> data){
		super(data);
	}
	
	/**
	 * This method is used to display the data from backing bean.
	 *
	 * @param rowKey the row key
	 * @return the row data
	 */
    @Override
    @SuppressWarnings("unchecked")
	public NotificationLogger getRowData(String rowKey) {
		List<NotificationLogger> notifications=(List<NotificationLogger>)getWrappedData();
        for(NotificationLogger notification : notifications) {  
            if(String.valueOf(notification.getIdNotificationLoggerPk()).equals(rowKey))
                return notification;  
        }  
		return null;
	}



	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(NotificationLogger notification) {
		return notification.getIdNotificationLoggerPk();
	}
	
}