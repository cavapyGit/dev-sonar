package com.pradera.generalparameters.messages.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notifications.remote.Message;
import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.generalparameters.messages.service.ConsultServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.notification.NotificationLogger;
import com.pradera.model.process.BusinessProcess;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ConsultServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/10/2013
 */
@Stateless
@Performance
public class NotificationManagementServiceFacade implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The ConsultServiceBean. */
	@EJB
	ConsultServiceBean consultServiceBean;
	
	/** The notification service. */
	@EJB
	NotificationServiceBean notificationService;
	
	
	/** The log. */
	@Inject
	PraderaLogger log;
	

	/**
	 * Gets the Message List according to Start Date and Form Date.
	 *
	 * @param filter the filter
	 * @return Messages List
	 * @throws ServiceException the service exception
	 */
	public List<Message> getNotifications(MessageFilter filter) throws ServiceException {
		List<NotificationLogger> notifications=consultServiceBean.searchNotificationsMessages(filter);
		List<Message> messageList = new ArrayList<>();
		for(NotificationLogger notification : notifications){
			Message message = new Message();
			if(NotificationType.MESSAGE.getCode().equals(filter.getNotificationType())){
				message.setIdMessage(notification.getIdNotificationLoggerPk());
			} else if(NotificationType.PROCESS.getCode().equals(filter.getNotificationType())){
				message.setIdMessage(notification.getProcessLogger().getIdProcessLoggerPk());
			}
			message.setMessageSubject(notification.getNotificationSubject());
			message.setMessageBody(notification.getNotificationMessage());
			message.setNotificationType(notification.getNotificationType());
			message.setMessageReceptor(notification.getDestUserName());
			message.setMessageIssuer(notification.getSourceUserName());
			message.setRegistryDate(notification.getRegistryDate());
			messageList.add(message);
		}
		return messageList;
	}

	

	/**
	 * Send notification remote.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param businessProcess the business process
	 * @param users the users
	 * @param subject the subject
	 * @param message the message
	 * @param notification the notification
	 */
	public void sendNotificationRemote(LoggerUser loggerUser, String userName,
			Long businessProcess, List<UserAccountSession> users,
			String subject, String message, NotificationType notification) {
		log.info("Start  method sendNotificationRemote() ");
		BusinessProcess businessProc = new BusinessProcess();
		businessProc.setIdBusinessProcessPk(businessProcess);
		notificationService.sendNotificationManual(loggerUser, userName, businessProc, users, subject, message, notification);
		log.info("End  method sendNotificationRemote() ");
	}


	/**
	 * Update notification status.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param notifications the notifications
	 * @param status the status
	 * @param notificationType the notification type
	 * @throws ServiceException the service exception
	 */
	public void updateNotificationStatus(LoggerUser loggerUser,
			String userName, List<Long> notifications, Integer status,
			Integer notificationType) throws ServiceException {
		// TODO Auto-generated method stub
		notificationService.updateNotificationStatus(loggerUser, userName, notifications, status, notificationType);
	}
	
	/**
	 * Send notification configurated.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param businessProcess the business process
	 * @param intitutionCode the intitution code
	 * @param parameters the parameters
	 */
	public void sendNotificationConfigurated(LoggerUser loggerUser, String userName, Long businessProcess, Object intitutionCode, Object[] parameters){
		log.info("Start  method sendNotificationConfigurated() ");
		BusinessProcess businessProc = new BusinessProcess();
		businessProc.setIdBusinessProcessPk(businessProcess);
		notificationService.sendNotification(loggerUser, userName, businessProc, intitutionCode, parameters);
	}

}
