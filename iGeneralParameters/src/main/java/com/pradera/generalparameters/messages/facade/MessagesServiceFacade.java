package com.pradera.generalparameters.messages.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.security.model.type.ExternalInterfaceType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.InstitutionType;
import com.pradera.core.framework.usersaccount.service.UserAccountControlServiceBean;
import com.pradera.generalparameters.messages.service.ConsultServiceBean;
import com.pradera.model.notification.NotificationLogger;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * Session Bean implementation class MessagesServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class MessagesServiceFacade {
	
	/** The user account control service. */
	@EJB
	UserAccountControlServiceBean userAccountControlService;
	
	/** The consult service. */
	@EJB
	ConsultServiceBean consultService;
	
	
	/**
     * Gets the users from institution.
     *
     * @param institution the institution
     * @param institutionCode the institution code
     * @return the users from institution
     */
    public List<UserAccountSession> getUsersFromInstitution(InstitutionType institution,Object institutionCode){
    	return userAccountControlService.getUsersInformation(null, institution, institutionCode, null,null,ExternalInterfaceType.NO.getCode());
    	
    }
    
    /**
     * Gets the business process level.
     *
     * @param name the name
     * @param level the level
     * @return the business process level
     */
    public List<BusinessProcess> getBusinessProcessLevel(String name,Integer level){
    	return consultService.getBusinessProcessLevel(name, level);
    }
    
    /**
     * Search notifications messages.
     *
     * @param message the message
     * @return the list
     */
    public List<NotificationLogger> searchNotificationsMessages(MessageFilter message){
    	return consultService.searchNotificationsMessages(message);
    }

    /**
     * Default constructor. 
     */
    public MessagesServiceFacade() {
        // TODO Auto-generated constructor stub
    }

}
