package com.pradera.generalparameters.messages.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.SelectEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.generalparameters.messages.facade.MessagesServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.notification.NotificationLogger;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MessagesInboxBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/10/2013
 */
@ViewDepositaryBean
public class MessagesInboxBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5403176512937491197L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The messages facade. */
	@EJB
	MessagesServiceFacade messagesFacade;
	
	/** The country residence. */
	@Inject @Configurable
	private Integer countryResidence; 
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The id notification logger. */
	private Long idNotificationLogger;
	
	/** The list users types. */
	private List<SelectItem> listUsersTypes;
	
	/** The list institutions. */
	private List<SelectItem> listInstitutions;
	
	/** The users accounts. */
	private List<UserAccountSession> usersAccounts;
	
	/** The users accounts aux. */
	private List<UserAccountSession> usersAccountsAux;
	
	/** The user type. */
	private Integer userType;
	
	/** The institution type. */
	private Object institutionType;
	
	/** The user selected. */
	private String userSelected;
	
	/** The notification logger. */
	private NotificationLogger notificationLogger;
	
	/** The message filter. */
	private MessageFilter messageFilter;
	
	/** The notifications. */
	private List<NotificationLogger> notifications;
	
	/** The levels process. */
	private List<SelectItem> levelsProcess;
	
	/** The business process. */
	private List<BusinessProcess> businessProcess;
	
	/** The process name. */
	private String processName;
	
	/** The messages data model. */
	private MessagesDataModel messagesDataModel;
	
	/** The bl requiried. */
	private boolean blRequiried;
	
	/**
	 * Load.
	 */
	public void load(){
		FacesContext faces =FacesContext.getCurrentInstance();
		if(!faces.isPostback()){
			if(idNotificationLogger != null){
				//load detail about notification
				messageFilter.setBeginDate(null);
				messageFilter.setEndDate(null);
				messageFilter.setIdNotificationLogger(idNotificationLogger);
				notifications = messagesFacade.searchNotificationsMessages(messageFilter);
				messagesDataModel = new MessagesDataModel(notifications);
				if(notifications != null && !notifications.isEmpty()){
					notificationLogger = notifications.get(0);
				}
				//return state filters 
				messageFilter.setBeginDate(CommonsUtilities.currentDate());
				messageFilter.setEndDate(CommonsUtilities.currentDate());
				messageFilter.setIdNotificationLogger(null);
			}
		}
	}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		listUsersTypes = new ArrayList<>();
		messageFilter = new MessageFilter();
		messageFilter.setBeginDate(CommonsUtilities.currentDate());
		messageFilter.setEndDate(CommonsUtilities.currentDate());
		for(InstitutionType institution : InstitutionType.list){
			listUsersTypes.add(new SelectItem(institution.getCode(), institution.getValue()));
		}
		usersAccounts= new ArrayList<>();
		listInstitutions = new ArrayList<>();
		//process
		levelsProcess= new ArrayList<>();
		for(int i=0;i<4;i++){
			levelsProcess.add(new SelectItem(Integer.valueOf(i),String.valueOf(i)));
		}
		//holidays
		 Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);
		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		  
		  validateUserType();
		  selectedUsser();
	}
	
	/**
	 * Validate user type.
	 */
	private void  validateUserType() {
		if(!isShowAllFilters()) {
			userType = userInfo.getUserAccountSession().getInstitutionType();
			institutionType = userInfo.getUserAccountSession().getInstitutionCode();
			
			InstitutionType institution = InstitutionType.get(userType);			
			usersAccountsAux = messagesFacade.getUsersFromInstitution(institution, institutionType);
		} else {
			userType = null;
			institutionType = null;
			usersAccountsAux = messagesFacade.getUsersFromInstitution(null, null);
		}
	}
	
	/**
	 * Selected usser.
	 */
	public void selectedUsser(){ 
		try{
			userType = userInfo.getUserAccountSession().getInstitutionType();
			institutionType = userInfo.getUserAccountSession().getInstitutionCode();
			InstitutionType institution = InstitutionType.get(userType);			
			usersAccountsAux = messagesFacade.getUsersFromInstitution(institution, institutionType);
			selectUserTypes();
			userSelected = userInfo.getUserAccountSession().getUserName();
			isDisabledInstitutionSearch();
		} catch(ServiceException ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}
	
	/**
	 * Select user types.
	 *
	 * @throws ServiceException the service exception
	 */
	public void selectUserTypes() throws ServiceException{
		usersAccountsAux = null;
		blRequiried = false;
		if(userType!= null){
			InstitutionType institution = InstitutionType.get(userType);
			switch (institution) {
			case AFP:
			case PARTICIPANT:	
				List<Participant> participantList = helperComponentFacade.findParticipantNativeQueryFacade(new Participant());
				listInstitutions.clear();
				for(Participant participant:participantList){
					listInstitutions.add(new SelectItem((Object)participant.getIdParticipantPk(), participant.getDescription()+"-"+participant.getIdParticipantPk()));
				}
				usersAccounts.clear();
				break;
			case ISSUER:
				List<IssuerTO> issuerList = helperComponentFacade.findIssuerByHelper(new IssuerSearcherTO());
				listInstitutions.clear();
				for(IssuerTO issuer : issuerList){
					listInstitutions.add(new SelectItem((Object)issuer.getIssuerCode(), issuer.getIssuerDesc(),issuer.getIssuerCode()));
				}
				usersAccounts.clear();
				break;
			default:
				usersAccounts = messagesFacade.getUsersFromInstitution(institution, null);					
				listInstitutions.clear();
				blRequiried = true;
				break;
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)) {
				usersAccountsAux = usersAccounts;
			} else {
				usersAccountsAux = messagesFacade.getUsersFromInstitution(institution, null);
			}
			
		} else {
			listInstitutions.clear();
			usersAccounts.clear();
		}
	}
	
	/**
	 * Select institution.
	 */
	public void selectInstitution(){
		usersAccountsAux = null;
		if(institutionType!=null){
			InstitutionType institution = InstitutionType.get(userType);
			String issuerCode = null;
			Long participantCode = null;
			if(institution.equals(InstitutionType.ISSUER)){
				issuerCode = institutionType.toString();
				usersAccounts = messagesFacade.getUsersFromInstitution(institution, issuerCode);
			} else if(institution.equals(InstitutionType.PARTICIPANT) || institution.equals(InstitutionType.AFP)){
				participantCode = Long.valueOf(institutionType.toString());
				usersAccounts = messagesFacade.getUsersFromInstitution(institution, participantCode);
			}						
			
		}else{
			usersAccounts.clear();
		}
		
		usersAccountsAux = usersAccounts;
	}
	
	/**
	 * Complete process.
	 *
	 * @param query the query
	 * @return the list
	 */
	public List<BusinessProcess> completeProcess(String query){
		businessProcess =  messagesFacade.getBusinessProcessLevel(query, messageFilter.getProcessLevel());
		return businessProcess;
	}
	
	/**
	 * Search messages.
	 */
	@LoggerAuditWeb
	public void searchMessages(){
		if(isShowAllFilters()){			
			if(StringUtils.isNotBlank(userSelected)){
				messageFilter.setUserReceptor(userSelected);
			} else {								
				if(Validations.validateListIsNotNullAndNotEmpty(usersAccountsAux)) {
					messageFilter.setLstUserNames(getListUsernames(usersAccountsAux));
				} else {
					notifications = new ArrayList<NotificationLogger>();
					messagesDataModel = new MessagesDataModel(notifications);
					return;
				}						
			}							
					
		} else {
			messageFilter.setUserReceptor(userInfo.getUserAccountSession().getUserName());
		}
		notifications = messagesFacade.searchNotificationsMessages(messageFilter);
		messagesDataModel = new MessagesDataModel(notifications);
		notificationLogger = null;
	}
	
	/**
	 * Gets the list usernames.
	 *
	 * @param lstUserAccounts the lst user accounts
	 * @return the list usernames
	 */
	private List<String> getListUsernames(List<UserAccountSession> lstUserAccounts) {
		if(Validations.validateListIsNotNullAndNotEmpty(lstUserAccounts)) {
			List<String> lstUsernameTmp = new ArrayList<String>();
			
			for (UserAccountSession objUserAccount : lstUserAccounts) {
				lstUsernameTmp.add(objUserAccount.getUserName());
			}
			
			return lstUsernameTmp;
			
		} else {
			return null;
		}
	}
	
	/**
	 * On row select.
	 *
	 * @param event the event
	 */
	public void onRowSelect(SelectEvent event) {  
        FacesMessage msg = new FacesMessage("Notification Selected", ((NotificationLogger) event.getObject()).getNotificationSubject());  
  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }  
	
	/**
	 * Clean filters.
	 */
	public void cleanFilters(){
		messageFilter = new MessageFilter();
		messageFilter.setBeginDate(CommonsUtilities.currentDate());
		messageFilter.setEndDate(CommonsUtilities.currentDate());
		notificationLogger = null;
		idNotificationLogger = null;
		if(notifications!=null){
			notifications.clear();
		}
		messagesDataModel = null;
		userType = null;
		institutionType = null;
		userSelected = null;
		if(usersAccounts!=null) {
			usersAccounts.clear();
		}		
	}
	
	/**
	 * Change process level.
	 */
	public void changeProcessLevel(){
		messageFilter.setProcessName(null);
	}
	
	/**
	 * Checks if is show all filters.
	 *
	 * @return true, if is show all filters
	 */
	public boolean isShowAllFilters(){
		return userInfo.getUserAccountSession().isDepositaryInstitution() || 
				userInfo.getUserAccountSession().isStockExchange() ||
				userInfo.getUserAccountSession().isFinanceMinistry();
	}
	
	/**
	 * Checks if is disabled user.
	 *
	 * @return true, if is disabled user
	 */
	public boolean isDisabledUser(){
		if(userInfo.getUserAccountSession().isDepositaryInstitution() || 
				userInfo.getUserAccountSession().isStockExchange() ||
				userInfo.getUserAccountSession().isFinanceMinistry()){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is disabled institution search.
	 *
	 * @return true, if is disabled institution search
	 */
	public boolean isDisabledInstitutionSearch() {
		return !(InstitutionType.AFP.getCode().equals(userType) || 
		InstitutionType.PARTICIPANT.getCode().equals(userType) ||
		InstitutionType.ISSUER.getCode().equals(userType)); 
	}

	/**
	 * Instantiates a new messages inbox bean.
	 */
	public MessagesInboxBean() {
	}



	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}



	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}



	/**
	 * Gets the log.
	 *
	 * @return the log
	 */
	public PraderaLogger getLog() {
		return log;
	}



	/**
	 * Sets the log.
	 *
	 * @param log the new log
	 */
	public void setLog(PraderaLogger log) {
		this.log = log;
	}



	/**
	 * Gets the list users types.
	 *
	 * @return the list users types
	 */
	public List<SelectItem> getListUsersTypes() {
		return listUsersTypes;
	}



	/**
	 * Sets the list users types.
	 *
	 * @param listUsersTypes the new list users types
	 */
	public void setListUsersTypes(List<SelectItem> listUsersTypes) {
		this.listUsersTypes = listUsersTypes;
	}



	/**
	 * Gets the list institutions.
	 *
	 * @return the list institutions
	 */
	public List<SelectItem> getListInstitutions() {
		return listInstitutions;
	}



	/**
	 * Sets the list institutions.
	 *
	 * @param listInstitutions the new list institutions
	 */
	public void setListInstitutions(List<SelectItem> listInstitutions) {
		this.listInstitutions = listInstitutions;
	}



	/**
	 * Gets the users accounts.
	 *
	 * @return the users accounts
	 */
	public List<UserAccountSession> getUsersAccounts() {
		return usersAccounts;
	}



	/**
	 * Sets the users accounts.
	 *
	 * @param usersAccounts the new users accounts
	 */
	public void setUsersAccounts(List<UserAccountSession> usersAccounts) {
		this.usersAccounts = usersAccounts;
	}



	/**
	 * Gets the user type.
	 *
	 * @return the user type
	 */
	public Integer getUserType() {
		return userType;
	}



	/**
	 * Sets the user type.
	 *
	 * @param userType the new user type
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}



	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Object getInstitutionType() {
		return institutionType;
	}



	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Object institutionType) {
		this.institutionType = institutionType;
	}



	/**
	 * Gets the user selected.
	 *
	 * @return the user selected
	 */
	public String getUserSelected() {
		return userSelected;
	}



	/**
	 * Sets the user selected.
	 *
	 * @param userSelected the new user selected
	 */
	public void setUserSelected(String userSelected) {
		this.userSelected = userSelected;
	}



	/**
	 * Gets the notification logger.
	 *
	 * @return the notification logger
	 */
	public NotificationLogger getNotificationLogger() {
		return notificationLogger;
	}



	/**
	 * Sets the notification logger.
	 *
	 * @param notificationLogger the new notification logger
	 */
	public void setNotificationLogger(NotificationLogger notificationLogger) {
		this.notificationLogger = notificationLogger;
	}



	/**
	 * Gets the message filter.
	 *
	 * @return the message filter
	 */
	public MessageFilter getMessageFilter() {
		return messageFilter;
	}



	/**
	 * Sets the message filter.
	 *
	 * @param messageFilte the new message filter
	 */
	public void setMessageFilter(MessageFilter messageFilte) {
		this.messageFilter = messageFilte;
	}



	/**
	 * Gets the notifications.
	 *
	 * @return the notifications
	 */
	public List<NotificationLogger> getNotifications() {
		return notifications;
	}



	/**
	 * Sets the notifications.
	 *
	 * @param notifications the new notifications
	 */
	public void setNotifications(List<NotificationLogger> notifications) {
		this.notifications = notifications;
	}

	/**
	 * Gets the levels process.
	 *
	 * @return the levels process
	 */
	public List<SelectItem> getLevelsProcess() {
		return levelsProcess;
	}

	/**
	 * Sets the levels process.
	 *
	 * @param levelsProcess the new levels process
	 */
	public void setLevelsProcess(List<SelectItem> levelsProcess) {
		this.levelsProcess = levelsProcess;
	}

	/**
	 * Gets the business process.
	 *
	 * @return the business process
	 */
	public List<BusinessProcess> getBusinessProcess() {
		return businessProcess;
	}

	/**
	 * Sets the business process.
	 *
	 * @param businessProcess the new business process
	 */
	public void setBusinessProcess(List<BusinessProcess> businessProcess) {
		this.businessProcess = businessProcess;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the process name
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName the new process name
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * Gets the id notification logger.
	 *
	 * @return the id notification logger
	 */
	public Long getIdNotificationLogger() {
		return idNotificationLogger;
	}

	/**
	 * Sets the id notification logger.
	 *
	 * @param idNotificationLogger the new id notification logger
	 */
	public void setIdNotificationLogger(Long idNotificationLogger) {
		this.idNotificationLogger = idNotificationLogger;
	}

	/**
	 * Gets the messages data model.
	 *
	 * @return the messages data model
	 */
	public MessagesDataModel getMessagesDataModel() {
		return messagesDataModel;
	}

	/**
	 * Sets the messages data model.
	 *
	 * @param messagesDataModel the new messages data model
	 */
	public void setMessagesDataModel(MessagesDataModel messagesDataModel) {
		this.messagesDataModel = messagesDataModel;
	}

	/**
	 * Checks if is bl requiried.
	 *
	 * @return the blRequiried
	 */
	public boolean isBlRequiried() {
		return blRequiried;
	}

	/**
	 * Sets the bl requiried.
	 *
	 * @param blRequiried the blRequiried to set
	 */
	public void setBlRequiried(boolean blRequiried) {
		this.blRequiried = blRequiried;
	}

	

}
