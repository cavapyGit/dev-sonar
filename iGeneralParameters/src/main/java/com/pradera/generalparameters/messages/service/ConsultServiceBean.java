package com.pradera.generalparameters.messages.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.notification.NotificationLogger;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ConsultServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/10/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class ConsultServiceBean extends CrudDaoServiceBean {
	

	/**
	 * Gets the business process level.
	 *
	 * @param name the name
	 * @param level the level
	 * @return the business process level
	 */
	public List<BusinessProcess> getBusinessProcessLevel(String name,Integer level){
		StringBuilder sqlQuery = new StringBuilder(50);
		sqlQuery.append("select new BusinessProcess(b.idBusinessProcessPk,b.processName) from  BusinessProcess b where b.processLevel = :level ").
		append(" and b.processName like :name").
		append(" order by b.processName ");
		TypedQuery<BusinessProcess> query = em.createQuery(sqlQuery.toString(), BusinessProcess.class);
		query.setParameter("level", level);
		query.setParameter("name", "%"+name+"%");
		return query.getResultList();
	}
	
	
	/**
	 * Search notifications messages.
	 *
	 * @param message the message
	 * @return the list
	 */
	public List<NotificationLogger> searchNotificationsMessages(MessageFilter message){
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select distinct n from NotificationLogger n, BusinessProcess b3,  BusinessProcess b2, BusinessProcess b1, BusinessProcess b0  where ").
		append(" b2.idBusinessProcessPk=b3.businessProcess.idBusinessProcessPk ").
		append(" and b1.idBusinessProcessPk=b2.businessProcess.idBusinessProcessPk ").
		append(" and b0.idBusinessProcessPk=b1.businessProcess.idBusinessProcessPk ").
		append(" and n.businessProcess.idBusinessProcessPk=b3.idBusinessProcessPk ").
		append(" and n.notificationType = :notificationType ");
		if(StringUtils.isNotBlank(message.getUserReceptor())) {
			sqlQuery.append(" and n.destUserName = :destinatary ");	
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(message.getLstUserNames())) {
			sqlQuery.append(" and n.destUserName in (:lstDestinataryUsernames) ");
		}
		
		if(message.getProcessLevel()!= null && message.getIdBusinessProcess() != null){
			switch (message.getProcessLevel().intValue()) {
			case 0:
				sqlQuery.append(" and b0.idBusinessProcessPk = :process ");	
				break;
			case 1:
				sqlQuery.append(" and b1.idBusinessProcessPk = :process ");	
				break;
			case 2:
				sqlQuery.append(" and b2.idBusinessProcessPk = :process ");	
				break;

			case 3:
				sqlQuery.append(" and b3.idBusinessProcessPk = :process ");	
				break;
			}
		}
		
		if(message.getProcessLevel()!= null && message.getProcessName() != null){
			switch (message.getProcessLevel().intValue()) {
			case 0:
				sqlQuery.append(" and b0.processName like :processName ");	
				break;
			case 1:
				sqlQuery.append(" and b1.processName like :processName ");	
				break;
			case 2:
				sqlQuery.append(" and b2.processName like :processName ");	
				break;

			case 3:
				sqlQuery.append(" and b3.processName like :processName ");	
				break;
			}
		}
		
		if(message.getRead()!=null){
			sqlQuery.append(" and n.indDelivered = :delivery ");
		}
		if(message.getBeginDate() != null){
			sqlQuery.append(" and n.registryDate >= :dateIni ");
		}
		if(message.getEndDate()!=null){
			sqlQuery.append(" and n.registryDate < :dateEnd ");
		}
		if(message.getNotificationType()!= null){
			sqlQuery.append(" and n.notificationType = :type ");
		}
		if(StringUtils.isNotBlank(message.getUserIssuer())){
			sqlQuery.append(" and n.sourceUserName = :issuer ");
		}
		if(message.getIdNotificationLogger()!=null){
			sqlQuery.append(" and n.idNotificationLoggerPk = :idNotification ");
		}
		//Order
		sqlQuery.append(" order by n.registryDate desc ");
		TypedQuery<NotificationLogger> query = em.createQuery(sqlQuery.toString(), NotificationLogger.class);
		query.setParameter("notificationType", NotificationType.MESSAGE.getCode());
		if(StringUtils.isNotBlank(message.getUserReceptor())) {
			query.setParameter("destinatary", message.getUserReceptor());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(message.getLstUserNames())) {
			query.setParameter("lstDestinataryUsernames", message.getLstUserNames());
		}
		
		if(message.getProcessLevel()!= null && message.getIdBusinessProcess() != null){
			query.setParameter("process", message.getIdBusinessProcess());
		}		
		if(message.getProcessLevel()!= null && message.getProcessName() != null){
			query.setParameter("processName", message.getProcessName() + GeneralConstants.PERCENTAGE_STRING);
		}
		if(message.getRead()!=null){
			query.setParameter("delivery", message.getRead());
		}
		if(message.getBeginDate() != null){
			query.setParameter("dateIni", message.getBeginDate());
		}
		if(message.getEndDate()!=null){
			query.setParameter("dateEnd", CommonsUtilities.addDate(message.getEndDate(), 1));
		}
		if(message.getNotificationType()!= null){
			query.setParameter("type", message.getNotificationType());
		}
		if(StringUtils.isNotBlank(message.getUserIssuer())){
			query.setParameter("issuer", message.getUserIssuer());
		}
		if(message.getIdNotificationLogger()!=null){
			query.setParameter("idNotification", message.getIdNotificationLogger());
		}
		return query.getResultList();
	}
	
	
}
