package com.pradera.generalparameters.processesnotification.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.generalparameters.processes.service.MonitoringProcessesServiceBean;
import com.pradera.generalparameters.processesnotification.view.to.ProcessLoggerDetailTO;
import com.pradera.generalparameters.processesnotification.view.to.ProcessLoggerNotificationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * Session Bean implementation class ProcessNotificationFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/02/2014
 */
@Stateless
@LocalBean
public class ProcessNotificationFacade {
	
	/** The process service. */
	@EJB
	MonitoringProcessesServiceBean processService;
	
	/** The parameter service. */
	@EJB
	ParameterServiceBean parameterService;
	
	
	/**
	 * Gets the macroprocess.
	 *
	 * @param system the system
	 * @return the macroprocess
	 * @throws ServiceException the service exception
	 */
	public List<BusinessProcess> getMacroprocess(Integer system) throws ServiceException {
		List<BusinessProcess> process =null;
		List<Object[]> objects = processService.getMacroProcessListbyidSystemServiceBean(system);
		if(objects != null && !objects.isEmpty()){
			process = new ArrayList<>();
			for(Object[] proc : objects){
				BusinessProcess bproc = new BusinessProcess();
				bproc.setIdBusinessProcessPk(Long.parseLong(proc[0].toString()));
				bproc.setProcessName(proc[1].toString());
				process.add(bproc);
			}
		}
		return process;
	}
	
	/**
	 * Gets the process by level father.
	 *
	 * @param businessProcessId the business process id
	 * @return the process by level father
	 * @throws ServiceException the service exception
	 */
	public List<BusinessProcess> getProcessByLevelFather(Long businessProcessId) throws ServiceException{
		List<BusinessProcess> process =null;
		List<Long> businessProc = new ArrayList<>();
		businessProc.add(businessProcessId);
		List<Object[]> objects = processService.getProcessLevelsServiceBean(businessProc);
		if(objects != null && !objects.isEmpty()){
			process = new ArrayList<>();
			for(Object[] proc : objects){
				BusinessProcess bproc = new BusinessProcess();
				bproc.setIdBusinessProcessPk(Long.parseLong(proc[0].toString()));
				bproc.setProcessName(proc[1].toString());
				process.add(bproc);
			}
		}
		return process;
	}
	
	/**
	 * Find process logger.
	 *
	 * @param processFilter the process filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProcessLoggerNotificationTO> findProcessLogger(MonitorSystemProcessFilter processFilter) throws ServiceException{
		List<ProcessLoggerNotificationTO> processLoggers = new ArrayList<>();
		List<Object[]> objects = processService.getProcessDetailServiceBean(processFilter);
		for(Object[] proc : objects){
			ProcessLoggerNotificationTO processLogger = new ProcessLoggerNotificationTO();
			processLogger.setIdProcessLogger(Long.parseLong(proc[0].toString()));
			processLogger.setProcessDate((Date)proc[1]);
			processLogger.setState(Integer.parseInt(proc[2].toString()));
			if(proc[3] != null){
				processLogger.setFinishDate((Date)proc[3]);
			}
			processLogger.setProcessType(Integer.parseInt(proc[4].toString()));
			if(proc[5]!= null){
				processLogger.setMnemonic(proc[5].toString());
			}
			if(proc[6]!= null){
				processLogger.setProcessName(proc[6].toString());
			}
			if(proc[10]!=null){
				processLogger.setErrorDetail(proc[10].toString());
			}
			processLoggers.add(processLogger);
		}
		//description
		ParameterTableTO parameter = new ParameterTableTO();
		parameter.setState(BooleanType.YES.getCode());
		parameter.setMasterTableFk(MasterTableType.PROCESS_LOGGER_STATE.getCode());
		Map<Integer,String> processLoggerStateMap = new HashMap<>();
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(parameter)){
			processLoggerStateMap.put(param.getParameterTablePk(), param.getParameterName());
		}
		for(ProcessLoggerNotificationTO  processLogger : processLoggers){
			processLogger.setStateDescription(processLoggerStateMap.get(processLogger.getState()));
			
			//Validate if process has process_logger_details
			processLogger.setExistParameters(processService.validateProcessHasParameters(processLogger.getIdProcessLogger()));			
		}
		return processLoggers;
	}
	
    /**
     * Default constructor. 
     */
    public ProcessNotificationFacade() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * Find process logger details.
	 *
	 * @param idProcessLogger the id process logger
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProcessLoggerDetailTO> findProcessLoggerDetails(
			Long idProcessLogger) throws ServiceException{
		List<Object[]> arrParameters = processService.getProcessParametersServiceBean(idProcessLogger);
		
		List<ProcessLoggerDetailTO> processDetails = new ArrayList<ProcessLoggerDetailTO>();
		ProcessLoggerDetailTO processLoggerDetailTO = null;
		for (Object[] objParameter : arrParameters) {
			processLoggerDetailTO = new ProcessLoggerDetailTO();
			processLoggerDetailTO.setParameterName(objParameter[0]!=null?objParameter[0].toString():"");
			processLoggerDetailTO.setParameterValue(objParameter[1]!=null?objParameter[1].toString():"");
			processDetails.add(processLoggerDetailTO);
		}
		
		return processDetails;
	}

}
