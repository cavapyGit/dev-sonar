package com.pradera.generalparameters.processesnotification.view.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ProcessNotificationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/12/2013
 */
public class ProcessLoggerNotificationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5101850835731234968L;

	/** The id system. */
	private Integer idSystem;
	
	/** The system description. */
	private String systemDescription;
	
	/** The id process logger. */
	private Long idProcessLogger;
	
	/** The id macroprocess. */
	private Long idMacroprocess;
	
	/** The id process level1. */
	private Long idProcessLevel1;
	
	/** The id process level2. */
	private Long idProcessLevel2;
	
	/** The id process level3. */
	private Long idProcessLevel3;
	
	/** The state. */
	private Integer state;
	
	/** The state description. */
	private String stateDescription;
	
	/** The initial date. */
	private Date processDate;
	
	/** The end date. */
	private Date finishDate;
	
	/** The process name. */
	private String processName;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The process type. */
	private Integer processType;
	
	/** The process type description. */
	private String processTypeDescription;
	
	/** The parameter values. */
	private List<String> parameterValues;
	
	/** The error detail. */
	private String errorDetail;
	
	/** The exist parameters. */
	private boolean existParameters;

	/**
	 * Instantiates a new process notification to.
	 */
	public ProcessLoggerNotificationTO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the parameters descriptions.
	 *
	 * @return the parameters descriptions
	 */
	public String getParametersDescriptions(){
		if(parameterValues != null && !parameterValues.isEmpty()){
			StringBuilder sbParam = new StringBuilder();
			for(String value :parameterValues ){
				sbParam.append(value).append(", ");
			}
			return StringUtils.removeEnd(sbParam.toString(), ", ");
		} else {
			return null;
		}
	}

	/**
	 * Gets the id system.
	 *
	 * @return the id system
	 */
	public Integer getIdSystem() {
		return idSystem;
	}

	/**
	 * Sets the id system.
	 *
	 * @param idSystem the new id system
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}

	/**
	 * Gets the system description.
	 *
	 * @return the system description
	 */
	public String getSystemDescription() {
		return systemDescription;
	}

	/**
	 * Sets the system description.
	 *
	 * @param systemDescription the new system description
	 */
	public void setSystemDescription(String systemDescription) {
		this.systemDescription = systemDescription;
	}

	/**
	 * Gets the id process logger.
	 *
	 * @return the id process logger
	 */
	public Long getIdProcessLogger() {
		return idProcessLogger;
	}

	/**
	 * Sets the id process logger.
	 *
	 * @param idProcessLogger the new id process logger
	 */
	public void setIdProcessLogger(Long idProcessLogger) {
		this.idProcessLogger = idProcessLogger;
	}

	/**
	 * Gets the id macroprocess.
	 *
	 * @return the id macroprocess
	 */
	public Long getIdMacroprocess() {
		return idMacroprocess;
	}

	/**
	 * Sets the id macroprocess.
	 *
	 * @param idMacroprocess the new id macroprocess
	 */
	public void setIdMacroprocess(Long idMacroprocess) {
		this.idMacroprocess = idMacroprocess;
	}

	/**
	 * Gets the id process level1.
	 *
	 * @return the id process level1
	 */
	public Long getIdProcessLevel1() {
		return idProcessLevel1;
	}

	/**
	 * Sets the id process level1.
	 *
	 * @param idProcessLevel1 the new id process level1
	 */
	public void setIdProcessLevel1(Long idProcessLevel1) {
		this.idProcessLevel1 = idProcessLevel1;
	}

	/**
	 * Gets the id process level2.
	 *
	 * @return the id process level2
	 */
	public Long getIdProcessLevel2() {
		return idProcessLevel2;
	}

	/**
	 * Sets the id process level2.
	 *
	 * @param idProcessLevel2 the new id process level2
	 */
	public void setIdProcessLevel2(Long idProcessLevel2) {
		this.idProcessLevel2 = idProcessLevel2;
	}

	/**
	 * Gets the id process level3.
	 *
	 * @return the id process level3
	 */
	public Long getIdProcessLevel3() {
		return idProcessLevel3;
	}

	/**
	 * Sets the id process level3.
	 *
	 * @param idProcessLevel3 the new id process level3
	 */
	public void setIdProcessLevel3(Long idProcessLevel3) {
		this.idProcessLevel3 = idProcessLevel3;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the process date
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the new process date
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the finish date.
	 *
	 * @return the finish date
	 */
	public Date getFinishDate() {
		return finishDate;
	}

	/**
	 * Sets the finish date.
	 *
	 * @param finishDate the new finish date
	 */
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the process name
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName the new process name
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the process type description.
	 *
	 * @return the process type description
	 */
	public String getProcessTypeDescription() {
		return processTypeDescription;
	}

	/**
	 * Sets the process type description.
	 *
	 * @param processTypeDescription the new process type description
	 */
	public void setProcessTypeDescription(String processTypeDescription) {
		this.processTypeDescription = processTypeDescription;
	}

	/**
	 * Gets the parameter values.
	 *
	 * @return the parameter values
	 */
	public List<String> getParameterValues() {
		return parameterValues;
	}

	/**
	 * Sets the parameter values.
	 *
	 * @param parameterValues the new parameter values
	 */
	public void setParameterValues(List<String> parameterValues) {
		this.parameterValues = parameterValues;
	}

	/**
	 * Gets the error detail.
	 *
	 * @return the error detail
	 */
	public String getErrorDetail() {
		return errorDetail;
	}

	/**
	 * Sets the error detail.
	 *
	 * @param errorDetail the new error detail
	 */
	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	/**
	 * Checks if is exist parameters.
	 *
	 * @return true, if is exist parameters
	 */
	public boolean isExistParameters() {
		return existParameters;
	}

	/**
	 * Sets the exist parameters.
	 *
	 * @param existParameters the new exist parameters
	 */
	public void setExistParameters(boolean existParameters) {
		this.existParameters = existParameters;
	}
	
	
}
