package com.pradera.generalparameters.processesnotification.view.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ProcessLoggerDetailTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/02/2014
 */
public class ProcessLoggerDetailTO implements Serializable{

	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4968035907876827257L;
	
	/** The parameter name. */
	private String parameterName;
	
	/** The paramter value. */
	private String parameterValue;

	/**
	 * Instantiates a new process logger detail to.
	 */
	public ProcessLoggerDetailTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return parameterName;
	}

	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the new parameter name
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	/**
	 * Gets the parameter value.
	 *
	 * @return the parameter value
	 */
	public String getParameterValue() {
		return parameterValue;
	}

	/**
	 * Sets the parameter value.
	 *
	 * @param parameterValue the new parameter value
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}		

}
