package com.pradera.generalparameters.processesnotification.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.generalparameters.processesnotification.facade.ProcessNotificationFacade;
import com.pradera.generalparameters.processesnotification.view.to.ProcessLoggerDetailTO;
import com.pradera.generalparameters.processesnotification.view.to.ProcessLoggerNotificationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ProcessNotificationInboxBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/12/2013
 */
@ViewDepositaryBean
public class ProcessNotificationInboxBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1531667937517867029L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	

	/** The country residence. */
	@Inject @Configurable
	private Integer countryResidence; 
	
	/** The i depositary system id. */
	@Inject @Configurable
	Integer iDepositarySystemId;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The process notification facade. */
	@EJB
	ProcessNotificationFacade processNotificationFacade;
	
	/** The id process logger. */
	private Long idProcessLogger;
	
	/** The list users types. */
	private List<SelectItem> listUsersTypes;
	
	/** The list institutions. */
	private List<SelectItem> listInstitutions;
	
	/** The users accounts. */
	private List<UserAccountSession> usersAccounts;
	
	/** The user type. */
	private Integer userType;
	
	/** The institution type. */
	private Object institutionType;
	
	/** The user selected. */
	private String userSelected;
	
	/** The processes. */
	private List<ProcessLoggerNotificationTO> processes;
	
	/** The process notification filter. */
	private MonitorSystemProcessFilter processNotificationFilter;
	
	/** The state list. */
	private List<SelectItem> stateList;
	
	/** The macroprocess list. */
	private List<BusinessProcess> macroprocessList;
	
	/** The process level one list. */
	private List<BusinessProcess> processLevelOneList;
	
	/** The process level two list. */
	private List<BusinessProcess> processLevelTwoList;
	
	/** The process level three list. */
	private List<BusinessProcess> processLevelThreeList;
	
	/** The reports data model. */
	private GenericDataModel<ProcessLoggerNotificationTO> processDataModel;
	
	/** The error process logger. */
	private String errorProcessLogger;
	
	/** The process details. */
	private List<ProcessLoggerDetailTO> processDetails;

	/**
	 * Instantiates a new process notification inbox bean.
	 */
	public ProcessNotificationInboxBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Load.
	 *
	 * @throws ServiceException the service exception
	 */
	public void load() throws ServiceException{
		FacesContext faces =FacesContext.getCurrentInstance();
		if(!faces.isPostback()){
			if(idProcessLogger != null){
					//load detail about notification
				processNotificationFilter.setInitialDate(null);
				processNotificationFilter.setFinalDate(null);
				processNotificationFilter.setIdProcessLogger(idProcessLogger);
				processNotificationFilter.setIdUser(userInfo.getUserAccountSession().getUserName());
				processes = processNotificationFacade.findProcessLogger(processNotificationFilter);
				processDataModel = new GenericDataModel<>(processes);
					//return state filters 
				processNotificationFilter.setInitialDate(CommonsUtilities.currentDate());
				processNotificationFilter.setFinalDate(CommonsUtilities.currentDate());
				processNotificationFilter.setIdProcessLogger(null);
			}
		}
	}
	
	/**
	 * Init.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		processNotificationFilter = new MonitorSystemProcessFilter();
		processNotificationFilter.setInitialDate(CommonsUtilities.currentDate());
		processNotificationFilter.setFinalDate(CommonsUtilities.currentDate());
		macroprocessList = processNotificationFacade.getMacroprocess(iDepositarySystemId);
		processLevelOneList = new ArrayList<>();
		processLevelTwoList = new ArrayList<>();
		processLevelThreeList = new ArrayList<>();
		
		ParameterTableTO parameter = new ParameterTableTO();
		parameter.setState(BooleanType.YES.getCode());
		parameter.setMasterTableFk(MasterTableType.PROCESS_LOGGER_STATE.getCode());
		stateList = new ArrayList<>();
		for(ParameterTable  paramState : generalParametersFacade.getListParameterTableServiceBean(parameter)){
			stateList.add(new SelectItem(paramState.getParameterTablePk(),paramState.getParameterName()));
		}
		
	}
	
	/**
	 * Select macro process.
	 *
	 * @throws ServiceException the service exception
	 */
	public void selectMacroProcess() throws ServiceException{
		if(processNotificationFilter.getIdMacroProcess() != null){
			processLevelOneList.clear();
			processLevelOneList = processNotificationFacade.getProcessByLevelFather(processNotificationFilter.getIdMacroProcess());
			processLevelTwoList.clear();
			processLevelThreeList.clear();
		} else {
			processLevelOneList.clear();
			processLevelTwoList.clear();
			processLevelThreeList.clear();
		}
	}
	
	/**
	 * Select process level one.
	 *
	 * @throws ServiceException the service exception
	 */
	public void selectProcessLevelOne() throws ServiceException{
		if(processNotificationFilter.getIdProcessLevelOne()!=null){
			processLevelTwoList.clear();
			processLevelTwoList = processNotificationFacade.getProcessByLevelFather(processNotificationFilter.getIdProcessLevelOne());
		} else {
			processLevelTwoList.clear();
			processLevelThreeList.clear();
		}
	}
	
	/**
	 * Select process level two.
	 *
	 * @throws ServiceException the service exception
	 */
	public void selectProcessLevelTwo() throws ServiceException{
		if(processNotificationFilter.getIdProcessLevelTwo()!=null){
			processLevelThreeList.clear();
			processLevelThreeList = processNotificationFacade.getProcessByLevelFather(processNotificationFilter.getIdProcessLevelTwo());
		} else {
			processLevelThreeList.clear();
		}
	}
	
	/**
	 * Search process.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void searchProcess() throws ServiceException{
		//check if is enabled privilege search others user's process
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			if(StringUtils.isNotBlank(userSelected)){
				processNotificationFilter.setIdUser(userSelected);
			} else {
				processNotificationFilter.setIdUser(userInfo.getUserAccountSession().getUserName());
			}
		} else {
			processNotificationFilter.setIdUser(userInfo.getUserAccountSession().getUserName());
		}
		processes = processNotificationFacade.findProcessLogger(processNotificationFilter);
		processDataModel = new GenericDataModel<>(processes);
	}
	
	/**
	 * Show process parameters.
	 *
	 * @param idProcessLogger the id process logger
	 */
	public void showProcessParameters(Long idProcessLogger){
		try {
			processDetails = processNotificationFacade.findProcessLoggerDetails(idProcessLogger);
			JSFUtilities.executeJavascriptFunction("PF('dlgwParams').show();");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean.
	 */
	public void clean(){
		processNotificationFilter = new MonitorSystemProcessFilter();
		processNotificationFilter.setFinalDate(CommonsUtilities.currentDate());
		processNotificationFilter.setInitialDate(CommonsUtilities.currentDate());
		
		if(idProcessLogger != null) {
			idProcessLogger = null;	
		}
		
		//processes.clear();
		processes = new ArrayList<ProcessLoggerNotificationTO>();
		processDataModel = new GenericDataModel<>(processes);
		processLevelOneList.clear();
		processLevelTwoList.clear();
		processLevelThreeList.clear();
	}
	

	/**
	 * Gets the id process logger.
	 *
	 * @return the id process logger
	 */
	public Long getIdProcessLogger() {
		return idProcessLogger;
	}

	/**
	 * Sets the id process logger.
	 *
	 * @param idProcessLogger the new id process logger
	 */
	public void setIdProcessLogger(Long idProcessLogger) {
		this.idProcessLogger = idProcessLogger;
	}

	/**
	 * Gets the list users types.
	 *
	 * @return the list users types
	 */
	public List<SelectItem> getListUsersTypes() {
		return listUsersTypes;
	}

	/**
	 * Sets the list users types.
	 *
	 * @param listUsersTypes the new list users types
	 */
	public void setListUsersTypes(List<SelectItem> listUsersTypes) {
		this.listUsersTypes = listUsersTypes;
	}

	/**
	 * Gets the list institutions.
	 *
	 * @return the list institutions
	 */
	public List<SelectItem> getListInstitutions() {
		return listInstitutions;
	}

	/**
	 * Sets the list institutions.
	 *
	 * @param listInstitutions the new list institutions
	 */
	public void setListInstitutions(List<SelectItem> listInstitutions) {
		this.listInstitutions = listInstitutions;
	}

	/**
	 * Gets the users accounts.
	 *
	 * @return the users accounts
	 */
	public List<UserAccountSession> getUsersAccounts() {
		return usersAccounts;
	}

	/**
	 * Sets the users accounts.
	 *
	 * @param usersAccounts the new users accounts
	 */
	public void setUsersAccounts(List<UserAccountSession> usersAccounts) {
		this.usersAccounts = usersAccounts;
	}

	/**
	 * Gets the user type.
	 *
	 * @return the user type
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * Sets the user type.
	 *
	 * @param userType the new user type
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Object getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Object institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the user selected.
	 *
	 * @return the user selected
	 */
	public String getUserSelected() {
		return userSelected;
	}

	/**
	 * Sets the user selected.
	 *
	 * @param userSelected the new user selected
	 */
	public void setUserSelected(String userSelected) {
		this.userSelected = userSelected;
	}

	
	/**
	 * Gets the processes.
	 *
	 * @return the processes
	 */
	public List<ProcessLoggerNotificationTO> getProcesses() {
		return processes;
	}

	/**
	 * Sets the processes.
	 *
	 * @param processes the new processes
	 */
	public void setProcesses(List<ProcessLoggerNotificationTO> processes) {
		this.processes = processes;
	}

	/**
	 * Gets the process notification filter.
	 *
	 * @return the process notification filter
	 */
	public MonitorSystemProcessFilter getProcessNotificationFilter() {
		return processNotificationFilter;
	}

	/**
	 * Sets the process notification filter.
	 *
	 * @param processNotificationFilter the new process notification filter
	 */
	public void setProcessNotificationFilter(MonitorSystemProcessFilter processNotificationFilter) {
		this.processNotificationFilter = processNotificationFilter;
	}

	/**
	 * Gets the macroprocess list.
	 *
	 * @return the macroprocess list
	 */
	public List<BusinessProcess> getMacroprocessList() {
		return macroprocessList;
	}

	/**
	 * Sets the macroprocess list.
	 *
	 * @param macroprocessList the new macroprocess list
	 */
	public void setMacroprocessList(List<BusinessProcess> macroprocessList) {
		this.macroprocessList = macroprocessList;
	}

	/**
	 * Gets the process level one list.
	 *
	 * @return the process level one list
	 */
	public List<BusinessProcess> getProcessLevelOneList() {
		return processLevelOneList;
	}

	/**
	 * Sets the process level one list.
	 *
	 * @param processLevelOneList the new process level one list
	 */
	public void setProcessLevelOneList(List<BusinessProcess> processLevelOneList) {
		this.processLevelOneList = processLevelOneList;
	}

	/**
	 * Gets the process level two list.
	 *
	 * @return the process level two list
	 */
	public List<BusinessProcess> getProcessLevelTwoList() {
		return processLevelTwoList;
	}

	/**
	 * Sets the process level two list.
	 *
	 * @param processLevelTwoList the new process level two list
	 */
	public void setProcessLevelTwoList(List<BusinessProcess> processLevelTwoList) {
		this.processLevelTwoList = processLevelTwoList;
	}

	/**
	 * Gets the process level three list.
	 *
	 * @return the process level three list
	 */
	public List<BusinessProcess> getProcessLevelThreeList() {
		return processLevelThreeList;
	}

	/**
	 * Sets the process level three list.
	 *
	 * @param processLevelThreeList the new process level three list
	 */
	public void setProcessLevelThreeList(List<BusinessProcess> processLevelThreeList) {
		this.processLevelThreeList = processLevelThreeList;
	}

	/**
	 * Gets the state list.
	 *
	 * @return the state list
	 */
	public List<SelectItem> getStateList() {
		return stateList;
	}

	/**
	 * Sets the state list.
	 *
	 * @param stateList the new state list
	 */
	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	/**
	 * Gets the process data model.
	 *
	 * @return the process data model
	 */
	public GenericDataModel<ProcessLoggerNotificationTO> getProcessDataModel() {
		return processDataModel;
	}

	/**
	 * Sets the process data model.
	 *
	 * @param processDataModel the new process data model
	 */
	public void setProcessDataModel(GenericDataModel<ProcessLoggerNotificationTO> processDataModel) {
		this.processDataModel = processDataModel;
	}

	/**
	 * Gets the error process logger.
	 *
	 * @return the error process logger
	 */
	public String getErrorProcessLogger() {
		return errorProcessLogger;
	}

	/**
	 * Sets the error process logger.
	 *
	 * @param errorProcessLogger the new error process logger
	 */
	public void setErrorProcessLogger(String errorProcessLogger) {
		this.errorProcessLogger = errorProcessLogger;
	}

	/**
	 * Gets the process details.
	 *
	 * @return the process details
	 */
	public List<ProcessLoggerDetailTO> getProcessDetails() {
		return processDetails;
	}

	/**
	 * Sets the process details.
	 *
	 * @param processDetails the new process details
	 */
	public void setProcessDetails(List<ProcessLoggerDetailTO> processDetails) {
		this.processDetails = processDetails;
	}

}
