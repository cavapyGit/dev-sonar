package com.pradera.generalparameters.utils.view;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FinancialConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class FinancialConstants {
	
	/**
	 * Instantiates a new financial constants.
	 */
	public FinancialConstants(){
		super();
	}
	
	/** The Constant NEGOTIATION_PARAMETERS_MASTER_TABLE_PK. */
	public static final Integer NEGOTIATION_PARAMETERS_MASTER_TABLE_PK 	= 285;
	
	/** The Constant INDEX_FINANCIAL_PARAMETER_TABLE_PK. */
	public static final Integer INDEX_FINANCIAL_PARAMETER_TABLE_PK 		= 711;
	
	/** The Constant RATE_FINANCIAL_PARAMETER_TABLE_PK. */
	public static final Integer RATE_FINANCIAL_PARAMETER_TABLE_PK 		= 712;
	
	/** The Constant RATE_PERIOD_TYPE_PARAMETER_TABLE_PK. */
	public static final Integer RATE_PERIOD_TYPE_PARAMETER_TABLE_PK 	= 73;
}
