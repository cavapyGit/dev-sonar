package com.pradera.generalparameters.utils.view;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class PropertiesConstants {

	/**
	 * Instantiates a new properties constants.
	 */
	private PropertiesConstants() {
		super();
	}
	
	/** The Constant ALT_NEED_ONE_FIELD. */
	public final static String ALT_NEED_ONE_FIELD = "error.required.one.field";

	/** The Constant QUOTATION_REGISTER_SUCCESS. */
	public static final String QUOTATION_REGISTER_SUCCESS = "quotation.msg.register.success";
	
	/** The Constant QUOTATION_MODIFY_SUCCESS. */
	public static final String QUOTATION_MODIFY_SUCCESS = "quotation.msg.modify.success";
	
	/** The Constant QUOTATION_FILE_INCORRECTNAME. */
	public static final String QUOTATION_FILE_INCORRECTNAME = "quotation.error.msg.filename";
	
	/** The Constant QUOTATION_FILE_REGISTER_SUCCESS. */
	public static final String QUOTATION_FILE_REGISTER_SUCCESS = "quotation.msg.register.file.success";
	
	/** The Constant QUOTATION_FILE_UPLOADED_SUCCESS. */
	public static final String QUOTATION_FILE_UPLOADED_SUCCESS = "quotation.msg.upload.success";
	
	/** The Constant QUOTATION_FILE_STATUS. */
	public static final String QUOTATION_FILE_STATUS = "quotation.msg.file.status";
	
	/** The Constant QUOTATION_DELETE_WRONG_STATUS. */
	public static final String QUOTATION_DELETE_WRONG_STATUS = "quotation.delete.wrong.status";
	
	/** The Constant TIPO_CAMBIO__REGISTER_SUCCESS. */
	public static final String TIPO_CAMBIO__REGISTER_SUCCESS = "tipo.msg.status.success";
	
	/** The Constant TYPE_SHIFT_REGISTER_FAIL. */
	public static final String TYPE_SHIFT_REGISTER_FAIL = "tipo.msg.status.falure";
	
	/** The Constant REGISTER_TYPE_CHANGE_SUCCESS. */
	public static final String REGISTER_TYPE_CHANGE_SUCCESS = "tipo.msg.register.success";
	
	/** The Constant REGISTER_TYPE_CHANGE_FAILURE. */
	public static final String REGISTER_TYPE_CHANGE_FAILURE = "tipo.msg.register.failure";
	
	/** The Constant MODIFY_TYPE_CHANGE_SUCCESS. */
	public static final String MODIFY_TYPE_CHANGE_SUCCESS = "tipo.msg.modify.success";
	
	/** The Constant MODIFY_TYPE_CONFIRMED_SUCCESS. */
	public static final String MODIFY_TYPE_CONFIRMED_SUCCESS = "tipo.msg.confirmed.success";
	
	/** The Constant MODIFY_TYPE_CHANGE_FAILURE. */
	public static final String MODIFY_TYPE_CHANGE_FAILURE = "tipo.msg.modfiy.failure";
	
	/** The Constant TYPE_EXCHANGE_LOSS_VALIDATION. */
	public static final String TYPE_EXCHANGE_LOSS_VALIDATION = "tipo.msg.negative.numbers";
	
	/** The Constant GEOGRAPHICLOCATION_REGISTER_SUCCESS. */
	public static final String GEOGRAPHICLOCATION_REGISTER_SUCCESS = "geolocation.msg.register.success";
	
	/** The Constant GEOGRAPHICLOCATION_MODIFY_SUCCESS. */
	public static final String GEOGRAPHICLOCATION_MODIFY_SUCCESS = "geolocation.msg.modify.success";
	
	/** The Constant GEOGRAPHICLOCATION_BLOCKED_SUCCESS. */
	public static final String GEOGRAPHICLOCATION_BLOCKED_SUCCESS = "geolocation.msg.block.success";
	
	/** The Constant GEOGRAPHICLOCATION_BLOCKED_FAIL. */
	public static final String GEOGRAPHICLOCATION_BLOCKED_FAIL = "geolocation.msg.block.fail";
	
	/** The Constant GEOGRAPHICLOCATION_LOCALCOUNTRY_REGISTERED. */
	public static final String GEOGRAPHICLOCATION_LOCALCOUNTRY_REGISTERED = "geolocation.error.localcountry.registered";
	
	/** The Constant GEOGRAPHICLOCATION_CNF_REGISTER. */
	public static final String GEOGRAPHICLOCATION_CNF_REGISTER="geolocation.msg.confirm.register";
	
	/** The Constant GEOGRAPHICLOCATION_CNF_MODIFY. */
	public static final String GEOGRAPHICLOCATION_CNF_MODIFY="geolocation.msg.confirm.modify";
	
	/** The Constant GEOGRAPHICLOCATION_REQUIRED_LOCATIONTYPE. */
	public static final String GEOGRAPHICLOCATION_REQUIRED_LOCATIONTYPE="geolocation.msg.required.locationtype";
	
	/** The Constant GEOGRAPHICLOCATION_REQUIRED_COUNTRY. */
	public static final String GEOGRAPHICLOCATION_REQUIRED_COUNTRY="geolocation.msg.required.country";
	
	/** The Constant GEOGRAPHICLOCATION_REQUIRED_PROVINCE. */
	public static final String GEOGRAPHICLOCATION_REQUIRED_PROVINCE="geolocation.msg.required.province";
	
	/** The Constant GEOGRAPHICLOCATION_NAME_REGISTERED. */
	public static final String GEOGRAPHICLOCATION_NAME_REGISTERED="geolocation.msg.name.alreadyregistered";
	
	/** The Constant GENDER_MESSAGE_FEMALE. */
	public static final String GENDER_MESSAGE_FEMALE = "message.gender.female";
	
	/** The Constant GENDER_MESSAGE_MALE. */
	public static final String GENDER_MESSAGE_MALE = "message.gender.male";
	
	/** The Constant MASTER_DEFINITION_CONFIRM_SAVE. */
	public static final String MASTER_DEFINITION_CONFIRM_SAVE="generalparameters.master.lbl.savedef";
	
	/** The Constant MASTER_ELEMENT_CONFIRM_SAVE. */
	public static final String MASTER_ELEMENT_CONFIRM_SAVE="generalparameters.master.lbl.saveelem";
	
	/** The Constant MASTER_DEFINITION_SAVE_SUCCESS. */
	public static final String MASTER_DEFINITION_SAVE_SUCCESS = "generalparameters.master.lbl.savedefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_MODIFY_SUCCESS. */
	public static final String MASTER_DEFINITION_MODIFY_SUCCESS = "generalparameters.master.lbl.modifydefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_MODIFY_FAIL. */
	public static final String MASTER_DEFINITION_MODIFY_FAIL = "generalparameters.master.lbl.modifydefinitionfail";
	
	/** The Constant MODIFY_FAIL_DEFINITION_CONTAINS_ELEMENTS. */
	public static final String MODIFY_FAIL_DEFINITION_CONTAINS_ELEMENTS = "generalparameters.master.lbl.modifydefinitionfail.ContainsElements";
	
	/** The Constant MASTER_DEFINITION_CONFIRM_SUCCESS. */
	public static final String MASTER_DEFINITION_CONFIRM_SUCCESS = "generalparameters.master.lbl.confirmdefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_CONFIRM_FAIL. */
	public static final String MASTER_DEFINITION_CONFIRM_FAIL = "generalparameters.master.lbl.confirmdefinitionfail";
	
	/** The Constant MASTER_DEFINITION_REJECT_SUCCESS. */
	public static final String MASTER_DEFINITION_REJECT_SUCCESS = "generalparameters.master.lbl.rejectdefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_REJECT_FAIL. */
	public static final String MASTER_DEFINITION_REJECT_FAIL = "generalparameters.master.lbl.rejectdefinitionfail";
	
	/** The Constant MASTER_DEFINITION_SAVE_FAILURE. */
	public static final String MASTER_DEFINITION_SAVE_FAILURE = "generalparameters.master.lbl.savedefinitionfail";
	
	/** The Constant MASTER_ELEMENT_SAVE_SUCCESS. */
	public static final String MASTER_ELEMENT_SAVE_SUCCESS = "generalparameters.master.lbl.saveelementsucess";
	
	/** The Constant MASTER_ELEMENT_SAVE_FAILURE. */
	public static final String MASTER_ELEMENT_SAVE_FAILURE = "generalparameters.master.lbl.savedelementfail";
	
	/** The Constant MASTER_ELEMENT_MODIFY_SUCCESS. */
	public static final String MASTER_ELEMENT_MODIFY_SUCCESS = "generalparameters.master.lbl.modifyelementsucess";
	
	/** The Constant MASTER_ELEMENT_MODIFY_FAILURE. */
	public static final String MASTER_ELEMENT_MODIFY_FAILURE = "generalparameters.master.lbl.modifyelementfail";
	
	/** The Constant MASTER_ELEMENT_CONFIRM_SUCCESS. */
	public static final String MASTER_ELEMENT_CONFIRM_SUCCESS = "generalparameters.master.lbl.confirmelementsucess";
	
	/** The Constant MASTER_ELEMENT_CONFIRM_FAILURE. */
	public static final String MASTER_ELEMENT_CONFIRM_FAILURE = "generalparameters.master.lbl.confirmelementfail";
	
	/** The Constant MASTER_ELEMENT_REJECT_SUCCESS. */
	public static final String MASTER_ELEMENT_REJECT_SUCCESS = "generalparameters.master.lbl.rejectlementsucess";
	
	/** The Constant MASTER_ELEMENT_REJECT_FAILURE. */
	public static final String MASTER_ELEMENT_REJECT_FAILURE = "generalparameters.master.lbl.rejectelementfail";
	
	/** The Constant MASTER_SEARCH_MESSAGE. */
	public static final String MASTER_SEARCH_MESSAGE = "generalparameters.master.msg.searchtype";
	
	/** The Constant MASTER_CONFIRM. */
	public static final String MASTER_CONFIRM = "generalparameters.master.msg.searchconfirmmaster";
	
	/** The Constant MASTER_REJECT. */
	public static final String MASTER_REJECT = "generalparameters.master.msg.searchrejectmaster";
	
	/** The Constant MASTER_DEFINITION_FOR_ELEMENT. */
	public static final String MASTER_DEFINITION_FOR_ELEMENT = "generalparameters.master.msg.selectelementdef";
	
	/** The Constant MASTER_DEFINITION_NOT_FOUND. */
	public static final String MASTER_DEFINITION_NOT_FOUND = "generalparameters.master.msg.selecteledef";
	
	/** The Constant MASTER_NO_RECORDS_FOUND. */
	public static final String MASTER_NO_RECORDS_FOUND = "generalparameters.master.msg.norecords";
	
	/** The Constant MASTER_EXCEPTION. */
	public static final String MASTER_EXCEPTION = "generalparameters.master.msg.exception";
	
	/** The Constant MASTER_SEARCH_FECHA. */
	public static final String MASTER_SEARCH_FECHA = "generalparameters.master.msg.searchfecha";
	
	/** The Constant MASTER_SEARCH_SELECT_NORECORDS. */
	public static final String MASTER_SEARCH_SELECT_NORECORDS = "generalparameters.master.msg.selrec";
	
	/** The Constant MASTER_SEARCH_REQUIRED_DEFINITION. */
	public static final String MASTER_SEARCH_REQUIRED_DEFINITION="generalparameters.master.definition.required";
	
	/** The Constant SELECT_ATLEAST_ONE_INDICATOR. */
	public static final String SELECT_ATLEAST_ONE_INDICATOR="generalparameters.master.lbl.indicadores.requierd";
	
	/** The Constant MASTERTABLE_NAME_ALREADY_EXISTS. */
	public static final String MASTERTABLE_NAME_ALREADY_EXISTS="mstrtbl.definitionname.exists";
	
	/** The Constant MASTERTABLE_CODE_ALREADY_EXISTS. */
	public static final String MASTERTABLE_CODE_ALREADY_EXISTS="mstrtbl.definitioncode.exists";
	
	/** The Constant MASTERTABLE_SHORTDESC_ALREADY_EXISTS. */
	public static final String MASTERTABLE_SHORTDESC_ALREADY_EXISTS="mstrtbl.definitionshortdesc.exists";
	
	/** The Constant MASTERTABLE_LONGDESC_ALREADY_EXISTS. */
	public static final String MASTERTABLE_LONGDESC_ALREADY_EXISTS="mstrtbl.definitionlongdesc.exists";
	
	/** The Constant PARAMETERTABLE_CODE_ALREADY_EXISTS. */
	public static final String PARAMETERTABLE_CODE_ALREADY_EXISTS="mstrtbl.parametercode.exists";
	
	/** The Constant PARAMETERTABLE_DESCRIPTION_ALREADY_EXISTS. */
	public static final String PARAMETERTABLE_DESCRIPTION_ALREADY_EXISTS="mstrtbl.parameterdescription.exists";
	
	/** The Constant MONITOR_SYSTEM_PROCESSES_EXECUTED_SUCCESS. */
	public static final String MONITOR_SYSTEM_PROCESSES_EXECUTED_SUCCESS = "monitor.system.process.msg.processes.executed";
	
	/** The Constant MONITOR_SYSTEM_PROCESSES_STOPPED_SUCCESS. */
	public static final String MONITOR_SYSTEM_PROCESSES_STOPPED_SUCCESS = "monitor.system.process.msg.processes.stopped";
	
	/** The Constant BUSINESS_PROCESS_MODIFY_CONFIRM. */
	public static final String BUSINESS_PROCESS_MODIFY_CONFIRM = "bussinessProcess.msg.modify.confirm";
	
	/** The Constant BUSINESS_PROCESS_MODIFY_SUCCESS. */
	public static final String BUSINESS_PROCESS_MODIFY_SUCCESS = "bussinessProcess.msg.modify.success";
	
	/** The Constant PROCESS_SCHEDULE_REGISTER_CONFIRM. */
	public static final String PROCESS_SCHEDULE_REGISTER_CONFIRM = "schedule.msg.register.confirm";
	
	/** The Constant PROCESS_SCHEDULE_REGISTER_SUCCESS. */
	public static final String PROCESS_SCHEDULE_REGISTER_SUCCESS = "schedule.msg.register.success";
	
	/** The Constant PROCESS_SCHEDULE_MODIFY_CONFIRM. */
	public static final String PROCESS_SCHEDULE_MODIFY_CONFIRM = "schedule.msg.modify.confirm";
	
	/** The Constant PROCESS_SCHEDULE_MODIFY_SUCCESS. */
	public static final String PROCESS_SCHEDULE_MODIFY_SUCCESS = "schedule.msg.modify.success";
	
	/** The Constant PROCESS_SCHEDULE_ACTIVATION_CONFIRM. */
	public static final String PROCESS_SCHEDULE_ACTIVATION_CONFIRM = "schedule.msg.activation.confirm";
	
	/** The Constant PROCESS_SCHEDULE_ACTIVATION_SUCCESS. */
	public static final String PROCESS_SCHEDULE_ACTIVATION_SUCCESS = "schedule.msg.activation.success";
	
	/** The Constant PROCESS_SCHEDULE_DEACTIVATION_CONFIRM. */
	public static final String PROCESS_SCHEDULE_DEACTIVATION_CONFIRM = "schedule.msg.deactivation.confirm";
	
	/** The Constant PROCESS_SCHEDULE_DEACTIVATION_SUCCESS. */
	public static final String PROCESS_SCHEDULE_DEACTIVATION_SUCCESS = "schedule.msg.deactivation.success";
	
	/** The Constant HOLIDAY_REGISTER_SUCCESS. */
	public static final String HOLIDAY_REGISTER_SUCCESS="holiday.msg.register.success";
	
	/** The Constant HOLIDAY_MODIFY_SUCCESS. */
	public static final String HOLIDAY_MODIFY_SUCCESS="holiday.msg.modify.success";
	
	/** The Constant CNF_DELETE_HOLIDAY. */
	public static final String CNF_DELETE_HOLIDAY="feriado.cnf.msg.eliminar";
	
	/** The Constant HOLIDAY_DELETE_SUCCESS. */
	public static final String HOLIDAY_DELETE_SUCCESS="holiday.msg.delete.success";
	
	/** The Constant ERROR_ADM_HOLIDAY_REGISTER_PAST_DATE. */
	public static final String ERROR_ADM_HOLIDAY_REGISTER_PAST_DATE = "error.adm.holiday.register.past.date";
	
	/** The Constant CODE_USER_EMPTY. */
	public static final String CODE_USER_EMPTY="generalparameters.msg.search.empty";
	
	/** The Constant GENERALPARAMETERS_REGISTER_SUCCESS. */
	public static final String GENERALPARAMETERS_REGISTER_SUCCESS = "general.msg.register.success";
	
	/** The Constant GENERALPARAMETERS_MODIFY_SUCCESS. */
	public static final String GENERALPARAMETERS_MODIFY_SUCCESS = "general.msg.modify.success";
	
	/** The Constant QUOTATION_VALOR_VALUE_REQUIRED. */
	public static final String QUOTATION_VALOR_VALUE_REQUIRED = "quotation.msg.required.valor";
	
	/** The Constant QUOTATION_VALOR_VALUE_INVALID. */
	public static final String QUOTATION_VALOR_VALUE_INVALID = "quotation.msg.invalid.valor";
	
	/** The Constant QUOTATION_MASSIVE_WRONG_STRUCTURE. */
	public static final String QUOTATION_MASSIVE_WRONG_STRUCTURE = "quotation.msg.massive.wrong.structure";
	
	/** The Constant QUOTATION_MASSIVE_DUPLICATED_REGISTERS. */
	public static final String QUOTATION_MASSIVE_DUPLICATED_REGISTERS = "quotation.msg.massive.duplicated.registers";
	
	/** The Constant QUOTATION_MASSIVE_ALREADY_REGISTERED. */
	public static final String QUOTATION_MASSIVE_ALREADY_REGISTERED = "quotation.msg.massive.already.registered";	
	
	/** The Constant INDEX_REGISTERED_SUCCESS. */
	public static final String INDEX_REGISTERED_SUCCESS ="general.msg.register.success";
	
	/** The Constant INDEX_MODIFIED_SUCCESS. */
	public static final String INDEX_MODIFIED_SUCCESS="general.msg.modify.success";
	
	/** The Constant USER_FOUND. */
	public static final String USER_FOUND = "notifications.msg.user.exist";
	
	/** The Constant INITIAL_FINAL_FAIL. */
	public static final String INITIAL_FINAL_FAIL = "notifications.msg.initial.final.fail.exist";
	
	/** The Constant USER_NOT_FOUND. */
	public static final String USER_NOT_FOUND = "notifications.msg.user.didNotExist"; 
	
	/** The Constant CHANGE_MONEY_TYPE_BUYPRICE. */
	public static final String CHANGE_MONEY_TYPE_BUYPRICE="tipo.msg.purchaseprice.required";
	
	/** The Constant CHANGE_MONEY_TYPE_SELLPRICE. */
	public static final String CHANGE_MONEY_TYPE_SELLPRICE="tipo.msg.saleprice.required";
	
	/** The Constant CHANGE_MONEY_TYPE_AVGPRICE. */
	public static final String CHANGE_MONEY_TYPE_AVGPRICE="tipo.msg.avgprice.required";
	
	/** The Constant CHANGE_MONEY_TYPE_BUYPRICE_ZERO. */
	public static final String CHANGE_MONEY_TYPE_BUYPRICE_ZERO="tipo.msg.purchaseprice.zero";
	
	/** The Constant CHANGE_MONEY_TYPE_SELLPRICE_ZERO. */
	public static final String CHANGE_MONEY_TYPE_SELLPRICE_ZERO="tipo.msg.saleprice.zero";
	
	/** The Constant CHANGE_MONEY_TYPE_AVGPRICE_ZERO. */
	public static final String CHANGE_MONEY_TYPE_AVGPRICE_ZERO="tipo.msg.avgprice.zero";
	
	/** The Constant ALERT_DAILY_EXCHANGE_ALREADY_CONFIRMED. */
	public static final String ALERT_DAILY_EXCHANGE_ALREADY_CONFIRMED="alert.daily.exchange.confirmed";
	
	/** The Constant SAVE_SUCCESS_MSG. */
	public static final String SAVE_SUCCESS_MSG = "business.save.success";
	
	/** The Constant USER_ID. */
	public static final String USER_ID = "notifications.id";
	
	/** The Constant USER_EMAIL. */
	public static final String USER_EMAIL= "opc.generales.email";
	
	/** The Constant USER_MOBILE. */
	public static final String USER_MOBILE="notifications.mobile";
	
	/** The Constant MASTER_MGMT_DEFINITION_EXISTS. */
	public static final String MASTER_MGMT_DEFINITION_EXISTS = "generalparameters.master.msg.defexists";
	
	/** The Constant MASTER_MGMT_ELEMENT_EXISTS. */
	public static final String MASTER_MGMT_ELEMENT_EXISTS = "generalparameters.master.msg.elementexists";
	
	/** The Constant MASTER_SERACH_DEF_MODIFY. */
	public static final String MASTER_SERACH_DEF_MODIFY = "generalparameters.master.msg.searchmodifymaster";
	
	/** The Constant INTERVAL_NOT_POSSIABLE. */
	public static final String INTERVAL_NOT_POSSIABLE = "notfications.msg.interval.fail";
	
	/** The Constant END_DATE_NOT_BEFORE_CURRENTDATE. */
	public static final String END_DATE_NOT_BEFORE_CURRENTDATE="indexrate.error.msg.datevalidation.fail";
	
	/** The Constant END_MONTH_NOT_BEFORE_CURRENTDATE. */
	public static final String END_MONTH_NOT_BEFORE_CURRENTDATE="indexrate.month.error.msg.datevalidation.fail";
	
	/** The Constant VALIDATION_DATE. */
	public static final String VALIDATION_DATE="message.error.msg.dateHeader";
	
	/** The Constant VALIDATION_MNEMONIC. */
	public static final String VALIDATION_MNEMONIC="message.error.msg.mnemonic";
	
	/** The Constant VALIDATION_VALUE. */
	public static final String VALIDATION_VALUE="message.error.msg.value";
	
	/** The Constant VALIDATION_INDEX_RANGE_ALREADY_REGISTERED. */
	public static final String VALIDATION_INDEX_RANGE_ALREADY_REGISTERED="validation.index.range.already.registered";
	
	/** The Constant VALIDATION_RATE_RANGE_ALREADY_REGISTERED. */
	public static final String VALIDATION_RATE_RANGE_ALREADY_REGISTERED="validation.rate.range.already.registered";
	
	/** The Constant VALIDATION_MNEMONIC_TEXT. */
	public static final String VALIDATION_MNEMONIC_TEXT="message.error.msg.mnemonic.text";
	
	/** The Constant VALIDATION_VALUE_TEXT. */
	public static final String VALIDATION_VALUE_TEXT="message.error.msg.value.text";
	
	/** The Constant INDEX_REGISTER_SUCCESS. */
	public static final String INDEX_REGISTER_SUCCESS="indexrate.index.register.success";
	
	/** The Constant RATE_REGISTER_SUCCESS. */
	public static final String RATE_REGISTER_SUCCESS="indexrate.rate.register.success";
	
	/** The Constant INDEX_MODIFY_SUCCESS. */
	public static final String INDEX_MODIFY_SUCCESS="indexrate.modify.index.success";
	
	/** The Constant RATE_MODIFY_SUCCESS. */
	public static final String RATE_MODIFY_SUCCESS="indexrate.modify.rate.success";
	
	/** The Constant INDEX_UPDATE_SUCCESS. */
	public static final String INDEX_UPDATE_SUCCESS="indexrate.update.index.success";
	
	/** The Constant RATE_UPDATE_SUCCESS. */
	public static final String RATE_UPDATE_SUCCESS="indexrate.update.rate.success";
	
	/** The Constant ERROR_COUNTRY_REQUIRED. */
	public static final String ERROR_COUNTRY_REQUIRED="holiday.error.msg.country.required";
	
	/** The Constant ERROR_COMPORTEMENTAL_REQUIRED. */
	public static final String ERROR_COMPORTEMENTAL_REQUIRED="holiday.error.msg.behaviour.required";
	
	/** The Constant CNF_REGISTER_HOLIDAY. */
	public static final String CNF_REGISTER_HOLIDAY="holidayMgmt.mesg.dateResistration";
	
	/** The Constant INDICATOR_REQUIRED. */
	//masterTable
	public static final String INDICATOR_REQUIRED="mstrtbl.indicator.required";
	
	/** The Constant INTEGER_REQUIRED. */
	public static final String INTEGER_REQUIRED="mstrtbl.NumeroEntero.required";
	
	/** The Constant DATE_REQUIRED. */
	public static final String DATE_REQUIRED="mstrtbl.date.required";
	
	/** The Constant DECIMAL_AMOUNT_REQUIRED. */
	public static final String DECIMAL_AMOUNT_REQUIRED="mstrtbl.decimalamount.required";
	
	/** The Constant STRING_REQUIRED. */
	public static final String STRING_REQUIRED="mstrtbl.string.required";
	
	/** The Constant SUBLOCATION_REQUIRED. */
	public static final String SUBLOCATION_REQUIRED="mstrtbl.sublocation.required";
	
	/** The Constant STATE_REQUIRED. */
	public static final String STATE_REQUIRED="mstrtbl.state.required";
	
	/** The Constant OBSERVATION_REQUIRED. */
	public static final String OBSERVATION_REQUIRED="mstrtbl.observation.required";
	
	/** The Constant SELECT_RECORD_TOACTIVE. */
	public static final String SELECT_RECORD_TOACTIVE="process.err.msg.select.record.active";
	
	/** The Constant SELECT_RECORD_TODEACTIVE. */
	public static final String SELECT_RECORD_TODEACTIVE="process.err.msg.select.record.deactive";
	
	/** The Constant SCHEDULE_PROCESS_ALREADY_EXISTS. */
	public static final String 	SCHEDULE_PROCESS_ALREADY_EXISTS="process.err.msg.scheduleprocess.exist";
	
	/** The Constant ERROR_FIELD_REQUIRED. */
	public static final String ERROR_FIELD_REQUIRED="error.field.required";
	
	/** The Constant ERROR_RECORD_NOT_SELECTED. */
	public static final String ERROR_RECORD_NOT_SELECTED = "error.record.not.selected";
	
	/** The Constant ERROR_INPUT_TEXT. */
	public static final String ERROR_INPUT_TEXT = "error.input.text";
	
	/** The Constant ERROR_COMBO_SELECT. */
	public static final String ERROR_COMBO_SELECT= "error.combo.select";
	
	/** The Constant PROPERTYFILE. */
	public static final String PROPERTYFILE = "ValidationMessages";
	
	/** The Constant HOLIDAY_TYPE_PERMANENT. */
	//HOLIDAY TYPES
	public static final String HOLIDAY_TYPE_PERMANENT = "holiday.type.permanent";
	
	/** The Constant HOLIDAY_TYPE_VARIABLE. */
	public static final String HOLIDAY_TYPE_VARIABLE = "holiday.type.variable";
	
	/** The Constant HOLIDAY_TYPE_NONE. */
	public static final String HOLIDAY_TYPE_NONE = "holiday.type.none";
	
	/** The Constant ERROR_SELECTION_IN_HOLIDAY_DATE. */
	public static final String ERROR_SELECTION_IN_HOLIDAY_DATE="error.adm.holiday.register.past.date";
	
	/** The Constant DELETE_HOLIDAY_CONFIRM. */
	public static final String DELETE_HOLIDAY_CONFIRM="amd.feriado.lbl.confirmar.eliminacion";
	
	/** The Constant SAVE_HOLIDAY_CONFIRM. */
	public static final String SAVE_HOLIDAY_CONFIRM="amd.feriado.lbl.confirmar.registro";
	
	/** The Constant DIALOG_HEADER_CONFIRM. */
	// confirmation dialogs headers
		public static final String DIALOG_HEADER_CONFIRM="lbl.header.alert.confirm";
		
		/** The Constant DIALOG_HEADER_REJECT. */
		public static final String DIALOG_HEADER_REJECT="lbl.header.alert.reject";
		
		/** The Constant DIALOG_HEADER_APPROVE. */
		public static final String DIALOG_HEADER_APPROVE="lbl.header.alert.approve";
		
		/** The Constant DIALOG_HEADER_MODIFY. */
		public static final String DIALOG_HEADER_MODIFY="lbl.header.alert.modify";
		
		/** The Constant DIALOG_HEADER_ANNUL. */
		public static final String DIALOG_HEADER_ANNUL="lbl.header.alert.annul";
		
		/** The Constant DIALOG_HEADER_REGISTER. */
		public static final String DIALOG_HEADER_REGISTER="lbl.header.alert.register";
		
		/** The Constant DIALOG_HEADER_SUCCESS. */
		public static final String DIALOG_HEADER_SUCCESS="lbl.header.alert.success";
		
		/** The Constant DIALOG_HEADER_MOTIVE_REJECT. */
		public static final String DIALOG_HEADER_MOTIVE_REJECT="lbl.header.alert.motiveReject";
		
		/** The Constant DIALOG_HEADER_MOTIVE_ANNUL. */
		public static final String DIALOG_HEADER_MOTIVE_ANNUL="lbl.header.alert.motiveAnnul";
		
		/** The Constant DIALOG_HEADER_REVIEW. */
		public static final String DIALOG_HEADER_REVIEW="lbl.header.alert.review";
		
		/** The Constant DIALOG_HEADER_ERROR. */
		public static final String DIALOG_HEADER_ERROR="lbl.header.alert.error";
		
		/** The Constant DIALOG_HEADER_WARNING. */
		public static final String DIALOG_HEADER_WARNING="lbl.header.alert.warning";
		
		/** The Constant DIALOG_HEADER_ALERT. */
		public static final String DIALOG_HEADER_ALERT="lbl.header.alert";
		
		/** The Constant DIALOG_HEADER_ELIMINAR. */
		public static final String DIALOG_HEADER_ELIMINAR="lbl.header.alert.delete";
		
		/** The Constant DIALOG_HEADER_BLOCK. */
		public static final String DIALOG_HEADER_BLOCK="lbl.header.alert.block";
		
		/** The Constant DIALOG_HEADER_UNBLOCK. */
		public static final String DIALOG_HEADER_UNBLOCK="lbl.header.alert.unblock";
		
		/** The Constant MSG_FAIL_MODIFY_QUOTATION. */
		public static final String MSG_FAIL_MODIFY_QUOTATION="msg.modify.fail.quotation";
		
		/** The Constant MSG_SECURITY_NOT_REGISTERED_STATE. */
		public static final String MSG_SECURITY_NOT_REGISTERED_STATE = "msg.security.not.registered.state";
		
		/** The Constant MSG_FAIL_REGISTER_SAME_SECURITY_DATE_MECHANISM. */
		public static final String MSG_FAIL_REGISTER_SAME_SECURITY_DATE_MECHANISM="msg.register.fail.same.security.date.mechanism";		
		
		/** The Constant MECHANISM_REQUIRED. */
		public static final String MECHANISM_REQUIRED="quotation.error.msg.mechanism.required";
		
		/** The Constant MSG_NO_DATA_MODIFY. */
		public static final String MSG_NO_DATA_MODIFY = "msg.quotation.notmodify";
		
		/** The Constant MSG_MODIFY_SELECT_SINGLE_RECORD. */
		public static final String MSG_MODIFY_SELECT_SINGLE_RECORD="msg.modfiy.single.record";
		
		/** The Constant MSG_REMOVER_ERROR_OPER. */
		public static final String MSG_REMOVER_ERROR_OPER="msg.remove.error";
		
		/** The Constant MSG_REMOVE_OPER_SUCC. */
		public static final String MSG_REMOVE_OPER_SUCC = "msg.remove.oper.succ";
		
		/** The Constant MSG_REMOVE_OPER_CONFIRMATION. */
		public static final String MSG_REMOVE_OPER_CONFIRMATION = "msg.remove.oper.confirm";
		
		/** The Constant MSG_QUOTATION_NOT_ACTIVED_STATE. */
		public static final String MSG_QUOTATION_NOT_ACTIVED_STATE = "msg.quotation.not.actived.state";
		
		/** The Constant INDEXRATE_CONFIRM_REGISTER_INDEX. */
		public static final String INDEXRATE_CONFIRM_REGISTER_INDEX = "indexrate.confirm.register.index";
		
		/** The Constant INDEXRATE_CONFIRM_REGISTER_RATE. */
		public static final String INDEXRATE_CONFIRM_REGISTER_RATE = "indexrate.confirm.register.rate";
		
		/** The Constant INDEXRATE_CONFIRM_MODIFY_INDEX. */
		public static final String INDEXRATE_CONFIRM_MODIFY_INDEX = "indexrate.confirm.modify.index";
		
		/** The Constant INDEXRATE_CONFIRM_MODIFY_RATE. */
		public static final String INDEXRATE_CONFIRM_MODIFY_RATE = "indexrate.confirm.modify.rate";
		
		/** The Constant INDEXRATE_CONFIRM_UPDATE_INDEX. */
		public static final String INDEXRATE_CONFIRM_UPDATE_INDEX = "indexrate.confirm.update.index";
		
		/** The Constant INDEXRATE_CONFIRM_UPDATE_RATE. */
		public static final String INDEXRATE_CONFIRM_UPDATE_RATE = "indexrate.confirm.update.rate";
		
		/** The Constant INDEXRATE_SELECTED_NOT_MODIFIABLE. */
		public static final String INDEXRATE_SELECTED_NOT_MODIFIABLE = "indexrate.selected.not.modifiable";
		
		/** The Constant INDEXRATE_SELECTED_NOT_UPDATABLE. */
		public static final String INDEXRATE_SELECTED_NOT_UPDATABLE = "indexrate.selected.not.updatable";
		
		/** The Constant SCHEDULE_ACTIVE_ERROR. */
		public static final String SCHEDULE_ACTIVE_ERROR = "schedule.active.erorr";
		
		/** The Constant SCHEDULE_ACTIVE_CONFIRM. */
		public static final String SCHEDULE_ACTIVE_CONFIRM ="schedule.active.confirm";
		
		/** The Constant SCHEDULE_DEACTIVE_SUCC. */
		public static final String SCHEDULE_DEACTIVE_SUCC ="schedule.deactive.succ";
		
		/** The Constant SCHEDULE_MODIFY_CONFIRM. */
		public static final String SCHEDULE_MODIFY_CONFIRM ="schedule.modify.confirm";
		
		/** The Constant SCHEDULE_MODIFY_SUCC. */
		public static final String SCHEDULE_MODIFY_SUCC ="schedule.modify.succ";
		
		/** The Constant SCHEDULE_ADD_SUCC. */
		public static final String SCHEDULE_ADD_SUCC ="schedule.add.succ";
		
		/** The Constant SCHEDULE_ADD_CONFIRM. */
		public static final String SCHEDULE_ADD_CONFIRM ="schedule.add.confirm";
		
		/** The Constant SCHEDULE_DEACTIVE_CONFIRM. */
		public static final String SCHEDULE_DEACTIVE_CONFIRM ="schedule.deactive.confirm";
		
		/** The Constant MSG_FAIL_MODIFY_EXCHANGERATE. */
		public static final String MSG_FAIL_MODIFY_EXCHANGERATE = "tipo.msg.unalbe.modify";
		
		/** The Constant MSG_CNF_MODIFY_EXCHANGERATE. */
		public static final String MSG_CNF_MODIFY_EXCHANGERATE = "tipocambio.confirm.modificar";
		
		/** The Constant MSG_CNF_CONFIRMED_EXCHANGERATE. */
		public static final String MSG_CNF_CONFIRMED_EXCHANGERATE = "tipocambio.confirm.confirmed";
		
		public static final String MSG_REGISTER_EXCHANGERATE_WS = "tipocambio.confirm.register.ws";

		/** The Constant NOTIFY_MSG_REGISTER_EXCHANGERATE. */
		public static final String NOTIFY_MSG_REGISTER_EXCHANGERATE = "notificationmsg.registe.dialyExchange";
		
		/** The Constant NOTIFY_MSG_MODIFY_EXCHANGERATE. */
		public static final String NOTIFY_MSG_MODIFY_EXCHANGERATE="notificationmsg.modify.dialyExchange";
		
		/** The Constant NOTIFICATION_ADD_SUCC. */
		public static final String NOTIFICATION_ADD_SUCC ="notification.save.succ";
		
		/** The Constant SCHEDULE_ACTIVE_SUCC. */
		public static final String SCHEDULE_ACTIVE_SUCC ="schedule.active.succ";
		
		/** The Constant NOTIFICATION_DEACTIVE_SUCC. */
		public static final String NOTIFICATION_DEACTIVE_SUCC ="notification.deactive.succ";
		
		/** The Constant NOTIFICATION_DEACTIVE_CONFIRM. */
		public static final String NOTIFICATION_DEACTIVE_CONFIRM ="notification.deactive.confirm";
		
		/** The Constant NOTIFICATION_ACTIVE_SUCC. */
		public static final String NOTIFICATION_ACTIVE_SUCC ="notification.active.succ";
		
		/** The Constant NOTIFICATION_ACTIVE_CONFIRM. */
		public static final String NOTIFICATION_ACTIVE_CONFIRM ="notification.active.confirm";
		
		/** The Constant NOTIFICATION_ACTIVE_ERROR. */
		public static final String NOTIFICATION_ACTIVE_ERROR = "notification.active.erorr";
		
		/** The Constant NOTIFICATION_DEACTIVE_ERROR. */
		public static final String NOTIFICATION_DEACTIVE_ERROR = "notification.deactive.erorr";
		
		/** The Constant SCHEDULE_DEACTIVE_ERROR. */
		public static final String SCHEDULE_DEACTIVE_ERROR = "schedule.deactive.erorr";
		
		/** The Constant NOTIFICATION_MODIFY_SUCC. */
		public static final String NOTIFICATION_MODIFY_SUCC ="notification.modify.succ";
		
		/** The Constant NOTIFICATION_MODIFY_CONFIRM. */
		public static final String NOTIFICATION_MODIFY_CONFIRM ="notification.modify.confirm";
		
		/** The Constant NOTIFICATION_ADD_CONFIRM. */
		public static final String NOTIFICATION_ADD_CONFIRM ="notification.save.confirm";
		
		/** The Constant NOTIFICATION_ADD_ERROR. */
		public static final String NOTIFICATION_ADD_ERROR="msg.notification.save.error";
		
		/** The Constant SCHEDULE_ADD_NOT_POSSIBLE. */
		public static final String SCHEDULE_ADD_NOT_POSSIBLE="schedule.add.not.possible";
		
		/** Holds Message Notification Type Add Notification. */
		public static final String MSG_ADD_NOITIFICATION_SUCC="msg.notification.add.succ";
		
		/** Holds Message Notification Type Modify Notification. */
		public static final String MSG_MODIFY_NOITIFICATION_SUCC="msg.notification.modify.succ";
		
		/** Holds Message Notification Type Active Notification. */
		public static final String MSG_ACTIVE_NOITIFICATION_SUCC = "msg.notification.active.succ";
		
		/** Holds Message Notification Type InActive Notification. */
		public static final String MSG_DEACTIVE_NOITIFICATION_SUCC = "msg.notification.deactive.succ";
		
		/** Holds Message Notification Type Add SCHEDULE. */
		public static final String MSG_ADD_SCHEDULE_SUCC = "msg.schedule.add.succ";
		
		/** Holds Message Notification Type Modify SCHEDULE. */
		public static final String MSG_MODIFY_SCHEDULE_SUCC = "msg.schedule.modify.succ";
		
		/** Holds Message Notification Type Active SCHEDULE. */
		public static final String MSG_ACTIVE_SCHEDULE_SUCC = "msg.schedule.active.succ";
		
		/** Holds Message Notification Type Inactive SCHEDULE. */
		public static final String MSG_DEACTIVE_SCHEDULE_SUCC = "msg.schedule.deactive.succ";

		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_VALIDATION_STRUCTURE. */
		/*MESSAGES FOR EXTERNAL INTERFACE*/
		public static final String EXTERNAL_INTERFACE_TEMPLATE_VALIDATION_STRUCTURE = "external.interface.template.validation.structure";
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_REGISTER_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_REGISTER_CONFIRM = "external.interface.template.register.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_REGISTER_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_REGISTER_CONFIRM_OK="external.interface.template.register.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_MODIFY_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_MODIFY_CONFIRM = "external.interface.template.modify.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_MODIFY_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_MODIFY_CONFIRM_OK="external.interface.template.modify.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_CONFIRM_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_CONFIRM_CONFIRM = "external.interface.template.confirm.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_CONFIRM_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_CONFIRM_CONFIRM_OK="external.interface.template.confirm.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_ANNULAR_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_ANNULAR_CONFIRM = "external.interface.template.annular.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_ANNULAR_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_ANNULAR_CONFIRM_OK="external.interface.template.annular.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_BLOCK_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_BLOCK_CONFIRM = "external.interface.template.block.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_BLOCK_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_BLOCK_CONFIRM_OK="external.interface.template.block.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_UNBLOCK_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_UNBLOCK_CONFIRM = "external.interface.template.unblock.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_TEMPLATE_UNBLOCK_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_TEMPLATE_UNBLOCK_CONFIRM_OK="external.interface.template.unblock.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_VALIDATION_EXTENSION. */
		public static final String EXTERNAL_INTERFACE_VALIDATION_EXTENSION = "external.interface.validation.extension";
		
		/** The Constant EXTERNAL_INTERFACE_VALIDATION_USERTYPE. */
		public static final String EXTERNAL_INTERFACE_VALIDATION_USERTYPE = "external.interface.validation.usertype";
		
		/** The Constant EXTERNAL_INTERFACE_REGISTER_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_REGISTER_CONFIRM = "external.interface.register.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_REGISTER_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_REGISTER_CONFIRM_OK="external.interface.register.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_MODIFY_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_MODIFY_CONFIRM = "external.interface.modify.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_MODIFY_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_MODIFY_CONFIRM_OK="external.interface.modify.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_CONFIRM_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_CONFIRM_CONFIRM = "external.interface.confirm.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_CONFIRM_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_CONFIRM_CONFIRM_OK="external.interface.confirm.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_ANNULAR_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_ANNULAR_CONFIRM = "external.interface.annular.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_ANNULAR_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_ANNULAR_CONFIRM_OK="external.interface.annular.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_BLOCK_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_BLOCK_CONFIRM = "external.interface.block.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_BLOCK_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_BLOCK_CONFIRM_OK="external.interface.block.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_UNBLOCK_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_UNBLOCK_CONFIRM = "external.interface.unblock.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_UNBLOCK_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_UNBLOCK_CONFIRM_OK="external.interface.unblock.confirm.ok";
		
		/** The Constant EXTERNAL_INTERFACE_VALIDATION_QUERYS. */
		public static final String EXTERNAL_INTERFACE_VALIDATION_QUERYS = "external.interface.validation.querys";		
		
		/** The Constant EXTERNAL_INTERFACE_VALIDATION_PREDICATE. */
		public static final String EXTERNAL_INTERFACE_VALIDATION_PREDICATE = "external.interface.validation.predicate";
		
		/** The Constant EXTERNAL_INTERFACE_VALIDATION_FILTERS. */
		public static final String EXTERNAL_INTERFACE_VALIDATION_FILTERS = "external.interface.validation.filters";
		
		/** The Constant EXTERNAL_INTERFACE_VALIDATION_FILTER_USER_TYPE. */
		public static final String EXTERNAL_INTERFACE_VALIDATION_FILTER_USER_TYPE = "external.interface.validation.filter.user.type";
		
		/** The Constant EXTERNAL_INTERFACE_SEND_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_SEND_CONFIRM = "external.interface.send.confirm";	
		
		/** The Constant EXTERNAL_INTERFACE_SEND_CONFIRM_OK. */
		public static final String EXTERNAL_INTERFACE_SEND_CONFIRM_OK = "external.interface.send.confirm.ok";	
		
		/** The Constant MESSAGES_ALERT. */
		public static final String MESSAGES_ALERT="messages.alert";
		
		/** The Constant REPORT_SEND_SUCCESS_MSG. */
		public static final String REPORT_SEND_SUCCESS_MSG="admin.reportes.msg.report.success";
		
		/** The Constant IDLE_TIME_EXPIRED. */
		public static final String IDLE_TIME_EXPIRED = "idle.session.expired";
		
		/** The Constant EXTERNAL_INTERFACE_RECEPTION_CONFIRM. */
		public static final String EXTERNAL_INTERFACE_RECEPTION_CONFIRM = "external.interface.reception.confirm";
		
		/** The Constant EXTERNAL_INTERFACE_RECEPTION_OK. */
		public static final String EXTERNAL_INTERFACE_RECEPTION_OK = "external.interface.reception.ok";
		
		/** The Constant EXTERNAL_INTERFACE_RECEPTION_ERROR. */
		public static final String EXTERNAL_INTERFACE_RECEPTION_ERROR = "external.interface.reception.error";
		
		/** The Constant EXTERNAL_INTERFACE_RECEPTION_SEND_ERROR. */
		public static final String EXTERNAL_INTERFACE_RECEPTION_SEND_ERROR = "external.interface.reception.send.error";
		
		/** The Constant EXTERNAL_INTERFACE_RECEPTION_SEARCH_ERROR. */
		public static final String EXTERNAL_INTERFACE_RECEPTION_SEARCH_ERROR = "external.interface.reception.search.error";
		
		/**  Validation interface reception. */
		public static final String EXTERNAL_INTERFACE_ERROR_STRUCTURE_INTERFACE = "external.interface.error.structure.interface";
		
		/** The Constant EXTERNAL_INTERFACE_ERROR_DATA_INCONSISTENT. */
		public static final String EXTERNAL_INTERFACE_ERROR_DATA_INCONSISTENT = "external.interface.error.data.inconsistent";
		
		/** The Constant EXTERNAL_INTERFACE_ERROR_COUPON_NUMBER. */
		public static final String EXTERNAL_INTERFACE_ERROR_COUPON_NUMBER = "external.interface.error.coupon.number";
		
		/** The Constant EXTERNAL_INTERFACE_ERROR_LEGAL_REPRESENTATIVE. */
		public static final String EXTERNAL_INTERFACE_ERROR_LEGAL_REPRESENTATIVE = "external.interface.error.legal.representative";
		
		/** The Constant EXTERNAL_INTERFACE_ERROR_ACCOUNT_REPRESENTATIVE. */
		public static final String EXTERNAL_INTERFACE_ERROR_ACCOUNT_REPRESENTATIVE = "external.interface.error.account.representative";
		
		/** The Constant EXTERNAL_INTERFACE_ERROR_ACCOUNT_NATURAL. */
		public static final String EXTERNAL_INTERFACE_ERROR_ACCOUNT_NATURAL = "external.interface.error.account.natural";
		
		/** The Constant EXTERNAL_INTERFACE_ERROR_INCONSISTENT. */
		public static final String EXTERNAL_INTERFACE_ERROR_INCONSISTENT = "external.interface.error.inconsistent";
		
		/** The Constant EXTERNAL_INTERFACE_ERROR_INCOMPLETE. */
		public static final String EXTERNAL_INTERFACE_ERROR_INCOMPLETE = "external.interface.error.incomplete";
		
		/** The Constant ALT_WEB_SERVICE_ERROR_DOWNLOADING_FILE. */
		public static final String ALT_WEB_SERVICE_ERROR_DOWNLOADING_FILE = "alt.webservice.error.downloading.file";
		
		/** The Constant ALT_WEB_SERVICE_NO_SELECTED. */
		public static final String ALT_WEB_SERVICE_NO_SELECTED = "alt.webservice.no.selected";
		
		/** The Constant ALT_INTERFACE_FILE_NO_SELECTED. */
		public static final String ALT_INTERFACE_FILE_NO_SELECTED = "alt.interface.file.no.selected";
		
		/** The Constant MESSAGE_DATA_REQUIRED. */
		public static final String MESSAGE_DATA_REQUIRED = "message.data.required";
		
		/** The Constant MSG_WEBSERVICE_PROCESS_ASK. */
		public static final String MSG_WEBSERVICE_PROCESS_ASK = "msg.webservice.process.ask";
		
		/** The Constant MSG_WEBSERVICE_PROCESS_SUCCESS. */
		public static final String MSG_WEBSERVICE_PROCESS_SUCCESS = "msg.webservice.process.success";
		
		/** The Constant MSG_WEBSERVICE_PROCESS_MONEY_EXCHANGE. */
		public static final String MSG_WEBSERVICE_PROCESS_MONEY_EXCHANGE = "msg.webservice.process.money.exchange";
		
		public static final String LBL_VALIDATION_QUANTITY_ROWSELECTED_SECURITIES="lbl.validation.quantity.rowSelected.securities"; 
		
		public static final String LBL_VALIDATION_QUANTITY_REQUIRED_SELECTEDROW="lbl.validation.quantity.required.selectedRow";
		
		public static final String MSG_MONITOR_SYSTEM_PROCESS_USER_EMPTY= "monitor.system.process.user.empty";
}
