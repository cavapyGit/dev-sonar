package com.pradera.generalparameters.movement.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketMovementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
//movimientos de hechos de mercado
public class MarketMovementTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/**
	 *  SELECT HMM.MOVEMENT_DATE FECHA_MOV,--------------------0
		 HMM.ID_MARKETFACT_MOVEMENT_PK NRO_MOVIMIENTO,----------1
		 HAM.OPERATION_DATE FECHA_OPE, -------------------------2
		 HAM.OPERATION_NUMBER NRO_OPERACION,--------------------3
		 HAM.ID_MECHANISM MECANISMO, ---------------------------4
		 HAM.ID_MODALITY MODALIDAD, ----------------------------5
		 mvt.MOVEMENT_NAME NOM_MOV,  ---------------------------6
		CASE  mvb.id_behavior  
		  WHEN 1 THEN  'SUMA - '  
		  WHEN 2 THEN 'RESTA - '  
		  ELSE  ''  
		END || balTyp.balance_name AS NOM_BALANCE,--------------7
		HAM.ID_SOURCE_PARTICIPANT PART_ORIGEN,------------------8
		HAM.ID_TARGET_PARTICIPANT PART_DEST,--------------------9
		HMM.MOVEMENT_QUANTITY CANTIDAD, ------------------------10
		hmm.MARKET_PRICE PRECIO--------------------------------11*/
	private Date movementDate;
	
	/** The nro movement. */
	private Long nroMovement;// este es el pk que importa
	
	/** The operation date. */
	private Date operationDate;
	
	/** The nro operation. */
	private Long nroOperation;
	
	/** The mechanism. */
	private String mechanism;
	
	/** The modality. */
	private String modality;
	
	/** The description. */
	private String description;//NOM_BALANCE
	
	/** The help message. */
	private String helpMessage;
	
	/** The source participant. */
	private String sourceParticipant;
	
	/** The target participant. */
	private String targetParticipant;
	
	/** The movement quantity. */
	private BigDecimal movementQuantity;
	
	/** The operation price. */
	private BigDecimal operationPrice;
	
	/**
	 * Instantiates a new market movement to.
	 */
	public MarketMovementTO() {
		movementDate=null;
		nroMovement=null;//
		operationDate=null;
		nroOperation=null;
		mechanism=null;
		modality=null;
		description=null;//NOM_BALANCE
		helpMessage=null;
		sourceParticipant=null;
		targetParticipant=null;
		movementQuantity=null;
		operationPrice=null;
	}
	
	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	
	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	
	/**
	 * Gets the nro movement.
	 *
	 * @return the nro movement
	 */
	public Long getNroMovement() {
		return nroMovement;
	}
	
	/**
	 * Sets the nro movement.
	 *
	 * @param nroMovement the new nro movement
	 */
	public void setNroMovement(Long nroMovement) {
		this.nroMovement = nroMovement;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the nro operation.
	 *
	 * @return the nro operation
	 */
	public Long getNroOperation() {
		return nroOperation;
	}
	
	/**
	 * Sets the nro operation.
	 *
	 * @param nroOperation the new nro operation
	 */
	public void setNroOperation(Long nroOperation) {
		this.nroOperation = nroOperation;
	}
	
	/**
	 * Gets the mechanism.
	 *
	 * @return the mechanism
	 */
	public String getMechanism() {
		return mechanism;
	}
	
	/**
	 * Sets the mechanism.
	 *
	 * @param mechanism the new mechanism
	 */
	public void setMechanism(String mechanism) {
		this.mechanism = mechanism;
	}
	
	/**
	 * Gets the modality.
	 *
	 * @return the modality
	 */
	public String getModality() {
		return modality;
	}
	
	/**
	 * Sets the modality.
	 *
	 * @param modality the new modality
	 */
	public void setModality(String modality) {
		this.modality = modality;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the help message.
	 *
	 * @return the help message
	 */
	public String getHelpMessage() {
		return helpMessage;
	}
	
	/**
	 * Sets the help message.
	 *
	 * @param helpMessage the new help message
	 */
	public void setHelpMessage(String helpMessage) {
		this.helpMessage = helpMessage;
	}
	
	/**
	 * Gets the source participant.
	 *
	 * @return the source participant
	 */
	public String getSourceParticipant() {
		return sourceParticipant;
	}
	
	/**
	 * Sets the source participant.
	 *
	 * @param sourceParticipant the new source participant
	 */
	public void setSourceParticipant(String sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}
	
	/**
	 * Gets the target participant.
	 *
	 * @return the target participant
	 */
	public String getTargetParticipant() {
		return targetParticipant;
	}
	
	/**
	 * Sets the target participant.
	 *
	 * @param targetParticipant the new target participant
	 */
	public void setTargetParticipant(String targetParticipant) {
		this.targetParticipant = targetParticipant;
	}
	
	/**
	 * Gets the movement quantity.
	 *
	 * @return the movement quantity
	 */
	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}
	
	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the new movement quantity
	 */
	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}
	
	/**
	 * Gets the operation price.
	 *
	 * @return the operation price
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}
	
	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the new operation price
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}
	
	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
