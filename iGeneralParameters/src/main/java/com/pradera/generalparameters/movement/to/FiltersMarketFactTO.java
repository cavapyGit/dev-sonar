package com.pradera.generalparameters.movement.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FiltersMarketFactTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
// esta clase reune los datos para la busqueda
public class FiltersMarketFactTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant. */
	private Long idParticipant; 
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The holder. */
	private Holder holder;
	
	/** The id security code. */
	private String idSecurityCode;//clase y clave de valor
	
	/** The date type. */
	private Long dateType;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/**
	 * Instantiates a new filters market fact to.
	 */
	public FiltersMarketFactTO(){
		idParticipant=null;
		holderAccount=null;
		holder=null;
		idSecurityCode=null;
		dateType=1512L;//fecha de corte, luego llevar a la tabla parameter table.
		initialDate=CommonsUtilities.currentDate();
		finalDate=CommonsUtilities.currentDate();
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	/**
	 * Gets the date type.
	 *
	 * @return the date type
	 */
	public Long getDateType() {
		return dateType;
	}

	/**
	 * Sets the date type.
	 *
	 * @param dateType the new date type
	 */
	public void setDateType(Long dateType) {
		this.dateType = dateType;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return "Participante pk:"+idParticipant+
				"\nHolder Account:"+holderAccount+
				"\nValor:"+idSecurityCode;
	}
}
