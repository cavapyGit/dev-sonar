package com.pradera.generalparameters.movement.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.movement.to.FiltersMarketFactTO;
import com.pradera.generalparameters.movement.to.MarketFactTO;
import com.pradera.generalparameters.movement.to.MarketMovementTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MovementsMarketEventsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MovementsMarketEventsServiceBean extends CrudDaoServiceBean {
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 *  Method to get the facts market by the date range.
	 *
	 * @param filters the filters
	 * @param stockCalculationId the stock calculation id
	 * @return Returns a list of objects MarketFactTO type.
	 * @Param Search Filters and id stock calculation.
	 */
	public List<MarketFactTO> getHolderMarketFactsFromCalculoStock(
			FiltersMarketFactTO filters, Long stockCalculationId){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select new com.pradera.generalparameters.movement.to.MarketFactTO("
				+ "scm.participant,"
				+ "scm.holderAccount,"
				+ "scm.security,"
				+ "scm.marketRate,"
				+ "scm.marketDate,"
				+ "scm.marketPrice)"
				+ "FROM StockCalculationMarketfact scm	");
		sbQuery.append("WHERE scm.participant.idParticipantPk = :idParticipantParam	");
		sbQuery.append("AND scm.holderAccount.idHolderAccountPk = :idHolderAccountParam	");
		sbQuery.append("AND scm.security.idSecurityCodePk = :idSecurityCodeParam ");
		sbQuery.append("AND scm.stockCalculationProcess.idStockCalculationPk = :idStockCalculationPkParam ");
		Query queryJpql = em.createQuery(sbQuery.toString());
		queryJpql.setParameter("idParticipantParam",filters.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam", filters.getHolderAccount().getIdHolderAccountPk());
		queryJpql.setParameter("idSecurityCodeParam",filters.getIdSecurityCode());
		queryJpql.setParameter("idStockCalculationPkParam",stockCalculationId);
		
		@SuppressWarnings("unchecked")
		List<MarketFactTO> results = queryJpql.getResultList();
		
		if(Validations.validateIsNotNullAndNotEmpty(results))
			try {
				return getHolderMarketFacts(filters);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		for (MarketFactTO marketFactTO : results) {
			List<MarketMovementTO> listAux=new ArrayList<MarketMovementTO>();
			listAux=getMarketsMovements(marketFactTO, filters);
			marketFactTO.setMovementMarketFacts(new ArrayList<MarketMovementTO>());
			marketFactTO.getMovementMarketFacts().addAll(listAux);
		}
		return results;
	}
	
	/**
	 *  Method that returns the market events.
	 *
	 * @param filters the filters
	 * @return Returns a list of objects MarketFactTO type.
	 * @throws ServiceException the service exception
	 * @Param Search Filters (FiltersMarketFactTO filters)
	 */
	//Fecha de corte
	public List<MarketFactTO> getHolderMarketFacts(
			FiltersMarketFactTO filters) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select new com.pradera.generalparameters.movement.to.MarketFactTO("
				+ "hmb.participant,"
				+ "hmb.holderAccount,"
				+ "hmb.security,"
				+ "hmb.marketRate,"
				+ "hmb.marketDate,"
				+ "hmb.marketPrice)"
				+ "FROM HolderMarketFactBalance hmb	");
		sbQuery.append("WHERE hmb.participant.idParticipantPk = :idParticipantParam	");
		sbQuery.append("AND hmb.holderAccount.idHolderAccountPk = :idHolderAccountParam	");
		sbQuery.append("AND hmb.security.idSecurityCodePk = :idSecurityCodeParam ");
		Query queryJpql = em.createQuery(sbQuery.toString());
		queryJpql.setParameter("idParticipantParam",filters.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam", filters.getHolderAccount().getIdHolderAccountPk());
		queryJpql.setParameter("idSecurityCodeParam",filters.getIdSecurityCode());
		
		System.out.println("DATOS-HECHO.\nPARTICIPANTE:"+filters.getIdParticipant()+""
				+ "\nCUI-TITULAR:"+filters.getHolderAccount()+""
				+ "\nCLAVE DE VALOR:"+filters.getIdSecurityCode());
		@SuppressWarnings("unchecked")
		List<MarketFactTO> results = queryJpql.getResultList();
		if(Validations.validateIsNotNullAndNotEmpty(results))
			for (MarketFactTO marketFactTO : results) {
				List<MarketMovementTO> listAux=new ArrayList<MarketMovementTO>();
				listAux=getMarketsMovements(marketFactTO, filters);
				marketFactTO.setMovementMarketFacts(new ArrayList<MarketMovementTO>());
				marketFactTO.getMovementMarketFacts().addAll(listAux);
			}
		else
			return null;
		return results;
	}
	
	/**
	 *  Returns market movements.
	 *
	 * @param marketFactTO the market fact to
	 * @param filters the filters
	 * @return Returns a list of objects of type MarketFactTO.
	 */
	@SuppressWarnings({ "unchecked"})
	private List<MarketMovementTO> getMarketsMovements(MarketFactTO marketFactTO,FiltersMarketFactTO filters) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(""
				+ " SELECT HMM.MOVEMENT_DATE FECHA_MOV, "
				+ " HMM.ID_MARKETFACT_MOVEMENT_PK NRO_MOVIMIENTO, "
				+ " HAM.OPERATION_DATE FECHA_OPE, HAM.OPERATION_NUMBER NRO_OPERACION, "
				+ " (SELECT NM.MECHANISM_CODE||  ' - ' ||NM.DESCRIPTION FROM NEGOTIATION_MECHANISM NM WHERE HAM.ID_MECHANISM =NM.ID_NEGOTIATION_MECHANISM_PK) MECANISMO, "
				+ " (SELECT NMO.MODALITY_NAME FROM NEGOTIATION_MODALITY NMO WHERE NMO.ID_NEGOTIATION_MODALITY_PK=HAM.ID_MODALITY) MODALIDAD, "
				+ " mvt.MOVEMENT_NAME NOM_MOV,"
				+ "	CASE  mvb.id_behavior  WHEN 1 THEN  'SUMA - '  WHEN 2 THEN 'RESTA - '   ELSE  ''  END || balTyp.balance_name AS NOM_BALANCE,"
				+ " (select PA.MNEMONIC||  ' - ' ||PA.ID_PARTICIPANT_PK from PARTICIPANT PA where PA.ID_PARTICIPANT_PK=HAM.ID_SOURCE_PARTICIPANT) PART_ORIGEN,"
				+ " (select PA.MNEMONIC||  ' - ' ||PA.ID_PARTICIPANT_PK from PARTICIPANT PA where PA.ID_PARTICIPANT_PK=HAM.ID_TARGET_PARTICIPANT) PART_DEST,"
				+ " HMM.MOVEMENT_QUANTITY CANTIDAD, "
				+ " HMM.MARKET_PRICE PRECIO"
				+ " FROM HOLDER_MARKETFACT_MOVEMENT hmm INNER JOIN MOVEMENT_TYPE mvt ON mvt.ID_MOVEMENT_TYPE_PK = HMM.ID_MOVEMENT_TYPE_FK "
				+ " LEFT OUTER JOIN MOVEMENT_BEHAVIOR mvb ON mvb.ID_MOVEMENT_TYPE_FK = mvt.ID_MOVEMENT_TYPE_PK  "
				+ " LEFT OUTER JOIN BALANCE_TYPE balTyp ON balTyp.id_balance_type_pk = mvb.id_balance_type_fk "
				+ " LEFT JOIN HOLDER_ACCOUNT_MOVEMENT HAM ON (HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK=hmm.ID_HOLDER_ACCOUNT_MOVEMENT_FK) ");
		
		sbQuery.append(" WHERE 1=1 ");
	    sbQuery.append(" AND HMM.ID_PARTICIPANT_FK = :idParticipantParam ");
		sbQuery.append(" AND hmm.ID_HOLDER_ACCOUNT_FK = :idHolderAccountParam ");
		sbQuery.append(" AND HMM.ID_SECURITY_CODE_FK = :idSecurityCodeParam ");
		
		if(Validations.validateIsNotNullAndNotEmpty(marketFactTO.getMarketDate()))
			sbQuery.append(" AND HMM.MARKET_DATE = :marketDateParam ");
		
		if(Validations.validateIsNotNullAndNotEmpty(marketFactTO.getMarketPrice()))
			sbQuery.append(" AND HMM.MARKET_PRICE = :marketPriceParam ");
		
		if(Validations.validateIsNotNullAndNotEmpty(marketFactTO.getMarketRate()))
			sbQuery.append(" AND HMM.MARKET_RATE = :marketRateParam ");
		//para la fecha de corte, tomar en cuenta solo la fecha inicial
				if(filters.getDateType()==1512)
					if(Validations.validateIsNotNullAndNotEmpty(filters.getInitialDate())){
						sbQuery.append(" AND TRUNC(HMM.MOVEMENT_DATE) <= TRUNC(:initialDateParam) ");
					}
		//Para el rango(1513) de fecha se cosidera la fecha inicial y fecha final
		if(filters.getDateType()==1513)
			if(Validations.validateIsNotNullAndNotEmpty(filters.getFinalDate())){
				sbQuery.append(" AND TRUNC(HMM.MOVEMENT_DATE) >= TRUNC(:initialDateParam) ");	
				sbQuery.append(" AND TRUNC(HMM.MOVEMENT_DATE) <= TRUNC(:finalDateParam) ");
			}
		sbQuery.append(" order BY hmm.ID_MARKETFACT_MOVEMENT_PK desc, HMM.MOVEMENT_DATE desc");
		
		Query queryJpql = em.createNativeQuery(sbQuery.toString());
		
		queryJpql.setParameter("idParticipantParam", marketFactTO.getParticipant().getIdParticipantPk());
		queryJpql.setParameter("idHolderAccountParam",marketFactTO.getHolderAccount().getIdHolderAccountPk());
		queryJpql.setParameter("idSecurityCodeParam", marketFactTO.getSecurity().getIdSecurityCodePk());
		
		if(Validations.validateIsNotNullAndNotEmpty(marketFactTO.getMarketDate()))
			queryJpql.setParameter("marketDateParam", marketFactTO.getMarketDate());
		
		if(Validations.validateIsNotNullAndNotEmpty(marketFactTO.getMarketPrice()))
			queryJpql.setParameter("marketPriceParam", marketFactTO.getMarketPrice());
		
		if(Validations.validateIsNotNullAndNotEmpty(marketFactTO.getMarketRate()))
			queryJpql.setParameter("marketRateParam", marketFactTO.getMarketRate());
		//Para rango de fecha.
		if(filters.getDateType()==1513)
			if(Validations.validateIsNotNullAndNotEmpty(filters.getFinalDate())){
				queryJpql.setParameter("initialDateParam", filters.getInitialDate());
				queryJpql.setParameter("finalDateParam", filters.getFinalDate());
			}
				
		if(filters.getDateType()==1512)//para la fecha de corte
			if(Validations.validateIsNotNullAndNotEmpty(filters.getInitialDate()))
				queryJpql.setParameter("initialDateParam", filters.getInitialDate());

		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)queryJpql.getResultList();
		}catch(NoResultException ex){
			System.out.println("No se pudo obtener los movimientos de mercado");
			return null;
		}
		
		/**
		 *  SELECT HMM.MOVEMENT_DATE FECHA_MOV,---------------------0
			 HMM.ID_MARKETFACT_MOVEMENT_PK NRO_MOVIMIENTO,----------1
			 HAM.OPERATION_DATE FECHA_OPE, -------------------------2
			 HAM.OPERATION_NUMBER NRO_OPERACION,--------------------3
			 HAM.ID_MECHANISM MECANISMO, ---------------------------4
			 HAM.ID_MODALITY MODALIDAD, ----------------------------5
			 mvt.MOVEMENT_NAME NOM_MOV,  ---------------------------6 descripsion
			CASE  mvb.id_behavior  
			  WHEN 1 THEN  'SUMA - '  
			  WHEN 2 THEN 'RESTA - '  
			  ELSE  ''  
			END || balTyp.balance_name AS NOM_BALANCE,--------------7
			HAM.ID_SOURCE_PARTICIPANT PART_ORIGEN,------------------8
			HAM.ID_TARGET_PARTICIPANT PART_DEST,--------------------9
			HMM.MOVEMENT_QUANTITY CANTIDAD, ------------------------10
			hmm.MARKET_PRICE PRECIO--------------------------------11*/
		List<MarketMovementTO> list = new ArrayList<MarketMovementTO>();
		for(Object[] movementObjet: objectData){
			MarketMovementTO momementResult = new MarketMovementTO();
			momementResult.setMovementDate((Date)movementObjet[0]);
			momementResult.setNroMovement(((BigDecimal)(movementObjet[1]==null?BigDecimal.ZERO:movementObjet[1])).longValue());
			momementResult.setOperationDate((Date)movementObjet[2]);
			momementResult.setNroOperation(((BigDecimal)(movementObjet[3]==null?BigDecimal.ZERO:movementObjet[3])).longValue());
			momementResult.setMechanism((movementObjet[4]!=null)?String.valueOf(movementObjet[4]):"");
			momementResult.setModality((movementObjet[5]!=null)?String.valueOf(movementObjet[5]):"");
			momementResult.setDescription(String.valueOf(movementObjet[6]));
			momementResult.setHelpMessage(String.valueOf(movementObjet[7]));
			momementResult.setSourceParticipant((String.valueOf(movementObjet[8])!=null)?String.valueOf(movementObjet[8]):"");
			momementResult.setTargetParticipant((String.valueOf(movementObjet[9])!=null)?String.valueOf(movementObjet[9]):"");
			momementResult.setMovementQuantity(((BigDecimal)(movementObjet[10]==null?BigDecimal.ZERO:movementObjet[10])));
			momementResult.setOperationPrice(((BigDecimal)(movementObjet[11]==null?BigDecimal.ZERO:movementObjet[11])));
			list.add(momementResult);
		}
		return simplifyList(list);
		
	}
	/**
	 * Removes duplicate.
	 * @param list MarketMovementTO
	 * @return list MarketMovementTO
	 */
	private List<MarketMovementTO> simplifyList(List<MarketMovementTO> list) {
		HashMap<Long, MarketMovementTO> liHashMap=new HashMap<Long, MarketMovementTO>();
		for (MarketMovementTO mov : list) 
			liHashMap.put(mov.getNroMovement(), mov);
		List<MarketMovementTO> listAux = new ArrayList<MarketMovementTO>();
		Iterator it = liHashMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry e = (Map.Entry)it.next();
			Object pk=e.getKey();
			MarketMovementTO to=(MarketMovementTO) e.getValue();
			String msg="";
			
			for (MarketMovementTO date : list) 
				if (pk.equals(date.getNroMovement())) 
					msg=msg+date.getHelpMessage()+ "</br>";
			to.setHelpMessage(msg);
			listAux.add((MarketMovementTO)e.getValue());
		}
		return listAux;
	}
	
	/**
	 * Gets the total balance.
	 *
	 * @param movementsSearchTO the movements search to
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance(FiltersMarketFactTO movementsSearchTO){
		//System.out.println("PAR:"+movementsSearchTO.getIdParticipant()+ " ACC:"+movementsSearchTO.getHolderAccount().getIdHolderAccountPk()+" SEC:"+movementsSearchTO.getIdSecurityCode());
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select hab.totalBalance from HolderAccountBalance hab	");
		sbQuery.append(" where hab.participant.idParticipantPk = :idParticipantParam ");
		sbQuery.append(" and hab.holderAccount.idHolderAccountPk = :idHolderAccountParam ");
		sbQuery.append(" and hab.security.idSecurityCodePk = :idSecurityCodeParam ");
		
		Query queryJpql = em.createQuery(sbQuery.toString());
		
		queryJpql.setParameter("idParticipantParam",movementsSearchTO.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam",movementsSearchTO.getHolderAccount().getIdHolderAccountPk());
		queryJpql.setParameter("idSecurityCodeParam",movementsSearchTO.getIdSecurityCode());
		BigDecimal totalBalance;
		try {
			totalBalance=(BigDecimal) queryJpql.getSingleResult();
		} catch (Exception e) {
			return BigDecimal.ZERO;
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(totalBalance))
			return totalBalance;
		return BigDecimal.ZERO;
	}	
	
	/**
	 * Gets the security.
	 *
	 * @param pkSecurityCode the pk security code
	 * @return the security
	 */
	public Security getSecurity(String pkSecurityCode){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select se from Security se	");
		sbQuery.append(" where se.idSecurityCodePk = :idCodeSecurity ");
		
		Query queryJpql = em.createQuery(sbQuery.toString());
		queryJpql.setParameter("idCodeSecurity",pkSecurityCode);
		try {
			Security security=(Security) queryJpql.getSingleResult();
			return security;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param idParticipant the id participant
	 * @return the participant
	 */
	public Participant getParticipant(Long idParticipant){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select pa from Participant pa	");
		sbQuery.append(" where pa.idParticipantPk = :idParticipant ");
		
		Query queryJpql = em.createQuery(sbQuery.toString());
		queryJpql.setParameter("idParticipant",idParticipant);
		try {
			Participant participant=(Participant) queryJpql.getSingleResult();
			return participant;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the mechanism modality list service bean.
	 *
	 * @return the mechanism modality list service bean
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getMechanismModalityListServiceBean() throws ServiceException{
		String querySql = "SELECT nm.ID_NEGOTIATION_MODALITY_PK, nm.MODALITY_NAME FROM NEGOTIATION_MODALITY nm";
		Query queryJpql = em.createNativeQuery(querySql);
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)queryJpql.getResultList();
		}catch(NoResultException ex){
			ex.printStackTrace();
			return null;
		}
		List<NegotiationModality> negotiationModalityList = new ArrayList<NegotiationModality>();
		for(Object[] objectRow: objectData){
			NegotiationModality negotiationModality = new NegotiationModality();
			negotiationModality.setIdNegotiationModalityPk(((BigDecimal)objectRow[0]).longValue());
			negotiationModality.setModalityName((String)objectRow[1]);
			negotiationModalityList.add(negotiationModality);
		}
		return negotiationModalityList;
	}
}