package com.pradera.generalparameters.movement.to;

import java.math.BigDecimal;

import com.pradera.model.component.HolderAccountBalance;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderBalanceResult.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class HolderBalanceResult {
	
	/** The available balance. */
	protected BigDecimal availableBalance;
	
	/** The transit balance. */
	protected BigDecimal transitBalance;
	
	/** The pawn balance. */
	protected BigDecimal pawnBalance;
	
	/** The ban balance. */
	protected BigDecimal banBalance;
	
	/** The other block balance. */
	protected BigDecimal otherBlockBalance;
	
	/** The reserve balance. */
	protected BigDecimal reserveBalance;
	
	/** The opposition balance. */
	protected BigDecimal oppositionBalance;
	
	/** The accreditation balance. */
	protected BigDecimal accreditationBalance;
	
	/** The purchase balance. */
	protected BigDecimal purchaseBalance;
	
	/** The sale balance. */
	protected BigDecimal saleBalance;
	
	/** The reporting balance. */
	protected BigDecimal reportingBalance;
	
	/** The reported balance. */
	protected BigDecimal reportedBalance;
	
	/** The margin balance. */
	protected BigDecimal marginBalance;
	
	/** The borrower balance. */
	protected BigDecimal borrowerBalance;
	
	/** The lender balance. */
	protected BigDecimal lenderBalance;
	
	/** The loanable balance. */
	protected BigDecimal loanableBalance;
	
	/**
	 * Instantiates a new holder balance result.
	 */
	public HolderBalanceResult() {
		availableBalance=new BigDecimal(0);
		transitBalance=new BigDecimal(0);
		pawnBalance=new BigDecimal(0);
		banBalance=new BigDecimal(0);
		otherBlockBalance=new BigDecimal(0);
		reserveBalance=new BigDecimal(0);
		oppositionBalance=new BigDecimal(0);
		accreditationBalance=new BigDecimal(0);
		purchaseBalance=new BigDecimal(0);
		saleBalance=new BigDecimal(0);
		reportingBalance=new BigDecimal(0);
		reportedBalance=new BigDecimal(0);
		marginBalance=new BigDecimal(0);
		borrowerBalance=new BigDecimal(0);
		lenderBalance=new BigDecimal(0);
		loanableBalance=new BigDecimal(0);
	}

	/**
	 * Instantiates a new holder balance result.
	 *
	 * @param availableBalance the available balance
	 * @param transitBalance the transit balance
	 * @param pawnBalance the pawn balance
	 * @param banBalance the ban balance
	 * @param otherBlockBalance the other block balance
	 * @param reserveBalance the reserve balance
	 * @param oppositionBalance the opposition balance
	 * @param accreditationBalance the accreditation balance
	 * @param purchaseBalance the purchase balance
	 * @param saleBalance the sale balance
	 * @param reportingBalance the reporting balance
	 * @param reportedBalance the reported balance
	 * @param marginBalance the margin balance
	 * @param borrowerBalance the borrower balance
	 * @param lenderBalance the lender balance
	 * @param loanableBalance the loanable balance
	 */
	public HolderBalanceResult(BigDecimal availableBalance,
			BigDecimal transitBalance, BigDecimal pawnBalance,
			BigDecimal banBalance, BigDecimal otherBlockBalance,
			BigDecimal reserveBalance, BigDecimal oppositionBalance,
			BigDecimal accreditationBalance, BigDecimal purchaseBalance,
			BigDecimal saleBalance, BigDecimal reportingBalance,
			BigDecimal reportedBalance, BigDecimal marginBalance,
			BigDecimal borrowerBalance, BigDecimal lenderBalance,
			BigDecimal loanableBalance) {
		super();
		this.availableBalance = availableBalance;
		this.transitBalance = transitBalance;
		this.pawnBalance = pawnBalance;
		this.banBalance = banBalance;
		this.otherBlockBalance = otherBlockBalance;
		this.reserveBalance = reserveBalance;
		this.oppositionBalance = oppositionBalance;
		this.accreditationBalance = accreditationBalance;
		this.purchaseBalance = purchaseBalance;
		this.saleBalance = saleBalance;
		this.reportingBalance = reportingBalance;
		this.reportedBalance = reportedBalance;
		this.marginBalance = marginBalance;
		this.borrowerBalance = borrowerBalance;
		this.lenderBalance = lenderBalance;
		this.loanableBalance = loanableBalance;
	}

	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the transit balance.
	 *
	 * @return the transit balance
	 */
	public BigDecimal getTransitBalance() {
		return transitBalance;
	}

	/**
	 * Sets the transit balance.
	 *
	 * @param transitBalance the new transit balance
	 */
	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	/**
	 * Gets the pawn balance.
	 *
	 * @return the pawn balance
	 */
	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	/**
	 * Sets the pawn balance.
	 *
	 * @param pawnBalance the new pawn balance
	 */
	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	/**
	 * Gets the ban balance.
	 *
	 * @return the ban balance
	 */
	public BigDecimal getBanBalance() {
		return banBalance;
	}

	/**
	 * Sets the ban balance.
	 *
	 * @param banBalance the new ban balance
	 */
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * Gets the other block balance.
	 *
	 * @return the other block balance
	 */
	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	/**
	 * Sets the other block balance.
	 *
	 * @param otherBlockBalance the new other block balance
	 */
	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	/**
	 * Gets the reserve balance.
	 *
	 * @return the reserve balance
	 */
	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	/**
	 * Sets the reserve balance.
	 *
	 * @param reserveBalance the new reserve balance
	 */
	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	/**
	 * Gets the opposition balance.
	 *
	 * @return the opposition balance
	 */
	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	/**
	 * Sets the opposition balance.
	 *
	 * @param oppositionBalance the new opposition balance
	 */
	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	/**
	 * Gets the accreditation balance.
	 *
	 * @return the accreditation balance
	 */
	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	/**
	 * Sets the accreditation balance.
	 *
	 * @param accreditationBalance the new accreditation balance
	 */
	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	/**
	 * Gets the purchase balance.
	 *
	 * @return the purchase balance
	 */
	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	/**
	 * Sets the purchase balance.
	 *
	 * @param purchaseBalance the new purchase balance
	 */
	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	/**
	 * Gets the sale balance.
	 *
	 * @return the sale balance
	 */
	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	/**
	 * Sets the sale balance.
	 *
	 * @param saleBalance the new sale balance
	 */
	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	/**
	 * Gets the reporting balance.
	 *
	 * @return the reporting balance
	 */
	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	/**
	 * Sets the reporting balance.
	 *
	 * @param reportingBalance the new reporting balance
	 */
	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	/**
	 * Gets the reported balance.
	 *
	 * @return the reported balance
	 */
	public BigDecimal getReportedBalance() {
		return reportedBalance;
	}

	/**
	 * Sets the reported balance.
	 *
	 * @param reportedBalance the new reported balance
	 */
	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	/**
	 * Gets the margin balance.
	 *
	 * @return the margin balance
	 */
	public BigDecimal getMarginBalance() {
		return marginBalance;
	}

	/**
	 * Sets the margin balance.
	 *
	 * @param marginBalance the new margin balance
	 */
	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	/**
	 * Gets the borrower balance.
	 *
	 * @return the borrower balance
	 */
	public BigDecimal getBorrowerBalance() {
		return borrowerBalance;
	}

	/**
	 * Sets the borrower balance.
	 *
	 * @param borrowerBalance the new borrower balance
	 */
	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	/**
	 * Gets the lender balance.
	 *
	 * @return the lender balance
	 */
	public BigDecimal getLenderBalance() {
		return lenderBalance;
	}

	/**
	 * Sets the lender balance.
	 *
	 * @param lenderBalance the new lender balance
	 */
	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	/**
	 * Gets the loanable balance.
	 *
	 * @return the loanable balance
	 */
	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	/**
	 * Sets the loanable balance.
	 *
	 * @param loanableBalance the new loanable balance
	 */
	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}
}
