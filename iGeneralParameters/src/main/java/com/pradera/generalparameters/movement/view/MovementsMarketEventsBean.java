package com.pradera.generalparameters.movement.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.view.SecuritiesPartHolAccount;
import com.pradera.generalparameters.movement.facade.MovementsFacade;
import com.pradera.generalparameters.movement.to.FiltersMarketFactTO;
import com.pradera.generalparameters.movement.to.MarketFactTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MovementsMarketEventsBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MovementsMarketEventsBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder account request. */
	private HolderAccount holderAccountRequest;
	
	/** The lst account to. */
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountTo;
	
	/** The selected account to. */
	private HolderAccountHelperResultTO selectedAccountTO;
	
	/** The lst account aux. */
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountAux;
    
    /** The filters. */
    private FiltersMarketFactTO filters;
    
    /** The user info. */
    @Inject
	private UserInfo userInfo;
    
    /** The market facts. */
    private GenericDataModel<MarketFactTO> marketFacts;
	
	/** The movements facade. */
	@EJB
	private MovementsFacade  movementsFacade;
	
	/** The securities part hol account. */
	@Inject
	private SecuritiesPartHolAccount securitiesPartHolAccount;
	
	/** The helper component facade. */
	@EJB
	private HelperComponentFacade helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The date types. */
	private List<ParameterTable> dateTypes;
    
    /** The holder request. */
    private Holder holderRequest;
    
    /** The list participant. */
    private List<Participant> listParticipant;
    
    /** The nro mov display. */
    private Long nroMovDisplay;//pk con el cual debemos visualizar
    
    /** The enable end date. */
    private boolean enableEndDate;
    
   
    /**
     * Init.
     */
    @PostConstruct
	public void init() {
    	defaultsLoadValues();
    	Participant participantTO = new Participant();		
		listParticipant=new ArrayList<Participant>(0);
		try {
			setParticipantList(movementsFacade.getLisParticipantServiceBean(participantTO));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
    
    /**
     * Defaults load values.
     */
    public void defaultsLoadValues(){
    	enableEndDate=false;
    	dateTypes=movementsFacade.getListDateTypes();
    	filters=new FiltersMarketFactTO();
    	holderAccountRequest=new HolderAccount();
    	holderRequest = new Holder();
    	if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
    		filters.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
    	}
    }
    
    /**
     * Participant change.
     */
    public void participantChange(){
		filters.setIdSecurityCode(null);
		this.holderAccountRequest = new HolderAccount();
		this.holderRequest = new Holder();
		this.marketFacts = new GenericDataModel<MarketFactTO>();
		lstAccountTo = null;
		securitiesPartHolAccount.setDescription(null);	
		JSFUtilities.resetComponent("frmCrudMovement:searchFilters");	
	}
    
    /**
     * Change select account.
     */
    public void changeSelectAccount(){
		if(selectedAccountTO!=null){
			holderAccountRequest = new HolderAccount();
			holderAccountRequest.setIdHolderAccountPk(selectedAccountTO.getAccountPk());
			holderAccountRequest.setAccountNumber(selectedAccountTO.getAccountNumber());
			joinPartHoldAccHandler();
		}
	}
    
    /**
     * Clean search page.
     */
    public void cleanSearchPage(){
    	defaultsLoadValues();
		filters.setIdSecurityCode(null);
		this.holderAccountRequest = new HolderAccount();
		this.holderRequest = new Holder();
		this.marketFacts = null;
		lstAccountTo = null;
		securitiesPartHolAccount.setDescription(null);
		JSFUtilities.resetComponent("frmCrudMovement:searchFilters");
    }
    
    /**
     * Clean participant cui.
     */
    public void cleanParticipantCui(){
		filters.setIdSecurityCode(null);
		this.marketFacts = null;
		securitiesPartHolAccount.setDescription(null);
		JSFUtilities.resetComponent("frmCrudMovement:searchFilters");
    }
	
	/**
	 * Join part hold acc handler.
	 */
	public void joinPartHoldAccHandler(){
		securitiesPartHolAccount.setDescription(null);
		SecuritiesSearcherTO securitiesSearcherTO = new SecuritiesSearcherTO();
		securitiesSearcherTO.setParticipantCode(filters.getIdParticipant());
		securitiesSearcherTO.setHolderAccount(this.holderAccountRequest);
		securitiesPartHolAccount.setSecuritiesSearcher(securitiesSearcherTO);
		
		this.marketFacts = null;
		JSFUtilities.resetComponent("frmCrudMovement:pnlSecurityAccHelp");
	}
    
    /**
     * Cui change.
     */
    public void cuiChange(){	
		try {
			changeHolderCuiHandler();
			Holder holder = this.getHolderRequest();			
			if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {
//				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL,
//				new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
//				this.setHolderRequest(new Holder());
//				JSFUtilities.showSimpleValidationDialog();
//				return;
			} else {
				HolderTO holderTO = new HolderTO();
				holderTO.setHolderId(holder.getIdHolderPk());
				holderTO.setFlagAllHolders(true);
				holderTO.setParticipantFk(filters.getIdParticipant());
				List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
				
				if (lista==null || lista.isEmpty()){
					lstAccountTo = new GenericDataModel<>();
				}else{
					lstAccountTo = new GenericDataModel<>(lista);
					if(lista.size() == ComponentConstant.ONE){
						selectedAccountTO = lista.get(0); 
					}
				}
				if(lstAccountTo.getSize()<0){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getGenericMessage("Participante incorrecto"));//PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
					holderRequest = new Holder();
					JSFUtilities.showSimpleValidationDialog();
				} else {
					fillDataParametersAccountTO();
				}
			}
			if (lstAccountTo!=null){
				JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
    
    /**
     * Fill data parameters account to.
     *
     * @throws ServiceException the service exception
     */
    public void fillDataParametersAccountTO() throws ServiceException{
		List<ParameterTable> lstHolderAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for (HolderAccountHelperResultTO objAccount : lstAccountTo) 
			for (ParameterTable parameterTable : lstHolderAccountState) 
				objAccount.setAccountStatusDescription(parameterTable.getParameterName());
	}
    
    /**
     * On change date type.
     */
    public void onChangeDateType(){
    	if(filters.getDateType()==1513)//1513 para rango de fecha
    		enableEndDate=true;   
    	else
    		enableEndDate=false;
    }
    
    /**
     * View detail movement fact.
     */
    public void viewDetailMovementFact(){
    	JSFUtilities.executeJavascriptFunction("PF('dialogMomvementFactMov').show()");
    }
    
    /**
     * Change holder cui handler.
     */
    public void changeHolderCuiHandler(){
    	filters.setIdSecurityCode(null);
    	filters.setHolderAccount(null);
		securitiesPartHolAccount.setDescription(null);	
		this.holderAccountRequest = new HolderAccount();
		this.marketFacts = null;
		securitiesPartHolAccount.setDescription(null);		
		lstAccountTo = null;
		JSFUtilities.resetComponent("frmCrudMovement:searchFilters");		
	}
    
    /**
     * Selected key value.
     */
    public void selectedKeyValue(){
		this.marketFacts = null;
		if(!isExistTotalBalance(filters))
			JSFUtilities.executeJavascriptFunction("PF('wVarReportedNoexistenceBalances').show()");
	}
    
    /**
     * Before search market events.
     */
    public void beforeSearchMarketEvents() {
    	if(!Validations.validateIsNotNullAndNotEmpty(filters.getHolderAccount()))
    		JSFUtilities.executeJavascriptFunction("PF('wVarDialogNoSelectAccount').show()");
		try {
			if(Validations.validateIsNotNullAndNotEmpty(filters))
				executeSearch();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
   
    /**
     * Checks if is exist total balance.
     *
     * @param filters the filters
     * @return true, if is exist total balance
     */
    public boolean  isExistTotalBalance(FiltersMarketFactTO filters){
    	filters.setHolder(holderRequest);
    	filters.setHolderAccount(holderAccountRequest);
    	return movementsFacade.existsRegistInHAB(filters);
    }
    /** This method is responsible for ejecute according Search filters.
     * @return No return any value.
     */
    public void executeSearch(){
    	filters.setHolder(holderRequest);
    	filters.setHolderAccount(holderAccountRequest);
    	List<MarketFactTO> list=movementsFacade.getMarketFactAndMovementFact(filters);
    	marketFacts=new GenericDataModel<MarketFactTO>(list);
    	JSFUtilities.updateComponent("frmCrudMovement:searchResult"); 
    }
    
    /**
     * Search participant on list from pk.
     *
     * @param idParticipantPk the id participant pk
     * @return the participant
     */
    public Participant searchParticipantOnListFromPK(Long idParticipantPk){
		for(Participant participant: listParticipant)
			if(participant.getIdParticipantPk().equals(idParticipantPk))
				return participant;
		return null;
	}
    
    /**
     * Sets the participant list.
     *
     * @param participantList the new participant list
     */
    public void setParticipantList(List<Participant> participantList) {
		this.listParticipant = participantList;
	}    
	
	/**
	 * Gets the list participant.
	 *
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}
	
	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}
	
	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public Holder getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(Holder holderRequest) {
		this.holderRequest = holderRequest;
	}
	
	/**
	 * Gets the lst account to.
	 *
	 * @return the lst account to
	 */
	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountTo() {
		return lstAccountTo;
	}
	
	/**
	 * Sets the lst account to.
	 *
	 * @param lstAccountTo the new lst account to
	 */
	public void setLstAccountTo(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}
	
	/**
	 * Gets the selected account to.
	 *
	 * @return the selected account to
	 */
	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}
	
	/**
	 * Sets the selected account to.
	 *
	 * @param selectedAccountTO the new selected account to
	 */
	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}
	
	/**
	 * Gets the lst account aux.
	 *
	 * @return the lst account aux
	 */
	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountAux() {
		return lstAccountAux;
	}
	
	/**
	 * Sets the lst account aux.
	 *
	 * @param lstAccountAux the new lst account aux
	 */
	public void setLstAccountAux(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountAux) {
		this.lstAccountAux = lstAccountAux;
	}
//	public GenericDataModel<FiltersMarketFactTO> getDataModel() {
//		return dataModel;
//	}
//	public void setDataModel(GenericDataModel<FiltersMarketFactTO> dataModel) {
//		this.dataModel = dataModel;
//	}
//	public HolderAccountBalanceTO getHolderAccountBalanceTO() {
//		return holderAccountBalanceTO;
//	}
//	public void setHolderAccountBalanceTO(
//			HolderAccountBalanceTO holderAccountBalanceTO) {
//		this.holderAccountBalanceTO = holderAccountBalanceTO;
/**
 * Gets the holder account request.
 *
 * @return the holder account request
 */
//	}
	public HolderAccount getHolderAccountRequest() {
		return holderAccountRequest;
	}
	
	/**
	 * Gets the date types.
	 *
	 * @return the date types
	 */
	public List<ParameterTable> getDateTypes() {
		return dateTypes;
	}
	
	/**
	 * Sets the date types.
	 *
	 * @param dateTypes the new date types
	 */
	public void setDateTypes(List<ParameterTable> dateTypes) {
		this.dateTypes = dateTypes;
	}
	
	/**
	 * Gets the filters.
	 *
	 * @return the filters
	 */
	public FiltersMarketFactTO getFilters() {
		return filters;
	}
	
	/**
	 * Sets the filters.
	 *
	 * @param filters the new filters
	 */
	public void setFilters(FiltersMarketFactTO filters) {
		this.filters = filters;
	}
	
	/**
	 * Gets the market facts.
	 *
	 * @return the market facts
	 */
	public GenericDataModel<MarketFactTO> getMarketFacts() {
		return marketFacts;
	}
	
	/**
	 * Gets the nro mov display.
	 *
	 * @return the nro mov display
	 */
	public Long getNroMovDisplay() {
		return nroMovDisplay;
	}
	
	/**
	 * Sets the nro mov display.
	 *
	 * @param nroMovDisplay the new nro mov display
	 */
	public void setNroMovDisplay(Long nroMovDisplay) {
		this.nroMovDisplay = nroMovDisplay;
	}
	
	/**
	 * Checks if is enable end date.
	 *
	 * @return true, if is enable end date
	 */
	public boolean isEnableEndDate() {
		return enableEndDate;
	}
	
	/**
	 * Sets the enable end date.
	 *
	 * @param enableEndDate the new enable end date
	 */
	public void setEnableEndDate(boolean enableEndDate) {
		this.enableEndDate = enableEndDate;
	}
}

