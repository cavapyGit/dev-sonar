package com.pradera.generalparameters.movement.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketFactTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
//hecho de mercado.
public class MarketFactTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The participant. */
	private Participant participant;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The security. */
	private Security security;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market date. */
	private Date marketDate;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The movement market facts. */
	private List<MarketMovementTO> movementMarketFacts;
	
	
	/**
	 * Instantiates a new market fact to.
	 *
	 * @param participant the participant
	 * @param holderAccount the holder account
	 * @param security the security
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param marketPrice the market price
	 */
	public MarketFactTO(Participant participant,
			HolderAccount holderAccount, Security security,
			BigDecimal marketRate, Date marketDate, BigDecimal marketPrice) {
		super();
		this.participant = participant;
		this.holderAccount = holderAccount;
		this.security = security;
		this.marketRate = marketRate;
		this.marketDate = marketDate;
		this.marketPrice = marketPrice;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}
	
	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
	
	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}
	
	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}
	
	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	
	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	
	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * Gets the movement market facts.
	 *
	 * @return the movement market facts
	 */
	public List<MarketMovementTO> getMovementMarketFacts() {
		return movementMarketFacts;
	}
	
	/**
	 * Sets the movement market facts.
	 *
	 * @param movementMarketFacts the new movement market facts
	 */
	public void setMovementMarketFacts(List<MarketMovementTO> movementMarketFacts) {
		this.movementMarketFacts = movementMarketFacts;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return "Participante pk:"+participant.getIdParticipantPk()+
				"\nHolder Account:"+holderAccount.getIdHolderAccountPk()+
				"\nValor:"+security.getIdSecurityCodePk()+
				"\n Market Date:"+marketDate.getDay()+"/"+marketDate.getMonth()+"/"+marketDate.getYear()+
				"\n Market Price:"+marketPrice+
				"\n Market Rate:"+marketRate;
	}
}
