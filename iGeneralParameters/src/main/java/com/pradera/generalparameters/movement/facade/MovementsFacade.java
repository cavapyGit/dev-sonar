package com.pradera.generalparameters.movement.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.stockcalculation.StockCalculationService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.generalparameters.movement.service.MovementsMarketEventsServiceBean;
import com.pradera.generalparameters.movement.to.FiltersMarketFactTO;
import com.pradera.generalparameters.movement.to.MarketFactTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MovementsFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class MovementsFacade {
	
	/** The movement service bean. */
	@EJB
	private MovementsMarketEventsServiceBean movementServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The stock calculate. */
	@Inject
	private Instance<StockCalculationService> stockCalculate;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The service. */
	private StockCalculationService service;
	
	/**
	 * Gets the lis participant service bean.
	 *
	 * @param filter the filter
	 * @return the lis participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLisParticipantServiceBean(Participant filter)
			throws ServiceException {
		return participantServiceBean.getLisParticipantServiceBean(filter);
	}
	
	/**
	 * Exists regist in hab.
	 *
	 * @param filters the filters
	 * @return true, if successful
	 */
	public boolean existsRegistInHAB(FiltersMarketFactTO filters){
		BigDecimal totalBalance=movementServiceBean.getTotalBalance(filters);
		if(totalBalance!=null)
			if(totalBalance.compareTo(BigDecimal.ZERO)>0)
				return true;
			else 
				return false;
		return false;
	}
		
	/**
	 * Gets the market fact and movement fact.
	 *
	 * @param filters the filters
	 * @return the market fact and movement fact
	 */
	public List<MarketFactTO> getMarketFactAndMovementFact(FiltersMarketFactTO filters){
		if(filters.getDateType()==1512){
			try {
				return movementServiceBean.getHolderMarketFacts(filters);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		if(filters.getDateType()==1513)
			return createStockCalculation(filters);
		return null;
	}
	
	/**
	 * Create stock calculation.
	 *
	 * @param filters the filters
	 * @return the list
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<MarketFactTO> createStockCalculation(FiltersMarketFactTO filters){ 
    	try {
    		StockCalculationProcess t=new StockCalculationProcess();
    		t.setStockType(StockType.MARKET_FACT.getCode());
    		t.setStockClass(StockClassType.NORMAL.getCode());
    		t.setStockState(StockStateType.REGISTERED.getCode());
    		t.setCreationDate(CommonsUtilities.currentDate());
    		t.setRegistryDate(CommonsUtilities.currentDate());
    		t.setRegistryUser(userInfo.getUserAccountSession().getUserName());
    		t.setCutoffDate(filters.getFinalDate());
    		t.setIndAutomactic(BooleanType.NO.getCode());
    		
    		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());   
    		StockCalculationProcess obj=movementServiceBean.create(t);
    		//System.out.println("PK:"+obj.getIdStockCalculationPk());
    		service=new StockCalculationService() {
				@Override
				public void executeStockMarketFact(Long stockCalculationId,
						LoggerUser loggerUser) throws ServiceException {
					
				}
				@Override
				public Long executeStockCalculationMonthly(Date cutoffDate,
						Date registryDate, LoggerUser loggerUser) throws ServiceException {
					return null;
				}
				@Override
				public Long executeStockCalculationMarketFactByCutDate(
						Date cutoffDate, Date registryDate,
						LoggerUser loggerUser) throws ServiceException {
					// TODO Auto-generated method stub
					return null;
				}
			};
			service.executeStockMarketFact(obj.getIdStockCalculationPk(), loggerUser);
			return movementServiceBean.getHolderMarketFactsFromCalculoStock(filters,obj.getIdStockCalculationPk());
		} catch (Exception e ) {
			//System.out.println("Posible falta de permisos al usuario.");
			e.printStackTrace();
		}
    	return null;
    }
	
	/**
	 * Gets the security.
	 *
	 * @param pkSecurityCode the pk security code
	 * @return the security
	 */
	public Security getSecurity(String pkSecurityCode){
		return movementServiceBean.getSecurity(pkSecurityCode);
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param idParticipant the id participant
	 * @return the participant
	 */
	public Participant getParticipant(Long idParticipant){
		return movementServiceBean.getParticipant(idParticipant);
	}
	
	/**
	 * Gets the list date types.
	 *
	 * @return the list date types
	 */
	public List<ParameterTable> getListDateTypes(){
		List<ParameterTable> list= new ArrayList<ParameterTable>();
		ParameterTable fechaCorte=new ParameterTable();
		fechaCorte.setParameterTablePk(1512);
		fechaCorte.setDescription("Fecha de Corte");
		list.add(fechaCorte);
		
		ParameterTable rangoFecha=new ParameterTable();
		rangoFecha.setParameterTablePk(1513);
		rangoFecha.setDescription("Rango de Fecha");
		list.add(rangoFecha);
		return list;
	}
}
