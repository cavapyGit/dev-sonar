package com.pradera.generalparameters.cluster.cache;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.model.process.type.ScheduleExecuteType;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SingletonControlResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Path("/singletons")
public class SingletonControlResource {
	
	/** The Constant CACHE_INFINISPAN_JNDI. */
	static final String CACHE_INFINISPAN_JNDI= "java:jboss/infinispan/container/batch-cache-container";
	
	/** The default cache manager. */
	@Resource(name = "infinispan/batch-cache-container")
	EmbeddedCacheManager defaultCacheManager;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The em. */
	@Inject @DepositaryDataBase
	protected EntityManager em;
	
	/** The cache. */
	@Resource(name = "infinispan/batch-cache")
	Cache<String,Set<Long>> cache;

    /**
     * Default constructor. 
     */
    public SingletonControlResource() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Init control.
     *
     * @throws NamingException the naming exception
     */
    public void initControl() throws NamingException{
    	defaultCacheManager.addListener(new ClusterListener(2));
    }
    
    /**
     * Register process.
     *
     * @param serverIp the server ip
     * @param processSchedule the process schedule
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/register/{serverip}/{process}")
    @Lock(LockType.WRITE)
    public void registerProcess(@PathParam("serverip") String serverIp,@PathParam("process") Long processSchedule){
    	serverIp=serverIp.replace("|", "/");
    	log.info("Register server "+serverIp+" process "+processSchedule);
    	Set<Long> process = cache.get(serverIp);
    	if(process != null){
    		process.add(processSchedule);
    	} else {
    		process = new HashSet<>();
    		process.add(processSchedule);
    	}
    	cache.put(serverIp, process);
    }
    
    /**
     * Removes the process.
     *
     * @param serverIp the server ip
     * @param processSchedule the process schedule
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/remove/{serverip}/{process}")
    @Lock(LockType.WRITE)
    public void removeProcess(@PathParam("serverip") String serverIp,@PathParam("process") Long processSchedule){
    	serverIp=serverIp.replace("|", "/");
    	log.info("Removiendo process: "+processSchedule+" del nodo "+serverIp);
    	Set<Long> process = cache.get(serverIp);
    	if(process!=null){
    		log.info("Removed process from server:"+serverIp+" and id process:"+processSchedule);
    		process.remove(processSchedule);
    	}
    	cache.put(serverIp, process);
    }
    
    
    /**
     * Crash node.
     *
     * @param serverIp the server ip
     */
    public void crashNode(String serverIp){
    	Set<Long> process = cache.get(serverIp);
    	if(process!=null && !process.isEmpty()){
    		Query query =em.createQuery("update ProcessSchedule p set p.scheduleExecution=:execute,p.version=p.version+1,p.lastModifyDate=:now "
    				+ " where p.idProcessSchedulePk in (:processList) ");
    		query.setParameter("execute", ScheduleExecuteType.RUNNING_ERROR.getCode());
    		query.setParameter("now", CommonsUtilities.currentDateTime());
    		List<Long> sheduler = new ArrayList<>(process);
    		query.setParameter("processList", sheduler);
    		query.executeUpdate();
    	}
    	cache.remove(serverIp);
    }

}
