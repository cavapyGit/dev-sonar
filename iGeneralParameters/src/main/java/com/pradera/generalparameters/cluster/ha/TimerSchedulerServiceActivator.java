package com.pradera.generalparameters.cluster.ha;

//import org.jboss.as.clustering.singleton.SingletonService;
//import org.jboss.as.clustering.singleton.election.NamePreference;
//import org.jboss.as.clustering.singleton.election.PreferredSingletonElectionPolicy;
//import org.jboss.as.clustering.singleton.election.SimpleSingletonElectionPolicy;
import org.jboss.logging.Logger;
import org.jboss.msc.service.DelegatingServiceContainer;
import org.jboss.msc.service.ServiceActivator;
import org.jboss.msc.service.ServiceActivatorContext;
import org.jboss.msc.service.ServiceController;
import org.jboss.msc.service.ServiceRegistryException;

import com.pradera.generalparameters.schedulerprocess.service.SchedulerServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class TimerSchedulerServiceActivator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class TimerSchedulerServiceActivator implements ServiceActivator {
	
	/** The log. */
	private final Logger log = Logger.getLogger(this.getClass());

	/* (non-Javadoc)
	 * @see org.jboss.msc.service.ServiceActivator#activate(org.jboss.msc.service.ServiceActivatorContext)
	 */
	@Override
	public void activate(ServiceActivatorContext context)
			throws ServiceRegistryException {
		log.info("TimerSchedulerService will be installed!");
//		TimerSchedulerService service = new TimerSchedulerService();
//        SingletonService<SchedulerServiceBean> singleton = new SingletonService<SchedulerServiceBean>(service, TimerSchedulerService.SINGLETON_SERVICE_NAME);
//        
        
        /*
         * The NamePreference is a combination of the node name (-Djboss.node.name) and the name of
         * the configured cache "singleton". If there is more than 1 node, it is possible to add more than
         * one name and the election will use the first available node in that list.
         *   -  To pass a chain of election policies to the singleton and tell JGroups to run the
         * singleton on a node with a particular name, uncomment the first line  and
         * comment the second line below.
         *   - To pass a list of more than one node, comment the first line and uncomment the
         * second line below.
         */
//        singleton.setElectionPolicy(new PreferredSingletonElectionPolicy(new SimpleSingletonElectionPolicy(), new NamePreference("raul-lenovo/singleton")));
//       singleton.setElectionPolicy(new PreferredSingletonElectionPolicy(new SimpleSingletonElectionPolicy(), new NamePreference("slave:edv-server-two/singleton"), new NamePreference("master:edv-server-one/singleton")));

//        singleton.build(new DelegatingServiceContainer(context.getServiceTarget(), context.getServiceRegistry()))
//                .setInitialMode(ServiceController.Mode.ACTIVE)
//                .install()
//        ;
		
	}

}
