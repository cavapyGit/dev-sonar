package com.pradera.generalparameters.cluster.service;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.logging.Logger;

import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.generalparameters.schedulerprocess.service.SchedulerServiceBean;
import com.pradera.integration.common.validation.Validations;

@Startup
@Singleton
public class SchedulerControlService {

    @Inject
	StageApplication stageApplication;
    
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(SchedulerControlService.class);
        
    /** The Constant JNDISERVICE. */
    public static final String JNDISERVICE="global/iGeneralParameters/SchedulerServiceBean!com.pradera.generalparameters.schedulerprocess.service.SchedulerServiceBean";
    
    /**
     * A flag whether the service is started.
     */
    private final AtomicBoolean started = new AtomicBoolean(false);
    
    /** The scheduler service. */
    private SchedulerServiceBean schedulerService;
    
	/* (non-Javadoc)
	 * @see org.jboss.msc.value.Value#getValue()
	 */	
	
	@PostConstruct
	public void init() {
		
	        try {
	    		if (!started.compareAndSet(false, true)) {
	                throw new Exception("The service is still started!");
	            }
	    		String nodeName = System.getProperty("jboss.node.name");
	            LOGGER.info("Start TimerSchedulerServiceBean timer service in node '" +nodeName+":"+ this.getClass().getName() + "'");
	            
	            InitialContext ic = new InitialContext();
	            schedulerService = ((SchedulerServiceBean) ic.lookup(JNDISERVICE));
	            TimeUnit.SECONDS.sleep(1l);
	            schedulerService.reloadTimerSaved();//  initialize("HASingleton timer @" + node + " " + new Date());
	        } catch (NamingException e) {
	        	e.printStackTrace();
	        } catch (InterruptedException ie) {
	        	ie.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
}
