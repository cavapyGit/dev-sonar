package com.pradera.generalparameters.cluster.ha;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.logging.Logger;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;

import com.pradera.generalparameters.schedulerprocess.service.SchedulerServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class TimerSchedulerService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class TimerSchedulerService  implements Service<SchedulerServiceBean> {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(TimerSchedulerService.class);
    
    /** The Constant SINGLETON_SERVICE_NAME. */
    public static final ServiceName SINGLETON_SERVICE_NAME = ServiceName.JBOSS.append("ha", "singleton", "schedulerservicebean");
    
    /** The Constant JNDISERVICE. */
    public static final String JNDISERVICE="global/iGeneralParameters/SchedulerServiceBean!com.pradera.generalparameters.schedulerprocess.service.SchedulerServiceBean";

    /**
     * A flag whether the service is started.
     */
    private final AtomicBoolean started = new AtomicBoolean(false);
    
    /** The scheduler service. */
    private SchedulerServiceBean schedulerService;
    
	/* (non-Javadoc)
	 * @see org.jboss.msc.value.Value#getValue()
	 */
	@Override
	public SchedulerServiceBean getValue() throws IllegalStateException,
			IllegalArgumentException {
        if (!started.get()) {
            throw new IllegalStateException("The service '" + this.getClass().getName() + "' is not ready!");
        }
        return this.schedulerService;
	}

	/* (non-Javadoc)
	 * @see org.jboss.msc.service.Service#start(org.jboss.msc.service.StartContext)
	 */
	@Override
	public void start(StartContext arg0) throws StartException {
		if (!started.compareAndSet(false, true)) {
            throw new StartException("The service is still started!");
        }
		String nodeName = System.getProperty("jboss.node.name");
        LOGGER.info("Start TimerSchedulerServiceBean timer service in node '" +nodeName+":"+ this.getClass().getName() + "'");
        try {
            InitialContext ic = new InitialContext();
            schedulerService = ((SchedulerServiceBean) ic.lookup(JNDISERVICE));
            TimeUnit.SECONDS.sleep(1l);
            schedulerService.reloadTimerSaved();//  initialize("HASingleton timer @" + node + " " + new Date());
        } catch (NamingException e) {
            throw new StartException("Could not initialize timer", e);
        } catch (InterruptedException ie) {
        	 throw new StartException("Could not initialize timer", ie);
		}
	}

	/* (non-Javadoc)
	 * @see org.jboss.msc.service.Service#stop(org.jboss.msc.service.StopContext)
	 */
	@Override
	public void stop(StopContext arg0) {
		if (!started.compareAndSet(true, false)) {
            LOGGER.warn("The service '" + this.getClass().getName() + "' is not active!");
        } else {
            LOGGER.info("Stop TimerSchedulerServiceBean timer service in node '"+System.getProperty("jboss.node.name")+":" + this.getClass().getName() + "'");
            try {
                InitialContext ic = new InitialContext();
                ((SchedulerServiceBean) ic.lookup(JNDISERVICE)).stopTimers();
            } catch (NamingException e) {
                LOGGER.error("Could not stop timer", e);
            }
        }

	}

}
