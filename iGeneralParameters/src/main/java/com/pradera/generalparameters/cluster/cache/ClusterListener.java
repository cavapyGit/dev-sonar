package com.pradera.generalparameters.cluster.cache;

import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryCreated;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryModified;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryRemoved;
import org.infinispan.notifications.cachelistener.event.CacheEntryCreatedEvent;
import org.infinispan.notifications.cachelistener.event.CacheEntryModifiedEvent;
import org.infinispan.notifications.cachelistener.event.CacheEntryRemovedEvent;
import org.infinispan.notifications.cachemanagerlistener.annotation.CacheStarted;
import org.infinispan.notifications.cachemanagerlistener.annotation.CacheStopped;
import org.infinispan.notifications.cachemanagerlistener.annotation.ViewChanged;
import org.infinispan.notifications.cachemanagerlistener.event.CacheStartedEvent;
import org.infinispan.notifications.cachemanagerlistener.event.CacheStoppedEvent;
import org.infinispan.notifications.cachemanagerlistener.event.ViewChangedEvent;
import org.infinispan.remoting.transport.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The listener interface for receiving cluster events.
 * The class that is interested in processing a cluster
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addClusterListener<code> method. When
 * the cluster event occurs, that object's appropriate
 * method is invoked.
 *
 * @see ClusterEvent
 */
@Listener(sync=false)
public class ClusterListener {

	
	Logger log = LoggerFactory.getLogger(ClusterListener.class);
	
	private final int expectedNodes;
	
	public static final String JNDISERVICE="global/iGeneralParameters/SingletonControlResource!com.pradera.generalparameters.cluster.cache.SingletonControlResource";

	
	SingletonControlResource singletonControl;
	
	public ClusterListener(int expectedNodes) {
		this.expectedNodes = expectedNodes;
		try {
		InitialContext ic = new InitialContext();
		singletonControl = ((SingletonControlResource) ic.lookup(JNDISERVICE));
		} catch (NamingException e) {
			log.error(e.getMessage());
        }
	}
	
	/**
	 * Change cluster.
	 *
	 * @param event the event
	 */
	@ViewChanged
	public void changeCluster(ViewChangedEvent event){
		//Only check after server instance cache in node
		if(event.getOldMembers()!=null){
			//Check if node was shutdown
			if(event.getNewMembers().size()<event.getOldMembers().size()){
				for(Address address :event.getOldMembers()){
					//find crash node
					if(!address.toString().equals(event.getLocalAddress().toString())){
						//call register error in process running in crash node
						try{
						singletonControl.crashNode(address.toString());
						} catch(Exception ex){
							log.info("No se pudo llamar al ejb SingletonControlResource server apagandose");
						}
					}
				}
			}
			
		}
	}

	@CacheStopped
	public void cacheStop(CacheStoppedEvent event){
		log.info("Parado cache "+event.getCacheName());
	}
	
	@CacheStarted
	public void cacheStar(CacheStartedEvent event){
		log.info("iniciado cache "+event.getCacheName());
	}
    
	
	
	/**
	 * Created.
	 *
	 * @param event the event
	 */
	@CacheEntryCreated
	public void created(CacheEntryCreatedEvent<String,List<Long>> event){
		log.info("Registrado nodo " + event.getKey());
	}
	
	/**
	 * Modified.
	 *
	 * @param event the event
	 */
	@CacheEntryModified
	public void modified(CacheEntryModifiedEvent<String,Set<Long>> event ){
		String serverKey=event.getKey()!=null?event.getKey():"nuevo server";
		String valor = event.getValue()!=null?event.getValue().toString():"valor removido";
		if(event.isPre()){
			log.info("Antes de modificacion nodo "+serverKey+" valor "+valor);
		} else {
			log.info("Despues de modificacion nodo "+serverKey+" valor "+valor);
		}
	}
	
	/**
	 * Removed.
	 *
	 * @param event the event
	 */
	@CacheEntryRemoved
	public void removed(CacheEntryRemovedEvent<String,Set<Long>> event){
		if(event.isPre()){
			log.info("Removiendo nodo "+event.getKey()+" valor "+event.getValue().toString());
		}
	}

	public int getExpectedNodes() {
		return expectedNodes;
	}
}
