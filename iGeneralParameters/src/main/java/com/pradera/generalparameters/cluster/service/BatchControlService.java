package com.pradera.generalparameters.cluster.service;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.naming.NamingException;

import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.generalparameters.cluster.cache.SingletonControlResource;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BatchControlService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09-jul-2015
 */
@Startup
@Singleton
public class BatchControlService {
	
	/** The singleton control. */
	@EJB
	SingletonControlResource singletonControl;
	
	/** The stage application. */
	@Inject
	StageApplication stageApplication;
	
	
	/**
	 * Inits the.
	 *
	 * @throws NamingException the naming exception
	 */
	@PostConstruct
	public void init() throws NamingException{
		if(stageApplication !=null && stageApplication.equals(StageApplication.Production)){
			//instance cache infinispan
			singletonControl.initControl();
		}
	}

    /**
     * Default constructor. 
     */
    public BatchControlService() {
        // TODO Auto-generated constructor stub
    }

}
