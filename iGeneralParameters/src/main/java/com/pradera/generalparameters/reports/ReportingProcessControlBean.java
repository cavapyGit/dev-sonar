/**
 * 
 */
package com.pradera.generalparameters.reports;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.Instance;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.framework.usersaccount.service.UserAccountControlServiceBean;
import com.pradera.generalparameters.processes.view.UserAccountType;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.type.ProcessLoggerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportMonitoringMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ReportingProcessControlBean extends GenericBaseBean {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The list users types. */
	private List<UserAccountType> listUsersTypes;
	
    /** The list institutions. */
	private List<SelectItem> listInstitutions;
	
	/** The list institutions. */
	private List<InstitutionType> listInstitutionsType;
	
	
	/** The list institutions. */
	private List<InstitutionType> listInstitutionsTypeTemp;
	
	
	/** The list state. */
	private List<ProcessLoggerStateType> listState;
	
	/** The user type. */
	private Integer userType;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The institution. */
	private Integer institution;
	
	/** The institution type. */
	private Integer state;
	
	/** The user code. */
	private String userCode;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The report mgmt service facade. */
	@EJB
	UserAccountControlServiceBean userAccountService;
	
	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;
	
	/** The report service. */
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/**
	 * Init.
	 */
	@PostConstruct
	public void init(){
		try {
	    	if (!FacesContext.getCurrentInstance().isPostback()) {
	    		
	    		 listInstitutionsTypeTemp = InstitutionType.list;
				 setInstitutionType(Integer.valueOf(-1));
				 setInstitution(Integer.valueOf(-1));
				 setUserType(Integer.valueOf(-1));
				 setInitialDate(CommonsUtilities.currentDate());
				 setFinalDate(CommonsUtilities.currentDate());
				 listState = ProcessLoggerStateType.list;
				 listUsersTypes = UserAccountType.list; 
			}
		} catch (Exception e) {
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	/**
	 * Change user type for filter listener.
	 */
    public void changeUserTypeListener(){
		
	try {
			if(getUserType().intValue()>0){
				if(getUserType().intValue()==UserAccountType.INTERNAL.getCode().intValue()){
					
					listInstitutionsType=new ArrayList<>();
					listInstitutionsType.addAll(listInstitutionsTypeTemp);
					for(InstitutionType item: listInstitutionsTypeTemp){
						if(!InstitutionType.DEPOSITARY.getCode().equals(item.getCode())){
							listInstitutionsType.remove(item);
						}
					}
					
					setInstitutionType(Integer.parseInt(InstitutionType.DEPOSITARY.getCode().toString()));
					
					changeInstitutionTypeListener();
					HtmlSelectOneMenu combo=(HtmlSelectOneMenu)JSFUtilities.findViewComponent("reportGeneratedForm:cboInstitution");
					UISelectItems items=(UISelectItems)combo.getChildren().get(1);
					List<SelectItem> lstInstituions=(List<SelectItem>)items.getValue();
					setInstitution(Integer.valueOf(lstInstituions.get(0).getValue().toString()));
				}else{
					listInstitutionsType=new ArrayList<>();
					listInstitutionsType.addAll(listInstitutionsTypeTemp);
					for(InstitutionType item:listInstitutionsTypeTemp){
						if(InstitutionType.DEPOSITARY.getCode().equals(item.getCode())){
							listInstitutionsType.remove(item);
						}
					}
					
					listInstitutions=new ArrayList<>();
					setInstitutionType(Integer.valueOf(-1));
					setInstitution(Integer.valueOf(-1));
				}
			} else{
				listInstitutionsType=new ArrayList<>();
				listInstitutions=new ArrayList<>();
				setInstitutionType(Integer.valueOf(-1));
				setInstitution(Integer.valueOf(-1));
			}
	    } catch (Exception e) {
		    //excepcion.fire( new ExceptionToCatch(e) );
		}
	}
    
    /**
	 * Change institution type for filter listener.
	 */
	public void changeInstitutionTypeListener(){
		try {
			List<Object[]> objectLst = new ArrayList<Object[]>();
			
			if(getInstitutionType().intValue()>0){
				listInstitutions =new ArrayList<SelectItem>() ;
				objectLst=clientRestService.getInstitutionInformation(getInstitutionType());
				for(Object[] item:objectLst){
					SelectItem selectItem = new SelectItem();
					selectItem.setValue(((Double)item[0]).intValue());
					selectItem.setLabel(item[1].toString());
					listInstitutions.add(selectItem);
				}
				
			}else{
				listInstitutions=new ArrayList<SelectItem>();
				setInstitution(Integer.valueOf(-1));
			}
		} catch (Exception e) {
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	
	/**
	 * To send CorporativeProcessReport.
	 */
	@LoggerAuditWeb
	public void sendCorporativeProcessReports(){
		try{
			LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
			
			Map<String,String> reportParams = getReportParameters();
			Long reportId = ReportIdType.REPOERTING_AND_PROCESS_CONTROL.getCode();
			ReportUser reportUser = new ReportUser();
			reportUser.setUserName(loggerUser.getUserName());
			if(reportId != null){
				reportService.get().saveReportExecution(reportId, reportParams, null, reportUser, loggerUser);
			}
			JSFUtilities.showComponent(":formPopUp:idDialReportConfirm");
			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.REPORT_SEND_SUCCESS_MSG, null);
			return;
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * This method is to parameter values as a map.
	 *
	 * @return map
	 */
	public Map<String,String> getReportParameters(){
		Map<String,String> parameters = new HashMap<String, String>();
		
		parameters.put("user_type", String.valueOf(getUserType()));
		parameters.put("initial_date", CommonsUtilities.convertDatetoString(getInitialDate()));
		parameters.put("final_date", CommonsUtilities.convertDatetoString(getFinalDate()));
		if(Validations.validateIsNotNullAndPositive(getInstitutionType())){
			parameters.put("institution_type", String.valueOf(getInstitutionType()));
		}else{
			parameters.put("institution_type", "");
		}
		if(Validations.validateIsNotNullAndPositive(getInstitution())){
			parameters.put("institution", String.valueOf(getInstitution()));
		}else{
			parameters.put("institution", "");
		}
		if(getUserCode() != null){
			parameters.put("user_code", getUserCode());
		}else{
			parameters.put("user_code", "");
		}
		if(Validations.validateIsNotNullAndPositive(getState())){
			parameters.put("state", String.valueOf(getState()));
		}else{
			parameters.put("state", "");
		}
		
		return parameters;
	}
	
	/**
	 * Clean search users listener.
	 */
	public void clean(){
		try {
			 setInstitutionType(Integer.valueOf(-1));
			 setInstitution(Integer.valueOf(-1));
			 setUserType(Integer.valueOf(-1));
			 setInitialDate(CommonsUtilities.currentDate());
			 setFinalDate(CommonsUtilities.currentDate());
			 setUserCode(null);
			 listInstitutionsType.clear();
			 listInstitutions.clear();
			 
		} catch (Exception e) {
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	/**
	 * Gets the list users types.
	 *
	 * @return the listUsersTypes
	 */
	public List<UserAccountType> getListUsersTypes() {
		return listUsersTypes;
	}

	/**
	 * Sets the list users types.
	 *
	 * @param listUsersTypes the listUsersTypes to set
	 */
	public void setListUsersTypes(List<UserAccountType> listUsersTypes) {
		this.listUsersTypes = listUsersTypes;
	}

	/**
	 * Gets the list institutions.
	 *
	 * @return the listInstitutions
	 */
	public List<SelectItem> getListInstitutions() {
		return listInstitutions;
	}

	/**
	 * Sets the list institutions.
	 *
	 * @param listInstitutions the listInstitutions to set
	 */
	public void setListInstitutions(List<SelectItem> listInstitutions) {
		this.listInstitutions = listInstitutions;
	}

	/**
	 * Gets the list state.
	 *
	 * @return the listState
	 */
	public List<ProcessLoggerStateType> getListState() {
		return listState;
	}

	/**
	 * Sets the list state.
	 *
	 * @param listState the listState to set
	 */
	public void setListState(List<ProcessLoggerStateType> listState) {
		this.listState = listState;
	}

	/**
	 * Gets the user type.
	 *
	 * @return the userType
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * Sets the user type.
	 *
	 * @param userType the userType to set
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institutionType
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the institutionType to set
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public Integer getInstitution() {
		return institution;
	}

	/**
	 * Sets the institution.
	 *
	 * @param institution the institution to set
	 */
	public void setInstitution(Integer institution) {
		this.institution = institution;
	}

	/**
	 * Gets the user code.
	 *
	 * @return the userCode
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * Sets the user code.
	 *
	 * @param userCode the userCode to set
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the list institutions type.
	 *
	 * @return the listInstitutionsType
	 */
	public List<InstitutionType> getListInstitutionsType() {
		return listInstitutionsType;
	}

	/**
	 * Sets the list institutions type.
	 *
	 * @param listInstitutionsType the listInstitutionsType to set
	 */
	public void setListInstitutionsType(List<InstitutionType> listInstitutionsType) {
		this.listInstitutionsType = listInstitutionsType;
	}
	
	/**
	 * Gets the list institutions type temp.
	 *
	 * @return the listInstitutionsTypeTemp
	 */
	public List<InstitutionType> getListInstitutionsTypeTemp() {
		return listInstitutionsTypeTemp;
	}

	/**
	 * Sets the list institutions type temp.
	 *
	 * @param listInstitutionsTypeTemp the listInstitutionsTypeTemp to set
	 */
	public void setListInstitutionsTypeTemp(
			List<InstitutionType> listInstitutionsTypeTemp) {
		this.listInstitutionsTypeTemp = listInstitutionsTypeTemp;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}



}
