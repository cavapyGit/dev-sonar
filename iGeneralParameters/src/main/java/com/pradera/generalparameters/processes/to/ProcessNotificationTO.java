package com.pradera.generalparameters.processes.to;

import java.io.Serializable;

import com.pradera.model.notification.DestinationNotification;
import com.pradera.model.notification.ProcessNotification;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class ProcessNotificationTO.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
public class ProcessNotificationTO implements Serializable{
	
	/** Added Default Serial UID. */
	private static final long serialVersionUID = 1L;
	
	/** Holds ProcessNotification data. */
	private ProcessNotification processNotification;
	
	/** Holds DestinationNotification data. */
	private DestinationNotification destinationNotification;
	
	/**
	 * Get the ProcessNotification.
	 *
	 * @return the processNotification
	 */
	public ProcessNotification getProcessNotification() {
		return processNotification;
	}
	
	/**
	 * Set the ProcessNotification.
	 *
	 * @param processNotification the processNotification to set
	 */
	public void setProcessNotification(ProcessNotification processNotification) {
		this.processNotification = processNotification;
	}
	
	/**
	 * Get the DestinationNotification.
	 *
	 * @return the destinationNotification
	 */
	public DestinationNotification getDestinationNotification() {
		return destinationNotification;
	}
	
	/**
	 * Set the DestinationNotification.
	 *
	 * @param destinationNotification the destinationNotification to set
	 */
	public void setDestinationNotification(DestinationNotification destinationNotification) {
		this.destinationNotification = destinationNotification;
	}
	
	
}
