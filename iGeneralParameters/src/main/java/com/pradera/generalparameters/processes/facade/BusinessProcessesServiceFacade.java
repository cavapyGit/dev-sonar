package com.pradera.generalparameters.processes.facade;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.security.model.type.ExternalInterfaceType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.usersaccount.service.UserAccountControlServiceBean;
import com.pradera.generalparameters.processes.service.BusinessProcessesServiceBean;
import com.pradera.generalparameters.processes.service.MonitoringProcessesServiceBean;
import com.pradera.generalparameters.processes.to.BusinessProcessTO;
import com.pradera.generalparameters.processes.to.ProcessNotificationTO;
import com.pradera.generalparameters.processes.view.User;
import com.pradera.generalparameters.schedulerprocess.service.SchedulerAcessServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.notification.DestinationNotification;
import com.pradera.model.notification.NotificationConfiguration;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessSchedule;
import com.pradera.model.process.type.ProcessScheduleStateType;
import com.pradera.model.process.type.ProcessType;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class BusinessProcessesServiceFacade.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class BusinessProcessesServiceFacade {
	
	/** The business processes service bean. */
	@EJB
	BusinessProcessesServiceBean businessProcessesServiceBean;
	
	/** The user account service. */
	@EJB
	UserAccountControlServiceBean userAccountService;
	
	/** The monitoring processes service bean. */
	@EJB
	MonitoringProcessesServiceBean monitoringProcessesServiceBean;
	
	/** The client rest service. */
	@Inject 
    ClientRestService clientRestService;
	
	/** The Transaction synchronization Registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The schedulerservice. */
	@EJB
	SchedulerAcessServiceBean schedulerservice;
	
	/** The Constant BUSSINES_PROCESS_PK. */
	private static final int BUSSINES_PROCESS_PK=0;
	
	/** The Constant PROCESS_LEVEL1_NAME. */
	private static final int PROCESS_LEVEL1_NAME=1;
	
	/** The Constant PROCESS_LEVEL2_NAME. */
	private static final int PROCESS_LEVEL2_NAME=2;
	
	/** The Constant PROCESS_MACRO_NAME. */
	private static final int PROCESS_MACRO_NAME=3;
	
	/** The Constant PROCESS_TYPE. */
	private static final int PROCESS_TYPE=4;
	
	/** The Constant PROCESS_BEAN_NAME. */
	private static final int PROCESS_BEAN_NAME=5;
	
	/** The Constant PROCESS_DESCRIPTION. */
	private static final int PROCESS_DESCRIPTION=6;
	
	/** The Constant PROCESS_NAME. */
	private static final int PROCESS_NAME=7;
	
	/** The Constant PROCESS_MNEMONIC. */
	private static final int PROCESS_MNEMONIC=8;
	
	/** The Constant PROCESS_IND_NOTIFICATION. */
	private static final int PROCESS_IND_NOTIFICATION=9;

	/**
	 * Method for retrieving the BusinessProcess details from the service.
	 *
	 * @param microProcess the micro process
	 * @param level1 the level1
	 * @param level2 the level2
	 * @param process the process
	 * @param type Integer
	 * @param negocio String
	 * @param nemonico String
	 * @param indNotification the ind notification
	 * @return  the list of BusinessProcess Entities
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BusinessProcessTO> listBusinessProcessFacade(Long microProcess, Long level1, Long level2, Long process,
			Integer type, String negocio, String nemonico,Integer indNotification)
			throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<Object[]> listProcess = businessProcessesServiceBean.listBusinessProcessService(microProcess, level1, level2, process, type, negocio, nemonico,indNotification);
		BusinessProcessTO bussProc = null;
		List<BusinessProcessTO> businessProcessList = new ArrayList<BusinessProcessTO>();
		for (Object[] arrObj : listProcess) {
			bussProc = new BusinessProcessTO();
			bussProc.setIdBusinessProcessPk(Long.valueOf(arrObj[BUSSINES_PROCESS_PK].toString()));
			if (arrObj[PROCESS_LEVEL1_NAME] != null) {
				bussProc.setProcessLevel1Name(arrObj[PROCESS_LEVEL1_NAME].toString());
			}
			if (arrObj[PROCESS_LEVEL2_NAME] != null) {
				bussProc.setProcessLevel2Name(arrObj[PROCESS_LEVEL2_NAME].toString());
			}
			if (arrObj[PROCESS_MACRO_NAME] != null) {
				bussProc.setMacroProcessName(arrObj[PROCESS_MACRO_NAME].toString());
			}
			if (arrObj[PROCESS_TYPE] != null) {
				bussProc.setProcessType(Integer.valueOf(arrObj[PROCESS_TYPE].toString()));
				ProcessType description = ProcessType.get(bussProc.getProcessType());
				bussProc.setProcessTypeDescription(description == null? GeneralConstants.EMPTY_STRING:description.getValue());
			}
			if (arrObj[PROCESS_BEAN_NAME] != null) {
				bussProc.setBeanName(arrObj[PROCESS_BEAN_NAME].toString());
			}
			if (arrObj[PROCESS_DESCRIPTION] != null) {
				bussProc.setDescription(arrObj[PROCESS_DESCRIPTION].toString());
			}
			if (arrObj[PROCESS_NAME] != null) {
				bussProc.setProcessName(arrObj[PROCESS_NAME].toString());
			}
			if (arrObj[PROCESS_MNEMONIC] != null) {
				bussProc.setMnemonic(arrObj[PROCESS_MNEMONIC].toString());
			}
			if (arrObj[PROCESS_IND_NOTIFICATION] != null) {
				bussProc.setIndNotification(Integer.valueOf(arrObj[PROCESS_IND_NOTIFICATION].toString()));
			}
			businessProcessList.add(bussProc);
		}
		 return businessProcessList;
	}
	
	
	/**
	 * Gets the notification business process.
	 *
	 * @param businessProcess the business process
	 * @return the notification business process
	 * @throws ServiceException the service exception
	 */
	public List<ProcessNotification> getNotificationBusinessProcess(Long businessProcess) throws ServiceException{
		List<ProcessNotification> lstResult = businessProcessesServiceBean.getNotificationBusinessProcess(businessProcess);
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			for(ProcessNotification notification:lstResult){
				notification.setBlExist(true);
			}
		}
		return lstResult;
	}
	
	/**
	 * Gets the users from institution.
	 *
	 * @param institution the institution
	 * @param institutionCode the institution code
	 * @return the users from institution
	 */
	public List<UserAccountSession> getUsersFromInstitution(InstitutionType institution,Object institutionCode){
    	return userAccountService.getUsersInformation(null, institution, institutionCode, null,null,ExternalInterfaceType.NO.getCode());
    	
    }
	

	/**
	 * Method for get the notifications.
	 *
	 * @param id Long
	 * @return List of Object arrays
	 * @throws ServiceException the service exception
	 */
	public List<ProcessNotificationTO> getNoticationProcess(Long id) throws ServiceException {
		List<Object[]>  lstNotifiaction = businessProcessesServiceBean.getNotificationsService(id);	
		 
		    List<ProcessNotificationTO> lstNotifiactionTo = new ArrayList<ProcessNotificationTO>();
		    ProcessNotificationTO tempProcessNotificationTo =null;
		    for (Object[] notification : lstNotifiaction) {		    	
					tempProcessNotificationTo = new ProcessNotificationTO();
					tempProcessNotificationTo.setDestinationNotification((DestinationNotification)notification[1]);
					tempProcessNotificationTo.setProcessNotification((ProcessNotification)notification[0]);
					lstNotifiactionTo.add(tempProcessNotificationTo);
				}
		    return lstNotifiactionTo;
			}
	
	/**
	 * Method for getting the user details.
	 *
	 * @param user User
	 * @return the list of object arrays of user type
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> showUsersFacade(User user) throws ServiceException{
		return businessProcessesServiceBean.showUsersService(user);
	}
	
	/**
	 * Method for saving the Notifications.
	 *
	 * @param notification the notification
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void registerNotification(ProcessNotification notification) throws ServiceException{
	    LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        businessProcessesServiceBean.create(notification);
	}
	
	/**
	 * Update notification.
	 *
	 * @param notification the notification
	 * @return the process notification
	 * @throws ServiceException the service exception
	 */
	public ProcessNotification updateNotification(ProcessNotification notification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
        return businessProcessesServiceBean.update(notification);
	}
	
	
	/**
	 * Method for getting the child Notifications.
	 *
	 * @param id  Long
	 * @return the list of object arrays of child notifications type
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getChildNotificationsFacade(Long id) throws ServiceException{
		return businessProcessesServiceBean.getChildNotificationsService(id);
	}
	
	
	/**
	 * Gets the executable processes.
	 *
	 * @param monitorSystemProcessFilter the monitor system process filter
	 * @return the executable processes
	 * @throws ServiceException the service exception
	 */
	public List<BusinessProcessTO> getExecutableProcesses(
			MonitorSystemProcessFilter monitorSystemProcessFilter) throws ServiceException {
		List<Object[]> resultList = businessProcessesServiceBean.listBusinessProcessServiceBean(monitorSystemProcessFilter);
		List<BusinessProcessTO> processes = new ArrayList<BusinessProcessTO>();
		for (Object[] objects : resultList) {
			BusinessProcessTO b = new BusinessProcessTO();
			b.setIdBusinessProcessPk((Long)objects[0]);
			b.setProcessName((String)objects[1]);
			if(Validations.validateIsNotNull(objects[2]))
				b.setMacroProcess((Long)objects[2]);
			if(Validations.validateIsNotNull(objects[3]))
				b.setMacroProcessName((String)objects[3]);
			
			if(Validations.validateIsNotNull(objects[4]))
				b.setProcessLevel1((Long)objects[4]);
			if(Validations.validateIsNotNull(objects[5]))
				b.setProcessLevel1Name((String)objects[5]);
			
			if(Validations.validateIsNotNull(objects[6]))
				b.setProcessLevel2((Long)objects[6]);
			if(Validations.validateIsNotNull(objects[7]))
				b.setProcessLevel2Name((String)objects[7]);
			
			b.setProcessType((Integer)objects[8]);

			if(Validations.validateIsNotNull(objects[9]))
				b.setMnemonic((String)objects[9]);
			if(Validations.validateIsNotNull(objects[10]))
				b.setBeanName((String)objects[10]);
			if(Validations.validateIsNotNull(objects[11]))
				b.setDescription((String)objects[11]);
			if(Validations.validateIsNotNull(objects[12]))
				b.setModuleName((String)objects[12]);
			
			processes.add(b);
		}
		return processes;
		
	}
	
	/**
	 * Execute remote batch processes.
	 *
	 * @param processes the processes
	 * @throws ServiceException the service exception
	 */
	public void executeRemoteBatchProcesses(BusinessProcessTO [] processes) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
        
		for (int i = 0; i < processes.length; i++) {
			BusinessProcessTO process = ((BusinessProcessTO)processes[i]);
			Schedulebatch scheduleBatch = new Schedulebatch();
	        scheduleBatch.setIdBusinessProcess(process.getIdBusinessProcessPk());
	        scheduleBatch.setUserName(loggerUser.getUserName());
	        scheduleBatch.setPrivilegeCode(loggerUser.getIdPrivilegeOfSystem());
	        
	        scheduleBatch.setParameters(businessProcessesServiceBean.loadProcessParameters(process.getIdBusinessProcessPk()));
	        scheduleBatch.getParameters().put(GeneralConstants.PARAMETER_DATE,
	        		CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
	        
	        clientRestService.excuteRemoteBatchProcess(scheduleBatch,process.getModuleName());
		}
		
	}
	
	/**
	 * Update process schedule.
	 *
	 * @param processSchedule the process schedule
	 * @throws ServiceException the service exception
	 */
	public void updateProcessSchedule(ProcessSchedule processSchedule) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		businessProcessesServiceBean.updateProcessSchedule(processSchedule);
	}
	
	/**
	 * Update notification facade.
	 *
	 * @param modifyNotification the modify notification
	 * @return the process notification to
	 * @throws ServiceException the service exception
	 */
	public ProcessNotificationTO updateNotificationFacade(ProcessNotificationTO modifyNotification)throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
        DestinationNotification modifyDestinationNotification = modifyNotification.getDestinationNotification();
        modifyDestinationNotification.setProcessNotification(modifyNotification.getProcessNotification());
        modifyDestinationNotification = businessProcessesServiceBean.update(modifyDestinationNotification);
        modifyNotification.setDestinationNotification(modifyDestinationNotification);
        modifyNotification.setProcessNotification(modifyDestinationNotification.getProcessNotification());
        return modifyNotification;
	}
	
	/**
	 * Update notification status.
	 *
	 * @param statusNotification the status notification
	 * @throws ServiceException the service exception
	 */
	public void updateNotificationStatus(ProcessNotification statusNotification) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//TODO Use Active and De active Privileges
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());      
		businessProcessesServiceBean.update(statusNotification);
	}
	
	/**
	 * To stop the process.
	 *
	 * @param selectedProcesses the selected processes
	 * @throws ServiceException the service exception
	 */
	public void stopProcesses(BusinessProcessTO[] selectedProcesses) throws ServiceException {
		for (int i = 0; i < selectedProcesses.length; i++) {
			List<ProcessSchedule> processSchedules = businessProcessesServiceBean.getProcessSchedulesServiceBean(selectedProcesses[i].getIdBusinessProcessPk());
			for(ProcessSchedule processSchedule : processSchedules ){
				processSchedule.setScheduleState(ProcessScheduleStateType.DEACTIVE.getCode());
				updateProcessSchedule(processSchedule);
			}
		}
	}
	
	/**
	 * Load busines process.
	 *
	 * @param idBusinessProc the id business proc
	 * @return the business process
	 */
	public BusinessProcess loadBusinesProcess(Long idBusinessProc){
		return businessProcessesServiceBean.find(BusinessProcess.class, idBusinessProc);
	}
	
	/**
	 * Modify business process.
	 *
	 * @param businessProcess the business process
	 * @param businessProcessDetail the business process detail
	 * @return the business process
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public BusinessProcess modifyBusinessProcess(BusinessProcess businessProcess, BusinessProcessTO businessProcessDetail) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
        if(Validations.validateIsNotNull(businessProcessDetail.getProcessScheduleDataModel())){
        	for(ProcessSchedule processSche:businessProcessDetail.getProcessScheduleDataModel().getDataList()){
        		if(processSche.isBlExist()){
        			businessProcessesServiceBean.update(processSche);
        			//remove old state of timer in server
        			schedulerservice.deleteTimer(processSche.getIdProcessSchedulePk());
        			//check state
	        			if(!processSche.getScheduleState().equals(ProcessScheduleStateType.DEACTIVE.getCode())){
	        				//If state is Active we update timer in server
	        				schedulerservice.registerTimer(processSche.getIdProcessSchedulePk(),processSche.getScheduleInfo());
	        			} 
        		}else{
        			processSche.setIdProcessSchedulePk(null);
        			businessProcessesServiceBean.create(processSche);
        			//register timer in server
        			schedulerservice.registerTimer(processSche.getIdProcessSchedulePk(),processSche.getScheduleInfo());
        		}
        	}
        }
        if(Validations.validateIsNotNull(businessProcessDetail.getProcessNotificationDataModel())){
        	for(ProcessNotification processNoti:businessProcessDetail.getProcessNotificationDataModel().getDataList()){
        		if(ProcessType.ONLINE.getCode().equals(businessProcess.getProcessType())){
        			if(NotificationType.PROCESS.getCode().equals(processNoti.getNotificationType())){
        				throw new ServiceException(ErrorServiceType.BUSINESS_PROCESS_NOTIFICATION_TYPE_ERROR);
        			}
        		}        		        		
        		
        		if(processNoti.isBlExist()){
        			List<Long> listId = monitoringProcessesServiceBean.getDestinationNotificationNew(processNoti.getIdProcessNotificationPk());
        			if(Validations.validateListIsNotNullAndNotEmpty(listId)) {
        				monitoringProcessesServiceBean.updateNotificationLogger(listId, loggerUser);
            		}        			
        			monitoringProcessesServiceBean.removeUserDestinations(processNoti.getIdProcessNotificationPk());        			
        			businessProcessesServiceBean.update(processNoti);
        		}else{
        			processNoti.setIdProcessNotificationPk(null);
        			businessProcessesServiceBean.create(processNoti);
        		}
        	}
        }
		return businessProcessesServiceBean.update(businessProcess);
	}
	
	/**
	 * To get the list of macroProcesses.
	 *
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getMacroProcessList() throws ServiceException {
		return monitoringProcessesServiceBean.getMacroProcessListServiceBean();
	}
	
	/**
	 * To get the list of macroProcesses.
	 *
	 * @param idSystem the id system
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */

	public List<Object[]> getMacroProcessListbyidSystem(Integer idSystem)throws ServiceException {
		return monitoringProcessesServiceBean.getMacroProcessListbyidSystemServiceBean(idSystem);
	}


	/**
	 * To get the list of processlevels.
	 *
	 * @param idBusinessProcess the id business process
	 * @return the values of processlevels
	 * @throws ServiceException the service exception
	 */

	public List<Object[]> getProcessLevels(List<Long> idBusinessProcess) throws ServiceException {
		return monitoringProcessesServiceBean.getProcessLevelsServiceBean(idBusinessProcess);
	}

	/**
	 * To get the ProcessDetails.
	 *
	 * @param monitorSystemProcessFilter the monitor system process filter
	 * @return the values of processDetails
	 * @throws ServiceException the service exception
	 */

	public List<Object[]> getProcessDetails(
			MonitorSystemProcessFilter monitorSystemProcessFilter) throws ServiceException {
		if(monitorSystemProcessFilter.getIdReferenceProcesses().size()!=0){
			return monitoringProcessesServiceBean.getProcessDetailServiceBean(monitorSystemProcessFilter);
		}
		else{
			return new ArrayList<Object[]>();
		}
	}

	/**
	 * Gets the system list.
	 *
	 * @return the system list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getSystemList() throws ServiceException{
		return monitoringProcessesServiceBean.getSystemList();
	}
	
	/**
	 * Gets the notification configuration.
	 *
	 * @param businessProcess the business process
	 * @return the notification configuration
	 * @throws ServiceException the service exception
	 */
	public List<NotificationConfiguration> getNotificationConfiguration(Long businessProcess) throws ServiceException {
		return businessProcessesServiceBean.getNotificationConfiguration(businessProcess);
	}
}
