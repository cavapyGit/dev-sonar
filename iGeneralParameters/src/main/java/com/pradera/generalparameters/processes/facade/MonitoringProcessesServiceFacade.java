package com.pradera.generalparameters.processes.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.commons.services.remote.parameters.ProcessesMonitoringService;
import com.pradera.generalparameters.processes.service.MonitoringProcessesServiceBean;
import com.pradera.integration.exception.ServiceException;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class MonitoringProcessesServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2014
 */
@Stateless
@Performance
@Remote(ProcessesMonitoringService.class)
public class MonitoringProcessesServiceFacade implements ProcessesMonitoringService {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The monitoring processes service bean. */
	@EJB
	MonitoringProcessesServiceBean monitoringProcessesServiceBean;

	/**
	 * To get the list of macroProcesses.
	 *
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<Object[]> getMacroProcessList() throws ServiceException {
		return monitoringProcessesServiceBean.getMacroProcessListServiceBean();
	}
	
	/**
	 * To get the list of macroProcesses.
	 *
	 * @param idSystem the id system
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<Object[]> getMacroProcessListbyidSystem(Integer idSystem)throws ServiceException {
		return monitoringProcessesServiceBean.getMacroProcessListbyidSystemServiceBean(idSystem);
	}


	/**
	 * To get the list of processlevels.
	 *
	 * @param idBusinessProcess the id business process
	 * @return the values of processlevels
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<Object[]> getProcessLevels(List<Long> idBusinessProcess) throws ServiceException {
		return monitoringProcessesServiceBean.getProcessLevelsServiceBean(idBusinessProcess);
	}

	/**
	 * To get the ProcessDetails.
	 *
	 * @param monitorSystemProcessFilter the monitor system process filter
	 * @return the values of processDetails
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<Object[]> getProcessDetails(
			MonitorSystemProcessFilter monitorSystemProcessFilter) throws ServiceException {
		if(monitorSystemProcessFilter.getIdReferenceProcesses().size()!=0){
			return monitoringProcessesServiceBean.getProcessDetailServiceBean(monitorSystemProcessFilter);
		}
		else{
			return new ArrayList<Object[]>();
		}
	}

	/**
	 * The method for getting the Audit process details from the data base.
	 *
	 * @param optionPrevilages the option previlages
	 * @param startDate starting date
	 * @param endDate starting date
	 * @return the List of Process beans
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<Object[]> getAuditProcessesDetails(
			List<Integer> optionPrevilages, Date startDate, Date endDate)
			throws ServiceException {
		return monitoringProcessesServiceBean.getAuditProcessesDetailsServiceBean(
				optionPrevilages, startDate, endDate);
	}
	
	/**
	 * Method for getting the parameters of selected process.
	 *
	 * @param id Integer
	 * @return the values of parameters
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<Object[]>  getProcessParameters(Long id) throws ServiceException{
		return monitoringProcessesServiceBean.getProcessParametersServiceBean(id);
	}

	
	
	
	/**
	 * Gets the business process details.
	 *
	 * @param refProcess the ref process
	 * @return the values of businessProcessDetails
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<Object[]> getBusinessProcessDetails(Long refProcess) throws ServiceException {
		return monitoringProcessesServiceBean.getBusinessProcessDetailsServiceBean(refProcess);
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.processes.remote.IProcessesMonitoringRemote#getSystemList()
	 */
	@Override
	public List<Object[]> getSystemList() throws ServiceException {
		return monitoringProcessesServiceBean.getSystemList();
	}

}
