package com.pradera.generalparameters.processes.view;

import java.io.Serializable;

import com.pradera.commons.security.model.type.UserAccountStateType;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class SystemServiceFacade.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */

public class User implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3033537987698939397L;
	
	/** The id user pk. */
	private String idUserPk;
	
	/** The id issuer fk. */
	private String idIssuerFk;
	
	/** The id participant fk. */
	private String idParticipantFk;
	
	/** The user names. */
	private String userNames;
	
	/** The user surname. */
	private String userSurname;
	
	/** The user second surname. */
	private String userSecondSurname;
	
	/** The user phone. */
	private String userPhone;
	
	/** The user email. */
	private String userEmail;
	
	/** The user type. */
	private String userType;
	
	/** The user status. */
	private String userStatus;
	
	/** The user name. */
	private String userName;
	
	/** The notification type. */
	private String notificationType;
	
	/** The regulator agent. */
	private boolean regulatorAgent;
	
	/** The restriction mac. */
	private boolean restrictionMac;
	
	/** The notification status. */
	private Integer notificationStatus;
	
	/** The seq id. */
	private Integer seqId;
	
	/** The seq number. */
	private Integer seqNumber;
	
	
	
	
	/**
	 * Gets the id user pk.
	 *
	 * @return the idUserPk
	 */
	public String getIdUserPk() {
		return idUserPk;
	}

	/**
	 * Sets the id user pk.
	 *
	 * @param idUserPk the idUserPk to set
	 */
	public void setIdUserPk(String idUserPk) {
		this.idUserPk = idUserPk;
	}

	/**
	 * Gets the id issuer fk.
	 *
	 * @return the idIssuerFk
	 */
	public String getIdIssuerFk() {
		return idIssuerFk;
	}

	/**
	 * Sets the id issuer fk.
	 *
	 * @param idIssuerFk the idIssuerFk to set
	 */
	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}

	/**
	 * Gets the id participant fk.
	 *
	 * @return the idParticipantFk
	 */
	public String getIdParticipantFk() {
		return idParticipantFk;
	}

	/**
	 * Sets the id participant fk.
	 *
	 * @param idParticipantFk the idParticipantFk to set
	 */
	public void setIdParticipantFk(String idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	/**
	 * Gets the user names.
	 *
	 * @return the userNames
	 */
	public String getUserNames() {
		return userNames;
	}

	/**
	 * Sets the user names.
	 *
	 * @param userNames the userNames to set
	 */
	public void setUserNames(String userNames) {
		this.userNames = userNames;
	}

	/**
	 * Gets the user surname.
	 *
	 * @return the userSurname
	 */
	public String getUserSurname() {
		return userSurname;
	}

	/**
	 * Sets the user surname.
	 *
	 * @param userSurname the userSurname to set
	 */
	public void setUserSurname(String userSurname) {
		this.userSurname = userSurname;
	}

	/**
	 * Gets the user second surname.
	 *
	 * @return the userSecondSurname
	 */
	public String getUserSecondSurname() {
		return userSecondSurname;
	}

	/**
	 * Sets the user second surname.
	 *
	 * @param userSecondSurname the userSecondSurname to set
	 */
	public void setUserSecondSurname(String userSecondSurname) {
		this.userSecondSurname = userSecondSurname;
	}

	/**
	 * Gets the user phone.
	 *
	 * @return the userPhone
	 */
	public String getUserPhone() {
		return userPhone;
	}

	/**
	 * Sets the user phone.
	 *
	 * @param userPhone the userPhone to set
	 */
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	/**
	 * Gets the user email.
	 *
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * Sets the user email.
	 *
	 * @param userEmail the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * Gets the user type.
	 *
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * Sets the user type.
	 *
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * Gets the user status.
	 *
	 * @return the userStatus
	 */
	public String getUserStatus() {
		return userStatus;
	}

	/**
	 * Sets the user status.
	 *
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the notification type.
	 *
	 * @return the notificationType
	 */
	public String getNotificationType() {
		return notificationType;
	}

	/**
	 * Sets the notification type.
	 *
	 * @param notificationType the notificationType to set
	 */
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * Checks if is regulator agent.
	 *
	 * @return the regulatorAgent
	 */
	public boolean isRegulatorAgent() {
		return regulatorAgent;
	}

	/**
	 * Sets the regulator agent.
	 *
	 * @param regulatorAgent the regulatorAgent to set
	 */
	public void setRegulatorAgent(boolean regulatorAgent) {
		this.regulatorAgent = regulatorAgent;
	}

	/**
	 * Checks if is restriction mac.
	 *
	 * @return the restrictionMac
	 */
	public boolean isRestrictionMac() {
		return restrictionMac;
	}

	/**
	 * Sets the restriction mac.
	 *
	 * @param restrictionMac the restrictionMac to set
	 */
	public void setRestrictionMac(boolean restrictionMac) {
		this.restrictionMac = restrictionMac;
	}

	/**
	 * Gets the notification status.
	 *
	 * @return the notificationStatus
	 */
	public Integer getNotificationStatus() {
		return notificationStatus;
	}

	/**
	 * Sets the notification status.
	 *
	 * @param notificationStatus the notificationStatus to set
	 */
	public void setNotificationStatus(Integer notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	/**
	 * Gets the seq id.
	 *
	 * @return the seqId
	 */
	public Integer getSeqId() {
		return seqId;
	}

	/**
	 * Sets the seq id.
	 *
	 * @param seqId the seqId to set
	 */
	public void setSeqId(Integer seqId) {
		this.seqId = seqId;
	}

	/**
	 * Constructor.
	 */
	public User() {
		super();
	}
	
	/**
	 * Instantiates a new user.
	 *
	 * @param idUserPk the id user pk
	 * @param idIssuerFk the id issuer fk
	 * @param idParticipantFk the id participant fk
	 * @param userNames the user names
	 * @param userSurname the user surname
	 * @param userSecondSurname the user second surname
	 * @param userPhone the user phone
	 * @param userEmail the user email
	 * @param userType the user type
	 * @param userStatus the user status
	 * @param regulatorAgent the regulator agent
	 * @param restrictionMac the restriction mac
	 * @param notificationType the notification type
	 * @param notificationStatus the notification status
	 */
	public User(String idUserPk, String idIssuerFk, String idParticipantFk,
			String userNames, String userSurname, String userSecondSurname,
			String userPhone, String userEmail, String userType,
			String userStatus,  boolean regulatorAgent,
			boolean restrictionMac,String notificationType,Integer notificationStatus) {
		super();
		this.idUserPk = idUserPk;
		this.idIssuerFk = idIssuerFk;
		this.idParticipantFk = idParticipantFk;
		this.userNames = userNames;
		this.userSurname = userSurname;
		this.userSecondSurname = userSecondSurname;
		this.userPhone = userPhone;
		this.userEmail = userEmail;
		this.userType = userType;
		this.userStatus = userStatus;
		this.regulatorAgent = regulatorAgent;
		this.restrictionMac = restrictionMac;
		this.notificationType=notificationType;
		this.notificationStatus=notificationStatus;
	}
	
	/**
	 * Instantiates a new user.
	 *
	 * @param idUserPk the id user pk
	 * @param idIssuerFk the id issuer fk
	 * @param idParticipantFk the id participant fk
	 * @param userNames the user names
	 * @param userSurname the user surname
	 * @param userSecondSurname the user second surname
	 * @param userPhone the user phone
	 * @param userEmail the user email
	 * @param userType the user type
	 * @param userStatus the user status
	 * @param notificationType the notification type
	 * @param notificationStatus the notification status
	 */
	public User(String idUserPk, String idIssuerFk, String idParticipantFk,
			String userNames, String userSurname, String userSecondSurname,
			String userPhone, String userEmail, String userType,
			String userStatus,String notificationType,Integer notificationStatus) {
		super();
		this.idUserPk = idUserPk;
		this.idIssuerFk = idIssuerFk;
		this.idParticipantFk = idParticipantFk;
		this.userNames = userNames;
		this.userSurname = userSurname;
		this.userSecondSurname = userSecondSurname;
		this.userPhone = userPhone;
		this.userEmail = userEmail;
		this.userType = userType;
		this.userStatus = userStatus;
		this.notificationType=notificationType;
		this.notificationStatus=notificationStatus;
	}
	  /**
  	 * Gets the state icon.
  	 *
  	 * @return the state icon
  	 */
  	public String getStateIcon() {
			String icono = null;
			if (userStatus != null) {
				icono = UserAccountStateType.get(Integer.valueOf(userStatus)).getIcon();
			}
			return icono;
		}
		  
	    /**
    	 * Gets the state description.
    	 *
    	 * @return the state description
    	 */
    	public String getStateDescription() {
			String description = null;
			if (userStatus != null) {
				description = UserAccountStateType.get(Integer.valueOf(userStatus)).getValue();
			}
			return description;
		}

    	
    	/**
	     * Gets the notification type description.
	     *
	     * @return the notification type description
	     */
    	public String getProcessNotificationType() {
			String description = null;
			if (notificationType != null) {
//				description = ProcessNotificationType.get(Integer.valueOf(notificationType)).getValue();
			}
			return description;
		}
	
    	

    	/**
	     * Gets the notification type description.
	     *
	     * @return the notification type description
	     */
    	public String getProcessNotificationStatus() {
			String description = null;
			if (notificationStatus != null) {
//				description = ProcessNotificationStatus.get(Integer.valueOf(notificationStatus)).getValue();
			}
			return description;
		}
		
		/**
		 * Gets the seq number.
		 *
		 * @return the seqNumber
		 */
		public Integer getSeqNumber() {
			return seqNumber;
		}
		
		/**
		 * Sets the seq number.
		 *
		 * @param seqNumber the seqNumber to set
		 */
		public void setSeqNumber(Integer seqNumber) {
			this.seqNumber = seqNumber;
		}
	
	
	
}
