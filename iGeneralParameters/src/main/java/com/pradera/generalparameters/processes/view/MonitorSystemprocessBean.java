package com.pradera.generalparameters.processes.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Minutes;
import org.primefaces.extensions.model.dynaform.DynaFormControl;
import org.primefaces.extensions.model.dynaform.DynaFormLabel;
import org.primefaces.extensions.model.dynaform.DynaFormModel;
import org.primefaces.extensions.model.dynaform.DynaFormRow;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.processes.remote.types.ComboBoxValueBean;
import com.pradera.commons.processes.remote.view.MonitoringSystemProcessDataModel;
import com.pradera.commons.processes.remote.view.beans.ManageSystemProcessBean;
import com.pradera.commons.processes.remote.view.beans.ParametersBean;
import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.commons.services.remote.parameters.ProcessesMonitoringService;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.generalparameters.messages.facade.MessagesServiceFacade;
import com.pradera.generalparameters.processes.facade.BusinessProcessesServiceFacade;
import com.pradera.generalparameters.processes.to.BusinessProcessTO;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.type.ProcessLoggerStateType;
import com.pradera.model.security.type.SystemType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitorSystemprocessBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03-feb-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MonitorSystemprocessBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The i depositary system id. */
	@Inject @Configurable Integer iDepositarySystemId;
	
	/** The security system id. */
	@Inject @Configurable Integer securitySystemId;
	
	/** The ProcessesMonitoringService. */
	@EJB
	private ProcessesMonitoringService monitoringProcessesServiceFacade;
	
	/** The business processes service facade. */
	@EJB
	private BusinessProcessesServiceFacade businessProcessesServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The messages facade. */
	@EJB
	MessagesServiceFacade messagesFacade;
	
	/** The userInfo. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The executable processes data model. */
	private GenericDataModel<BusinessProcessTO> executableProcessesDataModel;
	
	/** The list ProcessExecutionType. */
	private List<ParameterTable> typeProcessExecution;
	
	
	/** The selected processes. */
	private BusinessProcessTO[] selectedProcesses;

	/** The List Systems. */
	private List<ComboBoxValueBean> lstSystems;
	
	/** The List Macro Processes. */
	private List<ComboBoxValueBean> lstMacroProcess;
	
	/** The List ProcessLevelOne. */
	private List<ComboBoxValueBean> lstProcessLevelOne;
	
	/** The List ProcessLevelTwo. */
	private List<ComboBoxValueBean> lstProcessLevelTwo;
	
	/** The List Macro Processes. */
	private List<ComboBoxValueBean> macroProcessList;
	
	/** The List ProcessLevelOne. */
	private List<ComboBoxValueBean> processLevelOneList;
	
	/** The List ProcessLevelTwo. */
	private List<ComboBoxValueBean> processLevelTwoList;
	
	/** The obj ManagesystemProcessBean. */
	private ManageSystemProcessBean systemprocessBean;

	/** The MonitorSystemProcessFilter. */
	private MonitorSystemProcessFilter monitorSystemProcessFilter;
	
	/** The MonitorExecuteProcessFilter. */
	private MonitorSystemProcessFilter monitorExecuteProcessFilter;

	/** The obj MonitoringSystemProcessDataModel. */
	private MonitoringSystemProcessDataModel monitoringSystemProcessDataModel;
	
	/** The obj ManageSystemProcessBean. */
	private ManageSystemProcessBean selectedProcessBean;

	/** The obj DynaFormModel. */
	private DynaFormModel model;
	
	/** The list users types. */
	private List<SelectItem> listInstitutionsTypes;
	
	/** The list institutions. */
	private List<SelectItem> listInstitutions;
	
	/** The users accounts. */
	private List<UserAccountSession> usersAccounts;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The institution selected. */
	private Object institutionSelected;
	

	/**
	 * The Postconstrut method to initialize the objects.
	 */
	@PostConstruct
	public void init() {
		
		try {
			monitorSystemProcessFilter = new MonitorSystemProcessFilter();		
			monitorSystemProcessFilter.setIdUser(userInfo.getUserAccountSession().getUserName());
			monitorSystemProcessFilter.setInitialDate(CommonsUtilities.currentDate());
			monitorSystemProcessFilter.setFinalDate(CommonsUtilities.currentDate());
			
			lstSystems = new ArrayList<ComboBoxValueBean>();
			lstMacroProcess = new ArrayList<ComboBoxValueBean>();
			lstProcessLevelOne = new ArrayList<ComboBoxValueBean>();
			lstProcessLevelTwo = new ArrayList<ComboBoxValueBean>();
			macroProcessList = new ArrayList<ComboBoxValueBean>();
			processLevelOneList = new ArrayList<ComboBoxValueBean>();
			processLevelTwoList = new ArrayList<ComboBoxValueBean>();
			listExcecutionProcessType();
			listSystems();
			listHolidays();
			loadUserFilters();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Set valid buttons listener.
	 */
	public void setValidButtonsListener(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load user filters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadUserFilters() throws ServiceException{
		listInstitutionsTypes = new ArrayList<>();
		for(InstitutionType institution : InstitutionType.list){
			listInstitutionsTypes.add(new SelectItem(institution.getCode(), institution.getValue()));
		}
		listInstitutions = new ArrayList<>();
		usersAccounts = new ArrayList<>();
		institutionType = userInfo.getUserAccountSession().getInstitutionType();
		selectInstitutionType(); 
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			institutionSelected = userInfo.getUserAccountSession().getParticipantCode();
			selectInstitution();
		} else if(userInfo.getUserAccountSession().isIssuerInstitucion()){
			institutionSelected = userInfo.getUserAccountSession().getIssuerCode();
			selectInstitution();
		}
		monitorSystemProcessFilter.setIdUser(userInfo.getUserAccountSession().getUserName());
	}
	
	/**
	 * Select institution type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void selectInstitutionType() throws ServiceException{
		if(institutionType!= null){
			InstitutionType institution = InstitutionType.get(institutionType);
			switch (institution) {
			case AFP:
			case PARTICIPANT:
			case PARTICIPANT_INVESTOR:	
				List<Participant> participantList = helperComponentFacade.findParticipantNativeQueryFacade(new Participant());
				listInstitutions.clear();
				for(Participant participant:participantList){
					listInstitutions.add(new SelectItem((Object)participant.getIdParticipantPk(),participant.getMnemonic()+"-"+ participant.getIdParticipantPk()+"-"+participant.getDescription()));
				}
				usersAccounts.clear();
				break;
			case ISSUER:
			case ISSUER_DPF:
				List<IssuerTO> issuerList = helperComponentFacade.findIssuerByHelper(new IssuerSearcherTO());
				listInstitutions.clear();
				for(IssuerTO issuer : issuerList){
					listInstitutions.add(new SelectItem((Object)issuer.getIssuerCode(),issuer.getMnemonic()+"-"+ issuer.getIssuerDesc(),issuer.getMnemonic()));
				}
				usersAccounts.clear();
				break;
			default:
				usersAccounts = messagesFacade.getUsersFromInstitution(institution, null);					
				listInstitutions.clear();
				break;
			}
			
		} else {
			listInstitutions.clear();
			usersAccounts.clear();
		}
	}
	
	/**
	 * Select institution.
	 */
	public void selectInstitution(){
		if(institutionSelected!=null){
			InstitutionType institution = InstitutionType.get(institutionType);
			String issuerCode = null;
			Long participantCode = null;
			if(institution.equals(InstitutionType.ISSUER)){
				issuerCode = institutionSelected.toString();
				usersAccounts = messagesFacade.getUsersFromInstitution(institution, issuerCode);
			} else if(institution.equals(InstitutionType.PARTICIPANT) || institution.equals(InstitutionType.AFP)){
				participantCode = Long.valueOf(institutionSelected.toString());
				usersAccounts = messagesFacade.getUsersFromInstitution(institution, participantCode);
			}						
			
		}else{
			usersAccounts.clear();
		}
	}
	
	/**
	 * Generate parameters export.
	 *
	 * @return the map
	 */
	public Map<String, Object> generateParametersExport(){
		//using LinkedHashMap for using order of insert in map
		LinkedHashMap<String, Object> parametersExport = new LinkedHashMap<>();
		FacesContext context = FacesContext.getCurrentInstance();
	    ResourceBundle msgs = context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	    //parameter system
	    String systemName=null;
	    for(ComboBoxValueBean system :lstSystems){
	    	if(system.getItemValue().toString().equals(monitorSystemProcessFilter.getIdSystem().toString())){
	    		systemName = system.getItemLabel().toString();
	    		break;
	    	}
	    }
	    parametersExport.put(msgs.getString("monitor.system.process.lbl.system"), systemName);
	    //parameter macroprocess
	    String macroprocessName= null;
	    for(ComboBoxValueBean proc : lstMacroProcess){
	    	if(proc.getItemValue().toString().equals(monitorSystemProcessFilter.getIdMacroProcess().toString())){
	    		macroprocessName = proc.getItemLabel().toString();
	    		break;
	    	}
	    }
	    parametersExport.put(msgs.getString("monitor.system.process.lbl.macroprocess"),macroprocessName);
	    //user ejecutor
	    parametersExport.put(msgs.getString("monitor.system.process.lbl.user"),monitorSystemProcessFilter.getIdUser());
	    //empty parameter
	    parametersExport.put(" ","");
	    //parameter initial date
	    parametersExport.put(msgs.getString("monitor.system.process.lbl.initialDate"), CommonsUtilities.convertDatetoString(monitorSystemProcessFilter.getInitialDate()));
	    //parameter end date
	    parametersExport.put(msgs.getString("monitor.system.process.lbl.finaldate"), CommonsUtilities.convertDatetoString(monitorSystemProcessFilter.getFinalDate()));
		return parametersExport;
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Load process exec information.
	 *
	 * @return the string
	 */
	public String loadProcessExecInformation(){
		cleanExecuteProcesses();
		return "executeProcessesRule";
	}

	/**
	 * To get the Systems List.
	 */
	public void listSystems() {
		try{
			
			//monitorSystemProcessFilter.setIdSystem(Integer.valueOf(userInfo.getUserAccountSession()));
			monitorSystemProcessFilter.setIdMacroProcess(null);
			monitorSystemProcessFilter.setIdProcessLevelOne(null);
			monitorSystemProcessFilter.setIdProcessLevelTwo(null);
			
			lstMacroProcess.clear();
			lstProcessLevelOne.clear();
			lstProcessLevelTwo.clear();
			
			List<Object[]> listObjArrSystems = monitoringProcessesServiceFacade.getSystemList();
			ComboBoxValueBean comboBoxValueBean = null;
			for (Object[] objects : listObjArrSystems) {
				if(iDepositarySystemId.equals(Integer.valueOf(objects[0].toString())) || 
						securitySystemId.equals(Integer.valueOf(objects[0].toString()))) {
					comboBoxValueBean = new ComboBoxValueBean();
					comboBoxValueBean.setItemLabel(objects[1].toString());
					comboBoxValueBean.setItemValue(objects[0].toString());
					lstSystems.add(comboBoxValueBean);
				}
				
			}
			
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * List excecution process type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listExcecutionProcessType() throws ServiceException{
		typeProcessExecution = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.BUSINESS_PROCESS_TYPE.getCode());
	}

	/**
	 * To get the List MacroProcesses.
	 */
	public void listMacroProcess() {
		try{
			monitorSystemProcessFilter.setIdMacroProcess(null);
			monitorSystemProcessFilter.setIdProcessLevelOne(null);
			monitorSystemProcessFilter.setIdProcessLevelTwo(null);
			
			lstMacroProcess.clear();
			lstProcessLevelOne.clear();
			lstProcessLevelTwo.clear();
			
			List<Object[]> listObjArr = monitoringProcessesServiceFacade.getMacroProcessListbyidSystem(monitorSystemProcessFilter.getIdSystem());
	
			ComboBoxValueBean comboBoxValueBean = null;
			//convert list<Object[]> to list<ComboBoxValueBean>  
			for (Object[] objects : listObjArr) {
				comboBoxValueBean = new ComboBoxValueBean();
				comboBoxValueBean.setItemLabel(objects[1].toString());
				comboBoxValueBean.setItemValue(objects[0].toString());
				lstMacroProcess.add(comboBoxValueBean);
			}
		}
		catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * To get the ListProcessLevelOne.
	 */
	public void listProcessLevelOne() {
		try {
			lstProcessLevelOne.clear();
			lstProcessLevelTwo.clear();
			monitorSystemProcessFilter.setIdProcessLevelOne(null);
			monitorSystemProcessFilter.setIdProcessLevelTwo(null);
			
			List<Object[]> listObjArr = new ArrayList<Object[]>();
			List<Long> lstProcesses = new ArrayList<Long>();
			List<Long> lstRefProcesses = new ArrayList<Long>();
			ComboBoxValueBean comboBoxValueBean = null;
			if (Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdMacroProcess())) {
				lstProcesses.add(monitorSystemProcessFilter.getIdMacroProcess());
				listObjArr = monitoringProcessesServiceFacade.getProcessLevels(lstProcesses);
			}
			//convert list<Object[]> to list<ComboBoxValueBean> 
			for (Object[] objects : listObjArr) {
				comboBoxValueBean = new ComboBoxValueBean();
				comboBoxValueBean.setItemLabel(objects[1].toString());
				comboBoxValueBean.setItemValue(objects[0].toString());
				lstRefProcesses.add((new Long(objects[0].toString())));
				lstProcessLevelOne.add(comboBoxValueBean);
			}
			monitorSystemProcessFilter.setIdReferenceProcesses(lstRefProcesses);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * To get the List ProcessLevelTwo.
	 */
	public void listProcessLevelTwo() {
		try {
			lstProcessLevelTwo.clear();
			monitorSystemProcessFilter.setIdProcessLevelTwo(null);
			
			List<Object[]> listObjArr = new ArrayList<Object[]>();
			List<Long> lstProcesses = new ArrayList<Long>();
			List<Long> lstRefProcesses = new ArrayList<Long>();
			ComboBoxValueBean comboBoxValueBean = null;
			
			if (Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelOne())) {
				lstProcesses.add(monitorSystemProcessFilter.getIdProcessLevelOne());
				listObjArr = monitoringProcessesServiceFacade.getProcessLevels(lstProcesses);
			} 
//			else {
//				//To add all the list of processlevelone to get processleveltwo
//				for (ComboBoxValueBean macro : lstProcessLevelOne) {
//					lstProcesses.add((new Long(macro.getItemValue().toString())));
//				}
//				if(lstProcesses.size()!=0){
//					listObjArr = monitoringProcessesServiceFacade.getProcessLevels(lstProcesses);
//				}
//			}
			//convert list<Object[]> to list<ComboBoxValueBean> 
			for (Object[] objects : listObjArr) {
				comboBoxValueBean = new ComboBoxValueBean();
				comboBoxValueBean.setItemLabel(objects[1].toString());
				comboBoxValueBean.setItemValue(objects[0].toString());
				lstRefProcesses.add((new Long(objects[0].toString())));
				lstProcessLevelTwo.add(comboBoxValueBean);
			}
			monitorSystemProcessFilter.setIdReferenceProcesses(lstRefProcesses);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * To get SearchCriteria Results.
	 */
	public void fillListProcess() {
		try {
			hideDialogs();
						
			List<Object[]> processDetails = monitoringProcessesServiceFacade.getProcessDetails(monitorSystemProcessFilter);
			List<ManageSystemProcessBean> systemProcesses = new ArrayList<ManageSystemProcessBean>();
			for (Object obj[] : processDetails) {
				systemprocessBean = new ManageSystemProcessBean();
				systemprocessBean.setIdProcessLoggerPk((Integer.parseInt(obj[0].toString())));
				systemprocessBean.setProcessDate((Date) obj[1]);
				systemprocessBean.setState((Integer.parseInt(obj[2].toString())));
				systemprocessBean.setStateDiscription(ProcessLoggerStateType.lookup.get(systemprocessBean.getState()).getValue());
				if(Validations.validateIsNotNull(obj[3]))
					systemprocessBean.setFinishingDate((Date) obj[3]);
				systemprocessBean.setProcessType((Integer)obj[4]);
				if(Validations.validateIsNotNull(obj[5]))
					systemprocessBean.setMnemonic((String) obj[5]);
				systemprocessBean.setProcessName((String) obj[6]);
				systemprocessBean.setIdSystem(monitorSystemProcessFilter.getIdSystem());
				for(ComboBoxValueBean system : lstSystems){
					if(Integer.parseInt(system.getItemValue().toString()) == monitorSystemProcessFilter.getIdSystem()){
						systemprocessBean.setSystem(system.getItemLabel().toString());
					}
				}
				systemprocessBean.setUserName(Validations.validateIsNotNull(obj[11])? (String) obj[11] : "");
				systemprocessBean.setProcessLevel1((String) obj[8]);
				systemprocessBean.setProcessLevel2((String) obj[9]);
				for(ComboBoxValueBean macroprocess : lstMacroProcess){
					if(new Long(macroprocess.getItemValue().toString()).equals(monitorSystemProcessFilter.getIdMacroProcess())){
						systemprocessBean.setMacroProcess(macroprocess.getItemLabel().toString());
					}
				}
				
				if(Validations.validateIsNotNull(systemprocessBean.getFinishingDate())){
					DateTime finish = new DateTime(systemprocessBean.getFinishingDate());
					DateTime initial = new DateTime(systemprocessBean.getProcessDate());
					Interval interval = new Interval(initial,finish);
					Minutes duration = Minutes.minutesIn(interval);
					systemprocessBean.setProcessDuration(duration.getMinutes());
				}
				
				systemProcesses.add(systemprocessBean);
			}

			monitoringSystemProcessDataModel = new MonitoringSystemProcessDataModel(systemProcesses);

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To hide the dialogs.
	 */
	private void hideDialogs() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("frmManageSystemProcess:modalDialogSystemProcess");
	}

	/**
	 * To Show the details of the selected Process in a dialog.
	 *
	 * @return the selected details
	 */
	public void getSelectedDetails() {
		getProcessDetails();
		JSFUtilities
				.showComponent("frmManageSystemProcess:modalDialogSystemProcess");
	}

	/**
	 * To clean the Search Screen.
	 */
	public void cleanMonitoringProcess() {
		//hideDialogs();
		monitorSystemProcessFilter = new MonitorSystemProcessFilter();		
		monitorSystemProcessFilter.setIdUser(userInfo.getUserAccountSession().getUserName());
		monitorSystemProcessFilter.setInitialDate(CommonsUtilities.currentDate());
		monitorSystemProcessFilter.setFinalDate(CommonsUtilities.currentDate());
		selectedProcessBean=null;
		lstMacroProcess.clear();
		lstProcessLevelOne.clear();
		lstProcessLevelTwo.clear();
		monitoringSystemProcessDataModel = null;
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Method for displaying the process details of selected row.
	 *
	 * @return the process details
	 */
	public void getProcessDetails() {
		Long id = new Long(selectedProcessBean.getIdProcessLoggerPk());
		List<Object[]> listProcParams = null;
		DynaFormRow dynaFormRow = null;
		DynaFormControl dynaFormControl = null;
		DynaFormLabel dynaFormLabel = null;
		ParametersBean param = null;
		try {
			model = new DynaFormModel();
			listProcParams = monitoringProcessesServiceFacade
					.getProcessParameters(id);
			// Iterating the listProcParams for getting the key and values of
			// the selected process parameters
			for (Object[] obj : listProcParams) {
				String value = "";
				if(obj[1] != null){
					value = obj[1].toString();
				}
				param = new ParametersBean(value);
				dynaFormRow = model.createRegularRow();
				dynaFormLabel = dynaFormRow.addLabel(obj[0].toString(), 1, 1);
				dynaFormControl = dynaFormRow.addControl(param, "input", 1, 1);
				dynaFormLabel.setForControl(dynaFormControl);
			}
			// Showing the dialogue box parameters
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Search executable processes.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchExecutableProcesses() throws ServiceException{
		hideDialogs();
		List<BusinessProcessTO> processDetails = businessProcessesServiceFacade.getExecutableProcesses(monitorExecuteProcessFilter);
		executableProcessesDataModel = new GenericDataModel<BusinessProcessTO>();
		executableProcessesDataModel.setWrappedData(processDetails);
	}
	
	/**
	 * Execute process.
	 */
	@LoggerAuditWeb
	public void executeProcess(){
			hideDialogs();
		if(Validations.validateIsNull(selectedProcesses) || selectedProcesses.length == 0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return;
		}
		
		try {
			businessProcessesServiceFacade.executeRemoteBatchProcesses(selectedProcesses);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		selectedProcesses = null;
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.MONITOR_SYSTEM_PROCESSES_EXECUTED_SUCCESS));
		JSFUtilities.showComponent("cnfEndTransactionSamePage");
	}
	
	/**
	 * Stop process.
	 */
	@LoggerAuditWeb
	public void stopProcess(){
		hideDialogs();
		if(Validations.validateIsNull(selectedProcesses) || selectedProcesses.length == 0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return;
		}
		try {
			businessProcessesServiceFacade.stopProcesses(selectedProcesses);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		selectedProcesses = null;
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.MONITOR_SYSTEM_PROCESSES_STOPPED_SUCCESS));
		JSFUtilities.showComponent("cnfEndTransactionSamePage");
		
	}
	
	/**
	 * Clean execute processes.
	 */
	public void cleanExecuteProcesses() {
		
		JSFUtilities.setValidViewComponentAndChildrens(":frmExecuteProcesses");
		monitorExecuteProcessFilter = new MonitorSystemProcessFilter();
		executableProcessesDataModel = null;
		selectedProcesses = null;
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * To get the List MacroProcesses.
	 */
	public void loadMacroProcesses() {
		try{
			monitorExecuteProcessFilter.setIdMacroProcess(null);
			monitorExecuteProcessFilter.setIdProcessLevelOne(null);
			monitorExecuteProcessFilter.setIdProcessLevelTwo(null);
			
			macroProcessList.clear();
			processLevelOneList.clear();
			processLevelTwoList.clear();
			
			List<Object[]> listObjArr = monitoringProcessesServiceFacade.getMacroProcessListbyidSystem(monitorExecuteProcessFilter.getIdSystem());
	
			ComboBoxValueBean comboBoxValueBean = null;
			//convert list<Object[]> to list<ComboBoxValueBean>  
			for (Object[] objects : listObjArr) {
				comboBoxValueBean = new ComboBoxValueBean();
				comboBoxValueBean.setItemLabel(objects[1].toString());
				comboBoxValueBean.setItemValue(objects[0].toString());
				macroProcessList.add(comboBoxValueBean);
			}
		}
		catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To get the ListProcessLevelOne.
	 */
	public void loadProcessLevelOne() {
		try {
			processLevelOneList.clear();
			processLevelTwoList.clear();
			monitorExecuteProcessFilter.setIdProcessLevelOne(null);
			monitorExecuteProcessFilter.setIdProcessLevelTwo(null);
			
			List<Object[]> listObjArr = new ArrayList<Object[]>();
			List<Long> lstProcesses = new ArrayList<Long>();
			List<Long> lstRefProcesses = new ArrayList<Long>();
			ComboBoxValueBean comboBoxValueBean = null;
			if (Validations.validateIsNotNullAndPositive(monitorExecuteProcessFilter.getIdMacroProcess())) {
				lstProcesses.add(monitorExecuteProcessFilter.getIdMacroProcess());
				listObjArr = monitoringProcessesServiceFacade.getProcessLevels(lstProcesses);
			}
			//convert list<Object[]> to list<ComboBoxValueBean> 
			for (Object[] objects : listObjArr) {
				comboBoxValueBean = new ComboBoxValueBean();
				comboBoxValueBean.setItemLabel(objects[1].toString());
				comboBoxValueBean.setItemValue(objects[0].toString());
				lstRefProcesses.add((new Long(objects[0].toString())));
				processLevelOneList.add(comboBoxValueBean);
			}
			monitorExecuteProcessFilter.setIdReferenceProcesses(lstRefProcesses);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * To get the List ProcessLevelTwo.
	 */
	public void loadProcessLevelTwo() {
		try {
			processLevelTwoList.clear();
			monitorExecuteProcessFilter.setIdProcessLevelTwo(null);
			
			List<Object[]> listObjArr = new ArrayList<Object[]>();
			List<Long> lstProcesses = new ArrayList<Long>();
			List<Long> lstRefProcesses = new ArrayList<Long>();
			ComboBoxValueBean comboBoxValueBean = null;
			
			if (Validations.validateIsNotNullAndPositive(monitorExecuteProcessFilter.getIdProcessLevelOne())) {
				lstProcesses.add(monitorExecuteProcessFilter.getIdProcessLevelOne());
				listObjArr = monitoringProcessesServiceFacade.getProcessLevels(lstProcesses);
			} 
//			else {
//				//To add all the list of processlevelone to get processleveltwo
//				for (ComboBoxValueBean macro : lstProcessLevelOne) {
//					lstProcesses.add((new Long(macro.getItemValue().toString())));
//				}
//				if(lstProcesses.size()!=0){
//					listObjArr = monitoringProcessesServiceFacade.getProcessLevels(lstProcesses);
//				}
//			}
			//convert list<Object[]> to list<ComboBoxValueBean> 
			for (Object[] objects : listObjArr) {
				comboBoxValueBean = new ComboBoxValueBean();
				comboBoxValueBean.setItemLabel(objects[1].toString());
				comboBoxValueBean.setItemValue(objects[0].toString());
				lstRefProcesses.add((new Long(objects[0].toString())));
				processLevelTwoList.add(comboBoxValueBean);
			}
			monitorExecuteProcessFilter.setIdReferenceProcesses(lstRefProcesses);
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}


	/**
	 * Gets the lst systems.
	 *
	 * @return the lstSystems
	 */
	public List<ComboBoxValueBean> getLstSystems() {
		return lstSystems;
	}

	/**
	 * Sets the lst systems.
	 *
	 * @param lstSystems            the lstSystems to set
	 */
	public void setLstSystems(List<ComboBoxValueBean> lstSystems) {
		this.lstSystems = lstSystems;
	}

	/**
	 * Gets the lst macro process.
	 *
	 * @return the lstMacroProcess
	 */
	public List<ComboBoxValueBean> getLstMacroProcess() {
		return lstMacroProcess;
	}

	/**
	 * Sets the lst macro process.
	 *
	 * @param lstMacroProcess            the lstMacroProcess to set
	 */
	public void setLstMacroProcess(List<ComboBoxValueBean> lstMacroProcess) {
		this.lstMacroProcess = lstMacroProcess;
	}

	/**
	 * Gets the lst process level one.
	 *
	 * @return the lstProcessLevelOne
	 */
	public List<ComboBoxValueBean> getLstProcessLevelOne() {
		return lstProcessLevelOne;
	}

	/**
	 * Sets the lst process level one.
	 *
	 * @param lstProcessLevelOne            the lstProcessLevelOne to set
	 */
	public void setLstProcessLevelOne(List<ComboBoxValueBean> lstProcessLevelOne) {
		this.lstProcessLevelOne = lstProcessLevelOne;
	}

	/**
	 * Gets the lst process level two.
	 *
	 * @return the lstProcessLevelTwo
	 */
	public List<ComboBoxValueBean> getLstProcessLevelTwo() {
		return lstProcessLevelTwo;
	}

	/**
	 * Sets the lst process level two.
	 *
	 * @param lstProcessLevelTwo            the lstProcessLevelTwo to set
	 */
	public void setLstProcessLevelTwo(List<ComboBoxValueBean> lstProcessLevelTwo) {
		this.lstProcessLevelTwo = lstProcessLevelTwo;
	}

	/**
	 * Gets the monitor system process filter.
	 *
	 * @return the monitorSystemProcessFilter
	 */
	public MonitorSystemProcessFilter getMonitorSystemProcessFilter() {
		return monitorSystemProcessFilter;
	}

	/**
	 * Sets the monitor system process filter.
	 *
	 * @param monitorSystemProcessFilter            the monitorSystemProcessFilter to set
	 */
	public void setMonitorSystemProcessFilter(
			MonitorSystemProcessFilter monitorSystemProcessFilter) {
		this.monitorSystemProcessFilter = monitorSystemProcessFilter;
	}

	/**
	 * Gets the systemprocess bean.
	 *
	 * @return the systemprocessBean
	 */
	public ManageSystemProcessBean getSystemprocessBean() {
		return systemprocessBean;
	}

	/**
	 * Sets the systemprocess bean.
	 *
	 * @param systemprocessBean            the systemprocessBean to set
	 */
	public void setSystemprocessBean(ManageSystemProcessBean systemprocessBean) {
		this.systemprocessBean = systemprocessBean;
	}

	/**
	 * Gets the monitoring system process data model.
	 *
	 * @return the monitoringSystemProcessDataModel
	 */
	public MonitoringSystemProcessDataModel getMonitoringSystemProcessDataModel() {
		return monitoringSystemProcessDataModel;
	}

	/**
	 * Sets the monitoring system process data model.
	 *
	 * @param monitoringSystemProcessDataModel            the monitoringSystemProcessDataModel to set
	 */
	public void setMonitoringSystemProcessDataModel(
			MonitoringSystemProcessDataModel monitoringSystemProcessDataModel) {
		this.monitoringSystemProcessDataModel = monitoringSystemProcessDataModel;
	}

	/**
	 * Gets the selected process bean.
	 *
	 * @return the selectedProcessBean
	 */
	public ManageSystemProcessBean getSelectedProcessBean() {
		return selectedProcessBean;
	}

	/**
	 * Sets the selected process bean.
	 *
	 * @param selectedProcessBean            the selectedProcessBean to set
	 */
	public void setSelectedProcessBean(
			ManageSystemProcessBean selectedProcessBean) {
		this.selectedProcessBean = selectedProcessBean;
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public DynaFormModel getModel() {
		return model;
	}

	/**
	 * Sets the model.
	 *
	 * @param model            the model to set
	 */
	public void setModel(DynaFormModel model) {
		this.model = model;
	}
	
	/**
	 * Gets the executable processes data model.
	 *
	 * @return the executable processes data model
	 */
	public GenericDataModel<BusinessProcessTO> getExecutableProcessesDataModel() {
		return executableProcessesDataModel;
	}

	/**
	 * Sets the executable processes data model.
	 *
	 * @param executableProcessesDataModel the new executable processes data model
	 */
	public void setExecutableProcessesDataModel(
			GenericDataModel<BusinessProcessTO> executableProcessesDataModel) {
		this.executableProcessesDataModel = executableProcessesDataModel;
	}

	/**
	 * Gets the selected processes.
	 *
	 * @return the selected processes
	 */
	public BusinessProcessTO[] getSelectedProcesses() {
		return selectedProcesses;
	}

	/**
	 * Sets the selected processes.
	 *
	 * @param selectedProcesses the new selected processes
	 */
	public void setSelectedProcesses(BusinessProcessTO[] selectedProcesses) {
		this.selectedProcesses = selectedProcesses;
	}

	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}	

	/**
	 * Gets the type process execution.
	 *
	 * @return the type process execution
	 */
	public List<ParameterTable> getTypeProcessExecution() {
		return typeProcessExecution;
	}

	/**
	 * Sets the type process execution.
	 *
	 * @param typeProcessExecution the new type process execution
	 */
	public void setTypeProcessExecution(List<ParameterTable> typeProcessExecution) {
		this.typeProcessExecution = typeProcessExecution;
	}

	/**
	 * Gets the monitor execute process filter.
	 *
	 * @return the monitorExecuteProcessFilter
	 */
	public MonitorSystemProcessFilter getMonitorExecuteProcessFilter() {
		return monitorExecuteProcessFilter;
	}

	/**
	 * Sets the monitor execute process filter.
	 *
	 * @param monitorExecuteProcessFilter the monitorExecuteProcessFilter to set
	 */
	public void setMonitorExecuteProcessFilter(
			MonitorSystemProcessFilter monitorExecuteProcessFilter) {
		this.monitorExecuteProcessFilter = monitorExecuteProcessFilter;
	}

	/**
	 * Gets the macro process list.
	 *
	 * @return the macroProcessList
	 */
	public List<ComboBoxValueBean> getMacroProcessList() {
		return macroProcessList;
	}

	/**
	 * Sets the macro process list.
	 *
	 * @param macroProcessList the macroProcessList to set
	 */
	public void setMacroProcessList(List<ComboBoxValueBean> macroProcessList) {
		this.macroProcessList = macroProcessList;
	}

	/**
	 * Gets the process level one list.
	 *
	 * @return the processLevelOneList
	 */
	public List<ComboBoxValueBean> getProcessLevelOneList() {
		return processLevelOneList;
	}

	/**
	 * Sets the process level one list.
	 *
	 * @param processLevelOneList the processLevelOneList to set
	 */
	public void setProcessLevelOneList(List<ComboBoxValueBean> processLevelOneList) {
		this.processLevelOneList = processLevelOneList;
	}

	/**
	 * Gets the process level two list.
	 *
	 * @return the processLevelTwoList
	 */
	public List<ComboBoxValueBean> getProcessLevelTwoList() {
		return processLevelTwoList;
	}

	/**
	 * Sets the process level two list.
	 *
	 * @param processLevelTwoList the processLevelTwoList to set
	 */
	public void setProcessLevelTwoList(List<ComboBoxValueBean> processLevelTwoList) {
		this.processLevelTwoList = processLevelTwoList;
	}

	

	/**
	 * Gets the list institutions.
	 *
	 * @return the list institutions
	 */
	public List<SelectItem> getListInstitutions() {
		return listInstitutions;
	}

	/**
	 * Sets the list institutions.
	 *
	 * @param listInstitutions the new list institutions
	 */
	public void setListInstitutions(List<SelectItem> listInstitutions) {
		this.listInstitutions = listInstitutions;
	}

	/**
	 * Gets the users accounts.
	 *
	 * @return the users accounts
	 */
	public List<UserAccountSession> getUsersAccounts() {
		return usersAccounts;
	}

	/**
	 * Sets the users accounts.
	 *
	 * @param usersAccounts the new users accounts
	 */
	public void setUsersAccounts(List<UserAccountSession> usersAccounts) {
		this.usersAccounts = usersAccounts;
	}

	/**
	 * Gets the list institutions types.
	 *
	 * @return the list institutions types
	 */
	public List<SelectItem> getListInstitutionsTypes() {
		return listInstitutionsTypes;
	}

	/**
	 * Sets the list institutions types.
	 *
	 * @param listInstitutionsTypes the new list institutions types
	 */
	public void setListInstitutionsTypes(List<SelectItem> listInstitutionsTypes) {
		this.listInstitutionsTypes = listInstitutionsTypes;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the institution selected.
	 *
	 * @return the institution selected
	 */
	public Object getInstitutionSelected() {
		return institutionSelected;
	}

	/**
	 * Sets the institution selected.
	 *
	 * @param institutionSelected the new institution selected
	 */
	public void setInstitutionSelected(Object institutionSelected) {
		this.institutionSelected = institutionSelected;
	}

	public Boolean requestFromSecurity() {
		return Validations.validateIsNotNull(this.userInfo) 
				&& Validations.validateIsNotNull(this.userInfo.getCurrentSystem())
				&& this.userInfo.getCurrentSystem().equals(SystemType.SECURITY.getValue());
	}
}
