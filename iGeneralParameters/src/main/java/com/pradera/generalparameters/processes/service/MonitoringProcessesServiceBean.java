package com.pradera.generalparameters.processes.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class MonitoringProcessesServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/02/2014
 */
@Stateless
public class MonitoringProcessesServiceBean extends CrudDaoServiceBean {
	
	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;

	/**
	 * To get the list of macroProcesses.
	 *
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getMacroProcessListServiceBean()throws ServiceException  {
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql
				.append(" Select BP.idBusinessProcessPk, BP.processName  ");
		stringBuilderSql.append(" From BusinessProcess BP");
		stringBuilderSql
				.append(" Where BP.processLevel = 0 ");
		Query queryString = em.createQuery(stringBuilderSql.toString());
		List<Object[]> listMacroProcess = (List<Object[]>) queryString
				.getResultList();
		return listMacroProcess;
	}
	
	/**
	 * To get the list of macroProcesses by idSsytem.
	 *
	 * @param idSystem the id system
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getMacroProcessListbyidSystemServiceBean(Integer idSystem)throws ServiceException  {
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql
				.append(" Select BP.idBusinessProcessPk, BP.processName  ");
		stringBuilderSql.append(" From BusinessProcess BP");
		stringBuilderSql
				.append(" Where BP.processLevel = 0 and idSystem = :idSystem");
		Query queryString = em.createQuery(stringBuilderSql.toString());
		queryString.setParameter("idSystem", idSystem);
		List<Object[]> listMacroProcess = (List<Object[]>) queryString
				.getResultList();
		return listMacroProcess;
	}

	/**
	 * To get the list of processlevels.
	 *
	 * @param idBusinessProcess the id business process
	 * @return the values of processlevels
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getProcessLevelsServiceBean(
			List<Long> idBusinessProcess)throws ServiceException  {
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql
				.append(" Select BP.idBusinessProcessPk, BP.processName  ");
		stringBuilderSql.append(" From BusinessProcess BP");
		stringBuilderSql
				.append(" Where BP.businessProcess.idBusinessProcessPk in (:lstProcesses)");
		Query queryString = em.createQuery(stringBuilderSql.toString());
		queryString.setParameter("lstProcesses", idBusinessProcess);
		List<Object[]> listProcesses = (List<Object[]>) queryString
				.getResultList();
		return listProcesses;
	}

	/**
	 * To get the ProcessDetails.
	 *
	 * @param monitorSystemProcessFilter the monitor system process filter
	 * @return the values of processDetails
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getProcessDetailServiceBean(
			MonitorSystemProcessFilter monitorSystemProcessFilter) throws ServiceException  {
		
		List<Object[]> resuList = new ArrayList<Object[]>();
		try{
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("Select PL.idProcessLoggerPk , PL.processDate , PL.loggerState , ");
			stringBuilderSql.append("  PL.finishingDate , BP.processType , BP.mnemonic , BP.processName , BP.businessProcess.idBusinessProcessPk , BP1.processName , BP2.processName");
			stringBuilderSql.append(" ,PL.errorDetail, PL.idUserAccount ");
			stringBuilderSql.append(" From BusinessProcess BP");
			stringBuilderSql.append(" Inner Join BP.businessProcess BP2 ");
			stringBuilderSql.append(" Inner Join BP2.businessProcess BP1 ");
			stringBuilderSql.append(" Inner Join BP1.businessProcess BP0 ");
			stringBuilderSql.append(" Inner Join BP.processLoggers PL");
			
			stringBuilderSql.append(" where 1=1 ");
			
			if( Validations.validateIsNotNull(monitorSystemProcessFilter.getIdUser())) {
				stringBuilderSql.append(" and PL.idUserAccount = :idUserAccount");
			}
			
			if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelTwo())){
				stringBuilderSql.append(" and BP2.idBusinessProcessPk in (:idProcessLevelTwo) ");			
			}
			if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelOne())){
				stringBuilderSql.append(" and BP1.idBusinessProcessPk in (:idProcessLevelOne)  ");			
			}
			if(monitorSystemProcessFilter.getIdMacroProcess() != null) {
				stringBuilderSql.append(" and BP0.idBusinessProcessPk = :idMacroProcess  ");
			}
			if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdBusinessProcessPk())){
				stringBuilderSql.append(" and BP.idBusinessProcessPk = :idBusinessProcessPk ");			
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getMnemonicProcess())){
				stringBuilderSql.append(" And BP.mnemonic like :procMnemonic ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getSelectedExecutionType())&&monitorSystemProcessFilter.getSelectedExecutionType()!=0){
				stringBuilderSql.append(" And BP.processType = :processtype");
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getProcessName())){
				stringBuilderSql.append(" And BP.processName like :procName");
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getInitialDate())){
				stringBuilderSql.append(" And PL.processDate >= :initialDate");
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getFinalDate())){
				stringBuilderSql.append(" And PL.processDate <= :finalDate");
			}
			if(monitorSystemProcessFilter.getState()!=null){
				stringBuilderSql.append(" and PL.loggerState = :state ");
			}
			if(monitorSystemProcessFilter.getIdProcessLogger()!= null){
				stringBuilderSql.append(" and PL.idProcessLoggerPk = :processLoggerId ");
			}
			
			stringBuilderSql.append(" order by PL.finishingDate desc ");
			Query query = em.createQuery(stringBuilderSql.toString());
			
			if( Validations.validateIsNotNull(monitorSystemProcessFilter.getIdUser())) {
				query.setParameter("idUserAccount",monitorSystemProcessFilter.getIdUser());
			}			
			if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelTwo())){
				query.setParameter("idProcessLevelTwo",monitorSystemProcessFilter.getIdProcessLevelTwo());
			}
			if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelOne())){
				query.setParameter("idProcessLevelOne",monitorSystemProcessFilter.getIdProcessLevelOne());		
			}
			if(monitorSystemProcessFilter.getIdMacroProcess() != null) {
				query.setParameter("idMacroProcess", monitorSystemProcessFilter.getIdMacroProcess());
			}
			if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdBusinessProcessPk())){
				query.setParameter("idBusinessProcessPk",monitorSystemProcessFilter.getIdBusinessProcessPk());		
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getMnemonicProcess())){
				if(monitorSystemProcessFilter.getProcessName().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
					query.setParameter("procMnemonic", 
							monitorSystemProcessFilter.getMnemonicProcess().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
				} else {
					query.setParameter("procMnemonic", monitorSystemProcessFilter.getMnemonicProcess());
				}
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getProcessName())){
				if(monitorSystemProcessFilter.getProcessName().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
					query.setParameter("procName", 
							monitorSystemProcessFilter.getProcessName().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
				} else {
					query.setParameter("procName", monitorSystemProcessFilter.getProcessName());
				}
			}
			if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getSelectedExecutionType())){
				query.setParameter("processtype",monitorSystemProcessFilter.getSelectedExecutionType());
			}
			if(Validations.validateIsNotNull(monitorSystemProcessFilter.getInitialDate())){
				query.setParameter("initialDate", monitorSystemProcessFilter.getInitialDate());
			}
			if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getFinalDate())){
				Calendar cal = Calendar.getInstance();  
				cal.setTime(monitorSystemProcessFilter.getFinalDate());  
				cal.add(Calendar.DAY_OF_YEAR, 1); 
				query.setParameter("finalDate", cal.getTime());
			}
			if(monitorSystemProcessFilter.getState()!=null){
				query.setParameter("state", monitorSystemProcessFilter.getState());
			}
			if(monitorSystemProcessFilter.getIdProcessLogger()!= null){
				query.setParameter("processLoggerId", monitorSystemProcessFilter.getIdProcessLogger());
			}
			resuList  =  query.getResultList();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return resuList;
	}
	
	/**
	 * The method for getting the Audit process details from the data base.
	 *
	 * @param integers the integers
	 * @param startDate starting date
	 * @param endDate starting date
	 * @return the List of Process beans
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getAuditProcessesDetailsServiceBean(
			List<Integer> integers, Date startDate, Date endDate)
			throws ServiceException {
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql
				.append(" Select BP.processType, PL.idProcessLoggerPk,BP.beanName, ");
		stringBuilderSql.append(" BP.description,BP.processName,");
		stringBuilderSql
				.append("PL.processDate,PL.idUserAccount, PL.lastModifyIp,BP.processState ");
		stringBuilderSql.append(" From BusinessProcess BP");
		stringBuilderSql.append(" Join BP.processLoggers PL");
		stringBuilderSql
				.append(" Where BP.idBusinessProcessPk = PL.businessProcess.idBusinessProcessPk ");
		stringBuilderSql
				.append(" And BP.idOptionPrivilege  in (:lstOptionPrivilege)");
		if (startDate != null & endDate != null) {
			stringBuilderSql.append(" And  PL.processDate >= :startDate");
			stringBuilderSql.append(" And  PL.finishingDate <= :endDate");
		}
		Query queryString = em.createQuery(stringBuilderSql.toString());
		queryString.setParameter("lstOptionPrivilege", integers);
		if (startDate != null & endDate != null) {
			queryString.setParameter("startDate", startDate);
			queryString.setParameter("endDate", endDate);
		}
		List<Object[]> listOptions = (List<Object[]>) queryString
				.getResultList();
		return listOptions;

	}

	/**
	 * Method for getting the parameters of selected process.
	 *
	 * @param id Integer
	 * @return the values of parameters
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getProcessParametersServiceBean(Long id)throws ServiceException  {
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql
				.append(" Select PD.parameterName, pd.parameterValue  ");
		stringBuilderSql.append(" From ProcessLogger PL");
		stringBuilderSql.append(" Join PL.processLoggerDetails PD");
		stringBuilderSql
				.append(" Where PL.idProcessLoggerPk = PD.processLogger.idProcessLoggerPk ");
		stringBuilderSql
				.append(" and PD.processLogger.idProcessLoggerPk = :id");
		Query queryString = em.createQuery(stringBuilderSql.toString());
		queryString.setParameter("id", id);
		List<Object[]> listOptions = (List<Object[]>) queryString
				.getResultList();
		return listOptions;
	}

	/**
	 * Gets the business process details service bean.
	 *
	 * @param refProcess the ref process
	 * @return the values of businessProcessDetails
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getBusinessProcessDetailsServiceBean(Long refProcess)throws ServiceException  {
		 StringBuilder builder=new StringBuilder();
		  builder.append(" select b.processName,b0.processName,b1.processName,b2.processName ");
		  builder.append(" from BusinessProcess b,  BusinessProcess b2, BusinessProcess b1, BusinessProcess b0 ");
		  builder.append(" where b.businessProcess.idBusinessProcessPk in (:listRefProcess) ");
		  builder.append(" and b2.idBusinessProcessPk=b.businessProcess.idBusinessProcessPk ");
		  builder.append(" and b1.idBusinessProcessPk=b2.businessProcess.idBusinessProcessPk ");
		  builder.append(" and b0.idBusinessProcessPk=b1.businessProcess.idBusinessProcessPk ");
		  Query queryString = em.createQuery(builder.toString());
		  queryString.setParameter("listRefProcess", refProcess);
		  List<Object[]> listBussProcess = (List<Object[]>) queryString.getResultList();
		  return listBussProcess;
	}
	
	/**
	 * Validate process has parameters.
	 *
	 * @param idProcessLogger the id process logger
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateProcessHasParameters(Long idProcessLogger) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select Count(PLD) From ProcessLoggerDetail PLD ");
		sbQuery.append(" Where PLD.processLogger.idProcessLoggerPk = :idProcessPkPrm ");

		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("idProcessPkPrm", idProcessLogger);
		
		Integer countResult = Integer.valueOf(query.getSingleResult().toString());
		
		return countResult > 0;	
	}
	
	/**
	 * Gets the system list.
	 *
	 * @return the system list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getSystemList() throws ServiceException {
		List<Object[]> systems = clientRestService.getSystemList();
		for(Object[] row: systems){
			row[0]= ((Double)row[0]).intValue();
		}
		return systems;
	}
	
	/**
	 * Removes the user destinations.
	 *
	 * @param idDestinationPk the id destination Pk
	 */
	public void removeUserDestinations(Long idDestinationPk) {
		StringBuilder sbQuery=new StringBuilder();	
		sbQuery.append(" Delete DestinationNotification dest  ");
		sbQuery.append(" where dest.processNotification.idProcessNotificationPk = :parId");			
		Query query = em.createQuery(sbQuery.toString());	
		query.setParameter("parId", idDestinationPk);			
		query.executeUpdate();
	}
	
	/**
	 * Update notification logger.
	 *
	 * @param idDestinationPk the id destination pk
	 * @param loggerUser the logger user
	 */
	public void updateNotificationLogger(List<Long> idDestinationPk, LoggerUser loggerUser) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update NotificationLogger notification set  "+				
				" notification.destinationNotification.idDestinationPk = :parNull , " +							
				" notification.lastModifyUser = :parLastModifyUser , notification.lastModifyDate = :parLastModifyDate , " +
				" notification.lastModifyIp = :parLastModifyIp , notification.lastModifyApp = :parLastModifyApp " +
				" where notification.destinationNotification.idDestinationPk in (:parId) ");
		parameters.put("parId", idDestinationPk);		
		parameters.put("parNull", null);				
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * Ge holder account movement for marke fact.
	 *
	 * @param idDestinationPk the id destination pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getDestinationNotificationNew(Long idDestinationPk) throws ServiceException{
		List<Long> lstIdsPks = null;		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" SELECT des.idDestinationPk FROM DestinationNotification des				");				
		jpqlQuery.append(" where des.processNotification.idProcessNotificationPk = :parId			");		
		Query query = em.createQuery(jpqlQuery.toString());
		query.setParameter("parId", idDestinationPk);		
		try {			
			List<Object> resultList = query.getResultList();	
			lstIdsPks = new ArrayList<Long>();
			int i = 0;
			while (i < resultList.size()) {
				Object object = resultList.get(i);
				lstIdsPks.add((Long) object);
				i++;
			}
		} catch(NoResultException ex) {
			return null;
		}		
		return lstIdsPks;
	}

}
