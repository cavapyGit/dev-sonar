package com.pradera.generalparameters.processes.view;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class BeanType
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */

public class BeanTypeBusiness implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id type pk. */
	private Long idTypePK;
	
	/** The description. */
	private String description;
	
	
	/**
	 * Gets the id type pk.
	 *
	 * @return the idTypePK
	 */
	public Long getIdTypePK() {
		return idTypePK;
	}
	
	/**
	 * Sets the id type pk.
	 *
	 * @param idTypePK the idTypePK to set
	 */
	public void setIdTypePK(Long idTypePK) {
		this.idTypePK = idTypePK;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Instantiates a new bean type business.
	 *
	 * @param idTypePK the id type pk
	 * @param description the description
	 */
	public BeanTypeBusiness(Long idTypePK, String description) {
		super();
		this.idTypePK = idTypePK;
		this.description = description;
	}
	
	/**
	 * Constructor.
	 */
	public BeanTypeBusiness() {
		super();
	}
}
	