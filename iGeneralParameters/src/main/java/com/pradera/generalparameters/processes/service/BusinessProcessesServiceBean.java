package com.pradera.generalparameters.processes.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.generalparameters.processes.view.User;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.notification.NotificationConfiguration;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.process.ProcessSchedule;
import com.pradera.model.process.type.ProcessType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class BusinessProcessesServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BusinessProcessesServiceBean extends
		CrudDaoServiceBean {
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	
	/**
	 * Method for retrieving the BusinessProcess details from the database.
	 *
	 * @param microProcess the micro process
	 * @param level1 the level1
	 * @param level2 the level2
	 * @param process the process
	 * @param type Integer
	 * @param negocio String
	 * @param nemonico String
	 * @param indNotification the ind notification
	 * @return the list of BusinessProcess Entities
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> listBusinessProcessService(Long microProcess, Long level1, Long level2, Long process,
			Integer type, String negocio, String nemonico,Integer indNotification)
			throws ServiceException {
		StringBuilder builder = new StringBuilder();
		builder.append(" select b.idBusinessProcessPk,b1.processName,b2.processName,b0.processName,b.processType, ");
		builder.append(" b.beanName,b.description, b.processName , b.mnemonic,b.indNotices ");
		builder.append(" from BusinessProcess b,  BusinessProcess b2, BusinessProcess b1, BusinessProcess b0  where ");
		builder.append(" b2.idBusinessProcessPk=b.businessProcess.idBusinessProcessPk ");
		builder.append(" and b1.idBusinessProcessPk=b2.businessProcess.idBusinessProcessPk ");
		builder.append(" and b0.idBusinessProcessPk=b1.businessProcess.idBusinessProcessPk ");
		if (process != null && process > 0) {
			builder.append(" and b.idBusinessProcessPk = :process ");
		}
		if (microProcess != null && microProcess > 0) {
			builder.append(" and b0.idBusinessProcessPk = :microprocess ");
		}
		if (level1 != null && level1 > 0) {
			builder.append(" and b1.idBusinessProcessPk = :level1process ");
		}
		if (level2 != null && level2 > 0) {
			builder.append(" and b2.idBusinessProcessPk = :level2process ");
		}
		if (Validations.validateIsNotNull(type) && type > 0) {
			builder.append(" and b.processType=:processType ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(negocio)) {
			builder.append(" and b.processName like :processNeg ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(nemonico)) {
			builder.append(" and b.mnemonic like  :mnemonic ");
		}
		if(indNotification != null){
			builder.append(" and b.indNotices =:flagNotification ");
		}
		builder.append(" order by b.idBusinessProcessPk");
		Query queryString = em.createQuery(builder.toString());
		if (process != null && process > 0) {
			queryString.setParameter("process", process);
		}
		if (microProcess != null && microProcess > 0) {
			queryString.setParameter("microprocess", microProcess);
		}
		if (level1 != null && level1 > 0) {
			queryString.setParameter("level1process", level1);
		}
		if (level2 != null && level2 > 0) {
			queryString.setParameter("level2process", level2);
		}
		if (Validations.validateIsNotNullAndNotEmpty(type) && type != 0) {
			queryString.setParameter("processType", type);
		}
		if (Validations.validateIsNotNullAndNotEmpty(nemonico)) {
			queryString.setParameter("mnemonic", "%"+nemonico+"%");	
		}
		if (Validations.validateIsNotNullAndNotEmpty(negocio)) {
			queryString.setParameter("processNeg", "%"+negocio+"%");	
		}
		if(indNotification != null){
			queryString.setParameter("flagNotification", indNotification);
		}
		List<Object[]> listBussProcess = (List<Object[]>) queryString
				.getResultList();
		return listBussProcess;

	}
	

	/**
	 * Method for retrieving the BusinessProcess details from the database.
	 *
	 * @param monitorSystemProcessFilter the monitor system process filter
	 * @return  the list of BusinessProcess Entities
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> listBusinessProcessServiceBean(MonitorSystemProcessFilter monitorSystemProcessFilter) throws ServiceException{
		
		StringBuilder builder=new StringBuilder();
		builder.append(" select b.idBusinessProcessPk, "); 	//0
		builder.append(" b.processName, "); 				//1
		
		builder.append(" b0.idBusinessProcessPk, "); 		//2
		builder.append(" b0.processName, ");				//3
		
		builder.append(" b1.idBusinessProcessPk, ");		//4
		builder.append(" b1.processName, ");				//5
		
		builder.append(" b2.idBusinessProcessPk, ");		//6
		builder.append(" b2.processName, ");				//7
		
		builder.append(" b.processType, ");					//8
		builder.append(" b.mnemonic, ");					//9
		builder.append(" b.beanName, ");					//10
		builder.append(" b.description, ");					//11
		builder.append(" b.moduleName ");					//12
		
		builder.append(" from BusinessProcess b,  BusinessProcess b2, BusinessProcess b1, BusinessProcess b0 ");
		builder.append(" where 1 = 1 ");
		builder.append(" and b.processType = " + ProcessType.BATCH.getCode());
		builder.append(" and b2.idBusinessProcessPk=b.businessProcess.idBusinessProcessPk ");
		builder.append(" and b1.idBusinessProcessPk=b2.businessProcess.idBusinessProcessPk ");
		builder.append(" and b0.idBusinessProcessPk=b1.businessProcess.idBusinessProcessPk ");

		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdMacroProcess())){
			builder.append(" and b0.idBusinessProcessPk = :macroProcess ");
		}
		
		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelOne())){
			builder.append(" and b1.idBusinessProcessPk = :processLevel1 ");
		}
		
		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelTwo())){
			builder.append(" and b2.idBusinessProcessPk = :processLevel2 ");
		}
		
		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdBusinessProcessPk())){
			builder.append(" and b.idBusinessProcessPk = :idBusinessProcessPk ");
		}
		
//		if(Validations.validateIsNotNullAndPositive(businessProcessTO.getProcessType())){
//			builder.append(" and b.processType = :processType ");
//		}
		
		if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getProcessName())){
			builder.append(" and b.processName like :processName ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getMnemonicProcess())){
			builder.append(" and b.mnemonic=:mnemonic ");
		}
		
		Query queryString = em.createQuery(builder.toString());
		
		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdMacroProcess())){
			queryString.setParameter("macroProcess", monitorSystemProcessFilter.getIdMacroProcess());
		}
		
		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelOne())){
			queryString.setParameter("processLevel1", monitorSystemProcessFilter.getIdProcessLevelOne());
		}
		
		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdProcessLevelTwo())){
			queryString.setParameter("processLevel2", monitorSystemProcessFilter.getIdProcessLevelTwo());
		}
		
		if(Validations.validateIsNotNullAndPositive(monitorSystemProcessFilter.getIdBusinessProcessPk())){
			queryString.setParameter("idBusinessProcessPk", monitorSystemProcessFilter.getIdBusinessProcessPk());
		}
		
//		if(Validations.validateIsNotNullAndPositive(businessProcessTO.getProcessType())){
//			builder.append(" and b.processType = :processType ");
//		}
		
		if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getProcessName())){
			if(monitorSystemProcessFilter.getProcessName().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				queryString.setParameter("processName", 
						monitorSystemProcessFilter.getProcessName().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
			} else {
				queryString.setParameter("processName", monitorSystemProcessFilter.getProcessName());
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(monitorSystemProcessFilter.getMnemonicProcess())){
			queryString.setParameter("mnemonic", monitorSystemProcessFilter.getMnemonicProcess());
		}
		
		@SuppressWarnings("unchecked")
		List<Object[]> listBussProcess = (List<Object[]>) queryString.getResultList();
		return listBussProcess;
	}


	/**
	 * Method for retrieving the schedules for the selected business process id.
	 *
	 * @param id Integer
	 * @return the list of object arrays of shedule objectes
	 * @throws ServiceException the service exception
	 */
	public List<ProcessSchedule> getProcessSchedulesServiceBean(Long id)throws ServiceException  {
		StringBuilder builder = new StringBuilder();
		builder.append("select ps ");
		builder.append("from ProcessSchedule ps join fetch ps.businessProcess bp ");
		builder.append("where bp.processType =:processType ");
		if (Validations.validateIsNotNull(id)) {
			builder.append("and bp.idBusinessProcessPk = :id ");
		}
		TypedQuery<ProcessSchedule> queryString = em.createQuery(builder.toString(),ProcessSchedule.class);
		if (Validations.validateIsNotNull(id)) {
			queryString.setParameter("id", id);
		}
		queryString.setParameter("processType", ProcessType.BATCH.getCode());
		List<ProcessSchedule> listSchedules = queryString.getResultList();
		return listSchedules;

	}
	
	/**
	 * Gets the notification business process.
	 *
	 * @param businessProcess the business process
	 * @return the notification business process
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ProcessNotification> getNotificationBusinessProcess(Long businessProcess) throws ServiceException{
		StringBuilder builder = new StringBuilder(100);
		builder.append("select Distinct(notification) from ProcessNotification notification ");
		builder.append("left join fetch notification.destinationNotifications destination ");
		builder.append("where notification.businessProcess.idBusinessProcessPk = :process ");
		TypedQuery<ProcessNotification> query = em.createQuery(builder.toString(), ProcessNotification.class);
		query.setParameter("process", businessProcess);
		return query.getResultList();
	}

	/**
	 * Method for getting the notifications from the data base.
	 *
	 * @param id Long
	 * @return the list of object arrays of type Notifications
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getNotificationsService(Long id)throws ServiceException  {
		StringBuilder builder = new StringBuilder();
		builder.append("select pn,dn ");
		builder.append("from ProcessNotification pn,");
		builder.append("BusinessProcess bp, DestinationNotification dn ");
		builder.append("where pn.businessProcess.idBusinessProcessPk = bp.idBusinessProcessPk ");
		
		builder.append("and dn.processNotification.idProcessNotificationPk = pn.idProcessNotificationPk");
		
		if (Validations.validateIsNotNull(id)) {
			builder.append("  and pn.businessProcess.idBusinessProcessPk = :id ");
		}
		Query queryString = em.createQuery(builder.toString());
		if (Validations.validateIsNotNull(id)) {
			queryString.setParameter("id", id);
		}
		List<Object[]> listSchedules = (List<Object[]>) queryString
				.getResultList();
		return listSchedules;
	}

	/**
	 * Method for getting the child Notifications.
	 *
	 * @param id Long
	 * @return the list of object arrays of child notifications type
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getChildNotificationsService(Long id)throws ServiceException  {
		StringBuilder builder = new StringBuilder();
		builder.append(" select dn.idUserAccount ,dn.fullName,dn.email, ");
		builder.append(" dn.mobileNumber , dn.indStatus from DestinationNotification dn");
		if (Validations.validateIsNotNull(id)) {
			builder.append("  where dn.processNotification.idProcessNotificationPk = :id ");
		}
		Query queryString = em.createQuery(builder.toString());
		if (Validations.validateIsNotNull(id)) {
			queryString.setParameter("id", id);
		}
		List<Object[]> listSchedules = (List<Object[]>) queryString
				.getResultList();
		return listSchedules;
	}

	/**
	 * Method for saving the Notifications in the database.
	 *
	 * @param notification ProcessNotification
	 * @throws ServiceException the service exception
	 */
	public void insertNotificationSevice(ProcessNotification notification)throws ServiceException  {
		update(notification);
	}

	/**
	 * Method for getting the user details.
	 *
	 * @param user User
	 * @return the list of object arrays of user type
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> showUsersService(User user)throws ServiceException  {
		StringBuilder builder = new StringBuilder();
		builder.append(" select dn.lastModifyUser ,dn.fullName, ");
		if (Validations.validateIsNotNull(user.getIdUserPk())) {
			builder.append(" dn.idUserAccount,");
		}
		if (Validations.validateIsNotNull(user.getUserEmail())) {
			builder.append(" dn.email,");
		}
		if (Validations.validateIsNotNull(user.getUserPhone())) {
			builder.append(" dn.mobileNumber,");
		}
		builder.append(" dn.destinationState ");
		builder.append(" from DestinationNotification dn, ProcessNotification pn");
		builder.append(" where dn.processNotification.idProcessNotificationPk = pn.idProcessNotificationPk and ");
		if (Validations.validateIsNotNull(user.getIdUserPk())) {
			builder.append(" dn.idUserAccount=:idUserAccount ");
		}
		if (Validations.validateIsNotNull(user.getUserEmail())) {
			builder.append(" dn.email = :email");
		}
		if (Validations.validateIsNotNull(user.getUserPhone())) {
			builder.append(" dn.mobileNumber = :mobile");
		}
		Query queryString = em.createQuery(builder.toString());

		if (Validations.validateIsNotNull(user.getIdUserPk())) {
			queryString.setParameter("idUserAccount", user.getIdUserPk());
		}
		if (Validations.validateIsNotNull(user.getUserEmail())) {
			queryString.setParameter("email", user.getUserEmail());
		}
		if (Validations.validateIsNotNull(user.getUserPhone())) {
			queryString.setParameter("mobile", user.getUserPhone());
		}

		List<Object[]> lstSchedule = (List<Object[]>) queryString
				.getResultList();
		return lstSchedule;

	}
	
	/**
	 * Gets the business processes for execution.
	 *
	 * @param filterMap the filter map
	 * @return the business processes for execution
	 */
	public List<Object[]> getBusinessProcessesForExecution(Map<String,Object> filterMap){

		List<Object[]> lstBusinessProcesses = new ArrayList<Object[]>();
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" select distinct ");
			stringBuilderSql.append("  bp.idBusinessProcessPk, "); //0
			stringBuilderSql.append("  bp.mnemonic, "); //1
			stringBuilderSql.append("  bp.processName, "); //2
			stringBuilderSql.append("  pl.idProcessLoggerPk, "); //3
			stringBuilderSql.append("  pl.loggerState, ");  //4
			stringBuilderSql.append("  pl.errorDetail ");  //5
			stringBuilderSql.append(" from BusinessProcess bp left outer join ProcessLogger pl ");
			stringBuilderSql.append("  on bp.idBusinessProcessPk = pl.idProcessLoggerPk ");
			//stringBuilderSql.append("  left outer join ProcessLogger pl ");	
			//stringBuilderSql.append("  on maxView.maxlogpk = pl.idProcessLoggerPk  ");	
			stringBuilderSql.append(" where 1 = 1 ");
			stringBuilderSql.append(" and pl.idProcessLoggerPk in ( ");
			stringBuilderSql.append("  select ");
			//stringBuilderSql.append("    bp2.idBusinessProcessPk as bppk, ");
			stringBuilderSql.append(" 	 max(pl2.idProcessLoggerPk) ");
			stringBuilderSql.append("  from BusinessProcess bp2 inner join ProcessLogger pl2 ");	
			stringBuilderSql.append("    on bp2.idBusinessProcessPk = pl2.businessProcess.idBusinessProcessPk ");	
			stringBuilderSql.append("    where pl2.processDate >= sysdate ");	
			stringBuilderSql.append("    and pl2.processDate <= sysdate ");
			stringBuilderSql.append("    group by bp2.idBusinessProcessPk ");
			stringBuilderSql.append("  )  ");
	  
			if (filterMap.get("idBusinessProcessPk") != null) {
				stringBuilderSql.append(" and bp.idBusinessProcessPk in :idBusinessProcessPk ");
			}
			if (filterMap.get("idReferenceProcessFk") != null) {
				stringBuilderSql.append(" and bp.businessProcess.idBusinessProcessPk in :idReferenceProcessFk ");
			}
			
			Query query =  em.createQuery(stringBuilderSql.toString());
			
			if (filterMap.get("idBusinessProcessPk") != null) {
				query.setParameter("idBusinessProcessPk", filterMap.get("idBusinessProcessPk"));	
			}
			if (filterMap.get("idReferenceProcessFk") != null) {
				query.setParameter("idReferenceProcessFk", filterMap.get("idReferenceProcessFk"));	
			}
			  
			lstBusinessProcesses = (List<Object[]>) query.getResultList(); 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lstBusinessProcesses;
		
	}

	/**
	 * Load process parameters.
	 *
	 * @param idBusinessProcessPk the id business process pk
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public Map<String, String> loadProcessParameters(Long idBusinessProcessPk) throws ServiceException {
		
		Map<String, String> parameters = new HashMap<String, String>();
		
		ParameterTableTO filter = new ParameterTableTO();
        filter.setLongInteger(idBusinessProcessPk);
        List<ParameterTable> params = parameterServiceBean.getListParameterTableServiceBean(filter);
        //parameter values reside in indicator4
        if(params.size() > 0){
        	for (ParameterTable parameterTable : params) {
				if(Validations.validateIsNotNull(parameterTable.getIndicator4())){
					parameters.put(parameterTable.getParameterName(), parameterTable.getIndicator4().toString());
				}
			}
        }
        return parameters;
	}

	/**
	 * Update process schedule.
	 *
	 * @param processSchedule the process schedule
	 * @throws ServiceException the service exception
	 */
	public void updateProcessSchedule(ProcessSchedule processSchedule) throws ServiceException {
		update(processSchedule);
	}
	
	/**
	 * Gets the notification configuration.
	 *
	 * @param businessProcess the business process
	 * @return the notification configuration
	 * @throws ServiceException the service exception
	 */
	public List<NotificationConfiguration> getNotificationConfiguration(Long businessProcess) throws ServiceException {
		StringBuilder builder = new StringBuilder(100);
		builder.append(" select notification from NotificationConfiguration notification ");		
		builder.append(" where notification.businessProcess.idBusinessProcessPk = :process ");
		TypedQuery<NotificationConfiguration> query = em.createQuery(builder.toString(), NotificationConfiguration.class);
		query.setParameter("process", businessProcess);
		return query.getResultList();
	}

}
