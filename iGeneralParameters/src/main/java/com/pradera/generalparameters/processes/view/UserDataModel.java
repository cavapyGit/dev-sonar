package com.pradera.generalparameters.processes.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class SystemServiceFacade.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
public class UserDataModel extends ListDataModel<User> implements SelectableDataModel<User>{

	/**
	 * Instantiates a new user data model.
	 */
	public UserDataModel(){
		
	}
	
	/**
	 * Instantiates a new user data model.
	 *
	 * @param data the data
	 */
	public UserDataModel(List<User> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public User getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<User> perfiles=(List<User>)getWrappedData();
        for(User perfil : perfiles) {  
        	
            if(String.valueOf(perfil.getSeqNumber()).equals(rowKey))
                return perfil;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(User perfilBean) {
		// TODO Auto-generated method stub
		return perfilBean.getSeqNumber();
	}
	
	

}


