package com.pradera.generalparameters.processes.service;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
//import com.pradera.core.component.business.service.ExecutorComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.component.generalparameters.to.MoneyExchangeTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.webclient.ParametersServiceConsumer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementProcessService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class ParameterProcessService extends CrudDaoServiceBean{
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(ParameterProcessService.class);
	
	/** The accounts component service. */
	@Inject
	private Instance<AccountsComponentService> accountsComponentService;
	
	/** The holder balance movements service bean. */
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The settlement service consumer. */
	@EJB
	private ParametersServiceConsumer parametersServiceConsumer;
	
	/** The batch service bean. */
	@EJB
	private BatchServiceBean batchServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/**
	 * Generate funds transfer register to.
	 *
	 * @param objFundsTransferRegisterTO the obj funds transfer register to
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the funds transfer register to
	 * @throws ServiceException the service exception
	 */
	public MoneyExchangeTO generateXmlExchangeTO(MoneyExchangeTO moneyExchangeTO, Long idInterfaceProcess, 
			LoggerUser loggerUser) throws ServiceException{
		moneyExchangeTO.setIdInterfaceProcess(idInterfaceProcess);
		Long idInterfaceTransaction = interfaceComponentServiceBean.get().saveInterfaceTransactionTx(moneyExchangeTO, BooleanType.YES.getCode(), loggerUser);
		moneyExchangeTO.setIdInterfaceTransaction(idInterfaceTransaction);
		return moneyExchangeTO;
	}
}