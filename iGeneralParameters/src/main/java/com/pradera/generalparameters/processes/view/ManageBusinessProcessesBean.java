package com.pradera.generalparameters.processes.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.processes.remote.types.ComboBoxValueBean;
import com.pradera.commons.security.model.type.UserAccountResponsabilityType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.generalparameters.processes.facade.BusinessProcessesServiceFacade;
import com.pradera.generalparameters.processes.to.BusinessProcessTO;
import com.pradera.generalparameters.schedulerprocess.facade.SchedulerServiceFacade;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.notification.DestinationNotification;
import com.pradera.model.notification.NotificationConfiguration;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.notification.type.NotificationDestinationType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessSchedule;
import com.pradera.model.process.type.ProcessScheduleStateType;
import com.pradera.model.process.type.ProcessStateType;
import com.pradera.model.process.type.ProcessType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ManageBusinessProcessesBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/09/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ManageBusinessProcessesBean extends GenericBaseBean implements Serializable {

	/** Default serial version uid. */
	private static final long serialVersionUID = 1L;	
	
	/** The business processes service facade. */
	@EJB
	private BusinessProcessesServiceFacade businessProcessesServiceFacade;	
	/** The scheduler service facade. */
	@EJB
	private SchedulerServiceFacade schedulerServiceFacade;	
	
	/** The parameter service facade. */
	@EJB
	private GeneralParametersFacade parameterServiceFacade;	
	
	/** The helper component facade. */
	@EJB
	private HelperComponentFacade helperComponentFacade;	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** Holds the Search result Business Process. */
	private List<BusinessProcessTO> lstBusinessProcesses;
	
	/** Holds the Search result Business Process Data Model. */
	private GenericDataModel<BusinessProcessTO> businessProcessDataModel;
	
	/** Holds the selected Business Process. */
	private BusinessProcessTO businessProcess, businessProcessDetail;
	
	/** Holds the selected Schedule Process. */
	private ProcessSchedule processSchedule;
	
	/** Check Schedule List is empty. */
	private boolean scheduleListNotEmpty;
	
	/** Check Notification List is empty. */
	private boolean notificationListNotEmpty;
	
	
	/** Holds the DataModel for the User selected Destination Notification Types. */
	private GenericDataModel<DestinationNotification> destNotificationDataModel ;
	
	/** The institucion. */
	private boolean institucion = false;
	
	
	/** Holds the User Types Details. */
	private List<ParameterTable> lstUserTypes;
	
	/** Holds the UserAccount state Details. */
	private List<ParameterTable> lstState;
		
	/** Holds the Institution Details. */
	private List<ParameterTable> lstInstitutionTypeTemp;
	
	/** Holds the Selected Macro Process. */
	private Long macroProcessSelected;
	
	/** Holds the Macro Process List. */
	private List<BeanTypeBusiness> listMacroProcess ;
	
	/** Holds the Selected Process Level1. */
	private Long level1Selected;
	
	/** Holds the Process Level1 List. */
	private List<BeanTypeBusiness> listLevel1 ;
	
	/** Holds the Selected Process Level2. */
	private Long level2Selected;
	
	/** Holds the Process Level2 List. */
	private List<BeanTypeBusiness> listLevel2;
	
	/** Holds the Selected Process Type. */
	private Integer typeSelected;
	
	/** Holds the Selected Process Negotiation. */
	private String negocioSelected;
	
	/** Holds the Selected Mnemonic. */
	private String mnemonicSelected;
	
	/** Holds The Process TYpe List. */
	private List<ProcessType> processTypes;
	
	/** The lst notification configuration. */
	private List<NotificationConfiguration> lstNotificationConfiguration;
	
	/** The list ref process. */
	private List<Long> listRefProcess = new ArrayList<Long>();
	
	/** Holds the modify operation status. */
	private boolean modify;
	
	
	/** Holds the Schedule Process Modification. */
	private boolean scheduleModifyFlag;
	
	/** Holds the Notification Process Modification. */
	private boolean notificationModifyFlag;
	
	/** Holds the DeActive Process Schedule Action. */
	private boolean deactiveFlag;
	/** Holds The institution type List. */
	private List<ParameterTable> lstInstitutionType;
	
	/** The rendered button. */
	private boolean renderedButton = false;
	
	/** The validation. */
	private Integer validation = 0;
	
	/** The id notification pk. */
	private Long idNotificationPk;
	
	/** Holds the Institution List. */
	private List<BeanTypeBusiness> lstEntities;
	
	/** Holds the Selected Institution. */
	private Integer institutionSelectedType;
	
	/** To identify the modification in the page. */
	private boolean formState;
	
	/** Holds the Process Notification Details. */
	private ProcessNotification processNotification;
	
	/** Holds the Notification Message details. */
	private ProcessNotification processNotificationDetails;
	
	/** Holds the Notification User details. */
	private DestinationNotification processNotificationUser;
	
	/** The destination details. */
	private List<DestinationNotification> destinationDetails;
	
	/** The business process selected. */
	private BusinessProcess businessProcessSelected;
	
	/** The state list. */
	private List<SelectItem> stateList;
	
	/** The type process list. */
	private List<SelectItem> typeProcessList;
	
	/** The module list. */
	private List<SelectItem> moduleList;
	
	/** The notifications type. */
	private List<ParameterTable> notificationsType;
	
	/** The lst notifications type. */
	private List<ParameterTable> lstNotificationsType;
	
	/** The destinations type. */
	private List<ParameterTable> destinationsType;
	
	/** The institutions type. */
	private Map<String,Integer> institutionsType;
	
	/** The responsabilities type. */
	private Map<String,Integer> responsabilitiesType;
	
	/** The status type. */
	private Map<String,Integer> statusType;
	
	/** The list institutions. */
	private List<SelectItem> listInstitutions;
	
	/** The users accounts. */
	private List<UserAccountSession> usersAccounts;
	
	/** The user type. */
	private Integer userType;
	
	/** The institution type. */
	private Object institutionType;
	
	/** The user selected. */
	private String userSelected;
	
	/** The indicator notification. */
	private Integer indicatorNotification;
	
	/** The modify notification. */
	private boolean modifyNotification;
	
	/** The open edit notification. */
	private boolean openEditNotification;
	
	/** The application configuration. */
	@Inject
	@InjectableResource(location = "GenericConstants.properties")
	Properties applicationConfiguration;
	
	/** The profiles. */
	private List<SelectItem> profiles;
	
	/** The clien rest service. */
	@Inject
	ClientRestService clienRestService;
	
	/** The id system selected. */
	private Integer idSystemSelected;
	
	/** The lst systems. */
	private List<ComboBoxValueBean> lstSystems;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The notification configuration. */
	private NotificationConfiguration notificationConfiguration;
	
	/** The list priority. */
	private List<BooleanType> listPriority = new ArrayList<BooleanType>();
	
	/**
	 * Find Process type selected.
	 *
	 * @return the process type
	 */
	public BeanTypeBusiness findInstitutionSelected() {
		for (BeanTypeBusiness type : lstEntities) {
			if (institutionSelectedType != null) {
				if ((institutionSelectedType.equals(type.getIdTypePK().intValue()))) {
					return type;
				}
			}
		}
		return null;
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try{			
			//To identify the modification in the page
			setFormState(false);
			processTypes = ProcessType.list;	
			lstSystems = new ArrayList<ComboBoxValueBean>();
			lstInstitutionType = new ArrayList<ParameterTable>();
			lstUserTypes = new ArrayList<ParameterTable>();
			lstState = new ArrayList<ParameterTable>();			
			lstInstitutionTypeTemp = new ArrayList<ParameterTable>();
			//privileges button
			//userInfo.getPrivilegeComponent().setBtnModifyView(true);
			PrivilegeComponent privilegeComp=new PrivilegeComponent();
			privilegeComp.setBtnModifyView(true);			
			userPrivilege.setPrivilegeComponent(privilegeComp);
			//load selects to notifications
			lstNotificationsType = new ArrayList<ParameterTable>();
			notificationsType = new ArrayList<ParameterTable>();
			destinationsType = new ArrayList<ParameterTable>();
			institutionsType = new LinkedHashMap<String, Integer>();
			responsabilitiesType = new LinkedHashMap<String, Integer>();
			statusType = new LinkedHashMap<String, Integer>();
			profiles = new ArrayList<SelectItem>();
			ParameterTableTO parameterTable = new ParameterTableTO();
			parameterTable.setState(BooleanType.YES.getCode());
			parameterTable.setIndicator4(BooleanType.YES.getCode());
			parameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_NOTIFICATION_TYPE.getCode());
			for(ParameterTable parameter :parameterServiceFacade.getListParameterTableServiceBean(parameterTable)){
				notificationsType.add(parameter);
			}
			
			parameterTable = new ParameterTableTO();
			parameterTable.setState(BooleanType.YES.getCode());
			parameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_NOTIFICATION_DESTINATION_TYPE.getCode());
			List<ParameterTable> lstDestinationType = parameterServiceFacade.getListParameterTableServiceBean(parameterTable);
			for(ParameterTable parameter : lstDestinationType){
				destinationsType.add(parameter);
			}
			for(InstitutionType institution : InstitutionType.values()){
				institutionsType.put(institution.getValue(), institution.getCode());
			}
			for(UserAccountResponsabilityType responsabilityType : UserAccountResponsabilityType.values()){
				responsabilitiesType.put(responsabilityType.getValue(), responsabilityType.getCode());
			}
			for(BooleanType answer : BooleanType.values()){
				statusType.put(answer.getValue(), answer.getCode());
			}
			usersAccounts= new ArrayList<UserAccountSession>();
			listInstitutions = new ArrayList<SelectItem>();
			listSystems();
			lstNotificationConfiguration = new ArrayList<NotificationConfiguration>();
			notificationConfiguration = new NotificationConfiguration();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * List systems.
	 */
	public void listSystems() {
		try{
			listMacroProcess = new ArrayList<BeanTypeBusiness>();
			listLevel1 = new ArrayList<BeanTypeBusiness>();
			listLevel2 = new ArrayList<BeanTypeBusiness>();
			
			List<Object[]> listObjArrSystems = businessProcessesServiceFacade.getSystemList();
			ComboBoxValueBean comboBoxValueBean = null;
			for (Object[] objects : listObjArrSystems) {
				comboBoxValueBean = new ComboBoxValueBean();
				comboBoxValueBean.setItemLabel(objects[1].toString());
				comboBoxValueBean.setItemValue(objects[0].toString());
				lstSystems.add(comboBoxValueBean);
			}
			
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load list macro process.
	 */
	public void loadListMacroProcess(){
		List<Object[]> listObjectMacros = new ArrayList<Object[]>();
		try {
			listMacroProcess = new ArrayList<BeanTypeBusiness>();
			if(Validations.validateIsNotNullAndPositive(idSystemSelected)){
				listObjectMacros = businessProcessesServiceFacade.getMacroProcessListbyidSystem(idSystemSelected);
				BeanTypeBusiness type = null;
				Long idSystemOptionPk = null;				
				// Iterating the Macro process related list
				for (Object[] arrObj : listObjectMacros) {
					type = new BeanTypeBusiness();
					idSystemOptionPk = new Long(arrObj[0].toString());
					type.setIdTypePK(idSystemOptionPk);
					if (arrObj[1] != null) {
						type.setDescription(arrObj[1].toString());
						listMacroProcess.add(type);
					}
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Method for loading the ProcessLevel1 list.
	 *
	 * @return the level1 list
	 */
	public void getLevel1List() {
		try{
			level1Selected=0L;
			if(Validations.validateIsNotNullAndNotEmpty(macroProcessSelected) && Validations.validateIsPositiveNumber(macroProcessSelected)){

			List<Long> listIdMacross = new ArrayList<Long>();
			listIdMacross.add(macroProcessSelected);
			List<Object[]> listObjectLevel1 = businessProcessesServiceFacade
					.getProcessLevels(listIdMacross);
			BeanTypeBusiness type = null;
			Long idSystemOptionPk = null;
			listLevel1 = new ArrayList<BeanTypeBusiness>();
			// Iterating the listObjectLevel1 
			for (Object[] arrObj : listObjectLevel1) {
				type = new BeanTypeBusiness();
				idSystemOptionPk = new Long(arrObj[0].toString());
				type.setIdTypePK(idSystemOptionPk);
				if (arrObj[1] != null) {
					type.setDescription(arrObj[1].toString());
					listLevel1.add(type);
				}
	
			
			}
			}//User select Macro process default value
			else{
				listLevel1 = null;
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Method for loading the ProcessLevel2 list.
	 *
	 * @return the level2 list
	 */
	public void getLevel2List() {
		try{
				if(Validations.validateIsNotNullAndNotEmpty(level1Selected) && Validations.validateIsPositiveNumber(level1Selected)){
			List<Long> listIdsLevel1 = new ArrayList<Long>();
			listIdsLevel1.add(level1Selected);
			List<Object[]> listObjectLevel1 = businessProcessesServiceFacade.getProcessLevels(listIdsLevel1);
			BeanTypeBusiness type = null;
			Long idSystemOptionPk = null;
			listLevel2 = new ArrayList<BeanTypeBusiness>();
			// Iterating the lstModulesArry list for getting the modules list
			for (Object[] arrObj : listObjectLevel1) {
				type = new BeanTypeBusiness();
				idSystemOptionPk = new Long(arrObj[0].toString());
				type.setIdTypePK(idSystemOptionPk);
				if (arrObj[1] != null) {
					type.setDescription(arrObj[1].toString());
					listLevel2.add(type);
				}
	
			}
				}//User select Process level1 default value
				else{
					listLevel2 = null;	
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Method for getting the Business Process Details.
	 */
	@LoggerAuditWeb
	public void listBusinessProcess() {
		listRefProcess.clear();
		firsRegisterDataTable = 0;
		try {
			lstBusinessProcesses = businessProcessesServiceFacade
						.listBusinessProcessFacade(macroProcessSelected,level1Selected, level2Selected, null,
								typeSelected, negocioSelected, mnemonicSelected,indicatorNotification);
			businessProcessDataModel = new GenericDataModel<BusinessProcessTO>(lstBusinessProcesses);
          businessProcess =null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Method for clear the screen components.
	 */
	public void clearBusinessProcesses() {
		JSFUtilities.resetViewRoot();
		if(Validations.validateIsNotNull(lstBusinessProcesses)){
			lstBusinessProcesses.clear();
		}		
		if(Validations.validateIsNotNull(listMacroProcess)){
			listMacroProcess.clear();
		}		
		if(Validations.validateIsNotNull(listLevel1)){
			listLevel1.clear();
		}
		if(Validations.validateIsNotNull(listLevel2)){
			listLevel2.clear();
		}
		businessProcessDataModel = null;
		macroProcessSelected = 0L;
		negocioSelected = GeneralConstants.EMPTY_STRING;
		mnemonicSelected = GeneralConstants.EMPTY_STRING;
		typeSelected = GeneralConstants.ZERO_VALUE_INTEGER;
		indicatorNotification = null;
		idSystemSelected = null;
	}

	/**
	 * Method for redirecting the screen to the ProcessData screen.
	 *
	 * @return the string of return page
	 */
	public String assignBusinessProcess() {
		if(Validations.validateIsNull(businessProcess)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
    		return null;
		}
		//To identify the modification in the page
		setFormState(false);
		processSchedule = new ProcessSchedule();
		processNotification = new ProcessNotification();
		loadNotificationAndSchedules();
		businessProcessSelected = businessProcessesServiceFacade.loadBusinesProcess(businessProcess.getIdBusinessProcessPk());
		businessProcessDetail = new BusinessProcessTO();
		businessProcessDetail.setIdBusinessProcessPk(businessProcess.getIdBusinessProcessPk());
		businessProcessDetail.setMacroProcessName(businessProcess.getMacroProcessName());
		businessProcessDetail.setProcessLevel1Name(businessProcess.getProcessLevel1Name());
		businessProcessDetail.setProcessLevel2Name(businessProcess.getProcessLevel2Name());
		businessProcessDetail.setProcessScheduleDataModel(businessProcess.getProcessScheduleDataModel());
		businessProcessDetail.setProcessNotificationDataModel(businessProcess.getProcessNotificationDataModel());
		stateList = new ArrayList<SelectItem>();
		for(ProcessStateType proc : ProcessStateType.values()){
			stateList.add(new SelectItem(proc.getCode(), proc.getValue()));
		}
		moduleList = new ArrayList<SelectItem>();
		for(ModuleWarType module : ModuleWarType.values()){
			moduleList.add(new SelectItem(module.getValue(), module.getValue()));
		}
		typeProcessList = new ArrayList<SelectItem>();
		for(ProcessType process : ProcessType.values()){
			typeProcessList.add(new SelectItem(process.getCode(), process.getValue()));
		}		
		return "processDataRule";
	}
	
	/**
	 * Before on modify.
	 */
	public void beforeOnModify(){
		JSFUtilities.hideGeneralDialogues();
		Object[] bodyData = new Object[1];
		bodyData[0] = businessProcessSelected.getIdBusinessProcessPk().toString();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
				PropertiesUtilities.getMessage(PropertiesConstants.BUSINESS_PROCESS_MODIFY_CONFIRM,bodyData));
		JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show();");;
	}
	
	/**
	 * Modify business process.
	 */
	@LoggerAuditWeb
	public void modifyBusinessProcess(){
		JSFUtilities.hideGeneralDialogues();
		try{			
			businessProcessesServiceFacade.modifyBusinessProcess(businessProcessSelected, businessProcessDetail);
			//reload search
			listBusinessProcess();
			Object[] bodyData = new Object[1];
			bodyData[0] = businessProcessSelected.getIdBusinessProcessPk().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.BUSINESS_PROCESS_MODIFY_SUCCESS,bodyData));			
			JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show();");
		} catch(ServiceException sea){
			if(sea.getErrorService().equals(ErrorServiceType.CONCURRENCY_LOCK_UPDATE)){
				//reload search
				listBusinessProcess();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, Locale.getDefault(), 
						        sea.getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show();");
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getExceptionMessage(sea.getErrorService().getMessage(),sea.getParams()));
				JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show();");
			}
		}catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	} 
	
	/**
	 * To get the notifications and schedules of the selected process.
	 */
	private void loadNotificationAndSchedules(){
		getSchedules(businessProcess.getIdBusinessProcessPk());
		getNotifications(businessProcess.getIdBusinessProcessPk());
	}
	
	/**
	 * Method for getting the schedules for the selected business process id.
	 *
	 * @param idBusinessProcessPk the id business process pk
	 * @return the schedules
	 */
	private void getSchedules(Long idBusinessProcessPk) {
		try{
			List<ProcessSchedule> lstSchedules = schedulerServiceFacade.getProcessScheduler(idBusinessProcessPk);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSchedules)){
				scheduleListNotEmpty = true;
				businessProcess.setProcessScheduleDataModel(new GenericDataModel<ProcessSchedule>(lstSchedules));				
			}else{
				scheduleListNotEmpty = false;
			}				
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
    
	
	/**
	 * Method for getting the Notifications.
	 *
	 * @param idBusinessProcessPk the id business process pk
	 * @return the notifications
	 */
	private void getNotifications(Long idBusinessProcessPk) {
		try{
			List<ProcessNotification>  lstNotifications =  businessProcessesServiceFacade.getNotificationBusinessProcess(idBusinessProcessPk);
			if(Validations.validateListIsNotNullAndNotEmpty(lstNotifications)){
				for(ProcessNotification notification : lstNotifications){
					if(notification.getIndStatus().equals(BooleanType.YES.getCode()))
						notification.setStateDescription(applicationConfiguration.getProperty("status.indicator.one"));
					else 
						notification.setStateDescription(applicationConfiguration.getProperty("status.indicator.zero"));
					
					for(ParameterTable paramTab:notificationsType){
						if(paramTab.getParameterTablePk().equals(notification.getNotificationType())){
							notification.setNotificationTypeDescription(paramTab.getParameterName());
							break;
						}
					}
					for(ParameterTable paramTab:destinationsType){
						if(paramTab.getParameterTablePk().equals(notification.getDestinationType())){
							notification.setDestinationTypeDescription(paramTab.getParameterName());
							break;
						}
					}
				}
				/*ParameterTable param = new ParameterTable();
				for(ProcessNotification notification : lstNotifications){
					param.setParameterTablePk(notification.getNotificationType());
					int index = notificationsType.indexOf(param);
					if(index != -1){
						notification.setNotificationTypeDescription(notificationsType.get(index).getParameterName());
					}
					param.setParameterTablePk(notification.getDestinationType());
					index = destinationsType.indexOf(param);
					if(index != -1){
					notification.setDestinationTypeDescription(destinationsType.get(index).getParameterName());
					}
					if(notification.getIndStatus().equals(BooleanType.YES.getCode())){
						notification.setStateDescription(applicationConfiguration.getProperty("status.indicator.one"));
					} else {
						notification.setStateDescription(applicationConfiguration.getProperty("status.indicator.zero"));
					}
					
				}*/
				notificationListNotEmpty=true;
				businessProcess.setProcessNotificationDataModel(new GenericDataModel<ProcessNotification>(lstNotifications));
			}else{
				notificationListNotEmpty=false;
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load process schedule reg data.
	 */
	public void loadProcessScheduleRegData(){
		JSFUtilities.hideGeneralDialogues();
		scheduleModifyFlag = false;
		if(ProcessType.BATCH.getCode().equals(businessProcessSelected.getProcessType())){
			processSchedule = new ProcessSchedule();
			JSFUtilities.executeJavascriptFunction("PF('dlgwProcessScheduleReg').show();");
		}else{
			JSFUtilities.showValidationDialog();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_ADD_NOT_POSSIBLE));
		}
	}

	/**
	 * Save the Schedule Process.
	 */
	@LoggerAuditWeb
	public void saveScheduleProcess() {
		//Object[] argObj = null;
//		String message = null;
		setFormState(true);
		hideDialogs();		
		//try {
			// Save the Process Schedule operation
			if (!scheduleModifyFlag) {
				processSchedule.setBusinessProcess(businessProcessSelected);
				processSchedule.setScheduleState(ProcessScheduleStateType.ACTIVE.getCode());
				processSchedule.setRegistryDate(CommonsUtilities.currentDate());
				processSchedule.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				//schedulerServiceFacade.registerScheduler(processSchedule);
				if(Validations.validateIsNull(businessProcessDetail.getProcessScheduleDataModel())
						|| businessProcessDetail.getProcessScheduleDataModel().getDataList().isEmpty()){
					businessProcessDetail.setProcessScheduleDataModel(new GenericDataModel<ProcessSchedule>());
				}
				Long row = new Long(businessProcessDetail.getProcessScheduleDataModel().getSize() + 1);
				processSchedule.setIdProcessSchedulePk(row);
				businessProcessDetail.getProcessScheduleDataModel().getDataList().add(processSchedule);
				scheduleListNotEmpty = true;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_ADD_SUCC,new Object[]{processSchedule.getScheduleDescription()}));
				/*argObj = new Object[2];				
				argObj[0] = processSchedule.getIdProcessSchedulePk();
//				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ADD_SCHEDULE_SUCC, argObj);
				// Send Notification register
				BusinessProcess businessProc = new BusinessProcess();
				businessProc.setIdBusinessProcessPk(BusinessProcessType.PROCESS_TIMES_ADD.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null,argObj);*/
			} else {
				//processSchedule = schedulerServiceFacade.updateScheduler(processSchedule);
				//getSchedules(processSchedule.getBusinessProcess().getIdBusinessProcessPk());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_MODIFY_SUCC,new Object[]{processSchedule.getScheduleDescription()}));
				/*argObj = new Object[3];							
				argObj[0] = processSchedule.getIdProcessSchedulePk();
//				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_MODIFY_SCHEDULE_SUCC, argObj);
				//send notification update
				BusinessProcess businessProc = new BusinessProcess();
				businessProc.setIdBusinessProcessPk(BusinessProcessType.PROCESS_TIMES_MODIFICATION.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null,argObj);*/
			}
			JSFUtilities.showValidationDialog();
			processSchedule = null;
		/*} catch(ServiceException sea){
			if(sea.getErrorService().equals(ErrorServiceType.CONCURRENCY_LOCK_UPDATE)){
				//update schedule
				getSchedules(processSchedule.getBusinessProcess().getIdBusinessProcessPk());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, Locale.getDefault(), 
						        sea.getMessage()));
				JSFUtilities.showComponent("frmDialogs:transactionDialog");
			}
			} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}*/
		
	}

	/**
	 * Open the Modify Schedule Window.
	 */
	public void modifyScheduleProcess() {
		hideDialogs();
		this.setViewOperationType(ViewOperationsType.MODIFY.getCode());		
		if(Validations.validateIsNotNull(processSchedule)){
			scheduleModifyFlag = true;
			JSFUtilities.executeJavascriptFunction("PF('dlgwProcessScheduleReg').show();");			
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();			
		}
		
	}

	


	/**
	 * Method for changing the process status to Active.
	 */
	@LoggerAuditWeb
	public void changeToActive() {
		hideDialogs();		
		if(Validations.validateIsNull(processSchedule)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}else if(ProcessScheduleStateType.DEACTIVE.getCode().equals(processSchedule.getScheduleState())){
			deactiveFlag = false;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ACTIVE)
					, PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_ACTIVE_CONFIRM,new Object[]{processSchedule.getIdProcessSchedulePk().toString()}));			
			JSFUtilities.executeJavascriptFunction("PF('idConfDialogScduleWin').show();");
		}else if(ProcessScheduleStateType.ACTIVE.getCode().equals(processSchedule.getScheduleState())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_ACTIVE_ERROR));
			JSFUtilities.showValidationDialog();
		}

	}

	/**
	 * Method for changing the process status to Deactive.
	 */
	public void changeToDeActive() {
		hideDialogs();		
		if(Validations.validateIsNull(processSchedule)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}else if(ProcessScheduleStateType.ACTIVE.getCode().equals(processSchedule.getScheduleState())){
			deactiveFlag = true;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DEACTIVE)
					, PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_DEACTIVE_CONFIRM,new Object[]{processSchedule.getIdProcessSchedulePk().toString()}));			
			JSFUtilities.executeJavascriptFunction("PF('idConfDialogScduleWin').show();");
		}else if(ProcessScheduleStateType.DEACTIVE.getCode().equals(processSchedule.getScheduleState())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_DEACTIVE_ERROR));
			JSFUtilities.showValidationDialog();
		}
		
	}

	
	/**
	 * Method for clear the fields.
	 */
	public void clearNotificationEdit() {
		openEditNotification=false;
		processNotification=new ProcessNotification();
		destNotificationDataModel = new GenericDataModel<DestinationNotification>();;
		userType = null;
		institutionType = null;
		userSelected=null;
		JSFUtilities.executeJavascriptFunction("PF('dlgwProcessNotifAdd').hide();");
//		lstUsers.clear();
//		userDataModels = new UserDataModel();
//		userTypeSelected = 0;
//		setUserTypeSelected(0);
//		institutionSelected = 0;
//		setInstitutionSelected(0);
//		userCode = GeneralConstants.EMPTY_STRING;
//		stateSelected = 0;
	}

	
		
	/**
	 * Method for getting the user list.
	 *
	 * @return the List<UserAccountType>
	 */
	public List<ParameterTable> getLstUserTypes() {		
		return this.lstUserTypes;
	}
	
	/**
	 * Sets the lst user types.
	 *
	 * @param lstUserTypes the lstUserTypes to set
	 */
	public void setLstUserTypes(List<ParameterTable> lstUserTypes) {
		this.lstUserTypes = lstUserTypes;
	}	

	/**
	 * Gets the lst state.
	 *
	 * @return the lstState
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}

	/**
	 * Sets the lst state.
	 *
	 * @param lstState the lstState to set
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	

	/**
	 * Gets the lst institution type temp.
	 *
	 * @return the lstInstitutionTypeTemp
	 */
	public List<ParameterTable> getLstInstitutionTypeTemp() {
		return lstInstitutionTypeTemp;
	}

	/**
	 * Sets the lst institution type temp.
	 *
	 * @param lstInstitutionTypeTemp the lstInstitutionTypeTemp to set
	 */
	public void setLstInstitutionTypeTemp(
			List<ParameterTable> lstInstitutionTypeTemp) {
		this.lstInstitutionTypeTemp = lstInstitutionTypeTemp;
	}
	
	/**
	 * Select modify notification.
	 */
	public void selectModifyNotification(){
		hideDialogs();
		listPriority = BooleanType.list;
		destNotificationDataModel = null;
		lstNotificationsType = new ArrayList<ParameterTable>();
		if(ProcessType.ONLINE.getCode().equals(businessProcessSelected.getProcessType())){
			for(ParameterTable paramTab:notificationsType){
				if(!NotificationType.PROCESS.getCode().equals(paramTab.getParameterTablePk()))
					lstNotificationsType.add(paramTab);
			}
		}else{
			lstNotificationsType = notificationsType;
		}
		if(processNotification == null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
		} else {
			this.setViewOperationType(ViewOperationsType.MODIFY.getCode());		
			modifyNotification = true;
			openEditNotification=true;
			if(Validations.validateListIsNotNullAndNotEmpty(processNotification.getDestinationNotifications())){
				destNotificationDataModel = new GenericDataModel<DestinationNotification>();
				destNotificationDataModel.getDataList().addAll(processNotification.getDestinationNotifications());
			}
			JSFUtilities.executeJavascriptFunction("PF('dlgwProcessNotifAdd').show();");
			loadProfilesSystem();
			getLoadLstNotificationConfiguration();
		}

	}
	
	/**
	 * Add user manual.
	 */
	public void addUserManual(){
		if(StringUtils.isNotBlank(userSelected)){
			if(destNotificationDataModel == null){
				destNotificationDataModel = new GenericDataModel<DestinationNotification>();
			}	
			DestinationNotification destinationNotification = new DestinationNotification();
			UserAccountSession userAccountSession = new UserAccountSession();
			userAccountSession.setUserName(userSelected);
			int index = usersAccounts.indexOf(userAccountSession);
			if(index != -1){
				userAccountSession = usersAccounts.get(index);
				destinationNotification.setEmail(userAccountSession.getEmail());
				destinationNotification.setIdUserAccount(userAccountSession.getUserName());
				destinationNotification.setMobileNumber(userAccountSession.getPhoneNumber());
				destinationNotification.setFullName(userAccountSession.getFullName().replaceAll("null", GeneralConstants.EMPTY_STRING).trim());
				destinationNotification.setProcessNotification(processNotification);
				destinationNotification.setIndStatus(BooleanType.YES.getCode());
				destinationNotification.setInstitutionType(userAccountSession.getInstitutionType());
				if(userAccountSession.getInstitutionType().equals(InstitutionType.ISSUER.getCode())||userAccountSession.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode())){
					destinationNotification.setInstitution(userAccountSession.getIssuerCode());
				}else if (userAccountSession.getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode())){
					destinationNotification.setInstitution(userAccountSession.getParticipantCode().toString());
				}
				destNotificationDataModel.getDataList().add(destinationNotification);
			}
			
		}
	}

	
	/**
	 * Method for saving the Notifications.
	 */
	@LoggerAuditWeb
	public void saveNotifications() {		
		//Object[] argObj = null;
		try{			
			processNotification.setIndStatus(BooleanType.YES.getCode());
			processNotification.setStateDescription(applicationConfiguration.getProperty("status.indicator.one"));
			processNotification.setBusinessProcess(businessProcessSelected);
			if(NotificationDestinationType.MANUAL.getCode().equals(processNotification.getDestinationType())
					&& Validations.validateIsNotNull(destNotificationDataModel)
					&& !destNotificationDataModel.getDataList().isEmpty()){
				processNotification.setDestinationNotifications(destNotificationDataModel.getDataList());
			}
			for(ParameterTable paramTab:notificationsType){
				if(paramTab.getParameterTablePk().equals(processNotification.getNotificationType())){
					processNotification.setNotificationTypeDescription(paramTab.getParameterName());
					break;
				}
			}
			for(ParameterTable paramTab:destinationsType){
				if(paramTab.getParameterTablePk().equals(processNotification.getDestinationType())){
					processNotification.setDestinationTypeDescription(paramTab.getParameterName());
					break;
				}
			}
			//businessProcessesServiceFacade.registerNotification(processNotification);
			/*ParameterTable param = new ParameterTable();
			param.setParameterTablePk(processNotification.getNotificationType());
				int index = notificationsType.indexOf(param);
				if(index != -1){
					processNotification.setNotificationTypeDescription(notificationsType.get(index).getParameterName());
				}
				param.setParameterTablePk(processNotification.getDestinationType());
				index = destinationsType.indexOf(param);
				if(index != -1){
					processNotification.setDestinationTypeDescription(destinationsType.get(index).getParameterName());
				}*/
			notificationListNotEmpty=true;
			if(Validations.validateIsNull(businessProcessDetail.getProcessNotificationDataModel())
					|| businessProcessDetail.getProcessNotificationDataModel().getDataList().isEmpty()){
				businessProcessDetail.setProcessNotificationDataModel(new GenericDataModel<ProcessNotification>());
			}
			Long row = new Long(businessProcessDetail.getProcessNotificationDataModel().getSize() + 1);
			processNotification.setIdProcessNotificationPk(row);
			businessProcessDetail.getProcessNotificationDataModel().getDataList().add(processNotification);
			JSFUtilities.executeJavascriptFunction("PF('dlgwProcessNotifAdd').hide();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_ADD_SUCC));
			JSFUtilities.showValidationDialog();			
			//To send Notification Message
			/*argObj = new Object[1];
			argObj[0] = processNotification.getIdProcessNotificationPk().toString();
			BusinessProcess busProcess = new BusinessProcess(); 
			busProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATIONS_ADD.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), busProcess, null, argObj);*/
			processNotification = null;
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
			
	}


	/**
	 * method used hide dialogs.
	 */
	public void hideDialogs() {
		JSFUtilities.executeJavascriptFunction("PF('dlgwProcessScheduleReg').hide();");								
		JSFUtilities.hideComponent(":frmProcessData:dlgwProcessNotifDetail");
		JSFUtilities.hideComponent(":frmProcessData:dlgwUserNotifDetail");
		JSFUtilities.hideGeneralDialogues();
	}

		
	
	/**
	 * Method for showing the Business process details.
	 *
	 * @return String
	 */
	public String  showBusinessProcessDetaisl(){
		JSFUtilities.putSessionMap("lstBussProcId", businessProcess);
		loadNotificationAndSchedules();
		return "processDataDetails";
	}
	
		
	/**
	 * Update States of Schedule Process.
	 */
	@LoggerAuditWeb
	public void deactiveScheduleProcess(){
		//Object[] argObj =null;
		//try{
			/*BusinessProcess businessProc = new BusinessProcess();
			//Preparing message related parameters
			argObj = new Object[3];
			argObj[0] = processSchedule.getIdProcessSchedulePk();
			argObj[1] = businessProcess.getIdBusinessProcessPk();
			argObj[2] = processSchedule.getIdProcessSchedulePk();
			 //De active Notification Process Operation Message */
			if(deactiveFlag){
				//processSchedule = schedulerServiceFacade.disableSchedule(processSchedule);
				processSchedule.setScheduleState(ProcessScheduleStateType.DEACTIVE.getCode());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_DEACTIVE_SUCC,
								new Object[]{processSchedule.getIdProcessSchedulePk().toString()}));			
				//businessProc.setIdBusinessProcessPk(BusinessProcessType.PROCESS_TIMES_DISACTIVATE.getCode());			
			}else{
				//processSchedule = schedulerServiceFacade.enableSchedule(processSchedule);
				processSchedule.setScheduleState(ProcessScheduleStateType.ACTIVE.getCode());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_ACTIVE_SUCC,
								new Object[]{processSchedule.getIdProcessSchedulePk().toString()}));
				//businessProc.setIdBusinessProcessPk(BusinessProcessType.PROCESS_TIMES_ACTIVATE.getCode());			
			}
			//notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null, argObj);
			//getSchedules(processSchedule.getBusinessProcess().getIdBusinessProcessPk());
			JSFUtilities.showValidationDialog();
			processSchedule = null;
		/*} catch(ServiceException sea){
			if(sea.getErrorService().equals(ErrorServiceType.CONCURRENCY_LOCK_UPDATE)){
				//update schedule
				getSchedules(processSchedule.getBusinessProcess().getIdBusinessProcessPk());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, Locale.getDefault(), 
						        sea.getMessage()));
				JSFUtilities.showComponent("frmDialogs:transactionDialog");
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(e));
		}*/
	}
	
	/**
	 * Get the Confirmation before modify the schedule Process.
	 */
	public void modifyScheduleProcessConFirm(){
		if(validateScheduleProcess()){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
					, PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_MODIFY_CONFIRM,new Object[]{processSchedule.getIdProcessSchedulePk().toString()}));		
			JSFUtilities.executeJavascriptFunction("PF('idConfDialogModifyScduleWin').show();");
		}		
	}
	
	
	/**
	 * Get the confirmation before add Schedule Process.
	 */
	public void addScheduleConfirm(){
		JSFUtilities.hideGeneralDialogues();
		/*processSchedule.getScheduleInfo().setScheduleHour(new Integer(processSchedule.getScheduleInfo().getTime().getHours()).toString());
		processSchedule.getScheduleInfo().setScheduleMinute(new Integer(processSchedule.getScheduleInfo().getTime().getMinutes()).toString());
		processSchedule.getScheduleInfo().setScheduleSecond(new Integer(processSchedule.getScheduleInfo().getTime().getSeconds()).toString());*/
		if(validateScheduleProcess()){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					PropertiesUtilities.getMessage(PropertiesConstants.SCHEDULE_ADD_CONFIRM));			
			JSFUtilities.executeJavascriptFunction("PF('idConfDialogModifyScduleWin').show();");
		}					
	}
	
	/**
	 * Validate schedule process.
	 *
	 * @return true, if successful
	 */
	public boolean validateScheduleProcess(){
		
		Date startDate = null;
		Date endDate = null;
		
		//Only matters start date when endDate is not null 
		if(processSchedule.getScheduleInfo().getEndDate()!=null){
			endDate = processSchedule.getScheduleInfo().getEndDate();
			if(processSchedule.getScheduleInfo().getStartDate()!=null){
				startDate = processSchedule.getScheduleInfo().getStartDate();
			} else {
				startDate = CommonsUtilities.currentDate();
			}
		}
		
		if(startDate!=null && endDate!=null){
			
			if(!processSchedule.getScheduleInfo().getScheduleDayOfMonth().equals(GeneralConstants.ASTERISK_STRING) || 
					!processSchedule.getScheduleInfo().getScheduleDayOfWeek().equals(GeneralConstants.ASTERISK_STRING) ||
					!processSchedule.getScheduleInfo().getScheduleDayOfWeek().equals(GeneralConstants.SCHEDULE_WORK_DAYS_STRING) || 
					!processSchedule.getScheduleInfo().getScheduleMonth().equals(GeneralConstants.ASTERISK_STRING) ||
					!processSchedule.getScheduleInfo().getScheduleYear().equals(GeneralConstants.ASTERISK_STRING)){
				
				Calendar calCompare = Calendar.getInstance();
				calCompare.setTime(startDate);
				
				Calendar calFinal = Calendar.getInstance();
				calFinal.setTime(endDate);
				
				boolean dayOfMonthFound = false;
				boolean dayOfWeekFound = false;
				boolean monthFound = false;
				boolean yearFound = false;
				
				Integer dayOfMonth = null;
				Integer dayOfWeek = null;
				Integer month = null;
				Integer year = null;
				
				if(!processSchedule.getScheduleInfo().getScheduleDayOfMonth().equals(GeneralConstants.ASTERISK_STRING)){
					dayOfMonth = Integer.parseInt(processSchedule.getScheduleInfo().getScheduleDayOfMonth());
				}
				
				if(! (processSchedule.getScheduleInfo().getScheduleDayOfWeek().equals(GeneralConstants.ASTERISK_STRING) ||
						processSchedule.getScheduleInfo().getScheduleDayOfWeek().equals(GeneralConstants.SCHEDULE_WORK_DAYS_STRING) )){
					if(processSchedule.getScheduleInfo().getScheduleDayOfWeek().equals("7")){
						dayOfWeek = Integer.valueOf(1);
					} else {
						dayOfWeek = Integer.parseInt(processSchedule.getScheduleInfo().getScheduleDayOfWeek()) + 1;
					}
				}
				
				if(!processSchedule.getScheduleInfo().getScheduleMonth().equals(GeneralConstants.ASTERISK_STRING)){
					month = Integer.parseInt(processSchedule.getScheduleInfo().getScheduleMonth()) -1;
				}

				if(!processSchedule.getScheduleInfo().getScheduleYear().equals(GeneralConstants.ASTERISK_STRING)){
					year = Integer.parseInt(processSchedule.getScheduleInfo().getScheduleYear());
				}
				
				while (calCompare.before(calFinal) || calCompare.equals(calFinal)){
					if(dayOfMonth!=null){
						if(calCompare.get(Calendar.DAY_OF_MONTH) == dayOfMonth.intValue()){
							dayOfMonthFound = true;
						}
					}
					
					if(dayOfWeek!=null){
						if(calCompare.get(Calendar.DAY_OF_WEEK) == dayOfWeek.intValue()){
							dayOfWeekFound = true;
						}
					}
					
					if(month!=null){
						if(calCompare.get(Calendar.MONTH) == month.intValue()){
							monthFound = true;
						}
					}
					
					if(year!=null){
						if(calCompare.get(Calendar.YEAR) == year.intValue()){
							yearFound = true;
						}
					}
					calCompare.add(Calendar.DAY_OF_MONTH, 1);
				}
				
				if(dayOfMonth!=null && !dayOfMonthFound){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage("error.dayofmonth.not.in.range"));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
				if(dayOfWeek!=null && !dayOfWeekFound){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage("error.dayofweek.not.in.range"));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
				if(month!=null && !monthFound){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage("error.month.not.in.range"));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
				if(year!=null && !yearFound){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage("error.year.not.in.range"));
					JSFUtilities.showSimpleValidationDialog();
					return false;
				}
				
			}
		}
		
		if(!processSchedule.getScheduleInfo().getScheduleDayOfMonth().equals(GeneralConstants.ASTERISK_STRING) || 
				!processSchedule.getScheduleInfo().getScheduleDayOfWeek().equals(GeneralConstants.ASTERISK_STRING) || 
				!processSchedule.getScheduleInfo().getScheduleDayOfWeek().equals(GeneralConstants.SCHEDULE_WORK_DAYS_STRING) || 
				!processSchedule.getScheduleInfo().getScheduleMonth().equals(GeneralConstants.ASTERISK_STRING) ||
				!processSchedule.getScheduleInfo().getScheduleYear().equals(GeneralConstants.ASTERISK_STRING)){
			
			if(!processSchedule.getScheduleInfo().getScheduleDayOfMonth().equals(GeneralConstants.ASTERISK_STRING) && 
					!processSchedule.getScheduleInfo().getScheduleMonth().equals(GeneralConstants.ASTERISK_STRING)){
				int dayOfMonth = Integer.parseInt(processSchedule.getScheduleInfo().getScheduleDayOfMonth());
				int month = Integer.parseInt(processSchedule.getScheduleInfo().getScheduleMonth());
				
				switch (month) {
				case 4:
				case 6:
				case 9:
				case 11:
					if(dayOfMonth>30){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage("error.day.invalid.in.month"));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					break;
				case 2:
					int maxDay = 28;
					if(!processSchedule.getScheduleInfo().getScheduleYear().equals(GeneralConstants.ASTERISK_STRING)){
						int anio = Integer.parseInt(processSchedule.getScheduleInfo().getScheduleYear());						
						if ((anio % 4 == 0) && ((anio % 100 != 0) || (anio % 400 == 0))) {
							maxDay = 29;
						}
					} else {
						maxDay = 29;
					}
					
					if(dayOfMonth>maxDay){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage("error.day.invalid.in.month"));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
				default:
					break;
				}
			}
		}
		
		return true;
	}

	/**
	 * Checks if is schedule modify flag.
	 *
	 * @return the scheduleModifyFlag
	 */
	public boolean isScheduleModifyFlag() {
		return scheduleModifyFlag;
	}

	/**
	 * Sets the schedule modify flag.
	 *
	 * @param scheduleModifyFlag the scheduleModifyFlag to set
	 */
	public void setScheduleModifyFlag(boolean scheduleModifyFlag) {
		this.scheduleModifyFlag = scheduleModifyFlag;
	}

	/**
	 * Gets the dest notification data model.
	 *
	 * @return the destNotificationDataModel
	 */
	public GenericDataModel<DestinationNotification> getDestNotificationDataModel() {
		return destNotificationDataModel;
	}

	/**
	 * Sets the dest notification data model.
	 *
	 * @param destNotificationDataModel the destNotificationDataModel to set
	 */
	public void setDestNotificationDataModel(
			GenericDataModel<DestinationNotification> destNotificationDataModel) {
		this.destNotificationDataModel = destNotificationDataModel;
	}
	
	
	/**
	 * Gets the process notification.
	 *
	 * @return the processNotification
	 */
	public ProcessNotification getProcessNotification() {
		return processNotification;
	}

	/**
	 * Sets the process notification.
	 *
	 * @param processNotification the processNotification to set
	 */
	public void setProcessNotification(ProcessNotification processNotification) {
		this.processNotification = processNotification;
	}
	 
	

	
	
	/**
	 * Confirmation before Save the Process Notification.
	 */
	public void saveNotificationConfirm(){
		hideDialogs();
		if(NotificationDestinationType.MANUAL.getCode().equals(processNotification.getDestinationType())){
			if(Validations.validateIsNull(destNotificationDataModel)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "Se debe adicionar por lo menos un usuario. Por favor verifique.");
				JSFUtilities.showValidationDialog();
				return;
			}else if(destNotificationDataModel.getDataList().isEmpty()){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "Se debe adicionar por lo menos un usuario. Por favor verifique.");
				JSFUtilities.showValidationDialog();
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
				PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_ADD_CONFIRM));		
		JSFUtilities.executeJavascriptFunction("PF('idConfDialogAddNotifWin').show();");
	}
	
	
	/**
	 * Open the Process Notification Dialog.
	 */
	public void loadProcessNotificationRegData(){
		lstNotificationsType = new ArrayList<ParameterTable>();
		if(ProcessType.ONLINE.getCode().equals(businessProcessSelected.getProcessType())){
			for(ParameterTable paramTab:notificationsType){
				if(!NotificationType.PROCESS.getCode().equals(paramTab.getParameterTablePk()))
					lstNotificationsType.add(paramTab);
			}
		}else{
			lstNotificationsType = notificationsType;
		}
		openEditNotification = true;
		processNotification = new ProcessNotification();
		listInstitutions.clear();
		usersAccounts.clear();
		userType=null;
		institutionType=null;
		userSelected=null;
		destNotificationDataModel = null;
		modifyNotification = false;
		JSFUtilities.resetComponent("dlgProcessNotifAdd");
		JSFUtilities.executeJavascriptFunction("PF('dlgwProcessNotifAdd').show();");
		loadProfilesSystem();
		notificationConfiguration = new NotificationConfiguration();
		lstNotificationConfiguration = new ArrayList<NotificationConfiguration>();
		listPriority = BooleanType.list;
		getLoadLstNotificationConfiguration();
	}
	
	/**
	 * Gets the load lst notification configuration.
	 *
	 * @return the load lst notification configuration
	 */
	public void getLoadLstNotificationConfiguration(){
		 try {
			 lstNotificationConfiguration =  businessProcessesServiceFacade.getNotificationConfiguration(businessProcessSelected.getIdBusinessProcessPk());			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Change parameter list.
	 */
	public void changeParameterList(){
		if(Validations.validateIsNotNullAndNotEmpty(notificationConfiguration.getParamValue())){
			if(Validations.validateIsNotNullAndNotEmpty(processNotification.getNotificationMessage())){
				processNotification.setNotificationMessage(processNotification.getNotificationMessage() + 
						GeneralConstants.BLANK_SPACE + notificationConfiguration.getParamValue());
			} else {
				processNotification.setNotificationMessage(notificationConfiguration.getParamValue());
			}
			
		}		
	}
	
	/**
	 * Change editor.
	 */
	public void changeEditor(){
		if(Validations.validateIsNotNullAndNotEmpty(processNotification.getNotificationMessage())){
			
		}
	}
	
	/**
	 * Select user types.
	 *
	 * @throws ServiceException the service exception
	 */
	public void selectUserTypes() throws ServiceException{
		if(userType!= null){
			InstitutionType institution = InstitutionType.get(userType);
			switch (institution) {
			case AFP:
			case PARTICIPANT:	
				List<Participant> participantList = helperComponentFacade.findParticipantNativeQueryFacade(new Participant());
				listInstitutions.clear();
				for(Participant participant:participantList){
					listInstitutions.add(new SelectItem((Object)participant.getIdParticipantPk(), participant.getDescription()+"-"+participant.getIdParticipantPk()));
				}
				usersAccounts.clear();
				break;
			case ISSUER:
				List<IssuerTO> issuerList = helperComponentFacade.findIssuerByHelper(new IssuerSearcherTO());
				listInstitutions.clear();
				for(IssuerTO issuer : issuerList){
					listInstitutions.add(new SelectItem((Object)issuer.getIssuerCode(), issuer.getIssuerDesc(),issuer.getIssuerCode()));
				}
				usersAccounts.clear();
				break;
			default:
				usersAccounts = businessProcessesServiceFacade.getUsersFromInstitution(institution, null);	
				listInstitutions.clear();
				break;
			}
		} else {
			listInstitutions.clear();
			usersAccounts.clear();
		}
	}
	
	/**
	 * Load profiles system.
	 */
	public void loadProfilesSystem(){
		profiles = new ArrayList<SelectItem>();
		List<Object[]> profilesSystem = clienRestService.getProfilesSystem(businessProcessSelected.getIdSystem());
		if(profilesSystem != null && !profilesSystem.isEmpty()){
			for(Object[] profile : profilesSystem){
				profiles.add(new SelectItem(profile[0],profile[1].toString()));
			}
		}
	}
	
	/**
	 * Select institution.
	 */
	public void selectInstitution(){
		if(institutionType!=null){
			InstitutionType institution = InstitutionType.get(userType);
			String issuerCode = null;
			Long participantCode = null;
			if(institution.equals(InstitutionType.ISSUER)){
				issuerCode = institutionType.toString();
				usersAccounts = businessProcessesServiceFacade.getUsersFromInstitution(institution, issuerCode);
			} else if(institution.equals(InstitutionType.PARTICIPANT) || institution.equals(InstitutionType.AFP)){
				participantCode = Long.valueOf(institutionType.toString());
				usersAccounts = businessProcessesServiceFacade.getUsersFromInstitution(institution, participantCode);
			}
			
		}else{
			usersAccounts.clear();
		}
	}
	
	/**
	 * Remove destination.
	 *
	 * @param destination the destination
	 */
	public void removeDestination(DestinationNotification destination) {
		destNotificationDataModel.getDataList().remove(destination);
		if(destNotificationDataModel.getSize() == 0)
			destNotificationDataModel = null;
	}

	/**
	 * Get the notificationModifyFlag.
	 *
	 * @return the notificationModifyFlag
	 */
	public boolean isNotificationModifyFlag() {
		return notificationModifyFlag;
	}

	/**
	 * Set the notificationModifyFlag.
	 *
	 * @param notificationModifyFlag the notificationModifyFlag to set
	 */
	public void setNotificationModifyFlag(boolean notificationModifyFlag) {
		this.notificationModifyFlag = notificationModifyFlag;
	}
	
	/**
	 * Get the Confirmation Before Modify the Notification Process.
	 */
	public void modifyNotificationProcessConFirm(){
		hideDialogs();
		if(NotificationDestinationType.MANUAL.getCode().equals(processNotification.getDestinationType())){
			if(Validations.validateIsNull(destNotificationDataModel)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "Se debe adicionar por lo menos un usuario. Por favor verifique.");
				JSFUtilities.showValidationDialog();
				return;
			}else if(destNotificationDataModel.getDataList().isEmpty()){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "Se debe adicionar por lo menos un usuario. Por favor verifique.");
				JSFUtilities.showValidationDialog();
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
				, PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_MODIFY_CONFIRM,
						new Object[]{processNotification.getIdProcessNotificationPk().toString()}));
		JSFUtilities.showComponent(":frmProcessData:idConfDialogModifyNotif");		
		JSFUtilities.executeJavascriptFunction("PF('idConfDialogModifyNotifWin').show();");
	}
	
	/**
	 * Modify the User Selected Notification details.
	 */
	@LoggerAuditWeb
	public void modifyNotificationProcessOperation(){
		Object[] argObj = null;
		//try{			
			if(NotificationDestinationType.MANUAL.getCode().equals(processNotification.getDestinationType())
				&& Validations.validateIsNotNullAndNotEmpty(destNotificationDataModel.getDataList())){
				processNotification.setDestinationNotifications(destNotificationDataModel.getDataList());
			}
			for(ParameterTable paramTab:notificationsType){
				if(paramTab.getParameterTablePk().equals(processNotification.getNotificationType())){
					processNotification.setNotificationTypeDescription(paramTab.getParameterName());
					break;
				}
			}
			for(ParameterTable paramTab:destinationsType){
				if(paramTab.getParameterTablePk().equals(processNotification.getDestinationType())){
					processNotification.setDestinationTypeDescription(paramTab.getParameterName());
					break;
				}
			}

			argObj = new Object[1];
			argObj[0] = processNotification.getIdProcessNotificationPk().toString();
			JSFUtilities.executeJavascriptFunction("PF('dlgwProcessNotifAdd').hide();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_MODIFY_SUCC,argObj));
			JSFUtilities.showValidationDialog();
			//To send Notification Message
			/*BusinessProcess busProcess = new BusinessProcess(); 
			busProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATIONS_MODIFICATIONS.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), busProcess, null, argObj);*/
			processNotification = null;
		/*} catch(ServiceException sexe){
			if(sexe.getErrorService().equals(ErrorServiceType.CONCURRENCY_LOCK_UPDATE)){
				//update schedule
				getNotifications(processNotification.getBusinessProcess().getIdBusinessProcessPk());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, Locale.getDefault(), 
						        sexe.getMessage()));
				JSFUtilities.showComponent("frmDialogs:transactionDialog");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}*/
	}
	
	/**
	 * Active Process Notification State Confirmation.
	 */
	public void activeProcessNitiicationConfirm(){		
		hideDialogs();		
		if(Validations.validateIsNull(processNotification)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}else if(GeneralConstants.ZERO_VALUE_INTEGER.equals(processNotification.getIndStatus())){
			deactiveFlag = false;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ACTIVE)
					, PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_ACTIVE_CONFIRM,
							new Object[]{processNotification.getIdProcessNotificationPk().toString()}));
			JSFUtilities.executeJavascriptFunction("PF('idConfDialogStatusNotiffWin').show();");
		}else if(GeneralConstants.ONE_VALUE_INTEGER.equals(processNotification.getIndStatus())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_ACTIVE_ERROR));
			JSFUtilities.showValidationDialog();
		}
	}
	
	/**
	 * Change State of the Process Notification.
	 */
	@LoggerAuditWeb
	public void deactiveOrActiveNotificationProcess(){
		 /*Object[] argObj = null;
		 //String message = null;
		 argObj = new Object[3];			
		try{
			argObj[0] = processNotification.getIdProcessNotificationPk();
			argObj[1] = businessProcess.getIdBusinessProcessPk();
			argObj[2] = processNotification.getIdProcessNotificationPk();*/
			//Checking Deactive Process Notification State
		if(deactiveFlag){
			processNotification.setIndStatus(GeneralConstants.ZERO_VALUE_INTEGER);
			processNotification.setStateDescription(applicationConfiguration.getProperty("status.indicator.zero"));
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_DEACTIVE_SUCC,
						new Object[]{processNotification.getIdProcessNotificationPk().toString()}));
			//message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_DEACTIVE_NOITIFICATION_SUCC, argObj);
		}else{
			processNotification.setIndStatus(GeneralConstants.ONE_VALUE_INTEGER);
			processNotification.setStateDescription(applicationConfiguration.getProperty("status.indicator.one"));
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_ACTIVE_SUCC,
							new Object[]{processNotification.getIdProcessNotificationPk().toString()}));
			//message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ACTIVE_NOITIFICATION_SUCC, argObj);
		}
		//businessProcessesServiceFacade.updateNotificationStatus(processNotification);
		processNotification = null;
		//getNotifications(businessProcess.getIdBusinessProcessPk());
		JSFUtilities.showValidationDialog();
		
		//To send Notification Message		
//		List<NotificationMessgaeDetail> internalUsersList = notificationFacade.getInternalUsers();
//	     Long processNotificationId = null;
//	     for(NotificationMessgaeDetail userDetails:internalUsersList){
//	    	 if(Validations.validateIsNullOrNotPositive(processNotificationId)){
//					processNotificationId = notificationFacade.saveProcessNotificationDetails(userDetails, message,BusinessProcessType.NOTIFICATIONS_ADD.getCode());
//				}
//	    	 if(deactiveFlag){
//	    		 notificationFacade.SendNotificationMessageServiceFacade(userDetails, message,BusinessProcessType.NOTIFICATIONS_DISACTIVATE.getCode(),processNotificationId);
//	    			}else{
//	    				 notificationFacade.SendNotificationMessageServiceFacade(userDetails, message,BusinessProcessType.NOTIFICATIONS_ACTIVATE.getCode(),processNotificationId);
//	    			}
//	    	
//			}
		/*}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}*/
	}
	
	/**
	 * De active Process Notification Confirmation.
	 */
	public void deActiveProcessNitiicationConfirm(){		
		hideDialogs();		
		if(Validations.validateIsNull(processNotification)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}else if(GeneralConstants.ONE_VALUE_INTEGER.equals(processNotification.getIndStatus())){
			deactiveFlag = true;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DEACTIVE)
					, PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_DEACTIVE_CONFIRM,
							new Object[]{processNotification.getIdProcessNotificationPk().toString()}));
			JSFUtilities.executeJavascriptFunction("PF('idConfDialogStatusNotiffWin').show();");
		}else if(GeneralConstants.ZERO_VALUE_INTEGER.equals(processNotification.getIndStatus())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.NOTIFICATION_DEACTIVE_ERROR));
			JSFUtilities.showValidationDialog();
		}
	}
	
	/**
	 * Get the deactiveFlag.
	 *
	 * @return the deactiveFlag
	 */
	public boolean isDeactiveFlag() {
		return deactiveFlag;
	}

	/**
	 * Set the deactiveFlag.
	 *
	 * @param deactiveFlag the deactiveFlag to set
	 */
	public void setDeactiveFlag(boolean deactiveFlag) {
		this.deactiveFlag = deactiveFlag;
	}	

	/**
	 * Get the ProcessNotificationDetails.
	 *
	 * @return the processNotificationDetails
	 */
	public ProcessNotification getProcessNotificationDetails() {
		return processNotificationDetails;
	}

	/**
	 * Set the processNotificationDetails.
	 *
	 * @param processNotificationDetails the processNotificationDetails to set
	 */
	public void setProcessNotificationDetails(ProcessNotification processNotificationDetails) {
		this.processNotificationDetails = processNotificationDetails;
	}
	
	

	/**
	 * Get the Process Notification User.
	 *
	 * @return the processNotificationUser
	 */
	public DestinationNotification getProcessNotificationUser() {
		return processNotificationUser;
	}

	/**
	 * Set the ProcessNotificationUser.
	 *
	 * @param processNotificationUser the processNotificationUser to set
	 */
	public void setProcessNotificationUser(DestinationNotification processNotificationUser) {
		this.processNotificationUser = processNotificationUser;
	}

	/**
	 * To view the ProcessNotification Message details.
	 *
	 * @return true, if is notification list not empty
	 */
	/*public void detailsNotificationMessage(ProcessNotificationTO messageDetail){
		hideDialogs();
		processNotificationDetails = messageDetail.getProcessNotification();
		JSFUtilities.showComponent(":frmProcessData:dlgwProcessNotifDetail");
	}*/
	
	/**
	 * To view the ProcessNotification User details.
	 *
	 * @param userDetail the ProcessNotificationTO type
	 */
	/*public void detailsNotificationUser(ProcessNotificationTO userDetail){
		hideDialogs();	
		processNotificationUser = userDetail.getDestinationNotification();
		JSFUtilities.showComponent(":frmProcessData:dlgwUserNotifDetail");
	}*/

	/**
	 * Checks if is notification list not empty.
	 *
	 * @return the notificationListNotEmpty
	 */
	public boolean isNotificationListNotEmpty() {
		return notificationListNotEmpty;
	}

	/**
	 * Sets the notification list not empty.
	 *
	 * @param notificationListNotEmpty the notificationListNotEmpty to set
	 */
	public void setNotificationListNotEmpty(boolean notificationListNotEmpty) {
		this.notificationListNotEmpty = notificationListNotEmpty;
	}

	/**
	 * Get the ParameterTable details of the corresponding master key.
	 *
	 * @param idDefinition the id definition
	 * @return the List<Object[]> type
	 */
	private List<ParameterTable> getParamtersSecurity(Integer idDefinition){
		List<ParameterTable> lstParamTable = new ArrayList<ParameterTable>();
		ParameterTable paramTable = null;
//		try{
//			//Getting parameter details from the security model
//			List<Object[]> params = businessProcessesRemote.getParameterTable(idDefinition);
//			if(Validations.validateIsNotNull(params)){
//			for (Object[] obj : params) {
//						paramTable = new ParameterTable();
//						paramTable.setParameterTablePk((Integer)obj[0]);
//						paramTable.setParameterName(String.valueOf(obj[1]));
//						lstParamTable.add(paramTable);
//					}
//			}
//			}catch (Exception e) {
//					excepcion.fire(new ExceptionToCatchEvent(e));
//				}
		
			return lstParamTable;
		}
	
	/**
	 * Validate hour format.
	 */
	public void validateHourFormat(){
		if(processSchedule.getScheduleInfo().getScheduleHour()!=null){
			Pattern pat = Pattern.compile("^([0-9]{1,2})|([*]{1,1})|([*]{1,1})([/]{1,1})([0-9]{1,2})");
			Matcher mat = pat.matcher(processSchedule.getScheduleInfo().getScheduleHour());
			
			if (!mat.matches()) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("error.hours.format"));
				JSFUtilities.showSimpleValidationDialog();
				processSchedule.getScheduleInfo().setScheduleHour(GeneralConstants.ASTERISK_STRING);
			}
		}
	}
	
	/**
	 * Validate minute format.
	 */
	public void validateMinuteFormat(){
		if(processSchedule.getScheduleInfo().getScheduleMinute()!=null){
			Pattern pat = Pattern.compile("^([0-9]{1,2})|([*]{1,1})|([*]{1,1})([/]{1,1})([0-9]{1,2})");
			Matcher mat = pat.matcher(processSchedule.getScheduleInfo().getScheduleMinute());
			
			if (!mat.matches()) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("error.minutes.format"));
				JSFUtilities.showSimpleValidationDialog();
				processSchedule.getScheduleInfo().setScheduleMinute(GeneralConstants.ASTERISK_STRING);
			}
		}
	}
	
	/**
	 * Validate second format.
	 */
	public void validateSecondFormat(){
		if(processSchedule.getScheduleInfo().getScheduleSecond()!=null){
			Pattern pat = Pattern.compile("^([0-9]{1,2})|([*]{1,1})|([*]{1,1})([/]{1,1})([0-9]{1,2})");
			Matcher mat = pat.matcher(processSchedule.getScheduleInfo().getScheduleSecond());
			
			if (!mat.matches()) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("error.seconds.format"));
				JSFUtilities.showSimpleValidationDialog();
				processSchedule.getScheduleInfo().setScheduleSecond(GeneralConstants.ASTERISK_STRING);
			}
		}
	}
	
	/**
	 * Validate year format.
	 */
	public void validateYearFormat(){
		if(processSchedule.getScheduleInfo().getScheduleYear()!=null){
			Pattern pat = Pattern.compile("^([0-9]{4,4})|([*]{1,1})");
			Matcher mat = pat.matcher(processSchedule.getScheduleInfo().getScheduleYear());
			
			if (!mat.matches()) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("error.year.format"));
				JSFUtilities.showSimpleValidationDialog();
				processSchedule.getScheduleInfo().setScheduleYear(GeneralConstants.ASTERISK_STRING);
			}
		}
	}

	
	/**
	 * Gets the indicator notification.
	 *
	 * @return the indicator notification
	 */
	public Integer getIndicatorNotification() {
		return indicatorNotification;
	}

	/**
	 * Sets the indicator notification.
	 *
	 * @param indicatorNotification the new indicator notification
	 */
	public void setIndicatorNotification(Integer indicatorNotification) {
		this.indicatorNotification = indicatorNotification;
	}

	/**
	 * Gets the business process selected.
	 *
	 * @return the business process selected
	 */
	public BusinessProcess getBusinessProcessSelected() {
		return businessProcessSelected;
	}

	/**
	 * Sets the business process selected.
	 *
	 * @param businessProcessSelected the new business process selected
	 */
	public void setBusinessProcessSelected(BusinessProcess businessProcessSelected) {
		this.businessProcessSelected = businessProcessSelected;
	}

	/**
	 * Gets the state list.
	 *
	 * @return the state list
	 */
	public List<SelectItem> getStateList() {
		return stateList;
	}

	/**
	 * Sets the state list.
	 *
	 * @param stateList the new state list
	 */
	public void setStateList(List<SelectItem> stateList) {
		this.stateList = stateList;
	}

	/**
	 * Gets the type process list.
	 *
	 * @return the type process list
	 */
	public List<SelectItem> getTypeProcessList() {
		return typeProcessList;
	}

	/**
	 * Sets the type process list.
	 *
	 * @param typeProcessList the new type process list
	 */
	public void setTypeProcessList(List<SelectItem> typeProcessList) {
		this.typeProcessList = typeProcessList;
	}

	/**
	 * Gets the module list.
	 *
	 * @return the module list
	 */
	public List<SelectItem> getModuleList() {
		return moduleList;
	}

	/**
	 * Sets the module list.
	 *
	 * @param moduleList the new module list
	 */
	public void setModuleList(List<SelectItem> moduleList) {
		this.moduleList = moduleList;
	}
	
	/**
	 * Gets the id notification pk.
	 *
	 * @return the idNotificationPk
	 */
	public Long getIdNotificationPk() {
		return idNotificationPk;
	}

	/**
	 * Sets the id notification pk.
	 *
	 * @param idNotificationPk the idNotificationPk to set
	 */
	public void setIdNotificationPk(Long idNotificationPk) {
		this.idNotificationPk = idNotificationPk;
	}	


	/**
	 * Checks if is institucion.
	 *
	 * @return boolean type
	 */
	public boolean isInstitucion() {
		return institucion;
	}

	/**
	 * Sets the institucion.
	 *
	 * @param institucion the new institucion
	 */
	public void setInstitucion(boolean institucion) {
		this.institucion = institucion;
	}

	/**
	 * Gets the lst entities.
	 *
	 * @return lstEntities
	 */
	public List<BeanTypeBusiness> getLstEntities() {
		return lstEntities;
	}

	/**
	 * Sets the lst entities.
	 *
	 * @param lstEntities the new lst entities
	 */
	public void setLstEntities(List<BeanTypeBusiness> lstEntities) {
		this.lstEntities = lstEntities;
	}

	/**
	 * Gets the institution selected type.
	 *
	 * @return institutionSelectedType
	 */
	public Integer getInstitutionSelectedType() {
		return institutionSelectedType;
	}

	/**
	 * Sets the institution selected type.
	 *
	 * @param institutionSelectedType the new institution selected type
	 */
	public void setInstitutionSelectedType(Integer institutionSelectedType) {
		this.institutionSelectedType = institutionSelectedType;
	}

	/**
	 * Gets the validation.
	 *
	 * @return validation
	 */
	public Integer getValidation() {
		return validation;
	}

	/**
	 * Sets the validation.
	 *
	 * @param validation the new validation
	 */
	public void setValidation(Integer validation) {
		this.validation = validation;
	}	
	

	/**
	 * Gets the lst institution type.
	 *
	 * @return the lstInstitutionType
	 */
	public List<ParameterTable> getLstInstitutionType() {
		return lstInstitutionType;
	}

	/**
	 * Sets the lst institution type.
	 *
	 * @param lstInstitutionType the lstInstitutionType to set
	 */
	public void setLstInstitutionType(List<ParameterTable> lstInstitutionType) {
		this.lstInstitutionType = lstInstitutionType;
	}

	

	/**
	 * Checks if is rendered button.
	 *
	 * @return renderedButton
	 */
	public boolean isRenderedButton() {
		return renderedButton;
	}

	/**
	 * Sets the rendered button.
	 *
	 * @param renderedButton the new rendered button
	 */
	public void setRenderedButton(boolean renderedButton) {
		this.renderedButton = renderedButton;
	}

	
	/**
	 * Gets the process types.
	 *
	 * @return the processTypes
	 */
	public List<ProcessType> getProcessTypes() {
		return processTypes;
	}

	/**
	 * Sets the process types.
	 *
	 * @param processTypes the processTypes to set
	 */
	public void setProcessTypes(List<ProcessType> processTypes) {
		this.processTypes = processTypes;
	}

	/**
	 * Gets the type selected.
	 *
	 * @return typeSelected
	 */
	public Integer getTypeSelected() {
		return typeSelected;
	}

	/**
	 * Sets the type selected.
	 *
	 * @param typeSelected the new type selected
	 */
	public void setTypeSelected(Integer typeSelected) {
		this.typeSelected = typeSelected;
	}

	/**
	 * Gets the negocio selected.
	 *
	 * @return negocioSelected
	 */ 
	public String getNegocioSelected() {
		return negocioSelected;
	}

	/**
	 * Sets the negocio selected.
	 *
	 * @param negocioSelected the new negocio selected
	 */
	public void setNegocioSelected(String negocioSelected) {
		this.negocioSelected = negocioSelected;
	}

	/**
	 * Gets the mnemonic selected.
	 *
	 * @return mnemonicSelected
	 */
	public String getMnemonicSelected() {
		return mnemonicSelected;
	}

	/**
	 * Sets the mnemonic selected.
	 *
	 * @param mnemonicSelected the new mnemonic selected
	 */
	public void setMnemonicSelected(String mnemonicSelected) {
		this.mnemonicSelected = mnemonicSelected;
	}

	/**
	 * Gets the macro process selected.
	 *
	 * @return macroProcessSelected
	 */
	public Long getMacroProcessSelected() {
		return macroProcessSelected;
	}

	/**
	 * Sets the macro process selected.
	 *
	 * @param macroProcessSelected the new macro process selected
	 */
	public void setMacroProcessSelected(Long macroProcessSelected) {
		this.macroProcessSelected = macroProcessSelected;
	}

	/**
	 * Gets the list macro process.
	 *
	 * @return listMacroProcess
	 */
	public List<BeanTypeBusiness> getListMacroProcess() {
		return listMacroProcess;
	}

	/**
	 * Sets the list macro process.
	 *
	 * @param listMacroProcess the new list macro process
	 */
	public void setListMacroProcess(List<BeanTypeBusiness> listMacroProcess) {
		this.listMacroProcess = listMacroProcess;
	}

	/**
	 * Gets the level1 selected.
	 *
	 * @return level1Selected
	 */
	public Long getLevel1Selected() {
		return level1Selected;
	}

	/**
	 * Sets the level1 selected.
	 *
	 * @param level1Selected the new level1 selected
	 */
	public void setLevel1Selected(Long level1Selected) {
		this.level1Selected = level1Selected;
	}

	/**
	 * Gets the list level1.
	 *
	 * @return listLevel1
	 */
	public List<BeanTypeBusiness> getListLevel1() {
		return listLevel1;
	}

	/**
	 * Sets the list level1.
	 *
	 * @param listLevel1 the new list level1
	 */
	public void setListLevel1(List<BeanTypeBusiness> listLevel1) {
		this.listLevel1 = listLevel1;
	}

	/**
	 * Gets the level2 selected.
	 *
	 * @return level2Selected
	 */
	public Long getLevel2Selected() {
		return level2Selected;
	}

	/**
	 * Sets the level2 selected.
	 *
	 * @param level2Selected the new level2 selected
	 */
	public void setLevel2Selected(Long level2Selected) {
		this.level2Selected = level2Selected;
	}

	/**
	 * Gets the list level2.
	 *
	 * @return listLevel2
	 */
	public List<BeanTypeBusiness> getListLevel2() {
		return listLevel2;
	}

	/**
	 * Sets the list level2.
	 *
	 * @param listLevel2 the new list level2
	 */
	public void setListLevel2(List<BeanTypeBusiness> listLevel2) {
		this.listLevel2 = listLevel2;
	}
	

	
	/**
	 * Gets the business process.
	 *
	 * @return businessProcess
	 */
	public BusinessProcessTO getBusinessProcess() {
		return businessProcess;
	}

	/**
	 * Sets the business process.
	 *
	 * @param businessProcess the new business process
	 */
	public void setBusinessProcess(BusinessProcessTO businessProcess) {
		this.businessProcess = businessProcess;
	}

	

	/**
	 * Gets the business process data model.
	 *
	 * @return the businessProcessDataModel
	 */
	public GenericDataModel<BusinessProcessTO> getBusinessProcessDataModel() {
		return businessProcessDataModel;
	}

	/**
	 * Sets the business process data model.
	 *
	 * @param businessProcessDataModel the businessProcessDataModel to set
	 */
	public void setBusinessProcessDataModel(
			GenericDataModel<BusinessProcessTO> businessProcessDataModel) {
		this.businessProcessDataModel = businessProcessDataModel;
	}

	/**
	 * Gets the lst business processes.
	 *
	 * @return lstBusinessProcesses
	 */
	public List<BusinessProcessTO> getLstBusinessProcesses() {
		return lstBusinessProcesses;
	}

	/**
	 * Sets the lst business processes.
	 *
	 * @param lstBusinessProcesses the new lst business processes
	 */
	public void setLstBusinessProcesses(
			List<BusinessProcessTO> lstBusinessProcesses) {
		this.lstBusinessProcesses = lstBusinessProcesses;
	}

	/**
	 * Checks if is form state.
	 *
	 * @return the formState
	 */
	public boolean isFormState() {
		return formState;
	}

	/**
	 * Sets the form state.
	 *
	 * @param formState the formState to set
	 */
	public void setFormState(boolean formState) {
		this.formState = formState;
	}

	/**
	 * Checks if is modify.
	 *
	 * @return the modify
	 */
	public boolean isModify() {
		return modify;
	}

	/**
	 * Sets the modify.
	 *
	 * @param modify the modify to set
	 */
	public void setModify(boolean modify) {
		this.modify = modify;
	}
	
	/**
	 * Clean.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}

	/**
	 * Gets the process schedule.
	 *
	 * @return the processSchedule
	 */
	public ProcessSchedule getProcessSchedule() {
		return processSchedule;
	}

	/**
	 * Sets the process schedule.
	 *
	 * @param processSchedule the processSchedule to set
	 */
	public void setProcessSchedule(ProcessSchedule processSchedule) {
		this.processSchedule = processSchedule;
	}

	/**
	 * Checks if is schedule list not empty.
	 *
	 * @return the scheduleListNotEmpty
	 */
	public boolean isScheduleListNotEmpty() {
		return scheduleListNotEmpty;
	}

	/**
	 * Sets the schedule list not empty.
	 *
	 * @param scheduleListNotEmpty the scheduleListNotEmpty to set
	 */
	public void setScheduleListNotEmpty(boolean scheduleListNotEmpty) {
		this.scheduleListNotEmpty = scheduleListNotEmpty;
	}

	
	/**
	 * Gets the institutions type.
	 *
	 * @return the institutions type
	 */
	public Map<String,Integer> getInstitutionsType() {
		return institutionsType;
	}

	/**
	 * Set institutions type.
	 *
	 * @param institutionsType the institutions type
	 */
	public void setInstitutionsType(Map<String,Integer> institutionsType) {
		this.institutionsType = institutionsType;
	}

	/**
	 * Gets the responsabilities type.
	 *
	 * @return the responsabilities type
	 */
	public Map<String,Integer> getResponsabilitiesType() {
		return responsabilitiesType;
	}

	/**
	 * Set responsabilities type.
	 *
	 * @param responsabilitiesType the responsabilities type
	 */
	public void setResponsabilitiesType(Map<String,Integer> responsabilitiesType) {
		this.responsabilitiesType = responsabilitiesType;
	}

	/**
	 * Gets the status type.
	 *
	 * @return the status type
	 */
	public Map<String,Integer> getStatusType() {
		return statusType;
	}

	/**
	 * Set status type.
	 *
	 * @param statusType the status type
	 */
	public void setStatusType(Map<String,Integer> statusType) {
		this.statusType = statusType;
	}

	/**
	 * Gets the list institutions.
	 *
	 * @return the list institutions
	 */
	public List<SelectItem> getListInstitutions() {
		return listInstitutions;
	}

	/**
	 * Sets the list institutions.
	 *
	 * @param listInstitutions the new list institutions
	 */
	public void setListInstitutions(List<SelectItem> listInstitutions) {
		this.listInstitutions = listInstitutions;
	}

	/**
	 * Gets the users accounts.
	 *
	 * @return the users accounts
	 */
	public List<UserAccountSession> getUsersAccounts() {
		return usersAccounts;
	}

	/**
	 * Sets the users accounts.
	 *
	 * @param usersAccounts the new users accounts
	 */
	public void setUsersAccounts(List<UserAccountSession> usersAccounts) {
		this.usersAccounts = usersAccounts;
	}

	/**
	 * Gets the user type.
	 *
	 * @return the user type
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * Sets the user type.
	 *
	 * @param userType the new user type
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Object getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Object institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the user selected.
	 *
	 * @return the user selected
	 */
	public String getUserSelected() {
		return userSelected;
	}

	/**
	 * Sets the user selected.
	 *
	 * @param userSelected the new user selected
	 */
	public void setUserSelected(String userSelected) {
		this.userSelected = userSelected;
	}

	/**
	 * Gets the notifications type.
	 *
	 * @return the notifications type
	 */
	public List<ParameterTable> getNotificationsType() {
		return notificationsType;
	}

	/**
	 * Sets the notifications type.
	 *
	 * @param notificationsType the new notifications type
	 */
	public void setNotificationsType(List<ParameterTable> notificationsType) {
		this.notificationsType = notificationsType;
	}

	/**
	 * Gets the destinations type.
	 *
	 * @return the destinations type
	 */
	public List<ParameterTable> getDestinationsType() {
		return destinationsType;
	}

	/**
	 * Sets the destinations type.
	 *
	 * @param destinationsType the new destinations type
	 */
	public void setDestinationsType(List<ParameterTable> destinationsType) {
		this.destinationsType = destinationsType;
	}

	/**
	 * Gets the destination details.
	 *
	 * @return the destination details
	 */
	public List<DestinationNotification> getDestinationDetails() {
		return destinationDetails;
	}

	/**
	 * Sets the destination details.
	 *
	 * @param destinationDetails the new destination details
	 */
	public void setDestinationDetails(List<DestinationNotification> destinationDetails) {
		this.destinationDetails = destinationDetails;
	}

	/**
	 * Checks if is modify notification.
	 *
	 * @return true, if is modify notification
	 */
	public boolean isModifyNotification() {
		return modifyNotification;
	}

	/**
	 * Sets the modify notification.
	 *
	 * @param modifyNotification the new modify notification
	 */
	public void setModifyNotification(boolean modifyNotification) {
		this.modifyNotification = modifyNotification;
	}

	/**
	 * Checks if is open edit notification.
	 *
	 * @return true, if is open edit notification
	 */
	public boolean isOpenEditNotification() {
		return openEditNotification;
	}

	/**
	 * Sets the open edit notification.
	 *
	 * @param openEditNotification the new open edit notification
	 */
	public void setOpenEditNotification(boolean openEditNotification) {
		this.openEditNotification = openEditNotification;
	}

	/**
	 * Gets the business process detail.
	 *
	 * @return the business process detail
	 */
	public BusinessProcessTO getBusinessProcessDetail() {
		return businessProcessDetail;
	}

	/**
	 * Sets the business process detail.
	 *
	 * @param businessProcessDetail the new business process detail
	 */
	public void setBusinessProcessDetail(BusinessProcessTO businessProcessDetail) {
		this.businessProcessDetail = businessProcessDetail;
	}

	/**
	 * Gets the lst notifications type.
	 *
	 * @return the lst notifications type
	 */
	public List<ParameterTable> getLstNotificationsType() {
		return lstNotificationsType;
	}

	/**
	 * Sets the lst notifications type.
	 *
	 * @param lstNotificationsType the new lst notifications type
	 */
	public void setLstNotificationsType(List<ParameterTable> lstNotificationsType) {
		this.lstNotificationsType = lstNotificationsType;
	}
	
	/**
	 * Gets the profiles.
	 *
	 * @return the profiles
	 */
	public List<SelectItem> getProfiles() {
		return profiles;
	}
	
	/**
	 * Sets the profiles.
	 *
	 * @param profiles the new profiles
	 */
	public void setProfiles(List<SelectItem> profiles) {
		this.profiles = profiles;
	}

	/**
	 * Gets the id system selected.
	 *
	 * @return the id system selected
	 */
	public Integer getIdSystemSelected() {
		return idSystemSelected;
	}

	/**
	 * Sets the id system selected.
	 *
	 * @param idSystemSelected the new id system selected
	 */
	public void setIdSystemSelected(Integer idSystemSelected) {
		this.idSystemSelected = idSystemSelected;
	}

	/**
	 * Gets the lst systems.
	 *
	 * @return the lst systems
	 */
	public List<ComboBoxValueBean> getLstSystems() {
		return lstSystems;
	}

	/**
	 * Sets the lst systems.
	 *
	 * @param lstSystems the new lst systems
	 */
	public void setLstSystems(List<ComboBoxValueBean> lstSystems) {
		this.lstSystems = lstSystems;
	}

	/**
	 * Gets the notification configuration.
	 *
	 * @return the notificationConfiguration
	 */
	public NotificationConfiguration getNotificationConfiguration() {
		return notificationConfiguration;
	}

	/**
	 * Sets the notification configuration.
	 *
	 * @param notificationConfiguration the notificationConfiguration to set
	 */
	public void setNotificationConfiguration(
			NotificationConfiguration notificationConfiguration) {
		this.notificationConfiguration = notificationConfiguration;
	}

	/**
	 * Gets the lst notification configuration.
	 *
	 * @return the lstNotificationConfiguration
	 */
	public List<NotificationConfiguration> getLstNotificationConfiguration() {
		return lstNotificationConfiguration;
	}

	/**
	 * Sets the lst notification configuration.
	 *
	 * @param lstNotificationConfiguration the lstNotificationConfiguration to set
	 */
	public void setLstNotificationConfiguration(
			List<NotificationConfiguration> lstNotificationConfiguration) {
		this.lstNotificationConfiguration = lstNotificationConfiguration;
	}

	/**
	 * Gets the list priority.
	 *
	 * @return the listPriority
	 */
	public List<BooleanType> getListPriority() {
		return listPriority;
	}

	/**
	 * Sets the list priority.
	 *
	 * @param listPriority the listPriority to set
	 */
	public void setListPriority(List<BooleanType> listPriority) {
		this.listPriority = listPriority;
	}
	
}
