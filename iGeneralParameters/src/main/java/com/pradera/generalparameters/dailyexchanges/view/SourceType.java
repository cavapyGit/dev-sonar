package com.pradera.generalparameters.dailyexchanges.view;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum SourceType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public enum SourceType {
	
	/** The definition. */
	DEFINITION(new Integer(1),"SOURCE TYPE1"),
	
	/** The element. */
	ELEMENT(new Integer(2),"SOURCE TYPE2");	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<SourceType> list = new ArrayList<SourceType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SourceType> lookup = new HashMap<Integer, SourceType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<SourceType> listSomeElements(SourceType... transferSecuritiesTypeParams){
		List<SourceType> retorno = new ArrayList<SourceType>();
		for(SourceType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (SourceType s : EnumSet.allOf(SourceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new source type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private SourceType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
