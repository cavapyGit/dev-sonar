package com.pradera.generalparameters.dailyexchanges.batch;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.generalparameters.dailyexchanges.facade.DailyExchangesFacade;
import com.pradera.integration.component.generalparameters.to.MoneyExchangeTO;
import com.pradera.model.generalparameter.type.CurrencyWSType;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ElectronicCatWebRegisterBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="ExchangeMoneyBatch")
@RequestScoped
public class ExchangeMoneyBatch extends GenericBaseBean implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The daily exchange facade. */
	@EJB
	DailyExchangesFacade dailyExchangeFacade ;
	
	@Inject
	private PraderaLogger log;
	
	

	/* (non-Javadoc)
	 * Batchero para registrar cat electronicos en la BD de la web
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		log.info("************************************* BATCH REGISTRO DE TIPO DE CAMBIO BCB  ******************************************************");
		String date;
		Date calendarDate = CommonsUtilities.currentDate(); 
		Calendar calendario = new GregorianCalendar();
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		
		List<Integer> lstCodMoneda=new ArrayList<>();
		lstCodMoneda.add(CurrencyWSType.USDC.getCode()); 
		lstCodMoneda.add(CurrencyWSType.EU.getCode());
		lstCodMoneda.add(CurrencyWSType.UFV.getCode()); 

		if(hora>=18){ //Si la hora es 6 pm entonces registra del dia siguiente
			calendarDate = CommonsUtilities.addDate(calendarDate, 1);
		}
		
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
			date = sdf.format(calendarDate); //Fecha actual de date a string	
			MoneyExchangeTO moneyExchangeTO = new MoneyExchangeTO();
				
				for (int i = 0; i < lstCodMoneda.size(); i++) {
					moneyExchangeTO.setCurrencyCode(lstCodMoneda.get(i).intValue());
					moneyExchangeTO.setStrDate(date);
					dailyExchangeFacade.moneyExchangeSend(moneyExchangeTO);
				}
			}catch(Exception e){
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			
		System.out.println("************************************* BATCH REGISTRO DE TIPO DE CAMBIO BCB ******************************************************");

	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
