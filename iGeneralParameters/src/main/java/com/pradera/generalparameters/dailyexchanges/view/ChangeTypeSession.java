package com.pradera.generalparameters.dailyexchanges.view;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeTypeSession.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class ChangeTypeSession implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The creation date. */
	private Date creationDate;
	
	/** The b price. */
	private Double bPrice;
	
	/** The s price. */
	private Double sPrice;
	
	/** The a price. */
	private Double aPrice;
	
	/** The source. */
	private int source;
	
	/** The currency type. */
	private int currencyType;
	
	/** The currency. */
	private int currency;
	
	/** The last modify ip. */
	private String lastModifyIp;
	
	/** The last modify date. */
	private Date lastModifyDate;
	
	/** The last modify user. */
	private String lastModifyUser;
	
	/** The last modify app. */
	private int lastModifyApp; 
	
	/** The id daily exchangepk. */
	private int idDailyExchangepk;

	/**
	 * Gets the creation date.
	 *
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public int getSource() {
		return source;
	}

	/**
	 * Sets the source.
	 *
	 * @param source the source to set
	 */
	public void setSource(int source) {
		this.source = source;
	}

	/**
	 * Gets the currency type.
	 *
	 * @return the currencyType
	 */
	public int getCurrencyType() {
		return currencyType;
	}

	/**
	 * Sets the currency type.
	 *
	 * @param currencyType the currencyType to set
	 */
	public void setCurrencyType(int currencyType) {
		this.currencyType = currencyType;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public int getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(int currency) {
		this.currency = currency;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the b price.
	 *
	 * @return the bPrice
	 */
	public Double getbPrice() {
		return bPrice;
	}

	/**
	 * Sets the b price.
	 *
	 * @param bPrice the bPrice to set
	 */
	public void setbPrice(Double bPrice) {
		this.bPrice = bPrice;
	}

	/**
	 * Gets the s price.
	 *
	 * @return the sPrice
	 */
	public Double getsPrice() {
		return sPrice;
	}

	/**
	 * Sets the s price.
	 *
	 * @param sPrice the sPrice to set
	 */
	public void setsPrice(Double sPrice) {
		this.sPrice = sPrice;
	}

	/**
	 * Gets the a price.
	 *
	 * @return the aPrice
	 */
	public Double getaPrice() {
		return aPrice;
	}

	/**
	 * Sets the a price.
	 *
	 * @param aPrice the aPrice to set
	 */
	public void setaPrice(Double aPrice) {
		this.aPrice = aPrice;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public int getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(int lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the id daily exchangepk.
	 *
	 * @return the idDailyExchangepk
	 */
	public int getIdDailyExchangepk() {
		return idDailyExchangepk;
	}

	/**
	 * Sets the id daily exchangepk.
	 *
	 * @param idDailyExchangepk the idDailyExchangepk to set
	 */
	public void setIdDailyExchangepk(int idDailyExchangepk) {
		this.idDailyExchangepk = idDailyExchangepk;
	}
	
}
