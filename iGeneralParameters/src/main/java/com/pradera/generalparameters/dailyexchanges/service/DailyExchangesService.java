package com.pradera.generalparameters.dailyexchanges.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.dailyexchanges.view.filter.DailyExchangeRateFilter;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.RequestDailyExchangeRates;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DailyExchangesService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DailyExchangesService extends CrudDaoServiceBean{	
	
	/**
	 * Instantiates a new daily exchanges service.
	 */
	public DailyExchangesService(){
		super();
	}
	
	/**
	 * To register ChangeTypeSession in DailyExchangerates .
	 *
	 * @param requestDailyExchangerate the request daily exchangerate
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer registerDailyExchangesFacade(RequestDailyExchangeRates requestDailyExchangerate)throws ServiceException{
		create(requestDailyExchangerate);
		return requestDailyExchangerate.getIdRequestDailyExchangepk();
	}
	
	/**
	 * To register ChangeTypeSession in DailyExchangerates .
	 *
	 * @param requestDailyExchangerate the request daily exchangerate
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer registerDailyExchanges(DailyExchangeRates dailyExchangeRates)throws ServiceException{
		create(dailyExchangeRates);
		return dailyExchangeRates.getIddailyExchangepk();
	}
	
	
	
	/**
	 * To Update ChangeTypeSession in DailyExchangerates.
	 *
	 * @param requestDailyExchangerate the request daily exchangerate
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	/*public int updateDailyExchangeService(DailyExchangeRates dailyExchangerate)throws ServiceException{
		int retVal = 0;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Update DailyExchangeRates d set");
		sbQuery.append(" d.sellPrice = :sellPrice, ");
		sbQuery.append(" d.buyPrice = :buyPrice, "); 
		sbQuery.append(" d.averagePrice = :averagePrice, "); 
		sbQuery.append(" d.referencePrice = :referencePrice, "); 
		sbQuery.append(" d.lastModifyApp = :lastModifyApp, "); 
		sbQuery.append(" d.lastModifyIp = :lastModifyIp, "); 
		sbQuery.append(" d.lastModifyUser = :lastModifyUser, "); 
		sbQuery.append(" d.lastModifyDate = :lastModifyDate "); 
		
		sbQuery.append(" Where d.iddailyExchangepk = :iddailyExchangepk  ");
		/*sbQuery.append("and d.idSourceinformation = :source and d.dateRate = :creationDate and");
		sbQuery.append(" d.idCurrency = :currencyType, ");*/
		/*Query query = em.createQuery(sbQuery.toString());
		query.setParameter("sellPrice",dailyExchangerate.getSellPrice());
		query.setParameter("buyPrice",dailyExchangerate.getBuyPrice());
		query.setParameter("averagePrice",dailyExchangerate.getAveragePrice());
		query.setParameter("referencePrice",dailyExchangerate.getReferencePrice());
		
		query.setParameter("lastModifyApp",dailyExchangerate.getLastModifyApp());
		query.setParameter("lastModifyIp",dailyExchangerate.getLastModifyIp());
		query.setParameter("lastModifyUser",dailyExchangerate.getLastModifyUser());
		query.setParameter("lastModifyDate",dailyExchangerate.getLastModifyDate());
		
		
		query.setParameter("iddailyExchangepk",dailyExchangerate.getIddailyExchangepk());
		*/
		/*query.setParameter("source",changeTypeSession.getSource());
		query.setParameter("creationDate",changeTypeSession.getCreationDate());		
		query.setParameter("currencyType",changeTypeSession.getCurrencyType());*/
		/*retVal = query.executeUpdate();
		return retVal;*/
	/*}*/
	/**
	 * To search ChangeTypeSession in DailyExchangerates 
	 * @param changeTypeSession
	 * @throws ServiceException the service exception
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<RequestDailyExchangeRates> searchExchangeMoneyTypeRandomListService(DailyExchangeRateFilter requestDailyExchangerate)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<RequestDailyExchangeRates> dailyExchangeList = null;
		sbQuery.append(" select d From RequestDailyExchangeRates d  Where");
		sbQuery.append(" d.idSourceinformation = :source ");
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getIdCurrency())
				&& requestDailyExchangerate.getIdCurrency() > 0) {
		sbQuery.append("and d.idCurrency = :currencyType");
		}
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getDateRate())) {
		sbQuery.append(" and d.dateRate = :creationDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getInitialDate())){
			sbQuery.append(" And d.dateRate >= :initialDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getFinalDate())){
			sbQuery.append(" And d.dateRate <= :finalDate");
		}
		sbQuery.append(" Order By d.dateRate,d.idRequestDailyExchangepk ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("source",requestDailyExchangerate.getIdSourceinformation());	
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getIdCurrency())
				&& requestDailyExchangerate.getIdCurrency() > 0) {
		query.setParameter("currencyType",requestDailyExchangerate.getIdCurrency());
		}
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getDateRate())) {
		query.setParameter("creationDate",requestDailyExchangerate.getDateRate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getInitialDate())){
			query.setParameter("initialDate", requestDailyExchangerate.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getFinalDate())){
			query.setParameter("finalDate", requestDailyExchangerate.getFinalDate());
		}
		
		dailyExchangeList = query.getResultList();
		
		return dailyExchangeList;
	}
	
	/**
	 * To validate the duplicate record.
	 *
	 * @param requestDailyExchangerate the request daily exchangerate
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long searchFiltersDataService(RequestDailyExchangeRates requestDailyExchangerate) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Long count= 0L;
		sbQuery.append(" select count(*) From RequestDailyExchangeRates d  Where");
		sbQuery.append(" d.idSourceinformation = :source ");
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getIdCurrency())
				&& requestDailyExchangerate.getIdCurrency() > 0) {
		sbQuery.append("and d.idCurrency = :currencyType");
		}
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getDateRate())) {
		sbQuery.append(" and d.dateRate = :creationDate");
		}	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("source",requestDailyExchangerate.getIdSourceinformation());	
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getIdCurrency())
				&& requestDailyExchangerate.getIdCurrency() > 0) {
		query.setParameter("currencyType",requestDailyExchangerate.getIdCurrency());
		}
		if (Validations.validateIsNotNullAndNotEmpty(requestDailyExchangerate.getDateRate())) {
		query.setParameter("creationDate",requestDailyExchangerate.getDateRate());
		}
		count = (Long)query.getSingleResult();
		return count;
	}
	
	/**
	 * Exist exchange.
	 *
	 * @param requestDailyExchangeRate the request daily exchange rate
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean existExchange(RequestDailyExchangeRates requestDailyExchangeRate) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT der FROM RequestDailyExchangeRates der");
			sbQuery.append("	WHERE der.idCurrency = :idCurrency");
			sbQuery.append("	AND der.idSourceinformation = :idSourceinformation");
			sbQuery.append("	AND TRUNC(der.dateRate) = TRUNC(:dateRate)");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idCurrency", requestDailyExchangeRate.getIdCurrency());
			query.setParameter("idSourceinformation", requestDailyExchangeRate.getIdSourceinformation());
			query.setParameter("dateRate", requestDailyExchangeRate.getDateRate());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return true;
		} catch (NoResultException e) {
			return false;
		}
		return false;
	}
}
