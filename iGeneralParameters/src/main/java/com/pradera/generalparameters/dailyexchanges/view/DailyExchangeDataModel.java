package com.pradera.generalparameters.dailyexchanges.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.generalparameter.DailyExchangeRates;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DailyExchangeDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class DailyExchangeDataModel extends ListDataModel<DailyExchangeRates> implements SelectableDataModel<DailyExchangeRates>,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8641102324511386295L;
	
	/** The size. */
	private int size;

	/**
	 * Instantiates a new daily exchange data model.
	 */
	public DailyExchangeDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new daily exchange data model.
	 *
	 * @param data the data
	 */
	public DailyExchangeDataModel(List<DailyExchangeRates> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public DailyExchangeRates getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<DailyExchangeRates> dailyExchangeRates=(List<DailyExchangeRates>)getWrappedData();
		if(dailyExchangeRates != null){
	        for(DailyExchangeRates dailyExchangeRate : dailyExchangeRates) {  
	            if(String.valueOf(dailyExchangeRate.getIddailyExchangepk()).equals(rowKey))
	                return dailyExchangeRate;  
	        }  
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(DailyExchangeRates dailyExchangeRates) {
		// TODO Auto-generated method stub
		return dailyExchangeRates.getIddailyExchangepk();
	}

	/**
	 * Gets the size.
	 *
	 * @return size
	 */
	public int getSize() {
		List<DailyExchangeRates> dailyExchangeRates=(List<DailyExchangeRates>)getWrappedData();
		if(dailyExchangeRates==null)
			return 0;
		return dailyExchangeRates.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
}
