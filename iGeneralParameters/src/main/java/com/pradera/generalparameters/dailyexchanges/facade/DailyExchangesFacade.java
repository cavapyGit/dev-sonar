package com.pradera.generalparameters.dailyexchanges.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.generalparameters.dailyexchanges.service.DailyExchangesService;
import com.pradera.generalparameters.dailyexchanges.view.filter.DailyExchangeRateFilter;
import com.pradera.integration.component.generalparameters.to.MoneyExchangeTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.RequestDailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRatesType;
import com.pradera.webclient.ParametersServiceConsumer;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class DailyExchangesFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/01/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class DailyExchangesFacade{
	
	/** The daily exchanges service. */
	@EJB
	DailyExchangesService dailyExchangesService;
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	@EJB
	ParametersServiceConsumer parametersServiceConsumer;
	
	/**
	 * Instantiates a new daily exchanges facade.
	 */
	public DailyExchangesFacade(){
		super();
	}
	
	/**
	 * To register ChangeTypeSession in DailyExchangerates.
	 *
	 * @param changeTypeSession the change type session
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.EXCHANGE_RATE_REGISTER)
	public Integer registerDailyExchangesFacade(RequestDailyExchangeRates changeTypeSession)throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());      
        changeTypeSession.setAudit(loggerUser);
        if(dailyExchangesService.existExchange(changeTypeSession)){
        	return null;
        }
		return dailyExchangesService.registerDailyExchangesFacade(changeTypeSession);
	}

	/**
	 * Confirm daily request.
	 *
	 * @param selectedDailyExchangeRates the selected daily exchange rates
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.EXCHANGE_RATE_REGISTRATION)
	public Boolean confirmDailyRequest(RequestDailyExchangeRates selectedDailyExchangeRates) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		DailyExchangeRates dailyExchangeRates = new DailyExchangeRates();
			dailyExchangeRates.setAveragePrice(selectedDailyExchangeRates.getAveragePrice());
			dailyExchangeRates.setBuyPrice(selectedDailyExchangeRates.getBuyPrice());
			dailyExchangeRates.setDateRate(selectedDailyExchangeRates.getDateRate());
			dailyExchangeRates.setIdSourceinformation(selectedDailyExchangeRates.getIdSourceinformation());
			dailyExchangeRates.setIndReferencePrice(selectedDailyExchangeRates.getIndReferencePrice());
			dailyExchangeRates.setReferencePrice(selectedDailyExchangeRates.getReferencePrice());
			dailyExchangeRates.setSellPrice(selectedDailyExchangeRates.getSellPrice());
			dailyExchangeRates.setIdCurrency(selectedDailyExchangeRates.getIdCurrency());
			dailyExchangeRates.setAudit(loggerUser);
			dailyExchangesService.create(dailyExchangeRates);
			if (selectedDailyExchangeRates.getIdCurrency().equals(CurrencyType.USD.getCode())){
				DailyExchangeRates dailyExchangeRatesDMV = new DailyExchangeRates();
				dailyExchangeRatesDMV.setAveragePrice(selectedDailyExchangeRates.getAveragePrice());
				dailyExchangeRatesDMV.setBuyPrice(selectedDailyExchangeRates.getBuyPrice());
				dailyExchangeRatesDMV.setDateRate(selectedDailyExchangeRates.getDateRate());
				dailyExchangeRatesDMV.setIdSourceinformation(selectedDailyExchangeRates.getIdSourceinformation());
				dailyExchangeRatesDMV.setIndReferencePrice(selectedDailyExchangeRates.getIndReferencePrice());
				dailyExchangeRatesDMV.setReferencePrice(selectedDailyExchangeRates.getReferencePrice());
				dailyExchangeRatesDMV.setSellPrice(selectedDailyExchangeRates.getSellPrice());
				dailyExchangeRatesDMV.setIdCurrency(CurrencyType.DMV.getCode());
				dailyExchangeRatesDMV.setAudit(loggerUser);
				dailyExchangesService.create(dailyExchangeRatesDMV);
			}
			RequestDailyExchangeRates requestDailyExchangeRates = selectedDailyExchangeRates;
			requestDailyExchangeRates.setConfirmUser(loggerUser.getUserName());
			requestDailyExchangeRates.setConfirmDate(CommonsUtilities.currentDate());
			requestDailyExchangeRates.setExchangeState(DailyExchangeRatesType.CONFIRM.getCode());
			dailyExchangesService.update(requestDailyExchangeRates);	
		return true;
	}
	
	
	/**
	 * To Update ChangeTypeSession in DailyExchangerates.
	 *
	 * @param changeTypeSession the change type session
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.EXCHANGE_RATE_MODIFICATION)
	public int updateDailyExchangefacdae(RequestDailyExchangeRates changeTypeSession)throws ServiceException{	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		dailyExchangesService.update(changeTypeSession);
		return GeneralConstants.ONE_VALUE_INTEGER;
	}
	
	/**
	 * To Search ChangeTypeSession in DailyExchangerates.
	 *
	 * @param requestDailyExchangerate the request daily exchangerate
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXCHANGE_RATE_QUERY)
	public List<RequestDailyExchangeRates> searchExchangeMoneyTypeRandomListFacade(DailyExchangeRateFilter requestDailyExchangerate)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return dailyExchangesService.searchExchangeMoneyTypeRandomListService(requestDailyExchangerate);
	}
	
	/**
	 * To validate the duplicate records.
	 *
	 * @param changeTypeSession the change type session
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long searchFiltersData(RequestDailyExchangeRates changeTypeSession)throws ServiceException{
		return dailyExchangesService.searchFiltersDataService(changeTypeSession);
	}
	
	/**
	 * To validate the duplicate records.
	 *
	 * @param changeTypeSession the change type session
	 * @return 
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	
	@ProcessAuditLogger(process=BusinessProcessType.EXCHANGE_RATE_REGISTRATION)
	public void moneyExchangeSend(MoneyExchangeTO moneyExchangeTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());//issue 63
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());//issue 63
		Integer currencyCode = parametersServiceConsumer.sendExchangeMoney(moneyExchangeTO,loggerUser);
        return;
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.EXCHANGE_RATE_REGISTRATION)
	public Integer moneyExchangeSendTC(MoneyExchangeTO moneyExchangeTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());//issue 63
		Integer currencyCode = parametersServiceConsumer.sendExchangeMoney(moneyExchangeTO,loggerUser);
        return currencyCode;
	}
}
