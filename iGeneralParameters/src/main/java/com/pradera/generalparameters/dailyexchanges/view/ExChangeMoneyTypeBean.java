package com.pradera.generalparameters.dailyexchanges.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.generalparameters.dailyexchanges.facade.DailyExchangesFacade;
import com.pradera.generalparameters.dailyexchanges.view.filter.DailyExchangeRateFilter;
import com.pradera.generalparameters.mastertables.facade.MasterTableFacade;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.generalparameters.to.MoneyExchangeTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.RequestDailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.CurrencyWSType;
import com.pradera.model.generalparameter.type.DailyExchangeRatesType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.webclient.ParametersServiceConsumer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExChangeMoneyTypeBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@LoggerCreateBean
@DepositaryWebBean
public class ExChangeMoneyTypeBean extends GenericBaseBean implements Serializable {
	 
 	/** The Constant serialVersionUID. */
	
	private static final long serialVersionUID = 1L;
	
	/** The daily exchange rates. */
	private DailyExchangeRates dailyExchangeRates;
	//private DailyExchangeRates dailyExchangeRatesAux;
	
	/** The source information name. */
	private String sourceInformationName;
	
	/** The source information selected. */
	private Integer sourceInformationSelected;
	
	/** The source information list. */
	private List<SelectItem> sourceInformationList;
	
	/** The currency list. */
	private List<SelectItem> currencyList;
	
	/** The sell price before initial. */
	//Prices before modification
	private BigDecimal sellPriceBeforeInitial;
	
	/** The buy price before initial. */
	private BigDecimal buyPriceBeforeInitial;
	
	/** The average price before initial. */
	private BigDecimal averagePriceBeforeInitial;
	
	/** The selected daily exchange rates. */
	private RequestDailyExchangeRates selectedDailyExchangeRates;
	
	/** The request daily exchange rates. */
	private RequestDailyExchangeRates requestDailyExchangeRates;
	
	/** The daily exchange facade. */
	@EJB
	DailyExchangesFacade dailyExchangeFacade ;
	
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The parameters facade. */
	@EJB
	GeneralParametersFacade parametersFacade;
	
	/** The master table facade. */
	@EJB
	MasterTableFacade masterTableFacade;
	
	@EJB
	ParametersServiceConsumer parametersServiceConsumer;
	
	/** The excepcion. */
//	@Inject
//	protected Event<ExceptionToCatch> excepcion;
	
	/**  User Information. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	PraderaLogger log;
	
	/**  True. */
	private boolean disbleMoney;
	
	/** The map description money. */
	Map<Integer, String> mapDescriptionMoney;
	
//	private DailyExchangeDataModel dailyExchangeDataModel;
	
	/** The request daily exchange data model. */
private RequestDailyExchangeDataModel requestDailyExchangeDataModel;
	
	/** The daily exchange rate filter. */
	private DailyExchangeRateFilter dailyExchangeRateFilter;

	/** The current date. */
	private Date currentDate = CommonsUtilities.currentDate();
	
	/** The map parameters desc. */
	private Map<Integer, String> mapParametersDesc;
	 
 	/**
 	 * To perform initialization.
 	 */
	
	public ExChangeMoneyTypeBean(){}
	
	/**
	 * Init.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		dailyExchangeRateFilter = new DailyExchangeRateFilter();
		dailyExchangeRateFilter.setInitialDate(CommonsUtilities.currentDate());
		dailyExchangeRateFilter.setFinalDate(CommonsUtilities.currentDate());
		loadInformationSources();
		dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());
		exchangeStatus();
		//privileges button
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userInfo.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);
			privilegeComponent.setBtnConfirmView(true);
		}
		userInfo.setPrivilegeComponent(privilegeComponent);
		selectedDailyExchangeRates = new RequestDailyExchangeRates();
	}
		
		/**
		 * Load information sources.
		 *
		 * @throws ServiceException the service exception
		 */
		@LoggerAuditWeb
		public void loadInformationSources() throws ServiceException{
			try{
				sourceInformationList = new ArrayList<SelectItem>();
				currencyList = new ArrayList<SelectItem>();
				mapDescriptionMoney = new HashMap<Integer, String>();
				ParameterTableTO paramTable = new ParameterTableTO();
				paramTable.setMasterTableFk(MasterTableType.INFORMATION_SOURCE.getCode());
				for(ParameterTable param : parametersFacade.getListParameterTableServiceBean(paramTable)) {
					sourceInformationList.add(new SelectItem(param.getParameterTablePk(), param.getDescription()));
				}
				paramTable = new ParameterTableTO();
				paramTable.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				for(ParameterTable param : parametersFacade.getListParameterTableServiceBean(paramTable)) {
					if(//CurrencyType.UFV.getCode().equals(param.getParameterTablePk()) || 
							CurrencyType.USD.getCode().equals(param.getParameterTablePk()) ||
							CurrencyType.EU.getCode().equals(param.getParameterTablePk()) 
//							||CurrencyType.DMV.getCode().equals(param.getParameterTablePk())
							) {
						currencyList.add(new SelectItem(param.getParameterTablePk(), param.getParameterName()));
						mapDescriptionMoney.put(param.getParameterTablePk(), param.getParameterName());
					}
					
				}
			}
			catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
		
		/**
		 * Before render register.
		 */
		public void beforeRenderRegister(){
			if (!FacesContext.getCurrentInstance().isPostback()) {
//				TODO Beto revisar como afecta esto
//				if(Validations.validateIsNullOrNotPositive(this.getOperationType())){
//					this.setOperationType(GeneralConstants.ONE_VALUE_INTEGER);
//				}
//				if(this.getOperationType()==ViewOperationsType.REGISTER.getCode()){
//					dailyExchangeRates.setDateRate(CommonsUtilities.currentDate());
//				}
			}
		}

	
	/**
	 * Choose reference price.
	 */
	public void chooseReferencePrice(){
		hideDialogs();
		if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getBuyPrice());
		}else if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.TWO_VALUE_INTEGER)){
			this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getSellPrice());
		}else{
			this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getAveragePrice());
		}
	}
	
	/**
	 * 	To register the Data.
	 */
	@LoggerAuditWeb
	public void registerDailyExchangefacade(){
		hideDialogs();
		try{	
			
			Integer exchangeRateId = dailyExchangeFacade.registerDailyExchangesFacade(requestDailyExchangeRates);
			if(Validations.validateIsNullOrEmpty(exchangeRateId)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
						, PropertiesUtilities.getMessage("alt.daily.exchange.exist.exchange"));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				return;
			}
			this.clearRegisterData();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.REGISTER_TYPE_CHANGE_SUCCESS));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			//To send Notification Message
			Object[] param = {exchangeRateId};
			BusinessProcess businessProc = new BusinessProcess();
			businessProc.setIdBusinessProcessPk(BusinessProcessType.EXCHANGE_RATE_REGISTRATION.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null, param);
			
			clearFilterAndResults();
			
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * 	To update the data.
	 */
	@LoggerAuditWeb
	public void updateDailyExchangefacade(){
		hideDialogs();
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		try{
			dailyExchangeFacade.updateDailyExchangefacdae(requestDailyExchangeRates);
			Object[] argObj = new Object[1];
			argObj[0] = requestDailyExchangeRates.getIdRequestDailyExchangepk();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_TYPE_CHANGE_SUCCESS, argObj));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			//To send Notification Message
			Object[] param = {requestDailyExchangeRates.getIdRequestDailyExchangepk()};
			BusinessProcess businessProc = new BusinessProcess();
			businessProc.setIdBusinessProcessPk(BusinessProcessType.EXCHANGE_RATE_MODIFICATION.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null, param);
			
			clearFilterAndResults();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	

	/**
	 * Exchange status.
	 *
	 * @throws ServiceException the service exception
	 */
	private void exchangeStatus() throws ServiceException{
		mapParametersDesc = new HashMap<>();
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setMasterTableFk(MasterTableType.DAILY_EXCHANGE_RATE_STATE.getCode());
		for(ParameterTable pm: parametersFacade.getListParameterTableServiceBean(parameterTO))
		   mapParametersDesc.put(pm.getParameterTablePk(),pm.getDescription());
		}

	/**
	 * TO search the data by passing the filters.
	 */	
	@LoggerAuditWeb
	public void searchExchangeMoneyType(){
		try{
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);	
			hideDialogs();
			requestDailyExchangeDataModel = new RequestDailyExchangeDataModel(dailyExchangeFacade.searchExchangeMoneyTypeRandomListFacade(dailyExchangeRateFilter));	
			setFirsRegisterDataTable(GeneralConstants.ZERO_VALUE_INTEGER);
			selectedDailyExchangeRates = new RequestDailyExchangeRates();
			JSFUtilities.updateComponent("frmConsultaMovTitulares:opnlResult");
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}

	/**
	 * Clear search.
	 */
	public void clearSearch(){
		JSFUtilities.resetViewRoot();
		clearFilterAndResults();
	}
	
	/**
	 * Clear filter and results.
	 */
	public void clearFilterAndResults(){
		requestDailyExchangeRates = new RequestDailyExchangeRates();
		selectedDailyExchangeRates = new RequestDailyExchangeRates();
		dailyExchangeRateFilter = new DailyExchangeRateFilter();
		requestDailyExchangeDataModel = null;	
		dailyExchangeRateFilter.setInitialDate(CommonsUtilities.currentDate());
		dailyExchangeRateFilter.setFinalDate(CommonsUtilities.currentDate());
	}
	
	/**
	 * Clear register data.
	 */
	public void clearRegisterData(){
		JSFUtilities.resetViewRoot();
		hideDialogs();
		//Clear the Registration details
		if(userInfo.getUserAcctions().isRegister()){
		requestDailyExchangeRates = new RequestDailyExchangeRates();
		requestDailyExchangeRates.setIndReferencePrice(GeneralConstants.ONE_VALUE_INTEGER);
		requestDailyExchangeRates.setDateRate(CommonsUtilities.currentDate());
		sourceInformationSelected = InformationSourceType.BCRD.getCode();
		disbleMoney = false;
		}		
	}
	
	
	/**
	 * Confirm daily exchange rates.
	 *
	 * @return String
	 */
	
	public void confirmDailyExchangeRates(){
		hideDialogs();
		if(Validations.validateIsNotNullAndNotEmpty(selectedDailyExchangeRates)){
			if(!selectedDailyExchangeRates.getExchangeState().equals(DailyExchangeRatesType.REGISTER.getCode())){
				JSFUtilities.showValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage("alt.daily.exchange.not.state.confirm"));
				}else{
					Date dateSelection = CommonsUtilities.truncateDateTime(selectedDailyExchangeRates.getDateRate());
					if(dateSelection.before(CommonsUtilities.currentDate())){
						JSFUtilities.showEndTransactionSamePageDialog();
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.MSG_FAIL_MODIFY_EXCHANGERATE, null);
					}else{
						
						ParameterTable paramTable = new ParameterTable();
						try {
							paramTable = masterTableFacade.getElementCode(MasterTableType.INFORMATION_SOURCE.getCode().toString(), 
									selectedDailyExchangeRates.getIdSourceinformation());
							sourceInformationName = paramTable.getDescription();
							JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
							hideDialogs();
						} catch (ServiceException e) {
							sourceInformationName = "BANCO CENTRAL DE BOLIVIA";
						}
						//JSFUtilities.updateComponent("frmConsultaMovTitulares:idcnfdialog");
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
								, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CNF_CONFIRMED_EXCHANGERATE, sourceInformationName));

						JSFUtilities.executeJavascriptFunction("PF('idwConfirmDialog').show();");
				}
			}
		}else{
			JSFUtilities.showValidationDialog();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage("alt.daily.exchange.not.record.selected"));
			}
	}
	
	/**
	 * Confirm daily exchangefacade.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void confirmDailyExchangefacade() throws ServiceException{
		Boolean confDaily =	dailyExchangeFacade.confirmDailyRequest(selectedDailyExchangeRates);
		if (confDaily){
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				    PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_TYPE_CONFIRMED_SUCCESS ));
				  JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * To modify the selected GeographicLocation on modification page.
	 *
	 * @return String
	 */
	public String modifyDailyExchangeRatesListener() {
		hideDialogs();
		if(Validations.validateIsNotNullAndNotEmpty(selectedDailyExchangeRates)){
			if(!selectedDailyExchangeRates.getExchangeState().equals(DailyExchangeRatesType.REGISTER.getCode())){
				JSFUtilities.showValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage("alt.daily.exchange.not.state.register"));
				return null;
			}
			Date dateSelection = CommonsUtilities.truncateDateTime(selectedDailyExchangeRates.getDateRate());
			if(dateSelection.before(CommonsUtilities.currentDate())){
				JSFUtilities.showEndTransactionSamePageDialog();
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.MSG_FAIL_MODIFY_EXCHANGERATE, null);
				return null;
			}
			try {
				requestDailyExchangeRates = new RequestDailyExchangeRates();
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				BeanUtils.copyProperties(requestDailyExchangeRates, selectedDailyExchangeRates);		
				disbleMoney = true;
				//GETTING CORRECT DESCRIPTION FROM INFORMATION SOURCE SELECTED
				ParameterTable paramTable = new ParameterTable();
				paramTable = masterTableFacade.getElementCode(MasterTableType.INFORMATION_SOURCE.getCode().toString(), 
						requestDailyExchangeRates.getIdSourceinformation());
				sourceInformationName = paramTable.getDescription();
				sourceInformationSelected = paramTable.getParameterTablePk();
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			} 
			return "dailyExchangeRatesModify";
		}else{
			JSFUtilities.showValidationDialog();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage("alt.daily.exchange.not.record.selected"));
			return GeneralConstants.EMPTY_STRING;
		}
	}
	

	/**
	 * Change listener.
	 */
	public void changeListener(){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		hideDialogs();
		if(requestDailyExchangeRates.getDateRate()!=null && 
				sourceInformationSelected!=null && requestDailyExchangeRates.getIdCurrency()!=null){
			try{
				//GETTING CORRECT DESCRIPTION FROM INFORMATION SOURCE SELECTED
				ParameterTable paramTable = new ParameterTable();
				paramTable = masterTableFacade.getElementCode(MasterTableType.INFORMATION_SOURCE.getCode().toString(), sourceInformationSelected);
				sourceInformationName = paramTable.getDescription();
				requestDailyExchangeRates.setIdSourceinformation(sourceInformationSelected);
				Long dailyExchangeRate =  dailyExchangeFacade.searchFiltersData(requestDailyExchangeRates);
				if(dailyExchangeRate != 0){
					JSFUtilities.showComponent("frmConsultaMovTitularesMgmt:confDailyExchange");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.daily.exchange.exist.exchange",loadMessage()));
					//dailyExchangeRates = new DailyExchangeRates();
//					requestDailyExchangeRates.setDateRate(CommonsUtilities.currentDate());
				}else{
					JSFUtilities.resetViewRoot();
				}
			}
			catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}else{
			JSFUtilities.resetViewRoot();
		}
	}
	

	
	/**
	 * Calculate referencial price.
	 */
	public void calculateReferencialPrice(){
		if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getBuyPrice());
		}else if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.THREE_VALUE_INTEGER)){
			this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getAveragePrice());
		}else{
			this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getSellPrice());
		}
		
		if(Validations.validateIsNotNull(getRequestDailyExchangeRates().getSellPrice())){
			calculateAveragePrice();
		}
	}
	
	/**
	 * Calculate average price.
	 */
	public void calculateAveragePrice(){
		//SELL Price And  BUY Price of average
		if(Validations.validateIsNotNull(this.getRequestDailyExchangeRates().getSellPrice()) &&
			Validations.validateIsNotNull(this.getRequestDailyExchangeRates().getBuyPrice())){
			this.getRequestDailyExchangeRates().setAveragePrice(this.getRequestDailyExchangeRates().getBuyPrice().add(this.getRequestDailyExchangeRates().getSellPrice()).divide(new BigDecimal(2)));
			if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getBuyPrice());
			}else if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.THREE_VALUE_INTEGER)){
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getAveragePrice());
			}else{
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getSellPrice());
			}
			//SELL Price As average
		}else if(Validations.validateIsNotNull(this.getRequestDailyExchangeRates().getSellPrice())){
			this.getRequestDailyExchangeRates().setAveragePrice(this.getRequestDailyExchangeRates().getSellPrice().divide(new BigDecimal(2)));
			if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getBuyPrice());
			}else if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.THREE_VALUE_INTEGER)){
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getAveragePrice());
			}else{
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getSellPrice());
			}
			//BUY Price As average
		}else if(Validations.validateIsNotNull(this.getRequestDailyExchangeRates().getBuyPrice())){
			this.getRequestDailyExchangeRates().setAveragePrice(this.getRequestDailyExchangeRates().getBuyPrice().divide(new BigDecimal(2)));
			if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getBuyPrice());
			}else if(this.getRequestDailyExchangeRates().getIndReferencePrice().equals(GeneralConstants.THREE_VALUE_INTEGER)){
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getAveragePrice());
			}else{
				this.getRequestDailyExchangeRates().setReferencePrice(this.getRequestDailyExchangeRates().getSellPrice());
			}
		}
	}

	/**
	 * Before save listener. This method is called to save or update a system
	 */
	public void beforeSaveListener() {
			try{
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				hideDialogs();
								
				if((requestDailyExchangeRates.getAveragePrice().compareTo(BigDecimal.ZERO) > 0 &&
						requestDailyExchangeRates.getBuyPrice().compareTo(BigDecimal.ZERO) > 0 && 
						requestDailyExchangeRates.getSellPrice().compareTo(BigDecimal.ZERO) > 0)){
					
					//GETTING CORRECT DESCRIPTION FROM INFORMATION SOURCE SELECTED
					ParameterTable paramTable = new ParameterTable();
					paramTable = masterTableFacade.getElementCode(MasterTableType.INFORMATION_SOURCE.getCode().toString(), sourceInformationSelected);
					sourceInformationName = paramTable.getDescription();
					requestDailyExchangeRates.setIdSourceinformation(sourceInformationSelected);
					requestDailyExchangeRates.setExchangeState(DailyExchangeRatesType.REGISTER.getCode());
					requestDailyExchangeRates.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					requestDailyExchangeRates.setRegistryDate(CommonsUtilities.currentDate());
					Long dailyExchangeRate =  dailyExchangeFacade.searchFiltersData(requestDailyExchangeRates);
					if(dailyExchangeRate != 0){
						JSFUtilities.showComponent("frmConsultaMovTitularesMgmt:confDailyExchange");
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.daily.exchange.exist.exchange"));
						requestDailyExchangeRates.setDateRate(CommonsUtilities.currentDate());
						return;
					}
					Object[] bodyData = new Object[1];
					bodyData[0] = sourceInformationName;
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage("tipocambio.confirm.register", bodyData));
					//JSFUtilities.showComponent("frmConsultaMovTitularesMgmt:idcnfdialog");
					JSFUtilities.executeJavascriptFunction("PF('idwConfirmDialog').show();");
				
				}else{
					JSFUtilities.showValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.daily.exchange.invalid.prices"));
					if(requestDailyExchangeRates.getBuyPrice().compareTo(new BigDecimal(0.0)) == 0){
						JSFUtilities.addContextMessage("frmConsultaMovTitularesMgmt:bPrice",FacesMessage.SEVERITY_ERROR,"",
									PropertiesUtilities.getMessage(PropertiesConstants.CHANGE_MONEY_TYPE_BUYPRICE_ZERO));
					}
					if(requestDailyExchangeRates.getSellPrice().compareTo(new BigDecimal(0.0)) ==0){
						JSFUtilities.addContextMessage("frmConsultaMovTitularesMgmt:sPrice",FacesMessage.SEVERITY_ERROR,"",
									PropertiesUtilities.getMessage(PropertiesConstants.CHANGE_MONEY_TYPE_SELLPRICE_ZERO));
					}					
				}
			}
			catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * Before save listener. This method is called to save or update a system
	 */
	public void beforemodifyListener(){
		try{
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		hideDialogs();
		JSFUtilities.executeJavascriptFunction("PF('idwConfirmDialogModifyWin').show();");
		//JSFUtilities.showComponent("frmConsultaMovTitularesMgmt:idcnfdialogModify");
		Object[] param = {sourceInformationName};
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CNF_MODIFY_EXCHANGERATE, param));
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Setting conf daily exchange to false.
	 */
	public void settingConfDailyExchangeToFalse(){
		if( sourceInformationSelected!=null && requestDailyExchangeRates.getIdCurrency()==null){
			JSFUtilities.hideComponent("frmConsultaMovTitularesMgmt:confDailyExchange");
		}
		clearRegisterData();
	}
	
	/**
	 * Before save listener. This method is called to save or update a system
	 */
	public void beforeSavExchangeMoney() {
		String date;
		Date calendarDate = CommonsUtilities.currentDate();
		Calendar calendario = new GregorianCalendar();
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		int minuto = calendario.get(Calendar.MINUTE);
		try{
			// Si la hora es mayor a 18:30 horas entonces registra del dia siguiente
			if (hora > 18) { 
				do {
					calendarDate = CommonsUtilities.addDate(calendarDate, 1);
				} while (holidayQueryServiceBean.isNonWorkingDayServiceBean(calendarDate, true));
			} else if (hora == 18 && minuto > 30) {
				do {
					calendarDate = CommonsUtilities.addDate(calendarDate, 1);
				} while (holidayQueryServiceBean.isNonWorkingDayServiceBean(calendarDate, true));
			}
		date = CommonsUtilities.convertDateToStringLocaleES(calendarDate, "dd ' de ' MMMM ' de ' yyyy");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage("tipocambio.confirm.register.ws",date));
		JSFUtilities.executeJavascriptFunction("PF('idwConfirmDialogModifyWin').show();");
		//JSFUtilities.showComponent("frmConsultaMovTitulares:idcnfdialogModify");
		//JSFUtilities.updateComponent("frmConsultaMovTitulares:idcnfdialogModify");
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Before save listener. This method is called to save or update a system
	 */
	@SuppressWarnings("null")
	@LoggerAuditWeb
	public void moneyExchangeSend(){
		String date;
		Date calendarDate = CommonsUtilities.currentDate(); 
		Calendar calendario = new GregorianCalendar();
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		int minuto = calendario.get(Calendar.MINUTE);
		
		List<Integer> lstCodMoneda=new ArrayList<>();
		lstCodMoneda.add(CurrencyWSType.USDC.getCode()); 
		lstCodMoneda.add(CurrencyWSType.EU.getCode());
		lstCodMoneda.add(CurrencyWSType.UFV.getCode()); 

		// Si la hora es mayor a 18:30 horas entonces registra del dia siguiente
		if (hora > 18) { 
			do {
				calendarDate = CommonsUtilities.addDate(calendarDate, 1);
			} while (holidayQueryServiceBean.isNonWorkingDayServiceBean(calendarDate, true));
		} else if (hora == 18 && minuto > 30) {
			do {
				calendarDate = CommonsUtilities.addDate(calendarDate, 1);
			} while (holidayQueryServiceBean.isNonWorkingDayServiceBean(calendarDate, true));
		}
		
		List <Integer> lstexchange = new ArrayList<>();
		List <Integer> lstexchangeregistered = new ArrayList<>();
		List <Integer> lstexchangerejected = new ArrayList<>();
		Integer contrejected = 0, contregistered = 0;	
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
			date = sdf.format(calendarDate); //Fecha actual de date a string	
			MoneyExchangeTO moneyExchangeTO = new MoneyExchangeTO();
			for (int i = 0; i < lstCodMoneda.size(); i++) {
				moneyExchangeTO.setCurrencyCode(lstCodMoneda.get(i).intValue());
				moneyExchangeTO.setStrDate(date);
				try {
					Integer exchangeCode = 0; 				
					exchangeCode = dailyExchangeFacade.moneyExchangeSendTC(moneyExchangeTO);
					if (exchangeCode != 0) {
						lstexchange.add(exchangeCode);
					} else {
						lstexchangeregistered.add(lstCodMoneda.get(i).intValue());
						contregistered++;
					}
				} catch (Exception e) {
					log.info(":::::::: no pudo obtener datos del origen: moneda " + lstCodMoneda.get(i).toString());
					lstexchangerejected.add(lstCodMoneda.get(i).intValue());
					contrejected++;					
				}
				
				// se registra tipo de cambio para las UFVs hasta el 10 de cada mes
				if (CurrencyWSType.UFV.getCode().equals(moneyExchangeTO.getCurrencyCode())) {
					int contador = 0;
					sdf = new SimpleDateFormat("dd/MM/yyyy");
					Integer codeMoneyResult = 0;
					for (;;) {
						try {
							calendarDate = CommonsUtilities.addDate(calendarDate, 1);
							date = sdf.format(calendarDate);
							moneyExchangeTO.setStrDate(date);

							codeMoneyResult = dailyExchangeFacade.moneyExchangeSendTC(moneyExchangeTO);

							// si el codeMoneyResult es null se rompe bucle (significa q ya no encontro mas tipo de cambio UFV)
							// si el codeMoneyResult es 0 se rompre blucle (significa que ya se registrO el tipo de cambio para el UFV)
							if (codeMoneyResult == null || codeMoneyResult.equals(Integer.valueOf(0)))
								break;

						} catch (Exception e) {
							e.printStackTrace();
							System.out.println("::: se rompio en la iteracion nro " + contador);
							System.out.println("entro a la excepcion " + e.getMessage());
							break;
						}
					}
				}
			}
			String registered = forwardList(lstexchangeregistered);				
			String rejected = forwardList(lstexchangerejected);					
			String inserted = forwardList(lstexchange);		
			if (lstexchange.size() == 3) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("tipocambio.detalle.webservice.reg") + " USDC, EU, UFV");
						JSFUtilities.showSimpleValidationDialog();
			} else if (contrejected == 3) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("tipocambio.detalle.webservice.disp"));
						JSFUtilities.showSimpleValidationDialog();
			} else if (contregistered == 3) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("tipocambio.detalle.webservice.non"));
						JSFUtilities.showSimpleValidationDialog();
			} else if (lstexchange.size() > 0) {									
				if (lstexchangeregistered.isEmpty()) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("tipocambio.detalle.webservice.reg") + inserted + ", Los siguientes tipos de cambio no estan disponibles: " + rejected);
							JSFUtilities.showSimpleValidationDialog();
				} else if (lstexchangerejected.isEmpty()) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("tipocambio.detalle.webservice.reg") + inserted + ", Los siguientes tipos de cambio ya estaban registrados: " + registered);
							JSFUtilities.showSimpleValidationDialog();
				} 
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, "Los siguientes tipos de cambio ya estaban registrados: " + registered + ", Los siguientes tipos de cambio no estan disponibles: " + rejected);
					JSFUtilities.showSimpleValidationDialog();				
			}
			searchExchangeMoneyType();				
		} catch(Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public String forwardList (List <Integer> lstexchange) {
		String msg = "";
		for (Integer exchangereg : lstexchange) {
			switch (exchangereg) {
			case 34 :
				msg = msg + "USDC ";
				break;
			case 53 :
				msg = msg + "EU ";
				break;
			case 76 :
				msg = msg + "UFV ";
				break;
			default:
				break;					
			}					
		}
		return msg;
	}
	
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs() {		
		JSFUtilities.hideComponent("frmConsultaMovTitularesMgmt:confDailyExchange");
		JSFUtilities.hideComponent("frmConsultaMovTitularesMgmt:cnfEndTransaction");		
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Getting parameter to Message.
	 *
	 * @return the object[]
	 */
	public Object[] loadMessage(){
		Object[] parameters=new Object[3];
		parameters[0]=CommonsUtilities.convertDatetoString(requestDailyExchangeRates.getDateRate());
		parameters[1]=mapDescriptionMoney.get(requestDailyExchangeRates.getIdCurrency());
		parameters[2]=sourceInformationName;
		return parameters;
	}
	
	
	/**
	 * To show the details of the selected location on detail page.
	 *
	 * @param event the event
	 * @return the selected details
	 */
	public void getSelectedDetails(ActionEvent event) {
	selectedDailyExchangeRates=(RequestDailyExchangeRates)event.getComponent().getAttributes().get("ExchangeRateSelected");
	}
	
	//GETTERS AND SETTERS
//	/**
//	 * @return the dailyExchangeDataModel
//	 */
//	public DailyExchangeDataModel getDailyExchangeDataModel() {
//		return dailyExchangeDataModel;
//	}
//	/**
//	 * @param dailyExchangeDataModel the dailyExchangeDataModel to set
//	 */
//	public void setDailyExchangeDataModel(
//			DailyExchangeDataModel dailyExchangeDataModel) {
//		this.dailyExchangeDataModel = dailyExchangeDataModel;
//	}

	/**
	 * Gets the request daily exchange data model.
	 *
	 * @return the request daily exchange data model
	 */
	public RequestDailyExchangeDataModel getRequestDailyExchangeDataModel() {
		return requestDailyExchangeDataModel;
	}

	/**
	 * Sets the request daily exchange data model.
	 *
	 * @param requestDailyExchangeDataModel the new request daily exchange data model
	 */
	public void setRequestDailyExchangeDataModel(
			RequestDailyExchangeDataModel requestDailyExchangeDataModel) {
		this.requestDailyExchangeDataModel = requestDailyExchangeDataModel;
	}

	/**
	 * Gets the daily exchange rate filter.
	 *
	 * @return the dailyExchangeRateFilter
	 */
	public DailyExchangeRateFilter getDailyExchangeRateFilter() {
		return dailyExchangeRateFilter;
	}
	
	/**
	 * Sets the daily exchange rate filter.
	 *
	 * @param dailyExchangeRateFilter the dailyExchangeRateFilter to set
	 */
	public void setDailyExchangeRateFilter(
			DailyExchangeRateFilter dailyExchangeRateFilter) {
		this.dailyExchangeRateFilter = dailyExchangeRateFilter;
	}
	
	/**
	 * Gets the selected daily exchange rates.
	 *
	 * @return the selectedDailyExchangeRates
	 */
	public RequestDailyExchangeRates getSelectedDailyExchangeRates() {
		return selectedDailyExchangeRates;
	}
	
	/**
	 * Sets the selected daily exchange rates.
	 *
	 * @param selectedDailyExchangeRates the selectedDailyExchangeRates to set
	 */
	public void setSelectedDailyExchangeRates(
			RequestDailyExchangeRates selectedDailyExchangeRates) {
		this.selectedDailyExchangeRates = selectedDailyExchangeRates;
	}
	
	/**
	 * To set as max date.
	 *
	 * @return current date
	 */
	public Date getMaxDate(){
		return CommonsUtilities.currentDate();
	}
	/**
	 * Checks if is modify.
	 * 
	 * @return true, if is modify
	 */	
	public boolean isCurrentDateRecord() {
		return this.dailyExchangeRates.getDateRate().compareTo(CommonsUtilities.currentDate()) < 0;
	}
	
	/**
	 * Gets the source information list.
	 *
	 * @return sourceInformationList
	 */
	public List<SelectItem> getSourceInformationList() {
		return sourceInformationList;
	}
	
	/**
	 * Sets the source information list.
	 *
	 * @param sourceInformationList the new source information list
	 */
	public void setSourceInformationList(List<SelectItem> sourceInformationList) {
		this.sourceInformationList = sourceInformationList;
	}
	
	/**
	 * Gets the source information name.
	 *
	 * @return sourceInformationName
	 */
	public String getSourceInformationName() {
		return sourceInformationName;
	}
	
	/**
	 * Sets the source information name.
	 *
	 * @param sourceInformationName the new source information name
	 */
	public void setSourceInformationName(String sourceInformationName) {
		this.sourceInformationName = sourceInformationName;
	}
	
	/**
	 * Gets the source information selected.
	 *
	 * @return sourceInformationSelected
	 */
	public Integer getSourceInformationSelected() {
		return sourceInformationSelected;
	}
	
	/**
	 * Sets the source information selected.
	 *
	 * @param sourceInformationSelected the new source information selected
	 */
	public void setSourceInformationSelected(Integer sourceInformationSelected) {
		this.sourceInformationSelected = sourceInformationSelected;
	}
	
	/**
	 * Gets the sell price before mod.
	 *
	 * @return sellPriceBeforeInitial
	 */
	public BigDecimal getSellPriceBeforeMod() {
		return sellPriceBeforeInitial;
	}
	
	/**
	 * Sets the sell price before mod.
	 *
	 * @param sellPriceBeforeMod the new sell price before mod
	 */
	public void setSellPriceBeforeMod(BigDecimal sellPriceBeforeMod) {
		this.sellPriceBeforeInitial = sellPriceBeforeMod;
	}
	
	/**
	 * Gets the buy price before mod.
	 *
	 * @return buyPriceBeforeInitial
	 */
	public BigDecimal getBuyPriceBeforeMod() {
		return buyPriceBeforeInitial;
	}
	
	/**
	 * Sets the buy price before mod.
	 *
	 * @param buyPriceBeforeMod the new buy price before mod
	 */
	public void setBuyPriceBeforeMod(BigDecimal buyPriceBeforeMod) {
		this.buyPriceBeforeInitial = buyPriceBeforeMod;
	}
	
	/**
	 * Gets the average price before mod.
	 *
	 * @return averagePriceBeforeInitial
	 */
	public BigDecimal getAveragePriceBeforeMod() {
		return averagePriceBeforeInitial;
	}
	
	/**
	 * Sets the average price before mod.
	 *
	 * @param averagePriceBeforeMod the new average price before mod
	 */
	public void setAveragePriceBeforeMod(BigDecimal averagePriceBeforeMod) {
		this.averagePriceBeforeInitial = averagePriceBeforeMod;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}	
	
	/**
	 * Checks if is disble money.
	 *
	 * @return the disbleMoney
	 */
	public boolean isDisbleMoney() {
		return disbleMoney;
	}

	/**
	 * Sets the disble money.
	 *
	 * @param disbleMoney the disbleMoney to set
	 */
	public void setDisbleMoney(boolean disbleMoney) {
		this.disbleMoney = disbleMoney;
	}

	/**
	 * Gets the map description money.
	 *
	 * @return the map description money
	 */
	public Map<Integer, String> getMapDescriptionMoney() {
		return mapDescriptionMoney;
	}

	/**
	 * Set map description money.
	 *
	 * @param mapDescriptionMoney the map description money
	 */
	public void setMapDescriptionMoney(Map<Integer, String> mapDescriptionMoney) {
		this.mapDescriptionMoney = mapDescriptionMoney;
	}

	/**
	 * Gets the currency list.
	 *
	 * @return the currency list
	 */
	public List<SelectItem> getCurrencyList() {
		return currencyList;
	}

	/**
	 * Sets the currency list.
	 *
	 * @param currencyList the new currency list
	 */
	public void setCurrencyList(List<SelectItem> currencyList) {
		this.currencyList = currencyList;
	}

	/**
	 * Gets the daily exchange rates.
	 *
	 * @return the changeTypeSession
	 */
	public DailyExchangeRates getDailyExchangeRates() {
		return dailyExchangeRates;
	}
	
	/**
	 * Sets the daily exchange rates.
	 *
	 * @param dailyExchangeRates the dailyExchangeRates to set
	 */
	public void setDailyExchangeRates(DailyExchangeRates dailyExchangeRates) {
		this.dailyExchangeRates = dailyExchangeRates;
	}
	
	/**
	 * Gets the source type list.
	 *
	 * @return SourceType.list
	 */
	public List<SourceType> getSourceTypeList(){
		return SourceType.list;
	}
	
	/**
	 * Gets the currency type list.
	 *
	 * @return com.pradera.model.generalparameter.type.CurrencyType.list
	 */
	public List<com.pradera.model.generalparameter.type.CurrencyType> getCurrencyTypeList(){
		return com.pradera.model.generalparameter.type.CurrencyType.list;
	}
	
	/**
	 * Gets the request daily exchange rates.
	 *
	 * @return the request daily exchange rates
	 */
	public RequestDailyExchangeRates getRequestDailyExchangeRates() {
		return requestDailyExchangeRates;
	}

	/**
	 * Sets the request daily exchange rates.
	 *
	 * @param requestDailyExchangeRates the new request daily exchange rates
	 */
	public void setRequestDailyExchangeRates(
			RequestDailyExchangeRates requestDailyExchangeRates) {
		this.requestDailyExchangeRates = requestDailyExchangeRates;
	}

	/**
	 * Gets the map parameters desc.
	 *
	 * @return the map parameters desc
	 */
	public Map<Integer, String> getMapParametersDesc() {
		return mapParametersDesc;
	}

	/**
	 * Set map parameters desc.
	 *
	 * @param mapParametersDesc the map parameters desc
	 */
	public void setMapParametersDesc(Map<Integer, String> mapParametersDesc) {
		this.mapParametersDesc = mapParametersDesc;
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	
	
}
