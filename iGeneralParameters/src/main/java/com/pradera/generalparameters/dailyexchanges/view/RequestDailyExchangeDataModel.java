package com.pradera.generalparameters.dailyexchanges.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.generalparameter.RequestDailyExchangeRates;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RequestDailyExchangeDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class RequestDailyExchangeDataModel extends ListDataModel<RequestDailyExchangeRates> implements SelectableDataModel<RequestDailyExchangeRates>,Serializable {

	/** The Constant serialVersionUID. */
	
	private static final long serialVersionUID = -8641102324511386295L;
	
	/** The size. */
	private int size;

	/**
	 * Instantiates a new request daily exchange data model.
	 */
	public RequestDailyExchangeDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new request daily exchange data model.
	 *
	 * @param data the data
	 */
	public RequestDailyExchangeDataModel(List<RequestDailyExchangeRates> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public RequestDailyExchangeRates getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<RequestDailyExchangeRates> requestDailyExchangeRates=(List<RequestDailyExchangeRates>)getWrappedData();
		if(requestDailyExchangeRates != null){
	        for(RequestDailyExchangeRates requestDailyExchangeRate : requestDailyExchangeRates) {  
	            if(String.valueOf(requestDailyExchangeRate.getIdRequestDailyExchangepk()).equals(rowKey))
	                return requestDailyExchangeRate;  
	        }  
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(RequestDailyExchangeRates requestDailyExchangeRates) {
		// TODO Auto-generated method stub
		return requestDailyExchangeRates.getIdRequestDailyExchangepk();
	}

	/**
	 * Gets the size.
	 *
	 * @return size
	 */
	public int getSize() {
		List<RequestDailyExchangeRates> requestDailyExchangeRates=(List<RequestDailyExchangeRates>)getWrappedData();
		if(requestDailyExchangeRates==null)
			return 0;
		return requestDailyExchangeRates.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
}
