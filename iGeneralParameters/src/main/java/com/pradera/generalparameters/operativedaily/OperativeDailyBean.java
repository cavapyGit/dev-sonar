package com.pradera.generalparameters.operativedaily;

import java.util.Date;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.pradera.commons.processes.scheduler.SynchronizeBatchEvent;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class OperativeDailyBean.
 */
@Named
@ApplicationScoped
@Path("/syncronizedBatch")
public class OperativeDailyBean extends GenericBaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The operative process. */
	private OperativeDailyProcessTO operativeProcess;
	
	/** The generic resources service. */
	@EJB OperativeDailyGenericResources genericResourcesService;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The rest service. */
	@Inject private ClientRestService restService;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		OperativeDailyProcessTO processTO = null;
		try {
			processTO = genericResourcesService.getOperativeDailyProcess(CommonsUtilities.currentDate());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(processTO == null){
			operativeProcess = new OperativeDailyProcessTO();
		}else{
			operativeProcess = processTO;
		}
	}

	/**
	 * Gets the operative process.
	 *
	 * @return the operative process
	 */
	public OperativeDailyProcessTO getOperativeProcess() {
		return operativeProcess;
	}

	/**
	 * Sets the operative process.
	 *
	 * @param operativeProcess the new operative process
	 */
	public void setOperativeProcess(OperativeDailyProcessTO operativeProcess) {
		this.operativeProcess = operativeProcess;
	}
	
	/**
	 * Save operative request.
	 */
	public void saveOperativeRequest(){
		String message = "";
		String userName = userInfo.getUserAccountSession().getUserName();
		String ipAdrress = userInfo.getUserAccountSession().getIpAddress();
		try {
			restService.startOperativeDailyProcess(userName, ipAdrress);
			Thread.sleep(new Random().nextInt(5)*1000);
			if(isViewOperationRegister()){
				message = "operative.daily.loading.start.ok";
			}else if(isViewOperationModify()){
				message = "operative.daily.loading.restart.ok";
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),PropertiesUtilities.getMessage(message));
			JSFUtilities.showSimpleEndTransactionDialog("operativeDailyExecution");
		} catch (ServiceException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Before save operative request.
	 */
	public void beforeSaveOperativeRequest(){
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage("operative.daily.loading.start"));
		JSFUtilities.executeJavascriptFunction("PF('cnfWMssgDialog').show()");
	}
	
	/**
	 * Before restart operative request.
	 */
	public void beforeRestartOperativeRequest(){
		setViewOperationType(ViewOperationsType.MODIFY.getCode());
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage("operative.daily.loading.restart"));
		JSFUtilities.executeJavascriptFunction("PF('cnfWMssgDialog').show()");
	}
	
	/**
	 * Event from synchronize batch.
	 *
	 * @param newEvent the new event
	 * @param request the request
	 * @return the response
	 */
	@POST
	@Path("/receivEevent")
	@Produces(MediaType.APPLICATION_JSON)
	public Response eventFromSynchronizeBatch(SynchronizeBatchEvent newEvent,@Context HttpServletRequest request){
		OperativeDailyProcessTO operativeProcessDaily = operativeProcess;
		try {
			if(operativeProcessDaily.getDailyDetailList() == null){
				Date processDate = CommonsUtilities.convertStringtoDate(newEvent.getProcessDate(), CommonsUtilities.DATE_PATTERN);
				operativeProcessDaily = genericResourcesService.getOperativeDailyProcess(processDate);
			}
			operativeProcessDaily = genericResourcesService.updateProcessExecution(newEvent, operativeProcessDaily);
			setOperativeProcess(operativeProcessDaily);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return Response.status(Response.Status.OK.getStatusCode()).entity("sucess").build();
	}
}