package com.pradera.generalparameters.indexandrates.service;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexAndRateExpireBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@BatchProcess(name="IndexAndRateExpireBatch")
@RequestScoped
public class IndexAndRateExpireBatch implements JobExecution {
	
	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(IndexAndRateExpireBatch.class);
	
	/** The Index And Rate service bean. */
	@EJB
	private IndexAndRatesServiceBean indexAndRatesServiceBean;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		try {
			this.autoExpireFinancialIndicator(processLogger);
		} catch (ServiceException e) {
			e.printStackTrace();
			logger.error("autoExpireFinancialIndicator");
		}
	}
	
	
	/**
	 * Change the Status for expired FinancialIndicator.
	 *
	 * @param processLogger the ProcessLogger
	 * @throws ServiceException the service exception
	 */
	public void autoExpireFinancialIndicator(ProcessLogger processLogger) throws ServiceException{
		List<Integer> expiredFinicials = indexAndRatesServiceBean.getExpiredFinancialInfoService();
		// Updating FinancialIndiactor Status 
		  if(expiredFinicials.size()>0){			 
			  indexAndRatesServiceBean.updateExpiredState(expiredFinicials,processLogger);			 
		  }
		
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}	
	
}
