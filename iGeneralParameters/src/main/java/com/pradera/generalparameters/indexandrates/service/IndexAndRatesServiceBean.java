package com.pradera.generalparameters.indexandrates.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.indexandrates.view.filter.IndexAndRateFilter;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.FinancialIndicator;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.FinancialIndicatorStateType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexAndRatesServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class IndexAndRatesServiceBean extends CrudDaoServiceBean{

	/**
	 * Find financial indicator service bean.
	 *
	 * @param indexAndRateFilter the index and rate filter
	 * @return the financial indicator
	 * @throws ServiceException the service exception
	 */
	public FinancialIndicator findFinancialIndicatorServiceBean(IndexAndRateFilter indexAndRateFilter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select fi from FinancialIndicator fi ");
		sbQuery.append(" where  1=1 ");
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorType())){
			sbQuery.append("and fi.indicatorType= :indicatorType ");
		}
		
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorCode())){
			sbQuery.append("and fi.indicatorCode= :indicatorCode ");
		}

		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getPeriodicityType())){
			sbQuery.append("and fi.periodicityType= :periodType ");
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getStatus())){
			sbQuery.append("and fi.finantialState= :financialState ");
		}
		
		TypedQuery<FinancialIndicator> query = em.createQuery(sbQuery.toString(),FinancialIndicator.class);
		
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorType())){
			query.setParameter("indicatorType", indexAndRateFilter.getIndicatorType());
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorCode())){
			query.setParameter("indicatorCode", indexAndRateFilter.getIndicatorCode());
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getPeriodicityType())){
			query.setParameter("periodType", indexAndRateFilter.getPeriodicityType());
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getStatus())){
			query.setParameter("financialState", indexAndRateFilter.getStatus());
		}
		
		return (FinancialIndicator)query.getSingleResult();
	}
	
	/**
	 * Gets the parameter list service bean.
	 *
	 * @param idMasterTable the id master table
	 * @return List
	 * @throws ServiceException the service exception
	 */  
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getparameterListServiceBean(Integer idMasterTable) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select paramTable from ParameterTable paramTable");
		sbQuery.append(" where  paramTable.masterTable.masterTablePk = :idMasterTable and paramTable.parameterState=:state");
		sbQuery.append(" order by paramTable.parameterTableCd");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idMasterTable", idMasterTable);
		query.setParameter("state", ParameterTableStateType.REGISTERED.getCode());
		return query.getResultList();
	}

	/**
	 * To search Index And Rates.
	 *
	 * @param indexAndRateFilter the index and rate filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FinancialIndicator> searchIndexAndRateServiceBean(IndexAndRateFilter indexAndRateFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select fi from FinancialIndicator fi ");
		sbQuery.append(" where  1=1 ");
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorType())){
			sbQuery.append("and fi.indicatorType= :indicatorType ");
		}
		
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorCode())){
			sbQuery.append("and fi.indicatorCode= :indicatorCode ");
		}

		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getPeriodicityType())){
			sbQuery.append("and fi.periodicityType= :periodType ");
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getStatus())){
			sbQuery.append("and fi.finantialState= :financialState ");
		}
		sbQuery.append("order by fi.idFinancialIndicatorPk ");
		TypedQuery<FinancialIndicator> query = em.createQuery(sbQuery.toString(),FinancialIndicator.class);
		
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorType())){
			query.setParameter("indicatorType", indexAndRateFilter.getIndicatorType());
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorCode())){
			query.setParameter("indicatorCode", indexAndRateFilter.getIndicatorCode());
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getPeriodicityType())){
			query.setParameter("periodType", indexAndRateFilter.getPeriodicityType());
		}
		if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getStatus())){
			query.setParameter("financialState", indexAndRateFilter.getStatus());
		}
		
		return (List<FinancialIndicator>)query.getResultList();
	}
	
	/**
	 * Gets the index rate correlative service bean.
	 *
	 * @return the index rate correlative service bean
	 * @throws ServiceException the service exception
	 */
	public Integer getIndexRateCorrelativeServiceBean() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select MAX(fi.idFinancialIndicatorPk) from FinancialIndicator fi");
		Query query = em.createQuery(sbQuery.toString());
		
		return (Integer)query.getSingleResult();
	}
	
	/**
	 * Modify index rate service service.
	 *
	 * @param financialIndicator the financial indicator
	 * @throws ServiceException the service exception
	 */
	public void modifyIndexRateServiceService(
			FinancialIndicator financialIndicator) throws ServiceException {
		update(financialIndicator);
	}
	
	/**
	 * Register index rate service service.
	 *
	 * @param financialIndicator the financial indicator
	 * @throws ServiceException the service exception
	 */
	public void registerIndexRateServiceService(FinancialIndicator financialIndicator)throws ServiceException{
		create(financialIndicator);
	}
	
	/**
	 * Get expired financial indicators.
	 *
	 * @return the List<Integer>
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> getExpiredFinancialInfoService(){
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();		
		sbQuery.append("Select fi.idFinancialIndicatorPk from FinancialIndicator fi");
		sbQuery.append(" where fi.endDate <=:expiredDate");
		sbQuery.append(" and fi.finantialState =:state");
		parameters.put("expiredDate", CommonsUtilities.currentDate());
		parameters.put("state",  FinancialIndicatorStateType.VIGENTE.getCode());
		List<Integer> lstFinicialPk = findListByQueryString(sbQuery.toString(), parameters);
		return lstFinicialPk;
	}
	
	/**
	 * Updates the expired financial state.
	 *
	 * @param expiredFinicials the expired financial
	 * @param logger the logger
	 */
	public void updateExpiredState(List<Integer> expiredFinicials, ProcessLogger logger){
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();		
		sbQuery.append("Update FinancialIndicator fi ");
		sbQuery.append("set fi.finantialState =:state, ");
		sbQuery.append("fi.lastModifyDate =:lastModifyDate, ");
		sbQuery.append("fi.lastModifyIp =:lastModifyIp, ");
		sbQuery.append("fi.lastModifyUser =:lastModifyUser, ");
		sbQuery.append("fi.lastModifyApp =:lastModifyApp ");
		sbQuery.append("where fi.idFinancialIndicatorPk in(:expiredPks)");
		parameters.put("expiredPks", expiredFinicials);
		parameters.put("state", FinancialIndicatorStateType.VENCIDO.getCode());
		parameters.put("lastModifyDate", logger.getLastModifyDate());
		parameters.put("lastModifyIp", logger.getLastModifyIp());
		parameters.put("lastModifyUser", logger.getLastModifyUser());
		parameters.put("lastModifyApp", logger.getLastModifyApp());
		updateByQuery(sbQuery.toString(),parameters);
		
	}
	
	/**
	 * Find current financial indicator service bean.
	 *
	 * @param objFinancialIndicator the obj financial indicator
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FinancialIndicator> findCurrentFinancialIndicatorServiceBean(
			FinancialIndicator objFinancialIndicator) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select FI From FinancialIndicator FI ");
		sbQuery.append(" Where FI.indicatorType = :indicatorTypePrm ");
		sbQuery.append(" And FI.indicatorCode = :indicatorCodePrm ");
		sbQuery.append(" And FI.finantialState = :financialStatePrm ");
		
		if(Validations.validateIsNotNull(objFinancialIndicator.getInitialDate()) && 
				Validations.validateIsNotNull(objFinancialIndicator.getEndDate())) {
			sbQuery.append(" And ( FI.initialDate Between :initialDatePrm And :endDatePrm Or FI.endDate Between :initialDatePrm And :endDatePrm ");
			sbQuery.append(" Or :initialDatePrm Between FI.initialDate And FI.endDate Or :endDatePrm Between FI.initialDate And FI.endDate )");
		}
		
		if(Validations.validateIsNotNullAndPositive(objFinancialIndicator.getIdFinancialIndicatorPk())) {
			sbQuery.append(" And FI.idFinancialIndicatorPk != :idFinancialIndicatorPrm ");
		}
		
		sbQuery.append(" Order By FI.endDate desc ");
		TypedQuery<FinancialIndicator> query = em.createQuery(sbQuery.toString(), FinancialIndicator.class);
		
		query.setParameter("indicatorTypePrm", objFinancialIndicator.getIndicatorType());
		query.setParameter("indicatorCodePrm", objFinancialIndicator.getIndicatorCode());
		query.setParameter("financialStatePrm", FinancialIndicatorStateType.VIGENTE.getCode());
		
		if(Validations.validateIsNotNull(objFinancialIndicator.getInitialDate()) && 
				Validations.validateIsNotNull(objFinancialIndicator.getEndDate())) {
			query.setParameter("initialDatePrm", objFinancialIndicator.getInitialDate(), TemporalType.DATE);
			query.setParameter("endDatePrm", objFinancialIndicator.getEndDate(), TemporalType.DATE );
		}
		
		if(Validations.validateIsNotNullAndPositive(objFinancialIndicator.getIdFinancialIndicatorPk())) {
			query.setParameter("idFinancialIndicatorPrm", objFinancialIndicator.getIdFinancialIndicatorPk());
		}
		
		return (List<FinancialIndicator>)query.getResultList();		
	}
}
