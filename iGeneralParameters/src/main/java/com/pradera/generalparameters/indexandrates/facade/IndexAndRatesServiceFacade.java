package com.pradera.generalparameters.indexandrates.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.generalparameters.indexandrates.service.IndexAndRatesServiceBean;
import com.pradera.generalparameters.indexandrates.view.filter.IndexAndRateFilter;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.FinancialIndicator;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexAndRatesServiceFacade.
 *
 * @author PraderaTechnologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class IndexAndRatesServiceFacade {
	
	/** The index and rates service bean. */
	@EJB
	private IndexAndRatesServiceBean indexAndRatesServiceBean;	
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * To get the parametersList.
	 *
	 * @param idMasterTable the id master table
	 * @return list
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterListServiceFacade(Integer idMasterTable) throws ServiceException{
		return indexAndRatesServiceBean.getparameterListServiceBean(idMasterTable);
	}

	
	/**
	 * To Register IndexAndRates.
	 *
	 * @param financialIndicator the financial indicator
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void registerIndexRateServiceFacade(
			FinancialIndicator financialIndicator) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(loggerUser.getUserAction().isRegister()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		indexAndRatesServiceBean.registerIndexRateServiceService(financialIndicator);
	}


	/**
	 * To search IndexandRates.
	 *
	 * @param indexAndRateFilter the index and rate filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FinancialIndicator> searchIndexAndRateServiceFacade(
			IndexAndRateFilter indexAndRateFilter) throws ServiceException {		
		return indexAndRatesServiceBean.searchIndexAndRateServiceBean(indexAndRateFilter);
	}


	/**
	 * To Modify IndexAndRates.
	 *
	 * @param financialIndicator the financial indicator
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyIndexRateServiceFacade(
			FinancialIndicator financialIndicator) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(loggerUser.getUserAction().isModify()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		indexAndRatesServiceBean.modifyIndexRateServiceService(financialIndicator);
	}
	
	/**
	 * Gets the index rate correlative service facade.
	 *
	 * @return the index rate correlative service facade
	 * @throws ServiceException the service exception
	 */
	public Integer getIndexRateCorrelativeServiceFacade() throws ServiceException{
		//GETTING CORRELATIVE AVAILABLE
		return (indexAndRatesServiceBean.getIndexRateCorrelativeServiceBean()+1);
	}
	
	/**
	 * To Update IndexAndRates.
	 *
	 * @param financialIndicator the financial indicator
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateIndexRateServiceFacade(
			FinancialIndicator financialIndicator) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//TODO No privileges for update, time being used confirm
		 if(loggerUser.getUserAction().isConfirm()){
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
    	}	 
		 else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		 indexAndRatesServiceBean.modifyIndexRateServiceService(financialIndicator);
	}	
	
	/**
	 * Find current financial indicator service facade.
	 *
	 * @param objFinancialIndicator the obj financial indicator
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FinancialIndicator> findCurrentFinancialIndicatorServiceFacade(
			FinancialIndicator objFinancialIndicator) throws ServiceException {
		return indexAndRatesServiceBean.findCurrentFinancialIndicatorServiceBean(objFinancialIndicator);
	}
}
