package com.pradera.generalparameters.indexandrates.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum MonthType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public enum MonthType {
	
	/** The january. */
	JANUARY(new Integer(1),"ENERO"),
	
	/** The february. */
	FEBRUARY(new Integer(2),"FEBRERO"),
	
	/** The march. */
	MARCH(new Integer(3),"MARZO"),
	
	/** The april. */
	APRIL(new Integer(4),"ABRIL"),
	
	/** The may. */
	MAY(new Integer(5),"MAYO"),
	
	/** The june. */
	JUNE(new Integer(6),"JUNIO"),
	
	/** The july. */
	JULY(new Integer(7),"JULIO"),
	
	/** The august. */
	AUGUST(new Integer(8),"AGOSTO"),
	
	/** The september. */
	SEPTEMBER(new Integer(9),"SEPTIEMBRE"),
	
	/** The october. */
	OCTOBER(new Integer(10),"OCTUBRE"),
	
	/** The november. */
	NOVEMBER(new Integer(11),"NOVIEMBRE"),
	
	/** The december. */
	DECEMBER(new Integer(12),"DICIEMBRE");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<MonthType> list = new ArrayList<MonthType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MonthType> lookup = new HashMap<Integer, MonthType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return list
	 */
	public static List<MonthType> listSomeElements(MonthType... transferSecuritiesTypeParams){
		List<MonthType> retorno = new ArrayList<MonthType>();
		for(MonthType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (MonthType s : EnumSet.allOf(MonthType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	

	/**
	 * Instantiates a new month type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private MonthType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
