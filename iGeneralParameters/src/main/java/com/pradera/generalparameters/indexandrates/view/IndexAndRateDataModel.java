package com.pradera.generalparameters.indexandrates.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.generalparameter.FinancialIndicator;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IndexAndRateDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class  IndexAndRateDataModel extends ListDataModel<FinancialIndicator> implements Serializable, SelectableDataModel<FinancialIndicator>{


	/** The size. */
	private int size;

	/**
	 * Constructor.
	 */
	public IndexAndRateDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new index and rate data model.
	 *
	 * @param data the data
	 */
	public IndexAndRateDataModel(List<FinancialIndicator> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public FinancialIndicator getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<FinancialIndicator> financialIndicators=(List<FinancialIndicator>)getWrappedData();
        for(FinancialIndicator financialIndicator : financialIndicators) {  
            if(String.valueOf(financialIndicator.getIdFinancialIndicatorPk()).equals(rowKey))
                return financialIndicator;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(FinancialIndicator financialIndicator) {
		// TODO Auto-generated method stub
		return financialIndicator.getIdFinancialIndicatorPk();
	}

	/**
	 * Gets the size.
	 *
	 * @return size
	 */
	public int getSize() {
		List<FinancialIndicator> financialIndicators=(List<FinancialIndicator>)getWrappedData();
		if(financialIndicators==null)
			return 0;
		return financialIndicators.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	
}