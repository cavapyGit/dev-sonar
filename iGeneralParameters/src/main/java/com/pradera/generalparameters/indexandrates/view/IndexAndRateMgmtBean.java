package com.pradera.generalparameters.indexandrates.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.generalparameters.indexandrates.facade.IndexAndRatesServiceFacade;
import com.pradera.generalparameters.indexandrates.type.MonthType;
import com.pradera.generalparameters.indexandrates.view.filter.IndexAndRateFilter;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.FinancialIndicator;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.FinancialIndicatorStateType;
import com.pradera.model.generalparameter.type.FinancialIndicatorType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.generalparameter.type.PeriodType;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexAndRateMgmtBean.
 *
 * @author PraderaTechnologies
 */

@DepositaryWebBean
public class IndexAndRateMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The disabled days. */
	private String disabledDays = "";
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The financial indicator code list. */
	private List<ParameterTable> financialIndicatorCodeList;
	
	/** The financial indicator code list for search. */
	private List<ParameterTable> financialIndicatorCodeListForSearch;
	
	/** The Indicator Type. */
	private Integer indicatorType;
	
	/** The Source Info. */
	private Integer sourceinfo;
	
	/** The FinancialIndicator. */
	private FinancialIndicator financialIndicator;
	
	/** The IndexRateFilter. */
	private IndexAndRateFilter indexAndRateFilter;
	
	/** The periodTypeList. */
	private List<ParameterTable> periodTypeList;		
	
	/** The IndexRateServiceFacade. */
	@EJB
	private IndexAndRatesServiceFacade indexAndRatesServiceFacade;	
	
	/** Financial indicator Types. */
	private List<ParameterTable> financialIndicatorTypeList;
	
	/** Financial indicator Types. */
	private List<ParameterTable> informationSourceList;
	
	/** The financial indicator status list. */
	private List<ParameterTable> financialIndicatorStatusList;
	
	/** The IndexAndRateDataModel. */
	private IndexAndRateDataModel indexAndRateDataModel;
	
	/** The selected financial indicator. */
	private FinancialIndicator selectedFinancialIndicator;		
	
	/** The month list. */
	private List<MonthType> monthList;

	/** The year list. */
	private List<SelectItem> yearList;
	
	/** The indicator type index. */
	private boolean indicatorTypeIndex;
	
	/** The indicator type rate. */
	private boolean indicatorTypeRate;
	
	/** The general parameters facade. */
	@EJB
    private GeneralParametersFacade generalParametersFacade;
	
	/** The Indicator code. */
	
	private List<ParameterTable> lstParametersIndicatorCode;
	
	/** The User Information. */
	@Inject
	private UserInfo userInfo;
	
	/**
	 * The PostConstruct Method.
	 */
	@PostConstruct
	public void init(){
		try {
	    	if (!FacesContext.getCurrentInstance().isPostback()) {
			financialIndicator = new FinancialIndicator();
			indexAndRateFilter = new IndexAndRateFilter();
			yearList   		   = new ArrayList<SelectItem>();
			loadFilters();
	    	this.setViewOperationType(ViewOperationsType.CONSULT.getCode());						
	    	
			lstParametersIndicatorCode = new ArrayList<ParameterTable>();
			lstParametersIndicatorCode.addAll(indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.FINANCIAL_INDEX.getCode()));
			lstParametersIndicatorCode.addAll(indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.RATE_TYPE.getCode())); 
			
	    	listHolidays();
	    	}
	    	}catch (Exception e) {
	    		excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
		
	/**
	 * Load filters.
	 */
	private void loadFilters(){
		//LOAD FINANCIAL INDICATOR TYPE
		try {
			ParameterTableTO filterParameter = new ParameterTableTO();
			filterParameter.setState(ParameterTableStateType.REGISTERED.getCode());
			
			filterParameter.setMasterTableFk(MasterTableType.NEGOTIATION_PARAMETERS_MASTER_TABLE_PK.getCode());
			financialIndicatorTypeList = generalParametersFacade.getListParameterTableServiceBean(filterParameter);

			//LOAD INFORMATION SOURCES			
			filterParameter.setMasterTableFk(MasterTableType.INFORMATION_SOURCE.getCode());
			informationSourceList = generalParametersFacade.getListParameterTableServiceBean(filterParameter);

			//LOAD PRIOD TYPES
			filterParameter.setMasterTableFk(MasterTableType.FINANCIAL_PERIOD.getCode());
			filterParameter.setOrderbyParameterTableCd(BooleanType.YES.getCode());
			periodTypeList = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
			//clean order
			filterParameter.setOrderbyParameterTableCd(null);
			//LOAD STATES
			filterParameter.setMasterTableFk(MasterTableType.FINANCIAL_INDICATOR_STATUS.getCode());
			financialIndicatorStatusList = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
			
			monthList = MonthType.list;
			
			Calendar calendar = Calendar.getInstance();
			for(Integer i=calendar.get(Calendar.YEAR)-5;i<calendar.get(Calendar.YEAR)+10;i++){
				yearList.add(new SelectItem(i,i.toString()));
			}
			financialIndicator.setInitialDate(getCurrentSystemDate());
			calendar.setTime(financialIndicator.getInitialDate());
			
			indexAndRateFilter.setIndicatorType(new Integer(-1));
			financialIndicator.setRegistryDate(getCurrentSystemDate());			
		}
		catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	
	/**
	 * Load index or rate or both.
	 */
	public void loadIndexOrRateOrBoth(){
		try{
			
			indexAndRateFilter.setIndicatorCode(null);
			indexAndRateFilter.setIndicatorDescription(null);
			if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorType())){
				
				if(FinancialIndicatorType.INDEX.getCode().equals(indexAndRateFilter.getIndicatorType())) {
					financialIndicatorCodeListForSearch = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.FINANCIAL_INDEX.getCode());
				} else if(FinancialIndicatorType.RATE.getCode().equals(indexAndRateFilter.getIndicatorType())) {
					financialIndicatorCodeListForSearch = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.RATE_TYPE.getCode());
				} else {
					financialIndicatorCodeListForSearch = null;
				}
				
			} else{			
				financialIndicatorCodeListForSearch = null;
			}
		}
		catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To perform operations when change on PeriodType.
	 */
	public void periodTypeChangeListener(){
		try {

		financialIndicator.setInitialDate(null);
		financialIndicator.setEndDate(null);
		
		disableAlreadyRegisteredDateRanges();
				
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Disable already registered date ranges.
	 *
	 * @throws ServiceException the service exception
	 */
	private void disableAlreadyRegisteredDateRanges() throws ServiceException {
		disabledDays = "";
		

		FinancialIndicator objFinancialIndicator = new FinancialIndicator();
		objFinancialIndicator.setIndicatorType(financialIndicator.getIndicatorType());
		objFinancialIndicator.setIndicatorCode(financialIndicator.getIndicatorCode());
		if(this.isViewOperationModify() || this.isViewOperationUpdate()) {
			objFinancialIndicator.setIdFinancialIndicatorPk(financialIndicator.getIdFinancialIndicatorPk());
		}
		List<FinancialIndicator> lstCurrentFinancialIndicator = indexAndRatesServiceFacade.findCurrentFinancialIndicatorServiceFacade(objFinancialIndicator);

		if(Validations.validateListIsNotNullAndNotEmpty(lstCurrentFinancialIndicator)) {
			
			StringBuilder stringBuilder = new StringBuilder();
			if(Validations.validateIsNotNullAndNotEmpty(allHolidays)){
				stringBuilder.append(allHolidays);					
			}
			
			Date initialDateValidation = null;
			Date finalDateValidation = null;
			Date currentDateValidation = null;
			
			List<Date> lstCurrentPeriodFinancialIndicator = null;
			Calendar cal = Calendar.getInstance();
			
			for (FinancialIndicator currentFinancialIndicator : lstCurrentFinancialIndicator) {
				initialDateValidation = currentFinancialIndicator.getInitialDate();
				finalDateValidation = currentFinancialIndicator.getEndDate();
				currentDateValidation = CommonsUtilities.addDaysToDate(initialDateValidation, -1);

				lstCurrentPeriodFinancialIndicator = new ArrayList<Date>();

				while (currentDateValidation.before(finalDateValidation)) {
					cal.setTime(currentDateValidation);
					cal.add(Calendar.DAY_OF_YEAR, 1);
					currentDateValidation = cal.getTime();
					lstCurrentPeriodFinancialIndicator.add(currentDateValidation);
				}
				
				if(Validations.validateIsNotEmpty(stringBuilder.toString())){
					stringBuilder.append(",");
				}					
				stringBuilder.append(CommonsUtilities.convertListDateToString(lstCurrentPeriodFinancialIndicator));
			}
			
			
			
			disabledDays = stringBuilder.toString();
		}
	}
	
	/**
	 * To initialize final date on change of initialDate.
	 */
	public void onInitialDateChangeListener(){
		try {
			hideDialogs();				
			if(Validations.validateIsNotNullAndPositive(financialIndicator.getPeriodicityType())){								
	
				Calendar cal = Calendar.getInstance();				
				cal.setTime(financialIndicator.getInitialDate());
				
				if(financialIndicator.getPeriodicityType().equals(PeriodType.WEEKLY.getCode())){
					cal.add(Calendar.DAY_OF_YEAR,7);
					validateAndSetTime(cal);
				}
				else if(financialIndicator.getPeriodicityType().equals(PeriodType.BIWEEKLY.getCode())){
					cal.add(Calendar.DAY_OF_YEAR, 15);
					validateAndSetTime(cal);
				}
				else if(financialIndicator.getPeriodicityType().equals(PeriodType.MONTHLY.getCode())){
					cal.add(Calendar.MONTH,1);				
					validateAndSetTime(cal);
				}
				else if(financialIndicator.getPeriodicityType().equals(PeriodType.BIMONTHLY.getCode())){
					cal.add(Calendar.MONTH, 2);				
					validateAndSetTime(cal);
				}
				else if(financialIndicator.getPeriodicityType().equals(PeriodType.QUARTERLY.getCode())){
					cal.add(Calendar.MONTH, 3);				
					validateAndSetTime(cal);
				}
				else if(financialIndicator.getPeriodicityType().equals(PeriodType.FOURMONTHLY.getCode())){
					cal.add(Calendar.MONTH, 4);				
					validateAndSetTime(cal);
				}
				else if(financialIndicator.getPeriodicityType().equals(PeriodType.BIANNUAL.getCode())){
					cal.add(Calendar.MONTH, 6);				
					validateAndSetTime(cal);
				}
				else if(financialIndicator.getPeriodicityType().equals(PeriodType.ANNUAL.getCode())){
					cal.add(Calendar.YEAR, 1);				
					validateAndSetTime(cal);
				}
				
				FinancialIndicator objFinancialIndicator = new FinancialIndicator();
				objFinancialIndicator.setIndicatorType(financialIndicator.getIndicatorType());
				objFinancialIndicator.setIndicatorCode(financialIndicator.getIndicatorCode());
				objFinancialIndicator.setInitialDate(financialIndicator.getInitialDate());
				objFinancialIndicator.setEndDate(financialIndicator.getEndDate());
				if(this.isViewOperationModify() || this.isViewOperationUpdate()){
					objFinancialIndicator.setIdFinancialIndicatorPk(financialIndicator.getIdFinancialIndicatorPk());
				}			
				
				List<FinancialIndicator> lstCurrentFinancialIndicator = indexAndRatesServiceFacade.findCurrentFinancialIndicatorServiceFacade(objFinancialIndicator);

				if(Validations.validateListIsNotNullAndNotEmpty(lstCurrentFinancialIndicator)) {
					JSFUtilities.showComponent("cnfMsgCustomValidation");
					if(FinancialIndicatorType.INDEX.getCode().equals(financialIndicator.getIndicatorType())){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,PropertiesConstants.VALIDATION_INDEX_RANGE_ALREADY_REGISTERED, null);
					} else {
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,PropertiesConstants.VALIDATION_RATE_RANGE_ALREADY_REGISTERED, null);
					}					
					financialIndicator.setInitialDate(null);
					financialIndicator.setEndDate(null);
				}
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To validate the date and set the date.
	 *
	 * @param cal the cal
	 */
	public void validateAndSetTime(Calendar cal){		
		financialIndicator.setEndDate(cal.getTime());
	}
	
	/**
	 * Setting end date.
	 *
	 * @param cal the cal
	 * @return the calendar
	 */
	public Calendar settingEndDate(Calendar cal){
		if(financialIndicator.getPeriodicityType().equals(PeriodType.WEEKLY.getCode())){
			cal.add(Calendar.DAY_OF_YEAR,7);
		}
		else if(financialIndicator.getPeriodicityType().equals(PeriodType.BIWEEKLY.getCode())){
			cal.add(Calendar.DAY_OF_YEAR, 15);
		}
		else if(financialIndicator.getPeriodicityType().equals(PeriodType.MONTHLY.getCode())){
			cal.add(Calendar.MONTH,1);
		}
		else if(financialIndicator.getPeriodicityType().equals(PeriodType.BIMONTHLY.getCode())){
			cal.add(Calendar.MONTH, 2);
		}
		else if(financialIndicator.getPeriodicityType().equals(PeriodType.QUARTERLY.getCode())){
			cal.add(Calendar.MONTH, 3);
		}
		else if(financialIndicator.getPeriodicityType().equals(PeriodType.FOURMONTHLY.getCode())){
			cal.add(Calendar.MONTH, 4);
		}
		else if(financialIndicator.getPeriodicityType().equals(PeriodType.BIANNUAL.getCode())){
			cal.add(Calendar.MONTH, 6);
		}
		else if(financialIndicator.getPeriodicityType().equals(PeriodType.ANNUAL.getCode())){
			cal.add(Calendar.YEAR, 1);
		}
		return cal;
	}
	
	/**
	 * Check value is positive.
	 *
	 * @return true, if successful
	 */
	public boolean checkValueIsPositive(){
		boolean isPositive;
		if(financialIndicator.getRateValue().compareTo(BigDecimal.ZERO)==GeneralConstants.ONE_VALUE_INTEGER){
			isPositive = true;
		}else{
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.VALIDATION_VALUE, null,PropertiesConstants.VALIDATION_VALUE_TEXT, null);
			isPositive = false;
		}
		return isPositive;
	}
	
	/**
	 * To hide the dialogs.
	 */
	public void hideDialogs(){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("frmRegisterIndexAndRate:cnfregindexandrate");
	}
	
	/**
	 * To Clear the screen.
	 */
	public void clear(){
			hideDialogs();			
			JSFUtilities.resetComponent("frmRegisterIndexAndRate");			
			clearSearch();
	}
	
	/**
	 * Clear search.
	 */
	private void clearSearch(){
		financialIndicator = new FinancialIndicator();
		indexAndRateFilter = new IndexAndRateFilter();
		financialIndicator.setRegistryDate(getCurrentSystemDate());			
		financialIndicator.setPeriodicityType(GeneralConstants.NEGATIVE_ONE_INTEGER);
		indexAndRateFilter.setIndicatorType(GeneralConstants.NEGATIVE_ONE_INTEGER);
		indexAndRateDataModel = null;
		selectedFinancialIndicator = null;
	}
	
	/**
	 * Clear update.
	 */
	public void clearUpdate(){
		
		JSFUtilities.resetComponent("frmRegisterIndexAndRate");
		
		financialIndicator.setPeriodicityType(GeneralConstants.NEGATIVE_ONE_INTEGER);
		financialIndicator.setRateValue(null);
		financialIndicator.setInitialDate(getCurrentSystemDate());
	}
	
	/**
	 * Clear modify.
	 */
	public void clearModify(){
		
		JSFUtilities.resetComponent("frmRegisterIndexAndRate");
		
		financialIndicator.setPeriodicityType(GeneralConstants.NEGATIVE_ONE_INTEGER);
		financialIndicator.setRateValue(null);
		financialIndicator.setObservations(null);
		financialIndicator.setInitialDate(null);
		financialIndicator.setEndDate(null);
	}
	
	/**
	 * To perform SearchQuery.
	 */
	public void searchIndexRatesListener(){
		try{
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			
			List<FinancialIndicator> lsFinancialIndicators = indexAndRatesServiceFacade.searchIndexAndRateServiceFacade(indexAndRateFilter);
		
							
			for (FinancialIndicator objFinancialIndicator : lsFinancialIndicators) {
				for (ParameterTable parameterTable : lstParametersIndicatorCode) {
					if(parameterTable.getParameterTablePk().equals(objFinancialIndicator.getIndicatorCode())){
						objFinancialIndicator.setIndicatorDescription(parameterTable.getDescription());
						break;
					}
				}
			}
			
			
		 	indexAndRateDataModel = new IndexAndRateDataModel(lsFinancialIndicators);
		 	selectedFinancialIndicator = null;
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	

	/**
	 * Before Register the screen.
	 */
	public void beforeRegisterIndexRate(){
		try{
			this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
			financialIndicator = new FinancialIndicator();
			financialIndicator.setRegistryDate(getCurrentSystemDate());			
			indicatorTypeIndex = false;
			indicatorTypeRate = false;
			financialIndicatorCodeList = null;
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before Modify The Record.
	 *
	 * @return the string
	 */
	public String beforeModifyIndexRate(){
		try {
		
		hideDialogs();
		if(selectedFinancialIndicator == null) {
			showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.DIALOG_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return null;
		} else if(!FinancialIndicatorStateType.VIGENTE.getCode().equals(selectedFinancialIndicator.getFinantialState())) {
			showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.DIALOG_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.INDEXRATE_SELECTED_NOT_MODIFIABLE));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return null;
		}
		
		this.setViewOperationType(ViewOperationsType.MODIFY.getCode());
		
		assignValues();				
		return "registerIndexAndRates";
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * To assign SelectedIndexRateBean to financialIndicator Object.
	 *
	 * @throws ServiceException the service exception
	 */
	public void assignValues() throws ServiceException{
		financialIndicator = new FinancialIndicator();
		
		//For Update Dates must be cleaned
		if(this.isViewOperationModify()) {
			financialIndicator.setPeriodicityType(selectedFinancialIndicator.getPeriodicityType());
			financialIndicator.setInitialDate(selectedFinancialIndicator.getInitialDate());	
			financialIndicator.setEndDate(selectedFinancialIndicator.getEndDate());
		}
		financialIndicator.setFinantialState(selectedFinancialIndicator.getFinantialState());
		financialIndicator.setIdFinancialIndicatorPk(selectedFinancialIndicator.getIdFinancialIndicatorPk());
		financialIndicator.setIdSourceInfo(selectedFinancialIndicator.getIdSourceInfo());		
		financialIndicator.setIndicatorType(selectedFinancialIndicator.getIndicatorType());
		financialIndicator.setIndicatorCode(selectedFinancialIndicator.getIndicatorCode());			
		financialIndicator.setObservations(selectedFinancialIndicator.getObservations());		
		financialIndicator.setRateValue(selectedFinancialIndicator.getRateValue());
		financialIndicator.setRegistryDate(selectedFinancialIndicator.getRegistryDate());
		financialIndicator.setUpdateDate(getCurrentSystemDate());
		
		if(FinancialIndicatorType.INDEX.getCode().equals(financialIndicator.getIndicatorType())) {
			indicatorTypeIndex = true;
			indicatorTypeRate = false;
			financialIndicatorCodeList = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.FINANCIAL_INDEX.getCode());
		} else if(FinancialIndicatorType.RATE.getCode().equals(financialIndicator.getIndicatorType())) {
			indicatorTypeIndex = false;
			indicatorTypeRate = true;
			financialIndicatorCodeList = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.RATE_TYPE.getCode());
		} else {
			indicatorTypeIndex = false;
			indicatorTypeRate = false;
		}
		
		for (ParameterTable parameterTable : financialIndicatorCodeList) {
			if(parameterTable.getParameterTablePk().equals(financialIndicator.getIndicatorCode())){
				financialIndicator.setIndicatorDescription(parameterTable.getDescription());
			}
		}
		
		disableAlreadyRegisteredDateRanges();
	}
	
	/**
	 * Before Show details.
	 *
	 * @param event the event
	 */
	public void beforeDetailIndexRate(ActionEvent event){
		
		try {
			hideDialogs();
			financialIndicator = (FinancialIndicator)event.getComponent().getAttributes().get("indexDetails");	
			if(FinancialIndicatorType.INDEX.getCode().equals(financialIndicator.getIndicatorType())) {
				indicatorTypeIndex = true;
				indicatorTypeRate = false;
				financialIndicatorCodeList = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.FINANCIAL_INDEX.getCode());
			} else if(FinancialIndicatorType.RATE.getCode().equals(financialIndicator.getIndicatorType())) {
				indicatorTypeIndex = false;
				indicatorTypeRate = true;
				financialIndicatorCodeList = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.RATE_TYPE.getCode());
			} else {
				indicatorTypeIndex = false;
				indicatorTypeRate = false;
			}
			
			for (ParameterTable parameterTable : financialIndicatorCodeList) {
				if(parameterTable.getParameterTablePk().equals(financialIndicator.getIndicatorCode())){
					financialIndicator.setIndicatorDescription(parameterTable.getDescription());
				}
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}
	
	
	/**
	 * Before update the record.
	 *
	 * @return the string
	 */
	public String beforeUpdateIndexRate(){
		try {
		
		hideDialogs();
			
		if(selectedFinancialIndicator == null) {
			showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.DIALOG_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return null;
		} else if(!FinancialIndicatorStateType.VENCIDO.getCode().equals(selectedFinancialIndicator.getFinantialState())) {
			showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.DIALOG_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.INDEXRATE_SELECTED_NOT_UPDATABLE));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
			return null;
		}
			
		this.setViewOperationType(ViewOperationsType.UPDATE.getCode());
		
		assignValues();				
		return "updateIndexAndRates";
		
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Before saveIndexrate.
	 */
	public void beforeSaveIndexRateListener(){
		hideDialogs();
				
		Object[] obj = new Object[1];	
		
		if(this.isViewOperationRegister()) {
			String indicatorCodeDescription = null;
			for (ParameterTable parameterTable : financialIndicatorCodeList) {
				if(parameterTable.getParameterTablePk().equals(financialIndicator.getIndicatorCode())){
					indicatorCodeDescription = parameterTable.getParameterName();
				}
			}
			obj[0] = indicatorCodeDescription;		
			
			if(FinancialIndicatorType.INDEX.getCode().equals(financialIndicator.getIndicatorType())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,PropertiesConstants.INDEXRATE_CONFIRM_REGISTER_INDEX, obj);
			} else {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,PropertiesConstants.INDEXRATE_CONFIRM_REGISTER_RATE, obj);
			}	
		} else if (this.isViewOperationModify()) {
			obj[0] = String.valueOf(financialIndicator.getIdFinancialIndicatorPk());
			if(FinancialIndicatorType.INDEX.getCode().equals(financialIndicator.getIndicatorType())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,PropertiesConstants.INDEXRATE_CONFIRM_MODIFY_INDEX, obj);
			} else {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,PropertiesConstants.INDEXRATE_CONFIRM_MODIFY_RATE, obj);
			}
		} else if (this.isViewOperationUpdate()) {
			obj[0] = String.valueOf(financialIndicator.getIdFinancialIndicatorPk());
			if(FinancialIndicatorType.INDEX.getCode().equals(financialIndicator.getIndicatorType())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,PropertiesConstants.INDEXRATE_CONFIRM_UPDATE_INDEX, obj);
			} else {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM, null,PropertiesConstants.INDEXRATE_CONFIRM_UPDATE_RATE, obj);
			}	
		}
		    
		
		JSFUtilities.showComponent("frmRegisterIndexAndRate:cnfregindexandrate");
	}
	
	/**
	 * To Save IndexRate.
	 */
	@LoggerAuditWeb
	public void saveIndexRateListener(){
		try{
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		hideDialogs();
	if(isViewOperationRegister()){
		if(checkValueIsPositive()){
			financialIndicator.setFinantialState(FinancialIndicatorStateType.VIGENTE.getCode());
			//SETTING PARAMETERS TO DIALOG MESSAGE
					String indicatorCodeDescription = null;
					Object[] obj = new Object[2];
					for (ParameterTable parameterTable : financialIndicatorCodeList) {
						if(parameterTable.getParameterTablePk().equals(financialIndicator.getIndicatorCode())){
							indicatorCodeDescription = parameterTable.getParameterName();
						}
					}
					obj[0] = indicatorCodeDescription;
					
			indexAndRatesServiceFacade.registerIndexRateServiceFacade(financialIndicator);
			obj[1] = String.valueOf(financialIndicator.getIdFinancialIndicatorPk());
					
			
			if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorType()) && 
			   financialIndicator.getIndicatorType().equals(FinancialIndicatorType.INDEX.getCode())){		
				  JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,PropertiesConstants.INDEX_REGISTER_SUCCESS, obj);
			}else if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorType()) && 
					   financialIndicator.getIndicatorType().equals(FinancialIndicatorType.RATE.getCode())){	
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,PropertiesConstants.RATE_REGISTER_SUCCESS, obj);
			}
		
			clearSearch();
			JSFUtilities.showComponent("cnfEndTransaction");
		}
	} else if(isViewOperationModify()){
		if(checkValueIsPositive()){
			financialIndicator.setFinantialState(FinancialIndicatorStateType.VIGENTE.getCode());	
			
			indexAndRatesServiceFacade.modifyIndexRateServiceFacade(financialIndicator);
			Object[] obj = {String.valueOf(financialIndicator.getIdFinancialIndicatorPk())};								
			
			if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorType()) && 
					   financialIndicator.getIndicatorType().equals(FinancialIndicatorType.INDEX.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,PropertiesConstants.INDEX_MODIFY_SUCCESS, obj);
			}else if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorType()) && 
					   financialIndicator.getIndicatorType().equals(FinancialIndicatorType.RATE.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,PropertiesConstants.RATE_MODIFY_SUCCESS, obj);
			}
			clearSearch();
			JSFUtilities.showComponent("cnfEndTransaction");
		}
	} else if(isViewOperationUpdate()){
			if(checkValueIsPositive()){
				financialIndicator.setFinantialState(FinancialIndicatorStateType.VIGENTE.getCode());		
				indexAndRatesServiceFacade.updateIndexRateServiceFacade(financialIndicator);
				Object[] obj = {String.valueOf(financialIndicator.getIdFinancialIndicatorPk())};				
				
				if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorType()) && 
						   financialIndicator.getIndicatorType().equals(FinancialIndicatorType.INDEX.getCode())){
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,PropertiesConstants.INDEX_UPDATE_SUCCESS, obj);
				}else if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorType()) && 
						   financialIndicator.getIndicatorType().equals(FinancialIndicatorType.RATE.getCode())){
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,PropertiesConstants.RATE_UPDATE_SUCCESS, obj);
				}
				
				clearSearch();
				JSFUtilities.showComponent("cnfEndTransaction");
			}
		}
		
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	/**
	 * Change selected indicator type.
	 */
	public void changeSelectedIndicatorType(){
		try {
			hideDialogs();
			removeValuesFinancianIndicator();
			financialIndicator.setIndicatorCode(null);
			financialIndicatorCodeList = null;			
			indicatorTypeIndex = false;
			indicatorTypeRate = false;
			if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorType())) {
				if(FinancialIndicatorType.INDEX.getCode().equals(financialIndicator.getIndicatorType())){
					financialIndicatorCodeList = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.FINANCIAL_INDEX.getCode());
					indicatorTypeIndex = true;
					indicatorTypeRate = false;					
				} else if(FinancialIndicatorType.RATE.getCode().equals(financialIndicator.getIndicatorType())){
					financialIndicatorCodeList = indexAndRatesServiceFacade.getParameterListServiceFacade(MasterTableType.RATE_TYPE.getCode());
					indicatorTypeIndex = false;
					indicatorTypeRate = true;
				}
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	/**
	 * Change selected indicator code.
	 */
	public void changeSelectedIndicatorCode(){
		try {
			hideDialogs();			
			removeValuesFinancianIndicator();			
			if(Validations.validateIsNotNullAndPositive(financialIndicator.getIndicatorCode())) {
				for (ParameterTable parameterTable : financialIndicatorCodeList) {
					if(parameterTable.getParameterTablePk().equals(financialIndicator.getIndicatorCode())){
						financialIndicator.setIndicatorDescription(parameterTable.getDescription());
						return;
					}
				}
			} else {
				financialIndicator.setIndicatorDescription(null);
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Change selected indicator code for search.
	 */
	public void changeSelectedIndicatorCodeForSearch(){
		try {		
			if(Validations.validateIsNotNullAndPositive(indexAndRateFilter.getIndicatorCode())) {
				for (ParameterTable parameterTable : financialIndicatorCodeListForSearch) {
					if(parameterTable.getParameterTablePk().equals(indexAndRateFilter.getIndicatorCode())){
						indexAndRateFilter.setIndicatorDescription(parameterTable.getDescription());
						return;
					}
				}
			} else {
				indexAndRateFilter.setIndicatorDescription(null);
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Removes the values financian indicator.
	 */
	private void removeValuesFinancianIndicator() {		
		financialIndicator.setIdSourceInfo(null);
		financialIndicator.setIndicatorDescription(null);
		financialIndicator.setRateValue(null);
		financialIndicator.setPeriodicityType(null);
		financialIndicator.setInitialDate(null);
		financialIndicator.setEndDate(null);
		financialIndicator.setObservations(null);
	}	

	
	/**
	 * Gets the indicator type.
	 *
	 * @return the indicatorType
	 */
	public Integer getIndicatorType() {
		return indicatorType;
	}

	/**
	 * Sets the indicator type.
	 *
	 * @param indicatorType the indicatorType to set
	 */
	public void setIndicatorType(Integer indicatorType) {
		this.indicatorType = indicatorType;
	}

	/**
	 * Gets the sourceinfo.
	 *
	 * @return the sourceinfo
	 */
	public Integer getSourceinfo() {
		return sourceinfo;
	}

	/**
	 * Sets the sourceinfo.
	 *
	 * @param sourceinfo the sourceinfo to set
	 */
	public void setSourceinfo(Integer sourceinfo) {
		this.sourceinfo = sourceinfo;
	}

	/**
	 * Gets the financial indicator.
	 *
	 * @return the financialIndicator
	 */
	public FinancialIndicator getFinancialIndicator() {
		return financialIndicator;
	}

	/**
	 * Sets the financial indicator.
	 *
	 * @param financialIndicator the financialIndicator to set
	 */
	public void setFinancialIndicator(FinancialIndicator financialIndicator) {
		this.financialIndicator = financialIndicator;
	}

	/**
	 * Gets the period type list.
	 *
	 * @return the periodTypeList
	 */
	public List<ParameterTable> getPeriodTypeList() {
		return periodTypeList;
	}

	/**
	 * Sets the period type list.
	 *
	 * @param periodTypeList the periodTypeList to set
	 */
	public void setPeriodTypeList(List<ParameterTable> periodTypeList) {
		this.periodTypeList = periodTypeList;
	}

	/**
	 * Gets the index and rate filter.
	 *
	 * @return the indexAndRateFilter
	 */
	public IndexAndRateFilter getIndexAndRateFilter() {
		return indexAndRateFilter;
	}

	/**
	 * Sets the index and rate filter.
	 *
	 * @param indexAndRateFilter the indexAndRateFilter to set
	 */
	public void setIndexAndRateFilter(IndexAndRateFilter indexAndRateFilter) {
		this.indexAndRateFilter = indexAndRateFilter;
	}

	/**
	 * Gets the financial indicator state typeslist.
	 *
	 * @return the financialIndicatorStateTypeslist
	 */
	public List<FinancialIndicatorStateType> getFinancialIndicatorStateTypeslist() {
		return FinancialIndicatorStateType.list;
	}

	/**
	 * Gets the index and rate data model.
	 *
	 * @return the indexAndRateDataModel
	 */
	public IndexAndRateDataModel getIndexAndRateDataModel() {
		return indexAndRateDataModel;
	}

	/**
	 * Sets the index and rate data model.
	 *
	 * @param indexAndRateDataModel the indexAndRateDataModel to set
	 */
	public void setIndexAndRateDataModel(IndexAndRateDataModel indexAndRateDataModel) {
		this.indexAndRateDataModel = indexAndRateDataModel;
	}

	/**
	 * Gets the selected financial indicator.
	 *
	 * @return the selectedFinancialIndicator
	 */
	public FinancialIndicator getSelectedFinancialIndicator() {
		return selectedFinancialIndicator;
	}

	/**
	 * Sets the selected financial indicator.
	 *
	 * @param selectedFinancialIndicator the selectedFinancialIndicator to set
	 */
	public void setSelectedFinancialIndicator(
			FinancialIndicator selectedFinancialIndicator) {
		this.selectedFinancialIndicator = selectedFinancialIndicator;
	}

	/**
	 * Gets the financial indicator type list.
	 *
	 * @return the financial indicator type list
	 */
	public List<ParameterTable> getFinancialIndicatorTypeList() {
		return financialIndicatorTypeList;
	}


	/**
	 * Sets the financial indicator type list.
	 *
	 * @param financialIndicatorTypeList the new financial indicator type list
	 */
	public void setFinancialIndicatorTypeList(
			List<ParameterTable> financialIndicatorTypeList) {
		this.financialIndicatorTypeList = financialIndicatorTypeList;
	}

	/**
	 * Gets the information source list.
	 *
	 * @return the information source list
	 */
	public List<ParameterTable> getInformationSourceList() {
		return informationSourceList;
	}

	/**
	 * Sets the information source list.
	 *
	 * @param informationSourceList the new information source list
	 */
	public void setInformationSourceList(List<ParameterTable> informationSourceList) {
		this.informationSourceList = informationSourceList;
	}

	/**
	 * Gets the financial indicator status list.
	 *
	 * @return the financial indicator status list
	 */
	public List<ParameterTable> getFinancialIndicatorStatusList() {
		return financialIndicatorStatusList;
	}

	/**
	 * Sets the financial indicator status list.
	 *
	 * @param financialIndicatorStatusList the new financial indicator status list
	 */
	public void setFinancialIndicatorStatusList(
			List<ParameterTable> financialIndicatorStatusList) {
		this.financialIndicatorStatusList = financialIndicatorStatusList;
	}

	/**
	 * Gets the month list.
	 *
	 * @return the month list
	 */
	public List<MonthType> getMonthList() {
		return monthList;
	}

	/**
	 * Gets the year list.
	 *
	 * @return the year list
	 */
	public List<SelectItem> getYearList() {
		return yearList;
	}

	/**
	 * Sets the year list.
	 *
	 * @param yearList the new year list
	 */
	public void setYearList(List<SelectItem> yearList) {
		this.yearList = yearList;
	}

	/**
	 * Sets the month list.
	 *
	 * @param monthList the new month list
	 */
	public void setMonthList(List<MonthType> monthList) {
		this.monthList = monthList;
	}

	/**
	 * Gets the financial indicator code list.
	 *
	 * @return the financial indicator code list
	 */
	public List<ParameterTable> getFinancialIndicatorCodeList() {
		return financialIndicatorCodeList;
	}

	/**
	 * Sets the financial indicator code list.
	 *
	 * @param financialIndicatorCodeList the new financial indicator code list
	 */
	public void setFinancialIndicatorCodeList(
			List<ParameterTable> financialIndicatorCodeList) {
		this.financialIndicatorCodeList = financialIndicatorCodeList;
	}		

	/**
	 * Checks if is indicator type index.
	 *
	 * @return true, if is indicator type index
	 */
	public boolean isIndicatorTypeIndex() {
		return indicatorTypeIndex;
	}

	/**
	 * Sets the indicator type index.
	 *
	 * @param indicatorTypeIndex the new indicator type index
	 */
	public void setIndicatorTypeIndex(boolean indicatorTypeIndex) {
		this.indicatorTypeIndex = indicatorTypeIndex;
	}

	/**
	 * Checks if is indicator type rate.
	 *
	 * @return true, if is indicator type rate
	 */
	public boolean isIndicatorTypeRate() {
		return indicatorTypeRate;
	}

	/**
	 * Sets the indicator type rate.
	 *
	 * @param indicatorTypeRate the new indicator type rate
	 */
	public void setIndicatorTypeRate(boolean indicatorTypeRate) {
		this.indicatorTypeRate = indicatorTypeRate;
	}
	
	/**
	 * Checks if is ind disabled initial date.
	 *
	 * @return true, if is ind disabled initial date
	 */
	public boolean isIndDisabledInitialDate(){
		return Validations.validateIsNullOrNotPositive(financialIndicator.getPeriodicityType());
	}				

	/**
	 * Gets the disabled days.
	 *
	 * @return the disabled days
	 */
	public String getDisabledDays() {
		return disabledDays;
	}

	/**
	 * Sets the disabled days.
	 *
	 * @param disabledDays the new disabled days
	 */
	public void setDisabledDays(String disabledDays) {
		this.disabledDays = disabledDays;
	}
	
	/**
	 * Checks if is indicator type index for search.
	 *
	 * @return true, if is indicator type index for search
	 */
	public boolean isIndicatorTypeIndexForSearch(){
		return Validations.validateIsNotNull(indexAndRateFilter) && FinancialIndicatorType.INDEX.getCode().equals(indexAndRateFilter.getIndicatorType());
	}
	
	/**
	 * Checks if is indicator type rate for search.
	 *
	 * @return true, if is indicator type rate for search
	 */
	public boolean isIndicatorTypeRateForSearch(){
		return Validations.validateIsNotNull(indexAndRateFilter) && FinancialIndicatorType.RATE.getCode().equals(indexAndRateFilter.getIndicatorType());
	}		

	/**
	 * Gets the financial indicator code list for search.
	 *
	 * @return the financial indicator code list for search
	 */
	public List<ParameterTable> getFinancialIndicatorCodeListForSearch() {
		return financialIndicatorCodeListForSearch;
	}

	/**
	 * Sets the financial indicator code list for search.
	 *
	 * @param financialIndicatorCodeListForSearch the new financial indicator code list for search
	 */
	public void setFinancialIndicatorCodeListForSearch(
			List<ParameterTable> financialIndicatorCodeListForSearch) {
		this.financialIndicatorCodeListForSearch = financialIndicatorCodeListForSearch;
	}	
	
	/**
	 * Gets the user info.
	 *
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}
	
}
