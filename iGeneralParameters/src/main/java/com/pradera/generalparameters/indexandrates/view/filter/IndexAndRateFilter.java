package com.pradera.generalparameters.indexandrates.view.filter;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexAndRateFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class IndexAndRateFilter implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1755140896443946845L;

	/** The indicator Type. */
	private Integer indicatorType;
	
	/** The indicator code. */
	private Integer indicatorCode;
	
	/** The periodicity Type. */
	private Integer periodicityType;
	
	/** The Status. */
	private Integer status;
	
	/** The indicator description. */
	private String indicatorDescription;
	
	/**
	 * Instantiates a new index and rate filter.
	 */
	public IndexAndRateFilter() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Instantiates a new index and rate filter.
	 *
	 * @param indicatorType the indicator type
	 * @param indicatorCode the indicator code
	 * @param status the status
	 */
	public IndexAndRateFilter(Integer indicatorType, Integer indicatorCode,
			Integer status) {
		super();
		this.indicatorType = indicatorType;
		this.indicatorCode = indicatorCode;
		this.status = status;
	}

	/**
	 * Gets the indicator type.
	 *
	 * @return the indicatorType
	 */
	public Integer getIndicatorType() {
		return indicatorType;
	}
	
	/**
	 * Sets the indicator type.
	 *
	 * @param indicatorType the indicatorType to set
	 */
	public void setIndicatorType(Integer indicatorType) {
		this.indicatorType = indicatorType;
	}

	/**
	 * Gets the periodicity type.
	 *
	 * @return the periodicityType
	 */
	public Integer getPeriodicityType() {
		return periodicityType;
	}
	
	/**
	 * Sets the periodicity type.
	 *
	 * @param periodicityType the periodicityType to set
	 */
	public void setPeriodicityType(Integer periodicityType) {
		this.periodicityType = periodicityType;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Gets the indicator code.
	 *
	 * @return the indicator code
	 */
	public Integer getIndicatorCode() {
		return indicatorCode;
	}
	
	/**
	 * Sets the indicator code.
	 *
	 * @param indicatorCode the new indicator code
	 */
	public void setIndicatorCode(Integer indicatorCode) {
		this.indicatorCode = indicatorCode;
	}
	
	/**
	 * Gets the indicator description.
	 *
	 * @return the indicator description
	 */
	public String getIndicatorDescription() {
		return indicatorDescription;
	}
	
	/**
	 * Sets the indicator description.
	 *
	 * @param indicatorDescription the new indicator description
	 */
	public void setIndicatorDescription(String indicatorDescription) {
		this.indicatorDescription = indicatorDescription;
	}		
		
}
