package com.pradera.generalparameters.holiday.view.filter;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class HolidayFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class HolidayFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/**  The GeographicLocationPk. */
	private Integer geographicLocationPk;
	
	/**  The year. */
	private Integer year;
	
	/**  The Code of the GeographicLocatio. */
	private String codeGeographicLoc;

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * Sets the year.
	 *
	 * @param year            the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * Gets the geographic location pk.
	 *
	 * @return the geographicLocationPk
	 */
	public Integer getGeographicLocationPk() {
		return geographicLocationPk;
	}

	/**
	 * Sets the geographic location pk.
	 *
	 * @param geographicLocationPk            the geographicLocationPk to set
	 */
	public void setGeographicLocationPk(Integer geographicLocationPk) {
		this.geographicLocationPk = geographicLocationPk;
	}

	/**
	 * Gets the code geographic loc.
	 *
	 * @return the codeGeographicLoc
	 */
	public String getCodeGeographicLoc() {
		return codeGeographicLoc;
	}

	/**
	 * Sets the code geographic loc.
	 *
	 * @param codeGeographicLoc the codeGeographicLoc to set
	 */
	public void setCodeGeographicLoc(String codeGeographicLoc) {
		this.codeGeographicLoc = codeGeographicLoc;
	}

	

	
}
