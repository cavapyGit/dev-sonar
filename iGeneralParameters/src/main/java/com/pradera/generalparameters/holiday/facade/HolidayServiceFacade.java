package com.pradera.generalparameters.holiday.facade;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.generalparameters.holiday.batch.HolidayProcessContants;
import com.pradera.generalparameters.holiday.service.HolidayServiceBean;
import com.pradera.generalparameters.holiday.view.filter.HolidayFilter;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.process.BusinessProcess;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolidayServiceFacade.
 *
 * @author PraderaTechnologies
 * The Class HolidayServiceFacade
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolidayServiceFacade {

	
	/** The HolidayServiceBean. */
	@EJB
	HolidayServiceBean holidayServiceBean;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The batch service bean. */
	@EJB
	private BatchServiceBean batchServiceBean;
	
	
	/**
	 * Gets the holiday service bean.
	 *
	 * @return the holidayServiceBean
	 */
	public HolidayServiceBean getHolidayServiceBean() {
		return holidayServiceBean;
	}

	/**
	 * Sets the holiday service bean.
	 *
	 * @param holidayServiceBean the holidayServiceBean to set
	 */
	public void setHolidayServiceBean(HolidayServiceBean holidayServiceBean) {
		this.holidayServiceBean = holidayServiceBean;
	}

	/**
	 * Register Holiday Service Facade.
	 *
	 * @param holiday the holiday
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean registerHolidayFacade(Holiday holiday) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        holidayServiceBean.create(holiday);
        
        
        //Call batch process
        Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(ViewOperationsType.REGISTER.getCode().toString(), ViewOperationsType.REGISTER.getCode());
		processParameters.put(HolidayProcessContants.COUNTRY_PARAM, holiday.getGeographicLocation().getIdGeographicLocationPk());
	    DateFormat df = new SimpleDateFormat(HolidayProcessContants.DATE_FORMAT); 
		processParameters.put(HolidayProcessContants.DATE_PARAM, df.format(holiday.getHolidayDate()));
		
		BusinessProcess objBusinessProcess=new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLIDAY_WORK_DAY.getCode());
        batchServiceBean.registerBatchTx(loggerUser.getUserName(), objBusinessProcess, processParameters);
		return true;
	}
	
	/**
	 * Update holiday facade.
	 *
	 * @param holiday the holiday
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean updateHolidayFacade(Holiday holiday) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        holidayServiceBean.update(holiday);
		return true;
	}
	
	/**
	 * Delete holiday facade.
	 *
	 * @param holiday the holiday
	 * @return true, if successful
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean deleteHolidayFacade(Holiday holiday){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
        holidayServiceBean.delete(Holiday.class,holiday.getIdHolidayPk());
		return true;
	}
	
	/**
	 * Gets the Country List.
	 *
	 * @return GeographicLocation
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getCountryListServiceFacade() throws ServiceException {
		return holidayServiceBean.getCountryListServiceBean();
	}
	
	
	/**
	 * Gets the Holiday List according to country and year.
	 *
	 * @param holidayFilter the holiday filter
	 * @return Holiday
	 * @throws ServiceException the service exception
	 */	
	public List<Holiday> searchHolidayByFilterServiceFacade(HolidayFilter holidayFilter) throws ServiceException{
		return holidayServiceBean.searchHolidayByFilterServiceFacade(holidayFilter);
	}
}