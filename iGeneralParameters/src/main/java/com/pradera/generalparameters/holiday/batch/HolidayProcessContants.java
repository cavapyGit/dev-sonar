package com.pradera.generalparameters.holiday.batch;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2014.</li>
 * </ul>
 * 
 * The Class HolidayProcessContants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/01/2014
 */
public class HolidayProcessContants {

	/** The date param. */
	public static String DATE_PARAM = "date_param";
	
	/** The country param. */
	public static String COUNTRY_PARAM = "country_param";
	
	/** The date format. */
	public static String DATE_FORMAT = "dd/MM/yyyy";
}