package com.pradera.generalparameters.holiday.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;


// TODO: Auto-generated Javadoc
/**
* The Class HolidayServiceBean.
* @author PraderaTechnologies.
*/
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolidaySettlementService extends CrudDaoServiceBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Gets the operations by settlement date.
	 *
	 * @param holidayDate the holiday date
	 * @return the operations by settlement date
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getOperationsBySettlementDate(Date holidayDate) throws ServiceException{
		
		StringBuilder query = new StringBuilder();
		query.append("Select so.idSettlementOperationPk, so.operationState	");
		query.append("From SettlementOperation so ");
		query.append("Where	( (so.operationPart = :cashPart and so.operationState IN (:cashStates) ) or ");
		query.append("        (so.operationPart = :termPart and so.operationState IN (:termStates) ) )");
		query.append(" and  so.settlementDate = :settlementDate ");
		Query queryJpql = em.createQuery(query.toString());
		queryJpql.setParameter("settlementDate", holidayDate);
		queryJpql.setParameter("cashPart", ComponentConstant.CASH_PART);
		queryJpql.setParameter("termPart", ComponentConstant.TERM_PART);
		List<Integer> cashStates = new ArrayList<Integer>();
		cashStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		cashStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		queryJpql.setParameter("cashStates", cashStates);
		List<Integer> termStates = new ArrayList<Integer>();
		termStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		queryJpql.setParameter("termStates", termStates);
		
		return queryJpql.getResultList();
	}
	
	/**
	 * Assigment operation extension.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param nextWorkDay the next work day
	 * @throws ServiceException the service exception
	 */
	public void assigmentOperationExtension(Long idSettlementOperation, Date nextWorkDay) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		StringBuilder queryExecute = new StringBuilder();
		
		queryExecute.append("Update SettlementOperation Set ");
		queryExecute.append(" settlementDate = :nextWorkDay,	");
		queryExecute.append(" lastModifyApp = :lastModifyApp, lastModifyIp = :lastModifyIp, lastModifyUser = :lastModifyUser, lastModifyDate = :lastModifyDate	");
		queryExecute.append("Where idSettlementOperationPk = :operationId");
		
		Query queryJpql = em.createQuery(queryExecute.toString());
		queryJpql.setParameter("oneIndicator", BooleanType.YES.getCode());
		queryJpql.setParameter("nextWorkDay", nextWorkDay);
		queryJpql.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		queryJpql.setParameter("lastModifyIp", loggerUser.getIpAddress());
		queryJpql.setParameter("lastModifyUser", loggerUser.getUserName());
		queryJpql.setParameter("lastModifyDate", CommonsUtilities.currentDateTime());
		queryJpql.setParameter("operationId", idSettlementOperation);
		
		queryJpql.executeUpdate();

	}
}