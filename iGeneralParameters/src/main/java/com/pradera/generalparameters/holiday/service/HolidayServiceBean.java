package com.pradera.generalparameters.holiday.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.holiday.view.filter.HolidayFilter;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.type.HolidayStateType;


// TODO: Auto-generated Javadoc
/**
* The Class HolidayServiceBean.
* @author PraderaTechnologies.
*/
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolidayServiceBean extends CrudDaoServiceBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	

	
	/**
	 * Gets the Country.
	 *
	 * @return GeographicLocation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<GeographicLocation> getCountryListServiceBean() throws ServiceException {
		Query query = em.createQuery("select gl from GeographicLocation gl where gl.geographicLocation is null order by name asc");
		return (List<GeographicLocation>) query.getResultList();
	}
	
	
	/**
	 *  Search Holiday .
	 *
	 * @param holidayFilter the holiday filter
	 * @return the List
	 * @throws ServiceException the service exception
	 */
	   public List<Holiday> searchHolidayByFilterServiceFacade(HolidayFilter holidayFilter) throws ServiceException{
		   	StringBuilder sbQuery = new StringBuilder();
			
			sbQuery.append("select hol from Holiday hol where hol.state != :holidayState ");
			if (Validations.validateIsNotNullAndNotEmpty(holidayFilter.getGeographicLocationPk()) && holidayFilter.getGeographicLocationPk() > 0) 
			{
				sbQuery.append(" and hol.geographicLocation.idGeographicLocationPk= :geographicLocationPK");		
			}
			if (Validations.validateIsNotNullAndNotEmpty(holidayFilter.getYear())) 
			{
				sbQuery.append(" and EXTRACT(Year from hol.holidayDate) = :Year");	
			}
			
			TypedQuery<Holiday> typedQuery =  em.createQuery(sbQuery.toString(),Holiday.class);
			typedQuery.setParameter("holidayState", HolidayStateType.DELETED.getCode());
			
			if (Validations.validateIsNotNullAndNotEmpty(holidayFilter.getGeographicLocationPk()) && holidayFilter.getGeographicLocationPk() > 0){
				typedQuery.setParameter("geographicLocationPK", holidayFilter.getGeographicLocationPk());
			}
			if (Validations.validateIsNotNullAndNotEmpty(holidayFilter.getYear())){
				typedQuery.setParameter("Year", holidayFilter.getYear());
			}
			
			List<Holiday> list = (List<Holiday>)typedQuery.getResultList();
			
			if(Validations.validateListIsNotNullAndNotEmpty(list)){
				for(Holiday holiday : list){
					holiday.getGeographicLocation().getIdGeographicLocationPk();
				}
			}
		
			return list;
	}
}
