package com.pradera.generalparameters.holiday.view;

import java.io.Serializable;

import com.pradera.model.generalparameter.Holiday;


// TODO: Auto-generated Javadoc
/**
 *  The Class HolidayDayMonthDtls
 *  
 * @author PraderaTechnologies
 *
 */
public class HolidayDayMonthDtls implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 *  Default constructor.
	 */
	public HolidayDayMonthDtls(){
	} 
	
	/** The holidayobj. */
	private Holiday[][] holidayobj;
	
	/**
	 *  
	 * Parameterized constructor for the HolidayDayMonthDtls.
	 *
	 * @param month the month
	 * @param dayOne the day one
	 * @param dayTwo the day two
	 * @param dayThree the day three
	 * @param dayFour the day four
	 * @param dayFive the day five
	 * @param daySix the day six
	 * @param daySeven the day seven
	 * @param dayEight the day eight
	 * @param dayNine the day nine
	 * @param dayTen the day ten
	 * @param dayEleven the day eleven
	 * @param dayTwelve the day twelve
	 * @param dayThirteen the day thirteen
	 * @param dayFourteen the day fourteen
	 * @param dayFifteen the day fifteen
	 * @param daySixteen the day sixteen
	 * @param daySeventeen the day seventeen
	 * @param dayEighteen the day eighteen
	 * @param dayNineteen the day nineteen
	 * @param dayTwenty the day twenty
	 * @param dayTwentyOne the day twenty one
	 * @param dayTwentyTwo the day twenty two
	 * @param dayTwentyThree the day twenty three
	 * @param dayTwentyFour the day twenty four
	 * @param dayTwentyFive the day twenty five
	 * @param dayTwentySix the day twenty six
	 * @param dayTwentySeven the day twenty seven
	 * @param dayTwentyEight the day twenty eight
	 * @param dayTwentyNine the day twenty nine
	 * @param dayThirty the day thirty
	 * @param dayThirtyOne the day thirty one
	 * @param current the current
	 * @param currentDay the current day
	 * @param holiday the holiday
	 */
	public HolidayDayMonthDtls(String month, String dayOne, String dayTwo,String dayThree, String dayFour,String dayFive, 
			            String daySix, String daySeven, String dayEight, String dayNine, String dayTen, String dayEleven, 
			            String dayTwelve,String dayThirteen, String dayFourteen,String dayFifteen, String daySixteen,
			            String daySeventeen, String dayEighteen, String dayNineteen, String dayTwenty, String dayTwentyOne,
			            String dayTwentyTwo, String dayTwentyThree, String dayTwentyFour, String dayTwentyFive, String dayTwentySix,
			            String dayTwentySeven, String dayTwentyEight,String dayTwentyNine, String dayThirty, String dayThirtyOne, boolean current, int currentDay,Holiday[][] holiday) {
		super();
		this.holidayobj = holiday;
		this.month = month;
		this.dayOne = dayOne;
		this.dayTwo = dayTwo;
		this.dayThree = dayThree;
		this.dayFour = dayFour;
		this.dayFive = dayFive;
		this.daySix = daySix;
		this.daySeven = daySeven;
		this.dayEight = dayEight;
		this.dayNine = dayNine;
		this.dayTen = dayTen;
		this.dayEleven = dayEleven;
		this.dayTwelve = dayTwelve;
		this.dayThirteen = dayThirteen;
		this.dayFourteen = dayFourteen;
		this.dayFifteen = dayFifteen;
		this.daySixteen = daySixteen;
		this.daySeventeen = daySeventeen;
		this.dayEighteen = dayEighteen;
		this.dayNineteen = dayNineteen;
		this.dayTwenty = dayTwenty;
		this.dayTwentyOne = dayTwentyOne;
		this.dayTwentyTwo = dayTwentyTwo;
		this.dayTwentyThree = dayTwentyThree;
		this.dayTwentyFour = dayTwentyFour;
		this.dayTwentyFive = dayTwentyFive;
		this.dayTwentySix = dayTwentySix;
		this.dayTwentySeven = dayTwentySeven;
		this.dayTwentyEight = dayTwentyEight;
		this.dayTwentyNine = dayTwentyNine;
		this.dayThirty = dayThirty;
		this.dayThirtyOne = dayThirtyOne;
		this.current = current;
		switch (currentDay) {
		case 1:
			bdayOne = true;
			break;
		case 2:
			bdayTwo = true;
			break;
		case 3:
			bdayThree = true;
			break;
		case 4:
			bdayFour = true;
			break;
		case 5:
			bdayFive = true;
			break;
		case 6:
			bdaySix = true;
			break;
		case 7:
			bdaySeven = true;
			break;
		case 8:
			bdayEight = true;
			break;
		case 9:
			bdayNine = true;
			break;
		case 10:
			bdayTen = true;
			break;
		case 11:
			bdayEleven = true;
			break;
		case 12:
			bdayTwelve = true;
			break;
		case 13:
			bdayThirteen = true;
			break;
		case 14:
			bdayFourteen = true;
			break;
		case 15:
			bdayFifteen = true;
			break;
		case 16:
			bdaySixteen = true;
			break;
		case 17:
			bdaySeventeen = true;
			break;
		case 18:
			bdayEighteen = true;
			break;
		case 19:
			bdayNineteen = true;
			break;
		case 20:
			bdayTwenty = true;
			break;
		case 21:
			bdayTwentyOne = true;
			break;
		case 22:
			bdayTwentyTwo = true;
			break;
		case 23:
			bdayTwentyThree = true;
			break;
		case 24:
			bdayTwentyFour = true;
			break;
		case 25:
			bdayTwentyFive = true;
			break;
		case 26:
			bdayTwentySix = true;
			break;
		case 27:
			bdayTwentySeven = true;
			break;
		case 28:
			bdayTwentyEight = true;
			break;
		case 29:
			bdayTwentyNine = true;
			break;
		case 30:
			bdayThirty = true;
			break;
		case 31:
			bdayThirtyOne = true;
			break;
		default:
			break;
		}

	}

	/**  The Month. */
	private String month;
	
	/**  The dayOne of a month. */
	private String dayOne;	
	
	/**  The dayTwo  of a month. */
	private String dayTwo;	
	
	/**  The dayThree  of a month. */
	private String dayThree;
	
	/**  The dayFour  of a month. */
	private String dayFour;
	
	/**  The dayFive of a month. */
	private String dayFive;	
	
	/**  The daySix  of a month. */
	private String daySix;	
	
	/**  The daySeven  of a month. */
	private String daySeven;
	
	/**  The dayEight  of a month. */
	private String dayEight;
	
	/**  The dayNine of a month. */
	private String dayNine;	
	
	/**  The dayTen  of a month. */
	private String dayTen;	
	
	/**  The dayEleven  of a month. */
	private String dayEleven;
	
	/**  The dayTwelve  of a month. */
	private String dayTwelve;
	
	/**  The dayThirteen of a month. */
	private String dayThirteen;	
	
	/**  The dayFourteen  of a month. */
	private String dayFourteen;	
	
	/**  The dayFifteen  of a month. */
	private String dayFifteen;
	
	/**  The daySixteen  of a month. */
	private String daySixteen;
	
	/**  The daySeventeen of a month. */
	private String daySeventeen;	
	
	/**  The dayEighteen  of a month. */
	private String dayEighteen;	
	
	/**  The dayNineteen  of a month. */
	private String dayNineteen;
	
	/**  The dayTwenty  of a month. */
	private String dayTwenty;
	
	/**  The dayTwentyOne of a month. */
	private String dayTwentyOne;	
	
	/**  The dayTwentyTwo  of a month. */
	private String dayTwentyTwo;	
	
	/**  The dayTwentyThree  of a month. */
	private String dayTwentyThree;
	
	/**  The dayTwentyFour  of a month. */
	private String dayTwentyFour;
	
	/**  The dayTwentyFive of a month. */
	private String dayTwentyFive;	
	
	/**  The dayTwentySix  of a month. */
	private String dayTwentySix;	
	
	/**  The dayTwentySeven  of a month. */
	private String dayTwentySeven;
	
	/**  The dayTwentyEight  of a month. */
	private String dayTwentyEight;
	
	/**  The dayTwentyNine of a month. */
	private String dayTwentyNine;	
	
	/**  The dayThirty  of a month. */
	private String dayThirty;	
	
	/**  The dayThirtyOne  of a month. */
	private String dayThirtyOne;
	
	/**  The dayOne of a month. */
	private boolean bdayOne;	
	
	/**  The dayTwo  of a month. */
	private boolean bdayTwo;	
	
	/**  The dayThree  of a month. */
	private boolean bdayThree;
	
	/**  The dayFour  of a month. */
	private boolean bdayFour;
	
	/**  The dayFive of a month. */
	private boolean bdayFive;	
	
	/**  The daySix  of a month. */
	private boolean bdaySix;	
	
	/**  The daySeven  of a month. */
	private boolean bdaySeven;
	
	/**  The dayEight  of a month. */
	private boolean bdayEight;
	
	/**  The dayNine of a month. */
	private boolean bdayNine;	
	
	/**  The dayTen  of a month. */
	private boolean bdayTen;	
	
	/**  The dayEleven  of a month. */
	private boolean bdayEleven;
	
	/**  The dayTwelve  of a month. */
	private boolean bdayTwelve;
	
	/**  The dayThirteen of a month. */
	private boolean bdayThirteen;	
	
	/**  The dayFourteen  of a month. */
	private boolean bdayFourteen;	
	
	/**  The dayFifteen  of a month. */
	private boolean bdayFifteen;
	
	/**  The daySixteen  of a month. */
	private boolean bdaySixteen;
	
	/**  The daySeventeen of a month. */
	private boolean bdaySeventeen;	
	
	/**  The dayEighteen  of a month. */
	private boolean bdayEighteen;	
	
	/**  The dayNineteen  of a month. */
	private boolean bdayNineteen;
	
	/**  The dayTwenty  of a month. */
	private boolean bdayTwenty;
	
	/**  The dayTwentyOne of a month. */
	private boolean bdayTwentyOne;	
	
	/**  The dayTwentyTwo  of a month. */
	private boolean bdayTwentyTwo;	
	
	/**  The dayTwentyThree  of a month. */
	private boolean bdayTwentyThree;
	
	/**  The dayTwentyFour  of a month. */
	private boolean bdayTwentyFour;
	
	/**  The dayTwentyFive of a month. */
	private boolean bdayTwentyFive;	
	
	/**  The dayTwentySix  of a month. */
	private boolean bdayTwentySix;	
	
	/**  The dayTwentySeven  of a month. */
	private boolean bdayTwentySeven;
	
	/**  The dayTwentyEight  of a month. */
	private boolean bdayTwentyEight;
	
	/**  The dayTwentyNine of a month. */
	private boolean bdayTwentyNine;	
	
	/**  The dayThirty  of a month. */
	private boolean bdayThirty;	
	
	/**  The dayThirtyOne  of a month. */
	private boolean bdayThirtyOne;
	
	/** The current. */
	private boolean current;

	/**
	 * Gets the holidayobj.
	 *
	 * @return the holidayobj
	 */
	public Holiday[][] getHolidayobj() {
		return holidayobj;
	}

	/**
	 * Sets the holidayobj.
	 *
	 * @param holidayobj the holidayobj to set
	 */
	public void setHolidayobj(Holiday[][] holidayobj) {
		this.holidayobj = holidayobj;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * Sets the month.
	 *
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * Gets the day one.
	 *
	 * @return the dayOne
	 */
	public String getDayOne() {
		return dayOne;
	}

	/**
	 * Sets the day one.
	 *
	 * @param dayOne the dayOne to set
	 */
	public void setDayOne(String dayOne) {
		this.dayOne = dayOne;
	}

	/**
	 * Gets the day two.
	 *
	 * @return the dayTwo
	 */
	public String getDayTwo() {
		return dayTwo;
	}

	/**
	 * Sets the day two.
	 *
	 * @param dayTwo the dayTwo to set
	 */
	public void setDayTwo(String dayTwo) {
		this.dayTwo = dayTwo;
	}

	/**
	 * Gets the day three.
	 *
	 * @return the dayThree
	 */
	public String getDayThree() {
		return dayThree;
	}

	/**
	 * Sets the day three.
	 *
	 * @param dayThree the dayThree to set
	 */
	public void setDayThree(String dayThree) {
		this.dayThree = dayThree;
	}

	/**
	 * Gets the day four.
	 *
	 * @return the dayFour
	 */
	public String getDayFour() {
		return dayFour;
	}

	/**
	 * Sets the day four.
	 *
	 * @param dayFour the dayFour to set
	 */
	public void setDayFour(String dayFour) {
		this.dayFour = dayFour;
	}

	/**
	 * Gets the day five.
	 *
	 * @return the dayFive
	 */
	public String getDayFive() {
		return dayFive;
	}

	/**
	 * Sets the day five.
	 *
	 * @param dayFive the dayFive to set
	 */
	public void setDayFive(String dayFive) {
		this.dayFive = dayFive;
	}

	/**
	 * Gets the day six.
	 *
	 * @return the daySix
	 */
	public String getDaySix() {
		return daySix;
	}

	/**
	 * Sets the day six.
	 *
	 * @param daySix the daySix to set
	 */
	public void setDaySix(String daySix) {
		this.daySix = daySix;
	}

	/**
	 * Gets the day seven.
	 *
	 * @return the daySeven
	 */
	public String getDaySeven() {
		return daySeven;
	}

	/**
	 * Sets the day seven.
	 *
	 * @param daySeven the daySeven to set
	 */
	public void setDaySeven(String daySeven) {
		this.daySeven = daySeven;
	}

	/**
	 * Gets the day eight.
	 *
	 * @return the dayEight
	 */
	public String getDayEight() {
		return dayEight;
	}

	/**
	 * Sets the day eight.
	 *
	 * @param dayEight the dayEight to set
	 */
	public void setDayEight(String dayEight) {
		this.dayEight = dayEight;
	}

	/**
	 * Gets the day nine.
	 *
	 * @return the dayNine
	 */
	public String getDayNine() {
		return dayNine;
	}

	/**
	 * Sets the day nine.
	 *
	 * @param dayNine the dayNine to set
	 */
	public void setDayNine(String dayNine) {
		this.dayNine = dayNine;
	}

	/**
	 * Gets the day ten.
	 *
	 * @return the dayTen
	 */
	public String getDayTen() {
		return dayTen;
	}

	/**
	 * Sets the day ten.
	 *
	 * @param dayTen the dayTen to set
	 */
	public void setDayTen(String dayTen) {
		this.dayTen = dayTen;
	}

	/**
	 * Gets the day eleven.
	 *
	 * @return the dayEleven
	 */
	public String getDayEleven() {
		return dayEleven;
	}

	/**
	 * Sets the day eleven.
	 *
	 * @param dayEleven the dayEleven to set
	 */
	public void setDayEleven(String dayEleven) {
		this.dayEleven = dayEleven;
	}

	/**
	 * Gets the day twelve.
	 *
	 * @return the dayTwelve
	 */
	public String getDayTwelve() {
		return dayTwelve;
	}

	/**
	 * Sets the day twelve.
	 *
	 * @param dayTwelve the dayTwelve to set
	 */
	public void setDayTwelve(String dayTwelve) {
		this.dayTwelve = dayTwelve;
	}

	/**
	 * Gets the day thirteen.
	 *
	 * @return the dayThirteen
	 */
	public String getDayThirteen() {
		return dayThirteen;
	}

	/**
	 * Sets the day thirteen.
	 *
	 * @param dayThirteen the dayThirteen to set
	 */
	public void setDayThirteen(String dayThirteen) {
		this.dayThirteen = dayThirteen;
	}

	/**
	 * Gets the day fourteen.
	 *
	 * @return the dayFourteen
	 */
	public String getDayFourteen() {
		return dayFourteen;
	}

	/**
	 * Sets the day fourteen.
	 *
	 * @param dayFourteen the dayFourteen to set
	 */
	public void setDayFourteen(String dayFourteen) {
		this.dayFourteen = dayFourteen;
	}

	/**
	 * Gets the day fifteen.
	 *
	 * @return the dayFifteen
	 */
	public String getDayFifteen() {
		return dayFifteen;
	}

	/**
	 * Sets the day fifteen.
	 *
	 * @param dayFifteen the dayFifteen to set
	 */
	public void setDayFifteen(String dayFifteen) {
		this.dayFifteen = dayFifteen;
	}

	/**
	 * Gets the day sixteen.
	 *
	 * @return the daySixteen
	 */
	public String getDaySixteen() {
		return daySixteen;
	}

	/**
	 * Sets the day sixteen.
	 *
	 * @param daySixteen the daySixteen to set
	 */
	public void setDaySixteen(String daySixteen) {
		this.daySixteen = daySixteen;
	}

	/**
	 * Gets the day seventeen.
	 *
	 * @return the daySeventeen
	 */
	public String getDaySeventeen() {
		return daySeventeen;
	}

	/**
	 * Sets the day seventeen.
	 *
	 * @param daySeventeen the daySeventeen to set
	 */
	public void setDaySeventeen(String daySeventeen) {
		this.daySeventeen = daySeventeen;
	}

	/**
	 * Gets the day eighteen.
	 *
	 * @return the dayEighteen
	 */
	public String getDayEighteen() {
		return dayEighteen;
	}

	/**
	 * Sets the day eighteen.
	 *
	 * @param dayEighteen the dayEighteen to set
	 */
	public void setDayEighteen(String dayEighteen) {
		this.dayEighteen = dayEighteen;
	}

	/**
	 * Gets the day nineteen.
	 *
	 * @return the dayNineteen
	 */
	public String getDayNineteen() {
		return dayNineteen;
	}

	/**
	 * Sets the day nineteen.
	 *
	 * @param dayNineteen the dayNineteen to set
	 */
	public void setDayNineteen(String dayNineteen) {
		this.dayNineteen = dayNineteen;
	}

	/**
	 * Gets the day twenty.
	 *
	 * @return the dayTwenty
	 */
	public String getDayTwenty() {
		return dayTwenty;
	}

	/**
	 * Sets the day twenty.
	 *
	 * @param dayTwenty the dayTwenty to set
	 */
	public void setDayTwenty(String dayTwenty) {
		this.dayTwenty = dayTwenty;
	}

	/**
	 * Gets the day twenty one.
	 *
	 * @return the dayTwentyOne
	 */
	public String getDayTwentyOne() {
		return dayTwentyOne;
	}

	/**
	 * Sets the day twenty one.
	 *
	 * @param dayTwentyOne the dayTwentyOne to set
	 */
	public void setDayTwentyOne(String dayTwentyOne) {
		this.dayTwentyOne = dayTwentyOne;
	}

	/**
	 * Gets the day twenty two.
	 *
	 * @return the dayTwentyTwo
	 */
	public String getDayTwentyTwo() {
		return dayTwentyTwo;
	}

	/**
	 * Sets the day twenty two.
	 *
	 * @param dayTwentyTwo the dayTwentyTwo to set
	 */
	public void setDayTwentyTwo(String dayTwentyTwo) {
		this.dayTwentyTwo = dayTwentyTwo;
	}

	/**
	 * Gets the day twenty three.
	 *
	 * @return the dayTwentyThree
	 */
	public String getDayTwentyThree() {
		return dayTwentyThree;
	}

	/**
	 * Sets the day twenty three.
	 *
	 * @param dayTwentyThree the dayTwentyThree to set
	 */
	public void setDayTwentyThree(String dayTwentyThree) {
		this.dayTwentyThree = dayTwentyThree;
	}

	/**
	 * Gets the day twenty four.
	 *
	 * @return the dayTwentyFour
	 */
	public String getDayTwentyFour() {
		return dayTwentyFour;
	}

	/**
	 * Sets the day twenty four.
	 *
	 * @param dayTwentyFour the dayTwentyFour to set
	 */
	public void setDayTwentyFour(String dayTwentyFour) {
		this.dayTwentyFour = dayTwentyFour;
	}

	/**
	 * Gets the day twenty five.
	 *
	 * @return the dayTwentyFive
	 */
	public String getDayTwentyFive() {
		return dayTwentyFive;
	}

	/**
	 * Sets the day twenty five.
	 *
	 * @param dayTwentyFive the dayTwentyFive to set
	 */
	public void setDayTwentyFive(String dayTwentyFive) {
		this.dayTwentyFive = dayTwentyFive;
	}

	/**
	 * Gets the day twenty six.
	 *
	 * @return the dayTwentySix
	 */
	public String getDayTwentySix() {
		return dayTwentySix;
	}

	/**
	 * Sets the day twenty six.
	 *
	 * @param dayTwentySix the dayTwentySix to set
	 */
	public void setDayTwentySix(String dayTwentySix) {
		this.dayTwentySix = dayTwentySix;
	}

	/**
	 * Gets the day twenty seven.
	 *
	 * @return the dayTwentySeven
	 */
	public String getDayTwentySeven() {
		return dayTwentySeven;
	}

	/**
	 * Sets the day twenty seven.
	 *
	 * @param dayTwentySeven the dayTwentySeven to set
	 */
	public void setDayTwentySeven(String dayTwentySeven) {
		this.dayTwentySeven = dayTwentySeven;
	}

	/**
	 * Gets the day twenty eight.
	 *
	 * @return the dayTwentyEight
	 */
	public String getDayTwentyEight() {
		return dayTwentyEight;
	}

	/**
	 * Sets the day twenty eight.
	 *
	 * @param dayTwentyEight the dayTwentyEight to set
	 */
	public void setDayTwentyEight(String dayTwentyEight) {
		this.dayTwentyEight = dayTwentyEight;
	}

	/**
	 * Gets the day twenty nine.
	 *
	 * @return the dayTwentyNine
	 */
	public String getDayTwentyNine() {
		return dayTwentyNine;
	}

	/**
	 * Sets the day twenty nine.
	 *
	 * @param dayTwentyNine the dayTwentyNine to set
	 */
	public void setDayTwentyNine(String dayTwentyNine) {
		this.dayTwentyNine = dayTwentyNine;
	}

	/**
	 * Gets the day thirty.
	 *
	 * @return the dayThirty
	 */
	public String getDayThirty() {
		return dayThirty;
	}

	/**
	 * Sets the day thirty.
	 *
	 * @param dayThirty the dayThirty to set
	 */
	public void setDayThirty(String dayThirty) {
		this.dayThirty = dayThirty;
	}

	/**
	 * Gets the day thirty one.
	 *
	 * @return the dayThirtyOne
	 */
	public String getDayThirtyOne() {
		return dayThirtyOne;
	}

	/**
	 * Sets the day thirty one.
	 *
	 * @param dayThirtyOne the dayThirtyOne to set
	 */
	public void setDayThirtyOne(String dayThirtyOne) {
		this.dayThirtyOne = dayThirtyOne;
	}

	/**
	 * Checks if is bday one.
	 *
	 * @return the bdayOne
	 */
	public boolean isBdayOne() {
		return bdayOne;
	}

	/**
	 * Sets the bday one.
	 *
	 * @param bdayOne the bdayOne to set
	 */
	public void setBdayOne(boolean bdayOne) {
		this.bdayOne = bdayOne;
	}

	/**
	 * Checks if is bday two.
	 *
	 * @return the bdayTwo
	 */
	public boolean isBdayTwo() {
		return bdayTwo;
	}

	/**
	 * Sets the bday two.
	 *
	 * @param bdayTwo the bdayTwo to set
	 */
	public void setBdayTwo(boolean bdayTwo) {
		this.bdayTwo = bdayTwo;
	}

	/**
	 * Checks if is bday three.
	 *
	 * @return the bdayThree
	 */
	public boolean isBdayThree() {
		return bdayThree;
	}

	/**
	 * Sets the bday three.
	 *
	 * @param bdayThree the bdayThree to set
	 */
	public void setBdayThree(boolean bdayThree) {
		this.bdayThree = bdayThree;
	}

	/**
	 * Checks if is bday four.
	 *
	 * @return the bdayFour
	 */
	public boolean isBdayFour() {
		return bdayFour;
	}

	/**
	 * Sets the bday four.
	 *
	 * @param bdayFour the bdayFour to set
	 */
	public void setBdayFour(boolean bdayFour) {
		this.bdayFour = bdayFour;
	}

	/**
	 * Checks if is bday five.
	 *
	 * @return the bdayFive
	 */
	public boolean isBdayFive() {
		return bdayFive;
	}

	/**
	 * Sets the bday five.
	 *
	 * @param bdayFive the bdayFive to set
	 */
	public void setBdayFive(boolean bdayFive) {
		this.bdayFive = bdayFive;
	}

	/**
	 * Checks if is bday six.
	 *
	 * @return the bdaySix
	 */
	public boolean isBdaySix() {
		return bdaySix;
	}

	/**
	 * Sets the bday six.
	 *
	 * @param bdaySix the bdaySix to set
	 */
	public void setBdaySix(boolean bdaySix) {
		this.bdaySix = bdaySix;
	}

	/**
	 * Checks if is bday seven.
	 *
	 * @return the bdaySeven
	 */
	public boolean isBdaySeven() {
		return bdaySeven;
	}

	/**
	 * Sets the bday seven.
	 *
	 * @param bdaySeven the bdaySeven to set
	 */
	public void setBdaySeven(boolean bdaySeven) {
		this.bdaySeven = bdaySeven;
	}

	/**
	 * Checks if is bday eight.
	 *
	 * @return the bdayEight
	 */
	public boolean isBdayEight() {
		return bdayEight;
	}

	/**
	 * Sets the bday eight.
	 *
	 * @param bdayEight the bdayEight to set
	 */
	public void setBdayEight(boolean bdayEight) {
		this.bdayEight = bdayEight;
	}

	/**
	 * Checks if is bday nine.
	 *
	 * @return the bdayNine
	 */
	public boolean isBdayNine() {
		return bdayNine;
	}

	/**
	 * Sets the bday nine.
	 *
	 * @param bdayNine the bdayNine to set
	 */
	public void setBdayNine(boolean bdayNine) {
		this.bdayNine = bdayNine;
	}

	/**
	 * Checks if is bday ten.
	 *
	 * @return the bdayTen
	 */
	public boolean isBdayTen() {
		return bdayTen;
	}

	/**
	 * Sets the bday ten.
	 *
	 * @param bdayTen the bdayTen to set
	 */
	public void setBdayTen(boolean bdayTen) {
		this.bdayTen = bdayTen;
	}

	/**
	 * Checks if is bday eleven.
	 *
	 * @return the bdayEleven
	 */
	public boolean isBdayEleven() {
		return bdayEleven;
	}

	/**
	 * Sets the bday eleven.
	 *
	 * @param bdayEleven the bdayEleven to set
	 */
	public void setBdayEleven(boolean bdayEleven) {
		this.bdayEleven = bdayEleven;
	}

	/**
	 * Checks if is bday twelve.
	 *
	 * @return the bdayTwelve
	 */
	public boolean isBdayTwelve() {
		return bdayTwelve;
	}

	/**
	 * Sets the bday twelve.
	 *
	 * @param bdayTwelve the bdayTwelve to set
	 */
	public void setBdayTwelve(boolean bdayTwelve) {
		this.bdayTwelve = bdayTwelve;
	}

	/**
	 * Checks if is bday thirteen.
	 *
	 * @return the bdayThirteen
	 */
	public boolean isBdayThirteen() {
		return bdayThirteen;
	}

	/**
	 * Sets the bday thirteen.
	 *
	 * @param bdayThirteen the bdayThirteen to set
	 */
	public void setBdayThirteen(boolean bdayThirteen) {
		this.bdayThirteen = bdayThirteen;
	}

	/**
	 * Checks if is bday fourteen.
	 *
	 * @return the bdayFourteen
	 */
	public boolean isBdayFourteen() {
		return bdayFourteen;
	}

	/**
	 * Sets the bday fourteen.
	 *
	 * @param bdayFourteen the bdayFourteen to set
	 */
	public void setBdayFourteen(boolean bdayFourteen) {
		this.bdayFourteen = bdayFourteen;
	}

	/**
	 * Checks if is bday fifteen.
	 *
	 * @return the bdayFifteen
	 */
	public boolean isBdayFifteen() {
		return bdayFifteen;
	}

	/**
	 * Sets the bday fifteen.
	 *
	 * @param bdayFifteen the bdayFifteen to set
	 */
	public void setBdayFifteen(boolean bdayFifteen) {
		this.bdayFifteen = bdayFifteen;
	}

	/**
	 * Checks if is bday sixteen.
	 *
	 * @return the bdaySixteen
	 */
	public boolean isBdaySixteen() {
		return bdaySixteen;
	}

	/**
	 * Sets the bday sixteen.
	 *
	 * @param bdaySixteen the bdaySixteen to set
	 */
	public void setBdaySixteen(boolean bdaySixteen) {
		this.bdaySixteen = bdaySixteen;
	}

	/**
	 * Checks if is bday seventeen.
	 *
	 * @return the bdaySeventeen
	 */
	public boolean isBdaySeventeen() {
		return bdaySeventeen;
	}

	/**
	 * Sets the bday seventeen.
	 *
	 * @param bdaySeventeen the bdaySeventeen to set
	 */
	public void setBdaySeventeen(boolean bdaySeventeen) {
		this.bdaySeventeen = bdaySeventeen;
	}

	/**
	 * Checks if is bday eighteen.
	 *
	 * @return the bdayEighteen
	 */
	public boolean isBdayEighteen() {
		return bdayEighteen;
	}

	/**
	 * Sets the bday eighteen.
	 *
	 * @param bdayEighteen the bdayEighteen to set
	 */
	public void setBdayEighteen(boolean bdayEighteen) {
		this.bdayEighteen = bdayEighteen;
	}

	/**
	 * Checks if is bday nineteen.
	 *
	 * @return the bdayNineteen
	 */
	public boolean isBdayNineteen() {
		return bdayNineteen;
	}

	/**
	 * Sets the bday nineteen.
	 *
	 * @param bdayNineteen the bdayNineteen to set
	 */
	public void setBdayNineteen(boolean bdayNineteen) {
		this.bdayNineteen = bdayNineteen;
	}

	/**
	 * Checks if is bday twenty.
	 *
	 * @return the bdayTwenty
	 */
	public boolean isBdayTwenty() {
		return bdayTwenty;
	}

	/**
	 * Sets the bday twenty.
	 *
	 * @param bdayTwenty the bdayTwenty to set
	 */
	public void setBdayTwenty(boolean bdayTwenty) {
		this.bdayTwenty = bdayTwenty;
	}

	/**
	 * Checks if is bday twenty one.
	 *
	 * @return the bdayTwentyOne
	 */
	public boolean isBdayTwentyOne() {
		return bdayTwentyOne;
	}

	/**
	 * Sets the bday twenty one.
	 *
	 * @param bdayTwentyOne the bdayTwentyOne to set
	 */
	public void setBdayTwentyOne(boolean bdayTwentyOne) {
		this.bdayTwentyOne = bdayTwentyOne;
	}

	/**
	 * Checks if is bday twenty two.
	 *
	 * @return the bdayTwentyTwo
	 */
	public boolean isBdayTwentyTwo() {
		return bdayTwentyTwo;
	}

	/**
	 * Sets the bday twenty two.
	 *
	 * @param bdayTwentyTwo the bdayTwentyTwo to set
	 */
	public void setBdayTwentyTwo(boolean bdayTwentyTwo) {
		this.bdayTwentyTwo = bdayTwentyTwo;
	}

	/**
	 * Checks if is bday twenty three.
	 *
	 * @return the bdayTwentyThree
	 */
	public boolean isBdayTwentyThree() {
		return bdayTwentyThree;
	}

	/**
	 * Sets the bday twenty three.
	 *
	 * @param bdayTwentyThree the bdayTwentyThree to set
	 */
	public void setBdayTwentyThree(boolean bdayTwentyThree) {
		this.bdayTwentyThree = bdayTwentyThree;
	}

	/**
	 * Checks if is bday twenty four.
	 *
	 * @return the bdayTwentyFour
	 */
	public boolean isBdayTwentyFour() {
		return bdayTwentyFour;
	}

	/**
	 * Sets the bday twenty four.
	 *
	 * @param bdayTwentyFour the bdayTwentyFour to set
	 */
	public void setBdayTwentyFour(boolean bdayTwentyFour) {
		this.bdayTwentyFour = bdayTwentyFour;
	}

	/**
	 * Checks if is bday twenty five.
	 *
	 * @return the bdayTwentyFive
	 */
	public boolean isBdayTwentyFive() {
		return bdayTwentyFive;
	}

	/**
	 * Sets the bday twenty five.
	 *
	 * @param bdayTwentyFive the bdayTwentyFive to set
	 */
	public void setBdayTwentyFive(boolean bdayTwentyFive) {
		this.bdayTwentyFive = bdayTwentyFive;
	}

	/**
	 * Checks if is bday twenty six.
	 *
	 * @return the bdayTwentySix
	 */
	public boolean isBdayTwentySix() {
		return bdayTwentySix;
	}

	/**
	 * Sets the bday twenty six.
	 *
	 * @param bdayTwentySix the bdayTwentySix to set
	 */
	public void setBdayTwentySix(boolean bdayTwentySix) {
		this.bdayTwentySix = bdayTwentySix;
	}

	/**
	 * Checks if is bday twenty seven.
	 *
	 * @return the bdayTwentySeven
	 */
	public boolean isBdayTwentySeven() {
		return bdayTwentySeven;
	}

	/**
	 * Sets the bday twenty seven.
	 *
	 * @param bdayTwentySeven the bdayTwentySeven to set
	 */
	public void setBdayTwentySeven(boolean bdayTwentySeven) {
		this.bdayTwentySeven = bdayTwentySeven;
	}

	/**
	 * Checks if is bday twenty eight.
	 *
	 * @return the bdayTwentyEight
	 */
	public boolean isBdayTwentyEight() {
		return bdayTwentyEight;
	}

	/**
	 * Sets the bday twenty eight.
	 *
	 * @param bdayTwentyEight the bdayTwentyEight to set
	 */
	public void setBdayTwentyEight(boolean bdayTwentyEight) {
		this.bdayTwentyEight = bdayTwentyEight;
	}

	/**
	 * Checks if is bday twenty nine.
	 *
	 * @return the bdayTwentyNine
	 */
	public boolean isBdayTwentyNine() {
		return bdayTwentyNine;
	}

	/**
	 * Sets the bday twenty nine.
	 *
	 * @param bdayTwentyNine the bdayTwentyNine to set
	 */
	public void setBdayTwentyNine(boolean bdayTwentyNine) {
		this.bdayTwentyNine = bdayTwentyNine;
	}

	/**
	 * Checks if is bday thirty.
	 *
	 * @return the bdayThirty
	 */
	public boolean isBdayThirty() {
		return bdayThirty;
	}

	/**
	 * Sets the bday thirty.
	 *
	 * @param bdayThirty the bdayThirty to set
	 */
	public void setBdayThirty(boolean bdayThirty) {
		this.bdayThirty = bdayThirty;
	}

	/**
	 * Checks if is bday thirty one.
	 *
	 * @return the bdayThirtyOne
	 */
	public boolean isBdayThirtyOne() {
		return bdayThirtyOne;
	}

	/**
	 * Sets the bday thirty one.
	 *
	 * @param bdayThirtyOne the bdayThirtyOne to set
	 */
	public void setBdayThirtyOne(boolean bdayThirtyOne) {
		this.bdayThirtyOne = bdayThirtyOne;
	}

	/**
	 * Checks if is current.
	 *
	 * @return the current
	 */
	public boolean isCurrent() {
		return current;
	}

	/**
	 * Sets the current.
	 *
	 * @param current the current to set
	 */
	public void setCurrent(boolean current) {
		this.current = current;
	}
	

}


