package com.pradera.generalparameters.holiday.batch;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.generalparameters.holiday.service.HolidaySettlementService;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.type.OperationPartType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolidayProcessBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@BatchProcess(name="HolidayProcessBatch")
@RequestScoped
public class HolidayProcessBatch implements JobExecution{

	/** The holiday settlement service. */
	@EJB
	HolidaySettlementService holidaySettlementService;
	
	/** The holiday query service bean. */
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;
	
	/** The send notified. */
	Boolean sendNotified = Boolean.FALSE;
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {

		try {
			
			Integer indIsRegister = null;
			Date holidayDate = null;
			Integer holidayCountry = null;
			List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
			for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
				if ((Validations.validateIsNotNull(objDetail.getParameterName()))){
					if (StringUtils.equalsIgnoreCase(ViewOperationsType.REGISTER.getCode().toString(), objDetail.getParameterName())) {
						indIsRegister = BooleanType.YES.getCode();
					}else if (StringUtils.equalsIgnoreCase(ViewOperationsType.DELETE.toString(), objDetail.getParameterName())) {
						indIsRegister = BooleanType.NO.getCode();
					}else if (StringUtils.equalsIgnoreCase(HolidayProcessContants.DATE_PARAM, objDetail.getParameterName())) {
						DateFormat formatter = new SimpleDateFormat(HolidayProcessContants.DATE_FORMAT);
						holidayDate = formatter.parse(objDetail.getParameterValue());
					}else if (StringUtils.equalsIgnoreCase(HolidayProcessContants.COUNTRY_PARAM, objDetail.getParameterName())) {
						holidayCountry = new Integer(objDetail.getParameterValue());
					}
				}
			}

			if(indIsRegister.equals(BooleanType.YES.getCode())){
				if(holidayCountry.equals(countryResidence)){
					assigmentOperationsHoliday(holidayDate);
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Assigment operations holiday.
	 *
	 * @param holidayDate the holiday date
	 * @throws ServiceException the service exception
	 */
	public void assigmentOperationsHoliday(Date holidayDate) throws ServiceException{
		List<Object[]> cashOperationList = holidaySettlementService.getOperationsBySettlementDate(holidayDate);
		Date nextWorkDay = holidayQueryServiceBean.getCalculateDate(holidayDate, 1, 1, null);
		
		for(Object[] operationCashPar: cashOperationList){
			Long idSettlementOperation = (Long)operationCashPar[0];
			holidaySettlementService.assigmentOperationExtension(idSettlementOperation, nextWorkDay);
			sendNotified = Boolean.TRUE;
		}
		
	}
	
	/**
	 * Remove operation holiday.
	 *
	 * @param holidayDate the holiday date
	 * @throws ServiceException the service exception
	 */
	public void removeOperationHoliday(Date holidayDate) throws ServiceException{
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return sendNotified;
	}
}