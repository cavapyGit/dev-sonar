package com.pradera.generalparameters.holiday.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.generalparameters.holiday.facade.HolidayServiceFacade;
import com.pradera.generalparameters.holiday.view.filter.HolidayFilter;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class HolidayMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/09/2013
 */
@LoggerCreateBean
@DepositaryWebBean
public class HolidayMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long 			serialVersionUID = 1L;

	/** The country list. */
	private List<GeographicLocation> 	countryList;
	
	/** The list year. */
	private List<String> 				listYear;
	
	/** The holiday filter. */
	private HolidayFilter 				holidayFilter;
	
	/** The event model. */
	private ScheduleModel 				eventModel;
	
	/** The holiday list. */
	private List<Holiday>				holidayList;
	
	/** The holiday. */
	private Holiday						holiday;
	
	/** The schedule event. */
	private DefaultScheduleEvent 		scheduleEvent;
	
	/** The default schedule events list. */
	private List<DefaultScheduleEvent>	defaultScheduleEventsList;
	
	/** The searched. */
	private boolean						searched;
	
	/** The event. */
	private ScheduleEvent				event = new DefaultScheduleEvent();
	
	/** The holiday type list. */
	private List<ParameterTable>		holidayTypeList;
	
	/** The title message. */
	private String						titleMessage;
	
	/** The action type. */
	private Integer						actionType;
	
	/** Holds the selected year information. */
	private Date selectedYear;
	
	/** Holds the Selected Country locale. */
	private Locale currentLocale;
	
	/** The User info. */
	@Inject
	private UserInfo userInfo;
	
	/** The holiday facade. */
	@EJB
	private HolidayServiceFacade holidayServiceFacade;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade;
	
	/**
	 * Instantiates a new holiday mgmt bean.
	 */
	public HolidayMgmtBean(){
		eventModel = new DefaultScheduleModel();
	}
	
	/**
	 * init Method.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		currentLocale = FacesContext.getCurrentInstance().getApplication().getDefaultLocale();		
		countryList 				= new ArrayList<GeographicLocation>();
		listYear 					= new ArrayList<String>();
		holidayFilter				= new HolidayFilter();
		defaultScheduleEventsList	= new ArrayList<DefaultScheduleEvent>();
		holiday						= new Holiday();
		holiday.setGeographicLocation(new GeographicLocation());
		holidayTypeList				= new ArrayList<ParameterTable>();
		searched					= false;
		loadFilters();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		//por defecto Paraguay
		holidayFilter.setGeographicLocationPk(18156);
	}

	/**
	 *  Initializing Filters .
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadFilters() throws ServiceException {
		ParameterTableTO paramTableTO = new ParameterTableTO();
		paramTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_HOLIDAY_TYPE.getCode());
		
		loadCountryList();
		loadYears();
		holidayTypeList = generalParametersFacade.getListParameterTableServiceBean(paramTableTO);
	}

	/**
	 * Load list of country.
	 *
	 * @return void
	 */
	private void loadCountryList() {
		try {
			countryList = holidayServiceFacade.getCountryListServiceFacade();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Load list of 15 Years previous to current year.
	 *
	 * @return void
	 */
	private void loadYears() {
		for (int i = 0; i <= 15; i++) {
			int year = Calendar.getInstance().get(Calendar.YEAR) - (i-2);
			listYear.add(Integer.toString(year));
		}
	}
	
	
	/**
	 * To search for holidays.
	 */
	public void searchHolidayByFilter(){
		hideDialogs();
		try{
			//Setting selected country locale
			currentLocale = new Locale(getLocaleSelectGeographic());
			holidayList = holidayServiceFacade.searchHolidayByFilterServiceFacade(holidayFilter);
			searched = true;
			deleteAllEvents();
			if(!holidayList.isEmpty()){
				//FIRST ELETE ALL EVENTS				
				for (Holiday objHoliday : holidayList) {
					for(int i=0;i<holidayTypeList.size();i++){
						if(objHoliday.getHolidayType().equals(holidayTypeList.get(i).getParameterTablePk())){
							objHoliday.setDescription(holidayTypeList.get(i).getParameterName());
						}
					}
					scheduleEvent = new DefaultScheduleEvent(objHoliday.getDescription(), objHoliday.getHolidayDate() , objHoliday.getHolidayDate());
					scheduleEvent.setAllDay(true);
					scheduleEvent.setData(objHoliday);
					defaultScheduleEventsList.add(scheduleEvent);
					eventModel.addEvent(scheduleEvent);
				}
			}else{
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * To delete all the events from screen.
	 */
	public void deleteAllEvents(){
		if(Validations.validateIsNotNullAndNotEmpty(defaultScheduleEventsList)){
			for(DefaultScheduleEvent defaultSchEvent : defaultScheduleEventsList){
				eventModel.deleteEvent(defaultSchEvent);
			}
			defaultScheduleEventsList.clear();
		}
	}
	
	/*
	 * Update component
	 *This method is used to do update some control from managebean, for the puntual cases
	 * */
	public static void updateComponent(String componentId){
		RequestContext.getCurrentInstance().update(componentId);		
	}
	
	/**
     * Execute javascript function.
     *
     * @param function the function
     */
    public static void executeJavascriptFunction(String function){
   	 RequestContext requestContext = RequestContext.getCurrentInstance();  
		 requestContext.execute(function.toString());
    }
	
	/**
	 * Show simple validation dialog.
	 */
	public static void showSimpleValidationDialog(){
		updateComponent("cnfMsgCustomValidationSec");
		executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show();");
	}
	
	/**
	 * To Save the holiday.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
	public void saveHoliday(ActionEvent actionEvent) {
		//hideDialogs();
		try{
			Object[] argObj  = new Object[1];		
        if(event.getId() == null){ 
        	settingValuesForHoliday();
        	scheduleEvent = new DefaultScheduleEvent(holiday.getDescription(), holiday.getHolidayDate(), holiday.getHolidayDate());
        	scheduleEvent.setAllDay(true);
			scheduleEvent.setData(holiday);
			defaultScheduleEventsList.add(scheduleEvent);
        	eventModel.addEvent(scheduleEvent);
        	
        	//verificando si ya existe esta fecha registrada
			holidayList = holidayServiceFacade.searchHolidayByFilterServiceFacade(holidayFilter);
			if (holidayList != null && !holidayList.isEmpty())
				for (Holiday holidayDate : holidayList)
					if (holidayDate.getHolidayDate().equals(holiday.getHolidayDate())) {
						JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
						JSFUtilities.putViewMap("bodyMessageView", PropertiesUtilities.getMessage("msg.validation.no.duplicate.dateholiday.body"));
						JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getMessage("msg.validation.no.duplicate.dateholiday.header"));
						showSimpleValidationDialog();
						return;
					}
        	
        	holidayServiceFacade.registerHolidayFacade(holiday); // SAVE A NEW HOLIDAY
        	
        	argObj[0] = holidayFilter.getGeographicLocationPk() +GeneralConstants.HYPHEN+holidayFilter.getYear();
        	//send notification update
			BusinessProcess businessProc = new BusinessProcess();
			businessProc.setIdBusinessProcessPk(BusinessProcessType.HOLIDAY_REGISTRATION.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null,argObj);
        	hideDialogs();
        	searchHolidayByFilter();
        	JSFUtilities.showEndTransactionSamePageDialog();
        	Object[] param = {CommonsUtilities.convertDatetoString(holiday.getHolidayDate())};
        	JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null, PropertiesConstants.HOLIDAY_REGISTER_SUCCESS, param);
        }else{
        	settingValuesForHoliday();
        	scheduleEvent = new DefaultScheduleEvent(holiday.getDescription(), holiday.getHolidayDate(), holiday.getHolidayDate());
        	scheduleEvent.setAllDay(true);
			scheduleEvent.setData(holiday);	
			scheduleEvent.setTitle(holiday.getDescription());
            eventModel.updateEvent(event);
            holiday = (Holiday)event.getData();
            holidayServiceFacade.updateHolidayFacade(holiday);   // UPDATE EXISTING HOLIDAY
            
           
        	//send notification update
			BusinessProcess businessProc = new BusinessProcess();
			businessProc.setIdBusinessProcessPk(BusinessProcessType.HOLIDAY_DELETE.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null,argObj);
            searchHolidayByFilter(); // QUERY AGAIN
        }
        JSFUtilities.executeJavascriptFunction("PF('cnfwSave').hide()");
        JSFUtilities.executeJavascriptFunction("PF('eventDialog').hide()");
        
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
        event = new DefaultScheduleEvent();  
    }  
	
	/**
	 * To delete holiday.
	 *
	 * @param actionEvent the action event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void deleteEvent(ActionEvent actionEvent) throws ServiceException{
		  if(event.getId() != null){ 
			  Object[] argObj = new Object[2];
	        	settingValuesForHoliday();
	        	eventModel.deleteEvent(event);
	        	holiday.setState(HolidayStateType.DELETED.getCode());
	        	holidayServiceFacade.updateHolidayFacade(holiday);
	        	hideDialogs();
	        	//send notification update
				BusinessProcess businessProc = new BusinessProcess();
				businessProc.setIdBusinessProcessPk(BusinessProcessType.HOLIDAY_DELETE.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null,argObj);
	        	searchHolidayByFilter(); // QUERY AGAIN
	        	JSFUtilities.showEndTransactionSamePageDialog();
	        	Object[] param = {CommonsUtilities.convertDatetoString(holiday.getHolidayDate())};
	        	JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null, PropertiesConstants.HOLIDAY_DELETE_SUCCESS, param);	        	
	        }
	}
	
	/**
	 * To show confirmation dialog before.
	 */
	public void beforeDeleteEvent(){
		//JSFUtilities.showComponent("frmGeneral:cnfdelete");
		JSFUtilities.executeJavascriptFunction("PF('cnfwDelete').show()");
		Object[] obj = {CommonsUtilities.convertDatetoString(holiday.getHolidayDate())};
		JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ELIMINAR, null, PropertiesConstants.DELETE_HOLIDAY_CONFIRM, obj);
	}
	
	/**
	 * To set the values for holiday.
	 */
	public void settingValuesForHoliday(){
		try{
			for(int i=0;i<holidayTypeList.size();i++){
				if(holiday.getHolidayType().equals(holidayTypeList.get(i).getParameterTablePk())){
					holiday.setDescription(holidayTypeList.get(i).getParameterName());		// SETTING NEW HOLIDAY DESCRIPTION
				}
			}
			holiday.setHolidayDate(event.getStartDate());									// SETTING HOLIDAY START DATE
			if(actionType.equals(GeneralConstants.ONE_VALUE_INTEGER)){ 						// IF IS A NEW REGISTRY
				holiday.setRegistryUser(userInfo.getUserAccountSession().getUserName());	// SETTING A RECORDER USER
				holiday.setRegistryDate(new Date()); 										// SETTING REGISTER DATE
			}
			holiday.setState(HolidayStateType.REGISTERED.getCode()); 							// SETTING HOLIDAY STATUS					
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * On date select operation.
	 *
	 * @param selectEvent the select event
	 */
	public void onDateSelect(SelectEvent selectEvent) {
		hideDialogs();
		//capturando la fecha seleccionada
		Date selectedEventDate = (Date) selectEvent.getObject();
		if (selectedEventDate.before(CommonsUtilities.currentDate())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_HOLIDAY_REGISTER_PAST_DATE));
			JSFUtilities.showValidationDialog();
		}else if (selectedEventDate.equals(CommonsUtilities.currentDate())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage("error.adm.holiday.register.current.date"));
			JSFUtilities.showValidationDialog();
		} else {
			JSFUtilities.executeJavascriptFunction("PF('eventDialog').show()");
			//JSFUtilities.showComponent("frmGeneral:idDialogEvent");
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			titleMessage = PropertiesUtilities.getMessage("adm.feriado.registrar.titulo");
			JSFUtilities.setValidViewComponent("frmGeneral:cboBehaviorDialog");
		}
		holiday = new Holiday();
		holiday.setGeographicLocation(new GeographicLocation());
		holiday.getGeographicLocation().setIdGeographicLocationPk(holidayFilter.getGeographicLocationPk());
		event = new DefaultScheduleEvent("", selectedEventDate,
				selectedEventDate);
		List<ScheduleEvent> lstEvents = eventModel.getEvents();
		
		for (ScheduleEvent scheduleEvent : lstEvents) {
			if (selectedEventDate.equals(scheduleEvent.getStartDate()) && selectedEventDate.equals(scheduleEvent.getEndDate())) {
				holiday = (Holiday) scheduleEvent.getData();
				if(holiday.getHolidayDate().before(CommonsUtilities.currentDate())){
					this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
					actionType = GeneralConstants.TWO_VALUE_INTEGER;
					titleMessage = PropertiesUtilities.getMessage("adm.feriado.consultar.titulo");
				}else{
					this.setViewOperationType(ViewOperationsType.DELETE.getCode());
					actionType = GeneralConstants.TWO_VALUE_INTEGER;
					event = new DefaultScheduleEvent("", holiday.getHolidayDate(), holiday.getHolidayDate());
					event.setId(UUID.randomUUID().toString());
					titleMessage = PropertiesUtilities.getMessage("adm.feriado.eliminar.titulo");
				}
				JSFUtilities.executeJavascriptFunction("PF('eventDialog').show()");
				return;
			}
		}		
		actionType = GeneralConstants.ONE_VALUE_INTEGER; // SAVE
    } 
	
	/**
	 * On event select Operation.
	 *
	 * @param selectEvent the select event
	 */
	public void onEventSelect(SelectEvent selectEvent) {
		hideDialogs();
        event = (ScheduleEvent) selectEvent.getObject();
        holiday = (Holiday)event.getData();
        holiday.getGeographicLocation();
		JSFUtilities.executeJavascriptFunction("PF('eventDialog').show()");
        Date selectedEventDate = holiday.getHolidayDate();
        if(selectedEventDate.before(CommonsUtilities.currentDate())){
        	setViewOperationType(ViewOperationsType.CONSULT.getCode());
        	titleMessage = PropertiesUtilities.getMessage("adm.feriado.consultar.titulo");
        }else{
        	setViewOperationType(ViewOperationsType.DELETE.getCode());
        	titleMessage = PropertiesUtilities.getMessage("adm.feriado.eliminar.titulo");
        }
        actionType	 = GeneralConstants.TWO_VALUE_INTEGER; //MODIFY
	}  
	
	/**
	 * For limpiar operation.
	 */
	public void clear(){
		hideDialogs();
		searched = false;
		holidayList = new ArrayList<Holiday>();
		holidayFilter = new HolidayFilter();
		deleteAllEvents();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * To register the holiday.
	 */
	public void registerHoliday(){
		/*selectedYear = prepareDateSelectYear();		
		searched = false;
		holidayList.clear();
		deleteAllEvents();*/
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
	}
	
	/**
	 * To confirm register.
	 */
	public void beforeSaveHoliday(){
		Object[] obj = {CommonsUtilities.convertDatetoString(event.getStartDate())};
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
				, PropertiesUtilities.getMessage(PropertiesConstants.SAVE_HOLIDAY_CONFIRM, obj));

		JSFUtilities.executeJavascriptFunction("PF('cnfwSave').show()");
	}
	
	/**
	 * To hide Dialogs.
	 */
	public void hideDialogs(){
		JSFUtilities.hideGeneralDialogues();
		
		JSFUtilities.executeJavascriptFunction("PF('cnfwSave').hide()");
		//JSFUtilities.hideComponent("frmGeneral:cnfsave");
		JSFUtilities.executeJavascriptFunction("PF('cnfwDelete').hide()");
		//JSFUtilities.hideComponent("frmGeneral:cnfdelete");
		JSFUtilities.executeJavascriptFunction("PF('eventDialog').hide()");
		//JSFUtilities.hideComponent("frmGeneral:idDialogEvent");
	}
	
	/**
	 * Hide own.
	 */
	public void hideOwn(){
		JSFUtilities.executeJavascriptFunction("PF('cnfwSave').hide()");
	}
	
	/**
	 * Hide own delete.
	 */
	public void hideOwnDelete(){
		JSFUtilities.executeJavascriptFunction("PF('cnfwDelete').hide()");
	}
	
	//GETTERS AND SETTERS
	/**
	 * Gets the list year.
	 *
	 * @return the listYear
	 */
	public List<String> getListYear() {
		return listYear;
	}

	/**
	 * Sets the list year.
	 *
	 * @param listYear            the listYear to set
	 */
	public void setListYear(List<String> listYear) {
		this.listYear = listYear;
	}

	/**
	 * Gets the holiday filter.
	 *
	 * @return the holidayFilter
	 */
	public HolidayFilter getHolidayFilter() {
		return holidayFilter;
	}

	/**
	 * Sets the holiday filter.
	 *
	 * @param holidayFilter            the holidayFilter to set
	 */
	public void setHolidayFilter(HolidayFilter holidayFilter) {
		this.holidayFilter = holidayFilter;
	}

	/**
	 * Gets the country list.
	 *
	 * @return the countryList
	 */
	public List<GeographicLocation> getCountryList() {
		return countryList;
	}

	/**
	 * Sets the country list.
	 *
	 * @param countryList            the countryList to set
	 */
	public void setCountryList(List<GeographicLocation> countryList) {
		this.countryList = countryList;
	}

	/**
	 * Gets the event model.
	 *
	 * @return eventModel
	 */
	public ScheduleModel getEventModel() {
		return eventModel;
	}

	/**
	 * Sets the event model.
	 *
	 * @param eventModel the new event model
	 */
	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	/**
	 * Gets the holiday list.
	 *
	 * @return holidayList
	 */
	public List<Holiday> getHolidayList() {
		return holidayList;
	}

	/**
	 * Sets the holiday list.
	 *
	 * @param holidayList the new holiday list
	 */
	public void setHolidayList(List<Holiday> holidayList) {
		this.holidayList = holidayList;
	}

	/**
	 * Checks if is searched.
	 *
	 * @return searched
	 */
	public boolean isSearched() {
		return searched;
	}

	/**
	 * Sets the searched.
	 *
	 * @param searched the new searched
	 */
	public void setSearched(boolean searched) {
		this.searched = searched;
	}

	/**
	 * Gets the event.
	 *
	 * @return event
	 */
	public ScheduleEvent getEvent() {
		return event;
	}

	/**
	 * Sets the event.
	 *
	 * @param event the new event
	 */
	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	/**
	 * Gets the holiday.
	 *
	 * @return holiday
	 */
	public Holiday getHoliday() {
		return holiday;
	}

	/**
	 * Sets the holiday.
	 *
	 * @param holiday the new holiday
	 */
	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}

	/**
	 * Gets the holiday type list.
	 *
	 * @return holidayTypeList
	 */
	public List<ParameterTable> getHolidayTypeList() {
		return holidayTypeList;
	}

	/**
	 * Sets the holiday type list.
	 *
	 * @param holidayTypeList the new holiday type list
	 */
	public void setHolidayTypeList(List<ParameterTable> holidayTypeList) {
		this.holidayTypeList = holidayTypeList;
	}

	/**
	 * Gets the title message.
	 *
	 * @return titleMessage
	 */
	public String getTitleMessage() {
		return titleMessage;
	}

	/**
	 * Sets the title message.
	 *
	 * @param titleMessage the new title message
	 */
	public void setTitleMessage(String titleMessage) {
		this.titleMessage = titleMessage;
	}

	/**
	 * Gets the action type.
	 *
	 * @return actionType
	 */
	public Integer getActionType() {
		return actionType;
	}

	/**
	 * Sets the action type.
	 *
	 * @param actionType the new action type
	 */
	public void setActionType(Integer actionType) {
		this.actionType = actionType;
	}
	
	/**
	 * Gets the selected year.
	 *
	 * @return the selectedYear
	 */
	public Date getSelectedYear() {
		return selectedYear;
	}

	/**
	 * Sets the selected year.
	 *
	 * @param selectedYear the selectedYear to set
	 */
	public void setSelectedYear(Date selectedYear) {
		this.selectedYear = selectedYear;
	}
	
	
	
	/**
	 * Gets the current locale.
	 *
	 * @return the currentLocale
	 */
	public Locale getCurrentLocale() {
		return currentLocale;
	}

	/**
	 * Sets the current locale.
	 *
	 * @param currentLocale the currentLocale to set
	 */
	public void setCurrentLocale(Locale currentLocale) {
		this.currentLocale = currentLocale;
	}

	/**
	 * It format date based selected year.
	 *
	 * @return the date type
	 */
	/*private Date prepareDateSelectYear(){
		Date requiredDate = null;
		try{
			Calendar cal = Calendar.getInstance();	    
	    	cal.set(Calendar.YEAR, holidayFilter.getYear()); 	    	
	    	DateFormat dfDate = new SimpleDateFormat(GeneralConstants.DATE_FORMAT_PATTERN);			
			String strCurrentDate = dfDate.format(cal.getTime());
			requiredDate = dfDate.parse(strCurrentDate);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		return requiredDate;
	}*/
	
	/**
	 * Get the selected Locale details
	 * 
	 * @return the String type
	 */
	private String getLocaleSelectGeographic(){
		String selectLocale = "es";
		for(GeographicLocation loc : countryList){
			//Get the selected Locale
			if(loc.getIdGeographicLocationPk().equals(holidayFilter.getGeographicLocationPk())){
				selectLocale = loc.getCodeGeographicLocation();
				break;
			}
		}
		
		return selectLocale;
	}
	
	/**
	 * 
	 * @return default timeZone 
	 */
	public String getCurrentTimeZone() {
		return TimeZone.getDefault().getID();
	}
	
	/**
	 * 
	 * @return first day of January year selected by user
	 */
	@SuppressWarnings("deprecation")
	public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        if(today.getYear()==holidayFilter.getYear()){
        	calendar.set(holidayFilter.getYear(), today.getMonth(), 1);
        }else{
        	calendar.set(holidayFilter.getYear(), Calendar.JANUARY, 1);
        }
        return calendar.getTime();
    }
	
	/**
	 * Reset Calendar
	 * @param event
	 */
    public void yearSelectionChanged(final AjaxBehaviorEvent event){
		holidayList = new ArrayList<Holiday>();
		//searchHolidayByFilter();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
    }
}
