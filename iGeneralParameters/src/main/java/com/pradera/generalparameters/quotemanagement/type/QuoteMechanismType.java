package com.pradera.generalparameters.quotemanagement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum QuoteMechanismType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public enum QuoteMechanismType {
	
	/** The bolsa. */
	BOLSA(new Integer(1),"Bolsa"),
	
	/** The hacienda. */
	HACIENDA(new Integer(2),"Hacienda"),
	
	/** The bcr. */
	BCR(new Integer(3),"Banco Central"),
	
	/** The otc. */
	OTC(new Integer(4),"OTC");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<QuoteMechanismType> list = new ArrayList<QuoteMechanismType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, QuoteMechanismType> lookup = new HashMap<Integer, QuoteMechanismType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return list
	 */
	public static List<QuoteMechanismType> listSomeElements(QuoteMechanismType... transferSecuritiesTypeParams){
		List<QuoteMechanismType> retorno = new ArrayList<QuoteMechanismType>();
		for(QuoteMechanismType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (QuoteMechanismType s : EnumSet.allOf(QuoteMechanismType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new quote mechanism type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private QuoteMechanismType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
