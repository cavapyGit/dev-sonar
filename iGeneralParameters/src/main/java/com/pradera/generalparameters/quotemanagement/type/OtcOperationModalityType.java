package com.pradera.generalparameters.quotemanagement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum OtcOperationModalityType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public enum OtcOperationModalityType {
	
	/** The contado. */
	CONTADO(new Integer(1), "CONTADO"), 
 /** The reporto. */
 REPORTO(new Integer(2), "REPO PLAZO"), 
 /** The reporto vista. */
 REPORTO_VISTA(
			new Integer(8), "REPO A LA VISTA"), 
 /** The repo secundario. */
 REPO_SECUNDARIO(new Integer(3),
			"REPO SECUNDARIO"), 
 /** The renovation. */
 RENOVATION(new Integer(6), "RENOVACION"), 
 /** The tickets. */
 TICKETS(
			new Integer(7), "CESION DE CUPONES"), 
 /** The colocacion primaria. */
 COLOCACION_PRIMARIA(
			new Integer(4), "COLOCACION PRIMARIA"), 
 /** The canje. */
 CANJE(new Integer(5),
			"CANJE"), 
 /** The provide securities. */
 PROVIDE_SECURITIES(new Integer(9), "PRESTAMO DE VALORES");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<OtcOperationModalityType> list = new ArrayList<OtcOperationModalityType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OtcOperationModalityType> lookup = new HashMap<Integer, OtcOperationModalityType>();

	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return list
	 */
	public static List<OtcOperationModalityType> listSomeElements(
			OtcOperationModalityType... transferSecuritiesTypeParams) {
		List<OtcOperationModalityType> retorno = new ArrayList<OtcOperationModalityType>();
		for (OtcOperationModalityType TransferSecuritiesType : transferSecuritiesTypeParams) {
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}

	static {
		for (OtcOperationModalityType s : EnumSet
				.allOf(OtcOperationModalityType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Gets the code.
	 *
	 * @return code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Instantiates a new otc operation modality type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private OtcOperationModalityType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
