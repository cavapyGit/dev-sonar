package com.pradera.generalparameters.quotemanagement.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.generalparameters.quotemanagement.service.QuoteServiceBean;
import com.pradera.generalparameters.quotemanagement.view.filter.QuotationFilter;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.Quotation;
import com.pradera.model.generalparameter.QuotationFile;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class QuoteServiceFacade.
 *
 * @author PraderaTechnologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class QuoteServiceFacade {
	
	/** The Transaction synchronization Registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;

	/** The QuoteServiceBean. */
	@EJB
	private QuoteServiceBean quoteServiceBean;
	
	/**
	 * Gets the Mechanisms service facade.
	 *
	 * @return The list Mechanisms ServiceFacade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> searchMechanismsServiceFacade() throws ServiceException {
		return quoteServiceBean.searchMechanismsServiceBean();
	}
	
	
	/**
	 * Search Quotations by service Facade.
	 *
	 * @param quotationFilter the quotation filter
	 * @return the List
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> searchQuotationsByFilterServiceFacade(QuotationFilter quotationFilter) throws ServiceException {
        return quoteServiceBean.searchQuotationsByFiltersServiceBean(quotationFilter);
    }
	
	/**
	 * Register Quotation Service Facade.
	 *
	 * @param quotation the quotation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean registerQuotationServiceFacade(Quotation quotation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		return quoteServiceBean.registerQuotationServiceBean(quotation);
	}
	
	/**
	 * Modify Quotation ServiceFacade.
	 *
	 * @param quotation the quotation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean modifyQuotationServiceFacade(Quotation quotation) throws ServiceException {
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(loggerUser.getUserAction().isModify()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		return quoteServiceBean.modifyQuotationServiceBean(quotation);
	}
	
	/**
	 * Register QuotationFile Service Facade.
	 *
	 * @param quotationFile the quotation file
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerQuotationFileServiceFacade(QuotationFile quotationFile) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		quoteServiceBean.registerQuotationFileServiceBean(quotationFile);
		for(Quotation quotation:quotationFile.getQuotations()){
			if(validateExistsQuoteSameDayServiceFacade(quotation))
				continue;			
			quoteServiceBean.registerQuotationServiceBean(quotation);
		}
		return true;
	}
	
	/**
	 * To get the description of the given code.
	 *
	 * @param idIsinCodeFk the id isin code fk
	 * @return description
	 * @throws ServiceException the service exception
	 */
	public String getValorDescriptionServiceFacade(String idIsinCodeFk) throws ServiceException {
		return quoteServiceBean.getValorDescriptionServiceBean(idIsinCodeFk);
	}
	 
 	/**
 	 * Gets the security code.
 	 *
 	 * @param isincode the isincode
 	 * @return the security code
 	 * @throws ServiceException the service exception
 	 */
 	public boolean getSecurityCode(String isincode) throws ServiceException{
		 boolean registerFlag = false;
		 List<Object> data = quoteServiceBean.getRegisterCode(isincode);
		 if(data.size()>0){
			 registerFlag = true;
		 }
		 return registerFlag;
	 }
	 
	 /**
 	 * Remove the Quotations.
 	 *
 	 * @param quotation the quotation
 	 * @throws ServiceException the service exception
 	 */
	public void removeOperationQuotationFacade(Quotation quotation) throws ServiceException{	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());	 			 
		quoteServiceBean.update(quotation);		 		 
	 }

	/**
	 * Validate exists quote same day service facade.
	 *
	 * @param quotation the quotation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsQuoteSameDayServiceFacade(Quotation quotation) throws ServiceException{
		return quoteServiceBean.validateExistsQuoteSameDayServiceFacade(quotation);
	}

}
