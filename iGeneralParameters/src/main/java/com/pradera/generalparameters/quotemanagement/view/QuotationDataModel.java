package com.pradera.generalparameters.quotemanagement.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class QuotationDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class QuotationDataModel extends ListDataModel<QuotationBean> implements Serializable, SelectableDataModel<QuotationBean>{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8868706846949612447L;
	
	/** The size. */
	private int size;

	/**
	 * Instantiates a new quotation data model.
	 */
	public QuotationDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new quotation data model.
	 *
	 * @param data the data
	 */
	public QuotationDataModel(List<QuotationBean> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public QuotationBean getRowData(String rowKey) {
		List<QuotationBean> quotations=(List<QuotationBean>)getWrappedData();
        for(QuotationBean quotation : quotations) {  
            if(String.valueOf(quotation.getIdQuotationPk()).equals(rowKey))
                return quotation;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(QuotationBean quotaion) {
		return quotaion.getIdQuotationPk();
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		List<QuotationBean> quotations=(List<QuotationBean>)getWrappedData();
		if(quotations==null)
			return 0;
		return quotations.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	
}
