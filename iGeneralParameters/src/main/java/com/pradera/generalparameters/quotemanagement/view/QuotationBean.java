package com.pradera.generalparameters.quotemanagement.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class QuotationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/12/2014
 */
public class QuotationBean implements Serializable{
	
	/** Added default serial UID. */
	private static final long serialVersionUID = 1L;
	
	/** The id quotation pk. */
	private Integer idQuotationPk;
	
	/** The average price. */
	private BigDecimal averagePrice;
	
	/** The close price. */
	private BigDecimal closePrice;
	
	/** The opening price. */
	private BigDecimal openingPrice;
	
	/** The id isin code. */
	private String idIsinCode;
	
	/** The id isin code description. */
	private String idIsinCodeDescription;
	
	/** The id negotiation mechanism. */
	private Long idNegotiationMechanism;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The date registered. */
	private Date dateRegistered;
	
	/** The state quotation. */
	private Integer stateQuotation;
	
	/** The state description. */
	private String stateDescription;
	
	/** The security instrument type description. */
	private String securityInstrumentTypeDescription;
	
	/** The security. */
	private Security security = new Security();
	
	/**
	 * Gets the id quotation pk.
	 *
	 * @return the idQuotationPk
	 */
	public Integer getIdQuotationPk() {
		return idQuotationPk;
	}
	
	/**
	 * Sets the id quotation pk.
	 *
	 * @param idQuotationPk the idQuotationPk to set
	 */
	public void setIdQuotationPk(Integer idQuotationPk) {
		this.idQuotationPk = idQuotationPk;
	}
	
	/**
	 * Gets the average price.
	 *
	 * @return the averagePrice
	 */
	public BigDecimal getAveragePrice() {
		return averagePrice;
	}
	
	/**
	 * Sets the average price.
	 *
	 * @param averagePrice the averagePrice to set
	 */
	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}
	
	/**
	 * Gets the close price.
	 *
	 * @return the closePrice
	 */
	public BigDecimal getClosePrice() {
		return closePrice;
	}
	
	/**
	 * Sets the close price.
	 *
	 * @param closePrice the closePrice to set
	 */
	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}
	
	/**
	 * Gets the opening price.
	 *
	 * @return the openingPrice
	 */
	public BigDecimal getOpeningPrice() {
		return openingPrice;
	}
	
	/**
	 * Sets the opening price.
	 *
	 * @param openingPrice the openingPrice to set
	 */
	public void setOpeningPrice(BigDecimal openingPrice) {
		this.openingPrice = openingPrice;
	}
	
	/**
	 * Gets the id isin code.
	 *
	 * @return the idIsinCode
	 */
	public String getIdIsinCode() {
		return idIsinCode;
	}
	
	/**
	 * Sets the id isin code.
	 *
	 * @param idIsinCode the idIsinCode to set
	 */
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}
	
	/**
	 * Gets the id isin code description.
	 *
	 * @return the idIsinCodeDescription
	 */
	public String getIdIsinCodeDescription() {
		return idIsinCodeDescription;
	}
	
	/**
	 * Sets the id isin code description.
	 *
	 * @param idIsinCodeDescription the idIsinCodeDescription to set
	 */
	public void setIdIsinCodeDescription(String idIsinCodeDescription) {
		this.idIsinCodeDescription = idIsinCodeDescription;
	}
	
	/**
	 * Gets the id negotiation mechanism.
	 *
	 * @return the idNegotiationMechanism
	 */
	public Long getIdNegotiationMechanism() {
		return idNegotiationMechanism;
	}
	
	/**
	 * Sets the id negotiation mechanism.
	 *
	 * @param idNegotiationMechanism the idNegotiationMechanism to set
	 */
	public void setIdNegotiationMechanism(Long idNegotiationMechanism) {
		this.idNegotiationMechanism = idNegotiationMechanism;
	}
	
	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanismName
	 */
	public String getMechanismName() {
		return mechanismName;
	}
	
	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the mechanismName to set
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}
	
	/**
	 * Gets the date registered.
	 *
	 * @return the dateRegistered
	 */
	public Date getDateRegistered() {
		return dateRegistered;
	}
	
	/**
	 * Sets the date registered.
	 *
	 * @param dateRegistered the dateRegistered to set
	 */
	public void setDateRegistered(Date dateRegistered) {
		this.dateRegistered = dateRegistered;
	}
	
	/**
	 * Gets the state quotation.
	 *
	 * @return the stateQuotation
	 */
	public Integer getStateQuotation() {
		return stateQuotation;
	}
	
	/**
	 * Sets the state quotation.
	 *
	 * @param stateQuotation the stateQuotation to set
	 */
	public void setStateQuotation(Integer stateQuotation) {
		this.stateQuotation = stateQuotation;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the stateDescription
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the stateDescription to set
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the security instrument type description.
	 *
	 * @return the security instrument type description
	 */
	public String getSecurityInstrumentTypeDescription() {
		return securityInstrumentTypeDescription;
	}
	
	/**
	 * Sets the security instrument type description.
	 *
	 * @param securityInstrumentTypeDescription the new security instrument type description
	 */
	public void setSecurityInstrumentTypeDescription(
			String securityInstrumentTypeDescription) {
		this.securityInstrumentTypeDescription = securityInstrumentTypeDescription;
	}	
	
	/**
	 * Checks if is enabled quotation.
	 *
	 * @return true, if is enabled quotation
	 */
	public boolean isEnabledQuotation(){
		return GeneralConstants.ZERO_VALUE_INTEGER.equals(stateQuotation);
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
}
