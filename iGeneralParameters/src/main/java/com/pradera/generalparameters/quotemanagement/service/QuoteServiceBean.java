package com.pradera.generalparameters.quotemanagement.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.quotemanagement.view.filter.QuotationFilter;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.Quotation;
import com.pradera.model.generalparameter.QuotationFile;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.NegotiationMechanism;
// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* The Class QuoteServiceBean.
*
* @author PraderaTechnologies.
*/
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class QuoteServiceBean extends CrudDaoServiceBean {	

	/**
	 * Gets the Negotiation Mechanisms .
	 *
	 * @return the List the List<NegotiationMechanism>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> searchMechanismsServiceBean() throws ServiceException{
		StringBuffer sbSql = new StringBuffer();
		sbSql.append("Select N From NegotiationMechanism N ");
		sbSql.append("order by N.description ");
		//return (List<NegotiationMechanism>)findWithNamedQuery(NegotiationMechanism.SEARCH_NEGOTIATIONS);
		return (List<NegotiationMechanism>)em.createQuery(sbSql.toString()).getResultList();//
			
	}
	
	/**
	 *  Search Quotations .
	 *
	 * @param quotationFilter the quotation filter
	 * @return the List
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> searchQuotationsByFiltersServiceBean(QuotationFilter quotationFilter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("select q.idQuotationPk,q.openingPrice,q.closePrice,q.averagePrice, ");
		sbQuery.append(" q.negotiationMechanism.idNegotiationMechanismPk,q.negotiationMechanism.mechanismName, ");
		sbQuery.append(" s.idSecurityCodePk,s.description,q.quotationDate,q.quotationState, s.instrumentType  ");
		sbQuery.append(" from Quotation q inner join q.security s where 1 = 1 ");
		if(Validations.validateIsNotNullAndNotEmpty(quotationFilter.getSecurity().getIdSecurityCodePk())){
				sbQuery.append(" and s.idSecurityCodePk=:idSecurityPk");
		}
		//sbQuery.append(" and q.idIsinCodeFk=:idIsinCodeFk");
		if(Validations.validateIsNotNullAndNotEmpty(quotationFilter.getIdNegotiationMechanismPk())){
				sbQuery.append(" and q.negotiationMechanism.idNegotiationMechanismPk=:idNegotiationMechanism");
		}
		if(Validations.validateIsNotNullAndNotEmpty(quotationFilter.getQuotationDate())){
				sbQuery.append(" and q.quotationDate=:datequotation");
		}
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(quotationFilter.getSecurity().getIdSecurityCodePk())){
			query.setParameter("idSecurityPk",quotationFilter.getSecurity().getIdSecurityCodePk());
		}
		//query.setParameter("idIsinCodeFk",quotationFilter.getIdIsinCodeFk());
		if(Validations.validateIsNotNullAndNotEmpty(quotationFilter.getIdNegotiationMechanismPk())){
			query.setParameter("idNegotiationMechanism",quotationFilter.getIdNegotiationMechanismPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(quotationFilter.getQuotationDate())){
			query.setParameter("datequotation",quotationFilter.getQuotationDate());
		}

		return query.getResultList();
		
		
	}

	
	/**
	 * To get the description of the given code.
	 *
	 * @param idIsinCodeFk the id isin code fk
	 * @return description
	 * @throws ServiceException the service exception
	 */
	public String getValorDescriptionServiceBean(String idIsinCodeFk) throws ServiceException {
		if(Validations.validateIsNotNullAndNotEmpty(idIsinCodeFk)){
			try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("select sec.description from Security sec where sec.idIsinCodePk=:idisincode");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idisincode", idIsinCodeFk);
			 Object o = query.getSingleResult();
			 return o.toString();
			}catch(NoResultException  nre){
					
			}
		}
		return null;
	}
	
	/**
	 * Register quotation service bean.
	 *
	 * @param quotation the quotation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerQuotationServiceBean(Quotation quotation) throws ServiceException {
		create(quotation);
		return true;
	}
	
	/**
	 * Modify quotation service bean.
	 *
	 * @param quotation the quotation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean modifyQuotationServiceBean(Quotation quotation) throws ServiceException {
		update(quotation);
		return true;
	}
	
	/**
	 * Register quotation file service bean.
	 *
	 * @param quotationFile the quotation file
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerQuotationFileServiceBean(QuotationFile quotationFile) throws ServiceException {
		create(quotationFile);
		return true;
	}
	
	/**
	 * Gets the register code.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the register code
	 */
	public List<Object> getRegisterCode(String idSecurityCodePk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select 1 from Security sec where sec.idSecurityCodePk=:idSecurityPk and sec.stateSecurity =:state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityPk", idSecurityCodePk);
		query.setParameter("state", SecurityStateType.REGISTERED.getCode());
		List<Object> data = query.getResultList();
		 return data;
	}

	/**
	 * Validate exists quote same day service facade.
	 *
	 * @param quotation the quotation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsQuoteSameDayServiceFacade(Quotation quotation) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select T From Quotation T ");
			sbQuery.append(" Where T.security.idSecurityCodePk = :idSecurityCodePrm ");
			sbQuery.append(" And TRUNC(T.quotationDate) = :quotationDatePrm ");
			sbQuery.append(" And T.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPrm ");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePrm", quotation.getSecurity().getIdSecurityCodePk());
			query.setParameter("quotationDatePrm", quotation.getQuotationDate());
			query.setParameter("idNegotiationMechanismPrm", quotation.getNegotiationMechanism().getIdNegotiationMechanismPk());
			query.getSingleResult();
			return true;
		} catch (NoResultException e) {
			return false;
		} catch (NonUniqueResultException e) {
			return true;
		}
	}
}

