package com.pradera.generalparameters.quotemanagement.view;

import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.generalparameters.quotemanagement.facade.QuoteServiceFacade;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.Quotation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.NegotiationMechanismType;

// TODO: Auto-generated Javadoc
/**
 * To Validate the uploaded QuotationFile.
 *
 * @author PraderaSoftWare
 */
public class ValidateQuoteFile implements Serializable{
	
	/** Added Default Version UID. */
	private static final long serialVersionUID = 1L;
	
	/** Quotation facade bean. */
	@EJB
	private QuoteServiceFacade quoteServiceFacade;
	
	/**
	 * To Validate the uploaded file name.
	 *
	 * @param fileName the file name
	 * @param idMechanism the id mechanism
	 * @return true if success
	 */
	public static boolean validateFileName(String fileName,Long idMechanism){
		boolean result = false;
		
		boolean validLenghtName = false;
		if(NegotiationMechanismType.SIRTEX.getCode().equals(idMechanism)) {
			validLenghtName = fileName.length() == 18;
		} else {
			validLenghtName = fileName.length() == 15;
		}
		
		//To Check the length of the file name
		if(validLenghtName){
			//To Check Mechanism available or not
			String MechanismName = NegotiationMechanismType.SIRTEX.getCode().equals(idMechanism) ? fileName.substring(0, 6) : fileName.substring(0, 3);
			
			List<NegotiationMechanismType> lstMechanism = NegotiationMechanismType.list;
			for(NegotiationMechanismType mechanismType : lstMechanism){
				//To Validate QuoteFileName with selected Mechanism
				if(MechanismName.equals(mechanismType.getFormalDesc()) && idMechanism.equals(mechanismType.getCode())){
					
					try{
						//To Check the DateFormat 
						String date = NegotiationMechanismType.SIRTEX.getCode().equals(idMechanism) ? fileName.substring(6,14) : fileName.substring(3,11);
						SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						sdf.setLenient(false);
						sdf.parse(date);
						//To Check "."	
						boolean validDotPosition = false;
						if(NegotiationMechanismType.SIRTEX.getCode().equals(idMechanism)) {
							validDotPosition = fileName.indexOf(".")==14;
						} else {
							validDotPosition = fileName.indexOf(".")==11;
						}
						if(validDotPosition){
														
							//To Check Extention Type
							String extention = NegotiationMechanismType.SIRTEX.getCode().equals(idMechanism) ? fileName.substring(15) : fileName.substring(12);
							if(extention.equals("xls")){
								result = true;
							}
						}
						
					}catch(Exception e){
						result = false;
					}
				}
			}
		}
		
		return result;
		
	}
	
	/**
	 * To Validate QuotationFileStructure and To get the list of Quotations.
	 *
	 * @param is the is
	 * @param fileName the file name
	 * @param idMechanism the id mechanism
	 * @return List
	 */ 
	public  List<Quotation> getListQuotations(InputStream is,String fileName, Long idMechanism){	
		boolean isRegisteredSecurity = true;
		String mechanism = NegotiationMechanismType.SIRTEX.getCode().equals(idMechanism) ? fileName.substring(0, 6) : fileName.substring(0, 3);
		List<Quotation> lstQuotes = new ArrayList<Quotation>();
		try{
			Workbook workBook = Workbook.getWorkbook(is);
			Sheet sheet = workBook.getSheet(0);
			for (int i = 1; i < sheet.getRows(); i++) {
				Quotation quotation = new Quotation();
				NegotiationMechanism negotiationMechanism = new NegotiationMechanism();
				
				for (int j = 0; j < sheet.getColumns(); j++) {
					
					Cell cell = sheet.getCell(j, i);
					//To check date fiels id valid or not
					if (sheet.getCell(j, 0).getContents().equals("FECHA")) {
						
						SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						sdf.setLenient(false);
						quotation.setQuotationDate(sdf.parse(cell.getContents()));
					}
					//To check the given mechanism is valid or not
					if (sheet.getCell(j, 0).getContents().equals("MECANISMO")) {
						
						if(mechanism.equals(sheet.getCell(j, i).getContents())){
							if(NegotiationMechanismType.BOLSA.getFormalDesc().toString().equals(mechanism)){
								negotiationMechanism.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
							}
							if(NegotiationMechanismType.HAC.getFormalDesc().toString().equals(mechanism)){
								negotiationMechanism.setIdNegotiationMechanismPk(NegotiationMechanismType.HAC.getCode());
								}
							if(NegotiationMechanismType.BC.getFormalDesc().toString().equals(mechanism)){
								negotiationMechanism.setIdNegotiationMechanismPk(NegotiationMechanismType.BC.getCode());
								}
							if(NegotiationMechanismType.OTC.getFormalDesc().toString().equals(mechanism)){
								negotiationMechanism.setIdNegotiationMechanismPk(NegotiationMechanismType.OTC.getCode());
							}
							if(NegotiationMechanismType.SIRTEX.getFormalDesc().toString().equals(mechanism)){
								negotiationMechanism.setIdNegotiationMechanismPk(NegotiationMechanismType.SIRTEX.getCode());
							}
						}else{
							lstQuotes = null;
						}
						
					}
					
					//To get the valor value
					if (sheet.getCell(j, 0).getContents().equals("VALOR")) {
							quotation.setSecurity(new Security(cell.getContents()));
							//quotation.setIdIsinCodeFk(cell.getContents());
						isRegisteredSecurity = quoteServiceFacade.getSecurityCode(String.valueOf(cell.getContents()));							
					}
					//To get the Open price
					if (sheet.getCell(j, 0).getContents().equals("PRECIO_APERTURA")) {
							quotation.setOpeningPrice(new BigDecimal(cell.getContents().replaceAll(",", ".")));															
					}
					//To get the average price
					if (sheet.getCell(j, 0).getContents().equals("PRECIO_PROMEDIO")) {
							quotation.setAveragePrice(new BigDecimal(cell.getContents().replaceAll(",", ".")));							
					}
					//To get the closing price
					if (sheet.getCell(j, 0).getContents().equals("PRECIO_CIERRE")) {
							quotation.setClosePrice(new BigDecimal(cell.getContents().replaceAll(",", ".")));							
					}

					
				}
				if(!isRegisteredSecurity){
					lstQuotes = null;
					break;
				}
				quotation.setNegotiationMechanism(negotiationMechanism);
				//TODO for state parameter table
				quotation.setQuotationState(GeneralConstants.ZERO_VALUE_INTEGER);
				lstQuotes.add(quotation);
				
			}
			
			//Validate duplicated rows on excel
			int countSameRegister = 0;
			if(Validations.validateIsNotNullAndNotEmpty(lstQuotes)){
				for (Quotation objQuotation : lstQuotes) {
					countSameRegister = 0;
					for (Quotation objQuotationCompare : lstQuotes) {
						if(objQuotation.getQuotationDate().equals(objQuotationCompare.getQuotationDate()) && 
								objQuotation.getNegotiationMechanism().getIdNegotiationMechanismPk().equals(objQuotationCompare.getNegotiationMechanism().getIdNegotiationMechanismPk()) &&
								objQuotation.getSecurity().getIdSecurityCodePk().equals(objQuotationCompare.getSecurity().getIdSecurityCodePk()) ){
							countSameRegister++;
						}
						
						if(quoteServiceFacade.validateExistsQuoteSameDayServiceFacade(objQuotation)) {
							JSFUtilities.showComponent("cnfEndTransactionSamePage");
							Object[] arrBodyData = {CommonsUtilities.convertDatetoString(objQuotation.getQuotationDate()), NegotiationMechanismType.get(idMechanism).getValue()};
							JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ERROR,null, 
									PropertiesConstants.QUOTATION_MASSIVE_ALREADY_REGISTERED, arrBodyData);
							lstQuotes = null;
							return lstQuotes;
						}
						
					}
					if(countSameRegister > 1) {
						JSFUtilities.showComponent("cnfEndTransactionSamePage");
						GenericBaseBean.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
								PropertiesConstants.QUOTATION_MASSIVE_DUPLICATED_REGISTERS, new Object[]{fileName});
						lstQuotes = null;
						return lstQuotes;
					}
				}
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			GenericBaseBean.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
					PropertiesConstants.QUOTATION_MASSIVE_WRONG_STRUCTURE, new Object[]{fileName});
			lstQuotes = null;
		}

		
		return lstQuotes;
	}

	
}
