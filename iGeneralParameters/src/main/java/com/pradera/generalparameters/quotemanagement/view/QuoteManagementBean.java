package com.pradera.generalparameters.quotemanagement.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.generalparameters.quotemanagement.facade.QuoteServiceFacade;
import com.pradera.generalparameters.quotemanagement.view.filter.QuotationFilter;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.Quotation;
import com.pradera.model.generalparameter.QuotationFile;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.generalparameter.type.QuotationDeleteMotiveType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * The Class QuoteManagementBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */

/**
 * @author lakshmij
 *
 */
@DepositaryWebBean
public class QuoteManagementBean extends GenericBaseBean  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;

	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/**  The Quotation Bean. */
	private Quotation quotation;

	/**  The Selected Quotation Bean. */
	private QuotationBean selectedQuote;
	
	/**  The Selected Quotation Bean. */
	private QuotationBean selectedQuoteDetail;

	/**  The QuotationDataModel. */
	private QuotationDataModel quotationDataModel;

	/**  The QuotationFilter. */
	private QuotationFilter quotationFilter;

	/**  The NegotiationMechanism. */
	private NegotiationMechanism negotiationMechanism;

	/** The QuotationFile. */
	private QuotationFile quoteFile;
	
	/**  The list of Quotations. */
	private List<Quotation> lstQuotations;
	
	/**  The uploaded fileName. */
	private String fileName;
	
	/**  The uploaded file Status. */
	private String fileStatus;
	
	/**  The Indicator for delete and update. */
	private boolean indEnableModifyAndDelete;
	
	/** The QuotationBean. */
	private QuotationBean quoteBean;
	
	/** The valor Description. */
	private String valorDescription;
	
	/** Holds the current date. */
	private Date maxDate;
	
	/** List of Mechanisms. */
	private List<NegotiationMechanism> listMechanisms;
	
	/** Quotation facade bean. */
	@EJB
	private QuoteServiceFacade quoteServiceFacade;
	
	/** Validate File Information. */
	@Inject
	private ValidateQuoteFile validateQuoteFile;

	/**  The Detail Quotation Bean. */
	private QuotationBean detailQuote;
		
	/** GeneralParameter Facade. */
	@EJB
    private GeneralParametersFacade generalParametersFacade;
	
	/** Security Helper Bean. */
	@EJB
	private SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	/** Remove Opera. */
	private List<ParameterTable> lstRemoveType;
	
	/** Holds the Selected Remove type. */
	private Integer selectedRemoveType;
	
	/** Holds the Remove Description. */
	private String removeDescription;

	/** The User Information. */
	@Inject
	private UserInfo userInfo;
	
	/**
	 * Constructor.
	 */
	public QuoteManagementBean() {

	}

	/**
	 * Init Method.
	 */
	@PostConstruct
	public void init() {
		try {
		quotationFilter = new QuotationFilter();
		quotationFilter.setQuotationDate(CommonsUtilities.currentDate());
		quotationFilter.setSecurity(new Security());
		quoteFile = new QuotationFile();
		loadMechanisms();
		
		 listHolidays();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * List parameter tables.
	 *
	 * @param masterTableFk the master table fk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<ParameterTable> listParameterTables(Integer masterTableFk) throws ServiceException{
		ParameterTableTO filter = new ParameterTableTO();
		filter.setState(ParameterTableStateType.REGISTERED.getCode());
		filter.setMasterTableFk(masterTableFk);
		
		return generalParametersFacade.getListParameterTableServiceBean(filter);
	}
	
	/**
	 * List of Holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * Search quotation listener.
	 */
	public void searchQuotaionsListener() {
		try {
			//JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			loadQuotations();
			selectedQuote = null;
			indEnableModifyAndDelete = false;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load quotations.
	 * 
	 * This method fill the quotationDataModel with the quotations list
	 * according to the search criteria
	 */
	public void loadQuotations() {
		try {
			List<Object[]> quotationList = quoteServiceFacade.searchQuotationsByFilterServiceFacade(quotationFilter);
			List<QuotationBean> result = new ArrayList<QuotationBean>();
			for(Object[] obj : quotationList){
				quoteBean=new QuotationBean();
				quoteBean.setIdQuotationPk(Integer.parseInt(obj[0].toString()));
				quoteBean.setOpeningPrice((BigDecimal)obj[1]);
				quoteBean.setClosePrice((BigDecimal)obj[2]);
				quoteBean.setAveragePrice((BigDecimal)obj[3]);
				quoteBean.setIdNegotiationMechanism((Long)obj[4]);
				quoteBean.setMechanismName(obj[5].toString());
				quoteBean.setIdIsinCodeDescription(obj[7].toString());
				quoteBean.setIdIsinCode(obj[6].toString());
				quoteBean.setDateRegistered((Date)obj[8]);
				quoteBean.setStateQuotation((Integer)obj[9]);
				quoteBean.setSecurityInstrumentTypeDescription(InstrumentType.get((Integer)obj[10]).getValue());
				if(quoteBean.getStateQuotation().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
					quoteBean.setStateDescription("ACTIVO");
				}else{
					quoteBean.setStateDescription("INACTIVO");
				}
				result.add(quoteBean);
			}
			quotationDataModel = new QuotationDataModel(result);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Before Clean Quotation.
	 */
	public void cleanQuotationListener() {
		try {
			hideDialogs();
			cleanQuotationMgmt();
			quotation.setQuotationDate(CommonsUtilities.currentDate());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Clean QuotationManagement. This method clean the datatable and entered
	 * filters
	 */
	public void cleanQuotationMgmt() {
		Date quotationDate;
		if(Validations.validateIsNotNull(quotation) && 
				Validations.validateIsNotNull(quotation.getQuotationDate())) {
			quotationDate = quotation.getQuotationDate();
		} else {
			quotationDate = CommonsUtilities.currentDate();
		}
		
		quotation = new Quotation();
		quotation.setQuotationDate(quotationDate);
		quoteFile = new QuotationFile();
		quoteFile.setProcessDate(CommonsUtilities.currentDate());
		quotationFilter = new QuotationFilter();	
		quotationFilter.setQuotationDate(CommonsUtilities.currentDate());
		quotationFilter.setSecurity(new Security());
		quotationDataModel = null;
		lstQuotations = null;
		valorDescription = null;
		negotiationMechanism = new NegotiationMechanism();
		quotation.setSecurity(new Security());
		selectedQuote = null;		
		JSFUtilities.resetComponent("formAdministrarCotizaciones");
	}

	/**
	 * Before save listener. This method is called to save or update a system
	 */
	public void beforeSaveListener() {
		try {
		//	JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			
			if(this.isViewOperationRegister()) {
				quotation.setNegotiationMechanism(negotiationMechanism);
				if(quoteServiceFacade.validateExistsQuoteSameDayServiceFacade(quotation)) {
					JSFUtilities.showComponent(":cnfEndTransactionSamePage");
					Object[] arrBodyData = {CommonsUtilities.convertDatetoString(quotation.getQuotationDate())};
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null, PropertiesConstants.MSG_FAIL_REGISTER_SAME_SECURITY_DATE_MECHANISM, arrBodyData);
					cleanQuotationRegister();
					return;
				}
			}
			
			if(this.isViewOperationModify()){
				boolean notModifySave = false;
				boolean openModify = false;
				boolean averageModify = false;
				boolean closeModify=false;
				if(quotation.getOpeningPrice()!=null && selectedQuote.getOpeningPrice().equals(quotation.getOpeningPrice())){
					openModify = true;
				}
				
				if(quotation.getAveragePrice()!=null && selectedQuote.getAveragePrice().equals(quotation.getAveragePrice())){
					averageModify=true;
				}
				if(quotation.getClosePrice()!=null && selectedQuote.getClosePrice().equals(quotation.getClosePrice())){
					closeModify=true;
				}
				if(openModify && averageModify && closeModify){
					notModifySave = true; 
				}
				
				if(notModifySave){	
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null, PropertiesConstants.MSG_NO_DATA_MODIFY,null);
					JSFUtilities.showSimpleValidationDialog();
					return ;
				}
			}

				JSFUtilities.executeJavascriptFunction("PF('cnfwAdmSystem').show()");						
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before save masive listener.
	 */
	public void beforeSaveMasiveListener() {
		try {
			hideDialogs();
			
				for (Quotation objQuotation : lstQuotations) {
					if(quoteServiceFacade.validateExistsQuoteSameDayServiceFacade(objQuotation)) {
						JSFUtilities.showComponent(":cnfEndTransactionSamePage");
						Object[] arrBodyData = {CommonsUtilities.convertDatetoString(objQuotation.getQuotationDate())};
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null, PropertiesConstants.MSG_FAIL_REGISTER_SAME_SECURITY_DATE_MECHANISM, arrBodyData);
						cleanQuotationRegister();
						return;
					}
				}
			
				JSFUtilities.executeJavascriptFunction("PF('cnfwAdmSystem').show()");		
			
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean quotation register.
	 */
	public void cleanQuotationRegister(){
		quotation = new Quotation();
		quotation.setQuotationDate(CommonsUtilities.currentDate());
		quoteFile = new QuotationFile();
		quoteFile.setProcessDate(CommonsUtilities.currentDate());		
		negotiationMechanism = new NegotiationMechanism();
		quotation.setSecurity(new Security());
		JSFUtilities.resetComponent("formAdministrarCotizaciones");
	}
	
	/**
	 * Validate security.
	 */
	public void validateSecurity(){
		hideDialogs();
		try {			
			if(this.isViewOperationRegister()) {
				if(Validations.validateIsNotNull(quotation.getSecurity().getIdSecurityCodePk())) {
					if(quotation.getSecurity()!=null) {
						if(!quoteServiceFacade.getSecurityCode(quotation.getSecurity().getIdSecurityCodePk())) {
							JSFUtilities.showComponent(":cnfEndTransactionSamePage");
							Object[] arrBodyData = {CommonsUtilities.convertDatetoString(quotation.getQuotationDate())};
							JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null, PropertiesConstants.MSG_SECURITY_NOT_REGISTERED_STATE, arrBodyData);
							cleanInvalidSecurity();
							JSFUtilities.updateComponent("formAdministrarCotizaciones");
							JSFUtilities.updateComponent("opnlGenericDialogs");
							return;
						}
					}
					
					quotation.setNegotiationMechanism(negotiationMechanism);
					if(quoteServiceFacade.validateExistsQuoteSameDayServiceFacade(quotation)) {
						JSFUtilities.showComponent(":cnfEndTransactionSamePage");
						Object[] arrBodyData = {CommonsUtilities.convertDatetoString(quotation.getQuotationDate())};
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null, PropertiesConstants.MSG_FAIL_REGISTER_SAME_SECURITY_DATE_MECHANISM, arrBodyData);
						cleanInvalidSecurity();
						JSFUtilities.updateComponent("formAdministrarCotizaciones");
						JSFUtilities.updateComponent("opnlGenericDialogs");
						return;
					}
					
				}
				
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean invalid security.
	 */
	private void cleanInvalidSecurity(){			
		quotation.setSecurity(new Security());
		quotation.setOpeningPrice(null);
		quotation.setClosePrice(null);
		quotation.setAveragePrice(null);
		quotation.setSecurity(new Security());
		JSFUtilities.resetComponent("formAdministrarCotizaciones");
	}

	/**
	 * Validation Quotation Management This method validates the quotation
	 * required values.
	 *
	 * @return 0, if successful
	 */
	public int validateQuoteManagement() {
		int error = 0;
		return error;
	}

	/**
	 * Save Quotaion listener.
	 * 
	 * This method is called after confirming, if save or update the system
	 */
	@LoggerAuditWeb
	public void saveQuotationListener() {
		try {
			// if the operation is register then save a new Quotation
			if (isViewOperationRegister()) {			
				quotation.setNegotiationMechanism(negotiationMechanism);			
				//active state Code
				quotation.setQuotationState(GeneralConstants.ZERO_VALUE_INTEGER);
				quoteServiceFacade.registerQuotationServiceFacade(quotation);
				Object[] argObj = new Object[1];
				argObj[0] = new String("");
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						PropertiesConstants.QUOTATION_REGISTER_SUCCESS, argObj);
				cleanQuotationMgmt();
				
				BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.DAILY_COTIZATIONS_REGISTRATION.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															userInfo.getUserAccountSession().getInstitutionCode(), null);
			}
			// if the operation is modify then update the Quotation
			if (isViewOperationModify()) {				
				quotation.setNegotiationMechanism(negotiationMechanism);				
				quoteServiceFacade.modifyQuotationServiceFacade(quotation);
				Object[] argObj = new Object[1];
				argObj[0] = new String("");
				showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
						PropertiesConstants.QUOTATION_MODIFY_SUCCESS, argObj);
				cleanQuotationMgmt();
			
			}
			
			JSFUtilities.showSimpleEndTransactionDialog("search");

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Hide dialogs.
	 */
	public void hideDialogs() {								
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * To Modify Selected Quotation.
	 *
	 * @return The String
	 */
	public String loadModifyQuotationAction() {
		// Set the operation of Managed Bean to "MODIFY"
		String page=null;
		this.setViewOperationType(ViewOperationsType.MODIFY.getCode());	
		
		
		//Checks the single record 
		if(Validations.validateIsNotNull(selectedQuote)){
		
		if(!selectedQuote.getStateQuotation().equals(GeneralConstants.ZERO_VALUE_INTEGER)) {
			JSFUtilities.showComponent(":cnfEndTransactionSamePage");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null, PropertiesConstants.MSG_QUOTATION_NOT_ACTIVED_STATE, null);
		} else {
			// Validate specific record can modify or not
				if((selectedQuote.getDateRegistered().compareTo(getYesterdaySystemDate()) == 0 || 
								selectedQuote.getDateRegistered().compareTo(getCurrentSystemDate()) == 0)){
					quotation = new Quotation();
					quotation.setAveragePrice(selectedQuote.getAveragePrice());
					quotation.setClosePrice(selectedQuote.getClosePrice());
					quotation.setOpeningPrice(selectedQuote.getOpeningPrice());
					quotation.setSecurity(new Security(selectedQuote.getIdIsinCode()));
					quotation.getSecurity().setDescription(selectedQuote.getIdIsinCodeDescription());
					quotation.setQuotationDate(selectedQuote.getDateRegistered());
					quotation.setNegotiationMechanism(new NegotiationMechanism());
					quotation.getNegotiationMechanism().setIdNegotiationMechanismPk(selectedQuote.getIdNegotiationMechanism());
					quotation.setIdQuotationPk(selectedQuote.getIdQuotationPk());
					quotation.setQuotationState(selectedQuote.getStateQuotation());
					negotiationMechanism = quotation.getNegotiationMechanism();
					page = "singlequotationMgmtRule";
					} else{
						// Quotation was not registered today or yesterday
						JSFUtilities.showComponent(":cnfEndTransactionSamePage");
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null, PropertiesConstants.MSG_FAIL_MODIFY_QUOTATION, null);
					}	
		}
	}	else {
		// Checks single selection or not
		JSFUtilities.showComponent(":cnfEndTransactionSamePage");
		JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
	}
		return page;
	}

	/**
	 * To Register the Quotation.
	 *
	 * @param actionEvent the action event
	 */
	public void loadRegisterQuoteListener(ActionEvent actionEvent) {
		hideDialogs();
		cleanQuotationMgmt();
		quotation.setSecurity(new Security());
		quotation.setQuotationDate(CommonsUtilities.currentDate());
		// Set the Operation of ManagedBean to "REGISTER"
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());

	}

	/**
	 * To Register the Quotations as Bulk.
	 *
	 * @param actionEvent the action event
	 */

	public void loadRegisterBulkQuoteListener(ActionEvent actionEvent) {
		hideDialogs();
		cleanQuotationMgmt();
		quoteFile.setProcessDate(CommonsUtilities.currentDate());
		fileName = null;
		// Set the Operation of ManagedBean to "REGISTER"
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
		
	}

	/**
	 * File upload manager listener. This method is called when File is uploaded
	 * 
	 * @param event
	 *            the event
	 */
	public void fileUploadManagerListener(FileUploadEvent event) {
		try {
			
			hideDialogs();									
			
			if(Validations.validateIsNotNullAndPositive(negotiationMechanism.getIdNegotiationMechanismPk())){
				
				String fDisplayName=fUploadValidateFile(event.getFile(), null, null, "xls");
				 
				 if(fDisplayName!=null){				 				 
					 fileName=fDisplayName;
				 }
				
				// To validate the file name
				boolean result = ValidateQuoteFile.validateFileName(fileName,
						negotiationMechanism.getIdNegotiationMechanismPk());
				// To send the validation Fail message
				if (!result) {
					fileName = null;
					quoteFile.setProcessedFile(null);
					quoteFile.setFileName(null);
					JSFUtilities.executeJavascriptFunction("PF('filewidgetValidationFail').show()");
				} else {
					// To show Success message
					String successMsg = PropertiesUtilities
							.getMessage(PropertiesConstants.QUOTATION_FILE_UPLOADED_SUCCESS);
					JSFUtilities.addContextMessage(
							"formAdministrarCotizaciones:fuplMassiveQuotation",
							FacesMessage.SEVERITY_INFO, successMsg, fileName + GeneralConstants.BLANK_SPACE
									+ successMsg);
					
					quoteFile.setFileName(event.getFile().getFileName());
					quoteFile.setProcessedFile(event.getFile().getContents());
				}
			}else{
				JSFUtilities.addContextMessage("formAdministrarCotizaciones:cboMechanism",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.MECHANISM_REQUIRED),PropertiesUtilities.getMessage(PropertiesConstants.MECHANISM_REQUIRED));	
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage(PropertiesConstants.MECHANISM_REQUIRED));
				JSFUtilities.showSimpleValidationDialog();
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Delete massive file.
	 */
	public void deleteMassiveFile(){
		quoteFile.setProcessedFile(null);
		quoteFile.setFileName(null);
		fileName = null;
	}

	/**
	 * To Reset the values of the Screen when we change the mechanism.
	 */
	public void onMechanismChangeListener() {
		hideDialogs();
		quotation = new Quotation();
		quotation.setQuotationDate(CommonsUtilities.currentDate());
		quoteFile.setProcessDate(CommonsUtilities.currentDate());
		quoteFile.setProcessedFile(null);
		fileName = null;
		lstQuotations = null;
	}

	/**
	 * To Validate the structure of the file and to get the list of Quotations.
	 */
	@LoggerAuditWeb
	public void validateXLS() {
		hideDialogs();
		try {
			InputStream is = new ByteArrayInputStream(
					quoteFile.getProcessedFile());
			if(Validations.validateIsNotNull(fileName)){
			lstQuotations = validateQuoteFile.getListQuotations(is, fileName, negotiationMechanism.getIdNegotiationMechanismPk());
			}
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (lstQuotations == null) {
			fileName = null;
			quoteFile.setProcessedFile(null);
			quoteFile.setFileName(null);
			JSFUtilities.executeJavascriptFunction("PF('filewidgetContentFail').show()");
		} else {
			this.setFileStatus(lstQuotations.size()
					+ PropertiesUtilities.getMessage(PropertiesConstants.QUOTATION_FILE_STATUS));
		}

	}

	/**
	 * To save the Quotation file.
	 */
	@LoggerAuditWeb
	public void saveQuotationFile() {
		try {
		//	JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			quoteFile.setNegotiationMechanism(negotiationMechanism);
			quoteFile.setTotalRecords(new BigDecimal(lstQuotations.size()));
			quoteFile.setFileName(fileName);		
			// adding quotationfile to quotations
			for (Quotation quote : lstQuotations) {
				quote.setQuotationFile(quoteFile);
			}
			quoteFile.setQuotations(lstQuotations);
			quoteServiceFacade.registerQuotationFileServiceFacade(quoteFile);
			// To Show Success Message
			JSFUtilities.showComponent("cnfEndTransaction");
			Object[] argObj = new Object[1];
			argObj[0] = new String("");
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS, null,
					PropertiesConstants.QUOTATION_FILE_REGISTER_SUCCESS, argObj);
			cleanQuotationMgmt();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate selection for modify.
	 *
	 * @param selectEvent the select event
	 */
	public void validateSelectionForModify(SelectEvent selectEvent){
		QuotationBean objQuotationBean = (QuotationBean) selectEvent.getObject();
		if(getYesterdaySystemDate().compareTo(objQuotationBean.getDateRegistered()) == 0 ||
				getCurrentSystemDate().compareTo(objQuotationBean.getDateRegistered()) == 0){
			indEnableModifyAndDelete = true;
		} else {
			indEnableModifyAndDelete = false;
		}
	}
	
	/**
	 * To Validate the valor value and to get the description.
	 *
	 * @param valorCode the valor code
	 * @return the valor description
	 */
	public void getValorDescription(String valorCode){
		try{
		//	JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			hideDialogs();
			if(Validations.validateIsNotNullAndNotEmpty(valorCode)){
				String description = quoteServiceFacade.getValorDescriptionServiceFacade(valorCode);
				if(Validations.validateIsNotNullAndNotEmpty(description)){
					valorDescription = description;
				}else{
					valorDescription = null;
					getQuotationFilter().getSecurity().setDescription(null);
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT, null,
							PropertiesConstants.QUOTATION_VALOR_VALUE_INVALID, null);
				}
			}/*else{
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				showMessageOnDialog(null, null,
						PropertiesConstants.QUOTATION_VALOR_VALUE_REQUIRED, null);
			}*/
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * To set the processDate Externally.
	 *
	 * @param d the d
	 */
	public void processDateValue(Date d){
		quoteFile.setProcessDate(d);
	}

	/**
	 * Gets the quotation.
	 *
	 * @return the quotation
	 */
	public Quotation getQuotation() {
		return quotation;
	}

	/**
	 * Sets the quotation.
	 *
	 * @param quotation            the quotation to set
	 */
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	/**
	 * Gets the negotiation mechanism.
	 *
	 * @return the negotiationMechanism
	 */
	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	/**
	 * Sets the negotiation mechanism.
	 *
	 * @param negotiationMechanism            the negotiationMechanism to set
	 */
	public void setNegotiationMechanism(
			NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}

	/**
	 * Gets the quotation filter.
	 *
	 * @return the quotationFilter
	 */
	public QuotationFilter getQuotationFilter() {
		return quotationFilter;
	}

	/**
	 * Sets the quotation filter.
	 *
	 * @param quotationFilter            the quotationFilter to set
	 */
	public void setQuotationFilter(QuotationFilter quotationFilter) {
		this.quotationFilter = quotationFilter;
	}

	/**
	 * Gets the quotation data model.
	 *
	 * @return the quotationDataModel
	 */
	public QuotationDataModel getQuotationDataModel() {
		return quotationDataModel;
	}

	/**
	 * Sets the quotation data model.
	 *
	 * @param quotationDataModel            the quotationDataModel to set
	 */
	public void setQuotationDataModel(QuotationDataModel quotationDataModel) {
		this.quotationDataModel = quotationDataModel;
	}

	/**
	 * Gets the selected quote.
	 *
	 * @return the selectedQuote
	 */
	public QuotationBean getSelectedQuote() {
		return selectedQuote;
	}

	/**
	 * Sets the selected quote.
	 *
	 * @param selectedQuote            the selectedQuote to set
	 */
	public void setSelectedQuote(QuotationBean selectedQuote) {
		this.selectedQuote = selectedQuote;
	}

	/**
	 * Gets the file name.
	 *
	 * @return The Filename
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the lst quotations.
	 *
	 * @return list of quotations
	 */
	public List<Quotation> getLstQuotations() {
		return lstQuotations;
	}

	/**
	 * Sets the lst quotations.
	 *
	 * @param lstQuotations the new lst quotations
	 */
	public void setLstQuotations(List<Quotation> lstQuotations) {
		this.lstQuotations = lstQuotations;
	}

	/**
	 * Gets the file status.
	 *
	 * @return The file status
	 */
	public String getFileStatus() {
		return fileStatus;
	}

	/**
	 * Sets the file status.
	 *
	 * @param fileStatus the new file status
	 */
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	/**
	 * Gets the quote file.
	 *
	 * @return Quotation file
	 */
	public QuotationFile getQuoteFile() {
		return quoteFile;
	}

	/**
	 * Sets the quote file.
	 *
	 * @param quoteFile the new quote file
	 */
	public void setQuoteFile(QuotationFile quoteFile) {
		this.quoteFile = quoteFile;
	}

	/**
	 * Gets the valor description.
	 *
	 * @return the valorDescription
	 */
	public String getValorDescription() {
		return valorDescription;
	}

	/**
	 * Sets the valor description.
	 *
	 * @param valorDescription the valorDescription to set
	 */
	public void setValorDescription(String valorDescription) {
		this.valorDescription = valorDescription;
	}

	/**
	 * Gets the quote bean.
	 *
	 * @return the quoteBean
	 */
	public QuotationBean getQuoteBean() {
		return quoteBean;
	}

	/**
	 * Sets the quote bean.
	 *
	 * @param quoteBean the quoteBean to set
	 */
	public void setQuoteBean(QuotationBean quoteBean) {
		this.quoteBean = quoteBean;
	}
	
	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}

	/**
	 * To Load the mechanisms.
	 */
	public void loadMechanisms() {
		try {
			maxDate = CommonsUtilities.currentDate();
			listMechanisms = quoteServiceFacade.searchMechanismsServiceFacade();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Gets the max date.
	 *
	 * @return the maxDate
	 */
	public Date getMaxDate() {
		return maxDate;
	}

	/**
	 * Sets the max date.
	 *
	 * @param maxDate the maxDate to set
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	/**
	 * Gets the list mechanisms.
	 *
	 * @return the listMechanisms
	 */
	public List<NegotiationMechanism> getListMechanisms() {
		return listMechanisms;
	}

	/**
	 * Sets the list mechanisms.
	 *
	 * @param listMechanisms the listMechanisms to set
	 */
	public void setListMechanisms(List<NegotiationMechanism> listMechanisms) {
		this.listMechanisms = listMechanisms;
	}
	
	
	
	/**
	 * Gets the detail quote.
	 *
	 * @return the detailQuote
	 */
	public QuotationBean getDetailQuote() {
		return detailQuote;
	}

	/**
	 * Sets the detail quote.
	 *
	 * @param detailQuote the detailQuote to set
	 */
	public void setDetailQuote(QuotationBean detailQuote) {
		this.detailQuote = detailQuote;
	}

	/**
	 * View the Quotation details.
	 *
	 * @return the String type
	 */
	public String deatilsQuotationAction(){	
		detailQuote = selectedQuoteDetail;		
		detailQuote.getSecurity().setIdSecurityCodePk(detailQuote.getIdIsinCode());
		detailQuote.getSecurity().setDescription(detailQuote.getIdIsinCodeDescription());
		return "quotationDetails";
	}
	
	/**
	 * Return quote management.
	 */
	public void returnQuoteManagement () {
		try {
		if(Validations.validateIsNotNull(quotationFilter)) {
			if(Validations.validateIsNotNull(quotationFilter.getSecurity().getIdSecurityCodePk())) {
				Security objSecurity = securitiesQueryServiceBean.getSecurityHelpServiceBean(quotationFilter.getSecurity().getIdSecurityCodePk());
				if(Validations.validateIsNotNull(objSecurity)){
//				securitiesHelperBean.setSecurityDescription(objSecurity.getDescription());		
				}
			} else {
//				securitiesHelperBean.setSecurityDescription(null);
			}
		}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * It fires when click on the return button .
	 *
	 * @return page info String type
	 */
	 public String backSucc(){
		 returnQuoteManagement();
			return "search";
	 	}
	 
	 
	 /**
 	 * Gets the lst remove type.
 	 *
 	 * @return the lstRemoveType
 	 */
	public List<ParameterTable> getLstRemoveType() {
		return lstRemoveType;
	}

	/**
	 * Sets the lst remove type.
	 *
	 * @param lstRemoveType the lstRemoveType to set
	 */
	public void setLstRemoveType(List<ParameterTable> lstRemoveType) {
		this.lstRemoveType = lstRemoveType;
	}
	

	/**
	 * Gets the selected remove type.
	 *
	 * @return the selectedRemoveType
	 */
	public Integer getSelectedRemoveType() {
		return selectedRemoveType;
	}

	/**
	 * Sets the selected remove type.
	 *
	 * @param selectedRemoveType the selectedRemoveType to set
	 */
	public void setSelectedRemoveType(Integer selectedRemoveType) {
		this.selectedRemoveType = selectedRemoveType;
	}
	

	/**
	 * Gets the remove description.
	 *
	 * @return the removeDescription
	 */
	public String getRemoveDescription() {
		return removeDescription;
	}

	/**
	 * Sets the remove description.
	 *
	 * @param removeDescription the removeDescription to set
	 */
	public void setRemoveDescription(String removeDescription) {
		this.removeDescription = removeDescription;
	}	
	
	

	/**
	 * Get the User Info.
	 *
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 *  Set the User Info
	 *  
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Remove the Quotations.
	 */
	@LoggerAuditWeb
	public void removeQuotationAction(){
		try {
				hideDialogs();											
				
				selectedRemoveType = null;
				removeDescription = null;
				
				JSFUtilities.resetComponent("frmRemove");
				
				boolean validState =false;
					// Remove operation
					if(Validations.validateIsNotNull(selectedQuote)){
					
						if(!selectedQuote.getStateQuotation().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
							validState = true;
						}
					
					if(!validState){			
						lstRemoveType = listParameterTables(MasterTableType.QUOTATION_DELETE_MOTIVE.getCode());
						JSFUtilities.executeJavascriptFunction("PF('idDialogRemoveWin').show();");						
					}else{
						JSFUtilities.showComponent(":cnfEndTransactionSamePage");
						JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.QUOTATION_DELETE_WRONG_STATUS, null);
					}
			
				}//Checks the records are not selected
				else {
					JSFUtilities.showComponent(":cnfEndTransactionSamePage");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_ALERT,null,PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
				}
									
				
			}catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			
		}
	
	/**
	 * Remove Operation.
	 */
	@LoggerAuditWeb
	public void removeOperation(){
		hideDialogs();		
		this.setViewOperationType(ViewOperationsType.DELETE.getCode());
		try{
		if(Validations.validateIsNotNull(selectedQuote)) {
				
				 Quotation quotation = new Quotation();
					 	quotation.setIdQuotationPk(selectedQuote.getIdQuotationPk());
					 	quotation.setAveragePrice(selectedQuote.getAveragePrice());
						quotation.setClosePrice(selectedQuote.getClosePrice());
						quotation.setOpeningPrice(selectedQuote.getOpeningPrice());
						quotation.setSecurity(new Security(selectedQuote.getIdIsinCode()));
						//quotation.setIdIsinCodeFk(quotBean.getIdIsinCode());
						quotation.setQuotationDate(selectedQuote.getDateRegistered());
						quotation.setNegotiationMechanism(new NegotiationMechanism());
						quotation.getNegotiationMechanism().setIdNegotiationMechanismPk(selectedQuote.getIdNegotiationMechanism());
						quotation.setIdQuotationPk(selectedQuote.getIdQuotationPk());
						//TODO Remove Operation state
						quotation.setQuotationState(GeneralConstants.ONE_VALUE_INTEGER);
						quotation.setRemoveMotive(selectedRemoveType);
						if(isOtherMotiveDelete()){
							quotation.setRemoveOtherMotive(removeDescription);
						}
				
			quoteServiceFacade.removeOperationQuotationFacade(quotation);
			selectedRemoveType=null;
			removeDescription=null;			
			JSFUtilities.executeJavascriptFunction("PF('idRemoveCnfSuccWin').show();");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null,PropertiesConstants.MSG_REMOVE_OPER_SUCC, null);
		}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	 
	/**
	 * Confirmation before remove the Quotation.
	 */
	public void removeOperationConfirmation(){
		hideDialogs();		
		JSFUtilities.executeJavascriptFunction("PF('idRemoveCnfWin').show();");
		JSFUtilities.showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_CONFIRM,null,PropertiesConstants.MSG_REMOVE_OPER_CONFIRMATION , null);
	}

	/**
	 * Gets the selected quote detail.
	 *
	 * @return the selectedQuoteDetail
	 */
	public QuotationBean getSelectedQuoteDetail() {
		return selectedQuoteDetail;
	}

	/**
	 * Sets the selected quote detail.
	 *
	 * @param selectedQuoteDetail the selectedQuoteDetail to set
	 */
	public void setSelectedQuoteDetail(QuotationBean selectedQuoteDetail) {
		this.selectedQuoteDetail = selectedQuoteDetail;
	}	
	
	/**
	 * Fires when change the mechanism.
	 */
	public void changeMachanismAction(){
		if(Validations.validateIsNotNull(quotation)){
			quotation.setSecurity(new Security());
			//quotation.setIdIsinCodeFk(null);
			quotation.setOpeningPrice(null);
			quotation.setAveragePrice(null);
			quotation.setClosePrice(null);					
		}
		quoteFile = new QuotationFile();		
		quoteFile.setProcessDate(CommonsUtilities.currentDate());							
		quotationFilter.setQuotationDate(CommonsUtilities.currentDate());
		quotationFilter.setIdIsinCodeFk(null);	
		quotationFilter.setSecurity(new Security());
		valorDescription = null;		
	}
	
	/**
	 * Change selected motive delete.
	 */
	public void changeSelectedMotiveDelete(){
		removeDescription = null;
	}
	
	/**
	 * Checks if is ind enable modify and delete.
	 *
	 * @return true, if is ind enable modify and delete
	 */
	public boolean isIndEnableModifyAndDelete() {
		return indEnableModifyAndDelete;
	}	

	/**
	 * Sets the ind enable modify and delete.
	 *
	 * @param indEnableModifyAndDelete the new ind enable modify and delete
	 */
	public void setIndEnableModifyAndDelete(boolean indEnableModifyAndDelete) {
		this.indEnableModifyAndDelete = indEnableModifyAndDelete;
	}
	
	/**
	 * Checks if is ind disabled security helper for mgmt.
	 *
	 * @return true, if is ind disabled security helper for mgmt
	 */
	public boolean isIndDisabledSecurityHelperForMgmt(){
		return this.isViewOperationModify() || 
				Validations.validateIsNull(quotation.getQuotationDate()) || 
				Validations.validateIsNullOrNotPositive(negotiationMechanism.getIdNegotiationMechanismPk());
	}
	
	/**
	 * Checks if is other motive delete.
	 *
	 * @return true, if is other motive delete
	 */
	public boolean isOtherMotiveDelete() {
		return QuotationDeleteMotiveType.OTHER_MOTIVE.getCode().equals(selectedRemoveType);
	}

	/**
	 * Checks if is ind disabled save delete.
	 *
	 * @return true, if is ind disabled save delete
	 */
	public boolean isIndDisabledSaveDelete() {
		if(isOtherMotiveDelete()) {
			return selectedRemoveType == null || removeDescription == null;
		} else {
			return selectedRemoveType == null;
		}
	}
	
}
