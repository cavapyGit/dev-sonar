package com.pradera.generalparameters.quotemanagement.view.filter;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 *  The Class QuotationFilter
 *  
 * @author PraderaTechnologies
 *
 */
public class QuotationFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The IdNegotiationMechanismPk. */
	private Long idNegotiationMechanismPk;
	
	/** The QuotationDate. */
	private Date quotationDate;
	
	/** The idIsinCodeFk. */
	private String idIsinCodeFk;
	
	/** The security. */
	private Security security;
	
	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the idNegotiationMechanismPk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	
	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the idNegotiationMechanismPk to set
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	
	/**
	 * Gets the quotation date.
	 *
	 * @return the quotationDate
	 */
	public Date getQuotationDate() {
		return quotationDate;
	}
	
	/**
	 * Sets the quotation date.
	 *
	 * @param quotationDate the quotationDate to set
	 */
	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}
	
	/**
	 * Gets the id isin code fk.
	 *
	 * @return the idIsinCodeFk
	 */
	public String getIdIsinCodeFk() {
		return idIsinCodeFk;
	}
	
	/**
	 * Sets the id isin code fk.
	 *
	 * @param idIsinCodeFk the idIsinCodeFk to set
	 */
	public void setIdIsinCodeFk(String idIsinCodeFk) {
		this.idIsinCodeFk = idIsinCodeFk;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

}
