package com.pradera.generalparameters.mastertables.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.mastertables.view.MasterTableSearch;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.MasterTable;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MasterTableService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MasterTableService extends CrudDaoServiceBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Construtor of MasterTableService class.
	 */
	public MasterTableService() {
		super();
	}

	/**
	 * saveDefinitionService method used to save data into MASTER_TABLE.
	 *
	 * @param masterTableDef the master table def
	 * @return the master table
	 * @throws ServiceException the service exception
	 */
	public MasterTable saveDefinitionService(MasterTable masterTableDef)
			throws ServiceException {
		MasterTable obj = create(masterTableDef);
		return obj;
	}

	/**
	 * updateDefinition method used to update data into MASTER_TABLE.
	 *
	 * @param masterTableDef the master table def
	 * @return the master table
	 * @throws ServiceException the service exception
	 */
	public MasterTable updateDefinition(MasterTable masterTableDef)
			throws ServiceException {
		return update(masterTableDef);
	}
	
	/**
	 * service_leerMasterTableList method used to select data from MASTER_TABLE.
	 *
	 * @param masterTableSearch the master table search
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MasterTable> service_leerMasterTableList(MasterTableSearch masterTableSearch) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select m from MasterTable m");
		sbQuery.append(" Where 1=1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getDescription())) {
			sbQuery.append(" And m.tableName like :desc");
		}
				
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getCode())) {
			sbQuery.append("	and m.masterTableCd=:idmaster_Table_Cd");
		}
			
		sbQuery.append(" order by m.masterTablePk ASC ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getDescription())) {
			query.setParameter("desc", GeneralConstants.PERCENTAGE_STRING + masterTableSearch.getDescription() + GeneralConstants.PERCENTAGE_STRING);
		}
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getCode())) {
			query.setParameter("idmaster_Table_Cd", masterTableSearch.getCode());
		}
			
		return (List<MasterTable>) query.getResultList();
	}
	
	/**
	 * Gets the definition tables.
	 *
	 * @param masterTableSearch the master table search
	 * @return the definition tables
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MasterTable> getDefinitionTables(MasterTableSearch masterTableSearch) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select m from MasterTable m");
		sbQuery.append(" Where 1=1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getDescription())) {
			sbQuery.append(" And m.tableName like :desc");
		}		
		
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getCode())) {
			sbQuery.append("	and m.masterTableCd = :tableCd");
		}
			
		sbQuery.append("	order by m.masterTablePk Asc ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getDescription())) {
			query.setParameter("desc", GeneralConstants.PERCENTAGE_STRING + masterTableSearch.getDescription() + GeneralConstants.PERCENTAGE_STRING);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getCode())) {
			query.setParameter("tableCd", masterTableSearch.getCode());
		}
			
		return (List<MasterTable>) query.getResultList();
	}

	
	/**
	 * saveParameterService method used to save data into PARAMETER_TABLE.
	 *
	 * @param parameterTable the parameter table
	 * @return the parameter table
	 * @throws ServiceException the service exception
	 */
	public ParameterTable saveParameterService(ParameterTable parameterTable) throws ServiceException {
		return create(parameterTable);
	}

	/**
	 * leerParameterTableDetail method used to select data from MASTER_TABLE.
	 *
	 * @param pk the pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> leerParameterTableDetail(Integer pk)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from ParameterTable p ");
		sbQuery.append(" where  p.masterTable.masterTablePk=:idfk ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idfk", pk);
		return (List<ParameterTable>) query.getResultList();
	}

	/**
	 * searchMasterTableRandom method used to select data from MASTER_TABLE.
	 *
	 * @param masterTableCd the master table cd
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MasterTable> searchMasterTableRandom(String masterTableCd)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select m from MasterTable m ");
		sbQuery.append(" where  m.masterTableCd=:idmasterTableCd ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idmasterTableCd", masterTableCd);
		return (List<MasterTable>) query.getResultList();
	}

	/**
	 * leerParameterTableDetailSearch method used to select data from
	 * PARAMETER_TABLE.
	 *
	 * @param masterTableSearch the master table search
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> leerParameterTableDetailSearch(MasterTableSearch masterTableSearch)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from ParameterTable p join fetch p.masterTable mt where p.masterTable.masterTablePk=:idfk");
		
		if (Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getDescription())) {
			sbQuery.append(" and p.parameterName like :desc");
		}
					
		if (Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getCode())) {
			sbQuery.append(" and  p.parameterTableCd=:code ");
		}
					
		
		sbQuery.append(" order by p.parameterTableCd DESC");
		Query query = em.createQuery(sbQuery.toString());
		if (Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getDescription())) {
			query.setParameter("desc", GeneralConstants.PERCENTAGE_STRING + masterTableSearch.getDescription() + GeneralConstants.PERCENTAGE_STRING);
		}
			
		query.setParameter("idfk", new Integer(masterTableSearch.getDefinition()));
		
		if (Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getCode())){
			query.setParameter("code", masterTableSearch.getCode());
		}		
		return (List<ParameterTable>) query.getResultList();
	}

	/**
	 * getDefinitionCode method used to check the definition exist in database
	 * or not.
	 *
	 * @param definitionCode the definition code
	 * @return the definition code
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MasterTable> getDefinitionCode(String definitionCode) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select m from MasterTable m ");
		sbQuery.append(" where  m.masterTableCd=:idmasterTableCd ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idmasterTableCd", definitionCode);
		return (List<MasterTable>) query.getResultList();
	}

	/**
	 * getElementCode method used to check the element code exist in database or
	 * not.
	 *
	 * @param obj the obj
	 * @param elementCode the element code
	 * @return the element code
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getElementCode(MasterTable obj,
			Integer elementCode) throws ServiceException {
		StringBuilder subQuery = new StringBuilder();
		subQuery.append("Select p from ParameterTable p ");
		// subQuery.append(" where  p.parameterTableCd=:idcode and p.masterTable.masterTablePk=:idpk ");
		// p.parameterTableCd MODIFIED BY p.parameterTablePk
		subQuery.append(" where  p.parameterTablePk=:idcode and p.masterTable.masterTablePk=:idpk ");
		Query query1 = em.createQuery(subQuery.toString());
		query1.setParameter("idcode", elementCode);
		query1.setParameter("idpk", obj.getMasterTablePk());
		List<ParameterTable> list = (List<ParameterTable>) query1
				.getResultList();
		return list;
	}

	/**
	 * Gets the master table id.
	 *
	 * @param definitionCode the definition code
	 * @return the master table id
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MasterTable> getMasterTableID(String definitionCode)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select m from MasterTable m ");
		sbQuery.append(" where  m.masterTableCd=:idmasterTableCd ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idmasterTableCd", definitionCode);
		List<MasterTable> list = (List<MasterTable>) query.getResultList();
		return list;
	}

	/**
	 * selectMasterForElementDetail method used to select data from mastertable.
	 *
	 * @param pObj the obj
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MasterTable> selectMasterForElementDetail(ParameterTable pObj)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select m from MasterTable m ");
		sbQuery.append(" where  m.masterTablePk=:idmasterTableCd ");
		Query query = em.createQuery(sbQuery.toString());
		if (pObj.getMasterTable().getMasterTablePk() != null) {
			query.setParameter("idmasterTableCd", pObj.getMasterTable()
					.getMasterTablePk());
		}
		return (List<MasterTable>) query.getResultList();
	}

	/**
	 * To validate DefinitionName.
	 *
	 * @param masterTable the master table
	 * @return integer value if DefinitionName Exists.
	 * @throws ServiceException the service exception
	 */
	public int validateDefinitionNameServiceBean(MasterTable masterTable) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select Count(m) from MasterTable m where m.tableName = :tableName");
		if(Validations.validateIsNotNullAndPositive(masterTable.getMasterTablePk())){
			sbQuery.append(" and m.masterTablePk != :masterTablePk");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("tableName", masterTable.getTableName());
		if(Validations.validateIsNotNullAndPositive(masterTable.getMasterTablePk())){
			query.setParameter("masterTablePk", masterTable.getMasterTablePk());
		}
		return Integer.parseInt( query.getSingleResult().toString() );
	}

	/**
	 * To validate the short description.
	 *
	 * @param masterTable the master table
	 * @return integer if short description exists
	 * @throws ServiceException the service exception
	 */
	public int validateDefinitionShortDescServiceBean(MasterTable masterTable) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select Count(m) from MasterTable m where m.shortDescription = :shortDesc");
		if(Validations.validateIsNotNullAndPositive(masterTable.getMasterTablePk())){
			sbQuery.append(" and m.masterTablePk != :masterTablePk");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("shortDesc", masterTable.getShortDescription());
		if(Validations.validateIsNotNullAndPositive(masterTable.getMasterTablePk())){
			query.setParameter("masterTablePk", masterTable.getMasterTablePk());
		}
		return Integer.parseInt( query.getSingleResult().toString() );
	}
	
	/**
	 * To validate the long Description.
	 *
	 * @param masterTable the master table
	 * @return integer value if long description exists
	 * @throws ServiceException the service exception
	 */
	public int validateDefinitionLongDescServiceBean(MasterTable masterTable) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select Count(m) from MasterTable m where m.longDescription = :longDesc");
		if(Validations.validateIsNotNullAndPositive(masterTable.getMasterTablePk())){
			sbQuery.append(" and m.masterTablePk != :masterTablePk");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("longDesc", masterTable.getLongDescription());
		if(Validations.validateIsNotNullAndPositive(masterTable.getMasterTablePk())){
			query.setParameter("masterTablePk", masterTable.getMasterTablePk());
		}
		return Integer.parseInt( query.getSingleResult().toString() );
	}

	/**
	 * To validate Parameter Description.
	 *
	 * @param parameterTable the parameter table
	 * @param masterTablePk the master table pk
	 * @return integer value if parameter description exists
	 * @throws ServiceException the service exception
	 */
	public int validateParameterNameServiceFacade(ParameterTable parameterTable, Integer masterTablePk) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select Count(p) from ParameterTable p where p.parameterName = :parameterName");
		sbQuery.append(" and p.masterTable.masterTablePk = :masterTablePk");
		if(Validations.validateIsNotNullAndPositive(parameterTable.getParameterTablePk())){
			sbQuery.append(" and p.parameterTablePk != :parameterTablepk");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("parameterName", parameterTable.getParameterName());
		query.setParameter("masterTablePk", masterTablePk);
		if(Validations.validateIsNotNullAndPositive(parameterTable.getParameterTablePk())){
			query.setParameter("parameterTablepk", parameterTable.getParameterTablePk());
		}
		return Integer.parseInt( query.getSingleResult().toString() );
	}

	/**
	 * Gets the code for parameter table.
	 *
	 * @param masterTablePk the master table pk
	 * @return the code for parameter table
	 * @throws ServiceException the service exception
	 */
	public String getCodeForParameterTable(Integer masterTablePk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT MAX(parameterTableCd + 1)");
			sbQuery.append("	FROM ParameterTable pm");
			sbQuery.append("	WHERE pm.masterTable.masterTablePk = :masterTablePk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("masterTablePk", masterTablePk);
			if(Validations.validateIsNotNull(query.getSingleResult()))
				return query.getSingleResult().toString();
		} catch (NoResultException e) {
			return GeneralConstants.ONE_VALUE_STRING;
		}
		return GeneralConstants.ONE_VALUE_STRING; 
	}
}
