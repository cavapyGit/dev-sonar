package com.pradera.generalparameters.mastertables.view;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.generalparameters.mastertables.facade.MasterTableFacade;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.MasterTable;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableRegType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MasterTableBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MasterTableBean extends GenericBaseBean implements Serializable{
	
	/** Added Default serial version UID. */
	private static final long serialVersionUID = 1L;
	
	/** The current date. */
	private Date currentDate;
	
	/** The master table def. */
	private MasterTable masterTableDef;
	
	/** The selected options. */
	private List<String> selectedOptions;
	
	/** The master table indicators. */
	private MasterTableIndicators masterTableIndicators;
	
	/** The parameter table. */
	private ParameterTable parameterTable;
	
	/** The parameter modify data. */
	private ParameterTable parameterModifyData;
	
	/** The master table selected. */
	private int masterTableSelected;
	
	/** The reg no. */
	private Integer regNo;
	
	/** The ele reg no. */
	private int eleRegNo;
	
	/** The name flag. */
	private boolean nameFlag;
	
	/** The master size. */
	private Integer masterSize;
	
	/** The parameter size. */
	private Integer parameterSize;
	
	/** The selected master table list. */
	private List<MasterTable> selectedMasterTableList;
	
	/** The select master list. */
	private List<MasterTable> selectMasterList;	
	
	/** The sel master table bean. */
	private List<MasterTable> selMasterTableBean;	
	
	/** The parameter table list. */
	private List<ParameterTable> parameterTableList;
	
	/** The search parameter list. */
	private List<ParameterTable> searchParameterList;
	
	/** The sel master. */
	private MasterTable selMaster;
	
	/** The sel parameter. */
	private ParameterTable selParameter;
	
	/** The master table search. */
	private MasterTableSearch masterTableSearch;
	
	/** The element master table search. */
	private MasterTableSearch elementMasterTableSearch;
	
	/** The master table register search. */
	private MasterTableSearch masterTableRegisterSearch;
	
	/** The master table facade. */
	@EJB
	private MasterTableFacade masterTableFacade;	
	
	/** The excepcion. */
//	@Inject
//	protected Event<ExceptionToCatch> excepcion;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The bl modify. */
	private boolean blModify;	

	/**
	 * This method used to initialize the objects.
	 */
	@PostConstruct
	public void init(){
		currentDate = new Date();
		masterTableDef = new MasterTable();
		selectMasterList = null;
		masterTableSearch = new MasterTableSearch();
		parameterTable = new ParameterTable();
		parameterModifyData = new ParameterTable();
		elementMasterTableSearch = new MasterTableSearch();
		searchParameterList = null;
		nameFlag = true;
		showPrivilegeButtons();
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(true);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * beforeEleDefSearch method used to set type value to Definition.
	 */
	public void beforeEleDefSearch(){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetComponent("frmsearch:description");
		elementMasterTableSearch = new MasterTableSearch();
		this.getElementMasterTableSearch().setType(MasterTableRegType.DEFINITION.getCode());
		this.setSelectedMasterTableList(null);
	}
	
	/**
	 * beforeEleDefSearch method used to set type value to Definition.
	 */
	public void beforeEleDefSearchMgmt(){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetComponent("frmConsultaMovTitulares:idItxtMasterTblDescription");
		masterTableRegisterSearch = new MasterTableSearch();
		this.getMasterTableRegisterSearch().setType(MasterTableRegType.DEFINITION.getCode());
		this.setSelectedMasterTableList(null);
	}
	
	/**
	 * masterTypeChangeListener method used to hide the dialogs when combobox item changed.
	 */
	public void masterTypeChangeListener(){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetViewRoot();
		this.setSelectMasterList(null);
		this.setSearchParameterList(null);
		this.getMasterTableSearch().setDefinition(null);
		this.getMasterTableSearch().setDefinitionName(null);
		masterTableSearch.setCode(null);
		masterTableSearch.setDescription(null);
		this.nameFlag = false;
		if(MasterTableRegType.DEFINITION.getCode().equals(masterTableSearch.getType()))
			this.nameFlag = true;
	}
	
	/**
	 * masterTypeChangeModifyListener method used to hide the dialogs when combobox item changed.
	 */
	public void masterTypeChangeModifyListener(){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetViewRoot();
		masterTableIndicators = new MasterTableIndicators();
		parameterTableList = null;
		selectedOptions = null;
		if(MasterTableRegType.DEFINITION.getCode().equals(masterTableDef.getKeyType())){
			masterTableDef = new MasterTable();
			masterTableDef.setKeyType(MasterTableRegType.DEFINITION.getCode());
		}else{			
			masterTableDef = new MasterTable();
			masterTableDef.setKeyType(MasterTableRegType.ELEMENT.getCode());
		}		
	}
	
	/**
	 * Save definition.
	 *
	 * @throws ServiceException the service exception
	 */
	private void saveDefinition() throws ServiceException{		
		MasterTable obj = masterTableFacade.saveDefinition(masterTableDef);
		Object[] argObj = new Object[2];
		argObj[0] = obj.getTableName();
		argObj[1] = obj.getMasterTableCd();
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.MASTER_DEFINITION_SAVE_SUCCESS, argObj));	
	}
	
	/**
	 * Save element.
	 *
	 * @throws ServiceException the service exception
	 */
	private void saveElement() throws ServiceException{
		parameterTable.setMasterTable(this.getMasterTableDef());
		ParameterTable paramTab = masterTableFacade.saveParameterFecade(parameterTable);
		Object[] argObj = new Object[2];
		argObj[0] = paramTab.getParameterName();
		argObj[1] = paramTab.getParameterTableCd();
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.MASTER_ELEMENT_SAVE_SUCCESS, argObj));		
	}
	
	/**
	 * Validate modify.
	 *
	 * @return the string
	 */
	public String validateModify(){
		JSFUtilities.hideGeneralDialogues();
		if(MasterTableRegType.DEFINITION.getCode().equals(masterTableSearch.getType())){
			if(Validations.validateIsNullOrEmpty(selMaster)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage("alt.master.table.record.not.selected"));				
				JSFUtilities.showSimpleValidationDialog();
			}else{
				fillDefinition(selMaster);
				blModify = true;
				return "viewDefiOrParam";
			}
		}else if(MasterTableRegType.ELEMENT.getCode().equals(masterTableSearch.getType())){
			if(Validations.validateIsNullOrEmpty(selParameter)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("alt.parameter.table.record.not.selected"));
				JSFUtilities.showValidationDialog();
			}else{
				fillElement(selParameter);
				blModify = true;
				return "viewDefiOrParam";
			}
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Save defi or ele.
	 */
	@LoggerAuditWeb
	public void saveDefiOrEle(){
		JSFUtilities.hideGeneralDialogues();
		try{
			if(MasterTableRegType.DEFINITION.getCode().equals(masterTableDef.getKeyType())) {
				saveDefinition();
			} else {
				saveElement();
			}
									
			cleanSearchMmt();
		}catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}

	/**
	 * Before save definition listener.
	 */
	public void beforeSaveDefinitionListener(){
		JSFUtilities.hideGeneralDialogues();
		Object[] parametros = {masterTableDef.getTableName().toUpperCase()};
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
				, PropertiesUtilities.getMessage(PropertiesConstants.MASTER_DEFINITION_CONFIRM_SAVE, parametros));			
	}
	
	/**
	 * Before save element listener.
	 */
	public void beforeSaveElementListener(){
		JSFUtilities.hideGeneralDialogues();
		Object[] argObj=new Object[1];
		argObj[0] = parameterTable.getParameterName().toUpperCase();
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage(PropertiesConstants.MASTER_ELEMENT_CONFIRM_SAVE, argObj));
	}
	
	/**
	 * Before modify.
	 */
	public void beforeModify(){
		JSFUtilities.hideGeneralDialogues();
		Object[] argObj = new Object[1];
		String message = null;
		if(MasterTableRegType.DEFINITION.getCode().equals(masterTableDef.getKeyType())){
			message = "alt.master.table.askModify";
			argObj[0] = masterTableDef.getMasterTablePk().toString();
		}else if(MasterTableRegType.ELEMENT.getCode().equals(masterTableDef.getKeyType())){
			message = "alt.parameter.table.askModify";
			argObj[0] = parameterModifyData.getParameterTablePk().toString();
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
						, PropertiesUtilities.getMessage(message, argObj));
	}
	
	/**
	 * Do modify.
	 */
	@LoggerAuditWeb
	public void doModify(){
		JSFUtilities.hideGeneralDialogues();
		try {
			String message = null;
			Object[] argObj = new Object[1];
			if(MasterTableRegType.DEFINITION.getCode().equals(masterTableDef.getKeyType())){
				masterTableFacade.modifyMasterTable(masterTableDef, masterTableIndicators);
				message = "alt.master.table.modify";
				argObj[0] = masterTableDef.getMasterTablePk().toString();
			}else if(MasterTableRegType.ELEMENT.getCode().equals(masterTableDef.getKeyType())){
				masterTableFacade.modifyElementTable(parameterModifyData);
				message = "alt.parameter.table.modify";
				argObj[0] = parameterModifyData.getParameterTablePk().toString();
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
							, PropertiesUtilities.getMessage(message, argObj));
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
		cleanSearchMmt();
	}
	
	
	/**
	 * Valid filters search definition helper.
	 *
	 * @return true, if successful
	 */
	private boolean validFiltersSearchDefinitionHelper(){
		return Validations.validateIsNotNullAndNotEmpty(elementMasterTableSearch.getCode()) 
				|| Validations.validateIsNotNullAndNotEmpty(elementMasterTableSearch.getDescription()); 
	}
	
	/**
	 * selectMasterTableList method used to select data from MASTER_TABLE.
	 */
	
	public void selectMasterTableList(){
		JSFUtilities.hideGeneralDialogues();				
		try{
			if(!validFiltersSearchDefinitionHelper()) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwEnterFilterSearch').show();");
				this.setSelectedMasterTableList(null);
				return;
			}
			List<MasterTable> list = masterTableFacade.leerMasterTableList(elementMasterTableSearch);
			if(Validations.validateListIsNotNullAndNotEmpty(list)){
				this.setSelectedMasterTableList(list);
			}else{
				this.setSelectedMasterTableList(null);			
			}			
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Valid filters search definition mgmt helper.
	 *
	 * @return true, if successful
	 */
	private boolean validFiltersSearchDefinitionMgmtHelper(){
		return Validations.validateIsNotNullAndNotEmpty(masterTableRegisterSearch.getCode())
				|| Validations.validateIsNotNullAndNotEmpty(masterTableRegisterSearch.getDescription());
	}
	
	/**
	 * selectMasterTableListMgmt method used to select data from MASTER_TABLE.
	 */
	public void selectMasterTableListMgmt(){
		JSFUtilities.hideGeneralDialogues();
		try{
			if(!validFiltersSearchDefinitionMgmtHelper()) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwEnterFilterSearch').show();");
				this.setSelectedMasterTableList(null);
				return;
			}
			List<MasterTable> list = masterTableFacade.leerMasterTableList(masterTableRegisterSearch);
			if(Validations.validateListIsNotNullAndNotEmpty(list)){
				this.setSelectedMasterTableList(list);
			}else{
				this.setSelectedMasterTableList(new ArrayList<MasterTable>());			
			}			
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * selectMasterTableListForSearch method used to select data from MASTER_TABLE.
	 *
	 * @return true, if successful
	 */
	
	private boolean validFiltersMasterSearch(){
		return Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getCode())
				|| Validations.validateIsNotNullAndNotEmpty(masterTableSearch.getDescription());
	}
	
	/**
	 * Search master and parameters.
	 */
	@LoggerAuditWeb
	public void searchMasterAndParameters(){
		JSFUtilities.hideGeneralDialogues();
		try{
			selMaster = null;
			selParameter = null;
			if(MasterTableRegType.DEFINITION.getCode().equals(masterTableSearch.getType())){
				
				if(!validFiltersMasterSearch()){
					JSFUtilities.executeJavascriptFunction("PF('cnfwEnterFilterSearch').show();");
					this.getMasterTableSearch().setType(MasterTableRegType.DEFINITION.getCode());
					this.setSelectMasterList(null);
					return;
				}
				
				List<MasterTable> list = masterTableFacade.getDefinitionTables(masterTableSearch);
				if(Validations.validateListIsNotNullAndNotEmpty(list)) {
					this.setSelectMasterList(list);
					this.setMasterSize(list.size());
				}else{
					this.getMasterTableSearch().setType(MasterTableRegType.DEFINITION.getCode());
					this.setSelectMasterList(new ArrayList<MasterTable>());
				}
			}else if(MasterTableRegType.ELEMENT.getCode().equals(masterTableSearch.getType())){
				List<ParameterTable> paramList = masterTableFacade.leerParameterTableDetailSearch(masterTableSearch);
				if(Validations.validateListIsNotNullAndNotEmpty(paramList)){
					this.setSearchParameterList(paramList);
					this.setParameterSize(paramList.size());
				}else{
					this.getMasterTableSearch().setType(MasterTableRegType.ELEMENT.getCode());
					this.setSearchParameterList(new ArrayList<ParameterTable>());
				}
			}
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * searchMasterTableList method used to select data from PARAMETER_TABLE.
	 *
	 * @param event the event
	 */
	public void searchMasterTableList(ActionEvent event){
		try{
			this.setMasterTableDef((MasterTable)event.getComponent().getAttributes().get("masterBean"));
			this.getMasterTableDef().setKeyType(MasterTableRegType.ELEMENT.getCode());
			this.setParameterTableList(masterTableFacade.leerParameterTableDetail(this.getMasterTableDef().getMasterTablePk()));
			this.setSelectedOptions(new ArrayList<String>());
			if(this.getMasterTableDef().getReqIndicator1() != null || this.getMasterTableDef().getReqIndicator2() != null || 
					this.getMasterTableDef().getReqIndicator3() != null || this.getMasterTableDef().getReqIndicator4() != null || 
					this.getMasterTableDef().getReqIndicator5() != null || this.getMasterTableDef().getReqIndicator6() != null){
				this.getSelectedOptions().add(0,"1");
			}else{
				this.getSelectedOptions().add(0,"");
			}
			if(this.getMasterTableDef().getReqDecimalAmount1() != null || this.getMasterTableDef().getReqDecimalAmount2() != null){
				this.getSelectedOptions().add(1,"2");
			}else{
				this.getSelectedOptions().add(1,"");
			}
			if(this.getMasterTableDef().getReqText1() != null || this.getMasterTableDef().getReqText2() != null || 
					this.getMasterTableDef().getReqText3() != null){
				this.getSelectedOptions().add(2,"3");
			}else{
				this.getSelectedOptions().add(2,"");
			}
			if(this.getMasterTableDef().getReqInteger() != null || this.getMasterTableDef().getReqLongInteger() != null){
				this.getSelectedOptions().add(3,"4");
			}else{
				this.getSelectedOptions().add(3,"");
			}
			if(this.getMasterTableDef().getReqDate1() != null || this.getMasterTableDef().getReqDate2() != null){
				this.getSelectedOptions().add(4,"5");
			}else{
				this.getSelectedOptions().add(4,"");
			}
			if(this.getMasterTableDef().getReqSubclasification1() != null || this.getMasterTableDef().getReqSubclasification2() != null){
				this.getSelectedOptions().add(5,"6");
			}else{
				this.getSelectedOptions().add(5,"");
			}
			if(this.getMasterTableDef().getReqObservation() != null){
				this.getSelectedOptions().add(6,"7");
			}else{
				this.getSelectedOptions().add(6,"");
			}
			if(this.getMasterTableDef().getReqState() != null){
				this.getSelectedOptions().add(7,"8");
			}else{
				this.getSelectedOptions().add(7,"");
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
		
	/**
	 * Show new element dialog.
	 */
	public void showNewElementDialog(){
		JSFUtilities.hideGeneralDialogues();
		parameterTable = new ParameterTable();		
		JSFUtilities.resetComponent("frmConsultaMovTitulares:idnewElementDialog");		
	}
	
	/**
	 * modifyElement method used to set the attribute to Session object.
	 *
	 * @param event the event
	 */
	public void modifyElement(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		try{
			ParameterTable parTable =(ParameterTable)event.getComponent().getAttributes().get("selectedElement");
			parameterModifyData = new ParameterTable();
			parameterModifyData.setDate1(parTable.getDate1());
			parameterModifyData.setDate2(parTable.getDate2());
			parameterModifyData.setDecimalAmount1(parTable.getDecimalAmount1());
			parameterModifyData.setDecimalAmount2(parTable.getDecimalAmount2());
			parameterModifyData.setDescription(parTable.getDescription());
			parameterModifyData.setElementSubclasification1(parTable.getElementSubclasification1());
			parameterModifyData.setElementSubclasification2(parTable.getElementSubclasification2());
			parameterModifyData.setIndicator1(parTable.getIndicator1());
			parameterModifyData.setIndicator2(parTable.getIndicator2());
			parameterModifyData.setIndicator3(parTable.getIndicator3());
			parameterModifyData.setIndicator4(parTable.getIndicator4());
			parameterModifyData.setIndicator5(parTable.getIndicator5());
			parameterModifyData.setIndicator6(parTable.getIndicator6());
			parameterModifyData.setLastModifyApp(parTable.getLastModifyApp());
			parameterModifyData.setLastModifyDate(parTable.getLastModifyDate());
			parameterModifyData.setLastModifyIp(parTable.getLastModifyIp());
			parameterModifyData.setLastModifyUser(parTable.getLastModifyUser());
			parameterModifyData.setLongInteger(parTable.getLongInteger());
			parameterModifyData.setMasterTable(parTable.getMasterTable());
			parameterModifyData.setObservation(parTable.getObservation());
			parameterModifyData.setParameterName(parTable.getParameterName());
			parameterModifyData.setParameterState(parTable.getParameterState());
			parameterModifyData.setParameterTableCd(parTable.getParameterTableCd());
			parameterModifyData.setParameterTablePk(parTable.getParameterTablePk());
			parameterModifyData.setRelatedParameter(parTable.getRelatedParameter());
			parameterModifyData.setShortInteger(parTable.getShortInteger());
			parameterModifyData.setText1(parTable.getText1());
			parameterModifyData.setText2(parTable.getText2());
			parameterModifyData.setText3(parTable.getText3());
	    	//this.setParameterModifyData((ParameterTable)event.getComponent().getAttributes().get("selectedElement"));
	    	JSFUtilities.executeJavascriptFunction("PF('modifyElementW').show();");
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * searchMasterTableELement method used to masterTableDef object to elementMasterTableSearch.
	 *
	 * @param event the event
	 */
	public void searchMasterTableELement(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetComponent("frmsearch:iddefinition");
		masterTableDef = new MasterTable();
		this.setMasterTableDef((MasterTable)event.getComponent().getAttributes().get("masterBean"));
		this.getMasterTableDef().setKeyType(MasterTableRegType.ELEMENT.getCode());
		this.getMasterTableSearch().setDefinition(this.getMasterTableDef().getMasterTableCd());
		this.getMasterTableSearch().setDefinitionName(this.getMasterTableDef().getTableName());
	}
	
	/**
	 * searchParameterTableList method used to select data from PARAMETER_TABLE.
	 *
	 * @param event the event
	 */
	public void searchParameterTableList(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
			try{
			MasterTable obj = new MasterTable();
			obj = (MasterTable)event.getComponent().getAttributes().get("masterBean");
			this.setParameterTableList(masterTableFacade.leerParameterTableDetail(obj.getMasterTablePk()));
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * searchMasterTableRandom method used to select data from MASTER_TABLE.
	 */
	public void searchMasterTableRandom(){
		JSFUtilities.hideGeneralDialogues();
		try{
		if(Validations.validateIsNotNullAndNotEmpty(this.getMasterTableSearch().getDefinition() )){
			MasterTable obj = masterTableFacade.searchMasterTableRandom(this.getMasterTableSearch().getDefinition());
			if(Validations.validateIsNotNullAndNotEmpty(obj)){
				this.setMasterTableDef(obj);
				this.getMasterTableSearch().setType(MasterTableRegType.ELEMENT.getCode());
				this.getMasterTableSearch().setDefinition(obj.getMasterTableCd());
				this.getMasterTableSearch().setDefinitionName(obj.getTableName());
				this.getElementMasterTableSearch().setDefinition(obj.getMasterTableCd());
				this.getElementMasterTableSearch().setType(MasterTableRegType.ELEMENT.getCode());
			}else{
				this.getMasterTableSearch().setDefinition(null);
				this.getMasterTableSearch().setDefinitionName(null);				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
	 					PropertiesUtilities.getMessage(PropertiesConstants.MASTER_DEFINITION_NOT_FOUND));
				JSFUtilities.showSimpleValidationDialog();
			}
		}else{
			this.getMasterTableSearch().setDefinition(null);
			this.getMasterTableSearch().setDefinitionName(null);
		}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * searchMasterTableRandom method used to select data from MASTER_TABLE.
	 */
	public void searchMasterTableRandomMgmt(){
		JSFUtilities.hideGeneralDialogues();
		try{
			selectedOptions = null;
			if(Validations.validateIsNotNullAndNotEmpty(this.getMasterTableDef().getMasterTableCd())){
			MasterTable obj = masterTableFacade.searchMasterTableRandom(this.getMasterTableDef().getMasterTableCd());
			if(obj != null){
				this.setMasterTableDef(obj);
				this.getMasterTableDef().setKeyType(MasterTableRegType.ELEMENT.getCode());
				this.setParameterTableList(masterTableFacade.leerParameterTableDetail(this.getMasterTableDef().getMasterTablePk()));
				fillIndicatorsForElement();
			}else{
				masterTableDef = new MasterTable();
				masterTableDef.setKeyType(MasterTableRegType.ELEMENT.getCode());
				parameterTableList = null;
				JSFUtilities.showValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MASTER_DEFINITION_NOT_FOUND));
				}
			}else{
				masterTableDef = new MasterTable();
				masterTableDef.setKeyType(MasterTableRegType.ELEMENT.getCode());
				parameterTableList = null;
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Fill definition.
	 *
	 * @param event the event
	 */
	public void fillDefinition(ActionEvent event){
		blModify = false;
		masterTableDef = new MasterTable();
		masterTableDef = (MasterTable)event.getComponent().getAttributes().get("masterBean");
		masterTableDef.setKeyType(MasterTableRegType.DEFINITION.getCode());
		masterTableIndicators = new MasterTableIndicators();
		fillIndicatorsForDefinition();
	}
	
	/**
	 * Fill element.
	 *
	 * @param event the event
	 */
	public void fillElement(ActionEvent event){
		try{
			blModify = false;
			masterTableDef = new MasterTable();
			parameterModifyData = new ParameterTable();
			parameterModifyData = (ParameterTable)event.getComponent().getAttributes().get("selectedElement");    
		    masterTableDef = masterTableFacade.selectMasterForElementDetail(parameterModifyData);
		    masterTableDef.setKeyType(MasterTableRegType.ELEMENT.getCode());
		    masterTableDef.setRegisterDate(parameterModifyData.getLastModifyDate());
		    fillIndicatorsForElement();
		  }catch(ServiceException e){
		   	 e.printStackTrace();
		  }	     
	}
	
	/**
	 * limpiarMgmt method used to set the new object to MasterTable.
	 *
	 * @return the string
	 */
	public String limpiarMgmt(){
		JSFUtilities.resetViewRoot();
		return beforeRegister();
	}
	
	/**
	 * 	
	 * limpiarMasterTableSearch method used to assign new object to masterTableSearch.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanSearchMmt();
	}
	
	/**
	 * Clean search mmt.
	 */
	public void cleanSearchMmt(){
		JSFUtilities.hideGeneralDialogues();		
		nameFlag = true;
		this.setMasterTableSearch(new MasterTableSearch());
		this.setSelectMasterList(null);
		this.setSearchParameterList(null);
		this.setElementMasterTableSearch(new MasterTableSearch());
		this.setMasterTableDef(new MasterTable());
		this.setSelMaster(null);
	}
	
	/**
	 * 	
	 * limpiarElementMasterTableSearch method used to assign new object to elementMasterTableSearch.
	 */
	public void limpiarElementMasterTableSearch(){
		this.setElementMasterTableSearch(new MasterTableSearch());
		this.setSelectedMasterTableList(new ArrayList<MasterTable>());
	}
	
	/**
	 * Fill definition.
	 *
	 * @param masterTable the master table
	 */
	private void fillDefinition(MasterTable masterTable){
		try{
			masterTableDef = new MasterTable();
			masterTableDef = masterTableFacade.getMasterTable(masterTable.getMasterTablePk());
			masterTableDef.setKeyType(MasterTableRegType.DEFINITION.getCode());
			masterTableIndicators = new MasterTableIndicators();
			fillIndicatorsForDefinition();
		}catch(ServiceException e){
		   	 e.printStackTrace();
	  }	  
	}
	
	/**
	 * Fill element.
	 *
	 * @param parameterTable the parameter table
	 */
	private void fillElement(ParameterTable parameterTable){
		try{
			masterTableDef = new MasterTable();
			parameterModifyData = new ParameterTable();
			parameterModifyData = masterTableFacade.getParameterTable(parameterTable.getParameterTablePk());  
		    masterTableDef = masterTableFacade.selectMasterForElementDetail(parameterModifyData);
		    masterTableDef.setKeyType(MasterTableRegType.ELEMENT.getCode());
		    masterTableDef.setRegisterDate(parameterModifyData.getLastModifyDate());
		    fillIndicatorsForElement();
		  }catch(ServiceException e){
		   	 e.printStackTrace();
		  }	     
	}
	
	/**
	 * To perform before Register Operations.
	 *
	 * @return the string
	 */
	public String beforeRegister(){
		blModify = false;
		masterTableDef = new MasterTable();
		masterTableDef.setKeyType(MasterTableRegType.DEFINITION.getCode());
		masterTableIndicators = new MasterTableIndicators();
		parameterTable = new ParameterTable();
		masterTableRegisterSearch = new MasterTableSearch();
   	 	selectedOptions = null;
   	 	return "newDefiOrParam";
	}
		
	/**
	 * To refresh the search results when we back to search.
	 */
	public void backToSearch(){		
		JSFUtilities.hideGeneralDialogues();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
				, PropertiesUtilities.getGenericMessage("conf.msg.back"));
		JSFUtilities.executeJavascriptFunction("PF('cnfwBackDialog').show();");
	}
	
	/**
	 * Ask for clean.
	 */
	public void askForClean(){
		JSFUtilities.hideGeneralDialogues();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
				, PropertiesUtilities.getGenericMessage("conf.msg.clean"));
		JSFUtilities.executeJavascriptFunction("PF('cnfwCleanDialog').show();");
	}
	
	/**
	 * Ask back to defi.
	 */
	public void askBackToDefi(){
		JSFUtilities.hideGeneralDialogues();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
				, PropertiesUtilities.getGenericMessage("conf.msg.back"));
		JSFUtilities.executeJavascriptFunction("PF('cnfwBackEleDialog').show();");		
	}
	
	/**
	 * Back to defi.
	 */
	public void backToDefi(){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.executeJavascriptFunction("PF('newElementW').hide();");
	}
		
	/**
	 * To validate the definitionName.
	 */
	public void validateDefinitionName(){
		JSFUtilities.hideGeneralDialogues();
		try{
			if(Validations.validateIsNotNullAndNotEmpty(masterTableDef.getTableName())){
				boolean validateNameExists = false;			
				validateNameExists = masterTableFacade.validateDefinitionNameServiceFacade(masterTableDef);			
				if(validateNameExists){
					masterTableDef.setTableName(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MASTERTABLE_NAME_ALREADY_EXISTS));
					JSFUtilities.showValidationDialog();					
				}
			}
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * To validate the short Description.
	 */
	public void validateDefinitionShortDesc(){
		JSFUtilities.hideGeneralDialogues();
		try{
			if(Validations.validateIsNotNullAndNotEmpty(masterTableDef.getShortDescription())){
				boolean validateShortDescExists = false;
				validateShortDescExists = masterTableFacade.validateDefinitionShortDescServiceFacade(masterTableDef);
				if(validateShortDescExists){
					masterTableDef.setShortDescription(null);					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MASTERTABLE_SHORTDESC_ALREADY_EXISTS));
					JSFUtilities.showValidationDialog();
				}
			}
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * To validate long description.
	 */
	public void validateDefinitionLongDesc(){
		JSFUtilities.hideGeneralDialogues();
		try{
			if(Validations.validateIsNotNullAndNotEmpty(masterTableDef.getLongDescription())){
				boolean validateLongDescExists = false;
				validateLongDescExists = masterTableFacade.validateDefinitionLongDescServiceFacade(masterTableDef);
				if(validateLongDescExists){
					masterTableDef.setLongDescription(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MASTERTABLE_LONGDESC_ALREADY_EXISTS));
					JSFUtilities.showValidationDialog();
				}
			}
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}

	/**
	 * To validate ParameterDescription.
	 */
	public void validateParameterName(){
		JSFUtilities.hideGeneralDialogues();
		try{
			if(blModify){
				if(Validations.validateIsNotNullAndNotEmpty(parameterModifyData.getParameterName())){
					boolean validateCodeExists = false;
					validateCodeExists = masterTableFacade.validateParameterNameServiceFacade(parameterModifyData, masterTableDef.getMasterTablePk());
					if(validateCodeExists){
						parameterModifyData.setParameterName(null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.PARAMETERTABLE_DESCRIPTION_ALREADY_EXISTS));
						JSFUtilities.showValidationDialog();
					}
				}
			}else{
				if(Validations.validateIsNotNullAndNotEmpty(parameterTable.getParameterName())){
					boolean validateCodeExists = false;
					validateCodeExists = masterTableFacade.validateParameterNameServiceFacade(parameterTable, masterTableDef.getMasterTablePk());
					if(validateCodeExists){
						parameterTable.setParameterName(null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.PARAMETERTABLE_DESCRIPTION_ALREADY_EXISTS));
						JSFUtilities.showValidationDialog();
					}
				}
			}
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Fill indicators for definition.
	 */
	private void fillIndicatorsForDefinition(){
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqDate1()))
			masterTableIndicators.setDate1(true);
		else
			masterTableDef.setNameDate1(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqDate2()))
			masterTableIndicators.setDate2(true);
		else
			masterTableDef.setNameDate2(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqDecimalAmount1()))
			masterTableIndicators.setDec1(true);
		else
			masterTableDef.setNameDecimalAmount1(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqDecimalAmount2()))
			masterTableIndicators.setDec2(true);
		else
			masterTableDef.setNameDecimalAmount2(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqIndicator1()))
			masterTableIndicators.setInd1(true);
		else
			masterTableDef.setNameIndicator1(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqIndicator2()))
			masterTableIndicators.setInd2(true);
		else
			masterTableDef.setNameIndicator2(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqIndicator3()))
			masterTableIndicators.setInd3(true);
		else
			masterTableDef.setNameIndicator3(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqIndicator4()))
			masterTableIndicators.setInd4(true);
		else
			masterTableDef.setNameIndicator4(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqIndicator5()))
			masterTableIndicators.setInd5(true);
		else
			masterTableDef.setNameIndicator5(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqIndicator6()))
			masterTableIndicators.setInd6(true);
		else
			masterTableDef.setNameIndicator6(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqInteger()))
			masterTableIndicators.setInt1(true);
		else
			masterTableDef.setNameInteger(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqLongInteger()))
			masterTableIndicators.setIntLong(true);
		else
			masterTableDef.setNameLongInteger(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqSubclasification1()))
			masterTableIndicators.setSub1(true);
		else
			masterTableDef.setNameSubclasification1(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqSubclasification2()))
			masterTableIndicators.setSub2(true);
		else
			masterTableDef.setNameSubclasification2(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqText1()))
			masterTableIndicators.setText1(true);
		else
			masterTableDef.setNameText1(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqText2()))
			masterTableIndicators.setText2(true);
		else
			masterTableDef.setNameText2(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqText3()))
			masterTableIndicators.setText3(true);
		else
			masterTableDef.setNameText3(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqObservation()))
			masterTableIndicators.setObs(true);
		else
			masterTableDef.setNameObservation(null);
		
		if(BooleanType.YES.getCode().equals(masterTableDef.getReqState()))
			masterTableIndicators.setState(true);
		else
			masterTableDef.setNameState(null);
	}
	
	/**
	 * Fill indicators for element.
	 */
	private void fillIndicatorsForElement(){
		this.setSelectedOptions(new ArrayList<String>());
		if(this.getMasterTableDef().getReqIndicator1() != null || this.getMasterTableDef().getReqIndicator2() != null || 
				this.getMasterTableDef().getReqIndicator3() != null || this.getMasterTableDef().getReqIndicator4() != null || 
				this.getMasterTableDef().getReqIndicator5() != null || this.getMasterTableDef().getReqIndicator6() != null)
			this.getSelectedOptions().add(0,"1");
		else
			this.getSelectedOptions().add(0,"");
		
		if(this.getMasterTableDef().getReqDecimalAmount1() != null || this.getMasterTableDef().getReqDecimalAmount2() != null)
			this.getSelectedOptions().add(1,"2");
		else
			this.getSelectedOptions().add(1,"");
		
		if(this.getMasterTableDef().getReqText1() != null || this.getMasterTableDef().getReqText2() != null || 
				this.getMasterTableDef().getReqText3() != null)
			this.getSelectedOptions().add(2,"3");
		else
			this.getSelectedOptions().add(2,"");
		
		if(this.getMasterTableDef().getReqInteger() != null || this.getMasterTableDef().getReqLongInteger() != null)
			this.getSelectedOptions().add(3,"4");
		else
			this.getSelectedOptions().add(3,"");
		
		if(this.getMasterTableDef().getReqDate1() != null || this.getMasterTableDef().getReqDate2() != null)
			this.getSelectedOptions().add(4,"5");
		else
			this.getSelectedOptions().add(4,"");
		
		if(this.getMasterTableDef().getReqSubclasification1() != null || this.getMasterTableDef().getReqSubclasification2() != null)
			this.getSelectedOptions().add(5,"6");
		else
			this.getSelectedOptions().add(5,"");
		
		if(this.getMasterTableDef().getReqObservation() != null)
			this.getSelectedOptions().add(6,"7");
		else
			this.getSelectedOptions().add(6,"");
		
		if(this.getMasterTableDef().getReqState() != null)
			this.getSelectedOptions().add(7,"8");
		else
			this.getSelectedOptions().add(7,"");
	}
	
	
	/**
	 * Gets the master table register search.
	 *
	 * @return the master table register search
	 */
	public MasterTableSearch getMasterTableRegisterSearch() {
		return masterTableRegisterSearch;
	}
	
	/**
	 * Sets the master table register search.
	 *
	 * @param masterTableRegisterSearch the new master table register search
	 */
	public void setMasterTableRegisterSearch(
			MasterTableSearch masterTableRegisterSearch) {
		this.masterTableRegisterSearch = masterTableRegisterSearch;
	}
	
	/**
	 * Gets the master table indicators.
	 *
	 * @return the master table indicators
	 */
	public MasterTableIndicators getMasterTableIndicators() {
		return masterTableIndicators;
	}
	
	/**
	 * Sets the master table indicators.
	 *
	 * @param masterTableIndicators the new master table indicators
	 */
	public void setMasterTableIndicators(MasterTableIndicators masterTableIndicators) {
		this.masterTableIndicators = masterTableIndicators;
	}
	
	/**
	 * Checks if is bl modify.
	 *
	 * @return true, if is bl modify
	 */
	public boolean isBlModify() {
		return blModify;
	}
	
	/**
	 * Sets the bl modify.
	 *
	 * @param blModify the new bl modify
	 */
	public void setBlModify(boolean blModify) {
		this.blModify = blModify;
	}
	
	/**
	 * 	
	 * 	/**
	 * Gets the selectedOptions.
	 *
	 * @return selectedOptions List<String>
	 */
	public List<String> getSelectedOptions() {
		return selectedOptions;
	}
	
	/**
	 * set the value to selectedOptions.
	 *
	 * @param selectedOptions the new selected options
	 */
	public void setSelectedOptions(List<String> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
	
	/**
	 * Gets the masterTableDef.
	 *
	 * @return masterTableDef MasterTable
	 */
	public MasterTable getMasterTableDef() {
		return masterTableDef;
	}
	
	/**
	 * set the value to masterTableDef.
	 *
	 * @param masterTableDef the new master table def
	 */
	public void setMasterTableDef(MasterTable masterTableDef) {
		this.masterTableDef = masterTableDef;
	}
	
	/**
	 * Gets the selectedMasterTableList.
	 *
	 * @return selectedMasterTableList List<MasterTable>
	 */
	public List<MasterTable> getSelectedMasterTableList() {
		return selectedMasterTableList;
	}
	
	/**
	 * set the value to selectedMasterTableList.
	 *
	 * @param selectedMasterTableList the new selected master table list
	 */
	public void setSelectedMasterTableList(List<MasterTable> selectedMasterTableList) {
		this.selectedMasterTableList = selectedMasterTableList;
	}
	
	/**
	 * Gets the masterTableSearch.
	 *
	 * @return masterTableSearch MasterTableSearch
	 */
	public MasterTableSearch getMasterTableSearch() {
		return masterTableSearch;
	}
	
	/**
	 * set the value to masterTableSearch.
	 *
	 * @param masterTableSearch the new master table search
	 */
	public void setMasterTableSearch(MasterTableSearch masterTableSearch) {
		this.masterTableSearch = masterTableSearch;
	}
	
	/**
	 * Gets the parameterTable.
	 *
	 * @return parameterTable ParameterTable
	 */
	public ParameterTable getParameterTable() {
		return parameterTable;
	}
	
	/**
	 * set the value to parameterTable.
	 *
	 * @param parameterTable the new parameter table
	 */
	public void setParameterTable(ParameterTable parameterTable) {
		this.parameterTable = parameterTable;
	}
	
	/**
	 * Gets the selMasterTableBean.
	 *
	 * @return selMasterTableBean List<MasterTable>
	 */
	public List<MasterTable> getSelMasterTableBean() {
		return selMasterTableBean;
	}
	
	/**
	 * set the value to selMasterTableBean.
	 *
	 * @param selMasterTableBean the new sel master table bean
	 */
	public void setSelMasterTableBean(List<MasterTable> selMasterTableBean) {
		this.selMasterTableBean = selMasterTableBean;
	}
	
	/**
	 * Gets the parameterTableList.
	 *
	 * @return parameterTableList List<ParameterTable>
	 */
	public List<ParameterTable> getParameterTableList() {
		return parameterTableList;
	}
	
	/**
	 * set the value to parameterTableList.
	 *
	 * @param parameterTableList the new parameter table list
	 */
	public void setParameterTableList(List<ParameterTable> parameterTableList) {
		this.parameterTableList = parameterTableList;
	}
	
	/**
	 * Gets the searchParameterList.
	 *
	 * @return searchParameterList List<ParameterTable>
	 */
	public List<ParameterTable> getSearchParameterList() {
		return searchParameterList;
	}
	
	/**
	 * set the value to searchParameterList.
	 *
	 * @param searchParameterList the new search parameter list
	 */
	public void setSearchParameterList(List<ParameterTable> searchParameterList) {
		this.searchParameterList = searchParameterList;
	}
	
	/**
	 * Gets the selectMasterList.
	 *
	 * @return selectMasterList List<MasterTable>
	 */
	public List<MasterTable> getSelectMasterList() {
		return selectMasterList;
	}
	
	/**
	 * set the value to selectMasterList.
	 *
	 * @param selectMasterList the new select master list
	 */
	public void setSelectMasterList(List<MasterTable> selectMasterList) {
		this.selectMasterList = selectMasterList;
	}
	
	/**
	 * Gets the elementMasterTableSearch.
	 *
	 * @return elementMasterTableSearch MasterTableSearch
	 */
	public MasterTableSearch getElementMasterTableSearch() {
		return elementMasterTableSearch;
	}
	
	/**
	 * set the value to elementMasterTableSearch.
	 *
	 * @param elementMasterTableSearch the new element master table search
	 */
	public void setElementMasterTableSearch(
			MasterTableSearch elementMasterTableSearch) {
		this.elementMasterTableSearch = elementMasterTableSearch;
	}
	
	/**
	 * Gets the regNo.
	 *
	 * @return regNo Integer
	 */
	public Integer getRegNo() {
		return regNo;
	}
	
	/**
	 * set the value to regNo.
	 *
	 * @param regNo the new reg no
	 */
	public void setRegNo(Integer regNo) {
		this.regNo = regNo;
	}
	
	/**
	 * Gets the selParameter.
	 *
	 * @return selParameter ParameterTable
	 */
	public ParameterTable getSelParameter() {
		return selParameter;
	}
	
	/**
	 * set the value to selParameter.
	 *
	 * @param selParameter the new sel parameter
	 */
	public void setSelParameter(ParameterTable selParameter) {
		this.selParameter = selParameter;
	}
	
	/**
	 * Gets the eleRegNo.
	 *
	 * @return eleRegNo int
	 */
	public int getEleRegNo() {
		return eleRegNo;
	}
	
	/**
	 * set the value to eleRegNo.
	 *
	 * @param eleRegNo the new ele reg no
	 */
	public void setEleRegNo(int eleRegNo) {
		this.eleRegNo = eleRegNo;
	}	
   
	/**
	 * Gets the selMaster.
	 *
	 * @return selMaster MasterTable
	 */
	public MasterTable getSelMaster() {
		return selMaster;
	}
	
	/**
	 * set the value to selMaster.
	 *
	 * @param selMaster the new sel master
	 */
	public void setSelMaster(MasterTable selMaster) {
		this.selMaster = selMaster;
	}
	
	/**
	 * Gets the parameterModifyData.
	 *
	 * @return parameterModifyData ParameterTable
	 */
	public ParameterTable getParameterModifyData() {
		return parameterModifyData;
	}
	
	/**
	 * set the value to parameterModifyData.
	 *
	 * @param parameterModifyData the new parameter modify data
	 */
	public void setParameterModifyData(ParameterTable parameterModifyData) {
		this.parameterModifyData = parameterModifyData;
	}
	
	/**
	 * Gets the nameFlag.
	 *
	 * @return nameFlag boolean
	 */
	public boolean isNameFlag() {
		return nameFlag;
	}
	
	/**
	 * set the value to nameFlag.
	 *
	 * @param nameFlag the new name flag
	 */
	public void setNameFlag(boolean nameFlag) {
		this.nameFlag = nameFlag;
	}
	
	/**
	 * Gets the masterSize.
	 *
	 * @return masterSize Integer
	 */
	public Integer getMasterSize() {
		return masterSize;
	}
	
	/**
	 * set the value to masterSize.
	 *
	 * @param masterSize the new master size
	 */
	public void setMasterSize(Integer masterSize) {
		this.masterSize = masterSize;
	}
	
	/**
	 * Gets the parameterSize.
	 *
	 * @return parameterSize Integer
	 */
	public Integer getParameterSize() {
		return parameterSize;
	}
	
	/**
	 * set the value to parameterSize.
	 *
	 * @param parameterSize the new parameter size
	 */
	public void setParameterSize(Integer parameterSize) {
		this.parameterSize = parameterSize;
	}
	
	/**
	 * Gets the masterTableSelected.
	 *
	 * @return masterTableSelected
	 */
	public int getMasterTableSelected() {
		return masterTableSelected;
	}
	
	/**
	 * set the value to masterTableSelected.
	 *
	 * @param masterTableSelected int
	 */
	public void setMasterTableSelected(int masterTableSelected) {
		this.masterTableSelected = masterTableSelected;
	}
	
	/**
	 * Gets the currentDate.
	 *
	 * @return currentDate
	 */
	public Date getCurrentDate() {
		return currentDate;
	}
	
	/**
	 * set the value to currentDate.
	 *
	 * @param currentDate Date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	/**
	 * Gets the list.
	 *
	 * @return list
	 */
	public List<MasterTableRegType> getMasterTableTypeList(){
		return MasterTableRegType.list;
	}
}
