package com.pradera.generalparameters.mastertables.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.generalparameters.mastertables.service.MasterTableService;
import com.pradera.generalparameters.mastertables.view.MasterTableIndicators;
import com.pradera.generalparameters.mastertables.view.MasterTableSearch;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.MasterTable;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableStatusType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MasterTableFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class MasterTableFacade{
	
	/** The Transaction synchronization Registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The service. */
	@EJB
	private MasterTableService service;
	
	/**
	 * MasterTableFacade constructor of MasterTableFacade class.
	 */
	public MasterTableFacade(){
		super();
	}
	
	/**
	 * saveDefinition method used to save data into MASTER_TABLE.
	 *
	 * @param masterTableDef1 the master table def1
	 * @return the master table
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.MASTER_TABLE_REGISTRATION)
	public MasterTable saveDefinition(MasterTable masterTableDef1) throws ServiceException{
		synchronized (service) {
		    LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		    masterTableDef1.setState(MasterTableStatusType.ACTIVE.getCode());
		    masterTableDef1.setRegisterDate(CommonsUtilities.currentDate());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameIndicator1()))
		    	masterTableDef1.setReqIndicator1(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameIndicator2()))
		    	masterTableDef1.setReqIndicator2(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameIndicator3()))
		    	masterTableDef1.setReqIndicator3(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameIndicator4()))
		    	masterTableDef1.setReqIndicator4(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameIndicator5()))
		    	masterTableDef1.setReqIndicator5(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameIndicator6()))
		    	masterTableDef1.setReqIndicator6(BooleanType.YES.getCode());
		    
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameDecimalAmount1()))
		    	masterTableDef1.setReqDecimalAmount1(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameDecimalAmount2()))
		    	masterTableDef1.setReqDecimalAmount2(BooleanType.YES.getCode());
		    
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameText1()))
		    	masterTableDef1.setReqText1(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameText2()))
		    	masterTableDef1.setReqText2(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameText3()))
		    	masterTableDef1.setReqText3(BooleanType.YES.getCode());
		    
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameInteger()))
		    	masterTableDef1.setReqInteger(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameLongInteger()))
		    	masterTableDef1.setReqLongInteger(BooleanType.YES.getCode());
		    
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameDate1()))
		    	masterTableDef1.setReqDate1(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameDate2()))
		    	masterTableDef1.setReqDate2(BooleanType.YES.getCode());
		    
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameSubclasification1()))
		    	masterTableDef1.setReqSubclasification1(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameSubclasification2()))
		    	masterTableDef1.setReqSubclasification2(BooleanType.YES.getCode());
		    
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameState()))
		    	masterTableDef1.setReqState(BooleanType.YES.getCode());
		    if(Validations.validateIsNotNullAndNotEmpty(masterTableDef1.getNameObservation()))
		    	masterTableDef1.setReqObservation(BooleanType.YES.getCode());
			
		    MasterTable masterTab = service.saveDefinitionService(masterTableDef1);
		    masterTableDef1.setMasterTableCd(masterTab.getMasterTablePk().toString());
		    service.update(masterTableDef1);
			return masterTableDef1;
		}
	}
	
	/**
	 * updateDefinition method used to update data into MASTER_TABLE.
	 *
	 * @param masterTableSearch the master table search
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	/*@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public MasterTable updateDefinition(MasterTable masterTableDef1,List<String> selectedIndicators,List<String> selectedDecimals,List<String> selectedCadenas,List<String> selectedNumeros,
			List<String> selectedFechas,List<String> selectedSubLocations,List<String> selectedState,List<String> selectedObservation) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(loggerUser.getUserAction().isModify()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		
		MasterTable masterTableDef = new MasterTable();
		masterTableDef.setMasterTablePk(masterTableDef1.getMasterTablePk());
		masterTableDef.setMasterTableCd(masterTableDef1.getMasterTableCd());
		masterTableDef.setTableName(masterTableDef1.getTableName());
		masterTableDef.setLongDescription(masterTableDef1.getLongDescription());
		masterTableDef.setShortDescription(masterTableDef1.getShortDescription());
		masterTableDef.setKeySize(masterTableDef1.getKeySize());
		masterTableDef.setKeyType(masterTableDef1.getKeyType());
		masterTableDef.setState(masterTableDef1.getState());
		masterTableDef.setParameterTables(masterTableDef1.getParameterTables());
		if(selectedIndicators != null && selectedIndicators.size() > 0){
			for(int i = 0;i < selectedIndicators.size();i++){
				if(!selectedIndicators.get(i).trim().equals("") && Integer.parseInt(selectedIndicators.get(i).trim()) == 1){
					masterTableDef.setReqIndicator1(new Integer(selectedIndicators.get(i)));
					masterTableDef.setNameIndicator1(masterTableDef1.getNameIndicator1());
				}else if(!selectedIndicators.get(i).trim().equals("") && Integer.parseInt(selectedIndicators.get(i).trim()) == 2){
					masterTableDef.setReqIndicator2(new Integer(selectedIndicators.get(i)));
					masterTableDef.setNameIndicator2(masterTableDef1.getNameIndicator2());
				}else if(!selectedIndicators.get(i).trim().equals("") && Integer.parseInt(selectedIndicators.get(i).trim()) == 3){
					masterTableDef.setReqIndicator3(new Integer(selectedIndicators.get(i)));
					masterTableDef.setNameIndicator3(masterTableDef1.getNameIndicator3());
				}else if(!selectedIndicators.get(i).trim().equals("") && Integer.parseInt(selectedIndicators.get(i).trim()) == 4){
					masterTableDef.setReqIndicator4(new Integer(selectedIndicators.get(i)));
					masterTableDef.setNameIndicator4(masterTableDef1.getNameIndicator4());
				}else if(!selectedIndicators.get(i).trim().equals("") && Integer.parseInt(selectedIndicators.get(i).trim()) == 5){
					masterTableDef.setReqIndicator5(new Integer(selectedIndicators.get(i)));
					masterTableDef.setNameIndicator5(masterTableDef1.getNameIndicator5());
				}else if(!selectedIndicators.get(i).trim().equals("") && Integer.parseInt(selectedIndicators.get(i).trim()) == 6){
					masterTableDef.setReqIndicator6(new Integer(selectedIndicators.get(i)));
					masterTableDef.setNameIndicator6(masterTableDef1.getNameIndicator6());
				}
			}
		}
		if(selectedDecimals != null && selectedDecimals.size() > 0){
			for(int i = 0;i < selectedDecimals.size();i++){
				if(!selectedDecimals.get(i).trim().equals("") && Integer.parseInt(selectedDecimals.get(i).trim()) == 1){
					masterTableDef.setReqDecimalAmount1(new Integer(selectedDecimals.get(i)));
					masterTableDef.setNameDecimalAmount1(masterTableDef1.getNameDecimalAmount1());
				}else if(!selectedDecimals.get(i).trim().equals("") && Integer.parseInt(selectedDecimals.get(i).trim()) == 2){
					masterTableDef.setReqDecimalAmount2(new Integer(selectedDecimals.get(i)));
					masterTableDef.setNameDecimalAmount2(masterTableDef1.getNameDecimalAmount2());
				}
			}
		}
		if(selectedCadenas != null && selectedCadenas.size() > 0){
			for(int i = 0;i < selectedCadenas.size();i++){
				if(!selectedCadenas.get(i).trim().equals("") && Integer.parseInt(selectedCadenas.get(i).trim()) == 1){
					masterTableDef.setReqText1(new Integer(selectedCadenas.get(i)));
					masterTableDef.setNameText1(masterTableDef1.getNameText1());
				}else if(!selectedCadenas.get(i).trim().equals("") && Integer.parseInt(selectedCadenas.get(i).trim()) == 2){
					masterTableDef.setReqText2(new Integer(selectedCadenas.get(i)));
					masterTableDef.setNameText2(masterTableDef1.getNameText2());
				}else if(!selectedCadenas.get(i).trim().equals("") && Integer.parseInt(selectedCadenas.get(i).trim()) == 3){
					masterTableDef.setReqText3(new Integer(selectedCadenas.get(i)));
					masterTableDef.setNameText3(masterTableDef1.getNameText3());
				}
			}
		}
		if(selectedNumeros != null && selectedNumeros.size() > 0){
			for(int i = 0;i < selectedNumeros.size();i++){
				if(!selectedNumeros.get(i).trim().equals("") && Integer.parseInt(selectedNumeros.get(i).trim()) == 1){
					masterTableDef.setReqInteger(new Integer(selectedNumeros.get(i)));
					masterTableDef.setNameInteger(masterTableDef1.getNameInteger());
				}else if(!selectedNumeros.get(i).trim().equals("") && Integer.parseInt(selectedNumeros.get(i).trim()) == 2){
					masterTableDef.setReqLongInteger(new Integer(selectedNumeros.get(i)));
					masterTableDef.setNameLongInteger(masterTableDef1.getNameLongInteger());
				}
			}
		}
		if(selectedFechas != null && selectedFechas.size() > 0){
			for(int i = 0;i < selectedFechas.size();i++){
				if(!selectedFechas.get(i).trim().equals("") && Integer.parseInt(selectedFechas.get(i).trim()) == 1){
					masterTableDef.setReqDate1(new Integer(selectedFechas.get(i)));
					masterTableDef.setNameDate1(masterTableDef1.getNameDate1());
				}else if(!selectedFechas.get(i).trim().equals("") && Integer.parseInt(selectedFechas.get(i).trim()) == 2){
					masterTableDef.setReqDate2(new Integer(selectedFechas.get(i)));
					masterTableDef.setNameDate2(masterTableDef1.getNameDate2());
				}
			}
		}
		if(selectedSubLocations != null && selectedSubLocations.size() > 0){
			for(int i = 0;i < selectedSubLocations.size();i++){
				if(!selectedSubLocations.get(i).trim().equals("") && Integer.parseInt(selectedSubLocations.get(i).trim()) == 1){
					masterTableDef.setReqSubclasification1(new Integer(selectedSubLocations.get(i)));
					masterTableDef.setNameSubclasification1(masterTableDef1.getNameSubclasification1());
				}else if(!selectedSubLocations.get(i).trim().equals("") &&  Integer.parseInt(selectedSubLocations.get(i).trim()) == 2){
					masterTableDef.setReqSubclasification2(new Integer(selectedSubLocations.get(i)));
					masterTableDef.setNameSubclasification2(masterTableDef1.getNameSubclasification2());
				}
			}
			
			
		}
		if(selectedState != null && selectedState.size() > 0 && !selectedState.get(0).equals("")){
			masterTableDef.setReqState(new Integer(selectedState.get(0)));
			masterTableDef.setNameState(masterTableDef1.getNameState());
		}
		if(selectedObservation != null && selectedObservation.size() > 0 && !selectedObservation.get(0).equals("")){
			masterTableDef.setReqObservation(new Integer(selectedObservation.get(0)));
			masterTableDef.setNameObservation(masterTableDef1.getNameObservation()); 
		}
		return service.updateDefinition(masterTableDef);
	}*/
	/**
	 * confirmMasterTableList method used to update MASTER_TABLE status to Confirm
	 * @param selMasterTableBean
	 * @throws ServiceException 
	 */
	/*public void confirmMasterTableList(MasterTable selMasterTableBean) throws ServiceException{
	    LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    if(loggerUser.getUserAction().isConfirm()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
   	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
   		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
    	}
		service.confirmMasterTableList(selMasterTableBean);
	}*/
	/**
	 * rejectMasterTableList method used to update MASTER_TABLE status to Reject
	 * @param selMasterTableBean
	 * @throws ServiceException 
	 */
	/*@TransactionAttribute(TransactionAttributeType.REQUIRED)
	/*public void rejectMasterTableList(MasterTable selMasterTableBean) throws ServiceException{
	    LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    if(loggerUser.getUserAction().isReject()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
   	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
   		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
    	}
		service.rejectMasterTableList(selMasterTableBean);
	}*/
	/**
	 * confirmParameterTableList method used to update PARAMETER_TABLE status to Confirm
	 * @param parameterTable
	 * @throws ServiceException 
	 */
	/*@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void confirmParameterTableList(ParameterTable parameterTable) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(loggerUser.getUserAction().isConfirm()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
    	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		service.confirmParameterTableList(parameterTable);
	}*/
	/**
	 * rejectParameterTableList method used to update PARAMETER_TABLE status to Reject
	 * @param parameterTable
	 * @throws ServiceException 
	 */
	/*@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void rejectParameterTableList(ParameterTable parameterTable) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(loggerUser.getUserAction().isReject()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		service.rejectParameterTableList(parameterTable);
	}*/
	/**
	 * leerMasterTableList method used to select data from MASTER_TABLE
	 * @param masterTableSearch
	 * @return
	 * @throws ServiceException 
	 */
	public List<MasterTable> leerMasterTableList(MasterTableSearch masterTableSearch) throws ServiceException{
		return service.service_leerMasterTableList(masterTableSearch);
	}
	
	/**
	 * saveParameterFecade method used to save data into PARAMETER_TABLE.
	 *
	 * @param parameterTable the parameter table
	 * @return the parameter table
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.MASTER_ELEMENT_REGISTRATION)
	public ParameterTable saveParameterFecade(ParameterTable parameterTable) throws ServiceException{
		synchronized (service) {
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			parameterTable.setParameterState(ParameterTableStateType.REGISTERED.getCode());
			parameterTable.setParameterTableCd(service.getCodeForParameterTable(parameterTable.getMasterTable().getMasterTablePk()));
			service.saveParameterService(parameterTable);
			return parameterTable;
		}
		
	}
	
	/**
	 * leerParameterTableDetail method used to select data from PARAMETER_TABLE.
	 *
	 * @param pk the pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> leerParameterTableDetail(Integer pk) throws ServiceException{
		return service.leerParameterTableDetail(pk);
	}
	
	/**
	 * searchMasterTableRandom method used to select data from MASTER_TABLE.
	 *
	 * @param masterTableCd the master table cd
	 * @return the master table
	 * @throws ServiceException the service exception
	 */
	public MasterTable searchMasterTableRandom(String masterTableCd) throws ServiceException{
		MasterTable obj = null;
		List<MasterTable> list=service.searchMasterTableRandom(masterTableCd);
		if(list != null && list.size() > 0){
			obj = list.get(0);
		}
		return obj;
	}
	
	/**
	 * leerParameterTableDetailSearch method used to select data from PARAMETER_TABLE.
	 *
	 * @param masterTableSearch the master table search
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MASTER_TABLE_QUERY)
	public List<ParameterTable> leerParameterTableDetailSearch(MasterTableSearch masterTableSearch) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return service.leerParameterTableDetailSearch(masterTableSearch);
	}
	
	/**
	 * getDefinitionCode method used to check the definition code exist in database or not.
	 *
	 * @param definitionCode the definition code
	 * @param elementCode the element code
	 * @return the element code
	 * @throws ServiceException the service exception
	 */
	/*public MasterTable getDefinitionCode(String definitionCode) throws ServiceException{
		MasterTable obj = null;
		List<MasterTable> list = service.getDefinitionCode(definitionCode);
		if(list != null && list.size() > 0){
			obj = list.get(0);
		}
		return obj;
	}*/
	/**
	 * getElementCode method used to check the element code exist in database or not
	 * @param definitionCode
	 * @throws ServiceException 
	 */
	public ParameterTable getElementCode(String definitionCode,Integer elementCode) throws ServiceException{
		MasterTable obj = null;
		ParameterTable pObj = null;
		List<MasterTable> masterTableList = service.getMasterTableID(definitionCode);
		if(masterTableList != null && masterTableList.size() > 0){
			obj = masterTableList.get(0);
			List<ParameterTable> parameterTableList = service.getElementCode(obj,elementCode);
			if(parameterTableList != null && parameterTableList.size() > 0){
				pObj = parameterTableList.get(0);
			}
		}
		
		return pObj;
	}
	
	/**
	 * selectMasterForElementDetail method used to select data from mastertable.
	 *
	 * @param pObj the obj
	 * @return the master table
	 * @throws ServiceException the service exception
	 */
	public MasterTable selectMasterForElementDetail(ParameterTable pObj) throws ServiceException{
		MasterTable obj = null;
		List<MasterTable> list= service.selectMasterForElementDetail(pObj);
		if(list != null && list.size() > 0){
			obj = list.get(0);
		}
		return obj;
	}
	
	/**
	 * To validate the DefinitionName.
	 *
	 * @param masterTable the master table
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public boolean validateDefinitionNameServiceFacade(MasterTable masterTable) throws ServiceException{
		return service.validateDefinitionNameServiceBean(masterTable)>0;
	}
	
	/**
	 * To validate short Description.
	 *
	 * @param masterTable the master table
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public boolean validateDefinitionShortDescServiceFacade(MasterTable masterTable) throws ServiceException{
		return service.validateDefinitionShortDescServiceBean(masterTable)>0;
	}
	
	/**
	 * To validate LongDescription.
	 *
	 * @param masterTable the master table
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public boolean validateDefinitionLongDescServiceFacade(MasterTable masterTable) throws ServiceException{
		return service.validateDefinitionLongDescServiceBean(masterTable)>0;
	}
	
	/**
	 * To validate parameterDescription.
	 *
	 * @param parameterTable the parameter table
	 * @param masterTablePk the master table pk
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	public boolean validateParameterNameServiceFacade(ParameterTable parameterTable, Integer masterTablePk) throws ServiceException {
		return service.validateParameterNameServiceFacade(parameterTable, masterTablePk)>0;
	}
	
	/**
	 * Gets the definition tables.
	 *
	 * @param masterTableSearch the master table search
	 * @return the definition tables
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MASTER_TABLE_QUERY)
	public List<MasterTable> getDefinitionTables(MasterTableSearch masterTableSearch) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return service.getDefinitionTables(masterTableSearch);
	}
	
	/**
	 * Modify master table.
	 *
	 * @param masterTableDef the master table def
	 * @param masterTabInd the master tab ind
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MASTER_TABLE_MODIFICATION)
	public void modifyMasterTable(MasterTable masterTableDef, MasterTableIndicators masterTabInd)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		if(masterTabInd.isInd1())
			masterTableDef.setReqIndicator1(BooleanType.YES.getCode());
		else
			masterTableDef.setReqIndicator1(null);
		if(masterTabInd.isInd2())
	    	masterTableDef.setReqIndicator2(BooleanType.YES.getCode());
		else
			masterTableDef.setReqIndicator2(null);
		if(masterTabInd.isInd3())
	    	masterTableDef.setReqIndicator3(BooleanType.YES.getCode());
		else
			masterTableDef.setReqIndicator3(null);
		if(masterTabInd.isInd4())
	    	masterTableDef.setReqIndicator4(BooleanType.YES.getCode());
		else
			masterTableDef.setReqIndicator4(null);
		if(masterTabInd.isInd5())
	    	masterTableDef.setReqIndicator5(BooleanType.YES.getCode());
		else
			masterTableDef.setReqIndicator5(null);
		if(masterTabInd.isInd6())
	    	masterTableDef.setReqIndicator6(BooleanType.YES.getCode());
		else
			masterTableDef.setReqIndicator6(null);
	    
		if(masterTabInd.isDec1())
	    	masterTableDef.setReqDecimalAmount1(BooleanType.YES.getCode());
		else
			masterTableDef.setReqDecimalAmount1(null);
		if(masterTabInd.isDec2())
	    	masterTableDef.setReqDecimalAmount2(BooleanType.YES.getCode());
		else
			masterTableDef.setReqDecimalAmount2(null);
	    
		if(masterTabInd.isText1())
	    	masterTableDef.setReqText1(BooleanType.YES.getCode());
		else
			masterTableDef.setReqText1(null);
		if(masterTabInd.isText2())
	    	masterTableDef.setReqText2(BooleanType.YES.getCode());
		else
			masterTableDef.setReqText2(null);
		if(masterTabInd.isText3())
	    	masterTableDef.setReqText3(BooleanType.YES.getCode());
		else
			masterTableDef.setReqText3(null);
	    
		if(masterTabInd.isInt1())
	    	masterTableDef.setReqInteger(BooleanType.YES.getCode());
		else
			masterTableDef.setReqInteger(null);
		if(masterTabInd.isIntLong())
	    	masterTableDef.setReqLongInteger(BooleanType.YES.getCode());
		else
			masterTableDef.setReqLongInteger(null);
	    
		if(masterTabInd.isDate1())
	    	masterTableDef.setReqDate1(BooleanType.YES.getCode());
		else
			masterTableDef.setReqDate1(null);
		if(masterTabInd.isDate2())
	    	masterTableDef.setReqDate2(BooleanType.YES.getCode());
		else
			masterTableDef.setReqDate2(null);
	    
		if(masterTabInd.isSub1())
	    	masterTableDef.setReqSubclasification1(BooleanType.YES.getCode());
		else
			masterTableDef.setReqSubclasification1(null);
		if(masterTabInd.isSub2())
	    	masterTableDef.setReqSubclasification2(BooleanType.YES.getCode());
		else
			masterTableDef.setReqSubclasification2(null);
	    
		if(masterTabInd.isState())
	    	masterTableDef.setReqState(BooleanType.YES.getCode());
		else
			masterTableDef.setReqState(null);
		if(masterTabInd.isObs())
	    	masterTableDef.setReqObservation(BooleanType.YES.getCode());
		else
			masterTableDef.setReqObservation(null);
		service.update(masterTableDef);
	}
	
	/**
	 * Modify element table.
	 *
	 * @param parameterModifyData the parameter modify data
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MASTER_ELEMENT_MODIFICATION)
	public void modifyElementTable(ParameterTable parameterModifyData) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		service.update(parameterModifyData);		
	}
	
	/**
	 * Gets the parameter table.
	 *
	 * @param parameterTablePk the parameter table pk
	 * @return the parameter table
	 * @throws ServiceException the service exception
	 */
	public ParameterTable getParameterTable(Integer parameterTablePk) throws ServiceException{
		ParameterTable parameterTable = (ParameterTable)service.find(ParameterTable.class, parameterTablePk);
		parameterTable.getMasterTable().getShortDescription();
		return parameterTable;
	}
	
	/**
	 * Gets the master table.
	 *
	 * @param masterTablePk the master table pk
	 * @return the master table
	 * @throws ServiceException the service exception
	 */
	public MasterTable getMasterTable(Integer masterTablePk) throws ServiceException{
		return (MasterTable)service.find(MasterTable.class, masterTablePk);
	}
	
}
