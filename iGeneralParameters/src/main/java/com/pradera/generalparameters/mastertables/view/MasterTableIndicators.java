package com.pradera.generalparameters.mastertables.view;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MasterTableIndicators.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class MasterTableIndicators implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The ind1. */
	private boolean ind1;
	
	/** The ind2. */
	private boolean ind2;
	
	/** The ind3. */
	private boolean ind3;
	
	/** The ind4. */
	private boolean ind4;
	
	/** The ind5. */
	private boolean ind5;
	
	/** The ind6. */
	private boolean ind6;
	
	/** The dec1. */
	private boolean dec1;
	
	/** The dec2. */
	private boolean dec2;
	
	/** The text1. */
	private boolean text1;
	
	/** The text2. */
	private boolean text2;
	
	/** The text3. */
	private boolean text3;
	
	/** The int1. */
	private boolean int1;
	
	/** The int long. */
	private boolean intLong;
	
	/** The date1. */
	private boolean date1;
	
	/** The date2. */
	private boolean date2;
	
	/** The sub1. */
	private boolean sub1;
	
	/** The sub2. */
	private boolean sub2;
	
	/** The state. */
	private boolean state;
	
	/** The obs. */
	private boolean obs;
	
	/**
	 * Checks if is ind1.
	 *
	 * @return true, if is ind1
	 */
	public boolean isInd1() {
		return ind1;
	}
	
	/**
	 * Sets the ind1.
	 *
	 * @param ind1 the new ind1
	 */
	public void setInd1(boolean ind1) {
		this.ind1 = ind1;
	}
	
	/**
	 * Checks if is ind2.
	 *
	 * @return true, if is ind2
	 */
	public boolean isInd2() {
		return ind2;
	}
	
	/**
	 * Sets the ind2.
	 *
	 * @param ind2 the new ind2
	 */
	public void setInd2(boolean ind2) {
		this.ind2 = ind2;
	}
	
	/**
	 * Checks if is ind3.
	 *
	 * @return true, if is ind3
	 */
	public boolean isInd3() {
		return ind3;
	}
	
	/**
	 * Sets the ind3.
	 *
	 * @param ind3 the new ind3
	 */
	public void setInd3(boolean ind3) {
		this.ind3 = ind3;
	}
	
	/**
	 * Checks if is ind4.
	 *
	 * @return true, if is ind4
	 */
	public boolean isInd4() {
		return ind4;
	}
	
	/**
	 * Sets the ind4.
	 *
	 * @param ind4 the new ind4
	 */
	public void setInd4(boolean ind4) {
		this.ind4 = ind4;
	}
	
	/**
	 * Checks if is ind5.
	 *
	 * @return true, if is ind5
	 */
	public boolean isInd5() {
		return ind5;
	}
	
	/**
	 * Sets the ind5.
	 *
	 * @param ind5 the new ind5
	 */
	public void setInd5(boolean ind5) {
		this.ind5 = ind5;
	}
	
	/**
	 * Checks if is ind6.
	 *
	 * @return true, if is ind6
	 */
	public boolean isInd6() {
		return ind6;
	}
	
	/**
	 * Sets the ind6.
	 *
	 * @param ind6 the new ind6
	 */
	public void setInd6(boolean ind6) {
		this.ind6 = ind6;
	}
	
	/**
	 * Checks if is dec1.
	 *
	 * @return true, if is dec1
	 */
	public boolean isDec1() {
		return dec1;
	}
	
	/**
	 * Sets the dec1.
	 *
	 * @param dec1 the new dec1
	 */
	public void setDec1(boolean dec1) {
		this.dec1 = dec1;
	}
	
	/**
	 * Checks if is dec2.
	 *
	 * @return true, if is dec2
	 */
	public boolean isDec2() {
		return dec2;
	}
	
	/**
	 * Sets the dec2.
	 *
	 * @param dec2 the new dec2
	 */
	public void setDec2(boolean dec2) {
		this.dec2 = dec2;
	}
	
	/**
	 * Checks if is text1.
	 *
	 * @return true, if is text1
	 */
	public boolean isText1() {
		return text1;
	}
	
	/**
	 * Sets the text1.
	 *
	 * @param text1 the new text1
	 */
	public void setText1(boolean text1) {
		this.text1 = text1;
	}
	
	/**
	 * Checks if is text2.
	 *
	 * @return true, if is text2
	 */
	public boolean isText2() {
		return text2;
	}
	
	/**
	 * Sets the text2.
	 *
	 * @param text2 the new text2
	 */
	public void setText2(boolean text2) {
		this.text2 = text2;
	}
	
	/**
	 * Checks if is text3.
	 *
	 * @return true, if is text3
	 */
	public boolean isText3() {
		return text3;
	}
	
	/**
	 * Sets the text3.
	 *
	 * @param text3 the new text3
	 */
	public void setText3(boolean text3) {
		this.text3 = text3;
	}
	
	/**
	 * Checks if is int1.
	 *
	 * @return true, if is int1
	 */
	public boolean isInt1() {
		return int1;
	}
	
	/**
	 * Sets the int1.
	 *
	 * @param int1 the new int1
	 */
	public void setInt1(boolean int1) {
		this.int1 = int1;
	}
	
	/**
	 * Checks if is int long.
	 *
	 * @return true, if is int long
	 */
	public boolean isIntLong() {
		return intLong;
	}
	
	/**
	 * Sets the int long.
	 *
	 * @param intLong the new int long
	 */
	public void setIntLong(boolean intLong) {
		this.intLong = intLong;
	}
	
	/**
	 * Checks if is date1.
	 *
	 * @return true, if is date1
	 */
	public boolean isDate1() {
		return date1;
	}
	
	/**
	 * Sets the date1.
	 *
	 * @param date1 the new date1
	 */
	public void setDate1(boolean date1) {
		this.date1 = date1;
	}
	
	/**
	 * Checks if is date2.
	 *
	 * @return true, if is date2
	 */
	public boolean isDate2() {
		return date2;
	}
	
	/**
	 * Sets the date2.
	 *
	 * @param date2 the new date2
	 */
	public void setDate2(boolean date2) {
		this.date2 = date2;
	}
	
	/**
	 * Checks if is sub1.
	 *
	 * @return true, if is sub1
	 */
	public boolean isSub1() {
		return sub1;
	}
	
	/**
	 * Sets the sub1.
	 *
	 * @param sub1 the new sub1
	 */
	public void setSub1(boolean sub1) {
		this.sub1 = sub1;
	}
	
	/**
	 * Checks if is sub2.
	 *
	 * @return true, if is sub2
	 */
	public boolean isSub2() {
		return sub2;
	}
	
	/**
	 * Sets the sub2.
	 *
	 * @param sub2 the new sub2
	 */
	public void setSub2(boolean sub2) {
		this.sub2 = sub2;
	}
	
	/**
	 * Checks if is state.
	 *
	 * @return true, if is state
	 */
	public boolean isState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(boolean state) {
		this.state = state;
	}
	
	/**
	 * Checks if is obs.
	 *
	 * @return true, if is obs
	 */
	public boolean isObs() {
		return obs;
	}
	
	/**
	 * Sets the obs.
	 *
	 * @param obs the new obs
	 */
	public void setObs(boolean obs) {
		this.obs = obs;
	}	
}
