package com.pradera.generalparameters.mastertables.view;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MasterTableSearch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class MasterTableSearch implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The type. */
	private Integer type;
	
	/** The code. */
	private String code;
	
	/** The description. */
	private String description;
	
	/** The start_ date. */
	private Date start_Date;
	
	/** The end_ date. */
	private Date end_Date;
	
	/** The definition. */
	private String definition;
	
	/** The definition name. */
	private String definitionName;
	
	/**
	 * MasterTableSearch constructor of MasterTableSearch class.
	 */
	public MasterTableSearch(){
		start_Date = new Date();
		end_Date = new Date();
	}
	
	/**
	 * Gets the type.
	 *
	 * @return type
	 */
	public Integer getType() {
		return type;
	}
	
	/**
	 * set the value to type.
	 *
	 * @param type int
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * set the value to description.
	 *
	 * @param description String
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the start_Date.
	 *
	 * @return start_Date
	 */
	public Date getStart_Date() {
		return start_Date;
	}
	
	/**
	 * set the value to start_Date.
	 *
	 * @param start_Date Date
	 */
	public void setStart_Date(Date start_Date) {
		this.start_Date = start_Date;
	}
	
	/**
	 * Gets the end_Date.
	 *
	 * @return end_Date
	 */
	public Date getEnd_Date() {
		return end_Date;
	}
	
	/**
	 * set the value to end_Date.
	 *
	 * @param end_Date Date
	 */
	public void setEnd_Date(Date end_Date) {
		this.end_Date = end_Date;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * set the value to code.
	 *
	 * @param code String
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * Gets the definition.
	 *
	 * @return definition
	 */
	public String getDefinition() {
		return definition;
	}
	
	/**
	 * set the value to definition.
	 *
	 * @param definition String
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
	/**
	 * Gets the definitionName.
	 *
	 * @return definitionName
	 */
	public String getDefinitionName() {
		return definitionName;
	}
	
	/**
	 * set the value to definitionName.
	 *
	 * @param definitionName String
	 */
	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}
}
