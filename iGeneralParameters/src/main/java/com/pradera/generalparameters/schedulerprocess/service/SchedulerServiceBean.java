package com.pradera.generalparameters.schedulerprocess.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.pradera.commons.configuration.UserAdmin;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.resources.InterfaceSchedulerService;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessSchedule;
import com.pradera.model.process.SchedulerInfo;
import com.pradera.model.process.type.ProcessScheduleStateType;
import com.pradera.model.process.type.ProcessType;
import com.pradera.model.process.type.ScheduleExecuteType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SchedulerServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/06/2013
 */
@Path("/scheduler")
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class SchedulerServiceBean extends CrudDaoServiceBean implements Serializable {

   /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

/** The timer service. */
   @Resource
   TimerService timerService;
   
   /** The client rest service. */
   @Inject 
   ClientRestService clientRestService;
   
   /** The log. */
   @Inject
   transient PraderaLogger log;
   
   /** The user admin. */
   @Inject @UserAdmin
   String userAdmin;
   
   /** The interface scheduler service. */
   @Inject
   InterfaceSchedulerService interfaceSchedulerService;
   
   /** The transaction registry. */
   @Resource
   TransactionSynchronizationRegistry transactionRegistry;
   
   /** The stage application. */
   //@Inject
   //StageApplication stageApplication;
   
   /**
    * Reload timer saved.
    * In cluster commented PostConstructor
    */
//   @PostConstruct
   public void reloadTimerSaved(){
       try {
	       List<ProcessSchedule> listScheduler = getAllScheduler();
	       for(ProcessSchedule processSchedule : listScheduler){
	           //only register timer for not persistence timer
	           if (processSchedule.getScheduleInfo().getIndPersistence().equals(BooleanType.NO.getCode())){
	        	   updateScheduleTimer(processSchedule);
	        	   //if(stageApplication.equals(StageApplication.Production)){
		        	   if(processSchedule.getScheduleExecution()!=null && processSchedule.getScheduleExecution().equals(ScheduleExecuteType.RUNNING_ERROR.getCode())){
		        		   //If Singleton Timer start and Schedule is in state running call execution of process
		        		   retryExecuteProcess(processSchedule);
		        	   }
	        	   //}
	           }
	       }
	       interfaceSchedulerService.loadExternalInterfaceTimmer();
       } catch(ServiceException sex){
           log.info("error loading timers "+sex.getMessage());
       }
   }
   
	/**
	 * Stop timers.
	 */
	public void stopTimers(){
		 for (Timer timer : timerService.getTimers()) {
			 log.info("Stop HASingleton timer: " + timer.getInfo());
	            timer.cancel();
	     }
		 interfaceSchedulerService.stopTimers();
	}
	
	/**
	 * Retry execute process.
	 *
	 * @param processSchedule the process schedule
	 */
	private void retryExecuteProcess(ProcessSchedule processSchedule){
	   String modulename = processSchedule.getBusinessProcess().getModuleName();
       Map<String, String> parameters = new HashMap<String, String>();
       parameters.put(GeneralConstants.PARAMETER_SCHEDULE_ID, processSchedule.getIdProcessSchedulePk().toString());
       //Call remote batch
       Schedulebatch scheduleBatch = new Schedulebatch();
       scheduleBatch.setIdProcessSchedule(processSchedule.getIdProcessSchedulePk());
       scheduleBatch.setParameters(parameters);
       scheduleBatch.setIdBusinessProcess(processSchedule.getBusinessProcess().getIdBusinessProcessPk());
       if (Validations.validateIsNotNullAndNotEmpty(processSchedule.getRegistryUser())){
    	   scheduleBatch.setUserName(processSchedule.getRegistryUser());
       }else{
    	   scheduleBatch.setUserName(userAdmin);
       }
       scheduleBatch.setPrivilegeCode(BooleanType.YES.getCode()); 
       log.info("retry remote batch in case error process using ProcessSchedulePk: "+processSchedule.getIdProcessSchedulePk());
       clientRestService.excuteRemoteBatchProcess(scheduleBatch,modulename);
	}
   
   /**
    * Excute batch remote.
    *
    * @param timer the timer
    */
   @Timeout
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
   public void excuteBatchRemote(Timer timer){
       log.info("Running timer using ProcessSchedulePk: "+timer.getInfo() + " next execution in hour "+timer.getNextTimeout());
       //get scheduler
       Long idScheduler = (Long)timer.getInfo();
       ProcessSchedule procScheduler = em.find(ProcessSchedule.class, Long.valueOf(idScheduler));
       String modulename = procScheduler.getBusinessProcess().getModuleName();
       Map<String, String> parameters = new HashMap<String, String>();
       parameters.put(GeneralConstants.PARAMETER_SCHEDULE_ID, idScheduler.toString());
       //Call remote batch
       Schedulebatch scheduleBatch = new Schedulebatch();
       scheduleBatch.setIdProcessSchedule(idScheduler);
       scheduleBatch.setParameters(parameters);
       scheduleBatch.setIdBusinessProcess(procScheduler.getBusinessProcess().getIdBusinessProcessPk());
//       if (Validations.validateIsNotNullAndNotEmpty(procScheduler.getRegistryUser())){
//    	   scheduleBatch.setUserName(procScheduler.getRegistryUser());
//       }else{
//    	   scheduleBatch.setUserName(userAdmin);
//       }
       scheduleBatch.setUserName(userAdmin);
       scheduleBatch.setPrivilegeCode(BooleanType.YES.getCode()); 
       clientRestService.excuteRemoteBatchProcess(scheduleBatch,modulename);
       log.info("finish register remote batch using ProcessSchedulePk: "+idScheduler);
   }
   
    /**
     * Register scheduler.
     *
     * @param processSchedule the process schedule
     * @throws ServiceException the service exception
     */
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void registerScheduler(ProcessSchedule processSchedule) throws ServiceException{
        //Register in DB
        create(processSchedule);
        //update application server
        updateScheduleTimer(processSchedule);
    }
    
    /**
     * Update scheduler.
     *
     * @param processSchedule the process schedule
     * @return the process schedule
     * @throws ServiceException the service exception
     */
   public ProcessSchedule updateScheduleTimer(ProcessSchedule processSchedule) throws ServiceException{
	   processSchedule.getScheduleInfo().setIdProcessSchedulePk(processSchedule.getIdProcessSchedulePk());
       Timer newTimer = updateScheduleTimer(processSchedule.getScheduleInfo());
       processSchedule.setNextTimeOut(newTimer.getNextTimeout());
       return processSchedule;
   }
   
   /**
    * Update schedule timer.
    *
    * @param schedulerInfo the scheduler info
    * @return the timer
    */
   public Timer updateScheduleTimer(SchedulerInfo schedulerInfo){
	   //delete old timer
	   deleteTimer(schedulerInfo.getIdProcessSchedulePk());
	   //register timer
	   return registerTimer(schedulerInfo.getIdProcessSchedulePk(), schedulerInfo);
   }
   
   /**
    * Update timer remote.
    *
    * @param schedulerInfo the scheduler info
    * @throws ServiceException the service exception
    */
   @POST
   @Path("/update")
   @Produces({MediaType.APPLICATION_JSON})
   public void updateTimerRemote(SchedulerInfo schedulerInfo) throws ServiceException{
	    updateScheduleTimer(schedulerInfo);
   }
   
   /**
    * Enable schedule.
    *
    * @param processSchedule the process schedule
    * @return the process schedule
    * @throws ServiceException the service exception
    */
   public ProcessSchedule enableSchedule(ProcessSchedule processSchedule) throws ServiceException{
	 //update entity
	 processSchedule.setScheduleState(ProcessScheduleStateType.ACTIVE.getCode());
	 processSchedule = update(processSchedule);
	 
	 updateScheduleTimer(processSchedule);
     return processSchedule;
   } 
   
   /**
    * Disable schedule.
    *
    * @param processSchedule the process schedule
    * @return the process schedule
    * @throws ServiceException the service exception
    */
   public ProcessSchedule disableSchedule(ProcessSchedule processSchedule) throws ServiceException{
		 //update entity
		 processSchedule.setScheduleState(ProcessScheduleStateType.DEACTIVE.getCode());
		 processSchedule = update(processSchedule);
		 //delete old timer
	     deleteTimer(processSchedule.getIdProcessSchedulePk());
	     return processSchedule;
	   }
   
   /**
    * Gets the all scheduler.
    *
    * @return the all scheduler
    * @throws ServiceException the service exception
    */
   @GET
   @Produces({MediaType.APPLICATION_JSON})
   public List<ProcessSchedule> getAllScheduler() throws ServiceException{
       TypedQuery<ProcessSchedule> query = em.createNamedQuery(ProcessSchedule.PROCESSSCHEDULE, ProcessSchedule.class);
       query.setParameter("activeState", ProcessScheduleStateType.ACTIVE.getCode());
       List<ProcessSchedule> listSchedule = query.getResultList();
       List<ProcessSchedule> schedulerTransfer = new ArrayList<>();
       for(ProcessSchedule schedule :listSchedule){
    	   ProcessSchedule scheduleInstance = convertProxyToObject(schedule);
           Timer timer = getTimer(schedule.getIdProcessSchedulePk());
           if(timer != null){
        	   scheduleInstance.setNextTimeOut(timer.getNextTimeout());
           }
           schedulerTransfer.add(scheduleInstance);
       }
       return schedulerTransfer;
   }
   
   /**
    * Gets the process scheduler.
    *
    * @param businessProcess the business process
    * @return the process scheduler
    * @throws ServiceException the service exception
    */
   @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
   @GET
   @Path("/process/{businessProcess}")
   @Produces({MediaType.APPLICATION_JSON})
   public List<ProcessSchedule> getProcessScheduler(@PathParam("businessProcess") Long businessProcess) throws ServiceException{
	   StringBuilder builder = new StringBuilder();
		builder.append("select ps ");
		builder.append("from ProcessSchedule ps join fetch ps.businessProcess bp ");
		builder.append("where bp.processType =:processType ");
		builder.append("and bp.idBusinessProcessPk = :id ");
		TypedQuery<ProcessSchedule> queryString = em.createQuery(builder.toString(),ProcessSchedule.class);
		queryString.setParameter("id", businessProcess);
		queryString.setParameter("processType", ProcessType.BATCH.getCode());
		List<ProcessSchedule> listSchedules = queryString.getResultList();
		List<ProcessSchedule> schedulerTransfer = new ArrayList<>();
		for(ProcessSchedule schedule :listSchedules){
			ProcessSchedule scheduleInstance = convertProxyToObject(schedule);
			scheduleInstance.setBlExist(true);
			/*schedule.getScheduleInfo().setTime(new Date());
			schedule.getScheduleInfo().getTime().setHours(new Integer(schedule.getScheduleInfo().getScheduleHour()).intValue());
			schedule.getScheduleInfo().getTime().setMinutes(new Integer(schedule.getScheduleInfo().getScheduleMinute()).intValue());
			schedule.getScheduleInfo().getTime().setSeconds(new Integer(schedule.getScheduleInfo().getScheduleSecond()).intValue());*/
	        Timer timer = getTimer(scheduleInstance.getIdProcessSchedulePk());
	        if(timer != null){
	        	scheduleInstance.setNextTimeOut(timer.getNextTimeout());
	        }
	        schedulerTransfer.add(scheduleInstance);
	       }
		
		return schedulerTransfer;
   }
   
   /**
    * Convert proxy to object.
    *
    * @param processSchedule the process schedule
    * @return the process schedule
    */
   private ProcessSchedule convertProxyToObject(ProcessSchedule processSchedule){
	   ProcessSchedule realObject = new ProcessSchedule();
	   realObject.setIdProcessSchedulePk(processSchedule.getIdProcessSchedulePk());
	   realObject.setRegistryDate(processSchedule.getRegistryDate());
	   realObject.setRegistryUser(processSchedule.getRegistryUser());
	   realObject.setScheduleDescription(processSchedule.getScheduleDescription());
	   realObject.setScheduleExecution(processSchedule.getScheduleExecution());
	   realObject.setScheduleState(processSchedule.getScheduleState());
	   realObject.setScheduleInfo(processSchedule.getScheduleInfo());
	   realObject.setVersion(processSchedule.getVersion());
	   realObject.setLastModifyApp(processSchedule.getLastModifyApp());
	   realObject.setLastModifyDate(processSchedule.getLastModifyDate());
	   realObject.setLastModifyIp(processSchedule.getLastModifyIp());
	   realObject.setLastModifyUser(processSchedule.getLastModifyUser());
	   BusinessProcess businessProcess = new BusinessProcess();
	   businessProcess.setIdBusinessProcessPk(processSchedule.getBusinessProcess().getIdBusinessProcessPk());
	   businessProcess.setDescription(processSchedule.getBusinessProcess().getDescription());
	   businessProcess.setVersion(processSchedule.getBusinessProcess().getVersion());
	   businessProcess.setModuleName(processSchedule.getBusinessProcess().getModuleName());
	   businessProcess.setProcessName(processSchedule.getBusinessProcess().getProcessName());
	   realObject.setBusinessProcess(businessProcess);
	   return realObject;
   }
   
   /**
    * Register timer.
    *
    * @param idProcessSchedule the id process schedule
    * @param schedulerInfo the scheduler info
    * @return the timer
    */
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
   public Timer registerTimer(Long idProcessSchedule, SchedulerInfo schedulerInfo){
       boolean persistence = BooleanType.YES.getCode().equals(schedulerInfo.getIndPersistence());
        TimerConfig timerConfig = new TimerConfig(idProcessSchedule,persistence);
        ScheduleExpression schedExp = new ScheduleExpression();
        if(schedulerInfo.getStartDate()!= null){
           schedExp.start(schedulerInfo.getStartDate());
        } 
        if(schedulerInfo.getEndDate() != null){        	
            schedExp.end(CommonsUtilities.putTimeToDate(schedulerInfo.getEndDate(), 23, 59, 59, 999));
        }
        schedExp.second(schedulerInfo.getScheduleSecond());
        schedExp.minute(schedulerInfo.getScheduleMinute());
        schedExp.hour(schedulerInfo.getScheduleHour());
        schedExp.dayOfMonth(schedulerInfo.getScheduleDayOfMonth());
        schedExp.month(schedulerInfo.getScheduleMonth());
        schedExp.year(schedulerInfo.getScheduleYear());
        schedExp.dayOfWeek(schedulerInfo.getScheduleDayOfWeek());
        Timer newTimer = timerService.createCalendarTimer(schedExp, timerConfig);
        log.info("New timer created using ProcessSchedulePk: " + newTimer.getInfo()+" with Scheduler expr: "+schedExp.toString());
        return newTimer;
   }
   
   /**
    * Register timer remote.
    *
    * @param schedulerInfo the scheduler info
    */
   @Path("/registerTimer")
   @POST
   @Consumes({MediaType.APPLICATION_JSON})
   public void registerTimerRemote(SchedulerInfo schedulerInfo){
	   registerTimer(schedulerInfo.getIdProcessSchedulePk(), schedulerInfo);
   }
   
   /**
    * Update process schedule execution.
    *
    * @param processSchedule the process schedule
    * @param execution the execution
    */
   @POST
   @Path("/execution/{scheduler}/{action}")
   public void updateProcessScheduleExecution(@PathParam("scheduler")Long processSchedule,@PathParam("action")Integer execution){
	   ProcessSchedule schedule = em.find(ProcessSchedule.class, processSchedule);
	   schedule.setScheduleExecution(execution);
   }
   
   /**
    * Delete timer remote.
    *
    * @param idTimer the id timer
    */
   @Path("/deleteTimer/{timer}")
   @POST
   @Consumes({MediaType.APPLICATION_JSON})
   public void deleteTimerRemote(@PathParam("timer") Long idTimer){
	   deleteTimer(idTimer);
   }
   
   /**
    * Gets the timer.
    *
    * @param idTimer the id timer
    * @return the timer
    */
   private Timer getTimer(Long idTimer){
       Collection<Timer> timers = timerService.getTimers();
       for (Timer t : timers){
           if(idTimer.equals(((Long)t.getInfo()))){
               return t;
           }
       }
       return null;
   }
   
   /**
    * Delete timer.
    *
    * @param idTimer the id timer
    */
   public void deleteTimer(Long idTimer){
       Timer timer = getTimer(idTimer);
       if(timer != null){
    	   log.info("Deleting timer using ProcessSchedulePk: "+ timer.getInfo());
           timer.cancel();
       }
   }
   
   

}
