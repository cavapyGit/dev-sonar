package com.pradera.generalparameters.schedulerprocess.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessSchedule;
import com.pradera.model.process.SchedulerInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SchedulerAcessServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 21-may-2015
 */
@Stateless
public class SchedulerAcessServiceBean {

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;

	/**
	 * Default constructor.
	 */
	public SchedulerAcessServiceBean() {
		// TODO Auto-generated constructor stub
	}

	// /**
	// * Gets the singleton.
	// *
	// * @return the singleton
	// */
	// private SchedulerServiceBean getSingleton() {
	// return
	// (SchedulerServiceBean)CurrentServiceContainer.getServiceContainer().getService(
	// TimerSchedulerService.SINGLETON_SERVICE_NAME).getValue();
	// }

	/**
	 * Register scheduler.
	 *
	 * @param processSchedule            the process schedule
	 * @param schedulerInfo the scheduler info
	 * @throws ServiceException             the service exception
	 */
	public void registerTimer(Long processSchedule, SchedulerInfo schedulerInfo)
			throws ServiceException {
		schedulerInfo.setIdProcessSchedulePk(processSchedule);
		// Creates the json object which will manage the information received
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
			public JsonElement serialize(Date date, Type typeOfT,
					JsonSerializationContext context) throws JsonParseException {
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				return new JsonPrimitive(cal.getTimeInMillis());
			}
		});
		Gson gson = builder.create();
		String input = gson.toJson(schedulerInfo);
		clientRestService.registerTimer(input);
	}

	/**
	 * Update schedule timer.
	 *
	 * @param processSchedule            the process schedule
	 * @return the process schedule
	 * @throws ServiceException             the service exception
	 */
	public void deleteTimer(Long processSchedule) throws ServiceException {
		clientRestService.deleteTimer(processSchedule);
	}

	/**
	 * Gets the all scheduler.
	 * 
	 * @return the all scheduler
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<ProcessSchedule> getAllScheduler() throws ServiceException {
		List<ProcessSchedule> schedulers = new ArrayList<>();
		String result = clientRestService.getAllScheduler();
		if (StringUtils.isNotBlank(result)) {
			Type listOfObject = new TypeToken<List<ProcessSchedule>>() {
			}.getType();
			// Creates the json object which will manage the information
			// received
			GsonBuilder builder = new GsonBuilder();
			// Register an adapter to manage the date types as long values
			builder.registerTypeAdapter(Date.class,
					new JsonDeserializer<Date>() {
						public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context)
								throws JsonParseException {
							return new Date(json.getAsJsonPrimitive()
									.getAsLong());
						}
					});
			Gson gson = builder.create();
			schedulers = gson.fromJson(result, listOfObject);
		}
		return schedulers;
	}

	/**
	 * Gets the process scheduler.
	 * 
	 * @param businessProcess
	 *            the business process
	 * @return the process scheduler
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<ProcessSchedule> getProcessScheduler(Long businessProcess) throws ServiceException {
		List<ProcessSchedule> schedulers = new ArrayList<>();
		String result = clientRestService.getProcessScheduler(businessProcess);
		if (StringUtils.isNotBlank(result)) {
			Type listOfObject = new TypeToken<List<ProcessSchedule>>() {
			}.getType();
			// Creates the json object which will manage the information
			// received
			GsonBuilder builder = new GsonBuilder();
			// Register an adapter to manage the date types as long values
			builder.registerTypeAdapter(Date.class,
					new JsonDeserializer<Date>() {
						public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context)
								throws JsonParseException {
							return new Date(json.getAsJsonPrimitive()
									.getAsLong());
						}
					});
			Gson gson = builder.create();
			schedulers = gson.fromJson(result, listOfObject);
		}
		return schedulers;
	}

}
