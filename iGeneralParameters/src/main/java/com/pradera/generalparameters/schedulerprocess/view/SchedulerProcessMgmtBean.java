/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.generalparameters.schedulerprocess.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.generalparameters.schedulerprocess.facade.SchedulerServiceFacade;
import com.pradera.model.process.ProcessSchedule;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SchedulerProcessMgmtBean.
 *
 * @author raul
 */

@DepositaryWebBean
public class SchedulerProcessMgmtBean extends GenericBaseBean implements  Serializable{
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The log. */
    @Inject
    transient PraderaLogger log;
    
    /** The user info. */
    @Inject
    UserInfo userInfo;
    
    /** The scheduler facade. */
    @EJB
    SchedulerServiceFacade schedulerFacade;
    
    /** The new schedule. */
    private ProcessSchedule newSchedule;
    
    /** The selected schedule. */
    private ProcessSchedule selectedSchedule;
    
    /** The list schedules. */
    private List<ProcessSchedule> listSchedules;
    
    /**
     * Init.
     */
    @PostConstruct
    public void init(){
        //listSchedules = 
    }
    
    
    /**
     * New scheduler.
     *
     * @return the string
     */
    public String newScheduler(){
        newSchedule = new ProcessSchedule();
        return "schedulerProcessMgmt";
    }
    
    /**
     * Search schedulers.
     */
    public void searchSchedulers(){
        
    }
    
    /**
     * Edit scheduler.
     *
     * @return the string
     */
    public String editScheduler() {
        return "schedulerProcessMgmt";
    }
    
    /**
     * Create scheduler.
     *
     * @return the string
     */
    public String createScheduler(){
        return "schedulerSearch";
    }
    
    /**
     * Update scheduler.
     *
     * @return the string
     */
    public String updateScheduler() {
        return "schedulerSearch";
    }
    
    /**
     * Delete scheduler.
     *
     * @return the string
     */
    public String deleteScheduler() {
        return "schedulerSearch";
    }
    
    /**
     * Stop scheduler.
     *
     * @return the string
     */
    public String stopScheduler() {
        return "schedulerSearch";
    }
    
    /**
     * Start scheduler.
     *
     * @return the string
     */
    public String startScheduler() {
        return "schedulerSearch";
    }
	
	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}
}
