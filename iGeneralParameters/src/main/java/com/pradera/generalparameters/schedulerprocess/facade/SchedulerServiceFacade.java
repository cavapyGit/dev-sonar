package com.pradera.generalparameters.schedulerprocess.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.generalparameters.schedulerprocess.service.SchedulerAcessServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessSchedule;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SchedulerServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/06/2013
 */
@Interceptors(ContextHolderInterceptor.class)
@Stateless
public class SchedulerServiceFacade {

   /** The scheduler service. */
   @EJB
   SchedulerAcessServiceBean schedulerService;
   
   /** The transaction registry. */
   @Resource
   TransactionSynchronizationRegistry transactionRegistry;
   
//   /**
//    * Register scheduler.
//    *
//    * @param processSchedule the process schedule
//    * @throws ServiceException the service exception
//    */
//   @TransactionAttribute(TransactionAttributeType.REQUIRED)
//   public void registerScheduler(ProcessSchedule processSchedule) throws ServiceException{
//	   LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//	   loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
//       schedulerService.registerScheduler(processSchedule,loggerUser);
//   }
   
   /**
    * Gets the all scheduler.
    *
    * @return the all scheduler
    * @throws ServiceException the service exception
    */
   public List<ProcessSchedule> getAllScheduler() throws ServiceException{
       return schedulerService.getAllScheduler();
   }
   
   /**
    * Update scheduler.
    *
    * @param businessProcess the business process
    * @return the process scheduler
    * @throws ServiceException the service exception
    */
//   @TransactionAttribute(TransactionAttributeType.REQUIRED)
//   public ProcessSchedule updateScheduler(ProcessSchedule processSchedule) throws ServiceException{
//	   LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//	   loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
//       schedulerService.updateScheduleTimer(processSchedule,loggerUser);
//       return processSchedule;
//   }
   
   /**
    * Gets the process scheduler.
    *
    * @param businessProcess the business process
    * @return the process scheduler
    * @throws ServiceException the service exception
    */
   public List<ProcessSchedule> getProcessScheduler(Long businessProcess) throws ServiceException{
	   return schedulerService.getProcessScheduler(businessProcess);
   }
}
