package com.pradera.generalparameters.geographiclocations.view.filter;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class GeographicLocationFilter.
 *
 * @author PraderaTechnologies
 */
public class GeographicLocationFilter implements Serializable{

	/** The LocationCode. */
	private String LocationCode;
	
	/** The LocationName. */
	private String LocationName;
	
	/** The geo location type. */
	private Integer geoLocationType;

	/** The country code. */
	private Integer countryCode;
	
	/** The province code. */
	private Integer provinceCode;
	
	/** The department code. */
	private Integer departmentCode;
	
	/**
	 * Gets the location code.
	 *
	 * @return String
	 */
	public String getLocationCode() {
		return LocationCode;
	}

	/**
	 * Sets the location code.
	 *
	 * @param locationCode the new location code
	 */
	public void setLocationCode(String locationCode) {
		if(locationCode!=null){
			locationCode=locationCode.toUpperCase();
		}
		LocationCode = locationCode;
	}

	/**
	 * Gets the location name.
	 *
	 * @return String
	 */
	public String getLocationName() {
		return LocationName;
	}

	/**
	 * Sets the location name.
	 *
	 * @param locationName the new location name
	 */
	public void setLocationName(String locationName) {
		if(locationName!=null){
			locationName = locationName.toUpperCase();
		}
		LocationName = locationName;
	}

	/**
	 * Gets the geo location type.
	 *
	 * @return the geoLocationType
	 */
	public Integer getGeoLocationType() {
		return geoLocationType;
	}

	/**
	 * Sets the geo location type.
	 *
	 * @param geoLocationType the geoLocationType to set
	 */
	public void setGeoLocationType(Integer geoLocationType) {
		this.geoLocationType = geoLocationType;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the countryCode
	 */
	public Integer getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the country code.
	 *
	 * @param countryCode the new country code
	 */
	public void setCountryCode(Integer countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Gets the province code.
	 *
	 * @return the provinceCode
	 */
	public Integer getProvinceCode() {
		return provinceCode;
	}

	/**
	 * Sets the province code.
	 *
	 * @param provinceCode the new province code
	 */
	public void setProvinceCode(Integer provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * Gets the department code.
	 *
	 * @return the department code
	 */
	public Integer getDepartmentCode() {
		return departmentCode;
	}

	/**
	 * Sets the department code.
	 *
	 * @param departmentCode the new department code
	 */
	public void setDepartmentCode(Integer departmentCode) {
		this.departmentCode = departmentCode;
	}		
	
}
