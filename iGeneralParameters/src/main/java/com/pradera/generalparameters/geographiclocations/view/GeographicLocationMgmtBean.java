package com.pradera.generalparameters.geographiclocations.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.generalparameters.geographiclocations.facade.GeographicLocationServiceFacade;
import com.pradera.generalparameters.geographiclocations.view.filter.GeographicLocationFilter;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * The Class GeographicLocationMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/10/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class GeographicLocationMgmtBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;

	/** The GeographicLocationServiceFacade. */
	@EJB
	private GeographicLocationServiceFacade geographicLocationServiceFacade;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade;

	/** The GeographicLocation. */
	private GeographicLocation geographicLocation;

	/** The GeographicLocationType. */
	private List<GeographicLocationType> typeGeographicLocation;

	/** The CountryList. */
	private List<GeographicLocation> countryList;

	/** The DepartmentList. */
	private List<GeographicLocation> departmentList;

	/** The ProvinceList. */
	private List<GeographicLocation> provinceList;

	/** The SelectedCountry. */
	private Integer selectedCountry;

	/** The SelectedDepartment. */
	private Integer selectedDepartment;

	/** The SelectedProvince. */
	private Integer selectedProvince;

	/** The SelectedLocalityType. */
	private Integer selectedLocalityType;

	/** The GeographicLocationFilter. */
	private GeographicLocationFilter geographicLocationFilter;

	/** The GeographicLocationDataModel. */
	private GeographicLocationDataModel geographicLocationDataModel;

	/** The SelectedGeographicLocation. */
	private GeographicLocation selectedGeographicLocation;	
	
	/** The local country. */
	private Integer localCountry;
	
	/** The CountryList. */
	private List<GeographicLocation> countrySearchList;

	/** The ProvinceList. */
	private List<GeographicLocation> provinceSearchList;
	
	/** The department search list. */
	private List<GeographicLocation> departmentSearchList;
	
	/** The user info. */
	@Inject
	protected UserInfo userInfo;
	
	/** The bl view. */
	private boolean blModify, blView;
	
	/** The lst local. */
	private List<ParameterTable> lstLocal;
	
	/** The Constant STR_VIEW_MAPPING. */
	private static final String STR_VIEW_MAPPING = "geographicLocationMgmtRule";
	
	/** The Constant STR_SEARCH_MAPPING. */
	private static final String STR_SEARCH_MAPPING = "backToGeoLoca";
	/**
	 * The Construtor.
	 */
	public GeographicLocationMgmtBean() {

	}

	/**
	 * To perform initialization.
	 */
	@PostConstruct
	public void init() {
		geographicLocation = new GeographicLocation();
		geographicLocationFilter = new GeographicLocationFilter();
		showPrivilegeButtons();
	}

	/**
	 * To get the countryList on change of geographicLocationtype.
	 */
	public void geoLocationChangeListener() {
		JSFUtilities.hideGeneralDialogues();
		try {			
			countryList = null;
			departmentList = null;
			provinceList = null;
			selectedCountry = null;
			selectedDepartment = null;
			selectedProvince = null;
			localCountry = BooleanType.NO.getCode();
			geographicLocation = new GeographicLocation();
			if (Validations.validateIsNotNullAndNotEmpty(selectedLocalityType)
					&& !GeographicLocationType.COUNTRY.getCode().equals(selectedLocalityType)) {
				countryList = geographicLocationServiceFacade.getLocationsServiceFacade(GeographicLocationType.COUNTRY.getCode(), null);

				selectedCountry = countryResidence;
				
									
				if(GeographicLocationType.PROVINCE.getCode().equals(selectedLocalityType) || GeographicLocationType.DISTRICT.getCode().equals(selectedLocalityType)) {
					 onCountryChangeListener();
				}
									
			}
			geographicLocation.setTypeGeographicLocation(selectedLocalityType);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * on Country Change Listener.
	 */
	public void onCountryChangeListener() {
		JSFUtilities.hideGeneralDialogues();
		try {
			departmentList = null;
			provinceList = null;
			selectedDepartment = null;
			selectedProvince = null;
			geographicLocation = new GeographicLocation();
			if(Validations.validateIsNotNullAndNotEmpty(selectedCountry)){							
				
				if(GeographicLocationType.PROVINCE.getCode().equals(selectedLocalityType) || GeographicLocationType.DISTRICT.getCode().equals(selectedLocalityType)) {
					departmentList = geographicLocationServiceFacade.getLocationsServiceFacade(GeographicLocationType.DEPARTMENT.getCode(), selectedCountry);
				}
				
			}
			geographicLocation.setTypeGeographicLocation(selectedLocalityType);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * on Department Change Listener.
	 */
	public void onDepartmentChangeListener() {
		JSFUtilities.hideGeneralDialogues();
		try {
			provinceList = null;
			selectedProvince = null;
			geographicLocation = new GeographicLocation();
			if (GeographicLocationType.DISTRICT.getCode().equals(selectedLocalityType)) {
				provinceList = geographicLocationServiceFacade
						.getLocationsServiceFacade(
								GeographicLocationType.PROVINCE.getCode(),
								selectedDepartment);
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * To register the GeographicLocation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void registerGeographicLocation() throws ServiceException{
		GeographicLocation geoLocation = new GeographicLocation();
		if (GeographicLocationType.DEPARTMENT.getCode().equals(selectedLocalityType)) {
			geoLocation.setIdGeographicLocationPk(selectedCountry);
			geographicLocation.setGeographicLocation(geoLocation);
		}
		
		if (GeographicLocationType.PROVINCE.getCode().equals(selectedLocalityType)) {
			geoLocation.setIdGeographicLocationPk(selectedDepartment);
			geographicLocation.setGeographicLocation(geoLocation);
		}
		// To set the location reference to selected Province if the
		// locationType is DISTRICT
		if (GeographicLocationType.DISTRICT.getCode().equals(selectedLocalityType)){
			geoLocation.setIdGeographicLocationPk(selectedProvince);
			geographicLocation.setGeographicLocation(geoLocation);
		}
		geographicLocation.setTypeGeographicLocation(selectedLocalityType);
		geographicLocation.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocation.setLocalCountry(localCountry);		
		geographicLocation.setRegistryDate(CommonsUtilities.currentDateTime());
		geographicLocation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		geographicLocationServiceFacade.registerLocationServiceFacade(geographicLocation);
		Object[] argObj = new Object[2];
		argObj[0] = geographicLocation.getGeographiclocationTypeDescription();
		argObj[1] = geographicLocation.getName();
		//To send Notification Message
		BusinessProcess businessProc = new BusinessProcess();
		businessProc.setIdBusinessProcessPk(BusinessProcessType.GEOGRAFIC_LOCATION_REGISTRATION.getCode());
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null, argObj);
		cleanGeographicLocation();
	}

	
	/**
	 * Modify geographic location.
	 *
	 * @throws ServiceException the service exception
	 */
	private void modifyGeographicLocation() throws ServiceException{
		geographicLocationServiceFacade.updateGeographicLocationServiceFacade(geographicLocation);
		Object[] argObj = new Object[2];
		argObj[0] = GeographicLocationType.get(selectedLocalityType).getValue();
		argObj[1] = geographicLocation.getName();
		//To send Notification Message
		BusinessProcess businessProc = new BusinessProcess();
		businessProc.setIdBusinessProcessPk(BusinessProcessType.GEOGRAFIC_LOCATION_MODIFICATION.getCode());
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null, argObj);
		cleanGeographicLocation();
	}

	/**
	 * Before save functionality.
	 */
	public void beforeAction() {
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNotNull(geographicLocation.getName()) && Validations.validateIsNotNull(selectedLocalityType) && ((selectedLocalityType==1) || (selectedLocalityType==2 && Validations.validateIsNotNull(selectedCountry)) || (selectedLocalityType==3 && Validations.validateIsNotNull(selectedCountry) && Validations.validateIsNotNull(selectedDepartment)) || (selectedLocalityType==4 && Validations.validateIsNotNull(selectedCountry) && Validations.validateIsNotNull(selectedDepartment) && Validations.validateIsNotNull(selectedProvince)))) {
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
			if(!blModify)
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage("alt.geolocation.askRegister"));				
			else
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
						, PropertiesUtilities.getMessage("alt.geolocation.askModify"));
		} else {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED, null));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	
	/**
	 * Gets the gender message from location type.
	 *
	 * @param geographicLocationType the geographic location type
	 * @return the gender message from location type
	 */
	private String getGenderMessageFromLocationType(Integer geographicLocationType){
		if(GeographicLocationType.PROVINCE.getCode().equals(geographicLocationType)) {
			return PropertiesUtilities.getMessage(PropertiesConstants.GENDER_MESSAGE_FEMALE);
		} else {
			return PropertiesUtilities.getMessage(PropertiesConstants.GENDER_MESSAGE_MALE);
		}
	}

	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		JSFUtilities.hideGeneralDialogues();
		try {
			Object[] argObj = new Object[3];			
			argObj[1] = geographicLocation.getName();
			argObj[2] =  getGenderMessageFromLocationType(selectedLocalityType);
			if(!blModify){
				argObj[0] = GeographicLocationType.get(selectedLocalityType).getValue();
				registerGeographicLocation();				
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.GEOGRAPHICLOCATION_REGISTER_SUCCESS, argObj));
			}else{
				argObj[0] = GeographicLocationType.get(selectedLocalityType).getValue();
				modifyGeographicLocation();
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.GEOGRAPHICLOCATION_MODIFY_SUCCESS, argObj));
			}
			
			cleanManagement();
			
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}
	
	/**
	 * Clean functionality.
	 */
	public void cleanGeographicLocation() {
		JSFUtilities.hideGeneralDialogues();
		geographicLocation = new GeographicLocation();
		countryList = null;
		departmentList = null;
		provinceList = null;
		selectedLocalityType = null;
		selectedCountry = null;
		selectedDepartment = null;
		selectedProvince = null;
	}

	/**
	 * To Register the GeographicLocation.
	 */

	public void loadRegisterGeographicLocation() {
		JSFUtilities.hideGeneralDialogues();
		cleanGeographicLocation();
		lstLocal = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(BooleanType.YES.getCode());
		paramTab.setParameterName(BooleanType.YES.getValue());		
		lstLocal.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(BooleanType.NO.getCode());
		paramTab.setParameterName(BooleanType.NO.getValue());		
		lstLocal.add(paramTab);
		localCountry = BooleanType.NO.getCode();
		blModify = false;
		blView = false;
	}

	/**
	 * Ask clean.
	 */
	public void askClean(){
	JSFUtilities.resetViewRoot();
		if(validateRequiredFields()){
			loadRegisterGeographicLocation();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.clean"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwCleanDialog').show();");
		}
	}
	
	
	/**
	 * Search GeographicLocation Listener.
	 */
	@LoggerAuditWeb
	public void searchGeographicLocationListener() {
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNotNull(geographicLocationFilter.getGeoLocationType()) && ((geographicLocationFilter.getGeoLocationType()==1) || (geographicLocationFilter.getGeoLocationType()==2 && Validations.validateIsNotNull(geographicLocationFilter.getCountryCode())) || (geographicLocationFilter.getGeoLocationType()==3 && Validations.validateIsNotNull(geographicLocationFilter.getCountryCode()) && Validations.validateIsNotNull(geographicLocationFilter.getDepartmentCode())) || (geographicLocationFilter.getGeoLocationType()==4 && Validations.validateIsNotNull(geographicLocationFilter.getCountryCode()) && Validations.validateIsNotNull(geographicLocationFilter.getDepartmentCode()) && Validations.validateIsNotNull(geographicLocationFilter.getProvinceCode())))) {
			try {
				searchGeographicLocation();
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		} else {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED, null));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}

	/**
	 * To Search GeographicLocations.
	 */	
	public void searchGeographicLocation() {
		try {
			geographicLocationDataModel = new GeographicLocationDataModel(geographicLocationServiceFacade
							.searchGeographicLocationServiceFacade(geographicLocationFilter));
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * To show the details of the selected location on detail page.
	 *
	 * @return string
	 */
	public String getSelectedDetails() {
		JSFUtilities.hideGeneralDialogues();
		try {
			geographicLocation = new GeographicLocation();
			geographicLocation = geographicLocationServiceFacade.getGeographicLocation(selectedGeographicLocation.getIdGeographicLocationPk());
			getParentLocations();
			blView = true;
			blModify = false;	
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		return STR_VIEW_MAPPING;
	}

	/**
	 * Ask back.
	 *
	 * @return the string
	 */
	public String askBack(){
		JSFUtilities.hideGeneralDialogues();		
		if(blView){
			geographicLocation = null;
			return STR_SEARCH_MAPPING;
		}				
		if(validateRequiredFields()){
			geographicLocation = null;
			selectedGeographicLocation = null;
			return STR_SEARCH_MAPPING;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.back"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwBackDialog').show();");
			return GeneralConstants.EMPTY_STRING;
		}		
	}
	
	/**
	 * Validate required fields.
	 *
	 * @return true, if successful
	 */
	public boolean validateRequiredFields(){
		if(Validations.validateIsNullOrEmpty(selectedLocalityType)
			&& Validations.validateIsNullOrEmpty(selectedCountry)
			&& Validations.validateIsNullOrEmpty(selectedProvince)
			&& Validations.validateIsNullOrEmpty(geographicLocation.getName())
			&& Validations.validateIsNullOrEmpty(geographicLocation.getCodeUbigueo()))
			return true;
		return false;
	}
	
	/**
	 * To modify the selected GeographicLocation on modification page.
	 *
	 * @return String
	 */
	public String modifyGeographicLocationListener() {
		JSFUtilities.hideGeneralDialogues();
		try {
			if(Validations.validateIsNotNullAndNotEmpty(selectedGeographicLocation)){
				geographicLocation = new GeographicLocation();
				geographicLocation = geographicLocationServiceFacade.getGeographicLocation(selectedGeographicLocation.getIdGeographicLocationPk());
				getParentLocations();
				blModify = true;
				blView = false;
				return STR_VIEW_MAPPING;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("alt.geolocation.not.selected"));
			JSFUtilities.showValidationDialog();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return GeneralConstants.EMPTY_STRING;
	}

	/**
	 * To Block the Registered Location.
	 *
	 * @return the parent locations
	 */
	@LoggerAuditWeb
	/*public void blockGeographicLocation() {
		try {
			hideDialogs();
			// To check whether it is already in blocked state
			if (selectedGeographicLocation.getState() != GeographicLocationStateType.BLOCKED
					.getCode()) {
				geographicLocation = selectedGeographicLocation;
				geographicLocation.setState(GeographicLocationStateType.BLOCKED
						.getCode());
//				geographicLocation.setLastModifyApp(1);
//				geographicLocation.setLastModifyDate(CommonsUtilities
//						.currentDate());
//				// geographicLocation.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
//				geographicLocation.setLastModifyIp("127.0.0.1");
//				// geographicLocation.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
//				geographicLocation.setLastModifyUser("");
				if (geographicLocationServiceFacade
						.updateGeographicLocationServiceFacade(geographicLocation)) {
					searchGeographicLocation();
					JSFUtilities
							.showComponent("frmConsultaMovTitulares:cnfGeoLocation");
					Object[] argObj = new Object[1];
					argObj[0] = geographicLocation.getName();
					showMessageOnDialog(
							null,
							null,
							PropertiesConstants.GEOGRAPHICLOCATION_BLOCKED_SUCCESS,
							argObj);
				}
			} else {
				JSFUtilities.showComponent("cnfEndTransaction");
				Object[] argObj = new Object[1];
				argObj[0] = selectedGeographicLocation.getName();
				showMessageOnDialog(null, null,
						PropertiesConstants.GEOGRAPHICLOCATION_BLOCKED_FAIL,
						argObj);
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}*/

	public void getParentLocations() {
		try {
			if (Validations.validateIsNotNullAndNotEmpty(geographicLocation.getGeographicLocation())) {
				selectedLocalityType = geographicLocation.getTypeGeographicLocation();
				GeographicLocation geoLocation = geographicLocation;
				for (int i = 1; i < geographicLocation.getTypeGeographicLocation(); i++) {

					// To check referenceGeographiclocation is null or not
					if (geoLocation.getGeographicLocation() != null) {
						geoLocation = geographicLocationServiceFacade.getParentGeographicLocationServiceFacade(geoLocation);
					}
					
					if (Validations.validateIsNullOrEmpty(geoLocation.getGeographicLocation())
							&&  GeographicLocationType.COUNTRY.getCode().equals(geoLocation.getTypeGeographicLocation())) {
							countryList = new ArrayList<GeographicLocation>();
							countryList.add(geoLocation);
							selectedCountry = geoLocation.getIdGeographicLocationPk();
					}
					if (GeographicLocationType.DEPARTMENT.getCode().equals(geoLocation.getTypeGeographicLocation())) {
						departmentList = new ArrayList<GeographicLocation>();
						departmentList.add(geoLocation);
						selectedDepartment = geoLocation.getIdGeographicLocationPk();
					}
					if (GeographicLocationType.PROVINCE.getCode().equals(geoLocation.getTypeGeographicLocation())) {
						provinceList = new ArrayList<GeographicLocation>();
						provinceList.add(geoLocation);
						selectedProvince = geoLocation.getIdGeographicLocationPk();
					}
					
				}
				if (GeographicLocationType.DEPARTMENT.getCode().equals(selectedLocalityType)) {
					departmentList = null;
					selectedDepartment = null;
					provinceList = null;
					selectedProvince = null;
				}
				if (GeographicLocationType.PROVINCE.getCode().equals(selectedLocalityType)) {
					provinceList = null;
					selectedProvince = null;
				}
			} else if (Validations.validateIsNullOrEmpty(geographicLocation.getGeographicLocation())
					&& GeographicLocationType.COUNTRY.getCode().equals(geographicLocation.getTypeGeographicLocation())) {
				lstLocal = new ArrayList<ParameterTable>();
				ParameterTable paramTab = new ParameterTable();
				paramTab.setParameterTablePk(BooleanType.YES.getCode());
				paramTab.setParameterName(BooleanType.YES.getValue());		
				lstLocal.add(paramTab);
				paramTab = new ParameterTable();
				paramTab.setParameterTablePk(BooleanType.NO.getCode());
				paramTab.setParameterName(BooleanType.NO.getValue());
				lstLocal.add(paramTab);
				localCountry = geographicLocation.getLocalCountry();
				selectedLocalityType = geographicLocation.getTypeGeographicLocation();
				countryList = null;
				selectedCountry = null;
				departmentList = null;
				selectedDepartment = null;
				provinceList = null;
				selectedProvince = null;				
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * To get the countryList on change of geographicLocationtype.
	 */
	public void geoLocationSearchChangeListener() {
		JSFUtilities.hideGeneralDialogues();
		try {			
			countrySearchList = null;
			departmentSearchList = null;
			provinceSearchList = null;
			geographicLocationFilter.setCountryCode(null);
			geographicLocationFilter.setDepartmentCode(null);
			geographicLocationFilter.setProvinceCode(null);
			geographicLocationFilter.setLocationName(null);
			geographicLocationFilter.setLocationCode(null);
			if (!GeographicLocationType.COUNTRY.getCode().equals(geographicLocationFilter.getGeoLocationType())) {
				countrySearchList = geographicLocationServiceFacade.getLocationsServiceFacade(GeographicLocationType.COUNTRY.getCode(), null);

				geographicLocationFilter.setCountryCode(countryResidence);
				
				 if(GeographicLocationType.PROVINCE.getCode().equals(geographicLocationFilter.getGeoLocationType()) || GeographicLocationType.DISTRICT.getCode().equals(geographicLocationFilter.getGeoLocationType())){
					 onCountrySearchChangeListener();
				}
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * on Country Change Listener.
	 */
	public void onCountrySearchChangeListener() {
		JSFUtilities.hideGeneralDialogues();
		try {	
			departmentSearchList = null;
			provinceSearchList = null;
			geographicLocationFilter.setDepartmentCode(null);
			geographicLocationFilter.setProvinceCode(null);
			geographicLocationFilter.setLocationName(null);
			geographicLocationFilter.setLocationCode(null);
			if (GeographicLocationType.PROVINCE.getCode().equals(geographicLocationFilter.getGeoLocationType()) || GeographicLocationType.DISTRICT.getCode().equals(geographicLocationFilter.getGeoLocationType())) {
				departmentSearchList = geographicLocationServiceFacade.getLocationsServiceFacade(GeographicLocationType.DEPARTMENT.getCode(), 
																			geographicLocationFilter.getCountryCode());
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * On department search change listener.
	 */
	public void onDepartmentSearchChangeListener() {
		JSFUtilities.hideGeneralDialogues();
		try {
			provinceSearchList = null;
			geographicLocationFilter.setProvinceCode(null);
			geographicLocationFilter.setLocationName(null);
			geographicLocationFilter.setLocationCode(null);
			if (GeographicLocationType.DISTRICT.getCode().equals(geographicLocationFilter.getGeoLocationType())) {
				provinceSearchList = geographicLocationServiceFacade.getLocationsServiceFacade(GeographicLocationType.PROVINCE.getCode(),geographicLocationFilter.getDepartmentCode());
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * To clear the search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		geographicLocationFilter = new GeographicLocationFilter();
		geographicLocationDataModel = null;
		countrySearchList = null;
		provinceSearchList = null;
		selectedGeographicLocation = null;
	}
	
	/**
	 * Clean management.
	 */
	public void cleanManagement(){		
		geographicLocationFilter = new GeographicLocationFilter();
		geographicLocationDataModel = null;
		countrySearchList = null;
		provinceSearchList = null;
		selectedGeographicLocation = null;
	}
	
	/**
	 * To validate LocationName.
	 */
	public void validateLocationName(){
		JSFUtilities.hideGeneralDialogues();
		boolean nameExists = false;
		try {
			nameExists = geographicLocationServiceFacade.validateNameGeographicLocationServiceFacade(geographicLocation);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		if(nameExists){
			JSFUtilities.showValidationDialog();
			geographicLocation.setName(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.GEOGRAPHICLOCATION_NAME_REGISTERED));
		}
	}
	
	
	/**
	 * Method for logout the user session on idle time of the application.
	 */
	public void sessionLogout(){
		try{
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession(); 
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(true);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Gets the geographic location.
	 *
	 * @return GeographicLocation
	 */
	public GeographicLocation getGeographicLocation() {
		return geographicLocation;
	}

	/**
	 * Sets the geographic location.
	 *
	 * @param geographicLocation the new geographic location
	 */
	public void setGeographicLocation(GeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}

	/**
	 * Gets the country list.
	 *
	 * @return countrylist
	 */
	public List<GeographicLocation> getCountryList() {
		return countryList;
	}

	/**
	 * Sets the country list.
	 *
	 * @param countryList the new country list
	 */
	public void setCountryList(List<GeographicLocation> countryList) {
		this.countryList = countryList;
	}

	/**
	 * Gets the department list.
	 *
	 * @return departmentlist
	 */
	public List<GeographicLocation> getDepartmentList() {
		return departmentList;
	}

	/**
	 * Sets the department list.
	 *
	 * @param departmentList the new department list
	 */
	public void setDepartmentList(List<GeographicLocation> departmentList) {
		this.departmentList = departmentList;
	}

	/**
	 * Gets the province list.
	 *
	 * @return provinceList
	 */
	public List<GeographicLocation> getProvinceList() {
		return provinceList;
	}

	/**
	 * Sets the province list.
	 *
	 * @param provinceList the new province list
	 */
	public void setProvinceList(List<GeographicLocation> provinceList) {
		this.provinceList = provinceList;
	}

	/**
	 * Gets the type geographic location.
	 *
	 * @return typeGeographicLocation
	 */
	public List<GeographicLocationType> getTypeGeographicLocation() {
		typeGeographicLocation = GeographicLocationType.list;
		return typeGeographicLocation;
	}

	/**
	 * Sets the type geographic location.
	 *
	 * @param typeGeographicLocation the new type geographic location
	 */
	public void setTypeGeographicLocation(
			List<GeographicLocationType> typeGeographicLocation) {
		this.typeGeographicLocation = typeGeographicLocation;
	}

	/**
	 * Gets the selected country.
	 *
	 * @return selectedCountry
	 */
	public Integer getSelectedCountry() {
		return selectedCountry;
	}

	/**
	 * Sets the selected country.
	 *
	 * @param selectedCountry the new selected country
	 */
	public void setSelectedCountry(Integer selectedCountry) {
		this.selectedCountry = selectedCountry;
	}

	/**
	 * Gets the selected department.
	 *
	 * @return selectedDepartment
	 */
	public Integer getSelectedDepartment() {
		return selectedDepartment;
	}

	/**
	 * Sets the selected department.
	 *
	 * @param selectedDepartment the new selected department
	 */
	public void setSelectedDepartment(Integer selectedDepartment) {
		this.selectedDepartment = selectedDepartment;
	}

	/**
	 * Gets the selected province.
	 *
	 * @return selectedProvince
	 */
	public Integer getSelectedProvince() {
		return selectedProvince;
	}

	/**
	 * Sets the selected province.
	 *
	 * @param selectedProvince the new selected province
	 */
	public void setSelectedProvince(Integer selectedProvince) {
		this.selectedProvince = selectedProvince;
	}

	/**
	 * Gets the selected locality type.
	 *
	 * @return selectedLocalityType
	 */
	public Integer getSelectedLocalityType() {
		return selectedLocalityType;
	}

	/**
	 * Sets the selected locality type.
	 *
	 * @param selectedLocalityType the new selected locality type
	 */
	public void setSelectedLocalityType(Integer selectedLocalityType) {
		this.selectedLocalityType = selectedLocalityType;
	}

	/**
	 * Gets the geographic location filter.
	 *
	 * @return geographicLocationFilter
	 */
	public GeographicLocationFilter getGeographicLocationFilter() {
		return geographicLocationFilter;
	}

	/**
	 * Sets the geographic location filter.
	 *
	 * @param geographicLocationFilter the new geographic location filter
	 */
	public void setGeographicLocationFilter(
			GeographicLocationFilter geographicLocationFilter) {
		this.geographicLocationFilter = geographicLocationFilter;
	}

	/**
	 * Gets the geographic location data model.
	 *
	 * @return geographicLocationDataModel
	 */
	public GeographicLocationDataModel getGeographicLocationDataModel() {
		return geographicLocationDataModel;
	}

	/**
	 * Sets the geographic location data model.
	 *
	 * @param geographicLocationDataModel the new geographic location data model
	 */
	public void setGeographicLocationDataModel(
			GeographicLocationDataModel geographicLocationDataModel) {
		this.geographicLocationDataModel = geographicLocationDataModel;
	}

	/**
	 * Gets the selected geographic location.
	 *
	 * @return selectedGeographicLocation
	 */
	public GeographicLocation getSelectedGeographicLocation() {
		return selectedGeographicLocation;
	}

	/**
	 * Sets the selected geographic location.
	 *
	 * @param selectedGeographicLocation the new selected geographic location
	 */
	public void setSelectedGeographicLocation(
			GeographicLocation selectedGeographicLocation) {
		this.selectedGeographicLocation = selectedGeographicLocation;
	}

	/**
	 * Gets the local country.
	 *
	 * @return the localCountry
	 */
	public Integer getLocalCountry() {
		return localCountry;
	}

	/**
	 * Sets the local country.
	 *
	 * @param localCountry the localCountry to set
	 */
	public void setLocalCountry(Integer localCountry) {
		this.localCountry = localCountry;
	}


	/**
	 * Cleanup.
	 */
	@PreDestroy
	void cleanup()
	{
		endConversation();
	}

	/**
	 * Gets the country search list.
	 *
	 * @return the countrySearchList
	 */
	public List<GeographicLocation> getCountrySearchList() {
		return countrySearchList;
	}

	/**
	 * Sets the country search list.
	 *
	 * @param countrySearchList the countrySearchList to set
	 */
	public void setCountrySearchList(List<GeographicLocation> countrySearchList) {
		this.countrySearchList = countrySearchList;
	}

	/**
	 * Gets the province search list.
	 *
	 * @return the provinceSearchList
	 */
	public List<GeographicLocation> getProvinceSearchList() {
		return provinceSearchList;
	}

	/**
	 * Sets the province search list.
	 *
	 * @param provinceSearchList the provinceSearchList to set
	 */
	public void setProvinceSearchList(List<GeographicLocation> provinceSearchList) {
		this.provinceSearchList = provinceSearchList;
	}

	/**
	 * Checks if is bl modify.
	 *
	 * @return true, if is bl modify
	 */
	public boolean isBlModify() {
		return blModify;
	}

	/**
	 * Sets the bl modify.
	 *
	 * @param blModify the new bl modify
	 */
	public void setBlModify(boolean blModify) {
		this.blModify = blModify;
	}

	/**
	 * Gets the lst local.
	 *
	 * @return the lst local
	 */
	public List<ParameterTable> getLstLocal() {
		return lstLocal;
	}

	/**
	 * Sets the lst local.
	 *
	 * @param lstLocal the new lst local
	 */
	public void setLstLocal(List<ParameterTable> lstLocal) {
		this.lstLocal = lstLocal;
	}

	/**
	 * Checks if is bl view.
	 *
	 * @return true, if is bl view
	 */
	public boolean isBlView() {
		return blView;
	}

	/**
	 * Sets the bl view.
	 *
	 * @param blView the new bl view
	 */
	public void setBlView(boolean blView) {
		this.blView = blView;
	}

	/**
	 * Gets the department search list.
	 *
	 * @return the department search list
	 */
	public List<GeographicLocation> getDepartmentSearchList() {
		return departmentSearchList;
	}

	/**
	 * Sets the department search list.
	 *
	 * @param departmentSearchList the new department search list
	 */
	public void setDepartmentSearchList(
			List<GeographicLocation> departmentSearchList) {
		this.departmentSearchList = departmentSearchList;
	}	
	
	/**
	 * Checks if is required country.
	 *
	 * @return true, if is required country
	 */
	public boolean isRequiredCountry() {
		return GeographicLocationType.DEPARTMENT.getCode().equals(selectedLocalityType) || 
				GeographicLocationType.PROVINCE.getCode().equals(selectedLocalityType) || 
				GeographicLocationType.DISTRICT.getCode().equals(selectedLocalityType);
	}
	
	/**
	 * Checks if is required department.
	 *
	 * @return true, if is required department
	 */
	public boolean isRequiredDepartment() {
		return  GeographicLocationType.PROVINCE.getCode().equals(selectedLocalityType) || 
				GeographicLocationType.DISTRICT.getCode().equals(selectedLocalityType);
	}
	
	/**
	 * Checks if is required province.
	 *
	 * @return true, if is required province
	 */
	public boolean isRequiredProvince() {
		return  GeographicLocationType.DISTRICT.getCode().equals(selectedLocalityType);
	}		
	
	/**
	 * Checks if is required country search.
	 *
	 * @return true, if is required country search
	 */
	public boolean isRequiredCountrySearch() {
		return Validations.validateIsNotNull(geographicLocationFilter) &&
		(GeographicLocationType.DEPARTMENT.getCode().equals(geographicLocationFilter.getGeoLocationType()) || 
		GeographicLocationType.PROVINCE.getCode().equals(geographicLocationFilter.getGeoLocationType()) || 
		GeographicLocationType.DISTRICT.getCode().equals(geographicLocationFilter.getGeoLocationType()));
	}
	
	/**
	 * Checks if is required department search.
	 *
	 * @return true, if is required department search
	 */
	public boolean isRequiredDepartmentSearch() {
		return Validations.validateIsNotNull(geographicLocationFilter) &&
				(GeographicLocationType.PROVINCE.getCode().equals(geographicLocationFilter.getGeoLocationType()) || 
				GeographicLocationType.DISTRICT.getCode().equals(geographicLocationFilter.getGeoLocationType()));
	}
	
	/**
	 * Checks if is required province search.
	 *
	 * @return true, if is required province search
	 */
	public boolean isRequiredProvinceSearch() {
		return Validations.validateIsNotNull(geographicLocationFilter) &&
				(GeographicLocationType.DISTRICT.getCode().equals(geographicLocationFilter.getGeoLocationType()));
	}
	
}
