package com.pradera.generalparameters.geographiclocations.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.generalparameter.GeographicLocation;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GeographicLocationDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class GeographicLocationDataModel extends ListDataModel<GeographicLocation> implements Serializable, SelectableDataModel<GeographicLocation>{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2574400551428691589L;

	/**
	 * Instantiates a new geographic location data model.
	 */
	public GeographicLocationDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new geographic location data model.
	 *
	 * @param data the data
	 */
	public GeographicLocationDataModel(List<GeographicLocation> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public GeographicLocation getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<GeographicLocation> geographicLocations=(List<GeographicLocation>)getWrappedData();
        for(GeographicLocation geographicLocation : geographicLocations) {  
            if(String.valueOf(geographicLocation.getIdGeographicLocationPk()).equals(rowKey))
                return geographicLocation;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(GeographicLocation geographicLocation) {
		// TODO Auto-generated method stub
		return geographicLocation.getIdGeographicLocationPk();
	}

	/**
	 * Gets the size.
	 *
	 * @return size
	 */
	@SuppressWarnings("unchecked")
	public int getSize() {
		List<GeographicLocation> geographicLocations=(List<GeographicLocation>)getWrappedData();
		if(geographicLocations==null)
			return 0;
		return geographicLocations.size();
	}
}