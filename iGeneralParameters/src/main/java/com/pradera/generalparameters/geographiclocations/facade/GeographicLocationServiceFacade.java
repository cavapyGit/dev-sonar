package com.pradera.generalparameters.geographiclocations.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.generalparameters.geographiclocations.service.GeoLocationServiceBean;
import com.pradera.generalparameters.geographiclocations.view.filter.GeographicLocationFilter;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.GeographicLocationType;

// TODO: Auto-generated Javadoc
/**
 * The GeographicLocationServiceFacade Class.
 *
 * @author PraderaTechnologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class GeographicLocationServiceFacade {

	/** The Transaction synchronization Registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The GeographicLocationServiceBean. */
	@EJB
	GeoLocationServiceBean geographicLocationServiceBean;

	/**
	 * To get the GeographicLocations.
	 *
	 * @param typeGeoLocation the type geo location
	 * @param referenceGeoLocation the reference geo location
	 * @return list
	 * @throws ServiceException the service exception
	 */
	
	public List<GeographicLocation> getLocationsServiceFacade(
			Integer typeGeoLocation, Integer referenceGeoLocation) throws ServiceException {		
		return geographicLocationServiceBean.getLocationsServiceBean(typeGeoLocation, referenceGeoLocation);
	}

	/**
	 * To Register the GeographicLocation.
	 *
	 * @param geographicLocation the geographic location
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.GEOGRAFIC_LOCATION_REGISTRATION)
	public boolean registerLocationServiceFacade(GeographicLocation geographicLocation) throws ServiceException {
		synchronized (geographicLocationServiceBean) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		if (GeographicLocationType.COUNTRY.getCode().equals(geographicLocation.getTypeGeographicLocation())
				&& BooleanType.YES.getCode().equals(geographicLocation.getLocalCountry())){
			GeographicLocation localCountry = geographicLocationServiceBean.findIfExistLocalCountry();
			if(Validations.validateIsNotNullAndNotEmpty(localCountry))
				throw new ServiceException(ErrorServiceType.LOCAL_COUNTRY_EXIST, new Object[]{localCountry.getName()});		
		}
		return geographicLocationServiceBean.registerLocationService(geographicLocation);
		}
	}

	/**
	 * To search the GeographicLocations.
	 *
	 * @param geographicLocationFilter the geographic location filter
	 * @return list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.GEOGRAFIC_LOCATION_QUERY)
	public List<GeographicLocation> searchGeographicLocationServiceFacade(GeographicLocationFilter geographicLocationFilter) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return geographicLocationServiceBean.searchGeographicLocationServiceBean(geographicLocationFilter);
	}

	/**
	 * To update the GeographicLocation.
	 *
	 * @param geographicLocation the geographic location
	 * @return true if success
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.GEOGRAFIC_LOCATION_MODIFICATION)
	public boolean updateGeographicLocationServiceFacade(GeographicLocation geographicLocation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());		
		return geographicLocationServiceBean.updateGeographicLocationService(geographicLocation);
	}

	/**
	 * To get the reference location.
	 *
	 * @param geographicLocation the geographic location
	 * @return GeographicLocation
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation getParentGeographicLocationServiceFacade(
			GeographicLocation geographicLocation) throws ServiceException {
		GeographicLocation parentGeographicLocation = geographicLocationServiceBean
				.getparentGeographicLocationServiceBean(geographicLocation);
		if(Validations.validateIsNotNull(parentGeographicLocation.getGeographicLocation())) {
			parentGeographicLocation.getGeographicLocation().getName();
		}		
		return parentGeographicLocation;
	}

	/**
	 * To get the local Country.
	 *
	 * @return geogaphicLocation
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation getLocalCountryServiceFacade() throws ServiceException{
		return geographicLocationServiceBean.getLocalCountryServiceBean();
	}

	/**
	 * To get the geoGraphicLocation by idGeographicLocation.
	 *
	 * @param selectedCountry the selected country
	 * @return geog
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation getGeographicLocationbyidServiceFacade(
			Integer selectedCountry) throws ServiceException {
		return geographicLocationServiceBean.getGeographicLocationbyidServiceBean(selectedCountry);
	}

	/**
	 * To validate Name of Geograhpic location.
	 *
	 * @param geographicLocation the geographic location
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateNameGeographicLocationServiceFacade(
			GeographicLocation geographicLocation)throws ServiceException {
		return geographicLocationServiceBean.validateNameGeographicLocationServiceBean(geographicLocation)>0;
	}

	/**
	 * Gets the geographic location.
	 *
	 * @param idGeographicLocationPk the id geographic location pk
	 * @return the geographic location
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation getGeographicLocation(Integer idGeographicLocationPk) throws ServiceException{
		GeographicLocation geographicLocation = geographicLocationServiceBean.find(GeographicLocation.class, idGeographicLocationPk);
		if(Validations.validateIsNotNull(geographicLocation.getGeographicLocation())) {
			geographicLocation.getGeographicLocation().getName();
		}
		return geographicLocation;
	}
}
