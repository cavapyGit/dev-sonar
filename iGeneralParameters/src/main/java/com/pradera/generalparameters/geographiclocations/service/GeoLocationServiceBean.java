package com.pradera.generalparameters.geographiclocations.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.geographiclocations.view.filter.GeographicLocationFilter;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.GeographicLocationType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GeoLocationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class GeoLocationServiceBean extends CrudDaoServiceBean {

	/**
	 * To get the List of locations.
	 *
	 * @param typeGeoLocation the type geo location
	 * @param referenceGeoLocation the reference geo location
	 * @return list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<GeographicLocation> getLocationsServiceBean(
			Integer typeGeoLocation, Integer referenceGeoLocation) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select geoloc from GeographicLocation geoloc");
		sbQuery.append(" where  geoloc.typeGeographicLocation=:typeGeoLoc");
		if (referenceGeoLocation == null) {
			sbQuery.append(" and  geoloc.geographicLocation.idGeographicLocationPk is null");
		} else {
			sbQuery.append(" and  geoloc.geographicLocation.idGeographicLocationPk=:refGeoLoc");
		}
		sbQuery.append(" order by geoloc.name");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("typeGeoLoc", typeGeoLocation);
		if (referenceGeoLocation != null) {
			query.setParameter("refGeoLoc", referenceGeoLocation);
		}
		return query.getResultList();
	}

	/**
	 * To Search GeographicLocation.
	 *
	 * @param geographicLocationFilter the geographic location filter
	 * @return list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<GeographicLocation> searchGeographicLocationServiceBean(
			GeographicLocationFilter geographicLocationFilter) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select geoloc from GeographicLocation geoloc where 1=1 ");
		if(Validations.validateIsNotNullAndNotEmpty(geographicLocationFilter.getLocationCode())){
			sbQuery.append(" and geoloc.codeUbigueo=:codegeolocation ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(geographicLocationFilter.getLocationName())){
			sbQuery.append(" and geoloc.name like '"+geographicLocationFilter.getLocationName()+"%'");
		}
		if(Validations.validateIsNotNullAndNotEmpty(geographicLocationFilter.getGeoLocationType())){
			sbQuery.append(" and geoloc.typeGeographicLocation=:typegeolocation ");
		}
		
		if(Validations.validateIsNotNullAndPositive(geographicLocationFilter.getGeoLocationType()) && 
		   geographicLocationFilter.getGeoLocationType().equals(GeographicLocationType.DISTRICT.getCode())){
			
			sbQuery.append(" and geoloc.geographicLocation.idGeographicLocationPk=:provinceLocationCode ");
			
		}else if(Validations.validateIsNotNullAndPositive(geographicLocationFilter.getGeoLocationType()) && 
				 geographicLocationFilter.getGeoLocationType().equals(GeographicLocationType.PROVINCE.getCode())){
			
			sbQuery.append(" and geoloc.geographicLocation.idGeographicLocationPk=:departmentLocationCode ");
			
		}else if(Validations.validateIsNotNullAndPositive(geographicLocationFilter.getGeoLocationType()) && 
				 geographicLocationFilter.getGeoLocationType().equals(GeographicLocationType.DEPARTMENT.getCode())){
			
			sbQuery.append(" and geoloc.geographicLocation.idGeographicLocationPk=:countryLocationCode ");
			
		}
			
		sbQuery.append(" order by name,geoloc.codeUbigueo ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(geographicLocationFilter.getLocationCode())){
			query.setParameter("codegeolocation", geographicLocationFilter.getLocationCode());
		}
		if(Validations.validateIsNotNullAndNotEmpty(geographicLocationFilter.getGeoLocationType())){
			query.setParameter("typegeolocation", geographicLocationFilter.getGeoLocationType());
		}

		if(Validations.validateIsNotNullAndPositive(geographicLocationFilter.getGeoLocationType()) && 
				   geographicLocationFilter.getGeoLocationType().equals(GeographicLocationType.DISTRICT.getCode())){

					query.setParameter("provinceLocationCode", geographicLocationFilter.getProvinceCode());
					
		}else if(Validations.validateIsNotNullAndPositive(geographicLocationFilter.getGeoLocationType()) && 
				 geographicLocationFilter.getGeoLocationType().equals(GeographicLocationType.PROVINCE.getCode())){
			
			query.setParameter("departmentLocationCode", geographicLocationFilter.getDepartmentCode());
			
		}else if(Validations.validateIsNotNullAndPositive(geographicLocationFilter.getGeoLocationType()) && 
				 geographicLocationFilter.getGeoLocationType().equals(GeographicLocationType.DEPARTMENT.getCode())){
			
			query.setParameter("countryLocationCode", geographicLocationFilter.getCountryCode());
			
		}
		
		return (List<GeographicLocation>) query.getResultList();
	}

	/**
	 * To get the parent location.
	 *
	 * @param geographicLocation the geographic location
	 * @return GeographicLocation
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation getparentGeographicLocationServiceBean(
			GeographicLocation geographicLocation) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select geoloc from GeographicLocation geoloc");
		sbQuery.append(" where  geoloc.idGeographicLocationPk=:refGeoLocation");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("refGeoLocation", geographicLocation
				.getGeographicLocation().getIdGeographicLocationPk());
		return (GeographicLocation) query.getSingleResult();
	}

	/**
	 * To get LocalCountry.
	 *
	 * @return geoGraphicLocation
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation getLocalCountryServiceBean() throws ServiceException {
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select geoloc from GeographicLocation geoloc where geoloc.localCountry = :localCoutryCode");
			sbQuery.append("	and geoloc.typeGeographicLocation = :country");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("localCoutryCode", BooleanType.YES.getCode());
			query.setParameter("country", GeographicLocationType.COUNTRY.getCode());
			return (GeographicLocation) query.getSingleResult();
		}
		catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * To get the geographicLocation by idgeographiclocation.
	 *
	 * @param idGeoGraphicLocation the id geo graphic location
	 * @return geographicLocation
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation getGeographicLocationbyidServiceBean(Integer idGeoGraphicLocation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select geoloc from GeographicLocation geoloc");
		sbQuery.append(" where  geoloc.idGeographicLocationPk=:idGeoLocation");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idGeoLocation", idGeoGraphicLocation);
		return (GeographicLocation) query.getSingleResult();
	}
	
	/**
	 * Register location service.
	 *
	 * @param geographicLocation the geographic location
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerLocationService(GeographicLocation geographicLocation) throws ServiceException{
		create(geographicLocation);
		return true;
	}
	
	/**
	 * Update geographic location service.
	 *
	 * @param geographicLocation the geographic location
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateGeographicLocationService(GeographicLocation geographicLocation) throws ServiceException {
		update(geographicLocation);
		return true;
	}

	/**
	 * To validate the Name of GeographicLocation.
	 *
	 * @param geographicLocation the geographic location
	 * @return integer
	 * @throws ServiceException the service exception
	 */
	public int validateNameGeographicLocationServiceBean(
			GeographicLocation geographicLocation) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select count(geoloc) from GeographicLocation geoloc");
		sbQuery.append(" where  geoloc.name = :name ");
		sbQuery.append(" and  geoloc.typeGeographicLocation = :locationType ");
		
		if(Validations.validateIsNotNullAndPositive(geographicLocation.getIdGeographicLocationPk())){
			sbQuery.append(" and  geoloc.idGeographicLocationPk != :idGeographicLocation ");
		}
		
		if(Validations.validateIsNotNull(geographicLocation.getGeographicLocation())) {
			sbQuery.append(" and  geoloc.geographicLocation.idGeographicLocationPk = :idParentGeographicLocation ");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("name", geographicLocation.getName());
		query.setParameter("locationType", geographicLocation.getTypeGeographicLocation());
		
		if(Validations.validateIsNotNullAndPositive(geographicLocation.getIdGeographicLocationPk())){
			query.setParameter("idGeographicLocation", geographicLocation.getIdGeographicLocationPk());
		}
		
		if(Validations.validateIsNotNull(geographicLocation.getGeographicLocation())) {
			query.setParameter("idParentGeographicLocation", geographicLocation.getGeographicLocation().getIdGeographicLocationPk());
		}
		return Integer.parseInt(query.getSingleResult().toString());
	}

	/**
	 * Find if exist local country.
	 *
	 * @return the geographic location
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation findIfExistLocalCountry() throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select gl from GeographicLocation gl");
			sbQuery.append("	where gl.typeGeographicLocation = :locationType");
			sbQuery.append("	and gl.localCountry = :localCountry");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("locationType", GeographicLocationType.COUNTRY.getCode());
			query.setParameter("localCountry", BooleanType.YES.getCode());
			return (GeographicLocation)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
