package com.pradera.generalparameters.restservice;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notifications.remote.Message;
import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.notifications.remote.NotificationRegisterTO;
import com.pradera.generalparameters.messages.facade.NotificationManagementServiceFacade;
import com.pradera.generalparameters.schedulerprocess.service.SchedulerAcessServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessSchedule;

// TODO: Auto-generated Javadoc
/**
 * Session Bean implementation class NotificationResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Path("/notifications")
@Stateless
public class NotificationManagementResource implements Serializable {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4503816852668492927L;
	
	/** The notification management service. */
	@EJB
	NotificationManagementServiceFacade notificationManagementService;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The scheduler access service. */
	@EJB
	SchedulerAcessServiceBean schedulerAccessService;
	
	/**
	 * Test singleton.
	 *
	 * @return the response
	 */
	@GET
	@Path("singleton")
	public Response testSingleton(){
		StringBuilder process = new StringBuilder();
		try{
		for(ProcessSchedule proc : schedulerAccessService.getAllScheduler()){
			process.append(proc.getScheduleDescription()+":"+proc.getNextTimeOut()+"<br>");
		}	
		} catch(Exception ex)
		{
			log.error(ex.toString());
			return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity(process.toString()).build();
	}
	
	/**
	 * Search notifications.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@POST
	@Path("/search")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Message> searchNotifications(MessageFilter filter) throws ServiceException{
		return notificationManagementService.getNotifications(filter);
	}
	
	/**
	 * Send notification manual.
	 *
	 * @param notificationRegister the notification register
	 * @return the response
	 */
	@POST
	@Path("/sendmanual")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendNotificationManual(NotificationRegisterTO notificationRegister){
		try{
			NotificationType notification = NotificationType.get(notificationRegister.getNotificationType());
			notificationManagementService.sendNotificationRemote(notificationRegister.getLoggerUser(), notificationRegister.getUserName(),
					notificationRegister.getBusinessProcess(), notificationRegister.getUsers(), notificationRegister.getSubject(),
					notificationRegister.getMessage(),notification);
		} catch(Exception ex) {
			log.error(ex.toString());
			return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity("sucess").build();
	}
	
	/**
	 * Update notification status.
	 *
	 * @param notificationRegister the notification register
	 * @return the response
	 */
	@POST
	@Path("/updatestatus")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateNotificationStatus(NotificationRegisterTO notificationRegister){
		try{
			notificationManagementService.updateNotificationStatus(notificationRegister.getLoggerUser(), notificationRegister.getUserName(),
					notificationRegister.getNotifications(), notificationRegister.getStatus(), notificationRegister.getNotificationType());
		} catch(Exception ex) {
			log.error(ex.toString());
			return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity("sucess").build();
	}
	
	/**
	 * Send notification configurated.
	 *
	 * @param notificationRegister the notification register
	 * @return the response
	 */
	@POST
	@Path("/send")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendNotificationConfigurated(NotificationRegisterTO notificationRegister){
		try{
		notificationManagementService.sendNotificationConfigurated(notificationRegister.getLoggerUser(), notificationRegister.getUserName()
				, notificationRegister.getBusinessProcess(), notificationRegister.getInstitutionCode(), notificationRegister.getParameters());
		} catch(Exception ex) {
			log.error(ex.toString());
			return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
		}
		return Response.status(Status.OK.getStatusCode()).entity("sucess").build();
	}
	
	/**
     * Default constructor. 
     */
    public NotificationManagementResource() {
        // TODO Auto-generated constructor stub
    }

}
