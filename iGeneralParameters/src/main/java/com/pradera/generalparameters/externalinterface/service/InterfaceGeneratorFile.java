package com.pradera.generalparameters.externalinterface.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.externalinterface.utils.InterfaceXmlParser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceTemplateDetail;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateDetailType;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceGeneratorService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/11/2013
 */
@Stateless
public class InterfaceGeneratorFile {
	
	/**
	 * Generate txt file.
	 *
	 * @param fXmlFile the f xml file
	 * @param externalInterface the external interface
	 * @return the file
	 * @throws ServiceException the service exception
	 */
	public File generateTxtFile(File fXmlFile, ExternalInterface externalInterface) throws ServiceException{
		Document doc;
		try {
			doc = InterfaceXmlParser.getDocumentParsed(fXmlFile);
			NodeList templateList = doc.getElementsByTagName(ExtInterfaceConstanst.TEMPLATE_NODE);
			NodeList dataList = doc.getElementsByTagName(ExtInterfaceConstanst.DATA_NODE);
			List<String[]> headerData = InterfaceXmlParser.transformTemplateFromXml(templateList);
			List<String[]> contentData = InterfaceXmlParser.transformDataFromXml(dataList);
			
			File file =  File.createTempFile(fXmlFile.getName().substring(0,fXmlFile.getName().lastIndexOf(".")),InterfaceExtensionFileType.TXT.getPrefix());
			FileOutputStream is = new FileOutputStream(file);
	        OutputStreamWriter osw = new OutputStreamWriter(is);   
	        Writer w = new BufferedWriter(osw);
			
			for(String[] stringData: headerData){
				w.write("\n");
				for(String stringOfRow: stringData){
					w.append(stringOfRow);
				}
			}
			
			for(String[] stringData: contentData){
				w.write("\n");
				for(String stringOfRow: stringData){
					w.append(stringOfRow);
				}
			}
			w.close();
			return file;
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Generate xml file.
	 *
	 * @param fXmlFile the f xml file
	 * @param externalInterface the external interface
	 * @return the file
	 * @throws ServiceException the service exception
	 */
	public File generateXmlFile(File fXmlFile, ExternalInterface externalInterface) throws ServiceException{
		try {
			Document docXmlFile = InterfaceXmlParser.getDocumentParsed(fXmlFile);
			
			String fileName = fXmlFile.getName().substring(0,fXmlFile.getName().lastIndexOf("."));
			File file =  File.createTempFile(fileName,InterfaceExtensionFileType.XML.getPrefix());
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			
			NodeList templateList = docXmlFile.getElementsByTagName(ExtInterfaceConstanst.TEMPLATE_NODE);
			NodeList dataList = docXmlFile.getElementsByTagName(ExtInterfaceConstanst.DATA_NODE);
			
			Element rootElement = doc.createElement(ExtInterfaceConstanst.INTERFACE_NODE.concat(externalInterface.getIdExtInterfacePk().toString()));
			for(Element element: InterfaceXmlParser.getElementsFromNodeList(dataList, templateList, doc)){
				rootElement.appendChild(element);
			}
			doc.appendChild(rootElement);
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer;
			try {
				transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(file);
				transformer.transform(source, result);
			} catch (TransformerConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return file;
		} catch (ParserConfigurationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Generate cvs file.
	 *
	 * @param fXmlFile the f xml file
	 * @param externalInterface the external interface
	 * @return the file
	 * @throws ServiceException the service exception
	 */
	public File generateCvsFile(File fXmlFile, ExternalInterface externalInterface) throws ServiceException{
		try {
			Document doc = InterfaceXmlParser.getDocumentParsed(fXmlFile);
			
			NodeList templateList = doc.getElementsByTagName(ExtInterfaceConstanst.TEMPLATE_NODE);
			NodeList dataList = doc.getElementsByTagName(ExtInterfaceConstanst.DATA_NODE);
			
			List<String[]> headerData = InterfaceXmlParser.transformTemplateFromXml(templateList);
			
			List<String[]> contentData = InterfaceXmlParser.transformDataFromXml(dataList);
			
			//workbook variable to handle all data on xls
			HSSFWorkbook workBook = new HSSFWorkbook();
			
			List<String> numberHeaders = getNumbersHeaders(externalInterface);
			
			//genera primeras filas de cabecera del Excel
			workBook = createHeaderXls(headerData, contentData, workBook, numberHeaders);
			
			String fileName = fXmlFile.getName().substring(0,fXmlFile.getName().lastIndexOf("."));
			File file =  File.createTempFile(fileName,InterfaceExtensionFileType.CVS.getPrefix());
			FileOutputStream fileOut =  new FileOutputStream(file);
			workBook.write(fileOut);
			fileOut.close();
			return file;
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the numbers headers.
	 *
	 * @param externalInterface the external interface
	 * @return the numbers headers
	 */
	private List<String> getNumbersHeaders(ExternalInterface externalInterface) {
		List<String> numbersHeaders = new ArrayList<String>();
		Integer count = 1;
		for(ExtInterfaceTemplateDetail detailTemplate : externalInterface.getExternalInterfaceTemplate().getExtInterfaceTemplateDetails()){
			if(detailTemplate.getTemplateDetailType().equals(InterfaceTemplateDetailType.HEADER.getCode())){
				numbersHeaders.add(count.toString());
			}
			count++;
		}
		return numbersHeaders;
	}

	/**
	 * Creates the header xls.
	 *
	 * @param headerData the header data
	 * @param contentData the content data
	 * @param workBook the work book
	 * @param numbersHeaders the numbers headers
	 * @return the hSSF workbook
	 */
	private HSSFWorkbook createHeaderXls(List<String[]> headerData, List<String[]> contentData, HSSFWorkbook workBook, List<String> numbersHeaders){
		HSSFSheet sheet = workBook.createSheet("Sample sheet");
		
		HSSFFont fontBold =workBook.createFont();
		fontBold.setBold(true);
		HSSFCellStyle headerStyle = workBook.createCellStyle();
		headerStyle.setFont(fontBold);
		
		HSSFCellStyle backgroundColor = workBook.createCellStyle(); 
		backgroundColor.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		backgroundColor.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getIndex()); 
		
		Integer rowNum = 0;
		for(String[] rowOfData: headerData){
			Row row = sheet.createRow(rowNum++);
			int cellnum = 0;
			for(String cellOfRow: rowOfData){
				Cell cell = row.createCell(cellnum++);
		    	cell.setCellValue(((String)cellOfRow).toUpperCase());
		    	cell.setCellStyle(headerStyle);
			}
		}
		
		rowNum++;
		for(String[] contentOfData: contentData){
			Row row = sheet.createRow(rowNum++);
			int cellnum = 0;		
			boolean indHeader = false;			
			if(numbersHeaders.contains((String)contentOfData[0])){
				indHeader = true;
			}						
			for(String cellOfRow: contentOfData){				
				Cell cell = row.createCell(cellnum++);
		    	cell.setCellValue((String)cellOfRow);
	    		if(indHeader){
	    			cell.setCellStyle(backgroundColor);
	    		}	    	
			}			
		}
		return workBook;
	}
}