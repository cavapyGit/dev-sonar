package com.pradera.generalparameters.externalinterface.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum SearchTRCRType {
	
	CLIENT_PORTFOLIO(Integer.valueOf(1),"CARTERA DE CLIENTES"),
	NEGOTIATION(Integer.valueOf(2),"NEGOCIACION"),
	TRANSFER(Integer.valueOf(3),"TRASPASO"),
	ACV(Integer.valueOf(4),"ACV"),
	//SERIADOS(Integer.valueOf(5),"SERIADOS"),

	;
	
	/** The Constant list. */
	public static final List<SearchTRCRType> list = new ArrayList<SearchTRCRType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SearchTRCRType> lookup = new HashMap<Integer, SearchTRCRType>();
	static {
		for (SearchTRCRType s : EnumSet.allOf(SearchTRCRType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SearchTRCRType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static SearchTRCRType get(Integer code) {
		return lookup.get(code);
	}
}
