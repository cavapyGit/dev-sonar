package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceTemplateMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@DepositaryWebBean
public class ExternalInterfaceTemplateMgmt  extends GenericBaseBean implements Serializable {

	/** The start def date. */
	private Date startDefDate;
	
	/** The end def date. */
	private Date endDefDate;

	/** The id. */
	private Integer id;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The headers count. */
	private Integer headersCount;
	
	/** The details count. */
	private Integer detailsCount;
	
	/** The details list. */
	private List<ExternalInterfaceTemplateDetail> detailsList;
	
	/** The external interface template prot. */
	private ExternalInterfaceTemplateProt externalInterfaceTemplateProt;
	
	/** The external interface template selected. */
	private ExternalInterfaceTemplateProt externalInterfaceTemplateSelected;
	
	/** The external interface template list. */
	private GenericDataModel<ExternalInterfaceTemplateProt> externalInterfaceTemplateList;
	
	/**
	 * Instantiates a new external interface template mgmt.
	 */
	public ExternalInterfaceTemplateMgmt() {
		externalInterfaceTemplateProt = new ExternalInterfaceTemplateProt();
		externalInterfaceTemplateProt.setId(1);		
		detailsList = new ArrayList<ExternalInterfaceTemplateDetail>();
		cleanExternalInterfaceTemplate();
	}
	
	/**
	 * Before register.
	 *
	 * @return the string
	 */
	public String beforeRegister() {
		return "register";
	}
	
	/**
	 * Search external interface template.
	 */
	public void searchExternalInterfaceTemplate(){
		ExternalInterfaceTemplateProt eit = new ExternalInterfaceTemplateProt();
		eit.setId(1);
		eit.setDescription("Descripcion a Template de external interface");
		eit.setName("External Template ONE");
		eit.setRegisterDate(new Date());
		eit.setState(1);
		
		List<ExternalInterfaceTemplateProt> eitList = new ArrayList<ExternalInterfaceTemplateProt>();
		eitList.add(eit);
		eitList.add(eit);
		eitList.add(eit);
		eitList.add(eit);
		eitList.add(eit);
		
		externalInterfaceTemplateList = new GenericDataModel<ExternalInterfaceTemplateProt>(eitList);
	}
	
	/**
	 * Back search.
	 *
	 * @return the string
	 */
	public String backSearch() {
		return "search";
	}
	
	/**
	 * Clean external interface template.
	 */
	public void cleanExternalInterfaceTemplate(){
		externalInterfaceTemplateList = new GenericDataModel<ExternalInterfaceTemplateProt>(null);
		detailsList = new ArrayList<ExternalInterfaceTemplateDetail>();
		headersCount = 0;
		detailsCount = 0;		
	}

	/**
	 * Gets the external interface template.
	 *
	 * @return the external interface template
	 */
	public ExternalInterfaceTemplateProt getExternalInterfaceTemplate() {
		return externalInterfaceTemplateProt;
	}

	/**
	 * Sets the external interface template.
	 *
	 * @param externalInterfaceTemplateProt the new external interface template
	 */
	public void setExternalInterfaceTemplate(
			ExternalInterfaceTemplateProt externalInterfaceTemplateProt) {
		this.externalInterfaceTemplateProt = externalInterfaceTemplateProt;
	}
	
	/**
	 * Add details order list.
	 */
	public void addDetailsOrderList(){
		detailsList = new ArrayList<ExternalInterfaceTemplateDetail>();
		
		int id = 0;
		for(int i = 1 ; i <= headersCount; i++){
			ExternalInterfaceTemplateDetail eitd = new ExternalInterfaceTemplateDetail();
			eitd.setId(id);
			eitd.setType("HEADER");
			eitd.setName(""+i);
			detailsList.add(eitd);
			id++;
		}
		for(int i = 1 ; i <= detailsCount; i++){
			ExternalInterfaceTemplateDetail eitd = new ExternalInterfaceTemplateDetail();
			eitd.setId(id);
			eitd.setType("DETAILS");
			eitd.setName(""+i);
			detailsList.add(eitd);
			id++;
		}
	}

	/**
	 * Gets the start def date.
	 *
	 * @return the start def date
	 */
	public Date getStartDefDate() {
		return startDefDate;
	}

	/**
	 * Sets the start def date.
	 *
	 * @param startDefDate the new start def date
	 */
	public void setStartDefDate(Date startDefDate) {
		this.startDefDate = startDefDate;
	}

	/**
	 * Gets the end def date.
	 *
	 * @return the end def date
	 */
	public Date getEndDefDate() {
		return endDefDate;
	}

	/**
	 * Sets the end def date.
	 *
	 * @param endDefDate the new end def date
	 */
	public void setEndDefDate(Date endDefDate) {
		this.endDefDate = endDefDate;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the external interface template selected.
	 *
	 * @return the external interface template selected
	 */
	public ExternalInterfaceTemplateProt getExternalInterfaceTemplateSelected() {
		return externalInterfaceTemplateSelected;
	}

	/**
	 * Sets the external interface template selected.
	 *
	 * @param externalInterfaceTemplateSelected the new external interface template selected
	 */
	public void setExternalInterfaceTemplateSelected(
			ExternalInterfaceTemplateProt externalInterfaceTemplateSelected) {
		this.externalInterfaceTemplateSelected = externalInterfaceTemplateSelected;
	}

	/**
	 * Gets the external interface template list.
	 *
	 * @return the external interface template list
	 */
	public GenericDataModel<ExternalInterfaceTemplateProt> getExternalInterfaceTemplateList() {
		return externalInterfaceTemplateList;
	}

	/**
	 * Sets the external interface template list.
	 *
	 * @param externalInterfaceTemplateList the new external interface template list
	 */
	public void setExternalInterfaceTemplateList(
			GenericDataModel<ExternalInterfaceTemplateProt> externalInterfaceTemplateList) {
		this.externalInterfaceTemplateList = externalInterfaceTemplateList;
	}

	/**
	 * Gets the headers count.
	 *
	 * @return the headers count
	 */
	public Integer getHeadersCount() {
		return headersCount;
	}

	/**
	 * Sets the headers count.
	 *
	 * @param headersCount the new headers count
	 */
	public void setHeadersCount(Integer headersCount) {
		this.headersCount = headersCount;
	}

	/**
	 * Gets the details count.
	 *
	 * @return the details count
	 */
	public Integer getDetailsCount() {
		return detailsCount;
	}

	/**
	 * Sets the details count.
	 *
	 * @param detailsCount the new details count
	 */
	public void setDetailsCount(Integer detailsCount) {
		this.detailsCount = detailsCount;
	}

	/**
	 * Gets the details list.
	 *
	 * @return the details list
	 */
	public List<ExternalInterfaceTemplateDetail> getDetailsList() {
		return detailsList;
	}

	/**
	 * Sets the details list.
	 *
	 * @param detailsList the new details list
	 */
	public void setDetailsList(List<ExternalInterfaceTemplateDetail> detailsList) {
		this.detailsList = detailsList;
	}
	
	
	
}
