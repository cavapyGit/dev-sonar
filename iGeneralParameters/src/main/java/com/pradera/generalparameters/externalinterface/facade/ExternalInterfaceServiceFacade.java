package com.pradera.generalparameters.externalinterface.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.generalparameters.externalinterface.resources.InterfaceSchedulerService;
import com.pradera.generalparameters.externalinterface.service.ExternalInterfaceService;
import com.pradera.generalparameters.externalinterface.to.InterfaceProcessTO;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.externalinterface.utils.KeywordsSQL;
import com.pradera.generalparameters.externalinterface.utils.SqlController;
import com.pradera.generalparameters.externalinterface.view.ExternalInterfaceTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceTemplateDetail;
import com.pradera.model.generalparameter.externalinterface.ExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceColumn;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceExtension;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceFilter;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceQuery;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTemplate;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceUserType;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceColumnEngine;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceTableEngine;
import com.pradera.model.generalparameter.externalinterface.type.DataColumnType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceExecutionType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceOperatorLogicType;
import com.pradera.model.generalparameter.externalinterface.type.InterfacePredicateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTableStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceUserTypeStateType;
import com.pradera.model.generalparameter.externalinterface.type.SqlComparatorType;
import com.pradera.model.report.Report;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ExternalInterfaceServiceFacade  extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The external interface service. */
	@EJB
	ExternalInterfaceService externalInterfaceService;
	
	/** The interface scheduler service. */
	@EJB
	InterfaceSchedulerService interfaceSchedulerService;
	
	/**
	 * Registry interface template.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_REGISTER)
	public ExternalInterface registryInterfaceFacade(ExternalInterface externalInterface) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		ExternalInterface extInterface =  externalInterfaceService.registryInterfaceService(externalInterface);
		if(extInterface.getExecutionType().equals(ExternalInterfaceExecutionType.AUTOMATIC.getCode()) 
			|| extInterface.getExecutionType().equals(ExternalInterfaceExecutionType.BOTH.getCode())){
			interfaceSchedulerService.registerTimer(extInterface);
		}		
		return extInterface;
	}
	/**
	 * Gets the interface reseted.
	 * method to get ExternalInterface Reseted. maybe with some attribute from database 
	 * @return the interface reseted
	 */
	public ExternalInterface getInterfaceReseted(){
		ExternalInterface externalInterface = new ExternalInterface();
		
		externalInterface.setRegistryDate(CommonsUtilities.currentDateTime());	
		externalInterface.setExternalInterfaceState(ExternalInterfaceStateType.REGISTERED.getCode());
		externalInterface.setIndAccessWebservice(BooleanType.NO.getCode());
		externalInterface.setIndEmptySend(BooleanType.YES.getCode());
		externalInterface.setIdBusinessProcessFk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());		
		externalInterface.setExternalInterfaceTemplate(new ExternalInterfaceTemplate());
		externalInterface.setSendPeriocity(new Date());
		Report report = new Report();
		report.setIdReportPk(ExtInterfaceConstanst.REPORT_ASOCIATED);
		externalInterface.setReport(report);
		
		return externalInterface;
	}
	
	/**
	 * Validated query to execute service.
	 *
	 * @param query the query
	 * @throws ServiceException the service exception
	 */
	public void validatedQueryToExecuteService(String query) throws ServiceException {
		if(query!=null && !query.isEmpty()){
			externalInterfaceService.validatedQueryToExecuteService(query);			
			SqlController sqlSeparator = new SqlController(query);		
			if(!sqlSeparator.haveColumns()){
				throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_QUERY_COLUMN_EMPTY);
			}
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_QUERY_COLUMN_EMPTY);
		}
	}
	
	/**
	 * Gets the external interface with columns.
	 *
	 * @param interfaceQuery the external interface
	 * @return the external interface with columns
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceQuery getExternalInterfaceWithColumns(ExternalInterfaceQuery interfaceQuery) throws ServiceException{
		String query = interfaceQuery.getInterfaceTemplateQuery();
		
		if(query!=null && !query.isEmpty()){
			
			externalInterfaceService.validatedQueryToExecuteService(query);
			
			SqlController sqlSeparator = new SqlController(query);
			
			if(sqlSeparator.haveColumns()){
				
				interfaceQuery.setExternalInterfaceTables(new ArrayList<ExternalInterfaceTable>());
				//recorremos y creamos todas las tablas del query				
				for(Entry<String, String> tableQuery : sqlSeparator.getRelationAliasTable().entrySet()){
					ExternalInterfaceTable interfaceTable = new ExternalInterfaceTable();
					String tableName = (tableQuery.getValue().concat(KeywordsSQL.BLANK_SPACE).concat(tableQuery.getKey())).toUpperCase();
					interfaceTable.setTableName(tableName);
					interfaceTable.setExternalInterfaceColumns(new ArrayList<ExternalInterfaceColumn>());
														
					//le añadimos las columnas a la tabla
					if(sqlSeparator.getRelationships().get(tableQuery.getKey()) != null){
						
						for(String columnQuery: sqlSeparator.getRelationships().get(tableQuery.getKey())){
							ExternalInterfaceColumn externalColumn = new ExternalInterfaceColumn();
							externalColumn.setIsSelected(Boolean.TRUE);
							externalColumn.setColumnName(columnQuery.toUpperCase());
							externalColumn.setColumnNameAlias(sqlSeparator.getRelationAliasColumn().get(tableQuery.getKey()).get(columnQuery));
							externalColumn.setDataTypeTarget(DataColumnType.VARCHAR2.getCode());
							externalColumn.setTypeTargetValue(DataColumnType.VARCHAR2.getValue());
							interfaceTable.getExternalInterfaceColumns().add(externalColumn);
						}
					}
					//buscamos si la tabla ya existe
					if(interfaceQuery.getExternalInterfaceTables().size() > 0){
						Boolean existTable = Boolean.FALSE;
						for(ExternalInterfaceTable table : interfaceQuery.getExternalInterfaceTables()){
							if(table.getTableName().equals(tableName)){
								columnParentFor:
								for(ExternalInterfaceColumn interfaceColumn : interfaceTable.getExternalInterfaceColumns()){
									for(ExternalInterfaceColumn column : table.getExternalInterfaceColumns()){
										if(interfaceColumn.getColumnName().equals(column.getColumnName())){
											break columnParentFor;
										}
									}
									table.getExternalInterfaceColumns().add(interfaceColumn);
								}
								existTable = Boolean.TRUE;
							}
						}
						if(!existTable){
							interfaceQuery.getExternalInterfaceTables().add(interfaceTable);
						}
					}else{
						interfaceQuery.getExternalInterfaceTables().add(interfaceTable);
					}					
				}
			}else{				
				throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_QUERY_COLUMN_EMPTY);
			}
		}
		return interfaceQuery;
	}
	
	/**
	 * Gets the query from columns facade.
	 *
	 * @param _interface the _interface
	 * @param columnsList the columns list
	 * @return the query from columns facade
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceQuery getQueryFromColumnsFacade(ExternalInterfaceQuery _interface, List<InterfaceColumnEngine> columnsList) throws ServiceException{
		Map<String,List<String>> query = new HashMap<String, List<String>>();		
		_interface.setExternalInterfaceTables(new ArrayList<ExternalInterfaceTable>());
		
		List<String> tablesTemp = new ArrayList<String>();
		for(InterfaceColumnEngine columnTable: columnsList){
			String tableName = columnTable.getTableName();
			if(!tablesTemp.contains(tableName)){
				tablesTemp.add(tableName);
			}
		}
		
		for(String tableName: tablesTemp){
			ExternalInterfaceTable interfaceTable = new ExternalInterfaceTable();
			interfaceTable.setExternalInterfaceColumns(new ArrayList<ExternalInterfaceColumn>());
			interfaceTable.setTableName(tableName);
			interfaceTable.setInterfaceTableState(InterfaceTableStateType.REGISTERED.getCode());
			List<String> columnsQuery = new ArrayList<String>();
			for(InterfaceColumnEngine columnTable: columnsList){
				if(tableName.equals(columnTable.getTableName())){
					ExternalInterfaceColumn externalColumn = new ExternalInterfaceColumn();
					externalColumn.setIsSelected(Boolean.TRUE);
					externalColumn.setColumnName(columnTable.getColumnName());					
					externalColumn.setDataTypeOrigin(DataColumnType.valueOf(columnTable.getDataType()).getCode());
					externalColumn.setTypeOriginValue(columnTable.getDataType());
					externalColumn.setDataTypeTarget(DataColumnType.VARCHAR2.getCode());
					externalColumn.setTypeTargetValue(DataColumnType.VARCHAR2.getValue());
					externalColumn.setLength(columnTable.getDataLength());
					externalColumn.setExternalInterfaceTable(interfaceTable);					
					interfaceTable.getExternalInterfaceColumns().add(externalColumn);
					columnsQuery.add(columnTable.getColumnName());
				}
			}
			_interface.getExternalInterfaceTables().add(interfaceTable);
			query.put(tableName, columnsQuery);
		}
		SqlController sqlController = new SqlController(query);
		_interface.setInterfaceTemplateQuery(sqlController.getSimpleQuery());
		_interface.setIndQueryGenerated(BooleanType.NO.getCode());
		return _interface;
	}
	/**
	 * Gets the interface template list facade.
	 *
	 * @return the interface template list facade
	 * @throws ServiceException the service exception
	 */
	public List<ExternalInterfaceTemplate> getInterfaceTemplateListFacade() throws ServiceException{
		return externalInterfaceService.getInterfaceTemplateListService();
	}

	/**
	 * Gets the owner database facade.
	 *
	 * @return the owner database facade
	 * @throws ServiceException the service exception
	 */
	public List<String> getOwnerDatabaseFacade() throws ServiceException{
		return externalInterfaceService.getOwnerDatabaseService();
	}
	/**
	 * Gets the tables database facade.
	 *
	 * @param ownerDatabase the owner database
	 * @return the tables database facade
	 * @throws ServiceException the service exception
	 */
	public List<String> getTablesDatabaseFacade(String ownerDatabase) throws ServiceException{
		return externalInterfaceService.getTablesDatabaseService(ownerDatabase);
	}
	/**
	 * Gets the tables engine service.
	 *
	 * @param ownerDatabase the owner database
	 * @return the tables engine service
	 * @throws ServiceException the service exception
	 */
	public List<InterfaceTableEngine> getTablesFromOwnerFacade(String ownerDatabase) throws ServiceException{
		return externalInterfaceService.getTablesFromOwnerService(ownerDatabase);
	}
	/**
	 * Gets the columns from table service.
	 *
	 * @param tableName the table name
	 * @return the columns from table service
	 * @throws ServiceException the service exception
	 */
	public List<InterfaceColumnEngine> getColumnsFromTableFacade(String tableName) throws ServiceException{
		return externalInterfaceService.getColumnsFromTableService(tableName);
	}
	
	/**
	 * Gets the interface template detail list facade.
	 *
	 * @param idExtInferfaceTemplatePk the id ext inferface template pk
	 * @return the interface template detail list facade
	 * @throws ServiceException the service exception
	 */
	public List<ExtInterfaceTemplateDetail> getInterfaceTemplateDetailListFacade(Long idExtInferfaceTemplatePk) throws ServiceException{		
		return externalInterfaceService.getInterfaceTemplateDetailListFacade(idExtInferfaceTemplatePk);
	}
	
	/**
	 * Gets the interface template facade.
	 *
	 * @param idExtInferfaceTemplatePk the id ext inferface template pk
	 * @return the interface template facade
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate getInterfaceTemplateFacade(Long idExtInferfaceTemplatePk) throws ServiceException{			
		ExternalInterfaceTemplate template = externalInterfaceService.getInterfaceTemplateFacade(idExtInferfaceTemplatePk);
		return template;
	}
	
	/**
	 * Gets the extension type list facade.
	 *
	 * @return the extension type list facade
	 * @throws ServiceException the service exception
	 */
	public List<ExternalInterfaceExtension> getExtensionTypeListFacade() throws ServiceException{
		List<ExternalInterfaceExtension> externalInterfaceExtensions =  new ArrayList<ExternalInterfaceExtension>();
		
		for(ExtensionFileType extensionFileType: externalInterfaceService.getExtensionFileListService()){
			ExternalInterfaceExtension extension = new ExternalInterfaceExtension();
			
			extension.setExtensionFileType(extensionFileType);	
			extension.setInterfaceExtensionState(InterfaceExtensionStateType.REGISTERED.getCode());
			
			externalInterfaceExtensions.add(extension);
		}
		return externalInterfaceExtensions;
	}
	
	/**
	 * Gets the user type list facade.
	 *
	 * @return the user type list facade
	 */
	public List<ExternalInterfaceUserType> getUserTypeListFacade() {
		List<ExternalInterfaceUserType> externalInterfaceUserTypes = new ArrayList<ExternalInterfaceUserType>();
		
		for(InstitutionType institutionType: InstitutionType.list){			
			ExternalInterfaceUserType userType = new ExternalInterfaceUserType();
			
			userType.setIdUserTypeFk(institutionType.getCode());
			userType.setUserTypeDescription(institutionType.getValue());
			userType.setIsSelected(Boolean.FALSE);
			userType.setInterfaceUserState(InterfaceUserTypeStateType.REGISTERED.getCode());
			
			externalInterfaceUserTypes.add(userType);			
		}
		return externalInterfaceUserTypes;
	}
	
	/**
	 * Adds the filter to sql query.
	 *
	 * @param filter the filter
	 * @param query the query
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String addFilterToSqlQuery (ExternalInterfaceFilter filter,	String query) throws ServiceException {		
		SqlController sqlSeparator = new SqlController(query);		
		String filterValue = generateFilterAndPredicateValue(filter, sqlSeparator);
		
		if(filterValue == null){
			throw new SecurityException("Error al generar SQL");
		}
		
		sqlSeparator.getWhereParamsList().add(filterValue);
		String sqlQuery = sqlSeparator.addWhereToSQL();
				
		if(!sqlQuery.contains(ExtInterfaceConstanst.IDENTIFICATOR)){
			externalInterfaceService.validatedQueryToExecuteService(sqlQuery);
		}
		return sqlQuery;
	}
	
	/**
	 * Gets the external interface list.
	 *
	 * @param externalInterfaceSearch the external interface search
	 * @return the external interface list
	 * @throws ServiceException the service exception
	 */
	public List<ExternalInterfaceTO> getExternalInterfaceList(ExternalInterfaceTO externalInterfaceSearch) throws ServiceException{
		return externalInterfaceService.getExternalInterfaceList(externalInterfaceSearch);
	}
	
	/**
	 * Gets the external interface.
	 *
	 * @param id the id
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface getExternalInterface(Long id) throws ServiceException {
		return externalInterfaceService.getExternalInterface(id);
	}
	
	/**
	 * Confirm external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_CONFIRM)
	public ExternalInterface confirmExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		return externalInterfaceService.confirmExternalInterface(externalInterface);
	}
	
	/**
	 * Gets the external interface.
	 *
	 * @param id the id
	 * @param state the state
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface getExternalInterface(Long id,ExternalInterfaceStateType state) throws ServiceException{
		return externalInterfaceService.getExternalInterface(id,state);
	}
	
	/**
	 * Gets the external interface to unblock.
	 *
	 * @param id the id
	 * @return the external interface to unblock
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface getExternalInterfaceToUnblock(Long id) throws ServiceException{
		return externalInterfaceService.getExternalInterfaceToUnblock(id);
	}
	
	/**
	 * Annular external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_ANNULAR)
	public ExternalInterface annularExternalInterface(ExternalInterface externalInterface) throws ServiceException{		
		return externalInterfaceService.annularExternalInterface(externalInterface);
	}
	
	/**
	 * Block external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_BLOCK)
	public ExternalInterface blockExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		return externalInterfaceService.blockExternalInterface(externalInterface);
	}
	
	/**
	 * Unblock external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_CONFIRM)
	public ExternalInterface unblockExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		return externalInterfaceService.unblockExternalInterface(externalInterface);
	}
	
	/**
	 * Modify external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_MODIFY)
	public ExternalInterface modifyExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		ExternalInterface extInterface = externalInterfaceService.modifyExternalInterface(externalInterface);
		
		if(extInterface.getExecutionType().equals(ExternalInterfaceExecutionType.AUTOMATIC.getCode()) 
			|| extInterface.getExecutionType().equals(ExternalInterfaceExecutionType.BOTH.getCode())){
			interfaceSchedulerService.registerTimer(extInterface);
		}
		
		return extInterface;
	}
	
	/**
	 * Removes the filter to sql query.
	 *
	 * @param filter the filter
	 * @param interfaceQuery the interface query
	 * @return the string
	 */
	public String removeFilterToSqlQuery(ExternalInterfaceFilter filter,ExternalInterfaceQuery interfaceQuery){		
		SqlController sqlSeparator = new SqlController(interfaceQuery.getInterfaceTemplateQuery());	
		String filterValue = generateFilterAndPredicateValue(filter, sqlSeparator);
		
		String query = interfaceQuery.getInterfaceTemplateQuery();		
		if(query.contains(filterValue)){
			query = query.replace(filterValue, "");
		}else{
			//si el operador de inicio es AND o OR
			String and = (KeywordsSQL.AND.concat(KeywordsSQL.BLANK_SPACE)).toUpperCase();
			String or = (KeywordsSQL.OR.concat(KeywordsSQL.BLANK_SPACE)).toUpperCase();
			
			if(filterValue.contains(and)){
				filterValue = filterValue.replace(and, "");
			}else if (filterValue.contains(or)){
				filterValue = filterValue.replace(or, "");
			}
			if(query.contains(filterValue)){
				query = query.replace(filterValue, "");
			}
		}
		return query;
		
	}
	
	/**
	 * Generate filter and predicate value.
	 *
	 * @param filter the filter
	 * @param sqlSeparator the sql separator
	 * @return the string
	 */
	public String generateFilterAndPredicateValue(ExternalInterfaceFilter filter,SqlController sqlSeparator) {
				
		StringBuilder predicate = new StringBuilder();
		
		if(filter.getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.COLUMN.getCode())){
			predicate.append( filter.getInterfaceFilterPredicate().getExternalInterfaceColumn().getExternalInterfaceTable().getTableName());
			predicate.append(KeywordsSQL.DOT);
			predicate.append(filter.getInterfaceFilterPredicate().getExternalInterfaceColumn().getColumnName());	
			if(filter.getInterfaceFilterPredicate().getPredicateValueColumn().contains(ExtInterfaceConstanst.IDENTIFICATOR)){
				predicate.append(KeywordsSQL.DOT);
				predicate.append(ExtInterfaceConstanst.IDENTIFICATOR);
			}
		}else{
			predicate.append(filter.getInterfaceFilterPredicate().getPredicateValue());
		}	
		String filterValue = sqlSeparator.addFilterToQuery(	InterfaceOperatorLogicType.get(filter.getStartLogicalOperator()),
				filter.getExternalInterfaceTable().getTableName(),
				filter.getInterfaceFilterColumn(),
				SqlComparatorType.get(filter.getInterfaceFilterPredicate().getComparisonOperator()),
				InterfacePredicateType.get(filter.getInterfaceFilterPredicate().getPredicateType()),
				predicate.toString());
		
		return filterValue;
	}
	
	/**
	 * Gets the list external interface.
	 *
	 * @param indWebService the ind web service
	 * @param indInterfaceType the ind interface type
	 * @return the list external interface
	 */
	public List<ExternalInterface> getListExternalInterface(Integer indWebService, Integer indInterfaceType) {
		return externalInterfaceService.getListExternalInterface(indWebService, indInterfaceType);
	}
	
	/**
	 * Gets the list interface process.
	 *
	 * @param interfaceProcessTO the interface process to
	 * @return the list interface process
	 */
	public List<InterfaceProcess> getListInterfaceProcess(InterfaceProcessTO interfaceProcessTO) {
		return externalInterfaceService.getListInterfaceProcess(interfaceProcessTO);
	}
	
	/**
	 * Gets the list interface transaction.
	 *
	 * @param interfaceProcessTO the interface process to
	 * @return the list interface transaction
	 */
	public List<InterfaceTransaction> getListInterfaceTransaction(InterfaceProcessTO interfaceProcessTO) {
		return externalInterfaceService.getListInterfaceTransaction(interfaceProcessTO);
	}
	
	/**
	 * Find external interface.
	 *
	 * @param idExternalInterface the id external interface
	 * @return the external interface
	 */
	public ExternalInterface findExternalInterface(Long idExternalInterface) {
		return externalInterfaceService.findExternalInterface(idExternalInterface);
	}
	
	/**
	 * Gets the processed file content.
	 *
	 * @param idInterfaceProcess the id interface process
	 * @return the processed file content
	 */
	public byte[] getProcessedFileContent(Long idInterfaceProcess) {
		return externalInterfaceService.getProcessedFileContent(idInterfaceProcess);
	}
	
	/**
	 * Gets the response file content.
	 *
	 * @param idInterfaceProcess the id interface process
	 * @return the response file content
	 */
	public byte[] getResponseFileContent(Long idInterfaceProcess) {
		return externalInterfaceService.getResponseFileContent(idInterfaceProcess);
	}
	
}