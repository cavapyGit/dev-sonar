package com.pradera.generalparameters.externalinterface.view;

import java.util.ArrayList;
import java.util.List;

import com.pradera.generalparameters.externalinterface.utils.KeywordsSQL;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceColumn;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceFilter;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceQuery;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTable;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceColumnEngine;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceTableEngine;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceAttributtes.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/10/2013
 */
public class ExternalInterfaceAttributtes {
		
	/** The ower data base. */
	private List<String> owerDataBase;
	
	/** The owner database selected. */
	private String ownerBDSelected;
	
	/** The table data base. */
	private List<InterfaceTableEngine> tableDataBaseList;
	
	/** The table bd selected. */
	private InterfaceTableEngine tableBdSelected;
	
	/** The column from table list. */
	private List<InterfaceColumnEngine> columnFromTableList;
	
	/** The column from table frozen list. */
	private List<InterfaceColumnEngine> columnFromTableFrozenList;	
	
	/** The columns selected. */
	private List<InterfaceColumnEngine> columnsSelected;
	
	/** The table selected list. */
	private List<InterfaceTableEngine> tableSelectedList;
	
	
	//DATOS DE LA INTERFACE
	
	/** The interface query. */
	private ExternalInterfaceQuery interfaceQuerySelected;

			
	//FILTERS QUERY

	/** The all column filter available. */
	private List<ExternalInterfaceColumn> allColumnFilterAvailable;	
	
	/** The table filter list. */
	private List<ExternalInterfaceTable> tableFilterList;	
	
	/** The table filter list. */
	private List<ExternalInterfaceTable> tableFilterListPredicate;

	/** The column filter list. */
	private List<ExternalInterfaceColumn> columnFilterList;	
	
	/** The column filter list. */
	private List<ExternalInterfaceColumn> columnFilterListPredicate;	
	
	/** The table filter. */
	private ExternalInterfaceTable tableFilter;
	
	/** The column filter. */
	private ExternalInterfaceFilter columnFilter;
		
	/** The interface query selected to filters. */
	private ExternalInterfaceQuery interfaceQuerySelectedToFilters;
	
	/** The predicate type column. */
	private Boolean predicateTypeColumn;

	/** The predicate type query. */
	private Boolean predicateTypeQuery;

	/** The predicate type value. */
	private Boolean predicateTypeValue;

	/** The predicate type user. */
	private Boolean predicateTypeUser;
	
	
	
	
	/**
	 * Gets the ower data base.
	 *
	 * @return the ower data base
	 */
	public List<String> getOwerDataBase() {
		return owerDataBase;
	}

	/**
	 * Sets the ower data base.
	 *
	 * @param owerDataBase the new ower data base
	 */
	public void setOwerDataBase(List<String> owerDataBase) {
		this.owerDataBase = owerDataBase;
	}

	/**
	 * Instantiates a new external interface attributtes.
	 */
	public ExternalInterfaceAttributtes() {
		super();
		columnFromTableFrozenList = new ArrayList<InterfaceColumnEngine>();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the table data base list.
	 *
	 * @return the table data base list
	 */
	public List<InterfaceTableEngine> getTableDataBaseList() {
		return tableDataBaseList;
	}

	/**
	 * Sets the table data base list.
	 *
	 * @param tableDataBaseList the new table data base list
	 */
	public void setTableDataBaseList(List<InterfaceTableEngine> tableDataBaseList) {
		this.tableDataBaseList = tableDataBaseList;
	}

	/**
	 * Gets the owner bd selected.
	 *
	 * @return the owner bd selected
	 */
	public String getOwnerBDSelected() {
		return ownerBDSelected;
	}

	/**
	 * Sets the owner bd selected.
	 *
	 * @param ownerBDSelected the new owner bd selected
	 */
	public void setOwnerBDSelected(String ownerBDSelected) {
		this.ownerBDSelected = ownerBDSelected;
	}

	/**
	 * Gets the table bd selected.
	 *
	 * @return the table bd selected
	 */
	public InterfaceTableEngine getTableBdSelected() {
		return tableBdSelected;
	}

	/**
	 * Sets the table bd selected.
	 *
	 * @param tableBdSelected the new table bd selected
	 */
	public void setTableBdSelected(InterfaceTableEngine tableBdSelected) {
		this.tableBdSelected = tableBdSelected;
	}

	/**
	 * Gets the column from table list.
	 *
	 * @return the column from table list
	 */
	public List<InterfaceColumnEngine> getColumnFromTableList() {
		return columnFromTableList;
	}

	/**
	 * Sets the column from table list.
	 *
	 * @param columnFromTableList the new column from table list
	 */
	public void setColumnFromTableList(
			List<InterfaceColumnEngine> columnFromTableList) {
		this.columnFromTableList = columnFromTableList;
	}

	/**
	 * Gets the columns selected.
	 *
	 * @return the columns selected
	 */
	public List<InterfaceColumnEngine> getColumnsSelected() {
		columnsSelected =  new ArrayList<InterfaceColumnEngine>();
		if(tableSelectedList != null && tableSelectedList.size() > 0){
			for(InterfaceTableEngine table : tableSelectedList){
				if(table.getColumnsList() != null && table.getColumnsList().size() > 0){
					for(InterfaceColumnEngine column :table.getColumnsList()){
						if(column.getOwner() != null && column.getTableName() != null && column.getColumnName()!= null && column.getIsSelected() != null && column.getIsSelected()){
							column.setTableName(table.getTableName());
							columnsSelected.add(column);
						}
					}
				}
			}
		}		
		return columnsSelected;
	}

	/**
	 * Sets the columns selected.
	 *
	 * @param columnsSelected the new columns selected
	 */
	public void setColumnsSelected(List<InterfaceColumnEngine> columnsSelected) {
		this.columnsSelected = columnsSelected;
	}

	/**
	 * Gets the table selected list.
	 *
	 * @return the table selected list
	 */
	public List<InterfaceTableEngine> getTableSelectedList() {
		return tableSelectedList;
	}

	/**
	 * Sets the table selected list.
	 *
	 * @param tableSelectedList the new table selected list
	 */
	public void setTableSelectedList(List<InterfaceTableEngine> tableSelectedList) {
		this.tableSelectedList = tableSelectedList;
	}

	/**
	 * Gets the interface query selected.
	 *
	 * @return the interface query selected
	 */
	public ExternalInterfaceQuery getInterfaceQuerySelected() {
		return interfaceQuerySelected;
	}

	/**
	 * Sets the interface query selected.
	 *
	 * @param interfaceQuerySelected the new interface query selected
	 */
	public void setInterfaceQuerySelected(
			ExternalInterfaceQuery interfaceQuerySelected) {
		this.interfaceQuerySelected = interfaceQuerySelected;
	}

	/**
	 * Gets the all column filter available.
	 *
	 * @return the all column filter available
	 */
	public List<ExternalInterfaceColumn> getAllColumnFilterAvailable() {
		return allColumnFilterAvailable;
	}

	/**
	 * Sets the all column filter available.
	 *
	 * @param allColumnFilterAvailable the new all column filter available
	 */
	public void setAllColumnFilterAvailable(
			List<ExternalInterfaceColumn> allColumnFilterAvailable) {
		this.allColumnFilterAvailable = allColumnFilterAvailable;
	}

	/**
	 * Gets the table filter list.
	 *
	 * @return the table filter list
	 */
	public List<ExternalInterfaceTable> getTableFilterList() {
		return tableFilterList;
	}

	/**
	 * Sets the table filter list.
	 *
	 * @param tableFilterList the new table filter list
	 */
	public void setTableFilterList(List<ExternalInterfaceTable> tableFilterList) {
		this.tableFilterList = tableFilterList;
	}

	/**
	 * Gets the column filter list.
	 *
	 * @return the column filter list
	 */
	public List<ExternalInterfaceColumn> getColumnFilterList() {
		return columnFilterList;
	}

	/**
	 * Sets the column filter list.
	 *
	 * @param columnFilterList the new column filter list
	 */
	public void setColumnFilterList(List<ExternalInterfaceColumn> columnFilterList) {
		this.columnFilterList = columnFilterList;
	}	

	/**
	 * Gets the column filter.
	 *
	 * @return the column filter
	 */
	public ExternalInterfaceFilter getColumnFilter() {
		return columnFilter;
	}

	/**
	 * Sets the column filter.
	 *
	 * @param columnFilter the new column filter
	 */
	public void setColumnFilter(ExternalInterfaceFilter columnFilter) {
		this.columnFilter = columnFilter;
	}

	/**
	 * Gets the table filter.
	 *
	 * @return the table filter
	 */
	public ExternalInterfaceTable getTableFilter() {
		return tableFilter;
	}

	/**
	 * Sets the table filter.
	 *
	 * @param tableFilter the new table filter
	 */
	public void setTableFilter(ExternalInterfaceTable tableFilter) {
		this.tableFilter = tableFilter;
	}

	/**
	 * Gets the interface query selected to filters.
	 *
	 * @return the interface query selected to filters
	 */
	public ExternalInterfaceQuery getInterfaceQuerySelectedToFilters() {
		return interfaceQuerySelectedToFilters;
	}

	/**
	 * Sets the interface query selected to filters.
	 *
	 * @param interfaceQuerySelectedToFilters the new interface query selected to filters
	 */
	public void setInterfaceQuerySelectedToFilters(
			ExternalInterfaceQuery interfaceQuerySelectedToFilters) {
		this.interfaceQuerySelectedToFilters = interfaceQuerySelectedToFilters;
	}

	/**
	 * Gets the column from table frozen list.
	 *
	 * @return the column from table frozen list
	 */
	public List<InterfaceColumnEngine> getColumnFromTableFrozenList() {
		return columnFromTableFrozenList;
	}

	/**
	 * Sets the column from table frozen list.
	 *
	 * @param columnFromTableFrozenList the new column from table frozen list
	 */
	public void setColumnFromTableFrozenList(
			List<InterfaceColumnEngine> columnFromTableFrozenList) {
		this.columnFromTableFrozenList = columnFromTableFrozenList;
	}

	/**
	 * Gets the predicate type column.
	 *
	 * @return the predicate type column
	 */
	public Boolean getPredicateTypeColumn() {
		return predicateTypeColumn;
	}

	/**
	 * Sets the predicate type column.
	 *
	 * @param predicateTypeColumn the new predicate type column
	 */
	public void setPredicateTypeColumn(Boolean predicateTypeColumn) {
		this.predicateTypeColumn = predicateTypeColumn;
	}

	/**
	 * Gets the predicate type query.
	 *
	 * @return the predicate type query
	 */
	public Boolean getPredicateTypeQuery() {
		return predicateTypeQuery;
	}

	/**
	 * Sets the predicate type query.
	 *
	 * @param predicateTypeQuery the new predicate type query
	 */
	public void setPredicateTypeQuery(Boolean predicateTypeQuery) {
		this.predicateTypeQuery = predicateTypeQuery;
	}

	/**
	 * Gets the predicate type value.
	 *
	 * @return the predicate type value
	 */
	public Boolean getPredicateTypeValue() {
		return predicateTypeValue;
	}

	/**
	 * Sets the predicate type value.
	 *
	 * @param predicateTypeValue the new predicate type value
	 */
	public void setPredicateTypeValue(Boolean predicateTypeValue) {
		this.predicateTypeValue = predicateTypeValue;
	}

	/**
	 * Gets the predicate type user.
	 *
	 * @return the predicate type user
	 */
	public Boolean getPredicateTypeUser() {
		return predicateTypeUser;
	}

	/**
	 * Sets the predicate type user.
	 *
	 * @param predicateTypeUser the new predicate type user
	 */
	public void setPredicateTypeUser(Boolean predicateTypeUser) {
		this.predicateTypeUser = predicateTypeUser;
	}

	/**
	 * Gets the table filter list predicate.
	 *
	 * @return the table filter list predicate
	 */
	public List<ExternalInterfaceTable> getTableFilterListPredicate() {
		return tableFilterListPredicate;
	}

	/**
	 * Sets the table filter list predicate.
	 *
	 * @param tableFilterListPredicate the new table filter list predicate
	 */
	public void setTableFilterListPredicate(
			List<ExternalInterfaceTable> tableFilterListPredicate) {
		this.tableFilterListPredicate = tableFilterListPredicate;
	}

	/**
	 * Gets the column filter list predicate.
	 *
	 * @return the column filter list predicate
	 */
	public List<ExternalInterfaceColumn> getColumnFilterListPredicate() {
		return columnFilterListPredicate;
	}

	/**
	 * Sets the column filter list predicate.
	 *
	 * @param columnFilterListPredicate the new column filter list predicate
	 */
	public void setColumnFilterListPredicate(
			List<ExternalInterfaceColumn> columnFilterListPredicate) {
		this.columnFilterListPredicate = columnFilterListPredicate;
	}

	/**
	 * Gets the table alias.
	 *
	 * @return the table alias
	 */
	public String getTableAlias() {
		if(this.getTableBdSelected() != null){
			Integer counter = 1;
			if( this.getInterfaceQuerySelected() != null &&
				this.getInterfaceQuerySelected().getExternalInterfaceTables() != null){
				
				List<ExternalInterfaceTable> listTables = this.getInterfaceQuerySelected().getExternalInterfaceTables();
				
				for(ExternalInterfaceTable table : listTables){
					String tableName = table.getTableName().split(KeywordsSQL.BLANK_SPACE)[0];
					if(tableName.equals(this.getTableBdSelected().getTableName())){
						counter++;
					}				
				}
			}
			
			return this.getTableBdSelected().getTableName().concat(KeywordsSQL.UNDERSCORE+counter);
		}
		return null;
	}
	
	
}