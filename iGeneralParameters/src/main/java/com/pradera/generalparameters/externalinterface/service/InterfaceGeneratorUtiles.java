package com.pradera.generalparameters.externalinterface.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceColumn;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceQuery;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceContentData;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionFileType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceGeneratorService.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 04/11/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InterfaceGeneratorUtiles extends CrudDaoServiceBean {

	/**
	 * Generate xml file.
	 *
	 * @param externalInterface the external interface
	 * @return the file
	 * @throws ServiceException the service exception
	 */
	public File generateXmlFile(ExternalInterface externalInterface) throws ServiceException{
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			Document xml = builder.newDocument();
			
			Element root = xml.createElement(ExtInterfaceConstanst.INTERFACE_NODE);
			xml.appendChild(root);
			
			//metadata
			Element template = xml.createElement(ExtInterfaceConstanst.TEMPLATE_NODE);		
			Integer counter = 1;
			for(ExternalInterfaceQuery interfaceQuery : externalInterface.getExternalInterfaceQuerys()){				
				Element row_counter = xml.createElement(ExtInterfaceConstanst.NODE+(counter));
				
				if(interfaceQuery.getExternalInterfaceQuery() == null){
					Element row_element = xml.createElement(ExtInterfaceConstanst.ROW_NODE);
					for(ExternalInterfaceTable interfaceTable: interfaceQuery.getExternalInterfaceTables()){					
						
						for(ExternalInterfaceColumn interfaceColumn : interfaceTable.getExternalInterfaceColumns()){						
							if(interfaceColumn.getIndVisible().equals(BooleanType.YES.getCode())){
								Element row_column = xml.createElement(ExtInterfaceConstanst.COLUMN_NODE);
								Text row_value = xml.createTextNode(interfaceColumn.getColumnNameAlias());
								row_column.appendChild(row_value);
								row_element.appendChild(row_column);
							}						
						}
						row_counter.appendChild(row_element);
					}
					for(ExternalInterfaceQuery interfaceQueryChild : externalInterface.getExternalInterfaceQuerys()){
						if(	interfaceQueryChild.getExternalInterfaceQuery() != null &&
							interfaceQueryChild.getExternalInterfaceQuery().equals(interfaceQuery)){
							counter++;
							buildMetadata(interfaceQueryChild, interfaceQuery, externalInterface, xml,row_counter, counter);							
						}
					}
					
					template.appendChild(row_counter);
				}
			}
			root.appendChild(template);
			
			//data
			Element data = xml.createElement(ExtInterfaceConstanst.DATA_NODE);
			
			List<InterfaceContentData> contentDataList = externalInterface.getInterfaceContentDatas();
			
			for(InterfaceContentData contentdata : contentDataList){
				Element iteration_template = xml.createElement(ExtInterfaceConstanst.ITERATION_NODE);
				counter = 1;
				//valido q la estructura sea correcta y la recorro
				if(contentdata.getDataOfQuery() == null || contentdata.getDataOfQuery().size() == 0){
					for(InterfaceContentData item_template : contentdata.getDetail()){
						Element item = xml.createElement(ExtInterfaceConstanst.NODE+(counter++));						
						buildContentData(item_template, xml, item, counter);						
						iteration_template.appendChild(item);
					}
				}
				else{
					throw new ServiceException();
				}								
				data.appendChild(iteration_template);
			}
			
			root.appendChild(data);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(xml);
			File file = File.createTempFile("file",InterfaceExtensionFileType.XML.getPrefix());
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);
			
			return file;
						
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Builds the content data.
	 *
	 * @param item_template the item_template
	 * @param xml the xml
	 * @param element the element
	 * @param counter the counter
	 */
	public void buildContentData(InterfaceContentData item_template, Document xml, Element element, Integer counter){			
		//la data es una cabecera
		if( (item_template.getDataOfQuery() != null &&
			item_template.getDataOfQuery().size() == 1 && 
			item_template.getDetail() == null) ||
			(item_template.getDataOfQuery() != null && 
			item_template.getDetail() != null)){
			
			Element header = xml.createElement(ExtInterfaceConstanst.ROW_NODE);
			if(item_template.getDataOfQuery() != null && item_template.getDataOfQuery().size() > 0){
				for(Object columnValue : item_template.getDataOfQuery().get(0)){
					Element row_column = xml.createElement(ExtInterfaceConstanst.COLUMN_NODE);
					Text row_value = xml.createTextNode(columnValue.toString());
					row_column.appendChild(row_value);
					header.appendChild(row_column);
				}
				element.appendChild(header);
			}else{
				Text empty = xml.createTextNode("");
				header.appendChild(empty);
				element.appendChild(header);
			}
		}
		else{//la data son detalles
			Element rows_detail = xml.createElement(ExtInterfaceConstanst.DATA_NODE);
			for(Object[] row : item_template.getDataOfQuery()){
				
				Element row_element = xml.createElement(ExtInterfaceConstanst.ROW_NODE);
				for(Object columnValue : row){
					Element row_column = xml.createElement(ExtInterfaceConstanst.COLUMN_NODE);
					Text row_value = xml.createTextNode(columnValue.toString());
					row_column.appendChild(row_value);
					row_element.appendChild(row_column);
				}
				rows_detail.appendChild(row_element);
			}
			element.appendChild(rows_detail);
		}
		
		if(item_template.getDetail() != null){//posible lista de detalles
			for(InterfaceContentData contentdata : item_template.getDetail()){
				Element item = xml.createElement(ExtInterfaceConstanst.NODE+(counter++));
				buildContentData(contentdata, xml, item, counter);
				element.appendChild(item);
			}
		}
	}
	
	/**
	 * Builds the metadata.
	 *
	 * @param interfaceQueryChild the interface query child
	 * @param interfaceQueryParent the interface query parent
	 * @param externalInterface the external interface
	 * @param xml the xml
	 * @param template the template
	 * @param counter the counter
	 */
	public void buildMetadata(	ExternalInterfaceQuery interfaceQueryChild, 
								ExternalInterfaceQuery interfaceQueryParent,
								ExternalInterface externalInterface,
								Document xml,Element template, Integer counter){		
		
		Element row_counter = xml.createElement(ExtInterfaceConstanst.NODE+(counter));		
		Element row_element = xml.createElement(ExtInterfaceConstanst.ROW_NODE);
		for(ExternalInterfaceTable interfaceTable: interfaceQueryChild.getExternalInterfaceTables()){					
			
			for(ExternalInterfaceColumn interfaceColumn : interfaceTable.getExternalInterfaceColumns()){						
				if(interfaceColumn.getIndVisible().equals(BooleanType.YES.getCode())){
					Element row_column = xml.createElement(ExtInterfaceConstanst.COLUMN_NODE);
					Text row_value = xml.createTextNode(interfaceColumn.getColumnNameAlias());
					row_column.appendChild(row_value);
					row_element.appendChild(row_column);
				}						
			}
			row_counter.appendChild(row_element);
		}			
		
		for(ExternalInterfaceQuery intQueryChild : externalInterface.getExternalInterfaceQuerys()){
			if(	intQueryChild.getExternalInterfaceQuery() != null &&
				intQueryChild.getExternalInterfaceQuery().equals(interfaceQueryChild)){
				counter++;
				buildMetadata(intQueryChild, interfaceQueryChild, externalInterface, xml,row_counter, counter);				
			}
		}
		
		template.appendChild(row_counter);
	}
	
	/**
	 * Gets the report format from file name.
	 *
	 * @param fileName the file name
	 * @return the report format from file name
	 */
	public ReportFormatType getReportFormatFromFileName(String fileName){
		String prefix = fileName.substring(fileName.lastIndexOf("."),fileName.length());
		for(ReportFormatType reportFormat: ReportFormatType.list){
			if(prefix.contains(reportFormat.getValue())){
				return reportFormat;
			}
		}
		return ReportFormatType.XML;
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @param partCode the part code
	 * @return the participant list
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipantList(String partCode) {
		try {
			String sqlQuery = "Select new com.pradera.model.accounts.Participant(p.idParticipantPk, p.state) From Participant p Where p.state = :registeredParam ";
			if(partCode!=null){
				sqlQuery+= "	And p.idParticipantCodePk = :partCode";
			}
			sqlQuery+="Order By p.idParticipantPk Asc";
			Query query = em.createQuery(sqlQuery);
			query.setParameter("registeredParam", ParticipantStateType.REGISTERED.getCode());
			if(partCode!=null){
				query.setParameter("partCode", new Long(partCode));
			}
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}

	/**
	 * Gets the aFP list.
	 *
	 * @param partCode the part code
	 * @return the aFP list
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getAFPList(String partCode) {
		try {
			String sqlQuery = "Select new com.pradera.model.accounts.Participant(p.idParticipantPk, p.state) From Participant p Where p.state = :registeredParam And p.accountClass = :classParam";
			if(partCode!=null){
				sqlQuery+= "	And p.idParticipantCodePk = :partCode";
			}
			Query query = em.createQuery(sqlQuery);
			query.setParameter("registeredParam", ParticipantStateType.REGISTERED.getCode());
			if(partCode!=null){
				query.setParameter("partCode", new Long(partCode));
			}
			query.setParameter("classParam", ParticipantClassType.AFP.getCode());
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
	
	/**
	 * Gets the issuer list.
	 *
	 * @param issuerPk the issuer pk
	 * @return the issuer list
	 */
	@SuppressWarnings("unchecked")
	public List<Issuer> getIssuerList(String issuerPk){
		try {
			String sqlQuery = "SELECT new com.pradera.model.issuancesecuritie.Issuer(I.idIssuerPk,I.stateIssuer) From Issuer I Where I.stateIssuer = :registeredStateParam";
			if(issuerPk!=null){
				sqlQuery+="	And I.idIssuerPk = :idIssuerPk";
			}
			Query query = em.createQuery(sqlQuery);
			query.setParameter("registeredStateParam", IssuerStateType.REGISTERED.getCode());
			
			if(issuerPk!=null){
				query.setParameter("idIssuerPk", issuerPk);
			}
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
	
	/**
	 * Gets the last anna correlative.
	 *
	 * @return the last anna correlative
	 */
	public String getLastAnnaCorrelative(){
		StringBuilder sqlQuery = new StringBuilder(); 
		sqlQuery.append("SELECT DISTINCT TRIM(TO_CHAR((NVL(MAX(ID_CORRELATIVE_ANNA_CODE),0) + 1),'9999,000000')) AS CORRELAT	"); 
		sqlQuery.append("FROM SECURITY_INTERFACE_ANNA																			");
		Query query = em.createNativeQuery(sqlQuery.toString());
		return (String) query.getSingleResult();
	}
	
	/**
	 * Gets the issuer list.
	 *
	 * @param holderPk the holder pk
	 * @return the issuer list
	 */
	@SuppressWarnings("unchecked")
	public List<Holder> getHolderList(String holderPk){
		try {
			String sqlQuery = "SELECT new com.pradera.model.accounts.Holder(H.idHolderPk,H.stateHolder) From Holder H Where H.stateHolder = :registeredStateParam And H.economicActivity in (:classParam1,:classParam2)";
			if(holderPk!=null){
				sqlQuery+="	And H.idHolderPk = :idHolderPk";
			}
			Query query = em.createQuery(sqlQuery);
			query.setParameter("registeredStateParam", HolderStateType.REGISTERED.getCode());
			
			if(holderPk!=null){
				query.setParameter("idHolderPk", holderPk);
			}
			query.setParameter("classParam1", EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode());
			query.setParameter("classParam2", EconomicActivityType.COMPANIAS_SEGURO.getCode());
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
}