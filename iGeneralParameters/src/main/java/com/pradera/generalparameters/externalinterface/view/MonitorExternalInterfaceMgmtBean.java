package com.pradera.generalparameters.externalinterface.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.generalparameters.externalinterface.facade.ExternalInterfaceServiceFacade;
import com.pradera.generalparameters.externalinterface.to.InterfaceProcessTO;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.InterfaceFileTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitorExternalInterfaceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MonitorExternalInterfaceMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The upload file types. */
	private String fUploadFileTypes = "xml|dat";
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The search interface process to. */
	private InterfaceProcessTO searchInterfaceProcessTO;
	
	/** The process interface process to. */
	private InterfaceProcessTO processInterfaceProcessTO;
	
	/** The lst search external interfaces. */
	private List<com.pradera.model.generalparameter.externalinterface.ExternalInterface> lstSearchExternalInterfaces;
	
	/** The lst process external interfaces. */
	private List<com.pradera.model.generalparameter.externalinterface.ExternalInterface> lstProcessExternalInterfaces;
	
	/** The lst interface type. */
	private List<ParameterTable> lstInterfaceType = new ArrayList<ParameterTable>();
	
	/** The lst web service process. */
	private GenericDataModel<InterfaceTransaction> lstWebServiceProcess;
	
	/** The map parameters. */
	private HashMap<Integer, String> mapParameters = new HashMap<Integer,String>();
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The external interface service facade. */
	@EJB
	private ExternalInterfaceServiceFacade externalInterfaceServiceFacade;
	
	/** The general pameters facade. */
	@EJB
	private GeneralParametersFacade generalPametersFacade;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The i process remote. */
	@Inject 
	ClientRestService clientRestService;
	
	/**
	 * Init.
	 */
	@PostConstruct
	public void init()
	{
		try {
			this.searchInterfaceProcessTO= new InterfaceProcessTO();
			this.searchInterfaceProcessTO.setProcessDate(CommonsUtilities.currentDate());
			this.processInterfaceProcessTO= new InterfaceProcessTO();
			//reception web service by default 
			searchInterfaceProcessTO.setInterfaceType(BooleanType.YES.getCode());
			processInterfaceProcessTO.setInterfaceType(BooleanType.YES.getCode());
			loadParameters();
		} catch (ServiceException e) {
			
		}
	}

	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParameters() throws ServiceException{
		ParameterTable parameterTableReception= new ParameterTable();
		parameterTableReception.setParameterTablePk(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableReception.setDescription(GeneralConstants.RECEPTION_DESCRIPTION);
		ParameterTable parameterTableSending= new ParameterTable();
		parameterTableSending.setParameterTablePk(GeneralConstants.TWO_VALUE_INTEGER);
		parameterTableSending.setDescription(GeneralConstants.SENDING_DESCRIPTION);
		lstInterfaceType.add(parameterTableReception);
		lstInterfaceType.add(parameterTableSending);
		
		mapParameters.put(parameterTableReception.getParameterTablePk(), parameterTableReception.getDescription());
		mapParameters.put(parameterTableSending.getParameterTablePk(), parameterTableSending.getDescription());
		
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setLstMasterTableFk(new ArrayList<Integer>());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.EXTERNAL_INTERFACE_STATE.getCode());
		
		List<ParameterTable> lstState = generalPametersFacade.getListParameterTableServiceBean(parameterFilter);
		for (ParameterTable parameterTable : lstState) {
			mapParameters.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
		
		lstSearchExternalInterfaces= externalInterfaceServiceFacade.getListExternalInterface(
													BooleanType.YES.getCode(), searchInterfaceProcessTO.getInterfaceType());
		lstProcessExternalInterfaces= externalInterfaceServiceFacade.getListExternalInterface(
													BooleanType.YES.getCode(), processInterfaceProcessTO.getInterfaceType());
		
		if(participantList == null || participantList.size() <= 0){
			List<Integer> states = new ArrayList<Integer>();
			states.add(ParticipantStateType.REGISTERED.getCode());
			states.add(ParticipantStateType.BLOCKED.getCode());
			Participant filter = new Participant();
			filter.setLstParticipantStates(states);
			setParticipantList(helperComponentFacade.getLisParticipantServiceFacade(filter));
		}
		
	}
	
	/**
	 * On change interface type.
	 */
	public void onChangeInterfaceType() {
		this.lstWebServiceProcess= null;
		lstSearchExternalInterfaces= externalInterfaceServiceFacade.getListExternalInterface(
													BooleanType.YES.getCode(), searchInterfaceProcessTO.getInterfaceType());
	}
	
	/**
	 * On change search web service.
	 */
	public void onChangeSearchWebService() {
		this.lstWebServiceProcess= null;
	}
	
	/**
	 * On change process web service.
	 */
	public void onChangeProcessWebService() {
		processInterfaceProcessTO.setFileName(null);
		processInterfaceProcessTO.setInterfaceFile(null);
	}
	
	/**
	 * On change process date.
	 */
	public void onChangeProcessDate() {
		this.lstWebServiceProcess= null;
		this.processInterfaceProcessTO= new InterfaceProcessTO();
		this.processInterfaceProcessTO.setInterfaceType(BooleanType.YES.getCode());
	}
	
	/**
	 * Clean monitoring.
	 */
	public void cleanMonitoring() {
		this.lstWebServiceProcess= null;
		this.searchInterfaceProcessTO= new InterfaceProcessTO();
		this.searchInterfaceProcessTO.setProcessDate(CommonsUtilities.currentDate());
		this.processInterfaceProcessTO= new InterfaceProcessTO();
		this.processInterfaceProcessTO.setInterfaceType(BooleanType.YES.getCode());
	}
	
	/**
	 * Search web service processes.
	 */
	public void searchWebServiceProcesses() {
		List<InterfaceTransaction> lstInterfaceTransaction= externalInterfaceServiceFacade.getListInterfaceTransaction(searchInterfaceProcessTO);
		this.lstWebServiceProcess= new GenericDataModel<>(lstInterfaceTransaction);
		this.processInterfaceProcessTO= new InterfaceProcessTO();
		this.processInterfaceProcessTO.setInterfaceType(BooleanType.YES.getCode());
	}
	
	/**
	 * Gets the processed file content.
	 *
	 * @param objInterfaceProcess the obj interface process
	 * @return the processed file content
	 */
	public StreamedContent getProcessedFileContent(InterfaceProcess objInterfaceProcess) {
		try {
			byte[] processedFile = externalInterfaceServiceFacade.getProcessedFileContent(objInterfaceProcess.getIdInterfaceProcessPk());
			if (Validations.validateIsNotNullAndNotEmpty(processedFile)) {
				return getStreamedContentFromFile(processedFile, null, objInterfaceProcess.getFileName()+GeneralConstants.XML_FORMAT);
			} else {
				showMessageOnDialog(null,null,PropertiesConstants.ALT_WEB_SERVICE_ERROR_DOWNLOADING_FILE,null);
	    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
			}
		} catch (Exception e) {
			showMessageOnDialog(null,null,PropertiesConstants.ALT_WEB_SERVICE_ERROR_DOWNLOADING_FILE,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	
	/**
	 * Gets the response file content.
	 *
	 * @param objInterfaceProcess the obj interface process
	 * @return the response file content
	 */
	public StreamedContent getResponseFileContent(InterfaceProcess objInterfaceProcess) {
		try {
			byte[] responseFile= externalInterfaceServiceFacade.getResponseFileContent(objInterfaceProcess.getIdInterfaceProcessPk());
			
			return getStreamedContentFromFile(responseFile, null, objInterfaceProcess.getFileName() +
							GeneralConstants.RESPONSE_FILE_SUFIX + GeneralConstants.XML_FORMAT);
		} catch (Exception e) {
			showMessageOnDialog(null,null,PropertiesConstants.ALT_WEB_SERVICE_ERROR_DOWNLOADING_FILE,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
			if (fDisplayName != null) {
				processInterfaceProcessTO.setFileName(fDisplayName);
				processInterfaceProcessTO.setInterfaceFile(event.getFile().getContents());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removefile handler.
	 *
	 * @param actionEvent the action event
	 */
	public void removefileHandler(ActionEvent actionEvent) {
		processInterfaceProcessTO.setFileName(null);
		processInterfaceProcessTO.setInterfaceFile(null);
	}
	
	/**
	 * Gets the file stream content.
	 *
	 * @param objInterfaceProcessTO the obj interface process to
	 * @return the file stream content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getFileStreamContent(InterfaceProcessTO objInterfaceProcessTO) throws IOException{
		StreamedContent streamedContentFile = null;
		if (Validations.validateIsNotNull(objInterfaceProcessTO.getInterfaceFile())) {
			InputStream inputStream = new ByteArrayInputStream(objInterfaceProcessTO.getInterfaceFile());
			streamedContentFile = new DefaultStreamedContent(inputStream,null, objInterfaceProcessTO.getFileName());
			inputStream.close();
		}

		return streamedContentFile;
	}
	
	/**
	 * Before process web service file.
	 */
	public void beforeProcessWebServiceFile(){
		JSFUtilities.setValidViewComponentAndChildrens(":frmWebServices");
		int countValidationErrors = 0;
		
		if(Validations.validateIsNullOrNotPositive(processInterfaceProcessTO.getIdExternalInterface())){
    		JSFUtilities.addContextMessage("frmWebServices:idProcessWebService",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ALT_WEB_SERVICE_NO_SELECTED), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ALT_WEB_SERVICE_NO_SELECTED));
			countValidationErrors++;
    	}
		
		if(Validations.validateIsNull(processInterfaceProcessTO.getInterfaceFile())){
    		JSFUtilities.addContextMessage("frmWebServices:fuplProcessFile",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ALT_INTERFACE_FILE_NO_SELECTED), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ALT_INTERFACE_FILE_NO_SELECTED));
			countValidationErrors++;
    	}
		
		if(countValidationErrors > 0) {
    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
    		return;
    	}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_WEBSERVICE_PROCESS_ASK));
		JSFUtilities.executeJavascriptFunction("PF('cnfwWebServiceFile').show();");
	}
	
	/**
	 * Process web service file.
	 */
	@LoggerAuditWeb
	public void processWebServiceFile(){	
		ExternalInterface externalInterface= externalInterfaceServiceFacade.findExternalInterface(processInterfaceProcessTO.getIdExternalInterface());		
		InterfaceFileTO interfaceFileTO= new InterfaceFileTO();
		interfaceFileTO.setInterfaceFile(processInterfaceProcessTO.getInterfaceFile());
		interfaceFileTO.setRelativeServiceUrl(externalInterface.getRelativeServiceUrl());				
		String xmlResponde = clientRestService.executeDepositaryWebResourse(interfaceFileTO);
		log.info(xmlResponde);		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_WEBSERVICE_PROCESS_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();
		cleanMonitoring();
	}
	
	
	/**
	 * Gets the search interface process to.
	 *
	 * @return the search interface process to
	 */
	public InterfaceProcessTO getSearchInterfaceProcessTO() {
		return searchInterfaceProcessTO;
	}

	/**
	 * Sets the search interface process to.
	 *
	 * @param interfaceProcessTO the new search interface process to
	 */
	public void setSearchInterfaceProcessTO(InterfaceProcessTO interfaceProcessTO) {
		this.searchInterfaceProcessTO = interfaceProcessTO;
	}


	/**
	 * Gets the lst search external interfaces.
	 *
	 * @return the lst search external interfaces
	 */
	public List<com.pradera.model.generalparameter.externalinterface.ExternalInterface> getLstSearchExternalInterfaces() {
		return lstSearchExternalInterfaces;
	}

	/**
	 * Sets the lst search external interfaces.
	 *
	 * @param lstExternalInterfaces the new lst search external interfaces
	 */
	public void setLstSearchExternalInterfaces(
			List<com.pradera.model.generalparameter.externalinterface.ExternalInterface> lstExternalInterfaces) {
		this.lstSearchExternalInterfaces = lstExternalInterfaces;
	}

	/**
	 * Gets the map parameters.
	 *
	 * @return the map parameters
	 */
	public HashMap<Integer, String> getMapParameters() {
		return mapParameters;
	}

	/**
	 * Set map parameters.
	 *
	 * @param mapParameters the map parameters
	 */
	public void setMapParameters(HashMap<Integer, String> mapParameters) {
		this.mapParameters = mapParameters;
	}

	/**
	 * Gets the lst interface type.
	 *
	 * @return the lst interface type
	 */
	public List<ParameterTable> getLstInterfaceType() {
		return lstInterfaceType;
	}

	/**
	 * Sets the lst interface type.
	 *
	 * @param lstInterfaceType the new lst interface type
	 */
	public void setLstInterfaceType(List<ParameterTable> lstInterfaceType) {
		this.lstInterfaceType = lstInterfaceType;
	}

	/**
	 * Gets the lst web service process.
	 *
	 * @return the lst web service process
	 */
	public GenericDataModel<InterfaceTransaction> getLstWebServiceProcess() {
		return lstWebServiceProcess;
	}

	/**
	 * Sets the lst web service process.
	 *
	 * @param lstWebServiceProcess the new lst web service process
	 */
	public void setLstWebServiceProcess(
			GenericDataModel<InterfaceTransaction> lstWebServiceProcess) {
		this.lstWebServiceProcess = lstWebServiceProcess;
	}

	/**
	 * Gets the process interface process to.
	 *
	 * @return the process interface process to
	 */
	public InterfaceProcessTO getProcessInterfaceProcessTO() {
		return processInterfaceProcessTO;
	}

	/**
	 * Sets the process interface process to.
	 *
	 * @param processInterfaceProcessTO the new process interface process to
	 */
	public void setProcessInterfaceProcessTO(
			InterfaceProcessTO processInterfaceProcessTO) {
		this.processInterfaceProcessTO = processInterfaceProcessTO;
	}

	/**
	 * Gets the lst process external interfaces.
	 *
	 * @return the lst process external interfaces
	 */
	public List<com.pradera.model.generalparameter.externalinterface.ExternalInterface> getLstProcessExternalInterfaces() {
		return lstProcessExternalInterfaces;
	}

	/**
	 * Sets the lst process external interfaces.
	 *
	 * @param lstProcessExternalInterfaces the new lst process external interfaces
	 */
	public void setLstProcessExternalInterfaces(
			List<com.pradera.model.generalparameter.externalinterface.ExternalInterface> lstProcessExternalInterfaces) {
		this.lstProcessExternalInterfaces = lstProcessExternalInterfaces;
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}
	
}
