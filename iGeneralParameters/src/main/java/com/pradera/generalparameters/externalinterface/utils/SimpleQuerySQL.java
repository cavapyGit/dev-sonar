package com.pradera.generalparameters.externalinterface.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class SimpleQuerySQL.
 *
 * @author Allen
 */
public class SimpleQuerySQL {
	
	/** The simple query. */
	private String simpleQuery;
	
	/** The select. */
	protected String select;
	
	/** The from. */
	private String from;
	
	/** The where. */
	private String where;
	
	/** The select params list. */
	private List<String> selectParamsList;
	
	/** The from params list. */
	private List<String> fromParamsList;
	
	/** The where params list. */
	private List<String> whereParamsList;
	
	/** The relationships. */
	private Map<String,List<String>> relationships; 
	
	/** The relation alias table. */
	private Map<String,String> relationAliasTable;
	
	/** The relation alias column. */
	private Map<String,Map<String,String>> relationAliasColumn;
	
	/** The table list for alias. */
	public static List<String> tableListForAlias = new ArrayList<String>();
	
	/** The column list for alias. */
	private List<String> columnListForAlias;
	
	/**
	 * Instantiates a new simple query sql.
	 */
	public SimpleQuerySQL() {		
	}
	
	/**
	 * Instantiates a new simple query sql.
	 *
	 * @param simpleQuery the simple query
	 */
	public SimpleQuerySQL(String simpleQuery) {
		this.simpleQuery = simpleQuery;	
		
		extractLexicalGroup();
		extractParams();
	}

	/**
	 * Extract lexical group.
	 */
	protected void extractLexicalGroup() {
		select = null;
		from = null;
		where = null;
		
		int fromIndex = 0;
		int whereIndex = 0;
		
		if (simpleQuery.startsWith(KeywordsSQL.SELECT)) {
			fromIndex = simpleQuery.indexOf(KeywordsSQL.FROM);
			select = simpleQuery.substring(KeywordsSQL.SELECT.length(), fromIndex);
			
			whereIndex = simpleQuery.indexOf(KeywordsSQL.WHERE);
			if(whereIndex == -1){
				from = simpleQuery.substring(fromIndex+KeywordsSQL.FROM.length());
			}else{
				from = simpleQuery.substring(fromIndex+KeywordsSQL.FROM.length(), whereIndex);
				where = simpleQuery.substring(whereIndex+KeywordsSQL.WHERE.length());
			}
		}
	}
	
	/**
	 * Extract params.
	 */
	protected void extractParams(){	
		selectParamsList = new ArrayList<String>();
		fromParamsList = new ArrayList<String>();
		whereParamsList = new ArrayList<String>();
				
		if(select != null && from != null){
			for (String f : from.split(KeywordsSQL.COLUMN_SEPARATOR)) {
				fromParamsList.add(validateElementFrom(f));
			}
			for (String s : select.split(KeywordsSQL.COLUMN_SEPARATOR)) {
				selectParamsList.add(validateElementSelect(s));
			}			
			if(where != null){
				for (String w : where.split(KeywordsSQL.COLUMN_SEPARATOR)) {
					whereParamsList.add(w);
				}
			}
		}
	}
	
	
	/**
	 * Validate element from.
	 *
	 * @param element the element
	 * @return the string
	 */
	private String validateElementFrom(String element) {
		if(element != null){
			String[] splitElement = element.trim().split(KeywordsSQL.BLANK_SPACE);//[tabla , alias]
			if(splitElement.length == 2){
				relationAliasTable.put(splitElement[1].trim(), splitElement[0].trim());//.put(alias , tabla)
				return splitElement[0].trim();
			}
		}
		return "";
	}

	/**
	 * Validate element select.
	 *
	 * @param element the element
	 * @return the string
	 */
	private String validateElementSelect(String element) {
		if(element != null){
			String[] splitElement = element.trim().split(KeywordsSQL.AS);//[columna , alias]
			if(splitElement.length == 1 && element.trim().contains(KeywordsSQL.BLANK_SPACE)){//si la columna utiliza espacio en blanco para separar el alias
				splitElement = element.trim().split(KeywordsSQL.BLANK_SPACE);//[columna , alias]
			}
			if(splitElement.length == 1 || splitElement.length == 2){//(nombre de columna) o (nombre de columna y alias)
				String[] splitElementSelect = splitElement[0].split("\\".concat(KeywordsSQL.DOT));//[alias tabla , columna]				
				if(splitElementSelect.length ==2){
					String tableAlias = splitElementSelect[0].trim();
					String tableName = relationAliasTable.get(tableAlias);
					if(tableName != null){
						List<String> columns = relationships.get(tableAlias);
						if(columns == null){
							columns = new ArrayList<String>();
						}
						columns.add(splitElementSelect[1].trim());
						relationships.put(tableAlias, columns);
						
						if(relationAliasColumn.get(tableAlias) == null){
							Map<String,String> columnMap = new HashMap<String, String>();
							columnMap.put(splitElementSelect[1].trim(), splitElement.length == 2 ? splitElement[1].trim() : "");
							relationAliasColumn.put(tableAlias,columnMap);
						}else{
							relationAliasColumn.get(tableAlias).put(splitElementSelect[1].trim(), splitElement.length == 2 ? splitElement[1].trim() : "");
						}
						return splitElementSelect[1].trim();
					}
				}
			}
		}
		return "";
	}

	/**
	 * Adds the select from params.
	 *
	 * @param group the group
	 */
	protected void addSelectFromParams(Entry<String, List<String>> group){
		
		String[] tableNameAndAlias = group.getKey().split(KeywordsSQL.BLANK_SPACE);
		String tableName = tableNameAndAlias[0];		
		String aliasTable = tableNameAndAlias[1];
		
		for(String column : group.getValue()){
			String aliasColumn = generateColumnAlias(column);
			selectParamsList.add(aliasTable.concat(KeywordsSQL.DOT).concat(column).concat(KeywordsSQL.BLANK_SPACE)
					.concat(KeywordsSQL.AS).concat(KeywordsSQL.BLANK_SPACE).concat(aliasColumn));
		}
		fromParamsList.add(tableName.concat(KeywordsSQL.BLANK_SPACE).concat(aliasTable));
	}
	
	/**
	 * Generate lexical group.
	 */
	public void generateLexicalGroup() {		
		if(selectParamsList != null && selectParamsList.size() > 0 &&
			fromParamsList != null && fromParamsList.size() > 0){
			
			StringBuilder selectBuilder = new StringBuilder(KeywordsSQL.SELECT.concat(KeywordsSQL.BLANK_SPACE));
			for(String elementSelect : selectParamsList){
				selectBuilder.append(elementSelect.concat(KeywordsSQL.COLUMN_SEPARATOR));
			}			
			select = selectBuilder.substring(0, selectBuilder.length()-KeywordsSQL.COLUMN_SEPARATOR.length());
			
			StringBuilder fromBuilder = new StringBuilder(KeywordsSQL.FROM.concat(KeywordsSQL.BLANK_SPACE));			
			for(String elementFrom : fromParamsList){
				fromBuilder.append(elementFrom.concat(KeywordsSQL.COLUMN_SEPARATOR));
			}
			from = fromBuilder.substring(0, fromBuilder.length()-KeywordsSQL.COLUMN_SEPARATOR.length());
		}		
		updateSimpleQuery();
	}
	
	/**
	 * Update simple query.
	 */
	private void updateSimpleQuery() {
		if(select != null && from != null){
			simpleQuery = select.concat(KeywordsSQL.BLANK_SPACE).concat(from).toUpperCase();
		}		
		
	}

	/**
	 * Update simple query with where.
	 */
	public void updateSimpleQueryWithWhere(){
		if(whereParamsList != null && whereParamsList.size() > 0){
			StringBuilder whereBuilder = new StringBuilder(KeywordsSQL.WHERE.concat(KeywordsSQL.BLANK_SPACE));
			for(String elementWhere : whereParamsList){
				whereBuilder.append(elementWhere.concat(KeywordsSQL.BLANK_SPACE));
			}
			where = whereBuilder.toString();
		}
		
		if(where != null){
			String[] query = simpleQuery.split(KeywordsSQL.WHERE);
			if(query.length > 0){
				simpleQuery = query[0].concat(KeywordsSQL.BLANK_SPACE).concat(where).toUpperCase();
			}
		}
	}
	/**
	 * Generate alias.
	 *
	 * @param key the key
	 * @return the string
	 */
	protected static String generateTableAlias(String key) {	
		Integer counter = 0; 
		StringBuilder tableName = new StringBuilder();
		key = key.split(KeywordsSQL.UNDERSCORE)[0];
		if(key.length() > KeywordsSQL.ALIAS_LENGTH){
			tableName.append(key.substring(0,KeywordsSQL.ALIAS_LENGTH));
		}else{
			tableName.append(key);
		}
		for(String table : tableListForAlias){
			if(table.equalsIgnoreCase(tableName.toString())){
				counter++;
			}
		}
		tableListForAlias.add(tableName.toString());
		
		tableName.append(KeywordsSQL.UNDERSCORE);
		tableName.append(counter.toString());
		
		return tableName.toString();
	}
	
	/**
	 * Generate column alias.
	 *
	 * @param key the key
	 * @return the string
	 */
	private String generateColumnAlias(String key) {	
		Integer counter = 0; 
		StringBuilder columnName = new StringBuilder();
		key = key.split(KeywordsSQL.UNDERSCORE)[0];
		if(key.length() > KeywordsSQL.ALIAS_LENGTH){
			columnName.append(key.substring(0,KeywordsSQL.ALIAS_LENGTH));
		}else{
			columnName.append(key);
		}
		
		for(String column : columnListForAlias){
			if(column.equalsIgnoreCase(key)){
				counter++;
			}
		}
		columnListForAlias.add(key);
		
		columnName.append(KeywordsSQL.UNDERSCORE);		
		columnName.append(counter.toString());
		
		return columnName.toString();
	}

	/* getters and setters */
	
	/**
	 * Gets the simple query.
	 *
	 * @return the simple query
	 */
	public String getSimpleQuery() {
		return simpleQuery;
	}
	
	/**
	 * Sets the simple query.
	 *
	 * @param simpleQuery the new simple query
	 */
	public void setSimpleQuery(String simpleQuery) {
		this.simpleQuery = simpleQuery;
	}	
	
	/**
	 * Gets the select params list.
	 *
	 * @return the select params list
	 */
	public List<String> getSelectParamsList() {
		return selectParamsList;
	}
	
	/**
	 * Sets the select params list.
	 *
	 * @param selectParamsList the new select params list
	 */
	public void setSelectParamsList(List<String> selectParamsList) {
		this.selectParamsList = selectParamsList;
	}
	
	/**
	 * Gets the from params list.
	 *
	 * @return the from params list
	 */
	public List<String> getFromParamsList() {
		return fromParamsList;
	}
	
	/**
	 * Sets the from params list.
	 *
	 * @param fromParamsList the new from params list
	 */
	public void setFromParamsList(List<String> fromParamsList) {
		this.fromParamsList = fromParamsList;
	}
	
	/**
	 * Gets the where params list.
	 *
	 * @return the where params list
	 */
	public List<String> getWhereParamsList() {
		return whereParamsList;
	}
	
	/**
	 * Sets the where params list.
	 *
	 * @param whereParamsList the new where params list
	 */
	public void setWhereParamsList(List<String> whereParamsList) {
		this.whereParamsList = whereParamsList;
	}

	/**
	 * Gets the table list for alias.
	 *
	 * @return the table list for alias
	 */
	public List<String> getTableListForAlias() {
		return tableListForAlias;
	}

	/**
	 * Sets the table list for alias.
	 *
	 * @param tableListForAlias the new table list for alias
	 */
	public void setTableListForAlias(List<String> tableListForAlias) {
		this.tableListForAlias = tableListForAlias;
	}

	/**
	 * Gets the column list for alias.
	 *
	 * @return the column list for alias
	 */
	public List<String> getColumnListForAlias() {
		return columnListForAlias;
	}

	/**
	 * Sets the column list for alias.
	 *
	 * @param columnListForAlias the new column list for alias
	 */
	public void setColumnListForAlias(List<String> columnListForAlias) {
		this.columnListForAlias = columnListForAlias;
	}

	/**
	 * Gets the relationships.
	 *
	 * @return the relationships
	 */
	public Map<String, List<String>> getRelationships() {
		return relationships;
	}

	/**
	 * Sets the relationships.
	 *
	 * @param relationships the relationships
	 */
	public void setRelationships(Map<String, List<String>> relationships) {
		this.relationships = relationships;
	}

	/**
	 * Gets the relation alias table.
	 *
	 * @return the relation alias table
	 */
	public Map<String, String> getRelationAliasTable() {
		return relationAliasTable;
	}

	/**
	 * Sets the relation alias table.
	 *
	 * @param relationAliasTable the relation alias table
	 */
	public void setRelationAliasTable(Map<String, String> relationAliasTable) {
		this.relationAliasTable = relationAliasTable;
	}

	/**
	 * Gets the relation alias column.
	 *
	 * @return the relation alias column
	 */
	public Map<String, Map<String, String>> getRelationAliasColumn() {
		return relationAliasColumn;
	}

	/**
	 * Sets the relation alias column.
	 *
	 * @param relationAliasColumn the relation alias column
	 */
	public void setRelationAliasColumn(
			Map<String, Map<String, String>> relationAliasColumn) {
		this.relationAliasColumn = relationAliasColumn;
	}
}