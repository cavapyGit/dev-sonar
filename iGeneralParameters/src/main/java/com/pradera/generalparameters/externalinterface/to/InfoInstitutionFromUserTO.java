package com.pradera.generalparameters.externalinterface.to;

import java.io.Serializable;

public class InfoInstitutionFromUserTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idParticipant;
	private String mnemonic;
	private String descriptionInstitution;
	
	public InfoInstitutionFromUserTO(){}
	
	public Long getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	public String getDescriptionInstitution() {
		return descriptionInstitution;
	}
	public void setDescriptionInstitution(String descriptionInstitution) {
		this.descriptionInstitution = descriptionInstitution;
	}
	
	
}
