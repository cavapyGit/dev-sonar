package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.generalparameters.externalinterface.facade.ExternalInterfaceServiceFacade;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.externalinterface.utils.KeywordsSQL;
import com.pradera.generalparameters.externalinterface.utils.SqlController;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceFilterPredicate;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceTemplateDetail;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceColumn;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceExtension;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceFilter;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceQuery;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTemplate;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceUserType;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceColumnEngine;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceTableEngine;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceExecutionType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceColumnStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceFilterStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfacePredicateStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfacePredicateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTableStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceUserTypeStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
@DepositaryWebBean
public class ExternalInterfaceMgmt extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3231593381938351294L;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The interface service facade. */
	@EJB
	ExternalInterfaceServiceFacade interfaceServiceFacade;
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The external interface session. */
	private ExternalInterface externalInterfaceSession;//Entidad principal a persistir.
	
	/** The interface attributes. */
	private ExternalInterfaceAttributtes interfaceAttributes;//Clase que interactura solo con la vista.
	
	// DATOS GENERALES
	
	/** The execution type list. */
	private List<ParameterTable> executionTypeParameterList;
	
	// FORMATOS DE LA INTERFAZ
	
	/** The user type list. */
	private List<ExternalInterfaceUserType> userTypeList;
	
	/** The extension type list. */
	private List<ExternalInterfaceExtension> extensionTypeList;
	
	/** The interface template list. */
	private List<ExternalInterfaceTemplate> interfaceTemplateList;
	
	// DATOS DE LA INTERFACE
		
	/** The column type parameter list. */
	private List<ParameterTable> columnTypeParameterList;
	
	/** The operator logic type parameter list. */
	private List<ParameterTable> operatorLogicTypeParameterList;
	
	/** The sql comparator type parameter list. */
	private List<ParameterTable> sqlComparatorTypeParameterList;
	
	/** The predicate type parameter list. */
	private List<ParameterTable> predicateTypeParameterList;
	
	/** The external interface type parameter list. */
	private List<ParameterTable> externalInterfaceStateParameterList;
	
	/** The ext interface send hour parameter list. */
	private List<ParameterTable> extInterfaceSendHourParameterList;
	
	/** The ext interface format date parameter list. */
	private List<ParameterTable> extInterfaceFormatDateParameterList;
		
	/** The external interface search. */
	private ExternalInterfaceTO externalInterfaceSearch;
	
	/** The external interface list search. */
	private GenericDataModel<ExternalInterfaceTO> externalInterfaceListSearch;
	
	/** The external interface search selected. */
	private ExternalInterfaceTO externalInterfaceSearchSelected;
		
	/** The detail template order priority. */
	private ExtInterfaceTemplateDetail nextInterfaceTemplateDetail;
	
	/** The interface template table editing. */
	private ExternalInterfaceTable interfaceTemplateTableEditing;
		
	/** The interface query preview. */
	private ExternalInterfaceQuery interfaceQueryPreview;
	
	/** The external interface filter editing. */
	private ExternalInterfaceFilter externalInterfaceFilterEditing;
		
	/** The activate separator attribute. */
	private Boolean activateSeparatorAttribute;
	
	/** The activate filters query. */
	private Boolean activateFiltersQuery;
	
	/** The active send periodicity. */
	private Boolean activeSendPeriodicity;
	
	/** The interface to generate id. */
	private Long interfaceToGenerateId;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		loadUserSessionPrivileges();	
		loadParametersTable();
		loadExtensionTypeList();
		loadUserTypeList();
		loadTemplateList();	
		
		externalInterfaceSearch = new ExternalInterfaceTO();	
		resetExternalInterface();	
		
		setActivateSeparatorAttribute(Boolean.FALSE);
		setActivateFiltersQuery(Boolean.FALSE);
	}
	
	/* LOAD COMPONENTS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/**
	 * Load user session privileges.
	 */
	private void loadUserSessionPrivileges(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	/**
	 * Load parameters table.
	 */
	private void loadParametersTable(){
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		parameterTableTO.setState(1);
		try {
			parameterTableTO.setMasterTableFk(MasterTableType.EXTERNAL_INTERFACE_EXECUTION_TYPE.getCode());
			executionTypeParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.EXTERNAL_INTERFACE_COLUMN_TYPE.getCode());
			columnTypeParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.INTERFACE_PREDICATE_TYPE.getCode());
			predicateTypeParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.INTERFACE_OPERATOR_LOGIC_TYPE.getCode());
			operatorLogicTypeParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.SQL_COMPARATOR_TYPE.getCode());
			sqlComparatorTypeParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.EXTERNAL_INTERFACE_STATE_TYPE.getCode());
			externalInterfaceStateParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.EXTERNAL_INTERFACE_SEND_HOUR.getCode());
			extInterfaceSendHourParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.EXTERNAL_INTERFACE_FORMAT_DATE.getCode());
			extInterfaceFormatDateParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Load extension type list.
	 */
	private void loadExtensionTypeList(){
		try{
			extensionTypeList = interfaceServiceFacade.getExtensionTypeListFacade();
		}catch(ServiceException ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load user type list.
	 */
	private void loadUserTypeList(){
		userTypeList = interfaceServiceFacade.getUserTypeListFacade();
	}
	/**
	 * Load template list.
	 */
	private void loadTemplateList() {
		try{
			interfaceTemplateList = interfaceServiceFacade.getInterfaceTemplateListFacade();
		}catch(ServiceException ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load interface tables handler.
	 */
	public void loadInterfaceTablesHandler(){
		String owner = interfaceAttributes.getOwnerBDSelected();
		try {
			List<InterfaceTableEngine> tableList = interfaceServiceFacade.getTablesFromOwnerFacade(owner);
			interfaceAttributes.setTableDataBaseList(tableList);		
			interfaceAttributes.setTableBdSelected(new InterfaceTableEngine());
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	/**
	 * Load owner data base list.
	 * method to load owner database from oracle
	 */
	private void loadOwnerDataBaseList() {
		List<String> ownerList = null;
		try {			
			ownerList = interfaceServiceFacade.getOwnerDatabaseFacade();//Obtengo la lista de nombres de las Bases de Datos.
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		interfaceAttributes.setOwerDataBase(ownerList);
	}
	/* BEFORE ACTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/**
	 * Before unblock external interface.
	 */
	public void beforeUnblockExternalInterface(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_UNBLOCK, PropertiesConstants.EXTERNAL_INTERFACE_UNBLOCK_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmUnblockExternalInterface').show();");
	}
	
	/**
	 * Before block external interface.
	 */
	public void beforeBlockExternalInterface(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_BLOCK, PropertiesConstants.EXTERNAL_INTERFACE_BLOCK_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmBlockExternalInterface').show();");
	}
	
	/**
	 * Before confirm external interface.
	 */
	public void beforeConfirmExternalInterface(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_CONFIRM_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmConfirmExternalInterface').show();");
	}
	
	/**
	 * Before annular external interface.
	 */
	public void beforeAnnularExternalInterface(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ANNUL, PropertiesConstants.EXTERNAL_INTERFACE_ANNULAR_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmAnnularExternalInterface').show();");
	}
	
	/**
	 * Before modify external interface.
	 */
	public void beforeModifyExternalInterface(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_MODIFY, PropertiesConstants.EXTERNAL_INTERFACE_MODIFY_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmModifyExternalInterface').show();");
	}
	
	/**
	 * Before save external interface.
	 */
	public void beforeSaveExternalInterface(){		
		if(externalInterfaceSession.getInterfaceExtensionsSelected() == null || 
				externalInterfaceSession.getInterfaceExtensionsSelected().size() == 0){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_EXTENSION);
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			return;
		}
		if(externalInterfaceSession.getInterfaceUserTypesSelected() == null || 
				externalInterfaceSession.getInterfaceUserTypesSelected().size() == 0){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_USERTYPE);
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show()");
			return;
		}
		if(externalInterfaceSession.getExternalInterfaceQuerys() == null || externalInterfaceSession.getExternalInterfaceQuerys().size() == 0){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_QUERYS);
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			return;
		}
		Boolean contentUserTypeFilter = Boolean.FALSE;
		for(ExternalInterfaceQuery query : externalInterfaceSession.getExternalInterfaceQuerys()){
			if(query.getInterfaceTemplateDetail().getContainQuery() == null
				|| !query.getInterfaceTemplateDetail().getContainQuery()){
				showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_QUERYS);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				return;
			}
			if(query.getInterfaceTemplateDetail().getContainFilter() == null
				|| !query.getInterfaceTemplateDetail().getContainFilter()){
				showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_FILTERS);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				return;
			}
			ContentUserTypeFor:
			for(ExternalInterfaceTable table : query.getExternalInterfaceTables()){
				for(ExternalInterfaceFilter filter : table.getExternalInterfaceFilters()){
					if(filter.getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.USER_TYPE.getCode())){
						contentUserTypeFilter = Boolean.TRUE;
						break ContentUserTypeFor;
					}
				}
			}
		}
		//validacion para que la plantilla contenga al menos un filtro de tipo Usuario
		/*if(!contentUserTypeFilter){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_FILTER_USER_TYPE);
			JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationPar.show()");
			return;
		}*/
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_REGISTER_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmSaveExternalInterface').show();");
	}
	
	/**
	 * Before send external interface.
	 *
	 * @param externalInterfaceId the external interface id
	 */
	public void beforeSendExternalInterface(Long externalInterfaceId){
		setInterfaceToGenerateId(externalInterfaceId);
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrSendExternalInterface').show();");
	}
	
	/**
	 * Save external interface.
	 */
	@LoggerAuditWeb
	public void saveExternalInterface(){
		try{
			ExternalInterface externalInterface = this.externalInterfaceSession;	
			externalInterface.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			
			List<ExternalInterfaceExtension> extensions = new ArrayList<ExternalInterfaceExtension>();
			for(String extension : externalInterface.getInterfaceExtensionsSelected()){
				interfaceExtensionFor:
				for(ExternalInterfaceExtension interfaceExtension : extensionTypeList) {
					if(interfaceExtension.getExtensionFileType().getExtensionFile().equals(extension)){
						interfaceExtension.setExternalInterface(externalInterface);
						interfaceExtension.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						interfaceExtension.setRegistryDate(CommonsUtilities.currentDate());
						interfaceExtension.setInterfaceExtensionState(InterfaceExtensionStateType.REGISTERED.getCode());
						extensions.add(interfaceExtension);
						break interfaceExtensionFor;
					}
				}
			}
			externalInterface.setExternalInterfaceExtensions(extensions);
			
			List<ExternalInterfaceUserType> userTypes = new ArrayList<ExternalInterfaceUserType>();
			for(String userType : externalInterface.getInterfaceUserTypesSelected()){
				interfaceUserTypeFor:
				for(ExternalInterfaceUserType interfaceUserType : userTypeList) {
					if(interfaceUserType.getUserTypeDescription().equals(userType)){
						interfaceUserType.setExternalInterface(externalInterface);
						interfaceUserType.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						interfaceUserType.setRegistryDate(CommonsUtilities.currentDate());
						interfaceUserType.setInterfaceUserState(InterfaceUserTypeStateType.REGISTERED.getCode());
						userTypes.add(interfaceUserType);
						break interfaceUserTypeFor;
					}
				}
			}
			externalInterface.setExternalInterfaceUserTypes(userTypes);			
			
			for(ExternalInterfaceQuery query : externalInterface.getExternalInterfaceQuerys()){
				query.setExternalInterface(externalInterface);
				query.setRegisterUser(userInfo.getUserAccountSession().getUserName());
				query.setRegisterDate(CommonsUtilities.currentDate());
				query.setInterfaceQueryState(ExternalInterfaceReceptionStateType.REGISTERED.getCode());
				Integer counter = 1;
				for(ExternalInterfaceTable table : query.getExternalInterfaceTables()){
					table.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					table.setRegistryDate(CommonsUtilities.currentDate());
					table.setInterfaceTableState(InterfaceTableStateType.REGISTERED.getCode());
								
					for(ExternalInterfaceColumn column : table.getExternalInterfaceColumns()){
						column.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						column.setRegistryDate(CommonsUtilities.currentDate());
						column.setInterfaceColumnState(InterfaceColumnStateType.REGISTERED.getCode());						
						column.setOrderPriority(counter++);
						if(column.getIsSelected()){
							column.setIndVisible(BooleanType.YES.getCode());
						}else{
							column.setIndVisible(BooleanType.NO.getCode());
						}						
						if(column.getDefaultValue() == null || column.getDefaultValue().isEmpty()){
							column.setDefaultValue(KeywordsSQL.NO_DEFINE);
						}
					}
					
					for(ExternalInterfaceFilter filter : table.getExternalInterfaceFilters()){
						filter.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						filter.setRegistryDate(CommonsUtilities.currentDate());
						filter.setInterfaceFilterState(InterfaceFilterStateType.REGISTERED.getCode());
						
						filter.getInterfaceFilterPredicate().setRegistryDate(CommonsUtilities.currentDate());
						filter.getInterfaceFilterPredicate().setRegistryUser(userInfo.getUserAccountSession().getUserName());
						filter.getInterfaceFilterPredicate().setInterfacePredicState(InterfacePredicateStateType.REGISTERED.getCode());
						filter.getInterfaceFilterPredicate().setExternalInterfaceFilter(filter);
						filter.setExtInterfaceFilterPredicates(Arrays.asList(filter.getInterfaceFilterPredicate()));
					}
				}
			}
			externalInterface = interfaceServiceFacade.registryInterfaceFacade(externalInterface);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_REGISTER_CONFIRM_OK,new Object[]{externalInterface.getIdExtInterfacePk().toString()});
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show();");
						
			resetExternalInterface();
			externalInterfaceSearch = new ExternalInterfaceTO();	
			setActivateSeparatorAttribute(Boolean.FALSE);
			setActivateFiltersQuery(Boolean.FALSE);
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Confirm external interface.
	 */
	@LoggerAuditWeb
	public void confirmExternalInterface(){
		try{
			ExternalInterface externalInterface = this.externalInterfaceSession;						
			externalInterface = interfaceServiceFacade.confirmExternalInterface(externalInterface);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_CONFIRM_CONFIRM_OK,new Object[]{externalInterface.getIdExtInterfacePk().toString()});
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show();");
			searchExternalInterface();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Annular external interface.
	 */
	@LoggerAuditWeb
	public void annularExternalInterface(){
		try{
			ExternalInterface externalInterface = this.externalInterfaceSession;						
			externalInterface = interfaceServiceFacade.annularExternalInterface(externalInterface);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_ANNULAR_CONFIRM_OK,new Object[]{externalInterface.getIdExtInterfacePk().toString()});
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show();");
			searchExternalInterface();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Block external interface.
	 */
	@LoggerAuditWeb
	public void blockExternalInterface(){
		try{
			ExternalInterface externalInterface = this.externalInterfaceSession;						
			externalInterface = interfaceServiceFacade.blockExternalInterface(externalInterface);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_BLOCK_CONFIRM_OK,new Object[]{externalInterface.getIdExtInterfacePk().toString()});
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show();");
			searchExternalInterface();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Unblock external interface.
	 */
	@LoggerAuditWeb
	public void unblockExternalInterface(){
		try{
			ExternalInterface externalInterface = this.externalInterfaceSession;						
			externalInterface = interfaceServiceFacade.unblockExternalInterface(externalInterface);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_UNBLOCK_CONFIRM_OK,new Object[]{externalInterface.getIdExtInterfacePk().toString()});
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show();");
			searchExternalInterface();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Modify external interface.
	 */
	@LoggerAuditWeb
	public void modifyExternalInterface(){
		try{
			ExternalInterface externalInterface = this.externalInterfaceSession;						
			externalInterface = interfaceServiceFacade.modifyExternalInterface(externalInterface);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_MODIFY_CONFIRM_OK,new Object[]{externalInterface.getIdExtInterfacePk().toString()});
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show();");
			searchExternalInterface();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	/* ACTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/**
	 * Reset search external interface.
	 */
	public void resetSearchExternalInterface(){
		externalInterfaceSearchSelected = null;
		externalInterfaceListSearch = null;
		externalInterfaceSearch = new ExternalInterfaceTO();				
	}
	/**
	 * Reset external interface.
	 */
	public void resetExternalInterface(){
		externalInterfaceSession = interfaceServiceFacade.getInterfaceReseted();
		String[] hour = extInterfaceSendHourParameterList.get(0).getParameterName().split(KeywordsSQL.COLON);
		externalInterfaceSession.getSendPeriocity().setHours(Integer.parseInt(hour[0]));
		externalInterfaceSession.getSendPeriocity().setMinutes(Integer.parseInt(hour[1]));
		interfaceAttributes = new ExternalInterfaceAttributtes();
	}
	/**
	 * Change interface template.
	 */
	public void changeInterfaceTemplate(){
		Long templatePk = externalInterfaceSession.getExternalInterfaceTemplate().getIdExtInferfaceTemplatePk();
		
		try{			
			if(	templatePk != null && templatePk > 0){						
				externalInterfaceSession.setExternalInterfaceTemplate(interfaceServiceFacade.getInterfaceTemplateFacade(templatePk));				
				interfaceAttributes.setInterfaceQuerySelected(null);
				interfaceAttributes.setInterfaceQuerySelectedToFilters(null);
				externalInterfaceSession.setExternalInterfaceQuerys(null);

				setActivateFiltersQuery(Boolean.FALSE);
			}
		}catch(ServiceException ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}	
	/**
	 * Active separator attribute.
	 */
	public void activeSeparatorAttribute(){
		if(externalInterfaceSession.getInterfaceExtensionsSelected() != null){
			if(externalInterfaceSession.getInterfaceExtensionsSelected().size() > 0){
				for(String extension : externalInterfaceSession.getInterfaceExtensionsSelected()){
					if(extension.contains(InterfaceExtensionFileType.TXT.getPrefix()) || InterfaceExtensionFileType.TXT.getPrefix().contains(extension)){
						externalInterfaceSession.setSeparatorAttribute("");
						setActivateSeparatorAttribute(Boolean.TRUE);
						return;
					}
				}
			}
			setActivateSeparatorAttribute(Boolean.FALSE);
		}
	}
	/**
	 * Edits the external interface query.
	 *
	 * @param detail the detail
	 */
	public void editExternalInterfaceQuery(ExtInterfaceTemplateDetail detail){
		//el Query existe y lo estan modificando
		if(	Validations.validateIsNotNull(interfaceAttributes.getInterfaceQuerySelected()) &&
			Validations.validateIsNotNull(interfaceAttributes.getInterfaceQuerySelected().getInterfaceTemplateDetail()) &&
			interfaceAttributes.getInterfaceQuerySelected().getInterfaceTemplateDetail().getIdExtInterfaceTempDetPk().equals(detail.getIdExtInterfaceTempDetPk()) ){
			return;			
		}
		//el Query existe y lo han seleccionado para modificarlo
		if( externalInterfaceSession.getExternalInterfaceQuerys() != null &&
			externalInterfaceSession.getExternalInterfaceQuerys().size() > 0){
			
			for(ExternalInterfaceQuery externalInterfaceQuery : externalInterfaceSession.getExternalInterfaceQuerys()){				
				if(externalInterfaceQuery.getInterfaceTemplateDetail().equals(detail)){
					
					interfaceAttributes.setInterfaceQuerySelected(externalInterfaceQuery);
					interfaceAttributes.setTableSelectedList(new ArrayList<InterfaceTableEngine>());
					
					if(externalInterfaceQuery.getExternalInterfaceTables()!= null && externalInterfaceQuery.getExternalInterfaceTables().size()>0){
						
						List<InterfaceTableEngine> tableSelectedList = new ArrayList<InterfaceTableEngine>();
						
						for(ExternalInterfaceTable table : externalInterfaceQuery.getExternalInterfaceTables()){
							InterfaceTableEngine tableEngineTemp = new InterfaceTableEngine();
							tableEngineTemp.setTableName(table.getTableName());
							try {
								tableEngineTemp.setColumnsList(interfaceServiceFacade.getColumnsFromTableFacade(table.getTableName()));
							} catch (ServiceException ex) {
								excepcion.fire(new ExceptionToCatchEvent(ex));
							}
							
							if(table.getExternalInterfaceColumns() != null && table.getExternalInterfaceColumns().size() > 0){								
								for(ExternalInterfaceColumn column : table.getExternalInterfaceColumns()){	
									forColumEngine:
									for(InterfaceColumnEngine columEngine : tableEngineTemp.getColumnsList()){
										if(columEngine.getColumnName().equals(column.getColumnName())){
											columEngine.setIsSelected(Boolean.TRUE);
											break forColumEngine;
										}
									}									
								}
							}
							tableSelectedList.add(tableEngineTemp);							
						}
						
						interfaceAttributes.setTableSelectedList(tableSelectedList);
					}					
					return;
				}
			}
		}
		//el Query no existe		
		interfaceAttributes = new ExternalInterfaceAttributtes();
		
		ExternalInterfaceQuery externalInterfaceQuery = new ExternalInterfaceQuery();
		externalInterfaceQuery.setInterfaceTemplateDetail(detail);		
		interfaceAttributes.setInterfaceQuerySelected(externalInterfaceQuery);
	}
	/**
	 * Validate query handler.
	 */
	public void validateQueryHandler(){
		try{
			String query = null;
			if(interfaceAttributes.getColumnFilter() != null &&
			   interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate() != null){
				
				query =  interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateValue();
			}else{
				query = interfaceAttributes.getInterfaceQuerySelected().getInterfaceTemplateQuery();
			}
			interfaceServiceFacade.validatedQueryToExecuteService(query);		
			JSFUtilities.executeJavascriptFunction("PF('confirmQueryValidatedW').show();");
		}catch(ServiceException ex){
			if(ex.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, ex.getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	/**
	 * Generate query from dialog.
	 */
	public void generateQueryFromDialog(){
		try {
			ExternalInterfaceQuery interfaceQuery = null;
			interfaceQuery = interfaceServiceFacade.getExternalInterfaceWithColumns(interfaceAttributes.getInterfaceQuerySelected());			
			interfaceAttributes.setInterfaceQuerySelected(interfaceQuery);
			interfaceAttributes.getInterfaceQuerySelected().setIndQueryGenerated(BooleanType.YES.getCode());
			
			if(interfaceAttributes.getTableSelectedList() == null){
				interfaceAttributes.setTableSelectedList(new ArrayList<InterfaceTableEngine>()); 
			}
			for(ExternalInterfaceTable table : interfaceQuery.getExternalInterfaceTables()){
				InterfaceTableEngine tableEngineTemp = new InterfaceTableEngine();
				tableEngineTemp.setTableName(table.getTableName());
				tableEngineTemp.setColumnsList(interfaceServiceFacade.getColumnsFromTableFacade(table.getTableName()));
				for(ExternalInterfaceColumn columInterface : table.getExternalInterfaceColumns()){				
					columnFor:
					for(InterfaceColumnEngine column : tableEngineTemp.getColumnsList()){
						if(column.getColumnName().equals(columInterface.getColumnName())){
							column.setIsSelected(Boolean.TRUE);
							break columnFor;
						}
					}					
				}
				interfaceAttributes.getTableSelectedList().add(tableEngineTemp);
			}
			
			JSFUtilities.executeJavascriptFunction("PF('inputSqlDlgW').hide();");
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, ex.getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	/**
	 * Adds the query to detail template.
	 */
	public void applyQueryToDetailTemplate(){
		if(externalInterfaceSession.getExternalInterfaceQuerys()==null){
			externalInterfaceSession.setExternalInterfaceQuerys(new ArrayList<ExternalInterfaceQuery>());
		}
			
		Integer orderPriority = interfaceAttributes.getInterfaceQuerySelected().getInterfaceTemplateDetail().getOrderPriority();
		
		for(ExtInterfaceTemplateDetail detail : externalInterfaceSession.getExternalInterfaceTemplate().getExtInterfaceTemplateDetails()){
			if(detail.getOrderPriority().equals(orderPriority)){
				detail.setContainQuery(Boolean.TRUE);
				break;
			}
		}
		
		for(ExternalInterfaceQuery query : externalInterfaceSession.getExternalInterfaceQuerys()){
			if(query.getInterfaceTemplateDetail().getOrderPriority().equals(orderPriority)){					
				for(ExternalInterfaceTable interfaceTable : interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables()){			
					for(ExternalInterfaceColumn column : interfaceTable.getExternalInterfaceColumns()){
						paramFor:
						for(ParameterTable param : columnTypeParameterList){
							if(param.getParameterTablePk().equals(column.getDataTypeTarget())){
								column.setTypeTargetValue(param.getParameterName());
								break paramFor;
							}
						}
					}
				}
				query = interfaceAttributes.getInterfaceQuerySelected();				
				interfaceAttributes = new ExternalInterfaceAttributtes();	
				return;
			}
		}
		
		for(ExternalInterfaceTable interfaceTable : interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables()){			
			for(ExternalInterfaceColumn column : interfaceTable.getExternalInterfaceColumns()){
				paramFor:
				for(ParameterTable param : columnTypeParameterList){
					if(param.getParameterTablePk().equals(column.getDataTypeTarget())){
						column.setTypeTargetValue(param.getParameterName());
						break paramFor;
					}
				}
			}
		}
				
		externalInterfaceSession.getExternalInterfaceQuerys().add(interfaceAttributes.getInterfaceQuerySelected());
		interfaceAttributes = new ExternalInterfaceAttributtes();	
	}
	/**
	 * Select interface tables handler.
	 */
	public void selectInterfaceTablesHandler(){
		//validamos la tabla seleccionada
		if( interfaceAttributes.getTableBdSelected() != null && 
			interfaceAttributes.getTableBdSelected().getTableName() != null){
			
			String tableName = interfaceAttributes.getTableBdSelected().getTableName();
			
			//verificamos si ya existen tablas seleccionadas anteriormente
			if( interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables() != null &&
				interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables().size() > 0){
				//actualizamos la lista de alias de tablas
				SqlController.tableListForAlias = new ArrayList<String>();
				for(ExternalInterfaceTable tableInterface : interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables()){
					SqlController.getTableAlias(tableInterface.getTableName().split(KeywordsSQL.BLANK_SPACE)[0].trim());
				}
			}
			//la tabla a sido seleccionada por primera ves, la creamos, y la añadimos a las seleccionadas
			ExternalInterfaceTable table = new ExternalInterfaceTable();
			String tableAlias = SqlController.getTableAlias(tableName);
			table.setTableName(tableName.concat(KeywordsSQL.BLANK_SPACE).concat(tableAlias));		
			table.setInterfaceTableState(InterfaceTableStateType.REGISTERED.getCode());
			
			if(interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables() == null){
				interfaceAttributes.getInterfaceQuerySelected().setExternalInterfaceTables(new ArrayList<ExternalInterfaceTable>());
			}
			
			interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables().add(table);
			interfaceAttributes.setTableBdSelected(new InterfaceTableEngine());
		}
	}
	/**
	 * Edits the interface table handler.
	 *
	 * @param table the table
	 */
	public void editInterfaceTableHandler(ExternalInterfaceTable table){
		try {			
			InterfaceTableEngine tableEngineTemp = null;
			String tableName = table.getTableName().split(KeywordsSQL.BLANK_SPACE)[0];
			
			if(interfaceAttributes.getTableSelectedList() == null){
				interfaceAttributes.setTableSelectedList(new ArrayList<InterfaceTableEngine>());
			}else{
				for(InterfaceTableEngine tableEngine : interfaceAttributes.getTableSelectedList()){
					if(tableEngine.getTableName().equals(table.getTableName())){
						tableEngineTemp = tableEngine;
						break;
					}
				}
			}
			
			if(tableEngineTemp == null){
				tableEngineTemp = new InterfaceTableEngine();
				tableEngineTemp.setTableName(table.getTableName());//el nombre de la tableEngine es igual al InterfaceTable
				tableEngineTemp.setColumnsList(interfaceServiceFacade.getColumnsFromTableFacade(tableName));
				interfaceAttributes.getTableSelectedList().add(tableEngineTemp);
			}
			interfaceAttributes.setColumnFromTableList(tableEngineTemp.getColumnsList());
			setInterfaceTemplateTableEditing(table);
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	/**
	 * Removes the interface table handler.
	 *
	 * @param table the table
	 */
	public void removeInterfaceTableHandler(ExternalInterfaceTable table){
		interfaceAttributes.getInterfaceQuerySelected().getExternalInterfaceTables().remove(table);
		
		if(interfaceAttributes.getTableSelectedList() != null){
			for(InterfaceTableEngine tableEngine : interfaceAttributes.getTableSelectedList()){
				if(tableEngine.getTableName().equals(table.getTableName())){
					interfaceAttributes.getTableSelectedList().remove(tableEngine);
					break;
				}
			}
		}
		
		if(getInterfaceTemplateTableEditing() != null){
			if(getInterfaceTemplateTableEditing().getTableName().equals(table.getTableName())){
				setInterfaceTemplateTableEditing(new ExternalInterfaceTable());
				interfaceAttributes.setColumnFromTableList(new ArrayList<InterfaceColumnEngine>());	
			}
		}
		
	}
	/**
	 * Generate query from table form.
	 */
	public void generateQueryFromTableForm(){
		try {
			ExternalInterfaceQuery interfaceQuery = null;
			if(interfaceAttributes.getColumnsSelected() != null && interfaceAttributes.getColumnsSelected().size() > 0){
				interfaceQuery = interfaceServiceFacade.getQueryFromColumnsFacade(interfaceAttributes.getInterfaceQuerySelected(), interfaceAttributes.getColumnsSelected());				
				interfaceAttributes.setInterfaceQuerySelected(interfaceQuery);
				interfaceAttributes.setColumnFromTableList(new ArrayList<InterfaceColumnEngine>());
			}
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, ex.getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}		
	}
	
	
	
		
		
	/**
	 * Change interface table filter predicate.
	 */
	public void changeInterfaceTableFilterPredicate(){
		List<ExternalInterfaceColumn> columnList = new ArrayList<ExternalInterfaceColumn>();
		if(interfaceAttributes.getAllColumnFilterAvailable() != null){
			for(ExternalInterfaceColumn column : interfaceAttributes.getAllColumnFilterAvailable()){				
				ExternalInterfaceTable  tableFilterPredicate = interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getExternalInterfaceColumn().getExternalInterfaceTable();
				if( tableFilterPredicate != null &&
					column.getExternalInterfaceTable().getTableName().equals(tableFilterPredicate.getTableName())){
					columnList.add(column);
				}
			}
			interfaceAttributes.setColumnFilterListPredicate(new ArrayList<ExternalInterfaceColumn>());
			interfaceAttributes.getColumnFilterListPredicate().addAll(columnList);			
		}		
	}
	
	/**
	 * Cancel predicate to filter.
	 */
	public void cancelPredicateToFilter(){
		if(interfaceAttributes.getColumnFilter() != null &&
		   interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate() != null){
			interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().setPredicateType(0);
		}		
		JSFUtilities.executeJavascriptFunction("PF('interfacePredicateDialogW').hide();");
	}
	/**
	 * Delete interface query filter.
	 *
	 * @param columnFilter the column filter
	 */
	public void deleteInterfaceQueryFilter(ExternalInterfaceFilter columnFilter){
		if(interfaceAttributes.getInterfaceQuerySelectedToFilters() != null &&
			interfaceAttributes.getInterfaceQuerySelectedToFilters().getExternalInterfaceFilters() != null){
			String sqlQueryModify = interfaceServiceFacade.removeFilterToSqlQuery(columnFilter, interfaceAttributes.getInterfaceQuerySelectedToFilters());
			interfaceAttributes.getInterfaceQuerySelectedToFilters().setInterfaceTemplateQuery(sqlQueryModify);
			interfaceAttributes.getInterfaceQuerySelectedToFilters().getExternalInterfaceFilters().remove(columnFilter);
		}
	}
		
	/**
	 * Adds the filters to query.
	 */
	public void addFiltersToQuery(){
		if( externalInterfaceSession.getExternalInterfaceQuerys() == null ||
			externalInterfaceSession.getExternalInterfaceQuerys().size() < externalInterfaceSession.getExternalInterfaceTemplate().getExtInterfaceTemplateDetails().size()){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_QUERYS);
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			return;
		}
		setActivateFiltersQuery(Boolean.TRUE);		
		interfaceAttributes.setTableFilter(new ExternalInterfaceTable());
		
		resetExternalInterfaceFilter();
		
		//Lista de todas las columnas seleccionadas en DATOS DE LA INTERFAZ
		List<ExternalInterfaceColumn> columnList = new ArrayList<ExternalInterfaceColumn>();	
		for(ExternalInterfaceQuery query : externalInterfaceSession.getExternalInterfaceQuerys()){
			for(ExternalInterfaceTable table : query.getExternalInterfaceTables()){
				for(ExternalInterfaceColumn column : table.getExternalInterfaceColumns()){
					table.setExternalInterfaceQuery(query);
					column.setExternalInterfaceTable(table);
					columnList.add(column);
				}
			}
		}
		
				
		interfaceAttributes.setAllColumnFilterAvailable(new ArrayList<ExternalInterfaceColumn>());
		interfaceAttributes.getAllColumnFilterAvailable().addAll(columnList);	
	}
	/**
	 * Reset external interface filter.
	 */
	public void resetExternalInterfaceFilter(){
		interfaceAttributes.setColumnFilter(new ExternalInterfaceFilter());
		interfaceAttributes.getColumnFilter().setExternalInterfaceTable(new ExternalInterfaceTable());
		interfaceAttributes.getColumnFilter().setInterfaceFilterPredicate(new ExtInterfaceFilterPredicate());

		interfaceAttributes.setPredicateTypeColumn(Boolean.FALSE);
		interfaceAttributes.setPredicateTypeQuery(Boolean.FALSE);
		interfaceAttributes.setPredicateTypeValue(Boolean.FALSE);
		interfaceAttributes.setPredicateTypeUser(Boolean.FALSE);
		
		setExternalInterfaceFilterEditing(null);
	}
	/**
	 * Edits the external interface filter.
	 *
	 * @param query the query
	 */
	public void editExternalInterfaceFilter(ExternalInterfaceQuery query){	
		if(interfaceAttributes.getInterfaceQuerySelectedToFilters() == null){
			interfaceAttributes.setInterfaceQuerySelectedToFilters(new ExternalInterfaceQuery());
			interfaceAttributes.getInterfaceQuerySelectedToFilters().setExternalInterfaceFilters(new ArrayList<ExternalInterfaceFilter>());
		}
		
		if(interfaceAttributes.getInterfaceQuerySelectedToFilters().getInterfaceTemplateDetail() != null &&
		   query.getInterfaceTemplateDetail().equals(interfaceAttributes.getInterfaceQuerySelectedToFilters().getInterfaceTemplateDetail())){
			return;
		}
		
		interfaceAttributes.setInterfaceQuerySelectedToFilters(query);		
		interfaceAttributes.setTableFilterList(query.getExternalInterfaceTables());
		interfaceAttributes.setColumnFilterList(null);
		resetExternalInterfaceFilter();
	}
	/**
	 * Change interface table filter.
	 */
	public void changeInterfaceTableFilter(){
		List<ExternalInterfaceColumn> columnList = new ArrayList<ExternalInterfaceColumn>();
		if(interfaceAttributes.getAllColumnFilterAvailable() != null){
			for(ExternalInterfaceColumn column : interfaceAttributes.getAllColumnFilterAvailable()){				
				if(	column.getExternalInterfaceTable().getTableName().equals(interfaceAttributes.getColumnFilter().getExternalInterfaceTable().getTableName()) &&
					interfaceAttributes.getInterfaceQuerySelectedToFilters().equals(column.getExternalInterfaceTable().getExternalInterfaceQuery())){
					columnList.add(column);
				}
			}
			interfaceAttributes.setColumnFilterList(null);
			interfaceAttributes.setColumnFilterList(columnList);
			//interfaceAttributes.getColumnFilterList().addAll(columnList);
		}		
	}
	/**
	 * Change query predicate type.
	 */
	public void changeQueryPredicateType(){
		interfaceAttributes.setPredicateTypeColumn(Boolean.FALSE);
		interfaceAttributes.setPredicateTypeQuery(Boolean.FALSE);
		interfaceAttributes.setPredicateTypeValue(Boolean.FALSE);
		interfaceAttributes.setPredicateTypeUser(Boolean.FALSE);
	}
	/**
	 * Adds the filter to detail template.
	 */
	public void addFilterToDetailTemplate(){
		if(getExternalInterfaceFilterEditing() != null){
			setExternalInterfaceFilterEditing(null);			
		}
		if( interfaceAttributes.getInterfaceQuerySelectedToFilters() != null){
			
			Integer predicateType = interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType();
			//verifico que se haya llenado el predicado			
			if(predicateType.equals(InterfacePredicateType.COLUMN.getCode())){
				ExternalInterfaceColumn predicate = interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getExternalInterfaceColumn();
				if(predicate.getColumnName() == null || predicate.getColumnName().isEmpty()){
					
					showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_PREDICATE);
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
					return;
				}
			}else{
				if(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateValue() == null
				   || interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateValue().isEmpty()){
					
					showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_VALIDATION_PREDICATE);
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
					return;
				}
			}
			ExternalInterfaceFilter filter = new ExternalInterfaceFilter();
			//operador logico inicial
			for(ParameterTable pt : operatorLogicTypeParameterList) {
				if(pt.getParameterTablePk().equals(interfaceAttributes.getColumnFilter().getStartLogicalOperator())){
					filter.setStartLogicalOperator(pt.getParameterTablePk());
					filter.setLogicalOperator(pt.getParameterName());
					break;
				}
			}
			//tabla asociada al filtro
			for(ExternalInterfaceColumn columnTable : interfaceAttributes.getAllColumnFilterAvailable()){
				if( interfaceAttributes.getColumnFilter().getExternalInterfaceTable().getTableName().equals(
						columnTable.getExternalInterfaceTable().getTableName()) &&
					interfaceAttributes.getInterfaceQuerySelectedToFilters().equals(
						columnTable.getExternalInterfaceTable().getExternalInterfaceQuery())){
					filter.setExternalInterfaceTable(columnTable.getExternalInterfaceTable());
					break;
				}
			}
			//columna asociada al filtro
			filter.setInterfaceFilterColumn(interfaceAttributes.getColumnFilter().getInterfaceFilterColumn());
			//tipo comparador con el predicado
			filter.setInterfaceFilterPredicate(new ExtInterfaceFilterPredicate());
			for(ParameterTable pt : sqlComparatorTypeParameterList) {
				if(pt.getParameterTablePk().equals(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getComparisonOperator())){
					filter.getInterfaceFilterPredicate().setComparisonOperator(pt.getParameterTablePk());
					filter.getInterfaceFilterPredicate().setComparisonOperatorValue(pt.getParameterName());
					break;
				}
			}
			//tipo de predicado a comparar			
			for(ParameterTable pt : predicateTypeParameterList) {
				if(pt.getParameterTablePk().equals(predicateType)){	
					filter.getInterfaceFilterPredicate().setPredicateType(pt.getParameterTablePk());
					filter.getInterfaceFilterPredicate().setPredicateTypeValue(pt.getParameterName());
					break;
				}
			}
			//si el predicado es de tipo Columa
			if(predicateType.equals(InterfacePredicateType.COLUMN.getCode())){				
				//Columna asociado al predicado 
				StringBuilder predicateColumValue = new StringBuilder();
				for(ExternalInterfaceQuery query : externalInterfaceSession.getExternalInterfaceQuerys()){
					if(query.getInterfaceTemplateDetail().getIdExtInterfaceTempDetPk().equals(
					   interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getDetailTemplateParent().getIdExtInterfaceTempDetPk())){
					
					   filter.getInterfaceFilterPredicate().setDetailTemplateParent(query.getInterfaceTemplateDetail());	
					   predicateColumValue.append("[");
					   predicateColumValue.append(query.getInterfaceTemplateDetail().getOrderPriority());
					   predicateColumValue.append("]");		
					   predicateColumValue.append(KeywordsSQL.BLANK_SPACE);
					   predicateColumValue.append(query.getInterfaceTemplateDetail().getDescription());			 
					   predicateColumValue.append(KeywordsSQL.BLANK_SPACE);
					   predicateColumValue.append(KeywordsSQL.GREATER);
					   predicateColumValue.append(KeywordsSQL.BLANK_SPACE);
					   
						for(ExternalInterfaceTable table : query.getExternalInterfaceTables()){
							if(table.getTableName().equals(
								interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getExternalInterfaceColumn().getExternalInterfaceTable().getTableName())){
								predicateColumValue.append(table.getTableName());
								predicateColumValue.append(KeywordsSQL.DOT);
								for(ExternalInterfaceColumn column : table.getExternalInterfaceColumns()){
									if(column.getColumnName().equals(
										interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getExternalInterfaceColumn().getColumnName())){
										predicateColumValue.append(column.getColumnName());
										filter.getInterfaceFilterPredicate().setExternalInterfaceColumn(column);
										
										if(!interfaceAttributes.getInterfaceQuerySelectedToFilters().equals(query)){
											predicateColumValue.append("[");
											predicateColumValue.append(ExtInterfaceConstanst.IDENTIFICATOR);	
											predicateColumValue.append("]");
										}
										filter.getInterfaceFilterPredicate().setPredicateValueColumn(predicateColumValue.toString());
										break;
									}
								}
							}
						}
					}
				}
			}
			else if(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.QUERY.getCode())
					|| interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.VALUE.getCode())
					|| interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.USER_TYPE.getCode())){
				
				filter.getInterfaceFilterPredicate().setPredicateValue(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateValue());
			}	
			if(interfaceAttributes.getInterfaceQuerySelectedToFilters().getExternalInterfaceFilters() == null){
				interfaceAttributes.getInterfaceQuerySelectedToFilters().setExternalInterfaceFilters(new ArrayList<ExternalInterfaceFilter>());
			}
			
			try {
				String sqlQuery = interfaceServiceFacade.addFilterToSqlQuery(filter, interfaceAttributes.getInterfaceQuerySelectedToFilters().getInterfaceTemplateQuery());
				interfaceAttributes.getInterfaceQuerySelectedToFilters().setInterfaceTemplateQuery(sqlQuery);
				
				interfaceAttributes.getInterfaceQuerySelectedToFilters().getExternalInterfaceFilters().add(filter);
				resetExternalInterfaceFilter();
				interfaceAttributes.setColumnFilterList(null);
				JSFUtilities.executeJavascriptFunction("PF('interfacePredicateDialogW').hide();");
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}
	}	
	/**
	 * Open interface predicate dialog.
	 */
	public void openInterfacePredicateDialog(){	
		changeQueryPredicateType();
		
		setExternalInterfaceFilterEditing(null);
		
		for(ParameterTable pt : operatorLogicTypeParameterList) {
			if(pt.getParameterTablePk().equals(interfaceAttributes.getColumnFilter().getStartLogicalOperator())){
				interfaceAttributes.getColumnFilter().setLogicalOperator(pt.getParameterName());
				break;
			}
		}
		for(ParameterTable pt : predicateTypeParameterList) {
			if(pt.getParameterTablePk().equals(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType())){
				interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().setPredicateTypeValue(pt.getParameterName());
				break;
			}
		}
		for(ParameterTable pt : sqlComparatorTypeParameterList) {
			if(pt.getParameterTablePk().equals(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getComparisonOperator())){
				interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().setComparisonOperatorValue(pt.getParameterName());
				break;
			}
		}
		
		if(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.COLUMN.getCode())){
			interfaceAttributes.setPredicateTypeColumn(Boolean.TRUE);
			
			List<ExternalInterfaceQuery> queryList = new ArrayList<ExternalInterfaceQuery>();			
			for(ExternalInterfaceQuery query : externalInterfaceSession.getExternalInterfaceQuerys()){
				if(query.getInterfaceTemplateDetail().getOrderPriority() <= 
				interfaceAttributes.getInterfaceQuerySelectedToFilters().getInterfaceTemplateDetail().getOrderPriority()){
					queryList.add(query);
				}
			}			
			interfaceAttributes.getInterfaceQuerySelectedToFilters().setExternalInterfaceQueries(queryList);
			
			ExternalInterfaceTable table = new ExternalInterfaceTable();				
			ExternalInterfaceColumn columnPredicate =  new ExternalInterfaceColumn();
			columnPredicate.setExternalInterfaceTable(table);	
			
			interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().setExternalInterfaceColumn(columnPredicate);
			interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().setDetailTemplateParent(new ExtInterfaceTemplateDetail());
		}
		else if(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.QUERY.getCode())){
			interfaceAttributes.setPredicateTypeQuery(Boolean.TRUE);
			
		}
		else if(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.VALUE.getCode())){
			interfaceAttributes.setPredicateTypeValue(Boolean.TRUE);
		}
		else if(interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.USER_TYPE.getCode())){
			interfaceAttributes.setPredicateTypeUser(Boolean.TRUE);
		}
		JSFUtilities.executeJavascriptFunction("PF('interfacePredicateDialogW').show();");
	}
	/**
	 * Change interface query filter predicate.
	 */
	public void changeInterfaceQueryFilterPredicate(){
		List<ExternalInterfaceTable> tableList = new ArrayList<ExternalInterfaceTable>();
				
		if( interfaceAttributes.getColumnFilter()!= null &&
			interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate() != null){
			
			Long detailSelected = interfaceAttributes.getColumnFilter().getInterfaceFilterPredicate().getDetailTemplateParent().getIdExtInterfaceTempDetPk();
			
			for(ExternalInterfaceColumn column : interfaceAttributes.getAllColumnFilterAvailable()){
				if(!tableList.contains(column.getExternalInterfaceTable())){
					if(column.getExternalInterfaceTable().getExternalInterfaceQuery().getInterfaceTemplateDetail().getIdExtInterfaceTempDetPk()
						.equals(detailSelected)){
						tableList.add(column.getExternalInterfaceTable());
					}
				}
			}
			interfaceAttributes.setTableFilterListPredicate(tableList);
		}
	}
	/**
	 * Edits the interface query filter.
	 *
	 * @param columnFilter the column filter
	 */
	public void editInterfaceQueryFilter(ExternalInterfaceFilter columnFilter){		
		
		if(columnFilter.getInterfaceFilterPredicate() != null){
			
			Integer predicateType = columnFilter.getInterfaceFilterPredicate().getPredicateType();
			
			interfaceAttributes.setColumnFilter(columnFilter);	
			changeQueryPredicateType();
			
			if(predicateType.equals(InterfacePredicateType.COLUMN.getCode())){
				interfaceAttributes.setPredicateTypeColumn(Boolean.TRUE);				
			}else if(predicateType.equals(InterfacePredicateType.QUERY.getCode())){
				interfaceAttributes.setPredicateTypeQuery(Boolean.TRUE);
			}else if(predicateType.equals(InterfacePredicateType.VALUE.getCode())){
				interfaceAttributes.setPredicateTypeValue(Boolean.TRUE);
			}else if(predicateType.equals(InterfacePredicateType.USER_TYPE.getCode())){
				interfaceAttributes.setPredicateTypeUser(Boolean.TRUE);
			}
					
			setExternalInterfaceFilterEditing(columnFilter);
			JSFUtilities.executeJavascriptFunction("PF('interfacePredicateDialogW').show();");
		}
	}	
	/**
	 * Apply filters to detail template.
	 */
	public void applyFiltersToDetailTemplate(){
		if( interfaceAttributes.getInterfaceQuerySelectedToFilters() != null &&
			externalInterfaceSession.getExternalInterfaceQuerys() != null){
			
			for(ExternalInterfaceQuery query : externalInterfaceSession.getExternalInterfaceQuerys()){
				if(query.equals(interfaceAttributes.getInterfaceQuerySelectedToFilters())){			
					
					//limpio de los filtros de cada una de las tablas del query
					for(ExternalInterfaceTable table : query.getExternalInterfaceTables()){
						table.setExternalInterfaceFilters(new ArrayList<ExternalInterfaceFilter>());
					}
					//agrego los filtros modificados o nuevos a su tabla correspondiente
					for(ExternalInterfaceFilter filter : interfaceAttributes.getInterfaceQuerySelectedToFilters().getExternalInterfaceFilters()){												
						filterFor:
						for(ExternalInterfaceTable table : query.getExternalInterfaceTables()){
							if(table.getTableName().equals(filter.getExternalInterfaceTable().getTableName())){
								
								table.getExternalInterfaceFilters().add(filter);								
								query.getInterfaceTemplateDetail().setContainFilter(Boolean.TRUE);
								
								if(filter.getInterfaceFilterPredicate().getPredicateType().equals(InterfacePredicateType.COLUMN.getCode())){
									if(!filter.getInterfaceFilterPredicate().getDetailTemplateParent().getIdExtInterfaceTempDetPk().equals(
											query.getInterfaceTemplateDetail().getIdExtInterfaceTempDetPk()) ){
										queryRefFor:
										for(ExternalInterfaceQuery queryRef : externalInterfaceSession.getExternalInterfaceQuerys()){
											if(queryRef.getInterfaceTemplateDetail().getIdExtInterfaceTempDetPk().equals(
												filter.getInterfaceFilterPredicate().getDetailTemplateParent().getIdExtInterfaceTempDetPk())){
												query.setExternalInterfaceQuery(queryRef);
												
												break queryRefFor;
											}
										}
									}
								}								
								break filterFor;
							}						
						}
					}
					break;
				}
			}
			
			interfaceAttributes.setTableFilter(null);
			interfaceAttributes.setTableFilterList(null);
			interfaceAttributes.setTableFilterListPredicate(null);	
			interfaceAttributes.setInterfaceQuerySelectedToFilters(null);
			setExternalInterfaceFilterEditing(null);
		}
	}	
	
	
	/**
	 * Search external interface.
	 */
	public void searchExternalInterface(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		
		List<ExternalInterfaceTO> lista = null;
		try {
			lista = interfaceServiceFacade.getExternalInterfaceList(externalInterfaceSearch);		
			for(ExternalInterfaceTO to : lista){
				if( !to.getStateType().equals(ExternalInterfaceStateType.CONFIRMED.getCode()) &&
					!to.getExecutionType().equals(ExternalInterfaceExecutionType.AUTOMATIC.getCode())){
					to.setActivateSend(Boolean.TRUE);
				}
				parameterFor:
				for(ParameterTable param : externalInterfaceStateParameterList){
					if(param.getParameterTablePk().equals(to.getStateType())){
						to.setStateTypeValue(param.getParameterName());
						break parameterFor;
					}
				}
			}
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
		if(lista==null){
			externalInterfaceListSearch = new GenericDataModel<ExternalInterfaceTO>();
		}else{
			externalInterfaceListSearch = new GenericDataModel<ExternalInterfaceTO>(lista);
		}
	}	
	
	
	/* OPEN PAGE, DIALOG, HANDLERS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */	
	/**
	 * External interface register handler.
	 *
	 * @return the string
	 */
	public String externalInterfaceRegisterHandler(){
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		resetExternalInterface();
		setActivateSeparatorAttribute(Boolean.FALSE);
		setActivateFiltersQuery(Boolean.FALSE);
		return "externalInterfaceMgmtRule";
	}		
	/**
	 * External interface table handler.
	 *
	 * @return the string
	 */
	public String externalInterfaceTableHandler(){
		loadOwnerDataBaseList();
		setInterfaceTemplateTableEditing(null);	
		SqlController.tableListForAlias = new ArrayList<String>();		
		return "externalInterfaceTableDlgRule";
	}		
	/**
	 * Interface preview dialog handler.
	 */
	public void interfacePreviewDialogHandler(){
		JSFUtilities.executeJavascriptFunction("PF('interfacePreviewDialogW').show();");
	}		
	/**
	 * Interface preview filter handler.
	 */
	public void interfacePreviewFilterHandler(){
		JSFUtilities.executeJavascriptFunction("PF('interfacePreviewFiltersDialogW').show();");
	}	
	/**
	 * External interface consult handler.
	 */
	public void externalInterfaceConsultHandler(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Load components to preview.
	 */
	public void loadComponentsToPreview(){
		setActivateFiltersQuery(Boolean.TRUE);
		if(externalInterfaceSession.getInterfaceExtensionsSelected() != null){
			if(externalInterfaceSession.getInterfaceExtensionsSelected().size() > 0){
				for(String extension : externalInterfaceSession.getInterfaceExtensionsSelected()){
					if(extension.equals(InterfaceExtensionFileType.TXT.getValue())){
						setActivateSeparatorAttribute(Boolean.TRUE);
						break;
					}
				}
			}
		}
		loadTemplateList();
	}
	
	/**
	 * External interface modify handler.
	 *
	 * @return the string
	 */
	public String externalInterfaceModifyHandler(){
		if(externalInterfaceSearchSelected != null){
			try {	
				externalInterfaceSession = interfaceServiceFacade.getExternalInterface(externalInterfaceSearchSelected.getId(), ExternalInterfaceStateType.REGISTERED);				
				loadComponentsToPreview();				
				setViewOperationType(ViewOperationsType.MODIFY.getCode());
				return "externalInterfaceMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/**
	 * External interface comfirm handler.
	 *
	 * @return the string
	 */
	public String externalInterfaceConfirmHandler(){
		if(externalInterfaceSearchSelected != null){
			try {				
				externalInterfaceSession = interfaceServiceFacade.getExternalInterface(externalInterfaceSearchSelected.getId(), ExternalInterfaceStateType.CONFIRMED);
				loadComponentsToPreview();
				setViewOperationType(ViewOperationsType.CONFIRM.getCode());	
				return "externalInterfaceMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/**
	 * External interface annular handler.
	 *
	 * @return the string
	 */
	public String externalInterfaceAnnularHandler(){
		if(externalInterfaceSearchSelected != null){
			try {				
				externalInterfaceSession = interfaceServiceFacade.getExternalInterface(externalInterfaceSearchSelected.getId(), ExternalInterfaceStateType.ANNULATED);
				loadComponentsToPreview();
				setViewOperationType(ViewOperationsType.ANULATE.getCode());	
				return "externalInterfaceMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/**
	 * External interface block handler.
	 *
	 * @return the string
	 */
	public String externalInterfaceBlockHandler(){
		if(externalInterfaceSearchSelected != null){
			try {				
				externalInterfaceSession = interfaceServiceFacade.getExternalInterface(externalInterfaceSearchSelected.getId(), ExternalInterfaceStateType.BLOCKED);
				loadComponentsToPreview();
				setViewOperationType(ViewOperationsType.BLOCK.getCode());	
				return "externalInterfaceMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/**
	 * External interface unblock handler.
	 *
	 * @return the string
	 */
	public String externalInterfaceUnblockHandler(){
		if(externalInterfaceSearchSelected != null){
			try {								
				externalInterfaceSession = interfaceServiceFacade.getExternalInterfaceToUnblock(externalInterfaceSearchSelected.getId());
				loadComponentsToPreview();
				setViewOperationType(ViewOperationsType.UNBLOCK.getCode());	
				return "externalInterfaceMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}	
	/**
	 * Load form query dialog.
	 */
	public void loadFormQueryDialog(){		
		JSFUtilities.executeJavascriptFunction("PF('inputSqlDlgW').show();");
	}
	/**
	 * Clean query interface session.
	 */
	public void cleanQueryInterfaceSession(){
		interfaceAttributes.getInterfaceQuerySelected().setExternalInterfaceTables(new ArrayList<ExternalInterfaceTable>());
		interfaceAttributes.setColumnsSelected(new ArrayList<InterfaceColumnEngine>());
	}
	/**
	 * Show interface query handler.
	 *
	 * @param interfaceQuery the interface query
	 */
	public void showInterfaceQueryHandler(ExternalInterfaceQuery interfaceQuery){
		setInterfaceQueryPreview(interfaceQuery);
	}
	
	/**
	 * Interface detail handler.
	 *
	 * @param id the id
	 * @return the string
	 */
	public String interfaceDetailHandler(Long id){
		try {				
			externalInterfaceSession = interfaceServiceFacade.getExternalInterface(id);
			if(externalInterfaceSession.getSeparatorAttribute() != null){
				setActivateSeparatorAttribute(Boolean.TRUE);
			}			
			setActivateFiltersQuery(Boolean.TRUE);
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			return "externalInterfaceMgmtRule";
		} catch (ServiceException e) {
			showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
		}
		return "";
	}
	
	/**
	 * Send external interface.
	 */
	@LoggerAuditWeb
	public void sendExternalInterface(){
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(ExtInterfaceConstanst.INTERFACE_PK, getInterfaceToGenerateId());
		parameters.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, userInfo.getUserAccountSession().getInstitutionType());
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isAfpInstitution()){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getParticipantCode());
		}else if (userInfo.getUserAccountSession().isIssuerInstitucion()){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getIssuerCode());
		}
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, parameters);
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM_OK);
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Change execution type.
	 */
	public void changeExecutionType(){
		if(externalInterfaceSession.getExecutionType().equals(ExternalInterfaceExecutionType.MANUAL.getCode())){
			setActiveSendPeriodicity(Boolean.FALSE);
		}else{
			setActiveSendPeriodicity(Boolean.TRUE);
		}
	}
	/* GETTERS AND SETTERS- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/**
	 * Gets the external interface session.
	 *
	 * @return the external interface session
	 */
	public ExternalInterface getExternalInterfaceSession() {
		return externalInterfaceSession;
	}
	/**
	 * Sets the external interface session.
	 *
	 * @param externalInterfaceSession the new external interface session
	 */
	public void setExternalInterfaceSession(ExternalInterface externalInterfaceSession) {
		this.externalInterfaceSession = externalInterfaceSession;
	}
	/**
	 * Gets the execution type parameter list.
	 *
	 * @return the execution type parameter list
	 */
	public List<ParameterTable> getExecutionTypeParameterList() {
		return executionTypeParameterList;
	}
	/**
	 * Sets the execution type parameter list.
	 *
	 * @param executionTypeParameterList the new execution type parameter list
	 */
	public void setExecutionTypeParameterList(List<ParameterTable> executionTypeParameterList) {
		this.executionTypeParameterList = executionTypeParameterList;
	}
	/**
	 * Gets the interface template list.
	 *
	 * @return the interface template list
	 */
	public List<ExternalInterfaceTemplate> getInterfaceTemplateList() {
		return interfaceTemplateList;
	}
	/**
	 * Sets the interface template list.
	 *
	 * @param interfaceTemplateList the new interface template list
	 */
	public void setInterfaceTemplateList(List<ExternalInterfaceTemplate> interfaceTemplateList) {
		this.interfaceTemplateList = interfaceTemplateList;
	}
	/**
	 * Gets the external institution type list.
	 *
	 * @return the external institution type list
	 */
	public List<InstitutionType> getExternalInstitutionTypeList(){
		return InstitutionType.listExternalInstitutions;
	}
	/**
	 * Gets the boolean type list.
	 *
	 * @return the boolean type list
	 */
	public List<BooleanType> getBooleanTypeList(){
		return BooleanType.list;
	}

	/**
	 * Gets the interface attributes.
	 *
	 * @return the interface attributes
	 */
	public ExternalInterfaceAttributtes getInterfaceAttributes() {
		return interfaceAttributes;
	}
	/**
	 * Sets the interface attributes.
	 *
	 * @param interfaceAttributes the new interface attributes
	 */
	public void setInterfaceAttributes(
			ExternalInterfaceAttributtes interfaceAttributes) {
		this.interfaceAttributes = interfaceAttributes;
	}
	/**
	 * Gets the interface template table editing.
	 *
	 * @return the interface template table editing
	 */
	public ExternalInterfaceTable getInterfaceTemplateTableEditing() {
		return interfaceTemplateTableEditing;
	}
	/**
	 * Sets the interface template table editing.
	 *
	 * @param interfaceTemplateTableEditing the new interface template table editing
	 */
	public void setInterfaceTemplateTableEditing(
			ExternalInterfaceTable interfaceTemplateTableEditing) {
		this.interfaceTemplateTableEditing = interfaceTemplateTableEditing;
	}
	/**
	 * Gets the column type parameter list.
	 *
	 * @return the column type parameter list
	 */
	public List<ParameterTable> getColumnTypeParameterList() {
		return columnTypeParameterList;
	}
	/**
	 * Sets the column type parameter list.
	 *
	 * @param columnTypeParameterList the new column type parameter list
	 */
	public void setColumnTypeParameterList(
			List<ParameterTable> columnTypeParameterList) {
		this.columnTypeParameterList = columnTypeParameterList;
	}
	/**
	 * Gets the next interface template detail.
	 *
	 * @return the next interface template detail
	 */
	public ExtInterfaceTemplateDetail getNextInterfaceTemplateDetail() {
		return nextInterfaceTemplateDetail;
	}
	/**
	 * Sets the next interface template detail.
	 *
	 * @param nextInterfaceTemplateDetail the new next interface template detail
	 */
	public void setNextInterfaceTemplateDetail(
			ExtInterfaceTemplateDetail nextInterfaceTemplateDetail) {
		this.nextInterfaceTemplateDetail = nextInterfaceTemplateDetail;
	}
	/**
	 * Gets the interface query preview.
	 *
	 * @return the interface query preview
	 */
	public ExternalInterfaceQuery getInterfaceQueryPreview() {
		return interfaceQueryPreview;
	}
	/**
	 * Sets the interface query preview.
	 *
	 * @param interfaceQueryPreview the new interface query preview
	 */
	public void setInterfaceQueryPreview(
			ExternalInterfaceQuery interfaceQueryPreview) {
		this.interfaceQueryPreview = interfaceQueryPreview;
	}

	/**
	 * Gets the external interface search.
	 *
	 * @return the external interface search
	 */
	public ExternalInterfaceTO getExternalInterfaceSearch() {
		return externalInterfaceSearch;
	}

	/**
	 * Sets the external interface search.
	 *
	 * @param externalInterfaceSearch the new external interface search
	 */
	public void setExternalInterfaceSearch(
			ExternalInterfaceTO externalInterfaceSearch) {
		this.externalInterfaceSearch = externalInterfaceSearch;
	}

	/**
	 * Gets the external interface list search.
	 *
	 * @return the external interface list search
	 */
	public GenericDataModel<ExternalInterfaceTO> getExternalInterfaceListSearch() {
		return externalInterfaceListSearch;
	}

	/**
	 * Sets the external interface list search.
	 *
	 * @param externalInterfaceListSearch the new external interface list search
	 */
	public void setExternalInterfaceListSearch(
			GenericDataModel<ExternalInterfaceTO> externalInterfaceListSearch) {
		this.externalInterfaceListSearch = externalInterfaceListSearch;
	}

	/**
	 * Gets the activate separator attribute.
	 *
	 * @return the activate separator attribute
	 */
	public Boolean getActivateSeparatorAttribute() {
		return activateSeparatorAttribute;
	}

	/**
	 * Sets the activate separator attribute.
	 *
	 * @param activateSeparatorAttribute the new activate separator attribute
	 */
	public void setActivateSeparatorAttribute(Boolean activateSeparatorAttribute) {
		this.activateSeparatorAttribute = activateSeparatorAttribute;
	}

	/**
	 * Gets the activate filters query.
	 *
	 * @return the activate filters query
	 */
	public Boolean getActivateFiltersQuery() {
		return activateFiltersQuery;
	}

	/**
	 * Sets the activate filters query.
	 *
	 * @param activateFiltersQuery the new activate filters query
	 */
	public void setActivateFiltersQuery(Boolean activateFiltersQuery) {
		this.activateFiltersQuery = activateFiltersQuery;
	}

	/**
	 * Gets the predicate type parameter list.
	 *
	 * @return the predicate type parameter list
	 */
	public List<ParameterTable> getPredicateTypeParameterList() {
		return predicateTypeParameterList;
	}

	/**
	 * Sets the predicate type parameter list.
	 *
	 * @param predicateTypeParameterList the new predicate type parameter list
	 */
	public void setPredicateTypeParameterList(
			List<ParameterTable> predicateTypeParameterList) {
		this.predicateTypeParameterList = predicateTypeParameterList;
	}

	/**
	 * Gets the operator logic type parameter list.
	 *
	 * @return the operator logic type parameter list
	 */
	public List<ParameterTable> getOperatorLogicTypeParameterList() {
		return operatorLogicTypeParameterList;
	}

	/**
	 * Sets the operator logic type parameter list.
	 *
	 * @param operatorLogicTypeParameterList the new operator logic type parameter list
	 */
	public void setOperatorLogicTypeParameterList(
			List<ParameterTable> operatorLogicTypeParameterList) {
		this.operatorLogicTypeParameterList = operatorLogicTypeParameterList;
	}

	/**
	 * Gets the sql comparator type parameter list.
	 *
	 * @return the sql comparator type parameter list
	 */
	public List<ParameterTable> getSqlComparatorTypeParameterList() {
		return sqlComparatorTypeParameterList;
	}

	/**
	 * Sets the sql comparator type parameter list.
	 *
	 * @param sqlComparatorTypeParameterList the new sql comparator type parameter list
	 */
	public void setSqlComparatorTypeParameterList(
			List<ParameterTable> sqlComparatorTypeParameterList) {
		this.sqlComparatorTypeParameterList = sqlComparatorTypeParameterList;
	}

	/**
	 * Gets the user type list.
	 *
	 * @return the user type list
	 */
	public List<ExternalInterfaceUserType> getUserTypeList() {
		return userTypeList;
	}

	/**
	 * Sets the user type list.
	 *
	 * @param userTypeList the new user type list
	 */
	public void setUserTypeList(List<ExternalInterfaceUserType> userTypeList) {
		this.userTypeList = userTypeList;
	}

	/**
	 * Gets the extension type list.
	 *
	 * @return the extension type list
	 */
	public List<ExternalInterfaceExtension> getExtensionTypeList() {
		return extensionTypeList;
	}

	/**
	 * Sets the extension type list.
	 *
	 * @param extensionTypeList the new extension type list
	 */
	public void setExtensionTypeList(
			List<ExternalInterfaceExtension> extensionTypeList) {
		this.extensionTypeList = extensionTypeList;
	}

	/**
	 * Gets the external interface filter editing.
	 *
	 * @return the external interface filter editing
	 */
	public ExternalInterfaceFilter getExternalInterfaceFilterEditing() {
		return externalInterfaceFilterEditing;
	}

	/**
	 * Sets the external interface filter editing.
	 *
	 * @param externalInterfaceFilterEditing the new external interface filter editing
	 */
	public void setExternalInterfaceFilterEditing(
			ExternalInterfaceFilter externalInterfaceFilterEditing) {
		this.externalInterfaceFilterEditing = externalInterfaceFilterEditing;
	}

	/**
	 * Gets the external interface search selected.
	 *
	 * @return the external interface search selected
	 */
	public ExternalInterfaceTO getExternalInterfaceSearchSelected() {
		return externalInterfaceSearchSelected;
	}

	/**
	 * Sets the external interface search selected.
	 *
	 * @param externalInterfaceSearchSelected the new external interface search selected
	 */
	public void setExternalInterfaceSearchSelected(
			ExternalInterfaceTO externalInterfaceSearchSelected) {
		this.externalInterfaceSearchSelected = externalInterfaceSearchSelected;
	}

	/**
	 * Gets the external interface type parameter list.
	 *
	 * @return the external interface type parameter list
	 */
	public List<ParameterTable> getExternalInterfaceStateParameterList() {
		return externalInterfaceStateParameterList;
	}

	/**
	 * Sets the external interface type parameter list.
	 *
	 * @param externalInterfaceStateParameterList the new external interface state parameter list
	 */
	public void setExternalInterfaceStateParameterList(
			List<ParameterTable> externalInterfaceStateParameterList) {
		this.externalInterfaceStateParameterList = externalInterfaceStateParameterList;
	}

	/**
	 * Gets the interface to generate id.
	 *
	 * @return the interface to generate id
	 */
	public Long getInterfaceToGenerateId() {
		return interfaceToGenerateId;
	}

	/**
	 * Sets the interface to generate id.
	 *
	 * @param interfaceToGenerateId the new interface to generate id
	 */
	public void setInterfaceToGenerateId(Long interfaceToGenerateId) {
		this.interfaceToGenerateId = interfaceToGenerateId;
	}

	/**
	 * Gets the active send periodicity.
	 *
	 * @return the active send periodicity
	 */
	public Boolean getActiveSendPeriodicity() {
		return activeSendPeriodicity;
	}

	/**
	 * Sets the active send periodicity.
	 *
	 * @param activeSendPeriodicity the new active send periodicity
	 */
	public void setActiveSendPeriodicity(Boolean activeSendPeriodicity) {
		this.activeSendPeriodicity = activeSendPeriodicity;
	}

	/**
	 * Gets the ext interface send hour parameter list.
	 *
	 * @return the ext interface send hour parameter list
	 */
	public List<ParameterTable> getExtInterfaceSendHourParameterList() {
		return extInterfaceSendHourParameterList;
	}

	/**
	 * Sets the ext interface send hour parameter list.
	 *
	 * @param extInterfaceSendHourParameterList the new ext interface send hour parameter list
	 */
	public void setExtInterfaceSendHourParameterList(
			List<ParameterTable> extInterfaceSendHourParameterList) {
		this.extInterfaceSendHourParameterList = extInterfaceSendHourParameterList;
	}

	/**
	 * Gets the ext interface format date parameter list.
	 *
	 * @return the ext interface format date parameter list
	 */
	public List<ParameterTable> getExtInterfaceFormatDateParameterList() {
		return extInterfaceFormatDateParameterList;
	}

	/**
	 * Sets the ext interface format date parameter list.
	 *
	 * @param extInterfaceFormatDateParameterList the new ext interface format date parameter list
	 */
	public void setExtInterfaceFormatDateParameterList(
			List<ParameterTable> extInterfaceFormatDateParameterList) {
		this.extInterfaceFormatDateParameterList = extInterfaceFormatDateParameterList;
	}
	
}