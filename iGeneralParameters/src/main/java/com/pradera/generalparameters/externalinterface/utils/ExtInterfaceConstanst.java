package com.pradera.generalparameters.externalinterface.utils;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExtInterfaceConstanst.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/11/2013
 */
public class ExtInterfaceConstanst {
	
	/** The Constant IDENTIFICATOR. */
	public static final String IDENTIFICATOR = ":".concat(ExtInterfaceConstanst.PARAMETER_QUERY);
	
	/** The Constant PARAM. */
	public static final String PARAMETER_QUERY = "PARAM_";

	/** The Constant REPORT_ASOCIATED. */
	public static final Long REPORT_ASOCIATED = 121L;
	
	/** The Constant INTERFACE_PK. */
	public static final String INTERFACE_PK = "INTERFACE_PK";
	
	/** The Constant USER_INFO. */
	public static final String INSTITUTION_TYPE_ID = "INSTITUTION_TYPE_ID";
	
	/** The Constant INITIAL_DATE. */
	public static final String INITIAL_DATE = "initialDate";
	
	/** The Constant FINAL_DATE. */
	public static final String FINAL_DATE = "finalDate";
	
	/** The Constant ISSUER_CODE. */
	public static final String ISSUER_CODE = "issuerCode";
	
	/** The Constant PARTICIPANT_CODE. */
	public static final String PARTICIPANT_CODE = "participantCode";
	
	/** The Constant SECURITY_CODES. */
	public static final String SECURITY_CODES = "securityCodes";
	
	/** The Constant HOLDER_CODE. */
	public static final String HOLDER_CODE = "holderCode";
	
	/** The Constant INSTITUTION_ID. */
	public static final String INSTITUTION_ID = "INSTITUTION_ID";
	
	/** The Constant INTERFACE_NODE. */
	public static final String INTERFACE_NODE = "interface";
	
	/** The Constant TEMPLATE_NODE. */
	public static final String TEMPLATE_NODE = "template";
	
	/** The Constant NODE. */
	public static final String NODE = "item";

	/** The Constant DETAIL_NODE. */
	public static final String DETAIL_NODE = "detail";
	
	/** The Constant DATA_NODE. */
	public static final String DATA_NODE = "data";
	
	/** The Constant ITERATION_NODE. */
	public static final String ITERATION_NODE = "iteration";
	
	/** The Constant COLUMN_NODE. */
	public static final String COLUMN_NODE = "column";
	
	/** The Constant ROW_NODE. */
	public static final String ROW_NODE = "row_data";
	
	/** The Constant CHAR. */
	public static final String CHAR = "#";
	
	/** The Constant FILTER_INTERFACE_PK. */
	public static final String FILTER_INTERFACE_PK = "id_interface_pk";
	
	/** The Constant FILTER_INTERFACE_SEND_PK. */
	public static final String FILTER_INTERFACE_SEND_PK = "id_interface_send_pk";
	
	/** The Constant FILTER_INTERFACE_ANN_CORR. */
	public static final String FILTER_INTERFACE_ANN_CORR = "corr";
	
	/** The Constant SEND_FTP. */
	public static final String SEND_FTP = "sed_ftp";
	
	/**Estado de la consulta de la interface**/
	public static final String STATE = "state";
	
}