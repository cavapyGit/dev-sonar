package com.pradera.generalparameters.externalinterface.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.TransactionSynchronizationRegistry;

import org.hibernate.exception.SQLGrammarException;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.XMLUtils;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.to.TRCRtoBrokerageAgencyTO;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.externalinterface.view.InterfaceSendTO;
import com.pradera.generalparameters.externalinterface.view.InterfaceSendUserTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceSendFile;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceSendUser;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceSendFileStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceSendProcessStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceSendStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceSendUserStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceSendingService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/12/2013
 */ 
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InterfaceSendingService extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private transient PraderaLogger logger;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;
	
	public List<Object[]> getLstTrcRtoBrokerageAgencyTOSearchTransfer(TRCRtoBrokerageAgencyTO filter) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT DISTINCT S.ID_SECURITY_CODE_PK,                                                        ");
		sd.append(" 	S.CURRENCY,                                                                               ");
		sd.append(" 	S.CURRENT_NOMINAL_VALUE,                                                                  ");
		sd.append(" 	S.ISSUANCE_DATE,                                                                          ");
		sd.append(" 	CASE WHEN s.security_class IN ( 420,1976 )                                                ");
		sd.append(" 		THEN S.NOT_TRADED                                                                     ");
		sd.append(" 		ELSE NULL                                                                             ");
		sd.append(" 	END NOT_TRADED,                                                                           ");
		sd.append(" 	S.STATE_SECURITY                                                                          ");
		sd.append(" FROM SECURITY_TRANSFER_OPERATION sto                                                          ");
		sd.append(" INNER JOIN SECURITY s           ON sto.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK            ");
		sd.append(" INNER JOIN CUSTODY_OPERATION co ON sto.ID_TRANSFER_OPERATION_PK = co.ID_CUSTODY_OPERATION_PK  ");
		sd.append(" WHERE 0 = 0                                                                                   ");
		sd.append(" AND S.STATE_SECURITY = :stateSecurity                                                         ");
		sd.append(" AND ((TRUNC ( co.CONFIRM_DATE ) >= TRUNC ( TO_DATE ( :transferDate, 'DD/MM/YYYY' ) , 'DD' )    ");
		sd.append(" AND TRUNC ( co.CONFIRM_DATE ) < TRUNC ( TO_DATE ( :transferDate , 'DD/MM/YYYY' ) , 'DD' ) + 1) ");
		sd.append(" OR    																						   ");
		sd.append(" (TRUNC ( co.APPROVAL_DATE) >= TRUNC ( TO_DATE ( :transferDate , 'DD/MM/YYYY' ) , 'DD' )        ");
		sd.append(" AND TRUNC ( co.APPROVAL_DATE) < TRUNC ( TO_DATE ( :transferDate , 'DD/MM/YYYY' ) , 'DD' ) + 1) ");
		sd.append(" OR    																						   ");
		sd.append(" (TRUNC ( co.REVIEW_DATE) >= TRUNC ( TO_DATE ( :transferDate , 'DD/MM/YYYY' ) , 'DD' )        ");
		sd.append(" AND TRUNC ( co.REVIEW_DATE) < TRUNC ( TO_DATE ( :transferDate , 'DD/MM/YYYY' ) , 'DD' ) + 1) ");
		sd.append(" OR 																							   ");
		sd.append("(TRUNC ( co.REGISTRY_DATE) >= TRUNC ( TO_DATE ( :transferDate , 'DD/MM/YYYY' ) , 'DD' ) ");
		sd.append(" AND TRUNC ( co.REGISTRY_DATE) < TRUNC ( TO_DATE ( :transferDate , 'DD/MM/YYYY' ) , 'DD' ) + 1))");
		sd.append(" AND sto.STATE IN :lstStateTransfer");
		sd.append(" AND co.OPERATION_STATE IN :lstStateTransfer                                         ");
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			sd.append(" AND ( STO.ID_SOURCE_PARTICIPANT_FK = :idParticipantPk                                         ");
			sd.append("  OR STO.ID_TARGET_PARTICIPANT_FK = :idParticipantPk )                                         ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			sd.append(" AND S.ID_ISSUER_FK = :idIssuerPk                                                                    ");			
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			sd.append(" AND S.CURRENCY = :currency                                                                         ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			sd.append(" AND S.ALTERNATIVE_CODE = :alternateCode                                                            ");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			sd.append(" AND S.SECURITY_CLASS = :securityClass                                                              ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			sd.append(" AND S.ID_SECURITY_CODE_PK = :idSecurityCodePk 													   ");
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			sd.append(" AND S.SECURITY_CLASS in(420, 1976)");
			sd.append(" AND S.NOT_TRADED is not null");
			sd.append(" AND S.NOT_TRADED != :isNegotiable");
		}
		
		Query q = em.createNativeQuery(sd.toString());
		q.setParameter("stateSecurity", SecurityStateType.REGISTERED.getCode());
		q.setParameter("transferDate", CommonsUtilities.convertDatetoString(filter.getTransferDate()));
		List<Integer> lstStateTransfer= new ArrayList<Integer>();
		lstStateTransfer.add(TransferSecuritiesStateType.CONFIRMADO.getCode());
		lstStateTransfer.add(TransferSecuritiesStateType.REGISTRADO.getCode());
		lstStateTransfer.add(TransferSecuritiesStateType.APROBADO.getCode());
		lstStateTransfer.add(TransferSecuritiesStateType.REVISADO.getCode());
		q.setParameter("lstStateTransfer", lstStateTransfer);
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			q.setParameter("idParticipantPk", filter.getIdParticipantPk());	
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			q.setParameter("idIssuerPk", filter.getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			q.setParameter("currency", filter.getCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			q.setParameter("alternateCode", filter.getAlternateCode());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			q.setParameter("securityClass", filter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			q.setParameter("idSecurityCodePk", filter.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			q.setParameter("isNegotiable", filter.getIsNegotiable());
		}
		List<Object[]> lstSec = q.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstSec)){
			return lstSec;
		}else{
			return null;
		}
	}
	
	public List<Object[]> getLstTrcRtoBrokerageAgencyTOSearchNegotiation(TRCRtoBrokerageAgencyTO filter) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT DISTINCT S.ID_SECURITY_CODE_PK,                                                             ");
		sd.append(" 	S.CURRENCY,                                                                                    ");
		sd.append(" 	S.CURRENT_NOMINAL_VALUE,                                                                       ");
		sd.append(" 	S.ISSUANCE_DATE,                                                                               ");
		sd.append(" 	CASE WHEN s.security_class IN ( 420,1976 )                                                     ");
		sd.append(" 		THEN S.NOT_TRADED                                                                          ");
		sd.append(" 		ELSE NULL                                                                                  ");
		sd.append(" 	END NOT_TRADED,                                                                                ");
		sd.append(" 	S.STATE_SECURITY                                                                               ");
		sd.append(" FROM MECHANISM_OPERATION mo                                                                        ");
		sd.append(" INNER JOIN SECURITY s ON S.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                            ");
		sd.append(" WHERE 0 = 0                                                                                        ");
		// a requerimiento de usuario se restringe la informacion solo mecanismo BOLSA DE VALORES
		sd.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK = 1 ");
		sd.append(" AND S.STATE_SECURITY = :stateSecurity                                                              ");
		sd.append(" AND TRUNC ( MO.OPERATION_DATE ) >= TRUNC ( TO_DATE ( :negotiationDate , 'DD/MM/YYYY' ) , 'DD' )    ");
		sd.append(" AND TRUNC ( MO.OPERATION_DATE ) < TRUNC ( TO_DATE ( :negotiationDate , 'DD/MM/YYYY' ) , 'DD' ) + 1 ");
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			sd.append(" AND ( MO.ID_BUYER_PARTICIPANT_FK = :idParticipantPk                                                ");
			sd.append("  OR MO.ID_SELLER_PARTICIPANT_FK = :idParticipantPk )                                               ");	
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			sd.append(" AND S.ID_ISSUER_FK = :idIssuerPk                                                                    ");			
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			sd.append(" AND S.CURRENCY = :currency                                                                         ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			sd.append(" AND S.ALTERNATIVE_CODE = :alternateCode                                                            ");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			sd.append(" AND S.SECURITY_CLASS = :securityClass                                                              ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			sd.append(" AND S.ID_SECURITY_CODE_PK = :idSecurityCodePk 													   ");
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			sd.append(" AND S.SECURITY_CLASS in(420, 1976)");
			sd.append(" AND S.NOT_TRADED is not null");
			sd.append(" AND S.NOT_TRADED != :isNegotiable");
		}
		
		Query q = em.createNativeQuery(sd.toString());
		q.setParameter("stateSecurity", SecurityStateType.REGISTERED.getCode());
		q.setParameter("negotiationDate", CommonsUtilities.convertDatetoString(filter.getNegotiationDate()));
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			q.setParameter("idParticipantPk", filter.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			q.setParameter("idIssuerPk", filter.getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			q.setParameter("currency", filter.getCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			q.setParameter("alternateCode", filter.getAlternateCode());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			q.setParameter("securityClass", filter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			q.setParameter("idSecurityCodePk", filter.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			q.setParameter("isNegotiable", filter.getIsNegotiable());
		}
		List<Object[]> lstSec = q.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstSec)){
			return lstSec;
		}else{
			return null;
		}
	}
	
	
	public List<Object[]> getLstTrcRtoBrokerageAgencyTOSearchClientPortfolio(TRCRtoBrokerageAgencyTO filter) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT DISTINCT S.ID_SECURITY_CODE_PK,                                                      ");
		sd.append(" 	S.CURRENCY,                                                                             ");
		sd.append(" 	S.CURRENT_NOMINAL_VALUE,                                                                ");
		sd.append(" 	S.ISSUANCE_DATE,                                                                        ");
		sd.append(" 	CASE WHEN s.security_class IN ( 420,1976 )                                              ");
		sd.append(" 		THEN S.NOT_TRADED                                                                   ");
		sd.append(" 		ELSE NULL                                                                           ");
		sd.append(" 	END NOT_TRADED,                                                                         ");
		sd.append(" 	S.STATE_SECURITY                                                                        ");
		sd.append(" FROM MARKET_FACT_VIEW mfv                                                                   ");
		sd.append(" INNER JOIN SECURITY s                ON S.ID_SECURITY_CODE_PK = MFV.ID_SECURITY_CODE_PK     ");
		sd.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had ON HAD.ID_HOLDER_ACCOUNT_FK = MFV.ID_HOLDER_ACCOUNT_PK ");
		sd.append(" INNER JOIN HOLDER_ACCOUNT ha         ON HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK  ");
		sd.append(" WHERE 0 = 0                                                                                 ");
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			sd.append(" AND MFV.ID_PARTICIPANT_PK = :idParticipantPk                                                ");
		}
		sd.append(" AND S.STATE_SECURITY = :stateSecurity                                                       ");
		sd.append(" AND MFV.CUT_DATE >= TRUNC ( TO_DATE ( :cutDateParameter , 'DD/MM/YYYY' ) , 'DD' )           ");
		sd.append(" AND MFV.CUT_DATE < TRUNC ( TO_DATE ( :cutDateParameter , 'DD/MM/YYYY' ) , 'DD' ) + 1        ");
		if( Validations.validateIsNotNull(filter.getHolder()) && Validations.validateIsNotNullAndPositive(filter.getHolder().getIdHolderPk())){
			sd.append(" AND HAD.ID_HOLDER_FK = :idHolderPk                                                          ");
		}
		if( Validations.validateIsNotNull(filter.getHolderAccount()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getAccountNumber())){
			sd.append(" AND HA.ACCOUNT_NUMBER = :accountNumber														");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			sd.append(" AND S.ID_ISSUER_FK = :idIssuerPk                                                                    ");			
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			sd.append(" AND S.CURRENCY = :currency                                                                         ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			sd.append(" AND S.ALTERNATIVE_CODE = :alternateCode                                                            ");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			sd.append(" AND S.SECURITY_CLASS = :securityClass                                                              ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			sd.append(" AND S.ID_SECURITY_CODE_PK = :idSecurityCodePk 													   ");
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			sd.append(" AND S.SECURITY_CLASS in(420, 1976)");
			sd.append(" AND S.NOT_TRADED is not null");
			sd.append(" AND S.NOT_TRADED != :isNegotiable");
		}
		
		Query q = em.createNativeQuery(sd.toString());
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			q.setParameter("idParticipantPk", filter.getIdParticipantPk());
		}
		q.setParameter("stateSecurity", SecurityStateType.REGISTERED.getCode());
		q.setParameter("cutDateParameter", CommonsUtilities.convertDatetoString(filter.getCutDate()));
		if( Validations.validateIsNotNull(filter.getHolder()) && Validations.validateIsNotNullAndPositive(filter.getHolder().getIdHolderPk())){
			q.setParameter("idHolderPk", filter.getHolder().getIdHolderPk());
		}
		if( Validations.validateIsNotNull(filter.getHolderAccount()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getAccountNumber())){
			q.setParameter("accountNumber", filter.getHolderAccount().getAccountNumber());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			q.setParameter("idIssuerPk", filter.getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			q.setParameter("currency", filter.getCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			q.setParameter("alternateCode", filter.getAlternateCode());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			q.setParameter("securityClass", filter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			q.setParameter("idSecurityCodePk", filter.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			q.setParameter("isNegotiable", filter.getIsNegotiable());
		}
		List<Object[]> lstSec = q.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstSec)){
			return lstSec;
		}else{
			return null;
		}
		
		/*
		List<Object[]> lstSec = q.getResultList();
		List<TRCRtoBrokerageAgencyTO> lst=new ArrayList<>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstSec)){
			TRCRtoBrokerageAgencyTO trCrSec;
			Integer isNotTrader;
			for(Object[] row:lstSec){
				trCrSec = new TRCRtoBrokerageAgencyTO();
				trCrSec.setIdSecurityCodePk(Validations.validateIsNotNull(row[0]) ? String.valueOf(row[0]) : "");
				trCrSec.setCurrencyCodeText(Validations.validateIsNotNull(row[1]) ? CurrencyType.get(Integer.parseInt(row[1].toString())).getCodeIso() : "");
				trCrSec.setCurrentNominalValue(Validations.validateIsNotNull(row[2]) ? new BigDecimal(String.valueOf(row[2])) : null );
				trCrSec.setIssuanceDate(Validations.validateIsNotNull(row[3]) ? (Date)(row[3]) : null );
				trCrSec.setIsNegotiableText(null);
				if(Validations.validateIsNotNull(row[4])){
					isNotTrader = Integer.parseInt(row[4].toString());
					if(BooleanType.YES.getCode().equals(isNotTrader)){
						trCrSec.setIsNegotiableText(BooleanType.NO.getValue());		
					}else{
						trCrSec.setIsNegotiableText(BooleanType.YES.getValue());
					}
				}
				trCrSec.setStateSecurityText(Validations.validateIsNotNull(row[5]) ? SecurityStateType.get(Integer.parseInt(row[5].toString())).getValue() : "");
				lst.add(trCrSec);
			}
		}
		return lst;
		*/
	}
	
	public List<Object[]> getLstTrcRtoBrokerageAgencyTOSearchAcv(TRCRtoBrokerageAgencyTO filter) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT DISTINCT S.ID_SECURITY_CODE_PK,                                                        ");
		sd.append(" 	S.CURRENCY,                                                                               ");
		sd.append(" 	S.CURRENT_NOMINAL_VALUE,                                                                  ");
		sd.append(" 	S.ISSUANCE_DATE,                                                                          ");
		sd.append(" 	CASE WHEN s.security_class IN ( 420,1976 )                                                ");
		sd.append(" 		THEN S.NOT_TRADED                                                                     ");
		sd.append(" 		ELSE NULL                                                                             ");
		sd.append(" 	END NOT_TRADED,                                                                           ");
		sd.append(" 	S.STATE_SECURITY                                                                          ");
		sd.append(" FROM ACCOUNT_ANNOTATION_OPERATION AAO                                                         ");
		sd.append(" INNER JOIN SECURITY s           ON AAO.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK            ");
		sd.append(" INNER JOIN CUSTODY_OPERATION co ON AAO.ID_ANNOTATION_OPERATION_PK = co.ID_CUSTODY_OPERATION_PK");
		sd.append(" WHERE 0 = 0                                                                                   ");
		sd.append(" AND S.STATE_SECURITY = :stateSecurity                                                         ");
		sd.append(" AND TRUNC ( co.CONFIRM_DATE ) >= TRUNC ( TO_DATE ( :acvDate , 'DD/MM/YYYY' ) , 'DD' )    ");
		sd.append(" AND TRUNC ( co.CONFIRM_DATE ) < TRUNC ( TO_DATE ( :acvDate , 'DD/MM/YYYY' ) , 'DD' ) + 1 ");
		sd.append(" AND AAO.STATE_ANNOTATION NOT IN :lstStateOperation                                            ");
		sd.append(" AND co.OPERATION_STATE NOT IN :lstStateOperation		                                      ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			sd.append(" AND S.ID_ISSUER_FK = :idIssuerPk                                                          ");			
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			sd.append(" AND S.CURRENCY = :currency                                                                ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			sd.append(" AND S.ALTERNATIVE_CODE = :alternateCode                                                   ");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			sd.append(" AND S.SECURITY_CLASS = :securityClass                                                     ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			sd.append(" AND S.ID_SECURITY_CODE_PK = :idSecurityCodePk 											  ");
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			sd.append(" AND S.SECURITY_CLASS in(420, 1976)");
			sd.append(" AND S.NOT_TRADED is not null");
			sd.append(" AND S.NOT_TRADED != :isNegotiable");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			sd.append(" AND AAO.ID_HOLDER_ACCOUNT_FK IN (                                                          ");			
			sd.append(" SELECT DISTINCT HAD.ID_HOLDER_ACCOUNT_FK                                                   ");			
			sd.append(" FROM HOLDER_ACCOUNT_DETAIL HAD                                                             ");			
			sd.append(" WHERE HAD.ID_HOLDER_FK = (                                                                 ");			
			sd.append(" SELECT ID_HOLDER_FK                                                                        ");			
			sd.append(" FROM PARTICIPANT                                                                           ");			
			sd.append(" WHERE ID_PARTICIPANT_PK = :idParticipantPk))                                               ");			
		}
		
		Query q = em.createNativeQuery(sd.toString());
		q.setParameter("stateSecurity", SecurityStateType.REGISTERED.getCode());
		q.setParameter("acvDate", CommonsUtilities.convertDatetoString(filter.getAcvDate()));
		List<Integer> lstStateAnnotation = new ArrayList<Integer>();
		lstStateAnnotation.add(DematerializationStateType.ANNULLED.getCode());
		lstStateAnnotation.add(DematerializationStateType.REJECTED.getCode());
		lstStateAnnotation.add(DematerializationStateType.REVERSED.getCode());
		q.setParameter("lstStateOperation", lstStateAnnotation);
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			q.setParameter("idIssuerPk", filter.getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			q.setParameter("currency", filter.getCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getAlternateCode())){
			q.setParameter("alternateCode", filter.getAlternateCode());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			q.setParameter("securityClass", filter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			q.setParameter("idSecurityCodePk", filter.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(filter.getIsNegotiable())){
			q.setParameter("isNegotiable", filter.getIsNegotiable());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			q.setParameter("idParticipantPk", filter.getIdParticipantPk());
		}
		List<Object[]> lstSec = q.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstSec)){
			return lstSec;
		}else{
			return null;
		}
	}
	
	public List<Object[]> getLstTrcRtoBrokerageAgencyTOSearchSeriados(TRCRtoBrokerageAgencyTO filter) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT DISTINCT S.ID_SECURITY_CODE_PK,                                                        ");
		sd.append(" 	S.CURRENCY,                                                                               ");
		sd.append(" 	S.CURRENT_NOMINAL_VALUE,                                                                  ");
		sd.append(" 	S.ISSUANCE_DATE,                                                                          ");
		sd.append(" 	CASE WHEN s.security_class IN ( 420,1976 )                                                ");
		sd.append(" 		THEN S.NOT_TRADED                                                                     ");
		sd.append(" 		ELSE NULL                                                                             ");
		sd.append(" 	END NOT_TRADED,                                                                           ");
		sd.append(" 	S.STATE_SECURITY                                                                          ");
		sd.append(" FROM SECURITY s                                                         					  ");
		sd.append(" WHERE 0 = 0                                                                                   ");
		sd.append(" AND s.STATE_SECURITY = :stateSecurity                                                         ");
		sd.append(" AND (s.IND_AUTHORIZED IS NULL OR s.IND_AUTHORIZED != 0)                                       ");//Only authorized security
		sd.append(" AND TRUNC ( s.ISSUANCE_DATE ) >= TRUNC ( TO_DATE ( :issuanceDate , 'DD/MM/YYYY' ) , 'DD' )    ");
		sd.append(" AND TRUNC ( s.ISSUANCE_DATE ) < TRUNC ( TO_DATE ( :issuanceDate , 'DD/MM/YYYY' ) , 'DD' ) + 1 ");
		sd.append(" AND S.SECURITY_CLASS NOT IN :lstSecurityClass");
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			sd.append(" AND S.CURRENCY = :currency                                                                ");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			sd.append(" AND S.SECURITY_CLASS = :securityClass                                                     ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			sd.append(" AND S.ID_SECURITY_CODE_PK = :idSecurityCodePk 											  ");
		}
		
		Query q = em.createNativeQuery(sd.toString());
		q.setParameter("stateSecurity", SecurityStateType.REGISTERED.getCode());
		q.setParameter("issuanceDate", CommonsUtilities.convertDatetoString(filter.getEmisionDate()));
		List<Integer> lstSecurityClass = new ArrayList<Integer>();
		lstSecurityClass.add(SecurityClassType.DPF.getCode());
		lstSecurityClass.add(SecurityClassType.DPA.getCode());
		lstSecurityClass.add(SecurityClassType.PGS.getCode());
		q.setParameter("lstSecurityClass", lstSecurityClass);
		if(Validations.validateIsNotNullAndPositive(filter.getCurrency())){
			q.setParameter("currency", filter.getCurrency());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getSecurityClass())){
			q.setParameter("securityClass", filter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			q.setParameter("idSecurityCodePk", filter.getIdSecurityCodePk());
		}
		List<Object[]> lstSec = q.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstSec)){
			return lstSec;
		}else{
			return null;
		}
	}
	
	public List<Issuer> getLstIssuer() throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" select i ");
		sd.append(" from Issuer i");
		sd.append(" where 0=0");
		sd.append(" and i.stateIssuer = :stateIssuer");
		sd.append(" order by i.mnemonic asc");
		Query query=em.createQuery(sd.toString());
		query.setParameter("stateIssuer", IssuerStateType.REGISTERED.getCode());
		return query.getResultList();
	}
	
	public List<Object[]> infoInstitutionByUser(String loginUser) throws Exception{
		StringBuilder sd=new StringBuilder();
		sd.append(" select ins.ID_PARTICIPANT_FK, ins.MNEMONIC_INSTITUTION, ins.INSTITUTION_NAME from SECURITY.USER_ACCOUNT ua      ");
		sd.append(" inner join SECURITY.INSTITUTIONS_SECURITY ins on ins.ID_INSTITUTION_SECURITY_PK = ua.ID_SECURITY_INSTITUTION_FK ");
		sd.append(" where ua.LOGIN_USER = :loginUser																					");
		
		Query query=em.createNativeQuery(sd.toString());
		query.setParameter("loginUser", loginUser);
		
		return query.getResultList();
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.externalinterface.remote.IExtInterfaceGenerator#executeExternalInterface(java.lang.Long)
	 */
	/**
	 * Save send interface service.
	 *
	 * @param externalInterfaceID the external interface id
	 * @param sendingType the sending type
	 * @param institutionRequest the institution request
	 * @return the external interface send
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceSend saveSendInterfaceService(Long externalInterfaceID, Integer sendingType, String institutionRequest) throws ServiceException {
		// TODO Auto-generated method stub
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		logger.info("save send external interface=====> "+externalInterfaceID);
		
		ExternalInterface externalInterface = em.find(ExternalInterface.class, externalInterfaceID);
		ExternalInterfaceSend interfaceSend = new ExternalInterfaceSend();
		interfaceSend.setExternalInterface(externalInterface);
		interfaceSend.setInterfaceSendState(InterfaceSendStateType.REGISTERED.getCode());
		interfaceSend.setExtInterfaceSendType(sendingType);
		interfaceSend.setExecutionDate(CommonsUtilities.currentDate());
		interfaceSend.setExecutionHour(CommonsUtilities.currentDateTime());
		interfaceSend.setRealGenerateDate(CommonsUtilities.currentDateTime());
		interfaceSend.setAudit(loggerUser);
		interfaceSend.setRegistryDate(CommonsUtilities.currentDateTime());
		interfaceSend.setRegistryUser(loggerUser.getUserName());
		interfaceSend.setDescription(" ");
		interfaceSend.setTotalInterfaces(0);
		interfaceSend.setGeneratedInterfaces(0);
		interfaceSend.setFailedInterfaces(0);
		if(institutionRequest!=null && !institutionRequest.isEmpty()){
			interfaceSend.setInstitutionRequest(institutionRequest);
		}
		create(interfaceSend);
		logger.info("saved send external interface sucessfully=====> "+externalInterfaceID);
		return interfaceSend;
	}
	
	/**
	 * Update total interface.
	 *
	 * @param totalInterface the total interface
	 * @param interfaceSendId the interface send id
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateTotalInterface(Integer totalInterface, Long interfaceSendId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		StringBuilder querySql = new StringBuilder();
		querySql.append("Update ExternalInterfaceSend ");
		querySql.append("Set totalInterfaces = :totalParam, generatedInterfaces = 0, failedInterfaces = 0, ");
		querySql.append("	 lastModifyApp = :lastAppParam, lastModifyDate = :lastDateParam, lastModifyIp = :lastIpParam, lastModifyUser = :lastUserParam ");
		querySql.append("Where idExtInterfaceSendPk = :interfaceSendId");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("totalParam", totalInterface);
		query.setParameter("interfaceSendId", interfaceSendId);
		query.setParameter("lastAppParam", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastDateParam", CommonsUtilities.currentDateTime());
		query.setParameter("lastIpParam", loggerUser.getIpAddress());
		query.setParameter("lastUserParam", loggerUser.getUserName());
		query.executeUpdate();
	}
	
	/**
	 * Update interface generated.
	 *
	 * @param interfaceSendId the interface send id
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateInterfaceGenerated(Long interfaceSendId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		StringBuilder querySql = new StringBuilder();
		querySql.append("Update ExternalInterfaceSend ");
		querySql.append("Set generatedInterfaces = generatedInterfaces + 1, ");
		querySql.append("	 lastModifyApp = :lastAppParam, lastModifyDate = :lastDateParam, lastModifyIp = :lastIpParam, lastModifyUser = :lastUserParam ");
		querySql.append("Where idExtInterfaceSendPk = :interfaceSendId");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("interfaceSendId", interfaceSendId);
		query.setParameter("lastAppParam", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastDateParam", CommonsUtilities.currentDateTime());
		query.setParameter("lastIpParam", loggerUser.getIpAddress());
		query.setParameter("lastUserParam", loggerUser.getUserName());
		query.executeUpdate();
	}
	
	/**
	 * Update interface failed.
	 *
	 * @param interfaceSendId the interface send id
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateInterfaceFailed(Long interfaceSendId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		StringBuilder querySql = new StringBuilder();
		querySql.append("Update ExternalInterfaceSend ");
		querySql.append("Set failedInterfaces = failedInterfaces + 1, ");
		querySql.append("	 lastModifyApp = :lastAppParam, lastModifyDate = :lastDateParam, lastModifyIp = :lastIpParam, lastModifyUser = :lastUserParam ");
		querySql.append("Where idExtInterfaceSendPk = :interfaceSendId");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("interfaceSendId", interfaceSendId);
		query.setParameter("lastAppParam", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastDateParam", CommonsUtilities.currentDateTime());
		query.setParameter("lastIpParam", loggerUser.getIpAddress());
		query.setParameter("lastUserParam", loggerUser.getUserName());
		query.executeUpdate();
	}
	
	/**
	 * Begin interface execution.
	 *
	 * @param interfaceSend the interface send
	 * @param interfaceTarget the interface target
	 * @return the long
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long beginInterfaceExecution(ExternalInterfaceSend interfaceSend,Map<String, String> interfaceTarget) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		// TODO Auto-generated method stub
		ExtInterfaceSendUser interfaceSendUser = new ExtInterfaceSendUser();
		interfaceSendUser.setExternalInterfaceSend(interfaceSend);
		interfaceSendUser.setRegistryUser(loggerUser.getUserName());
		interfaceSendUser.setRegistryDate(CommonsUtilities.currentDateTime());
		interfaceSendUser.setProcessState(InterfaceSendProcessStateType.GENERATING.getCode());
		interfaceSendUser.setInterfaceSendState(InterfaceSendUserStateType.REGISTERED.getCode());
		
//		Set<Entry<Integer, Object>> rawParameters = interfaceTarget.entrySet();
//		for (Entry<Integer, Object> entry : rawParameters) {
			interfaceSendUser.setInstitutionType(Integer.parseInt(interfaceTarget.get(ExtInterfaceConstanst.INSTITUTION_TYPE_ID)));
			interfaceSendUser.setInstitutionCode(interfaceTarget.get(ExtInterfaceConstanst.INSTITUTION_ID));
//		}
		create(interfaceSendUser);
		return interfaceSendUser.getIdExtInterfSendPk();
	}
	/**
	 * Finish with errors.
	 *
	 * @param interfaceUserSend the interface user send
	 * @param ex the ex
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void finishInterfaceWithErrors(Long interfaceUserSend, Exception ex)  throws ServiceException{
		// TODO Auto-generated method stub
		ExtInterfaceSendUser interfaceUser = this.find(ExtInterfaceSendUser.class, interfaceUserSend);
		StringBuilder errorMessage = new StringBuilder();

		if(ex.getCause()!=null){
			errorMessage.append(ex.getCause().toString());
		}
		if(ex.getMessage()!=null){
			errorMessage.append(ex.getMessage());
		}
		interfaceUser.setErrorException(errorMessage.toString());
		interfaceUser.setProcessState(InterfaceSendProcessStateType.FAILED.getCode());
		update(interfaceUser); 
		//updating interface filed
		updateInterfaceFailed(interfaceUser.getExternalInterfaceSend().getIdExtInterfaceSendPk());
	}
	
	/**
	 * Finish interface execution.
	 *
	 * @param interfaceUserSendId the interface user send id
	 * @param filesToSend the files to send
	 * @param xmlGenericFile the xml generic file
	 * @param institutionType the institution type
	 * @param institutionCode the institution code
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void finishInterfaceExecution(Long interfaceUserSendId,List<File> filesToSend, File xmlGenericFile, InstitutionType institutionType, Object institutionCode)  throws ServiceException{
		// TODO Auto-generated method stub
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		ExtInterfaceSendUser interfaceUser = this.find(ExtInterfaceSendUser.class, interfaceUserSendId);
		interfaceUser.setProcessState(InterfaceSendProcessStateType.GENERATED.getCode());
		interfaceUser.setSendHour(CommonsUtilities.currentDateTime());
		interfaceUser.setExtInterfaceSendFiles(new ArrayList<ExtInterfaceSendFile>());
		
		List<UserAccountSession> userAccounts = clientRestService.getUsersInformation(UserAccountStateType.CONFIRMED.getCode(), institutionType, institutionCode, null, null, BooleanType.YES.getCode());
		StringBuilder userConcated = new StringBuilder();
		for(UserAccountSession userSession: userAccounts){
			userConcated.append(userSession.getUserName()).append(", ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(interfaceUser.getExternalInterfaceSend().getExternalInterface()) &&
				interfaceUser.getExternalInterfaceSend().getExternalInterface().getIdExtInterfacePk().equals(1034L)){
			interfaceUser.setUserSession(loggerUser.getUserName());
		}else{
			interfaceUser.setUserSession(userConcated.toString());
		}
		
		if(xmlGenericFile!=null){
			try {
				interfaceUser.setFileContentGeneric(XMLUtils.getBytesFromFile(xmlGenericFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		for(File file: filesToSend){
			ExtInterfaceSendFile interfaceFile = new ExtInterfaceSendFile();
			interfaceFile.setInterfaceSendfileState(InterfaceSendFileStateType.REGISTERED.getCode());
			interfaceFile.setExternalInterfaceUser(interfaceUser);
			interfaceFile.setFileName(file.getName());
			interfaceFile.setRegistryUser(loggerUser.getUserName());
			interfaceFile.setRegistryDate(CommonsUtilities.currentDateTime());
			create(interfaceFile);
		}
		//update interface Generated
		updateInterfaceGenerated(interfaceUser.getExternalInterfaceSend().getIdExtInterfaceSendPk());
	}	

	/**
	 * Gets the interface send list.
	 *
	 * @param interfaceSendSearch the interface send search
	 * @return the interface send list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InterfaceSendTO> getInterfaceSendList(InterfaceSendTO interfaceSendSearch) throws ServiceException{		
		Map<String,Object> parameters = new HashMap<>();		
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select Distinct send.idExtInterfaceSendPk, send.extInterfaceSendType, 		");
		querySql.append("		usr.idUserTypeFk, interf.idExtInterfacePk, interf.description,		");
		querySql.append("		send.registryDate, send.totalInterfaces, send.generatedInterfaces,	");
		querySql.append("		send.failedInterfaces												");
		querySql.append("From ExternalInterfaceSend send											");
		querySql.append("Inner Join send.externalInterface interf									");
		querySql.append("Inner Join interf.externalInterfaceUserTypes usr							");
		querySql.append("Inner Join send.extInterfaceSendUsers sendUsr								");
		querySql.append("Where 1 = 1																");
		
		if(interfaceSendSearch.getInterfaceId() != null && interfaceSendSearch.getInterfaceId() > 0){
			querySql.append("And interf.idExtInterfacePk = :interfacePk								");
			parameters.put("interfacePk", interfaceSendSearch.getInterfaceId());
		}
		
		if(interfaceSendSearch.getInstitutionType() != null && interfaceSendSearch.getInstitutionType() > 0){
			querySql.append("And usr.idUserTypeFk = :idUserTypeFk							");
			parameters.put("idUserTypeFk", interfaceSendSearch.getInstitutionType());
		}
		
		if(interfaceSendSearch.getSendType() != null && interfaceSendSearch.getSendType() > 0){
			querySql.append("And send.extInterfaceSendType = :extInterfaceSendType			");
			parameters.put("extInterfaceSendType", interfaceSendSearch.getSendType());
		}
		
		querySql.append("And trunc(send.executionDate) between (:initialDate) and (:finalDate)			");
		parameters.put("initialDate", interfaceSendSearch.getInitialDate());
		parameters.put("finalDate", interfaceSendSearch.getFinalDate());
		
		if(interfaceSendSearch.getInstitutionType() != null && interfaceSendSearch.getInstitutionType() > 0){
			if(interfaceSendSearch.getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
				querySql.append("And sendUsr.institutionCode = :institutionCode				");
				parameters.put("institutionCode", interfaceSendSearch.getIssuerRequest().getIdIssuerPk());
			}else if (interfaceSendSearch.getParticipantId() != null && interfaceSendSearch.getParticipantId() > 0){
				querySql.append("And sendUsr.institutionCode = :institutionCode				");
				parameters.put("institutionCode", interfaceSendSearch.getParticipantId());
			}
		}
		querySql.append("Order By send.idExtInterfaceSendPk Desc						"); 
//		Query query =  em.createQuery(querySql.toString(),parameters);
		List<InterfaceSendTO> result = new ArrayList<InterfaceSendTO>();
		try{
//			List<Object[]> objectDataList = query.getResultList();
			List<Object[]> objectDataList = findListByQueryString(querySql.toString(), parameters);
			for(Object[] objectData: objectDataList){
				InterfaceSendTO interfaceSend = new InterfaceSendTO();
				interfaceSend.setId((Long) objectData[0]);
				interfaceSend.setSendType((Integer) objectData[1]);
				interfaceSend.setInstitutionType((Integer) objectData[2]);
				interfaceSend.setInterfaceId((Long) objectData[3]);
				interfaceSend.setInterfaceName((String) objectData[4]);
				interfaceSend.setRegistryDate((Date) objectData[5]);
				interfaceSend.setNumberSend((Integer) objectData[6]);
				interfaceSend.setNumberSendOk((Integer) objectData[7]);
				interfaceSend.setNumberSendFail((Integer) objectData[8]);
				result.add(interfaceSend);
			}
		}catch(NoResultException ex){
			return null;
		}
		return result;
	}

	/**
	 * Gets the external interface list.
	 *
	 * @return the external interface list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ExternalInterface> getExternalInterfaceList() throws ServiceException{
		List<Long> idInterfaceExcluded=new ArrayList<>();
		idInterfaceExcluded.add(GeneralConstants.EXTERNAL_INTERFACE_TR_CR);
		StringBuilder sqlQuery = new StringBuilder();
		
		sqlQuery.append("Select distinct interface													");
		sqlQuery.append("From ExternalInterface interface									");
		sqlQuery.append("Left Outer Join Fetch interface.externalInterfaceUserTypes user	");
		sqlQuery.append("Where interface.indInterfaceType = :zeroParam						");
		sqlQuery.append("and not interface.idExtInterfacePk in :interfaceExcluded						");
		
		Query query = em.createQuery(sqlQuery.toString());
		query.setParameter("zeroParam",BooleanType.NO.getCode());
		query.setParameter("interfaceExcluded",idInterfaceExcluded);
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipantList() throws ServiceException{
		try {
			String sqlQuery = "Select new com.pradera.model.accounts.Participant(p.idParticipantPk, p.description, p.mnemonic) From Participant p Where p.state = :registeredParam  order by p.description asc";
			Query query = em.createQuery(sqlQuery);
			query.setParameter("registeredParam", ParticipantStateType.REGISTERED.getCode());
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
	
	/**
	 * Gets the aFP list.
	 *
	 * @return the aFP list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getAFPList() throws ServiceException{
		try {
			String sqlQuery = "Select new com.pradera.model.accounts.Participant(p.idParticipantPk, p.description, p.mnemonic) From Participant p Where p.state = :registeredParam And p.accountClass = :classParam order by p.description asc";
			Query query = em.createQuery(sqlQuery);
			query.setParameter("registeredParam", ParticipantStateType.REGISTERED.getCode());
			query.setParameter("classParam", ParticipantClassType.AFP.getCode());
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
	
	/**
	 * Gets the ext interface send user list.
	 *
	 * @param idInterfaceSend the id interface send
	 * @return the ext interface send user list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InterfaceSendUserTO> getExtInterfaceSendUserList(Long idInterfaceSend) throws ServiceException{
		StringBuilder stringQuery = new StringBuilder();
//		stringQuery.append("Select new com.pradera.generalparameters.externalinterface.view.InterfaceSendUserTO(");
		stringQuery.append("Select 																								");
		stringQuery.append("sendUser.idExtInterfSendPk, sendUser.institutionType, sendUser.institutionCode,						");
		stringQuery.append("sendUser.registryDate, sendUser.sendHour, sendUser.errorException, sendUser.processState,			");
		stringQuery.append("(select p.parameterName from ParameterTable p where p.parameterTablePk  = sendUser.processState )	");
		stringQuery.append(" 																									");
		stringQuery.append("From ExtInterfaceSendUser sendUser 																	");
		stringQuery.append("Where sendUser.externalInterfaceSend.idExtInterfaceSendPk = :interfaceSendId 						");
		stringQuery.append("Order By sendUser.idExtInterfSendPk DESC 															");
		Query query = em.createQuery(stringQuery.toString());
		query.setParameter("interfaceSendId", idInterfaceSend);
		List<InterfaceSendUserTO> userList = null;
		try{
//			userList = query.getResultList();
			
			List<InterfaceSendUserTO> interfaceUserList = new ArrayList<>();
			List<Object[]> dataList = query.getResultList();
			for(Object[] data: dataList){
				InterfaceSendUserTO interfaceUser = new InterfaceSendUserTO();
				interfaceUser.setTrackId((Long) data[0]);
				interfaceUser.setInstitutionType((Integer) data[1]);
				interfaceUser.setInstitutionCode((String) data[2]);
				interfaceUser.setSendStartHour((Date) data[3]);
				interfaceUser.setSendHour((Date) data[4]);
				interfaceUser.setError((String) data[5]);
				interfaceUser.setState((Integer) data[6]);
				interfaceUser.setStateDescription((String) data[7]);
				
				interfaceUserList.add(interfaceUser);
			}
			userList = interfaceUserList;
			
		}catch(NoResultException ex){
			return null;
		}
		for(InterfaceSendUserTO interfaceSendUser: userList){
			if(interfaceSendUser.getInstitutionType()!=null){
				interfaceSendUser.setInstitutionTypeDes(InstitutionType.get(interfaceSendUser.getInstitutionType()).getValue());
			}
		}
		return userList;
	}
	
	/**
	 * Gets the interface send.
	 *
	 * @param idInterfaceSend the id interface send
	 * @return the interface send
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceSend getInterfaceSend(Long idInterfaceSend) throws ServiceException{
		ExternalInterfaceSend interfaceSend = em.find(ExternalInterfaceSend.class, idInterfaceSend);
		interfaceSend.getExternalInterface().getIdExtInterfacePk();
		return interfaceSend;
	}
	
	/**
	 * Gets the interface file user to.
	 *
	 * @param interfaceUser the interface user
	 * @return the interface file user to
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public InterfaceSendUserTO getInterfaceFileUserTo(InterfaceSendUserTO interfaceUser) throws ServiceException{
		Long idInterfaceUser = interfaceUser.getTrackId();
		
		String querySqlUsers = "Select user.userSession From ExtInterfaceSendUser user Where user.idExtInterfSendPk = :idUserParam";

		Query query = em.createQuery(querySqlUsers);
		query.setParameter("idUserParam", idInterfaceUser);
		String userSession = (String) query.getSingleResult();
		
		if(userSession!=null){
			interfaceUser.setUsersOfInstitution(userSession);
		}
		
		//getting files
		String querySqlFiles = "Select file From ExtInterfaceSendFile file Where file.externalInterfaceUser.idExtInterfSendPk = :idParamUser";
		
		Query queryFiles = em.createQuery(querySqlFiles);
		queryFiles.setParameter("idParamUser", idInterfaceUser);
		List<ExtInterfaceSendFile> interfaceFile = null;
		
		try{
			interfaceFile = queryFiles.getResultList();
		}catch(NoResultException ex){
			return interfaceUser;
		}
		
		if(interfaceFile!=null && !interfaceFile.isEmpty()){
			interfaceUser.setStringFiles(new ArrayList<String>());
			for(ExtInterfaceSendFile file: interfaceFile){
				interfaceUser.getStringFiles().add(file.getFileName());
			}
		}
		
		return interfaceUser;
	}
	
	/**
	 * Getting Result Of External Interface Send.
	 *
	 * @param interfaceSend the interface send
	 * @return the results query interface
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object> getResultsQueryInterface(InterfaceSendTO interfaceSend) throws ServiceException{
		List<Object> listObject = null;
		StringBuilder sbQuery = new StringBuilder(interfaceSend.getQuerySql());

		Query query = em.createNativeQuery(sbQuery.toString());
		
		java.util.Set<Parameter<?>> 	listParameter =  query.getParameters();			
		Object[] parameterArr = listParameter.toArray();
		
		for (int i = 0; i < parameterArr.length; i++) {
			Object parameterObject	= parameterArr[i];
			if(ExtInterfaceConstanst.INITIAL_DATE.equals(((Parameter<Date>)parameterObject).getName())){
				Parameter<Date> parameter = (Parameter<Date>)parameterObject;
				query.setParameter(parameter.getName(), interfaceSend.getInitialDate());

			}else if(ExtInterfaceConstanst.FINAL_DATE.equals(((Parameter<Date>)parameterObject).getName())){
				Parameter<Date> parameter = (Parameter<Date>)parameterObject;
				query.setParameter(parameter.getName(), interfaceSend.getFinalDate());

			}else if(ExtInterfaceConstanst.ISSUER_CODE.equals(((Parameter<Date>)parameterObject).getName())){
				Parameter<String> parameter = (Parameter<String>)parameterObject;
				query.setParameter(parameter.getName(), interfaceSend.getIssuerRequest().getIdIssuerPk());

			}else if(ExtInterfaceConstanst.PARTICIPANT_CODE.equals(((Parameter<Date>)parameterObject).getName())){
				Parameter<Long> parameter = (Parameter<Long>)parameterObject;
				query.setParameter(parameter.getName(), interfaceSend.getParticipantId());

			}else if(ExtInterfaceConstanst.SECURITY_CODES.equals(((Parameter<Date>)parameterObject).getName())){
				Parameter<Long> parameter = (Parameter<Long>)parameterObject;
				query.setParameter(parameter.getName(), interfaceSend.getListIdSecurityCodePk());

			}else if(ExtInterfaceConstanst.HOLDER_CODE.equals(((Parameter<Date>)parameterObject).getName())){
				Parameter<Long> parameter = (Parameter<Long>)parameterObject;
				query.setParameter(parameter.getName(), interfaceSend.getHolderCode());

			}else if(ExtInterfaceConstanst.STATE.equalsIgnoreCase(((Parameter<Date>)parameterObject).getName())){ 
				Parameter<Long> parameter = (Parameter<Long>)parameterObject;
				query.setParameter(parameter.getName(), interfaceSend.getState());
			}
		}
		try{

			listObject=query.getResultList();
		}catch(Exception e){
			if(e.getCause() instanceof SQLGrammarException){
				throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_QUERY_INCORRECT);
			}
		}
		 
		return listObject;
	}
	

	/**
	 * Find issuer by filters service bean.
	 *
	 * @param filter the filter
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerByFiltersServiceBean(Issuer filter) throws ServiceException{
		try {
			CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
			CriteriaQuery<Issuer> criteriaQuery=criteriaBuilder.createQuery(Issuer.class);
			Root<Issuer> issuerRoot=criteriaQuery.from(Issuer.class);
			List<Predicate> criteriaParameters=new ArrayList<Predicate>();
			
			criteriaQuery.select(issuerRoot);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdIssuerPk())){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"idPrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("idIssuerPk"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getStateIssuer()) &&
					Validations.validateIsPositiveNumber(filter.getStateIssuer())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"statePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("stateIssuer"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("documentType"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentSource()) && 
					Validations.validateIsPositiveNumber(filter.getDocumentSource())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentSourcePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("documentSource"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentNumber() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("documentNumber"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getBusinessName() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"businessNamePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("businessName"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getMnemonic() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("mnemonic"), param));	
			}
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
			
			TypedQuery<Issuer> issuerRequestCriteria = em.createQuery(criteriaQuery);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdIssuerPk())){
				issuerRequestCriteria.setParameter("idPrm", filter.getIdIssuerPk());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getStateIssuer()) &&
					Validations.validateIsPositiveNumber(filter.getStateIssuer())){
				issuerRequestCriteria.setParameter("statePrm", filter.getStateIssuer());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				issuerRequestCriteria.setParameter("documentTypePrm", filter.getDocumentType());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentSource()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentSource())){
				issuerRequestCriteria.setParameter("documentSourcePrm", filter.getDocumentSource());
			}			
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
				issuerRequestCriteria.setParameter("documentNumberPrm", filter.getDocumentNumber());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getBusinessName())){
				issuerRequestCriteria.setParameter("businessNamePrm", filter.getBusinessName());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
				issuerRequestCriteria.setParameter("mnemonicPrm", filter.getMnemonic());
			}
			
			List<Issuer> list = (List<Issuer>) issuerRequestCriteria.getResultList();
			
			Issuer issuerResult = null;
			if(list!=null && list.size()>0){
				
				if(filter.getListSectorActivity()==null){
					issuerResult = list.get(0);
				}
				else{
					exit:
					for(Issuer iss : list){
					  for(Integer ea : filter.getListSectorActivity()){
						   if(iss.getEconomicActivity().equals(ea)){
							   issuerResult = iss;
							   break exit;
						   }
					  }
					
					}
				}
			}
			
			if(Validations.validateIsNotNull(issuerResult)){
				if(Validations.validateIsNotNull(issuerResult.getGeographicLocation())){
					issuerResult.getGeographicLocation().getName();
				}
				if(Validations.validateIsNotNull(issuerResult.getHolder())){
					issuerResult.getHolder().getFullName();
				}
			}
			
			return issuerResult;
		
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
/**
 * Gets the holder account list component service bean.
 *
 * @param holderAccountTO the holder account to
 * @return the holder account list component service bean
 * @throws ServiceException the service exception
 */
public List<HolderAccount> getHolderAccountListComponentServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select ha from HolderAccount ha " );
		
		if(holderAccountTO.isNeedParticipant()){
			sbQuery.append("	inner join fetch ha.participant pa ");
		}else{
			sbQuery.append("	inner join ha.participant pa ");
		}
		
		if(holderAccountTO.isNeedHolder()){
//			sbQuery.append("	inner join  ha.holderAccountDetails had ");
//			sbQuery.append("	inner join  had.holder ho ");	
		}
		
		sbQuery.append(" where 1 = 1 " );
		
		if(holderAccountTO.getParticipantTO()!=null || holderAccountTO.getParticipantStates()!=null){
			if(Validations.validateIsNotNullAndPositive(holderAccountTO.getParticipantTO())){
				sbQuery.append(" and pa.idParticipantPk = :idParticipant ");
				parameters.put("idParticipant", holderAccountTO.getParticipantTO() );
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(holderAccountTO.getParticipantStates())){
				sbQuery.append(" and pa.state in  :partstates");
				parameters.put("partstates", holderAccountTO.getParticipantStates() );
			}
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getIdHolderPk())){
			sbQuery.append(" and exists ( ");
			sbQuery.append("     select inhad.holder.idHolderPk from HolderAccountDetail inhad  ");
			sbQuery.append("     where inhad.holder.idHolderPk = :idHolder ");
			sbQuery.append("     and   inhad.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
			sbQuery.append("  ) ");
			parameters.put("idHolder", holderAccountTO.getIdHolderPk());
		}
		
		if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
			sbQuery.append(" and exists ( ");
			sbQuery.append("     select inhad.holder.idHolderPk from HolderAccountDetail inhad  ");
			sbQuery.append("     where inhad.holder.idHolderPk in (:lstHolder) ");
			sbQuery.append("     and   inhad.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
			sbQuery.append("  ) ");
			parameters.put("lstHolder", holderAccountTO.getLstHolderPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getStatus())){
			sbQuery.append(" and ha.stateAccount = :stateAccount ");
			parameters.put("stateAccount", holderAccountTO.getStatus() );
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getHolderAccountNumber())){
			sbQuery.append(" and ha.accountNumber = :accountNumber ");
			parameters.put("accountNumber", holderAccountTO.getHolderAccountNumber());
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getIdHolderAccountPk())){
			sbQuery.append(" and ha.idHolderAccountPk = :idHolderAccount ");
			parameters.put("idHolderAccount", holderAccountTO.getIdHolderAccountPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getHolderaccountGroupType())){
			sbQuery.append(" and ha.accountGroup = :accountGroup ");
			parameters.put("accountGroup", holderAccountTO.getHolderaccountGroupType());
		}
		
		sbQuery.append(" order by pa.mnemonic ");
		
		List<HolderAccount> result =  findListByQueryString(sbQuery.toString(), parameters);
		
		if(Validations.validateListIsNotNullAndNotEmpty(result)){
			for(HolderAccount objHolderAccount : result){
				objHolderAccount.getHolderAccountDetails().size();
				for(HolderAccountDetail objHolderAccountDetail : objHolderAccount.getHolderAccountDetails()){
					objHolderAccountDetail.getHolder().toString();
				}
			}
		}
		
		return result;
	}

	/**
	 * Find securities lite service bean.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecuritiesLiteServiceBean(SecurityTO securityTO) throws ServiceException{
	

		StringBuilder sbQuery = new StringBuilder();
	
		sbQuery.append(" select  new Security( se.idSecurityCodePk, ").
				append("   se.idIsinCode, ").
				append("   se.alternativeCode, ").
				append("   se.cfiCode, ").
				append("   se.description, ").
				append("   se.mnemonic, ").
				append("   se.securityType, ").
				append("   se.securityClass, ").
				append("   se.stateSecurity, ").
				append("   se.initialNominalValue, ").
				append("   se.currentNominalValue, ").
				append("   se.interestRate, ").
				append("   se.issuanceDate, ").
				append("   se.securityDaysTerm, ").
				append("   se.currency, ").
				append("   se.desmaterializedBalance, ").
				append("   se.expirationDate, ").
				append("   se.numberCoupons, ").
				append("   se.notTraded, ").
				append("   se.registryUser )").
				append("  From Security se   ").
				append("  ");
	
		sbQuery.append("  WHERE   1=1 ");
		
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			sbQuery.append(" and se.issuer.idIssuerPk= :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			sbQuery.append(" and se.issuance.idIssuanceCodePk= :idIssuanceCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			sbQuery.append(" and se.idSecurityCodePk= :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			sbQuery.append(" and se.idIsinCode= :idIsinCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			sbQuery.append(" and se.currency= :securityCurrency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			sbQuery.append(" and se.securityType= :securityType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass())){
			sbQuery.append(" and se.securityClass= :securityClass ");
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			sbQuery.append(" and se.securityClass in( :lstSecurityClass) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			sbQuery.append(" and se.issuanceDate= :issuanceDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			sbQuery.append(" and se.expirationDate= :expirationDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			sbQuery.append(" and se.cfiCode = :cfiCode");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			sbQuery.append(" and se.alternativeCode= :alternativeCode ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			sbQuery.append(" and se.description like :description ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			sbQuery.append(" and se.stateSecurity= :securityState ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			sbQuery.append(" and se.indIsCoupon= :indIsCoupon ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			sbQuery.append(" and se.indIsDetached= :indIsDetached ");
		}
		//Only TR CR With Indicator Operations Transfers
		if(Validations.validateListIsNotNullAndNotEmpty( securityTO.getListSecurityCode() )){
			sbQuery.append(" and se.idSecurityCodePk in :listIdSecurityCodePk ");
		}
		// verificando filtro de No Negociabliidad
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getNotTraded())){
			sbQuery.append(" and se.notTraded = :notTraded ");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
	
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			query.setParameter("idIssuanceCodePk",  securityTO.getIdIssuanceCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			query.setParameter("idSecurityCodePk",  securityTO.getIdSecurityCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			query.setParameter("idIsinCodePk",  securityTO.getIdIsinCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			query.setParameter("securityCurrency",  securityTO.getSecurityCurrency() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			query.setParameter("securityType",  securityTO.getSecurityType() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass() )){
			query.setParameter("securityClass",  securityTO.getSecurityClass() );
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			query.setParameter("lstSecurityClass",  securityTO.getLstSecurityClass() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			query.setParameter("issuanceDate",  securityTO.getIssuanceDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			query.setParameter("expirationDate",  securityTO.getExpirationDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			query.setParameter("cfiCode",   securityTO.getIdCfiCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			query.setParameter("alternativeCode",  securityTO.getAlternativeCode() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			query.setParameter("description",  new StringBuilder()
												.append(GeneralConstants.PERCENTAGE_CHAR)
												.append(securityTO.getDescriptionSecurity())
												.append(GeneralConstants.PERCENTAGE_CHAR).toString())  ;
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			query.setParameter("securityState", Integer.parseInt(securityTO.getSecurityState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			query.setParameter("indIsCoupon",  securityTO.getIndIsCoupon() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			query.setParameter("indIsDetached",  securityTO.getIndIsDetached() );
		}
		//Only TR CR With Indicator Operations Transfers
		if(Validations.validateListIsNotNullAndNotEmpty( securityTO.getListSecurityCode() )){
			query.setParameter("listIdSecurityCodePk",  securityTO.getListSecurityCode() );
		}
		// verificando filtro de No Negociabliidad
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getNotTraded())){
			query.setParameter("notTraded",  securityTO.getNotTraded() );
		}
		
		return  ((List<Security>)query.getResultList());

	}


	/**
	 * Find securities list for participant investor.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecuritiesListForParticipantInvestor(SecurityTO securityTO) throws ServiceException{
	

		StringBuilder sbQuery = new StringBuilder();
	
		sbQuery.append(" select distinct se ").
				append("  From HolderAccountBalance hab , Security se   ").
				append("  WHERE   1=1 and hab.totalBalance > 0");
	
		sbQuery.append("   and hab.security.idSecurityCodePk = se.idSecurityCodePk");		
		
	
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			sbQuery.append(" and se.issuer.idIssuerPk= :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			sbQuery.append(" and se.issuance.idIssuanceCodePk= :idIssuanceCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			sbQuery.append(" and se.idSecurityCodePk= :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			sbQuery.append(" and se.idIsinCode= :idIsinCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			sbQuery.append(" and se.currency= :securityCurrency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			sbQuery.append(" and se.securityType= :securityType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass())){
			sbQuery.append(" and se.securityClass= :securityClass ");
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			sbQuery.append(" and se.securityClass in( :lstSecurityClass) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			sbQuery.append(" and se.issuanceDate= :issuanceDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			sbQuery.append(" and se.expirationDate= :expirationDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			sbQuery.append(" and se.cfiCode = :cfiCode");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			sbQuery.append(" and se.alternativeCode= :alternativeCode ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			sbQuery.append(" and se.description like :description ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			sbQuery.append(" and se.stateSecurity= :securityState ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			sbQuery.append(" and se.indIsCoupon= :indIsCoupon ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			sbQuery.append(" and se.indIsDetached= :indIsDetached ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			sbQuery.append(" and hab.participant.idParticipantPk = :idParticipantPk ");
		}
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstHolderAccountPk())){
			sbQuery.append(" and hab.holderAccount.idHolderAccountPk in (:idHolderAccountPk) ");
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty( securityTO.getListSecurityCode()  )){
			sbQuery.append("  and se.idSecurityCodePk= :listIdSecurityCodePk ");
		}
		
		// verificando filtro de No Negociabliidad
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getNotTraded())){
			sbQuery.append(" and se.notTraded = :notTraded ");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
	
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			query.setParameter("idIssuanceCodePk",  securityTO.getIdIssuanceCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			query.setParameter("idSecurityCodePk",  securityTO.getIdSecurityCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			query.setParameter("idIsinCodePk",  securityTO.getIdIsinCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			query.setParameter("securityCurrency",  securityTO.getSecurityCurrency() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			query.setParameter("securityType",  securityTO.getSecurityType() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass() )){
			query.setParameter("securityClass",  securityTO.getSecurityClass() );
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			query.setParameter("lstSecurityClass",  securityTO.getLstSecurityClass() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			query.setParameter("issuanceDate",  securityTO.getIssuanceDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			query.setParameter("expirationDate",  securityTO.getExpirationDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			query.setParameter("cfiCode",   securityTO.getIdCfiCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			query.setParameter("alternativeCode",  securityTO.getAlternativeCode() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			query.setParameter("description",  new StringBuilder()
												.append(GeneralConstants.PERCENTAGE_CHAR)
												.append(securityTO.getDescriptionSecurity())
												.append(GeneralConstants.PERCENTAGE_CHAR).toString())  ;
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			query.setParameter("securityState", Integer.parseInt(securityTO.getSecurityState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			query.setParameter("indIsCoupon",  securityTO.getIndIsCoupon() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			query.setParameter("indIsDetached",  securityTO.getIndIsDetached() );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			query.setParameter("idParticipantPk",  securityTO.getIdParticipant() );
		}
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstHolderAccountPk())){
			query.setParameter("idHolderAccountPk",  securityTO.getLstHolderAccountPk() );
		}
		if(Validations.validateListIsNotNullAndNotEmpty( securityTO.getListSecurityCode()  )){
			query.setParameter("listIdSecurityCodePk",  securityTO.getListSecurityCode() );
		}
		// verificando filtro de No Negociabliidad
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getNotTraded())){
			query.setParameter("notTraded",  securityTO.getNotTraded() );
		}
		return  ((List<Security>)query.getResultList());

	}


	/**
	 * Find Securities With Transfer in the Date
	 * @param dateTransfer
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<String> findSecuritiesWithTransfer(Date dateTransfer,Long idParticipantPk) throws ServiceException{
		
	
		StringBuilder sbQuery = new StringBuilder();
	
		sbQuery.append(" select  se.idSecurityCodePk ").
				append("  From SecurityTransferOperation sto ").
				append("  Inner Join sto.securities se      ").
				append("  Inner Join sto.custodyOperation co   ").
				append("     ").
				append("  ");
	
		sbQuery.append("  WHERE   1=1 ");
		
		if(Validations.validateIsNotNull(dateTransfer)){
			sbQuery.append(" and trunc(co.confirmDate) = :confirmDate ");
		}
		if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
			sbQuery.append(" and sto.targetParticipant.idParticipantPk = :idParticipantPk ");
			
		}
		
		sbQuery.append(" and sto.state = :stateTransfer ");
		sbQuery.append(" and co.state = :stateCustody ");
		
		Query query = em.createQuery(sbQuery.toString());
			
		if(Validations.validateIsNotNull(dateTransfer)){
			query.setParameter("confirmDate",  dateTransfer );
		}
		if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
			query.setParameter("idParticipantPk",  idParticipantPk );
		}
	
		query.setParameter("stateTransfer",  TransferSecuritiesStateType.CONFIRMADO.getCode() );
		query.setParameter("stateCustody",  TransferSecuritiesStateType.CONFIRMADO.getCode() );
		
		return  ((List<String>)query.getResultList());

	}
}