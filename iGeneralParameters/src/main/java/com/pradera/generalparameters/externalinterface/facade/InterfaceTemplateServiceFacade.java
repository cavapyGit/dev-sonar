package com.pradera.generalparameters.externalinterface.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.generalparameters.externalinterface.service.InterfaceTemplateServiceBean;
import com.pradera.generalparameters.externalinterface.view.InterfaceTemplateTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTemplate;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/10/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InterfaceTemplateServiceFacade {
			
	/** The interface template service bean. */
	@EJB
	InterfaceTemplateServiceBean interfaceTemplateServiceBean;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Registry external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_TEMPLATE_REGISTER)
	public ExternalInterfaceTemplate registryExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		return interfaceTemplateServiceBean.registryExternalInterfaceTemplate(externalInterfaceTemplate);
	}
	
	/**
	 * Gets the external interface template list.
	 *
	 * @param interfaceTemplateSearch the interface template search
	 * @return the external interface template list
	 * @throws ServiceException the service exception
	 */
	public List<InterfaceTemplateTO> getExternalInterfaceTemplateList(InterfaceTemplateTO interfaceTemplateSearch)  throws ServiceException{			
		return interfaceTemplateServiceBean.getExternalInterfaceTemplateList(interfaceTemplateSearch);
	}

	/**
	 * Gets the external interface template.
	 *
	 * @param id the id
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate getExternalInterfaceTemplate(Long id) throws ServiceException {
		return interfaceTemplateServiceBean.getExternalInterfaceTemplate(id);
	}

	/**
	 * Confirm external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_TEMPLATE_CONFIRM)
	public ExternalInterfaceTemplate confirmExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {
		return interfaceTemplateServiceBean.confirmExternalInterfaceTemplate(externalInterfaceTemplate);
	}

	/**
	 * Annular external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_TEMPLATE_ANNULAR)
	public ExternalInterfaceTemplate annularExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {
		return interfaceTemplateServiceBean.annularExternalInterfaceTemplate(externalInterfaceTemplate);
	}

	/**
	 * Gets the external interface template.
	 *
	 * @param id the id
	 * @param state the state
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate getExternalInterfaceTemplate(Long id, InterfaceTemplateStateType state) throws ServiceException {
		return interfaceTemplateServiceBean.getExternalInterfaceTemplate(id,state);
	}

	/**
	 * Block external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_TEMPLATE_BLOCK)
	public ExternalInterfaceTemplate blockExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {
		return interfaceTemplateServiceBean.blockExternalInterfaceTemplate(externalInterfaceTemplate);
	}

	/**
	 * Modify external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_TEMPLATE_MODIFY)
	public ExternalInterfaceTemplate modifyExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {		
		return interfaceTemplateServiceBean.modifyExternalInterfaceTemplate(externalInterfaceTemplate);
	}

	/**
	 * Unblock external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EXTERNAL_INTERFACE_TEMPLATE_CONFIRM)
	public ExternalInterfaceTemplate unblockExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {
		return interfaceTemplateServiceBean.unblockExternalInterfaceTemplate(externalInterfaceTemplate);
	}

}
