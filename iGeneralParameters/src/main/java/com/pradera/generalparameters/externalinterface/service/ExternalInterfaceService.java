package com.pradera.generalparameters.externalinterface.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.to.InterfaceProcessTO;
import com.pradera.generalparameters.externalinterface.utils.KeywordsSQL;
import com.pradera.generalparameters.externalinterface.view.ExternalInterfaceTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceFilterPredicate;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceTemplateDetail;
import com.pradera.model.generalparameter.externalinterface.ExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceColumn;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceExtension;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceFilter;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceQuery;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTemplate;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceUserType;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceColumnEngine;
import com.pradera.model.generalparameter.externalinterface.engine.InterfaceTableEngine;
import com.pradera.model.generalparameter.externalinterface.type.ExtensionFileStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceExtensionStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfacePredicateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateDetailType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ExternalInterfaceService  extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The parameter table. */
	private Map<Integer,String> parameterTable = new HashMap<Integer,String>();
	
	/**
	 * Registry interface template.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface registryInterfaceService(ExternalInterface externalInterface) throws ServiceException{
		create(externalInterface);
		return externalInterface;
	}
	
	/**
	 * Gets the interface template service.
	 *
	 * @return the interface template service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ExternalInterfaceTemplate> getInterfaceTemplateListService() throws ServiceException{
		StringBuilder queryInterface = new StringBuilder();
		queryInterface.append("SELECT extInt FROM ExternalInterfaceTemplate extInt WHERE extInt.templateState = :templateStateParam");
		Query query = em.createQuery(queryInterface.toString());
		query.setParameter("templateStateParam", InterfaceTemplateStateType.CONFIRMED.getCode());
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<ExternalInterfaceTemplate>();
		}
	}
	/**
	 * Gets the extension file list service.
	 *
	 * @return the extension file list service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ExtensionFileType> getExtensionFileListService() throws ServiceException{
		StringBuilder queryInterface = new StringBuilder();
		queryInterface.append("SELECT extFil FROM ExtensionFileType extFil WHERE extFil.extensionFileState = :extensionStateParam");
		Query query = em.createQuery(queryInterface.toString());
		query.setParameter("extensionStateParam", ExtensionFileStateType.REGISTERED.getCode());
		try{
			List<ExtensionFileType> extensionList = query.getResultList();
			for(ExtensionFileType extension : extensionList){
				extension.setIsSelected(Boolean.FALSE);
			}			
			return extensionList;
		}catch(NoResultException ex){
			return new ArrayList<ExtensionFileType>();
		}
	}
	/**
	 * Validated query to execute service.
	 *
	 * @param queryToExecute the query to execute
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean validatedQueryToExecuteService(String queryToExecute) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(queryToExecute.trim());
		Query query = null;
		try{
			query = em.createNativeQuery(stringBuilder.toString());
			query.setMaxResults(1);
			query.getResultList();
		}catch(Exception ex){
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_QUERY_INCORRECT, ex.getMessage().toString());			
		}
		return Boolean.TRUE;
	}
	
	/**
	 * Gets the owner database service.
	 *
	 * @return the owner database service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<String> getOwnerDatabaseService() throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT DISTINCT(OWNER) FROM ALL_TABLES");
		Query query = em.createNativeQuery(stringBuilder.toString());
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<String>();
		}
	}
	/**
	 * Gets the tables database service.
	 *
	 * @param ownerDatabase the owner database
	 * @return the tables database service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<String> getTablesDatabaseService(String ownerDatabase) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT :ownerDatabaseParam, TABLE_NAME FROM ALL_TABLES WHERE OWNER = :ownerDatabaseParam");
		Query query = em.createNativeQuery(stringBuilder.toString());
		query.setParameter("ownerDatabaseParam", ownerDatabase);
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<String>();
		}
	}
	
	/**
	 * Gets the tables engine service.
	 *
	 * @param ownerDatabase the owner database
	 * @return the tables engine service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InterfaceTableEngine> getTablesFromOwnerService(String ownerDatabase) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT OWNER,TABLE_NAME,NUM_ROWS,READ_ONLY ");
		stringBuilder.append("		 FROM All_Tables tab WHERE tab.owner = :ownerDatabaseParam ");
		stringBuilder.append("		 ORDER BY TABLE_NAME ASC");
		Query query = em.createNativeQuery(stringBuilder.toString());
		query.setParameter("ownerDatabaseParam", ownerDatabase);
		try{
			List<InterfaceTableEngine> tablesEngineList = new ArrayList<InterfaceTableEngine>();
			Iterator<Object[]> objectIterator = query.getResultList().iterator();
			while(objectIterator.hasNext()){
				Object[] objectData = objectIterator.next();
				tablesEngineList.add(new InterfaceTableEngine(objectData[0], objectData[1],objectData[2],objectData[3]));
			}
			return tablesEngineList;
		}catch(NoResultException ex){
			return new ArrayList<InterfaceTableEngine>();
		}
	}
	/**
	 * Gets the columns from table service.
	 *
	 * @param tableName the table name
	 * @return the columns from table service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InterfaceColumnEngine> getColumnsFromTableService(String tableName) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT OWNER,TABLE_NAME,COLUMN_NAME,DATA_TYPE,DATA_LENGTH,NULLABLE ");
		stringBuilder.append("		 FROM ALL_TAB_COLUMNS WHERE table_name = :tableNameParam ");
		stringBuilder.append("		 ORDER BY COLUMN_NAME ASC");
		Query query = em.createNativeQuery(stringBuilder.toString());
		query.setParameter("tableNameParam", tableName);
		try{
			List<InterfaceColumnEngine> tablesEngineList = new ArrayList<InterfaceColumnEngine>();
			Iterator<Object[]> objectIterator = query.getResultList().iterator();
			while(objectIterator.hasNext()){
				Object[] objectData = objectIterator.next();
				tablesEngineList.add(new InterfaceColumnEngine(objectData[0], objectData[1],objectData[2],objectData[3],objectData[4],objectData[5]));
			}
			return tablesEngineList;
		}catch(NoResultException ex){
			return new ArrayList<InterfaceColumnEngine>();
		}
	}
	
	/**
	 * Gets the interface template detail list facade.
	 *
	 * @param idExtInferfaceTemplatePk the id ext inferface template pk
	 * @return the interface template detail list facade
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ExtInterfaceTemplateDetail> getInterfaceTemplateDetailListFacade(Long idExtInferfaceTemplatePk) throws ServiceException{
		StringBuilder queryInterfaceDetails = new StringBuilder();
		queryInterfaceDetails.append("SELECT detail FROM ExtInterfaceTemplateDetail detail WHERE detail.externalInterfaceTemplate.idExtInferfaceTemplatePk = :templateParam");
		Query query = em.createQuery(queryInterfaceDetails.toString());
		query.setParameter("templateParam", idExtInferfaceTemplatePk);
		try{
			List<ExtInterfaceTemplateDetail> detailList = query.getResultList();
			for(ExtInterfaceTemplateDetail detail : detailList){
				detail.setDescription(InterfaceTemplateDetailType.lookup.get(detail.getTemplateDetailType()).getValue());
			}
			return detailList;
		}catch(NoResultException ex){
			return new ArrayList<ExtInterfaceTemplateDetail>();
		}
	}
	
	/**
	 * Gets the interface template facade.
	 *
	 * @param idExtInferfaceTemplatePk the id ext inferface template pk
	 * @return the interface template facade
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate getInterfaceTemplateFacade(Long idExtInferfaceTemplatePk) throws ServiceException{
		ExternalInterfaceTemplate externalInterfaceTemplate = new ExternalInterfaceTemplate();
				
		externalInterfaceTemplate = em.getReference(ExternalInterfaceTemplate.class, idExtInferfaceTemplatePk);
		for(ExtInterfaceTemplateDetail detail : externalInterfaceTemplate.getExtInterfaceTemplateDetails()){
			detail.setDescription(em.getReference(ParameterTable.class,detail.getTemplateDetailType()).getParameterName());
		}
		
		return externalInterfaceTemplate;
	}

	/**
	 * Gets the external interface list.
	 *
	 * @param externalInterfaceSearch the external interface search
	 * @return the external interface list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ExternalInterfaceTO> getExternalInterfaceList(ExternalInterfaceTO externalInterfaceSearch) throws ServiceException{
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		CriteriaQuery<ExternalInterfaceTO> criteriaQuery = criteriaBuilder.createQuery(ExternalInterfaceTO.class);
		Root<ExternalInterface> interfaceRoot = criteriaQuery.from(ExternalInterface.class);
		Join<ExternalInterfaceUserType,ExternalInterface> joinUserType = (Join) interfaceRoot.join("externalInterfaceUserTypes");
		criteriaQuery.distinct(Boolean.TRUE);
		criteriaQuery.multiselect(
				interfaceRoot.get("idExtInterfacePk"),
				interfaceRoot.get("interfaceName"),
				interfaceRoot.get("description"),
				interfaceRoot.get("registryDate"),
				interfaceRoot.get("externalInterfaceState"),
				interfaceRoot.get("executionType")
		);
		
		if(externalInterfaceSearch.getStateType() != null && externalInterfaceSearch.getStateType() > 0){
			criteriaParameters.add(criteriaBuilder.equal(interfaceRoot.get("externalInterfaceState"), externalInterfaceSearch.getStateType()));
		}
		
		if(externalInterfaceSearch.getId() != null && externalInterfaceSearch.getId() > 0){
			criteriaParameters.add(criteriaBuilder.equal(interfaceRoot.get("idExtInterfacePk"), externalInterfaceSearch.getId()));
		}
		
		if(externalInterfaceSearch.getName() != null && !externalInterfaceSearch.getName().isEmpty()){
			criteriaParameters.add(criteriaBuilder.like(interfaceRoot.<String>get("interfaceName"),"%" + externalInterfaceSearch.getName() + "%"));
		}
		
		if(externalInterfaceSearch.getDescription() != null && !externalInterfaceSearch.getDescription().isEmpty()){
			criteriaParameters.add(criteriaBuilder.like(interfaceRoot.<String>get("description"), "%" + externalInterfaceSearch.getDescription() + "%"));
		}
		
		if(externalInterfaceSearch.getUserType() != null && externalInterfaceSearch.getUserType() > 0){
			criteriaParameters.add(criteriaBuilder.equal(joinUserType.get("idUserTypeFk"), externalInterfaceSearch.getUserType()));
		}
				
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		criteriaQuery.orderBy(criteriaBuilder.desc(interfaceRoot.get("idExtInterfacePk")));
		TypedQuery<ExternalInterfaceTO> interfaceCriteria = em.createQuery(criteriaQuery);
		
		List<ExternalInterfaceTO> result = null;
		try{
			result = interfaceCriteria.getResultList();			
		}catch(NoResultException ex){
			return null;
		}
		return result;
	}
	
	/**
	 * Gets the external interface state.
	 *
	 * @param id the id
	 * @return the external interface state
	 * @throws ServiceException the service exception
	 */
	public Integer getExternalInterfaceState(Long id)  throws ServiceException{
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT externalInterfaceState FROM ExternalInterface where idExtInterfacePk = :param");
		Query query = em.createQuery(queryBuilder.toString()); 
		query.setParameter("param", id);
		try{
			return (Integer) query.getSingleResult();
		}catch(NoResultException ex){
			return 0;
		}
	}
	
	/**
	 * Gets the external interface to unblock.
	 *
	 * @param id the id
	 * @return the external interface to unblock
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface getExternalInterfaceToUnblock(Long id) throws ServiceException{	// TODO Auto-generated method stub
		Integer externalInterfaceState = getExternalInterfaceState(id);
		if(!externalInterfaceState.equals(ExternalInterfaceStateType.BLOCKED.getCode())){
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);	
		}
		return getExternalInterface(id);
	}

	/**
	 * Gets the external interface.
	 *
	 * @param id the id
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface getExternalInterface(Long id)  throws ServiceException{		
		ExternalInterface externalInterface = find(ExternalInterface.class, id);
		
		if(externalInterface != null){
			//lista de tipos de archivo
			List<String> extensions = new ArrayList<String>();
			for(ExternalInterfaceExtension extension : externalInterface.getExternalInterfaceExtensions()){
				extensions.add(extension.getExtensionFileType().getExtensionFile());
			}
			externalInterface.setInterfaceExtensionsSelected(extensions);
			
			//lista de tipos de usuario
			List<String> userTypes = new ArrayList<String>();
			for(ExternalInterfaceUserType userType : externalInterface.getExternalInterfaceUserTypes()){
				userTypes.add(InstitutionType.get(userType.getIdUserTypeFk()).getValue());
			}
			externalInterface.setInterfaceUserTypesSelected(userTypes);
			
			//lista de detalles de plantilla
			externalInterface.getExternalInterfaceFileNames().size();
			for(ExtInterfaceTemplateDetail detail : externalInterface.getExternalInterfaceTemplate().getExtInterfaceTemplateDetails()){
				
				detail.setDescription(getParameterName(detail.getTemplateDetailType()));
			}		
						
			for(ExternalInterfaceQuery query : externalInterface.getExternalInterfaceQuerys()){
				for(ExternalInterfaceTable table : query.getExternalInterfaceTables()){
					
					 for(ExternalInterfaceColumn column : table.getExternalInterfaceColumns()){
						 column.getExtInterfaceFilterPredicates();
						 //indicador de visible
						 column.setIsSelected(BooleanType.get(column.getIndVisible()).getBooleanValue());
						 //indicador de parameter table
						 column.setIsParameterTable(BooleanType.get(column.getIndParameterTable()).getBooleanValue());
						 //tipo de dato de origen de la columna
						 if(column.getDataTypeOrigin() == null){
							 column.setTypeOriginValue("");
						 }else{
							 column.setTypeOriginValue(getParameterName(column.getDataTypeOrigin()));
						 }						 
						 //tipo de dato de destino de la columna
						 column.setTypeTargetValue(getParameterName(column.getDataTypeTarget()));						 
					 }					 
					 
					 for(ExternalInterfaceFilter filter : table.getExternalInterfaceFilters()){
						 //operador logico inicial
						 filter.setLogicalOperator(getParameterName(filter.getStartLogicalOperator()));
						 
						 ExtInterfaceFilterPredicate predicate =  filter.getExtInterfaceFilterPredicates().get(0);
						 //condicion de precedencia
						 predicate.setComparisonOperatorValue(getParameterName(predicate.getComparisonOperator()));
						 //tipo de predicado
						 predicate.setPredicateTypeValue(getParameterName(predicate.getPredicateType()));
						 
						 if(predicate.getPredicateType().equals(InterfacePredicateType.COLUMN.getCode())){
							 StringBuilder predicateValue = new StringBuilder();
							 //detalle
							 predicateValue.append("[");
							 predicateValue.append(predicate.getExternalInterfaceColumn().getExternalInterfaceTable().getExternalInterfaceQuery().getInterfaceTemplateDetail().getOrderPriority());
							 predicateValue.append("]");							 
							 predicateValue.append(getParameterName(predicate.getExternalInterfaceColumn().getExternalInterfaceTable().getExternalInterfaceQuery().getInterfaceTemplateDetail().getTemplateDetailType()));							 
							 predicateValue.append(KeywordsSQL.BLANK_SPACE);
							 predicateValue.append(KeywordsSQL.GREATER);
							 predicateValue.append(KeywordsSQL.BLANK_SPACE);
							 //tabla
							 predicateValue.append(predicate.getExternalInterfaceColumn().getExternalInterfaceTable().getTableName());
							 predicateValue.append(KeywordsSQL.DOT);
							 //columna
							 predicateValue.append(predicate.getExternalInterfaceColumn().getColumnName());
							 
							 predicate.setPredicateValueColumn(predicateValue.toString());
						 }
						 filter.setInterfaceFilterPredicate(predicate);				 
					 }
					 table.getExtInterfaceFilterPredicates().size();
			 	}
			}
			 
			 externalInterface.getExternalInterfaceQuerys().size();
			 externalInterface.getExternalInterfaceSends();
		}		
		return externalInterface;
	}

	/**
	 * Gets the parameter name.
	 *
	 * @param parameterPk the parameter pk
	 * @return the parameter name
	 */
	private String getParameterName(Integer parameterPk) {
		String paramName = parameterTable.get(parameterPk);
		if(paramName == null){
		     Query query = em.createNamedQuery(ParameterTable.FIND_NAME_BY_PK);
		     query.setParameter("pk", parameterPk);
		     parameterTable.put(parameterPk, ((ParameterTable)query.getSingleResult()).getParameterName());
		     
		     paramName = parameterTable.get(parameterPk);
	    }
		return paramName;
	}

	/**
	 * Confirm external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface confirmExternalInterface(ExternalInterface externalInterface) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterface extInterface = find(ExternalInterface.class, externalInterface.getIdExtInterfacePk());
		if(extInterface.getExternalInterfaceState().equals(ExternalInterfaceStateType.REGISTERED.getCode())){
			extInterface.setExternalInterfaceState(ExternalInterfaceStateType.CONFIRMED.getCode());
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);
		}		
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("UPDATE ExternalInterface SET externalInterfaceState = :paramState where idExtInterfacePk = :paramPk");
		Query query = em.createQuery(queryBuilder.toString()); 
		query.setParameter("paramState", ExternalInterfaceStateType.CONFIRMED.getCode());
		query.setParameter("paramPk", extInterface.getIdExtInterfacePk());
		try{
			query.executeUpdate();
		}catch(NoResultException ex){
			return null;
		}
		return extInterface;
	}

	/**
	 * Gets the external interface.
	 *
	 * @param id the id
	 * @param state the state
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface getExternalInterface(Long id,ExternalInterfaceStateType state) throws ServiceException {
		Integer externalInterfaceState = getExternalInterfaceState(id);
		if(externalInterfaceState != 0){
			switch (state) {
			case REGISTERED:
				if( !externalInterfaceState.equals(ExternalInterfaceStateType.REGISTERED.getCode()) &&
					!externalInterfaceState.equals(ExternalInterfaceStateType.CONFIRMED.getCode())){
					throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);					
				}
				break;
			case ANNULATED:
				if(!externalInterfaceState.equals(ExternalInterfaceStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);					
				}
				break;
			case BLOCKED:
				if(!externalInterfaceState.equals(ExternalInterfaceStateType.CONFIRMED.getCode())){
					throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);					
				}
				break;
			case CONFIRMED:
				if(!externalInterfaceState.equals(ExternalInterfaceStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);	
				}
				break;			
			default:
				break;
			}
		}
		return getExternalInterface(id);
	}

	/**
	 * Annular external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface annularExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterface extInterface = find(ExternalInterface.class, externalInterface.getIdExtInterfacePk());
		if(extInterface.getExternalInterfaceState().equals(ExternalInterfaceStateType.REGISTERED.getCode())){
			extInterface.setExternalInterfaceState(ExternalInterfaceStateType.ANNULATED.getCode());
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);
		}		
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("UPDATE ExternalInterface SET externalInterfaceState = :paramState where idExtInterfacePk = :paramPk");
		Query query = em.createQuery(queryBuilder.toString()); 
		query.setParameter("paramState", ExternalInterfaceStateType.ANNULATED.getCode());
		query.setParameter("paramPk", extInterface.getIdExtInterfacePk());
		try{
			query.executeUpdate();
		}catch(NoResultException ex){
			return null;
		}
		return extInterface;
	}

	/**
	 * Block external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface blockExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterface extInterface = find(ExternalInterface.class, externalInterface.getIdExtInterfacePk());
		if(extInterface.getExternalInterfaceState().equals(ExternalInterfaceStateType.CONFIRMED.getCode())){
			extInterface.setExternalInterfaceState(ExternalInterfaceStateType.BLOCKED.getCode());				
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);
		}		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("UPDATE ExternalInterface SET externalInterfaceState = :paramState where idExtInterfacePk = :paramPk");
		Query query = em.createQuery(queryBuilder.toString()); 
		query.setParameter("paramState", ExternalInterfaceStateType.BLOCKED.getCode());
		query.setParameter("paramPk", extInterface.getIdExtInterfacePk());
		try{
			query.executeUpdate();
		}catch(NoResultException ex){
			return null;
		}
		return extInterface;
	}

	/**
	 * Unblock external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface unblockExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterface extInterface = find(ExternalInterface.class, externalInterface.getIdExtInterfacePk());
		if(extInterface.getExternalInterfaceState().equals(ExternalInterfaceStateType.BLOCKED.getCode())){
			extInterface.setExternalInterfaceState(ExternalInterfaceStateType.CONFIRMED.getCode());				
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_INCORRECT_STATE);
		}		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("UPDATE ExternalInterface SET externalInterfaceState = :paramState where idExtInterfacePk = :paramPk");
		Query query = em.createQuery(queryBuilder.toString()); 
		query.setParameter("paramState", ExternalInterfaceStateType.CONFIRMED.getCode());
		query.setParameter("paramPk", extInterface.getIdExtInterfacePk());
		try{
			query.executeUpdate();
		}catch(NoResultException ex){
			return null;
		}
		return extInterface;
	}

	/**
	 * Modify external interface.
	 *
	 * @param externalInterface the external interface
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface modifyExternalInterface(ExternalInterface externalInterface) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterface extInterface = find(ExternalInterface.class, externalInterface.getIdExtInterfacePk());
		
		extInterface.setInterfaceName(externalInterface.getInterfaceName());
		extInterface.setFileName(externalInterface.getFileName());
		extInterface.setExecutionType(externalInterface.getExecutionType());
		extInterface.setDescription(externalInterface.getDescription());
		extInterface.setSendPeriocity(externalInterface.getSendPeriocity());
		extInterface.setIndEmptySend(externalInterface.getIndEmptySend());
		extInterface.setIndAccessWebservice(externalInterface.getIndAccessWebservice());
		
		if(externalInterface.getInterfaceExtensionsSelected() != null){
			//cambio el estado de extensiones borradas
			for(ExternalInterfaceExtension extension : extInterface.getExternalInterfaceExtensions()) {
				if(!externalInterface.getInterfaceExtensionsSelected().contains(extension.getExtensionFileType().getExtensionFile())){
					extension.setInterfaceExtensionState(InterfaceExtensionStateType.DELETED.getCode());
				}
			}
			//agrego nuevas extensiones
			interfaceExtensionNewFor:
			for(String extensionValue : externalInterface.getInterfaceExtensionsSelected()){		
				for(ExternalInterfaceExtension extension : extInterface.getExternalInterfaceExtensions()){
					if(extension.getExtensionFileType().getExtensionFile().equals(extensionValue)){
						continue interfaceExtensionNewFor;
					}
				}		

				ExternalInterfaceExtension interfaceExtension = new ExternalInterfaceExtension();
				interfaceExtension.setExternalInterface(extInterface);
				interfaceExtension.setRegistryUser(loggerUser.getUserName());
				interfaceExtension.setRegistryDate(CommonsUtilities.currentDate());
				interfaceExtension.setInterfaceExtensionState(InterfaceExtensionStateType.REGISTERED.getCode());				
				interfaceExtension.setExtensionFileType(em.find(ExtensionFileType.class, Long.parseLong(InterfaceExtensionFileType.valueOf(extensionValue).getCode().toString())));
				if(extInterface.getExternalInterfaceExtensions() == null){
					extInterface.setExternalInterfaceExtensions(new ArrayList<ExternalInterfaceExtension>());
				}
				extInterface.getExternalInterfaceExtensions().add(interfaceExtension);
			}		
			extInterface.setSeparatorAttribute(externalInterface.getSeparatorAttribute());
		}			
		update(extInterface);
		return extInterface;
	}
	
	/**
	 * Gets the list external interface.
	 *
	 * @param indWebService the ind web service
	 * @param indInterfaceType the ind interface type
	 * @return the list external interface
	 */
	public List<ExternalInterface> getListExternalInterface(Integer indWebService, Integer indInterfaceType) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT EI ");
		stringBuffer.append(" FROM ExternalInterface EI ");
		stringBuffer.append(" WHERE EI.externalInterfaceState = :externalInterfaceState ");
		stringBuffer.append(" and EI.indInterfaceType = :indInterfaceType ");
		if (Validations.validateIsNotNull(indWebService)) {
			stringBuffer.append(" and EI.indAccessWebservice = :indWebService ");
		}
		
		TypedQuery<ExternalInterface> typedQuery= em.createQuery(stringBuffer.toString(), ExternalInterface.class);
		typedQuery.setParameter("externalInterfaceState", ExternalInterfaceStateType.CONFIRMED.getCode());
		typedQuery.setParameter("indInterfaceType", indInterfaceType);
		if (Validations.validateIsNotNull(indWebService)) {
			typedQuery.setParameter("indWebService", indWebService);
		}
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Gets the list interface process.
	 *
	 * @param interfaceProcessTO the interface process to
	 * @return the list interface process
	 */
	public List<InterfaceProcess> getListInterfaceProcess(InterfaceProcessTO interfaceProcessTO) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IP ");
		stringBuffer.append(" FROM InterfaceProcess IP ");
		stringBuffer.append(" INNER JOIN FETCH IP.idExternalInterfaceFk ");
		stringBuffer.append(" WHERE TRUNC(IP.processDate) = :processDate ");
		stringBuffer.append(" and IP.idExternalInterfaceFk.indAccessWebservice = :indWebService ");
		stringBuffer.append(" and IP.idExternalInterfaceFk.indInterfaceType = :indInterfaceType ");
		if (Validations.validateIsNotNull(interfaceProcessTO.getIdExternalInterface())) {
			stringBuffer.append(" and IP.idExternalInterfaceFk.idExtInterfacePk = :idExternalInterface ");
		}
		stringBuffer.append(" ORDER BY IP.idInterfaceProcessPk desc");
		
		TypedQuery<InterfaceProcess> typedQuery= em.createQuery(stringBuffer.toString(), InterfaceProcess.class);
		typedQuery.setParameter("processDate", interfaceProcessTO.getProcessDate());
		typedQuery.setParameter("indWebService", BooleanType.YES.getCode());
		typedQuery.setParameter("indInterfaceType", interfaceProcessTO.getInterfaceType());
		if (Validations.validateIsNotNull(interfaceProcessTO.getIdExternalInterface())) {
			typedQuery.setParameter("idExternalInterface", interfaceProcessTO.getIdExternalInterface());
		}
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Gets the list interface transaction.
	 *
	 * @param interfaceProcessTO the interface process to
	 * @return the list interface transaction
	 */
	public List<InterfaceTransaction> getListInterfaceTransaction(InterfaceProcessTO interfaceProcessTO) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT ");
		stringBuffer.append(" FROM InterfaceTransaction IT ");
		stringBuffer.append(" INNER JOIN FETCH IT.interfaceProcess IP ");
		stringBuffer.append(" INNER JOIN FETCH IP.idExternalInterfaceFk ");
		stringBuffer.append(" LEFT JOIN FETCH IT.participant pa ");
		stringBuffer.append(" LEFT JOIN FETCH IT.security sec ");
		stringBuffer.append(" WHERE TRUNC(IP.processDate) = trunc(:processDate) ");
		stringBuffer.append(" and IP.idExternalInterfaceFk.indAccessWebservice = :indWebService ");
		stringBuffer.append(" and IP.idExternalInterfaceFk.indInterfaceType = :indInterfaceType ");
		if(Validations.validateIsNotNullAndNotEmpty(interfaceProcessTO.getIdExternalInterface())) {
			stringBuffer.append(" and IP.idExternalInterfaceFk.idExtInterfacePk = :idExternalInterface ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(interfaceProcessTO.getIdParticipantPk())) {
			stringBuffer.append(" and pa.idParticipantPk = :idParticipantPk ");
		}
		stringBuffer.append(" ORDER BY IP.idInterfaceProcessPk desc");
		
		TypedQuery<InterfaceTransaction> typedQuery= em.createQuery(stringBuffer.toString(), InterfaceTransaction.class);
		typedQuery.setParameter("processDate", interfaceProcessTO.getProcessDate());
		typedQuery.setParameter("indWebService", BooleanType.YES.getCode());
		typedQuery.setParameter("indInterfaceType", interfaceProcessTO.getInterfaceType());
		if (Validations.validateIsNotNullAndNotEmpty(interfaceProcessTO.getIdExternalInterface())) {
			typedQuery.setParameter("idExternalInterface", interfaceProcessTO.getIdExternalInterface());
		}
		if(Validations.validateIsNotNullAndNotEmpty(interfaceProcessTO.getIdParticipantPk())) {
			typedQuery.setParameter("idParticipantPk", interfaceProcessTO.getIdParticipantPk());
		}
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Find external interface.
	 *
	 * @param idExternalInterface the id external interface
	 * @return the external interface
	 */
	public ExternalInterface findExternalInterface(Long idExternalInterface) {
		return this.find(ExternalInterface.class, idExternalInterface);
	}
	
	/**
	 * Gets the processed file content.
	 *
	 * @param idInterfaceProcess the id interface process
	 * @return the processed file content
	 */
	public byte[] getProcessedFileContent(Long idInterfaceProcess) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IP.fileUpload ");
		stringBuffer.append(" FROM InterfaceProcess IP ");
		stringBuffer.append(" WHERE IP.idInterfaceProcessPk = :idInterfaceProcess ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idInterfaceProcess", idInterfaceProcess);
		
		return (byte[]) query.getSingleResult();
	}
	
	/**
	 * Gets the response file content.
	 *
	 * @param idInterfaceProcess the id interface process
	 * @return the response file content
	 */
	public byte[] getResponseFileContent(Long idInterfaceProcess) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IP.fileResponse ");
		stringBuffer.append(" FROM InterfaceProcess IP ");
		stringBuffer.append(" WHERE IP.idInterfaceProcessPk = :idInterfaceProcess ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idInterfaceProcess", idInterfaceProcess);
		
		return (byte[]) query.getSingleResult();
	}
	
	
}