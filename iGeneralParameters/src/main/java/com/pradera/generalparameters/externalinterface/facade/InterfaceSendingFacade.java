package com.pradera.generalparameters.externalinterface.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.generalparameters.externalinterface.service.InterfaceSendingService;
import com.pradera.generalparameters.externalinterface.to.InfoInstitutionFromUserTO;
import com.pradera.generalparameters.externalinterface.to.TRCRtoBrokerageAgencyTO;
import com.pradera.generalparameters.externalinterface.type.SearchTRCRType;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.externalinterface.view.InterfaceSendTO;
import com.pradera.generalparameters.externalinterface.view.InterfaceSendUserTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceSendType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InterfaceSendingFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InterfaceSendingFacade{
	
	
	/** The inter generator service. */
	@EJB InterfaceSendingService interfaceSendingService;
	
	/** The batch process service facade. */
	@EJB BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	public List<TRCRtoBrokerageAgencyTO> getLstTRCRtoBrokerageAgencyTO(TRCRtoBrokerageAgencyTO filter) throws ServiceException{
		List<Object[]> lstObj=null;
		List<TRCRtoBrokerageAgencyTO> lst=new ArrayList<>();
		
		if( Validations.validateIsNotNull(filter) 
			&& Validations.validateIsNotNullAndPositive(filter.getSearchType()) ){
			
			if( filter.getSearchType().equals(SearchTRCRType.CLIENT_PORTFOLIO.getCode())
				&& Validations.validateIsNotNull(filter.getCutDate())){
				lstObj = interfaceSendingService.getLstTrcRtoBrokerageAgencyTOSearchClientPortfolio(filter);
			}
			
			if( filter.getSearchType().equals(SearchTRCRType.NEGOTIATION.getCode())
					&& Validations.validateIsNotNull(filter.getNegotiationDate())){
				lstObj = interfaceSendingService.getLstTrcRtoBrokerageAgencyTOSearchNegotiation(filter);	
			}
			
			if( filter.getSearchType().equals(SearchTRCRType.TRANSFER.getCode())
					&& Validations.validateIsNotNull(filter.getTransferDate())){
				lstObj = interfaceSendingService.getLstTrcRtoBrokerageAgencyTOSearchTransfer(filter);
			}

			if( filter.getSearchType().equals(SearchTRCRType.ACV.getCode())
					&& Validations.validateIsNotNull(filter.getAcvDate())){
				lstObj = interfaceSendingService.getLstTrcRtoBrokerageAgencyTOSearchAcv(filter);
			}

			/*if( filter.getSearchType().equals(SearchTRCRType.SERIADOS.getCode())
					&& Validations.validateIsNotNull(filter.getEmisionDate())){
				lstObj = interfaceSendingService.getLstTrcRtoBrokerageAgencyTOSearchSeriados(filter);
			}*/
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstObj)){
				TRCRtoBrokerageAgencyTO trCrSec;
				Integer isNotTrader;
				for(Object[] row:lstObj){
					trCrSec = new TRCRtoBrokerageAgencyTO();
					trCrSec.setIdSecurityCodePk(Validations.validateIsNotNull(row[0]) ? String.valueOf(row[0]) : "");
					trCrSec.setCurrencyCodeText(Validations.validateIsNotNull(row[1]) ? CurrencyType.get(Integer.parseInt(row[1].toString())).getCodeIso() : "");
					trCrSec.setCurrentNominalValue(Validations.validateIsNotNull(row[2]) ? new BigDecimal(String.valueOf(row[2])) : null );
					trCrSec.setIssuanceDate(Validations.validateIsNotNull(row[3]) ? (Date)(row[3]) : null );
					trCrSec.setIsNegotiableText("");
					if(Validations.validateIsNotNull(row[4])){
						isNotTrader = Integer.parseInt(row[4].toString());
						if(BooleanType.YES.getCode().equals(isNotTrader)){
							trCrSec.setIsNegotiableText(BooleanType.NO.getValue());		
						}else{
							trCrSec.setIsNegotiableText(BooleanType.YES.getValue());
						}
					}
					trCrSec.setStateSecurityText(Validations.validateIsNotNull(row[5]) ? SecurityStateType.get(Integer.parseInt(row[5].toString())).getValue() : "");
					lst.add(trCrSec);
				}
			}
		}
		return lst;
	}
	
	public List<Issuer> getLstIssuer() throws ServiceException {
		return interfaceSendingService.getLstIssuer();
	}

	/**
	 * Save send interface.
	 *
	 * @param externalInterfaceId the external interface id
	 * @param mapParameters the map parameters
	 * @return the external interface send
	 * @throws ServiceException the service exception
	 */
	/* (non-Javadoc)
	 * @see com.pradera.commons.externalinterface.remote.IExtInterfaceGenerator#executeExternalInterface(java.lang.Long)
	 */
	public ExternalInterfaceSend saveSendInterface(Long externalInterfaceId, Map<String,String> mapParameters) throws ServiceException {
		// TODO Auto-generated method stub
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		//getting kind of sending.
		InterfaceSendType interfaceSend ;
		String institutionRequested = null;
		if(mapParameters!=null){
			interfaceSend = InterfaceSendType.MANUAL;
			if(mapParameters.get(ExtInterfaceConstanst.INSTITUTION_ID)!=null){
				institutionRequested = mapParameters.get(ExtInterfaceConstanst.INSTITUTION_ID);
			}
		}else{
			interfaceSend = InterfaceSendType.AUTOMATIC;
		}
		return interfaceSendingService.saveSendInterfaceService(externalInterfaceId,interfaceSend.getCode(), institutionRequested);
	}
	
	public InfoInstitutionFromUserTO infoInstitutionByUser(String loginUser) throws Exception{
		InfoInstitutionFromUserTO infoInstitution=null;
		List<Object[]> lstObj=interfaceSendingService.infoInstitutionByUser(loginUser);
		if(Validations.validateListIsNotNullAndNotEmpty(lstObj)){
			for(Object[] row:lstObj){
				infoInstitution=new InfoInstitutionFromUserTO();
				infoInstitution.setIdParticipant(row[0]!= null ? Long.parseLong(row[0].toString()): null);
				infoInstitution.setMnemonic(row[1] != null ? row[1].toString() : null);
				infoInstitution.setDescriptionInstitution(row[2] != null ? row[2].toString() : null);
				break;
			}
		}
		return infoInstitution;
	}
	
	/**
	 * Gets the ext interface send user list facade.
	 *
	 * @param idInterfaceSend the id interface send
	 * @return the ext interface send user list facade
	 * @throws ServiceException the service exception
	 */
	public List<InterfaceSendUserTO> getExtInterfaceSendUserListFacade(Long idInterfaceSend) throws ServiceException{
		return interfaceSendingService.getExtInterfaceSendUserList(idInterfaceSend);
	}
	
	/**
	 * Gets the interface send facade.
	 *
	 * @param idInterfaceSend the id interface send
	 * @return the interface send facade
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceSend getInterfaceSendFacade(Long idInterfaceSend) throws ServiceException{
		return interfaceSendingService.getInterfaceSend(idInterfaceSend);
	}
	
	/**
	 * Gets the interface file and users facade.
	 *
	 * @param interfaceUser the interface user
	 * @return the interface file and users facade
	 * @throws ServiceException the service exception
	 */
	public InterfaceSendUserTO getInterfaceFileAndUsersFacade(InterfaceSendUserTO interfaceUser) throws ServiceException{
		return interfaceSendingService.getInterfaceFileUserTo(interfaceUser);
	}

	/**
	 * Gets the interface send list.
	 *
	 * @param interfaceSendSearch the interface send search
	 * @return the interface send list
	 * @throws ServiceException the service exception
	 */
	public List<InterfaceSendTO> getInterfaceSendList(InterfaceSendTO interfaceSendSearch) throws ServiceException {		
		return interfaceSendingService.getInterfaceSendList(interfaceSendSearch);
	}

	/**
	 * Gets the external interface list.
	 *
	 * @return the external interface list
	 * @throws ServiceException the service exception
	 */
	public List<ExternalInterface> getExternalInterfaceList() throws ServiceException{
		return interfaceSendingService.getExternalInterfaceList();
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantList() throws ServiceException{
		return interfaceSendingService.getParticipantList();
	}
	
	/**
	 * Gets the AFP list.
	 *
	 * @return the AFP list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getAFPList() throws ServiceException{
		return interfaceSendingService.getAFPList();
	}
	
	
	/**
	 * Getting Result Of External Interface Send.
	 *
	 * @param interfaceSend the interface send
	 * @return the results query interface
	 * @throws ServiceException the service exception
	 */
	public List<Object> getResultsQueryInterface(InterfaceSendTO interfaceSend) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		return interfaceSendingService.getResultsQueryInterface(interfaceSend);
	}


	/**
	 * Send external interface.
	 *
	 * @param interfaceSendTO the interface send to
	 * @throws ServiceException the service exception
	 */
	public void sendExternalInterface(InterfaceSendTO interfaceSendTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(ExtInterfaceConstanst.INTERFACE_PK, interfaceSendTO.getInterfaceId());
		parameters.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, interfaceSendTO.getInstitutionType());
		
		if(interfaceSendTO.getIssuerRequest().getIdIssuerPk()!=null){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, interfaceSendTO.getIssuerRequest().getIdIssuerPk());
		}else if (interfaceSendTO.getParticipantId()!=null){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, interfaceSendTO.getParticipantId());
		}
		if(interfaceSendTO.getState()!=null){
			parameters.put(ExtInterfaceConstanst.STATE, interfaceSendTO.getState());
		}
		
		if(interfaceSendTO.getInitialDate()!=null){
			parameters.put(ExtInterfaceConstanst.INITIAL_DATE, CommonsUtilities.convertDateToString(interfaceSendTO.getInitialDate(),CommonsUtilities.DATE_PATTERN));
		}
		if(interfaceSendTO.getFinalDate()!=null){
			parameters.put(ExtInterfaceConstanst.FINAL_DATE, CommonsUtilities.convertDateToString(interfaceSendTO.getFinalDate(),CommonsUtilities.DATE_PATTERN));
		}
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());
		batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, parameters);
	}
	
	/**
	 * Gets the participant by pk.
	 *
	 * @param idParticipant the id participant
	 * @return the participant by pk
	 */
	public Participant getParticipantByPk(Long idParticipant){
		return participantServiceBean.getParticipantByPk(idParticipant);
	}
	
	/**
	 * Find securities service facade.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecuritiesServiceFacade(SecurityTO securityTO) throws ServiceException{
		return interfaceSendingService.findSecuritiesLiteServiceBean(securityTO);
	}
	
	/**
	 * Fins Securities With Transfer Confirmed in the date
	 * @param dateTransfer : Date Transfer
	 * @return List String securityCode
	 * @throws ServiceException
	 */
	public List<String> findSecuritiesWithTransfer(Date dateTransfer, Long idParticipantPk) throws ServiceException{
		return interfaceSendingService.findSecuritiesWithTransfer(dateTransfer, idParticipantPk);
	}
	
	
	/**
	 * Find securities list for participant investor.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecuritiesListForParticipantInvestor(SecurityTO securityTO) throws ServiceException{
		return interfaceSendingService.findSecuritiesListForParticipantInvestor(securityTO);
	}

	/**
	 * Find issuer by filters service facade.
	 *
	 * @param filter the filter
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerByFiltersServiceFacade(Issuer filter) throws ServiceException{
		
		Issuer issuer =  interfaceSendingService.findIssuerByFiltersServiceBean(filter);
		if(issuer==null){
			filter.setDocumentSource(null);
			issuer =interfaceSendingService.findIssuerByFiltersServiceBean(filter);
		}
		
		return issuer;
	}
	/**
	 * Gets the holder account list service facade.
	 * 
	 * @param holderAccountTO
	 *            the holder account to
	 * @return the holder account list service facade
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<HolderAccount> getHolderAccountListComponentServiceFacade(
			HolderAccountTO holderAccountTO) throws ServiceException {
		return interfaceSendingService
				.getHolderAccountListComponentServiceBean(holderAccountTO);
	}
	/**Busqueda de estado*/
	public List<ParameterTable> getListStateSecurity(){
		List<ParameterTable> list = new ArrayList<ParameterTable>();
		ParameterTable parameterTable = interfaceSendingService.find(ParameterTable.class, 131);
		list.add(parameterTable);
		parameterTable = interfaceSendingService.find(ParameterTable.class, 2033);
		list.add(parameterTable);
		return list;
	}
}