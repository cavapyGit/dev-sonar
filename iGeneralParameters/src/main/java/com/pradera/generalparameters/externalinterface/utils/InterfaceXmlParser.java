package com.pradera.generalparameters.externalinterface.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExtInterfaceConstanst.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/11/2013
 */
public class InterfaceXmlParser {
	/**
	 * Gets the document parsed.
	 *
	 * @param fXmlFile the f xml file
	 * @return the document parsed
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws SAXException the sAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static Document getDocumentParsed(File fXmlFile) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document doc = null ;
		dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		return doc;
	}
	
	 /**
	 * Transform template from xml.
	 *
	 * @param nodeList the node list
	 * @return the list
	 */
	public static List<String[]> transformTemplateFromXml(NodeList nodeList) {
		List<String[]> stringData = new ArrayList<>(); 
		for (int _count = 0; _count < nodeList.getLength(); _count++) {
			Node tempIterationNode = nodeList.item(_count);
			for (int count = 0; count < tempIterationNode.getChildNodes().getLength(); count++) {
				Node tempNode = tempIterationNode.getChildNodes().item(count);
				// make sure it's element node.
				if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
					if(tempNode.getNodeName().contains(ExtInterfaceConstanst.NODE)){
						String _itemNumber = tempNode.getNodeName().substring(ExtInterfaceConstanst.NODE.length(),tempNode.getNodeName().length());
						stringData.addAll(getDataFromItem(tempNode.getChildNodes(),_itemNumber));
					}
				}
		    }
		}
	    return stringData;
	 }
	 
	 /**
	 * Transform data from xml.
	 *
	 * @param nodeList the node list
	 * @return the list
	 */
	public static List<String[]> transformDataFromXml(NodeList nodeList) {
		List<String[]> stringData = new ArrayList<>(); 
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempIterationNode = nodeList.item(i);
			for (int j = 0; j < tempIterationNode.getChildNodes().getLength(); j++) {
				Node tempNode = tempIterationNode.getChildNodes().item(j);
				if(tempNode.getNodeName().contains(ExtInterfaceConstanst.ITERATION_NODE)){
					for(int k=0; k<tempNode.getChildNodes().getLength(); k++){
						Node tempNodeItem = tempNode.getChildNodes().item(k);
						if (tempNodeItem.getNodeType() == Node.ELEMENT_NODE) {
							if(tempNodeItem.getNodeName().contains(ExtInterfaceConstanst.NODE)){
								String _itemNumber = tempNode.getNodeName().substring(ExtInterfaceConstanst.NODE.length(),tempNode.getNodeName().length());
								stringData.addAll(getDataFromItem(tempNode.getChildNodes(),_itemNumber));
							}
						}
					}
				}
		    }
		}
	    return stringData;
	 }
	
	/**
	 * Gets the elements from node list.
	 *
	 * @param nodeDataList the node data list
	 * @param templateData the template data
	 * @param docBuilder the doc builder
	 * @return the elements from node list
	 */
	public static List<Element> getElementsFromNodeList(NodeList nodeDataList, NodeList templateData, Document docBuilder){
		List<Element> elementList = new ArrayList<>();
		for (int i = 0; i < nodeDataList.getLength(); i++) {
			Node dataIterationNode = nodeDataList.item(i);
			Node templateIterationNode = templateData.item(i);
			for (int j = 0; j < dataIterationNode.getChildNodes().getLength(); j++) {
				Node dataNode = dataIterationNode.getChildNodes().item(j);
				if(dataNode.getNodeName().contains(ExtInterfaceConstanst.ITERATION_NODE)){
					elementList.add(getELementFromNode(dataNode.getChildNodes(), templateIterationNode.getChildNodes(), docBuilder));
				}
		    }
		}
		return elementList;
	}
	
	/**
	 * Gets the e lement from node.
	 *
	 * @param nodeDataList the node data list
	 * @param templateData the template data
	 * @param docBuilder the doc builder
	 * @return the e lement from node
	 */
	private static Element getELementFromNode(NodeList nodeDataList, NodeList templateData, Document docBuilder){
		Element element = docBuilder.createElement(ExtInterfaceConstanst.DATA_NODE);
		for (int count = 0; count < nodeDataList.getLength(); count++) {
			Node nodeTemplateWithData = templateData.item(count);
			Node nodeWithData = nodeDataList.item(count);
			if(nodeWithData.getNodeName().contains(ExtInterfaceConstanst.ROW_NODE)){  
				for(Element elementData: getElementListWithData(nodeWithData.getChildNodes(), nodeTemplateWithData.getChildNodes(), docBuilder)){
					element.appendChild(elementData);
				}
			}else if(nodeWithData.getNodeName().contains(ExtInterfaceConstanst.NODE)){
				element.appendChild(getELementFromNode(nodeWithData.getChildNodes(), nodeTemplateWithData.getChildNodes(), docBuilder));
			}
		}
		return element;
	}
	
	/**
	 * Gets the element list with data.
	 *
	 * @param nodeDataList the node data list
	 * @param templateData the template data
	 * @param docBuilder the doc builder
	 * @return the element list with data
	 */
	private static List<Element> getElementListWithData(NodeList nodeDataList, NodeList templateData, Document docBuilder){
		List<Element> elementList = new ArrayList<>();
		for (int count = 0; count < nodeDataList.getLength(); count++) {
			Node templateDataNode = templateData.item(count);
			Node dataNode = nodeDataList.item(count);
			Element elementData = docBuilder.createElement(templateDataNode.getTextContent().replace(" ", "_").trim());
			elementData.appendChild(docBuilder.createTextNode(dataNode.getTextContent().trim()));
			elementList.add(elementData);
		}
		return elementList;
	}
	
	/**
	 * Gets the data from item.
	 *
	 * @param nodeList the node list
	 * @param itemNumber the item number
	 * @return the data from item
	 */
	private static List<String[]> getDataFromItem(NodeList nodeList, String itemNumber){
		List<String[]> dataOfItem = new ArrayList<>();
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);
			if(tempNode.getNodeName().contains(ExtInterfaceConstanst.ROW_NODE)){  
				dataOfItem.add(getDataFromRowData(tempNode.getChildNodes(),itemNumber));
			}else if(tempNode.getNodeName().contains(ExtInterfaceConstanst.NODE)){
				String _itemNumber = tempNode.getNodeName().substring(ExtInterfaceConstanst.NODE.length(),tempNode.getNodeName().length());
				dataOfItem.addAll(getDataFromItem(tempNode.getChildNodes(), _itemNumber));
			}
		}
		return dataOfItem;
	}
	
	/**
	 * Gets the data from row data.
	 *
	 * @param nodeList the node list
	 * @param item the item
	 * @return the data from row data
	 */
	private static String[] getDataFromRowData(NodeList nodeList, String item){
		List<String> dataOfRowData = new ArrayList<>();
		dataOfRowData.add(item);
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);
			if(tempNode.getNodeName().contains(ExtInterfaceConstanst.COLUMN_NODE)){
				dataOfRowData.add(tempNode.getTextContent().toString());
			}
		}
		return dataOfRowData.toArray(new String[0]);
	}
}