package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27-nov-2013
 */
public class ExternalInterfaceTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The id. */
	private Long id;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The register date. */
	private Date registerDate;
	
	/** The state type. */
	private Integer stateType;
	
	/** The execution type. */
	private Integer executionType;
	
	/** The state type value. */
	private String stateTypeValue;
	
	/** The user type. */
	private Integer userType;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The activate send. */
	private Boolean activateSend;
	/**
	 * Instantiates a new external interface to.
	 */
	public ExternalInterfaceTO() {
		initialDate = new Date();
		finalDate = new Date();		
	}
	
	/**
	 * Instantiates a new external interface to.
	 *
	 * @param id the id
	 * @param name the name
	 * @param description the description
	 * @param registerDate the register date
	 * @param stateType the state type
	 * @param executionType the execution type
	 */
	public ExternalInterfaceTO(Long id, String name, String description,
								Date registerDate,Integer stateType, Integer executionType) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.registerDate = registerDate;
		this.stateType = stateType;
		this.executionType = executionType;
	}
	
	/**
	 * Instantiates a new external interface to.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public ExternalInterfaceTO(long id, String name){
		this.id = id;
		this.name = name;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the state type.
	 *
	 * @return the state type
	 */
	public Integer getStateType() {
		return stateType;
	}

	/**
	 * Sets the state type.
	 *
	 * @param stateType the new state type
	 */
	public void setStateType(Integer stateType) {
		this.stateType = stateType;
	}

	/**
	 * Gets the state type value.
	 *
	 * @return the state type value
	 */
	public String getStateTypeValue() {
		return stateTypeValue;
	}

	/**
	 * Sets the state type value.
	 *
	 * @param stateTypeValue the new state type value
	 */
	public void setStateTypeValue(String stateTypeValue) {
		this.stateTypeValue = stateTypeValue;
	}

	/**
	 * Gets the user type.
	 *
	 * @return the user type
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * Sets the user type.
	 *
	 * @param userType the new user type
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * Gets the activate send.
	 *
	 * @return the activate send
	 */
	public Boolean getActivateSend() {
		return activateSend;
	}

	/**
	 * Sets the activate send.
	 *
	 * @param activateSend the new activate send
	 */
	public void setActivateSend(Boolean activateSend) {
		this.activateSend = activateSend;
	}

	/**
	 * Gets the execution type.
	 *
	 * @return the execution type
	 */
	public Integer getExecutionType() {
		return executionType;
	}

	/**
	 * Sets the execution type.
	 *
	 * @param executionType the new execution type
	 */
	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}
	
	
}
