package com.pradera.generalparameters.externalinterface.facade;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.generalparameters.externalinterface.service.InterfaceGeneratorService;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceUserType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InterfaceGeneratorFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
//public class InterfaceGeneratorFacade  implements IExtInterfaceGenerator{
	public class InterfaceGeneratorFacade {
	
	/** The log. */
	@Inject
	private PraderaLogger logger;
	
	/** The inter generator service. */
	@EJB
	InterfaceGeneratorService interGeneratorService;
	
//	@Inject @ReportGenerationServiceLocator
//	ReportGenerationService reportGenerationService;

	/**
 * Execute external interface.
 *
 * @param interfaceSend the interface send
 * @param parameters the parameters
 * @throws ServiceException the service exception
 */
/* (non-Javadoc)
	 * @see com.pradera.commons.externalinterface.remote.IExtInterfaceGenerator#executeExternalInterface(java.lang.Long)
	 */
	public void executeExternalInterface(ExternalInterfaceSend interfaceSend,Map<String,String> parameters) throws ServiceException {
		// TODO Auto-generated method stub
		ExternalInterface externalInterface = interGeneratorService.generateExternalInterface(interfaceSend,parameters);
		logger.info(externalInterface.toString());
	}
	
	/**
	 * Gets the list external interface user type.
	 *
	 * @param idInterfacePk the id interface pk
	 * @return the list external interface user type
	 * @throws ServiceException the service exception
	 */
	public List<ExternalInterfaceUserType> getListExternalInterfaceUserType(Long idInterfacePk) throws ServiceException{
		return interGeneratorService.getListExternalInterfaceUserType(idInterfacePk);
	}
}