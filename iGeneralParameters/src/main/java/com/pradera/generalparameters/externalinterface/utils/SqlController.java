package com.pradera.generalparameters.externalinterface.utils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.pradera.model.generalparameter.externalinterface.type.InterfaceOperatorLogicType;
import com.pradera.model.generalparameter.externalinterface.type.InterfacePredicateType;
import com.pradera.model.generalparameter.externalinterface.type.SqlComparatorType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class SqlController.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29-nov-2013
 */
public class SqlController extends SimpleQuerySQL{
	
	/** The subquery list. */
	private List<SimpleQuerySQL> subqueryList;

	/**
	 * Instantiates a new sql controller.
	 *
	 * @param query the query
	 */
	public SqlController(String query) {
		subqueryList = new ArrayList<SimpleQuerySQL>();	
		setRelationAliasTable(new HashMap<String, String>());
		setRelationships(new HashMap<String, List<String>>());
		setRelationAliasColumn(new HashMap<String,Map<String,String>>());
		extractSubQuerys(query.toLowerCase());			
	}
	
	/**
	 * Instantiates a new sql controller.
	 *
	 * @param Query the query
	 */
	public SqlController(Map<String,List<String>> Query){
		setSelectParamsList(new ArrayList<String>());
		setFromParamsList(new ArrayList<String>());
		setWhereParamsList(new ArrayList<String>());
		
		setTableListForAlias(new ArrayList<String>());
		setColumnListForAlias(new ArrayList<String>());
		
		transformToQuery(Query);
	}
	
	/**
	 * Gets the table alias.
	 *
	 * @param tableName the table name
	 * @return the table alias
	 */
	public static String getTableAlias(String tableName){
		return generateTableAlias(tableName);		
	}

	/**
	 * Transform to query.
	 *
	 * @param query the query
	 * @return the string
	 */
	private void transformToQuery(Map<String, List<String>> query) {
		for(Entry<String, List<String>> e : query.entrySet()){
			addSelectFromParams(e);
		}
		generateLexicalGroup();
	}

	/**
	 * Extract sub querys.
	 *
	 * @param query the query
	 */
	private void extractSubQuerys(String query) {
		int SQ_cont = 0;

		while (query.contains(KeywordsSQL.BRACE_OPEN)) {
			int start = 0;
			int end = 0;
			for (int i = 0; i < query.length(); i++) {
				if (query.charAt(i) == KeywordsSQL.BRACE_OPEN_CHAR) {
					start = i;
				}
				if (query.charAt(i) == KeywordsSQL.BRACE_CLOSE_CHAR) {
					end = i;
					SimpleQuerySQL subquery =  new SimpleQuerySQL(query.substring(start+1, end).trim());//(SELECT SYSDATE FROM DUAL)
					subqueryList.add(subquery);
					query = query.substring(0, start).concat(String.format(KeywordsSQL.SUBQUERY_PARAM, SQ_cont)).concat(query.substring(end + 1));
					SQ_cont++;
					break;
				}
			}
		}

		this.setSimpleQuery(query);
		this.extractLexicalGroup();
		this.extractParams();
	}
	
	/* getters and setters */

	/**
	 * Gets the subquery list.
	 *
	 * @return the subquery list
	 */
	public List<SimpleQuerySQL> getSubqueryList() {
		return subqueryList;
	}
	/**
	 * Sets the subquery list.
	 *
	 * @param subqueryList the new subquery list
	 */
	public void setSubqueryList(List<SimpleQuerySQL> subqueryList) {
		this.subqueryList = subqueryList;
	}	
	/**
	 * Have columns.
	 *
	 * @return the boolean
	 */
	public Boolean haveColumns(){
		if(this.select.trim().equals(KeywordsSQL.ASTERISK)){
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * Adds the filter to query.
	 *
	 * @param operatorLogicType the operator logic type
	 * @param tableName the table name
	 * @param columnName the column name
	 * @param sqlComparatorType the sql comparator type
	 * @param predicateType the predicate type
	 * @param predicate the predicate
	 * @return true, if successful
	 */
	public String addFilterToQuery(InterfaceOperatorLogicType operatorLogicType, 
									String tableName,
									String columnName,
									SqlComparatorType sqlComparatorType,
									InterfacePredicateType predicateType,
									String predicate) {
		
		String tableNameFilter = (tableName.split(KeywordsSQL.BLANK_SPACE)[1]).toLowerCase();
		//el nombre de la tabla ya debe estar contenido en los alias de tabla
		if(this.getRelationAliasTable().containsKey(tableNameFilter)){
			
			if(this.getWhereParamsList() == null){
				this.setWhereParamsList(new ArrayList<String>());
			}
			
			StringBuilder filter = new StringBuilder();
			
			for(Entry<String, String> entry : this.getRelationAliasTable().entrySet()){
				if(entry.getKey().equalsIgnoreCase(tableNameFilter)){
					//operador logico inicial
					if(this.getWhereParamsList().size() > 0){
						filter.append(operatorLogicType.getValue());
						filter.append(KeywordsSQL.BLANK_SPACE);
					}					
					//nombre de alias de tabla
					filter.append(entry.getKey());
					filter.append(KeywordsSQL.DOT);
					//nombre de columna
					filter.append(columnName);
					filter.append(KeywordsSQL.BLANK_SPACE);
					//comparador
					filter.append(sqlComparatorType.getValue());
					filter.append(KeywordsSQL.BLANK_SPACE);
					//si el predicado es otra columna de tabla
					if(InterfacePredicateType.COLUMN.equals(predicateType)){
						
						for(Entry<String, String> entryTableColumn : this.getRelationAliasTable().entrySet()){
							
							String[] tableColumn = predicate.split("\\".concat(KeywordsSQL.DOT));
							String tableAliasValue = tableColumn[0].split(KeywordsSQL.BLANK_SPACE)[1].trim();
							if(entryTableColumn.getKey().equalsIgnoreCase(tableAliasValue)){
								if(tableColumn.length > 2 && tableColumn[2].equals(ExtInterfaceConstanst.IDENTIFICATOR)){
									filter.append(ExtInterfaceConstanst.IDENTIFICATOR);
								}else{
									filter.append(entryTableColumn.getKey());
									filter.append(KeywordsSQL.DOT);
									filter.append(tableColumn[1]);
								}
								return filter.toString();
							}
						}
						filter.append(ExtInterfaceConstanst.IDENTIFICATOR);
					}else if (InterfacePredicateType.QUERY.equals(predicateType)){
						filter.append(KeywordsSQL.BRACE_OPEN);
						filter.append(predicate);
						filter.append(KeywordsSQL.BRACE_CLOSE);
					}else if(InterfacePredicateType.USER_TYPE.equals(predicateType)){
						filter.append(ExtInterfaceConstanst.IDENTIFICATOR);
					}else{	
						filter.append(predicate);
					}
					return filter.toString().toUpperCase();
				}
			}
		}
		return null;
	}

	/**
	 * Adds the where to sql.
	 *
	 * @return the string
	 */
	public String addWhereToSQL() {
		updateSimpleQueryWithWhere();
		return this.getSimpleQuery();
	}
}