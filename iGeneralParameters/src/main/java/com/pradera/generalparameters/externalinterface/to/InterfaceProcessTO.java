package com.pradera.generalparameters.externalinterface.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InterfaceProcessTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class InterfaceProcessTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id external interface. */
	private Long idExternalInterface;
	
	/** The interface type. */
	private Integer interfaceType;
	
	/** The process date. */
	private Date processDate;
	
	/** The file name. */
	private String fileName;
	
	/** The interface file. */
	private byte[] interfaceFile;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	
	/**
	 * Instantiates a new interface process to.
	 */
	public InterfaceProcessTO() {
		super();
	}

	/**
	 * Gets the id external interface.
	 *
	 * @return the id external interface
	 */
	public Long getIdExternalInterface() {
		return idExternalInterface;
	}

	/**
	 * Sets the id external interface.
	 *
	 * @param idExternalInterface the new id external interface
	 */
	public void setIdExternalInterface(Long idExternalInterface) {
		this.idExternalInterface = idExternalInterface;
	}

	/**
	 * Gets the interface type.
	 *
	 * @return the interface type
	 */
	public Integer getInterfaceType() {
		return interfaceType;
	}

	/**
	 * Sets the interface type.
	 *
	 * @param interfaceType the new interface type
	 */
	public void setInterfaceType(Integer interfaceType) {
		this.interfaceType = interfaceType;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the process date
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the new process date
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the interface file.
	 *
	 * @return the interface file
	 */
	public byte[] getInterfaceFile() {
		return interfaceFile;
	}

	/**
	 * Sets the interface file.
	 *
	 * @param interfaceFile the new interface file
	 */
	public void setInterfaceFile(byte[] interfaceFile) {
		this.interfaceFile = interfaceFile;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	
}
