package com.pradera.generalparameters.externalinterface.resources;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.generalparameters.externalinterface.facade.InterfaceGeneratorFacade;
import com.pradera.generalparameters.externalinterface.facade.InterfaceSendingFacade;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.indexandrates.service.IndexAndRateExpireBatch;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@BatchProcess(name="ExternalInterfaceGeneratorBatch")
@RequestScoped
public class ExternalInterfaceGeneratorBatch implements JobExecution {
	
	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(IndexAndRateExpireBatch.class);
	
	/** The interface sending service. */
	@EJB
	InterfaceSendingFacade interfaceSendFacade;
	
	/** The generator facade. */
	@EJB
	InterfaceGeneratorFacade generatorFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		try {
			Long externalInterfaceId = null;
			Map<String,String> mapUserParams = new HashMap<>();
			List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
			for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
				if(objDetail.getParameterName().equals(ExtInterfaceConstanst.INTERFACE_PK)){
					externalInterfaceId = new Long(objDetail.getParameterValue());
				}else if(objDetail.getParameterName().equals(ExtInterfaceConstanst.INSTITUTION_TYPE_ID)){
					mapUserParams.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, objDetail.getParameterValue());
				}else if(objDetail.getParameterName().equals(ExtInterfaceConstanst.INSTITUTION_ID)){
					mapUserParams.put(ExtInterfaceConstanst.INSTITUTION_ID, objDetail.getParameterValue());
				}else if(objDetail.getParameterName().equals(ExtInterfaceConstanst.INITIAL_DATE)){
					mapUserParams.put(ExtInterfaceConstanst.INITIAL_DATE, objDetail.getParameterValue());
				}else if(objDetail.getParameterName().equals(ExtInterfaceConstanst.FINAL_DATE)){
					mapUserParams.put(ExtInterfaceConstanst.FINAL_DATE, objDetail.getParameterValue());
				}else if(objDetail.getParameterName().equals(ExtInterfaceConstanst.SECURITY_CODES)){
					mapUserParams.put(ExtInterfaceConstanst.SECURITY_CODES, objDetail.getParameterValue());
				} else if(objDetail.getParameterName().equals(ExtInterfaceConstanst.SEND_FTP)) {
					mapUserParams.put(ExtInterfaceConstanst.SEND_FTP, objDetail.getParameterValue());
				} else if(objDetail.getParameterName().equals(ExtInterfaceConstanst.STATE)){
					mapUserParams.put(ExtInterfaceConstanst.STATE, objDetail.getParameterValue());
				}
			}
			executeExternalInterface(externalInterfaceId,mapUserParams);
		} catch (ServiceException e) {
			e.printStackTrace();
			logger.error("autoExpireFinancialIndicator");
		}
	}

	/**
	 * Execute external interface.
	 *
	 * @param interfaceId the interface id
	 * @param userParams the user params
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void executeExternalInterface(Long interfaceId, Map<String,String> userParams) throws ServiceException {
		// TODO Auto-generated method stub
		if(interfaceId!=null){
			generatorFacade.executeExternalInterface(interfaceSendFacade.saveSendInterface(interfaceId,userParams),userParams);			
		}else{
			for(ExternalInterface interfac: interfaceSendFacade.getExternalInterfaceList()){
				Map<String,String> parameters = new HashMap<>();
				parameters.put(ExtInterfaceConstanst.INITIAL_DATE, CommonsUtilities.currentDate().toString());
				parameters.put(ExtInterfaceConstanst.FINAL_DATE, CommonsUtilities.currentDate().toString());
				parameters.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, interfac.getExternalInterfaceUserTypes().get(0).getIdUserTypeFk().toString());
				
				generatorFacade.executeExternalInterface(interfaceSendFacade.saveSendInterface(interfac.getIdExtInterfacePk(),parameters),parameters);
			}
		}
	}
	

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
	
}
