package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.generalparameters.externalinterface.facade.InterfaceSendingFacade;
import com.pradera.generalparameters.externalinterface.to.InfoInstitutionFromUserTO;
import com.pradera.generalparameters.externalinterface.to.TRCRtoBrokerageAgencyTO;
import com.pradera.generalparameters.externalinterface.type.SearchTRCRType;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.process.BusinessProcess;



/**
 * @author rlarico
 */

@DepositaryWebBean
public class InterfaceSendTRCRtoABMgmt extends GenericBaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UserInfo userInfo;

	@Inject
	private UserPrivilege userPrivilege;

	@Inject
	private transient PraderaLogger log;

	@EJB
	GeneralParametersFacade generalParametersFacade;

	@EJB
	private AccountsFacade accountsFacade;

	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;

	@EJB
	InterfaceSendingFacade interfaceSendingFacade;
	
	private TRCRtoBrokerageAgencyTO modelTOSearch;
	private List<TRCRtoBrokerageAgencyTO> lstTRCRtoBrokerageAgencyTO;
	private TRCRtoBrokerageAgencyDataModel trCrtoBrokerageAgencyDataModel = new TRCRtoBrokerageAgencyDataModel();
	
	private List<TRCRtoBrokerageAgencyTO> listSecuritySelected;
	
	private boolean privilegeSearch;
	private boolean privilegeGenerate;
	
	private boolean isUserDepositary;
	
	private boolean quantityRowSelectValid;
	
	@PostConstruct
	public void init(){
		loadSearch();
	}
	
	/** metodo para cargar la pantalla de busqueda(administracion) */
	public void loadSearch() {
		
		privilegeSearch = false;
		privilegeGenerate = false;

		modelTOSearch = new TRCRtoBrokerageAgencyTO();
		if (userPrivilege.getUserAcctions().isSearch()) {
			log.info("::: tiene privilegios de BUSQUEDA");
			privilegeSearch = true;
		}
		if (userPrivilege.getUserAcctions().isGenerate()) {
			log.info("::: tiene privilegios de GENERAR");
			privilegeGenerate = true;
		}
		
		// verificando si es usuario iDepositary o participante
		isUserDepositary = false;
		modelTOSearch.setLstParticipant(getLstParticipant());
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			isUserDepositary = true;
		}else{
			restrictForExternalUser();
		}

		modelTOSearch.setLstSearchType(getLstSearchTRCRType());
		modelTOSearch.setLstIssuer(getLstIssuer());
		modelTOSearch.setLstCurrency(getLstCurrency());
		modelTOSearch.setLstSecurityClass(getLstSecurityClass());
		
		lstTRCRtoBrokerageAgencyTO = new ArrayList<>();
		listSecuritySelected = new ArrayList<>();
		trCrtoBrokerageAgencyDataModel = null;//new TRCRtoBrokerageAgencyDataModel();
	}
	
	/** realizando la busqueda segun los criterios seleccionados */
	public void btnSearch() {
		try {
			lstTRCRtoBrokerageAgencyTO = interfaceSendingFacade.getLstTRCRtoBrokerageAgencyTO(modelTOSearch);
			trCrtoBrokerageAgencyDataModel = new TRCRtoBrokerageAgencyDataModel(lstTRCRtoBrokerageAgencyTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** limpiando la pantalla de search */
	public void btnClearSearch() {
		loadSearch();
		JSFUtilities.resetViewRoot();
	}
	
	public void disabledFilterSecurityClass(){
		if(Validations.validateIsNotNull(modelTOSearch)
				&& Validations.validateIsNotNull(modelTOSearch.getIsNegotiable())){
			modelTOSearch.setSecurityClass(null);
		}
	}
	
	/** limpinado el helper de CUI cada q se selecciona el participante o cada que salta una validacion */
	public void cleanHelperCUI() {
		log.info(" valor del participante seleccionado: " + modelTOSearch.getIdParticipantPk());
		modelTOSearch.setHolder(new Holder());
		modelTOSearch.setHolderAccount(new HolderAccount());
	}
	
	/**
	 * metodo para restringir en caso de que el usuario logedo sea participante
	 */
	public void restrictForExternalUser(){
		if(Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getParticipantCode())){
			modelTOSearch.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			List<Participant> lstAux=new ArrayList<>();
			for(Participant p:modelTOSearch.getLstParticipant()){
				if(p.getIdParticipantPk().equals(modelTOSearch.getIdParticipantPk())){
					lstAux.add(p);
					break;
				}
			}
			modelTOSearch.setLstParticipant(lstAux);
		}
	}
	
	public void changeHolder(){
		modelTOSearch.setHolderAccount(new HolderAccount());
	}
	
	public void clearWhenChengSearchType(){
		lstTRCRtoBrokerageAgencyTO = new ArrayList<>();
		listSecuritySelected = new ArrayList<>();
		trCrtoBrokerageAgencyDataModel = null;//new TRCRtoBrokerageAgencyDataModel();
	}
	
	/**
	 * valida que el numero de caracteres sea maximo 1500 bytes, esto debido al limite del campo PARAMETER_VALUE de la tabla PROCESS_LOGGER_DETAIL
	 */
	public void validateQuantitySelectSecurities(){
		quantityRowSelectValid = true;
		try {
			JSFUtilities.updateComponent("opnlDialogs");
			List<Object> listIdSecurity=new ArrayList<>();
			for (TRCRtoBrokerageAgencyTO security : listSecuritySelected) {
				listIdSecurity.add(security.getIdSecurityCodePk());
			}

			String idSecurityPks=CommonsUtilities.convertListObjectToString(listIdSecurity, GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);
			// si es mayor a 1500 caracteres lanzar validacion
			if(Validations.validateIsNotNullAndNotEmpty(idSecurityPks)
					&& idSecurityPks.length()>1500){
				quantityRowSelectValid = false;
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.LBL_VALIDATION_QUANTITY_ROWSELECTED_SECURITIES, null);
				JSFUtilities.executeJavascriptFunction("PF('alertValidationMgmtBean').show();");
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("::: Error en la validacion de cantidad de selecion "+e.getMessage());
		}
		
	}
	
	/**
	 * issue 1368: Esta basado en el bean: InterfaceSendTRCRMgmt -> Metodo: generateInterfaceFileTRCR
	 * Generate interface file TR CR.
	 */
	@LoggerAuditWeb
	public void generateInterfaceFileTRCR(){
		
		validateQuantitySelectSecurities();
		if(quantityRowSelectValid){
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.hideComponent("alertValidationMgmtBean");
		}else{
			return;
		}
		
		List<Object> listIdSecurity=new ArrayList<>();
//		for (TRCRtoBrokerageAgencyTO secTRCR : trCrtoBrokerageAgencyDataModel) {
//			if( Validations.validateIsNotNull(secTRCR)
//				&& secTRCR.isSelectedRow() ){
//				listIdSecurity.add(secTRCR.getIdSecurityCodePk());
//			}
//		}
		
		if(Validations.validateListIsNullOrEmpty(listSecuritySelected)){
			JSFUtilities.updateComponent("opnlDialogs");
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.LBL_VALIDATION_QUANTITY_REQUIRED_SELECTEDROW, null);
			JSFUtilities.executeJavascriptFunction("PF('alertValidationMgmtBean').show();");
			return;	
		}
		
		for (TRCRtoBrokerageAgencyTO security : listSecuritySelected) {
			listIdSecurity.add(security.getIdSecurityCodePk());
		}

		String idSecurityPks=CommonsUtilities.convertListObjectToString(listIdSecurity, GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE); 
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(ExtInterfaceConstanst.INTERFACE_PK, GeneralConstants.EXTERNAL_INTERFACE_TR_CR.toString());
		parameters.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, userInfo.getUserAccountSession().getInstitutionType());
		parameters.put(ExtInterfaceConstanst.SECURITY_CODES, idSecurityPks);
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isAfpInstitution()){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getParticipantCode());
		}else if (userInfo.getUserAccountSession().isIssuerInstitucion()){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getIssuerCode());
		}else{
			
			// issue 1255: obtienedo forzamente el id del participante, porque en el userinfo no captura cuando pasa por esta ruta
			if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getUserName())){
				try {
					InfoInstitutionFromUserTO infoInstitution=interfaceSendingFacade.infoInstitutionByUser(userInfo.getUserAccountSession().getUserName());
					if(Validations.validateIsNotNull(infoInstitution)){
						if(Validations.validateIsNotNull(infoInstitution.getMnemonic())){
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, infoInstitution.getMnemonic());
						}else if(Validations.validateIsNotNull(infoInstitution.getIdParticipant())){
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, infoInstitution.getIdParticipant());
						}else if(Validations.validateIsNotNull(infoInstitution.getDescriptionInstitution())){
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, infoInstitution.getDescriptionInstitution());
						}else {
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getDescInstitutionType());
						}
					}
				} catch (Exception e) {
					parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getDescInstitutionType());	
				}
			}else{
				parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getDescInstitutionType());
			}
		}
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, parameters);
			
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(PropertiesConstants.DIALOG_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM_OK
									));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	// TODO metodos genericos para cargar las listas(combobox) necesarios para cualquier pantalla (registro, vista, search)
	public List<Participant> getLstParticipant() {
		try {
			Participant filter = new Participant();
			filter.setState(ParticipantStateType.REGISTERED.getCode());
			return accountsFacade.getLisParticipantServiceBean(filter);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	public List<SearchTRCRType> getLstSearchTRCRType(){
		try {
			List<SearchTRCRType> lst = new ArrayList<>();
			lst.addAll(SearchTRCRType.list);
			return lst;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return new ArrayList<>();
		}
	}
	
	public List<Issuer> getLstIssuer() {
		try {
			return interfaceSendingFacade.getLstIssuer();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	public List<ParameterTable> getLstCurrency() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	public List<ParameterTable> getLstSecurityClass() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

}