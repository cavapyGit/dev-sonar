package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceSendTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18-dic-2013
 */
public class InterfaceSendUserTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The Id. */
	private Long trackId;
	
	/** The interface id. */
	private Integer interfaceId;
	
	/** The institution type. */
	private Integer institutionType;
	 
	/** The institution description. */
	private String institutionTypeDes;
	
	/** The execution type. */
	private String institutionCode;

	/** The initial date. */
	private Date sendHour;
	
	/** The send start hour. */
	private Date sendStartHour;
	
	/** The final date. */
	private Integer state;
	
	/** The state description. */
	private String stateDescription;
	
	/** The error. */
	private String error;
	
	/** The string files. */
	private List<String> stringFiles;
	
	/** The users of institution. */
	private String usersOfInstitution;

	/**
	 * Gets the track id.
	 *
	 * @return the track id
	 */
	public Long getTrackId() {
		return trackId;
	}

	/**
	 * Sets the track id.
	 *
	 * @param trackId the new track id
	 */
	public void setTrackId(Long trackId) {
		this.trackId = trackId;
	}

	/**
	 * Gets the interface id.
	 *
	 * @return the interface id
	 */
	public Integer getInterfaceId() {
		return interfaceId;
	}

	/**
	 * Sets the interface id.
	 *
	 * @param interfaceId the new interface id
	 */
	public void setInterfaceId(Integer interfaceId) {
		this.interfaceId = interfaceId;
	}


	/**
	 * Gets the institution code.
	 *
	 * @return the institution code
	 */
	public String getInstitutionCode() {
		return institutionCode;
	}

	/**
	 * Sets the institution code.
	 *
	 * @param institutionCode the new institution code
	 */
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	/**
	 * Gets the send hour.
	 *
	 * @return the send hour
	 */
	public Date getSendHour() {
		return sendHour;
	}

	/**
	 * Sets the send hour.
	 *
	 * @param sendHour the new send hour
	 */
	public void setSendHour(Date sendHour) {
		this.sendHour = sendHour;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * Sets the error.
	 *
	 * @param error the new error
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * Gets the institution type des.
	 *
	 * @return the institution type des
	 */
	public String getInstitutionTypeDes() {
		return institutionTypeDes;
	}

	/**
	 * Sets the institution type des.
	 *
	 * @param institutionTypeDes the new institution type des
	 */
	public void setInstitutionTypeDes(String institutionTypeDes) {
		this.institutionTypeDes = institutionTypeDes;
	}

	/**
	 * Instantiates a new interface send user to.
	 *
	 * @param trackId the track id
	 * @param institutionType the institution type
	 * @param institutionCode the institution code
	 * @param sendStartHour the send start hour
	 * @param sendHour the send hour
	 * @param error the error
	 * @param state the state
	 * @param stateDescription the state description
	 */
	public InterfaceSendUserTO(Long trackId, Integer institutionType, String institutionCode, Date sendStartHour, Date sendHour, String error, 
							   Integer state, String stateDescription) {
		super();
		this.trackId = trackId;
		this.institutionType = institutionType;
		this.institutionCode = institutionCode;
		this.sendStartHour = sendStartHour;
		this.sendHour = sendHour;
		this.error = error;
		this.state = state;
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the send start hour.
	 *
	 * @return the send start hour
	 */
	public Date getSendStartHour() {
		return sendStartHour;
	}

	/**
	 * Sets the send start hour.
	 *
	 * @param sendStartHour the new send start hour
	 */
	public void setSendStartHour(Date sendStartHour) {
		this.sendStartHour = sendStartHour;
	}

	/**
	 * Gets the string files.
	 *
	 * @return the string files
	 */
	public List<String> getStringFiles() {
		return stringFiles;
	}

	/**
	 * Sets the string files.
	 *
	 * @param stringFiles the new string files
	 */
	public void setStringFiles(List<String> stringFiles) {
		this.stringFiles = stringFiles;
	}

	/**
	 * Gets the users of institution.
	 *
	 * @return the users of institution
	 */
	public String getUsersOfInstitution() {
		return usersOfInstitution;
	}

	/**
	 * Sets the users of institution.
	 *
	 * @param usersOfInstitution the new users of institution
	 */
	public void setUsersOfInstitution(String usersOfInstitution) {
		this.usersOfInstitution = usersOfInstitution;
	}

	/**
	 * Instantiates a new interface send user to.
	 */
	public InterfaceSendUserTO() {
		super();
	}
}