package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.generalparameters.externalinterface.to.TRCRtoBrokerageAgencyTO;


public class TRCRtoBrokerageAgencyDataModel extends ListDataModel<TRCRtoBrokerageAgencyTO> implements SelectableDataModel<TRCRtoBrokerageAgencyTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// constructor 1
	public TRCRtoBrokerageAgencyDataModel(){}
	
	// constructor 2
	public TRCRtoBrokerageAgencyDataModel(List<TRCRtoBrokerageAgencyTO> lst){
		super(lst);
	}

	@Override
	public TRCRtoBrokerageAgencyTO getRowData(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getRowKey(TRCRtoBrokerageAgencyTO arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
