package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceTemplateTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13-dic-2013
 */
public class InterfaceTemplateTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The id. */
	private Long id;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The register date. */
	private Date registerDate;

	/** The element template type. */
	private ParameterTable elementTemplateType;
	
	/** The number of elements. */
	private Integer numberOfElements;
	
	/** The state type. */
	private Integer stateType;
	
	/** The state type value. */
	private String stateTypeValue;

	/**
	 * Instantiates a new interface template to.
	 */
	public InterfaceTemplateTO() {
		elementTemplateType = new ParameterTable();
		initialDate = new Date();
		finalDate = new Date();
	}

	/**
	 * Instantiates a new interface template to.
	 *
	 * @param id the id
	 * @param name the name
	 * @param description the description
	 * @param registerDate the register date
	 * @param stateType the state type
	 */
	public InterfaceTemplateTO(Long id, String name, String description,
			Date registerDate,Integer stateType) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.registerDate = registerDate;
		this.stateType = stateType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the element template type.
	 *
	 * @return the element template type
	 */
	public ParameterTable getElementTemplateType() {
		return elementTemplateType;
	}

	/**
	 * Sets the element template type.
	 *
	 * @param elementTemplateType the new element template type
	 */
	public void setElementTemplateType(ParameterTable elementTemplateType) {
		this.elementTemplateType = elementTemplateType;
	}

	/**
	 * Gets the number of elements.
	 *
	 * @return the number of elements
	 */
	public Integer getNumberOfElements() {
		return numberOfElements;
	}

	/**
	 * Sets the number of elements.
	 *
	 * @param numberOfElements the new number of elements
	 */
	public void setNumberOfElements(Integer numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the state type.
	 *
	 * @return the state type
	 */
	public Integer getStateType() {
		return stateType;
	}

	/**
	 * Sets the state type.
	 *
	 * @param stateType the new state type
	 */
	public void setStateType(Integer stateType) {
		this.stateType = stateType;
	}

	/**
	 * Gets the state type value.
	 *
	 * @return the state type value
	 */
	public String getStateTypeValue() {
		if(stateType != null){
			stateTypeValue = InterfaceTemplateStateType.lookup.get(stateType).getValue();
		}		
		return stateTypeValue;
	}

	/**
	 * Sets the state type value.
	 *
	 * @param stateTypeValue the new state type value
	 */
	public void setStateTypeValue(String stateTypeValue) {
		this.stateTypeValue = stateTypeValue;
	}
	
}
