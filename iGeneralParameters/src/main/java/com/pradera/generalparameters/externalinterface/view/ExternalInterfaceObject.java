package com.pradera.generalparameters.externalinterface.view;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceObject.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
public class ExternalInterfaceObject {
	
	/** The id. */
	private String id;
	
	/** The description. */
	private String description;
	
	/** The query. */
	private String query;
	
	/** The extension. */
	private String extension;
	
	/** The ind wen service. */
	private String indWenService;
	
	/** The is query. */
	private Boolean isQuery;
	
	/** The interface name. */
	private String interfaceName;
	
	/** The file name. */
	private String fileName;
	
	/** The execution type. */
	private Long executionType;
	
	/** The periocidity. */
	private String periocidity;
	
	/** The extensions file. */
	private List<String> extensionsFile;
	
	/** The user type list. */
	private List<String> userTypeList;
	
	/** The id interface template pk. */
	private Long idInterfaceTemplatePk;
	
	/**
	 * Instantiates a new external interface object.
	 */
	public ExternalInterfaceObject() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Sets the query.
	 *
	 * @param query the new query
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Gets the extension.
	 *
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * Sets the extension.
	 *
	 * @param extension the new extension
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * Gets the ind wen service.
	 *
	 * @return the ind wen service
	 */
	public String getIndWenService() {
		return indWenService;
	}

	/**
	 * Sets the ind wen service.
	 *
	 * @param indWenService the new ind wen service
	 */
	public void setIndWenService(String indWenService) {
		this.indWenService = indWenService;
	}

	/**
	 * Gets the is query.
	 *
	 * @return the is query
	 */
	public Boolean getIsQuery() {
		return isQuery;
	}

	/**
	 * Sets the is query.
	 *
	 * @param isQuery the new is query
	 */
	public void setIsQuery(Boolean isQuery) {
		this.isQuery = isQuery;
	}

	/**
	 * Gets the interface name.
	 *
	 * @return the interface name
	 */
	public String getInterfaceName() {
		return interfaceName;
	}

	/**
	 * Sets the interface name.
	 *
	 * @param interfaceName the new interface name
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the execution type.
	 *
	 * @return the execution type
	 */
	public Long getExecutionType() {
		return executionType;
	}

	/**
	 * Sets the execution type.
	 *
	 * @param executionType the new execution type
	 */
	public void setExecutionType(Long executionType) {
		this.executionType = executionType;
	}

	/**
	 * Gets the periocidity.
	 *
	 * @return the periocidity
	 */
	public String getPeriocidity() {
		return periocidity;
	}

	/**
	 * Sets the periocidity.
	 *
	 * @param periocidity the new periocidity
	 */
	public void setPeriocidity(String periocidity) {
		this.periocidity = periocidity;
	}

	/**
	 * Gets the extensions file.
	 *
	 * @return the extensions file
	 */
	public List<String> getExtensionsFile() {
		return extensionsFile;
	}

	/**
	 * Sets the extensions file.
	 *
	 * @param extensionsFile the new extensions file
	 */
	public void setExtensionsFile(List<String> extensionsFile) {
		this.extensionsFile = extensionsFile;
	}

	/**
	 * Gets the user type list.
	 *
	 * @return the user type list
	 */
	public List<String> getUserTypeList() {
		return userTypeList;
	}

	/**
	 * Sets the user type list.
	 *
	 * @param userTypeList the new user type list
	 */
	public void setUserTypeList(List<String> userTypeList) {
		this.userTypeList = userTypeList;
	}

	/**
	 * Gets the id interface template pk.
	 *
	 * @return the id interface template pk
	 */
	public Long getIdInterfaceTemplatePk() {
		return idInterfaceTemplatePk;
	}

	/**
	 * Sets the id interface template pk.
	 *
	 * @param idInterfaceTemplatePk the new id interface template pk
	 */
	public void setIdInterfaceTemplatePk(Long idInterfaceTemplatePk) {
		this.idInterfaceTemplatePk = idInterfaceTemplatePk;
	}
}