package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.generalparameters.externalinterface.facade.InterfaceSendingFacade;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceUserType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceSendType;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceSendMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18-dic-2013
 */
@DepositaryWebBean
public class InterfaceSendMgmt extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The interface sending facade. */
	@EJB
	InterfaceSendingFacade interfaceSendingFacade;
	
	/** The interface send search. */
	private InterfaceSendTO interfaceSendSearch;
	
	/** The interface send register. */
	private InterfaceSendTO interfaceSendRegister;
	
	/** The external interface list. */
	private List<ExternalInterface> externalInterfaceList;
	
	/** The institution type list. */
	private List<ExternalInterfaceUserType> institutionTypeList;
	
	private List<ParameterTable> listStateCFiFsin;
	
	/** The execution type parameter list. */
	private List<ParameterTable> sendTypeParameterList;
	
	/** The interface send search list. */
	private  GenericDataModel<InterfaceSendTO> interfaceSendSearchList;
	
	/** The interface send session. */
	private ExternalInterfaceSend interfaceSendSession ;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The institution afp list. */
	private List<Participant> institutionAFPList;
	
	/** The institution participant list. */
	private List<Participant> institutionParticipantList;
	
	/** The is institution issuer. */
	private Boolean isInstitutionIssuer;
	
	/** The interface send user. */
	private List<InterfaceSendUserTO> interfaceSendUser;
	
	/** The interface send selected. */
	private InterfaceSendUserTO interfaceSendSelected;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		loadUserSessionPrivileges();	
		loadExternalInterfaceList();
		loadUserTypeList();
		loadParametersTable();
		setParticipantList(new ArrayList<Participant>());	
		setInterfaceSendSearch(new InterfaceSendTO());
		listStateCFiFsin = new ArrayList<ParameterTable>();
		listStateCFiFsin = interfaceSendingFacade.getListStateSecurity();
	}

	/**
	 * Load user session privileges.
	 */
	private void loadUserSessionPrivileges(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load external interface list.
	 */
	private void loadExternalInterfaceList() {
		try {
			externalInterfaceList =  interfaceSendingFacade.getExternalInterfaceList();	
		} catch (ServiceException e) {
		}
	}
	
	/**
	 * Load user type list.
	 */
	private void loadUserTypeList(){
		institutionTypeList = new ArrayList<ExternalInterfaceUserType>();
		for(InstitutionType institutionType: InstitutionType.list){			
			ExternalInterfaceUserType userType = new ExternalInterfaceUserType();			
			userType.setIdUserTypeFk(institutionType.getCode());
			userType.setUserTypeDescription(institutionType.getValue());		
			institutionTypeList.add(userType);			
		}
	}
		
	/**
	 * Load parameters table.
	 */
	private void loadParametersTable() {
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		parameterTableTO.setState(1);
		try {
			parameterTableTO.setMasterTableFk(MasterTableType.EXTERNAL_INTERFACE_SEND.getCode());
			sendTypeParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);			
		} catch (ServiceException e) {
		}
	}
	
	/**
	 * Search interface send.
	 */
	public void searchInterfaceSend(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		
		List<InterfaceSendTO> lista = null;
		try {
			lista =  interfaceSendingFacade.getInterfaceSendList(interfaceSendSearch);		
			for(InterfaceSendTO to : lista){
				to.setSendTypeValue(InterfaceSendType.get(to.getSendType()).getValue());
				to.setInstitutionTypeValue(InstitutionType.get(to.getInstitutionType()).getValue());
			}
		} catch (ServiceException e) {
		}
		if(lista==null){
			interfaceSendSearchList = new GenericDataModel<InterfaceSendTO>();
		}else{
			interfaceSendSearchList = new GenericDataModel<InterfaceSendTO>(lista);
		}
	}
	
	/**
	 * Reset search interface send.
	 */
	public void resetSearchInterfaceSend(){
		
	}
	
	
	/**
	 * Validate interface user type.
	 */
	public void validateInterfaceUserType(){
		List<ExternalInterface> interfaces = externalInterfaceList;
		Long interfacePk = interfaceSendRegister.getInterfaceId();
		if(interfacePk.equals(1021L))
			interfaceSendRegister.setRenderedState(true);
		else
			interfaceSendRegister.setRenderedState(false);
		for(ExternalInterface interfac: interfaces){
			if(interfac.getIdExtInterfacePk().equals(interfacePk)){
				for(ExternalInterfaceUserType interfaceUser: interfac.getExternalInterfaceUserTypes()){
					interfaceSendRegister.setInstitutionType(interfaceUser.getIdUserTypeFk());return;
				}
			}
		}
	}
	
	/**
	 * Change institution type.
	 */
	public void changeInstitutionType(){
		if(interfaceSendSearch.getInstitutionType() != null){
			isInstitutionIssuer = Boolean.FALSE;
			setParticipantList(new ArrayList<Participant>());
			try {				
				if(interfaceSendSearch.getInstitutionType().equals(InstitutionType.AFP.getCode())){
					if(institutionAFPList == null){
						institutionAFPList = interfaceSendingFacade.getAFPList();
					}
					setParticipantList(institutionAFPList);
				}else if(interfaceSendSearch.getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
					isInstitutionIssuer = Boolean.TRUE;
				}else if(interfaceSendSearch.getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode())){
					if(institutionParticipantList == null){
						institutionParticipantList = interfaceSendingFacade.getParticipantList();
					}
					setParticipantList(institutionParticipantList);
				}
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}
	}
	
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/	

	/**
	 * Gets the interface send search.
	 *
	 * @return the interface send search
	 */
	public InterfaceSendTO getInterfaceSendSearch() {
		return interfaceSendSearch;
	}

	/**
	 * Sets the interface send search.
	 *
	 * @param interfaceSendSearch the new interface send search
	 */
	public void setInterfaceSendSearch(InterfaceSendTO interfaceSendSearch) {
		this.interfaceSendSearch = interfaceSendSearch;
	}

	/**
	 * Gets the institution type list.
	 *
	 * @return the institution type list
	 */
	public List<ExternalInterfaceUserType> getInstitutionTypeList() {
		return institutionTypeList;
	}

	/**
	 * Sets the institution type list.
	 *
	 * @param institutionTypeList the new institution type list
	 */
	public void setInstitutionTypeList(
			List<ExternalInterfaceUserType> institutionTypeList) {
		this.institutionTypeList = institutionTypeList;
	}


	/**
	 * Gets the send type parameter list.
	 *
	 * @return the send type parameter list
	 */
	public List<ParameterTable> getSendTypeParameterList() {
		return sendTypeParameterList;
	}

	/**
	 * Sets the send type parameter list.
	 *
	 * @param sendTypeParameterList the new send type parameter list
	 */
	public void setSendTypeParameterList(List<ParameterTable> sendTypeParameterList) {
		this.sendTypeParameterList = sendTypeParameterList;
	}

	/**
	 * Gets the interface send search list.
	 *
	 * @return the interface send search list
	 */
	public GenericDataModel<InterfaceSendTO> getInterfaceSendSearchList() {
		return interfaceSendSearchList;
	}

	/**
	 * Sets the interface send search list.
	 *
	 * @param interfaceSendSearchList the new interface send search list
	 */
	public void setInterfaceSendSearchList(
			GenericDataModel<InterfaceSendTO> interfaceSendSearchList) {
		this.interfaceSendSearchList = interfaceSendSearchList;
	}

	/**
	 * Gets the interface send session.
	 *
	 * @return the interface send session
	 */
	public ExternalInterfaceSend getInterfaceSendSession() {
		return interfaceSendSession;
	}

	/**
	 * Sets the interface send session.
	 *
	 * @param interfaceSendSession the new interface send session
	 */
	public void setInterfaceSendSession(ExternalInterfaceSend interfaceSendSession) {
		this.interfaceSendSession = interfaceSendSession;
	}
	
	/**
	 * Gets the interface send user.
	 *
	 * @return the interface send user
	 */
	public List<InterfaceSendUserTO> getInterfaceSendUser() {
		return interfaceSendUser;
	}

	/**
	 * Sets the interface send user.
	 *
	 * @param interfaceSendUser the new interface send user
	 */
	public void setInterfaceSendUser(List<InterfaceSendUserTO> interfaceSendUser) {
		this.interfaceSendUser = interfaceSendUser;
	}

	/**
	 * Gets the interface send selected.
	 *
	 * @return the interface send selected
	 */
	public InterfaceSendUserTO getInterfaceSendSelected() {
		return interfaceSendSelected;
	}

	/**
	 * Sets the interface send selected.
	 *
	 * @param interfaceSendSelected the new interface send selected
	 */
	public void setInterfaceSendSelected(InterfaceSendUserTO interfaceSendSelected) {
		this.interfaceSendSelected = interfaceSendSelected;
	}

	/**
	 * Load interface send session.
	 *
	 * @param interfaceSendId the interface send id
	 */
	public void loadInterfaceSendSession(Long interfaceSendId){
		interfaceSendSession = new ExternalInterfaceSend();
		try {
			interfaceSendSession = interfaceSendingFacade.getInterfaceSendFacade(interfaceSendId);
			interfaceSendUser = interfaceSendingFacade.getExtInterfaceSendUserListFacade(interfaceSendId);
			interfaceSendSelected = null;
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load files with users.
	 *
	 * @param interfaceUser the interface user
	 */
	public void loadFilesWithUsers(InterfaceSendUserTO interfaceUser){
		if((interfaceUser.getStringFiles()!=null && !interfaceUser.getStringFiles().isEmpty()) || interfaceUser.getUsersOfInstitution()!=null){
			interfaceSendSelected = interfaceUser;
		}else{
			try {
				interfaceUser = interfaceSendingFacade.getInterfaceFileAndUsersFacade(interfaceUser);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		interfaceSendSelected = interfaceUser;
	}
	
	/**
	 * Udpdate tracking process.
	 */
	public void udpdateTrackingProcess(){
		try {
			interfaceSendUser = interfaceSendingFacade.getExtInterfaceSendUserListFacade(interfaceSendSession.getIdExtInterfaceSendPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		interfaceSendSelected = null;
	}
	
	/**
	 * *
	 * Method to load new InterfaceTO.
	 */
	public void loadNewInterfaceSendTO(){
		interfaceSendRegister = new InterfaceSendTO();
	}
	
	/**
	 * Gets the interface send type list.
	 *
	 * @return the interface send type list
	 */
	public List<InterfaceSendType> getInterfaceSendTypeList(){
		return InterfaceSendType.list;
	}

	/**
	 * Gets the checks if is institution issuer.
	 *
	 * @return the checks if is institution issuer
	 */
	public Boolean getIsInstitutionIssuer() {
		return isInstitutionIssuer;
	}

	/**
	 * Sets the checks if is institution issuer.
	 *
	 * @param isInstitutionIssuer the new checks if is institution issuer
	 */
	public void setIsInstitutionIssuer(Boolean isInstitutionIssuer) {
		this.isInstitutionIssuer = isInstitutionIssuer;
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}
	
	

	public List<ParameterTable> getListStateCFiFsin() {
		return listStateCFiFsin;
	}

	public void setListStateCFiFsin(List<ParameterTable> listStateCFiFsin) {
		this.listStateCFiFsin = listStateCFiFsin;
	}

	/**
	 * Gets the interface send register.
	 *
	 * @return the interface send register
	 */
	public InterfaceSendTO getInterfaceSendRegister() {
		return interfaceSendRegister;
	}

	/**
	 * Sets the interface send register.
	 *
	 * @param interfaceSendRegister the new interface send register
	 */
	public void setInterfaceSendRegister(InterfaceSendTO interfaceSendRegister) {
		this.interfaceSendRegister = interfaceSendRegister;
	}

	/**
	 * Gets the external interface list.
	 *
	 * @return the external interface list
	 */
	public List<ExternalInterface> getExternalInterfaceList() {
		return externalInterfaceList;
	}

	/**
	 * Sets the external interface list.
	 *
	 * @param externalInterfaceList the new external interface list
	 */
	public void setExternalInterfaceList(List<ExternalInterface> externalInterfaceList) {
		this.externalInterfaceList = externalInterfaceList;
	}
	
	/**
	 * Before send external interface.
	 */
	public void beforeSendExternalInterface(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrSendExternalInterface').show();");
	}
		
	/**
	 * Send external interface.
	 */
	@LoggerAuditWeb
	public void sendExternalInterface(){
		InterfaceSendTO interfaceSend = interfaceSendRegister;
		try {
			interfaceSendingFacade.sendExternalInterface(interfaceSend);
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM_OK);
			JSFUtilities.showSimpleEndTransactionDialog("search");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}