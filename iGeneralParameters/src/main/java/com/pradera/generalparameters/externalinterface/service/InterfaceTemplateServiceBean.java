package com.pradera.generalparameters.externalinterface.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.view.InterfaceTemplateTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceTemplateDetail;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTemplate;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateDetailType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/10/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InterfaceTemplateServiceBean extends CrudDaoServiceBean {
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Registry external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate registryExternalInterfaceTemplate (ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException{		
		return create(externalInterfaceTemplate);
	}

	/**
	 * Gets the external interface template list.
	 *
	 * @param interfaceTemplateSearch the interface template search
	 * @return the external interface template list
	 * @throws ServiceException the service exception
	 */
	public List<InterfaceTemplateTO> getExternalInterfaceTemplateList(InterfaceTemplateTO interfaceTemplateSearch) throws ServiceException {
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		CriteriaQuery<InterfaceTemplateTO> criteriaQuery = criteriaBuilder.createQuery(InterfaceTemplateTO.class);
		Root<ExternalInterfaceTemplate> interfaceTemplateRoot = criteriaQuery.from(ExternalInterfaceTemplate.class);
		criteriaQuery.distinct(Boolean.TRUE);
		criteriaQuery.multiselect(
				interfaceTemplateRoot.get("idExtInferfaceTemplatePk"),
				interfaceTemplateRoot.get("templateName"),
				interfaceTemplateRoot.get("templateDescription"),
				interfaceTemplateRoot.get("registerDate"),
				interfaceTemplateRoot.get("templateState")
		);
		
		if(interfaceTemplateSearch.getStateType() != null && interfaceTemplateSearch.getStateType() > 0){
			criteriaParameters.add(criteriaBuilder.equal(interfaceTemplateRoot.get("templateState"), interfaceTemplateSearch.getStateType()));
		}
		
		if(interfaceTemplateSearch.getId() != null && interfaceTemplateSearch.getId() > 0){
			criteriaParameters.add(criteriaBuilder.equal(interfaceTemplateRoot.get("idExtInferfaceTemplatePk"), interfaceTemplateSearch.getId()));
		}
		
		if(interfaceTemplateSearch.getName() != null && !interfaceTemplateSearch.getName().isEmpty()){
			criteriaParameters.add(criteriaBuilder.like(interfaceTemplateRoot.<String>get("templateName"),"%" + interfaceTemplateSearch.getName() + "%"));
		}
		
		if(interfaceTemplateSearch.getDescription() != null && !interfaceTemplateSearch.getDescription().isEmpty()){
			criteriaParameters.add(criteriaBuilder.like(interfaceTemplateRoot.<String>get("templateDescription"), "%" + interfaceTemplateSearch.getDescription() + "%"));
		}
		
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		criteriaQuery.orderBy(criteriaBuilder.desc(interfaceTemplateRoot.get("idExtInferfaceTemplatePk")));
		TypedQuery<InterfaceTemplateTO> interfaceTemplateCriteria = em.createQuery(criteriaQuery);
		
		List<InterfaceTemplateTO> result = null;
		try{
			result = interfaceTemplateCriteria.getResultList();
		}catch(NoResultException ex){
			return null;
		}
		return result;
	}

	/**
	 * Gets the external interface template.
	 *
	 * @param id the id
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate getExternalInterfaceTemplate(Long id) throws ServiceException {			
		ExternalInterfaceTemplate eit = find(ExternalInterfaceTemplate.class, id);
		if(eit != null){
			for(ExtInterfaceTemplateDetail eitd : eit.getExtInterfaceTemplateDetails()){			
				eitd.setDescription(InterfaceTemplateDetailType.lookup.get(eitd.getTemplateDetailType()).getValue());
			}
		}		
		return eit;
	}

	/**
	 * Gets the external interface template.
	 *
	 * @param id the id
	 * @param state the state
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate getExternalInterfaceTemplate(Long id, InterfaceTemplateStateType state) throws ServiceException {		
		ExternalInterfaceTemplate eit = getExternalInterfaceTemplate(id);
		if(eit != null){
			switch (state) {
			case REGISTERED:			
			case ANNULLED:
				if(!eit.getTemplateState().equals(InterfaceTemplateStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);					
				}
				break;
			case BLOCKED:
				if(!eit.getTemplateState().equals(InterfaceTemplateStateType.CONFIRMED.getCode())){
					throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);					
				}
				break;
			case CONFIRMED:
				if(!eit.getTemplateState().equals(InterfaceTemplateStateType.REGISTERED.getCode()) && !eit.getTemplateState().equals(InterfaceTemplateStateType.BLOCKED.getCode())){
					throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);	
				}
				break;
			}	
		}				
		return eit;
	}
	
	/**
	 * Confirm external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate confirmExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterfaceTemplate eit = getExternalInterfaceTemplate(externalInterfaceTemplate.getIdExtInferfaceTemplatePk());
		if(eit.getTemplateState().equals(InterfaceTemplateStateType.REGISTERED.getCode())){
			eit.setTemplateState(InterfaceTemplateStateType.CONFIRMED.getCode());
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);
		}		
		update(eit);
		return eit;
	}

	/**
	 * Annular external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate annularExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterfaceTemplate eit = getExternalInterfaceTemplate(externalInterfaceTemplate.getIdExtInferfaceTemplatePk());
		if(eit.getTemplateState().equals(InterfaceTemplateStateType.REGISTERED.getCode())){
			eit.setTemplateState(InterfaceTemplateStateType.ANNULLED.getCode());				
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);
		}		
		update(eit);
		return eit;
	}

	/**
	 * Block external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate blockExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterfaceTemplate eit = getExternalInterfaceTemplate(externalInterfaceTemplate.getIdExtInferfaceTemplatePk());
		if(eit.getTemplateState().equals(InterfaceTemplateStateType.CONFIRMED.getCode())){
			eit.setTemplateState(InterfaceTemplateStateType.BLOCKED.getCode());
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);
		}	
		update(eit);
		return eit;
	}

	/**
	 * Modify external interface template.
	 *
	 * @param interfaceTemplateParam the interface template param
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate modifyExternalInterfaceTemplate(ExternalInterfaceTemplate interfaceTemplateParam) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());		
		ExternalInterfaceTemplate interfaceTemplate = getExternalInterfaceTemplate(interfaceTemplateParam.getIdExtInferfaceTemplatePk());
		if(interfaceTemplate.getTemplateState().equals(InterfaceTemplateStateType.REGISTERED.getCode())){
			if(!interfaceTemplate.getTemplateName().equals(interfaceTemplateParam.getTemplateName())){
				interfaceTemplate.setTemplateName(interfaceTemplateParam.getTemplateName());
			}
			if(!interfaceTemplate.getTemplateDescription().equals(interfaceTemplateParam.getTemplateDescription())){
				interfaceTemplate.setTemplateDescription(interfaceTemplateParam.getTemplateDescription());
			}
			interfaceTemplate.getExtInterfaceTemplateDetails().removeAll(interfaceTemplate.getExtInterfaceTemplateDetails());
			interfaceTemplate.getExtInterfaceTemplateDetails().addAll(interfaceTemplateParam.getExtInterfaceTemplateDetails());
			
			update(interfaceTemplate);
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);
		}
		return interfaceTemplate;
	}

	/**
	 * Unblock external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 * @return the external interface template
	 * @throws ServiceException the service exception
	 */
	public ExternalInterfaceTemplate unblockExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate)  throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		ExternalInterfaceTemplate eit = getExternalInterfaceTemplate(externalInterfaceTemplate.getIdExtInferfaceTemplatePk());
		if(eit.getTemplateState().equals(InterfaceTemplateStateType.BLOCKED.getCode())){
			eit.setTemplateState(InterfaceTemplateStateType.CONFIRMED.getCode());
		}else{
			throw new ServiceException(ErrorServiceType.EXTERNAL_INTERFACE_TEMPLATE_INCORRECT_STATE);
		}	
		update(eit);
		return eit;
	}

}
