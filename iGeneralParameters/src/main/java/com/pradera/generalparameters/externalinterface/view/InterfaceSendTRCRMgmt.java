package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.generalparameters.externalinterface.facade.InterfaceSendingFacade;
import com.pradera.generalparameters.externalinterface.to.InfoInstitutionFromUserTO;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceUserType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceSendType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceSendMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18-dic-2013
 */
@DepositaryWebBean
public class InterfaceSendTRCRMgmt extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The interface sending facade. */
	@EJB
	InterfaceSendingFacade interfaceSendingFacade;
	

	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	

	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;
	
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The logged issuer. */
	private Issuer loggedIssuer;
	
	/** The issuer helper mgmt. */
	private Issuer issuerHelperMgmt;	
	
	/** The issuer helper search. */
	private Issuer issuerHelperSearch;
	
	/** The issuance helper search. */
	private Issuance issuanceHelperSearch;
	
	/** The security t osearch. */
	private SecurityTO securityTOsearch;
	
	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;
	
	/** The lst cbo security state. */
	private List<ParameterTable> lstCboSecurityState;
	
	/** The lst cbo filter securitie class. */
	private List<ParameterTable> lstCboFilterSecuritieClass;
	
	/** The security data model. */
	private GenericDataModel<Security> securityDataModel;	
	
	/** The search filters concat. */
	private String searchFiltersConcat;
	
	/** The security. */
	private Security security;
	
/** The list security selected. */
//	private List<Security> listSecuritySelected;
	private Security[] listSecuritySelected;
	
	/** The issuance helper mgmt. */
	private Issuance issuanceHelperMgmt;	
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The Boolean Date Transfer */
	private boolean boolDateTransfer;
	
	/** The interface send search. */
	private InterfaceSendTO interfaceSendSearch;
	
	/** The interface send register. */
	private InterfaceSendTO interfaceSendRegister;
	
	/** The external interface list. */
	private List<ExternalInterface> externalInterfaceList;
	
	/** The institution type list. */
	private List<ExternalInterfaceUserType> institutionTypeList;
	
	/** The execution type parameter list. */
	private List<ParameterTable> sendTypeParameterList;
	
	/** The interface send search list. */
	private  GenericDataModel<InterfaceSendTO> interfaceSendSearchList;
	
	/** The interface send session. */
	private ExternalInterfaceSend interfaceSendSession ;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The institution afp list. */
	private List<Participant> institutionAFPList;
	
	/** The institution participant list. */
	private List<Participant> institutionParticipantList;
	
	/** The is institution issuer. */
	private Boolean isInstitutionIssuer;
	
	/** The interface send user. */
	private List<InterfaceSendUserTO> interfaceSendUser;
	
	/** The interface send selected. */
	private InterfaceSendUserTO interfaceSendSelected;
	
	/** The initial state search form. */
	String initialStateSearchForm;
	
	/** The Date transfer*/
	private Date dateTransfer;
	
	/** The Date Validations Maximum */
	private Date validateMaxDate;
	// lista que llena el combobox notTraded
	private List<SelectItem> lstCboFilterNotTraded;
	// mostrar o no mostrara opciones notTraded
	private boolean showNotTraded;
	
	/**
	 * Inits the Constructor.
	 */
	@PostConstruct
	public void init(){
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			// por defecto no muestra las opciones NotTraded
			showNotTraded = false;
			
			security=new Security(); 
			
			securityTOsearch=new SecurityTO();
			
			lstCboFilterNotTraded = new ArrayList<SelectItem>();
			lstCboFilterNotTraded.add(new SelectItem(0, "SI"));
			lstCboFilterNotTraded.add(new SelectItem(1, "NO"));

			security.setIssuer( new Issuer() );
			security.setInterestPaymentSchedule(new InterestPaymentSchedule());
			security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
			initialStateSearchForm=securityTOsearch.toString();
			
			issuerHelperSearch=new Issuer();
			issuerHelperMgmt=new Issuer();
			issuanceHelperSearch=new Issuance();
			issuanceHelperMgmt=new Issuance();
			
			validateMaxDate=getCurrentSystemDate();
			
			allHolidays=generalParametersFacade.getAllHolidaysByCountry(countryResidence) ;
			
			
			loadCboInstrumentType();
			loadCboSecuritiesState();
			loadCboFilterSecuritieClass();
			loadCboFilterCurency();
			loadCboSecuritieClassTarget();
			
			if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) && 
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
				loggedIssuer=new Issuer();
				loggedIssuer=interfaceSendingFacade.findIssuerByFiltersServiceFacade(issuerFilter);				
				issuerHelperSearch=loggedIssuer;
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
					List<Integer>lstSecurityClass = new ArrayList<Integer>();
					for(ParameterTable objParameterTable : lstCboFilterSecuritieClass){
						if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
								objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
							lstSecuritieClass.add(objParameterTable);
							lstSecurityClass.add(objParameterTable.getParameterTablePk());
						}
					}
					lstCboFilterSecuritieClass = lstSecuritieClass;
					securityTOsearch.setLstSecurityClass(lstSecurityClass);
				}
			}
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			searchFiltersConcat=buildCiteriaSearchConcat();
			securityTOsearch.setSecurityState(SecurityStateType.REGISTERED.getCode().toString());
//			this.listSecuritySelected=new ArrayList<Security>();
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Load cbo instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboInstrumentType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
//		lstCboInstrumentType=generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/**
	 * Load cbo filter securitie class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterSecuritieClass() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		lstCboFilterSecuritieClass=generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboFilterSecuritieClass,true);
	}
	
	/**
	 * Load cbo securitie class target.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecuritieClassTarget() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		
		parameterTableTO.setLstParameterTablePk( new ArrayList<Integer>() );
		parameterTableTO.getLstParameterTablePk().add( SecurityClassType.ACC.getCode() );
		parameterTableTO.getLstParameterTablePk().add( SecurityClassType.ACC_RF.getCode());
		
//		lstCboSecuritieClassTrg=generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );
	}	
	/**
	 * Load cbo filter curency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterCurency() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency=generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboFilterCurrency, true);
	}
	/**
	 * Load cbo securities state.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecuritiesState() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_STATE.getCode() );
		lstCboSecurityState=generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboSecurityState);
	}

	/**
	 * Load user session privileges.
	 */
	private void loadUserSessionPrivileges(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load external interface list.
	 */
	private void loadExternalInterfaceList() {
		try {
			externalInterfaceList =  interfaceSendingFacade.getExternalInterfaceList();	
		} catch (ServiceException e) {
		}
	}
	
	/**
	 * Load user type list.
	 */
	private void loadUserTypeList(){
		institutionTypeList = new ArrayList<ExternalInterfaceUserType>();
		for(InstitutionType institutionType: InstitutionType.list){			
			ExternalInterfaceUserType userType = new ExternalInterfaceUserType();			
			userType.setIdUserTypeFk(institutionType.getCode());
			userType.setUserTypeDescription(institutionType.getValue());		
			institutionTypeList.add(userType);			
		}
	}
		
	/**
	 * Load parameters table.
	 */
	private void loadParametersTable() {
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		parameterTableTO.setState(1);
		try {
			parameterTableTO.setMasterTableFk(MasterTableType.EXTERNAL_INTERFACE_SEND.getCode());
			sendTypeParameterList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);			
		} catch (ServiceException e) {
		}
	}
	
	/**
	 * Search interface send.
	 */
	public void searchInterfaceSend(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		
		List<InterfaceSendTO> lista = null;
		try {
			lista =  interfaceSendingFacade.getInterfaceSendList(interfaceSendSearch);		
			for(InterfaceSendTO to : lista){
				to.setSendTypeValue(InterfaceSendType.get(to.getSendType()).getValue());
				to.setInstitutionTypeValue(InstitutionType.get(to.getInstitutionType()).getValue());
			}
		} catch (ServiceException e) {
		}
		if(lista==null){
			interfaceSendSearchList = new GenericDataModel<InterfaceSendTO>();
		}else{
			interfaceSendSearchList = new GenericDataModel<InterfaceSendTO>(lista);
		}
	}
	
	/**
	 * Reset search interface send.
	 */
	public void resetSearchInterfaceSend(){
		
	}
	
	
	/**
	 * Validate interface user type.
	 */
	public void validateInterfaceUserType(){
		List<ExternalInterface> interfaces = externalInterfaceList;
		Long interfacePk = interfaceSendRegister.getInterfaceId();
		
		for(ExternalInterface interfac: interfaces){
			if(interfac.getIdExtInterfacePk().equals(interfacePk)){
				for(ExternalInterfaceUserType interfaceUser: interfac.getExternalInterfaceUserTypes()){
					interfaceSendRegister.setInstitutionType(interfaceUser.getIdUserTypeFk());return;
				}
			}
		}
	}
	
	/**
	 * Change institution type.
	 */
	public void changeInstitutionType(){
		if(interfaceSendSearch.getInstitutionType() != null){
			isInstitutionIssuer = Boolean.FALSE;
			setParticipantList(new ArrayList<Participant>());
			try {				
				if(interfaceSendSearch.getInstitutionType().equals(InstitutionType.AFP.getCode())){
					if(institutionAFPList == null){
						institutionAFPList = interfaceSendingFacade.getAFPList();
					}
					setParticipantList(institutionAFPList);
				}else if(interfaceSendSearch.getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
					isInstitutionIssuer = Boolean.TRUE;
				}else if(interfaceSendSearch.getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode())){
					if(institutionParticipantList == null){
						institutionParticipantList = interfaceSendingFacade.getParticipantList();
					}
					setParticipantList(institutionParticipantList);
				}
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}
	}
	
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/	

	/**
	 * Gets the interface send search.
	 *
	 * @return the interface send search
	 */
	public InterfaceSendTO getInterfaceSendSearch() {
		return interfaceSendSearch;
	}

	/**
	 * Sets the interface send search.
	 *
	 * @param interfaceSendSearch the new interface send search
	 */
	public void setInterfaceSendSearch(InterfaceSendTO interfaceSendSearch) {
		this.interfaceSendSearch = interfaceSendSearch;
	}

	/**
	 * Gets the institution type list.
	 *
	 * @return the institution type list
	 */
	public List<ExternalInterfaceUserType> getInstitutionTypeList() {
		return institutionTypeList;
	}

	/**
	 * Sets the institution type list.
	 *
	 * @param institutionTypeList the new institution type list
	 */
	public void setInstitutionTypeList(
			List<ExternalInterfaceUserType> institutionTypeList) {
		this.institutionTypeList = institutionTypeList;
	}


	/**
	 * Gets the send type parameter list.
	 *
	 * @return the send type parameter list
	 */
	public List<ParameterTable> getSendTypeParameterList() {
		return sendTypeParameterList;
	}

	/**
	 * Sets the send type parameter list.
	 *
	 * @param sendTypeParameterList the new send type parameter list
	 */
	public void setSendTypeParameterList(List<ParameterTable> sendTypeParameterList) {
		this.sendTypeParameterList = sendTypeParameterList;
	}

	/**
	 * Gets the interface send search list.
	 *
	 * @return the interface send search list
	 */
	public GenericDataModel<InterfaceSendTO> getInterfaceSendSearchList() {
		return interfaceSendSearchList;
	}

	/**
	 * Sets the interface send search list.
	 *
	 * @param interfaceSendSearchList the new interface send search list
	 */
	public void setInterfaceSendSearchList(
			GenericDataModel<InterfaceSendTO> interfaceSendSearchList) {
		this.interfaceSendSearchList = interfaceSendSearchList;
	}

	/**
	 * Gets the interface send session.
	 *
	 * @return the interface send session
	 */
	public ExternalInterfaceSend getInterfaceSendSession() {
		return interfaceSendSession;
	}

	/**
	 * Sets the interface send session.
	 *
	 * @param interfaceSendSession the new interface send session
	 */
	public void setInterfaceSendSession(ExternalInterfaceSend interfaceSendSession) {
		this.interfaceSendSession = interfaceSendSession;
	}
	
	/**
	 * Gets the interface send user.
	 *
	 * @return the interface send user
	 */
	public List<InterfaceSendUserTO> getInterfaceSendUser() {
		return interfaceSendUser;
	}

	/**
	 * Sets the interface send user.
	 *
	 * @param interfaceSendUser the new interface send user
	 */
	public void setInterfaceSendUser(List<InterfaceSendUserTO> interfaceSendUser) {
		this.interfaceSendUser = interfaceSendUser;
	}

	/**
	 * Gets the interface send selected.
	 *
	 * @return the interface send selected
	 */
	public InterfaceSendUserTO getInterfaceSendSelected() {
		return interfaceSendSelected;
	}

	/**
	 * Sets the interface send selected.
	 *
	 * @param interfaceSendSelected the new interface send selected
	 */
	public void setInterfaceSendSelected(InterfaceSendUserTO interfaceSendSelected) {
		this.interfaceSendSelected = interfaceSendSelected;
	}

	/**
	 * Load interface send session.
	 *
	 * @param interfaceSendId the interface send id
	 */
	public void loadInterfaceSendSession(Long interfaceSendId){
		interfaceSendSession = new ExternalInterfaceSend();
		try {
			interfaceSendSession = interfaceSendingFacade.getInterfaceSendFacade(interfaceSendId);
			interfaceSendUser = interfaceSendingFacade.getExtInterfaceSendUserListFacade(interfaceSendId);
			interfaceSendSelected = null;
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load files with users.
	 *
	 * @param interfaceUser the interface user
	 */
	public void loadFilesWithUsers(InterfaceSendUserTO interfaceUser){
		if((interfaceUser.getStringFiles()!=null && !interfaceUser.getStringFiles().isEmpty()) || interfaceUser.getUsersOfInstitution()!=null){
			interfaceSendSelected = interfaceUser;
		}else{
			try {
				interfaceUser = interfaceSendingFacade.getInterfaceFileAndUsersFacade(interfaceUser);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		interfaceSendSelected = interfaceUser;
	}
	
	/**
	 * Udpdate tracking process.
	 */
	public void udpdateTrackingProcess(){
		try {
			interfaceSendUser = interfaceSendingFacade.getExtInterfaceSendUserListFacade(interfaceSendSession.getIdExtInterfaceSendPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		interfaceSendSelected = null;
	}
	
	/**
	 * *
	 * Method to load new InterfaceTO.
	 */
	public void loadNewInterfaceSendTO(){
		interfaceSendRegister = new InterfaceSendTO();
	}
	
	/**
	 * Gets the interface send type list.
	 *
	 * @return the interface send type list
	 */
	public List<InterfaceSendType> getInterfaceSendTypeList(){
		return InterfaceSendType.list;
	}

	/**
	 * Gets the checks if is institution issuer.
	 *
	 * @return the checks if is institution issuer
	 */
	public Boolean getIsInstitutionIssuer() {
		return isInstitutionIssuer;
	}

	/**
	 * Sets the checks if is institution issuer.
	 *
	 * @param isInstitutionIssuer the new checks if is institution issuer
	 */
	public void setIsInstitutionIssuer(Boolean isInstitutionIssuer) {
		this.isInstitutionIssuer = isInstitutionIssuer;
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the interface send register.
	 *
	 * @return the interface send register
	 */
	public InterfaceSendTO getInterfaceSendRegister() {
		return interfaceSendRegister;
	}

	/**
	 * Sets the interface send register.
	 *
	 * @param interfaceSendRegister the new interface send register
	 */
	public void setInterfaceSendRegister(InterfaceSendTO interfaceSendRegister) {
		this.interfaceSendRegister = interfaceSendRegister;
	}

	/**
	 * Gets the external interface list.
	 *
	 * @return the external interface list
	 */
	public List<ExternalInterface> getExternalInterfaceList() {
		return externalInterfaceList;
	}

	
	/**
	 * Gets the user info.
	 *
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the country residence.
	 *
	 * @return the countryResidence
	 */
	public Integer getCountryResidence() {
		return countryResidence;
	}

	/**
	 * Sets the country residence.
	 *
	 * @param countryResidence the countryResidence to set
	 */
	public void setCountryResidence(Integer countryResidence) {
		this.countryResidence = countryResidence;
	}

	/**
	 * Gets the logged issuer.
	 *
	 * @return the loggedIssuer
	 */
	public Issuer getLoggedIssuer() {
		return loggedIssuer;
	}

	/**
	 * Sets the logged issuer.
	 *
	 * @param loggedIssuer the loggedIssuer to set
	 */
	public void setLoggedIssuer(Issuer loggedIssuer) {
		this.loggedIssuer = loggedIssuer;
	}

	/**
	 * Gets the issuer helper mgmt.
	 *
	 * @return the issuerHelperMgmt
	 */
	public Issuer getIssuerHelperMgmt() {
		return issuerHelperMgmt;
	}

	/**
	 * Sets the issuer helper mgmt.
	 *
	 * @param issuerHelperMgmt the issuerHelperMgmt to set
	 */
	public void setIssuerHelperMgmt(Issuer issuerHelperMgmt) {
		this.issuerHelperMgmt = issuerHelperMgmt;
	}

	/**
	 * Gets the issuer helper search.
	 *
	 * @return the issuerHelperSearch
	 */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}

	/**
	 * Sets the issuer helper search.
	 *
	 * @param issuerHelperSearch the issuerHelperSearch to set
	 */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}

	/**
	 * Gets the issuance helper search.
	 *
	 * @return the issuanceHelperSearch
	 */
	public Issuance getIssuanceHelperSearch() {
		return issuanceHelperSearch;
	}

	/**
	 * Sets the issuance helper search.
	 *
	 * @param issuanceHelperSearch the issuanceHelperSearch to set
	 */
	public void setIssuanceHelperSearch(Issuance issuanceHelperSearch) {
		this.issuanceHelperSearch = issuanceHelperSearch;
	}

	/**
	 * Gets the security t osearch.
	 *
	 * @return the securityTOsearch
	 */
	public SecurityTO getSecurityTOsearch() {
		return securityTOsearch;
	}

	/**
	 * Sets the security t osearch.
	 *
	 * @param securityTOsearch the securityTOsearch to set
	 */
	public void setSecurityTOsearch(SecurityTO securityTOsearch) {
		this.securityTOsearch = securityTOsearch;
	}

	/**
	 * Gets the lst cbo filter currency.
	 *
	 * @return the lstCboFilterCurrency
	 */
	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}

	/**
	 * Sets the lst cbo filter currency.
	 *
	 * @param lstCboFilterCurrency the lstCboFilterCurrency to set
	 */
	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}

	/**
	 * Gets the lst cbo security state.
	 *
	 * @return the lstCboSecurityState
	 */
	public List<ParameterTable> getLstCboSecurityState() {
		return lstCboSecurityState;
	}

	/**
	 * Sets the lst cbo security state.
	 *
	 * @param lstCboSecurityState the lstCboSecurityState to set
	 */
	public void setLstCboSecurityState(List<ParameterTable> lstCboSecurityState) {
		this.lstCboSecurityState = lstCboSecurityState;
	}

	/**
	 * Gets the lst cbo filter securitie class.
	 *
	 * @return the lstCboFilterSecuritieClass
	 */
	public List<ParameterTable> getLstCboFilterSecuritieClass() {
		return lstCboFilterSecuritieClass;
	}

	/**
	 * Sets the lst cbo filter securitie class.
	 *
	 * @param lstCboFilterSecuritieClass the lstCboFilterSecuritieClass to set
	 */
	public void setLstCboFilterSecuritieClass(
			List<ParameterTable> lstCboFilterSecuritieClass) {
		this.lstCboFilterSecuritieClass = lstCboFilterSecuritieClass;
	}

	/**
	 * Gets the security data model.
	 *
	 * @return the securityDataModel
	 */
	public GenericDataModel<Security> getSecurityDataModel() {
		return securityDataModel;
	}

	/**
	 * Sets the security data model.
	 *
	 * @param securityDataModel the securityDataModel to set
	 */
	public void setSecurityDataModel(GenericDataModel<Security> securityDataModel) {
		this.securityDataModel = securityDataModel;
	}

	/**
	 * Gets the search filters concat.
	 *
	 * @return the searchFiltersConcat
	 */
	public String getSearchFiltersConcat() {
		return searchFiltersConcat;
	}

	/**
	 * Sets the search filters concat.
	 *
	 * @param searchFiltersConcat the searchFiltersConcat to set
	 */
	public void setSearchFiltersConcat(String searchFiltersConcat) {
		this.searchFiltersConcat = searchFiltersConcat;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the issuance helper mgmt.
	 *
	 * @return the issuanceHelperMgmt
	 */
	public Issuance getIssuanceHelperMgmt() {
		return issuanceHelperMgmt;
	}

	/**
	 * Sets the issuance helper mgmt.
	 *
	 * @param issuanceHelperMgmt the issuanceHelperMgmt to set
	 */
	public void setIssuanceHelperMgmt(Issuance issuanceHelperMgmt) {
		this.issuanceHelperMgmt = issuanceHelperMgmt;
	}

	/**
	 * Gets the parameter table to.
	 *
	 * @return the parameterTableTO
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * Sets the parameter table to.
	 *
	 * @param parameterTableTO the parameterTableTO to set
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * Gets the institution afp list.
	 *
	 * @return the institutionAFPList
	 */
	public List<Participant> getInstitutionAFPList() {
		return institutionAFPList;
	}

	/**
	 * Sets the institution afp list.
	 *
	 * @param institutionAFPList the institutionAFPList to set
	 */
	public void setInstitutionAFPList(List<Participant> institutionAFPList) {
		this.institutionAFPList = institutionAFPList;
	}

	/**
	 * Gets the institution participant list.
	 *
	 * @return the institutionParticipantList
	 */
	public List<Participant> getInstitutionParticipantList() {
		return institutionParticipantList;
	}

	/**
	 * Sets the institution participant list.
	 *
	 * @param institutionParticipantList the institutionParticipantList to set
	 */
	public void setInstitutionParticipantList(
			List<Participant> institutionParticipantList) {
		this.institutionParticipantList = institutionParticipantList;
	}

	/**
	 * Sets the external interface list.
	 *
	 * @param externalInterfaceList the new external interface list
	 */
	public void setExternalInterfaceList(List<ExternalInterface> externalInterfaceList) {
		this.externalInterfaceList = externalInterfaceList;
	}
	
	
	
//	public void beforeSendExternalInterface(){
//		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM);
//		JSFUtilities.executeJavascriptFunction("wrSendExternalInterface.show()");
//	}
//	
//	/**
//	 * Send external interface.
//	 */
//	@LoggerAuditWeb
//	public void sendExternalInterface(){
//		InterfaceSendTO interfaceSend = interfaceSendRegister;
//		try {
//			interfaceSendingFacade.sendExternalInterface(interfaceSend);
//			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM_OK);
//			JSFUtilities.showSimpleEndTransactionDialog("search");
//		} catch (ServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	
	
	
	

	/**
	 * Builds the citeria search concat.
	 *
	 * @return the string
	 */
	public String buildCiteriaSearchConcat(){
		StringBuilder strbAux=new StringBuilder();
		strbAux.append(issuerHelperSearch.getIdIssuerPk());
		strbAux.append(issuanceHelperSearch.getIdIssuanceCodePk());
		strbAux.append(securityTOsearch.getAlternativeCode());
		strbAux.append(securityTOsearch.getSecurityCurrency());
		strbAux.append(securityTOsearch.getSecurityState());
		strbAux.append(securityTOsearch.getSecurityClass());
		strbAux.append(securityTOsearch.getIssuanceDate());
		strbAux.append(securityTOsearch.getExpirationDate());
		return strbAux.toString();
	}
	
	/**
	 * Change filter field.
	 */
	public void changeFilterField(){
		try {
			showNotTraded = false;
			// solo se mostrara las opciones de no negociabilidad para los 
			// DPF y DPA			
			if(securityTOsearch != null && securityTOsearch.getSecurityClass() != null &&
				(securityTOsearch.getSecurityClass() == 420 || 
					securityTOsearch.getSecurityClass() == 1976)){
				showNotTraded = true;
			} else {
				// si no se trata de DPF o DPA no se debe 
				// filtrar por el campo notTraded
				if(securityTOsearch!= null){
					securityTOsearch.setNotTraded(null);
				}				
			}
			
			String strSearchFiltersAux=buildCiteriaSearchConcat();
			
			if(!strSearchFiltersAux.equals(searchFiltersConcat)){
				securityDataModel=null;
				security=new Security();
			}
			
			searchFiltersConcat=buildCiteriaSearchConcat();
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void changeNotTraded(){
		// limpiando tabla
		securityDataModel = null;
	}
	
	/**
	 * Validate not empty fields.
	 *
	 * @param securityTOsearch the security t osearch
	 * @return true, if successful
	 */
	private boolean validateNotEmptyFields(SecurityTO securityTOsearch) {
		if(Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIssuanceCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIssuerCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdSecurityCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityCurrency())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getAlternativeCode())				
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getDescriptionSecurity())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIssuanceDate())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getExpirationDate())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityClass())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityState())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIsinCodePk())){
			return true;
		}
		return false;
	}
	/**
	 * Search securities handler.
	 *
	 * @param event the event
	 */
	public void searchSecuritiesHandler(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		try {

			securityTOsearch.setIdIssuerCodePk( issuerHelperSearch.getIdIssuerPk() );
			securityTOsearch.setIdIssuanceCodePk( issuanceHelperSearch.getIdIssuanceCodePk() );
			
			if(validateNotEmptyFields(securityTOsearch)){
				List<Security> lstSecurity = null;
				
				/*
				 * Issue 2831
				 * Insercion de un Check que les permita visualizar solo los valores resultado de sus traspasos confirmados
				 * en la Fecha del Calendario
				 * Si el usuario es Participante, Considerar los traspasos del Participante Destino
				 */
				if(boolDateTransfer){
					Long idParticipantPk=null;
					//Si es usuario participante, lo considero como destino.
					if(userInfo.getUserAccountSession().isParticipantInstitucion()){
						idParticipantPk=userInfo.getUserAccountSession().getParticipantCode();
					}
					securityTOsearch.setListSecurityCode(interfaceSendingFacade.findSecuritiesWithTransfer(dateTransfer,idParticipantPk));
				}
				
				if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					Participant objParticipant = interfaceSendingFacade.getParticipantByPk(userInfo.getUserAccountSession().getParticipantCode());					
					securityTOsearch.setIdParticipant(objParticipant.getIdParticipantPk());
					HolderAccountTO holderAccountTO = new HolderAccountTO();
					holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
					List<Long> lstHolderPk = new ArrayList<Long>();
					HolderSearchTO holderSearchTO = new HolderSearchTO();
					holderSearchTO.setDocumentType(objParticipant.getDocumentType());
					holderSearchTO.setDocumentNumber(objParticipant.getDocumentNumber());
					holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
					List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
					if(lstHolder!=null && lstHolder.size()>0){
						for(Holder objHolder : lstHolder){
							lstHolderPk.add(objHolder.getIdHolderPk());
						}
					}
					holderAccountTO.setLstHolderPk(lstHolderPk);
					List<HolderAccount> lstHolderAccount = null;
					if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
						 lstHolderAccount = interfaceSendingFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
					}						
					List<Long> lstHolderAccountPk = new ArrayList<Long>();
					if(lstHolderAccount!=null && lstHolderAccount.size()>0){
						for(HolderAccount objHolderAccount : lstHolderAccount){
							lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
						}
					}
					securityTOsearch.setLstHolderAccountPk(lstHolderAccountPk);
					if(securityTOsearch.getIdParticipant()!=null && securityTOsearch.getLstHolderAccountPk()!=null 
							&& securityTOsearch.getLstHolderAccountPk().size()>0){
						lstSecurity=interfaceSendingFacade.findSecuritiesListForParticipantInvestor(securityTOsearch);
					}
				}else{
					 lstSecurity=interfaceSendingFacade.findSecuritiesServiceFacade(securityTOsearch);
				}
				
				this.securityDataModel=new GenericDataModel(lstSecurity);

			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALT_NEED_ONE_FIELD));
				JSFUtilities.showValidationDialog();
			}		
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Clean security search handler.
	 *
	 * @param event the event
	 */
	public void cleanSecuritySearchHandler(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		try { 
			securityDataModel=null;
			securityTOsearch=new SecurityTO();
			security=null;
			issuanceHelperSearch=new Issuance();
			securityTOsearch.setIssuanceDate(null);
			securityTOsearch.setExpirationDate(null);
			securityTOsearch.setSecurityState(SecurityStateType.REGISTERED.getCode().toString());
			dateTransfer=null;
			searchFiltersConcat=buildCiteriaSearchConcat();
			
			if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk( userInfo.getUserAccountSession().getIssuerCode() );
				loggedIssuer=new Issuer();
				loggedIssuer=interfaceSendingFacade.findIssuerByFiltersServiceFacade(issuerFilter);
				
				issuerHelperMgmt=issuerHelperSearch;
			} else {
				issuerHelperSearch = new Issuer();
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}

	/**
	 * Generate interface file TR CR.
	 */
	@LoggerAuditWeb
	public void generateInterfaceFileTRCR(){
		
		
		List<Object> listIdSecurity=new ArrayList<>();
		for (Security security : listSecuritySelected) {
			listIdSecurity.add(security.getIdSecurityCodePk());
		}

		String idSecurityPks=CommonsUtilities.convertListObjectToString(listIdSecurity, GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE); 
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(ExtInterfaceConstanst.INTERFACE_PK, GeneralConstants.EXTERNAL_INTERFACE_TR_CR.toString());
		parameters.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, userInfo.getUserAccountSession().getInstitutionType());
		parameters.put(ExtInterfaceConstanst.SECURITY_CODES, idSecurityPks);
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isAfpInstitution()){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getParticipantCode());
		}else if (userInfo.getUserAccountSession().isIssuerInstitucion()){
			parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getIssuerCode());
		}else{
			
			// issue 1255: obtienedo forzamente el id del participante, porque en el userinfo no captura cuando pasa por esta ruta
			if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getUserName())){
				try {
					InfoInstitutionFromUserTO infoInstitution=interfaceSendingFacade.infoInstitutionByUser(userInfo.getUserAccountSession().getUserName());
					if(Validations.validateIsNotNull(infoInstitution)){
						if(Validations.validateIsNotNull(infoInstitution.getMnemonic())){
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, infoInstitution.getMnemonic());
						}else if(Validations.validateIsNotNull(infoInstitution.getIdParticipant())){
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, infoInstitution.getIdParticipant());
						}else if(Validations.validateIsNotNull(infoInstitution.getDescriptionInstitution())){
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, infoInstitution.getDescriptionInstitution());
						}else {
							parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getDescInstitutionType());
						}
					}
				} catch (Exception e) {
					parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getDescInstitutionType());	
				}
			}else{
				parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, userInfo.getUserAccountSession().getDescInstitutionType());
			}
		}
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, parameters);
			
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(PropertiesConstants.DIALOG_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.EXTERNAL_INTERFACE_SEND_CONFIRM_OK
									));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	
		
	}
	
	/**
	 * Check date becoming.
	 */
	public void checkDateBecoming(){

		log.debug("checkDateTransfer: "+boolDateTransfer);
		if(boolDateTransfer){
			dateTransfer=getCurrentSystemDate();
		}else{
			dateTransfer=null;
		}
	}

	/**
	 * Gets the list security selected.
	 *
	 * @return the listSecuritySelected
	 */
	public Security[] getListSecuritySelected() {
		return listSecuritySelected;
	}

	/**
	 * Sets the list security selected.
	 *
	 * @param listSecuritySelected the listSecuritySelected to set
	 */
	public void setListSecuritySelected(Security[] listSecuritySelected) {
		this.listSecuritySelected = listSecuritySelected;
	}

	/**
	 * @return the boolDateTransfer
	 */
	public boolean isBoolDateTransfer() {
		return boolDateTransfer;
	}

	/**
	 * @param boolDateTransfer the boolDateTransfer to set
	 */
	public void setBoolDateTransfer(boolean boolDateTransfer) {
		this.boolDateTransfer = boolDateTransfer;
	}

	/**
	 * @return the dateTransfer
	 */
	public Date getDateTransfer() {
		return dateTransfer;
	}

	/**
	 * @param dateTransfer the dateTransfer to set
	 */
	public void setDateTransfer(Date dateTransfer) {
		this.dateTransfer = dateTransfer;
	}

	/**
	 * @return the validateMaxDate
	 */
	public Date getValidateMaxDate() {
		return validateMaxDate;
	}

	/**
	 * @param validateMaxDate the validateMaxDate to set
	 */
	public void setValidateMaxDate(Date validateMaxDate) {
		this.validateMaxDate = validateMaxDate;
	}

	public List<SelectItem> getLstCboFilterNotTraded() {
		return lstCboFilterNotTraded;
	}

	public void setLstCboFilterNotTraded(List<SelectItem> lstCboFilterNotTraded) {
		this.lstCboFilterNotTraded = lstCboFilterNotTraded;
	}

	public boolean isShowNotTraded() {
		return showNotTraded;
	}

	public void setShowNotTraded(boolean showNotTraded) {
		this.showNotTraded = showNotTraded;
	}
	
}