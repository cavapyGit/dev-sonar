package com.pradera.generalparameters.externalinterface.utils;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class KeywordsSQL.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29-nov-2013
 */
public class KeywordsSQL {
	
	/** The Constant SELECT. */
	public static final String SELECT = "select";
	
	/** The Constant FROM. */
	public static final String FROM = "from";
	
	/** The Constant WHERE. */
	public static final String WHERE = "where";
	
	/** The Constant COLUMN_SEPARATOR. */
	public static final String COLUMN_SEPARATOR = ",";
	
	/** The Constant AND. */
	public static final String AND = "and";
	
	/** The Constant OR. */
	public static final String OR = "or";
	
	/** The Constant BLANK_SPACE. */
	public static final String BLANK_SPACE = " ";
	
	/** The Constant ASTERISK. */
	public static final String ASTERISK = "*";
	
	/** The Constant SUBQUERY_PARAM. */
	public static final String SUBQUERY_PARAM = "�%d?";	
	
	/** The Constant BRACE_OPEN. */
	public static final String BRACE_OPEN = "(";
	
	/** The Constant BRACE_CLOSE. */
	public static final String BRACE_CLOSE = ")";
	
	/** The Constant BRACE_OPEN_CHAR. */
	public static final char BRACE_OPEN_CHAR = '(';
	
	/** The Constant BRACE_CLOSE_CHAR. */
	public static final char BRACE_CLOSE_CHAR = ')';
	
	/** The Constant UNDERSCORE. */
	public static final String UNDERSCORE = "_";
	
	/** The Constant DOT. */
	public static final String DOT = ".";
	
	/** The Constant AS. */
	public static final String AS = "as";
	
	/** The Constant EQUALS. */
	public static final String EQUALS = "=";
	
	/** The Constant GREATER. */
	public static final String GREATER = ">";
	
	/** The Constant LESS. */
	public static final String LESS = "<";
	
	/** The Constant GREATER_EQUALS. */
	public static final String GREATER_EQUALS = ">=";
	
	/** The Constant LESS_EQUALS. */
	public static final String LESS_EQUALS = "<=";
	
	/** The Constant UNEQUAL. */
	public static final String UNEQUAL = "<>";
	
	/** The Constant IN. */
	public static final String IN = "in";
	
	/** The Constant NOT_IN. */
	public static final String NOT_IN = "not in";
	
	/** The Constant EXISTS. */
	public static final String EXISTS = "exists";
	
	/** The Constant ALIAS_LENGTH. */
	public static final Integer ALIAS_LENGTH = 8;
	
	/** The Constant COLON. */
	public static final String COLON = ":";
	
	/** The Constant NO_DEFINE. */
	public static final String NO_DEFINE = "ND";
	
}
