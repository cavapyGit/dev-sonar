package com.pradera.generalparameters.externalinterface.view;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.orderlist.OrderList;

import com.pradera.model.generalparameter.externalinterface.ExtInterfaceTemplateDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrimeFacesOrderListConverter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/09/2015
 */
@FacesConverter(value = "primeFacesOrderListConverter")
public class PrimeFacesOrderListConverter implements Converter {

	/* (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Object ret = null;
		if (arg1 instanceof OrderList) {
			Object orderList = ((OrderList) arg1).getValue();
			List<ExtInterfaceTemplateDetail> list = (List<ExtInterfaceTemplateDetail>) orderList;
			for (ExtInterfaceTemplateDetail o : list) {
				String detalle = ""+o.getDescription();
				if (arg2.equals(detalle)) {
					ret = o;
					break;
				}
			}
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		String str = "";
		if (arg2 instanceof ExtInterfaceTemplateDetail) {
			str = "" + ((ExtInterfaceTemplateDetail) arg2).getDescription();
		}
		return str;
	}
}
