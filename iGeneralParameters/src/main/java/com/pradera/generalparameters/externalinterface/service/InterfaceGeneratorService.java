package com.pradera.generalparameters.externalinterface.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.FileData;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.XMLUtils;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.generalparameters.externalinterface.view.InterfaceSendTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.custody.to.BlockDocumentTO;
import com.pradera.integration.component.custody.to.CouponFeatureTO;
import com.pradera.integration.component.custody.to.DailyReportOfCustodyFCITO;
import com.pradera.integration.component.custody.to.DailyReportOfCustodyFRUVTO;
import com.pradera.integration.component.custody.to.DematerializationSecuritiesByIssuerTO;
import com.pradera.integration.component.custody.to.DetailCustodianAgbSafiFundsTO;
import com.pradera.integration.component.custody.to.DetailCustodianInsuranceTO;
import com.pradera.integration.component.custody.to.SecurityFeatureTO;
import com.pradera.integration.component.custody.to.SubProductIssuanceWithCouponTO;
import com.pradera.integration.component.custody.to.TransferBCBBEMTO;
import com.pradera.integration.component.custody.to.TransferBCBBVDTO;
import com.pradera.integration.component.custody.to.TransferTGNTEMTO;
import com.pradera.integration.component.securities.to.GenerationFileSecurityTRCRTO;
import com.pradera.integration.component.securities.to.SecurityAnnaTransferTO;
import com.pradera.integration.component.securities.to.SecurityNegotiationTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceUserType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityInterfaceAnna;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceGeneratorService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/11/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InterfaceGeneratorService  extends CrudDaoServiceBean{
	
	/** The report generation service. */
	@Inject
	Instance<ReportGenerationService> reportGenerationService;
	
	/** The generator utiles. */
	@EJB
	InterfaceGeneratorUtiles generatorUtiles;
	
	/** The interface extractor data. */
	@EJB
	InterfaceExtractorData interfaceExtractorData;
	
	/** The generator files. */
	@EJB
	InterfaceGeneratorFile generatorFiles;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sending service. */
	@EJB
	InterfaceSendingService sendingService;
	
	/**
	 * Generate external interface.
	 *
	 * @param interfaceSend the interface send
	 * @param parameters the parameters
	 * @return the external interface
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface generateExternalInterface(ExternalInterfaceSend interfaceSend,Map<String,String> parameters) throws ServiceException{
		Long idInterfacePk = interfaceSend.getExternalInterface().getIdExtInterfacePk();
		//start begin of generation external Interface
		ExternalInterface externalInterface = em.find(ExternalInterface.class, idInterfacePk);
		
		List<Participant> participantList = new ArrayList<>();
		List<Issuer> issuerList = null;
		List<Holder> holderList = null;
		
		boolean isUserDepositary = Boolean.FALSE;
		boolean isUserIssuer = Boolean.FALSE;
		String userDepositary = null;
		String userIssuer = null;

		//get all institution type on External Interface
		for(ExternalInterfaceUserType institutionType: externalInterface.getExternalInterfaceUserTypes()){					
			switch (InstitutionType.get(institutionType.getIdUserTypeFk())) {
			case PARTICIPANT:
				participantList.addAll(generatorUtiles.getParticipantList(interfaceSend.getInstitutionRequest()));
				break;
			case AFP:
				participantList.addAll(generatorUtiles.getAFPList(interfaceSend.getInstitutionRequest()));
				break;
			case ISSUER:
				if(Validations.validateIsNotNullAndNotEmpty(interfaceSend.getInstitutionRequest())) {
					issuerList = generatorUtiles.getIssuerList(interfaceSend.getInstitutionRequest());
				} else {	
					isUserIssuer = Boolean.TRUE;
					if(institutionType.getIdUserTypeFk() != null) {
						userIssuer = institutionType.getIdUserTypeFk().toString();
					}
					issuerList = generatorUtiles.getIssuerList(institutionType.getInstitutionCode());
				}				
				break;
			default:
				isUserDepositary = Boolean.TRUE;
				if(institutionType.getIdUserTypeFk() != null) {
					userDepositary = institutionType.getIdUserTypeFk().toString();
				}
				if(idInterfacePk.equals(1034L)){
					holderList = generatorUtiles.getHolderList(interfaceSend.getInstitutionRequest());
				}
				break;
			}
		}
		
		if(isUserDepositary && isUserIssuer) {
			
			//Sending interface for an other kind of user
			sendingService.updateTotalInterface(participantList.size(), interfaceSend.getIdExtInterfaceSendPk());			
			parameters.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, userDepositary);			
			String correlative = generatorUtiles.getLastAnnaCorrelative();
			parameters.put("corr", correlative);
			executeInterfaceByInstitution(parameters, externalInterface,interfaceSend,correlative);
			
			if(issuerList!=null && !issuerList.isEmpty()) {				
				sendingService.updateTotalInterface(participantList.size(), interfaceSend.getIdExtInterfaceSendPk());
				parameters.put(ExtInterfaceConstanst.INSTITUTION_TYPE_ID, userIssuer);
				for(Issuer issuer: issuerList){
					parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, issuer.getIdIssuerPk());
					executeInterfaceByInstitution(parameters, externalInterface,interfaceSend,null);
				}
			}
			
		} else {
			
			//sending interface for Participants && AFPS
			if (participantList != null && !participantList.isEmpty()){
				sendingService.updateTotalInterface(participantList.size(), interfaceSend.getIdExtInterfaceSendPk());
				for(Participant participant: participantList){
					parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, participant.getIdParticipantPk().toString());
					executeInterfaceByInstitution(parameters, externalInterface,interfaceSend,null);
				}
			}else if(issuerList!=null && !issuerList.isEmpty()){
				sendingService.updateTotalInterface(participantList.size(), interfaceSend.getIdExtInterfaceSendPk());
				for(Issuer issuer: issuerList){
					parameters.put(ExtInterfaceConstanst.INSTITUTION_ID, issuer.getIdIssuerPk());
					executeInterfaceByInstitution(parameters, externalInterface,interfaceSend,null);
				}
			}else{
				//Sending interface for an other kind of user
				sendingService.updateTotalInterface(participantList.size(), interfaceSend.getIdExtInterfaceSendPk());
				if(idInterfacePk.equals(1034L)){
					for(Holder holder: holderList){
						parameters.put("corr", holder.getIdHolderPk().toString());
						parameters.put(ExtInterfaceConstanst.HOLDER_CODE, holder.getIdHolderPk().toString());
						executeInterfaceByInstitution(parameters, externalInterface,interfaceSend,null);
					}
				}else{
					String correlative = generatorUtiles.getLastAnnaCorrelative();
					parameters.put("corr", correlative);
					executeInterfaceByInstitution(parameters, externalInterface,interfaceSend,correlative);
				}
			}
		}						
		return externalInterface;
	}
	
	/**
	 * Execute interface by institution.
	 *
	 * @param interfaceTarget the interface target
	 * @param externalInterface the external interface
	 * @param interfaceSend the interface send
	 * @param correlative the correlative
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void executeInterfaceByInstitution(Map<String, String> interfaceTarget, ExternalInterface externalInterface, ExternalInterfaceSend interfaceSend, String correlative) throws ServiceException{
		//setting logger user parameters
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		InstitutionType institutionType = null;
		Long interfaceUserSendId = null;
		String fileName=null;
		try{
			Object targetId = null;
			//Executing externalInterface
			//create tracking of register
			interfaceUserSendId = sendingService.beginInterfaceExecution(interfaceSend,interfaceTarget);//-->Start execution interface process


			institutionType = InstitutionType.get(Integer.parseInt(interfaceTarget.get(ExtInterfaceConstanst.INSTITUTION_TYPE_ID)));
			if(interfaceTarget.get(ExtInterfaceConstanst.INSTITUTION_ID)!=null){
				targetId = interfaceTarget.get(ExtInterfaceConstanst.INSTITUTION_ID).toString();
			}
			
			InterfaceSendTO interfaceSendTO = getInterfaceSendTO(externalInterface, interfaceTarget);
			fileName=CommonsUtilities.createNameFile(externalInterface.getFileName(), gettingParametersForName(interfaceSendTO,interfaceTarget));
			
			List<File> filesToSend = new ArrayList<>();
			
			List<Object> dataList = sendingService.getResultsQueryInterface(interfaceSendTO);

			File xmlGenericFile = null; 
			if(dataList!=null && !dataList.isEmpty()){
				List<Object>listObjectValidations=generateDataFromQuery(dataList, interfaceSendTO);
				if(interfaceSendTO.getId().equals(1021L)){
					boolean saveCorrelative = interfaceSendTO.getInitialDate().equals(interfaceSendTO.getFinalDate());
					saveSecurityInterfaceAnna( dataList, fileName,correlative,saveCorrelative);
				}else if(interfaceSendTO.getId().equals(GeneralConstants.EXTERNAL_INTERFACE_TR_CR)){
					listObjectValidations=regularizedBeanTRCR(listObjectValidations);
				}
				if(Validations.validateListIsNotNullAndNotEmpty(listObjectValidations)){
					if(GeneralConstants.EXTERNAL_INTERFACE_TR_CR.equals(interfaceSendTO.getId())){
						xmlGenericFile=generateXmlFile(listObjectValidations,fileName,externalInterface.getSchemaDirectory(),
								ComponentConstant.INTERFACE_TAG_ISSUANCES,ComponentConstant.INTERFACE_TAG_ISSUANCE);
					}else{
						xmlGenericFile=generateXmlFile(listObjectValidations,fileName,externalInterface.getSchemaDirectory(),
								ComponentConstant.INTERFACE_TAG_ROWSET,ComponentConstant.INTERFACE_TAG_ROW);
					}
					filesToSend.add(xmlGenericFile);
				}
				
				boolean blSendFtp = Boolean.FALSE;
				if(interfaceTarget.get(ExtInterfaceConstanst.SEND_FTP) != null){
					blSendFtp = Boolean.parseBoolean(interfaceTarget.get(ExtInterfaceConstanst.SEND_FTP).toString());					
				}
				
				//get id to send like parameters
				Long idInterfacePk = externalInterface.getIdExtInterfacePk();
				Long idInterfaceSendPk = interfaceSend.getIdExtInterfaceSendPk();
				sendInterfaceFileExecution(institutionType, targetId, filesToSend, idInterfacePk, idInterfaceSendPk, blSendFtp);
				
				
			}
			
			
			//finish track of interface generation 
			sendingService.finishInterfaceExecution(interfaceUserSendId,filesToSend,xmlGenericFile,institutionType,targetId);
		}catch(ServiceException ex){
			sendingService.finishInterfaceWithErrors(interfaceUserSendId, ex);
		}catch(Exception ex){
			sendingService.finishInterfaceWithErrors(interfaceUserSendId, ex);
//			ex.printStackTrace();
			try {
				throw ex;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	
	/**
	 * Gets the ting parameters for name.
	 *
	 * @param interfaceSendTO the interface send to
	 * @param params the params
	 * @return the ting parameters for name
	 */
	public Map<String,Object> gettingParametersForName(InterfaceSendTO interfaceSendTO,Map<String,String> params){
		Map<String,Object> parameter=new HashMap<String,Object>();
		
		if(params.get(ExtInterfaceConstanst.FILTER_INTERFACE_ANN_CORR)!=null){
			parameter.put(ExtInterfaceConstanst.FILTER_INTERFACE_ANN_CORR, params.get(ExtInterfaceConstanst.FILTER_INTERFACE_ANN_CORR));
		}
		
		parameter.put(ExtInterfaceConstanst.INITIAL_DATE, interfaceSendTO.getInitialDate());
		parameter.put(ExtInterfaceConstanst.FINAL_DATE, interfaceSendTO.getFinalDate());
		if(Validations.validateIsNotNull(interfaceSendTO.getIssuerRequest())){
			parameter.put(ExtInterfaceConstanst.ISSUER_CODE, interfaceSendTO.getIssuerRequest().getIdIssuerPk());
		}
		parameter.put(ExtInterfaceConstanst.PARTICIPANT_CODE, interfaceSendTO.getMnemonic() == null ? params.get(ExtInterfaceConstanst.INSTITUTION_ID) : interfaceSendTO.getMnemonic() );
		
		
		return parameter;
	}
	
	/**
	 * Generate data from query.
	 *
	 * @param dataList the data list
	 * @param interfaceSendTO the interface send to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("rawtypes")
	public List<Object> generateDataFromQuery(List<Object> dataList , InterfaceSendTO interfaceSendTO) throws ServiceException{
		Class entityTOClass=null;
		Map<String,Object> mapObject=null;
		List<Object> listClass=new ArrayList<>();
		List<String> listOfAlias=null;

		if(Validations.validateListIsNotNullAndNotEmpty(dataList)){
			if(interfaceSendTO.getInterfaceId().equals(1001L)){
				entityTOClass=DematerializationSecuritiesByIssuerTO.class;
				listOfAlias=Arrays.asList("index","instrument","idSecurityCode","securityAlter","currencyCode",
						"idHolderFk","indResidence","nitHolder","numTitle","quantiy","rate","unitPrice",
						"operationCode","holderName","dateDes");
			}else if(interfaceSendTO.getId().equals(1020L)){
				entityTOClass=BlockDocumentTO.class;
				listOfAlias=Arrays.asList("action","instrument","serie","serieAlter","cui","documentNumber","blockCode","documentDate","documentRequestPk","stockQuantity","holderName");
			}else if(interfaceSendTO.getId().equals(1021L)){
				entityTOClass=SecurityAnnaTransferTO.class;
				listOfAlias=
				Arrays.asList("idIsinCode","securityState","instrument","description","cfiCode","fisin","preliminaryTerms","nominalValue","currency","smallDenomination","conversionRatio",
							  "expirationDate","exercisePrice","exerciseCurrPrice","underlying","interestType","interestRate","firstPaymentDateSmall","frequency",
							  "firstPaymentDate","comments","mic","lei","fmn","fml","lei2","depositary","issuerName","issuerLei","mnemonicIssuer","supranacional",
							  "address_1","address_2","addressState","postCode","city","country","c_38","c_39","c_40","c_41","c_42","c_43",
							  "idSecurityCodePk");
			}else if(interfaceSendTO.getId().equals(1022L)){
				entityTOClass=SecurityNegotiationTO.class;
				listOfAlias=
				Arrays.asList("transactionDate","instrument","serie","alternativeCode","issuanceDate","expirationDate",
							  "issuanceRate","cui","documentNumber","resident","stockQuantity","operation","holderName");
			}else if(interfaceSendTO.getId().equals(1002L)){
				entityTOClass=SecurityFeatureTO.class;
				listOfAlias=Arrays.asList(  "cTipoInstrumento","tClaveInstrumento","cEmisor","cCustodio","fInformacion",
											"fRegistroCustodio","cEntidad","cMoneda","mValorNominal","fEmision",
											"fVencimiento","qValores","pTasaRendimiento","pTasaDescuento","qCupones",
											"cTipoAmortizacion","tPeriodicidad","tModalidadNegocia","tMesaDinero");
			}else if(interfaceSendTO.getId().equals(1003L)){
				entityTOClass=CouponFeatureTO.class;
				listOfAlias=Arrays.asList("cCustodio","fInformacion","cTipoInstrumento","tClaveInstrumento",
												"nCupon","fVencimiento","mAmortizacionCapital","mInteres","mTotalCupon");
			}else if(interfaceSendTO.getId().equals(1004L)){
				entityTOClass=SubProductIssuanceWithCouponTO.class;
				listOfAlias=Arrays.asList("cCustodio","fInformacion","cTipoInstrumento","tClaveInstrumento",
												"cCupDesprendidos","cCupVigentes");
			}else if(interfaceSendTO.getId().equals(1030L)){
				entityTOClass=TransferBCBBVDTO.class;
				listOfAlias=Arrays.asList("numeroRegistro","claveValor",
												"cedulaVendedor","nombreVendedor","cedulaComprador","nombreComprador","cantidad","fecha");
			}else if(interfaceSendTO.getId().equals(1031L)){
				entityTOClass=TransferBCBBEMTO.class;
				listOfAlias=Arrays.asList("numeroRegistro","claveValor",
												"cuentaVendedor","cuentaComprador","cantidad","fecha","mechanismOperationPk"); //issue 491
			}else if(interfaceSendTO.getId().equals(1032L)){
				entityTOClass=TransferTGNTEMTO.class;
				listOfAlias=Arrays.asList("numeroRegistro","claveValor",
												"cuentaVendedor","cuentaComprador","cantidad","fecha","mechanismOperationPk"); //issue 491

			}else if(interfaceSendTO.getId().equals(1033L)){
				entityTOClass=DetailCustodianAgbSafiFundsTO.class;
				listOfAlias=Arrays.asList("cCustodio","fInforme","nemonicoAgbSafi","fondo",
												"instrumento","serie","codValoracion","qValoresCustodia",
												"pTasaRendimientoEquivalente","mPrecioEquivalente","tCustodia","cReporto");
			}else if(interfaceSendTO.getId().equals(1034L)){
				entityTOClass=DetailCustodianInsuranceTO.class;
				listOfAlias=Arrays.asList("claseValor","claveValor","tasaRelevanteValoracion","precioNominalMonedaOriginal",
												"precioNominalBolivianos","precioMercadoMonedaOriginal","precioMercadoBolivianos",
												"cantidadTotalValores","montoTotalMonedaOriginal","codigoCustodia");
			}else if(interfaceSendTO.getId().equals(1035L)){
				entityTOClass=DailyReportOfCustodyFRUVTO.class;
				listOfAlias=Arrays.asList("claseValor","claveValor","tasaRelevanteValoracion","precioNominalMonedaOriginal","precioNominalBolivianos", 	
										  "precioMercadoMonedaOriginal","precioMercadoBolivianos","cantidadTotalValores", 		
										  "montoTotalMonedaOriginal","codigoCustodia","codigoReporto");
			}else if(interfaceSendTO.getId().equals(1036L)){
				entityTOClass=DailyReportOfCustodyFCITO.class;
				listOfAlias=Arrays.asList("claseValor","claveValor","tasaRelevanteValoracion","precioNominalMonedaOriginal","precioNominalBolivianos",
										  "precioMercadoMonedaOriginal","precioMercadoBolivianos","cantidadTotalValores","montoTotalMonedaOriginal",
										  "codigoCustodia");
			}else if(interfaceSendTO.getId().equals(1037L)){
				entityTOClass=DailyReportOfCustodyFCITO.class;
				listOfAlias=Arrays.asList("claseValor","claveValor","tasaRelevanteValoracion","precioNominalMonedaOriginal","precioNominalBolivianos",
										  "precioMercadoMonedaOriginal","precioMercadoBolivianos","cantidadTotalValores","montoTotalMonedaOriginal",
										  "codigoCustodia");
			}else if(interfaceSendTO.getId().equals(1038L)){
				entityTOClass=DailyReportOfCustodyFCITO.class;
				listOfAlias=Arrays.asList("claseValor","claveValor","tasaRelevanteValoracion","precioNominalMonedaOriginal","precioNominalBolivianos",
										  "precioMercadoMonedaOriginal","precioMercadoBolivianos","cantidadTotalValores","montoTotalMonedaOriginal",
										  "codigoCustodia");
			}else if(interfaceSendTO.getId().equals(1039L)){
				entityTOClass=DailyReportOfCustodyFCITO.class;
				listOfAlias=Arrays.asList("claseValor","claveValor","tasaRelevanteValoracion","precioNominalMonedaOriginal","precioNominalBolivianos",
										  "precioMercadoMonedaOriginal","precioMercadoBolivianos","cantidadTotalValores","montoTotalMonedaOriginal",
										  "codigoCustodia");			
			}else if(interfaceSendTO.getId().equals(GeneralConstants.EXTERNAL_INTERFACE_TR_CR)){
				entityTOClass=GenerationFileSecurityTRCRTO.class;
				listOfAlias=Arrays.asList("idSecurityCodePk","instrumentType","serie","emisor",
										  "registryDate","expirationDate","currency",
										  "valorEmision","valorNominal","tasaRend","valuacion","nroCupones",
										  "plazoAmortiza","prepago","subordinado","tipoBono","tipoTasa",
										  "tasaDif","negFci","subProducto","tasaPen",
										  
										  "nroCupon","fechaVenci","amortizaK","tasaCuponDif");			
			}
			
			listClass=CommonsUtilities.filledBean(listOfAlias, entityTOClass, dataList);

		} 
		return listClass;		
	}
	

	/**
	 * Generate xml file.
	 *
	 * @param listObjectValidations the list object validations
	 * @param nameFile the name file
	 * @param schemeName the scheme name
	 * @param rowset the rowset
	 * @param row the row
	 * @return the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public File generateXmlFile(List<Object>listObjectValidations,String nameFile, String schemeName,
								String rowset, String row) throws IOException{

 
		return CommonsUtilities.generateXmlFile(listObjectValidations, nameFile, schemeName, rowset,row);
	}
	
	/**
	 * Send interface to report execution.
	 *
	 * @param institutionType the institution type
	 * @param targetId the target id
	 * @param file the file
	 * @param interfacePk the interface pk
	 * @param interfaceSendPk the interface send pk
	 * @param blSendFtp the bl send ftp
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendInterfaceFileExecution(InstitutionType institutionType, Object targetId, List<File> file, Long interfacePk, 
			Long interfaceSendPk, boolean blSendFtp) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//Map to put detail of send
		Map<String,String> parametersMap = new HashMap<>();
		parametersMap.put(ExtInterfaceConstanst.FILTER_INTERFACE_PK, interfacePk.toString());
		parametersMap.put(ExtInterfaceConstanst.FILTER_INTERFACE_SEND_PK, interfaceSendPk.toString());
		
		//get ReportUser to send remote Method of Reports
		ReportUser reportUser = new ReportUser();
		reportUser.setIndExtInterface(BooleanType.YES.getCode());
		reportUser.setInstitutionType(institutionType.getCode());
		reportUser.setShowNotification(BooleanType.YES.getCode());
		
		if(institutionType.equals(InstitutionType.PARTICIPANT) || institutionType.equals(InstitutionType.AFP)){
			reportUser.setPartcipantCode(Long.parseLong(targetId.toString()));
		}else if(institutionType.equals(InstitutionType.ISSUER)){
			reportUser.setIssuerCode(targetId.toString());
		}else{
//			reportUser.setUserName(targetId.toString());
		}
		
		try {
			//Get reportIf from ExternalInterface
			Long reportId = ExtInterfaceConstanst.REPORT_ASOCIATED;
			
			//Get all report generated by External Interface
			List<ReportFile> reportFileList = new ArrayList<ReportFile>();
			for(File fileContent: file){
				ReportFile reportFile = new ReportFile();
				reportFile.setFile(XMLUtils.getBytesFromFile(fileContent));
				reportFile.setPhysicalName(fileContent.getName().substring(0,fileContent.getName().lastIndexOf(".")));
				reportFile.setReportType(generatorUtiles.getReportFormatFromFileName(fileContent.getName()).getCode());
				reportFileList.add(reportFile);
			}
			//send files on reports bitacore
			Long lgIdReportLogger = reportGenerationService.get().saveReportFile(reportId, reportUser, parametersMap, null, loggerUser, reportFileList);						
			Thread.sleep(10000);// 10 seconds waiting for send reports
			
			if(reportFileList != null && lgIdReportLogger != null && blSendFtp) {
				List<FileData> listDataReport = new ArrayList<FileData>();
				FileData objFileData = new FileData();
				byte [] reportData = reportGenerationService.get().getDataReport(lgIdReportLogger);
				if(reportData != null) {
					objFileData.setReportName(file.get(0).getName());
					objFileData.setReportData(reportData);				
					listDataReport.add(objFileData);
				}
				if(Validations.validateListIsNotNullAndNotEmpty(listDataReport)) {
					 reportGenerationService.get().sendFtpUpload(listDataReport, GeneralConstants.PK_SEND_INTERFACES_BCB);
				}
			}
			
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch(NullPointerException ex){
			throw new ServiceException(ErrorServiceType.INTERFACE_ERROR_SEND_FILE);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the interface send to.
	 *
	 * @param interfac the interfac
	 * @param parameter the parameter
	 * @return the interface send to
	 */
public InterfaceSendTO getInterfaceSendTO(ExternalInterface interfac, Map<String,String> parameter){
		
		InterfaceSendTO interfaceTO = new InterfaceSendTO();
		
		String query = interfac.getQuerySql();
		interfaceTO.setId(interfac.getIdExtInterfacePk());
		interfaceTO.setQuerySql(query);
		interfaceTO.setInterfaceId(interfac.getIdExtInterfacePk());
		String initialDate = parameter.get(ExtInterfaceConstanst.INITIAL_DATE);
		if(initialDate!=null){
			interfaceTO.setInitialDate(CommonsUtilities.convertStringtoDate(initialDate, CommonsUtilities.DATE_PATTERN));
		}
		
		String finalDate = parameter.get(ExtInterfaceConstanst.FINAL_DATE);
		if(finalDate!=null){
			interfaceTO.setFinalDate(CommonsUtilities.convertStringtoDate(finalDate, CommonsUtilities.DATE_PATTERN));
		}
		
		String institutionType = parameter.get(ExtInterfaceConstanst.INSTITUTION_TYPE_ID);
		if(institutionType!=null){
			interfaceTO.setInstitutionType(Integer.parseInt(institutionType));
		}
		
		String holderCode = parameter.get(ExtInterfaceConstanst.HOLDER_CODE);
		if(holderCode!=null){
			interfaceTO.setHolderCode(Long.parseLong(holderCode));
		} 
		
		/*Estado*/
		String state = parameter.get(ExtInterfaceConstanst.STATE);
		if(state!=null){
			interfaceTO.setState(Integer.parseInt(state));
		}else
			interfaceTO.setState(-1);
		
		String institutionCode = parameter.get(ExtInterfaceConstanst.INSTITUTION_ID);
		if(institutionCode!=null){
			if(interfaceTO.getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode())){
//				interfaceTO.setParticipantId(Long.parseLong(institutionCode));
				interfaceTO.setMnemonic(institutionCode);
			}else if(interfaceTO.getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
				interfaceTO.setIssuerRequest(new Issuer(institutionCode));
			}else if(interfaceTO.getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode())){
				interfaceTO.setMnemonic(institutionCode);
			}
		}
		
		String securityCodePks = parameter.get(ExtInterfaceConstanst.SECURITY_CODES);
		if(securityCodePks!=null && !securityCodePks.isEmpty()){
			List listIdSecurityCodePk=new ArrayList<>();
			String[]  listSecurityCode=securityCodePks.split(";");
			for (int i = 0; i < listSecurityCode.length; i++) {
				String idSecurityCodePk = listSecurityCode[i];
				listIdSecurityCodePk.add(idSecurityCodePk);
			}
			interfaceTO.setListIdSecurityCodePk(listIdSecurityCodePk);
			
		}
		return interfaceTO;
	}
	

	/**
	 * Create Security Interface Anna .
	 *
	 * @param dataList the data list
	 * @param fileName the file name
	 * @param correlative the correlative
	 * @param saveData the save data
	 */
	public void saveSecurityInterfaceAnna(List<Object> dataList, String fileName, String correlative, boolean saveData){		

		for (Object object : dataList) {
			SecurityInterfaceAnna securityInterfaceAnna=new SecurityInterfaceAnna();
			Object[] dataMap=(Object[])object;
			if(saveData){
				String security=dataMap[43].toString();
				securityInterfaceAnna.setSecurity(new Security(security));;
			}
			securityInterfaceAnna.setCorrelativeAnnaCode(correlative);
			securityInterfaceAnna.setNameFileInterface(fileName);
			create(securityInterfaceAnna);

			if(!saveData){
				return;
			}
		}
	}
	
	/**
	 * Regularized bean trcr.
	 *
	 * @param listObject the list object
	 * @return the list
	 */
	public List<Object> regularizedBeanTRCR(List<Object> listObject){
		
		List<Object> newList =new ArrayList<Object>();
		GenerationFileSecurityTRCRTO generationFileSecurityTRCRTO=null;
		String idSecurityCodeLast="";
		for (Object object : listObject) {
			if(object instanceof GenerationFileSecurityTRCRTO){
				GenerationFileSecurityTRCRTO generationFileSecurityTRCRTOObj=(GenerationFileSecurityTRCRTO)object;
				
				if(!idSecurityCodeLast.equals(generationFileSecurityTRCRTOObj.getIdSecurityCodePk())){
					generationFileSecurityTRCRTO=new GenerationFileSecurityTRCRTO(generationFileSecurityTRCRTOObj);
					
					newList.add(generationFileSecurityTRCRTO);
				}else{
					generationFileSecurityTRCRTO.addNewCouponDetail(generationFileSecurityTRCRTOObj);
					
				}
				
				idSecurityCodeLast=generationFileSecurityTRCRTOObj.getIdSecurityCodePk();
				
			}
			
		}
		return newList;
		
	}
	
	
	/**
	 * Gets the list external interface user type.
	 *
	 * @param idInterfacePk the id interface pk
	 * @return the list external interface user type
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ExternalInterfaceUserType> getListExternalInterfaceUserType(Long idInterfacePk) throws ServiceException{
		List<ExternalInterfaceUserType> lstExternalInterfaceUserType = new ArrayList<ExternalInterfaceUserType>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" SELECT eiut ");
			sbQuery.append(" FROM ExternalInterfaceUserType eiut  ");			
			sbQuery.append(" WHERE eiut.externalInterface.idExtInterfacePk = :idInterfacePk ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idInterfacePk", idInterfacePk);
			lstExternalInterfaceUserType = (List<ExternalInterfaceUserType>) query.getResultList();					
		} catch (NoResultException e) {
			return null;
		}				
		return lstExternalInterfaceUserType;		
	}		
}