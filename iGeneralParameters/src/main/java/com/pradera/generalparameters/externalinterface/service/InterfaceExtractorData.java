package com.pradera.generalparameters.externalinterface.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceFilterPredicate;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceColumn;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceFilter;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceQuery;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceContentData;
import com.pradera.model.generalparameter.externalinterface.type.DataColumnType;
import com.pradera.model.generalparameter.externalinterface.type.InterfacePredicateType;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceGeneratorService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/11/2013
 */
@Stateless
public class InterfaceExtractorData extends CrudDaoServiceBean{

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The parameter maps. */
	private static Map<Integer, String> parameterMaps = new HashMap<Integer,String>();
	
	/**
	 * Gets the children from query.
	 *
	 * @param interfaceQuery the interface query
	 * @param interfaceQueryList the interface query list
	 * @return the children from query
	 */
	public List<ExternalInterfaceQuery> getChildrenFromQuery(ExternalInterfaceQuery interfaceQuery, List<ExternalInterfaceQuery> interfaceQueryList){
		//break to iterate same ExternalInterfaceQuery List
		List<ExternalInterfaceQuery> queriesReferencedList = new ArrayList<>();
		//find if main query has some child
		for(ExternalInterfaceQuery interfaceAuxQuery : interfaceQueryList){
			//getting interfaceQuery referenced with parent
			ExternalInterfaceQuery queryReferenced = interfaceAuxQuery.getExternalInterfaceQuery();
			if(queryReferenced!=null){
				//if query reference is the same a interfaceMainQuery
				if(queryReferenced.getIdInterfaceQueryPk().equals(interfaceQuery.getIdInterfaceQueryPk())){
					queriesReferencedList.add(interfaceAuxQuery);
				}
			}
		}
		return queriesReferencedList;
	}
	/**
	 * Gets the content data.
	 *
	 * @param parameters the parameters
	 * @param interfaceQuery the interface query
	 * @param interfaceQueryList the interface query list
	 * @param institutionId the institution id
	 * @return the content data
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InterfaceContentData> getContentData(Map<String,Object> parameters,ExternalInterfaceQuery interfaceQuery, List<ExternalInterfaceQuery> interfaceQueryList, String institutionId) throws ServiceException{ 
		//Declare list to handle all data, of content data
		List<InterfaceContentData> conteDataList = new ArrayList<InterfaceContentData>();
		
		//get all children from interfaceQuery
		List<ExternalInterfaceQuery> queryChilds = getChildrenFromQuery(interfaceQuery, interfaceQueryList);
		
		//get query to execute
		String queryExecute = replaceQueryWithParameters(interfaceQuery.getInterfaceTemplateQuery());
		
		//execute query on database
		Query queryMain = em.createNativeQuery(queryExecute);
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		for (Entry<String, Object> entry : rawParameters) {
			queryMain.setParameter(entry.getKey(), entry.getValue());
		}
		
		List<Object[]> objectDataArray;
		try {
			objectDataArray = queryMain.getResultList();
		} catch (NoResultException ex) {
			objectDataArray = new ArrayList<Object[]>();
		}
		
		if(queryChilds.isEmpty()){
			List<Object[]> dataFormated = getObjectDataWithParameters(objectDataArray, interfaceQuery);
			InterfaceContentData contentData = new InterfaceContentData(dataFormated);
			return Arrays.asList(contentData);
		}else{
			for(Object[] objectData: objectDataArray){
				List<Object[]> objectDataList = new ArrayList<>();
				objectDataList.add(objectData);
				InterfaceContentData contentDataParent = new InterfaceContentData();
				
				InterfaceContentData contentData = new InterfaceContentData(getObjectDataWithParameters(objectDataList, interfaceQuery));
				contentDataParent.getDetail().add(contentData);
				
				Map<String,Object> mapParameters = null;
				for(ExternalInterfaceQuery queryChildren: queryChilds){
					mapParameters = getPredicateOnQuery(objectData, institutionId, queryChildren,interfaceQuery);
					contentData.getDetail().addAll(getContentData(mapParameters, queryChildren, interfaceQueryList,institutionId));
				}
				conteDataList.add(contentDataParent);
			}
		}
		return conteDataList;
	}
	/**
	 * Gets the object data with parameters.
	 *
	 * @param objectData the object data
	 * @param interfaceQuery the interface query
	 * @return the object data with parameters
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getObjectDataWithParameters(List<Object[]> objectData, ExternalInterfaceQuery interfaceQuery) throws ServiceException{
		List<ExternalInterfaceColumn> interfaceColumns = new ArrayList<>();
		List<Object[]> dataToReturn = new ArrayList<>();  
		for(ExternalInterfaceTable interfaceTable: interfaceQuery.getExternalInterfaceTables()){
			for(ExternalInterfaceColumn interfaceColumn: interfaceTable.getExternalInterfaceColumns()){
				if(interfaceColumn.getIndVisible().equals(BooleanType.YES.getCode())){
					interfaceColumns.add(interfaceColumn);
				}
			}
		}
		for(Object[] objectRowData: objectData){
			dataToReturn.add(getObjectRowDataFormated(objectRowData, interfaceColumns));
		}
		return dataToReturn;
	}
	/**
	 * Gets the object row data formated.
	 *
	 * @param objectRowData the object row data
	 * @param interfaceColumnList the interface column list
	 * @return the object row data formated
	 * @throws ServiceException the service exception
	 */
	public Object[] getObjectRowDataFormated(Object[] objectRowData, List<ExternalInterfaceColumn> interfaceColumnList) throws ServiceException{
		List<Object> objectRowDataReturn = new ArrayList<Object>();
		for(ExternalInterfaceColumn columnOfData: interfaceColumnList){
			Object data = objectRowData[columnOfData.getOrderPriority()-1];
			Integer maxLength = columnOfData.getLength();

			//validate, if column is parameterTable
			if(columnOfData.getIndParameterTable().equals(BooleanType.YES.getCode())){
				Integer parameterTablePk = Integer.parseInt(data.toString());
				data = getParameterValue(parameterTablePk);
			}
			if(data!=null){
				switch(DataColumnType.get(columnOfData.getDataTypeTarget())){
					case DATE: case TIMESTAMP:
						data = getDateFormated(columnOfData,data);break;
					case NUMBER: 
						data = getNumberFormated(columnOfData,data);break;
				}
				data = getDataWithLength(data.toString(), maxLength, " ");
				objectRowDataReturn.add(data);
			}else{
				objectRowDataReturn.add(columnOfData.getDefaultValue());
			}
		}
		return objectRowDataReturn.toArray();
	}
	/**
	 * Gets the parameter value.
	 *
	 * @param parameterTablePk the parameter table pk
	 * @return the parameter value
	 */
	public String getParameterValue(Integer parameterTablePk){
		String data = null;
		if(parameterMaps.get(parameterTablePk)!=null){
			data = parameterMaps.get(parameterTablePk);
		}else{
			Query query = em.createNamedQuery(ParameterTable.FIND_NAME_BY_PK);
			query.setParameter("pk", parameterTablePk);
			parameterMaps.put(parameterTablePk, ((ParameterTable)query.getSingleResult()).getParameterName());
			data = parameterMaps.get(parameterTablePk);
		}
		return data;
	}
	/**
	 * Gets the date formated.
	 *
	 * @param interfaceColumn the interface column
	 * @param objectDate the object date
	 * @return the date formated
	 * @throws ServiceException the service exception
	 */
	public String getDateFormated(ExternalInterfaceColumn interfaceColumn, Object objectDate) throws ServiceException{
		String formatOfDate = getParameterValue(interfaceColumn.getFormatValue());
		if(formatOfDate!=null){
			SimpleDateFormat formato = new SimpleDateFormat(formatOfDate);
			return formato.format(objectDate);
		}
		return objectDate.toString();
	}
	/**
	 * Gets the data with length.
	 *
	 * @param data the data
	 * @param length the length
	 * @param separator the separator
	 * @return the data with length
	 * @throws ServiceException the service exception
	 */
	public String getDataWithLength(String data, Integer length, String separator)throws ServiceException{
		length = length-1;
		if(data.trim().length()>length){
			return data.substring(0,length); 
		}else if(data.trim().length()<length){
			Integer tamFaltant = length - data.trim().length();
			StringBuffer stringBuffer = new StringBuffer(data);
			for(int i=0; i<tamFaltant; i++){
				stringBuffer.append(separator);
			}
			return stringBuffer.toString().substring(0,length);
		}
		return data;
	}
	/**
	 * Gets the number formated.
	 *
	 * @param interfaceColumn the interface column
	 * @param objectNumber the object number
	 * @return the number formated
	 * @throws ServiceException the service exception
	 */
	public String getNumberFormated(ExternalInterfaceColumn interfaceColumn, Object objectNumber) throws ServiceException{
		Integer decimalQuantity = interfaceColumn.getDecimalQuantity();
		String format = null;
		StringBuffer sb = new StringBuffer(ExtInterfaceConstanst.CHAR);
		if(decimalQuantity!=null && decimalQuantity>0){
			sb.append(".");
			for(int i=0; i<decimalQuantity; i++){
				sb.append(ExtInterfaceConstanst.CHAR);
			}
		}
		format = sb.toString();
		DecimalFormat myFormatter = new DecimalFormat(format);
		return myFormatter.format(objectNumber);
	}
	
	/**
	 * Gets the data query list.
	 *
	 * @param institutionId the institution id
	 * @param externalInterface the external interface
	 * @return the data query list
	 * @throws ServiceException the service exception
	 */
	public ExternalInterface getDataQueryList(String institutionId, ExternalInterface externalInterface) throws ServiceException{
		List<ExternalInterfaceQuery> intQueryList = externalInterface.getExternalInterfaceQuerys();
		List<InterfaceContentData> contentDataList = new ArrayList<InterfaceContentData>();//declare list to handle all data generated
		
		//iterate ExternalInterfaceQuery
		for(ExternalInterfaceQuery interfaceMainQuery: intQueryList){
			if(interfaceMainQuery.getExternalInterfaceQuery()==null){
				//break to iterate same ExternalInterfaceQuery List
				List<ExternalInterfaceQuery> queriesReferencedList = new ArrayList<>();
				queriesReferencedList = getChildrenFromQuery(interfaceMainQuery, externalInterface.getExternalInterfaceQuerys());
				
				Map<String,Object> mapData = new HashMap<String,Object>();
				if(havePredicateByType(interfaceMainQuery, InterfacePredicateType.USER_TYPE)){
					//get predicate to execute on query
					mapData = getPredicateOnQuery(null,institutionId, interfaceMainQuery,null);
				}
				//call recursive method
				contentDataList.addAll(getContentData(mapData, interfaceMainQuery, queriesReferencedList,institutionId));
			}
		}
		externalInterface.setInterfaceContentDatas(contentDataList);
		return externalInterface;
	}
	
	/**
	 * Replace query with parameters.
	 *
	 * @param query the query
	 * @return the string
	 */
	public String replaceQueryWithParameters(String query){
		String[] stringSplited = query.split(ExtInterfaceConstanst.IDENTIFICATOR);
		if(stringSplited.length!=0){
			StringBuilder queryWithParameters = new StringBuilder();
			for(Integer i=0; i<stringSplited.length; i++){
				if(i.equals(stringSplited.length-1) && stringSplited.length!=BigDecimal.ONE.intValue()){
					queryWithParameters.append(stringSplited[i]);
				}else{
					queryWithParameters.append(stringSplited[i]).append(ExtInterfaceConstanst.IDENTIFICATOR).append(i+1);
				}
			}
			return queryWithParameters.toString();
		}
		return query;
	}
	
	/**
	 * Gets the predicate on query.
	 *
	 * @param objectDataRow the object data row
	 * @param institutionCode the institution code
	 * @param queryChildren the query children
	 * @param queryParent the query parent
	 * @return the predicate on query
	 */
	public Map<String,Object> getPredicateOnQuery(Object[] objectDataRow, String institutionCode, ExternalInterfaceQuery queryChildren, ExternalInterfaceQuery queryParent){
		Map<String,Object> predicates = new HashMap<String,Object>();
		//find predicate with recursivity
		Integer count = 0;
		for(ExternalInterfaceTable table: queryChildren.getExternalInterfaceTables()){//start iterate tables
			for(ExternalInterfaceFilter filter: table.getExternalInterfaceFilters()){
				for(ExtInterfaceFilterPredicate predicate: filter.getExtInterfaceFilterPredicates()){
					Object data = null;
					switch(InterfacePredicateType.get(predicate.getPredicateType())){
						case USER_TYPE: data = institutionCode;
							break;
						case COLUMN:
								ExternalInterfaceColumn columnFilter = predicate.getExternalInterfaceColumn();
								Long interfaceQueryPk = columnFilter.getExternalInterfaceTable().getExternalInterfaceQuery().getIdInterfaceQueryPk();
								if(!interfaceQueryPk.equals(queryChildren.getIdInterfaceQueryPk()) && interfaceQueryPk.equals(queryParent.getIdInterfaceQueryPk())){
									data = objectDataRow[columnFilter.getOrderPriority().intValue()-1];
								}
							break;
					}
					if(data!=null){
						predicates.put(ExtInterfaceConstanst.PARAMETER_QUERY.concat((++count).toString()),data);
					}
				}
			}
		}
		return predicates;
	}
	/**
	 * Have predicate by type.
	 *
	 * @param interfaceQuery the interface query
	 * @param predicateType the predicate type
	 * @return the boolean
	 */
	public Boolean havePredicateByType(ExternalInterfaceQuery interfaceQuery, InterfacePredicateType predicateType){
		for(ExternalInterfaceTable table: interfaceQuery.getExternalInterfaceTables()){//start iterate tables
			for(ExternalInterfaceFilter filter: table.getExternalInterfaceFilters()){//iterate interfaceFilter
				for(ExtInterfaceFilterPredicate predicate: filter.getExtInterfaceFilterPredicates()){//iterate all predicates
					if(predicate.getPredicateType().equals(predicateType.getCode())){//compare code with parameter on method
						return Boolean.TRUE;
					}
				}
			}
		}
		return Boolean.FALSE;
	}
}