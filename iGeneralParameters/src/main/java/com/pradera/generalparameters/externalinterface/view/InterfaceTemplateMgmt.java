package com.pradera.generalparameters.externalinterface.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.generalparameters.externalinterface.facade.InterfaceTemplateServiceFacade;
import com.pradera.generalparameters.utils.view.PropertiesConstants;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExtInterfaceTemplateDetail;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceTemplate;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateDetailStateType;
import com.pradera.model.generalparameter.externalinterface.type.InterfaceTemplateStateType;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceTemplateMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-oct-2013
 */
@DepositaryWebBean
public class InterfaceTemplateMgmt extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5776760523558006664L;
		
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The interface template service facade. */
	@EJB
	private InterfaceTemplateServiceFacade interfaceTemplateServiceFacade;
	
	/** The interface template search. */
	private InterfaceTemplateTO interfaceTemplateSearch;//InterfaceTemplateTO for Search View
	
	/** The interface template list search. */
	private GenericDataModel<InterfaceTemplateTO> interfaceTemplateListSearch;//List of InterfaceTemplateTO for Search View
	
	/** The interface template search selected. */
	private InterfaceTemplateTO interfaceTemplateSearchSelected; //InterfaceTemplateTO selected in Search View
	
	/** The external interface template. */
	private ExternalInterfaceTemplate externalInterfaceTemplate;//Entity for Register,Modify View
	
	/** The details template type. */
	private List<ParameterTable> detailsTemplateType;//List of Interface Template elements types
	
	/** The state template type. */
	private List<ParameterTable> stateTemplateType;
	
	/** The detail template type selected. */
	private ParameterTable detailTemplateTypeSelected;
	
	/** The number detail template type. */
	private Integer numberOfDetails;
			
	/**
	 * Instantiates a new interface template mgmt.
	 */
	public InterfaceTemplateMgmt() {
		// TODO Auto-generated constructor stub
	}
		
	/**
	 * Inits the.
	 *
	 */
	@PostConstruct
	public void init() {
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		resetInterfaceTemplate();//limpia formularios
		loadUserSessionPrivileges();//carga privilegios de usuario para botones
		loadDetailsTemplateType();//carga tipos de detalles de plantilla	
	}
		
	/**
	 * Reset interface template.
	 */
	public void resetInterfaceTemplate(){
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())){
			interfaceTemplateSearch = new InterfaceTemplateTO();
			interfaceTemplateListSearch = null;
			interfaceTemplateSearchSelected =  null;
		}
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
			externalInterfaceTemplate = new ExternalInterfaceTemplate();
			externalInterfaceTemplate.setRegisterDate(CommonsUtilities.currentDate());
			externalInterfaceTemplate.setExtInterfaceTemplateDetails(new ArrayList<ExtInterfaceTemplateDetail>());
			detailTemplateTypeSelected = new ParameterTable();
			loadDetailsTemplateType();
		}
	}	
	
	/**
	 * Load user session privileges.
	 */
	private void loadUserSessionPrivileges(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load details template type.
	 */
	private void loadDetailsTemplateType() {
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		try {
			parameterTableTO.setMasterTableFk(MasterTableType.DETAIL_TEMPLATE_TYPE.getCode());
			detailsTemplateType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
			parameterTableTO.setMasterTableFk(MasterTableType.INTERFACE_TEMPLATE_STATE_TYPE.getCode());
			stateTemplateType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
		
	/* HANDLER ACTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	
	/**
	 * Interface template consult handler.
	 */
	public void interfaceTemplateConsultHandler(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Interface template detail handler.
	 *
	 * @param id the id
	 * @return the string
	 */
	public String interfaceTemplateDetailHandler(Long id){
		try {				
			externalInterfaceTemplate = interfaceTemplateServiceFacade.getExternalInterfaceTemplate(id);
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			return "interfaceTemplateMgmtRule";
		} catch (ServiceException e) {
			showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
		}
		return "";
	}
	
	/**
	 * Interface template register handler.
	 *
	 * @return the string
	 */
	public String interfaceTemplateRegisterHandler(){
		setViewOperationType(ViewOperationsType.REGISTER.getCode());	
		resetInterfaceTemplate();		
		return "interfaceTemplateMgmtRule";
	}
	
	/**
	 * Interface template modify handler.
	 *
	 * @return the string
	 */
	public String interfaceTemplateModifyHandler(){
		if(interfaceTemplateSearchSelected != null){
			try {				
				externalInterfaceTemplate = interfaceTemplateServiceFacade.getExternalInterfaceTemplate(interfaceTemplateSearchSelected.getId(),InterfaceTemplateStateType.REGISTERED);
				setViewOperationType(ViewOperationsType.MODIFY.getCode());
				detailTemplateTypeSelected = new ParameterTable();
				numberOfDetails = 0;				
				return "interfaceTemplateMgmtRule";
			} catch (ServiceException e) {
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}		
		return "";
	}
	
	/**
	 * Interface template comfirm handler.
	 *
	 * @return the string
	 */
	public String interfaceTemplateComfirmHandler(){
		if(interfaceTemplateSearchSelected != null){
			try {				
				externalInterfaceTemplate = interfaceTemplateServiceFacade.getExternalInterfaceTemplate(interfaceTemplateSearchSelected.getId(),InterfaceTemplateStateType.CONFIRMED);
				setViewOperationType(ViewOperationsType.CONFIRM.getCode());	
				return "interfaceTemplateMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/**
	 * Interface template annular handler.
	 *
	 * @return the string
	 */
	public String interfaceTemplateAnnularHandler(){
		if(interfaceTemplateSearchSelected != null){
			try {				
				externalInterfaceTemplate = interfaceTemplateServiceFacade.getExternalInterfaceTemplate(interfaceTemplateSearchSelected.getId(),InterfaceTemplateStateType.ANNULLED);
				setViewOperationType(ViewOperationsType.ANULATE.getCode());	
				return "interfaceTemplateMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/**
	 * Interface template block handler.
	 *
	 * @return the string
	 */
	public String interfaceTemplateBlockHandler(){
		if(interfaceTemplateSearchSelected != null){
			try {				
				externalInterfaceTemplate = interfaceTemplateServiceFacade.getExternalInterfaceTemplate(interfaceTemplateSearchSelected.getId(),InterfaceTemplateStateType.BLOCKED);
				setViewOperationType(ViewOperationsType.BLOCK.getCode());
				return "interfaceTemplateMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/**
	 * Interface template unblock handler.
	 *
	 * @return the string
	 */
	public String interfaceTemplateUnblockHandler(){
		if(interfaceTemplateSearchSelected != null){
			try {				
				externalInterfaceTemplate = interfaceTemplateServiceFacade.getExternalInterfaceTemplate(interfaceTemplateSearchSelected.getId(),InterfaceTemplateStateType.CONFIRMED);
				setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
				return "interfaceTemplateMgmtRule";
			} catch (ServiceException e) {
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
				}
			}
		}		
		return "";
	}
	
	/* BEFORE FORM ACTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
			
	/**
	 * Before save interface template.
	 */
	public void beforeSaveInterfaceTemplate(){
		if(externalInterfaceTemplate.getExtInterfaceTemplateDetails().size() == 0){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_VALIDATION_STRUCTURE);
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			return;
		}
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_REGISTER_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmSaveInterfaceTemplate').show();");
	}
	
	/**
	 * Before update interface template.
	 */
	public void beforeModifyInterfaceTemplate(){
		if(externalInterfaceTemplate.getExtInterfaceTemplateDetails().size() == 0){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ALERT, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_VALIDATION_STRUCTURE);
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			return;
		}
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_MODIFY_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmModifyInterfaceTemplate').show();");
	}
	
	/**
	 * Before confirm interface template.
	 */
	public void beforeConfirmInterfaceTemplate(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_CONFIRM_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmConfirmInterfaceTemplate').show();");
	}
	
	/**
	 * Before annular interface template.
	 */
	public void beforeAnnularInterfaceTemplate(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_ANNUL, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_ANNULAR_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmAnnularInterfaceTemplate').show();");
	}
	
	/**
	 * Before block interface template.
	 */
	public void beforeBlockInterfaceTemplate(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_BLOCK, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_BLOCK_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmBlockInterfaceTemplate').show();");
	}
		
	/**
	 * Before unblock interface template.
	 */
	public void beforeUnblockInterfaceTemplate(){
		showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_UNBLOCK, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_UNBLOCK_CONFIRM);
		JSFUtilities.executeJavascriptFunction("PF('wrConfirmUnblockInterfaceTemplate').show();");
	}
	
	/* FORM ACTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	
	/**
	 * Search interface template.
	 */
	public void searchInterfaceTemplate(){		
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		List<InterfaceTemplateTO> interfaceTemplateList = null;
		try {
			interfaceTemplateList = interfaceTemplateServiceFacade.getExternalInterfaceTemplateList(interfaceTemplateSearch);			
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
		if(interfaceTemplateList==null){
			interfaceTemplateListSearch = new GenericDataModel<InterfaceTemplateTO>();
		}else{
			interfaceTemplateListSearch = new GenericDataModel<InterfaceTemplateTO>(interfaceTemplateList);
		}
	}
	
	/**
	 * Save interface template.
	 */
	@LoggerAuditWeb
	public void saveInterfaceTemplate(){
		try{
			ExternalInterfaceTemplate externalInterfaceTemplate = this.externalInterfaceTemplate;
			externalInterfaceTemplate.setRegisterUser(userInfo.getUserAccountSession().getUserName());
			externalInterfaceTemplate.setTemplateState(InterfaceTemplateStateType.REGISTERED.getCode());
			
			int orderPriority = 0;
			for(ExtInterfaceTemplateDetail detail : externalInterfaceTemplate.getExtInterfaceTemplateDetails()){				
				detail.setExternalInterfaceTemplate(externalInterfaceTemplate);
				detail.setOrderPriority(orderPriority++);
				detail.setRegisterDate(CommonsUtilities.currentDate());
				detail.setRegisterUser(userInfo.getUserAccountSession().getUserName());
				detail.setTemplateDetailState(InterfaceTemplateDetailStateType.REGISTERED.getCode());
			}
			
			externalInterfaceTemplate = interfaceTemplateServiceFacade.registryExternalInterfaceTemplate(externalInterfaceTemplate);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_REGISTER_CONFIRM_OK,new Object[]{externalInterfaceTemplate.getIdExtInferfaceTemplatePk().toString()});
			JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
			searchInterfaceTemplate();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Update interface template.
	 */
	@LoggerAuditWeb
	public void modifyInterfaceTemplate(){
		try{
			ExternalInterfaceTemplate externalInterfaceTemplate = this.externalInterfaceTemplate;
			
			int orderPriority = 0;
			for(ExtInterfaceTemplateDetail detail : externalInterfaceTemplate.getExtInterfaceTemplateDetails()){
				if(detail.getIdExtInterfaceTempDetPk() == null){
					detail.setExternalInterfaceTemplate(externalInterfaceTemplate);
					detail.setOrderPriority(orderPriority++);
					detail.setRegisterUser(userInfo.getUserAccountSession().getUserName());
					detail.setRegisterDate(CommonsUtilities.currentDate());
					detail.setTemplateDetailState(InterfaceTemplateDetailStateType.REGISTERED.getCode());
				}else{
					detail.setOrderPriority(orderPriority++);
				}								
			}
			
			externalInterfaceTemplate = interfaceTemplateServiceFacade.modifyExternalInterfaceTemplate(externalInterfaceTemplate);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_MODIFY_CONFIRM_OK,new Object[]{externalInterfaceTemplate.getIdExtInferfaceTemplatePk().toString()});
			JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
			searchInterfaceTemplate();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Confirm interface template.
	 */
	@LoggerAuditWeb
	public void confirmInterfaceTemplate(){
		try{
			ExternalInterfaceTemplate externalInterfaceTemplate = this.externalInterfaceTemplate;						
			externalInterfaceTemplate = interfaceTemplateServiceFacade.confirmExternalInterfaceTemplate(externalInterfaceTemplate);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_CONFIRM_CONFIRM_OK,new Object[]{externalInterfaceTemplate.getIdExtInferfaceTemplatePk().toString()});
			JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
			searchInterfaceTemplate();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}

	/**
	 * Annular interface template.
	 */
	@LoggerAuditWeb
	public void annularInterfaceTemplate(){
		try{
			ExternalInterfaceTemplate externalInterfaceTemplate = this.externalInterfaceTemplate;						
			externalInterfaceTemplate = interfaceTemplateServiceFacade.annularExternalInterfaceTemplate(externalInterfaceTemplate);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_ANNULAR_CONFIRM_OK,new Object[]{externalInterfaceTemplate.getIdExtInferfaceTemplatePk().toString()});
			JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
			searchInterfaceTemplate();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Block interface template.
	 */
	@LoggerAuditWeb
	public void blockInterfaceTemplate(){
		try{
			ExternalInterfaceTemplate externalInterfaceTemplate = this.externalInterfaceTemplate;						
			externalInterfaceTemplate = interfaceTemplateServiceFacade.blockExternalInterfaceTemplate(externalInterfaceTemplate);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_BLOCK_CONFIRM_OK,new Object[]{externalInterfaceTemplate.getIdExtInferfaceTemplatePk().toString()});
			JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
			searchInterfaceTemplate();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/**
	 * Unblock interface template.
	 */
	@LoggerAuditWeb
	public void unblockInterfaceTemplate(){
		try{
			ExternalInterfaceTemplate externalInterfaceTemplate = this.externalInterfaceTemplate;						
			externalInterfaceTemplate = interfaceTemplateServiceFacade.unblockExternalInterfaceTemplate(externalInterfaceTemplate);
			
			showMessageOnDialog(PropertiesConstants.DIALOG_HEADER_SUCCESS,null, PropertiesConstants.EXTERNAL_INTERFACE_TEMPLATE_UNBLOCK_CONFIRM_OK,new Object[]{externalInterfaceTemplate.getIdExtInferfaceTemplatePk().toString()});
			JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
			searchInterfaceTemplate();
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationPar').show();");
			}
		}
	}
	
	/* HELPER METHODS */
	
	
	/* METHODS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	
	/**
	 * Adds the to template structure.
	 */
	public void addToTemplateStructure(){
		JUMP:
		for(int i = 0 ; i < numberOfDetails ; i++){
			ExtInterfaceTemplateDetail detail = new ExtInterfaceTemplateDetail();
			detail.setTemplateDetailType(detailTemplateTypeSelected.getParameterTablePk());
			for(ParameterTable pt : detailsTemplateType){
				if(pt.getParameterTablePk().equals(detailTemplateTypeSelected.getParameterTablePk())){					
					detail.setDescription(pt.getParameterName());
					externalInterfaceTemplate.getExtInterfaceTemplateDetails().add(detail);
					continue JUMP;
				}
			}	
		}
		numberOfDetails = 0;
	}
	
	/**
	 * Clean template structure.
	 */
	public void cleanTemplateStructure(){
		if(externalInterfaceTemplate != null){
			externalInterfaceTemplate.setExtInterfaceTemplateDetails(new ArrayList<ExtInterfaceTemplateDetail>());
		}		
	}
	
	/* GETTERS AND SETTERS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	
	/**
	 * Gets the external interface template.
	 *
	 * @return the external interface template
	 */
	public ExternalInterfaceTemplate getExternalInterfaceTemplate() {
		return externalInterfaceTemplate;
	}

	/**
	 * Sets the external interface template.
	 *
	 * @param externalInterfaceTemplate the new external interface template
	 */
	public void setExternalInterfaceTemplate(
			ExternalInterfaceTemplate externalInterfaceTemplate) {
		this.externalInterfaceTemplate = externalInterfaceTemplate;
	}

	/**
	 * Gets the general parameters facade.
	 *
	 * @return the general parameters facade
	 */
	public GeneralParametersFacade getGeneralParametersFacade() {
		return generalParametersFacade;
	}

	/**
	 * Sets the general parameters facade.
	 *
	 * @param generalParametersFacade the new general parameters facade
	 */
	public void setGeneralParametersFacade(
			GeneralParametersFacade generalParametersFacade) {
		this.generalParametersFacade = generalParametersFacade;
	}

	/**
	 * Gets the details template type.
	 *
	 * @return the details template type
	 */
	public List<ParameterTable> getDetailsTemplateType() {
		return detailsTemplateType;
	}

	/**
	 * Sets the details template type.
	 *
	 * @param detailsTemplateType the new details template type
	 */
	public void setDetailsTemplateType(List<ParameterTable> detailsTemplateType) {
		this.detailsTemplateType = detailsTemplateType;
	}

	/**
	 * Gets the detail template type selected.
	 *
	 * @return the detail template type selected
	 */
	public ParameterTable getDetailTemplateTypeSelected() {
		return detailTemplateTypeSelected;
	}

	/**
	 * Sets the detail template type selected.
	 *
	 * @param detailTemplateTypeSelected the new detail template type selected
	 */
	public void setDetailTemplateTypeSelected(
			ParameterTable detailTemplateTypeSelected) {
		this.detailTemplateTypeSelected = detailTemplateTypeSelected;
	}

	/**
	 * Gets the number of details.
	 *
	 * @return the number of details
	 */
	public Integer getNumberOfDetails() {
		return numberOfDetails;
	}

	/**
	 * Sets the number of details.
	 *
	 * @param numberOfDetails the new number of details
	 */
	public void setNumberOfDetails(Integer numberOfDetails) {
		this.numberOfDetails = numberOfDetails;
	}

	/**
	 * Gets the interface template service facade.
	 *
	 * @return the interface template service facade
	 */
	public InterfaceTemplateServiceFacade getInterfaceTemplateServiceFacade() {
		return interfaceTemplateServiceFacade;
	}

	/**
	 * Sets the interface template service facade.
	 *
	 * @param interfaceTemplateServiceFacade the new interface template service facade
	 */
	public void setInterfaceTemplateServiceFacade(
			InterfaceTemplateServiceFacade interfaceTemplateServiceFacade) {
		this.interfaceTemplateServiceFacade = interfaceTemplateServiceFacade;
	}

	/**
	 * Gets the interface template search.
	 *
	 * @return the interface template search
	 */
	public InterfaceTemplateTO getInterfaceTemplateSearch() {
		return interfaceTemplateSearch;
	}

	/**
	 * Sets the interface template search.
	 *
	 * @param interfaceTemplateSearch the new interface template search
	 */
	public void setInterfaceTemplateSearch(
			InterfaceTemplateTO interfaceTemplateSearch) {
		this.interfaceTemplateSearch = interfaceTemplateSearch;
	}

	/**
	 * Gets the interface template list search.
	 *
	 * @return the interface template list search
	 */
	public GenericDataModel<InterfaceTemplateTO> getInterfaceTemplateListSearch() {
		return interfaceTemplateListSearch;
	}

	/**
	 * Sets the interface template list search.
	 *
	 * @param interfaceTemplateListSearch the new interface template list search
	 */
	public void setInterfaceTemplateListSearch(
			GenericDataModel<InterfaceTemplateTO> interfaceTemplateListSearch) {
		this.interfaceTemplateListSearch = interfaceTemplateListSearch;
	}

	/**
	 * Gets the interface template search selected.
	 *
	 * @return the interface template search selected
	 */
	public InterfaceTemplateTO getInterfaceTemplateSearchSelected() {
		return interfaceTemplateSearchSelected;
	}

	/**
	 * Sets the interface template search selected.
	 *
	 * @param interfaceTemplateSearchSelected the new interface template search selected
	 */
	public void setInterfaceTemplateSearchSelected(
			InterfaceTemplateTO interfaceTemplateSearchSelected) {
		this.interfaceTemplateSearchSelected = interfaceTemplateSearchSelected;
	}

	/**
	 * Gets the state template type.
	 *
	 * @return the state template type
	 */
	public List<ParameterTable> getStateTemplateType() {
		return stateTemplateType;
	}

	/**
	 * Sets the state template type.
	 *
	 * @param stateTemplateType the new state template type
	 */
	public void setStateTemplateType(List<ParameterTable> stateTemplateType) {
		this.stateTemplateType = stateTemplateType;
	}
	
	
}

