package com.pradera.generalparameters.externalinterface.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.UserAdmin;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.generalparameters.externalinterface.utils.ExtInterfaceConstanst;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceExecutionType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceStateType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SchedulerServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/06/2013
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class InterfaceSchedulerService extends CrudDaoServiceBean{

   /** The timer service. */
   @Resource
   TimerService timerService;
   
   /** The dao service. */
   @EJB
   CrudDaoServiceBean daoService;
   
   /** The log. */
   @Inject
   PraderaLogger log;
   
   /** The user admin. */
   @Inject @UserAdmin
   String userAdmin;
   
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
   
   /**
    * Reload timer saved.
    */
   public void loadExternalInterfaceTimmer(){
       try {
	       List<ExternalInterface> listInterface = getInterfaceScheduler();
	       for(ExternalInterface externalInterface: listInterface){
	           //only register timer for not persistence timer
               registerTimer(externalInterface);
               log.info("Register External Interface Timer "+externalInterface.getIdExtInterfacePk());
	       }
       } catch(ServiceException sex){
           log.info("error loading timers "+sex.getMessage());
       }
   }
   
   /**
    * Stop timers.
    */
   public void stopTimers(){
		 for (Timer timer : timerService.getTimers()) {
			 log.info("Stop External Interface Timer: " + timer.getInfo());
	            timer.cancel();
	     }
	}
   
   /**
    * Excute batch remote.
    *
    * @param timer the timer
    */
   @Timeout
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
   public void excuteBatchRemote(Timer timer){
		log.info("Running timer :" + timer.getInfo());
		// get scheduler
		Long idInterface = (Long) timer.getInfo();

		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(ExtInterfaceConstanst.INTERFACE_PK,idInterface.toString());
		BusinessProcess businessProcess = this.find(BusinessProcess.class,BusinessProcessType.EXTERNAL_INTERFACE_AUTOMATIC.getCode());
		String moduleName = businessProcess.getModuleName();

		Schedulebatch scheduleBatch = new Schedulebatch();
		scheduleBatch.setIdBusinessProcess(businessProcess.getIdBusinessProcessPk());
		scheduleBatch.setUserName(userAdmin);
		scheduleBatch.setPrivilegeCode(1);
		scheduleBatch.setParameters(parameters);
		log.info("Invoking batch process on Scheduler:(paramateres) {pk["+ idInterface + "]}");
		clientRestService.excuteRemoteBatchProcess(scheduleBatch, moduleName);
		log.info("finish register remote batch");
   }
   /**
    * Update interface schedule.
    *
    * @param externalInterface the external interface
    * @return the external interface
    * @throws ServiceException the service exception
    */
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
   public ExternalInterface updateInterfaceSchedule(ExternalInterface externalInterface) throws ServiceException{
	   List<Integer> states = Arrays.asList(ExternalInterfaceExecutionType.AUTOMATIC.getCode(),ExternalInterfaceExecutionType.BOTH.getCode());
	   if(states.contains(externalInterface.getExecutionType())){
		   deleteTimer(externalInterface.getIdExtInterfacePk());//detele timer, to be added in a new TimeOut
		   registerTimer(externalInterface);
		   log.info("Updating, external Interface Timmer:(parameters) {pk["+externalInterface.getIdExtInterfacePk().toString()+"]}");
	   }
	   return externalInterface;
   }
   
   /**
    * Register timer.
    *
    * @param externalInterface the external interface
    */
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
   public void registerTimer(ExternalInterface externalInterface){
        TimerConfig timerConfig = new TimerConfig(externalInterface.getIdExtInterfacePk(),Boolean.FALSE);
		Date dateWithHour = externalInterface.getSendPeriocity();
		if(Validations.validateIsNotNullAndNotEmpty(dateWithHour)){
			ScheduleExpression schedExp = new ScheduleExpression();
			schedExp.second(dateWithHour.getSeconds());
			schedExp.minute(dateWithHour.getMinutes());
			schedExp.hour(dateWithHour.getHours());
			log.info("### Scheduler expr -> External interface: "+ schedExp.toString());
			timerService.createCalendarTimer(schedExp, timerConfig);
		}		
   }
   
   /**
    * Gets the timer.
    *
    * @param idTimer the id timer
    * @return the timer
    */
   private Timer getTimer(Long idTimer){
       Collection<Timer> timers = timerService.getTimers();
       for (Timer t : timers){
           if(idTimer.equals(((Long)t.getInfo()))){
               return t;
           }
       }
       return null;
   }
   
   /**
    * Delete timer.
    *
    * @param idTimer the id timer
    */
   public void deleteTimer(Long idTimer){
       Timer timer = getTimer(idTimer);
       if(timer != null){
           timer.cancel();
       }
   }
   
   /**
    * Gets the interface scheduler.
    *
    * @return the interface scheduler
    * @throws ServiceException the service exception
    */
   @SuppressWarnings("unchecked")
   public List<ExternalInterface> getInterfaceScheduler() throws ServiceException{
	   Query query = em.createNamedQuery(ExternalInterface.INTERFACE_GET);
	   query.setParameter("intState", ExternalInterfaceStateType.CONFIRMED.getCode());
	   query.setParameter("executionValues", Arrays.asList(ExternalInterfaceExecutionType.AUTOMATIC.getCode(),ExternalInterfaceExecutionType.BOTH.getCode()));
	   List<ExternalInterface> interfaceList = null;
	   
	   try{
		   interfaceList = query.getResultList();
	   }catch(NoResultException ex){
		   return new ArrayList<ExternalInterface>();
	   }
	   
	   for(ExternalInterface externalInterface: interfaceList){
		   Timer timer = getTimer(externalInterface.getIdExtInterfacePk());
           if(timer != null){
        	   externalInterface.setSendPeriocity(timer.getNextTimeout());
           }
	   }
	   return interfaceList;
   }
}