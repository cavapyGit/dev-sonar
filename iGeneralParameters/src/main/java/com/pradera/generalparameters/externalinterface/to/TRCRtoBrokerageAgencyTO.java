package com.pradera.generalparameters.externalinterface.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.generalparameters.externalinterface.type.SearchTRCRType;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

public class TRCRtoBrokerageAgencyTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idParticipantPk;
	private List<Participant> lstParticipant=new ArrayList<>();
	
	private Integer searchType;
	private List<SearchTRCRType> lstSearchType=new ArrayList<>();;
	
	// filtros para bUsqueda: CARTERA
	private Date cutDate;
	//private Long idHolderPk;
	private Holder holder=new Holder();
	
	//private Long accountNumber;
	private HolderAccount holderAccount=new HolderAccount();
	
	// filtros para bUsqueda: NEGOCIACION
	private Date negotiationDate;
	
	// filtros para bUsqueda: TRASPASO
	private Date transferDate;

	// filtros para bUsqueda: ACV
	private Date acvDate;

	// filtros para bUsqueda: SERIADOS
	private Date emisionDate;
	
	// filtros para TODOS los tipos de busqueda
	private String idIssuerPk;
	private List<Issuer> lstIssuer=new ArrayList<>();;
	
	private Integer currency;
	private List<ParameterTable> lstCurrency=new ArrayList<>();;
	
	private Integer securityClass;
	private List<ParameterTable> lstSecurityClass=new ArrayList<>();;
	
	private String idSecurityCodePk;
	
	private Integer isNegotiable;
	private List<BooleanType> lstBooleanType = BooleanType.list;
	
	private String alternateCode;
	
	// campos para el listado
	private String currencyCodeText;
	private BigDecimal currentNominalValue;
	private Date issuanceDate;
	private String isNegotiableText;
	private String stateSecurityText;
	private boolean isSelectedRow;
	
	// 1er constructor
	public TRCRtoBrokerageAgencyTO() {
		this.holder = new Holder();
		this.holderAccount = new HolderAccount();
		// todos por defecto a fecha actual
		this.cutDate = CommonsUtilities.currentDate();
		this.negotiationDate = CommonsUtilities.currentDate();
		this.transferDate = CommonsUtilities.currentDate();
		this.acvDate = CommonsUtilities.currentDate();
		this.emisionDate = CommonsUtilities.currentDate();
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	public Integer getSearchType() {
		return searchType;
	}

	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}

	public List<SearchTRCRType> getLstSearchType() {
		return lstSearchType;
	}

	public void setLstSearchType(List<SearchTRCRType> lstSearchType) {
		this.lstSearchType = lstSearchType;
	}

	public Date getCutDate() {
		return cutDate;
	}

	public void setCutDate(Date cutDate) {
		this.cutDate = cutDate;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Date getNegotiationDate() {
		return negotiationDate;
	}

	public void setNegotiationDate(Date negotiationDate) {
		this.negotiationDate = negotiationDate;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public Date getAcvDate() {
		return acvDate;
	}

	public void setAcvDate(Date acvDate) {
		this.acvDate = acvDate;
	}

	public Date getEmisionDate() {
		return emisionDate;
	}

	public void setEmisionDate(Date emisionDate) {
		this.emisionDate = emisionDate;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}

	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public Integer getIsNegotiable() {
		return isNegotiable;
	}

	public void setIsNegotiable(Integer isNegotiable) {
		this.isNegotiable = isNegotiable;
	}

	public List<BooleanType> getLstBooleanType() {
		return lstBooleanType;
	}

	public void setLstBooleanType(List<BooleanType> lstBooleanType) {
		this.lstBooleanType = lstBooleanType;
	}

	public String getAlternateCode() {
		return alternateCode;
	}

	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	public String getCurrencyCodeText() {
		return currencyCodeText;
	}

	public void setCurrencyCodeText(String currencyCodeText) {
		this.currencyCodeText = currencyCodeText;
	}

	public BigDecimal getCurrentNominalValue() {
		return currentNominalValue;
	}

	public void setCurrentNominalValue(BigDecimal currentNominalValue) {
		this.currentNominalValue = currentNominalValue;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public String getIsNegotiableText() {
		return isNegotiableText;
	}

	public void setIsNegotiableText(String isNegotiableText) {
		this.isNegotiableText = isNegotiableText;
	}

	public String getStateSecurityText() {
		return stateSecurityText;
	}

	public void setStateSecurityText(String stateSecurityText) {
		this.stateSecurityText = stateSecurityText;
	}

	public boolean isSelectedRow() {
		return isSelectedRow;
	}

	public void setSelectedRow(boolean isSelectedRow) {
		this.isSelectedRow = isSelectedRow;
	}
	
	
	
}
