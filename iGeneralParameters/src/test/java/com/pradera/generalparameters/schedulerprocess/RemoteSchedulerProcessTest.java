/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.generalparameters.schedulerprocess;

import java.io.File;
import java.util.HashMap;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.generalparameters.schedulerprocess.facade.SchedulerServiceFacade;
import com.pradera.generalparameters.schedulerprocess.service.SchedulerServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessSchedule;

/**
 *
 * @author raul
 */
//@RunWith(Arquillian.class)
public class RemoteSchedulerProcessTest {

    @Inject
    PraderaLogger log;
    @EJB
    SchedulerServiceFacade schedulerFacade;
    
    @Inject 
    ClientRestService clientRestService;

    @Deployment
    public static WebArchive createTestArchive() {
        File[] libs = Maven.resolver()
                .loadPomFromFile("pom.xml")
                .importRuntimeDependencies()
                .asFile();
        WebArchive war =
                ShrinkWrap.create(WebArchive.class, "PraderaParameters.war")
                .addAsLibraries(libs)
                .addPackages(true, SchedulerServiceFacade.class.getPackage())
                .addPackages(true, SchedulerServiceBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        System.out.println(war.toString((Formatters.VERBOSE)));
        return war;
    }

//    @Test
    public void checkCallRemoteBatch() throws ServiceException{
        LoggerUser loggerUser = new LoggerUser();
        loggerUser.setIpAddress("192.168.1.56");
        loggerUser.setUserName("rcaso");
        loggerUser.setIdUserSessionPk(1l);
        loggerUser.setSessionId("sessssxkxkdd");
        loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
        //Praderafilter in each request get current UserAcction
        UserAcctions userAcctions = new UserAcctions();
        userAcctions.setIdPrivilegeAdd(1);
        userAcctions.setIdPrivilegeRegister(1);
        loggerUser.setUserAction(userAcctions);
        loggerUser.setIdPrivilegeOfSystem(1);
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        ProcessSchedule schedule = new ProcessSchedule();
        BusinessProcess business = new BusinessProcess();
        business.setIdBusinessProcessPk(BusinessProcessType.STOCK_CALCULATION_MANUAL_EXECUTION.getCode());
        schedule.setBusinessProcess(business);
        schedule.setScheduleState(1);
        schedule.setScheduleDescription("Ejecutarse cada 15 segundos");
        schedule.setRegistryDate(CommonsUtilities.currentDateTime());
        schedule.setRegistryUser("rcaso");
        schedule.getScheduleInfo().setScheduleSecond("0");
        schedule.getScheduleInfo().setScheduleMinute("5/2");
        schedule.getScheduleInfo().setScheduleHour("18");
        schedule.getScheduleInfo().setIndPersistence(BooleanType.NO.getCode());
//        schedulerFacade.registerScheduler(schedule);
        log.info("registrado y esperando llamadas remotas");
    }
    
//    @Test
    public void interceptorLoggerUserBatchTest(){
    	Schedulebatch scheduleBatch = new Schedulebatch();
        scheduleBatch.setIdBusinessProcess(BusinessProcessType.STOCK_CALCULATION_MANUAL_EXECUTION.getCode());
        scheduleBatch.setUserName("rcaso");
        scheduleBatch.setPrivilegeCode(1);
        scheduleBatch.setParameters(new HashMap<String, String>());
        scheduleBatch.getParameters().put("usuario", "delfin");
        scheduleBatch.getParameters().put("status", "delfinExceptions");
    	clientRestService.excuteRemoteBatchProcess(scheduleBatch,"PraderaCorporateEvents");
    	log.info("remote batch fire");
    }
}
