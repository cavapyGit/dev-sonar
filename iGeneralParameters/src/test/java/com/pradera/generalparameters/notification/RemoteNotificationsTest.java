package com.pradera.generalparameters.notification;

import java.io.File;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.process.BusinessProcess;

//@RunWith(Arquillian.class)
public class RemoteNotificationsTest {

	 @Inject
	 PraderaLogger log;
	 
	 @Inject 
	 ClientRestService clientRestService;
	 
	 @EJB
	 NotificationServiceBean notificationService;
	 
	 
	 @Deployment
	    public static WebArchive createTestArchive() {
	        File[] libs = Maven.resolver()
	                .loadPomFromFile("pom.xml")
	                .importRuntimeDependencies()
	                .asFile();
	        WebArchive war =
	                ShrinkWrap.create(WebArchive.class, "PraderaParameters.war")
	                .addAsLibraries(libs)
	                .addPackages(true, CrudDaoServiceBean.class.getPackage())
	                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	        System.out.println(war.toString((Formatters.VERBOSE)));
	        return war;
	    }
	 
	 
	//@Test
	public void getUserRemotes() {
		List<String> users =clientRestService.getUsersOrEmails(null, true, InstitutionType.PARTICIPANT, Long.valueOf(62), null);
		if(users!= null){
			for(String user: users){
				log.info(user);
			}
		}
	}
	
//	@Test
	public void registerNotification(){
		 LoggerUser loggerUser = new LoggerUser();
	        loggerUser.setIpAddress("192.168.1.56");
	        loggerUser.setUserName("ADMIN");
	        loggerUser.setIdUserSessionPk(1l);
	        loggerUser.setSessionId("sessssxkxkdd");
	        loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
	        //Praderafilter in each request get current UserAcction
	        UserAcctions userAcctions = new UserAcctions();
	        userAcctions.setIdPrivilegeAdd(1);
	        userAcctions.setIdPrivilegeRegister(1);
	        loggerUser.setUserAction(userAcctions);
	        loggerUser.setIdPrivilegeOfSystem(1);
	        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		BusinessProcess process = new BusinessProcess();
		process.setIdBusinessProcessPk(BusinessProcessType.STOCK_CALCULATION_MANUAL_EXECUTION.getCode());
//		monitorNotificationService.registerNotification("ADMIN", process, null);
		log.info("notificacion debio ser enviada");
	}
	
//	@Test
	public void testNotificationsPopup(){
		for(int i = 0;i<3;i++){
			BrowserWindow browser = new BrowserWindow();
			browser.setUserChannel("ADMIN");
			browser.setNotificationType(NotificationType.MESSAGE.getCode());
			browser.setSubject("Notificación");
			browser.setMessage("CACULO");
			clientRestService.notifyEventBrowserWindow(browser);
			//process
			browser.setNotificationType(NotificationType.PROCESS.getCode());
			clientRestService.notifyEventBrowserWindow(browser);
			log.info("noti enviado");
		}
		
	}
	
}
