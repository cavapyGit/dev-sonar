package com.pradera.settlements.notificator;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Named;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class SettlementNotificatorListenerBean.
 */
@ApplicationScoped
@Named
public class SettlementNotificatorListenerBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
		
	/** The settlement notification track. */
	private SettlementNotificationTrack settlementNotificationTrack;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			settlementNotificationTrack = new SettlementNotificationTrack();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Receive settlement event.
	 *
	 * @param notificationEvent the notification event
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void receiveSettlementEvent(@Observes SettlementNotificationEvent notificationEvent) throws ServiceException {
		settlementNotificationTrack = getSettlementEventTrack(settlementNotificationTrack, notificationEvent);
	}
	
	/**
	 * Gets the settlement event track.
	 *
	 * @param notificationTrack the notification track
	 * @param notificationEvent the notification event
	 * @return the settlement event track
	 * @throws ServiceException the service exception
	 */
	public SettlementNotificationTrack getSettlementEventTrack(SettlementNotificationTrack notificationTrack, 
			SettlementNotificationEvent notificationEvent) throws ServiceException {
		Long rowQuantity = notificationEvent.getRowQuantity();
		Integer percentage = notificationEvent.getPercentage();
		notificationTrack = new SettlementNotificationTrack();
		notificationTrack.setBeginTime(CommonsUtilities.currentDateTime());	
		notificationTrack.setIndFinish(notificationEvent.getIndFinish());
		notificationTrack.setIndHasError(notificationEvent.getIndHasError());
		notificationTrack.setRowQuantity(rowQuantity);
		notificationTrack.setInExecution(notificationEvent.getInExecution());
		if(percentage == null) {
			notificationTrack.setPercentage(ComponentConstant.ZERO);
		} else {
			notificationTrack.setPercentage(percentage);
		}								
		return notificationTrack;
	}

	/**
	 * Gets the settlement notification track.
	 *
	 * @return the settlementNotificationTrack
	 */
	public SettlementNotificationTrack getSettlementNotificationTrack() {
		return settlementNotificationTrack;
	}

	/**
	 * Sets the settlement notification track.
	 *
	 * @param settlementNotificationTrack the settlementNotificationTrack to set
	 */
	public void setSettlementNotificationTrack(
			SettlementNotificationTrack settlementNotificationTrack) {
		this.settlementNotificationTrack = settlementNotificationTrack;
	}

}
