package com.pradera.settlements.notificator;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class SettlementNotificationEvent.
 */
public class SettlementNotificationEvent implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;		
	
	/** The indicator begin. */
	private Integer indBegin;
	
	/** The indicator finish. */
	private Integer indFinish;
	
	/** The begin time. */
	private Date beginTime;
	
	/** The finish time. */
	private Date finishTime;
	
	/** The indicator has error. */
	private Integer indHasError;
	
	/** The row quantity. */
	private Long rowQuantity;
	
	/** The percentage. */
	private Integer percentage;
	
	/** The in execution. */
	private Integer inExecution;
	
	/**
	 * Gets the indicator begin.
	 *
	 * @return the indBegin
	 */
	public Integer getIndBegin() {
		return indBegin;
	}
	
	/**
	 * Sets the indicator begin.
	 *
	 * @param indBegin the indBegin to set
	 */
	public void setIndBegin(Integer indBegin) {
		this.indBegin = indBegin;
	}
	
	/**
	 * Gets the indicator finish.
	 *
	 * @return the indFinish
	 */
	public Integer getIndFinish() {
		return indFinish;
	}
	
	/**
	 * Sets the indicator finish.
	 *
	 * @param indFinish the indFinish to set
	 */
	public void setIndFinish(Integer indFinish) {
		this.indFinish = indFinish;
	}
	
	/**
	 * Gets the begin time.
	 *
	 * @return the beginTime
	 */
	public Date getBeginTime() {
		return beginTime;
	}
	
	/**
	 * Sets the begin time.
	 *
	 * @param beginTime the beginTime to set
	 */
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	
	/**
	 * Gets the finish time.
	 *
	 * @return the finishTime
	 */
	public Date getFinishTime() {
		return finishTime;
	}
	
	/**
	 * Sets the finish time.
	 *
	 * @param finishTime the finishTime to set
	 */
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	
	/**
	 * Gets the indicator has error.
	 *
	 * @return the indHasError
	 */
	public Integer getIndHasError() {
		return indHasError;
	}
	
	/**
	 * Sets the indicator has error.
	 *
	 * @param indHasError the indHasError to set
	 */
	public void setIndHasError(Integer indHasError) {
		this.indHasError = indHasError;
	}
	
	/**
	 * Gets the row quantity.
	 *
	 * @return the rowQuantity
	 */
	public Long getRowQuantity() {
		return rowQuantity;
	}
	
	/**
	 * Sets the row quantity.
	 *
	 * @param rowQuantity the rowQuantity to set
	 */
	public void setRowQuantity(Long rowQuantity) {
		this.rowQuantity = rowQuantity;
	}
	
	/**
	 * Gets the percentage.
	 *
	 * @return the percentage
	 */
	public Integer getPercentage() {
		return percentage;
	}
	
	/**
	 * Sets the percentage.
	 *
	 * @param percentage the percentage to set
	 */
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	/**
	 * Gets the in execution.
	 *
	 * @return the inExecution
	 */
	public Integer getInExecution() {
		return inExecution;
	}

	/**
	 * Sets the in execution.
	 *
	 * @param inExecution the inExecution to set
	 */
	public void setInExecution(Integer inExecution) {
		this.inExecution = inExecution;
	}
	
	

}
