package com.pradera.settlements.reports.facade;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.service.SettlementReportService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementReportFacade.
 *
 * @author Pradera Technologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SettlementReportFacade {
	
	/** The settlement report service. */
	@EJB
	private SettlementReportService settlementReportService;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Send Reports .
	 *
	 * @param lstSettlementReportTO the lst settlement report to
	 * @param idReportPk the id report pk
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void sendReports(List<SettlementReportTO> lstSettlementReportTO, Long idReportPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		settlementReportService.sendReports(lstSettlementReportTO, idReportPk, loggerUser);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> detailExpirationRepurchaseQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.detailExpirationRepurchaseQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> purchaseForceExpirationSettledQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.purchaseForceExpirationSettledQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> settledOperationsQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.settledOperationsQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> settledAdvanceOperationsQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.settledAdvanceOperationsQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> netSettlementPositionsPreQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.netSettlementPositionsPreQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> prepaidSettlementQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.prepaidSettlementQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> settlementTypeExchangeQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.settlementTypeExchangeQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> operDetailWithSecPendingDelivQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.operDetailWithSecPendingDelivQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> unfulfilledOperationsQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.unfulfilledOperationsQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> detailSettledOperationsQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.detailSettledOperationsQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> exlackSecurOperationsFundQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.exlackSecurOperationsFundQuery(filter);
	}
	
	
	/**
	 * Net settlement positions settlement query.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> netSettlementPositionsSettlementQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.netSettlementPositionsSettlementQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> netSettlementPositionsRetirementQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.netSettlementPositionsRetirementQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> edvComissionSettlementOperQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.edvComissionSettlementOperQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> grossSchemeSettlementQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.grossSchemeSettlementQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> netPositionsOperationsBagPreQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.netPositionsOperationsBagPreQuery(filter);
	}
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementReportTO> netPositionsOperationsDefExRegQuery(SettlementReportTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return settlementReportService.netPositionsOperationsDefExRegQuery(filter);
	}
}

