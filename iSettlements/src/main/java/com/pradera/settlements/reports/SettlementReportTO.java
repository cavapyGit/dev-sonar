package com.pradera.settlements.reports;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementReportTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementReportTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The currency. */
	private String currency;
	
	/** The participant. */
	private String participant;
	
	/** The modality group. */
	private String modalityGroup;
	
	/** The schedule type. */
	private String scheduleType;
	
	/** The initial date. */
	private String initialDate;
	
	/** The end date. */
	private String endDate;
	
	/** The date. */
	private String date;
	
	/** The securities. */
	private String securities;
	
	/** The schema. */
	private String schema;
	
	/** The ind extended. */
	private String indExtended;
	
	/** The only user. */
	private boolean onlyUser;
	
	/** The all user. */
	private boolean allUser;
	
	/**
	 * Instantiates a new settlement report to.
	 */
	public SettlementReportTO() {
		super();
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public String getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the participant to set
	 */
	public void setParticipant(String participant) {
		this.participant = participant;
	}

	/**
	 * Gets the modality group.
	 *
	 * @return the modalityGroup
	 */
	public String getModalityGroup() {
		return modalityGroup;
	}

	/**
	 * Sets the modality group.
	 *
	 * @param modalityGroup the modalityGroup to set
	 */
	public void setModalityGroup(String modalityGroup) {
		this.modalityGroup = modalityGroup;
	}

	/**
	 * Gets the schedule type.
	 *
	 * @return the scheduleType
	 */
	public String getScheduleType() {
		return scheduleType;
	}

	/**
	 * Sets the schedule type.
	 *
	 * @param scheduleType the scheduleType to set
	 */
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public String getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public String getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the securities to set
	 */
	public void setSecurities(String securities) {
		this.securities = securities;
	}

	/**
	 * Gets the schema.
	 *
	 * @return the schema
	 */
	public String getSchema() {
		return schema;
	}

	/**
	 * Sets the schema.
	 *
	 * @param schema the schema to set
	 */
	public void setSchema(String schema) {
		this.schema = schema;
	}

	/**
	 * Gets the ind extended.
	 *
	 * @return the indExtended
	 */
	public String getIndExtended() {
		return indExtended;
	}

	/**
	 * Sets the ind extended.
	 *
	 * @param indExtended the indExtended to set
	 */
	public void setIndExtended(String indExtended) {
		this.indExtended = indExtended;
	}

	/**
	 * Checks if is only user.
	 *
	 * @return the onlyUser
	 */
	public boolean isOnlyUser() {
		return onlyUser;
	}

	/**
	 * Sets the only user.
	 *
	 * @param onlyUser the onlyUser to set
	 */
	public void setOnlyUser(boolean onlyUser) {
		this.onlyUser = onlyUser;
	}

	/**
	 * @return the allUser
	 */
	public boolean isAllUser() {
		return allUser;
	}

	/**
	 * @param allUser the allUser to set
	 */
	public void setAllUser(boolean allUser) {
		this.allUser = allUser;
	}
	
}