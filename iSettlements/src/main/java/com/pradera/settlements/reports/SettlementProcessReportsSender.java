package com.pradera.settlements.reports;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.settlements.reports.service.SettlementReportService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementProcessReportsSender.
 *
 * @author PraderaTechnologies
 */
@Stateless
public class SettlementProcessReportsSender implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The report service. */
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/** The settlement report service. */
	@EJB
	private SettlementReportService settlementReportService;
	
	/**
	 * Send reports.
	 *
	 * @param lstSettlementReportTO the lst settlement report to
	 * @param idReport the id report
	 * @param lgUser the lg user
	 * @throws ServiceException the service exception
	 */
	public void sendReports(List<SettlementReportTO> lstSettlementReportTO, Long idReport, LoggerUser lgUser) throws ServiceException{
		if(reportService != null){
			Map<String,String> parameters = new HashMap<String, String>();
			Map<String,String> parameterDetails = new HashMap<String, String>();
			ReportUser user;
			for(SettlementReportTO settlementReportTO : lstSettlementReportTO){
				user = new ReportUser();
				parameters = new HashMap<String, String>();	
				if(settlementReportTO.isAllUser()){
					user.setUserName(lgUser.getUserName());
					if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())){
						user.setPartcipantCode(new Long(settlementReportTO.getParticipant()));
					}
				} else {
					if(settlementReportTO.isOnlyUser()){
						user.setUserName(lgUser.getUserName());
					} else {
						if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())){
							user.setPartcipantCode(new Long(settlementReportTO.getParticipant()));
						}					
					}
				}								
				parameters = asingmentParameters(settlementReportTO, idReport);
				parameterDetails = asingmentParametersDetail(settlementReportTO, idReport);
				reportService.get().saveReportExecution(idReport, parameters, parameterDetails, user, lgUser);
			}
		}
	}
	
	/**
	 * Asingment parameters.
	 *
	 * @param settlementReportTO the settlement report to
	 * @param idReport the id report
	 * @return the map
	 */
	public Map<String,String> asingmentParameters(SettlementReportTO settlementReportTO,Long  idReport){
		Map<String,String> parameters = new HashMap<String, String>();
		//INICIO DE DIA
		if(ReportIdType.DETAIL_EXPIRATION_REPURCHASE.getCode().equals(idReport)){
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("date", settlementReportTO.getDate());
			parameters.put("currency", settlementReportTO.getCurrency());
		}else if(ReportIdType.PURCHASE_FORCE_EXPIRATION_SETTLED.getCode().equals(idReport)){
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("date_initial", settlementReportTO.getInitialDate());
			parameters.put("date_end", settlementReportTO.getEndDate());
		}
		//CIERRE DE DIA
		else if(ReportIdType.SETTLED_OPERATIONS.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())){
				parameters.put("participant", settlementReportTO.getParticipant());
			}			
			parameters.put("schema", settlementReportTO.getSchema());
			parameters.put("date_initial", settlementReportTO.getInitialDate());
			parameters.put("date_end", settlementReportTO.getEndDate());
		}else if(ReportIdType.SETTLED_ADVANCE_OPERATIONS.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())){
				parameters.put("participant", settlementReportTO.getParticipant());
			}			
			parameters.put("date_initial", settlementReportTO.getInitialDate());
			parameters.put("date_end", settlementReportTO.getEndDate());
		}
				
		//CIERRE DE ASIGNACION
		else if(ReportIdType.OPERATIONS_DETAIL_WITH_PENDING_DELIV.getCode().equals(idReport)){
			parameters.put("settlement_schema", settlementReportTO.getSchema());
			parameters.put("operations_extended", settlementReportTO.getIndExtended());
			parameters.put("date", settlementReportTO.getDate());
		}else if(ReportIdType.TRADING_RECORD_BBV.getCode().equals(idReport)){
			parameters.put("settlement_date", settlementReportTO.getDate());
		}else if(ReportIdType.NET_SETTLEMENT_POSITIONS_PRE.getCode().equals(idReport)){
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("date", settlementReportTO.getDate());
			parameters.put("stage", settlementReportTO.getScheduleType());
			parameters.put("currency", settlementReportTO.getCurrency());
		}else if(ReportIdType.PREPAID_SETTLEMENT.getCode().equals(idReport)){
			parameters.put("part_source", settlementReportTO.getParticipant());
			parameters.put("date_initial", settlementReportTO.getInitialDate());
			parameters.put("date_end", settlementReportTO.getEndDate());
		}else if(ReportIdType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(idReport)){
			parameters.put("part_source", settlementReportTO.getParticipant());
			parameters.put("date_initial", settlementReportTO.getInitialDate());
			parameters.put("date_end", settlementReportTO.getEndDate());
		}
		//DESPUES DE LOS INCUMPLIMIENTOS
		else if(ReportIdType.UNFULFILLED_OPERATIONS.getCode().equals(idReport)){
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("date_initial", settlementReportTO.getInitialDate());
			parameters.put("date_end", settlementReportTO.getEndDate());
		}
		//DESPUES DE CADA ETAPA DE LIQUIDACION
		else if(ReportIdType.NET_POSITIONS_WITHDRAWALS.getCode().equals(idReport)){
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("currency", settlementReportTO.getCurrency());
			parameters.put("schedule_type", settlementReportTO.getScheduleType());
			parameters.put("date", settlementReportTO.getDate());
		}else if(ReportIdType.DETAILS_SETTLED_OPERATIONS.getCode().equals(idReport)){
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("currency", settlementReportTO.getCurrency());
			parameters.put("schedule_type", settlementReportTO.getScheduleType());
			parameters.put("date", settlementReportTO.getDate());
		}else if(ReportIdType.EXLACK_SECURITY_OPER_FUND_BAG.getCode().equals(idReport)){
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("currency", settlementReportTO.getCurrency());
			parameters.put("schedule_type", settlementReportTO.getScheduleType());
			parameters.put("date", settlementReportTO.getDate());
		} else if(ReportIdType.NET_POSITIONS_SETTLED.getCode().equals(idReport)){			
			parameters.put("currency", settlementReportTO.getCurrency());
			parameters.put("schedule_type", settlementReportTO.getScheduleType());
			parameters.put("date", settlementReportTO.getDate());
		} else if(ReportIdType.GENERATOR_CHAINED_OPERATION.getCode().equals(idReport)) {
			parameters.put("date_initial", settlementReportTO.getInitialDate());
			parameters.put("date_end", settlementReportTO.getEndDate());
		} 
		// Report Manual
		else if(ReportIdType.NET_POSITIONS_OPERATIONS_BAG_PRE.getCode().equals(idReport)) {
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("currency", settlementReportTO.getCurrency());
			parameters.put("schedule_type", settlementReportTO.getScheduleType());
			parameters.put("date", settlementReportTO.getDate());
		} else if(ReportIdType.NET_POSITIONS_OPERATIONS_DEF_EX_REG.getCode().equals(idReport)) {			
			parameters.put("date", settlementReportTO.getDate());
			parameters.put("participant", settlementReportTO.getParticipant());
		} else if(ReportIdType.GROSS_SCHEME_SETTLEMENT.getCode().equals(idReport)) {
			parameters.put("participant", settlementReportTO.getParticipant());
			parameters.put("currency", settlementReportTO.getCurrency());			
			parameters.put("settlement_date", settlementReportTO.getDate());
		}
		
		parameters.put("reportType", idReport.toString());
		parameters.put("reportFormats", String.valueOf(ReportFormatType.PDF.getCode()));
		return parameters;
	}
	
	/**
	 * Asingment parameters detail.
	 *
	 * @param settlementReportTO the settlement report to
	 * @param idReport the id report
	 * @return the map
	 */
	public Map<String,String> asingmentParametersDetail(SettlementReportTO settlementReportTO,Long  idReport){
		Map<String,String> parameters = new HashMap<String, String>();
		//INICIO DE DIA
		if(ReportIdType.DETAIL_EXPIRATION_REPURCHASE.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())){
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())){
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}			
		} else if(ReportIdType.PURCHASE_FORCE_EXPIRATION_SETTLED.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}			
		}
		//CIERRE DE DIA
		else if(ReportIdType.SETTLED_OPERATIONS.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getSchema())) {
				parameters.put("schema", SettlementSchemaType.get(new Integer(settlementReportTO.getSchema())).getValue());
			}			
		}else if(ReportIdType.SETTLED_ADVANCE_OPERATIONS.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}			
		}				
		//CIERRE DE ASIGNACION
		else if(ReportIdType.OPERATIONS_DETAIL_WITH_PENDING_DELIV.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getSchema())) {
				parameters.put("settlement_schema", SettlementSchemaType.get(new Integer(settlementReportTO.getSchema())).getValue());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getIndExtended())) {
				if(BooleanType.NO.getCode().equals(new Integer(settlementReportTO.getIndExtended()))){
					parameters.put("operations_extended", BooleanType.get(new Integer(settlementReportTO.getIndExtended())).getValue());
				} else if(BooleanType.YES.getCode().equals(new Integer(settlementReportTO.getIndExtended()))){ 
					parameters.put("operations_extended", BooleanType.get(new Integer(settlementReportTO.getIndExtended())).getValue());
				}
			}						
		} else if(ReportIdType.NET_SETTLEMENT_POSITIONS_PRE.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getScheduleType())) {
				parameters.put("stage", SettlementScheduleType.lookup.get(Integer.parseInt(settlementReportTO.getScheduleType())).getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())) {
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}						
		} else if(ReportIdType.PREPAID_SETTLEMENT.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("part_source", objParticipant.getDescription());
			}			
		} else if(ReportIdType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("part_source", objParticipant.getDescription());
			}			
		}
		//DESPUES DE LOS INCUMPLIMIENTOS
		else if(ReportIdType.UNFULFILLED_OPERATIONS.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}			
		}
		//DESPUES DE CADA ETAPA DE LIQUIDACION
		else if(ReportIdType.NET_POSITIONS_WITHDRAWALS.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())) {
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getScheduleType())) {
				parameters.put("schedule_type", SettlementScheduleType.lookup.get(Integer.parseInt(settlementReportTO.getScheduleType())).getDescription());
			}						
		} else if(ReportIdType.DETAILS_SETTLED_OPERATIONS.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}		
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())) {
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}	
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getScheduleType())) {
				parameters.put("schedule_type", SettlementScheduleType.lookup.get(Integer.parseInt(settlementReportTO.getScheduleType())).getDescription());
			}			
		} else if(ReportIdType.EXLACK_SECURITY_OPER_FUND_BAG.getCode().equals(idReport)){
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}	
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())) {
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}	
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getScheduleType())) {
				parameters.put("schedule_type", SettlementScheduleType.lookup.get(Integer.parseInt(settlementReportTO.getScheduleType())).getDescription());
			}			
		} else if(ReportIdType.NET_POSITIONS_SETTLED.getCode().equals(idReport)){	
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())) {
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getScheduleType())) {
				parameters.put("schedule_type", SettlementScheduleType.lookup.get(Integer.parseInt(settlementReportTO.getScheduleType())).getDescription());
			}			
		}	
		// Report Manual
		else if(ReportIdType.NET_POSITIONS_OPERATIONS_BAG_PRE.getCode().equals(idReport)) {			
			
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}	
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())) {
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}	
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getScheduleType())) {
				parameters.put("schedule_type", SettlementScheduleType.lookup.get(Integer.parseInt(settlementReportTO.getScheduleType())).getDescription());
			}
		} else if(ReportIdType.NET_POSITIONS_OPERATIONS_DEF_EX_REG.getCode().equals(idReport)) {			
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}
		} else if(ReportIdType.GROSS_SCHEME_SETTLEMENT.getCode().equals(idReport)) {
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getParticipant())) {
				Participant objParticipant = settlementReportService.find(Participant.class, new Long(settlementReportTO.getParticipant()));
				parameters.put("participant", objParticipant.getDescription());
			}	
			if(Validations.validateIsNotNullAndNotEmpty(settlementReportTO.getCurrency())) {
				parameters.put("currency", CurrencyType.get(new Integer(settlementReportTO.getCurrency())).getValue());
			}			
		}
		
		return parameters;
	}
	
}