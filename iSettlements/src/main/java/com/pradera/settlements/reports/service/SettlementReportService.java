package com.pradera.settlements.reports.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.settlements.reports.SettlementProcessReportsSender;
import com.pradera.settlements.reports.SettlementReportTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementReportService.
 *
 * @author Pradera Technologies
 */
@Stateless
public class SettlementReportService extends CrudDaoServiceBean {
	
	/** The report sender. */
	@EJB 
	private SettlementProcessReportsSender reportSender;
	
	/**
	 * Send reports.
	 *
	 * @param lstSettlementReportTO the lst settlement report to
	 * @param idReportPk the id report pk
	 * @param lgUser the lg user
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void sendReports(List<SettlementReportTO> lstSettlementReportTO, Long idReportPk,LoggerUser lgUser) throws ServiceException{
		reportSender.sendReports(lstSettlementReportTO, idReportPk, lgUser);
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE DETALLE DE VENCIMIENTO DE REPORTOS.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> detailExpirationRepurchaseQuery(SettlementReportTO filter) throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();
		
		stringBuilderQry.append(" SELECT DISTINCT																			");
		stringBuilderQry.append(" 	HAO.inchargeStockParticipant.idParticipantPk,											");
		stringBuilderQry.append("  	SO.settlementCurrency,																	");
		stringBuilderQry.append("  	TO_CHAR(SYSDATE,'DD/MM/YYYY')															");
		stringBuilderQry.append(" FROM SettlementOperation SO, MechanismOperation MO, SettlementAccountOperation SAO,   	");
		stringBuilderQry.append(" 	HolderAccountOperation HAO, Security SE, NegotiationModality NM                      	");
		stringBuilderQry.append(" WHERE SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk            ");
		stringBuilderQry.append("	AND SO.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk	    ");
		stringBuilderQry.append("	AND SAO.holderAccountOperation.idHolderAccountOperationPk = HAO.idHolderAccountOperationPk ");
		stringBuilderQry.append("	AND SE.idSecurityCodePk = MO.securities.idSecurityCodePk                                ");
		stringBuilderQry.append("   AND NM.idNegotiationModalityPk = MO.mechanisnModality.negotiationModality.idNegotiationModalityPk ");
		stringBuilderQry.append("	AND SO.operationPart = 2                                                        		");// PLAZO
		stringBuilderQry.append("	AND SO.indPrepaid = 0                                                                  	");
		stringBuilderQry.append("	AND SO.indExtended = 0                                                                 	");
		stringBuilderQry.append("	AND SAO.operationState = 625 			                                                ");// CONFIRMADO
		stringBuilderQry.append("	AND SO.settlementType = 2 					                                            ");// LIQUIDACION NETA
		stringBuilderQry.append("	AND SO.settlementDate <> SE.expirationDate 							                	");// OMITIMOS COMPRAS FORZOZAS
		stringBuilderQry.append("	AND MO.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = 1 				");// BOLSA DE VALORES
		stringBuilderQry.append("	AND TRUNC(SO.settlementDate) = TO_DATE(:date,'DD/MM/YYYY')                      		");
		stringBuilderQry.append(" ORDER BY 1,2                                                                              ");
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			parameters.put("date", filter.getDate());
		}	
		List<Object[]> lstResult = findListByQueryString(stringBuilderQry.toString(), parameters);
		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.TRUE);
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setCurrency(obj[1].toString());
			settlementReportTO.setDate(filter.getDate());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE LIQUIDACIONES DE COMPRAS FORZOSAS, SELVE Y/O CAMBIO DE TIPO DE LIQUIDACION ESPECIAL.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> purchaseForceExpirationSettledQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																						");
		stringBuilderQry.append(" 	PA_BUYER.ID_PARTICIPANT_PK AS PART_COMPRADOR,                                                       ");
		stringBuilderQry.append(" 	PA_SELLER.ID_PARTICIPANT_PK AS PART_VENDEDOR                                                        ");
		stringBuilderQry.append(" FROM SETTLEMENT_OPERATION SO, MECHANISM_OPERATION MO, SETTLEMENT_ACCOUNT_OPERATION SAO, 				");
		stringBuilderQry.append("   PARTICIPANT PA_SELLER, PARTICIPANT PA_BUYER 														");
		stringBuilderQry.append(" WHERE SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                     ");
		stringBuilderQry.append(" 	AND SO.ID_SETTLEMENT_OPERATION_PK = SAO.ID_SETTLEMENT_OPERATION_FK                                  ");
		stringBuilderQry.append("	AND MO.ID_NEGOTIATION_MECHANISM_FK=1                                                                ");
		stringBuilderQry.append("	AND SO.IND_FORCED_PURCHASE=1																		");	
		stringBuilderQry.append("	AND SO.OPERATION_STATE=616                                                                          ");
		stringBuilderQry.append(" 	AND PA_SELLER.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK                                       ");
		stringBuilderQry.append("	AND PA_BUYER.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK                                         ");
		stringBuilderQry.append("	AND trunc(so.SETTLEMENT_DATE) >= to_date(:initialDate,'dd/MM/yyyy')                                 ");
		stringBuilderQry.append("	AND trunc(so.SETTLEMENT_DATE) <= to_date(:endDate,'dd/MM/yyyy')                                     ");
		stringBuilderQry.append(" UNION                                                                                                 ");
		stringBuilderQry.append(" SELECT DISTINCT                                  														");
		stringBuilderQry.append("	PA_BUYER.ID_PARTICIPANT_PK AS PART_COMPRADOR,                                                       ");
		stringBuilderQry.append("	PA_SELLER.ID_PARTICIPANT_PK AS PART_VENDEDOR                                                        ");
		stringBuilderQry.append(" FROM SETTLEMENT_OPERATION SO, MECHANISM_OPERATION MO, SETTLEMENT_ACCOUNT_OPERATION SAO,               ");
		stringBuilderQry.append(" 	SETTLEMENT_REQUEST SR, SETTLEMENT_DATE_OPERATION SDO, PARTICIPANT PA_SELLER, PARTICIPANT PA_BUYER   ");
		stringBuilderQry.append(" WHERE SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                     ");
		stringBuilderQry.append(" 	AND SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK                                      ");
		stringBuilderQry.append(" 	AND SDO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK									");
		stringBuilderQry.append(" 	AND SO.ID_SETTLEMENT_OPERATION_PK = SAO.ID_SETTLEMENT_OPERATION_FK                                  ");
		stringBuilderQry.append(" 	AND PA_SELLER.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK                                       ");
		stringBuilderQry.append(" 	AND PA_BUYER.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK                                         ");
		stringBuilderQry.append("	AND MO.ID_NEGOTIATION_MECHANISM_FK=1                                                                ");
		stringBuilderQry.append("	AND ((SO.OPERATION_PART=1 AND SO.OPERATION_STATE=615) OR                                            ");
		stringBuilderQry.append("	       (SO.OPERATION_PART=2 AND SO.OPERATION_STATE=616))                                            ");
		stringBuilderQry.append(" 	AND SR.REQUEST_TYPE IN (2020,2257)																	");
		stringBuilderQry.append("	AND trunc(so.SETTLEMENT_DATE) >= to_date(:initialDate,'dd/MM/yyyy')                                 ");
		stringBuilderQry.append("	AND trunc(so.SETTLEMENT_DATE) <= to_date(:endDate,'dd/MM/yyyy')                                     ");
		stringBuilderQry.append("	ORDER BY                                                                                            ");
		stringBuilderQry.append("	1,2                                                                                                 ");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInitialDate())){
			query.setParameter("initialDate", filter.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getEndDate())){
			query.setParameter("endDate", filter.getEndDate());
		}
		List<Object[]>  lstResult = query.getResultList();
		
		List<String> lstParticipantsReport = new ArrayList<String>();
		for(Object[] objFilter : lstResult){
			if(!lstParticipantsReport.contains(objFilter[0].toString())){
				lstParticipantsReport.add(objFilter[0].toString());
			}
			if(!lstParticipantsReport.contains(objFilter[1].toString())){
				lstParticipantsReport.add(objFilter[1].toString());
			}
		}
		for(String obj : lstParticipantsReport){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.FALSE);
			settlementReportTO.setOnlyUser(Boolean.FALSE);
			settlementReportTO.setParticipant(obj.toString());
			settlementReportTO.setInitialDate(filter.getInitialDate());
			settlementReportTO.setEndDate(filter.getEndDate());
			lstSettlementReportTO.add(settlementReportTO);
		}
		
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE OPERACIONES LIQUIDADAS.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> settledOperationsQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																							");
		stringBuilderQry.append("     PS.ID_PARTICIPANT_FK, 																				");
		stringBuilderQry.append("     SO.SETTLEMENT_SCHEMA, 																				");
		stringBuilderQry.append("     TO_CHAR(SYSDATE,'DD/MM/YYYY')																			");
		stringBuilderQry.append(" FROM																										");
		stringBuilderQry.append("     MECHANISM_OPERATION MO,  SETTLEMENT_OPERATION SO, PARTICIPANT BUYER_PA , PARTICIPANT SELLER_PA 		");
		stringBuilderQry.append("     ,MODALITY_GROUP MG , MODALITY_GROUP_DETAIL MGD , MECHANISM_MODALITY MM , NEGOTIATION_MODALITY  NMO 	");
		stringBuilderQry.append("    ,SETTLEMENT_PROCESS SP , OPERATION_SETTLEMENT OS, PARTICIPANT_SETTLEMENT PS 							");
		stringBuilderQry.append(" WHERE																										");	
		stringBuilderQry.append("    MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK 											");
		stringBuilderQry.append("    AND MG.ID_MODALITY_GROUP_PK = MGD.ID_MODALITY_GROUP_FK 												");
		stringBuilderQry.append("    AND MGD.ID_NEGOTIATION_MECHANISM_FK = MM.ID_NEGOTIATION_MECHANISM_PK 									");
		stringBuilderQry.append("    AND MGD.ID_NEGOTIATION_MODALITY_FK = MM.ID_NEGOTIATION_MODALITY_PK 									");
		stringBuilderQry.append("    AND MM.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK 									");
		stringBuilderQry.append("    AND MM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK 										");
		stringBuilderQry.append("    AND MM.ID_NEGOTIATION_MODALITY_PK = NMO.ID_NEGOTIATION_MODALITY_PK										");
		stringBuilderQry.append("    AND MG.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA 														");
		stringBuilderQry.append("    AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK 											");
		stringBuilderQry.append("    AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK 										");
		stringBuilderQry.append("    AND SP.PROCESS_STATE = 1284 				 															");
		stringBuilderQry.append("    AND SP.SETTLEMENT_DATE = SO.SETTLEMENT_DATE 															");
		stringBuilderQry.append("    AND SP.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA 														");
		stringBuilderQry.append("    AND SP.CURRENCY = SO.SETTLEMENT_CURRENCY																");
		stringBuilderQry.append("    AND SP.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK 									");
		stringBuilderQry.append("    AND SP.ID_MODALITY_GROUP_FK = MG.ID_MODALITY_GROUP_PK 													");
		stringBuilderQry.append("    AND ((SO.OPERATION_PART = 1 AND  SO.OPERATION_STATE IN (615)) 											");
		stringBuilderQry.append("         OR (SO.OPERATION_PART = 2 AND  SO.OPERATION_STATE IN (616))) 										");
		stringBuilderQry.append("    AND MO.ID_BUYER_PARTICIPANT_FK = BUYER_PA.ID_PARTICIPANT_PK 											");
		stringBuilderQry.append("    AND MO.ID_SELLER_PARTICIPANT_FK = SELLER_PA.ID_PARTICIPANT_PK 											");
		stringBuilderQry.append("    AND OS.OPERATION_STATE=1288 																			");
		stringBuilderQry.append("    AND PS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK 										");
		stringBuilderQry.append("    AND TRUNC(SO.SETTLEMENT_DATE) BETWEEN TO_DATE(:initialDate,'DD/MM/YYYY') AND TO_DATE(:endDate,'DD/MM/YYYY') ");
		stringBuilderQry.append("ORDER BY 1,2 ");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInitialDate())){
			query.setParameter("initialDate", filter.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getEndDate())){
			query.setParameter("endDate", filter.getEndDate());
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.FALSE);
			settlementReportTO.setOnlyUser(Boolean.FALSE);
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setSchema(obj[1].toString());
			settlementReportTO.setInitialDate(filter.getInitialDate());
			settlementReportTO.setEndDate(filter.getEndDate());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE OPERACIONES LIQUIDADAS ANTICIPADAMENTE.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> settledAdvanceOperationsQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																									");
		stringBuilderQry.append("   SR.ID_SOURCE_PARTICIPANT_FK,																					");
		stringBuilderQry.append("   SR.ID_TARGET_PARTICIPANT_FK,																					");
		stringBuilderQry.append("   TO_CHAR(SYSDATE,'DD/MM/YYYY')																					");
		stringBuilderQry.append(" FROM MECHANISM_OPERATION MO																						");
		stringBuilderQry.append("   INNER JOIN SETTLEMENT_OPERATION SO        ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK		");
		stringBuilderQry.append("   INNER JOIN NEGOTIATION_MODALITY  NMO      ON MO.ID_NEGOTIATION_MODALITY_FK = NMO.ID_NEGOTIATION_MODALITY_PK		");
		stringBuilderQry.append("   INNER JOIN SETTLEMENT_DATE_OPERATION SDO  ON SDO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK		");
		stringBuilderQry.append("   INNER JOIN SETTLEMENT_REQUEST SR          ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK			");	
		stringBuilderQry.append("   INNER JOIN PARTICIPANT SRC_PA             ON SR.ID_SOURCE_PARTICIPANT_FK = SRC_PA.ID_PARTICIPANT_PK				");
		stringBuilderQry.append(" WHERE SR.REQUEST_TYPE = 2019 																						");
		stringBuilderQry.append("   AND SR.REQUEST_STATE = 1543 																					");
		stringBuilderQry.append("   AND trunc(Sr.Register_Date) >= to_date(:initialDate,'dd/MM/yyyy')												");
		stringBuilderQry.append("   AND trunc(Sr.Register_Date) <= to_date(:endDate,'dd/MM/yyyy')													");
		stringBuilderQry.append(" ORDER BY																											");
		stringBuilderQry.append("   1,2																												");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInitialDate())){
			query.setParameter("initialDate", filter.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getEndDate())){
			query.setParameter("endDate", filter.getEndDate());
		}
		List<Object[]>  lstResult = query.getResultList();
		
		List<String> lstParticipantsReport = new ArrayList<String>();
		for(Object[] objFilter : lstResult){
			if(!lstParticipantsReport.contains(objFilter[0].toString())){
				lstParticipantsReport.add(objFilter[0].toString());
			}
			if(!lstParticipantsReport.contains(objFilter[1].toString())){
				lstParticipantsReport.add(objFilter[1].toString());
			}
		}
		for(String obj : lstParticipantsReport){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.FALSE);
			settlementReportTO.setOnlyUser(Boolean.TRUE);
			settlementReportTO.setParticipant(obj.toString());//participant
			settlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//initialDate
			settlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//endDate
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE RESUMEN DE POSICIONES NETAS (PRELIMINAR).
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> netSettlementPositionsPreQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();	
		stringBuilderQry.append("  SELECT  DISTINCT PS.SETTLEMENT_CURRENCY,  SP.SCHEDULE_TYPE										");
		stringBuilderQry.append(" FROM MECHANISM_OPERATION MO, SETTLEMENT_OPERATION SO, PARTICIPANT_SETTLEMENT PS, PARTICIPANT P,	");
		stringBuilderQry.append(" MODALITY_GROUP MG, MODALITY_GROUP_DETAIL MGD, MECHANISM_MODALITY MM, SETTLEMENT_PROCESS SP		");
		stringBuilderQry.append(" WHERE PS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK 								");		
		stringBuilderQry.append(" AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK									");
		stringBuilderQry.append("  AND P.ID_PARTICIPANT_PK = PS.ID_PARTICIPANT_FK													");
		stringBuilderQry.append("  AND MG.ID_MODALITY_GROUP_PK = MGD.ID_MODALITY_GROUP_FK											");
		stringBuilderQry.append(" AND MGD.ID_NEGOTIATION_MECHANISM_FK = MM.ID_NEGOTIATION_MECHANISM_PK								");		
		stringBuilderQry.append(" AND MGD.ID_NEGOTIATION_MODALITY_FK = MM.ID_NEGOTIATION_MODALITY_PK								");
		stringBuilderQry.append(" AND MGD.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK								");
		stringBuilderQry.append(" AND MGD.ID_NEGOTIATION_MODALITY_FK = MO.ID_NEGOTIATION_MODALITY_FK								");
		stringBuilderQry.append(" AND MG.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA													");
		stringBuilderQry.append(" AND SP.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK								");		
		stringBuilderQry.append(" AND SP.ID_MODALITY_GROUP_FK = MGD.ID_MODALITY_GROUP_FK					");
		stringBuilderQry.append(" AND SP.CURRENCY = PS.SETTLEMENT_CURRENCY									");
		stringBuilderQry.append(" AND MO.ID_NEGOTIATION_MECHANISM_FK = 1									");
		stringBuilderQry.append("  AND SO.SETTLEMENT_TYPE =  2												");
		stringBuilderQry.append(" AND PS.OPERATION_STATE = 1943												");
		stringBuilderQry.append("  AND ( (SO.OPERATION_PART = 1 AND  SO.OPERATION_STATE IN (612,614))		");		
		stringBuilderQry.append("  OR (SO.OPERATION_PART = 2 AND  SO.OPERATION_STATE IN (615))      )		");		
		stringBuilderQry.append("    AND (TRUNC(SO.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY') )			");		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append(" AND SP.SCHEDULE_TYPE = :scheludeType									");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())) {
			stringBuilderQry.append(" AND PS.SETTLEMENT_CURRENCY = :parCurrency								");
		}
		stringBuilderQry.append(" UNION																		");	
		stringBuilderQry.append(" SELECT DISTINCT															");		
		stringBuilderQry.append(" 	  SP.CURRENCY, SP.SCHEDULE_TYPE											");
		stringBuilderQry.append(" 	FROM PARTICIPANT P, SETTLEMENT_PROCESS SP, PARTICIPANT_POSITION PP		");
		stringBuilderQry.append(" 	WHERE P.ID_PARTICIPANT_PK = PP.ID_PARTICIPANT_FK						");
		stringBuilderQry.append(" 	  AND SP.ID_SETTLEMENT_PROCESS_PK = PP.ID_SETTLEMENT_PROCESS_FK			");
		stringBuilderQry.append(" 	  AND SP.ID_NEGOTIATION_MECHANISM_FK = 1 								");
		stringBuilderQry.append(" 	  AND PP.IND_PROCESS=0													");	
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append(" AND TRUNC(SP.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')			");
		}		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append(" AND SP.SCHEDULE_TYPE = :scheludeType									");
		}	
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())) {
			stringBuilderQry.append(" AND SP.CURRENCY = :parCurrency										");
		}
		stringBuilderQry.append(" 	ORDER BY 1,2															");			
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())) {
			query.setParameter("parCurrency", new Integer(filter.getCurrency()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			query.setParameter("scheludeType", new Integer(filter.getScheduleType()));
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();	
			settlementReportTO.setOnlyUser(Boolean.TRUE);
			settlementReportTO.setCurrency(obj[0].toString());
			settlementReportTO.setScheduleType(obj[1].toString());
			settlementReportTO.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));			
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE OPERACIONES LIQUIDADAS ANTICIPADAMENTE.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> prepaidSettlementQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" select DISTINCT																							");
		stringBuilderQry.append("   sr.id_target_participant_fk,																			");
		stringBuilderQry.append("   sr.id_source_participant_fk																				");
		stringBuilderQry.append(" from																										");
		stringBuilderQry.append(" 	settlement_request sr																					");
		stringBuilderQry.append(" 	INNER JOIN settlement_date_operation sdo on Sr.Id_Settlement_Request_Pk = Sdo.Id_Settlement_Request_Fk	");
		stringBuilderQry.append(" 	INNER JOIN settlement_operation so on Sdo.Id_Settlement_Operation_Fk = So.Id_Settlement_Operation_Pk	");
		stringBuilderQry.append(" 	INNER JOIN mechanism_operation mo on So.Id_Mechanism_Operation_Fk = Mo.Id_Mechanism_Operation_Pk		");
		stringBuilderQry.append(" 	INNER JOIN participant SOL_PART on sol_part.id_participant_pk = Sr.Id_Source_Participant_Fk				");	
		stringBuilderQry.append(" 	INNER JOIN participant TARGET_PART on TARGET_PART.id_participant_pk = Sr.ID_TARGET_PARTICIPANT_FK		");
		stringBuilderQry.append(" 	INNER JOIN participant PA_SELLER ON PA_SELLER.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK			");
		stringBuilderQry.append(" 	INNER JOIN participant PA_BUYER ON PA_BUYER.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK				");
		stringBuilderQry.append(" where																										");
		stringBuilderQry.append(" 	sr.request_type = 2019																					");
		stringBuilderQry.append(" 	AND mo.ID_NEGOTIATION_MECHANISM_FK=1																	");
		stringBuilderQry.append(" 	AND trunc(sr.register_date) >= to_date(:initialDate,'dd/MM/yyyy')										");
		stringBuilderQry.append(" 	AND trunc(sr.register_date) <= to_date(:endDate,'dd/MM/yyyy')											");
		stringBuilderQry.append(" ORDER BY																									");
		stringBuilderQry.append(" 1,2																										");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInitialDate())){
			query.setParameter("initialDate", filter.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getEndDate())){
			query.setParameter("endDate", filter.getEndDate());
		}
		List<Object[]>  lstResult = query.getResultList();
		
		List<String> lstParticipantsReport = new ArrayList<String>();
		for(Object[] objFilter : lstResult){
			if(!lstParticipantsReport.contains(objFilter[0].toString())){
				lstParticipantsReport.add(objFilter[0].toString());
			}
			if(!lstParticipantsReport.contains(objFilter[1].toString())){
				lstParticipantsReport.add(objFilter[1].toString());
			}
		}
		for(String obj : lstParticipantsReport){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setOnlyUser(Boolean.FALSE);
			settlementReportTO.setParticipant(obj.toString());//participant
			settlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//initialDate
			settlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//endDate
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE SOLICITUDES DE CAMBIO DE TIPO DE LIQUIDACION (SELVE).
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> settlementTypeExchangeQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();
		
		stringBuilderQry.append(" select DISTINCT																								");
		stringBuilderQry.append(" 	  SR.ID_SOURCE_PARTICIPANT_FK,																				");
		stringBuilderQry.append(" 	  SR.ID_TARGET_PARTICIPANT_FK																				");
		stringBuilderQry.append(" 	from																										");
		stringBuilderQry.append(" 	  settlement_request sr																						");
		stringBuilderQry.append(" 	  INNER JOIN settlement_date_operation sdo on Sr.Id_Settlement_Request_Pk = Sdo.Id_Settlement_Request_Fk	");
		stringBuilderQry.append(" 	  INNER JOIN settlement_operation so on Sdo.Id_Settlement_Operation_Fk = So.Id_Settlement_Operation_Pk		");
		stringBuilderQry.append(" 	  INNER JOIN mechanism_operation mo on So.Id_Mechanism_Operation_Fk = Mo.Id_Mechanism_Operation_Pk			");
		stringBuilderQry.append(" 	  INNER JOIN participant SOL_PART on sol_part.id_participant_pk = Sr.Id_Source_Participant_Fk				");	
		stringBuilderQry.append(" 	  INNER JOIN participant PA_BUYER ON PA_BUYER.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK				");
		stringBuilderQry.append(" 	  INNER JOIN participant PA_SELLER ON PA_SELLER.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK				");
		stringBuilderQry.append(" 	where																										");
		stringBuilderQry.append(" 	  SR.REQUEST_TYPE=2020																						");
		stringBuilderQry.append(" 	  AND trunc(sr.register_date) >= to_date(:initialDate,'dd/MM/yyyy')											");
		stringBuilderQry.append(" 	  AND trunc(sr.register_date) <= to_date(:endDate,'dd/MM/yyyy')												");
		stringBuilderQry.append(" 	order by																									");
		stringBuilderQry.append(" 	  1,2																										");
		
		Query query = em.createNativeQuery(stringBuilderQry.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInitialDate())){
			query.setParameter("initialDate", filter.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getEndDate())){
			query.setParameter("endDate", filter.getEndDate());
		}
		List<Object[]>  lstResult = query.getResultList();
		
		List<String> lstParticipantsReport = new ArrayList<String>();
		for(Object[] objFilter : lstResult){
			if(!lstParticipantsReport.contains(objFilter[0].toString())){
				lstParticipantsReport.add(objFilter[0].toString());
			}
			if(!lstParticipantsReport.contains(objFilter[1].toString())){
				lstParticipantsReport.add(objFilter[1].toString());
			}
		}
		for(String obj : lstParticipantsReport){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setOnlyUser(Boolean.FALSE);
			settlementReportTO.setParticipant(obj.toString());//participant
			settlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//initialDate
			settlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//endDate
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DETALLE DE OPERACIONES PENDIENTES DE ENTREGA DE VALORES.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> operDetailWithSecPendingDelivQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																					");
		stringBuilderQry.append(" 	  SO.SETTLEMENT_SCHEMA,																			");
		stringBuilderQry.append(" 	  SO.IND_EXTENDED																				");
		stringBuilderQry.append(" 	FROM HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO, SETTLEMENT_ACCOUNT_OPERATION SAO,	");
		stringBuilderQry.append(" 	  SETTLEMENT_ACCOUNT_MARKETFACT sam, SETTLEMENT_OPERATION SO, SECURITY SE						");
		stringBuilderQry.append(" 	WHERE HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK								");
		stringBuilderQry.append(" 	  AND SAO.ID_SETTLEMENT_ACCOUNT_PK =sam.ID_SETTLEMENT_ACCOUNT_FK								");
		stringBuilderQry.append(" 	  AND MO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK											");
		stringBuilderQry.append(" 	  AND SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK					");	
		stringBuilderQry.append(" 	  AND MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK								");
		stringBuilderQry.append(" 	  AND SO.ID_SETTLEMENT_OPERATION_PK = SAO.ID_SETTLEMENT_OPERATION_FK							");
		stringBuilderQry.append(" 	  AND HAO.OPERATION_PART = SO.OPERATION_PART													");
		stringBuilderQry.append(" 	  AND MO.OPERATION_STATE = 614 																	");
		stringBuilderQry.append(" 	  AND HAO.HOLDER_ACCOUNT_STATE = 625 															");
		stringBuilderQry.append(" 	  AND HAO.ROLE=2 																				");
		stringBuilderQry.append(" 	  AND HAO.OPERATION_PART=1 																		");
		stringBuilderQry.append(" 	  AND MO.ID_NEGOTIATION_MECHANISM_FK=1 															");
		stringBuilderQry.append(" 	  AND SAO.STOCK_REFERENCE IS NULL																");
		stringBuilderQry.append(" 	  AND SAO.STOCK_QUANTITY>SAO.CHAINED_QUANTITY 													");
		stringBuilderQry.append(" 	  AND sam.IND_ACTIVE=1																			");
		stringBuilderQry.append(" 	  AND TRUNC(SO.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')									");
		stringBuilderQry.append(" 	ORDER BY 1,2																					");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setOnlyUser(Boolean.TRUE);
			settlementReportTO.setSchema(obj[0].toString());
			settlementReportTO.setIndExtended(obj[1].toString());
			settlementReportTO.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE OPERACIONES INCUMPLIDAS.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> unfulfilledOperationsQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																									");
		stringBuilderQry.append(" 	  OU.ID_UNFULFILLED_PARTICIPANT_FK,																				");
		stringBuilderQry.append(" 	  OU.ID_AFFECTED_PARTICIPANT_FK																					");
		stringBuilderQry.append(" 	FROM																											");
		stringBuilderQry.append(" 	  OPERATION_UNFULFILLMENT OU, SETTLEMENT_OPERATION SO, MECHANISM_OPERATION MO, NEGOTIATION_MECHANISM nm			");
		stringBuilderQry.append(" 	WHERE																											");
		stringBuilderQry.append(" 	  OU.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK													");
		stringBuilderQry.append(" 	  AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK												");
		stringBuilderQry.append(" 	  AND mo.ID_NEGOTIATION_MECHANISM_FK = nm.ID_NEGOTIATION_MECHANISM_PK											");	
		stringBuilderQry.append(" 		AND (0 < (SELECT COUNT(*) FROM HOLDER_ACCOUNT_OPERATION HAO 												");
		stringBuilderQry.append(" 	  	WHERE HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK AND HAO.HOLDER_ACCOUNT_STATE = 625))		");
		stringBuilderQry.append(" 	  AND trunc(OU.UNFULFILLMENT_DATE) >= to_date(:initialDate,'dd/MM/yyyy')										");
		stringBuilderQry.append(" 	  AND trunc(OU.UNFULFILLMENT_DATE) <= to_date(:endDate,'dd/MM/yyyy')											");
		stringBuilderQry.append(" 	ORDER BY																										");
		stringBuilderQry.append(" 	  1,2																											");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		query.setParameter("initialDate", filter.getInitialDate());
		query.setParameter("endDate", filter.getEndDate());
		List<Object[]>  lstResult = query.getResultList();		
		List<String> lstParticipantsReport = new ArrayList<String>();
		for(Object[] objFilter : lstResult){
			if(!lstParticipantsReport.contains(objFilter[0].toString())){
				lstParticipantsReport.add(objFilter[0].toString());
			}
			if(!lstParticipantsReport.contains(objFilter[1].toString())){
				lstParticipantsReport.add(objFilter[1].toString());
			}
		}
		for(String obj : lstParticipantsReport){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.FALSE);
			settlementReportTO.setOnlyUser(Boolean.FALSE);
			settlementReportTO.setParticipant(obj.toString());//participant
			settlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//initialDate
			settlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//endDate
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE DETALLE DE OPERACIONES LIQUIDADAS.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> detailSettledOperationsQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();
		
		stringBuilderQry.append(" SELECT DISTINCT																								");
		stringBuilderQry.append("   HAO.ID_INCHARGE_FUNDS_PARTICIPANT,																			");
		stringBuilderQry.append("   SP.SCHEDULE_TYPE,																							");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY,																						");
		stringBuilderQry.append("   TO_CHAR(SP.SETTLEMENT_DATE,'DD/MM/YYYY')																	");
		stringBuilderQry.append(" FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,	");
		stringBuilderQry.append(" 	SETTLEMENT_OPERATION SO, NEGOTIATION_MODALITY NM, settlement_account_operation sao							");
		stringBuilderQry.append(" WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK												");
		stringBuilderQry.append(" 	AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK											");	
		stringBuilderQry.append(" 	AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK												");
		stringBuilderQry.append(" 	AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK											");
		stringBuilderQry.append(" 	and sao.id_holder_account_operation_fk = hao.id_holder_account_operation_pk									");
		stringBuilderQry.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK											");
		stringBuilderQry.append(" 	AND SP.SETTLEMENT_SCHEMA=2																					");
		stringBuilderQry.append(" 	AND OS.OPERATION_STATE = 1288 																				");
		stringBuilderQry.append("           AND ((SO.OPERATION_STATE = 615 AND SO.OPERATION_PART = 1) OR										");
		stringBuilderQry.append("                 (SO.OPERATION_STATE = 616 AND SO.OPERATION_PART = 2))											");
		stringBuilderQry.append(" 	AND HAO.HOLDER_ACCOUNT_STATE= 625 																			");
		stringBuilderQry.append("   AND sao.operation_state = 625																				");
		stringBuilderQry.append(" 	AND HAO.OPERATION_PART = SO.OPERATION_PART																	");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append("   AND TRUNC(SP.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append("   AND SP.SCHEDULE_TYPE = :scheduleType																	");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND SO.SETTLEMENT_CURRENCY = :currency																	");
		}		
		stringBuilderQry.append(" ORDER BY 1,2,3																								");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			query.setParameter("scheduleType", new Integer(filter.getScheduleType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			query.setParameter("currency", new Integer(filter.getCurrency()));
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.TRUE);
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setScheduleType(obj[1].toString());
			settlementReportTO.setCurrency(obj[2].toString());
			settlementReportTO.setDate(obj[3].toString());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE EXCLUSION POR FALTA DE VALORES Y/O FONDOS DE OPERACION DE BOLSA.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> exlackSecurOperationsFundQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																									");
		stringBuilderQry.append("   HAO.ID_INCHARGE_STOCK_PARTICIPANT,																				");
		stringBuilderQry.append("   SP.SCHEDULE_TYPE,																								");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY,																							");
		stringBuilderQry.append("   TO_CHAR(SP.SETTLEMENT_DATE,'DD/MM/YYYY')																		");
		stringBuilderQry.append(" FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,		");
		stringBuilderQry.append(" 	SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM			");
		stringBuilderQry.append(" WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK													");
		stringBuilderQry.append("   AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK												");	
		stringBuilderQry.append("   AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK													");
		stringBuilderQry.append("   AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK												");
		stringBuilderQry.append("   AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK										");
		stringBuilderQry.append("   AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK															");
		stringBuilderQry.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK												");
		stringBuilderQry.append("   AND HAO.OPERATION_PART = SO.OPERATION_PART																		");
		stringBuilderQry.append("   AND SP.SETTLEMENT_SCHEMA=2																						");
		stringBuilderQry.append("   AND HAO.HOLDER_ACCOUNT_STATE= 625																				");
		stringBuilderQry.append("   AND OS.OPERATION_STATE IN (1286,1287,1638)																		");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append("   AND (TRUNC(SP.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY'))												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append("   AND (SP.SCHEDULE_TYPE = :scheduleType)																		");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND (SO.SETTLEMENT_CURRENCY = :currency)																	");
		}		
		stringBuilderQry.append(" ORDER BY 1,2,3																									");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			query.setParameter("scheduleType", new Integer(filter.getScheduleType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			query.setParameter("currency", new Integer(filter.getCurrency()));
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.TRUE);
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setScheduleType(obj[1].toString());
			settlementReportTO.setCurrency(obj[2].toString());
			settlementReportTO.setDate(obj[3].toString());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE RESUMEN DE POSICIONES NETAS (LIQUIDADAS).
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> netSettlementPositionsSettlementQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT													");
		stringBuilderQry.append("   SP.CURRENCY,													");
		stringBuilderQry.append("   SP.SCHEDULE_TYPE												");
		stringBuilderQry.append(" FROM PARTICIPANT P, SETTLEMENT_PROCESS SP, PARTICIPANT_POSITION PP");
		stringBuilderQry.append(" WHERE P.ID_PARTICIPANT_PK = PP.ID_PARTICIPANT_FK					");
		stringBuilderQry.append(" AND SP.ID_SETTLEMENT_PROCESS_PK = PP.ID_SETTLEMENT_PROCESS_FK		");
		stringBuilderQry.append("   AND SP.ID_NEGOTIATION_MECHANISM_FK = 1							");	
		stringBuilderQry.append("   AND PP.IND_PROCESS = 1  										");	
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append(" AND TRUNC(SP.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')	");
		}		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append("   AND SP.SCHEDULE_TYPE = :scheduleType						");
		}		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND SP.CURRENCY = :currency									");
		}
		stringBuilderQry.append(" ORDER BY															");
		stringBuilderQry.append("   1,2																");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			query.setParameter("scheduleType", new Integer(filter.getScheduleType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			query.setParameter("currency", new Integer(filter.getCurrency()));
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.FALSE);
			settlementReportTO.setOnlyUser(Boolean.TRUE);	
			settlementReportTO.setCurrency(obj[0].toString());
			settlementReportTO.setScheduleType(obj[1].toString());			
			settlementReportTO.setDate(filter.getDate());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE RESUMEN DE POSICIONES NETAS (RETIROS).
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> netSettlementPositionsRetirementQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																												");
		stringBuilderQry.append("   SP.SCHEDULE_TYPE,																											");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY,																										");
		stringBuilderQry.append("   TO_CHAR(SP.SETTLEMENT_DATE,'DD/MM/YYYY')																					");
		stringBuilderQry.append(" FROM MECHANISM_OPERATION MO, SETTLEMENT_OPERATION SO, PARTICIPANT_SETTLEMENT PS, PARTICIPANT P, OPERATION_SETTLEMENT OS,		");
		stringBuilderQry.append("   SETTLEMENT_PROCESS SP,MODALITY_GROUP MG, MODALITY_GROUP_DETAIL MGD, MECHANISM_MODALITY MM									");
		stringBuilderQry.append(" WHERE PS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK															");
		stringBuilderQry.append("   AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK										");	
		stringBuilderQry.append("   AND P.ID_PARTICIPANT_PK = PS.ID_PARTICIPANT_FK														");
		stringBuilderQry.append("   AND MG.ID_MODALITY_GROUP_PK = MGD.ID_MODALITY_GROUP_FK												");
		stringBuilderQry.append("   AND MGD.ID_NEGOTIATION_MECHANISM_FK = MM.ID_NEGOTIATION_MECHANISM_PK								");
		stringBuilderQry.append("   AND MGD.ID_NEGOTIATION_MODALITY_FK = MM.ID_NEGOTIATION_MODALITY_PK									");
		stringBuilderQry.append("   AND MGD.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK								");
		stringBuilderQry.append("   AND MGD.ID_NEGOTIATION_MODALITY_FK = MO.ID_NEGOTIATION_MODALITY_FK									");
		stringBuilderQry.append("   AND MG.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA														");
		stringBuilderQry.append("   AND OS.ID_SETTLMENT_PROCESS_FK = SP.ID_SETTLEMENT_PROCESS_PK										");
		stringBuilderQry.append("   AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK									");
		stringBuilderQry.append("   AND SP.SETTLEMENT_SCHEMA = SO.SETTLEMENT_SCHEMA														");
		stringBuilderQry.append("   AND SP.CURRENCY = SO.SETTLEMENT_CURRENCY															");
		stringBuilderQry.append("   AND SP.ID_NEGOTIATION_MECHANISM_FK = MO.ID_NEGOTIATION_MECHANISM_FK									");
		stringBuilderQry.append("   AND SP.ID_MODALITY_GROUP_FK = MG.ID_MODALITY_GROUP_PK												");
		stringBuilderQry.append("   AND OS.OPERATION_STATE IN (1286,1287,1638, 1288, 1533)												");
		stringBuilderQry.append("   AND SP.PROCESS_STATE IN (1281,1283,1284,1285)														");
		stringBuilderQry.append("   AND SO.SETTLEMENT_TYPE = 2 																			");
		stringBuilderQry.append("   AND PS.OPERATION_STATE = 1943																		");
		stringBuilderQry.append("   AND MO.ID_NEGOTIATION_MECHANISM_FK=1																");
		stringBuilderQry.append("   AND SO.SETTLEMENT_SCHEMA=2																			");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append("   AND TRUNC(SP.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')										");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){ 
			stringBuilderQry.append("   AND SP.SCHEDULE_TYPE = :scheduleType															");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND SO.SETTLEMENT_CURRENCY = :currency															");
		}			
		stringBuilderQry.append(" ORDER BY	1,2,3	");
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			query.setParameter("scheduleType", new Integer(filter.getScheduleType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			query.setParameter("currency", new Integer(filter.getCurrency()));
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.FALSE);
			settlementReportTO.setOnlyUser(Boolean.TRUE);			
			settlementReportTO.setScheduleType(obj[0].toString());
			settlementReportTO.setCurrency(obj[1].toString());
			settlementReportTO.setDate(obj[2].toString());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE COMISION EDV POR LA LIQUIDACION DE OPERACIONES.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> edvComissionSettlementOperQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT																												");
		stringBuilderQry.append(" 	  CR.ID_PARTICIPANT_FK,																								");
		stringBuilderQry.append(" 	  TO_CHAR(CR.CALCULATION_DATE,'DD/MM/YYYY')																			");
		stringBuilderQry.append(" 	FROM COLLECTION_RECORD CR, COLLECTION_RECORD_DETAIL CRD, PROCESSED_SERVICE PS,										");
		stringBuilderQry.append(" 	  BILLING_SERVICE BS, SERVICE_RATE SR, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM,									");
		stringBuilderQry.append(" 	  SECURITY SE																										");
		stringBuilderQry.append(" 	WHERE BS.ID_BILLING_SERVICE_PK = PS.ID_BILLING_SERVICE_FK															");
		stringBuilderQry.append(" 	  AND PS.ID_PROCESSED_SERVICE_PK = CR.ID_PROCESSED_SERVICE_FK														");
		stringBuilderQry.append(" 	  AND CR.ID_COLLECTION_RECORD_PK = CRD.ID_COLLECTION_RECORD_FK														");	
		stringBuilderQry.append(" 	  AND SR.ID_BILLING_SERVICE_FK = BS.ID_BILLING_SERVICE_PK															");
		stringBuilderQry.append(" 	  AND HA.ID_HOLDER_ACCOUNT_PK = CRD.ID_HOLDER_ACCOUNT_FK															");
		stringBuilderQry.append(" 	  AND BS.BASE_COLLECTION IN (1414,1803)																				");
		stringBuilderQry.append(" 	  AND NM.ID_NEGOTIATION_MODALITY_PK = CRD.MODALITY																	");
		stringBuilderQry.append(" 	  AND CRD.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK																");
		stringBuilderQry.append(" 	  AND SR.RATE_TYPE=1524																								");
		stringBuilderQry.append(" 	  AND trunc(CR.CALCULATION_DATE) between to_date(:initialDate,'dd/mm/yyyy') AND to_date(:endDate,'dd/mm/yyyy')	");
		stringBuilderQry.append(" 	ORDER BY																											");
		stringBuilderQry.append(" 	  1,2																												");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInitialDate())){
			query.setParameter("initialDate", filter.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getEndDate())){
			query.setParameter("endDate", filter.getEndDate());
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setInitialDate(obj[1].toString());
			settlementReportTO.setEndDate(obj[1].toString());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE LIQUIDADAS POR ESQUEMA BRUTO.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> grossSchemeSettlementQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();		
		stringBuilderQry.append(" SELECT DISTINCT																							");
		stringBuilderQry.append("   HAO.ID_INCHARGE_FUNDS_PARTICIPANT,																		");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY,																					");
		stringBuilderQry.append("   TO_CHAR(SP.SETTLEMENT_DATE,'DD/MM/YYYY')																");
		stringBuilderQry.append(" FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,");
		stringBuilderQry.append(" 	SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM	");
		stringBuilderQry.append(" WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK											");
		stringBuilderQry.append(" 	AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK										");
		stringBuilderQry.append(" 	AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK											");	
		stringBuilderQry.append(" 	AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK										");
		stringBuilderQry.append(" 	AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK								");
		stringBuilderQry.append(" 	AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK													");
		stringBuilderQry.append("   AND HAO.OPERATION_PART = SO.OPERATION_PART																");
		stringBuilderQry.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK										");
		stringBuilderQry.append("   AND HAO.OPERATION_PART = SO.OPERATION_PART																");
		stringBuilderQry.append(" 	AND MO.ID_NEGOTIATION_MECHANISM_FK = 1																	");
		stringBuilderQry.append(" 	AND SP.SETTLEMENT_SCHEMA = 1																			");
		stringBuilderQry.append(" 	AND HAO.HOLDER_ACCOUNT_STATE = 625																		");
		stringBuilderQry.append("   AND TRUNC(SP.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')												");
		stringBuilderQry.append(" ORDER BY 1,2,3																							");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		List<Object[]>  lstResult = query.getResultList();		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.TRUE);
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setCurrency(obj[1].toString());
			settlementReportTO.setDate(obj[2].toString());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA (PRELIMINAR).
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> netPositionsOperationsBagPreQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();
		
		stringBuilderQry.append(" SELECT																										");
		stringBuilderQry.append("   PARTICIPANT,																								");
		stringBuilderQry.append("   CURRENCY,																									");
		stringBuilderQry.append("   DATE_S,																										");
		stringBuilderQry.append("   SCHEDULE_TYPE																										");
		stringBuilderQry.append(" FROM (																										");
		stringBuilderQry.append(" SELECT																										");
		stringBuilderQry.append("   HAO.ID_INCHARGE_STOCK_PARTICIPANT PARTICIPANT,																");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY CURRENCY,																			");
		stringBuilderQry.append("   TO_CHAR(SP.SETTLEMENT_DATE,'DD/MM/YYYY') DATE_S	,															");
		stringBuilderQry.append("   SP.SCHEDULE_TYPE SCHEDULE_TYPE																				");	
		stringBuilderQry.append(" FROM OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP, HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO,	");
		stringBuilderQry.append(" 	SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM		");
		stringBuilderQry.append(" WHERE SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK												");
		stringBuilderQry.append(" 	AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK											");
		stringBuilderQry.append(" 	AND SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK												");
		stringBuilderQry.append(" 	AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK											");
		stringBuilderQry.append(" 	AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK									");
		stringBuilderQry.append(" 	AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK														");
		stringBuilderQry.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK											");
		stringBuilderQry.append("   AND HAO.OPERATION_PART = SO.OPERATION_PART																	");
		stringBuilderQry.append(" 	AND SP.SETTLEMENT_SCHEMA=2																					");
		stringBuilderQry.append(" 	AND HAO.HOLDER_ACCOUNT_STATE= 625																			");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append("   AND TRUNC(SP.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append("   AND SP.SCHEDULE_TYPE = :scheludeType																	");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND SO.SETTLEMENT_CURRENCY = :currency																		");
		}
		
		stringBuilderQry.append(" UNION																											");
		stringBuilderQry.append(" SELECT																										");
		stringBuilderQry.append("   HAO.ID_INCHARGE_STOCK_PARTICIPANT PARTICIPANT,																");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY CURRENCY,																			");
		stringBuilderQry.append("   TO_CHAR(SO.SETTLEMENT_DATE,'DD/MM/YYYY') DATE_S,															");
		
		stringBuilderQry.append("   SP.SCHEDULE_TYPE SCHEDULE_TYPE																				");	
		
		stringBuilderQry.append(" FROM HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO, SETTLEMENT_OPERATION SO,							");
		stringBuilderQry.append("   SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM,								");
		
		stringBuilderQry.append("   OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP																");
		
		stringBuilderQry.append(" WHERE SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK												");
		stringBuilderQry.append(" 	AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK											");
		stringBuilderQry.append(" 	AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK									");
		stringBuilderQry.append(" 	AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK														");
		stringBuilderQry.append("   AND HAO.OPERATION_PART = SO.OPERATION_PART																	");
		stringBuilderQry.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK											");
		stringBuilderQry.append("   AND HAO.OPERATION_PART = SO.OPERATION_PART																	");
						
		stringBuilderQry.append("   AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK												");
		stringBuilderQry.append("   AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK											");
						
		stringBuilderQry.append(" 	AND SO.SETTLEMENT_SCHEMA = 2																				");
		stringBuilderQry.append(" 	AND HAO.HOLDER_ACCOUNT_STATE = 625																			");
		stringBuilderQry.append(" 	AND (SO.OPERATION_PART = 1 AND SO.OPERATION_STATE = 614 OR SO.OPERATION_PART = 2 AND SO.OPERATION_STATE = 615)	");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append("   AND TRUNC(SO.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')												");
		}				
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append("   AND SP.SCHEDULE_TYPE = :scheludeType																	");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND  SO.SETTLEMENT_CURRENCY = :currency																		");
		}		
		
		stringBuilderQry.append(" UNION																											");
		stringBuilderQry.append(" SELECT																										");
		stringBuilderQry.append("   MO.ID_BUYER_PARTICIPANT_FK PARTICIPANT,																		");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY CURRENCY,																			");
		stringBuilderQry.append("   TO_CHAR(SO.SETTLEMENT_DATE,'DD/MM/YYYY') DATE_S	,															");
		
		stringBuilderQry.append("   SP.SCHEDULE_TYPE SCHEDULE_TYPE																				");
		
		stringBuilderQry.append(" FROM MECHANISM_OPERATION MO, SETTLEMENT_OPERATION SO, NEGOTIATION_MODALITY NM,								");
		
		stringBuilderQry.append("   OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP																");
		
		stringBuilderQry.append(" WHERE SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK												");
		stringBuilderQry.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK											");
		
		stringBuilderQry.append("   AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK												");
		stringBuilderQry.append("   AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK											");
		
		stringBuilderQry.append(" 	AND SO.SETTLEMENT_SCHEMA = 2																				");
		stringBuilderQry.append(" 	AND SO.OPERATION_PART = 1 AND SO.OPERATION_STATE = 612														");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append("   AND TRUNC(SO.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')												");
		}			
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append("   AND SP.SCHEDULE_TYPE = :scheludeType																	");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND  SO.SETTLEMENT_CURRENCY = :currency																		");
		}
		
		stringBuilderQry.append(" UNION																											");
		stringBuilderQry.append(" SELECT																										");
		stringBuilderQry.append("   MO.ID_SELLER_PARTICIPANT_FK PARTICIPANT,																	");
		stringBuilderQry.append("   SO.SETTLEMENT_CURRENCY CURRENCY,																			");
		stringBuilderQry.append("   TO_CHAR(SO.SETTLEMENT_DATE,'DD/MM/YYYY') DATE_S	,															");
		
		stringBuilderQry.append("   SP.SCHEDULE_TYPE SCHEDULE_TYPE																				");
		
		stringBuilderQry.append(" FROM MECHANISM_OPERATION MO, SETTLEMENT_OPERATION SO, NEGOTIATION_MODALITY NM	,								");
		
		stringBuilderQry.append("   OPERATION_SETTLEMENT OS, SETTLEMENT_PROCESS SP																");
		
		stringBuilderQry.append(" WHERE SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK												");
		stringBuilderQry.append("   AND NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK											");
		
		stringBuilderQry.append("   AND SP.ID_SETTLEMENT_PROCESS_PK = OS.ID_SETTLMENT_PROCESS_FK												");
		stringBuilderQry.append("   AND OS.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK											");
		
		stringBuilderQry.append(" 	AND SO.SETTLEMENT_SCHEMA = 2																				");
		stringBuilderQry.append(" 	AND SO.OPERATION_PART = 1 AND SO.OPERATION_STATE = 612														");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			stringBuilderQry.append("   AND TRUNC(SO.SETTLEMENT_DATE) = TO_DATE(:date,'DD/MM/YYYY')												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			stringBuilderQry.append("   AND SP.SCHEDULE_TYPE = :scheludeType																	");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			stringBuilderQry.append("   AND  SO.SETTLEMENT_CURRENCY = :currency																		");
		}
		
		stringBuilderQry.append(" ) T																											");
		stringBuilderQry.append(" GROUP BY																										");
		stringBuilderQry.append("   PARTICIPANT,																								");
		stringBuilderQry.append("   CURRENCY,																									");
		stringBuilderQry.append("   DATE_S, SCHEDULE_TYPE																						");
		stringBuilderQry.append(" ORDER BY 1,2,3,4																								");		
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getScheduleType())){
			query.setParameter("scheludeType", new Integer(filter.getScheduleType().toString()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCurrency())){
			query.setParameter("currency", new Integer(filter.getCurrency().toString()));
		}
		
		List<Object[]>  lstResult = query.getResultList();
		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.TRUE);
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setCurrency(obj[1].toString());
			settlementReportTO.setDate(obj[2].toString());
			settlementReportTO.setScheduleType(obj[3].toString());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
	
	/**
	 * METODO PARA CONSULTAR LOS PARTICIPANTES A QUIEN SE LES ENVIARAN LOS REPORTES DE : 
	 * REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA REGISTRO DE OPERACIONES DIFERIDAS.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> netPositionsOperationsDefExRegQuery(SettlementReportTO filter) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		SettlementReportTO settlementReportTO;
		List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();
		
		stringBuilderQry.append(" SELECT																									");
		stringBuilderQry.append(" 	  HAO.ID_INCHARGE_STOCK_PARTICIPANT,																	");
		stringBuilderQry.append(" 	  TO_CHAR(SR.REGISTER_DATE,'DD/MM/YYYY')																");
		stringBuilderQry.append(" 	FROM HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO, SECURITY SE,									");
		stringBuilderQry.append(" 	  SETTLEMENT_DATE_OPERATION SDO, SETTLEMENT_REQUEST SR,													");
		stringBuilderQry.append(" 	  SETTLEMENT_OPERATION SO, SETTLEMENT_ACCOUNT_OPERATION SAO, HOLDER_ACCOUNT HA, NEGOTIATION_MODALITY NM	");
		stringBuilderQry.append(" 	WHERE SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK										");
		stringBuilderQry.append(" 		AND MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK									");
		stringBuilderQry.append(" 		AND HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK							");	
		stringBuilderQry.append(" 		AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK												");
		stringBuilderQry.append(" 		AND SE.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK													");
		stringBuilderQry.append(" 		AND SDO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK									");
		stringBuilderQry.append(" 		AND SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK										");
		stringBuilderQry.append(" 	  AND MO.ID_NEGOTIATION_MODALITY_FK = NM.ID_NEGOTIATION_MODALITY_PK										");
		stringBuilderQry.append(" 		AND MO.ID_NEGOTIATION_MECHANISM_FK=1																");
		stringBuilderQry.append(" 		AND HAO.HOLDER_ACCOUNT_STATE= 625																	");
		stringBuilderQry.append(" 		AND HAO.OPERATION_PART = SO.OPERATION_PART															");
		stringBuilderQry.append(" 		AND SO.IND_EXTENDED=1																				");
		stringBuilderQry.append(" 	  AND TRUNC(SR.REGISTER_DATE) = TO_DATE(:date,'DD/MM/YYYY')												");
		stringBuilderQry.append(" 	ORDER BY 1,2																							");
		
		Query query = em.createNativeQuery(stringBuilderQry.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDate())){
			query.setParameter("date", filter.getDate());
		}
		List<Object[]>  lstResult = query.getResultList();
		
		for(Object[] obj : lstResult){
			settlementReportTO = new SettlementReportTO();
			settlementReportTO.setAllUser(Boolean.TRUE);
			settlementReportTO.setParticipant(obj[0].toString());
			settlementReportTO.setDate(obj[1].toString());
			lstSettlementReportTO.add(settlementReportTO);
		}
		return lstSettlementReportTO;
	}
}