package com.pradera.settlements.prepaidextended.service;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantOperationStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementDateAccount;
import com.pradera.model.settlement.SettlementDateMarketfact;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.SettlementAnticipationType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementExchangeType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.negotiations.accountassignment.service.AccountAssignmentService;
import com.pradera.negotiations.operations.service.MechanismOperationService;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.currencyexchange.to.SettlementRequestDetailTO;
import com.pradera.settlements.currencyexchange.to.SettlementRequestTO;
import com.pradera.settlements.prepaidextended.to.RegisterPrepaidExtendedTO;
import com.pradera.settlements.prepaidextended.to.SearchPrepaidExtendedTO;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrepaidExtendedService.
 *
 * @author Pradera Technologies
 */
@Stateless
public class PrepaidExtendedService extends CrudDaoServiceBean {

	/** The mechanism operation service. */
	@EJB 
	MechanismOperationService mechanismOperationService;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The account assignment service. */
	@EJB
	private AccountAssignmentService accountAssignmentService;
	
	/** The notification service. */
	@EJB
	private NotificationServiceBean notificationService;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	public Participant getOneParticipant(Long idParticipant){
		Participant p=null;
		p=em.find(Participant.class, idParticipant);
		return p;
	}
	
	/**
	 * METODO PARA CONSULTAR SOLICITUDES DE AMP/ANT
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementRequestTO> searchSettlementDateRequest(SearchPrepaidExtendedTO filter) throws ServiceException{
		Map<String, Object> parameters =new HashMap<String, Object>();
		StringBuilder stringBuilderQry= new StringBuilder();
		stringBuilderQry.append("select sr.idSettlementRequestPk, sr.requestType, "); // 0 1 
		stringBuilderQry.append("       (select pt1.parameterName from ParameterTable pt1 where pt1.parameterTablePk = sr.requestType), "); //2
		stringBuilderQry.append("       sr.requestState, ");//3
		stringBuilderQry.append("       (select pt2.parameterName from ParameterTable pt2 where pt2.parameterTablePk = sr.requestState), ");//4
		stringBuilderQry.append("       mo.operationDate,");//5
		stringBuilderQry.append("       mo.securities.idSecurityCodePk,");//6
		stringBuilderQry.append("       mo.currency,");//7
		stringBuilderQry.append("       (select pt3.parameterName from ParameterTable pt3 where pt3.parameterTablePk = sdo.settlementCurrency), ");//8
		stringBuilderQry.append("       sdo.idSettlementDateOperationPk,");//9
		stringBuilderQry.append("       sdo.settlementDate,");//10
		stringBuilderQry.append("       sdo.settlementAmount,");//11
		stringBuilderQry.append("       sdo.stockQuantity, ");//12
		stringBuilderQry.append("       spart, ");//13
		stringBuilderQry.append("       tpart, ");//14
		stringBuilderQry.append("       mo.operationNumber,  ");//15
		stringBuilderQry.append("       mo.ballotNumber, ");//16
		stringBuilderQry.append("       mo.sequential, ");//17
		stringBuilderQry.append("       sdo.operationPart, ");//18
		stringBuilderQry.append("       sr.participantRole, ");//19
		stringBuilderQry.append("       sr.anticipationType, ");//20
		stringBuilderQry.append("       (select pt3.parameterName from ParameterTable pt3 where pt3.parameterTablePk = sr.anticipationType), ");//21
		stringBuilderQry.append("       sr.registerDate ");//22
		stringBuilderQry.append("from SettlementDateOperation sdo ");
		stringBuilderQry.append("	inner join sdo.settlementRequest sr ");
		stringBuilderQry.append("	inner join sdo.settlementOperation so ");
		stringBuilderQry.append("	inner join so.mechanismOperation mo ");
		stringBuilderQry.append("	inner join sr.sourceParticipant spart ");
		stringBuilderQry.append("	left join sr.targetParticipant tpart ");
		stringBuilderQry.append("	where mo.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk =:idNegotiationMechanism ");
		stringBuilderQry.append("	and trunc(sr.registerDate) between :initialDate and :endDate ");
		stringBuilderQry.append("	and (  sr.sourceParticipant.idParticipantPk =:idParticipantPk or sr.targetParticipant.idParticipantPk =:idParticipantPk ");
		stringBuilderQry.append("		   or ( sr.sourceParticipant.idParticipantPk =:idParticipantPk and sr.targetParticipant.idParticipantPk is null ) ) ");
		
		if (filter.getIdNegotiationModalityPk()!=null){
			stringBuilderQry.append("	and mo.mechanisnModality.negotiationModality.idNegotiationModalityPk =:idNegotiationModality ");
			parameters.put("idNegotiationModality", filter.getIdNegotiationModalityPk());
		}
		
		if (filter.getBallotNumberSelected()!=null){
			stringBuilderQry.append("	and mo.ballotNumber =:ballotNumberSelected");
			parameters.put("ballotNumberSelected", filter.getBallotNumberSelected());
		}
		if (filter.getSequentialSelected()!=null){
			stringBuilderQry.append("	and mo.sequential =:sequentialSelected ");
			parameters.put("sequentialSelected", filter.getSequentialSelected());
		}
		if (filter.getSecuritie()!=null && filter.getSecuritie().getIdSecurityCodePk()!=null ){
			stringBuilderQry.append("	and mo.securities.idSecurityCodePk = :idSecurityCodeFkSelected ");
			parameters.put("idSecurityCodeFkSelected", filter.getSecuritie().getIdSecurityCodePk());	
		}
		if(filter.getRequestTypeSelected()!=null){
			stringBuilderQry.append(" and sr.requestType = :requestType ");
			parameters.put("requestType", filter.getRequestTypeSelected());
		}
		if(filter.getIdSettlementRequestDatePk()!=null){
			stringBuilderQry.append(" and sr.idSettlementRequestPk = :requestNumber ");
			parameters.put("requestNumber", filter.getIdSettlementRequestDatePk());
		}	
		if (filter.getStateSelected()!=null){
			stringBuilderQry.append(" and sr.requestState = :state");
			parameters.put("state", filter.getStateSelected());
		}
		stringBuilderQry.append("   order by sr.idSettlementRequestPk  DESC  ");
		parameters.put("idParticipantPk", filter.getIdParticipantPk());
		parameters.put("idNegotiationMechanism", filter.getIdNegotiationMechanismPk());
		parameters.put("initialDate", filter.getInitialDate());
		parameters.put("endDate", filter.getEndDate());
		List<Object[]> lstResult = findListByQueryString(stringBuilderQry.toString(), parameters);
		Map<Long,SettlementRequestTO> mapSettlementRequests = new LinkedHashMap<Long, SettlementRequestTO>();
		for(Object[] objResult : lstResult){
			Long idSettlementRequest = (Long)objResult[0];
			SettlementRequestTO prepaidExtendedTO = mapSettlementRequests.get(idSettlementRequest);
			if(prepaidExtendedTO == null){
				prepaidExtendedTO = new SettlementRequestTO();
				prepaidExtendedTO.setSettlementRequestDetail(new ArrayList<SettlementRequestDetailTO>());
				prepaidExtendedTO.setIdSettlementRequest(idSettlementRequest);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ;;;;;;;;;;;;;;;;;;;;;                                                                                                                                                                                                     
				prepaidExtendedTO.setRequestType((Integer)objResult[1]);
				prepaidExtendedTO.setRequestTypeDescription((String)objResult[2]);
				prepaidExtendedTO.setRequestState((Integer)objResult[3]);
				prepaidExtendedTO.setStateDescription((String)objResult[4]);
				prepaidExtendedTO.setSourceParticipant((Participant)objResult[13]);
				prepaidExtendedTO.setTargetParticipant((Participant)objResult[14]);
				prepaidExtendedTO.setParticipantRole((Integer)objResult[19]);
				prepaidExtendedTO.setAnticipationType((Integer)objResult[20]);
				prepaidExtendedTO.setAnticipationTypeDescription((String)objResult[21]);
				prepaidExtendedTO.setRegisterDate((Date)objResult[22]);
				if(ComponentConstant.SALE_ROLE.equals(prepaidExtendedTO.getParticipantRole())){
					prepaidExtendedTO.setCounterPartRole(ComponentConstant.PURCHARSE_ROLE);
				}else if(ComponentConstant.PURCHARSE_ROLE.equals(prepaidExtendedTO.getParticipantRole())){
					prepaidExtendedTO.setCounterPartRole(ComponentConstant.SALE_ROLE);
				}
				mapSettlementRequests.put(idSettlementRequest, prepaidExtendedTO);
			}
			
			SettlementRequestDetailTO settlementRequestDetailTO = new SettlementRequestDetailTO();
			settlementRequestDetailTO.setOperationDate((Date)objResult[5]);
			settlementRequestDetailTO.setIdSecurityCode((String)objResult[6]);
			settlementRequestDetailTO.setCurrency((Integer)objResult[7]);
			settlementRequestDetailTO.setCurrencyDescription((String)objResult[8]);
			settlementRequestDetailTO.setIdSettlementDateOperation((Long)objResult[9]);
			settlementRequestDetailTO.setSettlementDate((Date)objResult[10]);
			settlementRequestDetailTO.setSettlementAmount((BigDecimal)objResult[11]);
			settlementRequestDetailTO.setStockQuantity((BigDecimal)objResult[12]);
			settlementRequestDetailTO.setOperationNumber((Long)objResult[15]);
			settlementRequestDetailTO.setBallotNumber((Long)objResult[16]);
			settlementRequestDetailTO.setSequential((Long)objResult[17]);
			settlementRequestDetailTO.setOperationPart((Integer)objResult[18]);
			prepaidExtendedTO.getSettlementRequestDetail().add(settlementRequestDetailTO);
			
		}
		
		return new ArrayList<SettlementRequestTO>(mapSettlementRequests.values());
	}
	
	/**
	 * Search mechanism operations.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	/**
	 * METODO PARA BUSCAR ANTES DE REGISTRAR
	 * @param filter
	 * @return
	 * @throws ServiceException
	 */
	
	public List<SettlementOperation> searchMechanismOperations(RegisterPrepaidExtendedTO filter) throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilderQry = new StringBuilder();
		stringBuilderQry.append("   select so from SettlementOperation SO ");
		stringBuilderQry.append("	join fetch so.mechanismOperation MO ");
		stringBuilderQry.append("	join fetch mo.securities ");
		stringBuilderQry.append("	join fetch mo.buyerParticipant ");
		stringBuilderQry.append("	join fetch mo.sellerParticipant ");
		stringBuilderQry.append("	join fetch mo.mechanisnModality mm ");
		stringBuilderQry.append("	join fetch mm.negotiationMechanism nme ");
		stringBuilderQry.append("	join fetch mm.negotiationModality nm ");
		stringBuilderQry.append("	where mo.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanism ");
		
		if(filter.getIdNegotiationModalityPk()!=null){
			parameters.put("idNegotiationModality", filter.getIdNegotiationModalityPk());
			stringBuilderQry.append("	and mo.mechanisnModality.negotiationModality.idNegotiationModalityPk = :idNegotiationModality ");
		}
		
		if (filter.getBallotNumber()!=null){
			stringBuilderQry.append(" and MO.ballotNumber =:ballotNumber ");
			parameters.put("ballotNumber", filter.getBallotNumber());
		}
		
		if (filter.getSequential()!=null){
			stringBuilderQry.append(" and MO.sequential =:sequential");
			parameters.put("sequential", filter.getSequential());
		}
		
		if (filter.getOperationDate()!=null){
			stringBuilderQry.append(" and so.settlementDate = :settlementDate ");
			parameters.put("settlementDate", filter.getOperationDate());
		}
		
		if (filter.getSecuritie()!=null && filter.getSecuritie().getIdSecurityCodePk()!=null ){
			stringBuilderQry.append("	and mo.securities.idSecurityCodePk = :idSecurityCode ");
			parameters.put("idSecurityCode", filter.getSecuritie().getIdSecurityCodePk());	
		}
		
		if (filter.getRequestTypeSelected().equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode())){
			stringBuilderQry.append(" and mo.indTermSettlement = :indicatorOne ");
			stringBuilderQry.append(" and so.indPartial != :indicatorOne ");
			stringBuilderQry.append(" and so.operationPart = :termPart ");
			stringBuilderQry.append(" and so.settlementDate >= :currentDate ");
			
			parameters.put("currentDate", CommonsUtilities.currentDate());
			parameters.put("indicatorOne", ComponentConstant.ONE);
			
			if(filter.getAnticipationTypeSelected()!=null && filter.getAnticipationTypeSelected().equals(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode())){
				stringBuilderQry.append(" and mo.securities.securityClass != :dpfType ");
				parameters.put("dpfType", SecurityClassType.DPF.getCode());
			}
			
		}else if (filter.getRequestTypeSelected().equals(SettlementRequestType.SETTLEMENT_EXTENSION.getCode())){
			stringBuilderQry.append(" and so.indExtended != :indicatorOne  ");
			stringBuilderQry.append(" and so.indSettlementExchange != :indicatorOne  ");
			
			parameters.put("indicatorOne", ComponentConstant.ONE);
			
		}else if(filter.getRequestTypeSelected().equals(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode()) || 
				filter.getRequestTypeSelected().equals(SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode())){
			
			stringBuilderQry.append("  and SO.settlementType =:settlementType ");
			
			if(filter.getRequestTypeSelected().equals(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode())){
				stringBuilderQry.append("  and so.indPrepaid != :indicatorOne ");
				stringBuilderQry.append("  and so.indPartial != :indicatorOne ");
				stringBuilderQry.append("  and so.operationPart = :termPart ");
				stringBuilderQry.append("  and SO.indExtended != :indicatorOne ");	
						
				parameters.put("indicatorOne", ComponentConstant.ONE);
			}
			
			if( SettlementExchangeType.DVP_TO_FOP.getCode().equals(filter.getSettlementExchangeType())){
				parameters.put("settlementType", SettlementType.DVP.getCode());
			}else if( SettlementExchangeType.FOP_TO_DVP.getCode().equals(filter.getSettlementExchangeType())){
				parameters.put("settlementType", SettlementType.FOP.getCode());
			}
		}
		stringBuilderQry.append(" and ( 	 (SO.operationPart = :cashPart and SO.operationState in :cashStates ");
		if (ComponentConstant.PURCHARSE_ROLE.equals(filter.getRoleSelected())){
			stringBuilderQry.append("    and ( mo.buyerParticipant.idParticipantPk=:idParticipant ");
			if(filter.getIdCounterPartParticipant()!=null){
				stringBuilderQry.append("     and mo.sellerParticipant.idParticipantPk=:idCounterpartParticipant  ");
				parameters.put("idCounterpartParticipant", filter.getIdCounterPartParticipant());
			}
			stringBuilderQry.append("  ) ");
		}else if (ComponentConstant.SALE_ROLE.equals(filter.getRoleSelected())){
			stringBuilderQry.append("    and ( mo.sellerParticipant.idParticipantPk=:idParticipant  ");
			if(filter.getIdCounterPartParticipant()!=null){
				stringBuilderQry.append("     and mo.buyerParticipant.idParticipantPk=:idCounterpartParticipant ");
				parameters.put("idCounterpartParticipant", filter.getIdCounterPartParticipant());
			}
			stringBuilderQry.append("  ) ");
		}
		stringBuilderQry.append("       ) or (SO.operationPart = :termPart and SO.operationState in :termStates ");
		if (ComponentConstant.PURCHARSE_ROLE.equals(filter.getRoleSelected())){
			stringBuilderQry.append("    and ( mo.sellerParticipant.idParticipantPk=:idParticipant  ");
			if(filter.getIdCounterPartParticipant()!=null){
				stringBuilderQry.append("     and mo.buyerParticipant.idParticipantPk=:idCounterpartParticipant ");
				parameters.put("idCounterpartParticipant", filter.getIdCounterPartParticipant());
			}
			stringBuilderQry.append("  ) ");
		}else if (ComponentConstant.SALE_ROLE.equals(filter.getRoleSelected())){
			stringBuilderQry.append("    and ( mo.buyerParticipant.idParticipantPk=:idParticipant  ");
			if(filter.getIdCounterPartParticipant()!=null){
				stringBuilderQry.append("     and mo.sellerParticipant.idParticipantPk=:idCounterpartParticipant ");
				parameters.put("idCounterpartParticipant", filter.getIdCounterPartParticipant());
			}
			stringBuilderQry.append("  ) ");
		}
		stringBuilderQry.append("            )  ) ");
		
		parameters.put("idParticipant", filter.getIdParticipant());
		
		stringBuilderQry.append("   order by mo.operationDate DESC  ");
		
		parameters.put("idParticipant", filter.getIdParticipant());
		
		
		List<Integer> lstCashStates = new ArrayList<Integer>();
		lstCashStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstCashStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		
		List<Integer> lstTermStates = new ArrayList<Integer>();
		lstTermStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		
		parameters.put("cashStates", lstCashStates);
		parameters.put("termStates", lstTermStates);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("termPart", ComponentConstant.TERM_PART);
		parameters.put("idNegotiationMechanism", filter.getIdNegotiationMechanismPk());			
		
		return (List<SettlementOperation>)findListByQueryString(stringBuilderQry.toString(), parameters);
	}

	/**
	 * Metodo para verificar si una operacion fue registrada en selid
	 * @param idMechanismOperationPk
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public Boolean searchMechanismOperationsSelid(Long idMechanismOperationPk) throws ServiceException{
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  SELECT                                                                                                 ");
		querySql.append("  MO.ID_MECHANISM_OPERATION_PK                                                                           ");
		querySql.append("  from SETTLEMENT_DATE_OPERATION SDO                                                                     ");
		querySql.append("  inner join SETTLEMENT_REQUEST SR on SDO.ID_SETTLEMENT_REQUEST_FK = SR.ID_SETTLEMENT_REQUEST_PK         ");
		querySql.append("  inner join SETTLEMENT_OPERATION SO on SDO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK   ");
		querySql.append("  inner join MECHANISM_OPERATION MO on SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK       ");
		querySql.append("  where 1 = 1                                                                                            ");
		querySql.append("  and MO.ID_NEGOTIATION_MECHANISM_FK = 1                                                                 "); //Negociado en Bolsa
		querySql.append("  AND MO.ID_NEGOTIATION_MODALITY_FK in (3,4)                                                             "); //Reporto RF o RV
		querySql.append("  AND MO.OPERATION_STATE = 615                                                                           "); //Liquidada Contado
		querySql.append("  and SR.REQUEST_TYPE = 2018                                                                             "); //Selid
		querySql.append("  AND SR.REQUEST_STATE = 1543                                                                            "); //Selid Autorizado
		querySql.append("  AND MO.ID_MECHANISM_OPERATION_PK =:idMechanismOperationPk 						                      ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		
		List<Long> result = query.getResultList();
		
		if(result.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * METODO PARA ADQUIRIR LA LISTA DE PARTICIPANTES SEGUN EL MECANISMO DE NEGOCIACION Y LA MODALIDAD.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 * @return the lst participants
	 * @throws ServiceException the service exception
	 */
	
	@SuppressWarnings("unchecked")
	public List<Participant> getLstParticipants(Long idNegotiationMechanismPk, Long idNegotiationModalityPk) throws ServiceException{
		String strQuery = "SELECT p" +
							"	FROM Participant p" +
							"	WHERE p.idParticipantPk IN " +
							"	(SELECT pm.participant.idParticipantPk" +
							"		FROM ParticipantMechanism pm" +
							"		WHERE pm.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk" +
							"		AND pm.mechanismModality.id.idNegotiationModalityPk = :idNegotiationModalityPk" +
							"		AND pm.stateParticipantMechanism = :stateParticipantMechanism" +
							"		AND pm.mechanismModality.stateMechanismModality = :stateMechanismModality" +							
							"		GROUP BY pm.participant.idParticipantPk) " +
							"	AND p.state = :state" +
							"	ORDER BY p.description ASC";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		parameters.put("stateMechanismModality", MechanismModalityStateType.ACTIVE.getCode());
		parameters.put("idNegotiationMechanismPk", idNegotiationMechanismPk);
		parameters.put("idNegotiationModalityPk", idNegotiationModalityPk);
		parameters.put("state", ParticipantStateType.REGISTERED.getCode());
		return (List<Participant>)findListByQueryString(strQuery, parameters);
	}

	/**
	 * METODO PARA ADQUIRIR LA OPERACION SEGUN LA MODALIDAD.
	 *
	 * @param registerPrepaidExtendedTO the register prepaid extended to
	 * @return the dates settlement for modality
	 */
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getDatesSettlementForModality(RegisterPrepaidExtendedTO registerPrepaidExtendedTO){
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("SELECT NVL(mm.negotiationModality.cashMaxSettlementDays, 0),");	//0
		strQuery.append("		NVL(mm.negotiationModality.termMaxSettlementDays, 0),");	//1
		strQuery.append("		mm.negotiationModality.indCashCallendar,");					//2
		strQuery.append("		mm.negotiationModality.indTermCallendar,");					//3
		strQuery.append("		NVL(mm.negotiationModality.cashMinSettlementDays, 0),");	//4
		strQuery.append("		NVL(mm.negotiationModality.termMinSettlementDays, 0)");		//5
		strQuery.append("		FROM MechanismModality mm");
		strQuery.append("		WHERE mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		strQuery.append("		AND mm.id.idNegotiationModalityPk = :idNegotiationModalityPk");
		strQuery.append("		AND mm.stateMechanismModality = :stateMechanismModality");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("stateMechanismModality", MechanismModalityStateType.ACTIVE.getCode());
		parameters.put("idNegotiationMechanismPk", registerPrepaidExtendedTO.getIdNegotiationMechanismPk());
		parameters.put("idNegotiationModalityPk", registerPrepaidExtendedTO.getIdNegotiationModalityPk());
		return (List<Object[]>)findListByQueryString(strQuery.toString(), parameters);
	}
//
//	/**
//	 * METODO PARA ADQUIRIR EL PK DE LA SOLICITUD A REGISTRAR
//	 * @param idSettlementDateRequestPk
//	 * @return
//	 */
//	
//	public SettlementRequest getSettlementDateRequestByPk(Long idSettlementRequestPk) {
//		try {
//			StringBuilder sbQuery = new StringBuilder();
//			sbQuery.append("SELECT sr ");
//			sbQuery.append("	FROM SettlementRequest sr");
//			sbQuery.append("	WHERE sr.idSettlementRequestPk = :idSettlementRequestPk");
//			Query query = em.createQuery(sbQuery.toString());
//			query.setParameter("idSettlementRequestPk", idSettlementRequestPk);
//			return (SettlementRequest)query.getSingleResult();
//		} catch (NoResultException e) {
//			e.printStackTrace();
//			return null;
//		}		
//	}
	
	/**
 * Gets the settlement date operations.
 *
 * @param idSettlementRequest the id settlement request
 * @return the settlement date operations
 */
@SuppressWarnings("unchecked")
	public List<SettlementDateOperation> getSettlementDateOperations(Long idSettlementRequest) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT sdo ");
		sbQuery.append("	FROM SettlementDateOperation sdo ");
		sbQuery.append("	JOIN FETCH sdo.settlementOperation so ");
		sbQuery.append("	JOIN FETCH so.mechanismOperation mo ");
		sbQuery.append("	JOIN FETCH mo.mechanisnModality mm");
		sbQuery.append("	JOIN FETCH mm.negotiationMechanism nme");
		sbQuery.append("	JOIN FETCH mm.negotiationModality nmo");
		sbQuery.append("	JOIN FETCH mo.securities ");
		sbQuery.append("	JOIN FETCH mo.sellerParticipant ");
		sbQuery.append("	JOIN FETCH mo.buyerParticipant ");
		sbQuery.append("	JOIN FETCH sdo.settlementRequest ");
		sbQuery.append("	WHERE sdo.settlementRequest.idSettlementRequestPk = :idSettlementRequestPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementRequestPk", idSettlementRequest);
		return (List<SettlementDateOperation>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the settlement date accounts.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idParticipant the id participant
	 * @return the settlement date accounts
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementDateAccount> getSettlementDateAccounts(Long idSettlementOperation, Long idParticipant) {
		
		List<Object[]> accountOperations = null;
		List<SettlementDateAccount> settlementDateAccounts = new ArrayList<SettlementDateAccount>();
		Map<Long,HolderAccount> accounts = new HashMap<Long, HolderAccount>();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT  sda , hao.holderAccount.idHolderAccountPk ");
		sbQuery.append("	FROM SettlementDateAccount sda ");
		sbQuery.append("	inner join fetch sda.settlementAccountOperation sao ");
		sbQuery.append("	inner join fetch sao.holderAccountOperation hao ");
		sbQuery.append("	WHERE sda.settlementDateOperation.idSettlementDateOperationPk = :idSettlementDateOperation ");
		if (Validations.validateIsNotNull(idParticipant)) {
			sbQuery.append(" and hao.inchargeStockParticipant.idParticipantPk = :idParticipant ");
		}
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementDateOperation", idSettlementOperation);
		if (Validations.validateIsNotNull(idParticipant)) {
			parameters.put("idParticipant", idParticipant);
		}
		
		accountOperations = (List<Object[]>) findListByQueryString(sbQuery.toString(), parameters);
		
		for(Object[] accOperation: accountOperations){
			SettlementDateAccount settlementDateAccount = (SettlementDateAccount) accOperation[0];
			Long idHolderAccount = (Long) accOperation[1];
			HolderAccount holderAccount = accounts.get(idHolderAccount);
			if(holderAccount==null){
				holderAccount = mechanismOperationService.getHolderAccount(idHolderAccount);
				accounts.put(idHolderAccount, holderAccount);
			}
			settlementDateAccount.getSettlementAccountOperation().getHolderAccountOperation().setHolderAccount(holderAccount);
			settlementDateAccounts.add(settlementDateAccount);
		}
		
		return settlementDateAccounts;
		
	}

	/**
	 * METODO PARA VERIFICAR SI UNA SOLICITUD YA EXISTE.
	 *
	 * @param idSettlementOperationPk the id settlement operation pk
	 * @param requestType the request type
	 * @throws ServiceException the service exception
	 */
	
	@SuppressWarnings("unchecked")
	public void checkPreviousDateRequest(Long idSettlementOperationPk, Integer requestType) throws ServiceException{
		ErrorServiceType errorType = null;
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT sr.idSettlementRequestPk,  ");
		sbQuery.append("    (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = sr.requestType), ");
		sbQuery.append("    mo.ballotNumber, mo.sequential, mo.operationDate, mo.operationNumber ");
		sbQuery.append("	FROM SettlementDateOperation sdo inner join sdo.settlementRequest sr inner join sdo.settlementOperation so ");
		sbQuery.append("	     inner join sdo.settlementOperation so inner join so.mechanismOperation mo ");
		sbQuery.append("	WHERE so.idSettlementOperationPk = :idSettlementOperationPk ");
		sbQuery.append("	AND sr.requestState not in (:state)");
		sbQuery.append("	AND sr.requestType in (:requestType) ");
		
		List<Integer> lstState = new ArrayList<Integer>();
		lstState.add(SettlementDateRequestStateType.REJECTED.getCode());
		lstState.add(SettlementDateRequestStateType.ANNULATE.getCode());
		if(requestType.equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode())){
			sbQuery.append("	AND trunc(sr.registerDate)  = :currentDate ");
			parameters.put("currentDate", CommonsUtilities.currentDate());
			errorType = ErrorServiceType.SETTLEMENT_DATE_REQUEST_ANTICIPATION_DUPLICATE;
		}else{
			errorType = ErrorServiceType.SETTLEMENT_DATE_REQUEST_DUPLICATE;
		}
		sbQuery.append("	order by sr.requestType ");
		
		List<Integer> lstRequestType = new ArrayList<Integer>();
		lstRequestType.add(requestType);
		
		if(requestType.equals(SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode())){
			lstRequestType.add(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode());
		}
		
		parameters.put("idSettlementOperationPk", idSettlementOperationPk);
		parameters.put("state", lstState);
		parameters.put("requestType", lstRequestType);
		
		List<Object[]> resultList = (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
		if(Validations.validateListIsNotNullAndNotEmpty(resultList)){
			Object[] firstResult = resultList.get(0);
			String requestTypeDescription = (String)firstResult[1];
			
			StringBuffer stringBuffer = new StringBuffer();
			Long ballotNumber = (Long)firstResult[2];
			Long sequential = (Long)firstResult[3];
			if(ballotNumber!=null && sequential !=null){
				stringBuffer.append(ballotNumber).append(GeneralConstants.SLASH).append(sequential);
			}else{
				Long operationNumber = (Long)firstResult[4];
				Date date = (Date)firstResult[5];
				stringBuffer.append(CommonsUtilities.convertDatetoString(date)).append(GeneralConstants.DASH).append(operationNumber);
			}
			throw new ServiceException(errorType, new Object[]{stringBuffer.toString(),requestTypeDescription});
		}
	}	
	
	/**
	 * Gets the settlement date marketfacts.
	 *
	 * @param idSettlementDateAccount the id settlement date account
	 * @return the settlement date marketfacts
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementDateMarketfact> getSettlementDateMarketfacts(Long idSettlementDateAccount) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select sdm "); // ID Mechanism operation
		stringBuffer.append(" from SettlementDateMarketfact sdm where ");
		stringBuffer.append(" sdm.settlementDateAccount.idSettlementDateAccountPk = :idSettlementDateAccount "); 
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementDateAccount", idSettlementDateAccount);
		
		return findListByQueryString(stringBuffer.toString(), parameters);
	}

	/**
	 * Creates the settlement date operation.
	 *
	 * @param settlementDateOperation the settlement date operation
	 * @throws ServiceException the service exception
	 */
	public void createSettlementDateOperation(SettlementDateOperation settlementDateOperation) throws ServiceException {
		SettlementOperation settlementOperation = settlementDateOperation.getSettlementOperation();
		
		//populate and create new SettlementOperation
		SettlementOperation newSettlementOperation = populateSettlementOperation(settlementDateOperation);
		create(newSettlementOperation);
		
		//update stock and amount on original SettlementOperation
		BigDecimal remainingQuantity = settlementOperation.getStockQuantity().subtract(settlementDateOperation.getStockQuantity());
		settlementOperation.setStockQuantity(remainingQuantity);
		settlementOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), remainingQuantity));
		update(settlementOperation);
		
		Map<Long,BigDecimal> mapSaleParticipant = new HashMap<Long, BigDecimal>();
		Map<Long,BigDecimal> mapPurchaseParticipant = new HashMap<Long, BigDecimal>();
		
		for(SettlementDateAccount settlementDateAccount : settlementDateOperation.getSettlementDateAccounts()){
			
			SettlementAccountOperation settlementAccountOperation = settlementDateAccount.getSettlementAccountOperation();
			
			//populate and create new SettlementAccountOperation
			SettlementAccountOperation newSettlementAccountOperation = populateSettlementAccountOperation(settlementDateAccount,newSettlementOperation);
			create(newSettlementAccountOperation);
			saveAll(newSettlementAccountOperation.getSettlementAccountMarketfacts());

			//update stock and amount on original SettlementAccountOperation
			BigDecimal remainingAccQuantity = settlementAccountOperation.getStockQuantity().subtract(settlementDateAccount.getStockQuantity());
			if(remainingAccQuantity.compareTo(BigDecimal.ZERO) == 0 ){
				settlementAccountOperation.setOperationState(HolderAccountOperationStateType.PREPAID.getCode());
			}
			settlementAccountOperation.setStockQuantity(remainingAccQuantity);
			settlementAccountOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), remainingAccQuantity));
			
			update(settlementAccountOperation);
			
			//create and update new and original MarketFacts
			for(SettlementDateMarketfact settlementDateMarketfact : settlementDateAccount.getSettlementDateMarketfacts()){
				Date marketDate = settlementDateMarketfact.getMarketDate();
				BigDecimal marketRate = settlementDateMarketfact.getMarketRate();
				BigDecimal marketPrice = settlementDateMarketfact.getMarketPrice();
				SettlementAccountMarketfact settlementAccountMarketfact =	settlementProcessService.getSettlementAccountMarketfacts(
							settlementAccountOperation.getIdSettlementAccountPk(), marketDate, marketRate, marketPrice);
				if(settlementAccountMarketfact!=null){
					settlementAccountMarketfact.setMarketQuantity(settlementAccountMarketfact.getMarketQuantity().subtract(settlementDateMarketfact.getMarketQuantity()));
					if(settlementAccountMarketfact.getMarketQuantity().compareTo(BigDecimal.ZERO) == 0){
						settlementAccountMarketfact.setIndActive(ComponentConstant.ZERO);
					}
					update(settlementAccountMarketfact);
				}
			}
			
			//gather ParticipantSettlements by accounts
			Long idFundsParticipant = getParticipantOnAccount(newSettlementAccountOperation.getIdSettlementAccountPk());
			if(ComponentConstant.SALE_ROLE.equals(settlementAccountOperation.getRole())){
				BigDecimal stockQuantity = mapSaleParticipant.get(idFundsParticipant);
				if (stockQuantity == null){
					stockQuantity = BigDecimal.ZERO;
				}
				mapSaleParticipant.put(idFundsParticipant, stockQuantity.add(newSettlementAccountOperation.getStockQuantity()));
			}else if (ComponentConstant.PURCHARSE_ROLE.equals(settlementAccountOperation.getRole())){
				BigDecimal stockQuantity = mapPurchaseParticipant.get(idFundsParticipant);
				if (stockQuantity == null){
					stockQuantity = BigDecimal.ZERO;
				}
				mapPurchaseParticipant.put(idFundsParticipant, stockQuantity.add(newSettlementAccountOperation.getStockQuantity()));
			}
		}
		
		//sale role ParticipantSettlement
		for (Entry<Long, BigDecimal> entry : mapSaleParticipant.entrySet()){
		    Long idParticipant = entry.getKey();
		    BigDecimal stockQuantity = entry.getValue();
		    
		    // update original ParticipantSettlement
		    ParticipantSettlement initialPartSettlement = (ParticipantSettlement) settlementProcessService.getParticipantSettlements(
		    		settlementOperation.getIdSettlementOperationPk(), ComponentConstant.SALE_ROLE, idParticipant);
		    if(initialPartSettlement!=null){
		    	initialPartSettlement.setStockQuantity(initialPartSettlement.getStockQuantity().subtract(stockQuantity));
		    	initialPartSettlement.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), initialPartSettlement.getStockQuantity()));
		    	if(initialPartSettlement.getStockQuantity().compareTo(BigDecimal.ZERO) == 0){
		    		initialPartSettlement.setOperationState(ParticipantOperationStateType.PREPAID.getCode());
		    	}
		    	update(initialPartSettlement);
		    }
		    
		    // create new ParticipantSettlement
		    ParticipantSettlement participantSettlement = settlementProcessService.populateParticipantSettlement(
		    		newSettlementOperation, stockQuantity, idParticipant, ComponentConstant.SALE_ROLE, ComponentConstant.ZERO);
		    create(participantSettlement);
		}
		
		//purchase role ParticipantSettlement
		for (Entry<Long, BigDecimal> entry : mapPurchaseParticipant.entrySet()){
		    Long idParticipant = entry.getKey();
		    BigDecimal stockQuantity = entry.getValue();
		    
		    // update original ParticipantSettlement
		    ParticipantSettlement initialPartSettlement = (ParticipantSettlement) settlementProcessService.getParticipantSettlements(
		    		settlementOperation.getIdSettlementOperationPk(), ComponentConstant.PURCHARSE_ROLE, idParticipant);
		    if(initialPartSettlement!=null){
		    	initialPartSettlement.setStockQuantity(initialPartSettlement.getStockQuantity().subtract(stockQuantity));
		    	initialPartSettlement.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), initialPartSettlement.getStockQuantity()));
		    	if(initialPartSettlement.getStockQuantity().compareTo(BigDecimal.ZERO) == 0){
		    		initialPartSettlement.setOperationState(ParticipantOperationStateType.PREPAID.getCode());
		    	}
		    	update(initialPartSettlement);
		    }
		    
		 // create new ParticipantSettlement
		    ParticipantSettlement participantSettlement = settlementProcessService.populateParticipantSettlement(
		    		newSettlementOperation, stockQuantity, idParticipant, ComponentConstant.PURCHARSE_ROLE, ComponentConstant.ZERO);
		    create(participantSettlement);
		}
		
	}
	
	
	/**
	 * Gets the participant on account.
	 *
	 * @param idSettlementAccount the id settlement account
	 * @return the participant on account
	 */
	private Long getParticipantOnAccount(Long idSettlementAccount) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select  ");
		stringBuffer.append(" hao.inchargeFundsParticipant.idParticipantPk ");
		stringBuffer.append(" from SettlementAccountOperation sao ");
		stringBuffer.append(" inner join sao.holderAccountOperation hao ");
		stringBuffer.append(" where sao.idSettlementAccountPk = :idSettlementAccount");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementAccount", idSettlementAccount);
		
		return (Long)findObjectByQueryString(stringBuffer.toString(), parameters);
	}

	/**
	 * Populate settlement operation.
	 *
	 * @param settlementDateOperation the settlement date operation
	 * @return the settlement operation
	 */
	private SettlementOperation populateSettlementOperation(SettlementDateOperation settlementDateOperation) {
		MechanismOperation mechanismOperation = settlementDateOperation.getSettlementOperation().getMechanismOperation();
		
		SettlementOperation settlementOperation = new SettlementOperation();
		settlementOperation.setSettlementDate(settlementDateOperation.getSettlementDate());
		settlementOperation.setInitialStockQuantity(settlementDateOperation.getStockQuantity());
		settlementOperation.setStockQuantity(settlementDateOperation.getStockQuantity());
		settlementOperation.setSettlementPrice(settlementDateOperation.getSettlementPrice());
		settlementOperation.setSettlementAmount(settlementDateOperation.getSettlementAmount());
		settlementOperation.setSettlementCurrency(settlementDateOperation.getSettlementCurrency());
		settlementOperation.setInitialSettlementCurrency(settlementOperation.getSettlementCurrency());
		settlementOperation.setInitialSettlementPrice(settlementOperation.getSettlementPrice());
		settlementOperation.setSettlementDays(settlementDateOperation.getSettlementDays());
		settlementOperation.setAmountRate(settlementDateOperation.getSettlementAmountRate());
		
		settlementOperation.setMechanismOperation(mechanismOperation);
		settlementOperation.setExchangeRate(mechanismOperation.getExchangeRate());
		settlementOperation.setIndPartial(ComponentConstant.ONE);
		settlementOperation.setIndPrepaid(ComponentConstant.ONE);
		//settlementOperation.setFundsReference(settlementDateOperation.getSettlementOperation().getFundsReference());
		settlementOperation.setStockReference(settlementDateOperation.getSettlementOperation().getStockReference());
		settlementOperation.setStockBlockDate(settlementDateOperation.getSettlementOperation().getStockBlockDate());
		settlementOperation.setOperationPart(settlementDateOperation.getOperationPart());
		settlementOperation.setOperationState(settlementDateOperation.getSettlementOperation().getOperationState());
		settlementOperation.setSettlementType(settlementDateOperation.getSettlementOperation().getSettlementType());
		settlementOperation.setSettlementSchema(settlementDateOperation.getSettlementOperation().getSettlementSchema());
		return settlementOperation;
	}
	
	/**
	 * Populate settlement account operation.
	 *
	 * @param settlementDateAccount the settlement date account
	 * @param settlementOperation the settlement operation
	 * @return the settlement account operation
	 */
	private SettlementAccountOperation populateSettlementAccountOperation(SettlementDateAccount settlementDateAccount,SettlementOperation settlementOperation) {
		SettlementAccountOperation settlementAccount = new SettlementAccountOperation();
		settlementAccount.setOperationState(HolderAccountOperationStateType.CONFIRMED.getCode());
		settlementAccount.setInitialStockQuantity(settlementDateAccount.getStockQuantity());
		settlementAccount.setStockQuantity(settlementDateAccount.getStockQuantity());
		settlementAccount.setSettlementAmount(settlementDateAccount.getSettlementAmount());
		
		settlementAccount.setRole(settlementDateAccount.getSettlementAccountOperation().getRole());
		
		settlementAccount.setStockReference(settlementDateAccount.getSettlementAccountOperation().getStockReference());
		settlementAccount.setSettlementOperation(settlementOperation);
		settlementAccount.setHolderAccountOperation(settlementDateAccount.getSettlementAccountOperation().getHolderAccountOperation());
		
		List<SettlementDateMarketfact> settlementDateMarketFacts = getSettlementDateMarketfacts(settlementDateAccount.getIdSettlementDateAccountPk());
		settlementAccount.setSettlementAccountMarketfacts(new ArrayList<SettlementAccountMarketfact>());
		for(SettlementDateMarketfact settlementDateMarketfact : settlementDateMarketFacts){
			SettlementAccountMarketfact settlementAccountMarketfact = new SettlementAccountMarketfact();
			settlementAccountMarketfact.setSettlementAccountOperation(settlementAccount);
			settlementAccountMarketfact.setMarketQuantity(settlementDateMarketfact.getMarketQuantity());
			settlementAccountMarketfact.setMarketPrice(settlementDateMarketfact.getMarketPrice());
			settlementAccountMarketfact.setMarketRate(settlementDateMarketfact.getMarketRate());
			settlementAccountMarketfact.setMarketDate(settlementDateMarketfact.getMarketDate());
			settlementAccount.getSettlementAccountMarketfacts().add(settlementAccountMarketfact);
		}
		return settlementAccount;
	}

	/**
	 * Gets the settlement date req details.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request pk
	 * @return the settlement date req details
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getSettlementDateReqDetails(Long idSettlementDateRequestPk) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select  ");
		stringBuffer.append(" mo.mechanisnModality.negotiationMechanism.mechanismName, ");
		stringBuffer.append(" mo.mechanisnModality.negotiationModality.modalityName, ");
		stringBuffer.append(" mo.operationDate, ");
		stringBuffer.append(" mo.ballotNumber, ");
		stringBuffer.append(" mo.sequential, ");
		stringBuffer.append(" mo.operationNumber, ");
		stringBuffer.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = sr.requestType)");
		stringBuffer.append(" from SettlementDateOperation sdo ");
		stringBuffer.append(" inner join sdo.settlementRequest sr");
		stringBuffer.append(" inner join sdo.settlementOperation so ");
		stringBuffer.append(" inner join so.mechanismOperation mo");
		stringBuffer.append(" where sr.idSettlementRequestPk = :idSettlementRequest");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementRequest", idSettlementDateRequestPk);
		
		return findListByQueryString(stringBuffer.toString(), parameters);
	}

	/**
	 * Update settlement date amounts.
	 *
	 * @param settlementDateOperation the settlement date operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void updateSettlementDateAmounts(SettlementDateOperation settlementDateOperation, LoggerUser loggerUser) throws ServiceException {
		//update settlementOperation
		SettlementOperation settlementOperation = settlementDateOperation.getSettlementOperation();
		settlementOperation.setSettlementPrice(settlementDateOperation.getSettlementPrice());
		if(!settlementOperation.getIndCurrencyExchange().equals(ComponentConstant.ONE)){
			settlementOperation.setInitialSettlementPrice(settlementOperation.getSettlementPrice());
		}
		settlementOperation.setSettlementAmount(settlementDateOperation.getSettlementAmount());
		settlementOperation.setSettlementDate(settlementDateOperation.getSettlementDate());
		settlementOperation.setSettlementDays(settlementDateOperation.getSettlementDays());
		settlementOperation.setIndPrepaid(ComponentConstant.ONE);
		update(settlementOperation);
		
		//update participantSettlement
		List<ParticipantSettlement> participantSettlements = (List<ParticipantSettlement>) settlementProcessService.
				getParticipantSettlements(settlementOperation.getIdSettlementOperationPk(), null, null);
		BigDecimal rate = settlementDateOperation.getSettlementOperation().getAmountRate();
		BigDecimal cashPrice = settlementDateOperation.getSettlementOperation().getMechanismOperation().getCashPrice();
		BigDecimal cashSettlementPrice = settlementDateOperation.getSettlementOperation().getMechanismOperation().getCashSettlementPrice();
		Integer operationCurrency = settlementDateOperation.getSettlementOperation().getMechanismOperation().getCurrency();
		BigDecimal realPrice = null;
		DailyExchangeRates dailyExchange = null;
		ParameterTable currencyParameter = parameterServiceBean.find(ParameterTable.class, operationCurrency);
		if(currencyParameter != null && currencyParameter.getIndicator2().equals(GeneralConstants.ZERO_VALUE_STRING) && 
				!currencyParameter.getIndicator4().equals(currencyParameter.getIndicator5())){
			dailyExchange = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(), operationCurrency);
			if(dailyExchange == null){
				throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST);
			}
			realPrice = cashPrice;
		}else{
			realPrice = cashSettlementPrice;
		}
		BigDecimal newPrice = NegotiationUtils.getTermPrice(realPrice, rate, settlementDateOperation.getSettlementDays());
		for (ParticipantSettlement participantSettlement : participantSettlements) {
			BigDecimal settlementAmount= null;
			if (Validations.validateIsNotNull(dailyExchange)) {
				settlementAmount = NegotiationUtils.getSettlementAmount(newPrice, participantSettlement.getStockQuantity()).
																multiply(dailyExchange.getBuyPrice()).setScale(2, RoundingMode.HALF_UP);
			} else {
				settlementAmount = NegotiationUtils.getSettlementAmount(newPrice, participantSettlement.getStockQuantity());
			}
			participantSettlement.setSettlementAmount(settlementAmount);
//			participantSettlement.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementDateOperation.getSettlementPrice(), 
//																							participantSettlement.getStockQuantity()));
			//participantSettlement.setSettlementAmount(settlementOperation.getSettlementAmount().divide(participantSettlement.getStockQuantity(), 2, RoundingMode.HALF_UP));
			update(participantSettlement);
		}
		
		//update settlementAccounts
		List<Integer> states = new ArrayList<Integer>();
		states.add(HolderAccountOperationStateType.CONFIRMED.getCode());
		List<SettlementAccountOperation> settlementAccounts = (List<SettlementAccountOperation>) settlementProcessService.
				getSettlementAccountOperations(settlementOperation.getIdSettlementOperationPk(), states);
		
		for (SettlementAccountOperation settlementAccount : settlementAccounts) {
			//settlementAccount.setSettlementAmount(settlementOperation.getSettlementAmount().divide( settlementAccount.getStockQuantity(), 2, RoundingMode.HALF_UP));
			settlementAccount.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementDateOperation.getSettlementPrice(), 
																							settlementAccount.getStockQuantity()));
			update(settlementAccount);
		}
		
//		if(!ComponentConstant.ONE.equals(settlementOperation.getIndPartial())){
//			settlementProcessService.updateMechanismOperationDate(settlementOperation.getMechanismOperation().getIdMechanismOperationPk(), 
//					ComponentConstant.ONE, null, settlementDateOperation.getSettlementDate(), settlementDateOperation.getOperationPart(), loggerUser);
//		}
	}

	/**
	 * Check assignment process.
	 *
	 * @param settlementOperation the settlement operation
	 * @throws ServiceException the service exception
	 */
	public void checkAssignmentProcess(SettlementOperation settlementOperation) throws ServiceException {
		accountAssignmentService.checkAssignmentProcess(
				settlementOperation.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk(),
				settlementOperation.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk(),
				AssignmentProcessStateType.CLOSED.getCode(),
				settlementOperation.getSettlementDate());
	}
	
	/**
	 * Metodo para veriricar si una operacion se encuentra en una cadena Confirmada
	 * @param settlementOperation
	 * @return
	 * @throws ServiceException
	 */
	public Boolean verifyChainedOperationSelid(Long idMechanismOperationPk,Date chainedDateOperation) throws ServiceException{
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("    SELECT DISTINCT MO.ID_MECHANISM_OPERATION_PK                                                                          ");
		querySql.append("    from HOLDER_CHAIN_DETAIL HCD                                                                                          ");
		querySql.append("    inner join CHAINED_HOLDER_OPERATION CHO ON HCD.ID_CHAINED_HOLDER_OPERATION_FK = CHO.ID_CHAINED_HOLDER_OPERATION_PK    ");
		querySql.append("    inner join SETTLEMENT_ACCOUNT_MARKETFACT SAM on HCD.ID_ACCOUNT_MARKETFACT_FK=SAM.ID_ACCOUNT_MARKETFACT_PK             ");
		querySql.append("    inner join SETTLEMENT_ACCOUNT_OPERATION SAO on SAM.ID_SETTLEMENT_ACCOUNT_FK=SAO.ID_SETTLEMENT_ACCOUNT_PK              ");
		querySql.append("    inner join SETTLEMENT_OPERATION SO on SAO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK                    ");
		querySql.append("    inner join MECHANISM_OPERATION MO on SO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK                        ");
		querySql.append("    where 1 = 1                                                                                                           ");
		querySql.append("    AND MO.ID_MECHANISM_OPERATION_PK= :idMechanismOperationPk  											               ");
		querySql.append("    AND CHO.CHAIN_STATE = 1959                                                                                            ");
		querySql.append("    and SAM.IND_ACTIVE=1                                                                                                  ");
		querySql.append("    and TRUNC(CHO.REGISTER_DATE) = :dateOperation 														                   ");
																																				   
		Query querySec = em.createNativeQuery(querySql.toString());
		querySec.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		querySec.setParameter("dateOperation", CommonsUtilities.truncateDateTime(chainedDateOperation));

		@SuppressWarnings("unchecked")
		List<Object[]>  resultList = (List<Object[]>)querySec.getResultList();
		
		if(resultList.size()>GeneralConstants.ZERO_VALUE_INTEGER){
			return false;
		}else{
			return true;
		}
		
	}
	

	/**
	 * Execute change state settlement request.
	 *
	 * @param idSettlementRequest the id settlement request
	 * @param newState the new state
	 * @param idParticipant the id participant
	 * @param lstSettlementDateOperation the lst settlement date operation
	 * @param lstSettlementDateAccounts the lst settlement date accounts
	 * @param sendNotification the send notification
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateSettlementRequest(Long idSettlementRequest,
			Integer newState, Long idParticipant, List<SettlementDateOperation> lstSettlementDateOperation, List<SettlementDateAccount> lstSettlementDateAccounts, 
			boolean sendNotification, LoggerUser loggerUser) throws ServiceException {
		
		SettlementRequest settlementRequest = find(SettlementRequest.class,idSettlementRequest);
	    Long idBusinessProcess = null;
		
		if(newState.equals(SettlementDateRequestStateType.CONFIRMED.getCode())){
			
			if(!SettlementDateRequestStateType.REVIEW.getCode().equals(settlementRequest.getRequestState())){
		    	throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_STATE_MODIFY, new Object[]{idSettlementRequest.toString()});
			}
			if(idParticipant!=null && !settlementRequest.getTargetParticipant().getIdParticipantPk().equals(idParticipant)){
		    	throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_INVALID_PARTICIPANT, new Object[]{idSettlementRequest.toString()});
			}
			settlementRequest.setRequestState(SettlementDateRequestStateType.CONFIRMED.getCode());	  	
    		settlementRequest.setConfirmDate(CommonsUtilities.currentDateTime());
    		settlementRequest.setConfirmUser(loggerUser.getUserName());
    		update(settlementRequest);
    		
    		idBusinessProcess = BusinessProcessType.EXTENSION_ADVANCE_CONFIRMATION_REQUEST.getCode();
    		
		}else if (newState.equals(SettlementDateRequestStateType.REJECTED.getCode())){
			
			if(idParticipant!=null){
				
				if(!SettlementDateRequestStateType.REVIEW.getCode().equals(settlementRequest.getRequestState())
						&& !SettlementDateRequestStateType.CONFIRMED.getCode().equals(settlementRequest.getRequestState())
							&& !SettlementDateRequestStateType.APPROVED.getCode().equals(settlementRequest.getRequestState())){
					throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_STATE_MODIFY, new Object[]{idSettlementRequest.toString()});
				}
				
				if(!settlementRequest.getTargetParticipant().getIdParticipantPk().equals(idParticipant)){
			    	throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_INVALID_PARTICIPANT, new Object[]{idSettlementRequest.toString()});
				}
			}else{
				if(!SettlementDateRequestStateType.REVIEW.getCode().equals(settlementRequest.getRequestState())
						&& !SettlementDateRequestStateType.CONFIRMED.getCode().equals(settlementRequest.getRequestState())
							&& !SettlementDateRequestStateType.APPROVED.getCode().equals(settlementRequest.getRequestState())){
					throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_STATE_MODIFY, new Object[]{idSettlementRequest.toString()});
				}
			}
			
			settlementRequest.setRejectDate(CommonsUtilities.currentDateTime());
			settlementRequest.setRejectUser(loggerUser.getUserName());
			settlementRequest.setRequestState(SettlementDateRequestStateType.REJECTED.getCode());
			update(settlementRequest);
			
			idBusinessProcess = BusinessProcessType.EXTENSION_ADVANCE_REJECT_REQUEST.getCode();
			
		} else if (newState.equals(SettlementDateRequestStateType.APPROVED.getCode())){
			
			if(!SettlementDateRequestStateType.REGISTERED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_STATE_MODIFY, new Object[]{idSettlementRequest.toString()});
			}
			
			if(idParticipant!=null && !settlementRequest.getSourceParticipant().getIdParticipantPk().equals(idParticipant)){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_INVALID_PARTICIPANT, new Object[]{idSettlementRequest.toString()});
			}
			
			settlementRequest.setRequestState(SettlementDateRequestStateType.APPROVED.getCode());
			settlementRequest.setApprovalUser(loggerUser.getUserName());
			settlementRequest.setApprovalDate(CommonsUtilities.currentDateTime());
			update(settlementRequest);
			
			idBusinessProcess = BusinessProcessType.EXTENSION_ADVANCE_APPROVAL_REQUEST.getCode();
			
		} else if (newState.equals(SettlementDateRequestStateType.ANNULATE.getCode())){
			
			if(!SettlementDateRequestStateType.REGISTERED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_STATE_MODIFY, new Object[]{idSettlementRequest.toString()});
			}
			
			if(idParticipant!=null && !settlementRequest.getSourceParticipant().getIdParticipantPk().equals(idParticipant)){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_INVALID_PARTICIPANT, new Object[]{idSettlementRequest.toString()});
			}
			
			settlementRequest.setAnnulUser(loggerUser.getUserName());
			settlementRequest.setAnnulDate(CommonsUtilities.currentDateTime());
			settlementRequest.setRequestState(SettlementDateRequestStateType.ANNULATE.getCode());
			update(settlementRequest);
			
			idBusinessProcess = BusinessProcessType.EXTENSION_ADVANCE_ANNUL_REQUEST.getCode();
			
		} else if (newState.equals(SettlementDateRequestStateType.REVIEW.getCode())){
			
			if(!SettlementDateRequestStateType.APPROVED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_STATE_MODIFY, new Object[]{idSettlementRequest.toString()});
			}
			
			if(idParticipant!=null && !settlementRequest.getTargetParticipant().getIdParticipantPk().equals(idParticipant)){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_INVALID_PARTICIPANT, new Object[]{idSettlementRequest.toString()});
			}
			
			settlementRequest.setRequestState(SettlementDateRequestStateType.REVIEW.getCode());
			settlementRequest.setReviewUser(loggerUser.getUserName());
			settlementRequest.setReviewDate(CommonsUtilities.currentDateTime());
			update(settlementRequest);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementDateAccounts)){
				saveAll(lstSettlementDateAccounts);
    		}
			
			idBusinessProcess = BusinessProcessType.EXTENSION_ADVANCE_REVIEW_REQUEST.getCode();
			
		} else if (newState.equals(SettlementDateRequestStateType.AUTHORIZE.getCode())){
			
			if(!SettlementDateRequestStateType.CONFIRMED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_DATE_REQUEST_STATE_MODIFY, new Object[]{idSettlementRequest.toString()});
			}
			settlementRequest.setRequestState(SettlementDateRequestStateType.AUTHORIZE.getCode());
			settlementRequest.setAuthorizeUser(loggerUser.getUserName());
			settlementRequest.setAuthorizeDate(CommonsUtilities.currentDateTime());
			update(settlementRequest);
			
			executeAuthorizeActions(settlementRequest,lstSettlementDateOperation, loggerUser);
			
			idBusinessProcess = BusinessProcessType.EXTENSION_ADVANCE_AUTHORIZE_REQUEST.getCode();
			
		}  
	   
	    if(sendNotification){
	    	sendNotification(settlementRequest.getIdSettlementRequestPk(),idBusinessProcess,loggerUser);
	    }
	}
	
	/**
	 * Execute authorize actions.
	 *
	 * @param settlementRequest the settlement request
	 * @param lstSettlementDateOperation the lst settlement date operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void executeAuthorizeActions(SettlementRequest settlementRequest, 
			List<SettlementDateOperation> lstSettlementDateOperation, LoggerUser loggerUser) throws ServiceException {
		
		if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(settlementRequest.getRequestType())){
			
			for(SettlementDateOperation settlementDateOperation : lstSettlementDateOperation){
				SettlementOperation settlementOperation = settlementDateOperation.getSettlementOperation();
				if(settlementOperation.getIdSettlementOperationPk()!=null){
					settlementProcessService.updateSettlementOperationDate(settlementOperation.getIdSettlementOperationPk(), 
		    				ComponentConstant.ONE, null, settlementDateOperation.getSettlementDate(), loggerUser);
		    	}
			}
	    }
		else if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(settlementRequest.getRequestType())){
			
			for(SettlementDateOperation settlementDateOperation : lstSettlementDateOperation){
				
				if(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(settlementRequest.getAnticipationType())){
					createSettlementDateOperation(settlementDateOperation);
					
				}else if(SettlementAnticipationType.SETTLEMENT_TOTAL.getCode().equals(settlementRequest.getAnticipationType())){
					updateSettlementDateAmounts(settlementDateOperation,loggerUser);
				}
			}
			
	    }else if(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(settlementRequest.getRequestType()) || 
	    		SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(settlementRequest.getRequestType())){
			
			for(SettlementDateOperation settlementDateOperation : lstSettlementDateOperation){
				if(settlementDateOperation.getSettlementOperation().getIdSettlementOperationPk()!=null){
					settlementProcessService.updateOperationSettlementType(settlementDateOperation.getSettlementOperation().getIdSettlementOperationPk(), 
							SettlementSchemaType.GROSS.getCode(),
							settlementDateOperation.getSettlementType(), loggerUser);
		    	}
			}
	    }
	}

	/**
	 * Send notification.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request pk
	 * @param idBusinessProcess the id business process
	 * @param loggerUser the logger user
	 */
	public void sendNotification(Long idSettlementDateRequestPk, Long idBusinessProcess, LoggerUser loggerUser) {
		
		List<Object[]> data= getSettlementDateReqDetails(idSettlementDateRequestPk);
		
		for(Object[] arrObj : data){
			BusinessProcess businessProcess = new BusinessProcess();
		    businessProcess.setIdBusinessProcessPk(idBusinessProcess);
		    Object[] parameters = new Object[6];
		    parameters[0]= (String)arrObj[6];
		    parameters[1]= idSettlementDateRequestPk.toString();
		    parameters[2]= (String)arrObj[0];
		    parameters[3]= (String)arrObj[1];
		    parameters[4]= CommonsUtilities.convertDatetoString((Date)arrObj[2]);
		    String operationNumber = null;
		    Long ballotNumber = (Long)arrObj[3];
		    Long sequential = (Long)arrObj[4];
		    if(ballotNumber == null && sequential == null){	
		    	operationNumber = ((Long)arrObj[5]).toString();
		    }else{
		    	operationNumber = new StringBuffer().append(ballotNumber).append(GeneralConstants.SLASH).append(sequential).toString();
		    }
		    parameters[5]= operationNumber;
		    notificationService.sendNotification(loggerUser, loggerUser.getUserName(), businessProcess, null,parameters);
		}
	}

	/**
	 * Save settlement date request.
	 *
	 * @param settlementRequest the settlement request
	 * @param settlementDateAccounts the settlement date accounts
	 * @param sendNotification the send notification
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void saveSettlementDateRequest(SettlementRequest settlementRequest,
			List<SettlementDateAccount> settlementDateAccounts, boolean sendNotification, LoggerUser loggerUser) throws ServiceException {
		
		settlementRequest.setRequestState(SettlementDateRequestStateType.REGISTERED.getCode());
	    settlementRequest.setRegisterDate(CommonsUtilities.currentDateTime());
	    settlementRequest.setRegisterUser(loggerUser.getUserName());
	    
	    if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(settlementRequest.getRequestType())){  
	    	
	    }else if (SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(settlementRequest.getRequestType())){
	    	
	    	if(settlementRequest.getSourceParticipant().getIdParticipantPk().equals(settlementRequest.getTargetParticipant().getIdParticipantPk())){
	    		settlementRequest.setRequestState(SettlementDateRequestStateType.REVIEW.getCode());
	    	    settlementRequest.setApprovalDate(CommonsUtilities.currentDateTime());
	    	    settlementRequest.setApprovalUser(loggerUser.getUserName());
	    	    settlementRequest.setReviewDate(CommonsUtilities.currentDateTime());
	    	    settlementRequest.setReviewUser(loggerUser.getUserName());
	    	}
	    	
	    }else if (SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(settlementRequest.getRequestType())){
    		settlementRequest.setRequestState(SettlementDateRequestStateType.CONFIRMED.getCode());
    	    settlementRequest.setApprovalDate(CommonsUtilities.currentDateTime());
    	    settlementRequest.setApprovalUser(loggerUser.getUserName());
    	    settlementRequest.setReviewDate(CommonsUtilities.currentDateTime());
    	    settlementRequest.setReviewUser(loggerUser.getUserName());
    	    settlementRequest.setConfirmDate(CommonsUtilities.currentDateTime());
    	    settlementRequest.setConfirmUser(loggerUser.getUserName());
	    	
	    }else if (SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(settlementRequest.getRequestType())){
	    	
	    	if(settlementRequest.getSourceParticipant().getIdParticipantPk().equals(settlementRequest.getTargetParticipant().getIdParticipantPk())){
	    		settlementRequest.setRequestState(SettlementDateRequestStateType.REVIEW.getCode());
	    	    settlementRequest.setApprovalDate(CommonsUtilities.currentDateTime());
	    	    settlementRequest.setApprovalUser(loggerUser.getUserName());
	    	    settlementRequest.setReviewDate(CommonsUtilities.currentDateTime());
	    	    settlementRequest.setReviewUser(loggerUser.getUserName());
	    	}
	    }
	    
	    create(settlementRequest);
	    
	    for(SettlementDateOperation settlementDateOperation : settlementRequest.getLstSettlementDateOperations()){
	    	
	    	// if assignment was closed, cannot be opened, this was validated on operation selection
	    	// prepaidExtendedService.validateAssignmentProcess(settlementDateOperation.getSettlementOperation());
	    	
	    	settlementProcessService.checkExistChainedAccount(
	    			settlementDateOperation.getSettlementOperation().getIdSettlementOperationPk());
	    	
	    	checkPreviousDateRequest(settlementDateOperation.getSettlementOperation().getIdSettlementOperationPk(),
		    			settlementRequest.getRequestType());
		    
	    	if(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(settlementRequest.getAnticipationType())){
	    		//settlementDateOperation.setSettlementPrice(settlementDateOperation.getSettlementAmount().divide(settlementDateOperation.getStockQuantity()));
	    		settlementDateOperation.setSettlementPrice(CommonsUtilities.divideTwoDecimal(settlementDateOperation.getSettlementAmount(), settlementDateOperation.getStockQuantity(), 2));	
	    	}
	    	
		    create(settlementDateOperation);
		    
		    if(Validations.validateListIsNotNullAndNotEmpty(settlementDateAccounts)){
		    	for(SettlementDateAccount settlementDateAccount : settlementDateAccounts){
		    		settlementDateAccount.setSettlementDateOperation(settlementDateOperation);
		    		create(settlementDateAccount);
		    	}
		    }
	    }
	    
	    if(sendNotification){
	    	sendNotification(settlementRequest.getIdSettlementRequestPk(),BusinessProcessType.EXTENSION_ADVANCE_REGISTER_REQUEST.getCode(),loggerUser);
	    }
		
	}
		
	/**
	 * Gets the data notification.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request PK
	 * @return the data notification
	 */
	public Object[] getDataNotification(Long idSettlementDateRequestPk) throws ServiceException {		
		List<Object[]> data= getSettlementDateReqDetails(idSettlementDateRequestPk);
		Object[] parameters = null;
		for(Object[] arrObj : data){			
		    parameters = new Object[6];
		    parameters[0]= (String)arrObj[6];
		    parameters[1]= idSettlementDateRequestPk.toString();
		    parameters[2]= (String)arrObj[0];
		    parameters[3]= (String)arrObj[1];
		    parameters[4]= CommonsUtilities.convertDatetoString((Date)arrObj[2]);
		    String operationNumber = null;
		    Long ballotNumber = (Long)arrObj[3];
		    Long sequential = (Long)arrObj[4];
		    if(ballotNumber == null && sequential == null){	
		    	operationNumber = ((Long)arrObj[5]).toString();
		    }else{
		    	operationNumber = new StringBuffer().append(ballotNumber).append(GeneralConstants.SLASH).append(sequential).toString();
		    }
		    parameters[5]= operationNumber;		    		    
		}
		return parameters;
	}
	
}