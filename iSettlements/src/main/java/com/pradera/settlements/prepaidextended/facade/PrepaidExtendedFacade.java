package com.pradera.settlements.prepaidextended.facade;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementDateAccount;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.negotiations.mcnoperations.service.McnOperationServiceBean;
import com.pradera.negotiations.operations.service.MechanismOperationService;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.currencyexchange.to.SettlementRequestTO;
import com.pradera.settlements.prepaidextended.service.PrepaidExtendedService;
import com.pradera.settlements.prepaidextended.to.RegisterPrepaidExtendedTO;
import com.pradera.settlements.prepaidextended.to.SearchPrepaidExtendedTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrepaidExtendedFacade.
 *
 * @author Pradera Technologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class PrepaidExtendedFacade {
	
	/** the prepaidExtendedService. */
	@EJB
	private PrepaidExtendedService prepaidExtendedService;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The mechanism operation service. */
	@EJB
	MechanismOperationService mechanismOperationService;
	
	/** The mcn operation service. */
	@EJB
	McnOperationServiceBean mcnOperationService;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The holiday query service bean. */
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;
	
	/**
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	
	public Participant getParticipantToGloss(Long idParticipant) {
		Participant p = null;
		p = prepaidExtendedService.getOneParticipant(idParticipant);
		return p;
	}
	
	//registra la consulta del usuario
	
	public List<SettlementRequestTO> searchSettlementDateRequest(SearchPrepaidExtendedTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return prepaidExtendedService.searchSettlementDateRequest(filter);
	}
	
	/**
	 * METODO PARA ADQUIRIR EL RESULTADO BASADO EN LOS FILTROS DE REGISTRO
	 * to get the search result based on the search filter.
	 *
	 * @param filter the filter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<SettlementOperation> searchMechanismOperations(RegisterPrepaidExtendedTO filter) throws ServiceException{
		return prepaidExtendedService.searchMechanismOperations(filter);
	}

	
	
	/**
	 * Metodo para verificar si una operacion fue registrada en selid
	 * @param idMechanismOperationPk
	 * @return
	 * @throws ServiceException
	 */
	public Boolean searchMechanismOperationsSelid(Long idMechanismOperationPk) throws ServiceException{
		return prepaidExtendedService.searchMechanismOperationsSelid(idMechanismOperationPk);
	}
	
	/**
	 * Save request.
	 *
	 * @param settlementRequest the settlement request
	 * @param settlementDateAccounts the settlement date accounts
	 * @param sendNotification the send notification
	 * @throws ServiceException the service exception
	 */
	public void saveRequest(SettlementRequest settlementRequest, List<SettlementDateAccount> settlementDateAccounts, boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
	    prepaidExtendedService.saveSettlementDateRequest(settlementRequest, settlementDateAccounts, sendNotification, loggerUser);
	     
	}

	/**
	 * Confirm request.
	 *
	 * @param idSettlementRequest the id settlement request
	 * @param idParticipant the id participant
	 * @param sendNotification the send notification
	 * @throws ServiceException the service exception
	 */
	//CONTRAPARTE
	public void confirmRequest(Long idSettlementRequest, Long idParticipant , boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    
	    prepaidExtendedService.executeChangeStateSettlementRequest(idSettlementRequest, 
	    		SettlementDateRequestStateType.CONFIRMED.getCode(), idParticipant , null , null , sendNotification, loggerUser);
	}

	
	/**
	 * Reject request.
	 *
	 * @param idSettlementRequestPk the id settlement request pk
	 * @param idParticipantPk the id participant pk
	 * @param sendNotification the send notification
	 * @throws ServiceException the service exception
	 */
	//CONTRAPARTE, MECANISMO
	public void rejectRequest(Long idSettlementRequestPk, Long idParticipantPk, boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
	    
	    prepaidExtendedService.executeChangeStateSettlementRequest(idSettlementRequestPk, 
	    		SettlementDateRequestStateType.REJECTED.getCode(), idParticipantPk , null , null , sendNotification, loggerUser);
	}

	/**
	 * METODO QUE HACE EL CAMBIO DE ESTADO A AUTORIZADO.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request pk
	 * @param lstSettlementDateOperation the lst settlement date operation
	 * @param sendNotification the send notification
	 * @throws ServiceException the service exception
	 */
	
	public void authorizeRequest(Long idSettlementDateRequestPk, List<SettlementDateOperation> lstSettlementDateOperation, boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAuthorize());
	    
	    prepaidExtendedService.executeChangeStateSettlementRequest(idSettlementDateRequestPk, 
	    		SettlementDateRequestStateType.AUTHORIZE.getCode(), null , lstSettlementDateOperation , null , sendNotification, loggerUser);
	}
	
	/**
	 * Review request.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request pk
	 * @param settlementDateAccounts the settlement date accounts
	 * @param idParticipantPk the id participant pk
	 * @param sendNotification the send notification
	 * @throws ServiceException the service exception
	 */
	//CONTRAPARTE
	public void reviewRequest(Long idSettlementDateRequestPk, List<SettlementDateAccount> settlementDateAccounts, Long idParticipantPk, boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
	    
	    prepaidExtendedService.executeChangeStateSettlementRequest(idSettlementDateRequestPk, 
	    		SettlementDateRequestStateType.REVIEW.getCode(), idParticipantPk , null , settlementDateAccounts , sendNotification, loggerUser);
	}
	
	/**
	 * Metodo para revisar una operacion multiple SELAR
	 */
	//CONTRAPARTE
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void reviewRequestMultiple(Long idSettlementDateRequestPk, List<SettlementDateAccount> settlementDateAccounts, Long idParticipantPk, boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
	    
	    prepaidExtendedService.executeChangeStateSettlementRequest(idSettlementDateRequestPk, 
	    		SettlementDateRequestStateType.REVIEW.getCode(), idParticipantPk , null , settlementDateAccounts , sendNotification, loggerUser);
	}

	/**
	 * Approve request.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request pk
	 * @param idParticipantPk the id participant pk
	 * @param sendNotification the send notification
	 * @throws ServiceException the service exception
	 */
	//SOLICITANTE
	public void approveRequest(Long idSettlementDateRequestPk, Long idParticipantPk, boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
	    
	    prepaidExtendedService.executeChangeStateSettlementRequest(idSettlementDateRequestPk, 
	    		SettlementDateRequestStateType.APPROVED.getCode(), idParticipantPk , null , null , sendNotification, loggerUser);
	}

	/**
	 * Annul request.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request pk
	 * @param idParticipantPk the id participant pk
	 * @param sendNotification the send notification
	 * @throws ServiceException the service exception
	 */
	//SOLICITANTE
	public void annulRequest(Long idSettlementDateRequestPk, Long idParticipantPk, boolean sendNotification) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
	    
	    prepaidExtendedService.executeChangeStateSettlementRequest(idSettlementDateRequestPk, 
	    		SettlementDateRequestStateType.ANNULATE.getCode(), idParticipantPk , null , null , sendNotification, loggerUser);
	}

	/**
	 * Search settlement account operations.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idRole the id role
	 * @param idParticipant the id participant
	 * @return the list
	 */
	public List<SettlementAccountOperation> searchSettlementAccountOperations(Long idSettlementOperation, Integer idRole, Long idParticipant) {
		return mechanismOperationService.getSettlementAccountOperations(idSettlementOperation, idRole, idParticipant);
	}

	/**
	 * Gets the settlement date operations.
	 *
	 * @param idSettlementRequest the id settlement request
	 * @param fetchAccounts the fetch accounts
	 * @param idParticipant the id participant
	 * @return the settlement date operations
	 */
	public List<SettlementDateOperation> getSettlementDateOperations(Long idSettlementRequest, boolean fetchAccounts, Long idParticipant) {
		List<SettlementDateOperation> settlementDateOperations = prepaidExtendedService.getSettlementDateOperations(idSettlementRequest);
		if(fetchAccounts){
			for (SettlementDateOperation settlementDateOperation : settlementDateOperations) {
				List<SettlementDateAccount> settlementDateAccounts = prepaidExtendedService.getSettlementDateAccounts(
																			settlementDateOperation.getIdSettlementDateOperationPk(), idParticipant);
				for(SettlementDateAccount settlementDateAccount : settlementDateAccounts){
					settlementDateAccount.setSettlementDateMarketfacts(prepaidExtendedService.getSettlementDateMarketfacts(settlementDateAccount.getIdSettlementDateAccountPk()));
				}
				settlementDateOperation.setSettlementDateAccounts(settlementDateAccounts);
			}
		}
		return settlementDateOperations;
	}

	/**
	 * Gets the settlement schedule.
	 *
	 * @param currentDateTime the current date time
	 * @return the settlement schedule
	 * @throws ServiceException the service exception
	 */
	public void getSettlementSchedule(Date currentDateTime) throws ServiceException {
		settlementProcessService.getSettlementSchedule(SettlementScheduleType.MELID.getCode(), currentDateTime, null, ComponentConstant.ONE);
	}

	/**
	 * Gets the settlement account marketfacts.
	 *
	 * @param idSettlementAccountPk the id settlement account pk
	 * @return the settlement account marketfacts
	 */
	public List<SettlementAccountMarketfact> getSettlementAccountMarketfacts(Long idSettlementAccountPk) {
		return settlementProcessService.getSettlementAccountMarketfacts(idSettlementAccountPk);
	}

	/**
	 * Gets the settlement date req details.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request pk
	 * @return the settlement date req details
	 */
	public List<Object[]> getSettlementDateReqDetails(Long idSettlementDateRequestPk) {
		return prepaidExtendedService.getSettlementDateReqDetails(idSettlementDateRequestPk);
	}
	
	/**
	 * Check assignment process.
	 *
	 * @param settlementOperation the settlement operation
	 * @throws ServiceException the service exception
	 */
	public void checkAssignmentProcess(SettlementOperation settlementOperation) throws ServiceException{
		prepaidExtendedService.checkAssignmentProcess(settlementOperation);
	}
	
	/**
	 * Metodo para veriricar si una operacion se encuentra en una cadena Confirmada
	 *
	 * @param settlementOperation the settlement operation
	 * @throws ServiceException the service exception
	 */
	public Boolean verifyChainedOperationSelid(Long idMechanismOperationPk, Date chainedDateOperation) throws ServiceException{
		Boolean chainedOperationSelid = prepaidExtendedService.verifyChainedOperationSelid(idMechanismOperationPk,chainedDateOperation);
		return chainedOperationSelid;
	}
	
	/**
	 * Check previous date request.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param requestType the request type
	 * @throws ServiceException the service exception
	 */
	public void checkPreviousDateRequest(Long idSettlementOperation, Integer requestType) throws ServiceException{
		prepaidExtendedService.checkPreviousDateRequest(idSettlementOperation,requestType);
	}
		
	/**
	 * Gets the data notification.
	 *
	 * @param idSettlementDateRequestPk the id settlement date request PK
	 * @return the data notification
	 * @throws ServiceException the service exception
	 */
	public Object[] getDataNotification(Long idSettlementDateRequestPk) throws ServiceException {
		return prepaidExtendedService.getDataNotification(idSettlementDateRequestPk) ;
	}

}

