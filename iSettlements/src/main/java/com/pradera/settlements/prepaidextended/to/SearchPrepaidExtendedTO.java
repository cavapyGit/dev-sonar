package com.pradera.settlements.prepaidextended.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchPrepaidExtendedTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchPrepaidExtendedTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst anticipation type. */
	private List<ParameterTable> lstState, lstRequestType, lstAnticipationType;
	
	/** The lst role. */
	private List<SelectItem> lstRole;
	
	/** The anticipation type selected. */
	private Integer requestTypeSelected, roleSelected, anticipationTypeSelected;
	
	/** The lst settlement exchange type. */
	private List<ParameterTable> lstSettlementExchangeType;
	
	/** The lst mechanism. */
	private List<NegotiationMechanism> lstMechanism;	
	
	/** The lst modality. */
	private List<NegotiationModality> lstModality;	
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The id negotiation mechanism pk. */
	private Long idNegotiationMechanismPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The ballot number selected. */
	private Long ballotNumberSelected; 
	
	/** The sequential selected. */
	private Long sequentialSelected;
	
	/** The state selected. */
	private Integer stateSelected;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The id settlement request date pk. */
	private Long idSettlementRequestDatePk;
	
	/** The securitie. */
	private Security securitie;
	
	/**
	 * Instantiates a new search prepaid extended to.
	 */
	public SearchPrepaidExtendedTO() {
		super();
	}
	
	/**
	 * Gets the lst anticipation type.
	 *
	 * @return the lst anticipation type
	 */
	public List<ParameterTable> getLstAnticipationType() {
		return lstAnticipationType;
	}
	
	/**
	 * Sets the lst anticipation type.
	 *
	 * @param lstAnticipationType the new lst anticipation type
	 */
	public void setLstAnticipationType(List<ParameterTable> lstAnticipationType) {
		this.lstAnticipationType = lstAnticipationType;
	}
	
	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	
	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	
	/**
	 * Gets the lst request type.
	 *
	 * @return the lst request type
	 */
	public List<ParameterTable> getLstRequestType() {
		return lstRequestType;
	}
	
	/**
	 * Sets the lst request type.
	 *
	 * @param lstRequestType the new lst request type
	 */
	public void setLstRequestType(List<ParameterTable> lstRequestType) {
		this.lstRequestType = lstRequestType;
	}
	
	/**
	 * Gets the state selected.
	 *
	 * @return the state selected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}
	
	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the new state selected
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}
	
	/**
	 * Gets the request type selected.
	 *
	 * @return the request type selected
	 */
	public Integer getRequestTypeSelected() {
		return requestTypeSelected;
	}
	
	/**
	 * Sets the request type selected.
	 *
	 * @param requestTypeSelected the new request type selected
	 */
	public void setRequestTypeSelected(Integer requestTypeSelected) {
		this.requestTypeSelected = requestTypeSelected;
	}
	
	/**
	 * Gets the role selected.
	 *
	 * @return the role selected
	 */
	public Integer getRoleSelected() {
		return roleSelected;
	}
	
	/**
	 * Sets the role selected.
	 *
	 * @param roleSelected the new role selected
	 */
	public void setRoleSelected(Integer roleSelected) {
		this.roleSelected = roleSelected;
	}
	
	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}
	
	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}
	
	/**
	 * Gets the lst modality.
	 *
	 * @return the lst modality
	 */
	public List<NegotiationModality> getLstModality() {
		return lstModality;
	}
	
	/**
	 * Sets the lst modality.
	 *
	 * @param lstModality the new lst modality
	 */
	public void setLstModality(List<NegotiationModality> lstModality) {
		this.lstModality = lstModality;
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}
	
	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	
	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	
	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}
	
	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the id settlement request date pk.
	 *
	 * @return the id settlement request date pk
	 */
	public Long getIdSettlementRequestDatePk() {
		return idSettlementRequestDatePk;
	}
	
	/**
	 * Sets the id settlement request date pk.
	 *
	 * @param idSettlementRequestDatePk the new id settlement request date pk
	 */
	public void setIdSettlementRequestDatePk(Long idSettlementRequestDatePk) {
		this.idSettlementRequestDatePk = idSettlementRequestDatePk;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the anticipation type selected.
	 *
	 * @return the anticipation type selected
	 */
	public Integer getAnticipationTypeSelected() {
		return anticipationTypeSelected;
	}
	
	/**
	 * Sets the anticipation type selected.
	 *
	 * @param anticipationTypeSelected the new anticipation type selected
	 */
	public void setAnticipationTypeSelected(Integer anticipationTypeSelected) {
		this.anticipationTypeSelected = anticipationTypeSelected;
	}
	
	/**
	 * Gets the ballot number selected.
	 *
	 * @return the ballot number selected
	 */
	public Long getBallotNumberSelected() {
		return ballotNumberSelected;
	}
	
	/**
	 * Sets the ballot number selected.
	 *
	 * @param ballotNumberSelected the new ballot number selected
	 */
	public void setBallotNumberSelected(Long ballotNumberSelected) {
		this.ballotNumberSelected = ballotNumberSelected;
	}
	
	/**
	 * Gets the sequential selected.
	 *
	 * @return the sequential selected
	 */
	public Long getSequentialSelected() {
		return sequentialSelected;
	}
	
	/**
	 * Sets the sequential selected.
	 *
	 * @param sequentialSelected the new sequential selected
	 */
	public void setSequentialSelected(Long sequentialSelected) {
		this.sequentialSelected = sequentialSelected;
	}
	
	/**
	 * Gets the securitie.
	 *
	 * @return the securitie
	 */
	public Security getSecuritie() {
		return securitie;
	}
	
	/**
	 * Sets the securitie.
	 *
	 * @param securitie the new securitie
	 */
	public void setSecuritie(Security securitie) {
		this.securitie = securitie;
	}
	
	/**
	 * Gets the lst settlement exchange type.
	 *
	 * @return the lst settlement exchange type
	 */
	public List<ParameterTable> getLstSettlementExchangeType() {
		return lstSettlementExchangeType;
	}
	
	/**
	 * Sets the lst settlement exchange type.
	 *
	 * @param lstSettlementExchangeType the new lst settlement exchange type
	 */
	public void setLstSettlementExchangeType(
			List<ParameterTable> lstSettlementExchangeType) {
		this.lstSettlementExchangeType = lstSettlementExchangeType;
	}
	
	/**
	 * Sets the lst role.
	 *
	 * @param lstRole the new lst role
	 */
	public void setLstRole(List<SelectItem> lstRole) {
		this.lstRole = lstRole;
	}
	
	/**
	 * Gets the lst role.
	 *
	 * @return the lst role
	 */
	public List<SelectItem> getLstRole() {
		return lstRole;
	}
	
}