package com.pradera.settlements.prepaidextended.to;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.settlement.SettlementDateAccount;
import com.pradera.model.settlement.SettlementDateOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterPrepaidExtendedTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterPrepaidExtendedTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst state. */
	private List<ParameterTable> lstRequestType, lstState;
	
	/** The lst role. */
	private List<SelectItem> lstRole;
	
	/** The lst anticipation type. */
	private List<ParameterTable> lstAnticipationType;
	
	/** The lst settlement exchange type. */
	private List<ParameterTable> lstSettlementExchangeType;
	
	/** The anticipation type selected. */
	private Integer requestTypeSelected, roleSelected, anticipationTypeSelected;
	
	/** The settlement exchange type. */
	private Integer settlementExchangeType;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The lst mechanism. */
	private List<NegotiationMechanism> lstMechanism;	
	
	/** The lst modality. */
	private List<NegotiationModality> lstModality;	
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The lst counterpart participant. */
	private List<Participant> lstCounterpartParticipant;
	
	/** The id negotiation mechanism pk. */
	private Long idNegotiationMechanismPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The id counter part participant. */
	private Long idCounterPartParticipant;
	
	/** The securitie. */
	private Security securitie;
	
	/** The new settlement date. */
	private Date operationDate, newSettlementDate;
	
	/** The bl prepaid. */
	private boolean blPrepaid;
	
	/** The bl extended. */
	private boolean blExtended;
	
	/** The bl settlement exchange. */
	private boolean blSettlementExchange;
	
	/** The bl special settlement exchange. */
	private boolean blSpecialSettlementExchange;
	
	/** The bl cross operation. */
	private boolean blCrossOperation;
	
	/** The operation role. */
	private Integer operationRole;
	
	/** The lst settlement date operation. */
	private List<SettlementDateOperation> lstSettlementDateOperation;
	
	/** The lst settlement date accounts. */
	private List<SettlementDateAccount> lstSettlementDateAccounts;
	
	/** The selected date account. */
	private SettlementDateAccount selectedDateAccount;
	
	/** The max quantity. */
	private BigDecimal maxQuantity = BigDecimal.ZERO;
	
	/** The sale quantity. */
	private BigDecimal saleQuantity = BigDecimal.ZERO;
	
	/** The sale amount. */
	private BigDecimal saleAmount = BigDecimal.ZERO;
	
	/** The purchase quantity. */
	private BigDecimal purchaseQuantity = BigDecimal.ZERO;
	
	/** The purchase amount. */
	private BigDecimal purchaseAmount = BigDecimal.ZERO;
	
	/** The check all. */
	private boolean checkAll;
	
	/**
	 * Clean summary.
	 */
	public void cleanSummary(){
		maxQuantity = BigDecimal.ZERO;
		saleQuantity = BigDecimal.ZERO;
		saleAmount = BigDecimal.ZERO;
		purchaseQuantity = BigDecimal.ZERO;
		purchaseAmount = BigDecimal.ZERO;
	}
	
	/**
	 * Gets the lst settlement date operation.
	 *
	 * @return the lst settlement date operation
	 */
	public List<SettlementDateOperation> getLstSettlementDateOperation() {
		return lstSettlementDateOperation;
	}
	
	/**
	 * Sets the lst settlement date operation.
	 *
	 * @param lstSettlementDateOperation the new lst settlement date operation
	 */
	public void setLstSettlementDateOperation(
			List<SettlementDateOperation> lstSettlementDateOperation) {
		this.lstSettlementDateOperation = lstSettlementDateOperation;
	}
	
	/**
	 * Gets the lst anticipation type.
	 *
	 * @return the lst anticipation type
	 */
	public List<ParameterTable> getLstAnticipationType() {
		return lstAnticipationType;
	}
	
	/**
	 * Sets the lst anticipation type.
	 *
	 * @param lstAnticipationType the new lst anticipation type
	 */
	public void setLstAnticipationType(List<ParameterTable> lstAnticipationType) {
		this.lstAnticipationType = lstAnticipationType;
	}
	
	/**
	 * Gets the lst request type.
	 *
	 * @return the lst request type
	 */
	public List<ParameterTable> getLstRequestType() {
		return lstRequestType;
	}
	
	/**
	 * Sets the lst request type.
	 *
	 * @param lstRequestType the new lst request type
	 */
	public void setLstRequestType(List<ParameterTable> lstRequestType) {
		this.lstRequestType = lstRequestType;
	}
	
	/**
	 * Gets the request type selected.
	 *
	 * @return the request type selected
	 */
	public Integer getRequestTypeSelected() {
		return requestTypeSelected;
	}
	
	/**
	 * Sets the request type selected.
	 *
	 * @param requestTypeSelected the new request type selected
	 */
	public void setRequestTypeSelected(Integer requestTypeSelected) {
		this.requestTypeSelected = requestTypeSelected;
	}
	
	/**
	 * Gets the role selected.
	 *
	 * @return the role selected
	 */
	public Integer getRoleSelected() {
		return roleSelected;
	}
	
	/**
	 * Sets the role selected.
	 *
	 * @param roleSelected the new role selected
	 */
	public void setRoleSelected(Integer roleSelected) {
		this.roleSelected = roleSelected;
	}
	
	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}
	
	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}
	
	/**
	 * Gets the lst modality.
	 *
	 * @return the lst modality
	 */
	public List<NegotiationModality> getLstModality() {
		return lstModality;
	}
	
	/**
	 * Sets the lst modality.
	 *
	 * @param lstModality the new lst modality
	 */
	public void setLstModality(List<NegotiationModality> lstModality) {
		this.lstModality = lstModality;
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}
	
	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	
	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	
	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}
	
	/**
	 * Gets the id counter part participant.
	 *
	 * @return the id counter part participant
	 */
	public Long getIdCounterPartParticipant() {
		return idCounterPartParticipant;
	}
	
	/**
	 * Sets the id counter part participant.
	 *
	 * @param idCounterPartParticipant the new id counter part participant
	 */
	public void setIdCounterPartParticipant(Long idCounterPartParticipant) {
		this.idCounterPartParticipant = idCounterPartParticipant;
	}
	
	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipantPk the new id participant
	 */
	public void setIdParticipant(Long idParticipantPk) {
		this.idParticipant = idParticipantPk;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Checks if is bl prepaid.
	 *
	 * @return true, if is bl prepaid
	 */
	public boolean isBlPrepaid() {
		return blPrepaid;
	}
	
	/**
	 * Sets the bl prepaid.
	 *
	 * @param blPrepaid the new bl prepaid
	 */
	public void setBlPrepaid(boolean blPrepaid) {
		this.blPrepaid = blPrepaid;
	}
	
	/**
	 * Checks if is bl extended.
	 *
	 * @return true, if is bl extended
	 */
	public boolean isBlExtended() {
		return blExtended;
	}
	
	/**
	 * Sets the bl extended.
	 *
	 * @param blExtended the new bl extended
	 */
	public void setBlExtended(boolean blExtended) {
		this.blExtended = blExtended;
	}
	
	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	
	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	
	/**
	 * Gets the new settlement date.
	 *
	 * @return the new settlement date
	 */
	public Date getNewSettlementDate() {
		return newSettlementDate;
	}
	
	/**
	 * Sets the new settlement date.
	 *
	 * @param newSettlementDate the new new settlement date
	 */
	public void setNewSettlementDate(Date newSettlementDate) {
		this.newSettlementDate = newSettlementDate;
	}
	
	/**
	 * Gets the anticipation type selected.
	 *
	 * @return the anticipation type selected
	 */
	public Integer getAnticipationTypeSelected() {
		return anticipationTypeSelected;
	}
	
	/**
	 * Sets the anticipation type selected.
	 *
	 * @param anticipationTypeSelected the new anticipation type selected
	 */
	public void setAnticipationTypeSelected(Integer anticipationTypeSelected) {
		this.anticipationTypeSelected = anticipationTypeSelected;
	}
	
	/**
	 * Gets the securitie.
	 *
	 * @return the securitie
	 */
	public Security getSecuritie() {
		return securitie;
	}
	
	/**
	 * Sets the securitie.
	 *
	 * @param securitie the new securitie
	 */
	public void setSecuritie(Security securitie) {
		this.securitie = securitie;
	}

	/**
	 * Gets the lst settlement date accounts.
	 *
	 * @return the lst settlement date accounts
	 */
	public List<SettlementDateAccount> getLstSettlementDateAccounts() {
		return lstSettlementDateAccounts;
	}
	
	/**
	 * Sets the lst settlement date accounts.
	 *
	 * @param lstSettlementDateAccounts the new lst settlement date accounts
	 */
	public void setLstSettlementDateAccounts(
			List<SettlementDateAccount> lstSettlementDateAccounts) {
		this.lstSettlementDateAccounts = lstSettlementDateAccounts;
	}
	
	/**
	 * Checks if is bl settlement exchange.
	 *
	 * @return true, if is bl settlement exchange
	 */
	public boolean isBlSettlementExchange() {
		return blSettlementExchange;
	}
	
	/**
	 * Sets the bl settlement exchange.
	 *
	 * @param blSettlementExchange the new bl settlement exchange
	 */
	public void setBlSettlementExchange(boolean blSettlementExchange) {
		this.blSettlementExchange = blSettlementExchange;
	}
	
	/**
	 * Gets the lst settlement exchange type.
	 *
	 * @return the lst settlement exchange type
	 */
	public List<ParameterTable> getLstSettlementExchangeType() {
		return lstSettlementExchangeType;
	} 	
	
	/**
	 * Sets the lst settlement exchange type.
	 *
	 * @param lstSettlementExchangeType the new lst settlement exchange type
	 */
	public void setLstSettlementExchangeType(
			List<ParameterTable> lstSettlementExchangeType) {
		this.lstSettlementExchangeType = lstSettlementExchangeType;
	}
	
	/**
	 * Gets the settlement exchange type.
	 *
	 * @return the settlement exchange type
	 */
	public Integer getSettlementExchangeType() {
		return settlementExchangeType;
	}
	
	/**
	 * Sets the settlement exchange type.
	 *
	 * @param settlementExchangeType the new settlement exchange type
	 */
	public void setSettlementExchangeType(Integer settlementExchangeType) {
		this.settlementExchangeType = settlementExchangeType;
	}
	
	/**
	 * Gets the selected date account.
	 *
	 * @return the selected date account
	 */
	public SettlementDateAccount getSelectedDateAccount() {
		return selectedDateAccount;
	}
	
	/**
	 * Sets the selected date account.
	 *
	 * @param selectedDateAccount the new selected date account
	 */
	public void setSelectedDateAccount(SettlementDateAccount selectedDateAccount) {
		this.selectedDateAccount = selectedDateAccount;
	}
	
	/**
	 * Gets the max quantity.
	 *
	 * @return the max quantity
	 */
	public BigDecimal getMaxQuantity() {
		return maxQuantity;
	}
	
	/**
	 * Sets the max quantity.
	 *
	 * @param maxQuantity the new max quantity
	 */
	public void setMaxQuantity(BigDecimal maxQuantity) {
		this.maxQuantity = maxQuantity;
	}
	
	/**
	 * Checks if is bl cross operation.
	 *
	 * @return true, if is bl cross operation
	 */
	public boolean isBlCrossOperation() {
		return blCrossOperation;
	}
	
	/**
	 * Sets the bl cross operation.
	 *
	 * @param blCrossOperation the new bl cross operation
	 */
	public void setBlCrossOperation(boolean blCrossOperation) {
		this.blCrossOperation = blCrossOperation;
	}
	
	/**
	 * Gets the sale quantity.
	 *
	 * @return the sale quantity
	 */
	public BigDecimal getSaleQuantity() {
		return saleQuantity;
	}
	
	/**
	 * Sets the sale quantity.
	 *
	 * @param saleQuantity the new sale quantity
	 */
	public void setSaleQuantity(BigDecimal saleQuantity) {
		this.saleQuantity = saleQuantity;
	}
	
	/**
	 * Gets the sale amount.
	 *
	 * @return the sale amount
	 */
	public BigDecimal getSaleAmount() {
		return saleAmount;
	}
	
	/**
	 * Sets the sale amount.
	 *
	 * @param saleAmount the new sale amount
	 */
	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}
	
	/**
	 * Gets the purchase quantity.
	 *
	 * @return the purchase quantity
	 */
	public BigDecimal getPurchaseQuantity() {
		return purchaseQuantity;
	}
	
	/**
	 * Sets the purchase quantity.
	 *
	 * @param purchaseQuantity the new purchase quantity
	 */
	public void setPurchaseQuantity(BigDecimal purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}
	
	/**
	 * Gets the purchase amount.
	 *
	 * @return the purchase amount
	 */
	public BigDecimal getPurchaseAmount() {
		return purchaseAmount;
	}
	
	/**
	 * Sets the purchase amount.
	 *
	 * @param purchaseAmount the new purchase amount
	 */
	public void setPurchaseAmount(BigDecimal purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	
	/**
	 * Gets the operation role.
	 *
	 * @return the operation role
	 */
	public Integer getOperationRole() {
		return operationRole;
	}
	
	/**
	 * Gets the lst counterpart participant.
	 *
	 * @return the lst counterpart participant
	 */
	public List<Participant> getLstCounterpartParticipant() {
		return lstCounterpartParticipant;
	}
	
	/**
	 * Sets the lst counterpart participant.
	 *
	 * @param lstCounterpartParticipant the new lst counterpart participant
	 */
	public void setLstCounterpartParticipant(
			List<Participant> lstCounterpartParticipant) {
		this.lstCounterpartParticipant = lstCounterpartParticipant;
	}
	
	/**
	 * Sets the operation role.
	 *
	 * @param operationRole the new operation role
	 */
	public void setOperationRole(Integer operationRole) {
		this.operationRole = operationRole;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		if(operationRole!=null){
			if(ComponentConstant.SALE_ROLE.equals(operationRole)){
				return saleQuantity;
			}else if(ComponentConstant.PURCHARSE_ROLE.equals(operationRole)){
				return purchaseQuantity;
			}
		}
		
		return BigDecimal.ZERO;
	}
	
	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		if(operationRole!=null){
			if(ComponentConstant.SALE_ROLE.equals(operationRole)){
				return saleAmount;
			}else if(ComponentConstant.PURCHARSE_ROLE.equals(operationRole)){
				return purchaseAmount;
			}
		}
		return BigDecimal.ZERO;
	}
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
	/**
	 * Sets the lst role.
	 *
	 * @param lstRole the new lst role
	 */
	public void setLstRole(List<SelectItem> lstRole) {
		this.lstRole = lstRole;
	}
	
	/**
	 * Gets the lst role.
	 *
	 * @return the lst role
	 */
	public List<SelectItem> getLstRole() {
		return lstRole;
	}

	/**
	 * Checks if is bl special settlement exchange.
	 *
	 * @return true, if is bl special settlement exchange
	 */
	public boolean isBlSpecialSettlementExchange() {
		return blSpecialSettlementExchange;
	}

	/**
	 * Sets the bl special settlement exchange.
	 *
	 * @param blSpecialSettlementExchange the new bl special settlement exchange
	 */
	public void setBlSpecialSettlementExchange(boolean blSpecialSettlementExchange) {
		this.blSpecialSettlementExchange = blSpecialSettlementExchange;
	}

	/**
	 * Checks if is check all.
	 *
	 * @return true, if is check all
	 */
	public boolean isCheckAll() {
		return checkAll;
	}

	/**
	 * Sets the check all.
	 *
	 * @param checkAll the new check all
	 */
	public void setCheckAll(boolean checkAll) {
		this.checkAll = checkAll;
	}
	
}
