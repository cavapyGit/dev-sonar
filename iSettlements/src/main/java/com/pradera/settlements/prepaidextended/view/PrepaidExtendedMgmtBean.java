package com.pradera.settlements.prepaidextended.view;


import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementDateAccount;
import com.pradera.model.settlement.SettlementDateMarketfact;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.SettlementAnticipationType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementExchangeType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.settlements.currencyexchange.to.SettlementRequestTO;
import com.pradera.settlements.prepaidextended.facade.PrepaidExtendedFacade;
import com.pradera.settlements.prepaidextended.to.RegisterPrepaidExtendedTO;
import com.pradera.settlements.prepaidextended.to.SearchPrepaidExtendedTO;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrepaidExtendedMgmtBean.
 *
 * @author Pradera Technologies
 */
@LoggerCreateBean
@DepositaryWebBean
public class PrepaidExtendedMgmtBean extends GenericBaseBean implements Serializable {

	/** default serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search prepaid extended to. */
	private SearchPrepaidExtendedTO searchPrepaidExtendedTO;
	
	/** The register prepaid extended to. */
	private RegisterPrepaidExtendedTO registerPrepaidExtendedTO;
	
	/** The lst settlement date request. */
	private GenericDataModel<SettlementRequestTO> lstSettlementDateRequest;
	
	/** The selected request. */
	private SettlementRequestTO selectedRequest;
	
	/** The selected request. */
	private List<SettlementRequestTO> selectedRequestList;
	
	/** The lst settlement date negotiations. */
	private GenericDataModel<SettlementDateOperation> lstSettlementDateNegotiations; // operaciones de negociacion no anticipadas
	
	/** The lst settlement date accounts. */
	private GenericDataModel<SettlementDateAccount> lstSettlementDateAccounts;
	
	/** The selected date operation. */
	private SettlementDateOperation selectedDateOperation; // only for anticipations
	
	/** The settlement date request. */
	private SettlementRequest settlementDateRequest; // new request
	
	/** The bl no result. */
	private boolean blNoResult;
	
	
	private Integer newMultipleState;
	
	/** The mechanism user. */
	private boolean depositaryUser, participantUser, mechanismUser;
	
	/** The mp parameter objects. */
	private Map<Integer,ParameterTable> mpParameterObjects = new HashMap<Integer, ParameterTable>();
	
	/** The mp parameters. */
	private Map<Integer,String> mpParameters = new HashMap<Integer, String>();
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	private String glossToSelveRequest;
	
	/**  the userInfo. */
	@Inject
	UserInfo userInfo;	
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;	
	
	/** the prepaidExtendedFacade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The prepaid extended facade. */
	@EJB
	private PrepaidExtendedFacade prepaidExtendedFacade;
	
	/** The mcn operation service facade. */
	@EJB
	private McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The holiday query service bean. */
	@EJB 
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	private Integer quantitySettlementDays; 
	
	/**
	 * Instantiates a new prepaid extended mgmt bean.
	 */
	public PrepaidExtendedMgmtBean(){

	}

	/**
	 * to initialize default values.
	 */
	@PostConstruct
	public void init(){
		try{
			searchPrepaidExtendedTO = new SearchPrepaidExtendedTO();
			searchPrepaidExtendedTO.setSecuritie(new Security());
			searchPrepaidExtendedTO.setInitialDate(getCurrentSystemDate());
			searchPrepaidExtendedTO.setEndDate(getCurrentSystemDate());	
			
			lstSettlementDateRequest = null;
			selectedDateOperation = null;
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				
				depositaryUser = true;
				searchPrepaidExtendedTO.setLstMechanism(mcnOperationServiceFacade.getLstNegotiationMechanism(null));
				searchPrepaidExtendedTO.setLstParticipant(mcnOperationServiceFacade.getLstParticipants(ParticipantType.DIRECT.getCode()));
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()){	
				
				participantUser = true;
				searchPrepaidExtendedTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				searchPrepaidExtendedTO.setLstMechanism(mcnOperationServiceFacade.getLstNegotiationMechanismByParticipant(searchPrepaidExtendedTO.getIdParticipantPk()));
				Participant partUser = mcnOperationServiceFacade.getParticipantObject(searchPrepaidExtendedTO.getIdParticipantPk());
				searchPrepaidExtendedTO.setLstParticipant(new ArrayList<Participant>());
				searchPrepaidExtendedTO.getLstParticipant().add(partUser);
				
			} else {
				mechanismUser = true;
				searchPrepaidExtendedTO.setLstMechanism(mcnOperationServiceFacade.getLstNegotiationMechanism(null));						
//				if(InstitutionType.BOLSA.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
//					searchPrepaidExtendedTO.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
//				}
			}
			
			fillCombos();	
			searchPrepaidExtendedTO.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
			changeNegotitationMechanism();
		}catch (ServiceException e) {
			 e.printStackTrace();
		}
	}
	
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		init();
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Clean up.
	 */
	public void cleanUp(){
		searchPrepaidExtendedTO.setAnticipationTypeSelected(null);
		searchPrepaidExtendedTO.setRequestTypeSelected(null);
		searchPrepaidExtendedTO.setIdParticipantPk(null);
	}
	
	/**
	 * Change negotitation mechanism.
	 *
	 * @throws ServiceException the service exception
	 */
	public void changeNegotitationMechanism() throws ServiceException{
		cleanUp();
		
		searchPrepaidExtendedTO.setIdNegotiationModalityPk(null);
		searchPrepaidExtendedTO.setRequestTypeSelected(null);
		
		if(depositaryUser){				
			searchPrepaidExtendedTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModality(searchPrepaidExtendedTO.getIdNegotiationMechanismPk()));
		}else if(participantUser){
			searchPrepaidExtendedTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			searchPrepaidExtendedTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
					searchPrepaidExtendedTO.getIdNegotiationMechanismPk(), searchPrepaidExtendedTO.getIdParticipantPk()));		
		}
	}
	

	/**
	 * Change negotitation modality.
	 */
	public void changeNegotitationModality(){
		if(!participantUser){
			cleanUp();
		}
		
		searchPrepaidExtendedTO.setRequestTypeSelected(null);
	}
	
	
	/**
	 * Sets the search init.
	 */
	public void setSearchInit(){
		if (!FacesContext.getCurrentInstance().isPostback()) {
			setViewOperationType(Integer.valueOf(0));
		}
	}
	
	/**
	 * METODO QUE SE EJECUTA AL CAMBIAR DE MECANISMO DE NEGOCIACION EN LA PANTALLA DE REGISTRO.
	 *
	 * @throws ServiceException the service exception
	 */
	
	public void changeNegotitationMechanismForRegister() throws ServiceException{
		lstSettlementDateNegotiations = null;
		registerPrepaidExtendedTO.setIdNegotiationModalityPk(null);
		
		if(depositaryUser){				
			registerPrepaidExtendedTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModality(registerPrepaidExtendedTO.getIdNegotiationMechanismPk()));
			cleanResult();
		}else if(participantUser){
			registerPrepaidExtendedTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
			registerPrepaidExtendedTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
															registerPrepaidExtendedTO.getIdNegotiationMechanismPk(), 
															registerPrepaidExtendedTO.getIdParticipant()));		
		}
	}
	
	/**
	 * METODO QUE SE EJECUTA AL CAMBIAR DE MODALIDAD DE NEGOCIACION EN EL REGISTRO DE SOLICITUD.
	 */
	
	public void changeNegotitationModalityForRegister(){
		lstSettlementDateNegotiations = null;
		if(!participantUser){
			registerPrepaidExtendedTO.setIdParticipant(null);
			cleanResult();
		}
		registerPrepaidExtendedTO.setIdCounterPartParticipant(null);
		registerPrepaidExtendedTO.setRequestTypeSelected(null);
	}
	
	/**
	 * METODO BUSCAR DEL BOTON DE BUSCAR SOLICITUDES
	 * to get the search result based on the search filter.
	 */
	@LoggerAuditWeb
	public void searchSettlementDateRequest(){
		
		//por defecto vacio
		glossToSelveRequest="";
		
		JSFUtilities.hideGeneralDialogues();
		blNoResult = true;
		try{
			selectedRequest = null;
			List<SettlementRequestTO> lstResult = prepaidExtendedFacade.searchSettlementDateRequest(searchPrepaidExtendedTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				lstSettlementDateRequest = new GenericDataModel<SettlementRequestTO>(lstResult);
				blNoResult = false;
				showPrivilegeButtons();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				lstSettlementDateRequest = null;
			}
		}catch (Exception e) {
			 excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * METODO QUE CARGA EL FILLDATA EN SEARCH.
	 *
	 * @param event the event
	 */
	
	public void showDetail(ActionEvent event){
		selectedRequest = (SettlementRequestTO)event.getComponent().getAttributes().get("settlementRequest");
 		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		fillData(selectedRequest);	
		
		//Construyendo la glosa correspondiente solo para SELVE
		glossToSelveRequest="";
		Object[] parametrosContenido = null;
		if (searchPrepaidExtendedTO.getRequestTypeSelected().equals(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode())
				&& Validations.validateIsNotNull(selectedRequest)
				&& Validations.validateIsNotNull(selectedRequest.getSourceParticipant())) {
			if(Validations.validateIsNotNull(selectedRequest.getTargetParticipant())){
				//glosa para dos participantes
				parametrosContenido = new Object[] { selectedRequest.getSourceParticipant().getDescription() != null ? selectedRequest.getSourceParticipant().getDescription() : "", selectedRequest.getTargetParticipant().getDescription() != null ? selectedRequest.getTargetParticipant().getDescription() : ""};
				glossToSelveRequest = PropertiesUtilities.getMessage("text.gloss.selve.request.two.participant", parametrosContenido);
			}else{
				//glosa para un participante
				parametrosContenido = new Object[] { selectedRequest.getSourceParticipant().getDescription() != null ? selectedRequest.getSourceParticipant().getDescription() : "" };
				glossToSelveRequest = PropertiesUtilities.getMessage("text.gloss.selve.request.one.participant", parametrosContenido);
			}
		}
		
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		JSFUtilities.hideGeneralDialogues();
		Object[] bodyData = new Object[2];
		bodyData[0] = selectedRequest.getRequestTypeDescription();
		bodyData[1] = selectedRequest.getIdSettlementRequest();
		if(isViewOperationConfirm()){
			question(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					 PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_CONFIRM_CONFIRM, bodyData) );
			
		}else if(isViewOperationAnnul()){
			question(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL), 
					 PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_CONFIRM_ANNUL, bodyData));
			
		}else if(isViewOperationApprove()){
			question(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE), 
					 PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_CONFIRM_APPROVAL, bodyData));
			
		}else if(isViewOperationAuthorize()){
			question(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_AUTHORIZE), 
					 PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_CONFIRM_AUTHORIZE, bodyData));
			
		}else if(isViewOperationReject()){
			question(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT), 
					 PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_CONFIRM_REJECT, bodyData));
		
		}else if(isViewOperationReview()){
			
			if(selectedRequest.getRequestType().equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode()) && !registerPrepaidExtendedTO.isBlCrossOperation()){
				
				if(!(registerPrepaidExtendedTO.getStockQuantity().compareTo(registerPrepaidExtendedTO.getMaxQuantity()) == 0)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_ACCOUNT_INVALID_QTY));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			
			question(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REVIEW), 
					 PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_CONFIRM_REVIEW, bodyData));
			
		}
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		JSFUtilities.hideGeneralDialogues();
		try {
			Object[] bodyData = new Object[3];
			bodyData[0] = selectedRequest.getRequestTypeDescription();
			bodyData[1] = selectedRequest.getIdSettlementRequest();
			if(participantUser){
				bodyData[2] = registerPrepaidExtendedTO.getIdParticipant();
			}
			if(isViewOperationConfirm()){
				confirmRequest(bodyData);
			}else if(isViewOperationAnnul()){
				annulRequest(bodyData);
			}else if(isViewOperationApprove()){
				approveRequest(bodyData);
			}else if(isViewOperationAuthorize()){
				authorizeRequest(bodyData);
			}else if(isViewOperationReject()){
				rejectRequest(bodyData);
			}else if(isViewOperationReview()){
				reviewRequest(bodyData);
			}
			searchSettlementDateRequest();
		} catch (ServiceException e) {
			endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					  PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
		
	}
	
	/**
	 * METODO QUE SE EJECUTA AL CARGAR LA PAGINA
	 * to load Registration Page.
	 *
	 * @return String
	 */
	public String loadRegistrationPage(){
		JSFUtilities.hideGeneralDialogues();
		
		try{
			lstSettlementDateNegotiations = null;
			lstSettlementDateAccounts = null;
			settlementDateRequest = null;
			
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			
			registerPrepaidExtendedTO = new RegisterPrepaidExtendedTO();
			registerPrepaidExtendedTO.setOperationDate(getCurrentSystemDate());
			registerPrepaidExtendedTO.setSecuritie(new Security());
			registerPrepaidExtendedTO.setLstParticipant(searchPrepaidExtendedTO.getLstParticipant());
			if(participantUser){
				registerPrepaidExtendedTO.setIdParticipant(searchPrepaidExtendedTO.getIdParticipantPk());
				registerPrepaidExtendedTO.setLstCounterpartParticipant(mcnOperationServiceFacade.getLstParticipants(ParticipantType.DIRECT.getCode()));
			}else{
				registerPrepaidExtendedTO.setLstCounterpartParticipant(searchPrepaidExtendedTO.getLstParticipant());
			}
			registerPrepaidExtendedTO.setLstMechanism(searchPrepaidExtendedTO.getLstMechanism());
			registerPrepaidExtendedTO.setLstRole(searchPrepaidExtendedTO.getLstRole());
			registerPrepaidExtendedTO.setLstSettlementExchangeType(searchPrepaidExtendedTO.getLstSettlementExchangeType());
			registerPrepaidExtendedTO.setLstRequestType(searchPrepaidExtendedTO.getLstRequestType());
			registerPrepaidExtendedTO.setSecuritie(new Security());
			registerPrepaidExtendedTO.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
			changeNegotitationMechanismForRegister();
			listHolidays();
		}catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)						
						, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			 excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return "registerRequest";
	}
	
	/**
	 * METODO QUE SE EJECUTA AL PRESIONAR EL BOTON LIMPIAR PANTALLA REGISTRO
	 * To clear the search filters and search results.
	 *
	 * @return the string
	 */
	public String cleanRegister(){
		JSFUtilities.hideGeneralDialogues();
		try{
			JSFUtilities.resetViewRoot();
			loadRegistrationPage();
		}catch (Exception e) {
			 excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return GeneralConstants.EMPTY_STRING;
	}

	
	public String onChangePrepaidType() {
		cleanOperationsResult();
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * On change request type.
	 */
	public void onChangeRequestType(){
		cleanPartialResult();
		
		try{
			
			if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected())){
				prepaidExtendedFacade.getSettlementSchedule(CommonsUtilities.currentDateTime());
				registerPrepaidExtendedTO.setBlExtended(true);
				registerPrepaidExtendedTO.setOperationDate(CommonsUtilities.currentDate());
			}else if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected())){
				registerPrepaidExtendedTO.setOperationDate(null);
				registerPrepaidExtendedTO.setBlPrepaid(true);
				ParameterTableTO paramTabTO = new ParameterTableTO();
				paramTabTO .setMasterTableFk(MasterTableType.SETTLEMENT_ANTICIPATION_TYPE.getCode());			
				registerPrepaidExtendedTO.setLstAnticipationType(generalParameterFacade.getListParameterTableServiceBean(paramTabTO));
			}else if(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected())){
				registerPrepaidExtendedTO.setOperationDate(CommonsUtilities.currentDate());
				registerPrepaidExtendedTO.setBlSettlementExchange(true);
				registerPrepaidExtendedTO.setSettlementExchangeType(SettlementExchangeType.DVP_TO_FOP.getCode());
			}else if(SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected())){
				registerPrepaidExtendedTO.setOperationDate(CommonsUtilities.currentDate());
				registerPrepaidExtendedTO.setBlSpecialSettlementExchange(true);
				registerPrepaidExtendedTO.setSettlementExchangeType(SettlementExchangeType.DVP_TO_FOP.getCode());
				registerPrepaidExtendedTO.setRoleSelected(ComponentConstant.SALE_ROLE);
			}
			
		}catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)						
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			registerPrepaidExtendedTO.setRequestTypeSelected(null);
		}
		
		
	}
	
	/**
	 * Gets the settlement type.
	 *
	 * @param settlementType the settlement type
	 * @return the settlement type
	 */
	public String getSettlementType(Integer settlementType){
		SettlementType type = SettlementType.get(settlementType);
		if(type != null){
			return type.getMnemonic();	
		}
		return null;
	}
	
	/**
	 * Gets the role type.
	 *
	 * @param role the role
	 * @return the role type
	 */
	public String getRoleType(Integer role){
		ParticipantRoleType type = ParticipantRoleType.get(role);
		if(type != null){
			return type.getValue();	
		}
		return null;
	}
	
	/**
	 * METODO QUE SE EJECUTA AL BUSCAR OPERACIONES 
	 * to get the search result based on the search filter.
	 *
	 * @throws ServiceException the service exception
	 */
	
	public void searchSettlementOperations() throws ServiceException{
		
		selectedDateOperation = null;
		lstSettlementDateNegotiations = null;
		lstSettlementDateAccounts = null;
		settlementDateRequest = null;
		
		registerPrepaidExtendedTO.cleanSummary();
		Integer requestType = registerPrepaidExtendedTO.getRequestTypeSelected();
		Integer role = registerPrepaidExtendedTO.getRoleSelected();
		Long idParticipant = registerPrepaidExtendedTO.getIdParticipant();
		Long idCounterParticipant = registerPrepaidExtendedTO.getIdCounterPartParticipant();
		
		//por defecto vacio
		glossToSelveRequest="";
		Object[] parametrosContenido = null;
		Participant p=null;
		//verificando si el tipi de solicitud es SELVE
		if (registerPrepaidExtendedTO.getRequestTypeSelected().equals(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode())) {
			p=prepaidExtendedFacade.getParticipantToGloss(idParticipant);
			if(idParticipant.equals(idCounterParticipant)){
				//si son iguales hacer glosa para un participante
				parametrosContenido = new Object[] { p.getDescription() };
				glossToSelveRequest = PropertiesUtilities.getMessage("text.gloss.selve.request.one.participant", parametrosContenido);
				
			}else{
				String participantDescription1=p.getDescription();
				p=prepaidExtendedFacade.getParticipantToGloss(idCounterParticipant);
				String participantDescription2=p.getDescription();
				
				//si son distintos hacer glosa para 2 participantes
				parametrosContenido = new Object[] { participantDescription1, participantDescription2};
				glossToSelveRequest = PropertiesUtilities.getMessage("text.gloss.selve.request.two.participant", parametrosContenido);
			}
		}
		
		if (registerPrepaidExtendedTO.getRequestTypeSelected()!=null){
						
			Date extensionDate = null;
			Integer settlementType = null;
			Integer anticipationType = null;
			
			if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected())){
				anticipationType = registerPrepaidExtendedTO.getAnticipationTypeSelected();
			}else if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected())){
				extensionDate = holidayQueryServiceBean.getCalculateDate(getCurrentSystemDate(),ComponentConstant.ONE,ComponentConstant.ONE,null);
			}else if(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected()) || 
					SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected())){
				if(SettlementExchangeType.DVP_TO_FOP.getCode().equals(registerPrepaidExtendedTO.getSettlementExchangeType())){
					settlementType = SettlementType.FOP.getCode();
				}else if(SettlementExchangeType.FOP_TO_DVP.getCode().equals(registerPrepaidExtendedTO.getSettlementExchangeType())){
					settlementType = SettlementType.DVP.getCode();
				}
			}

			List<SettlementOperation> lstOperations = prepaidExtendedFacade.searchMechanismOperations(registerPrepaidExtendedTO);
			
			List<SettlementDateOperation> settlementDateOperations = new ArrayList<SettlementDateOperation>();
			lstSettlementDateNegotiations = new GenericDataModel<SettlementDateOperation>(settlementDateOperations);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
				
				settlementDateRequest = new SettlementRequest();
				settlementDateRequest.setRequestType(requestType);
				settlementDateRequest.setParticipantRole(role);
				settlementDateRequest.setLstSettlementDateOperations(new ArrayList<SettlementDateOperation>());
				settlementDateRequest.setSourceParticipant(new Participant(idParticipant));
				if(!SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(registerPrepaidExtendedTO.getRequestTypeSelected()) && idCounterParticipant!=null){
					settlementDateRequest.setTargetParticipant(new Participant(idCounterParticipant));
				}
				settlementDateRequest.setAnticipationType(anticipationType);
				
				for (SettlementOperation settlementOperation:lstOperations) {
					settlementOperation.setStateDescription(mpParameters.get(settlementOperation.getOperationState()));
					settlementOperation.setCurrencyDescription(mpParameters.get(settlementOperation.getSettlementCurrency()));
					settlementOperation.getMechanismOperation().setCurrencyDescription(mpParameters.get(settlementOperation.getSettlementCurrency()));
					
					SettlementDateOperation settlementDateOperation = populateSettlementDateOperation(
											settlementOperation, settlementDateRequest, idParticipant,
											settlementOperation.getSettlementType(), settlementType , extensionDate, anticipationType);
					
					settlementDateOperations.add(settlementDateOperation);
				}
			}
				
		}		

	}
	
	/**
	 * Populate settlement date operation.
	 *
	 * @param settlementOperation the settlement operation
	 * @param settlementRequest the settlement request
	 * @param sourceParticipantId the source participant id
	 * @param initialSettlementType the initial settlement type
	 * @param settlementType the settlement type
	 * @param extensionDate the extension date
	 * @param anticipationType the anticipation type
	 * @return the settlement date operation
	 */
	private SettlementDateOperation populateSettlementDateOperation(
			SettlementOperation settlementOperation, SettlementRequest settlementRequest ,
			Long sourceParticipantId,Integer initialSettlementType, Integer settlementType, Date extensionDate , Integer anticipationType) {
		
		SettlementDateOperation settlementDateOperation = new SettlementDateOperation();
		settlementDateOperation.setSettlementRequest(settlementRequest);
		settlementDateOperation.setOperationPart(settlementOperation.getOperationPart());
		settlementDateOperation.setSettlementCurrency(settlementOperation.getSettlementCurrency());
		settlementDateOperation.setSettlementPrice(settlementOperation.getSettlementPrice());
		
		settlementDateOperation.setSettlementOperation(settlementOperation);
		if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(settlementDateOperation.getSettlementRequest().getRequestType())){
			settlementDateOperation.setStockQuantity(BigDecimal.ZERO);
			settlementDateOperation.setSettlementAmount(BigDecimal.ZERO);
			settlementDateOperation.setSettlementDate(CommonsUtilities.currentDate());
			settlementDateOperation.setSettlementDays(CommonsUtilities.diffDatesInDays(settlementDateOperation.getSettlementDate(), settlementOperation.getSettlementDate()).longValue());
		}else if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(settlementDateOperation.getSettlementRequest().getRequestType())){
			settlementDateOperation.setStockQuantity(settlementOperation.getStockQuantity());
			settlementDateOperation.setSettlementDays(ComponentConstant.ONE.longValue());
			settlementDateOperation.setSettlementDate(extensionDate);
			settlementDateOperation.setSettlementAmount(settlementOperation.getSettlementAmount());
			
		}else if(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(settlementDateOperation.getSettlementRequest().getRequestType()) ||
				SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(settlementDateOperation.getSettlementRequest().getRequestType())){
			settlementDateOperation.setInitialSettlementType(initialSettlementType);
			settlementDateOperation.setSettlementType(settlementType);
			settlementDateOperation.setStockQuantity(settlementOperation.getStockQuantity());
			settlementDateOperation.setSettlementAmount(settlementOperation.getSettlementAmount());
		}
		settlementDateOperation.setSettlementAmountRate(settlementOperation.getAmountRate());
		settlementDateOperation.setSettlementSchema(settlementOperation.getSettlementSchema());
		
		return settlementDateOperation;
		
	}
	
	/**
	 * On change settlement days.
	 *
	 * @param settlementDateOperation the settlement date operation
	 */
	public Boolean onChangeSettlementDays(SettlementDateOperation settlementDateOperation){
		try {
 			registerPrepaidExtendedTO.setSaleQuantity(BigDecimal.ZERO);
			registerPrepaidExtendedTO.setSaleAmount(BigDecimal.ZERO);
			registerPrepaidExtendedTO.setPurchaseQuantity(BigDecimal.ZERO);
			registerPrepaidExtendedTO.setPurchaseAmount(BigDecimal.ZERO);
			
			Date cashSettlementDate = settlementDateOperation.getSettlementOperation().getMechanismOperation().getCashSettlementDate();
			
			//issue 1223
			if(prepaidExtendedFacade.searchMechanismOperationsSelid(settlementDateOperation.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk())){
				
				cashSettlementDate = settlementDateOperation.getSettlementOperation().getMechanismOperation().getOperationDate(); 	
			}
			
			BigDecimal cashPrice = settlementDateOperation.getSettlementOperation().getMechanismOperation().getCashPrice();
			BigDecimal cashSettlementPrice = settlementDateOperation.getSettlementOperation().getMechanismOperation().getCashSettlementPrice();
			Integer operationCurrency = settlementDateOperation.getSettlementOperation().getMechanismOperation().getCurrency();
			BigDecimal rate = settlementDateOperation.getSettlementOperation().getAmountRate();
			Date anticipationDate = CommonsUtilities.currentDate(); 
					 
			Long settlementDays = new Long(CommonsUtilities.getDaysBetween(cashSettlementDate, anticipationDate));
			
			if(anticipationDate.compareTo(cashSettlementDate) <= 0){
				settlementDateOperation.setSettlementDays(null);
				settlementDateOperation.setSettlementDate(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_NEW_SETT_DATE_GREATER_CASH,
								new Object[]{CommonsUtilities.convertDatetoString(anticipationDate)}));
				JSFUtilities.showSimpleValidationDialog();
				return Boolean.FALSE;
			}
			
			if(anticipationDate.compareTo(settlementDateOperation.getSettlementOperation().getSettlementDate()) >= 0){
				settlementDateOperation.setSettlementDays(null);
				settlementDateOperation.setSettlementDate(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_NEW_SETT_DATE_GREATER,
								new Object[]{CommonsUtilities.convertDatetoString(anticipationDate)}));
				JSFUtilities.showSimpleValidationDialog();
				return Boolean.FALSE;
			}
			
			BigDecimal realPrice = null;
			DailyExchangeRates dailyExchange = null;
			ParameterTable currencyParameter = mpParameterObjects.get(operationCurrency);
			if(currencyParameter != null && currencyParameter.getIndicator2().equals(GeneralConstants.ZERO_VALUE_STRING) && 
					!currencyParameter.getIndicator4().equals(currencyParameter.getIndicator5())){
				dailyExchange = generalParameterFacade.getdailyExchangeRate(CommonsUtilities.currentDate(), operationCurrency);
				if(dailyExchange == null){
					throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST);
				}
				
				realPrice = cashPrice;
			}else{
				realPrice = cashSettlementPrice;
			}
			
			BigDecimal newPrice = NegotiationUtils.getTermPrice(realPrice, rate, settlementDays);
			
			settlementDateOperation.setSettlementDate(anticipationDate);
			settlementDateOperation.setSettlementAmountRate(rate);
			settlementDateOperation.setSettlementDays(settlementDays);
			settlementDateOperation.setSettlementPrice(newPrice);
			settlementDateOperation.setOldSettlementPrice(realPrice);
			
			if(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(settlementDateOperation.getSettlementRequest().getAnticipationType())){
				settlementDateOperation.setSettlementPrice(realPrice);
				if(lstSettlementDateAccounts !=null){
					for(SettlementDateAccount settlementDateAccount : lstSettlementDateAccounts.getDataList()){
						settlementDateAccount.setStockQuantity(BigDecimal.ZERO);
						settlementDateAccount.setSettlementAmount(BigDecimal.ZERO);
						settlementDateAccount.setSelected(false);
					}
				}
			
			} else if(SettlementAnticipationType.SETTLEMENT_TOTAL.getCode().equals(settlementDateOperation.getSettlementRequest().getAnticipationType())){
				registerPrepaidExtendedTO.setSaleQuantity(settlementDateOperation.getSettlementOperation().getStockQuantity());
				registerPrepaidExtendedTO.setPurchaseQuantity(settlementDateOperation.getSettlementOperation().getStockQuantity());
				if(dailyExchange!=null){					
					registerPrepaidExtendedTO.setSaleAmount(NegotiationUtils.getSettlementAmount(newPrice, registerPrepaidExtendedTO.getSaleQuantity()).multiply(dailyExchange.getBuyPrice()).setScale(2, RoundingMode.HALF_UP));
					registerPrepaidExtendedTO.setPurchaseAmount(NegotiationUtils.getSettlementAmount(newPrice, registerPrepaidExtendedTO.getPurchaseQuantity()).multiply(dailyExchange.getBuyPrice()).setScale(2, RoundingMode.HALF_UP));
					settlementDateOperation.setSettlementPrice(registerPrepaidExtendedTO.getPurchaseAmount().divide(registerPrepaidExtendedTO.getPurchaseQuantity(), 2, RoundingMode.HALF_UP));
				}else{
					registerPrepaidExtendedTO.setSaleAmount(NegotiationUtils.getSettlementAmount(newPrice, registerPrepaidExtendedTO.getSaleQuantity()));
					registerPrepaidExtendedTO.setPurchaseAmount(NegotiationUtils.getSettlementAmount(newPrice, registerPrepaidExtendedTO.getPurchaseQuantity()));
					settlementDateOperation.setSettlementPrice(newPrice);
				}				
			}
		} catch (ServiceException e) {
			settlementDateOperation.setSettlementDays(null);
			settlementDateOperation.setSettlementDate(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	/**
	 * On select settlement operation.
	 *
	 * @param selectedSettlementDate the selected settlement date
	 */
	public void onSelectSettlementOperation(SettlementDateOperation selectedSettlementDate){
						
		selectedDateOperation = null;
		lstSettlementDateAccounts = null;
		registerPrepaidExtendedTO.setBlCrossOperation(false);
		
		try {
		
			if(selectedSettlementDate == null){
				for (SettlementDateOperation tempSettlementDate : lstSettlementDateNegotiations.getDataList()){
					tempSettlementDate.setSelected(registerPrepaidExtendedTO.isCheckAll());
					selectSettlementOperation(tempSettlementDate);
				}
			}else{
				selectSettlementOperation(selectedSettlementDate);
			}
		
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			
			if(selectedSettlementDate != null){
				selectedSettlementDate.setSelected(false);
			}
			
			if(registerPrepaidExtendedTO.isCheckAll()){
				for (SettlementDateOperation tempSettlementDate : lstSettlementDateNegotiations.getDataList()){
					tempSettlementDate.setSelected(false);
				}
				registerPrepaidExtendedTO.setCheckAll(false);
			}
		}
		
		checkListIsCheckedAll(lstSettlementDateNegotiations.getDataList());
	}
	
	/**
	 * Check list is checked all.
	 *
	 * @param dataList the data list
	 */
	private void checkListIsCheckedAll(List<SettlementDateOperation> dataList) {
		for (SettlementDateOperation settlementDateOperation : dataList) {
			if(!settlementDateOperation.isSelected()){
				registerPrepaidExtendedTO.setCheckAll(false);
				return;
			}
		}
	}
	
	/**
	 * Select settlement operation.
	 *
	 * @param selectedSettlementDate the selected settlement date
	 * @throws ServiceException the service exception
	 */
	public void selectSettlementOperation(SettlementDateOperation selectedSettlementDate) throws ServiceException{
		
		if(!SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(settlementDateRequest.getRequestType()) && 
				!SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(settlementDateRequest.getRequestType()) && 
					!SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(settlementDateRequest.getRequestType())){
			
			registerPrepaidExtendedTO.setSaleQuantity(BigDecimal.ZERO);
			registerPrepaidExtendedTO.setSaleAmount(BigDecimal.ZERO);
			registerPrepaidExtendedTO.setPurchaseQuantity(BigDecimal.ZERO);
			registerPrepaidExtendedTO.setPurchaseAmount(BigDecimal.ZERO);
			
			for(SettlementDateOperation tempSettlementDate : lstSettlementDateNegotiations.getDataList()){
				/*if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(settlementDateRequest.getRequestType())){
					tempSettlementDate.setStockQuantity(BigDecimal.ZERO);
					tempSettlementDate.setSettlementAmount(BigDecimal.ZERO);
					tempSettlementDate.setSettlementDate(null);
					tempSettlementDate.setSettlementDays(null);
				}*/
				
				if(!selectedSettlementDate.equals(tempSettlementDate)){
					tempSettlementDate.setSelected(false);
				}
			}
		}
		
		if(selectedSettlementDate.isSelected()){
			
			prepaidExtendedFacade.checkPreviousDateRequest(
					selectedSettlementDate.getSettlementOperation().getIdSettlementOperationPk(), settlementDateRequest.getRequestType());
			
			//Validaciones en caso de registro SELID
			if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(settlementDateRequest.getRequestType())){
				
				//Validamos que la nueva fecha de liquidacion no sea mayor que la fecha de vencimiento del valor //issue 1040
				if(Validations.validateIsNotNull(selectedSettlementDate.getSettlementOperation().getMechanismOperation().getSecurities().getExpirationDate()) &&
						CommonsUtilities.truncateDateTime(selectedSettlementDate.getSettlementDate()).
							after(CommonsUtilities.truncateDateTime(selectedSettlementDate.getSettlementOperation().getMechanismOperation().getSecurities().getExpirationDate()))){
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("msg.settlement.dateExchange.newSettDate.greater.selid"));
							JSFUtilities.showSimpleValidationDialog();
							
							selectedSettlementDate.setSelected(false);
							
							return;
				}
				
				//Validamos que la operacion seleccionada no se encuentre en una cadena Confirmada //issue 1040
				if(prepaidExtendedFacade.verifyChainedOperationSelid(
						selectedSettlementDate.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk(),
						CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()))
						.equals(false)){
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("msg.settlement.dateExchange.chained.greater.selid"));
							JSFUtilities.showSimpleValidationDialog();
							selectedSettlementDate.setSelected(false);
							
							return;

				}
			}
			
			if(SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(settlementDateRequest.getRequestType())){
				if(ComponentConstant.CASH_PART.equals(selectedSettlementDate.getOperationPart())){
					prepaidExtendedFacade.checkAssignmentProcess(selectedSettlementDate.getSettlementOperation());
				}
			}
			
			if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(settlementDateRequest.getRequestType())){
				
				if(selectedSettlementDate.getSettlementOperation().getSettlementDate().equals(CommonsUtilities.currentDate())){
					selectedSettlementDate.setSelected(false);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("msg.settlement.dateExchange.settDate.today"));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				Long originParticipant = settlementDateRequest.getSourceParticipant().getIdParticipantPk();
				Long targetParticipant = settlementDateRequest.getTargetParticipant().getIdParticipantPk();
				Integer idRole = null;
				if(originParticipant.equals(targetParticipant)){
					registerPrepaidExtendedTO.setBlCrossOperation(true);
				}else{
					idRole = settlementDateRequest.getParticipantRole();
				}
				registerPrepaidExtendedTO.setOperationRole(settlementDateRequest.getParticipantRole());
				registerPrepaidExtendedTO.setMaxQuantity(selectedSettlementDate.getSettlementOperation().getStockQuantity());
				
//				if(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(settlementDateRequest.getAnticipationType())){
//					getHolderAccountOperations(selectedSettlementDate, idRole, originParticipant, settlementDateRequest.getAnticipationType());
//				}
				getHolderAccountOperations(selectedSettlementDate, idRole, originParticipant, settlementDateRequest.getAnticipationType());
				
				selectedDateOperation = selectedSettlementDate;
				
				Boolean changeOk = onChangeSettlementDays(selectedSettlementDate);
				
				if(changeOk && SettlementAnticipationType.SETTLEMENT_TOTAL.getCode().equals(settlementDateRequest.getAnticipationType())){
					updateOperationAmounts();
				}
			}			
		}
	}
	
	/**
	 * Gets the holder account operations.
	 *
	 * @param selectedSettlementDate the selected settlement date
	 * @param idRole the id role
	 * @param idParticipant the id participant
	 * @return the holder account operations
	 */
	public void getHolderAccountOperations(SettlementDateOperation selectedSettlementDate, Integer idRole, Long idParticipant, Integer anticipationType){
		List<SettlementAccountOperation> lstAccounts = prepaidExtendedFacade.searchSettlementAccountOperations(
				selectedSettlementDate.getSettlementOperation().getIdSettlementOperationPk(), idRole,  idParticipant);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstAccounts)){
			lstSettlementDateAccounts = new GenericDataModel<SettlementDateAccount>();
			for(SettlementAccountOperation settlementAccountOperation : lstAccounts){
				settlementAccountOperation.setSettlementAccountMarketfacts(prepaidExtendedFacade.getSettlementAccountMarketfacts(
																			settlementAccountOperation.getIdSettlementAccountPk()));
				SettlementDateAccount settlementDateAccount = new SettlementDateAccount();
				settlementDateAccount.setSettlementAccountOperation(settlementAccountOperation);
				if(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(anticipationType)){
					settlementDateAccount.setStockQuantity(BigDecimal.ZERO);
					settlementDateAccount.setSettlementAmount(BigDecimal.ZERO);
				} else {
					
					//issue 574
					if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(selectedSettlementDate.getSettlementRequest().getRequestType())){
						settlementDateAccount.setStockQuantity(settlementAccountOperation.getStockQuantity());
					}else{
						settlementDateAccount.setStockQuantity(settlementAccountOperation.getStockQuantity().subtract(settlementAccountOperation.getSettledQuantity()));
					}
					
					settlementDateAccount.setSettlementAmount(settlementAccountOperation.getSettlementAmount());
					settlementDateAccount.setSelected(true);
					settlementDateAccount.setTotalPrepaid(true);
				}
				settlementDateAccount.setSettlementDateOperation(selectedSettlementDate);
				lstSettlementDateAccounts.getDataList().add(settlementDateAccount);
			}
		}
	}
	
	/**
	 * On change select account.
	 *
	 * @param settlementDateAccount the settlement date account
	 */
	public void onChangeSelectAccount(SettlementDateAccount settlementDateAccount){
		if(settlementDateAccount.getSelected()){
			if(settlementDateAccount.getSettlementDateOperation().getSettlementDays()==null){
				settlementDateAccount.setSelected(false);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_DAYS_EMPTY));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
		}else{
			if(settlementDateAccount.getStockQuantity().compareTo(BigDecimal.ZERO) > 0){
				settlementDateAccount.setSelected(false);
			}
			updateOperationAmounts();
			
		}
		//updateOperationAmounts();
		settlementDateAccount.setStockQuantity(BigDecimal.ZERO);
		settlementDateAccount.setSettlementAmount(BigDecimal.ZERO);
	}
	
	/**
	 * Update operation amounts.
	 *
	 * @return true, if successful
	 */
	private boolean updateOperationAmounts(){
		registerPrepaidExtendedTO.setSaleQuantity(BigDecimal.ZERO);
		registerPrepaidExtendedTO.setSaleAmount(BigDecimal.ZERO);
		registerPrepaidExtendedTO.setPurchaseQuantity(BigDecimal.ZERO);
		registerPrepaidExtendedTO.setPurchaseAmount(BigDecimal.ZERO);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementDateAccounts.getDataList())){
			BigDecimal opSaleQuantity = BigDecimal.ZERO;
			BigDecimal opSaleAmount = BigDecimal.ZERO;
			BigDecimal opPurchaseQuantity = BigDecimal.ZERO;
			BigDecimal opPurchaseAmount = BigDecimal.ZERO;
			for(SettlementDateAccount settlementDateAccount : lstSettlementDateAccounts.getDataList()){
				
				Integer operationCurrency = settlementDateAccount.getSettlementDateOperation().getSettlementOperation().getMechanismOperation().getCurrency();
				BigDecimal rate = settlementDateAccount.getSettlementDateOperation().getSettlementOperation().getAmountRate();				
				BigDecimal settlementPrice = settlementDateAccount.getSettlementDateOperation().getSettlementPrice();
				BigDecimal cashPrice = settlementDateAccount.getSettlementDateOperation().getSettlementOperation().getMechanismOperation().getCashPrice();
				BigDecimal cashSettlementPrice = settlementDateAccount.getSettlementDateOperation().getSettlementOperation().getMechanismOperation().getCashSettlementPrice();				
				DailyExchangeRates dailyExchange = null;
				BigDecimal realPrice= null;
				ParameterTable currencyParameter = mpParameterObjects.get(operationCurrency);
				if(currencyParameter != null && currencyParameter.getIndicator2().equals(GeneralConstants.ZERO_VALUE_STRING) && 
						!currencyParameter.getIndicator4().equals(currencyParameter.getIndicator5())){
					dailyExchange = generalParameterFacade.getdailyExchangeRate(CommonsUtilities.currentDate(), operationCurrency);
					if(dailyExchange == null){
						try {
							throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST);
						} catch (ServiceException e) {
							settlementDateAccount.getSettlementDateOperation().setSettlementDays(null);
							settlementDateAccount.getSettlementDateOperation().setSettlementDate(null);
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
							JSFUtilities.showSimpleValidationDialog();
						}
					}
					realPrice = cashPrice;
				} else{
					realPrice = cashSettlementPrice;
				}
				
			
				BigDecimal newPrice = null;
				BigDecimal settlementAmount = BigDecimal.ZERO;
				/*
				if(selectedRequest!=null && selectedRequest.getRequestState()!=null && selectedRequest.getRequestType()!=null && selectedRequest.getAnticipationType()!=null &&
						selectedRequest.getRequestState().equals(SettlementDateRequestStateType.APPROVED.getCode()) &&
						selectedRequest.getRequestType().equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode())){
					newPrice = settlementPrice;
					//settlementAmount= settlementDateAccount.getSettlementAmount();
					settlementAmount = NegotiationUtils.getSettlementAmount(newPrice, settlementDateAccount.getStockQuantity());
				}else{
					newPrice = NegotiationUtils.getTermPrice(settlementDateAccount.getSettlementDateOperation().getOldSettlementPrice(), rate, 
																settlementDateAccount.getSettlementDateOperation().getSettlementDays());
					if(dailyExchange!=null){
						settlementAmount = NegotiationUtils.getSettlementAmount(newPrice, settlementDateAccount.getStockQuantity()).
																			multiply(dailyExchange.getBuyPrice()).setScale(2, RoundingMode.HALF_UP);
						settlementDateAccount.getSettlementDateOperation().setSettlementPrice(
														settlementAmount.divide(settlementDateAccount.getStockQuantity(), 2, RoundingMode.HALF_UP));
					}else{
						settlementAmount = NegotiationUtils.getSettlementAmount(newPrice, settlementDateAccount.getStockQuantity());
						settlementDateAccount.getSettlementDateOperation().setSettlementPrice(newPrice);
					}
				}
				*/
				newPrice = NegotiationUtils.getTermPrice(realPrice, rate, settlementDateAccount.getSettlementDateOperation().getSettlementDays());
				if (settlementDateAccount.getStockQuantity().compareTo(BigDecimal.ZERO) > 0) {
					if(dailyExchange!=null){
						settlementAmount = NegotiationUtils.getSettlementAmount(newPrice, settlementDateAccount.getStockQuantity()).
																				multiply(dailyExchange.getBuyPrice()).setScale(2, RoundingMode.HALF_UP);
						settlementDateAccount.getSettlementDateOperation().setSettlementPrice(
															settlementAmount.divide(settlementDateAccount.getStockQuantity(), 2, RoundingMode.HALF_UP));
					}else{
						settlementAmount = NegotiationUtils.getSettlementAmount(newPrice, settlementDateAccount.getStockQuantity());
						settlementDateAccount.getSettlementDateOperation().setSettlementPrice(newPrice);
					}
				}
								
				if(settlementDateAccount.getSettlementAccountOperation().getRole().equals(ComponentConstant.SALE_ROLE)){
					
					if(settlementDateAccount.getSelected()){
						opSaleQuantity = opSaleQuantity.add(settlementDateAccount.getStockQuantity());
						opSaleAmount = opSaleAmount.add(settlementAmount);
					}else{
						opSaleQuantity = opSaleQuantity.subtract(settlementDateAccount.getStockQuantity());
						opSaleAmount = opSaleAmount.subtract(settlementAmount);
					}
				}else if(settlementDateAccount.getSettlementAccountOperation().getRole().equals(ComponentConstant.PURCHARSE_ROLE)){
					if(settlementDateAccount.getSelected()){
						opPurchaseQuantity = opPurchaseQuantity.add(settlementDateAccount.getStockQuantity());
						opPurchaseAmount = opPurchaseAmount.add(settlementAmount);
					}else{
						opPurchaseQuantity = opPurchaseQuantity.subtract(settlementDateAccount.getStockQuantity());
						opPurchaseAmount = opPurchaseAmount.subtract(settlementAmount);
					}
				}
				
				BigDecimal newAmount = settlementAmount;
				settlementDateAccount.setSettlementAmount(newAmount);
			}
			
			if(opSaleQuantity.compareTo(registerPrepaidExtendedTO.getMaxQuantity()) > 0 || 
					opPurchaseQuantity.compareTo(registerPrepaidExtendedTO.getMaxQuantity()) > 0){
				registerPrepaidExtendedTO.setSaleQuantity(BigDecimal.ZERO);
				registerPrepaidExtendedTO.setSaleAmount(BigDecimal.ZERO);
				registerPrepaidExtendedTO.setPurchaseQuantity(BigDecimal.ZERO);
				registerPrepaidExtendedTO.setPurchaseAmount(BigDecimal.ZERO);
				String message = null;
				if(isViewOperationReview()){
					message = PropertiesConstants.MSG_SETTLEMENT_REQUEST_ACCOUNT_QTY_ZERO_REVIEW;
				}else{
					message = PropertiesConstants.MSG_SETTLEMENT_REQUEST_ACCOUNT_QTY_ZERO;
				}
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(message,new Object[]{registerPrepaidExtendedTO.getMaxQuantity().toString()}));
				JSFUtilities.showSimpleValidationDialog();
				
				return false;
			}
			
			if(registerPrepaidExtendedTO.isBlCrossOperation()){
				registerPrepaidExtendedTO.setSaleQuantity(opSaleQuantity);
				registerPrepaidExtendedTO.setSaleAmount(opSaleAmount);
				registerPrepaidExtendedTO.setPurchaseQuantity(opPurchaseQuantity);
				registerPrepaidExtendedTO.setPurchaseAmount(opPurchaseAmount);
			}else{
				if(registerPrepaidExtendedTO.getOperationRole().equals(ComponentConstant.SALE_ROLE)){
					registerPrepaidExtendedTO.setSaleQuantity(opSaleQuantity);
					registerPrepaidExtendedTO.setSaleAmount(opSaleAmount);
				}else if(registerPrepaidExtendedTO.getOperationRole().equals(ComponentConstant.PURCHARSE_ROLE)){
					registerPrepaidExtendedTO.setPurchaseQuantity(opPurchaseQuantity);
					registerPrepaidExtendedTO.setPurchaseAmount(opPurchaseAmount);
				}
				
			}
			
		}
		return true;
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param settlementDateAccount the settlement date account
	 */
	public void showMarketFactUI(SettlementDateAccount settlementDateAccount){
		registerPrepaidExtendedTO.setSelectedDateAccount(settlementDateAccount);
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		Security security = settlementDateAccount.getSettlementDateOperation().getSettlementOperation().getMechanismOperation().getSecurities();
			
		marketFactBalance.setSecurityCodePk(security.getIdSecurityCodePk());
		marketFactBalance.setInstrumentType(security.getInstrumentType());
		marketFactBalance.setInstrumentDescription(security.getInstrumentTypeDescription());
		marketFactBalance.setUiComponentName("balancemarket");
		marketFactBalance.setIndHandleDetails(ComponentConstant.ONE);
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		BigDecimal totalBalance = BigDecimal.ZERO;
		for(SettlementAccountMarketfact accountMarketFact : settlementDateAccount.getSettlementAccountOperation().getSettlementAccountMarketfacts()){
			MarketFactDetailHelpTO marketFactDetailHelpTO = new MarketFactDetailHelpTO();
			marketFactDetailHelpTO.setMarketDate(accountMarketFact.getMarketDate());
			marketFactDetailHelpTO.setMarketPrice(accountMarketFact.getMarketPrice());
			marketFactDetailHelpTO.setMarketRate(accountMarketFact.getMarketRate());
			marketFactDetailHelpTO.setTotalBalance(accountMarketFact.getMarketQuantity());
			marketFactDetailHelpTO.setAvailableBalance(accountMarketFact.getMarketQuantity());
			if (settlementDateAccount.isTotalPrepaid()) {
				marketFactDetailHelpTO.setEnteredBalance(marketFactDetailHelpTO.getAvailableBalance());
				marketFactDetailHelpTO.setIsSelected(true);
				totalBalance= totalBalance.add(marketFactDetailHelpTO.getEnteredBalance());
			}
			marketFactDetailHelpTO.setIsRendered(true);
			marketFactDetailHelpTO.setMarketFactBalanceTO(marketFactBalance);
			marketFactBalance.getMarketFacBalances().add(marketFactDetailHelpTO);
		}
		
		marketFactBalance.setBalanceResult(totalBalance);
		//marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		SettlementDateAccount selectedDateAccount = registerPrepaidExtendedTO.getSelectedDateAccount();
		selectedDateAccount.setSettlementDateMarketfacts(new ArrayList<SettlementDateMarketfact>());
		
		BigDecimal totalQuantity = BigDecimal.ZERO;
		for(MarketFactDetailHelpTO marketFactDetailHelpTO : holderMarketFactBalance.getMarketFacBalances()){
			SettlementDateMarketfact marketFact = new SettlementDateMarketfact();
			
			marketFact.setMarketDate(marketFactDetailHelpTO.getMarketDate());
			marketFact.setMarketRate(marketFactDetailHelpTO.getMarketRate());
			marketFact.setMarketPrice(marketFactDetailHelpTO.getMarketPrice());
			marketFact.setMarketQuantity(marketFactDetailHelpTO.getEnteredBalance());
			marketFact.setSettlementDateAccount(selectedDateAccount);
			
			totalQuantity = totalQuantity.add(marketFactDetailHelpTO.getEnteredBalance());
			selectedDateAccount.getSettlementDateMarketfacts().add(marketFact);
		}
		selectedDateAccount.setStockQuantity(totalQuantity);
		onChangeAccountQuantity(selectedDateAccount);
		
		JSFUtilities.updateComponent("frmPrepaidExtendedRequest:pnlOperationAccounts");
		JSFUtilities.updateComponent("frmPrepaidExtendedRequest:pnlSummary");
		JSFUtilities.updateComponent("opnlGenericDialogs");
	}
	
	/**
	 * On change account quantity.
	 *
	 * @param settlementDateAccount the settlement date account
	 */
	public void onChangeAccountQuantity(SettlementDateAccount settlementDateAccount){
		BigDecimal enteredQuantity = settlementDateAccount.getStockQuantity();
		if(enteredQuantity==null){
			settlementDateAccount.setStockQuantity(BigDecimal.ZERO);
		}
		if(!updateOperationAmounts()){
			settlementDateAccount.setStockQuantity(BigDecimal.ZERO);
			settlementDateAccount.setSettlementAmount(BigDecimal.ZERO);
		};
	}
	
	/**
	 * METODO PARA LIMPIAR EL RESULTADO.
	 */
	
	public void cleanOperationsResult(){
		JSFUtilities.hideGeneralDialogues();
		lstSettlementDateNegotiations = null;
		lstSettlementDateAccounts = null;
		selectedDateOperation = null;
	}
	
	/**
	 * Clean result.
	 */
	public void cleanResult(){
		JSFUtilities.hideGeneralDialogues();
		lstSettlementDateNegotiations = null;
		lstSettlementDateAccounts = null;
		selectedDateOperation = null;
		registerPrepaidExtendedTO.setRequestTypeSelected(null);
		registerPrepaidExtendedTO.setAnticipationTypeSelected(null);
		registerPrepaidExtendedTO.setRoleSelected(null);
		registerPrepaidExtendedTO.setBlExtended(false);
		registerPrepaidExtendedTO.setBlPrepaid(false);
		registerPrepaidExtendedTO.setBlSettlementExchange(false);
		registerPrepaidExtendedTO.setBlSpecialSettlementExchange(false);
	}
	
	/**
	 * Clean partial result.
	 */
	public void cleanPartialResult(){
		JSFUtilities.hideGeneralDialogues();
		
		lstSettlementDateNegotiations = null;
		lstSettlementDateAccounts = null;
		selectedDateOperation = null;
		if(!participantUser){
			registerPrepaidExtendedTO.setIdParticipant(null);
			//registerPrepaidExtendedTO.setIdCounterPartParticipant(null);
		}
		registerPrepaidExtendedTO.setIdCounterPartParticipant(null);
		registerPrepaidExtendedTO.setAnticipationTypeSelected(null);
		registerPrepaidExtendedTO.setRoleSelected(null);
		registerPrepaidExtendedTO.setBlExtended(false);
		registerPrepaidExtendedTO.setBlPrepaid(false);
		registerPrepaidExtendedTO.setBlSettlementExchange(false);
		registerPrepaidExtendedTO.setBlSpecialSettlementExchange(false);
	}
	
	/**
	 * METODO ANTES DE GRABAR
	 * Utilizado en el boton grabar de la vista.
	 */
	
	public void beforeSave(){
		if(settlementDateRequest != null && isAnySelectedSettlementOperation()){
			for(SettlementDateOperation settlementDateOperation : lstSettlementDateNegotiations.getDataList()){
				if(settlementDateOperation.isSelected()){
					if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(settlementDateRequest.getRequestType())){
						if(settlementDateOperation.getSettlementDays()==null){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_DAYS_EMPTY));
							JSFUtilities.showSimpleValidationDialog();
							return;
						}
						
						if(registerPrepaidExtendedTO.getStockQuantity().compareTo(BigDecimal.ZERO) == 0){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_ACCOUNT_NOT_SELECTED));
							JSFUtilities.showSimpleValidationDialog();
							return;
						}
						
						if(registerPrepaidExtendedTO.isBlCrossOperation() && registerPrepaidExtendedTO.getSaleQuantity().compareTo(registerPrepaidExtendedTO.getPurchaseQuantity()) != 0){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_OPERATION_CROSS_DIFFERS));
							JSFUtilities.showSimpleValidationDialog();
							return;
						}
						
						if(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(settlementDateRequest.getAnticipationType()) && 
								registerPrepaidExtendedTO.getStockQuantity().compareTo(settlementDateOperation.getSettlementOperation().getStockQuantity()) >= 0){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_ACCOUNT_LESS_QTY));
							JSFUtilities.showSimpleValidationDialog();
							return;
						}										
					}
					
					if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(settlementDateRequest.getRequestType())){
						if (settlementDateOperation.getSettlementDays()==null) { 
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_DAYS_EMPTY));
							JSFUtilities.showSimpleValidationDialog();
							return;
						}else {
							Integer daysMaxAmp = mcnOperationServiceFacade.findDaysMaxAmp(settlementDateOperation, this.registerPrepaidExtendedTO);
							
							if(settlementDateOperation.getSettlementDays() > daysMaxAmp) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_MAXIMUM_EXTENSION_DAYS,
											new Object[]{daysMaxAmp.toString()}));
							JSFUtilities.showSimpleValidationDialog();
							settlementDateOperation.setSettlementDays(daysMaxAmp.longValue());
							return;
								}
							}	
						}
					}
				}
			
			question(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					 PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_CONFIRM_REGISTER, 
							 new Object[]{mpParameters.get(settlementDateRequest.getRequestType())}));
				
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_REQUEST_OPERATION_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	
	/**
	 * Checks if is any selected settlement operation.
	 *
	 * @return true, if is any selected settlement operation
	 */
	public boolean isAnySelectedSettlementOperation(){
		if(lstSettlementDateNegotiations!=null){
			for (SettlementDateOperation settlementDateOp : lstSettlementDateNegotiations.getDataList()) {
				if(settlementDateOp.isSelected()){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * METODO PARA GRABAR LA SOLICITUD
	 * Utilizado en el boton si que aparece despues del boton grabar
	 * Detalles del proceso en SaveRequest.
	 */
	
	@LoggerAuditWeb
	public void saveRequest(){
		JSFUtilities.hideGeneralDialogues();
		try {
			
			List<SettlementDateAccount> newDateAccounts =null;
			settlementDateRequest.setLstSettlementDateOperations(new ArrayList<SettlementDateOperation>());
			for(SettlementDateOperation settlementDateOperation : lstSettlementDateNegotiations.getDataList()){
				if(settlementDateOperation.isSelected()){
					if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(settlementDateRequest.getRequestType())){
						settlementDateOperation.setStockQuantity(registerPrepaidExtendedTO.getStockQuantity());
						settlementDateOperation.setSettlementAmount(registerPrepaidExtendedTO.getSettlementAmount());
						if(lstSettlementDateAccounts!=null){
						//if (SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(settlementDateRequest.getAnticipationType())) {
							newDateAccounts = new ArrayList<SettlementDateAccount>();
							for(SettlementDateAccount settlementDateAccount : lstSettlementDateAccounts.getDataList()){
								if(settlementDateAccount.getSelected() && settlementDateAccount.getStockQuantity().compareTo(BigDecimal.ZERO) > 0){
									newDateAccounts.add(settlementDateAccount);
								}
							}
						}
					}
					settlementDateRequest.getLstSettlementDateOperations().add(settlementDateOperation);
				}
			}
			
			boolean isDepositary = Boolean.FALSE;
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				isDepositary = Boolean.TRUE; 
			}
			
			prepaidExtendedFacade.saveRequest(settlementDateRequest, newDateAccounts, isDepositary);
			
			// Send Notification (Is not depositary institution user)			
			if(!isDepositary && Validations.validateIsNotNullAndNotEmpty(registerPrepaidExtendedTO.getIdParticipant())) {	
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTENSION_ADVANCE_REGISTER_REQUEST.getCode());				
				Object[] dataNotification = prepaidExtendedFacade.getDataNotification(settlementDateRequest.getIdSettlementRequestPk());							
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
						registerPrepaidExtendedTO.getIdParticipant(), dataNotification, userInfo.getUserAccountSession());
			}				
			
			Object[] bodyData = new Object[2];
			bodyData[0] = mpParameters.get(settlementDateRequest.getRequestType());
			bodyData[1] = settlementDateRequest.getIdSettlementRequestPk().toString();
			endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
					  PropertiesUtilities.getMessage("msg.settlement.dateExchange.register", bodyData));
			
		} catch (ServiceException e) {
			settlementDateRequest.setLstSettlementDateOperations(null);
			endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					  PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		JSFUtilities.hideGeneralDialogues();
		return validateAction(SettlementDateRequestStateType.CONFIRMED.getCode());
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		JSFUtilities.hideGeneralDialogues();
		return validateAction(SettlementDateRequestStateType.REJECTED.getCode());
	}
	
	/**
	 * Validate approve.
	 *
	 * @return the string
	 */
	public String validateApprove(){
		JSFUtilities.hideGeneralDialogues();
		return validateAction(SettlementDateRequestStateType.APPROVED.getCode());
	}
	
	/**
	 * Validate annular.
	 *
	 * @return the string
	 */
	public String validateAnnular(){
		JSFUtilities.hideGeneralDialogues();
		return validateAction(SettlementDateRequestStateType.ANNULATE.getCode());
	}
	
	/**
	 * Validate review.
	 *
	 * @return the string
	 */
	public String validateReview(){
		JSFUtilities.hideGeneralDialogues();
		return validateAction(SettlementDateRequestStateType.REVIEW.getCode());
	}
	
	/**
	 * Validate authorize.
	 *
	 * @return the string
	 */
	public String validateAuthorize(){
		JSFUtilities.hideGeneralDialogues();
		return validateAction(SettlementDateRequestStateType.AUTHORIZE.getCode());
	}
	
	/**
	 * Ask back.
	 *
	 * @return the string
	 */
	public String askBack(){
		if(validateRequiredFields()){
			return "backToSearch";	
		}else{
			backDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					   PropertiesUtilities.getGenericMessage("conf.msg.back"));
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * METODO QUE EJECUTA EL BOTON LIMPIAR.
	 *
	 * @return the string
	 */
	
	public String askClean(){
		if(validateRequiredFields()){
			return cleanRegister();
		}else{
			cleanDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					    PropertiesUtilities.getGenericMessage("conf.msg.clean"));
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Validate required fields.
	 *
	 * @return true, if successful
	 */
	private boolean validateRequiredFields(){
		if(Validations.validateIsNullOrEmpty(registerPrepaidExtendedTO.getIdNegotiationMechanismPk())
			&& Validations.validateIsNullOrEmpty(registerPrepaidExtendedTO.getIdNegotiationModalityPk())
			&& (participantUser || Validations.validateIsNullOrEmpty(registerPrepaidExtendedTO.getIdParticipant()))
			&& Validations.validateIsNullOrEmpty(registerPrepaidExtendedTO.getRequestTypeSelected())
			&& Validations.validateIsNullOrEmpty(registerPrepaidExtendedTO.getRoleSelected())){
			return true;
		}
		return false;
	}
	
	/**
	 * Confirm request.
	 *
	 * @param bodyData the body data
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	private void confirmRequest(Object[] bodyData) throws ServiceException{		
		boolean isDepositary = Boolean.FALSE;
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			isDepositary = Boolean.TRUE; 
		}
		
		Long idParticipant = (Long) bodyData[2];
		prepaidExtendedFacade.confirmRequest(selectedRequest.getIdSettlementRequest(), idParticipant ,isDepositary);
		
		// Send notification
		if(!isDepositary) {			
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTENSION_ADVANCE_CONFIRMATION_REQUEST.getCode());	
			Object[] dataNotification = prepaidExtendedFacade.getDataNotification(selectedRequest.getIdSettlementRequest());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, null, dataNotification);
		}
						
		endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				  PropertiesUtilities.getMessage("msg.settlement.dateExchange.confirm", bodyData));
	}
	
	/**
	 * Reject request.
	 *
	 * @param bodyData the body data
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	private void rejectRequest(Object[] bodyData) throws ServiceException{
		Long idParticipant = (Long) bodyData[2];
		prepaidExtendedFacade.rejectRequest(selectedRequest.getIdSettlementRequest(), idParticipant ,true);
		endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				 PropertiesUtilities.getMessage("msg.settlement.dateExchange.reject", bodyData));
	}
	
	/**
	 * Authorize request.
	 *
	 * @param bodyData the body data
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	private void authorizeRequest(Object[] bodyData) throws ServiceException{
		prepaidExtendedFacade.authorizeRequest(selectedRequest.getIdSettlementRequest(),registerPrepaidExtendedTO.getLstSettlementDateOperation(),true);
		endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				  PropertiesUtilities.getMessage("msg.settlement.dateExchange.authorize", bodyData));
	}
	
	/**
	 * METODO PARA REVISAR SOLICITUD.
	 *
	 * @param bodyData the body data
	 * @throws ServiceException the service exception
	 */
	
	@LoggerAuditWeb
	private void reviewRequest(Object[] bodyData) throws ServiceException{		
		boolean isDepositary = Boolean.FALSE;
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			isDepositary = Boolean.TRUE; 
		}	
		
		Long idParticipant = (Long) bodyData[2];
		List<SettlementDateAccount> settlementDateAccounts = null;
		if(selectedRequest.getRequestType().equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode()) && lstSettlementDateAccounts !=null){
			settlementDateAccounts = new ArrayList<SettlementDateAccount>();
			for(SettlementDateAccount settlementDateAccount : lstSettlementDateAccounts.getDataList()){
				if(settlementDateAccount.getSelected()){
					settlementDateAccounts.add(settlementDateAccount);
				}
			}
		}
		
		prepaidExtendedFacade.reviewRequest(selectedRequest.getIdSettlementRequest(), settlementDateAccounts, idParticipant , isDepositary);
		
		// Send Notification (Is not depositary institution user)			
		if(!isDepositary && Validations.validateIsNotNullAndNotEmpty(selectedRequest.getTargetParticipant().getIdParticipantPk())) {	
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTENSION_ADVANCE_REVIEW_REQUEST.getCode());				
			Object[] dataNotification = prepaidExtendedFacade.getDataNotification(selectedRequest.getIdSettlementRequest());							
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
					selectedRequest.getTargetParticipant().getIdParticipantPk(), dataNotification, userInfo.getUserAccountSession());
		}
		
		endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				  PropertiesUtilities.getMessage("msg.settlement.dateExchange.review", bodyData));
			
	}
	
	/**
	 * METODO PARA APROBAR SOLICITUD.
	 *
	 * @param bodyData the body data
	 * @throws ServiceException the service exception
	 */
	
	@LoggerAuditWeb
	private void approveRequest(Object[] bodyData) throws ServiceException{
		boolean isDepositary = Boolean.FALSE;
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			isDepositary = Boolean.TRUE; 
		}
		
		Long idParticipant = (Long) bodyData[2];						
		prepaidExtendedFacade.approveRequest(selectedRequest.getIdSettlementRequest(), idParticipant , isDepositary);
		
		// Send Notification (Is not depositary institution user)			
		if(!isDepositary && Validations.validateIsNotNullAndNotEmpty(selectedRequest.getTargetParticipant().getIdParticipantPk())) {	
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTENSION_ADVANCE_APPROVAL_REQUEST.getCode());				
			Object[] dataNotification = prepaidExtendedFacade.getDataNotification(selectedRequest.getIdSettlementRequest());							
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
					selectedRequest.getTargetParticipant().getIdParticipantPk(), dataNotification, userInfo.getUserAccountSession());
		}
		
		endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				  PropertiesUtilities.getMessage("msg.settlement.dateExchange.approve", bodyData));
		
	}
	
	/**
	 * METODO PARA ANULAR SOLICITUD.
	 *
	 * @param bodyData the body data
	 * @throws ServiceException the service exception
	 */
	
	@LoggerAuditWeb
	private void annulRequest(Object[] bodyData) throws ServiceException{
		prepaidExtendedFacade.annulRequest(selectedRequest.getIdSettlementRequest(), registerPrepaidExtendedTO.getIdParticipant(), true);
		endDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				  PropertiesUtilities.getMessage("msg.settlement.dateExchange.annul", bodyData));
	}

	
	/**
	 * METODO PARA LLENAR LOS COMBOS AL INICIAR.
	 */
	
	private void fillCombos(){
		try {
			
			ParameterTableTO  paramTabTO = new ParameterTableTO();
			paramTabTO.setLstMasterTableFk(new ArrayList<Integer>());
			paramTabTO.getLstMasterTableFk().add(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
			paramTabTO.getLstMasterTableFk().add(MasterTableType.INSTRUMENT_TYPE.getCode());
			paramTabTO.getLstMasterTableFk().add(MasterTableType.SETTLEMENT_ANTICIPATION_TYPE.getCode());
			paramTabTO.getLstMasterTableFk().add(MasterTableType.CURRENCY.getCode());
			
			for(ParameterTable param : generalParameterFacade.getListParameterTableServiceBean(paramTabTO)){
				mpParameters.put(param.getParameterTablePk(), param.getParameterName());
				mpParameterObjects.put(param.getParameterTablePk(), param);
			}
			
			// setetlement type exhcange types
			paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setMasterTableFk(MasterTableType.SETTLEMENT_TYPE_EXCHANGE_TYPE.getCode());

			searchPrepaidExtendedTO.setLstSettlementExchangeType(generalParameterFacade.getListParameterTableServiceBean(paramTabTO));
			
			// request types
			paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setIndicator4(ComponentConstant.ONE);
			paramTabTO.setOrderbyParameterTableCd(ComponentConstant.ONE);
			paramTabTO.setMasterTableFk(MasterTableType.SETTLEMENT_REQUEST_TYPE.getCode());

			searchPrepaidExtendedTO.setLstRequestType(generalParameterFacade.getListParameterTableServiceBean(paramTabTO));
			for(ParameterTable obj: searchPrepaidExtendedTO.getLstRequestType()){
				mpParameters.put(obj.getParameterTablePk(), obj.getParameterName());
			}
			
			// request states
			paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setMasterTableFk(MasterTableType.SETTLEMENT_REQUEST_STATE.getCode());
			searchPrepaidExtendedTO.setLstState(generalParameterFacade.getListParameterTableServiceBean(paramTabTO));
			for(ParameterTable obj: searchPrepaidExtendedTO.getLstState()){
				mpParameters.put(obj.getParameterTablePk(), obj.getParameterName());
			}
			
			searchPrepaidExtendedTO.setLstRole(NegotiationUtils.getLstRoles());
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * METODO QUE VALIDA SI ESTA SELECCIONADO UN REGISTRO.
	 *
	 * @param newState the new state
	 * @return the string
	 */
	
	public String validateAction(Integer newState){
		
		List<Integer> expectedStates = new ArrayList<Integer>();
		List<Long> expectedParticipants = new ArrayList<Long>();
		
		newMultipleState = newState;

		//Verificamos que se seleccionen registros 
		if(selectedRequestList.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			
			//Si es mas de un registro comenzamos a validar y recien enviamos a procesar solicitud a solicitud
			if(selectedRequestList.size() > GeneralConstants.ONE_VALUE_INTEGER){
				
				//Concatenamos los ID para mostrar en pantalla
				List <Integer> idsConcatReview = new ArrayList<>();
				Integer countMultipleSelar = 0;
						
				//Primero verificamos que no hayan SELAR solo cuando la opcion es REVISAR
				if(newMultipleState.equals(SettlementDateRequestStateType.REVIEW.getCode())){
							
					for (SettlementRequestTO settlementRequestValidate : selectedRequestList){
								
						if(settlementRequestValidate.getRequestType().equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode())){
									
							//Aumentamos contador
							countMultipleSelar++;
							//Concatenamos IDs del tipo de solicitud parcial
							idsConcatReview.add(settlementRequestValidate.getIdSettlementRequest().intValue());
						}
					}
				}
						
				//Si alguna de las solicitudes es parcial mandamos mensaje de error y no procedemos 
				if(countMultipleSelar > GeneralConstants.ZERO_VALUE_INTEGER){

					//Variable constante de error
					String messageProperties = PropertiesConstants.SELAR_OPERATION_REVIEW_ERROR_MULTIPLE;
							
					//Generamos el mensaje
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(messageProperties, new Object[]{StringUtils.join(idsConcatReview,",")}));
							
					//actualizamos la pantalla
					JSFUtilities.showSimpleEndTransactionDialog("searchSettlementDateRequest");
							
					return null;
					
				}
				
				//Segundo si es q no es SELAR y REVISAR validar que todas las opciones tengan el mismo estado valido para ejecutar la accion
				for (SettlementRequestTO settlementRequest : selectedRequestList){
					
					//Para aprobar o anular debe estar en registrado
					if(SettlementDateRequestStateType.APPROVED.getCode().equals(newState)
							||SettlementDateRequestStateType.ANNULATE.getCode().equals(newState)){
						if(!settlementRequest.getRequestState().equals(SettlementDateRequestStateType.REGISTERED.getCode())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
							JSFUtilities.showValidationDialog();
							return null;
						}
					}
					//Para revisar o rechazar debe estar en aprobado
					if(SettlementDateRequestStateType.REVIEW.getCode().equals(newState)
							||SettlementDateRequestStateType.REJECTED.getCode().equals(newState)){
						if(!settlementRequest.getRequestState().equals(SettlementDateRequestStateType.APPROVED.getCode())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
							JSFUtilities.showValidationDialog();
							return null;
						}
					}
					//Para confirmar debe estar en revisado
					if(SettlementDateRequestStateType.CONFIRMED.getCode().equals(newState)){
						if(!settlementRequest.getRequestState().equals(SettlementDateRequestStateType.REVIEW.getCode())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
							JSFUtilities.showValidationDialog();
							return null;
						}
					}
					//Para autorizar debe estar en confirmado
					if(SettlementDateRequestStateType.AUTHORIZE.getCode().equals(newState)){
						if(!settlementRequest.getRequestState().equals(SettlementDateRequestStateType.CONFIRMED.getCode())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
							JSFUtilities.showValidationDialog();
							return null;
						}
					}
				}
				
				//Si pasa las validaciones enviamos mensaje de confirmacion
				String messageProperties = "";
				
				switch(newState){
				
					case 1474:	//aprobada
									messageProperties = PropertiesConstants.SELAR_OPERATION_APPROVE_CONFIRM_MULTIPLE;
									break;
					case 1475:	//anulada
									messageProperties = PropertiesConstants.SELAR_OPERATION_ANNULATE_CONFIRM_MULTIPLE;
									break;
					case 1482: 	//revisada
									messageProperties = PropertiesConstants.SELAR_OPERATION_REVIEW_CONFIRM_MULTIPLE;
									break;
					case 1228:	//rechazada
									messageProperties = PropertiesConstants.SELAR_OPERATION_REJECT_CONFIRM_MULTIPLE;
									break;
					case 1229:	//confirmada	
									messageProperties = PropertiesConstants.SELAR_OPERATION_CONFIRM_CONFIRM_MULTIPLE;
									break;
					case 1543:	//autorizada
									messageProperties = PropertiesConstants.SELAR_OPERATION_AUTHORIZED_CONFIRM_MULTIPLE;
									break;
				}
				//Concatenamos los ID para mostrar en pantalla
				List <Integer> idsConcat = new ArrayList<>();
				for (SettlementRequestTO settlementRequest : selectedRequestList){
					idsConcat.add(settlementRequest.getIdSettlementRequest().intValue());
				}
				//Mostramos mensaje	
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(messageProperties,new Object[]{StringUtils.join(idsConcat,",")}));
				JSFUtilities.executeJavascriptFunction("PF('cnfwSelarSirtexOperations').show()");
				
				//TODO Debo retornar algo
				return null;
			}else{
				
				newMultipleState = null;
				
				if(SettlementDateRequestStateType.APPROVED.getCode().equals(newState)){
					expectedParticipants.add(selectedRequestList.get(0).getSourceParticipant().getIdParticipantPk());
					expectedStates.add(SettlementDateRequestStateType.REGISTERED.getCode());
					setViewOperationType(ViewOperationsType.APPROVE.getCode());
				}else if(SettlementDateRequestStateType.ANNULATE.getCode().equals(newState)){
					expectedParticipants.add(selectedRequestList.get(0).getSourceParticipant().getIdParticipantPk());
					expectedStates.add(SettlementDateRequestStateType.REGISTERED.getCode());
					setViewOperationType(ViewOperationsType.ANULATE.getCode());
				}else if(SettlementDateRequestStateType.REVIEW.getCode().equals(newState)){
					if(selectedRequestList.get(0).getTargetParticipant()!=null){
						expectedParticipants.add(selectedRequestList.get(0).getTargetParticipant().getIdParticipantPk());
					}
					expectedStates.add(SettlementDateRequestStateType.APPROVED.getCode());
					setViewOperationType(ViewOperationsType.REVIEW.getCode());
				}else if(SettlementDateRequestStateType.REJECTED.getCode().equals(newState)){
					if(selectedRequestList.get(0).getTargetParticipant()!=null){
						expectedParticipants.add(selectedRequestList.get(0).getTargetParticipant().getIdParticipantPk());
					}
					if(userInfo.getUserAccountSession().isParticipantInstitucion()){
						expectedStates.add(SettlementDateRequestStateType.REVIEW.getCode());
						expectedStates.add(SettlementDateRequestStateType.APPROVED.getCode());
					}else{
						expectedStates.add(SettlementDateRequestStateType.REVIEW.getCode());
						expectedStates.add(SettlementDateRequestStateType.APPROVED.getCode());
						expectedStates.add(SettlementDateRequestStateType.CONFIRMED.getCode());
					}
					
					setViewOperationType(ViewOperationsType.REJECT.getCode());
				}else if(SettlementDateRequestStateType.CONFIRMED.getCode().equals(newState)){
					if(selectedRequestList.get(0).getTargetParticipant()!=null){
						expectedParticipants.add(selectedRequestList.get(0).getTargetParticipant().getIdParticipantPk());	
					}
					expectedStates.add(SettlementDateRequestStateType.REVIEW.getCode());
					setViewOperationType(ViewOperationsType.CONFIRM.getCode());
				}else if(SettlementDateRequestStateType.AUTHORIZE.getCode().equals(newState)){
					expectedStates.add(SettlementDateRequestStateType.CONFIRMED.getCode());
					setViewOperationType(ViewOperationsType.AUTHORIZE.getCode());
				}
				
				if(participantUser && searchPrepaidExtendedTO.getIdParticipantPk()!=null && !expectedParticipants.contains(searchPrepaidExtendedTO.getIdParticipantPk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
							, PropertiesUtilities.getMessage("msg.settlement.dateExchange.invalidAction.participant"));
					JSFUtilities.showValidationDialog();
					return null;
				}
				
				if(!expectedStates.contains(selectedRequestList.get(0).getRequestState())){
					
					List<String> stringStates = new ArrayList<String>();
					for(Integer state : expectedStates){
						stringStates.add(mpParameters.get(state));
					}
					
					Object[] bodyData = new Object[]{selectedRequestList.get(0).getIdSettlementRequest().toString(),StringUtils.join(stringStates.toArray(),GeneralConstants.STR_COMMA_WITHOUT_SPACE)};
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
							, PropertiesUtilities.getMessage("msg.settlement.dateExchange.invalid.state", bodyData));
					JSFUtilities.showValidationDialog();
					setViewOperationType(ViewOperationsType.CONSULT.getCode());
					return null;
				}
				
				fillData(selectedRequestList.get(0));
				selectedRequest = selectedRequestList.get(0);
				return "viewDetail";
			}
		}else{
			//Si no se selecciona nungun registro enviamos mensaje
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
			return null;
		}
	}
	
	/**
	 * Metodo para ejecutar la accion seleccionada tras la confirmacion
	 * @param button
	 * @param listSelected
	 * @throws ServiceException
	 */
	@LoggerAuditWeb
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void executeActionMultiple () {
		
		searchPrepaidExtendedTO.getRequestTypeSelected();
		
		//Concatenamos los ID para mostrar en pantalla
		List <Integer> idsConcat = new ArrayList<>();
		
		Boolean error = false;
		
		//Realizamos la accion para cada solicitud
		for (SettlementRequestTO settlementRequestValidate : selectedRequestList){
			
			//Concatenamos IDs
			idsConcat.add(settlementRequestValidate.getIdSettlementRequest().intValue());
			
			try {
				
				//Si es ANULAR
				if(newMultipleState.equals(SettlementDateRequestStateType.ANNULATE.getCode())){
					prepaidExtendedFacade.annulRequest(settlementRequestValidate.getIdSettlementRequest(), settlementRequestValidate.getSourceParticipant().getIdParticipantPk(), true);
				}
				
				//Si es CONFIRMAR
				if(newMultipleState.equals(SettlementDateRequestStateType.CONFIRMED.getCode())){
					boolean isDepositary = Boolean.FALSE;
					if(userInfo.getUserAccountSession().isDepositaryInstitution()){
						isDepositary = Boolean.TRUE; 
					}
					
					//Long idParticipant = (Long) bodyData[2];
					prepaidExtendedFacade.confirmRequest(settlementRequestValidate.getIdSettlementRequest(), settlementRequestValidate.getTargetParticipant().getIdParticipantPk() ,isDepositary);
					
					// Send notification
					if(!isDepositary) {			
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTENSION_ADVANCE_CONFIRMATION_REQUEST.getCode());	
						Object[] dataNotification = prepaidExtendedFacade.getDataNotification(settlementRequestValidate.getIdSettlementRequest());
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								   businessProcess, null, dataNotification);
					}
				}
				//Si es APROBAR
				if(newMultipleState.equals(SettlementDateRequestStateType.APPROVED.getCode())){
					boolean isDepositary = Boolean.FALSE;
					if(userInfo.getUserAccountSession().isDepositaryInstitution()){
						isDepositary = Boolean.TRUE; 
					}
					
					//Long idParticipant = (Long) bodyData[2];						
					prepaidExtendedFacade.approveRequest(settlementRequestValidate.getIdSettlementRequest(), settlementRequestValidate.getSourceParticipant().getIdParticipantPk() , isDepositary);
					
					// Send Notification (Is not depositary institution user)			
					if(!isDepositary && Validations.validateIsNotNullAndNotEmpty(settlementRequestValidate.getTargetParticipant().getIdParticipantPk())) {	
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTENSION_ADVANCE_APPROVAL_REQUEST.getCode());				
						Object[] dataNotification = prepaidExtendedFacade.getDataNotification(settlementRequestValidate.getIdSettlementRequest());							
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
								settlementRequestValidate.getTargetParticipant().getIdParticipantPk(), dataNotification, userInfo.getUserAccountSession());
					}
				}
				
				//Si es REVISAR
				if(newMultipleState.equals(SettlementDateRequestStateType.REVIEW.getCode())){
					
					boolean isDepositary = Boolean.FALSE;
					if(userInfo.getUserAccountSession().isDepositaryInstitution()){
						isDepositary = Boolean.TRUE; 
					}	
					
					//Long idParticipant = (Long) bodyData[2];
					List<SettlementDateAccount> settlementDateAccounts = null;
					/*if(settlementRequestValidate.getRequestType().equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode()) && lstSettlementDateAccounts !=null){
						settlementDateAccounts = new ArrayList<SettlementDateAccount>();
						for(SettlementDateAccount settlementDateAccount : lstSettlementDateAccounts.getDataList()){
							if(settlementDateAccount.getSelected()){
								settlementDateAccounts.add(settlementDateAccount);
							}
						}
					}*/
					
					prepaidExtendedFacade.reviewRequestMultiple(settlementRequestValidate.getIdSettlementRequest(), settlementDateAccounts,  settlementRequestValidate.getTargetParticipant().getIdParticipantPk() , isDepositary);
					
					// Send Notification (Is not depositary institution user)			
					if(!isDepositary && Validations.validateIsNotNullAndNotEmpty(settlementRequestValidate.getTargetParticipant().getIdParticipantPk())) {	
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTENSION_ADVANCE_REVIEW_REQUEST.getCode());				
						Object[] dataNotification = prepaidExtendedFacade.getDataNotification(settlementRequestValidate.getIdSettlementRequest());							
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
								settlementRequestValidate.getTargetParticipant().getIdParticipantPk(), dataNotification, userInfo.getUserAccountSession());
					}
				}
				
				//Si es AUTORIZAR
				if(newMultipleState.equals(SettlementDateRequestStateType.AUTHORIZE.getCode())){
					
					registerPrepaidExtendedTO = new RegisterPrepaidExtendedTO();
					
					registerPrepaidExtendedTO.setLstSettlementDateOperation(prepaidExtendedFacade.getSettlementDateOperations(
							settlementRequestValidate.getIdSettlementRequest(),true, userInfo.getUserAccountSession().getParticipantCode()));
					
					prepaidExtendedFacade.authorizeRequest(settlementRequestValidate.getIdSettlementRequest(),registerPrepaidExtendedTO.getLstSettlementDateOperation(),true);
				}
				//Si es rechazar
				if(newMultipleState.equals(SettlementDateRequestStateType.REJECTED.getCode())){
					//Long idParticipant = (Long) bodyData[2];
					prepaidExtendedFacade.rejectRequest(settlementRequestValidate.getIdSettlementRequest(), settlementRequestValidate.getTargetParticipant().getIdParticipantPk() ,true);
				}
				
			} catch (Exception e) {
				
				error = true;
				
				e.printStackTrace();
				//Mensaje de error en pantalla
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.MESSAGES_UNSUCCESFUL ));
				
				//actualizamos la pantalla
				JSFUtilities.showSimpleEndTransactionDialog("searchSettlementDateRequest");
				
				break;
			}
		}
		
		//Si no hay error entonces mostramos mensaje de confirmacion
		if(error.equals(Boolean.FALSE)){

			String messageProperties = "";
			
			switch(newMultipleState){

				case 1474:	//aprobada
								messageProperties = PropertiesConstants.SELAR_OPERATION_APPROVE_CONFIRM_OK_MULTIPLE;
								break;
				case 1475:	//anulada
								messageProperties = PropertiesConstants.SELAR_OPERATION_ANNULATE_CONFIRM_OK_MULTIPLE;
								break;
				case 1482: 	//revisada
								messageProperties = PropertiesConstants.SELAR_OPERATION_REVIEW_CONFIRM_OK_MULTIPLE;
								break;
				case 1228:	//rechazada
								messageProperties = PropertiesConstants.SELAR_OPERATION_REJECT_CONFIRM_OK_MULTIPLE;
								break;
				case 1229:	//confirmada	
								messageProperties = PropertiesConstants.SELAR_OPERATION_CONFIRM_CONFIRM_OK_MULTIPLE;
								break;
				case 1543:	//autorizada
								messageProperties = PropertiesConstants.SELAR_OPERATION_AUTHORIZED_CONFIRM_OK_MULTIPLE;
								break;
			}

			//Mensaje de exito
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(messageProperties, new Object[]{StringUtils.join(idsConcat,",")}));
			
			//actualizamos la pantalla
			JSFUtilities.showSimpleEndTransactionDialog("searchSettlementDateRequest");
			searchSettlementDateRequest();
		}
	}
	
	/**
	 * METODO PARA LLENAR LOS DATOS AL MOMENTO DE VER EL DETALLE (SHOW DETAIL).
	 *
	 * @param selected the selected
	 */
	
	private void fillData(SettlementRequestTO selected){
		registerPrepaidExtendedTO = new RegisterPrepaidExtendedTO();
		registerPrepaidExtendedTO.setIdParticipant(searchPrepaidExtendedTO.getIdParticipantPk());

		boolean fetchAccounts = false;
		
		if(SettlementRequestType.SETTLEMENT_EXTENSION.getCode().equals(selected.getRequestType())){
			registerPrepaidExtendedTO.setBlExtended(true);
		}else if(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode().equals(selected.getRequestType())){
			registerPrepaidExtendedTO.setBlSettlementExchange(true);
		}else if(SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode().equals(selected.getRequestType())){
			registerPrepaidExtendedTO.setBlSpecialSettlementExchange(true);
		}else if(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode().equals(selected.getRequestType())){
			registerPrepaidExtendedTO.setBlPrepaid(true);
			
			fetchAccounts = true;
			if(selected.getSourceParticipant().getIdParticipantPk().equals(selected.getTargetParticipant().getIdParticipantPk())){
				registerPrepaidExtendedTO.setBlCrossOperation(true);
			}
			
//			if(participantUser && !registerPrepaidExtendedTO.isBlCrossOperation()){
//				fetchAccounts = false;
//			}
		}
		
		registerPrepaidExtendedTO.setLstSettlementDateOperation(prepaidExtendedFacade.getSettlementDateOperations(
											selected.getIdSettlementRequest(),fetchAccounts, userInfo.getUserAccountSession().getParticipantCode()));
		
		if(registerPrepaidExtendedTO!=null && registerPrepaidExtendedTO.getLstSettlementDateOperation()!=null
				&& registerPrepaidExtendedTO.getLstSettlementDateOperation().size()==1){
			selectedDateOperation = (SettlementDateOperation)registerPrepaidExtendedTO.getLstSettlementDateOperation().get(0);
		}
		
		for(SettlementDateOperation settlementDateOper : registerPrepaidExtendedTO.getLstSettlementDateOperation()){
			
			if(registerPrepaidExtendedTO.isBlPrepaid() && !registerPrepaidExtendedTO.isBlCrossOperation() && isViewOperationReview()){
				
				if(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode().equals(selected.getAnticipationType())){
					
					registerPrepaidExtendedTO.setMaxQuantity(settlementDateOper.getStockQuantity());
					registerPrepaidExtendedTO.setOperationRole(selected.getCounterPartRole());
					
					if(ComponentConstant.SALE_ROLE.equals(selected.getParticipantRole())){	
						registerPrepaidExtendedTO.setSaleQuantity(settlementDateOper.getStockQuantity());
						registerPrepaidExtendedTO.setSaleAmount(settlementDateOper.getSettlementAmount());
					}else if(ComponentConstant.PURCHARSE_ROLE.equals(selected.getParticipantRole())){
						registerPrepaidExtendedTO.setPurchaseQuantity(settlementDateOper.getStockQuantity());
						registerPrepaidExtendedTO.setPurchaseAmount(settlementDateOper.getSettlementAmount());
					}
					
//					getHolderAccountOperations(settlementDateOper, selected.getCounterPartRole(), selected.getTargetParticipant().getIdParticipantPk(),
//												settlementDateRequest.getAnticipationType());
				}else{
					registerPrepaidExtendedTO.setMaxQuantity(settlementDateOper.getStockQuantity());
					//registerPrepaidExtendedTO.setOperationRole(selected.getParticipantRole());
					registerPrepaidExtendedTO.setOperationRole(selected.getCounterPartRole());
					registerPrepaidExtendedTO.setSaleQuantity(settlementDateOper.getStockQuantity());
					registerPrepaidExtendedTO.setSaleAmount(settlementDateOper.getSettlementAmount());
					registerPrepaidExtendedTO.setPurchaseQuantity(settlementDateOper.getStockQuantity());
					registerPrepaidExtendedTO.setPurchaseAmount(settlementDateOper.getSettlementAmount());
				}
				
				getHolderAccountOperations(settlementDateOper, selected.getCounterPartRole(), selected.getTargetParticipant().getIdParticipantPk(),
						selected.getAnticipationType());
				
				updateOperationAmounts();
			}
			
			registerPrepaidExtendedTO.setLstSettlementDateAccounts(settlementDateOper.getSettlementDateAccounts());
			
			SettlementOperation settlementOperation = settlementDateOper.getSettlementOperation();
			settlementOperation.setStateDescription(mpParameters.get(settlementOperation.getOperationState()));
			settlementOperation.setCurrencyDescription(mpParameters.get(settlementOperation.getSettlementCurrency()));
			settlementOperation.getMechanismOperation().setCurrencyDescription(mpParameters.get(settlementOperation.getSettlementCurrency()));
		}
	
	}

	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);		
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnApproveView(true);
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnReview(true);
		privilegeComponent.setBtnAuthorize(true);		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParameterFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * Question.
	 *
	 * @param message the message
	 */
	public void question(String message){
		String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Question.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void question(String header,String message){
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * End dialog.
	 *
	 * @param message the message
	 */
	public void endDialog(String message){
		String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * End dialog.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void endDialog(String header,String message){
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Back dialog.
	 *
	 * @param message the message
	 */
	public void backDialog(String message){
		String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfwBackDialog').show();");
	}
	
	/**
	 * Back dialog.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void backDialog(String header,String message){
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfwBackDialog').show();");
	}
	
	/**
	 * Clean dialog.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void cleanDialog(String header,String message){
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfwCleanDialog').show();");
	}

	/**
	 * Method for logout the user session on idle time of the application.
	 */
	public void sessionLogout(){
		try{
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession(); 
			JSFUtilities.showMessageOnDialog(null, null,com.pradera.settlements.utils.PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the search prepaid extended to.
	 *
	 * @return the search prepaid extended to
	 */
	public SearchPrepaidExtendedTO getSearchPrepaidExtendedTO() {
		return searchPrepaidExtendedTO;
	}
	
	/**
	 * Sets the search prepaid extended to.
	 *
	 * @param searchPrepaidExtendedTO the new search prepaid extended to
	 */
	public void setSearchPrepaidExtendedTO(
			SearchPrepaidExtendedTO searchPrepaidExtendedTO) {
		this.searchPrepaidExtendedTO = searchPrepaidExtendedTO;
	}

	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Checks if is participant user.
	 *
	 * @return true, if is participant user
	 */
	public boolean isParticipantUser() {
		return participantUser;
	}
	
	/**
	 * Sets the participant user.
	 *
	 * @param participantUser the new participant user
	 */
	public void setParticipantUser(boolean participantUser) {
		this.participantUser = participantUser;
	}
	
	/**
	 * Checks if is mechanism user.
	 *
	 * @return true, if is mechanism user
	 */
	public boolean isMechanismUser() {
		return mechanismUser;
	}
	
	/**
	 * Sets the mechanism user.
	 *
	 * @param mechanismUser the new mechanism user
	 */
	public void setMechanismUser(boolean mechanismUser) {
		this.mechanismUser = mechanismUser;
	}
	
	/**
	 * Gets the register prepaid extended to.
	 *
	 * @return the register prepaid extended to
	 */
	public RegisterPrepaidExtendedTO getRegisterPrepaidExtendedTO() {
		return registerPrepaidExtendedTO;
	}
	
	/**
	 * Sets the register prepaid extended to.
	 *
	 * @param registerPrepaidExtendedTO the new register prepaid extended to
	 */
	public void setRegisterPrepaidExtendedTO(
			RegisterPrepaidExtendedTO registerPrepaidExtendedTO) {
		this.registerPrepaidExtendedTO = registerPrepaidExtendedTO;
	}
	
	/**
	 * Checks if is depositary user.
	 *
	 * @return true, if is depositary user
	 */
	public boolean isDepositaryUser() {
		return depositaryUser;
	}
	
	/**
	 * Sets the depositary user.
	 *
	 * @param depositaryUser the new depositary user
	 */
	public void setDepositaryUser(boolean depositaryUser) {
		this.depositaryUser = depositaryUser;
	}
	
	/**
	 * Gets the lst settlement date request.
	 *
	 * @return the lst settlement date request
	 */
	public GenericDataModel<SettlementRequestTO> getLstSettlementDateRequest() {
		return lstSettlementDateRequest;
	}
	
	/**
	 * Sets the lst settlement date request.
	 *
	 * @param lstSettlementDateRequest the new lst settlement date request
	 */
	public void setLstSettlementDateRequest(
			GenericDataModel<SettlementRequestTO> lstSettlementDateRequest) {
		this.lstSettlementDateRequest = lstSettlementDateRequest;
	}
	
	/**
	 * Gets the lst settlement date accounts.
	 *
	 * @return the lst settlement date accounts
	 */
	public GenericDataModel<SettlementDateAccount> getLstSettlementDateAccounts() {
		return lstSettlementDateAccounts;
	}
	
	/**
	 * Sets the lst settlement date accounts.
	 *
	 * @param lstSettlementDateAccounts the new lst settlement date accounts
	 */
	public void setLstSettlementDateAccounts(
			GenericDataModel<SettlementDateAccount> lstSettlementDateAccounts) {
		this.lstSettlementDateAccounts = lstSettlementDateAccounts;
	}
	
	/**
	 * Gets the selected request.
	 *
	 * @return the selected request
	 */
	public SettlementRequestTO getSelectedRequest() {
		return selectedRequest;
	}
	
	
	
	/**
	 * Sets the selected request.
	 *
	 * @param selectedRequest the new selected request
	 */
	public void setSelectedRequest(SettlementRequestTO selectedRequest) {
		this.selectedRequest = selectedRequest;
	}
	
	/**
	 * Gets the lst settlement date negotiations.
	 *
	 * @return the lst settlement date negotiations
	 */
	public GenericDataModel<SettlementDateOperation> getLstSettlementDateNegotiations() {
		return lstSettlementDateNegotiations;
	}
	
	/**
	 * Sets the lst settlement date negotiations.
	 *
	 * @param lstSettlementDateNegotiations the new lst settlement date negotiations
	 */
	public void setLstSettlementDateNegotiations(
			GenericDataModel<SettlementDateOperation> lstSettlementDateNegotiations) {
		this.lstSettlementDateNegotiations = lstSettlementDateNegotiations;
	}
	
	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}
	
	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}
	
	/**
	 * Gets the settlement date request.
	 *
	 * @return the settlement date request
	 */
	public SettlementRequest getSettlementDateRequest() {
		return settlementDateRequest;
	}
	
	/**
	 * Sets the settlement date request.
	 *
	 * @param settlementDateRequest the new settlement date request
	 */
	public void setSettlementDateRequest(SettlementRequest settlementDateRequest) {
		this.settlementDateRequest = settlementDateRequest;
	}
	
	public String getGlossToSelveRequest() {
		return glossToSelveRequest;
	}

	public void setGlossToSelveRequest(String glossToSelveRequest) {
		this.glossToSelveRequest = glossToSelveRequest;
	}

	/**
	 * Gets the selected date operation.
	 *
	 * @return the selected date operation
	 */
	public SettlementDateOperation getSelectedDateOperation() {
		return selectedDateOperation;
	}
	
	/**
	 * Sets the selected date operation.
	 *
	 * @param selectedDateOperation the new selected date operation
	 */
	public void setSelectedDateOperation(SettlementDateOperation selectedDateOperation) {
		this.selectedDateOperation = selectedDateOperation;
	}

	public List<SettlementRequestTO> getSelectedRequestList() {
		return selectedRequestList;
	}

	public void setSelectedRequestList(List<SettlementRequestTO> selectedRequestList) {
		this.selectedRequestList = selectedRequestList;
	}

	/**
	 * @return the settlementDays
	 */
	public Integer getQuantitySettlementDays() {
		return quantitySettlementDays;
	}

	/**
	 * @param settlementDays the settlementDays to set
	 */
	public void setQuantitySettlementDays(Integer quantitySettlementDays) {
		this.quantitySettlementDays = quantitySettlementDays;
	}
	
	

}
