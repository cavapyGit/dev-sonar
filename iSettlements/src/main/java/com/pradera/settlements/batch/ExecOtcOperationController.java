package com.pradera.settlements.batch;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;

@Singleton
@Performance
@Path("/ExecOtcOperationResource")
@Interceptors(ContextHolderInterceptor.class)
public class ExecOtcOperationController {

	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The batch process service facade. */
	@EJB private BatchProcessServiceFacade batchProcessServiceFacade;
	
	@GET
	@Path("/createOtcOperationController/{userParam}/{ipParam}/{listIdAccountAnnotationPk}")
	public void testBatch(@PathParam("userParam")String userNameParam, @PathParam("ipParam")String ipParam, @PathParam("listIdAccountAnnotationPk")String listIdAccountAnnotationPk){
    	
    	LoggerUser loggerUser = CommonsUtilities.getLoggerUser(userNameParam, ipParam);
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);

		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.CREATE_OTC_OPERATION_WITH_CONFIRM_ANNOTATION.getCode());
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.LIST_ID_ACCOUNT_ANNOTATION_PK, listIdAccountAnnotationPk);
			details.put(GeneralConstants.USER_NAME, userNameParam);
			batchProcessServiceFacade.registerBatch(userNameParam,businessProcess,details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
	}
	
}