package com.pradera.settlements.batch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.negotiations.otcoperations.facade.OtcOperationServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExecIssuanceSecurityBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="ExecOtcOperationBatch")
@RequestScoped
public class ExecOtcOperationBatch implements Serializable,JobExecution {

	private static final long serialVersionUID = 1L;
	
	@EJB 
	private OtcOperationServiceFacade otcOperationFacade;
	
	@EJB 
	private ParameterServiceBean parameterServiceBean;
	
	@Override
	public void startJob(ProcessLogger processLogger) {

		String tmpParameterPk= "";
		
		List<String> lstAccountAnnotationPk = null;
		String userName=null;
		
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.ID_ACCOUNT_ANNOTATION_PK)){
				tmpParameterPk = detail.getParameterValue();
				lstAccountAnnotationPk = new ArrayList<String>();
				lstAccountAnnotationPk.add(tmpParameterPk);
				
			}else if(detail.getParameterName().equals(GeneralConstants.LIST_ID_ACCOUNT_ANNOTATION_PK)){
				tmpParameterPk = detail.getParameterValue();
				String[] valuesHeader = detail.getParameterValue().split(",");
				lstAccountAnnotationPk = Arrays.asList(valuesHeader);

			}else if(detail.getParameterName().equals(GeneralConstants.USER_NAME)){
				userName = detail.getParameterValue();
			}
		}
		
		try {
			
			LoggerUser loggerUser = new LoggerUser();
			loggerUser.setUserName(userName);
			loggerUser.setIpAddress("127.0.0.1");
			loggerUser.setAuditTime(CommonsUtilities.currentDate());
			loggerUser.setIdPrivilegeOfSystem(1);
			
			List<AccountAnnotationOperation> lstAccountAnnotationOperation = lstAccountAnnotationOperations(lstAccountAnnotationPk);
			/*for(AccountAnnotationOperation current: lstAccountAnnotationOperation) {
				MechanismOperation newMechanismOperation = registerNewOTCWithAccountAnnotation(current, loggerUser);
				AccountAnnotationConfirm(newMechanismOperation, loggerUser);
			}*/

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	public MechanismOperation registerNewOTCWithAccountAnnotation(AccountAnnotationOperation accountAnnotation, LoggerUser loggerUser) throws ServiceException {	
		MechanismOperation mechanismOperation = mechanismOperationWithAccountAnnotation(accountAnnotation, loggerUser);
		MechanismOperation newOperation = otcOperationFacade.registryNewMechanismOperation(mechanismOperation,null, null, loggerUser);
		return newOperation;
	}
	
	public MechanismOperation mechanismOperationWithAccountAnnotation(AccountAnnotationOperation accountAnnotation, LoggerUser loggerUser) throws ServiceException {
		
		Long mechanismPk = NegotiationMechanismType.OTC.getCode();
		Long modalityPk = NegotiationModalityType.CASH_FIXED_INCOME.getCode();
		MechanismModality mechanismModality = otcOperationFacade.findMechanismModality(mechanismPk, modalityPk);
		
		
		DailyExchangeRates dailyExchangeRates = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());
		
		MechanismOperation mechanismOperation = new MechanismOperation(); //otcOperationFacade.getNewMechanismOperation();
		mechanismOperation.setMechanisnModality(new MechanismModality());
		mechanismOperation.getMechanisnModality().getId().setIdNegotiationMechanismPk(NegotiationMechanismType.OTC.getCode());
		mechanismOperation.setOperationDate(CommonsUtilities.currentDate());
		mechanismOperation.setReferenceDate(CommonsUtilities.currentDateTime());
		mechanismOperation.setBuyerParticipant(new Participant());
		mechanismOperation.setSellerParticipant(new Participant());
		mechanismOperation.setSecurities(new Security());
//		mechanismOperationOtcSession.setSwapOperations(new ArrayList<SwapOperation>());
		mechanismOperation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		mechanismOperation.setOperationState(OtcOperationStateType.CONFIRMED.getCode());
		mechanismOperation.setIndTermSettlement(BooleanType.NO.getCode());
		
		
		mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.SALE.getCode());//SIEMPRE SE REGISTRA COMO VENTA, Y SE COLOCA COMO COMPRADOR A QUIEN RECIBE EL VALOR
		mechanismOperation.setBuyerParticipant(accountAnnotation.getHolderAccount().getParticipant());
		mechanismOperation.setSellerParticipant(accountAnnotation.getHolderAccountSeller().getParticipant());
		mechanismOperation.setSecurities(accountAnnotation.getSecurity());
		mechanismOperation.setCashPrice(accountAnnotation.getCashPrice());
		mechanismOperation.setCurrency(accountAnnotation.getSecurity().getCurrency());
		mechanismOperation.setAmountRate(accountAnnotation.getAmountRate());
		mechanismOperation.setStockQuantity(accountAnnotation.getTotalBalance());
		mechanismOperation.setNominalValue(mechanismOperation.getSecurities().getCurrentNominalValue());
		mechanismOperation.setCashSettlementDays(0L);
		mechanismOperation.setTermSettlementDays(0L);
		mechanismOperation.setCashSettlementDate(accountAnnotation.getRegistryDate());
		mechanismOperation.setExchangeRate(dailyExchangeRates.getReferencePrice());
		mechanismOperation.setSwapOperations(null);
		mechanismOperation.setMechanisnModality(mechanismModality);
		mechanismOperation.setOperationNumber(accountAnnotation.getHolderAccount().getAccountNumber().longValue());
		//mechanismOperation.setReferenceNumber(""+accountAnnotation.getHolderAccount().getAccountNumber());
		mechanismOperation.setReferenceDate(accountAnnotation.getRegistryDate());
		mechanismOperation.setOperationState(615);
		mechanismOperation.setCashSettlementCurrency(accountAnnotation.getSecurity().getCurrency());
		mechanismOperation.setTermSettlementCurrency(accountAnnotation.getSecurity().getCurrency());
		mechanismOperation.setSettlementSchema(1);
		mechanismOperation.setSettlementType(1);
		mechanismOperation.setIndPrincipalGuarantee(0);
		mechanismOperation.setIndMarginGuarantee(0);
		mechanismOperation.setIndReportingBalance(0);
		mechanismOperation.setIndCashSettlementExchange(0);
		mechanismOperation.setIndTermSettlementExchange(0);
		mechanismOperation.setIndTermSettlement(0);
		mechanismOperation.setIndCashStockBlock(0);
		mechanismOperation.setIndTermStockBlock(0);
		mechanismOperation.setIndPrimaryPlacement(0);
		mechanismOperation.setIndDpf(1);
		
		
		mechanismOperation.setCashPrice(mechanismOperation.getCashPricePercentage().multiply(mechanismOperation.getNominalValue()));
		
		BigDecimal PporQ = mechanismOperation.getSecurities().getCurrentNominalValue().multiply(mechanismOperation.getStockQuantity());
		mechanismOperation.setCashAmount(PporQ);
		mechanismOperation.setRealCashAmount(PporQ);
		
		BigDecimal cleanCashAmount = mechanismOperation.getRealCashAmount();
		BigDecimal stockQuantity = mechanismOperation.getStockQuantity();
		BigDecimal realCashPrice = cleanCashAmount.divide(stockQuantity,8,RoundingMode.CEILING);
		mechanismOperation.setRealCashPrice(realCashPrice);
		
		
		List<HolderAccountOperation> lstHolderAccountOperation = new ArrayList<HolderAccountOperation>();
		
		HolderAccountOperation holderAccountOperation = new HolderAccountOperation();
		holderAccountOperation.setHolderAccount(accountAnnotation.getHolderAccount());
		holderAccountOperation.setInchargeFundsParticipant(accountAnnotation.getHolderAccount().getParticipant());
		holderAccountOperation.setInchargeStockParticipant(accountAnnotation.getHolderAccount().getParticipant());
		holderAccountOperation.setIdIsinCode(accountAnnotation.getSecurity().getIdSecurityCodePk());
		holderAccountOperation.setStockQuantity(mechanismOperation.getStockQuantity());
		holderAccountOperation.setCashAmount(mechanismOperation.getCashAmount());
		holderAccountOperation.setSettlementAmount(mechanismOperation.getCashSettlementAmount());
		holderAccountOperation.setRole(1);
		holderAccountOperation.setIndAutomaticAsignment(0);
		holderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		holderAccountOperation.setConfirmDate(CommonsUtilities.currentDateTime());
		holderAccountOperation.setRegisterUser(loggerUser.getUserName());
		holderAccountOperation.setIndIncharge(BooleanType.NO.getCode());
		holderAccountOperation.setMechanismOperation(mechanismOperation);
		holderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
		holderAccountOperation.setOperationPart(1);
		holderAccountOperation.setIndUnfulfilled(0);
		holderAccountOperation.setIndAccreditation(0);
		lstHolderAccountOperation.add(holderAccountOperation);
		
		holderAccountOperation = new HolderAccountOperation();
		holderAccountOperation.setHolderAccount(accountAnnotation.getHolderAccount());
		holderAccountOperation.setInchargeFundsParticipant(accountAnnotation.getHolderAccountSeller().getParticipant());
		holderAccountOperation.setInchargeStockParticipant(accountAnnotation.getHolderAccountSeller().getParticipant());
		holderAccountOperation.setIdIsinCode(accountAnnotation.getSecurity().getIdSecurityCodePk());
		holderAccountOperation.setStockQuantity(mechanismOperation.getStockQuantity());
		holderAccountOperation.setCashAmount(mechanismOperation.getCashAmount());
		holderAccountOperation.setSettlementAmount(mechanismOperation.getCashSettlementAmount());		
		holderAccountOperation.setRole(2);
		holderAccountOperation.setIndAutomaticAsignment(0);
		holderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		holderAccountOperation.setConfirmDate(CommonsUtilities.currentDateTime());
		holderAccountOperation.setRegisterUser(loggerUser.getUserName());
		holderAccountOperation.setIndIncharge(BooleanType.NO.getCode());
		holderAccountOperation.setMechanismOperation(mechanismOperation);
		holderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
		holderAccountOperation.setOperationPart(1);
		holderAccountOperation.setIndUnfulfilled(0);
		holderAccountOperation.setIndAccreditation(0);
		lstHolderAccountOperation.add(holderAccountOperation);
		
		return mechanismOperation;
	}
	
	private List<AccountAnnotationOperation> lstAccountAnnotationOperations(List<String> lstStrAccountAnnotationPk) throws ServiceException{
		
		List<Long> lstIdAccountAnnotationPk = new ArrayList<Long>();
		for(int i=0; i<lstStrAccountAnnotationPk.size();i++) {
			lstIdAccountAnnotationPk.add(Long.valueOf(lstStrAccountAnnotationPk.get(i)));
		}
		return otcOperationFacade.findAccountAnnotationOTCMotive(lstIdAccountAnnotationPk);
		
	}
	
	public void AccountAnnotationConfirm(MechanismOperation mechanismOperation, LoggerUser loggerUser) throws ServiceException {
		
		mechanismOperation.getTradeOperation().setConfirmDate(CommonsUtilities.currentDateTime());
		mechanismOperation.getTradeOperation().setConfirmUser(loggerUser.getUserName());
		mechanismOperation = otcOperationFacade.confirmOtcOperationServiceFacade(mechanismOperation);
		
	}


	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
