package com.pradera.settlements.chainedoperation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.settlement.SettlementAccountMarketfact;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChainedOperationDataTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class ChainedOperationDataTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The market date. */
	private Date marketDate;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The is selected. */
	private boolean isSelected;
	
	/** The lst settlement account marketfact. */
	//List<SettlementAccountOperation> lstSettlementAccountOperation;
	List<SettlementAccountMarketfact> lstSettlementAccountMarketfact;
	
	
	/**
	 * Instantiates a new chained operation data to.
	 */
	public ChainedOperationDataTO() {
		super();
	}


	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}


	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}


	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}


	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}


	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return isSelected;
	}


	/**
	 * Sets the selected.
	 *
	 * @param isSelected the new selected
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}


	/**
	 * Gets the lst settlement account marketfact.
	 *
	 * @return the lst settlement account marketfact
	 */
	public List<SettlementAccountMarketfact> getLstSettlementAccountMarketfact() {
		return lstSettlementAccountMarketfact;
	}


	/**
	 * Sets the lst settlement account marketfact.
	 *
	 * @param lstSettlementAccountMarketfact the new lst settlement account marketfact
	 */
	public void setLstSettlementAccountMarketfact(
			List<SettlementAccountMarketfact> lstSettlementAccountMarketfact) {
		this.lstSettlementAccountMarketfact = lstSettlementAccountMarketfact;
	}

}
