package com.pradera.settlements.chainedoperation.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.settlement.ChainedHolderOperation;
import com.pradera.model.settlement.HolderChainDetail;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.settlements.chainedoperation.to.ChainedOperationDataTO;
import com.pradera.settlements.chainedoperation.to.RegisterChainedOperationTO;
import com.pradera.settlements.chainedoperation.to.SearchChainedOperationTO;
import com.pradera.settlements.core.service.SettlementProcessService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChainedOperationsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class ChainedOperationsServiceBean extends CrudDaoServiceBean {

	/** The accounts component service. */
	@Inject
	private Instance<AccountsComponentService> accountsComponentService; 
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The partipant query service. */
	@EJB
	private ParticipantServiceBean participantService;
	
	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getListParticipants() {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pa FROM Participant pa");
		sbQuery.append("	WHERE pa.state = :state");
		sbQuery.append("	AND pa.accountType = :accountType ");
		sbQuery.append("	ORDER BY pa.description ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("accountType", ParticipantType.DIRECT.getCode());
		return (List<Participant>)query.getResultList();			
	}

	/**
	 * Gets the list participants by nego mechanism.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @param idParticipant the id participant
	 * @return the list participants by nego mechanism
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getListParticipantsByNegoMechanism(Long idNegotiationMechanismPk, Long idParticipant) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct pa ");
		sbQuery.append("	FROM ParticipantMechanism pm, Participant pa ");
		sbQuery.append("	WHERE pm.stateParticipantMechanism = :stateParticipantMechanism ");
		sbQuery.append("	and pm.participant.idParticipantPk = pa.idParticipantPk ");
		sbQuery.append("	AND pm.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk ");
		sbQuery.append("	AND pa.state = :state ");
		sbQuery.append("	AND pa.accountType = :accountType ");
		if (Validations.validateIsNotNull(idParticipant)) {
			sbQuery.append("	AND pa.idParticipantPk = :idParticipant ");
		}
		sbQuery.append("	ORDER BY pa.mnemonic ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("accountType", ParticipantType.DIRECT.getCode());
		query.setParameter("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		query.setParameter("idNegotiationMechanismPk", idNegotiationMechanismPk);
		if (Validations.validateIsNotNull(idParticipant)) {
			query.setParameter("idParticipant", idParticipant);
		}
		return (List<Participant>)query.getResultList();
	}
	
	/**
	 * Gets the list modality group.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the list modality group
	 */
	@SuppressWarnings("unchecked")
	public List<ModalityGroup> getListModalityGroup(Long idNegotiationMechanismPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct mg");
		sbQuery.append("	FROM ModalityGroup mg");
		sbQuery.append("	WHERE mg.groupState = :state");
		sbQuery.append("	AND mg.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	and mg.settlementSchema = :settlementSchema ");
		sbQuery.append("	ORDER BY mg.groupName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ComponentConstant.ONE);
		query.setParameter("idNegotiationMechanismPk", idNegotiationMechanismPk);
		query.setParameter("settlementSchema", SettlementSchemaType.NET.getCode());
		return (List<ModalityGroup>)query.getResultList();
	}

	/**
	 * Search chained holder request.
	 *
	 * @param searchChainedOperationTO the search chained operation to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<ChainedHolderOperation> searchChainedHolderRequest(SearchChainedOperationTO searchChainedOperationTO) {
		List<ChainedHolderOperation> lstChainedHolderOperation= null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT DISTINCT cho FROM ChainedHolderOperation cho, HolderAccountDetail had ");
		sbQuery.append("	INNER JOIN FETCH cho.holderAccount ha");
//		sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
//		sbQuery.append("	INNER JOIN FETCH had.holder");
		sbQuery.append("	INNER JOIN FETCH cho.security");
		sbQuery.append("	WHERE cho.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	and ha.idHolderAccountPk = had.holderAccount.idHolderAccountPk ");
		sbQuery.append("	AND TRUNC(cho.registerDate) between :initialDate AND :endDate");
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getHolderAccount().getIdHolderAccountPk()))
			sbQuery.append("	AND cho.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getSecurity().getIdSecurityCodePk()))
			sbQuery.append("	AND cho.security.idSecurityCodePk = :idSecurityCodePk");
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getState()))
			sbQuery.append("	AND cho.chaintState = :state");
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getHolder().getIdHolderPk()))
			sbQuery.append("	AND had.holder.idHolderPk = :idHolderPk");
		sbQuery.append(" 	ORDER BY cho.idChainedHolderOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPk", searchChainedOperationTO.getIdParticipantPk());
		query.setParameter("initialDate", searchChainedOperationTO.getInitialDate());
		query.setParameter("endDate", searchChainedOperationTO.getEndDate());
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getHolderAccount().getIdHolderAccountPk()))
			query.setParameter("idHolderAccountPk", searchChainedOperationTO.getHolderAccount().getIdHolderAccountPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", searchChainedOperationTO.getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getState()))
			query.setParameter("state", searchChainedOperationTO.getState());
		if(Validations.validateIsNotNullAndNotEmpty(searchChainedOperationTO.getHolder().getIdHolderPk()))
			query.setParameter("idHolderPk", searchChainedOperationTO.getHolder().getIdHolderPk());

		lstChainedHolderOperation= (List<ChainedHolderOperation>)query.getResultList();
		if (Validations.validateListIsNotNullAndNotEmpty(lstChainedHolderOperation)) {
			for (ChainedHolderOperation objChainedHolderOperation: lstChainedHolderOperation) {
				objChainedHolderOperation.setDesChainState(ChainedOperationStateType.get(objChainedHolderOperation.getChaintState()).getDescription());
				objChainedHolderOperation.setDesCurrency(CurrencyType.get(objChainedHolderOperation.getCurrency()).getCodeIso());
				objChainedHolderOperation.setDesIndExtended(BooleanType.get(objChainedHolderOperation.getIndExtended()).getValue());
			}
		}
		return lstChainedHolderOperation;
	}

	/**
	 * Gets the lst settlement account operation to chain.
	 *
	 * @param objRegisterChainedOperationTO the obj register chained operation to
	 * @return the lst settlement account operation to chain
	 */
	public List<ChainedOperationDataTO> getLstSettlementAccountOperationToChain(RegisterChainedOperationTO objRegisterChainedOperationTO)
	{
		List<Object[]> lstSettlementAccountOperationToChain= getLstSettlementAccountOperation(objRegisterChainedOperationTO.getSettlementDate(), 
																			objRegisterChainedOperationTO.getParticipant().getIdParticipantPk(), 
																			objRegisterChainedOperationTO.getSecurity().getIdSecurityCodePk(), 
																			objRegisterChainedOperationTO.getHolderAccount().getIdHolderAccountPk(),
																			objRegisterChainedOperationTO.getModalityGroup().getIdModalityGroupPk(),
																			objRegisterChainedOperationTO.getCurrency().getParameterTablePk(),
																			objRegisterChainedOperationTO.getIndExtended().getParameterTablePk(),
																			objRegisterChainedOperationTO.getInstrumentType());
		Map<String, ChainedOperationDataTO> mpChainedOperationData= new HashMap<String, ChainedOperationDataTO>();
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountOperationToChain)) {
			for (Object[] objSettlementAccountToChain: lstSettlementAccountOperationToChain) {
				Date marketDate= (Date) objSettlementAccountToChain[3];
				BigDecimal marketRate= new BigDecimal(objSettlementAccountToChain[4].toString());
				String marketFact= marketRate + "*" + marketDate;
				ChainedOperationDataTO objChainedOperationDataTO= mpChainedOperationData.get(marketFact);
				if (Validations.validateIsNull(objChainedOperationDataTO)) {
					objChainedOperationDataTO= new ChainedOperationDataTO();
					objChainedOperationDataTO.setMarketDate(marketDate);
					objChainedOperationDataTO.setMarketRate(marketRate);
					//objChainedOperationDataTO.setLstSettlementAccountOperation(new ArrayList<SettlementAccountOperation>());
					objChainedOperationDataTO.setLstSettlementAccountMarketfact(new ArrayList<SettlementAccountMarketfact>());
					mpChainedOperationData.put(marketFact, objChainedOperationDataTO);
				}
				Long idSettlementAccountMarketfact= new Long(objSettlementAccountToChain[9].toString());
				Long idSettlementAccountOperation= new Long(objSettlementAccountToChain[8].toString());
				BigDecimal stockQuantity= new BigDecimal(objSettlementAccountToChain[5].toString());
				Integer role= new Integer(objSettlementAccountToChain[7].toString());
				Long idModality= new Long(objSettlementAccountToChain[12].toString());
				String modalityName= objSettlementAccountToChain[13].toString();
				Long ballotNumber= new Long(objSettlementAccountToChain[14].toString());
				Long sequential= new Long(objSettlementAccountToChain[15].toString());
				Long operationNumber= new Long(objSettlementAccountToChain[16].toString());
				Date operationDate= (Date) objSettlementAccountToChain[17];
				Integer operationState= new Integer(objSettlementAccountToChain[18].toString());
				Integer operationPart= new Integer(objSettlementAccountToChain[19].toString());
				Long idSettlementOperation= new Long(objSettlementAccountToChain[20].toString());
				Long idMechanismOperation= new Long(objSettlementAccountToChain[21].toString());
				String mnemonicBuyer= objSettlementAccountToChain[22].toString();
				String mnemonicSeller= objSettlementAccountToChain[23].toString();
				
				MechanismModality objMechanismModality= new MechanismModality();
				NegotiationMechanism objNegotiationMechanism= new NegotiationMechanism();
				objNegotiationMechanism.setIdNegotiationMechanismPk(objRegisterChainedOperationTO.getIdNegotiationMechanismPk());
				NegotiationModality objNegotiationModality= new NegotiationModality();
				objNegotiationModality.setIdNegotiationModalityPk(idModality);
				objNegotiationModality.setModalityName(modalityName);
				objMechanismModality.setNegotiationMechanism(objNegotiationMechanism);
				objMechanismModality.setNegotiationModality(objNegotiationModality);
				
				Participant buyerParticipant= new Participant();
				Participant sellerParticipant= new Participant();
				if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
					buyerParticipant.setMnemonic(mnemonicSeller);
					sellerParticipant.setMnemonic(mnemonicBuyer);
				}else{
					buyerParticipant.setMnemonic(mnemonicBuyer);
					sellerParticipant.setMnemonic(mnemonicSeller);
				}				
				
				MechanismOperation objMechanismOperation= new MechanismOperation();
				objMechanismOperation.setIdMechanismOperationPk(idMechanismOperation);
				objMechanismOperation.setMechanisnModality(objMechanismModality);
				objMechanismOperation.setOperationDate(operationDate);
				objMechanismOperation.setOperationNumber(operationNumber);
				objMechanismOperation.setBallotNumber(ballotNumber);
				objMechanismOperation.setSequential(sequential);
				objMechanismOperation.setBuyerParticipant(buyerParticipant);
				objMechanismOperation.setSellerParticipant(sellerParticipant);
				objMechanismOperation.setOperationState(operationState);
				objMechanismOperation.setStateDescription(MechanismOperationStateType.get(operationState).getValue());
				SettlementOperation objSettlementOperation= new SettlementOperation();
				objSettlementOperation.setIdSettlementOperationPk(idSettlementOperation);
				objSettlementOperation.setMechanismOperation(objMechanismOperation);
				objSettlementOperation.setOperationState(operationState);
				objSettlementOperation.setStateDescription(MechanismOperationStateType.get(operationState).getValue());
				objSettlementOperation.setOperationPart(operationPart);
				objSettlementOperation.setOperationPartDescription(OperationPartType.get(operationPart).getDescription());
				
				SettlementAccountOperation objSettlementAccountOperation= new SettlementAccountOperation();
				objSettlementAccountOperation.setIdSettlementAccountPk(idSettlementAccountOperation);
				objSettlementAccountOperation.setSettlementOperation(objSettlementOperation);
				objSettlementAccountOperation.setStockQuantity(stockQuantity);
				objSettlementAccountOperation.setChainedQuantity(stockQuantity);
				objSettlementAccountOperation.setRole(role);
				objSettlementAccountOperation.setDesRole(ParticipantRoleType.get(role).getValue());
				
				SettlementAccountMarketfact objSettlementAccountMarketfact= new SettlementAccountMarketfact();
				objSettlementAccountMarketfact.setIdSettAccountMarketfactPk(idSettlementAccountMarketfact);
				objSettlementAccountMarketfact.setMarketDate(marketDate);
				objSettlementAccountMarketfact.setMarketRate(marketRate);
				objSettlementAccountMarketfact.setMarketQuantity(stockQuantity);
				objSettlementAccountMarketfact.setChainedQuantity(stockQuantity);
				objSettlementAccountMarketfact.setSettlementAccountOperation(objSettlementAccountOperation);
				
				//objChainedOperationDataTO.getLstSettlementAccountOperation().add(objSettlementAccountOperation);
				objChainedOperationDataTO.getLstSettlementAccountMarketfact().add(objSettlementAccountMarketfact);
			}
		}
		return new ArrayList<ChainedOperationDataTO>(mpChainedOperationData.values());
	}
	
	/**
	 * Gets the lst settlement account operation.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id security code
	 * @param idHolderAccount the id holder account
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param indExtended the ind extended
	 * @param instrumentType the instrument type
	 * @return the lst settlement account operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getLstSettlementAccountOperation(Date settlementDate, Long idParticipant, String idSecurityCode, Long idHolderAccount, 
												Long idModalityGroup, Integer currency, Integer indExtended, Integer instrumentType)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT MO.securities.idSecurityCodePk, "); 								//0
		stringBuffer.append(" SAO.holderAccountOperation.inchargeStockParticipant.idParticipantPk, ");	//1
		stringBuffer.append(" SAO.holderAccountOperation.holderAccount.idHolderAccountPk, ");			//2
		stringBuffer.append(" SAM.marketDate, ");														//3
		stringBuffer.append(" SAM.marketRate, ");														//4
		stringBuffer.append(" SAM.marketQuantity, ");													//5
		stringBuffer.append(" SAO.stockQuantity, ");													//6
		stringBuffer.append(" SAO.role, ");																//7
		stringBuffer.append(" SAO.idSettlementAccountPk, ");											//8
		stringBuffer.append(" SAM.idSettAccountMarketfactPk, ");										//9
		stringBuffer.append(" MGD.modalityGroup.idModalityGroupPk, ");									//10
		stringBuffer.append(" SAO.settlementOperation.settlementCurrency, ");							//11
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.idNegotiationModalityPk, ");		//12
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.modalityName, ");				//13
		stringBuffer.append(" MO.ballotNumber, ");														//14
		stringBuffer.append(" MO.sequential, ");														//15
		stringBuffer.append(" MO.operationNumber, ");													//16
		stringBuffer.append(" MO.operationDate, ");														//17
		stringBuffer.append(" SAO.settlementOperation.operationState, ");								//18
		stringBuffer.append(" SAO.settlementOperation.operationPart, ");								//19
		stringBuffer.append(" SAO.settlementOperation.idSettlementOperationPk, ");						//20
		stringBuffer.append(" MO.idMechanismOperationPk, ");											//21
		stringBuffer.append(" MO.buyerParticipant.mnemonic, ");											//22
		stringBuffer.append(" MO.sellerParticipant.mnemonic, ");										//23
		stringBuffer.append(" MO.securities.securityClass ");											//24
		stringBuffer.append(" FROM SettlementAccountOperation SAO, ModalityGroupDetail MGD, MechanismOperation MO, SettlementAccountMarketfact SAM ");
		stringBuffer.append(" WHERE SAO.settlementOperation.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and MO.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = MGD.negotiationMechanism.idNegotiationMechanismPk ");
		stringBuffer.append(" and MO.mechanisnModality.negotiationModality.idNegotiationModalityPk = MGD.negotiationModality.idNegotiationModalityPk ");
		stringBuffer.append(" and SAM.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk ");
		stringBuffer.append(" and SAO.settlementOperation.settlementDate = :settlementDate ");
		stringBuffer.append(" and MO.securities.instrumentType = :instrumentType ");
		stringBuffer.append(" and ( (SAO.stockReference is null or SAO.stockReference = :referentialStock) and SAO.settlementOperation.operationState in (:lstCashOperationState) "
								+ " or (SAO.stockReference in (:lstTermStockReference) and SAO.settlementOperation.operationState = :cashSettledState) ) ");
		//we exclude the operations inside chains
		stringBuffer.append(" and not exists (SELECT HCD FROM HolderChainDetail HCD "
											+ " WHERE HCD.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk "
											+ " and HCD.chainedHolderOperation.chaintState in (:lstChainState) ) ");
		stringBuffer.append(" and SAO.operationState in (:lstSettlementAccountState) ");
		stringBuffer.append(" and SAO.settlementOperation.settlementSchema = :NETSchema ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = SAO.settlementOperation.settlementSchema ");
		stringBuffer.append(" and SAM.indActive = :constantOne ");
		//we exclude the operation inside alive settlement processes
		stringBuffer.append(" and not exists (SELECT OS FROM OperationSettlement OS "
											+ " WHERE OS.settlementOperation.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk "
											+ " and OS.settlementProcess.settlementDate = :settlementDate "
											+ " and OS.settlementProcess.processState in (:lstProcessState) ) ");
		//we exclude the report operations in cash part as buyers
		stringBuffer.append(" and not exists (SELECT SAO_REPO FROM SettlementAccountOperation SAO_REPO "
											+ " WHERE SAO_REPO.settlementOperation.mechanismOperation.indReportingBalance = :constantOne "
											+ " and SAO_REPO.settlementOperation.operationPart = :constantOne "
											+ " and SAO_REPO.role = :constantOne "
											+ " and SAO_REPO.operationState in (:lstSettlementAccountState) "
											+ " and SAO_REPO.idSettlementAccountPk = SAO.idSettlementAccountPk) ");
		if (Validations.validateIsNotNull(idModalityGroup)) {
			stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		}
		if(Validations.validateIsNotNull(indExtended)) {
			stringBuffer.append(" and SAO.settlementOperation.indExtended = :indExtended ");
		}
		if (Validations.validateIsNotNull(currency)) {
			stringBuffer.append(" and SAO.settlementOperation.settlementCurrency = :currency ");
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			stringBuffer.append(" and SAO.holderAccountOperation.inchargeStockParticipant.idParticipantPk = :idParticipant ");
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			stringBuffer.append(" and MO.securities.idSecurityCodePk = :idSecurityCode ");
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			stringBuffer.append(" and SAO.holderAccountOperation.holderAccount.idHolderAccountPk = :idHolderAccount ");
		}
		stringBuffer.append(" ORDER BY MO.securities.idSecurityCodePk, SAO.holderAccountOperation.holderAccount.idHolderAccountPk, "
									+ " SAM.marketDate, SAM.marketRate, SAM.marketQuantity, SAO.role ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("settlementDate", settlementDate);
		query.setParameter("instrumentType", instrumentType);
		query.setParameter("referentialStock", AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		List<Integer> lstCashOperationState= new ArrayList<Integer>();
		lstCashOperationState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstCashOperationState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("lstCashOperationState", lstCashOperationState);
		List<Long> lstTermStockReference= new ArrayList<Long>();
		lstTermStockReference.add(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		lstTermStockReference.add(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		query.setParameter("lstTermStockReference", lstTermStockReference);
		query.setParameter("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		List<Integer> lstChainState= new ArrayList<Integer>();
		lstChainState.add(ChainedOperationStateType.REGISTERED.getCode());
		lstChainState.add(ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("lstChainState", lstChainState);
		List<Integer> lstSettlementAccountState= new ArrayList<Integer>();
		lstSettlementAccountState.add(HolderAccountOperationStateType.REGISTERED.getCode());
		lstSettlementAccountState.add(HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("lstSettlementAccountState", lstSettlementAccountState);
		query.setParameter("NETSchema", SettlementSchemaType.NET.getCode());
		query.setParameter("constantOne", BooleanType.YES.getCode());
		List<Integer> lstProcessState= new ArrayList<Integer>();
		lstProcessState.add(SettlementProcessStateType.IN_PROCESS.getCode());
		lstProcessState.add(SettlementProcessStateType.STARTED.getCode());
		lstProcessState.add(SettlementProcessStateType.WAITING.getCode());
		lstProcessState.add(SettlementProcessStateType.ERROR.getCode());
		query.setParameter("lstProcessState", lstProcessState);
		
		if (Validations.validateIsNotNull(idModalityGroup)) {
			query.setParameter("idModalityGroup", idModalityGroup);
		}
		if(Validations.validateIsNotNull(indExtended)) {
			query.setParameter("indExtended", indExtended);
		}
		if (Validations.validateIsNotNull(currency)) {
			query.setParameter("currency", currency);
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			query.setParameter("idParticipant", idParticipant);
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			query.setParameter("idSecurityCode", idSecurityCode);
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			query.setParameter("idHolderAccount", idHolderAccount);
		}
		
		return query.getResultList();
	}
	
	/**
	 * Gets the lst chained holder operation.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id security code
	 * @param idHolderAccount the id holder account
	 * @param indExtended the ind extended
	 * @param idChainedHolderOperation the id chained holder operation
	 * @return the lst chained holder operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getLstChainedHolderOperation(Date settlementDate, Long idParticipant, String idSecurityCode, Long idHolderAccount, 
														Integer indExtended, Long idChainedHolderOperation)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HCD.chainedHolderOperation.idChainedHolderOperationPk, "); 			//0
		stringBuffer.append(" SAO.holderAccountOperation.inchargeStockParticipant.idParticipantPk, ");		//1
		stringBuffer.append(" HCD.chainedHolderOperation.security.idSecurityCodePk, ");						//2
		stringBuffer.append(" SAO.holderAccountOperation.holderAccount.idHolderAccountPk, ");				//3
		stringBuffer.append(" SAO.idSettlementAccountPk, ");												//4
		stringBuffer.append(" SAO.role, ");																	//5
		stringBuffer.append(" SAO.stockReference, ");														//6
		stringBuffer.append(" SAM.idSettAccountMarketfactPk, ");											//7
		stringBuffer.append(" SAM.marketDate, ");															//8
		stringBuffer.append(" SAM.marketRate, ");															//9
		stringBuffer.append(" SAM.chainedQuantity, ");														//10
		stringBuffer.append(" SAO.settlementOperation.idSettlementOperationPk, ");		//11
		stringBuffer.append(" SAO.settlementOperation.mechanismOperation.mechanisnModality.negotiationModality.idNegotiationModalityPk, "); //12
		stringBuffer.append(" SAO.settlementOperation.operationPart,  ");									//13
		stringBuffer.append(" SAM.marketQuantity  ");									//14
		stringBuffer.append(" FROM SettlementAccountOperation SAO, HolderChainDetail HCD , SettlementAccountMarketfact SAM ");
		stringBuffer.append(" WHERE SAO.idSettlementAccountPk = HCD.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk ");
		stringBuffer.append(" and SAM.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk ");
		stringBuffer.append(" and SAO.settlementOperation.settlementDate = :settlementDate ");
		stringBuffer.append(" and SAM.indActive = :constantOne ");
		stringBuffer.append(" and SAM.marketRate = HCD.chainedHolderOperation.marketRate ");
		stringBuffer.append(" and SAM.marketDate = HCD.chainedHolderOperation.marketDate ");
		stringBuffer.append(" and HCD.chainedHolderOperation.chaintState = :chainState ");
		stringBuffer.append(" and (SAO.settlementOperation.operationState in (:lstCashOperationState) and SAO.settlementOperation.operationPart = :cashPart "
								+ " or SAO.settlementOperation.operationState = :cashSettled and SAO.settlementOperation.operationPart = :termPart) ");
		stringBuffer.append(" and not exists (SELECT OS FROM OperationSettlement OS "
											+ " WHERE OS.settlementOperation.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk "
											+ " and OS.settlementProcess.settlementDate = :settlementDate "
											+ " and OS.settlementProcess.processState in (:lstProcessState) ) ");
		if (Validations.validateIsNotNull(idChainedHolderOperation)) {
			stringBuffer.append(" and HCD.chainedHolderOperation.idChainedHolderOperationPk = :idChainedHolderOperation ");
		}
		if(Validations.validateIsNotNull(indExtended)) {
			stringBuffer.append(" and SAO.settlementOperation.indExtended = :indExtended ");
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			stringBuffer.append(" and HCD.chainedHolderOperation.participant.idParticipantPk = :idParticipant ");
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			stringBuffer.append(" and HCD.chainedHolderOperation.security.idSecurityCodePk = :idSecurityCode ");
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			stringBuffer.append(" and HCD.chainedHolderOperation.holderAccount.idHolderAccountPk = :idHolderAccount ");
		}
		stringBuffer.append(" ORDER BY HCD.chainedHolderOperation.idChainedHolderOperationPk ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("settlementDate", settlementDate);
		query.setParameter("chainState", ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("constantOne", BooleanType.YES.getCode());
		List<Integer> lstCashOperationState= new ArrayList<Integer>();
		lstCashOperationState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstCashOperationState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("lstCashOperationState", lstCashOperationState);
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		query.setParameter("termPart", OperationPartType.TERM_PART.getCode());
		query.setParameter("cashSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
		List<Integer> lstProcessState= new ArrayList<Integer>();
		lstProcessState.add(SettlementProcessStateType.IN_PROCESS.getCode());
		lstProcessState.add(SettlementProcessStateType.STARTED.getCode());
		lstProcessState.add(SettlementProcessStateType.WAITING.getCode());
		query.setParameter("lstProcessState", lstProcessState);
		if (Validations.validateIsNotNull(idChainedHolderOperation)) {
			query.setParameter("idChainedHolderOperation", idChainedHolderOperation);
		}
		if(Validations.validateIsNotNull(indExtended)) {
			query.setParameter("indExtended", indExtended);
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			query.setParameter("idParticipant", idParticipant);
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			query.setParameter("idSecurityCode", idSecurityCode);
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			query.setParameter("idHolderAccount", idHolderAccount);
		}
		
		return query.getResultList();
	}
	
	/**
	 * Update chained quantity settlement account.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @param chainedQuantity the chained quantity
	 * @param behavior the behavior
	 * @param loggerUser the logger user
	 */
	public void updateChainedQuantitySettlementAccount(Long idSettlementAccountOperation, BigDecimal chainedQuantity, long behavior, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer= new StringBuffer();
		if (behavior == ComponentConstant.SUM) {
			stringBuffer.append(" UPDATE SettlementAccountOperation SET chainedQuantity = chainedQuantity + :chainedQuantity ");
		} else if (behavior == ComponentConstant.SUBTRACTION) {
			stringBuffer.append(" UPDATE SettlementAccountOperation SET chainedQuantity = chainedQuantity - :chainedQuantity ");
		}
		stringBuffer.append(" ,lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" ,lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" ,lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" ,lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk = :idSettlementAccountOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccountOperation", idSettlementAccountOperation);
		query.setParameter("chainedQuantity", chainedQuantity);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();		
	}
	
	/**
	 * Update chained quantity settlement marketfact.
	 *
	 * @param idSettlementAccountMarketfact the id settlement account marketfact
	 * @param chainedQuantity the chained quantity
	 * @param behavior the behavior
	 * @param loggerUser the logger user
	 */
	public void updateChainedQuantitySettlementMarketfact(Long idSettlementAccountMarketfact, BigDecimal chainedQuantity, long behavior, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer= new StringBuffer();
		if (behavior == ComponentConstant.SUM) {
			stringBuffer.append(" UPDATE SettlementAccountMarketfact SET chainedQuantity = chainedQuantity + :chainedQuantity ");
		} else if (behavior == ComponentConstant.SUBTRACTION) {
			stringBuffer.append(" UPDATE SettlementAccountMarketfact SET chainedQuantity = chainedQuantity - :chainedQuantity ");
		}
		stringBuffer.append(" ,lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" ,lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" ,lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" ,lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettAccountMarketfactPk = :idSettlementAccountMarketfact ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccountMarketfact", idSettlementAccountMarketfact);
		query.setParameter("chainedQuantity", chainedQuantity);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();		
	}
	
	/**
	 * Update state chained holder operation.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param chainState the chain state
	 * @param loggerUser the logger user
	 */
	public void updateStateChainedHolderOperation(Long idChainedHolderOperation, Integer chainState, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ChainedHolderOperation SET chaintState = :chainState ");
		if (ChainedOperationStateType.CONFIRMED.getCode().equals(chainState)) {
			stringBuffer.append(" ,confirmDate = :lastModifyDate ");
			stringBuffer.append(" ,confirmyUser = :lastModifyUser ");
		} else if (ChainedOperationStateType.REJECTED.getCode().equals(chainState)) {
			stringBuffer.append(" ,rejectDate = :lastModifyDate ");
			stringBuffer.append(" ,rejectUser = :lastModifyUser ");
		} else if (ChainedOperationStateType.CANCELLED.getCode().equals(chainState)) {
			stringBuffer.append(" ,cancelDate = :lastModifyDate ");
			stringBuffer.append(" ,cancelUser = :lastModifyUser ");
		}
		stringBuffer.append(" ,lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" ,lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" ,lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" ,lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idChainedHolderOperationPk = :idChainedHolderOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idChainedHolderOperation", idChainedHolderOperation);
		query.setParameter("chainState", chainState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Cancel chained holder operations.
	 *
	 * @param settlementDate the settlement date
	 * @param indExtended the ind extended
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCode the id security code
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void cancelChainedHolderOperations(Date settlementDate, Integer indExtended, Long idParticipant, Long idHolderAccount, 
												String idSecurityCode, Long idChainedHolderOperation, LoggerUser loggerUser) throws ServiceException
	{
		List<Object[]> lstChainedHolderOperation= getLstChainedHolderOperation(settlementDate, 
														idParticipant, idSecurityCode, idHolderAccount, indExtended, idChainedHolderOperation);
		if (Validations.validateListIsNotNullAndNotEmpty(lstChainedHolderOperation)) {
			Long beforeChainedHolderOperation= null;
			for (Object[] objChainedHolderOperation: lstChainedHolderOperation) {
				idChainedHolderOperation = new Long(objChainedHolderOperation[0].toString());
				if (Validations.validateIsNull(beforeChainedHolderOperation)) {
					beforeChainedHolderOperation= idChainedHolderOperation; 
				}
				Integer role= new Integer(objChainedHolderOperation[5].toString());
				Long idSettlementAccountOperation= new Long(objChainedHolderOperation[4].toString());
				Long idSettlementAccountMarketfact= new Long(objChainedHolderOperation[7].toString());
				Long stockreference= (Long)objChainedHolderOperation[6];
				BigDecimal chainedQuantity= (BigDecimal)objChainedHolderOperation[10];
				BigDecimal marketQuantity = (BigDecimal)objChainedHolderOperation[14];
				BigDecimal normalQuantity = marketQuantity.subtract(chainedQuantity);
				//we subtract the chained quantity 
				updateChainedQuantitySettlementAccount(idSettlementAccountOperation, chainedQuantity, 
											ComponentConstant.SUBTRACTION, loggerUser);
				updateChainedQuantitySettlementMarketfact(idSettlementAccountMarketfact, chainedQuantity, 
											ComponentConstant.SUBTRACTION, loggerUser);
				//we unblock the normal sale balance only
				if (ComponentConstant.SALE_ROLE.equals(role)) {
					if (AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(stockreference) && normalQuantity.compareTo(BigDecimal.ZERO) > 0) {
						idParticipant= (Long)objChainedHolderOperation[1];
						idSecurityCode= objChainedHolderOperation[2].toString();
						idHolderAccount= (Long)objChainedHolderOperation[3];
						Long idSettlementOperation= (Long)objChainedHolderOperation[11];
						Long idModality= (Long)objChainedHolderOperation[12];
						Integer operationPart = (Integer)objChainedHolderOperation[13];
						
						MarketFactAccountTO objMarketFactAccountTO= new MarketFactAccountTO();
						objMarketFactAccountTO.setMarketDate((Date) objChainedHolderOperation[8]);
						objMarketFactAccountTO.setMarketRate((BigDecimal)objChainedHolderOperation[9]);
						objMarketFactAccountTO.setQuantity(normalQuantity);
						List<MarketFactAccountTO> lstMarketFactAccountTO= new ArrayList<MarketFactAccountTO>();
						lstMarketFactAccountTO.add(objMarketFactAccountTO);
						
						HolderAccountBalanceTO objHolderAccountBalanceTO= new HolderAccountBalanceTO();
						List<HolderAccountBalanceTO> lstHolderAccountBalanceTO= new ArrayList<HolderAccountBalanceTO>();
						objHolderAccountBalanceTO.setIdParticipant(idParticipant);
						objHolderAccountBalanceTO.setIdHolderAccount(idHolderAccount);
						objHolderAccountBalanceTO.setIdSecurityCode(idSecurityCode);
						objHolderAccountBalanceTO.setStockQuantity(normalQuantity);
						objHolderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccountTO);
						lstHolderAccountBalanceTO.add(objHolderAccountBalanceTO);
						
						AccountsComponentTO objAccountsComponentTO= new AccountsComponentTO();
						objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.CANCEL_CHAIN_OPERATION.getCode());
						objAccountsComponentTO.setIdSettlementOperation(idSettlementOperation);
						objAccountsComponentTO.setIndMarketFact(BooleanType.YES.getCode());
						objAccountsComponentTO.setIdOperationType(NegotiationModalityType.get(idModality).getParameterOperationType());
						objAccountsComponentTO.setLstSourceAccounts(lstHolderAccountBalanceTO);
						objAccountsComponentTO.setOperationPart(operationPart);
						
						accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
						
						settlementProcessService.updateStockSettlementAccountOperation(idSettlementAccountOperation, null, loggerUser);
					}
				}
				//we change to other chained operation
				if (!beforeChainedHolderOperation.equals(idChainedHolderOperation)) {
					updateStateChainedHolderOperation(idChainedHolderOperation, ChainedOperationStateType.CANCELLED.getCode(), loggerUser);
				}
				beforeChainedHolderOperation= idChainedHolderOperation;
			}
			//we update for the last chained operation
			updateStateChainedHolderOperation(idChainedHolderOperation, ChainedOperationStateType.CANCELLED.getCode(), loggerUser);
		}
	}
	
	/**
	 * Creates the chained holder operations automatic.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param indExtended the ind extended
	 * @param instrumentType the instrument type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void createChainedHolderOperationsAutomatic(Date settlementDate, Long idParticipant, Integer indExtended, 
														Integer instrumentType, LoggerUser loggerUser) throws ServiceException
	{
		//we keep in mind the settlementAccountOperation for all participants, holder accounts and securities 
		Long idModalityGroup= null, idHolderAccount= null;
		String idSecurityCode= null;
		Integer currency= null;
		List<Object[]> lstSettlementAccountOperation= getLstSettlementAccountOperation(settlementDate, idParticipant, 
																idSecurityCode, idHolderAccount, idModalityGroup, currency, indExtended, instrumentType);
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountOperation)) {
			Map<String,RegisterChainedOperationTO> mpChainedHolderOperation= new LinkedHashMap<String,RegisterChainedOperationTO>();
			for (Object[] objChainedSettlementOperation: lstSettlementAccountOperation) {
				idSecurityCode= objChainedSettlementOperation[0].toString();
				idParticipant= new Long(objChainedSettlementOperation[1].toString());
				idHolderAccount= new Long(objChainedSettlementOperation[2].toString());
				idModalityGroup= new Long(objChainedSettlementOperation[10].toString());
				Date marketDate= (Date) objChainedSettlementOperation[3];
				BigDecimal marketRate= new BigDecimal(objChainedSettlementOperation[4].toString());
				currency= new Integer(objChainedSettlementOperation[11].toString());
				BigDecimal stockQuantity= new BigDecimal(objChainedSettlementOperation[5].toString());
				Integer role= new Integer(objChainedSettlementOperation[7].toString());
				Long idSettlementAccount= new Long(objChainedSettlementOperation[8].toString());
				SettlementAccountOperation objSettlementAccountOperation= new SettlementAccountOperation(idSettlementAccount);
				Long idSettlementAccountMarketfact= new Long(objChainedSettlementOperation[9].toString());
				Integer securityClass= new Integer(objChainedSettlementOperation[24].toString());
				SettlementAccountMarketfact settlementAccountMarketfact= new SettlementAccountMarketfact();
				settlementAccountMarketfact.setIdSettAccountMarketfactPk(idSettlementAccountMarketfact);
				settlementAccountMarketfact.setMarketDate(marketDate);
				settlementAccountMarketfact.setMarketQuantity(stockQuantity);
				settlementAccountMarketfact.setMarketRate(marketRate);
				settlementAccountMarketfact.setChainedQuantity(stockQuantity);
				settlementAccountMarketfact.setSettlementAccountOperation(objSettlementAccountOperation);
				
				String strChainedHolderOperation= idParticipant+"*"+idHolderAccount+"*"+idSecurityCode+"*"+idModalityGroup+"*"+marketDate+"*"+marketRate+"*"+currency;
				//we verify if exists the same combination to be chain holder operation
				if (mpChainedHolderOperation.containsKey(strChainedHolderOperation)) {
					RegisterChainedOperationTO objRegisterChainedOperationTO= mpChainedHolderOperation.get(strChainedHolderOperation);
					if (ComponentConstant.SALE_ROLE.equals(role)) {
						objRegisterChainedOperationTO.setSaleQuantity(objRegisterChainedOperationTO.getSaleQuantity().add(stockQuantity));
						objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().add(settlementAccountMarketfact);
					} else if (ComponentConstant.PURCHARSE_ROLE.equals(role)) {
						objRegisterChainedOperationTO.setPurchaseQuantity(objRegisterChainedOperationTO.getPurchaseQuantity().add(stockQuantity));
						objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().add(settlementAccountMarketfact);
					}
				} else { //we must create a new object to register
					RegisterChainedOperationTO objRegisterChainedOperationTO= new RegisterChainedOperationTO();
					objRegisterChainedOperationTO.setIndAutomatic(BooleanType.YES.getCode());
					objRegisterChainedOperationTO.getParticipant().setIdParticipantPk(idParticipant);
					objRegisterChainedOperationTO.getHolderAccount().setIdHolderAccountPk(idHolderAccount);
					objRegisterChainedOperationTO.getSecurity().setIdSecurityCodePk(idSecurityCode);
					objRegisterChainedOperationTO.getSecurity().setSecurityClass(securityClass);
					objRegisterChainedOperationTO.getModalityGroup().setIdModalityGroupPk(idModalityGroup);
					objRegisterChainedOperationTO.setMarketDate(marketDate);
					objRegisterChainedOperationTO.setMarketRate(marketRate);
					objRegisterChainedOperationTO.setSettlementDate(settlementDate);
					objRegisterChainedOperationTO.getCurrency().setParameterTablePk(currency);
					objRegisterChainedOperationTO.getIndExtended().setParameterTablePk(indExtended);
					if (ComponentConstant.SALE_ROLE.equals(role)) {
						objRegisterChainedOperationTO.setSaleQuantity(stockQuantity);
						objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().add(settlementAccountMarketfact);
					} else if (ComponentConstant.PURCHARSE_ROLE.equals(role)) {
						objRegisterChainedOperationTO.setPurchaseQuantity(stockQuantity);
						objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().add(settlementAccountMarketfact);
					}
					mpChainedHolderOperation.put(strChainedHolderOperation, objRegisterChainedOperationTO);
				}
			}
			if (!mpChainedHolderOperation.isEmpty()) {
				List<String> lstKeyChainedHolderOperation= new ArrayList<String>(mpChainedHolderOperation.keySet());
				for (String keyChainedHolderOperation: lstKeyChainedHolderOperation) {
					RegisterChainedOperationTO objRegisterChainedOperationTO= mpChainedHolderOperation.get(keyChainedHolderOperation);
					configureChainedHolderOperation(objRegisterChainedOperationTO);
					if (objRegisterChainedOperationTO.getPurchaseQuantity().compareTo(BigDecimal.ZERO) > 0 &&
						objRegisterChainedOperationTO.getSaleQuantity().compareTo(BigDecimal.ZERO) > 0) 
					{
						//at this time, we got the correct chain settlement account operations in order to save them
						createChainedHolderOperation(objRegisterChainedOperationTO, loggerUser);
						//if the security class is DPF then we confirm the chain
						if (SecurityClassType.DPF.getCode().equals(objRegisterChainedOperationTO.getSecurity().getSecurityClass())) {
							List<ChainedOperationDataTO> lstChainedOperationOperation= new ArrayList<ChainedOperationDataTO>();
							ChainedOperationDataTO chainedOperationDataTO= new ChainedOperationDataTO();
							chainedOperationDataTO.setLstSettlementAccountMarketfact(objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact());
							chainedOperationDataTO.getLstSettlementAccountMarketfact().addAll(objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact());
							lstChainedOperationOperation.add(chainedOperationDataTO);
							objRegisterChainedOperationTO.setLstChainedOperationDataTO(new GenericDataModel<>(lstChainedOperationOperation));
							confirmChainedHolderOperation(objRegisterChainedOperationTO, loggerUser);
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * Configure chained holder operation.
	 *
	 * @param objRegisterChainedOperationTO the obj register chained operation to
	 */
	private void configureChainedHolderOperation(RegisterChainedOperationTO objRegisterChainedOperationTO)
	{
		BigDecimal saleQuantity= objRegisterChainedOperationTO.getSaleQuantity();
		BigDecimal purchasQuantity= objRegisterChainedOperationTO.getPurchaseQuantity();
		BigDecimal deltaQuantity= saleQuantity.subtract(purchasQuantity);
		if (deltaQuantity.compareTo(BigDecimal.ZERO) > 0 ) {
			//we must verify the the sale role operations. The list of SettlementAccountOperation is order by stock quantity
			//SettlementAccountOperation objSettlementAccountOperation= objRegisterChainedOperationTO.getSaleSettlementAccountOperation().get(0);
			SettlementAccountMarketfact settlementAccountMarketfact= objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().get(0);
			BigDecimal stockQuantity= settlementAccountMarketfact.getMarketQuantity();
			if (deltaQuantity.compareTo(stockQuantity) >= 0) {
				//we cannot have settlementAccountOperation with stock quantity greater than deltaQuantity
				objRegisterChainedOperationTO.setSaleQuantity(objRegisterChainedOperationTO.getSaleQuantity().subtract(stockQuantity));
				//objRegisterChainedOperationTO.getSaleSettlementAccountOperation().remove(objSettlementAccountOperation);
				objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().remove(settlementAccountMarketfact);
				configureChainedHolderOperation(objRegisterChainedOperationTO);
			} else {
				//we'll chain only the rest of stock quantity
				//objSettlementAccountOperation.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
				settlementAccountMarketfact.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
			}
		} else if (deltaQuantity.compareTo(BigDecimal.ZERO) < 0) {
			//we must verify the the purchase role operations. The list of SettlementAccountOperation is order by stock quantity
			deltaQuantity= deltaQuantity.abs();
			//SettlementAccountOperation objSettlementAccountOperation= objRegisterChainedOperationTO.getPurchaseSettlementAccountOperation().get(0);
			SettlementAccountMarketfact settlementAccountMarketfact= objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().get(0);
			BigDecimal stockQuantity= settlementAccountMarketfact.getMarketQuantity();
			if (deltaQuantity.compareTo(stockQuantity) >= 0) {
				//we cannot have settlementAccountOperation with stock quantity greater than deltaQuantity
				objRegisterChainedOperationTO.setPurchaseQuantity(objRegisterChainedOperationTO.getPurchaseQuantity().subtract(stockQuantity));
				//objRegisterChainedOperationTO.getPurchaseSettlementAccountOperation().remove(objSettlementAccountOperation);
				objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().remove(settlementAccountMarketfact);
				configureChainedHolderOperation(objRegisterChainedOperationTO);
			} else {
				//we'll chain only the rest of stock quantity
				//objSettlementAccountOperation.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
				settlementAccountMarketfact.setChainedQuantity(stockQuantity.subtract(deltaQuantity));
			}
		}
	}
	
	/**
	 * Creates the chained holder operation.
	 *
	 * @param objRegisterChainedOperationTO the obj register chained operation to
	 * @param loggerUser the logger user
	 */
	public void createChainedHolderOperation(RegisterChainedOperationTO objRegisterChainedOperationTO, LoggerUser loggerUser)
	{
		ChainedHolderOperation objChainedHolderOperation= new ChainedHolderOperation();
		objChainedHolderOperation.setChaintState(ChainedOperationStateType.REGISTERED.getCode());
		objChainedHolderOperation.setHolderAccount(objRegisterChainedOperationTO.getHolderAccount());
		objChainedHolderOperation.setMarketDate(objRegisterChainedOperationTO.getMarketDate());
		objChainedHolderOperation.setMarketRate(objRegisterChainedOperationTO.getMarketRate());
		objChainedHolderOperation.setModalityGroup(objRegisterChainedOperationTO.getModalityGroup());
		objChainedHolderOperation.setParticipant(objRegisterChainedOperationTO.getParticipant());
		objChainedHolderOperation.setPurchaseQuantity(objRegisterChainedOperationTO.getPurchaseQuantity());
		objChainedHolderOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		objChainedHolderOperation.setRegisterUser(loggerUser.getUserName());
		objChainedHolderOperation.setSaleQuantity(objRegisterChainedOperationTO.getSaleQuantity());
		objChainedHolderOperation.setSecurity(objRegisterChainedOperationTO.getSecurity());
		objChainedHolderOperation.setSettlementDate(objRegisterChainedOperationTO.getSettlementDate());
		objChainedHolderOperation.setCurrency(objRegisterChainedOperationTO.getCurrency().getParameterTablePk());
		objChainedHolderOperation.setIndExtended(objRegisterChainedOperationTO.getIndExtended().getParameterTablePk());
		objChainedHolderOperation.setIndAutomatic(objRegisterChainedOperationTO.getIndAutomatic());
		this.create(objChainedHolderOperation);
		objRegisterChainedOperationTO.setIdChainedHolderOperation(objChainedHolderOperation.getIdChainedHolderOperationPk());
		
		createHolderChainDetail(objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact(), objChainedHolderOperation);
		createHolderChainDetail(objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact(), objChainedHolderOperation);
	}
	
	/**
	 * Creates the holder chain detail.
	 *
	 * @param lstSettlementAccountMarketfact the lst settlement account marketfact
	 * @param objChainedHolderOperation the obj chained holder operation
	 */
	private void createHolderChainDetail(List<SettlementAccountMarketfact> lstSettlementAccountMarketfact, ChainedHolderOperation objChainedHolderOperation)
	{
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountMarketfact)) {
			for (SettlementAccountMarketfact settlementAccountMarketfact: lstSettlementAccountMarketfact) {
				HolderChainDetail objHolderChainDetail= new HolderChainDetail();
				objHolderChainDetail.setChainedHolderOperation(objChainedHolderOperation);
				objHolderChainDetail.setChainedQuantity(settlementAccountMarketfact.getChainedQuantity());
				objHolderChainDetail.setSettlementAccountMarketfact(settlementAccountMarketfact);
				this.create(objHolderChainDetail);
			}
		}
	}
	
	/**
	 * This method confirm the chain, update indicators 
	 * @param RegisterChainedOperationTO chainedOperationTO
	 * @param LoggerUser loggerUser
	 * @throws ServiceException
	 */
	public void confirmChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO, LoggerUser loggerUser) throws ServiceException
	{
		boolean hasNormalPart= false; //flag to identify if the settlement account has sale normal part
		BigDecimal chainedQuantityTemp = BigDecimal.ZERO;
		SettlementAccountMarketfact settlementAccountMarketfactActual = null;
		
		if (chainedOperationTO.getLstChainedOperationDataTO() != null) {
			List<ChainedOperationDataTO> lstChainedOperationOperation= chainedOperationTO.getLstChainedOperationDataTO().getDataList();
			for (ChainedOperationDataTO objChainedOperationDataTO: lstChainedOperationOperation) {
				//List<SettlementAccountOperation> lstSettlementAccountOperation= objChainedOperationDataTO.getLstSettlementAccountOperation();
				List<SettlementAccountMarketfact> lstSettlementAccountMarketfact= objChainedOperationDataTO.getLstSettlementAccountMarketfact();
				for (SettlementAccountMarketfact settlementAccountMarketfact: lstSettlementAccountMarketfact) {
					
					chainedQuantityTemp = BigDecimal.ZERO;
					// Recuperando objeto actual SettlementAccountMarketfact,
					// para obtener cantidad de Valores Actual
					settlementAccountMarketfactActual = em.find(SettlementAccountMarketfact.class, settlementAccountMarketfact.getIdSettAccountMarketfactPk());
					
					if (settlementAccountMarketfactActual != null) {
						chainedQuantityTemp = settlementAccountMarketfactActual.getChainedQuantity();
						// sumando la cantidad que sera actualizado por una operacion de la cadena
						chainedQuantityTemp = chainedQuantityTemp.add(settlementAccountMarketfact.getChainedQuantity());
					}
					
					// validando que se la cantidad que se este encadenando no
					// supere la cantidad de estock
					if (chainedQuantityTemp.compareTo(settlementAccountMarketfact.getMarketQuantity()) > 0) {
						// propagando excepcion
						throw new ServiceException(ErrorServiceType.ERROR_CHAINED_QUANTITY_INVALID);
					}
					
					//we update the settlement account market fact
					updateChainedSettlementMarketFact(settlementAccountMarketfact.getIdSettAccountMarketfactPk(),  
													settlementAccountMarketfact.getChainedQuantity(), 
													ComponentConstant.SUM, loggerUser);
					//we update the settlement account operation
					updateChainedQuantitySettlementAccount(settlementAccountMarketfact.getSettlementAccountOperation().getIdSettlementAccountPk(), 
															settlementAccountMarketfact.getChainedQuantity(), ComponentConstant.SUM, loggerUser);
					if (ComponentConstant.SALE_ROLE.equals(settlementAccountMarketfact.getSettlementAccountOperation().getRole())) {
						//if the settlement account has sale normal part, then we cannot check as VC to the chain. It require to block balances
						if (settlementAccountMarketfact.getMarketQuantity().compareTo(settlementAccountMarketfact.getChainedQuantity()) != 0) {
							hasNormalPart= true;
						}
					}
				}
			}
		}
		//we update the chained holder operation state
		updateStateChainedHolderOperation(chainedOperationTO.getIdChainedHolderOperation(), 
											ChainedOperationStateType.CONFIRMED.getCode(), loggerUser);
		//if the purchase quantity is greater than sale quantity, then is not required to block balance at sale
		if (chainedOperationTO.getPurchaseQuantity().compareTo(chainedOperationTO.getSaleQuantity()) >= 0 && !hasNormalPart) {
			updateChainedHolderOperationStockReference(chainedOperationTO.getIdChainedHolderOperation(), 
														AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		}
	}
	
	
	/**
	 * Update chained settlement market fact.
	 *
	 * @param idSettlementAccountMarketfact the id settlement account marketfact
	 * @param chainedQuantity the chained quantity
	 * @param behavior the behavior
	 * @param loggerUser the logger user
	 */
	public void updateChainedSettlementMarketFact(Long idSettlementAccountMarketfact, BigDecimal chainedQuantity, long behavior, LoggerUser loggerUser) 
	{
		StringBuffer stringBuffer= new StringBuffer();
		if (behavior == ComponentConstant.SUM) {
			stringBuffer.append(" UPDATE SettlementAccountMarketfact SET chainedQuantity = chainedQuantity + :chainedQuantity ");
		} else if (behavior == ComponentConstant.SUBTRACTION) {
			stringBuffer.append(" UPDATE SettlementAccountMarketfact SET chainedQuantity = chainedQuantity - :chainedQuantity ");
		}
		stringBuffer.append(" ,lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" ,lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" ,lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" ,lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettAccountMarketfactPk = :idSettlementAccountMarketfact ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccountMarketfact", idSettlementAccountMarketfact);
		query.setParameter("chainedQuantity", chainedQuantity);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	
	/**
	 * Gets the list holder chain detail.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @return the list holder chain detail
	 */
	@SuppressWarnings("unchecked")
	public List<HolderChainDetail> getListHolderChainDetail(Long idChainedHolderOperation)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HCD FROM HolderChainDetail HCD ");
		stringBuffer.append(" INNER JOIN FETCH HCD.settlementAccountMarketfact SAM ");
		stringBuffer.append(" INNER JOIN FETCH SAM.settlementAccountOperation SAO ");
		stringBuffer.append(" INNER JOIN FETCH SAO.settlementOperation SO ");
		stringBuffer.append(" INNER JOIN FETCH SO.mechanismOperation MO ");
		stringBuffer.append(" INNER JOIN FETCH MO.mechanisnModality MM ");
		stringBuffer.append(" INNER JOIN FETCH MO.buyerParticipant ");
		stringBuffer.append(" INNER JOIN FETCH MO.sellerParticipant ");
		stringBuffer.append(" INNER JOIN FETCH MM.negotiationModality MOD ");
		stringBuffer.append(" WHERE HCD.chainedHolderOperation.idChainedHolderOperationPk = :idChainedHolderOperation ");
		stringBuffer.append(" and SAM.indActive =:indActive ");
		stringBuffer.append(" ORDER BY SAO.role ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idChainedHolderOperation", idChainedHolderOperation);
		query.setParameter("indActive", BooleanType.YES.getCode());
		return query.getResultList();
	}
	
	/**
	 * Gets the chained holder operation.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @return the chained holder operation
	 */
	public ChainedHolderOperation getChainedHolderOperation(Long idChainedHolderOperation)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT CHO FROM ChainedHolderOperation CHO ");
		stringBuffer.append(" INNER JOIN FETCH CHO.modalityGroup MG ");
		stringBuffer.append(" INNER JOIN FETCH MG.negotiationMechanism ");
		stringBuffer.append(" INNER JOIN FETCH CHO.participant ");
		stringBuffer.append(" INNER JOIN FETCH CHO.holderAccount HA ");
		stringBuffer.append(" INNER JOIN FETCH HA.holderAccountDetails HAD ");
		stringBuffer.append(" INNER JOIN FETCH HAD.holder ");
		stringBuffer.append(" INNER JOIN FETCH CHO.security ");
		stringBuffer.append(" WHERE CHO.idChainedHolderOperationPk = :idChainedHolderOperation ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idChainedHolderOperation", idChainedHolderOperation);
		return (ChainedHolderOperation) query.getSingleResult();
	}
	
	/**
	 * Update chained holder operation stock reference.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param stockReference the stock reference
	 */
	public void updateChainedHolderOperationStockReference(Long idChainedHolderOperation, Long stockReference)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ChainedHolderOperation SET stockReference= :stockReference ");
		stringBuffer.append(" WHERE idChainedHolderOperationPk = :idChainedHolderOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idChainedHolderOperation", idChainedHolderOperation);
		query.setParameter("stockReference", stockReference);
		query.executeUpdate();
	}
	
	/**
	 * Gets the list settlement account operation by chain.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @return the list settlement account operation by chain
	 */
	public List<SettlementAccountOperation> getListSettlementAccountOperationByChain(Long idChainedHolderOperation)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SAO FROM SettlementAccountOperation SAO, SettlementAccountMarketfact SAM, HolderChainDetail HCD ");
		stringBuffer.append(" INNER JOIN FETCH SAO.settlementOperation ");
		stringBuffer.append(" WHERE HCD.settlementAccountMarketfact.idSettAccountMarketfactPk = SAM.idSettAccountMarketfactPk ");
		stringBuffer.append(" and SAO.idSettlementAccountPk = SAM.settlementAccountOperation.idSettlementAccountPk ");
		stringBuffer.append(" and HCD.chainedHolderOperation.idChainedHolderOperationPk = :idChainedHolderOperation ");
		
		TypedQuery<SettlementAccountOperation> typedQuery= em.createQuery(stringBuffer.toString(), SettlementAccountOperation.class);
		typedQuery.setParameter("idChainedHolderOperation", idChainedHolderOperation);
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Gets the list chained settlement account marketfact.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @return the list chained settlement account marketfact
	 */
	@SuppressWarnings("unchecked")
	public List<HolderChainDetail> getListChainedSettlementAccountMarketfact(Long idSettlementProcess) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HCD ");
//		stringBuffer.append(" SELECT HCD.chainedHolderOperation.idChainedHolderOperationPk, "); //0
//		stringBuffer.append(" SAM, "); //1
//		stringBuffer.append(" HCD.settlementAccountMarketfact.settlementAccountOperation.role "); //2
		stringBuffer.append(" FROM HolderChainDetail HCD, OperationSettlement OS ");
		stringBuffer.append(" INNER JOIN FETCH HCD.settlementAccountMarketfact SAM ");
		stringBuffer.append(" INNER JOIN FETCH SAM.settlementAccountOperation SAO ");
		stringBuffer.append(" WHERE OS.settlementOperation.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk ");
		stringBuffer.append(" and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		stringBuffer.append(" and OS.operationState = :operationState ");
		stringBuffer.append(" and SAM.indActive = :indActive ");
		stringBuffer.append(" and SAM.chainedQuantity > 0 ");
		stringBuffer.append(" and HCD.chainedHolderOperation.stockReference = :stockReference ");
		stringBuffer.append(" and HCD.chainedHolderOperation.security.securityClass not in (:lstSecurityClass) ");
		stringBuffer.append(" and HCD.chainedHolderOperation.chaintState = :chaintState ");
		stringBuffer.append(" and HCD.chainedHolderOperation.purchaseQuantity >= HCD.chainedHolderOperation.saleQuantity ");
		stringBuffer.append(" ORDER BY HCD.chainedHolderOperation.idChainedHolderOperationPk, HCD.settlementAccountMarketfact.settlementAccountOperation.role,"
							+ "	HCD.settlementAccountMarketfact.marketQuantity, HCD.idHolderChainDetailPk ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.setParameter("operationState", OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode());
		query.setParameter("indActive", BooleanType.YES.getCode());
		query.setParameter("stockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		query.setParameter("chaintState", ChainedOperationStateType.CONFIRMED.getCode());
		List<Integer> lstSecurityClass= new ArrayList<Integer>();
		lstSecurityClass.add(SecurityClassType.DPA.getCode());
		lstSecurityClass.add(SecurityClassType.DPF.getCode());
		query.setParameter("lstSecurityClass", lstSecurityClass);
		
		return query.getResultList();
	}
	
	/**
	 * Validate chained holder operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @throws ServiceException the service exception
	 */	
	public void validateChainedHolderOperation(Long idHolderAccountOperation, Long idParticipant) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT distinct mo.ballotNumber , mo.sequential , mo.operationNumber, mo.operationDate ");
		sbQuery.append("	from SettlementAccountOperation sao ");
		sbQuery.append("	inner join sao.settlementOperation so ");
		sbQuery.append("	inner join so.mechanismOperation mo ");
		sbQuery.append("	inner join sao.holderAccountOperation hao ");
		sbQuery.append("	inner join hao.holderAccount ha ");
		sbQuery.append("	where hao.idHolderAccountOperationPk = :idHolderAccountOperation  ");
		sbQuery.append("	and exists ( ");
		sbQuery.append("		select hcd.idHolderChainDetailPk from HolderChainDetail hcd inner join hcd.chainedHolderOperation cho ");
		sbQuery.append("		where hcd.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = sao.idSettlementAccountPk  ");
		sbQuery.append("		and cho.chaintState not in  (:xcludedStates)   ");
		if(Validations.validateIsNotNullAndNotEmpty(idParticipant)){
			sbQuery.append("		and cho.participant.idParticipantPk = :idParticipant  ) ");
		} else {
			sbQuery.append("		 ) ");
		}
		
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		List<Integer> xcludedStates= new ArrayList<Integer>();
		xcludedStates.add(ChainedOperationStateType.CANCELLED.getCode());
		xcludedStates.add(ChainedOperationStateType.REJECTED.getCode());
		parameters.put("xcludedStates",xcludedStates);
		if(Validations.validateIsNotNullAndNotEmpty(idParticipant)){
			parameters.put("idParticipant", idParticipant);
		}
						
		Object[] result = (Object[])findObjectByQueryString(sbQuery.toString(), parameters);
		if(result != null){
			String ballotNumber = result[0].toString();
			String sequential = result[1].toString();
			StringBuffer operation = new StringBuffer();
			if(ballotNumber != null && sequential !=null){
				operation.append(ballotNumber).append(GeneralConstants.SLASH).append(sequential);
			}else{
				Long operationNumber = new Long(result[2].toString());
				Date date = (Date)result[3];
				operation.append(operationNumber).append(GeneralConstants.DASH).append(date);
			}
			throw new ServiceException(ErrorServiceType.CHAINED_OPERATIONS_OPERATION_EXISTS_AS_CHAIN, new Object[]{operation.toString()});
		}
	}
	
	
	/**
	 * Validate the chained holder operations automatic.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param indExtended the indicator extended
	 * @param instrumentType the instrument type
	 * @param loggerUser the logger user
	 * @return the Map Register Chained Operation TO
	 * @throws ServiceException the service exception
	 */
	public List<RegisterChainedOperationTO> validateChainedHolderOperations(Date settlementDate, Long idParticipant, List<Integer> lstIndExtended, 
														Integer instrumentType) throws ServiceException {
		//we keep in mind the settlementAccountOperation for all participants, holder accounts and securities 
		Map<String,RegisterChainedOperationTO> mpChainedHolderOperation = null;
		List<RegisterChainedOperationTO> lstRegisterChainedOperationTO = null;
		Long idModalityGroup = null, idHolderAccount = null;
		String idSecurityCode = null;
		Integer currency = null;
		String mnemonicPart = null;
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstIndExtended)){
			mpChainedHolderOperation = new LinkedHashMap<String,RegisterChainedOperationTO>();
			for(Integer indExtended : lstIndExtended) {
				
				List<Object[]> lstSettlementAccountOperation = getLstSettlementAccountOperationByValidateChained(settlementDate, idParticipant, 
						idSecurityCode, idHolderAccount, idModalityGroup, currency, indExtended, instrumentType);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountOperation)) {					
					for (Object[] objChainedSettlementOperation : lstSettlementAccountOperation) {
						idSecurityCode = objChainedSettlementOperation[0].toString();
						idParticipant = new Long(objChainedSettlementOperation[1].toString());	
						mnemonicPart = objChainedSettlementOperation[24].toString();
						idHolderAccount = new Long(objChainedSettlementOperation[2].toString());
						idModalityGroup = new Long(objChainedSettlementOperation[10].toString());
						Date marketDate = (Date) objChainedSettlementOperation[3];
						BigDecimal marketRate = new BigDecimal(objChainedSettlementOperation[4].toString());
						currency = new Integer(objChainedSettlementOperation[11].toString());
						BigDecimal stockQuantity = new BigDecimal(objChainedSettlementOperation[5].toString());
						Integer role = new Integer(objChainedSettlementOperation[7].toString());				
						Long idSettlementAccountMarketfact = new Long(objChainedSettlementOperation[9].toString());
						SettlementAccountMarketfact settlementAccountMarketfact= new SettlementAccountMarketfact();
						settlementAccountMarketfact.setIdSettAccountMarketfactPk(idSettlementAccountMarketfact);
						settlementAccountMarketfact.setMarketDate(marketDate);
						settlementAccountMarketfact.setMarketQuantity(stockQuantity);
						settlementAccountMarketfact.setMarketRate(marketRate);
						settlementAccountMarketfact.setChainedQuantity(stockQuantity);				
						String strChainedHolderOperation= idParticipant+"*"+idHolderAccount+"*"+idSecurityCode+"*"+idModalityGroup+"*"+marketDate+"*"+marketRate+"*"+currency;
						//we verify if exists the same combination to be chain holder operation
						if (mpChainedHolderOperation.containsKey(strChainedHolderOperation)) {
							RegisterChainedOperationTO objRegisterChainedOperationTO= mpChainedHolderOperation.get(strChainedHolderOperation);
							if (ComponentConstant.SALE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setSaleQuantity(objRegisterChainedOperationTO.getSaleQuantity().add(stockQuantity));
								objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().add(settlementAccountMarketfact);
							} else if (ComponentConstant.PURCHARSE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setPurchaseQuantity(objRegisterChainedOperationTO.getPurchaseQuantity().add(stockQuantity));
								objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().add(settlementAccountMarketfact);
							}
						} else { 
							//we must create a new object to register
							RegisterChainedOperationTO objRegisterChainedOperationTO= new RegisterChainedOperationTO();
							objRegisterChainedOperationTO.setIndAutomatic(BooleanType.YES.getCode());
							objRegisterChainedOperationTO.getParticipant().setIdParticipantPk(idParticipant);							
							objRegisterChainedOperationTO.getParticipant().setMnemonic(mnemonicPart);							
							objRegisterChainedOperationTO.getHolderAccount().setIdHolderAccountPk(idHolderAccount);
							objRegisterChainedOperationTO.getSecurity().setIdSecurityCodePk(idSecurityCode);
							objRegisterChainedOperationTO.getModalityGroup().setIdModalityGroupPk(idModalityGroup);
							objRegisterChainedOperationTO.setMarketDate(marketDate);
							objRegisterChainedOperationTO.setMarketRate(marketRate);
							objRegisterChainedOperationTO.setSettlementDate(settlementDate);
							objRegisterChainedOperationTO.getCurrency().setParameterTablePk(currency);
							objRegisterChainedOperationTO.getIndExtended().setParameterTablePk(indExtended);
							if (ComponentConstant.SALE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setSaleQuantity(stockQuantity);
								objRegisterChainedOperationTO.getSaleSettlementAccountMarketfact().add(settlementAccountMarketfact);
							} else if (ComponentConstant.PURCHARSE_ROLE.equals(role)) {
								objRegisterChainedOperationTO.setPurchaseQuantity(stockQuantity);
								objRegisterChainedOperationTO.getPurchaseSettlementAccountMarketfact().add(settlementAccountMarketfact);
							}
							mpChainedHolderOperation.put(strChainedHolderOperation, objRegisterChainedOperationTO);
						}
					}		
					
					if (!mpChainedHolderOperation.isEmpty()) {
						lstRegisterChainedOperationTO = new ArrayList<RegisterChainedOperationTO>();
						List<String> lstKeyChainedHolderOperation= new ArrayList<String>(mpChainedHolderOperation.keySet());
						for (String keyChainedHolderOperation: lstKeyChainedHolderOperation) {
							RegisterChainedOperationTO objRegisterChainedOperationTO = mpChainedHolderOperation.get(keyChainedHolderOperation);
							configureChainedHolderOperation(objRegisterChainedOperationTO);
							if (objRegisterChainedOperationTO.getPurchaseQuantity().compareTo(BigDecimal.ZERO) > 0 &&
								objRegisterChainedOperationTO.getSaleQuantity().compareTo(BigDecimal.ZERO) > 0) {
								//at this time, we got the correct chain settlement account operations in order to save them
								lstRegisterChainedOperationTO.add(objRegisterChainedOperationTO);
							}
						}
					}
				}				
			}
		}		
		return lstRegisterChainedOperationTO;
	}	
	
	/**
	 * Gets the lst settlement account operation by validate chained.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id security code
	 * @param idHolderAccount the id holder account
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param indExtended the ind extended
	 * @param instrumentType the instrument type
	 * @return the lst settlement account operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getLstSettlementAccountOperationByValidateChained(Date settlementDate, Long idParticipant, String idSecurityCode, Long idHolderAccount, 
												Long idModalityGroup, Integer currency, Integer indExtended, Integer instrumentType)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT MO.securities.idSecurityCodePk, "); 								//0
		stringBuffer.append(" SAO.holderAccountOperation.inchargeStockParticipant.idParticipantPk, ");	//1
		stringBuffer.append(" SAO.holderAccountOperation.holderAccount.idHolderAccountPk, ");			//2
		stringBuffer.append(" SAM.marketDate, ");														//3
		stringBuffer.append(" SAM.marketRate, ");														//4
		stringBuffer.append(" SAM.marketQuantity, ");													//5
		stringBuffer.append(" SAO.stockQuantity, ");													//6
		stringBuffer.append(" SAO.role, ");																//7
		stringBuffer.append(" SAO.idSettlementAccountPk, ");											//8
		stringBuffer.append(" SAM.idSettAccountMarketfactPk, ");										//9
		stringBuffer.append(" MGD.modalityGroup.idModalityGroupPk, ");									//10
		stringBuffer.append(" SAO.settlementOperation.settlementCurrency, ");							//11
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.idNegotiationModalityPk, ");		//12
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.modalityName, ");				//13
		stringBuffer.append(" MO.ballotNumber, ");														//14
		stringBuffer.append(" MO.sequential, ");														//15
		stringBuffer.append(" MO.operationNumber, ");													//16
		stringBuffer.append(" MO.operationDate, ");														//17
		stringBuffer.append(" SAO.settlementOperation.operationState, ");								//18
		stringBuffer.append(" SAO.settlementOperation.operationPart, ");								//19
		stringBuffer.append(" SAO.settlementOperation.idSettlementOperationPk, ");						//20
		stringBuffer.append(" MO.idMechanismOperationPk, ");											//21
		stringBuffer.append(" MO.buyerParticipant.mnemonic, ");											//22
		stringBuffer.append(" MO.sellerParticipant.mnemonic, ");										//23
		stringBuffer.append(" SAO.holderAccountOperation.inchargeStockParticipant.mnemonic ");			//24
		stringBuffer.append(" FROM SettlementAccountOperation SAO, ModalityGroupDetail MGD, MechanismOperation MO, SettlementAccountMarketfact SAM ");
		stringBuffer.append(" WHERE SAO.settlementOperation.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and MO.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = MGD.negotiationMechanism.idNegotiationMechanismPk ");
		stringBuffer.append(" and MO.mechanisnModality.negotiationModality.idNegotiationModalityPk = MGD.negotiationModality.idNegotiationModalityPk ");
		stringBuffer.append(" and SAM.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk ");
		stringBuffer.append(" and SAO.settlementOperation.settlementDate = :settlementDate ");
		stringBuffer.append(" and MO.securities.instrumentType = :instrumentType ");
		stringBuffer.append(" and ( (SAO.stockReference is null or SAO.stockReference = :referentialStock) and SAO.settlementOperation.operationState in (:lstCashOperationState) "
								+ " or (SAO.stockReference in (:lstTermStockReference) and SAO.settlementOperation.operationState = :cashSettledState) ) ");
		//we exclude the operations inside chains
		stringBuffer.append(" and not exists (SELECT HCD FROM HolderChainDetail HCD "
											+ " WHERE HCD.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk "
											+ " and HCD.chainedHolderOperation.chaintState in (:lstChainState) ) ");
		stringBuffer.append(" and SAO.operationState in (:lstSettlementAccountState) ");
		stringBuffer.append(" and SAO.settlementOperation.settlementSchema = :NETSchema ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = SAO.settlementOperation.settlementSchema ");
		stringBuffer.append(" and SAM.indActive = :constantOne ");
		//we exclude the operation inside alive settlement processes
		stringBuffer.append(" and not exists (SELECT OS FROM OperationSettlement OS "
											+ " WHERE OS.settlementOperation.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk "
											+ " and OS.settlementProcess.settlementDate = :settlementDate "
											+ " and OS.settlementProcess.processState in (:lstProcessState) ) ");
		//we exclude the report operations in cash part as buyers
		stringBuffer.append(" and not exists (SELECT SAO_REPO FROM SettlementAccountOperation SAO_REPO "
											+ " WHERE SAO_REPO.settlementOperation.mechanismOperation.indReportingBalance = :constantOne "
											+ " and SAO_REPO.settlementOperation.operationPart = :constantOne "
											+ " and SAO_REPO.role = :constantOne "
											+ " and SAO_REPO.operationState in (:lstSettlementAccountState) "
											+ " and SAO_REPO.idSettlementAccountPk = SAO.idSettlementAccountPk) ");
		if (Validations.validateIsNotNull(idModalityGroup)) {
			stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		}
		if(Validations.validateIsNotNull(indExtended)) {
			stringBuffer.append(" and SAO.settlementOperation.indExtended = :indExtended ");
		}
		if (Validations.validateIsNotNull(currency)) {
			stringBuffer.append(" and SAO.settlementOperation.settlementCurrency = :currency ");
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			stringBuffer.append(" and SAO.holderAccountOperation.inchargeStockParticipant.idParticipantPk = :idParticipant ");
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			stringBuffer.append(" and MO.securities.idSecurityCodePk = :idSecurityCode ");
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			stringBuffer.append(" and SAO.holderAccountOperation.holderAccount.idHolderAccountPk = :idHolderAccount ");
		}
		stringBuffer.append(" ORDER BY MO.securities.idSecurityCodePk, SAO.holderAccountOperation.holderAccount.idHolderAccountPk, "
									+ " SAM.marketDate, SAM.marketRate, SAM.marketQuantity, SAO.role ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("settlementDate", settlementDate);
		query.setParameter("instrumentType", instrumentType);
		query.setParameter("referentialStock", AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		List<Integer> lstCashOperationState= new ArrayList<Integer>();
		lstCashOperationState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstCashOperationState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("lstCashOperationState", lstCashOperationState);
		List<Long> lstTermStockReference= new ArrayList<Long>();
		lstTermStockReference.add(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		lstTermStockReference.add(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		query.setParameter("lstTermStockReference", lstTermStockReference);
		query.setParameter("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		List<Integer> lstChainState= new ArrayList<Integer>();
		lstChainState.add(ChainedOperationStateType.REGISTERED.getCode());
		lstChainState.add(ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("lstChainState", lstChainState);
		List<Integer> lstSettlementAccountState= new ArrayList<Integer>();
		lstSettlementAccountState.add(HolderAccountOperationStateType.REGISTERED.getCode());
		lstSettlementAccountState.add(HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("lstSettlementAccountState", lstSettlementAccountState);
		query.setParameter("NETSchema", SettlementSchemaType.NET.getCode());
		query.setParameter("constantOne", BooleanType.YES.getCode());
		List<Integer> lstProcessState= new ArrayList<Integer>();
		lstProcessState.add(SettlementProcessStateType.IN_PROCESS.getCode());
		lstProcessState.add(SettlementProcessStateType.STARTED.getCode());
		lstProcessState.add(SettlementProcessStateType.WAITING.getCode());
		lstProcessState.add(SettlementProcessStateType.ERROR.getCode());
		query.setParameter("lstProcessState", lstProcessState);
		
		if (Validations.validateIsNotNull(idModalityGroup)) {
			query.setParameter("idModalityGroup", idModalityGroup);
		}
		if(Validations.validateIsNotNull(indExtended)) {
			query.setParameter("indExtended", indExtended);
		}
		if (Validations.validateIsNotNull(currency)) {
			query.setParameter("currency", currency);
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			query.setParameter("idParticipant", idParticipant);
		}
		if (Validations.validateIsNotNull(idSecurityCode)) {
			query.setParameter("idSecurityCode", idSecurityCode);
		}
		if (Validations.validateIsNotNull(idHolderAccount)) {
			query.setParameter("idHolderAccount", idHolderAccount);
		}
		
		return query.getResultList();
	}
}
