package com.pradera.settlements.chainedoperation.facade;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.settlement.ChainedHolderOperation;
import com.pradera.model.settlement.HolderChainDetail;
import com.pradera.settlements.chainedoperation.service.ChainedOperationsServiceBean;
import com.pradera.settlements.chainedoperation.to.ChainedOperationDataTO;
import com.pradera.settlements.chainedoperation.to.RegisterChainedOperationTO;
import com.pradera.settlements.chainedoperation.to.SearchChainedOperationTO;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChainedOperationsFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ChainedOperationsFacade {
	
	/** The chained operations service bean. */
	@EJB
	ChainedOperationsServiceBean chainedOperationsServiceBean;
	
	/** The holder account component service bean. */
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The settlements component singleton. */
	@EJB
	private SettlementsComponentSingleton settlementsComponentSingleton;
	

	/**
	 * Gets the list participants by nego mechanism.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @param idParticipant the id participant
	 * @return the list participants by nego mechanism
	 */
	public List<Participant> getListParticipantsByNegoMechanism(Long idNegotiationMechanismPk, Long idParticipant) {
		return chainedOperationsServiceBean.getListParticipantsByNegoMechanism(idNegotiationMechanismPk, idParticipant);
	}

	/**
	 * Gets the list modality group.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the list modality group
	 */
	public List<ModalityGroup> getListModalityGroup(Long idNegotiationMechanismPk) {
		return chainedOperationsServiceBean.getListModalityGroup(idNegotiationMechanismPk);
	}

	/**
	 * Search chained holder request.
	 *
	 * @param searchChainedOperationTO the search chained operation to
	 * @return the list
	 */
	public List<ChainedHolderOperation> searchChainedHolderRequest(SearchChainedOperationTO searchChainedOperationTO) {
		return chainedOperationsServiceBean.searchChainedHolderRequest(searchChainedOperationTO);
	}
	
	/**
	 * Gets the holder accounts.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder accounts
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccounts(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountComponentServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO );
	}

	/**
	 * Save chained holder operation.
	 *
	 * @param objRegisterChainedOperationTO the obj register chained operation to
	 * @throws ServiceException the service exception
	 */
	public void saveChainedHolderOperation(RegisterChainedOperationTO objRegisterChainedOperationTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        settlementsComponentSingleton.createChainedHolderOperation(objRegisterChainedOperationTO, loggerUser);
	}

	/**
	 * Cancel all chained holder operations.
	 *
	 * @param settlementDate the settlement date
	 * @param indExtended the ind extended
	 * @throws ServiceException the service exception
	 */
	public void cancelAllChainedHolderOperations(Date settlementDate, Integer indExtended) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
        settlementsComponentSingleton.cancelAllChainedHolderOperations(settlementDate, indExtended, loggerUser);
	}
	
	/**
	 * Creates the chained holder operations automatic.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param indExtended the ind extended
	 * @param instrumentType the instrument type
	 * @throws ServiceException the service exception
	 */
	public void createChainedHolderOperationsAutomatic(Date settlementDate, Long idParticipant, Integer indExtended, Integer instrumentType) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        settlementsComponentSingleton.createChainedHolderOperationsAutomatic(settlementDate, idParticipant, indExtended, instrumentType, loggerUser);
	}
	
	/**
	 * Creates the chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @throws ServiceException the service exception
	 */
	public void createChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        settlementsComponentSingleton.createChainedHolderOperation(chainedOperationTO, loggerUser);
	}
	
	/**
	 * Gets the lst settlement account operation to chain.
	 *
	 * @param objRegisterChainedOperationTO the obj register chained operation to
	 * @return the lst settlement account operation to chain
	 */
	public List<ChainedOperationDataTO> getLstSettlementAccountOperationToChain(RegisterChainedOperationTO objRegisterChainedOperationTO)
	{
		return chainedOperationsServiceBean.getLstSettlementAccountOperationToChain(objRegisterChainedOperationTO);
	}
	
	/**
	 * Confirm chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @throws ServiceException the service exception
	 */
	public void confirmChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        settlementsComponentSingleton.confirmChainedHolderOperation(chainedOperationTO, loggerUser);
	}
	
	/**
	 * Confirm chained holder operation Multiple.
	 *
	 * @param lstChainedOperationTO the list chained operation to
	 * @throws ServiceException the service exception
	 */
	public void confirmChainedHolderOperationMultiple(List<RegisterChainedOperationTO> lstChainedOperationTO) throws ServiceException {
		if(Validations.validateListIsNotNullAndNotEmpty(lstChainedOperationTO)){
			for(RegisterChainedOperationTO objRegisterChainedOperationTO : lstChainedOperationTO) {
				confirmChainedHolderOperation(objRegisterChainedOperationTO);
			}
		}
	}
	
	/**
	 * Reject chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @throws ServiceException the service exception
	 */
	public void rejectChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
        settlementsComponentSingleton.rejectChainedHolderOperation(chainedOperationTO, loggerUser);
	}
	
	/**
	 * Reject chained holder operation Multiple.
	 *
	 * @param lstChainedOperationTO the list chained operation to
	 * @throws ServiceException the service exception
	 */
	public void rejectChainedHolderOperationMultiple(List<RegisterChainedOperationTO> lstChainedOperationTO) throws ServiceException {
		if(Validations.validateListIsNotNullAndNotEmpty(lstChainedOperationTO)){
			for(RegisterChainedOperationTO objRegisterChainedOperationTO : lstChainedOperationTO) {
				rejectChainedHolderOperation(objRegisterChainedOperationTO);
			}
		}
	}
	
	/**
	 * Cancel chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @throws ServiceException the service exception
	 */
	public void cancelChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
        settlementsComponentSingleton.cancelChainedHolderOperation(chainedOperationTO, loggerUser);
	}
	
	/**
	 * Cancel chained holder operation Multiple.
	 *
	 * @param lstChainedOperationTO the list chained operation to
	 * @throws ServiceException the service exception
	 */
	public void cancelChainedHolderOperationMultiple(List<RegisterChainedOperationTO> lstChainedOperationTO) throws ServiceException {
		if(Validations.validateListIsNotNullAndNotEmpty(lstChainedOperationTO)){
			for(RegisterChainedOperationTO objRegisterChainedOperationTO : lstChainedOperationTO) {
				cancelChainedHolderOperation(objRegisterChainedOperationTO);
			}
		}
	}
	
	
	/**
	 * Gets the list holder chain detail.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @return the list holder chain detail
	 */
	public List<HolderChainDetail> getListHolderChainDetail(Long idChainedHolderOperation)
	{
		return chainedOperationsServiceBean.getListHolderChainDetail(idChainedHolderOperation);
	}
	
	/**
	 * Gets the chained holder operation.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @return the chained holder operation
	 */
	public ChainedHolderOperation getChainedHolderOperation(Long idChainedHolderOperation)
	{
		return chainedOperationsServiceBean.getChainedHolderOperation(idChainedHolderOperation);
	}
	
	/**
	 * Validate the chained holder operations automatic.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param lstIndExtended the list indicator extended
	 * @param instrumentType the instrument type
	 * @param loggerUser the logger user
	 * @return the Map Register Chained Operation TO
	 * @throws ServiceException the service exception
	 */
	public List<RegisterChainedOperationTO> validateChainedHolderOperations(Date settlementDate, Long idParticipant,  List<Integer> lstIndExtended, 
														Integer instrumentType) throws ServiceException {
		return chainedOperationsServiceBean.validateChainedHolderOperations(settlementDate, idParticipant, lstIndExtended, instrumentType);
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param participantCode the participant code
	 * @return the participant
	 */
	public Participant getParticipant(Long participantCode){
		return chainedOperationsServiceBean.find(Participant.class, participantCode);
	}
}
