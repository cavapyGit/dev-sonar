package com.pradera.settlements.chainedoperation.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ChainedHolderOperation;
import com.pradera.model.settlement.HolderChainDetail;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.settlements.chainedoperation.facade.ChainedOperationsFacade;
import com.pradera.settlements.chainedoperation.to.ChainedOperationDataTO;
import com.pradera.settlements.chainedoperation.to.RegisterChainedOperationTO;
import com.pradera.settlements.chainedoperation.to.SearchChainedOperationTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChainedOperationsMgmBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@LoggerCreateBean
@DepositaryWebBean
public class ChainedOperationsMgmBean extends GenericBaseBean{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The map parameters. */
	private HashMap<Integer, ParameterTable> mapParameters = new HashMap<Integer,ParameterTable>();
	
	/** The search chained operation to. */
	private SearchChainedOperationTO searchChainedOperationTO;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst state. */
	private List<ParameterTable> lstState;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The lst modality group. */
	private List<ModalityGroup> lstModalityGroup;
	
	/** The lst ind extended. */
	private List<ParameterTable> lstIndExtended;
	
	/** The chained operation to. */
	private RegisterChainedOperationTO chainedOperationTO;
	
	/** The is participant user. */
	private boolean isParticipantUser;
	
	/** The bl cancel. */
	private boolean blConfirm, blReject, blCancel;
	
	/** The Constant VIEW_MAPPING. */
	private final static String VIEW_MAPPING = "viewChainedOperation";
	
	/** The Constant REGISTER_MAPPING. */
	private final static String REGISTER_MAPPING = "registerChainedOperation";
	
	/** The chained holder operation. */
	private ChainedHolderOperation chainedHolderOperation;
	
	/** The chained holder array operation . */
	private ChainedHolderOperation [] arrayChainedHolderOperation;
	
	/** The chained operations facade. */
	@EJB
	ChainedOperationsFacade chainedOperationsFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The mcn operation service facade. */
	@EJB
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		searchChainedOperationTO = new SearchChainedOperationTO();
		searchChainedOperationTO.setInitialDate(getCurrentSystemDate());
		searchChainedOperationTO.setEndDate(getCurrentSystemDate());
		searchChainedOperationTO.setHolder(new Holder());
		searchChainedOperationTO.setHolderAccount(new HolderAccount());
		searchChainedOperationTO.setSecurity(new Security());
		NegotiationMechanism objMechanism= mcnOperationServiceFacade.getNegotiationMechanism(NegotiationMechanismType.BOLSA.getCode());
		try {
			fillCombos();
			Long idParticipant= userInfo.getUserAccountSession().getParticipantCode();
			isParticipantUser= userInfo.getUserAccountSession().isParticipantInstitucion();
			if(!isParticipantUser){
				lstParticipants =chainedOperationsFacade.getListParticipantsByNegoMechanism(objMechanism.getIdNegotiationMechanismPk(), idParticipant);
			}else{
				lstParticipants = new ArrayList<Participant>();
				lstParticipants.add(mcnOperationServiceFacade.getParticipantObject(idParticipant));
				searchChainedOperationTO.setIdParticipantPk(idParticipant);
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		showPrivilegeButtons();
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();		
		searchChainedOperationTO.setHolder(new Holder());
		searchChainedOperationTO.setHolderAccount(new HolderAccount());
		searchChainedOperationTO.setSecurity(new Security());
		if(!isParticipantUser){
			searchChainedOperationTO.setIdParticipantPk(null);
		}
		searchChainedOperationTO.setState(null);
		searchChainedOperationTO.setInitialDate(getCurrentSystemDate());
		searchChainedOperationTO.setEndDate(getCurrentSystemDate());
		cleanResult();
		arrayChainedHolderOperation = null;
	}
	
	/**
	 * Clean result.
	 */
	public void cleanResult(){
		searchChainedOperationTO.setBlNoResult(false);
		searchChainedOperationTO.setLstChainedHolderOperation(null);
		chainedHolderOperation = null;
	}
	
	/**
	 * Change participant search.
	 */
	public void changeParticipantSearch(){
		searchChainedOperationTO.setHolder(new Holder());
		searchChainedOperationTO.setHolderAccount(new HolderAccount());
		cleanResult();
	}
	
	/**
	 * Change holder search.
	 */
	public void changeHolderSearch(){
		searchChainedOperationTO.setHolderAccount(new HolderAccount());
	}
	
	/**
	 * Search request.
	 */
	public void searchRequest(){
		try {
			cleanResult();
			searchChainedOperationTO.setBlNoResult(true);
			List<ChainedHolderOperation> lstResult = chainedOperationsFacade.searchChainedHolderRequest(searchChainedOperationTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				searchChainedOperationTO.setBlNoResult(false);
				searchChainedOperationTO.setLstChainedHolderOperation(new GenericDataModel<ChainedHolderOperation>(lstResult));
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * New request.
	 *
	 * @return the string
	 */
	public String newRequest(){
		try {
			chainedOperationTO = new RegisterChainedOperationTO();
			chainedOperationTO.setIndAutomatic(BooleanType.NO.getCode());
			chainedOperationTO.setHolder(new Holder());
			if(isParticipantUser){
				chainedOperationTO.setParticipant(new Participant(searchChainedOperationTO.getIdParticipantPk()));
			}
			NegotiationMechanism objMechanism= mcnOperationServiceFacade.getNegotiationMechanism(NegotiationMechanismType.BOLSA.getCode());
			chainedOperationTO.setMechanism(objMechanism);
			lstModalityGroup= chainedOperationsFacade.getListModalityGroup(chainedOperationTO.getMechanism().getIdNegotiationMechanismPk());
			chainedOperationTO.setModalityGroup(lstModalityGroup.get(0));
			chainedOperationTO.getCurrency().setParameterTablePk(CurrencyType.PYG.getCode());
			return REGISTER_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	
	/**
	 * Onchange participant.
	 */
	public void onchangeParticipant(){
		cleanHolderAccountOperation();
		chainedOperationTO.setHolder(new Holder());
		chainedOperationTO.setHolderAccount(new HolderAccount());
		chainedOperationTO.setLstHolderAccounts(null);
		chainedOperationTO.setSecurity(new Security());
		chainedOperationTO.setCurrency(new ParameterTable());
		chainedOperationTO.setIndExtended(new ParameterTable());
		chainedOperationTO.setModalityGroup(new ModalityGroup());
	}
	
	/**
	 * Validate holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateHolder() throws ServiceException{
		chainedOperationTO.setHolderAccount(null);
		
		if(chainedOperationTO.getHolder().getIdHolderPk()!=null){
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(chainedOperationTO.getParticipant().getIdParticipantPk());
			holderAccountTO.setIdHolderPk(chainedOperationTO.getHolder().getIdHolderPk());
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.TRUE);
			holderAccountTO.setNeedBanks(Boolean.FALSE);
			List<HolderAccount> accountList = chainedOperationsFacade.getHolderAccounts(holderAccountTO);
			if(accountList.size() > 0 ){
				chainedOperationTO.setLstHolderAccounts(accountList);
			}else{
				return;
			}
		}
	}
	
	/**
	 * Validate holder account.
	 */
	public void validateHolderAccount(){
		cleanHolderAccountOperation();
	}
	
	
	/**
	 * Clean holder account operation.
	 */
	public void cleanHolderAccountOperation(){
		chainedOperationTO.setLstChainedOperationDataTO(null);
//		chainedOperationTO.setSaleSettlementAccountOperation(null);
//		chainedOperationTO.setPurchaseSettlementAccountOperation(null);
		chainedOperationTO.setSaleSettlementAccountMarketfact(null);
		chainedOperationTO.setPurchaseSettlementAccountMarketfact(null);
	}
	
	/**
	 * Search holder account operation.
	 */
	public void searchHolderAccountOperation(){
		chainedOperationTO.setLstChainedOperationDataTO(null);
//		chainedOperationTO.setSaleSettlementAccountOperation(new LinkedList<SettlementAccountOperation>());
//		chainedOperationTO.setPurchaseSettlementAccountOperation(new LinkedList<SettlementAccountOperation>());
		chainedOperationTO.setSaleSettlementAccountMarketfact(new LinkedList<SettlementAccountMarketfact>());
		chainedOperationTO.setPurchaseSettlementAccountMarketfact(new LinkedList<SettlementAccountMarketfact>());
		chainedOperationTO.setSalePN(BigDecimal.ZERO);
		chainedOperationTO.setPurchasePN(BigDecimal.ZERO);
		chainedOperationTO.setSaleQuantity(BigDecimal.ZERO);
		chainedOperationTO.setPurchaseQuantity(BigDecimal.ZERO);
		try {
			if(Validations.validateIsNull(chainedOperationTO.getHolderAccount())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			List<ChainedOperationDataTO> lstChainedOperationDataTO= chainedOperationsFacade.getLstSettlementAccountOperationToChain(chainedOperationTO);
			chainedOperationTO.setLstChainedOperationDataTO(new  GenericDataModel<>(lstChainedOperationDataTO));
		} catch (Exception e) { 
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);		
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnCancel(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.CHAIN_OPERATION_STATE.getCode());
		lstState = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		parameterTableTO.setIndicator2(GeneralConstants.ONE_VALUE_STRING);
		lstCurrency =  generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		lstIndExtended= new LinkedList<ParameterTable>();
		ParameterTable parameterExtendedNO= new ParameterTable();
		parameterExtendedNO.setParameterTablePk(BooleanType.NO.getCode());
		parameterExtendedNO.setParameterName(BooleanType.NO.getValue());
		lstIndExtended.add(parameterExtendedNO);
		ParameterTable parameterExtendedYes= new ParameterTable();
		parameterExtendedYes.setParameterTablePk(BooleanType.YES.getCode());
		parameterExtendedYes.setParameterName(BooleanType.YES.getValue());
		lstIndExtended.add(parameterExtendedYes);
		
		ParameterTableTO paramTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
			masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());
			masterPks.add(MasterTableType.HOLDER_STATE_TYPE.getCode());
			masterPks.add(MasterTableType.CURRENCY.getCode());
			masterPks.add(MasterTableType.CHAIN_OPERATION_STATE.getCode());
			masterPks.add(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		paramTableTO.setLstMasterTableFk(masterPks);
		
		List<ParameterTable> lst = generalParametersFacade.getListParameterTableServiceBean(paramTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt);
		}
	}
	
	/**
	 * Gets the parameter.
	 *
	 * @param parameterPk the parameter pk
	 * @return the parameter
	 */
	public ParameterTable getParameter(Integer parameterPk){
		return mapParameters.get(parameterPk);
	}
	
	/**
	 * Before save chain.
	 *
	 * @throws ServiceException the service exception
	 */
	public void beforeSaveChain() throws ServiceException
	{	
		if (Validations.validateListIsNotNullAndNotEmpty(chainedOperationTO.getSaleSettlementAccountMarketfact())) {
			if (Validations.validateListIsNotNullAndNotEmpty(chainedOperationTO.getPurchaseSettlementAccountMarketfact())) {
				BigDecimal normalPartQuantity= chainedOperationTO.getSaleQuantity().subtract(chainedOperationTO.getPurchaseQuantity());
				if (normalPartQuantity.compareTo(BigDecimal.ZERO) > 0) {
					//sale normal part 
					for (SettlementAccountMarketfact objSettlementAccountMarketfact: chainedOperationTO.getSaleSettlementAccountMarketfact()) {
						//we verify if exists any sale quantity greater than normal part
						if (normalPartQuantity.compareTo(objSettlementAccountMarketfact.getMarketQuantity()) > 0 ) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_SALE_NP_GREATER,
										new Object[]{objSettlementAccountMarketfact.getSettlementAccountOperation().getSettlementOperation().
													getMechanismOperation().getOperationNumberBallotSequential()}));
							JSFUtilities.showSimpleValidationDialog();
							return;
						}
					}
				} else if (normalPartQuantity.compareTo(BigDecimal.ZERO) < 0) {
					//purchase normal part
					normalPartQuantity= normalPartQuantity.abs();
					for (SettlementAccountMarketfact objSettlementAccountMarketfact: chainedOperationTO.getPurchaseSettlementAccountMarketfact()) {
						//we verify if exists any purchase quantity greater than normal part
						if (normalPartQuantity.compareTo(objSettlementAccountMarketfact.getMarketQuantity()) > 0 ) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_PURCHASE_NP_GREATER,
										new Object[]{objSettlementAccountMarketfact.getSettlementAccountOperation().getSettlementOperation().
													getMechanismOperation().getOperationNumberBallotSequential()}));
							JSFUtilities.showSimpleValidationDialog();
							return;
						}
					}
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_PURCHASE_OPERATION_NO_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_SALE_OPERATION_NO_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		//if the chain has NORMAL PART, then we get the fewer settlement account to update its chained quantity according the normal part quantity
		if (chainedOperationTO.getSalePN().compareTo(BigDecimal.ZERO) > 0) { 
			SettlementAccountMarketfact objSettlementAccountMarketfact= chainedOperationTO.getSaleSettlementAccountMarketfact().get(0);
			objSettlementAccountMarketfact.setChainedQuantity(objSettlementAccountMarketfact.getChainedQuantity().subtract(chainedOperationTO.getSalePN()));
		} else if (chainedOperationTO.getPurchasePN().compareTo(BigDecimal.ZERO) > 0) {
			SettlementAccountMarketfact objSettlementAccountMarketfact= chainedOperationTO.getPurchaseSettlementAccountMarketfact().get(0);
			objSettlementAccountMarketfact.setChainedQuantity(objSettlementAccountMarketfact.getChainedQuantity().subtract(chainedOperationTO.getPurchasePN()));
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_REGISTER_CONFIRM,
								new Object[]{chainedOperationTO.getHolderAccount().getAccountNumber()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Save chain.
	 */
	@LoggerAuditWeb
	public void saveChain()
	{
		try {
			chainedOperationsFacade.saveChainedHolderOperation(chainedOperationTO);	
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
													 PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_REGISTER_SUCCESS));
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				return;
			}
		}
	}
	
	/**
	 * Clean registry form.
	 */
	public void cleanRegistryForm(){
		JSFUtilities.resetViewRoot();
		chainedOperationTO = new RegisterChainedOperationTO();
		chainedOperationTO.setSettlementDate(getCurrentSystemDate());
		chainedOperationTO.setHolder(new Holder());
		chainedOperationTO.setSecurity(new Security());
		if(isParticipantUser){
			chainedOperationTO.setParticipant(new Participant(searchChainedOperationTO.getIdParticipantPk()));
		}
	}
	
	
	/**
	 * Before generate chains.
	 */
	public void beforeGenerateChains() {
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
							PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_GENERATE_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Generate chained operations.
	 */
	@LoggerAuditWeb
	public void generateChainedOperations() 
	{
		BusinessProcess objBusinessProcess= new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GENERATE_CHAIN_OPERATION.getCode());
		Map<String, Object> parameters= new HashMap<String, Object>();
		parameters.put(SettlementConstant.ID_PARTICIPANT, searchChainedOperationTO.getIdParticipantPk());
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, parameters);
		} catch (Exception e) {
			e.printStackTrace();
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_GENERATE_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Show chain detail.
	 *
	 * @param objChainedHolderOperation the obj chained holder operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String showChainDetail(ChainedHolderOperation objChainedHolderOperation) throws ServiceException
	{
		this.chainedHolderOperation = objChainedHolderOperation;
		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		return loadChainedHolderOperation(chainedHolderOperation);
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateConfirm() throws ServiceException{
		return validateAction(ViewOperationsType.CONFIRM.getCode());
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateReject() throws ServiceException{
		return validateAction(ViewOperationsType.REJECT.getCode());
	}
	
	/**
	 * Validate cancel.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateCancel() throws ServiceException{
		return validateAction(ViewOperationsType.CANCEL.getCode());
	}
	
	/**
	 * Validate action.
	 *
	 * @param action the action
	 * @return the string
	 */
	public String validateAction(Integer action){

		executeAction();
		
		try {
			
			// Add multiple selection
			
			if(arrayChainedHolderOperation != null && arrayChainedHolderOperation.length > 0){
				
				if(arrayChainedHolderOperation.length == GeneralConstants.ONE_VALUE_INTEGER){
					
					chainedHolderOperation = arrayChainedHolderOperation[0];
					
					if (ViewOperationsType.CANCEL.getCode().equals(action)) {
						if(!ChainedOperationStateType.CONFIRMED.getCode().equals(chainedHolderOperation.getChaintState())){			
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_NO_CONFIRMED));	
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					} else if(!ChainedOperationStateType.REGISTERED.getCode().equals(chainedHolderOperation.getChaintState())){			
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_NO_REGISTERED));	
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}					
					setViewOperationType(action);					
					return loadChainedHolderOperation(chainedHolderOperation);
					
				} else {
					setViewOperationType(action);
					String strChainedNumber = GeneralConstants.EMPTY_STRING;					
					for(ChainedHolderOperation objChainedHolderOperation : arrayChainedHolderOperation) {						
						if (ViewOperationsType.CANCEL.getCode().equals(action)) {
							if(!ChainedOperationStateType.CONFIRMED.getCode().equals(objChainedHolderOperation.getChaintState())){			
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_NO_CONFIRMED));	
								JSFUtilities.showSimpleValidationDialog();
								return null;
							}
						} else if(!ChainedOperationStateType.REGISTERED.getCode().equals(objChainedHolderOperation.getChaintState())){			
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_NO_REGISTERED));	
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}						
						if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strChainedNumber)) {
							strChainedNumber += objChainedHolderOperation.getIdChainedHolderOperationPk();
						} else {
							strChainedNumber += GeneralConstants.STR_COMMA + objChainedHolderOperation.getIdChainedHolderOperationPk();
						}						
					}
					
					if(ViewOperationsType.REJECT.getCode().equals(action)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_OPERATION_REJECT_MULTIPLE, new Object[]{strChainedNumber}));
					} else if(ViewOperationsType.CONFIRM.getCode().equals(action)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_OPERATION_CONFIRM_MULTIPLE, new Object[]{strChainedNumber}));
					} else if (ViewOperationsType.CANCEL.getCode().equals(action)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_OPERATION_CANCEL_MULTIPLE, new Object[]{strChainedNumber}));
					}
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogMul').show();");					
				}
				
			} else {
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
				 JSFUtilities.showSimpleValidationDialog();
				 return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}								
		return null;
	}
	
	
	/**
	 * Load chained holder operation.
	 *
	 * @param objChainedHolderOperation the obj chained holder operation
	 * @return the string
	 */
	private String loadChainedHolderOperation(ChainedHolderOperation objChainedHolderOperation)
	{
		chainedOperationTO= new RegisterChainedOperationTO();
		objChainedHolderOperation= chainedOperationsFacade.getChainedHolderOperation(objChainedHolderOperation.getIdChainedHolderOperationPk());
		chainedOperationTO.setIdChainedHolderOperation(objChainedHolderOperation.getIdChainedHolderOperationPk());
		chainedOperationTO.setParticipant(objChainedHolderOperation.getParticipant());
		chainedOperationTO.setSecurity(objChainedHolderOperation.getSecurity());
		chainedOperationTO.setHolderAccount(objChainedHolderOperation.getHolderAccount());
		chainedOperationTO.setModalityGroup(objChainedHolderOperation.getModalityGroup());
		chainedOperationTO.getCurrency().setParameterTablePk(objChainedHolderOperation.getCurrency());
		chainedOperationTO.getCurrency().setParameterName(CurrencyType.get(objChainedHolderOperation.getCurrency()).getCodeIso());
		chainedOperationTO.getChainState().setParameterTablePk(objChainedHolderOperation.getChaintState());
		chainedOperationTO.getChainState().setParameterName(ChainedOperationStateType.get(objChainedHolderOperation.getChaintState()).getDescription());
		chainedOperationTO.setIdNegotiationMechanismPk(objChainedHolderOperation.getModalityGroup().getNegotiationMechanism().getIdNegotiationMechanismPk());
		chainedOperationTO.setMarketDate(objChainedHolderOperation.getMarketDate());
		chainedOperationTO.setMarketRate(objChainedHolderOperation.getMarketRate());
		chainedOperationTO.setPurchaseQuantity(objChainedHolderOperation.getPurchaseQuantity());
		chainedOperationTO.setSaleQuantity(objChainedHolderOperation.getSaleQuantity());
		chainedOperationTO.setSettlementDate(objChainedHolderOperation.getSettlementDate());
		chainedOperationTO.getIndExtended().setParameterTablePk(objChainedHolderOperation.getIndExtended());
		chainedOperationTO.getIndExtended().setParameterName(BooleanType.get(objChainedHolderOperation.getIndExtended()).getValue());
		
		ChainedOperationDataTO objChainedOperationDataTO= new ChainedOperationDataTO();
		objChainedOperationDataTO.setMarketDate(objChainedHolderOperation.getMarketDate());
		objChainedOperationDataTO.setMarketRate(objChainedHolderOperation.getMarketRate());
		//List<SettlementAccountOperation> lstSettlementAccountOperation = new ArrayList<SettlementAccountOperation>();
		List<SettlementAccountMarketfact> lstSettlementAccountMarketfact = new ArrayList<SettlementAccountMarketfact>();
		List<HolderChainDetail> lstHolderChainDetail= chainedOperationsFacade.getListHolderChainDetail(objChainedHolderOperation.getIdChainedHolderOperationPk());
		
		Participant buyer = null , seller = null;
		for (HolderChainDetail objHolderChainDetail: lstHolderChainDetail) {
			buyer  = objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getBuyerParticipant();
			seller = objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getSellerParticipant();
						 
			objHolderChainDetail.getSettlementAccountMarketfact().setChainedQuantity(objHolderChainDetail.getChainedQuantity());
			objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation()
																		.setOperationPartDescription(OperationPartType.get(objHolderChainDetail
																		.getSettlementAccountMarketfact().getSettlementAccountOperation()
																		.getSettlementOperation().getOperationPart()).getDescription());
			objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().setDesRole(ParticipantRoleType.get(
																objHolderChainDetail.getSettlementAccountMarketfact().
																getSettlementAccountOperation().getRole()).getValue());
			objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setStateDescription(
																MechanismOperationStateType.get(objHolderChainDetail.getSettlementAccountMarketfact().
																		getSettlementAccountOperation().
																		getSettlementOperation().getMechanismOperation().
																		getOperationState()).getValue());
			if(objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getOperationPart().equals(OperationPartType.TERM_PART.getCode())){
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setBuyerParticipant(seller);
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setSellerParticipant(buyer);
			}else{
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setBuyerParticipant(buyer);
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setSellerParticipant(seller);
			}
			//lstSettlementAccountOperation.add(objHolderChainDetail.getSettlementAccountOperation());
			lstSettlementAccountMarketfact.add(objHolderChainDetail.getSettlementAccountMarketfact());
		}
		//objChainedOperationDataTO.setLstSettlementAccountOperation(lstSettlementAccountOperation);
		objChainedOperationDataTO.setLstSettlementAccountMarketfact(lstSettlementAccountMarketfact);
		List<ChainedOperationDataTO> lstChainedOperationDataTO= new ArrayList<ChainedOperationDataTO>();
		lstChainedOperationDataTO.add(objChainedOperationDataTO);
		chainedOperationTO.setLstChainedOperationDataTO(new  GenericDataModel<>(lstChainedOperationDataTO));
		
		return VIEW_MAPPING;
	}
	
	
	/**
	 * Clean tab market fact.
	 *
	 * @param chainedOperationData the chained operation data
	 */
	public void cleanTabMarketFact(ChainedOperationDataTO chainedOperationData)
	{
		//we changed of market fact tab
//		chainedOperationTO.setSaleSettlementAccountOperation(new LinkedList<SettlementAccountOperation>());
//		chainedOperationTO.setPurchaseSettlementAccountOperation(new LinkedList<SettlementAccountOperation>());
		chainedOperationTO.setSaleSettlementAccountMarketfact(new LinkedList<SettlementAccountMarketfact>());
		chainedOperationTO.setPurchaseSettlementAccountMarketfact(new LinkedList<SettlementAccountMarketfact>());
		chainedOperationTO.setSaleQuantity(BigDecimal.ZERO);
		chainedOperationTO.setPurchaseQuantity(BigDecimal.ZERO);
		chainedOperationTO.setSalePN(BigDecimal.ZERO);
		chainedOperationTO.setPurchasePN(BigDecimal.ZERO);
		for (ChainedOperationDataTO objChainedOperationDataTO: chainedOperationTO.getLstChainedOperationDataTO()) {
			objChainedOperationDataTO.setSelected(false);
			for (SettlementAccountMarketfact settlementAccountMarketfact: objChainedOperationDataTO.getLstSettlementAccountMarketfact()) {
				//objSettlementAccountOperation.setSelected(false);
				settlementAccountMarketfact.setSelected(false);
			}
		}
		
		//chainedOperationData.setSelected(true);
	}
	
	/**
	 * Validate chain position.
	 *
	 * @param objSettlementAccountMarketfact the obj settlement account marketfact
	 * @param chainedOperationData the chained operation data
	 */
	public void validateChainPosition(SettlementAccountMarketfact objSettlementAccountMarketfact, ChainedOperationDataTO chainedOperationData)
	{
		chainedOperationTO.setMarketDate(chainedOperationData.getMarketDate());
		chainedOperationTO.setMarketRate(chainedOperationData.getMarketRate());
		
		for (ChainedOperationDataTO chainedOperationDataTO: chainedOperationTO.getLstChainedOperationDataTO().getDataList()) {
			if (!chainedOperationTO.getMarketDate().equals(chainedOperationDataTO.getMarketDate()) ||
				chainedOperationTO.getMarketRate().compareTo(chainedOperationDataTO.getMarketRate()) != 0 ) 
			{
				for (SettlementAccountMarketfact settlementAccountMarketfact: chainedOperationDataTO.getLstSettlementAccountMarketfact()) {
					if (settlementAccountMarketfact.isSelected() && 
						!settlementAccountMarketfact.getIdSettAccountMarketfactPk().equals(objSettlementAccountMarketfact.getIdSettAccountMarketfactPk())) 
					{
						settlementAccountMarketfact.setSelected(false);
						updateChainPosition(settlementAccountMarketfact);
					}
				}
			}
		}
		updateChainPosition(objSettlementAccountMarketfact);
	}
	
	/**
	 * Update chain position.
	 *
	 * @param objSettlementAccountMarketfact the obj settlement account marketfact
	 */
	private void updateChainPosition(SettlementAccountMarketfact objSettlementAccountMarketfact) {
		if (objSettlementAccountMarketfact.isSelected()) {
			if (ComponentConstant.SALE_ROLE.equals(objSettlementAccountMarketfact.getSettlementAccountOperation().getRole())) {
				chainedOperationTO.getSaleSettlementAccountMarketfact().add(objSettlementAccountMarketfact);
				chainedOperationTO.setSaleQuantity(chainedOperationTO.getSaleQuantity().add(objSettlementAccountMarketfact.getMarketQuantity()));
				chainedOperationTO.setSalePN(chainedOperationTO.getSalePN().add(objSettlementAccountMarketfact.getMarketQuantity()));
			} else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementAccountMarketfact.getSettlementAccountOperation().getRole())) {
				chainedOperationTO.getPurchaseSettlementAccountMarketfact().add(objSettlementAccountMarketfact);
				chainedOperationTO.setPurchasePN(chainedOperationTO.getPurchaseQuantity().add(objSettlementAccountMarketfact.getMarketQuantity()));
				chainedOperationTO.setPurchaseQuantity(chainedOperationTO.getPurchaseQuantity().add(objSettlementAccountMarketfact.getMarketQuantity()));
			}
		} else {
			if (ComponentConstant.SALE_ROLE.equals(objSettlementAccountMarketfact.getSettlementAccountOperation().getRole())) {
				if (Validations.validateListIsNotNullAndNotEmpty(chainedOperationTO.getSaleSettlementAccountMarketfact())) {
					chainedOperationTO.getSaleSettlementAccountMarketfact().remove(objSettlementAccountMarketfact);
					chainedOperationTO.setSaleQuantity(chainedOperationTO.getSaleQuantity().subtract(objSettlementAccountMarketfact.getMarketQuantity()));
					chainedOperationTO.setSalePN(chainedOperationTO.getSalePN().subtract(objSettlementAccountMarketfact.getMarketQuantity()));
				}
			} else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementAccountMarketfact.getSettlementAccountOperation().getRole())) {
				if (Validations.validateListIsNotNullAndNotEmpty(chainedOperationTO.getPurchaseSettlementAccountMarketfact())) {
					chainedOperationTO.getPurchaseSettlementAccountMarketfact().remove(objSettlementAccountMarketfact);
					chainedOperationTO.setPurchaseQuantity(chainedOperationTO.getPurchaseQuantity().subtract(objSettlementAccountMarketfact.getMarketQuantity()));
					chainedOperationTO.setPurchasePN(chainedOperationTO.getPurchasePN().subtract(objSettlementAccountMarketfact.getMarketQuantity()));
				}
			}
		}
		BigDecimal normalPartQuantity= chainedOperationTO.getSaleQuantity().subtract(chainedOperationTO.getPurchaseQuantity());
		if (normalPartQuantity.compareTo(BigDecimal.ZERO) > 0) {
			chainedOperationTO.setSalePN(normalPartQuantity);
			chainedOperationTO.setPurchasePN(BigDecimal.ZERO);
		} else {
			chainedOperationTO.setPurchasePN(normalPartQuantity.abs());
			chainedOperationTO.setSalePN(BigDecimal.ZERO);
		}
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_OPERATION_REJECT));
		} else if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_OPERATION_CONFIRM));
		} else if (getViewOperationType().equals(ViewOperationsType.CANCEL.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_OPERATION_CANCEL));
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		try{
			if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
				rejectChainedHolderOperation();
			} else if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
				confirmChainedHolderOperation();
			} else if(getViewOperationType().equals(ViewOperationsType.CANCEL.getCode())){
				cancelChainedHolderOperation();
			}
			searchRequest();
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Do action Multiple.
	 */
	@LoggerAuditWeb
	public void doActionMultiple(){
		try{
			List<RegisterChainedOperationTO> lstRegisterChainedOperationTO = null;
			if(arrayChainedHolderOperation != null && arrayChainedHolderOperation.length > 0){
				lstRegisterChainedOperationTO = new ArrayList<RegisterChainedOperationTO>();
				for(ChainedHolderOperation objChainedHolderOperation : arrayChainedHolderOperation) {
					lstRegisterChainedOperationTO.add(populateRegisterChainedOperationTO(objChainedHolderOperation));					
				}				
				if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
					rejectChainedHolderOperationMultiple(lstRegisterChainedOperationTO);
				} else if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
					confirmChainedHolderOperationMultiple(lstRegisterChainedOperationTO);
				} else if(getViewOperationType().equals(ViewOperationsType.CANCEL.getCode())){
					cancelChainedHolderOperationMultiple(lstRegisterChainedOperationTO);
				}					
			}									
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Populate Register Chained Operation TO.
	 *
	 * @param objChainedHolderOperation the object chained holder operation
	 * @return the RegisterChainedOperationTO
	 */
	private RegisterChainedOperationTO populateRegisterChainedOperationTO(ChainedHolderOperation objChainedHolderOperation) {
		RegisterChainedOperationTO chainedOperationTO = new RegisterChainedOperationTO();
		objChainedHolderOperation= chainedOperationsFacade.getChainedHolderOperation(objChainedHolderOperation.getIdChainedHolderOperationPk());
		chainedOperationTO.setIdChainedHolderOperation(objChainedHolderOperation.getIdChainedHolderOperationPk());
		chainedOperationTO.setParticipant(objChainedHolderOperation.getParticipant());
		chainedOperationTO.setSecurity(objChainedHolderOperation.getSecurity());
		chainedOperationTO.setHolderAccount(objChainedHolderOperation.getHolderAccount());
		chainedOperationTO.setModalityGroup(objChainedHolderOperation.getModalityGroup());
		chainedOperationTO.getCurrency().setParameterTablePk(objChainedHolderOperation.getCurrency());
		chainedOperationTO.getCurrency().setParameterName(CurrencyType.get(objChainedHolderOperation.getCurrency()).getCodeIso());
		chainedOperationTO.getChainState().setParameterTablePk(objChainedHolderOperation.getChaintState());
		chainedOperationTO.getChainState().setParameterName(ChainedOperationStateType.get(objChainedHolderOperation.getChaintState()).getDescription());
		chainedOperationTO.setIdNegotiationMechanismPk(objChainedHolderOperation.getModalityGroup().getNegotiationMechanism().getIdNegotiationMechanismPk());
		chainedOperationTO.setMarketDate(objChainedHolderOperation.getMarketDate());
		chainedOperationTO.setMarketRate(objChainedHolderOperation.getMarketRate());
		chainedOperationTO.setPurchaseQuantity(objChainedHolderOperation.getPurchaseQuantity());
		chainedOperationTO.setSaleQuantity(objChainedHolderOperation.getSaleQuantity());
		chainedOperationTO.setSettlementDate(objChainedHolderOperation.getSettlementDate());
		chainedOperationTO.getIndExtended().setParameterTablePk(objChainedHolderOperation.getIndExtended());
		chainedOperationTO.getIndExtended().setParameterName(BooleanType.get(objChainedHolderOperation.getIndExtended()).getValue());
		
		ChainedOperationDataTO objChainedOperationDataTO= new ChainedOperationDataTO();
		objChainedOperationDataTO.setMarketDate(objChainedHolderOperation.getMarketDate());
		objChainedOperationDataTO.setMarketRate(objChainedHolderOperation.getMarketRate());
		//List<SettlementAccountOperation> lstSettlementAccountOperation = new ArrayList<SettlementAccountOperation>();
		List<SettlementAccountMarketfact> lstSettlementAccountMarketfact = new ArrayList<SettlementAccountMarketfact>();
		List<HolderChainDetail> lstHolderChainDetail= chainedOperationsFacade.getListHolderChainDetail(objChainedHolderOperation.getIdChainedHolderOperationPk());
		
		Participant buyer = null , seller = null;
		for (HolderChainDetail objHolderChainDetail: lstHolderChainDetail) {
			buyer  = objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getBuyerParticipant();
			seller = objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getSellerParticipant();
						 
			objHolderChainDetail.getSettlementAccountMarketfact().setChainedQuantity(objHolderChainDetail.getChainedQuantity());
			objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation()
																		.setOperationPartDescription(OperationPartType.get(objHolderChainDetail
																		.getSettlementAccountMarketfact().getSettlementAccountOperation()
																		.getSettlementOperation().getOperationPart()).getDescription());
			objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().setDesRole(ParticipantRoleType.get(
																objHolderChainDetail.getSettlementAccountMarketfact().
																getSettlementAccountOperation().getRole()).getValue());
			objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setStateDescription(
																MechanismOperationStateType.get(objHolderChainDetail.getSettlementAccountMarketfact().
																		getSettlementAccountOperation().
																		getSettlementOperation().getMechanismOperation().
																		getOperationState()).getValue());
			if(objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getOperationPart().equals(OperationPartType.TERM_PART.getCode())){
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setBuyerParticipant(seller);
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setSellerParticipant(buyer);
			}else{
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setBuyerParticipant(buyer);
				objHolderChainDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().setSellerParticipant(seller);
			}
			//lstSettlementAccountOperation.add(objHolderChainDetail.getSettlementAccountOperation());
			lstSettlementAccountMarketfact.add(objHolderChainDetail.getSettlementAccountMarketfact());
		}
		//objChainedOperationDataTO.setLstSettlementAccountOperation(lstSettlementAccountOperation);
		objChainedOperationDataTO.setLstSettlementAccountMarketfact(lstSettlementAccountMarketfact);
		List<ChainedOperationDataTO> lstChainedOperationDataTO= new ArrayList<ChainedOperationDataTO>();
		lstChainedOperationDataTO.add(objChainedOperationDataTO);
		chainedOperationTO.setLstChainedOperationDataTO(new  GenericDataModel<>(lstChainedOperationDataTO));
		
		return chainedOperationTO;
	}
	
	
	/**
	 * Confirm chained holder operation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void confirmChainedHolderOperation() throws ServiceException
	{
		chainedOperationsFacade.confirmChainedHolderOperation(chainedOperationTO);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_CONFIRM_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Confirm chained holder operation Multiple.
	 *
	 * @throws ServiceException the service exception
	 */
	public void confirmChainedHolderOperationMultiple(List<RegisterChainedOperationTO> lstChainedOperationTO) throws ServiceException {
		chainedOperationsFacade.confirmChainedHolderOperationMultiple(lstChainedOperationTO);
		searchRequest();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_CONFIRM_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Reject chained holder operation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void rejectChainedHolderOperation() throws ServiceException
	{
		chainedOperationsFacade.rejectChainedHolderOperation(chainedOperationTO);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_REJECT_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Reject chained holder operation Multiple.
	 *
	 * @throws ServiceException the service exception
	 */
	public void rejectChainedHolderOperationMultiple(List<RegisterChainedOperationTO> lstChainedOperationTO) throws ServiceException {
		chainedOperationsFacade.rejectChainedHolderOperationMultiple(lstChainedOperationTO);
		searchRequest();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_REJECT_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Cancel chained holder operation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cancelChainedHolderOperation() throws ServiceException
	{
		chainedOperationsFacade.cancelChainedHolderOperation(chainedOperationTO);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_CANCEL_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Cancel chained holder operation Multiple.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cancelChainedHolderOperationMultiple(List<RegisterChainedOperationTO> lstChainedOperationTO) throws ServiceException {
		chainedOperationsFacade.cancelChainedHolderOperationMultiple(lstChainedOperationTO);
		searchRequest();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CHAINS_CANCEL_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Gets the search chained operation to.
	 *
	 * @return the search chained operation to
	 */
	public SearchChainedOperationTO getSearchChainedOperationTO() {
		return searchChainedOperationTO;
	}
	
	/**
	 * Sets the search chained operation to.
	 *
	 * @param searchChainedOperationTO the new search chained operation to
	 */
	public void setSearchChainedOperationTO(
			SearchChainedOperationTO searchChainedOperationTO) {
		this.searchChainedOperationTO = searchChainedOperationTO;
	}
	
	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}
	
	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}
	
	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}
	
	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}

	/**
	 * Gets the chained operation to.
	 *
	 * @return the chained operation to
	 */
	public RegisterChainedOperationTO getChainedOperationTO() {
		return chainedOperationTO;
	}

	/**
	 * Sets the chained operation to.
	 *
	 * @param chainedOperationTO the new chained operation to
	 */
	public void setChainedOperationTO(RegisterChainedOperationTO chainedOperationTO) {
		this.chainedOperationTO = chainedOperationTO;
	}

	/**
	 * Gets the chained holder operation.
	 *
	 * @return the chained holder operation
	 */
	public ChainedHolderOperation getChainedHolderOperation() {
		return chainedHolderOperation;
	}

	/**
	 * Sets the chained holder operation.
	 *
	 * @param chainedHolderOperation the new chained holder operation
	 */
	public void setChainedHolderOperation(
			ChainedHolderOperation chainedHolderOperation) {
		this.chainedHolderOperation = chainedHolderOperation;
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}

	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}

	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	/**
	 * Checks if is participant user.
	 *
	 * @return true, if is participant user
	 */
	public boolean isParticipantUser() {
		return isParticipantUser;
	}

	/**
	 * Sets the participant user.
	 *
	 * @param isParticipantUser the new participant user
	 */
	public void setParticipantUser(boolean isParticipantUser) {
		this.isParticipantUser = isParticipantUser;
	}

	/**
	 * Checks if is bl cancel.
	 *
	 * @return true, if is bl cancel
	 */
	public boolean isBlCancel() {
		return blCancel;
	}

	/**
	 * Sets the bl cancel.
	 *
	 * @param blCancel the new bl cancel
	 */
	public void setBlCancel(boolean blCancel) {
		this.blCancel = blCancel;
	}

	/**
	 * Gets the lst ind extended.
	 *
	 * @return the lst ind extended
	 */
	public List<ParameterTable> getLstIndExtended() {
		return lstIndExtended;
	}

	/**
	 * Sets the lst ind extended.
	 *
	 * @param lstIndExtended the new lst ind extended
	 */
	public void setLstIndExtended(List<ParameterTable> lstIndExtended) {
		this.lstIndExtended = lstIndExtended;
	}

	/**
	 * @return the arrayChainedHolderOperation
	 */
	public ChainedHolderOperation[] getArrayChainedHolderOperation() {
		return arrayChainedHolderOperation;
	}

	/**
	 * @param arrayChainedHolderOperation the arrayChainedHolderOperation to set
	 */
	public void setArrayChainedHolderOperation(
			ChainedHolderOperation[] arrayChainedHolderOperation) {
		this.arrayChainedHolderOperation = arrayChainedHolderOperation;
	}
	
}
