package com.pradera.settlements.chainedoperation.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.settlement.ChainedHolderOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchChainedOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchChainedOperationTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The holder. */
	private Holder holder;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The security. */
	private Security security;
	
	/** The obj mechanism. */
	private NegotiationMechanism objMechanism;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The state. */
	private Integer state;
	
	/** The lst chained holder operation. */
	private GenericDataModel<ChainedHolderOperation> lstChainedHolderOperation;
	
	/** The bl no result. */
	private boolean blNoResult;

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the lst chained holder operation.
	 *
	 * @return the lst chained holder operation
	 */
	public GenericDataModel<ChainedHolderOperation> getLstChainedHolderOperation() {
		return lstChainedHolderOperation;
	}
	
	/**
	 * Sets the lst chained holder operation.
	 *
	 * @param lstChainedHolderOperation the new lst chained holder operation
	 */
	public void setLstChainedHolderOperation(
			GenericDataModel<ChainedHolderOperation> lstChainedHolderOperation) {
		this.lstChainedHolderOperation = lstChainedHolderOperation;
	}
	
	/**
	 * Gets the obj mechanism.
	 *
	 * @return the obj mechanism
	 */
	public NegotiationMechanism getObjMechanism() {
		return objMechanism;
	}
	
	/**
	 * Sets the obj mechanism.
	 *
	 * @param objMechanism the new obj mechanism
	 */
	public void setObjMechanism(NegotiationMechanism objMechanism) {
		this.objMechanism = objMechanism;
	}
	
}
