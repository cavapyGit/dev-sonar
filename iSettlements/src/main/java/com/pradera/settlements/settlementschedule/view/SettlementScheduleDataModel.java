package com.pradera.settlements.settlementschedule.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.settlement.SettlementSchedule;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementScheduleDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementScheduleDataModel extends ListDataModel<SettlementSchedule> implements SelectableDataModel<SettlementSchedule>,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8641102324511386295L;
	
	/** The size. */
	private int size;

	/**
	 * Instantiates a new settlement schedule data model.
	 *
	 * @param data the data
	 */
	public SettlementScheduleDataModel(List<SettlementSchedule> data){
		super(data);
	}

	/**
	 * Gets the size.
	 *
	 * @return size
	 */
	public int getSize() {
		List<SettlementSchedule> settlementSchedule=(List<SettlementSchedule>)getWrappedData();
		if(settlementSchedule==null)
			return 0;
		return settlementSchedule.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(SettlementSchedule settlementSchedule) {
		return settlementSchedule.getIdSettlementSchedulePk();
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public SettlementSchedule getRowData(String rowKey) {
		List<SettlementSchedule> settlementSchedules=(List<SettlementSchedule>)getWrappedData();
		if(settlementSchedules != null){
	        for(SettlementSchedule settlementSchedule : settlementSchedules) {  
	            if(String.valueOf(settlementSchedule.getIdSettlementSchedulePk()).equals(rowKey))
	                return settlementSchedule;  
	        }  
		}
		return null;
	}
	
}
