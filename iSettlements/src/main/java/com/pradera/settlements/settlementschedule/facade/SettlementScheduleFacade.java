package com.pradera.settlements.settlementschedule.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementSchedule;
import com.pradera.settlements.settlementschedule.service.SettlementScheduleService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SettlementScheduleFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/12/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SettlementScheduleFacade {
	
	/** The settlement schedule service. */
	@EJB
	private SettlementScheduleService settlementScheduleService;	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The notification service facade. */
	@Inject
	private NotificationServiceFacade notificationServiceFacade;
	/**
	 * Instantiates a new settlement schedule facade.
	 */
	public SettlementScheduleFacade() {
		super();
	}
	
	/**
	 * Register schedule facade.
	 *
	 * @param settlementSchedule the settlement schedule
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer registerScheduleFacade(SettlementSchedule settlementSchedule)throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());      
		settlementSchedule.setAudit(loggerUser);
        if(settlementScheduleService.existsSchedule(settlementSchedule)){
        	return null;
        }
		settlementScheduleService.registerScheduleService(settlementSchedule);
		
		settlementScheduleService.updateProcessScheduler(settlementSchedule);
		
		return settlementSchedule.getScheduleType();
	}
	
	/**
	 * Search settlement schedule facade.
	 *
	 * @param settlementSchedule the settlement schedule
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SettlementSchedule> searchSettlementScheduleFacade(SettlementSchedule settlementSchedule)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return settlementScheduleService.searchSettlementScheduleService(settlementSchedule);
	}

	/**
	 * Search ss order by schedules facade.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SettlementSchedule> searchSSOrderBySchedulesFacade()throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return settlementScheduleService.searchSSOrderBySchedulesService();
	}
	
	/**
	 * Update settlement schedule facade.
	 *
	 * @param settlementSchedule the settlement schedule
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int updateSettlementScheduleFacade(SettlementSchedule settlementSchedule)throws ServiceException{	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		settlementScheduleService.updateScheduleService(settlementSchedule);
		
		settlementScheduleService.updateProcessScheduler(settlementSchedule);
		
		return settlementSchedule.getScheduleType();
	}
	
	/**
	 * Gets the schedule type by schedule process.
	 *
	 * @param idParameterTable the id parameter table
	 * @param indicator1 the indicator1
	 * @return the schedule type by schedule process
	 * @throws ServiceException the service exception
	 */
	public ParameterTable getScheduleTypeByScheduleProcess(Integer idParameterTable, String indicator1) throws ServiceException{
		return settlementScheduleService.getScheduleTypeByScheduleProcess(idParameterTable, indicator1);
	}
	
	/**
	 * Send notification.
	 *
	 * @param pt the pt
	 */
	public void sendNotification(ParameterTable pt){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		String settlementType = pt.getDescription();
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFIER_SETTLEMENT_SCHEDULE.getCode());
		notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, null, new Object[]{settlementType});
	}
	
}
