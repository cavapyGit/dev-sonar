package com.pradera.settlements.settlementschedule.view.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementSchedulesTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementSchedulesTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id schedule pk. */
	private Long idSchedulePk;
	
	/** The schedule name. */
	private String scheduleName;
	
	/** The schedule type description. */
	private String scheduleTypeDescription;
	
	/** The schedule type. */
	private Integer scheduleType;
	
	/** The execution time. */
	private Integer executionTime;
	
	/** The ind sanction. */
	private boolean indSanction;
	
	/** The ind extend. */
	private boolean indExtend;
	
	/** The ind extend settlement. */
	private boolean indExtendSettlement;
	
	/** The ind settlement. */
	private boolean indSettlement;
	
	/** The ind unfulfillment. */
	private boolean indUnfulfillment;
	
	/** The schedule type ref. */
	private Integer scheduleTypeRef;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/**
	 * Gets the schedule name.
	 *
	 * @return the schedule name
	 */
	public String getScheduleName() {
		return scheduleName;
	}
	
	/**
	 * Sets the schedule name.
	 *
	 * @param scheduleName the new schedule name
	 */
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}
	
	/**
	 * Gets the schedule type description.
	 *
	 * @return the schedule type description
	 */
	public String getScheduleTypeDescription() {
		return scheduleTypeDescription;
	}
	
	/**
	 * Sets the schedule type description.
	 *
	 * @param scheduleTypeDescription the new schedule type description
	 */
	public void setScheduleTypeDescription(String scheduleTypeDescription) {
		this.scheduleTypeDescription = scheduleTypeDescription;
	}
	
	/**
	 * Gets the schedule type.
	 *
	 * @return the schedule type
	 */
	public Integer getScheduleType() {
		return scheduleType;
	}
	
	/**
	 * Sets the schedule type.
	 *
	 * @param scheduleType the new schedule type
	 */
	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}
	
	/**
	 * Gets the execution time.
	 *
	 * @return the execution time
	 */
	public Integer getExecutionTime() {
		return executionTime;
	}
	
	/**
	 * Sets the execution time.
	 *
	 * @param executionTime the new execution time
	 */
	public void setExecutionTime(Integer executionTime) {
		this.executionTime = executionTime;
	}
	
	/**
	 * Gets the schedule type ref.
	 *
	 * @return the schedule type ref
	 */
	public Integer getScheduleTypeRef() {
		return scheduleTypeRef;
	}
	
	/**
	 * Sets the schedule type ref.
	 *
	 * @param scheduleTypeRef the new schedule type ref
	 */
	public void setScheduleTypeRef(Integer scheduleTypeRef) {
		this.scheduleTypeRef = scheduleTypeRef;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Checks if is ind sanction.
	 *
	 * @return true, if is ind sanction
	 */
	public boolean isIndSanction() {
		return indSanction;
	}
	
	/**
	 * Sets the ind sanction.
	 *
	 * @param indSanction the new ind sanction
	 */
	public void setIndSanction(boolean indSanction) {
		this.indSanction = indSanction;
	}
	
	/**
	 * Checks if is ind extend.
	 *
	 * @return true, if is ind extend
	 */
	public boolean isIndExtend() {
		return indExtend;
	}
	
	/**
	 * Sets the ind extend.
	 *
	 * @param indExtend the new ind extend
	 */
	public void setIndExtend(boolean indExtend) {
		this.indExtend = indExtend;
	}
	
	/**
	 * Checks if is ind extend settlement.
	 *
	 * @return true, if is ind extend settlement
	 */
	public boolean isIndExtendSettlement() {
		return indExtendSettlement;
	}
	
	/**
	 * Sets the ind extend settlement.
	 *
	 * @param indExtendSettlement the new ind extend settlement
	 */
	public void setIndExtendSettlement(boolean indExtendSettlement) {
		this.indExtendSettlement = indExtendSettlement;
	}
	
	/**
	 * Checks if is ind settlement.
	 *
	 * @return true, if is ind settlement
	 */
	public boolean isIndSettlement() {
		return indSettlement;
	}
	
	/**
	 * Sets the ind settlement.
	 *
	 * @param indSettlement the new ind settlement
	 */
	public void setIndSettlement(boolean indSettlement) {
		this.indSettlement = indSettlement;
	}
	
	/**
	 * Checks if is ind unfulfillment.
	 *
	 * @return true, if is ind unfulfillment
	 */
	public boolean isIndUnfulfillment() {
		return indUnfulfillment;
	}
	
	/**
	 * Sets the ind unfulfillment.
	 *
	 * @param indUnfulfillment the new ind unfulfillment
	 */
	public void setIndUnfulfillment(boolean indUnfulfillment) {
		this.indUnfulfillment = indUnfulfillment;
	}
	
	/**
	 * Gets the id schedule pk.
	 *
	 * @return the id schedule pk
	 */
	public Long getIdSchedulePk() {
		return idSchedulePk;
	}
	
	/**
	 * Sets the id schedule pk.
	 *
	 * @param idSchedulePk the new id schedule pk
	 */
	public void setIdSchedulePk(Long idSchedulePk) {
		this.idSchedulePk = idSchedulePk;
	}

}
