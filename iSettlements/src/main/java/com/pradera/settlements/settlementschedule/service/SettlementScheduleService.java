/*
 * 
 */
package com.pradera.settlements.settlementschedule.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.configuration.UserAdmin;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.ProcessSchedule;
import com.pradera.model.process.type.ProcessType;
import com.pradera.model.settlement.SettlementSchedule;
import com.google.gson.Gson;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementScheduleService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SettlementScheduleService extends CrudDaoServiceBean{
	
	/** The timer service. */
	@Resource
    TimerService timerService;
	
	/** The log. */
	@Inject
    PraderaLogger log;
	
	/** The client rest service. */
	@Inject 
    ClientRestService clientRestService;
	
	/** The user admin. */
	@Inject @UserAdmin
    String userAdmin;
	
	/**
	 * Instantiates a new settlement schedule service.
	 */
	public SettlementScheduleService() {
		super();
	}
	
	/**
	 * Exists Schedule.
	 *
	 * @param ss the ss
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean existsSchedule(SettlementSchedule ss) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ss FROM SettlementSchedule ss");
			sbQuery.append("	WHERE ss.scheduleType = :idScheduleType");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idScheduleType", ss.getScheduleType());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult())){
				return true;
			}
		} catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Register schedule service.
	 *
	 * @param ss the ss
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer registerScheduleService(SettlementSchedule ss)throws ServiceException{
		create(ss);
		return ss.getScheduleType();
	}
	
	/**
	 * Search settlement schedule service.
	 *
	 * @param ss the ss
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementSchedule> searchSettlementScheduleService(SettlementSchedule ss)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<SettlementSchedule> settlementScheduleList = null;
		sbQuery.append(" SELECT ss FROM SettlementSchedule ss");
		sbQuery.append(" where 1=1 ");
		if (Validations.validateIsNotNullAndNotEmpty(ss.getScheduleType())) {
		sbQuery.append("and ss.scheduleType = :scheduleType");
		}
		if(Validations.validateIsNotNullAndNotEmpty(ss.getStartDate())){
			sbQuery.append(" And ss.startDate >= :initialDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(ss.getEndDate())){
			sbQuery.append(" And ss.endDate <= :finalDate");
		}
		
		Query query = em.createQuery(sbQuery.toString());

		if (Validations.validateIsNotNullAndNotEmpty(ss.getScheduleType())) {
		query.setParameter("scheduleType",ss.getScheduleType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(ss.getStartDate())){
			query.setParameter("initialDate", ss.getStartDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(ss.getEndDate())){
			query.setParameter("finalDate", ss.getEndDate());
		}
		
		settlementScheduleList = query.getResultList();
		
		return settlementScheduleList;
	}
	
	/**
	 * Search ss order by schedules service.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementSchedule> searchSSOrderBySchedulesService()throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<SettlementSchedule> settlementScheduleList = null;
		sbQuery.append(" SELECT ss FROM SettlementSchedule ss order by ss.startDate, ss.endDate");
		
		Query query = em.createQuery(sbQuery.toString());
		settlementScheduleList = query.getResultList();
		
		return settlementScheduleList;
	}
	
	/**
	 * Update schedule service.
	 *
	 * @param ss the ss
	 * @throws ServiceException the service exception
	 */
	public void updateScheduleService(SettlementSchedule ss) throws ServiceException{
		update(ss);
	}

	/**
	 * Update schedule process service.
	 *
	 * @param ps the ps
	 * @throws ServiceException the service exception
	 */
	public void updateScheduleProcessService(ProcessSchedule ps) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("UPDATE ProcessSchedule ps SET ps.scheduleInfo.scheduleMinute =:minute");
	}
	
	/**
	 * Gets the schedule type by schedule process.
	 *
	 * @param idParameterTable the id parameter table
	 * @param indicator1 the indicator1
	 * @return the schedule type by schedule process
	 * @throws ServiceException the service exception
	 */
	public ParameterTable getScheduleTypeByScheduleProcess(Integer idParameterTable, String indicator1) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		ParameterTable parameterTable = null;
		sbQuery.append(" SELECT pt FROM ParameterTable pt ");
		sbQuery.append(" where pt.masterTable.masterTablePk = :scheduleMT ");
		if(idParameterTable!=null){
			sbQuery.append(" and pt.parameterTablePk=:idScheduleType");
		}
		if(indicator1!=null){
			sbQuery.append(" and pt.indicator1= :idSchedule");
		}
		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("scheduleMT", MasterTableType.PARAMETER_TABLE_SCHED_SETTLEMENT.getCode());
		if(idParameterTable!=null){
			query.setParameter("idScheduleType", idParameterTable);
		}
		if(indicator1!=null){
			query.setParameter("idSchedule", indicator1);
		}
		parameterTable = (ParameterTable) query.getSingleResult();
		
		return parameterTable;
	}
	
	/**
	 * Gets the process scheduler.
	 *
	 * @param businessProcess the business process
	 * @return the process scheduler
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<ProcessSchedule> getProcessScheduler(Long businessProcess) throws ServiceException{
	    StringBuilder builder = new StringBuilder();
		builder.append("select ps ");
		builder.append("from ProcessSchedule ps join fetch ps.businessProcess bp ");
		builder.append("where bp.processType =:processType ");
		builder.append("and bp.idBusinessProcessPk = :id ");
		TypedQuery<ProcessSchedule> queryString = em.createQuery(builder.toString(),ProcessSchedule.class);
		queryString.setParameter("id", businessProcess);
		queryString.setParameter("processType", ProcessType.BATCH.getCode());
		List<ProcessSchedule> listSchedules = queryString.getResultList();
		return listSchedules;
    }
	
	/**
	 * Gets the timer.
	 *
	 * @param idTimer the id timer
	 * @return the timer
	 */
	public Timer getTimer(Long idTimer){
       Collection<Timer> timers = timerService.getTimers();
       for (Timer t : timers){
           if(idTimer.equals(((Long)t.getInfo()))){
               return t;
           }
       }
       return null;
   }

	/**
	 * Update process scheduler.
	 *
	 * @param settlementSchedule the settlement schedule
	 * @throws ServiceException the service exception
	 */
	public void updateProcessScheduler(SettlementSchedule settlementSchedule) throws ServiceException {
		List<ProcessSchedule> lstSchedules = getProcessScheduler(BusinessProcessType.NOTIFIER_SETTLEMENT_SCHEDULE.getCode());
		ParameterTable parameterTable = getScheduleTypeByScheduleProcess(settlementSchedule.getScheduleType(),null);
		for(ProcessSchedule ps: lstSchedules){
			if(ps.getIdProcessSchedulePk().toString().equals(parameterTable.getIndicator1())){
				Calendar endDateCalendar = Calendar.getInstance();
				endDateCalendar.setTime(settlementSchedule.getEndDate());
				Integer hour = endDateCalendar.get(Calendar.HOUR_OF_DAY);
				Integer minute = endDateCalendar.get(Calendar.MINUTE);
				ps.getScheduleInfo().setScheduleHour(hour.toString());
				ps.getScheduleInfo().setScheduleMinute(minute.toString());
				ps.getScheduleInfo().setIdProcessSchedulePk(ps.getIdProcessSchedulePk());
				update(ps);
				String input = new Gson().toJson(ps.getScheduleInfo());
				clientRestService.updateProcessScheduleTimer(input);
				break;
			}
		}
	}
	

}
