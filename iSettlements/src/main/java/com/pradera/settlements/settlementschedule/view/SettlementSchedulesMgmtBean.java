package com.pradera.settlements.settlementschedule.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementSchedule;
import com.pradera.settlements.settlementschedule.facade.SettlementScheduleFacade;
import com.pradera.settlements.settlementschedule.view.to.SettlementSchedulesTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementSchedulesMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@LoggerCreateBean
@DepositaryWebBean
public class SettlementSchedulesMgmtBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The parameters facade. */
	@EJB
	private GeneralParametersFacade parametersFacade;
	
	/** The settlement schedule facade. */
	@EJB
	private SettlementScheduleFacade settlementScheduleFacade;
	
	/** The settlement schedule data model. */
	private SettlementScheduleDataModel settlementScheduleDataModel;
	
	/** The settlement schedule to. */
	private SettlementSchedulesTO settlementScheduleTO;
	
	/** The settlement schedule. */
	private SettlementSchedule settlementSchedule;
	
	/** The settlement schedule selected. */
	private SettlementSchedule settlementScheduleSelected;
	
	/** The modify. */
	private boolean modify;
	
	/** The lst schedule type. */
	private List<SelectItem> lstScheduleType;
	
	/** The lst schedule type ref. */
	private List<SelectItem> lstScheduleTypeRef;
	
	/** The map schedule types. */
	private Map<Integer, String> mapScheduleTypes;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/**
	 * Instantiates a new settlement schedules mgmt bean.
	 */
	public SettlementSchedulesMgmtBean() {
	}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		settlementScheduleTO = new SettlementSchedulesTO();
		settlementSchedule = new SettlementSchedule();
		settlementScheduleTO.setInitialDate(CommonsUtilities.getDefaultDateWithCurrentTime());
		settlementScheduleTO.setFinalDate(CommonsUtilities.getDefaultDateWithCurrentTime());
		settlementScheduleSelected = new SettlementSchedule();
		modify=false;
		loadScheduleType();
		showPrivileges();
	}
	
	/**
	 * Show privileges.
	 *
	 * @throws ServiceException the service exception
	 */
	private void showPrivileges() throws ServiceException {
		PrivilegeComponent privilegeComp = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isRegister()){
			privilegeComp.setBtnRegisterView(true);	
		}
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComp.setBtnModifyView(true);
		}			
		userPrivilege.setPrivilegeComponent(privilegeComp);
	}
	
	/**
	 * Load schedule type.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void loadScheduleType() throws ServiceException{
		try{
			lstScheduleType = new ArrayList<SelectItem>();
			lstScheduleTypeRef = new ArrayList<SelectItem>();
			mapScheduleTypes = new HashMap<Integer, String>();
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.PARAMETER_TABLE_SCHED_SETTLEMENT.getCode());
			for(ParameterTable param : parametersFacade.getListParameterTableServiceBean(paramTable)) {
				lstScheduleType.add(new SelectItem(param.getParameterTablePk(), param.getDescription()));
				lstScheduleTypeRef.add(new SelectItem(param.getParameterTablePk(), param.getDescription()));
				mapScheduleTypes.put(param.getParameterTablePk(), param.getDescription());
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change schedule type.
	 */
	public void changeScheduleType(){
		if(settlementScheduleTO.getScheduleType()!=null && settlementScheduleTO.getScheduleTypeRef()!=null){
			if(settlementScheduleTO.getScheduleType().equals(settlementScheduleTO.getScheduleTypeRef())){
				if(!modify){//always this field should be filled on a modification
					settlementScheduleTO.setScheduleType(null);
				}
				settlementScheduleTO.setScheduleTypeRef(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALT_SCHEDULES_SELECTED_WRONG));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Search settlement schedule.
	 */
	@LoggerAuditWeb
	public void searchSettlementSchedule(){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);	
		try{	
			settlementScheduleDataModel = new SettlementScheduleDataModel(settlementScheduleFacade.searchSettlementScheduleFacade(settlementSchedule));
			PrivilegeComponent pc = new PrivilegeComponent();
			pc.setBtnModifyView(true);
			userPrivilege.setPrivilegeComponent(pc);
		}catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}
	
	/**
	 * Clear search.
	 */
	public void clearSearch(){
		JSFUtilities.resetViewRoot();
		clear();
	}
	
	/**
	 * Clear.
	 */
	public void clear(){
		settlementSchedule = new SettlementSchedule();
		settlementScheduleDataModel = null;	
	}
	
	/**
	 * Register view.
	 *
	 * @throws ServiceException the service exception
	 */
	public void registerView() throws ServiceException{
		modify=false;
		init();
	}
	
	/**
	 * Fill schedule data.
	 *
	 * @param settlementScheduleSelected the settlement schedule selected
	 */
	public void fillScheduleData(SettlementSchedule settlementScheduleSelected){
		settlementScheduleTO.setExecutionTime(settlementScheduleSelected.getExecutionTimes());
		settlementScheduleTO.setFinalDate(settlementScheduleSelected.getEndDate());
		settlementScheduleTO.setInitialDate(settlementScheduleSelected.getStartDate());
		settlementScheduleTO.setScheduleType(settlementScheduleSelected.getScheduleType());
		settlementScheduleTO.setScheduleTypeRef(settlementScheduleSelected.getScheduleTypeRef());
		settlementScheduleTO.setIdSchedulePk(settlementScheduleSelected.getIdSettlementSchedulePk());
		if(BooleanType.NO.getCode().equals(settlementScheduleSelected.getIndExtend())){
			settlementScheduleTO.setIndExtend(false);
		}else{
			settlementScheduleTO.setIndExtend(true);
		}
		if(BooleanType.NO.getCode().equals(settlementScheduleSelected.getIndExtendSettlement())){
			settlementScheduleTO.setIndExtendSettlement(false);
		}else{
			settlementScheduleTO.setIndExtendSettlement(true);
		}
		if(BooleanType.NO.getCode().equals(settlementScheduleSelected.getIndSanction())){
			settlementScheduleTO.setIndSanction(false);
		}else{
			settlementScheduleTO.setIndSanction(true);
		}
		if(BooleanType.NO.getCode().equals(settlementScheduleSelected.getIndSettlement())){
			settlementScheduleTO.setIndSettlement(false);
		}else{
			settlementScheduleTO.setIndSettlement(true);
		}
		if(BooleanType.NO.getCode().equals(settlementScheduleSelected.getIndUnfulfillment())){
			settlementScheduleTO.setIndUnfulfillment(false);
		}else{
			settlementScheduleTO.setIndUnfulfillment(true);
		}
	}
	
	/**
	 * Modify view.
	 *
	 * @param settlementScheduleSelected the settlement schedule selected
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String modifyView(SettlementSchedule settlementScheduleSelected) throws ServiceException{
		modify=true;
		fillScheduleData(settlementScheduleSelected);
		return "settlementScheduleMgmt";
	}
	
	/**
	 * Detail view.
	 *
	 * @param settlementScheduleSelected the settlement schedule selected
	 */
	public void detailView(SettlementSchedule settlementScheduleSelected){
		fillScheduleData(settlementScheduleSelected);
	}
	
	/**
	 * Validate dates.
	 *
	 * @param indicador the indicador
	 * @throws ServiceException the service exception
	 */
	public void validateDates(Integer indicador) throws ServiceException{
		boolean blHasError=false;
		if(settlementScheduleTO.getInitialDate().equals(settlementScheduleTO.getFinalDate())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_SAME_DATES));
			showMessageDefault(true);
		}else if(settlementScheduleTO.getInitialDate().after(settlementScheduleTO.getFinalDate())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_FINAL_BEFORE_INITIAL_DATES));
			showMessageDefault(true);
		}else if(settlementScheduleTO.getFinalDate().before(settlementScheduleTO.getInitialDate())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_INITIAL_AFTER_FINAL_DATES));
			showMessageDefault(true);
		}else{
			settlementScheduleSelected.setStartDate(settlementScheduleTO.getInitialDate());
			settlementScheduleSelected.setEndDate(settlementScheduleTO.getFinalDate());
			if(!settlementScheduleTO.getInitialDate().after(settlementScheduleTO.getFinalDate()) && BooleanType.NO.getCode().equals(indicador)){
				blHasError= validateCrossSchedules(indicador);
				showMessageDefault(blHasError);
			}else if(!settlementScheduleTO.getFinalDate().before(settlementScheduleTO.getInitialDate()) && BooleanType.YES.getCode().equals(indicador)){
				blHasError= validateCrossSchedules(indicador);
				showMessageDefault(blHasError);
			}
		}
	}
	
	/**
	 * Reset dates.
	 */
	public void resetDates(){
		settlementScheduleTO.setInitialDate(settlementScheduleSelected.getStartDate());
		settlementScheduleTO.setFinalDate(settlementScheduleSelected.getEndDate());
	}
	
	/**
	 * Show message default.
	 *
	 * @param blHasError the bl has error
	 */
	public void showMessageDefault(boolean blHasError){
		if(blHasError){
			JSFUtilities.showSimpleValidationDialog();
			resetDates();
			return;
		}
	}
	
	/**
	 * 0=initial date
	 * 1=final date.
	 *
	 * @param indicador the indicador
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateCrossSchedules(Integer indicador) throws ServiceException{
		boolean flag=false;
		List<SettlementSchedule> lstSchedules= settlementScheduleFacade.searchSSOrderBySchedulesFacade();
		SettlementSchedule ssAfter = null;
		SettlementSchedule ssBefore = null;
		
		for (int i = 0; i < lstSchedules.size(); i++) {
			SettlementSchedule ss = lstSchedules.get(i);
				
			if(lstSchedules.size()==i+1){
				ssBefore = lstSchedules.get(i-1);
				ssAfter = lstSchedules.get(0);
			}else if(i==0){
				ssBefore = lstSchedules.get(lstSchedules.size()-1);
				ssAfter = lstSchedules.get(i+1);
			}else{
				ssBefore = lstSchedules.get(i-1);
				ssAfter = lstSchedules.get(i+1);
			}
			
			if(settlementScheduleSelected.getStartDate().equals(ss.getStartDate()) && settlementScheduleSelected.getEndDate().equals(ss.getEndDate())){
				if(BooleanType.NO.getCode().equals(indicador)){
					if(settlementScheduleTO.getInitialDate().before(ssBefore.getEndDate())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_INITIAL_AFTER_FINAL_DATES_CROSS2));
						flag=true;
						break;
					}
				}else if(BooleanType.YES.getCode().equals(indicador)){
					if(settlementScheduleTO.getFinalDate().after(ssAfter.getStartDate())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_FINAL_AFTER_INITIAL_DATES_CROSS));
						flag=true;
						break;
					}
				}
			}		
		}
		return flag;
	}
	
	/**
	 * Before save or modify.
	 */
	public void beforeSaveOrModify() {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		if(settlementScheduleTO.getInitialDate().equals(settlementScheduleTO.getFinalDate())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_SAME_DATES));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		try{
			Object[] bodyData = new Object[1];
			bodyData[0] = mapScheduleTypes.get(settlementScheduleTO.getScheduleType());
			
			if(modify){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
									PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_PRE_MODIFY, bodyData));
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
									PropertiesUtilities.getMessage(PropertiesConstants.MSG_SCHEDULE_PRE_CONFIRM, bodyData));
			}
			JSFUtilities.executeJavascriptFunction("PF('idwConfirmDialog').show()");					
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Register or update schedule.
	 */
	@LoggerAuditWeb
	public void registerOrUpdateSchedule(){
		BusinessProcess businessProcess = new BusinessProcess();
		Integer scheduleType=null;
		if(modify){
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.SCHEDULE_SETTLEMENT_MODIFY.getCode());	
			scheduleType= modifyScheduleSelected();
		}else{
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.SCHEDULE_SETTLEMENT_REGISTER.getCode());	
			scheduleType= registerSchedule();
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(scheduleType)) {
			// Send notification						
			Object[] parameters = new Object[3];
			parameters[0] = mapScheduleTypes.get(scheduleType);
			parameters[1] = settlementScheduleTO.getInitialDate().toString(); 
			parameters[2] =settlementScheduleTO.getFinalDate().toString();
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, null, parameters);
		}		
		
		try{			
			Object[] bodyData = new Object[1];
			bodyData[0] = mapScheduleTypes.get(scheduleType);
			if(Validations.validateIsNullOrEmpty(scheduleType)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getMessage(PropertiesConstants.ALT_SCHEDULE_ALREADY_EXISTS, bodyData));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}else{
				if(modify){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.ALT_SCHEDULE_MODIFY_SUCCESS, bodyData));
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.ALT_SCHEDULE_REGISTER_SUCCESS, bodyData));
				}
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
			}
			
			clearRegisterData();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Register schedule.
	 *
	 * @return the integer
	 */
	public Integer registerSchedule(){
		Integer scheduleType=null;
		SettlementSchedule ss = fillDataFacade();
		try {
			scheduleType = settlementScheduleFacade.registerScheduleFacade(ss);
			if(scheduleType==null){
				scheduleType=settlementScheduleTO.getScheduleType();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return scheduleType;
	}
	
	/**
	 * Fill data facade.
	 *
	 * @return the settlement schedule
	 */
	public SettlementSchedule fillDataFacade(){
		SettlementSchedule ss = new SettlementSchedule();
		
		ss.setEndDate(settlementScheduleTO.getFinalDate());
		ss.setExecutionTimes(settlementScheduleTO.getExecutionTime());
		ss.setScheduleType(settlementScheduleTO.getScheduleType());
		ss.setScheduleTypeRef(settlementScheduleTO.getScheduleTypeRef());
		ss.setStartDate(settlementScheduleTO.getInitialDate());
		ss.setIdSettlementSchedulePk(settlementScheduleTO.getIdSchedulePk());
		if(BooleanType.NO.getBooleanValue() == settlementScheduleTO.isIndExtend()){
			ss.setIndExtend(BooleanType.NO.getCode());
		}else{
			ss.setIndExtend(BooleanType.YES.getCode());
		}
		if(BooleanType.NO.getBooleanValue() == settlementScheduleTO.isIndExtendSettlement()){
			ss.setIndExtendSettlement(BooleanType.NO.getCode());
		}else{
			ss.setIndExtendSettlement(BooleanType.YES.getCode());
		}
		if(BooleanType.NO.getBooleanValue() == settlementScheduleTO.isIndSanction()){
			ss.setIndSanction(BooleanType.NO.getCode());
		}else{
			ss.setIndSanction(BooleanType.YES.getCode());
		}
		if(BooleanType.NO.getBooleanValue() == settlementScheduleTO.isIndSettlement()){
			ss.setIndSettlement(BooleanType.NO.getCode());
		}else{
			ss.setIndSettlement(BooleanType.YES.getCode());
		}
		if(BooleanType.NO.getBooleanValue() == settlementScheduleTO.isIndUnfulfillment()){
			ss.setIndUnfulfillment(BooleanType.NO.getCode());
		}else{
			ss.setIndUnfulfillment(BooleanType.YES.getCode());
		}
		
		return ss;
	}
	
	/**
	 * Modify schedule selected.
	 *
	 * @return the integer
	 */
	public Integer modifyScheduleSelected(){
		Integer scheduleType=null;
		SettlementSchedule ss = fillDataFacade();
		try {
			scheduleType = settlementScheduleFacade.updateSettlementScheduleFacade(ss);
			if(scheduleType==null){
				scheduleType=settlementScheduleTO.getScheduleType();
			}
			clearSearch();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return scheduleType;
	}
	
	/**
	 * Clear register data.
	 *
	 * @throws ServiceException the service exception
	 */
	public void clearRegisterData() throws ServiceException{
//		//Clear the Registration details
		if(userInfo.getUserAcctions().isRegister()){
			settlementScheduleTO = new SettlementSchedulesTO();
			init();
		}		
	}

	/**
	 * Gets the settlement schedule data model.
	 *
	 * @return the settlement schedule data model
	 */
	public SettlementScheduleDataModel getSettlementScheduleDataModel() {
		return settlementScheduleDataModel;
	}

	/**
	 * Sets the settlement schedule data model.
	 *
	 * @param settlementScheduleDataModel the new settlement schedule data model
	 */
	public void setSettlementScheduleDataModel(
			SettlementScheduleDataModel settlementScheduleDataModel) {
		this.settlementScheduleDataModel = settlementScheduleDataModel;
	}

	/**
	 * Gets the settlement schedule to.
	 *
	 * @return the settlement schedule to
	 */
	public SettlementSchedulesTO getSettlementScheduleTO() {
		return settlementScheduleTO;
	}

	/**
	 * Sets the settlement schedule to.
	 *
	 * @param settlementScheduleTO the new settlement schedule to
	 */
	public void setSettlementScheduleTO(SettlementSchedulesTO settlementScheduleTO) {
		this.settlementScheduleTO = settlementScheduleTO;
	}

	/**
	 * Gets the settlement schedule.
	 *
	 * @return the settlement schedule
	 */
	public SettlementSchedule getSettlementSchedule() {
		return settlementSchedule;
	}

	/**
	 * Sets the settlement schedule.
	 *
	 * @param settlementSchedule the new settlement schedule
	 */
	public void setSettlementSchedule(SettlementSchedule settlementSchedule) {
		this.settlementSchedule = settlementSchedule;
	}

	/**
	 * Checks if is modify.
	 *
	 * @return true, if is modify
	 */
	public boolean isModify() {
		return modify;
	}

	/**
	 * Sets the modify.
	 *
	 * @param modify the new modify
	 */
	public void setModify(boolean modify) {
		this.modify = modify;
	}

	/**
	 * Gets the lst schedule type.
	 *
	 * @return the lst schedule type
	 */
	public List<SelectItem> getLstScheduleType() {
		return lstScheduleType;
	}

	/**
	 * Sets the lst schedule type.
	 *
	 * @param lstScheduleType the new lst schedule type
	 */
	public void setLstScheduleType(List<SelectItem> lstScheduleType) {
		this.lstScheduleType = lstScheduleType;
	}

	/**
	 * Gets the lst schedule type ref.
	 *
	 * @return the lst schedule type ref
	 */
	public List<SelectItem> getLstScheduleTypeRef() {
		return lstScheduleTypeRef;
	}

	/**
	 * Sets the lst schedule type ref.
	 *
	 * @param lstScheduleTypeRef the new lst schedule type ref
	 */
	public void setLstScheduleTypeRef(List<SelectItem> lstScheduleTypeRef) {
		this.lstScheduleTypeRef = lstScheduleTypeRef;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the map schedule types.
	 *
	 * @return the map schedule types
	 */
	public Map<Integer, String> getMapScheduleTypes() {
		return mapScheduleTypes;
	}

	/**
	 * Sets the map schedule types.
	 *
	 * @param mapScheduleTypes the map schedule types
	 */
	public void setMapScheduleTypes(Map<Integer, String> mapScheduleTypes) {
		this.mapScheduleTypes = mapScheduleTypes;
	}

	/**
	 * Gets the settlement schedule selected.
	 *
	 * @return the settlement schedule selected
	 */
	public SettlementSchedule getSettlementScheduleSelected() {
		return settlementScheduleSelected;
	}

	/**
	 * Sets the settlement schedule selected.
	 *
	 * @param settlementScheduleSelected the new settlement schedule selected
	 */
	public void setSettlementScheduleSelected(
			SettlementSchedule settlementScheduleSelected) {
		this.settlementScheduleSelected = settlementScheduleSelected;
	}

}
