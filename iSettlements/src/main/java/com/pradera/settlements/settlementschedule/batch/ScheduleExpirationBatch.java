package com.pradera.settlements.settlementschedule.batch;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.process.ProcessSchedule;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.settlementschedule.facade.SettlementScheduleFacade;
import com.pradera.settlements.settlementschedule.service.SettlementScheduleService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ScheduleExpirationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/12/2014
 */
@BatchProcess(name="ScheduleExpirationBatch")
@RequestScoped
public class ScheduleExpirationBatch implements JobExecution , Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	@Inject
	private static PraderaLogger logger;
	
	/** The settlement schedule facade. */
	@EJB
	private SettlementScheduleFacade settlementScheduleFacade;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The id schedule process. */
	private String idScheduleProcess;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		try {
			for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
				if(detail.getParameterName().equals(GeneralConstants.PARAMETER_SCHEDULE_ID)){
					idScheduleProcess = detail.getParameterValue();
				}
			}
			this.executeExpirationSchedules();
		} catch (ServiceException e) {
			e.printStackTrace();
			logger.error("scheduleExpirationBatch");
		}
		
	}

	/**
	 * Execute expiration schedules.
	 *
	 * @throws ServiceException the service exception
	 */
	private void executeExpirationSchedules() throws ServiceException {
		ParameterTable pt= settlementScheduleFacade.getScheduleTypeByScheduleProcess(null,idScheduleProcess);
		
		settlementScheduleFacade.sendNotification(pt);
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
