package com.pradera.settlements.sanction.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ParticipantPenalty;
import com.pradera.model.settlement.type.SanctionReasonType;
import com.pradera.model.settlement.type.SanctionStateType;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.settlements.sanction.facade.SanctionServiceFacade;
import com.pradera.settlements.sanction.to.SanctionByModalityTO;
import com.pradera.settlements.sanction.to.SanctionMotiveTO;
import com.pradera.settlements.sanction.to.SanctionOperationTO;
import com.pradera.settlements.sanction.to.SanctionRequestTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchSanctionMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class SearchSanctionMgmtBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The list participant. */
	private List<Participant> listParticipant;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The sanction service facade. */
	@EJB
	SanctionServiceFacade sanctionServiceFacade;
	
	/** The mcn operation service facade. */
	@EJB
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The holiday query service bean. */
	@EJB 
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
    /** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;

	/** The holder filter. */
	private SanctionRequestTO sanctionFilter;
	
	/** The holder filter. */
	private SanctionRequestTO searchSanction;
	
	/** The lst participant state for search. */
	private List<ParameterTable> lstParticipantStateForSearch;
	
	/** The lst level for search. */
	private List<ParameterTable> lstLevelSanctionParticipant;
	
	/** The lst reason state for search. */
	private List<ParameterTable> lstStateSanctionParticipant;
	
	/** The lst reason state for search. */
	private List<ParameterTable> lstReasonSanctionParticipant;
	
	/** The lst currencies. */
	private List<ParameterTable> lstCurrencies;
	
	/** The lst sanction motive. */
	private List<SanctionMotiveTO> lstSanctionMotive;
	
	/** The selected sanction motive. */
	private SanctionMotiveTO selectedSanctionMotive;
	
	/**  The sanctionRequestDataModel for search. */
	private GenericDataModel<SanctionRequestTO> sanctionRequestDataModel;
	
	/** The render other motive. */
	private boolean renderOtherMotive;
	
	/** The enable amount. */
	private boolean enableAmount;
	
	/** The Enable Reprimand*/
	private boolean enableReprimand;
	
	/** The radio selected. */
	private Integer radioSelected = 1;
	
	/** The date period ini. */
	private Date datePeriodIni;
	
	/** The date period end. */
	private Date datePeriodEnd;
	
	/** The map descriptions. */
	private Map<Integer,String> mapDescriptions = null;

	/**
	 * The Constructor.
	 */
	public SearchSanctionMgmtBean() {
		sanctionFilter = new SanctionRequestTO();
		mapDescriptions=new HashMap<Integer,String>();
	}

	/**
	 * Inits components for view.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {

		if (!FacesContext.getCurrentInstance().isPostback()) {
			
			cleanSearch();
			
			listParticipant = mcnOperationServiceFacade.getLstParticipants(null);
			
			try {
				
				loadCombosForSearch();
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}

	}

	/**
	 * Load combos for search sanction.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCombosForSearch() throws ServiceException {

		ParameterTableTO filterParameterTable = new ParameterTableTO();

		// List of Participant
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_STATE.getCode());
		lstParticipantStateForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		
		// List the level sanction
		filterParameterTable.setMasterTableFk(MasterTableType.LEVEL_SANCTION_PARTICIPAN.getCode());
		lstLevelSanctionParticipant = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		
		if (lstLevelSanctionParticipant.size()>0){
			for(ParameterTable param : lstLevelSanctionParticipant){
				mapDescriptions.put(param.getParameterTablePk(), param.getParameterName());
			}
		}
		
		// List the state sanction
		filterParameterTable.setMasterTableFk(MasterTableType.STATE_SANCTION_PARTICIPAN.getCode());
		lstStateSanctionParticipant = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		if (lstStateSanctionParticipant.size()>0){
			for(ParameterTable param : lstStateSanctionParticipant){
				mapDescriptions.put(param.getParameterTablePk(), param.getParameterName());
				}
		}
		
		// List the reason sanction
		filterParameterTable.setMasterTableFk(MasterTableType.REASON_SANCTION_PARTICIPAN.getCode());
		filterParameterTable.setOrderbyParameterTableCd(ComponentConstant.ONE);
		lstReasonSanctionParticipant = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		if (lstReasonSanctionParticipant.size()>0){
			for(ParameterTable param : lstReasonSanctionParticipant){
				mapDescriptions.put(param.getParameterTablePk(), param.getParameterName());
			}
		}

		filterParameterTable.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		lstCurrencies = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		for(ParameterTable param : lstCurrencies){
			mapDescriptions.put(param.getParameterTablePk(), param.getParameterName());
		}

	}
	
	/**
	 * clean components for search sanction.
	 * 
	 */
	public void cleanSearch(){
		renderOtherMotive=false;
		searchSanction = new SanctionRequestTO();
		if(radioSelected == 1){
			searchSanction.setInitialDate(CommonsUtilities.currentDate());
			searchSanction.setEndDate(CommonsUtilities.currentDate());
		}else{
			searchSanction.setInitialDate(null);
			searchSanction.setEndDate(null);
		}
		sanctionRequestDataModel = null;
	}
	
	/**
	 * search sanction by filter parameters.
	 */
	@LoggerAuditWeb
	public void searchSanctionByFilters(){
		try{
			sanctionFilter = null;
			
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			
			List<SanctionRequestTO> lstSanctionResult = sanctionServiceFacade.getListSanctionSearch(searchSanction);
			
			sanctionRequestDataModel = new GenericDataModel<SanctionRequestTO>(lstSanctionResult);
			
			if(sanctionRequestDataModel.getRowCount()>0){
				showPrivilegeButtons();
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load register sanction action.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadRegisterSanctionAction(){
		try {
			selectedSanctionMotive = null;
			lstSanctionMotive = null;
			sanctionFilter = new SanctionRequestTO();
			sanctionFilter.setRegisterDate(CommonsUtilities.currentDate());
			sanctionFilter.setSettlementDate(CommonsUtilities.currentDate());
			sanctionFilter.setMinSettlementDate(holidayQueryServiceBean.getCalculateDateServiceBean(CommonsUtilities.currentDate(), 2, 0, null));
			
			definitionPeriodSanction(CommonsUtilities.currentDate());
			
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return "registerParticipantPenalty";
		
	}
	
	/**
	 * Definition period for notification sanction.
	 *
	 * @param fechaActual the fecha actual
	 */
	private void definitionPeriodSanction(Date fechaActual){
		try {
		List<ParameterTable> listPeriodSanction = new ArrayList<ParameterTable>();
		ParameterTableTO periodParameterTable = new ParameterTableTO(); 
		periodParameterTable.setMasterTableFk(MasterTableType.PERIOD_SANCTION_PARTICIPAN.getCode());
		
		listPeriodSanction = generalParametersFacade.getListParameterTableServiceBean(periodParameterTable);
		
		SimpleDateFormat dayMonthFormatter = new SimpleDateFormat(CommonsUtilities.DATE_PATTERN);
		for (ParameterTable objPeriodSanction :listPeriodSanction){
			StringBuffer sbInitialDate = new StringBuffer();
			sbInitialDate.append(objPeriodSanction.getText1()).append(GeneralConstants.SLASH).append(CommonsUtilities.currentYear());
			
			StringBuffer sbFinalDate = new StringBuffer();
			sbFinalDate.append(objPeriodSanction.getText2()).append(GeneralConstants.SLASH).append(CommonsUtilities.currentYear());
			
			Date initialDate = dayMonthFormatter.parse(sbInitialDate.toString());
			Date finalDate = dayMonthFormatter.parse(sbFinalDate.toString());
			if (fechaActual.after(initialDate) && fechaActual.before(finalDate)){
				
				if(sanctionFilter.getIdPeriodSanction()==null){
					sanctionFilter.setIdPeriodSanction(objPeriodSanction.getParameterTablePk());
					sanctionFilter.setPeriodSanction(String.format(objPeriodSanction.getDescription(), new Object[]{CommonsUtilities.currentYear()}));
				}
				datePeriodIni = initialDate;
				datePeriodEnd = finalDate;
				break;
			}
		}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * action for changeSelectedParticipantSearch.
	 *
	 * @param selectedSanction the selected sanction
	 */
	@LoggerAuditWeb
	public void searchSanctionMotives(Integer selectedSanction){
		selectedSanctionMotive = null;
		if(selectedSanction==null){
			sanctionFilter.setSanctionAmount(null);
		}
		renderOtherMotive = false;
		enableAmount = false;
		lstSanctionMotive = null;
		sanctionFilter.setMotiveOther(null);
		try {
			
			if(Validations.validateIsNotNullAndPositive(sanctionFilter.getIdParticipant()) && Validations.validateIsNotNull(sanctionFilter.getSettlementDate())){
				
				List<ParticipantPenalty> historialPenalties = sanctionServiceFacade.getParticipantPenalties(sanctionFilter.getIdParticipant(), datePeriodIni,datePeriodEnd);
				
				lstSanctionMotive = new ArrayList<SanctionMotiveTO>(); 
				
				for(ParameterTable paramTable : lstReasonSanctionParticipant){
					
					SanctionMotiveTO sanctionMotive = new SanctionMotiveTO();
					sanctionMotive.setSanctionCount(0);
					sanctionMotive.setParticipantPenalties(new ArrayList<ParticipantPenalty>());
					sanctionMotive.setMotiveDescription(paramTable.getParameterName());
					sanctionMotive.setSanctionMotive(paramTable.getParameterTablePk());
					sanctionMotive.setTypeSanction(paramTable.getShortInteger());
					
					for(ParticipantPenalty participantPenalty : historialPenalties){
						if(paramTable.getParameterTablePk().equals(participantPenalty.getPenaltyMotive())){
							//if(participantPenalty.getPenaltyAmount().compareTo(BigDecimal.ZERO)!=0){
								//Solo considerar en el Conteo las sanciones Pecuniarias
								sanctionMotive.setSanctionCount(sanctionMotive.getSanctionCount()+1);
								sanctionMotive.getParticipantPenalties().add(participantPenalty);
							//}
						}
					}
					
					if(selectedSanction!=null && selectedSanction.equals(paramTable.getParameterTablePk())){
						selectedSanctionMotive = sanctionMotive;
					}
					
					lstSanctionMotive.add(sanctionMotive);
				}
			}else{
				lstSanctionMotive = null;
			}
			orderSanctionMotive(lstSanctionMotive);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * On select sanction motive.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void onSelectSanctionMotive() throws ServiceException{
		renderOtherMotive = false;
		enableAmount = false;
		enableReprimand = false;
		selectedSanctionMotive.setLstSanctionByModalityTO(null);
		selectedSanctionMotive.setCurrentSanctionByModality(null);
		sanctionFilter.setSanctionAmount(null);
		sanctionFilter.setCurrency(CurrencyType.USD.getCode());
		sanctionFilter.setSanctionCount(selectedSanctionMotive.getSanctionCount()+1);
		
		//get penalty levels  NO SIRVE PARA NADA
		//PenaltyLevel penaltyLevel = 
		//			sanctionServiceFacade.getPenaltyLevel(selectedSanctionMotive.getSanctionMotive(),sanctionFilter.getSanctionCount());
		//sanctionFilter.setSanctionAmount(penaltyLevel.getPenaltyAmount());
		//sanctionFilter.setCurrency(penaltyLevel.getCurrency());
		
		Integer sanctionAmount = new Integer(0);
		
		List<ParticipantPenalty> historialPenalties = sanctionServiceFacade.getParticipantPenalties(sanctionFilter.getIdParticipant(), datePeriodIni,datePeriodEnd);
		
		if (Validations.validateIsNotNullAndNotEmpty(historialPenalties)){
			
			//definimos nueva lista
			List<ParticipantPenalty> historialPenaltiesMount = new 	ArrayList<>();
			//solo tomamos en cuenta las sanciones con algun costo
			for (ParticipantPenalty lsthistorialPenaltiesMount  : historialPenalties){
				if((lsthistorialPenaltiesMount.getPenaltyAmount().compareTo(GeneralConstants.ZERO))
						>GeneralConstants.ZERO_VALUE_INTEGER){
					historialPenaltiesMount.add(lsthistorialPenaltiesMount);
				}
			}
			
			//Si existen anteriores sanciones
			if(historialPenaltiesMount.size()>GeneralConstants.ZERO_VALUE_INTEGER){
				
				if(selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_FUNDS.getCode())
						|| selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_STOCK.getCode()) ){
					if(sanctionFilter.getQuantitySanction()< GeneralConstants.THREE_VALUE_INTEGER){
						sanctionAmount =  150;
					}else{
						sanctionAmount = historialPenaltiesMount.get(historialPenaltiesMount.size()-GeneralConstants.ONE_VALUE_INTEGER).getPenaltyAmount().intValue() + 50;
					}
				}else{
					sanctionAmount = historialPenaltiesMount.get(historialPenaltiesMount.size()-GeneralConstants.ONE_VALUE_INTEGER).getPenaltyAmount().intValue() + 50;
				}
			}else{
				//si no hay sanciones con monto mayor a 0
				if(selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_FUNDS.getCode())
						|| selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_STOCK.getCode()) ){
					sanctionAmount =  150;
				}else{
					sanctionAmount =  50;
				}
			}
		}else{
			//Si la lista esta vacia
			if(selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_FUNDS.getCode())
					|| selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_STOCK.getCode()) ){
				sanctionAmount =  150;
			}else{
				sanctionAmount =  50;
			}
		}
		
//		if(selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_FUNDS.getCode())
//				|| selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.UNFULFILLMENT_STOCK.getCode()) ){
//			
//			if(sanctionFilter.getQuantitySanction()< GeneralConstants.THREE_VALUE_INTEGER){
//				sanctionAmount =  150;
//			}else{
//				sanctionAmount = (50 * sanctionFilter.getQuantitySanction()) + 50;
//			}
//		}else{
//			sanctionAmount = (50 * sanctionFilter.getQuantitySanction()) + 50;
//		}
		
		BigDecimal sanctionAmountBD = new BigDecimal(sanctionAmount);
		sanctionFilter.setSanctionAmountHist(sanctionAmountBD);
		sanctionFilter.setSanctionAmount(sanctionAmountBD);
		
		if(SanctionReasonType.OTHER.getCode().equals(selectedSanctionMotive.getSanctionMotive())){
			renderOtherMotive = true;
		//	enableAmount = true;
		}

		
		getOperationsByMotive(sanctionFilter.getIdParticipant(),sanctionFilter.getSettlementDate(),null);
		
	}
	
	/**
	 * Gets the operations by motive.
	 *
	 * @param idParticipant the id participant
	 * @param settlementDate the settlement date
	 * @param idParticipantPenalty the id participant penalty
	 * @return the operations by motive
	 * @throws ServiceException the service exception
	 */
	private void getOperationsByMotive(Long idParticipant, Date settlementDate, Long idParticipantPenalty) throws ServiceException {
		// get operations 
		if(!selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.OTHER.getCode())){
			
			List<NegotiationModality> negotiationModalities = mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
					NegotiationMechanismType.BOLSA.getCode(),sanctionFilter.getIdParticipant());
			
			for(SanctionMotiveTO sanctionMotive : lstSanctionMotive){
				sanctionMotive.setLstSanctionByModalityTO(new ArrayList<SanctionByModalityTO>());
				
				for(NegotiationModality negotiationModality : negotiationModalities){
					SanctionByModalityTO sanctionByModalityTO = new SanctionByModalityTO();
					sanctionByModalityTO.setIdModality(negotiationModality.getIdNegotiationModalityPk());
					sanctionByModalityTO.setModalityName(negotiationModality.getModalityName());
					sanctionByModalityTO.setOperationCount(0);
					sanctionMotive.getLstSanctionByModalityTO().add(sanctionByModalityTO);
				}
			}
			
			List<SanctionOperationTO> operations = sanctionServiceFacade.getListMechanismOperationSearch(
					NegotiationMechanismType.BOLSA.getCode(),idParticipant,selectedSanctionMotive.getSanctionMotive(),settlementDate,idParticipantPenalty);
			for (SanctionOperationTO sanctionOperationTO : operations) {
				for (SanctionByModalityTO sanctionByModalityTO : selectedSanctionMotive.getLstSanctionByModalityTO()) {
					if(sanctionByModalityTO.getIdModality().equals(sanctionOperationTO.getIdModality())){
						sanctionByModalityTO.setOperationCount(sanctionByModalityTO.getOperationCount()+1);
						if(sanctionByModalityTO.getListSettlementOperations() == null){
							sanctionByModalityTO.setListSettlementOperations(new ArrayList<SanctionOperationTO>());
						}
						sanctionOperationTO.setSanctionByModalityTO(sanctionByModalityTO);
						sanctionByModalityTO.getListSettlementOperations().add(sanctionOperationTO);
						if(idParticipantPenalty==null){
							sanctionOperationTO.setSelected(true);
							sanctionByModalityTO.setSelected(true);
						}else{
							onSelectOperationByModalityCount(sanctionByModalityTO);
						}
						break;
					}
				}
			}
		}
	}

	/**
	 * On select operation by modality count.
	 *
	 * @param sanctionByModalityTO the sanction by modality to
	 */
	public void onSelectOperationByModalityCount(SanctionByModalityTO sanctionByModalityTO){
		selectedSanctionMotive.setCurrentSanctionByModality(sanctionByModalityTO);
	}
	
	/**
	 * On select operation by modality.
	 *
	 * @param sanctionByModalityTO the sanction by modality to
	 */
	public void onSelectOperationByModality(SanctionByModalityTO sanctionByModalityTO){
		if(sanctionByModalityTO.getListSettlementOperations()!=null){
			for (SanctionOperationTO sanctionOperationTO : sanctionByModalityTO.getListSettlementOperations()) {
				if(sanctionByModalityTO.isSelected()){
					sanctionOperationTO.setSelected(true);
				}else{
					sanctionOperationTO.setSelected(false);
				}
			}
		}
	}
	
	/**
	 * On select operation.
	 *
	 * @param sanctionOperationTO the sanction operation to
	 */
	public void onSelectOperation(SanctionOperationTO sanctionOperationTO){
		if(isTotalSelectedOperations(sanctionOperationTO.getSanctionByModalityTO())){
			sanctionOperationTO.getSanctionByModalityTO().setSelected(true);
		}else{
			sanctionOperationTO.getSanctionByModalityTO().setSelected(false);
		}
	}
	
	/**
	 * Checks if is total selected operations.
	 *
	 * @param sanctionByModalityTO the sanction by modality to
	 * @return true, if is total selected operations
	 */
	public boolean isTotalSelectedOperations(SanctionByModalityTO sanctionByModalityTO){
		if(sanctionByModalityTO.getListSettlementOperations()!=null){
			for (SanctionOperationTO sanctionOperationTO : sanctionByModalityTO.getListSettlementOperations()) {
				if(!sanctionOperationTO.isSelected()){
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Gets the list total selected operations.
	 *
	 * @param getAll the get all
	 * @return the list total selected operations
	 */
	public List<SanctionOperationTO> getListTotalSelectedOperations(boolean getAll){
		List<SanctionOperationTO> totalOperations = null;
		if(Validations.validateIsNotNull(selectedSanctionMotive)){
			totalOperations = new ArrayList<SanctionOperationTO>();
			if(selectedSanctionMotive.getLstSanctionByModalityTO() != null){
				for(SanctionByModalityTO sanctionByModalityTO : selectedSanctionMotive.getLstSanctionByModalityTO()){
					if(sanctionByModalityTO.getListSettlementOperations()!=null){
						for (SanctionOperationTO sanctionOperationTO : sanctionByModalityTO.getListSettlementOperations()) {
							if(getAll){
								totalOperations.add(sanctionOperationTO);
							}else{
								if(sanctionOperationTO.isSelected()){
									totalOperations.add(sanctionOperationTO);
								}
							}
						}
					}
				}
			}
		}
		return totalOperations;
	}

	/**
     * Begin register action listener.
     *
     * @return the string
     */
	@LoggerAuditWeb
    public String beginRegisterAction(){
		
		try {
			if(selectedSanctionMotive == null){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_SANCTION_SANCTION_MOTIVE_REQUIRED));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}else{
				if(!selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.OTHER.getCode())){
					if (Validations.validateListIsNullOrEmpty(getListTotalSelectedOperations(false))){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_SANCTION_OPERATION_REQUIRED));
						JSFUtilities.showSimpleValidationDialog();
						return null;
						
					}
				}
				//issue 642
//				else if(selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.OTHER.getCode())){
//					//If it's Sanction Others
//					if(!validateRangeSanction()){
//						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
//								PropertiesUtilities.getMessage(PropertiesConstants.MSG_SANCTION_SANCTION_AMOUNT_FAILED));
//						JSFUtilities.showSimpleValidationDialog();
//						return null;
//					}
//				}
			}
			/**Cuando es otros que coincida los rangos*/
			
			if(haveParticipantSanction()){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_SANCTION_SANCTION_MOTIVE_REPEAT));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));

		}
		
		if(!isViewOperationModify()){
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REGISTER_SANCTION_PENALTY,
							new Object[]{getSelectedParticipant(sanctionFilter.getIdParticipant()).getDisplayCodeMnemonic()}));
			
			JSFUtilities.executeJavascriptFunction("PF('cnfwRegisterSanctionParticipant').show()");
		}else{
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_MODIFY_SANCTION_PENALTY,
							new Object[]{getSelectedParticipant(sanctionFilter.getIdParticipant()).getDisplayCodeMnemonic()}));
			
			JSFUtilities.executeJavascriptFunction("PF('cnfwModifySanctionParticipant').show()");
		}
		

		return null;
    }
	
	/**
	 * Begin modify action listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
    public void beginModifyAction(ActionEvent actionEvent){
		Object[] bodyData = new Object[0];
	
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_MODIFY_SANCTION_PENALTY,
						bodyData));

		JSFUtilities.executeJavascriptFunction("PF('cnfwModifySanctionParticipant').show()");

    }
	
	/**
	 * Begin confirm action listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
    public void beginConfirmAction(ActionEvent actionEvent){
		Object[] bodyData = new Object[0];
		
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_CONFIRM_SANCTION_PENALTY,
						bodyData));

		JSFUtilities.executeJavascriptFunction("PF('cnfwModifySanctionParticipant').show()");

    }
	
	/**
	 * Begin delete action listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
    public void beginDeleteAction(ActionEvent actionEvent){
		Object[] bodyData = new Object[0];
		
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DELETE_SANCTION_PENALTY,
						bodyData));

		JSFUtilities.executeJavascriptFunction("PF('cnfwModifySanctionParticipant').show()");

    }
	
	/**
	 * Begin appeal action listener.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
    public void beginAppealAction(ActionEvent actionEvent){
		Object[] bodyData = new Object[0];
		
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPEAL),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_APPEAL_SANCTION_PENALTY,
						bodyData));

		JSFUtilities.executeJavascriptFunction("PF('cnfwModifySanctionParticipant').show()");

    }
	
	/**
	 * Initialize sanction for save.
	 *
	 * @param actionEvent the action event
	 */
	@LoggerAuditWeb
    public void saveRegisterSanctionParticipant(ActionEvent actionEvent){
    	
    	try {
    		ParticipantPenalty participantPenalty = new ParticipantPenalty();
    		participantPenalty.setParticipant(getSelectedParticipant(sanctionFilter.getIdParticipant()));
    		participantPenalty.setPenaltyAmount(sanctionFilter.getSanctionAmount());
    		participantPenalty.setPenaltyMotiveOther(sanctionFilter.getMotiveOther());
    		participantPenalty.setSettlementDate(sanctionFilter.getSettlementDate());
    		participantPenalty.setPenaltyPeriod(sanctionFilter.getIdPeriodSanction());
    		participantPenalty.setCurrency(sanctionFilter.getCurrency());
    		if(enableReprimand){
    			//Si es amonestacion se deja en Penalty Level = 0 
    			participantPenalty.setPenaltyLevel(0);
    		}else{
    			participantPenalty.setPenaltyLevel(sanctionFilter.getSanctionCount());
    		}
    		
    		participantPenalty.setPenaltyMotive(selectedSanctionMotive.getSanctionMotive());
    		participantPenalty.setPenaltyExpireDate(datePeriodEnd);
    		participantPenalty.setPenaltyState(SanctionStateType.REGISTERED.getCode());
    		
    		List<SanctionOperationTO> lstOperations =  null;
    		if(!selectedSanctionMotive.getSanctionMotive().equals(SanctionReasonType.OTHER.getCode())){
    			lstOperations =  getListTotalSelectedOperations(false);
    		}
    		sanctionServiceFacade.registryParticipantPenalty(participantPenalty,lstOperations);
    	
    		Object[] bodyData = new Object[2];
    		
    		bodyData[0]=participantPenalty.getParticipant().getDisplayCodeMnemonic();
    		bodyData[1]=participantPenalty.getIdParticipantPenaltyPk().toString();

    		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
    				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_REGISTER_SANCTION_PENALTY,
    						bodyData));

    		JSFUtilities.executeJavascriptFunction("PF('cnfwEndTransactionParticipant').show()"); //ejecuta javascript

    		sendNotification(participantPenalty, participantPenalty.getParticipant(), BusinessProcessType.SANCTION_REGISTER.getCode());
    		
    	} catch (ServiceException e) {
    		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)						
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
    }
	
	/**
	 * Gets the selected participant.
	 *
	 * @param idParticipant the id participant
	 * @return the selected participant
	 */
	private Participant getSelectedParticipant(Long idParticipant) {
		for(Participant participant : listParticipant){
			if(participant.getIdParticipantPk().equals(idParticipant)){
				return participant;
			}
		}
		return null;
	}

	/**
	 * Send notification.
	 *
	 * @param participantPenalty the participant penalty
	 * @param participant the participant
	 * @param idBusinessProcess the id business process
	 */
	public void sendNotification(ParticipantPenalty participantPenalty, Participant participant, Long idBusinessProcess){
		BusinessProcess businessProcess = new BusinessProcess();
	    businessProcess.setIdBusinessProcessPk(idBusinessProcess);
	    Object[] parameters = new Object[4];
	    parameters[0] = participantPenalty.getIdParticipantPenaltyPk().toString();
	    if(Validations.validateIsNotNullAndNotEmpty(participantPenalty.getResolutionNumber())) {
	    	parameters[1] = participantPenalty.getResolutionNumber();	 
	    } else {
	    	parameters[1] = GeneralConstants.N_A;
	    }	
	    parameters[2] = participant.getMnemonic();
	    parameters[3] = mapDescriptions.get(participantPenalty.getPenaltyMotive()).toString();	    
	    notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
	    		participant.getIdParticipantPk(), parameters);
	}
	
	/**
	 * Clean appeal view.
	 */
	@LoggerAuditWeb
	public void cleanAppealView() {
		JSFUtilities.resetViewRoot();
		sanctionFilter.setAppealDate(null);
		sanctionFilter.setComments(null);
	}
	
	/**
	 * Clean sanction participant request mgmt.
	 */
	@LoggerAuditWeb
	public void cleanSanctionParticipantRequestMgmt() {
		cleanSanctionParticipantRequest();
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Clean sanction participant request.
	 */	
	private void cleanSanctionParticipantRequest(){
		try{
			
			selectedSanctionMotive = null;
			renderOtherMotive = false;
			lstSanctionMotive = null;
			sanctionFilter = new SanctionRequestTO();
			sanctionFilter.setRegisterDate(CommonsUtilities.currentDate());
			sanctionFilter.setSettlementDate(CommonsUtilities.currentDate());
		
			definitionPeriodSanction(CommonsUtilities.currentDate());
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean motives data.
	 */
	public void cleanMotivesData(){
		selectedSanctionMotive = null;
		renderOtherMotive = false;
		sanctionFilter.setSanctionAmount(null);
		lstSanctionMotive = null;
	}
	
	/**
	 * View request.
	 *
	 * @param sanctionRequestTO the sanction request to
	 * @return the string
	 */
	@LoggerAuditWeb
	public String viewRequest(SanctionRequestTO sanctionRequestTO){
		sanctionFilter = sanctionRequestTO;
		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		searchParticipantPenaltyOperation();
		return "modifyParticipantPenalty";
	}
	/**
	 * Validate request delete state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestDelete(){
		  if (sanctionFilter == null) {
			   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					   	PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			   JSFUtilities.showSimpleValidationDialog();
			   return null;
		  } else {
		  
			    if (!sanctionFilter.getRequestState().equals(SanctionStateType.REGISTERED.getCode()) &&
			    		!sanctionFilter.getRequestState().equals(SanctionStateType.MODIFIED.getCode())) {
				   JSFUtilities.showMessageOnDialog(
						   PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),      
						   PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_MODIFY_INVALID));
				   JSFUtilities.showSimpleValidationDialog();
				   return null;
			    }
				
			    setViewOperationType(ViewOperationsType.ANULATE.getCode());
				searchParticipantPenaltyOperation();
				return "modifyParticipantPenalty";
		  }
	}
	
	/**
	 * Validate request appeal state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestAppeal(){
		if (sanctionFilter == null) {
			   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					   	PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			   JSFUtilities.showSimpleValidationDialog();
			   return null;
		  } else {
		  
			    if (!sanctionFilter.getRequestState().equals(SanctionStateType.CONFIRMED.getCode())) {
				   JSFUtilities.showMessageOnDialog(
						   PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),      
						   PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_APPEAL_INVALID));
				   JSFUtilities.showSimpleValidationDialog();
				   return null;
			    }
				      
				setViewOperationType(ViewOperationsType.APPEAL.getCode());
				searchParticipantPenaltyOperation();
				return "modifyParticipantPenalty";
		  }
	}
	
	
	/**
	 * Validate request modify state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestModify(){

		if (sanctionFilter == null) {
			   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					   	PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			   JSFUtilities.showSimpleValidationDialog();
			   return null;
		  } else {
		  
			    if (!sanctionFilter.getRequestState().equals(SanctionStateType.REGISTERED.getCode()) && 
			    		!sanctionFilter.getRequestState().equals(SanctionStateType.MODIFIED.getCode())) {
				   JSFUtilities.showMessageOnDialog(
						   PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),      
						   PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_MODIFY_INVALID));
				   JSFUtilities.showSimpleValidationDialog();
				   return null;
			    }
				      
				setViewOperationType(ViewOperationsType.MODIFY.getCode());
				return searchParticipantPenaltyOperation();
		  }
	}
	
	/**
	 * Validate request confirm state.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateRequestConfirmState(){
		
		if (sanctionFilter == null) {
			   JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					   	PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			   JSFUtilities.showSimpleValidationDialog();
			   return null;
		  } else {
		  
		    if (!sanctionFilter.getRequestState().equals(SanctionStateType.REGISTERED.getCode()) && 
		    		!sanctionFilter.getRequestState().equals(SanctionStateType.MODIFIED.getCode())) {
			   JSFUtilities.showMessageOnDialog(
					   PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),      
					   PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_MODIFY_INVALID));
			   JSFUtilities.showSimpleValidationDialog();
			   return null;
		    }
			      
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			searchParticipantPenaltyOperation();
			return "modifyParticipantPenalty";
		 }
		
	}
	
	
	/**
	 * searchParticipantPenaltyOperation for register.
	 *
	 * @return the string
	 * @throw Exception e
	 */
	public String searchParticipantPenaltyOperation(){
		
		try {
			if(!isViewOperationModify()){
				
				if(isViewOperationAppeal()){
					if(sanctionFilter.getEmitionDate()!=null){
						sanctionFilter.setMaxAppealDate(holidayQueryServiceBean.getCalculateDate(sanctionFilter.getEmitionDate(), 1, 1, null));
					}
				}
				
				if(!sanctionFilter.getSanctionMotive().equals(SanctionReasonType.OTHER.getCode())){
					sanctionFilter.setLstSettlementOperations(sanctionServiceFacade.getListParticipantPenaltyOperationSearch(sanctionFilter.getIdSanctionRequest()));
				}
				
			}else{
				definitionPeriodSanction(sanctionFilter.getRegisterDate());
				
				searchSanctionMotives(sanctionFilter.getSanctionMotive());
				
				getOperationsByMotive(sanctionFilter.getIdParticipant(),sanctionFilter.getSettlementDate(),sanctionFilter.getIdSanctionRequest());
				
				if(sanctionFilter.getMotiveOther()!=null){
					renderOtherMotive = true;
					enableAmount = true;
				}
				
				return "registerParticipantPenalty";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Action do.
	 */
	@LoggerAuditWeb
	public void actionDo(){

		if (isViewOperationConfirm()){
			saveConfirmSanctionParticipant();
		}else if (isViewOperationAnnul()){
			saveDeleteSanctionParticipant();
		}else if (isViewOperationAppeal()){
			saveAppealSanctionParticipant();
		}
	}
	
	/**
	 * Save Appeal Sanction Participant for change state.
	 *
	 * @throw Exception e
	 */
	@LoggerAuditWeb
	public void saveAppealSanctionParticipant(){
    	try {
    		
    		ParticipantPenalty participantPenalty = sanctionServiceFacade.appealParticipantPenalty(sanctionFilter);
    	
    		Object[] bodyData = new Object[3];
    		bodyData[0] = sanctionFilter.getIdSanctionRequest().toString();
    		bodyData[1] = sanctionFilter.getParticipant().getDisplayCodeMnemonic();
    		bodyData[2] = sanctionFilter.getMotiveDescription();
    		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPEAL),
    				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_APPEAL_SANCTION_PENALTY,
    						bodyData));

    		JSFUtilities.executeJavascriptFunction("PF('cnfwEndTransactionParticipant').show()"); 
    		
    		sendNotification(participantPenalty,sanctionFilter.getParticipant(),BusinessProcessType.SANCTION_APPEAL.getCode());
    		
    	} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
    }
	
	/**
	 * Save Delete Sanction Participant for change state.
	 *
	 * @throw Exception e
	 */
	@LoggerAuditWeb
	public void saveDeleteSanctionParticipant(){
    	try {
    		
    		ParticipantPenalty participantPenalty = sanctionServiceFacade.deleteParticipantPenalty(sanctionFilter);
    	
    		Object[] bodyData = new Object[3];
    		bodyData[0] = sanctionFilter.getIdSanctionRequest().toString();
    		bodyData[1] = sanctionFilter.getParticipant().getDisplayCodeMnemonic();
    		bodyData[2] = sanctionFilter.getMotiveDescription();
    		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE),
    				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_DELETE_SANCTION_PENALTY,
    						bodyData));

    		JSFUtilities.executeJavascriptFunction("PF('cnfwEndTransactionParticipant').show()"); 
    		
    		sendNotification(participantPenalty,sanctionFilter.getParticipant(),BusinessProcessType.SANCTION_DELETE.getCode());
    		
    	} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
    }
	
	/**
	 * Save Confirm Sanction Participant for change state.
	 *
	 * @throw Exception e
	 */
	@LoggerAuditWeb
	public void saveConfirmSanctionParticipant(){
    	try {
    		boolean reprimand=false;
    		ParticipantPenalty participantPenalty = sanctionServiceFacade.confirmParticipantPenalty(sanctionFilter);
    		if(participantPenalty.getPenaltyAmount().compareTo(BigDecimal.ZERO)==0){
    			reprimand=true;
    		}
    		
    		Map<String,String> parameters = new HashMap<String, String>();
    		
    		Object[] bodyData = new Object[5];
    		bodyData[0] = sanctionFilter.getIdSanctionRequest().toString();
    		bodyData[1] = sanctionFilter.getParticipant().getDisplayCodeMnemonic();
    		bodyData[2] = participantPenalty.getResolutionNumber().toString();
    		bodyData[3] = CommonsUtilities.convertDatetoString(participantPenalty.getPenaltyEmitionDate());
    		bodyData[4] = sanctionFilter.getMotiveDescription();
    		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
    				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_CONFIRM_SANCTION_PENALTY,
    						bodyData));

    		JSFUtilities.executeJavascriptFunction("PF('cnfwEndTransactionParticipant').show()"); 
    		
    		sendNotification(participantPenalty,sanctionFilter.getParticipant(),BusinessProcessType.SANCTION_CONFIRM.getCode());
    		
    		parameters.put("idParticipantPenalty",participantPenalty.getIdParticipantPenaltyPk().toString());
    		
    		sanctionServiceFacade.sendRecordsOnSanctions(parameters,sanctionFilter.getParticipant().getIdParticipantPk(),participantPenalty.getPenaltyMotive(),reprimand);
    		
    	} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
    }
	
	/**
	 * Save Modify Sanction Participant for change state.
	 *
	 * @throw Exception e
	 */
	@LoggerAuditWeb
    public void saveModifySanctionParticipant(){
    	try {
    		
    		ParticipantPenalty participantPenalty = sanctionServiceFacade.modifyParticipantPenalty(sanctionFilter,getListTotalSelectedOperations(true));
    	
    		Object[] bodyData = new Object[3];
    		bodyData[0] = sanctionFilter.getIdSanctionRequest().toString();
    		bodyData[1] = sanctionFilter.getParticipant().getDisplayCodeMnemonic();
    		bodyData[2] = sanctionFilter.getMotiveDescription();
    		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
    				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_MODIFY_SANCTION_PENALTY,
    						bodyData));

    		JSFUtilities.executeJavascriptFunction("PF('cnfwEndTransactionParticipant').show()"); 
    		
    		sendNotification(participantPenalty,sanctionFilter.getParticipant(),BusinessProcessType.SANCTION_MODIFY.getCode());
    		
    	} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
    }
	
	/**
	 * Sets the true valid buttons.
	 */
	public void showPrivilegeButtons(){
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);	
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnModifyView(true);
		privilegeComponent.setBtnAppeal(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Search the Max Sanction or Breach by Type Sanction and
	 * Assignment All the Type
	 * @param lstSanctionMotive 
	 */
	public void orderSanctionMotive(List<SanctionMotiveTO> lstSanctionMotive){
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstSanctionMotive)){
			/**
			 * Search the Max Sanction or Breach by Type Sanction and
			 * Assignment All the Type
			 */
			
			TreeSet<Integer> listSanct=  new TreeSet<Integer>();
			TreeSet<Integer> listBreach=  new TreeSet<Integer>();
			TreeSet<Integer> listOthers=  new TreeSet<Integer>();
			
			for (SanctionMotiveTO sanctionMotive : lstSanctionMotive) {
				/**Only Sanctions**/
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(sanctionMotive.getTypeSanction())&&
						Validations.validateIsNotNull(sanctionMotive.getSanctionCount())){
					listSanct.add(sanctionMotive.getSanctionCount());
				/**Only Breach*/
				}else if(GeneralConstants.THREE_VALUE_INTEGER.equals(sanctionMotive.getTypeSanction())&&
						Validations.validateIsNotNull(sanctionMotive.getSanctionCount())){
					listBreach.add(sanctionMotive.getSanctionCount());
				/**Only Others*/
				}else if(GeneralConstants.TWO_VALUE_INTEGER.equals(sanctionMotive.getTypeSanction())&&
						Validations.validateIsNotNull(sanctionMotive.getSanctionCount())){
					listOthers.add(sanctionMotive.getSanctionCount());
				}
			}
			
			Integer cantSanct = new Integer(0), cantBreach = new Integer(0), cantOther = new Integer(0);
			
			for(Integer cantlist : listSanct ){ cantSanct  = cantSanct  + cantlist; }
			for(Integer cantlist : listBreach){ cantBreach = cantBreach + cantlist; }
			for(Integer cantlist : listOthers){ cantOther  = cantOther  + cantlist; }
			
			sanctionFilter.setQuantitySanction(cantSanct);
			sanctionFilter.setQuantityBreach(cantBreach);
			sanctionFilter.setQuantityOthersSanctions(cantOther);
			
			for (SanctionMotiveTO sanctionMotive : lstSanctionMotive) {
				/**Only Sanctions**/
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(sanctionMotive.getTypeSanction())&&
						Validations.validateIsNotNull(sanctionMotive.getSanctionCount())){
					sanctionMotive.setSanctionCount(sanctionFilter.getQuantitySanction());
				/**Only Breach*/
				}else if(GeneralConstants.THREE_VALUE_INTEGER.equals(sanctionMotive.getTypeSanction())&&
						Validations.validateIsNotNull(sanctionMotive.getSanctionCount())){
					sanctionMotive.setSanctionCount(sanctionFilter.getQuantityBreach());
				/**Only Others*/
				}else if(GeneralConstants.TWO_VALUE_INTEGER.equals(sanctionMotive.getTypeSanction())&&
						Validations.validateIsNotNull(sanctionMotive.getSanctionCount())){
					sanctionMotive.setSanctionCount(sanctionFilter.getQuantityOthersSanctions());
				}
			}
		}else{
			sanctionFilter.setQuantitySanction(0);
			sanctionFilter.setQuantityBreach(0);
			sanctionFilter.setQuantityOthersSanctions(0);
		}
	}
	
	/**
	 * Verify If Participant Have Sanctions in with Type Motive Sanction and same Date Operation
	 * @param sanctionMotiveTo
	 * @return
	 * @throws ServiceException 
	 */
	public boolean haveParticipantSanction() throws ServiceException{
		return sanctionServiceFacade.haveParticipantSanction(selectedSanctionMotive, sanctionFilter);
	}
	
	/**
	 * Validate Range Sanctions
	 * @return
	 */
	public boolean validateRangeSanction() throws ServiceException{
		return sanctionServiceFacade.validateRangeSanction(selectedSanctionMotive, sanctionFilter);
	}
	
	/**
	 * Check reprimand
	 */
	public void checkReprimand(){
		
		if(enableReprimand){
			sanctionFilter.setSanctionAmount(BigDecimal.ZERO);
		}else{
			sanctionFilter.setSanctionAmount(sanctionFilter.getSanctionAmountHist());
		}
	}
	
	
	/**
	 * Gets the list participant.
	 *
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}

	
	/**
	 * Gets the sanction filter.
	 *
	 * @return the sanction filter
	 */
	public SanctionRequestTO getSanctionFilter() {
		return sanctionFilter;
	}

	/**
	 * Sets the sanction filter.
	 *
	 * @param sanctionFilter the new sanction filter
	 */
	public void setSanctionFilter(SanctionRequestTO sanctionFilter) {
		this.sanctionFilter = sanctionFilter;
	}

	/**
	 * Gets the lst level sanction participant.
	 *
	 * @return the lst level sanction participant
	 */
	public List<ParameterTable> getLstLevelSanctionParticipant() {
		return lstLevelSanctionParticipant;
	}

	/**
	 * Sets the lst level sanction participant.
	 *
	 * @param lstLevelSanctionParticipant the new lst level sanction participant
	 */
	public void setLstLevelSanctionParticipant(
			List<ParameterTable> lstLevelSanctionParticipant) {
		this.lstLevelSanctionParticipant = lstLevelSanctionParticipant;
	}
	
	/**
	 * Gets the lst state sanction participant.
	 *
	 * @return the lst state sanction participant
	 */
	public List<ParameterTable> getLstStateSanctionParticipant() {
		return lstStateSanctionParticipant;
	}

	/**
	 * Sets the lst state sanction participant.
	 *
	 * @param lstStateSanctionParticipant the new lst state sanction participant
	 */
	public void setLstStateSanctionParticipant(
			List<ParameterTable> lstStateSanctionParticipant) {
		this.lstStateSanctionParticipant = lstStateSanctionParticipant;
	}
	
	/**
	 * Gets the lst reason sanction participant.
	 *
	 * @return the lst reason sanction participant
	 */
	public List<ParameterTable> getLstReasonSanctionParticipant() {
		return lstReasonSanctionParticipant;
	}

	/**
	 * Sets the lst reason sanction participant.
	 *
	 * @param lstReasonSanctionParticipant the new lst reason sanction participant
	 */
	public void setLstReasonSanctionParticipant(
			List<ParameterTable> lstReasonSanctionParticipant) {
		this.lstReasonSanctionParticipant = lstReasonSanctionParticipant;
	}

	/**
	 * Gets the lst participant state for search.
	 *
	 * @return the lst participant state for search
	 */
	public List<ParameterTable> getLstParticipantStateForSearch() {
		return lstParticipantStateForSearch;
	}

	/**
	 * Sets the lst participant state for search.
	 *
	 * @param lstParticipantStateForSearch the new lst participant state for search
	 */
	public void setLstParticipantStateForSearch(
			List<ParameterTable> lstParticipantStateForSearch) {
		this.lstParticipantStateForSearch = lstParticipantStateForSearch;
	}

	/**
	 * Checks if is render other motive.
	 *
	 * @return true, if is render other motive
	 */
	public boolean isRenderOtherMotive() {
		return renderOtherMotive;
	}

	/**
	 * Sets the render other motive.
	 *
	 * @param renderOtherMotive the new render other motive
	 */
	public void setRenderOtherMotive(boolean renderOtherMotive) {
		this.renderOtherMotive = renderOtherMotive;
	}

	/**
	 * Gets the date period ini.
	 *
	 * @return the date period ini
	 */
	public Date getDatePeriodIni() {
		return datePeriodIni;
	}

	/**
	 * Sets the date period ini.
	 *
	 * @param datePeriodIni the new date period ini
	 */
	public void setDatePeriodIni(Date datePeriodIni) {
		this.datePeriodIni = datePeriodIni;
	}

	/**
	 * Gets the date period end.
	 *
	 * @return the date period end
	 */
	public Date getDatePeriodEnd() {
		return datePeriodEnd;
	}

	/**
	 * Sets the date period end.
	 *
	 * @param datePeriodEnd the new date period end
	 */
	public void setDatePeriodEnd(Date datePeriodEnd) {
		this.datePeriodEnd = datePeriodEnd;
	}

	/**
	 * Gets the sanction request data model.
	 *
	 * @return the sanction request data model
	 */
	public GenericDataModel<SanctionRequestTO> getSanctionRequestDataModel() {
		return sanctionRequestDataModel;
	}

	/**
	 * Sets the sanction request data model.
	 *
	 * @param sanctionRequestDataModel the new sanction request data model
	 */
	public void setSanctionRequestDataModel(
			GenericDataModel<SanctionRequestTO> sanctionRequestDataModel) {
		this.sanctionRequestDataModel = sanctionRequestDataModel;
	}

	/**
	 * Gets the lst sanction motive.
	 *
	 * @return the lst sanction motive
	 */
	public List<SanctionMotiveTO> getLstSanctionMotive() {
		return lstSanctionMotive;
	}

	/**
	 * Sets the lst sanction motive.
	 *
	 * @param lstSanctionMotive the new lst sanction motive
	 */
	public void setLstSanctionMotive(List<SanctionMotiveTO> lstSanctionMotive) {
		this.lstSanctionMotive = lstSanctionMotive;
	}

	/**
	 * Gets the selected sanction motive.
	 *
	 * @return the selected sanction motive
	 */
	public SanctionMotiveTO getSelectedSanctionMotive() {
		return selectedSanctionMotive;
	}

	/**
	 * Sets the selected sanction motive.
	 *
	 * @param selectedSanctionMotive the new selected sanction motive
	 */
	public void setSelectedSanctionMotive(SanctionMotiveTO selectedSanctionMotive) {
		this.selectedSanctionMotive = selectedSanctionMotive;
	}

	/**
	 * Gets the lst currencies.
	 *
	 * @return the lst currencies
	 */
	public List<ParameterTable> getLstCurrencies() {
		return lstCurrencies;
	}

	/**
	 * Sets the lst currencies.
	 *
	 * @param lstCurrencies the new lst currencies
	 */
	public void setLstCurrencies(List<ParameterTable> lstCurrencies) {
		this.lstCurrencies = lstCurrencies;
	}

	/**
	 * Gets the search sanction.
	 *
	 * @return the search sanction
	 */
	public SanctionRequestTO getSearchSanction() {
		return searchSanction;
	}

	/**
	 * Gets the radio selected.
	 *
	 * @return the radio selected
	 */
	public Integer getRadioSelected() {
		return radioSelected;
	}

	/**
	 * Sets the radio selected.
	 *
	 * @param radioSelected the new radio selected
	 */
	public void setRadioSelected(Integer radioSelected) {
		this.radioSelected = radioSelected;
	}

	/**
	 * Sets the search sanction.
	 *
	 * @param searchSanction the new search sanction
	 */
	public void setSearchSanction(SanctionRequestTO searchSanction) {
		this.searchSanction = searchSanction;
	}

	/**
	 * Checks if is enable amount.
	 *
	 * @return true, if is enable amount
	 */
	public boolean isEnableAmount() {
		return enableAmount;
	}

	/**
	 * Sets the enable amount.
	 *
	 * @param enableAmount the new enable amount
	 */
	public void setEnableAmount(boolean enableAmount) {
		this.enableAmount = enableAmount;
	}

	/**
	 * @return the enableReprimand
	 */
	public boolean isEnableReprimand() {
		return enableReprimand;
	}

	/**
	 * @param enableReprimand the enableReprimand to set
	 */
	public void setEnableReprimand(boolean enableReprimand) {
		this.enableReprimand = enableReprimand;
	}

	
}
