package com.pradera.settlements.sanction.to;

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SanctionByModalityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SanctionByModalityTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The selected. */
	private boolean selected;
	
	/** The id modality. */
	private Long idModality;
	
	/** The modality name. */
	private String modalityName;
	
	/** The operation count. */
	private Integer operationCount;
	
	/** The list settlement operations. */
	private List<SanctionOperationTO> listSettlementOperations;
	
	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}

	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}

	/**
	 * Gets the operation count.
	 *
	 * @return the operation count
	 */
	public Integer getOperationCount() {
		return operationCount;
	}

	/**
	 * Sets the operation count.
	 *
	 * @param operationCount the new operation count
	 */
	public void setOperationCount(Integer operationCount) {
		this.operationCount = operationCount;
	}

	/**
	 * Gets the list settlement operations.
	 *
	 * @return the list settlement operations
	 */
	public List<SanctionOperationTO> getListSettlementOperations() {
		return listSettlementOperations;
	}

	/**
	 * Sets the list settlement operations.
	 *
	 * @param listSettlementOperations the new list settlement operations
	 */
	public void setListSettlementOperations(
			List<SanctionOperationTO> listSettlementOperations) {
		this.listSettlementOperations = listSettlementOperations;
	}

	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}

	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	
}
