package com.pradera.settlements.sanction.batch;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.sanction.facade.SanctionServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ElectronicCatWebRegisterBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="SanctionMgmtServiceBatch")
@RequestScoped
public class SanctionMgmtServiceBatch extends GenericBaseBean implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The sanction service facade. */
	@EJB
	SanctionServiceFacade sanctionServiceFacade;
	
	@Inject
	private PraderaLogger log;

	/* (non-Javadoc)
	 * Batchero para registrar cat electronicos en la BD de la web
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		log.info("************************************* BATCH REINICIO DE SECUENCIA RESOLUTION_NUMBER  ******************************************************");
		
		try{
			sanctionServiceFacade.resetSequenceParticipantPenalty();
			}catch(Exception e){
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			
		System.out.println("************************************* BATCH REINICIO DE SECUENCIA RESOLUTION_NUMBER ******************************************************");

	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
