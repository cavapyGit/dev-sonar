package com.pradera.settlements.sanction.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SanctionRequestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SanctionRequestTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id sanction request. */
	private Long idSanctionRequest;

	/** The id participant. */
	private Long idParticipant;
	
	/** The participant. */
	private Participant participant;
	
	/** The register date. */
	private Date registerDate;
	
	/** The appeal date. */
	private Date appealDate;
	
	/** The min settlement date. */
	private Date minSettlementDate;
	
	/** The max appeal date. */
	private Date maxAppealDate;
	
	/** The expiration date. */
	private Date expirationDate;
	
	/** The emition date. */
	private Date emitionDate;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The resolution number. */
	private String resolutionNumber;
	
	/** The request state. */
	private Integer requestState;
	
	/** The state description. */
	private String stateDescription;
	
	/** The sanction motive. */
	private Integer sanctionMotive;
	
	/** The motive description. */
	private String motiveDescription;
	
	/** The motive other. */
	private String motiveOther;
	
	/** The comments. */
	private String comments;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The period sanction. */
	private String periodSanction;
	
	/** The id period sanction. */
	private Integer idPeriodSanction;
	
	/** The sanction amount. */
	private BigDecimal sanctionAmount;
	
	/** The sanction amount historic. */
	private BigDecimal sanctionAmountHist;
	
	/** The sanction count. */
	private Integer sanctionCount;
	
	/** The currency. */
	private Integer currency;
	
	/** The currency description. */
	private String currencyDescription;
	
	/** The lst settlement operations. */
	List<MechanismOperationTO> lstSettlementOperations;
	
	/** Issue 2719 Fields To Change Method Sanctions.
	 * quantitySanction and quantityBreach
	 *  */
	/** The count to Sanctions. */
	private Integer quantitySanction;
	
	/** The count to breach. */
	private Integer quantityBreach;
	
	/** The count to Sanctions Others. */
	private Integer quantityOthersSanctions;
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the sanction motive.
	 *
	 * @return the sanction motive
	 */
	public Integer getSanctionMotive() {
		return sanctionMotive;
	}

	/**
	 * Sets the sanction motive.
	 *
	 * @param sanctionMotive the new sanction motive
	 */
	public void setSanctionMotive(Integer sanctionMotive) {
		this.sanctionMotive = sanctionMotive;
	}

	/**
	 * Gets the lst settlement operations.
	 *
	 * @return the lst settlement operations
	 */
	public List<MechanismOperationTO> getLstSettlementOperations() {
		return lstSettlementOperations;
	}

	/**
	 * Sets the lst settlement operations.
	 *
	 * @param lstSettlementOperations the new lst settlement operations
	 */
	public void setLstSettlementOperations(
			List<MechanismOperationTO> lstSettlementOperations) {
		this.lstSettlementOperations = lstSettlementOperations;
	}

	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}

	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	/**
	 * Gets the motive other.
	 *
	 * @return the motive other
	 */
	public String getMotiveOther() {
		return motiveOther;
	}

	/**
	 * Sets the motive other.
	 *
	 * @param motiveOther the new motive other
	 */
	public void setMotiveOther(String motiveOther) {
		this.motiveOther = motiveOther;
	}

	/**
	 * Gets the period sanction.
	 *
	 * @return the period sanction
	 */
	public String getPeriodSanction() {
		return periodSanction;
	}

	/**
	 * Sets the period sanction.
	 *
	 * @param periodSanction the new period sanction
	 */
	public void setPeriodSanction(String periodSanction) {
		this.periodSanction = periodSanction;
	}

	/**
	 * Gets the sanction amount.
	 *
	 * @return the sanction amount
	 */
	public BigDecimal getSanctionAmount() {
		return sanctionAmount;
	}

	/**
	 * Sets the sanction amount.
	 *
	 * @param sanctionAmount the new sanction amount
	 */
	public void setSanctionAmount(BigDecimal sanctionAmount) {
		this.sanctionAmount = sanctionAmount;
	}

	/**
	 * Gets the id period sanction.
	 *
	 * @return the id period sanction
	 */
	public Integer getIdPeriodSanction() {
		return idPeriodSanction;
	}

	/**
	 * Sets the id period sanction.
	 *
	 * @param idPeriodSanction the new id period sanction
	 */
	public void setIdPeriodSanction(Integer idPeriodSanction) {
		this.idPeriodSanction = idPeriodSanction;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Gets the resolution number.
	 *
	 * @return the resolution number
	 */
	public String getResolutionNumber() {
		return resolutionNumber;
	}

	/**
	 * Sets the resolution number.
	 *
	 * @param resolutionNumber the new resolution number
	 */
	public void setResolutionNumber(String resolutionNumber) {
		this.resolutionNumber = resolutionNumber;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the id sanction request.
	 *
	 * @return the id sanction request
	 */
	public Long getIdSanctionRequest() {
		return idSanctionRequest;
	}

	/**
	 * Sets the id sanction request.
	 *
	 * @param idSanctionRequest the new id sanction request
	 */
	public void setIdSanctionRequest(Long idSanctionRequest) {
		this.idSanctionRequest = idSanctionRequest;
	}

	/**
	 * Gets the appeal date.
	 *
	 * @return the appeal date
	 */
	public Date getAppealDate() {
		return appealDate;
	}

	/**
	 * Sets the appeal date.
	 *
	 * @param appealDate the new appeal date
	 */
	public void setAppealDate(Date appealDate) {
		this.appealDate = appealDate;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the motive description.
	 *
	 * @return the motive description
	 */
	public String getMotiveDescription() {
		return motiveDescription;
	}

	/**
	 * Sets the motive description.
	 *
	 * @param motiveDescription the new motive description
	 */
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	/**
	 * Gets the emition date.
	 *
	 * @return the emition date
	 */
	public Date getEmitionDate() {
		return emitionDate;
	}

	/**
	 * Sets the emition date.
	 *
	 * @param emitionDate the new emition date
	 */
	public void setEmitionDate(Date emitionDate) {
		this.emitionDate = emitionDate;
	}

	/**
	 * Gets the max appeal date.
	 *
	 * @return the max appeal date
	 */
	public Date getMaxAppealDate() {
		return maxAppealDate;
	}

	/**
	 * Sets the max appeal date.
	 *
	 * @param maxAppealDate the new max appeal date
	 */
	public void setMaxAppealDate(Date maxAppealDate) {
		this.maxAppealDate = maxAppealDate;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the min settlement date.
	 *
	 * @return the min settlement date
	 */
	public Date getMinSettlementDate() {
		return minSettlementDate;
	}

	/**
	 * Sets the min settlement date.
	 *
	 * @param minSettlementDate the new min settlement date
	 */
	public void setMinSettlementDate(Date minSettlementDate) {
		this.minSettlementDate = minSettlementDate;
	}

	/**
	 * Gets the sanction count.
	 *
	 * @return the sanction count
	 */
	public Integer getSanctionCount() {
		return sanctionCount;
	}

	/**
	 * Sets the sanction count.
	 *
	 * @param sanctionCount the new sanction count
	 */
	public void setSanctionCount(Integer sanctionCount) {
		this.sanctionCount = sanctionCount;
	}



	/**
	 * @return the quantitySanction
	 */
	public Integer getQuantitySanction() {
		return quantitySanction;
	}



	/**
	 * @param quantitySanction the quantitySanction to set
	 */
	public void setQuantitySanction(Integer quantitySanction) {
		this.quantitySanction = quantitySanction;
	}



	/**
	 * @return the quantityBreach
	 */
	public Integer getQuantityBreach() {
		return quantityBreach;
	}



	/**
	 * @param quantityBreach the quantityBreach to set
	 */
	public void setQuantityBreach(Integer quantityBreach) {
		this.quantityBreach = quantityBreach;
	}



	/**
	 * @return the quantityOthersSanctions
	 */
	public Integer getQuantityOthersSanctions() {
		return quantityOthersSanctions;
	}



	/**
	 * @param quantityOthersSanctions the quantityOthersSanctions to set
	 */
	public void setQuantityOthersSanctions(Integer quantityOthersSanctions) {
		this.quantityOthersSanctions = quantityOthersSanctions;
	}



	public BigDecimal getSanctionAmountHist() {
		return sanctionAmountHist;
	}



	public void setSanctionAmountHist(BigDecimal sanctionAmountHist) {
		this.sanctionAmountHist = sanctionAmountHist;
	}



		
}
