package com.pradera.settlements.sanction.to;

import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SanctionOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SanctionOperationTO extends MechanismOperationTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant penalty operation. */
	private Long idParticipantPenaltyOperation;
	
	/** The selected. */
	private boolean selected;
	
	/** The state. */
	private Integer state;
	
	/** The sanction by modality to. */
	private SanctionByModalityTO sanctionByModalityTO;
	
	/**
	 * Gets the id participant penalty operation.
	 *
	 * @return the id participant penalty operation
	 */
	public Long getIdParticipantPenaltyOperation() {
		return idParticipantPenaltyOperation;
	}
	
	/**
	 * Sets the id participant penalty operation.
	 *
	 * @param idParticipantPenaltyOperation the new id participant penalty operation
	 */
	public void setIdParticipantPenaltyOperation(Long idParticipantPenaltyOperation) {
		this.idParticipantPenaltyOperation = idParticipantPenaltyOperation;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the sanction by modality to.
	 *
	 * @return the sanction by modality to
	 */
	public SanctionByModalityTO getSanctionByModalityTO() {
		return sanctionByModalityTO;
	}
	
	/**
	 * Sets the sanction by modality to.
	 *
	 * @param sanctionByModalityTO the new sanction by modality to
	 */
	public void setSanctionByModalityTO(SanctionByModalityTO sanctionByModalityTO) {
		this.sanctionByModalityTO = sanctionByModalityTO;
	}
	
	
	
}
