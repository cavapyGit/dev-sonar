package com.pradera.settlements.sanction.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.settlement.ParticipantPenalty;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SanctionMotiveTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SanctionMotiveTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The sanction count. */
	private Integer sanctionCount;
	
	/** The sanction motive. */
	private Integer sanctionMotive;
	
	/** The motive description. */
	private String motiveDescription;
	
	/** The participant penalties. */
	private List<ParticipantPenalty> participantPenalties;
	
	/** The lst sanction by modality to. */
	private List<SanctionByModalityTO> lstSanctionByModalityTO;
	
	/** The current sanction by modality. */
	private SanctionByModalityTO currentSanctionByModality;
	
	/** The Type Sanction*/
	private Integer typeSanction;
	
	/**
	 * Gets the participant penalties.
	 *
	 * @return the participant penalties
	 */
	public List<ParticipantPenalty> getParticipantPenalties() {
		return participantPenalties;
	}
	
	/**
	 * Sets the participant penalties.
	 *
	 * @param participantPenalties the new participant penalties
	 */
	public void setParticipantPenalties(
			List<ParticipantPenalty> participantPenalties) {
		this.participantPenalties = participantPenalties;
	}
	
	/**
	 * Gets the sanction count.
	 *
	 * @return the sanction count
	 */
	public Integer getSanctionCount() {
		return sanctionCount;
	}
	
	/**
	 * Sets the sanction count.
	 *
	 * @param sanctionCount the new sanction count
	 */
	public void setSanctionCount(Integer sanctionCount) {
		this.sanctionCount = sanctionCount;
	}
	
	/**
	 * Gets the sanction motive.
	 *
	 * @return the sanction motive
	 */
	public Integer getSanctionMotive() {
		return sanctionMotive;
	}
	
	/**
	 * Sets the sanction motive.
	 *
	 * @param sanctionMotive the new sanction motive
	 */
	public void setSanctionMotive(Integer sanctionMotive) {
		this.sanctionMotive = sanctionMotive;
	}
	
	/**
	 * Gets the motive description.
	 *
	 * @return the motive description
	 */
	public String getMotiveDescription() {
		return motiveDescription;
	}
	
	/**
	 * Sets the motive description.
	 *
	 * @param motiveDescription the new motive description
	 */
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}
	
	/**
	 * Gets the current sanction by modality.
	 *
	 * @return the current sanction by modality
	 */
	public SanctionByModalityTO getCurrentSanctionByModality() {
		return currentSanctionByModality;
	}
	
	/**
	 * Sets the current sanction by modality.
	 *
	 * @param currentSanctionByModality the new current sanction by modality
	 */
	public void setCurrentSanctionByModality(
			SanctionByModalityTO currentSanctionByModality) {
		this.currentSanctionByModality = currentSanctionByModality;
	}
	
	/**
	 * Gets the lst sanction by modality to.
	 *
	 * @return the lst sanction by modality to
	 */
	public List<SanctionByModalityTO> getLstSanctionByModalityTO() {
		return lstSanctionByModalityTO;
	}
	
	/**
	 * Sets the lst sanction by modality to.
	 *
	 * @param lstSanctionByModalityTO the new lst sanction by modality to
	 */
	public void setLstSanctionByModalityTO(
			List<SanctionByModalityTO> lstSanctionByModalityTO) {
		this.lstSanctionByModalityTO = lstSanctionByModalityTO;
	}

	/**
	 * @return the typeSanction
	 */
	public Integer getTypeSanction() {
		return typeSanction;
	}

	/**
	 * @param typeSanction the typeSanction to set
	 */
	public void setTypeSanction(Integer typeSanction) {
		this.typeSanction = typeSanction;
	}
	
	
}
