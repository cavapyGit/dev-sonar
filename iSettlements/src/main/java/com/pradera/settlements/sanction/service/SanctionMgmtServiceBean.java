package com.pradera.settlements.sanction.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.settlement.ParticipantPenalty;
import com.pradera.model.settlement.ParticipantPenaltyOperation;
import com.pradera.model.settlement.PenaltyLevel;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.SanctionOperationStateType;
import com.pradera.model.settlement.type.SanctionReasonType;
import com.pradera.model.settlement.type.SanctionStateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.model.settlement.type.UnfulfillmentParameterType;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.settlements.sanction.to.SanctionMotiveTO;
import com.pradera.settlements.sanction.to.SanctionOperationTO;
import com.pradera.settlements.sanction.to.SanctionRequestTO;

// TODO: Auto-generated Javadoc
/**
 * Session Bean implementation class SanctionMgmtServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SanctionMgmtServiceBean extends CrudDaoServiceBean{
	
//	@EJB
//	CrudDaoServiceBean crudDaoServiceBean;
	
	@EJB
	ParameterServiceBean parameterServiceBean;

    /**
     * Default constructor. 
     */
    public SanctionMgmtServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * Gets the list sanction by filters service bean.
	 *
	 * @param filter the filter
	 * @return the list sanction by filters service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SanctionRequestTO> getListSanctionByFiltersServiceBean(SanctionRequestTO filter) throws ServiceException{
		
		List<SanctionRequestTO> list = new ArrayList<SanctionRequestTO>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" Select ");
		sbQuery.append(" pp.idParticipantPenaltyPk, pp.resolutionNumber, ");//01
		sbQuery.append(" pp.penaltyAppealDate, pp.settlementDate, ");//23
		sbQuery.append(" pp.penaltyEmitionDate, pp.penaltyExpireDate, ");// 4  5
		sbQuery.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = pp.penaltyMotive), "); // 6 
		sbQuery.append(" pp.penaltyMotiveOther, pp.penaltyAmount, ");// 7 8 
		sbQuery.append(" pp.penaltyPeriod,"); // 9
		sbQuery.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = pp.penaltyState), ");// 10
		sbQuery.append(" pp.participant, ");//11
		sbQuery.append(" (select pt.description from ParameterTable pt where pt.parameterTablePk = pp.penaltyPeriod), "); // 12
		sbQuery.append(" pp.penaltyState, "); // 13
		sbQuery.append(" pp.penaltyMotive, "); // 14
		sbQuery.append(" pp.registerDate, "); // 15
		sbQuery.append(" pp.currency, "); // 16
		sbQuery.append(" pp.comments "); // 17
		sbQuery.append(" from ParticipantPenalty pp ");
		sbQuery.append(" Where 1 = 1 ");
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getSettlementDate())) {
			sbQuery.append(" And pp.settlementDate = :settlementDate");
			parameters.put("settlementDate", filter.getSettlementDate());
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipant())) {
			sbQuery.append(" And pp.participant.idParticipantPk = :participanSelected");
			parameters.put("participanSelected", filter.getIdParticipant());
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdSanctionRequest())) {
			sbQuery.append(" And pp.idParticipantPenaltyPk = :idParticipantPenaltyPk");
			parameters.put("idParticipantPenaltyPk", filter.getIdSanctionRequest());
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getResolutionNumber())) {
			sbQuery.append(" And pp.resolutionNumber = :resolutionNumber");
			parameters.put("resolutionNumber", filter.getResolutionNumber());
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getRequestState())) {
			sbQuery.append(" And pp.penaltyState = :penaltyState");
			parameters.put("penaltyState", filter.getRequestState());
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getSanctionMotive())) {
			sbQuery.append(" And pp.penaltyMotive = :penaltyMotive");
			parameters.put("penaltyMotive", filter.getSanctionMotive());
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getInitialDate())) {
			sbQuery.append(" And trunc(pp.registerDate) >= :registerDate");
			parameters.put("registerDate", filter.getInitialDate());
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getEndDate())) {
			sbQuery.append(" And trunc(pp.registerDate) <= :finalRegisterDate");
			parameters.put("finalRegisterDate", filter.getEndDate());
		}
		
		sbQuery.append(" order by  pp.resolutionNumber desc");
		
		List<Object[]> arrayList = findListByQueryString(sbQuery.toString(),parameters);
		
		for (Object[] arrSanctionObject : arrayList) {
			SanctionRequestTO sanctionRequestTO = new SanctionRequestTO();
			sanctionRequestTO.setIdSanctionRequest((Long)arrSanctionObject[0]);
			sanctionRequestTO.setResolutionNumber((String)arrSanctionObject[1]);
			sanctionRequestTO.setAppealDate((Date)arrSanctionObject[2]);
			sanctionRequestTO.setSettlementDate((Date)arrSanctionObject[3]);
			sanctionRequestTO.setEmitionDate((Date)arrSanctionObject[4]);
			sanctionRequestTO.setExpirationDate((Date)arrSanctionObject[5]);
			sanctionRequestTO.setMotiveDescription((String)arrSanctionObject[6]);
			sanctionRequestTO.setMotiveOther((String)arrSanctionObject[7]);
			sanctionRequestTO.setSanctionAmount((BigDecimal)arrSanctionObject[8]);
			sanctionRequestTO.setIdPeriodSanction((Integer) arrSanctionObject[9]);
			sanctionRequestTO.setStateDescription((String) arrSanctionObject[10]);
			sanctionRequestTO.setParticipant((Participant) arrSanctionObject[11]);
			sanctionRequestTO.setIdParticipant(sanctionRequestTO.getParticipant().getIdParticipantPk());
			sanctionRequestTO.setPeriodSanction(String.format((String)arrSanctionObject[12], new Object[]{CommonsUtilities.currentYear()}));
			sanctionRequestTO.setRequestState((Integer)arrSanctionObject[13]);
			sanctionRequestTO.setSanctionMotive((Integer)arrSanctionObject[14]);
			sanctionRequestTO.setRegisterDate((Date)arrSanctionObject[15]);
			sanctionRequestTO.setCurrency((Integer)arrSanctionObject[16]);
			sanctionRequestTO.setComments((String)arrSanctionObject[17]);
			list.add(sanctionRequestTO);
		}
		
		return list;
		
	}
	
	/**
	 * Gets the list mechanism operation service bean.
	 *
	 * @param idMechanism the id mechanism
	 * @param idParticipant the id participant
	 * @param sanctionMotive the sanction motive
	 * @param settlementDate the settlement date
	 * @param idParticipantPenalty the id participant penalty
	 * @return the list mechanism operation service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SanctionOperationTO> getListMechanismOperationServiceBean(Long idMechanism, Long idParticipant, Integer sanctionMotive, Date settlementDate, Long idParticipantPenalty) throws ServiceException{
		
		List<SanctionOperationTO> list = new ArrayList<SanctionOperationTO>();
		StringBuilder sbQuery=new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT ");
		sbQuery.append("	nme.idNegotiationMechanismPk, nme.mechanismName, "); // 0 1 
		sbQuery.append("	nmo.idNegotiationModalityPk, nmo.modalityName, "); // 2 3 
		sbQuery.append("	mo.idMechanismOperationPk, mo.operationNumber,  "); // 4 5 
		sbQuery.append("	mo.operationDate, mo.ballotNumber, mo.sequential,  ");// 6 7 8
		sbQuery.append("	mo.securities.idSecurityCodePk, ");//9
		sbQuery.append("	mo.stockQuantity, ");//10
		sbQuery.append("	mo.buyerParticipant.idParticipantPk, mo.buyerParticipant.mnemonic, ");//11 12
		sbQuery.append(" 	mo.sellerParticipant.idParticipantPk, mo.sellerParticipant.mnemonic, "); // 13 14
		sbQuery.append(" 	mo.operationState, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = so.operationState ), ");// 15 16
		sbQuery.append(" 	(select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = so.settlementCurrency ), ");// 17
		sbQuery.append(" 	so.settlementDate, ");// 18
		sbQuery.append(" 	so.idSettlementOperationPk, ");// 19
		sbQuery.append(" 	so.settlementAmount ");// 20
		if(idParticipantPenalty!=null){
			sbQuery.append(" , (select ppo.idParticipantPenaltyOpePk from ParticipantPenaltyOperation ppo  ");
			sbQuery.append("    where ppo.settlementOperation.idSettlementOperationPk = so.idSettlementOperationPk ");
			sbQuery.append("      and ppo.operationState = :penaltyOperationState  ");
			sbQuery.append("      and ppo.participantPenalty.idParticipantPenaltyPk = :idParticipantPenalty ) ");// 21
			parameters.put("idParticipantPenalty", idParticipantPenalty);
			parameters.put("penaltyOperationState", SanctionOperationStateType.REGISTERED.getCode());
		}
		sbQuery.append("	FROM SettlementOperation so inner join so.mechanismOperation mo");
		sbQuery.append("	inner join mo.mechanisnModality mm");
		sbQuery.append("	inner join mm.negotiationMechanism nme");
		sbQuery.append("	inner join mm.negotiationModality nmo");
		sbQuery.append(" Where (mo.buyerParticipant.idParticipantPk = :participant or mo.sellerParticipant.idParticipantPk = :participant) ");
		sbQuery.append(" and so.settlementDate = :settlementDate ");
		sbQuery.append(" and nme.idNegotiationMechanismPk = :idMechanism "); 
		
		if(sanctionMotive.equals(SanctionReasonType.UNFULFILLMENT_FUNDS.getCode())  || 
				sanctionMotive.equals(SanctionReasonType.UNFULFILLMENT_STOCK.getCode()) ){
			sbQuery.append(" and so.indUnfulfilled = :indicatorOne "); 
			sbQuery.append(" and ( (so.operationPart = :cashPart and so.operationState in (:cashCancel)) or  ");
			sbQuery.append("       (so.operationPart = :termPart and so.operationState in (:termCancel)) ) ");
			sbQuery.append(" and exists ");
			sbQuery.append("  ( select OU.idOperationUnfulfillmentPk from OperationUnfulfillment OU ");
			sbQuery.append("    where OU.unfulfilledParticipant.idParticipantPk = :participant ");
			sbQuery.append("    and OU.settlementDate = :settlementDate  ");
			sbQuery.append("    and OU.unfulfillmentReason = :unfulfillmentMotive  ");
			sbQuery.append("    and OU.settlementOperation.idSettlementOperationPk = so.idSettlementOperationPk ) ");
			
			parameters.put("indicatorOne", ComponentConstant.ONE);
			parameters.put("cashCancel", MechanismOperationStateType.CANCELED_STATE.getCode());
			parameters.put("termCancel", MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
			
			if(sanctionMotive.equals(SanctionReasonType.UNFULFILLMENT_FUNDS.getCode())){
				parameters.put("unfulfillmentMotive", UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode());
			}else if(sanctionMotive.equals(SanctionReasonType.UNFULFILLMENT_STOCK.getCode())){
				parameters.put("unfulfillmentMotive", UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode());
			}
			
		}else{
			sbQuery.append(" and ( (so.operationPart = :cashPart and so.operationState in (:cashSettled)) or  ");
			sbQuery.append("       (so.operationPart = :termPart and so.operationState in (:termSettled)) ) ");
			sbQuery.append(" and so.indExtended = :indExtended ");
			if(sanctionMotive.equals(SanctionReasonType.MELID_FUNDS.getCode()) || 
					sanctionMotive.equals(SanctionReasonType.MELID_STOCKS.getCode())){
				parameters.put("indExtended", ComponentConstant.ONE);
			}else if(sanctionMotive.equals(SanctionReasonType.MELOR_FUNDS.getCode()) || 
					sanctionMotive.equals(SanctionReasonType.MELOR_STOCKS.getCode())){
				
				sbQuery.append(" and exists ");
				sbQuery.append("  ( select os.idOperationSettlementPk from OperationSettlement os inner join os.settlementProcess sp ");
				sbQuery.append("    where sp.scheduleType = (select scheduleTypeRef from SettlementSchedule where scheduleType = :scheduleType ) ");
				sbQuery.append("    and sp.processState = :finishedState ");
				sbQuery.append("    and sp.settlementDate = :settlementDate ");
				sbQuery.append("    and os.operationState = :removedState  ");
				sbQuery.append("    and os.settlementOperation.idSettlementOperationPk = so.idSettlementOperationPk ) ");
				parameters.put("finishedState", SettlementProcessStateType.FINISHED.getCode());
				parameters.put("indExtended", ComponentConstant.ZERO);
				
				if(sanctionMotive.equals(SanctionReasonType.MELOR_STOCKS.getCode())){
					parameters.put("removedState", OperationSettlementSateType.WITHDRAWN_BY_SECURITIES.getCode());
					parameters.put("scheduleType", SettlementScheduleType.MELOR.getCode());
				}else if(sanctionMotive.equals(SanctionReasonType.MELOR_FUNDS.getCode())){
					parameters.put("removedState", OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode());
					parameters.put("scheduleType", SettlementScheduleType.MELOR.getCode());
				}
				
			}
			
			parameters.put("cashSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
			parameters.put("termSettled", MechanismOperationStateType.TERM_SETTLED.getCode());
		}
		
		parameters.put("settlementDate", settlementDate);
		parameters.put("participant", idParticipant);
		parameters.put("idMechanism", idMechanism);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("termPart", ComponentConstant.TERM_PART);
		
		List<Object[]> result =  findListByQueryString(sbQuery.toString(), parameters);
		for (Object[] objOperation : result) {
			SanctionOperationTO sanctionOperationTO = new SanctionOperationTO();
			sanctionOperationTO.setIdMechanism((Long)objOperation[0]);
			sanctionOperationTO.setMechanismName((String)objOperation[1]);
			sanctionOperationTO.setIdModality((Long)objOperation[2]);
			sanctionOperationTO.setModalityName((String)objOperation[3]);
			sanctionOperationTO.setIdMechanismOperationPk((Long)objOperation[4]);
			sanctionOperationTO.setOperationNumber((Long)objOperation[5]);
			sanctionOperationTO.setOperationDate((Date)objOperation[6]);
			sanctionOperationTO.setBallotNumber((Long)objOperation[7]);
			sanctionOperationTO.setSequential((Long)objOperation[8]);
			sanctionOperationTO.setIdSecurityCodePk((String)objOperation[9]);
			sanctionOperationTO.setStockQuantity((BigDecimal)objOperation[10]);
//			mechanismOperationTO.setCashSettlementDate((Date)objOperation[11]);
//			mechanismOperationTO.setTermSettlementDate((Date)objOperation[12]);
			sanctionOperationTO.setIdPurchaseParticipant((Long)objOperation[11]);
			sanctionOperationTO.setPurchaseParticipantNemonic((String)objOperation[12]);
			sanctionOperationTO.setIdSaleParticipant((Long)objOperation[13]);
			sanctionOperationTO.setSaleParticipantNemonic((String)objOperation[14]);
			sanctionOperationTO.setOperationState((Integer)objOperation[15]);
			sanctionOperationTO.setStateDescription((String)objOperation[16]);
			sanctionOperationTO.setCurrencyDescription((String)objOperation[17]);
			sanctionOperationTO.setSettlementDate((Date)objOperation[18]);
			sanctionOperationTO.setIdSettlementOperation((Long)objOperation[19]);
			sanctionOperationTO.setSettlementAmount((BigDecimal)objOperation[20]);
			if(idParticipantPenalty!=null){
				if(objOperation[21]!=null){
					sanctionOperationTO.setSelected(true);
					sanctionOperationTO.setIdParticipantPenaltyOperation((Long)objOperation[21]);
				}
			}
			list.add(sanctionOperationTO);
		}
		
		return list;
	}
	

	/**
	 * Gets the participant penalties.
	 *
	 * @param idParticipant the id participant
	 * @param datePeriodIni the date period ini
	 * @param datePeriodEnd the date period end
	 * @return the participant penalties
	 */
	public List<ParticipantPenalty> getParticipantPenalties(Long idParticipant, Date datePeriodIni, Date datePeriodEnd) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" Select pp ");
		sbQuery.append(" from ParticipantPenalty pp ");
		sbQuery.append(" Where trunc(pp.registerDate) >= :dateIni ");
		sbQuery.append(" and trunc(pp.registerDate) <= :dateEnd ");
		sbQuery.append(" and penaltyState = :confirmed ");
		sbQuery.append(" and pp.participant.idParticipantPk = :idParticipant ");
		sbQuery.append(" order by pp.idParticipantPenaltyPk  ");
		
		parameters.put("dateIni", datePeriodIni);
		parameters.put("dateEnd", datePeriodEnd);
		parameters.put("confirmed", SanctionStateType.CONFIRMED.getCode());
		parameters.put("idParticipant", idParticipant);
		return findListByQueryString(sbQuery.toString(),parameters);
	}

	/**
	 * Gets the penalty level.
	 *
	 * @param sanctionMotive the sanction motive
	 * @param penaltyCount the penalty count
	 * @return the penalty level
	 */
	public PenaltyLevel getPenaltyLevel(Integer sanctionMotive, Integer penaltyCount) {
		StringBuilder sbQuery=new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" Select pl from PenaltyLevel pl ");
		sbQuery.append(" Where pl.penaltyMotive = :penaltyMotive ");
		sbQuery.append(" and pl.penaltyNumber = :penaltyNumber ");
		
		parameters.put("penaltyMotive", sanctionMotive);
		parameters.put("penaltyNumber", penaltyCount);
		
		return (PenaltyLevel) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the list participant penalty operation search.
	 *
	 * @param idSanctionRequest the id sanction request
	 * @return the list participant penalty operation search
	 */
	public List<MechanismOperationTO> getListParticipantPenaltyOperationSearch(Long idSanctionRequest) {
		List<MechanismOperationTO> list = new ArrayList<MechanismOperationTO>();
		StringBuilder sbQuery=new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT ");
		sbQuery.append("	nme.idNegotiationMechanismPk, nme.mechanismName, "); // 0 1 
		sbQuery.append("	nmo.idNegotiationModalityPk, nmo.modalityName, "); // 2 3 
		sbQuery.append("	mo.idMechanismOperationPk, mo.operationNumber,  "); // 4 5 
		sbQuery.append("	mo.operationDate, mo.ballotNumber, mo.sequential,  ");// 6 7 8
		sbQuery.append("	mo.securities.idSecurityCodePk, ");//9
		sbQuery.append("	mo.stockQuantity, ");//10
		sbQuery.append("	mo.buyerParticipant.idParticipantPk, mo.buyerParticipant.mnemonic, ");//11 12
		sbQuery.append(" 	mo.sellerParticipant.idParticipantPk, mo.sellerParticipant.mnemonic, "); // 13 14
		sbQuery.append(" 	mo.operationState, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = so.operationState ), ");// 15 16
		sbQuery.append(" 	(select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = so.settlementCurrency ), ");// 17
		sbQuery.append(" 	so.settlementDate, ");// 18
		sbQuery.append(" 	so.idSettlementOperationPk, ");// 19
		sbQuery.append(" 	so.settlementAmount ");// 20
		sbQuery.append("	FROM ParticipantPenaltyOperation pp inner join pp.settlementOperation so inner join so.mechanismOperation mo");
		sbQuery.append("	inner join mo.mechanisnModality mm");
		sbQuery.append("	inner join mm.negotiationMechanism nme");
		sbQuery.append("	inner join mm.negotiationModality nmo");
		sbQuery.append(" Where pp.participantPenalty.idParticipantPenaltyPk = :idSanctionRequest ");
		sbQuery.append(" and pp.operationState = :registeredOps ");

		parameters.put("idSanctionRequest", idSanctionRequest);
		parameters.put("registeredOps", SanctionOperationStateType.REGISTERED.getCode());
		
		List<Object[]> result =  findListByQueryString(sbQuery.toString(), parameters);
		for (Object[] objOperation : result) {
			MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
			mechanismOperationTO.setIdMechanism((Long)objOperation[0]);
			mechanismOperationTO.setMechanismName((String)objOperation[1]);
			mechanismOperationTO.setIdModality((Long)objOperation[2]);
			mechanismOperationTO.setModalityName((String)objOperation[3]);
			mechanismOperationTO.setIdMechanismOperationPk((Long)objOperation[4]);
			mechanismOperationTO.setOperationNumber((Long)objOperation[5]);
			mechanismOperationTO.setOperationDate((Date)objOperation[6]);
			mechanismOperationTO.setBallotNumber((Long)objOperation[7]);
			mechanismOperationTO.setSequential((Long)objOperation[8]);
			mechanismOperationTO.setIdSecurityCodePk((String)objOperation[9]);
			mechanismOperationTO.setStockQuantity((BigDecimal)objOperation[10]);
//			mechanismOperationTO.setCashSettlementDate((Date)objOperation[11]);
//			mechanismOperationTO.setTermSettlementDate((Date)objOperation[12]);
			mechanismOperationTO.setIdPurchaseParticipant((Long)objOperation[11]);
			mechanismOperationTO.setPurchaseParticipantNemonic((String)objOperation[12]);
			mechanismOperationTO.setIdSaleParticipant((Long)objOperation[13]);
			mechanismOperationTO.setSaleParticipantNemonic((String)objOperation[14]);
			mechanismOperationTO.setOperationState((Integer)objOperation[15]);
			mechanismOperationTO.setStateDescription((String)objOperation[16]);
			mechanismOperationTO.setCurrencyDescription((String)objOperation[17]);
			mechanismOperationTO.setSettlementDate((Date)objOperation[18]);
			mechanismOperationTO.setIdSettlementOperation((Long)objOperation[19]);
			mechanismOperationTO.setSettlementAmount((BigDecimal)objOperation[20]);
			list.add(mechanismOperationTO);
		}
		
		return list;
	}
	
	/**
	 * Creates the participant penalty operation.
	 *
	 * @param sanctionOperationTO the sanction operation to
	 * @param participantPenalty the participant penalty
	 * @param loggerUser the logger user
	 */
	public void createParticipantPenaltyOperation(SanctionOperationTO sanctionOperationTO, ParticipantPenalty participantPenalty, LoggerUser loggerUser){
		
		ParticipantPenaltyOperation participantPenaltyOperation = new ParticipantPenaltyOperation();
		participantPenaltyOperation.setParticipantPenalty(participantPenalty);
		participantPenaltyOperation.setOperationState(SanctionOperationStateType.REGISTERED.getCode());
		participantPenaltyOperation.setSettlementOperation(new SettlementOperation(sanctionOperationTO.getIdSettlementOperation()));    	
		participantPenaltyOperation.setRegisterUser(participantPenalty.getRegisterUser());
	    participantPenaltyOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		
	    create(participantPenaltyOperation);
	}

	/**
	 * Exists previous penalty.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idParticipant the id participant
	 * @param idMotive the id motive
	 * @return true, if successful
	 */
	public boolean existsPreviousPenalty(Long idSettlementOperation, Long idParticipant, Integer idMotive) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" Select nvl(count(ppo.idParticipantPenaltyOpePk),0) ");
		sbQuery.append(" from ParticipantPenalty pp , ParticipantPenaltyOperation ppo ");
		sbQuery.append(" Where pp.idParticipantPenaltyPk = ppo.participantPenalty.idParticipantPenaltyPk ");
		sbQuery.append(" and pp.penaltyState not in (:deletedState , :deletedAppealState ) ");
		sbQuery.append(" and pp.participant.idParticipantPk = :idParticipant ");
		sbQuery.append(" and pp.penaltyMotive = :idMotive ");
		sbQuery.append(" and ppo.operationState = :registeredDetail ");
		sbQuery.append(" and ppo.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		
		parameters.put("registeredDetail", SanctionOperationStateType.REGISTERED.getCode());
		parameters.put("deletedState", SanctionStateType.DELETED.getCode());
		parameters.put("deletedAppealState", SanctionStateType.DELETED_BY_APELATION.getCode());
		parameters.put("idSettlementOperation", idSettlementOperation);
		parameters.put("idParticipant", idParticipant);
		parameters.put("idMotive", idMotive);
		
		Long find = (Long) findObjectByQueryString(sbQuery.toString(),parameters);
		if(find > 0l){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Change state participant penalty.
	 *
	 * @param sanctionRequest the sanction request
	 * @param state the state
	 * @return the participant penalty
	 * @throws ServiceException the service exception
	 */
	public ParticipantPenalty changeStateParticipantPenalty(SanctionRequestTO sanctionRequest, Integer state) throws ServiceException {
		ParticipantPenalty participantPenalty = find(sanctionRequest.getIdSanctionRequest(), ParticipantPenalty.class);
		if(state.equals(SanctionStateType.MODIFIED.getCode()) || 
				state.equals(SanctionStateType.CONFIRMED.getCode()) || 
				state.equals(SanctionStateType.DELETED.getCode())){
			if(!participantPenalty.getPenaltyState().equals(SanctionStateType.REGISTERED.getCode()) &&
					!participantPenalty.getPenaltyState().equals(SanctionStateType.MODIFIED.getCode())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}else if(state.equals(SanctionStateType.DELETED_BY_APELATION.getCode())){
			if(!participantPenalty.getPenaltyState().equals(SanctionStateType.CONFIRMED.getCode())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}
		
		if(state.equals(SanctionStateType.MODIFIED.getCode())){
			participantPenalty.setPenaltyAmount(sanctionRequest.getSanctionAmount());
			participantPenalty.setPenaltyMotiveOther(sanctionRequest.getMotiveOther());
		}
		
		if(state.equals(SanctionStateType.CONFIRMED.getCode())){
			participantPenalty.setResolutionNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_PARTICIPANT_PENALTY).toString());
			participantPenalty.setPenaltyEmitionDate(CommonsUtilities.currentDate());
		}
		
		if(state.equals(SanctionStateType.DELETED_BY_APELATION.getCode())){
			participantPenalty.setComments(sanctionRequest.getComments());
			participantPenalty.setPenaltyAppealDate(sanctionRequest.getAppealDate());
		}
		
		participantPenalty.setPenaltyState(state);
		update(participantPenalty);
		
		/**
		 * Here Send Reports
		 */
		return participantPenalty;
	}
	

	/**
	 * Verify If Participant Have Sanctions in with Type Motive Sanction and same Date Operation
	 * Get Sanction Motive To Type Sanction y for each Participant Penalty find if 
	 * Short Integer same, and settlement date 
	 * @param sanctionMotiveTo
	 * @return
	 * @throws ServiceException 
	 */
	public boolean haveParticipantSanction(SanctionMotiveTO sanctionMotiveTo, SanctionRequestTO sanctionRequestTO) 
										throws ServiceException{

		List<ParameterTable> reasonSanction= parameterServiceBean.getListParameterTableServiceBean(MasterTableType.REASON_SANCTION_PARTICIPAN.getCode());
		List<ParticipantPenalty> listParticipantPenalty=findParticipantPenaltyBySave(sanctionMotiveTo,sanctionRequestTO);
		Map<Integer, Integer > mapReason=new HashMap<Integer, Integer >();
		for (ParameterTable parameterTable : reasonSanction) {
			mapReason.put(parameterTable.getParameterTablePk(), parameterTable.getShortInteger());	
		}
		if(sanctionMotiveTo!=null){
			for(ParticipantPenalty participantPenalty : listParticipantPenalty){
				Integer typeSanction=mapReason.get(participantPenalty.getPenaltyMotive());
				
				//issue 1149 - Cuando el motivo de la sancion sea Melid Fondos-Valores o Melor Fondos-Valores ya no validara la fecha de liquidacion
				if ( !participantPenalty.getPenaltyMotive().equals(SanctionReasonType.MELID_FUNDS.getCode())
						&& !participantPenalty.getPenaltyMotive().equals(SanctionReasonType.MELID_STOCKS.getCode())
							&&	!participantPenalty.getPenaltyMotive().equals(SanctionReasonType.MELOR_FUNDS.getCode())
								&& !participantPenalty.getPenaltyMotive().equals(SanctionReasonType.MELOR_STOCKS.getCode())){
					
								if(sanctionMotiveTo.getTypeSanction().equals(typeSanction)&&
										CommonsUtilities.isEqualDate(participantPenalty.getSettlementDate(), sanctionRequestTO.getSettlementDate())
										){
									return true;
								}
				}
			}
		}
		return false;
		
	}
	
	/**
	 * Validate Range Sanctions
	 * @param sanctionMotiveTo
	 * @param sanctionRequestTO
	 * @return
	 * @throws ServiceException
	 */
	public boolean validateRangeSanction(SanctionMotiveTO sanctionMotiveTo, SanctionRequestTO sanctionRequestTO) 
			throws ServiceException{
		
		//Si es una funcion de llamada de Atencion
		if(sanctionRequestTO.getSanctionAmount()!=null && 
				sanctionRequestTO.getSanctionAmount().compareTo(BigDecimal.ZERO)==0){
			return true;
		}
		
		List<PenaltyLevel> listPenaltyLevel= findPenaltyLevelByMotive(sanctionMotiveTo);
		
		for (PenaltyLevel penaltyLevel : listPenaltyLevel) {
			if(penaltyLevel.getPenaltyAmount().compareTo(sanctionRequestTO.getSanctionAmount())==0){
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Find All Participant Penalty with same 
	 * @param sanctionMotiveTo
	 * @param idParticipantPk
	 * @return
	 */
	public List<ParticipantPenalty> findParticipantPenaltyBySave(SanctionMotiveTO sanctionMotiveTo, SanctionRequestTO sanctionRequestTO){
		List<ParticipantPenalty> listParticipantPenalty= new ArrayList<ParticipantPenalty>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" Select  ");
		sbQuery.append("  pp");
		sbQuery.append("  ");
		sbQuery.append(" from ParticipantPenalty pp , ParameterTable pt ");
		sbQuery.append(" Where pp.penaltyMotive = pt.parameterTablePk ");
		sbQuery.append(" and pp.penaltyState=:state ");
		sbQuery.append(" and pp.participant.idParticipantPk = :idParticipant ");
		sbQuery.append(" and pp.settlementDate = :settlementDate ");
		
		parameters.put("state", SanctionStateType.CONFIRMED.getCode());
		parameters.put("idParticipant", sanctionRequestTO.getIdParticipant());
		parameters.put("settlementDate", sanctionRequestTO.getSettlementDate());
		
		listParticipantPenalty = (List<ParticipantPenalty>) findListByQueryString(sbQuery.toString(),parameters);
		
		return listParticipantPenalty;
	}
	
	public List<PenaltyLevel> findPenaltyLevelByMotive(SanctionMotiveTO sanctionMotiveTo){
		List<PenaltyLevel> listPenaltyLevel= new ArrayList<PenaltyLevel>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" Select  ");
		sbQuery.append("  pl");
		sbQuery.append("  ");
		sbQuery.append(" from PenaltyLevel pl ");
		sbQuery.append(" Where pl.penaltyMotive = :penaltyMotive ");
		sbQuery.append(" and pl.penaltyNumber=:penaltyNumber ");
		
		parameters.put("penaltyMotive", sanctionMotiveTo.getSanctionMotive());
		parameters.put("penaltyNumber", sanctionMotiveTo.getSanctionCount()+1);
		
		listPenaltyLevel = (List<PenaltyLevel>) findListByQueryString(sbQuery.toString(),parameters);
		
		return listPenaltyLevel;
	}
	
	/**
	 * Metodo para reiniciar la secuencia de RESOLUTION_NUMBER 
	 */
	
	public void resetSequenceParticipantPenalty(){
		
		String nameSequence = (RequestCorrelativeType.REQUEST_PARTICIPANT_PENALTY.getValue()).toString();
		
		StringBuilder sb = new StringBuilder();
		sb.append("DROP SEQUENCE "+ nameSequence );
		Query query = em.createNativeQuery(sb.toString());
    	query.executeUpdate();
		
    	StringBuilder sb2 = new StringBuilder();
		sb2.append("CREATE SEQUENCE " + nameSequence +" START WITH 1  INCREMENT BY  1 NOCACHE ");
		Query query2 = em.createNativeQuery(sb2.toString());
    	query2.executeUpdate();
	}
}
