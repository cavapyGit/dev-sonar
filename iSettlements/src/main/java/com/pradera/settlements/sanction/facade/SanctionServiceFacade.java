package com.pradera.settlements.sanction.facade;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.settlement.ParticipantPenalty;
import com.pradera.model.settlement.ParticipantPenaltyOperation;
import com.pradera.model.settlement.PenaltyLevel;
import com.pradera.model.settlement.type.SanctionOperationStateType;
import com.pradera.model.settlement.type.SanctionReasonType;
import com.pradera.model.settlement.type.SanctionStateType;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.settlements.sanction.service.SanctionMgmtServiceBean;
import com.pradera.settlements.sanction.to.SanctionMotiveTO;
import com.pradera.settlements.sanction.to.SanctionOperationTO;
import com.pradera.settlements.sanction.to.SanctionRequestTO;

// TODO: Auto-generated Javadoc
/**
 * Session Bean implementation class SanctionServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SanctionServiceFacade {
	
	/** The sanction mgmt service bean. */
	@EJB
	SanctionMgmtServiceBean sanctionMgmtServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	@Inject
 	Instance<ReportGenerationService> reportService;
    /**
     * Default constructor. 
     */
    public SanctionServiceFacade() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Select participant penalty by filter.
     *
     * @param filter the filter
     * @return the list sanction search
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.SANCTION_QUERY)
	public List<SanctionRequestTO> getListSanctionSearch(SanctionRequestTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		return sanctionMgmtServiceBean.getListSanctionByFiltersServiceBean(filter);
	}
    
    /**
     * Select mechanism operation by participant.
     *
     * @param idMechanism the id mechanism
     * @param idParticipant the id participant
     * @param sanctionMotive the sanction motive
     * @param settlementDate the settlement date
     * @param idParticipantPenalty the id participant penalty
     * @return the list mechanism operation search
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.SANCTION_QUERY)
	public List<SanctionOperationTO> getListMechanismOperationSearch(Long idMechanism, Long idParticipant, Integer sanctionMotive, Date settlementDate, Long idParticipantPenalty) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		return sanctionMgmtServiceBean.getListMechanismOperationServiceBean(idMechanism,idParticipant,sanctionMotive, settlementDate, idParticipantPenalty);
	}
	
    /**
     * register penalty participant.
     *
     * @param participantPenalty the participant penalty
     * @param listMechanismOperationsSelection the list mechanism operations selection
     * @throws ServiceException the service exception
     */
	@ProcessAuditLogger(process=BusinessProcessType.SANCTION_REGISTER)
	public void registryParticipantPenalty(ParticipantPenalty participantPenalty,List<SanctionOperationTO> listMechanismOperationsSelection) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		participantPenalty.setRegisterUser(loggerUser.getUserName());
        participantPenalty.setRegisterDate(CommonsUtilities.currentDateTime());
        
        sanctionMgmtServiceBean.create(participantPenalty);
        
        if(Validations.validateListIsNotNullAndNotEmpty(listMechanismOperationsSelection)){
        	for (SanctionOperationTO sanctionOperationTO : listMechanismOperationsSelection) {
            	if(sanctionOperationTO.isSelected()){
            		
            		boolean exists = sanctionMgmtServiceBean.existsPreviousPenalty(
            				sanctionOperationTO.getIdSettlementOperation(), 
            				participantPenalty.getParticipant().getIdParticipantPk(),
            				participantPenalty.getPenaltyMotive());
            		if(exists){
            			StringBuffer sb = new StringBuffer();
            			if(sanctionOperationTO.getBallotNumber()==null && sanctionOperationTO.getSequential()==null){
            				sb.append(sanctionOperationTO.getModalityName()).
            					append(GeneralConstants.SLASH_SPACE).
            					append(CommonsUtilities.convertDatetoString(sanctionOperationTO.getOperationDate())).
            					append(GeneralConstants.SLASH_SPACE).
            					append(sanctionOperationTO.getOperationNumber().toString());
            			}else{
            				sb.append(sanctionOperationTO.getBallotSequential());
            			}
            			
            			
            			throw new ServiceException(ErrorServiceType.SETTLEMENT_SANCTION_EXISTS_OPERATION,new Object[]{sb.toString()});
            		}
            		
            		sanctionMgmtServiceBean.createParticipantPenaltyOperation(sanctionOperationTO,participantPenalty,loggerUser);
            	}
    		}
        }
        
	}
	
	/**
	 * modify penalty participant.
	 *
	 * @param sanctionRequest the sanction request
	 * @param listSanctionOperationTOs the list sanction operation t os
	 * @return the participant penalty
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SANCTION_MODIFY)
	public ParticipantPenalty modifyParticipantPenalty(SanctionRequestTO sanctionRequest, List<SanctionOperationTO> listSanctionOperationTOs) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
        
        ParticipantPenalty participantPenalty = sanctionMgmtServiceBean.changeStateParticipantPenalty(sanctionRequest,SanctionStateType.MODIFIED.getCode());
        
        if(Validations.validateListIsNotNullAndNotEmpty(listSanctionOperationTOs)){
        	for(SanctionOperationTO sanctionOperationTO: listSanctionOperationTOs){
            	if(sanctionOperationTO.getIdParticipantPenaltyOperation()!=null){
            		if(!sanctionOperationTO.isSelected()){
            			ParticipantPenaltyOperation participantPenaltyOperation = sanctionMgmtServiceBean.find(ParticipantPenaltyOperation.class, sanctionOperationTO.getIdParticipantPenaltyOperation());
            			participantPenaltyOperation.setOperationState(SanctionOperationStateType.DELETED.getCode());
            			sanctionMgmtServiceBean.update(participantPenaltyOperation);
            		}
            	}else{
            		if(sanctionOperationTO.isSelected()){
            			sanctionMgmtServiceBean.createParticipantPenaltyOperation(sanctionOperationTO, participantPenalty, loggerUser);
            		}
            	}
            }
        }
        
		return participantPenalty;
	}
	
	/**
	 * confirm penalty participant.
	 *
	 * @param sanctionRequestTO the sanction request to
	 * @return the participant penalty
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SANCTION_CONFIRM)
	public ParticipantPenalty confirmParticipantPenalty(SanctionRequestTO sanctionRequestTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        
        return sanctionMgmtServiceBean.changeStateParticipantPenalty(sanctionRequestTO,SanctionStateType.CONFIRMED.getCode());

	}
	
	/**
	 * Reset resolution correlative sequence.
	 *
	 * @param sanctionRequestTO the sanction request to
	 * @return the participant penalty
	 * @throws ServiceException the service exception
	 */

	public void resetSequenceParticipantPenalty() throws ServiceException{       
        LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        sanctionMgmtServiceBean.resetSequenceParticipantPenalty();

	}
	
	/**
	 * delete penalty participant.
	 *
	 * @param sanctionRequestTO the sanction request to
	 * @return the participant penalty
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SANCTION_DELETE)
	public ParticipantPenalty deleteParticipantPenalty(SanctionRequestTO sanctionRequestTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        return sanctionMgmtServiceBean.changeStateParticipantPenalty(sanctionRequestTO,SanctionStateType.DELETED.getCode());

	}
	
	/**
	 * appeal penalty participant.
	 *
	 * @param sanctionRequestTO the sanction request to
	 * @return the participant penalty
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SANCTION_APPEAL)
	public ParticipantPenalty appealParticipantPenalty(SanctionRequestTO sanctionRequestTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAppeal());
        
        return sanctionMgmtServiceBean.changeStateParticipantPenalty(sanctionRequestTO,SanctionStateType.DELETED_BY_APELATION.getCode());
	}

	/**
	 * Gets the participant penalties.
	 *
	 * @param idParticipant the id participant
	 * @param datePeriodIni the date period ini
	 * @param datePeriodEnd the date period end
	 * @return the participant penalties
	 */
	public List<ParticipantPenalty> getParticipantPenalties(Long idParticipant, Date datePeriodIni, Date datePeriodEnd) {
		return sanctionMgmtServiceBean.getParticipantPenalties(idParticipant,datePeriodIni,datePeriodEnd);
	}

	/**
	 * Gets the penalty level.
	 *
	 * @param sanctionMotive the sanction motive
	 * @param penaltyCount the penalty count
	 * @return the penalty level
	 */
	public PenaltyLevel getPenaltyLevel(Integer sanctionMotive, Integer penaltyCount) {
		return sanctionMgmtServiceBean.getPenaltyLevel(sanctionMotive,penaltyCount);
	}

	/**
	 * Gets the list participant penalty operation search.
	 *
	 * @param idSanctionRequest the id sanction request
	 * @return the list participant penalty operation search
	 */
	public List<MechanismOperationTO> getListParticipantPenaltyOperationSearch(Long idSanctionRequest) {
		return sanctionMgmtServiceBean.getListParticipantPenaltyOperationSearch(idSanctionRequest);
	}
	
	public void sendRecordsOnSanctions( Map<String, String> parameters, Long participantCode, Integer penaltyMotive, boolean reprimend) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        
		ReportUser user = new ReportUser();
//		user.setPartcipantCode(participantCode);
		user.setUserName(loggerUser.getUserName());
		user.setReportFormat(ReportFormatType.PDF.getCode());
		
		//Reporte de Amonestaciones
//		if(SanctionReasonType.OTHER.getCode().equals(penaltyMotive) && reprimend){
//			reportService.get().saveReportExecution(ReportIdType.RECORD_ON_REPRIMAND.getCode(), parameters , null, user, loggerUser);
//		}else{
//			reportService.get().saveReportExecution(ReportIdType.RECORD_ON_SANCTIONS.getCode(), parameters , null, user, loggerUser);
//		}
		
//		if(penaltyMotive.equals(SanctionReasonType.UNFULFILLMENT_FUNDS.getCode()) || penaltyMotive.equals(SanctionReasonType.UNFULFILLMENT_STOCK.getCode())){
//			reportService.get().saveReportExecution(255l,parameters , null, user, loggerUser);
//		}else{
//			reportService.get().saveReportExecution(243l,parameters , null, user, loggerUser);
//		}
	}
	

	/**
	 * Verify If Participant Have Sanctions in with Type Motive Sanction and same Date Operation
	 * @param sanctionMotiveTo
	 * @return
	 * @throws ServiceException 
	 */
	public boolean haveParticipantSanction(SanctionMotiveTO sanctionMotiveTo, SanctionRequestTO sanctionRequestTO) 
														throws ServiceException{
		return sanctionMgmtServiceBean.haveParticipantSanction(sanctionMotiveTo, sanctionRequestTO);
	}
	
	/**
	 * Validate Range Sanctions
	 * @param sanctionMotiveTo
	 * @param sanctionRequestTO
	 * @return
	 * @throws ServiceException
	 */
	public boolean validateRangeSanction(SanctionMotiveTO sanctionMotiveTo, SanctionRequestTO sanctionRequestTO) 
			throws ServiceException{
		return sanctionMgmtServiceBean.validateRangeSanction(sanctionMotiveTo, sanctionRequestTO);
	}
	
}
