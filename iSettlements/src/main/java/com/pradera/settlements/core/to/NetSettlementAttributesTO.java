package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.SettlementSchedule;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NetSettlementAttributesTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class NetSettlementAttributesTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The lst pending settlement operations. */
	private List<NetSettlementOperationTO> lstPendingSettlementOperations;
	
	/** The lst guarantee removed operations. */
	private List<NetSettlementOperationTO> lstGuaranteeRemovedOperations;
	
	/** The lst stock removed operations. */
	private List<NetSettlementOperationTO> lstStockRemovedOperations;
	
	/** The map participant position. */
	private Map<Long,NetParticipantPositionTO> mapParticipantPosition;
	
	/** The mp cash accounts. */
	private Map<Long,InstitutionCashAccount> mpCashAccounts;
	
	/** The lst participant position. */
	private List<NetParticipantPositionTO> lstParticipantPosition; // temp
	
	/** The participant position to edit. */
	private NetParticipantPositionTO participantPositionToEdit;
	
	/** The participant removed position. */
	private NetParticipantPositionTO participantRemovedPosition;
	
	/** The lst guarantee participant position. */
	private List<NetParticipantPositionTO> lstGuaranteeParticipantPosition; // removed by guarantees grouped by part - modality
	
	/** The lst stock participant position. */
	private List<NetParticipantPositionTO> lstStockParticipantPosition; // removed by stock grouped by part - modality
	
	/** The settlement process. */
	private SettlementProcess settlementProcess;
	
	/** The settlement schedule. */
	private SettlementSchedule settlementSchedule;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/**
	 * Instantiates a new net settlement attributes to.
	 */
	public NetSettlementAttributesTO() {
		super();
		this.mpCashAccounts = new HashMap<Long, InstitutionCashAccount>();
		this.lstPendingSettlementOperations = new LinkedList<NetSettlementOperationTO>();
		this.lstGuaranteeRemovedOperations = new LinkedList<NetSettlementOperationTO>();
		this.lstStockRemovedOperations = new LinkedList<NetSettlementOperationTO>();
	}
	
	/**
	 * Gets the all settlement operations.
	 *
	 * @return the all settlement operations
	 */
	public List<NetSettlementOperationTO> getAllSettlementOperations(){
		List<NetSettlementOperationTO> totalList = new ArrayList<NetSettlementOperationTO>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstPendingSettlementOperations)){
			totalList.addAll(lstPendingSettlementOperations);
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstGuaranteeRemovedOperations)){
			totalList.addAll(lstGuaranteeRemovedOperations);
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstStockRemovedOperations)){
			totalList.addAll(lstStockRemovedOperations);
		}
		return totalList;
	}
	
	/**
	 * Gets the lst participant position.
	 *
	 * @return the lst participant position
	 */
	public List<NetParticipantPositionTO> getLstParticipantPosition(){
		if(lstParticipantPosition == null){
			lstParticipantPosition = new ArrayList<NetParticipantPositionTO>(mapParticipantPosition.values());
		}
		return lstParticipantPosition;
	}
	
	/**
	 * Sets the lst participant position.
	 *
	 * @param lstParticipantPosition the new lst participant position
	 */
	public void setLstParticipantPosition(
			List<NetParticipantPositionTO> lstParticipantPosition) {
		this.lstParticipantPosition = lstParticipantPosition;
	}

	/**
	 * Gets the lst pending settlement operations.
	 *
	 * @return the lst pending settlement operations
	 */
	public List<NetSettlementOperationTO> getLstPendingSettlementOperations() {
		return lstPendingSettlementOperations;
	}

	/**
	 * Sets the lst pending settlement operations.
	 *
	 * @param lstPendingSettlementOperations the new lst pending settlement operations
	 */
	public void setLstPendingSettlementOperations(
			List<NetSettlementOperationTO> lstPendingSettlementOperations) {
		this.lstPendingSettlementOperations = lstPendingSettlementOperations;
	}

	/**
	 * Gets the lst guarantee removed operations.
	 *
	 * @return the lst guarantee removed operations
	 */
	public List<NetSettlementOperationTO> getLstGuaranteeRemovedOperations() {
		return lstGuaranteeRemovedOperations;
	}

	/**
	 * Sets the lst guarantee removed operations.
	 *
	 * @param lstGuaranteeRemovedOperations the new lst guarantee removed operations
	 */
	public void setLstGuaranteeRemovedOperations(
			List<NetSettlementOperationTO> lstGuaranteeRemovedOperations) {
		this.lstGuaranteeRemovedOperations = lstGuaranteeRemovedOperations;
	}

	/**
	 * Gets the lst stock removed operations.
	 *
	 * @return the lst stock removed operations
	 */
	public List<NetSettlementOperationTO> getLstStockRemovedOperations() {
		return lstStockRemovedOperations;
	}

	/**
	 * Sets the lst stock removed operations.
	 *
	 * @param lstStockRemovedOperations the new lst stock removed operations
	 */
	public void setLstStockRemovedOperations(
			List<NetSettlementOperationTO> lstStockRemovedOperations) {
		this.lstStockRemovedOperations = lstStockRemovedOperations;
	}

	/**
	 * Gets the map participant position.
	 *
	 * @return the map participant position
	 */
	public Map<Long, NetParticipantPositionTO> getMapParticipantPosition() {
		return mapParticipantPosition;
	}

	/**
	 * Sets the map participant position.
	 *
	 * @param mapParticipantPosition the map participant position
	 */
	public void setMapParticipantPosition(
			Map<Long, NetParticipantPositionTO> mapParticipantPosition) {
		this.mapParticipantPosition = mapParticipantPosition;
	}

	/**
	 * Gets the settlement process.
	 *
	 * @return the settlement process
	 */
	public SettlementProcess getSettlementProcess() {
		return settlementProcess;
	}

	/**
	 * Sets the settlement process.
	 *
	 * @param settlementProcess the new settlement process
	 */
	public void setSettlementProcess(SettlementProcess settlementProcess) {
		this.settlementProcess = settlementProcess;
	}

	/**
	 * Gets the lst guarantee participant position.
	 *
	 * @return the lst guarantee participant position
	 */
	public List<NetParticipantPositionTO> getLstGuaranteeParticipantPosition() {
		return lstGuaranteeParticipantPosition;
	}

	/**
	 * Sets the lst guarantee participant position.
	 *
	 * @param lstGuaranteeParticipantPosition the new lst guarantee participant position
	 */
	public void setLstGuaranteeParticipantPosition(
			List<NetParticipantPositionTO> lstGuaranteeParticipantPosition) {
		this.lstGuaranteeParticipantPosition = lstGuaranteeParticipantPosition;
	}

	/**
	 * Gets the lst stock participant position.
	 *
	 * @return the lst stock participant position
	 */
	public List<NetParticipantPositionTO> getLstStockParticipantPosition() {
		return lstStockParticipantPosition;
	}

	/**
	 * Sets the lst stock participant position.
	 *
	 * @param lstStockParticipantPosition the new lst stock participant position
	 */
	public void setLstStockParticipantPosition(
			List<NetParticipantPositionTO> lstStockParticipantPosition) {
		this.lstStockParticipantPosition = lstStockParticipantPosition;
	}

	/**
	 * Gets the participant position to edit.
	 *
	 * @return the participant position to edit
	 */
	public NetParticipantPositionTO getParticipantPositionToEdit() {
		return participantPositionToEdit;
	}

	/**
	 * Sets the participant position to edit.
	 *
	 * @param participantPositionToEdit the new participant position to edit
	 */
	public void setParticipantPositionToEdit(
			NetParticipantPositionTO participantPositionToEdit) {
		this.participantPositionToEdit = participantPositionToEdit;
	}

	/**
	 * Gets the participant removed position.
	 *
	 * @return the participant removed position
	 */
	public NetParticipantPositionTO getParticipantRemovedPosition() {
		return participantRemovedPosition;
	}

	/**
	 * Sets the participant removed position.
	 *
	 * @param participantRemovedPosition the new participant removed position
	 */
	public void setParticipantRemovedPosition(
			NetParticipantPositionTO participantRemovedPosition) {
		this.participantRemovedPosition = participantRemovedPosition;
	}

	/**
	 * Gets the settlement schedule.
	 *
	 * @return the settlement schedule
	 */
	public SettlementSchedule getSettlementSchedule() {
		return settlementSchedule;
	}

	/**
	 * Sets the settlement schedule.
	 *
	 * @param settlementSchedule the new settlement schedule
	 */
	public void setSettlementSchedule(SettlementSchedule settlementSchedule) {
		this.settlementSchedule = settlementSchedule;
	}

	/**
	 * Gets the mp cash accounts.
	 *
	 * @return the mp cash accounts
	 */
	public Map<Long, InstitutionCashAccount> getMpCashAccounts() {
		return mpCashAccounts;
	}

	/**
	 * Sets the mp cash accounts.
	 *
	 * @param mpCashAccounts the mp cash accounts
	 */
	public void setMpCashAccounts(Map<Long, InstitutionCashAccount> mpCashAccounts) {
		this.mpCashAccounts = mpCashAccounts;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	
}
