package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrimaryPlacementSettlement.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class PrimaryPlacementSettlement extends CrudDaoServiceBean{

	/** The securities component service. */
	@Inject
	private Instance<SecuritiesComponentService> securitiesComponentService;
	
	/** The settlement process service. */
	@EJB
	SettlementProcessService settlementProcessService;
	
	/** The securities remote service. */
	@Inject Instance<SecuritiesRemoteService> securitiesRemoteService;

	
	/**
	 * Settle primary placement operation.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param loggerUser the logger user
	 * @param indMarketFact the ind market fact
	 * @throws ServiceException the service exception
	 */
	public void settlePrimaryPlacementOperation(SettlementOperationTO objSettlementOperationTO, LoggerUser loggerUser, Integer indMarketFact) throws ServiceException
	{
		
		if(ComponentConstant.ONE.equals(objSettlementOperationTO.getIndDematerialization())){
			Object[] issuerDetail= settlementProcessService.getIssuerDetails(objSettlementOperationTO.getIdSecurityCode());
			createAccountAnnotations(objSettlementOperationTO,issuerDetail,indMarketFact,loggerUser);
		}else{
			updateSecurityAndIssuanceAmounts(objSettlementOperationTO);
			updatePlacementSegment(objSettlementOperationTO);
		}
	}
	
	/**
	 * Update placement segment.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @throws ServiceException the service exception
	 */
	private void updatePlacementSegment(SettlementOperationTO objSettlementOperationTO) throws ServiceException{
		/**Iterate only participantSellers*/
		for(SettlementHolderAccountTO settlementAccount: objSettlementOperationTO.getLstSellerHolderAccounts()){
			/**Reading data from settlementOperation*/
			BigDecimal settledQuantity = settlementAccount.getStockQuantity();
			/**Getting participant*/
			Long participantPk = settlementAccount.getIdParticipant();
			/**Getting securityCode*/
			String securityCode = settlementAccount.getIdSecurityCode();
			/**Update information on placement segment*/
			securitiesRemoteService.get().substractOperationPlacement(participantPk, securityCode, objSettlementOperationTO.getIdMechanismOperation(), settledQuantity);
//			
//			/**Read plaementDetail from bd*/
//			PlacementSegmentDetail placementDetail = getPlacementDetail(securityCode, participantPk);
//			/**Get requestBalance from placementDetail*/
//			BigDecimal currentBalance = placementDetail.getRequestBalance();
//			/**Add Request Balance*/
//			BigDecimal requestBalance=CommonsUtilities.addTwoDecimal(currentBalance, settledQuantity, 4);
//			/**Update request balance in detail*/
//			placementDetail.setRequestBalance(requestBalance);
//			/**update on bd*/
//			update(placementDetail);
		}
	}
	
	/**
	 * Update security and issuance amounts.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityAndIssuanceAmounts(SettlementOperationTO objSettlementOperationTO) throws ServiceException{
		
		//we save the securities operation
		SecuritiesOperation objSecuritiesOperation = new SecuritiesOperation();
		objSecuritiesOperation.setCashAmount(objSettlementOperationTO.getStockQuantity().multiply(objSettlementOperationTO.getCurrentNominalValue()));
		objSecuritiesOperation.setOperationDate(CommonsUtilities.currentDateTime());
		objSecuritiesOperation.setStockQuantity(objSettlementOperationTO.getStockQuantity());
		objSecuritiesOperation.setOperationType(ParameterOperationType.SEC_PRIMARY_PLACEMENT.getCode());
		objSecuritiesOperation.setSecurities(new Security(objSettlementOperationTO.getIdSecurityCode()));
		objSecuritiesOperation.setTradeOperation(new TradeOperation(objSettlementOperationTO.getIdMechanismOperation()));
		this.create(objSecuritiesOperation);
		
		//to execute the security component to update the dematerialized balance from security
		SecuritiesComponentTO objSecuritiesComponentTO = new SecuritiesComponentTO();
		objSecuritiesComponentTO.setIdOperationType(ParameterOperationType.SEC_PRIMARY_PLACEMENT.getCode());
		if (SettlementSchemaType.GROSS.getCode().equals(objSettlementOperationTO.getSettlementSchema())){
			objSecuritiesComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());
		}else if (SettlementSchemaType.NET.getCode().equals(objSettlementOperationTO.getSettlementSchema())){
			objSecuritiesComponentTO.setIdBusinessProcess(BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode());
		}
		objSecuritiesComponentTO.setDematerializedBalance(objSettlementOperationTO.getStockQuantity());
		objSecuritiesComponentTO.setShareBalance(objSettlementOperationTO.getStockQuantity());
		objSecuritiesComponentTO.setCirculationBalance(objSettlementOperationTO.getStockQuantity());
		objSecuritiesComponentTO.setPlacedBalance(objSettlementOperationTO.getStockQuantity());
		objSecuritiesComponentTO.setIdSecuritiesOperation(objSecuritiesOperation.getIdSecuritiesOperationPk());
		objSecuritiesComponentTO.setIdSecurityCode(objSettlementOperationTO.getIdSecurityCode());
		securitiesComponentService.get().executeSecuritiesComponent(objSecuritiesComponentTO);
	}
	
	/**
	 * Creates the account annotations.
	 *
	 * @param settlementOperationTO the settlement operation to
	 * @param issuerDetail the issuer detail
	 * @param indMarketFact the ind market fact
	 * @param loggerUser the logger user
	 */
	private void createAccountAnnotations(SettlementOperationTO settlementOperationTO, Object[] issuerDetail, Integer indMarketFact, LoggerUser loggerUser) {

		Long participantSeller = null;
		/**Iterate sellers on update information on placement segment*/
		for(SettlementHolderAccountTO settlementHolderAccountTO : settlementOperationTO.getLstSellerHolderAccounts()){
			/**Assignment participant seller*/
			participantSeller = settlementHolderAccountTO.getIdParticipant();
			/**setup variables to call remote method of placement segment*/
			Long participantCode = settlementHolderAccountTO.getIdParticipant();
			String securityCode = settlementHolderAccountTO.getIdSecurityCode();
			Long operationCode= settlementOperationTO.getIdMechanismOperation();
			BigDecimal quantit= settlementHolderAccountTO.getStockQuantity();
			try {
				securitiesRemoteService.get().substractOperationPlacement(participantCode, securityCode, operationCode, quantit);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(SettlementHolderAccountTO settlementHolderAccountTO : settlementOperationTO.getLstBuyerHolderAccounts()){
			AccountAnnotationOperation accountAnnotationOperation = new AccountAnnotationOperation();
			accountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
			accountAnnotationOperation.setExpeditionDate(issuerDetail[1] == null ? null : (Date)issuerDetail[1]);
			accountAnnotationOperation.setAlternativeCode(issuerDetail[0] == null ? null : (String)issuerDetail[0]);
			accountAnnotationOperation.setIndPrimarySettlement(ComponentConstant.ONE);
			accountAnnotationOperation.setIndLien(ComponentConstant.ZERO);
			//accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
			//accountAnnotationOperation.setCertificateNumber("POR COMPLETAR"); // wut 
			accountAnnotationOperation.setHolderAccount(new HolderAccount(settlementHolderAccountTO.getIdHolderAccount()));
			accountAnnotationOperation.setSecurity(new Security(settlementHolderAccountTO.getIdSecurityCode()));
			//accountAnnotationOperation.setSerialNumber("NO"); //wut 
			accountAnnotationOperation.setTotalBalance(settlementHolderAccountTO.getStockQuantity());
			accountAnnotationOperation.setPlacementParticipant(new Participant(getSellerParticipant(settlementOperationTO.getIdMechanismOperation())));
			
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
			custodyOperation.setApprovalDate(CommonsUtilities.currentDateTime());
			custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
			custodyOperation.setState(DematerializationStateType.APPROVED.getCode());
			custodyOperation.setRegistryUser(loggerUser.getUserName());
			custodyOperation.setApprovalUser(loggerUser.getUserName());
			custodyOperation.setOperationNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_DEMATERIALIZATION));
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_BANK_SHARES_PURCHASE.getCode());
			
			accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode());
			accountAnnotationOperation.setCustodyOperation(custodyOperation);
			create(accountAnnotationOperation);
			
			if(ComponentConstant.ONE.equals(indMarketFact)){
				accountAnnotationOperation.setAccountAnnotationMarketFact(new ArrayList<AccountAnnotationMarketFact>());
				List<SettlementAccountMarketfact> accountsMktFacts = settlementProcessService.getSettlementAccountMarketfacts(settlementHolderAccountTO.getIdSettlementAccountOperation());
				for(SettlementAccountMarketfact accountMarketFact: accountsMktFacts){
					AccountAnnotationMarketFact accountAnnotationMarketFact = new AccountAnnotationMarketFact();
					accountAnnotationMarketFact.setAccountAnnotationOperation(accountAnnotationOperation);
					accountAnnotationMarketFact.setMarketDate(accountMarketFact.getMarketDate());
					accountAnnotationMarketFact.setMarketRate(accountMarketFact.getMarketRate());
					accountAnnotationMarketFact.setMarketPrice(accountMarketFact.getMarketPrice());
					accountAnnotationMarketFact.setOperationQuantity(accountMarketFact.getMarketQuantity());
					accountAnnotationOperation.getAccountAnnotationMarketFact().add(accountAnnotationMarketFact);
					create(accountAnnotationMarketFact);
				}
			}
			
			/**setup variables to call remote method of placement segment*/
			String securityCode = settlementHolderAccountTO.getIdSecurityCode();
			Long operationCode= accountAnnotationOperation.getIdAnnotationOperationPk();
			BigDecimal quantit= settlementHolderAccountTO.getStockQuantity();
			try {
				securitiesRemoteService.get().addAnnotationPlacement(participantSeller, securityCode, operationCode, quantit);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the seller participant.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the seller participant
	 */
	private Long getSellerParticipant(Long idMechanismOperation) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select mo.sellerParticipant.idParticipantPk from MechanismOperation mo");  
		stringBuffer.append(" where mo.idMechanismOperationPk = :idMechanismOperation ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		
		return (Long)findObjectByQueryString(stringBuffer.toString(), parameters);
	}

	/**
	 * Gets the placement detail.
	 *
	 * @param securityCode the security code
	 * @param participantPk the participant pk
	 * @return the placement detail
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unused")
	private PlacementSegmentDetail getPlacementDetail(String securityCode, Long participantPk) throws ServiceException{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" Select det From PlacementSegmentDetail det					");
		stringBuffer.append(" Inner Join det.placementSegment placement						");
		stringBuffer.append(" Inner Join placement.placementSegParticipaStructs part		");
		stringBuffer.append(" Where det.security.idSecurityCodePk = :securityCode 	  And	");
		stringBuffer.append(" 		part.participant.idParticipantPk = :participantPk And	");
		stringBuffer.append(" 		placement.placementSegmentState = :openState			");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("securityCode", securityCode);
		parameters.put("participantPk", participantPk);
		parameters.put("openState", PlacementSegmentStateType.OPENED.getCode());		
		
		return (PlacementSegmentDetail) findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
}
