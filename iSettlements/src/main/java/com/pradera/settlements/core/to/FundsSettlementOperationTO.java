package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FundsSettlementOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class FundsSettlementOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The operation part. */
	private Integer operationPart;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The settled quantity. */
	private BigDecimal settledQuantity;
	
	/** The total quantity. */
	private BigDecimal totalQuantity;
	
	/** The settlement accounts. */
	private List<FundsSettlementAccountTO> settlementAccounts;
	
	/** The participant settlements. */
	private List<FundsParticipantSettlementTO> participantSettlements;
	
	/**
	 * Instantiates a new funds settlement operation to.
	 */
	public FundsSettlementOperationTO() {
		super();
		this.settlementAccounts = new ArrayList<FundsSettlementAccountTO>();
		this.participantSettlements = new ArrayList<FundsParticipantSettlementTO>();
	}
	
	/**
	 * Gets the id settlement operation.
	 *
	 * @return the id settlement operation
	 */
	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}
	
	/**
	 * Sets the id settlement operation.
	 *
	 * @param idSettlementOperation the new id settlement operation
	 */
	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the settled quantity.
	 *
	 * @return the settled quantity
	 */
	public BigDecimal getSettledQuantity() {
		return settledQuantity;
	}
	
	/**
	 * Sets the settled quantity.
	 *
	 * @param settledQuantity the new settled quantity
	 */
	public void setSettledQuantity(BigDecimal settledQuantity) {
		this.settledQuantity = settledQuantity;
	}
	
	/**
	 * Gets the total quantity.
	 *
	 * @return the total quantity
	 */
	public BigDecimal getTotalQuantity() {
		return totalQuantity;
	}
	
	/**
	 * Sets the total quantity.
	 *
	 * @param totalQuantity the new total quantity
	 */
	public void setTotalQuantity(BigDecimal totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}
	
	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}
	
	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}
	
	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}

	/**
	 * Gets the settlement accounts.
	 *
	 * @return the settlement accounts
	 */
	public List<FundsSettlementAccountTO> getSettlementAccounts() {
		return settlementAccounts;
	}

	/**
	 * Sets the settlement accounts.
	 *
	 * @param settlementAccounts the new settlement accounts
	 */
	public void setSettlementAccounts(
			List<FundsSettlementAccountTO> settlementAccounts) {
		this.settlementAccounts = settlementAccounts;
	}

	/**
	 * Gets the participant settlements.
	 *
	 * @return the participant settlements
	 */
	public List<FundsParticipantSettlementTO> getParticipantSettlements() {
		return participantSettlements;
	}

	/**
	 * Sets the participant settlements.
	 *
	 * @param participantSettlements the new participant settlements
	 */
	public void setParticipantSettlements(
			List<FundsParticipantSettlementTO> participantSettlements) {
		this.participantSettlements = participantSettlements;
	}
	
	
}
