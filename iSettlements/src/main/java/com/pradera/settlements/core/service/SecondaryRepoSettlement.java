package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecondaryRepoSettlement.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class SecondaryRepoSettlement extends CrudDaoServiceBean{

	
	/** The settlement process service bean. */
	@EJB
	private SettlementProcessService settlementProcessServiceBean;
	
	/** The guarantees settlement. */
	@EJB
	private GuaranteesSettlement guaranteesSettlement;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	
	/**
	 * Settle secondary repo operation.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param idOperationPart the id operation part
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void settleSecondaryRepoOperation(SettlementOperationTO objSettlementOperationTO, Integer idOperationPart, LoggerUser loggerUser) throws ServiceException
	{
		try {
			//we have to update the state for the holder account from the original REPORT operation in TERM PART because they will replaced
			//we get the list of holder account operation from the original REPORT
			List<Object[]> lstReportHolderAccountOperation= settlementProcessServiceBean.getListReportAccountOperation(objSettlementOperationTO.getIdReferencedOperation());
			
			/* STEP 01:
			 * TO MODIFY THE HOLDER ACCOUNT OPERATION FROM ORIGINAL REPORT OPERATION. 
			 * TO MOVE THE RECEIVED GUARANTEES BY REPORTING HOLDER ACCOUNT IN STOCK DIVIDENS PROCESSES.
			 *  - The old seller holder accounts in TERM part will be updated to MODIFIED state
			 *  - To withdrawal the stock dividends guarantees from the reporting holder account in ratio of sale stock quantity
			 *  - The new seller holder accounts in TERM part will be saved as CONFIRMED state
			 *  - To deposit the stock dividends guarantees to the new reporting holder account in ratio of bought stock quantity  
			 */
			modifyHolderAccountOperationFromOriginalReport(objSettlementOperationTO,lstReportHolderAccountOperation, idOperationPart, loggerUser);
			
			/*
			 * STEP 02:
			 * UPDATE THE STATES AND STOCK REFERENCE FROM THE REPO OPERATION
			 * - To update the mechanism operation state
			 * - To update the stock reference from participant operation
			 */
			//update the state of the mechanism operation
//			settlementProcessServiceBean.updateMechanisOperationState(objSettlementOperationTO.getIdMechanismOperation(), 
//														MechanismOperationStateType.CASH_SETTLED.getCode(), loggerUser, false,null);
			
			//update the stock reference for the participants inside the mechanism operation
			List<Object[]> lstIdParticipantOperation= new ArrayList<Object[]>();
			List<Object[]> lstIdParticipantMechanismOperation = settlementProcessServiceBean.getListIdParticipantOperation(objSettlementOperationTO.getIdMechanismOperation(), idOperationPart, 
																																				NegotiationConstant.STOCK_INCHARGE);
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdParticipantMechanismOperation)) {
				lstIdParticipantOperation.addAll(lstIdParticipantMechanismOperation);
			}
			settlementProcessServiceBean.updateStockReferenceParticipantOperation(lstIdParticipantOperation, loggerUser);
			
		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	/**
	 * Modify holder account operation from original report.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param lstReportHolderAccountOperation the lst report holder account operation
	 * @param idOperationPart the id operation part
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void modifyHolderAccountOperationFromOriginalReport(SettlementOperationTO objSettlementOperationTO, List<Object[]> lstReportHolderAccountOperation, 
																	Integer idOperationPart, LoggerUser loggerUser) throws ServiceException
	{
		//List<SettlementHolderAccountTO> lstSellerRepoHolderAccounts = objSettlementOperationTO.getLstSellerHolderAccounts(); //seller from REPO operation
		//List<SettlementHolderAccountTO> lstBuyerRepoHolderAccounts = objSettlementOperationTO.getLstBuyerHolderAccounts(); //buyer from REPO operation
		List<HolderAccountOperation> lstNewReportAccountOperation= new ArrayList<HolderAccountOperation>(); //new holder account operation to be created in original report operation
		List<Long> lstIdHolderAccountOperation= new ArrayList<Long>(); //list of HolderAccountOperation ID from original report operation to be updated to MODIFIED state 
		
		//list of holder accounts for REPO settlement in component
		List<HolderAccountBalanceTO> lstSourceHolderAccountBalance= new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> lstTargetHolderAccountBalance= new ArrayList<HolderAccountBalanceTO>();
		
		BigDecimal totalMovedGuaranteeBalance= new BigDecimal(0); //total guarantee from delivered stock dividends to reporting holder account
		//we must identify the holder account from original REPORT who sell part of their position to be MODIFIED
		if (Validations.validateListIsNotNullAndNotEmpty(objSettlementOperationTO.getLstSellerHolderAccounts()) && Validations.validateListIsNotNullAndNotEmpty(lstReportHolderAccountOperation)) 
		{
			for (SettlementHolderAccountTO objSettlementHolderAccountTO: objSettlementOperationTO.getLstSellerHolderAccounts())
			{
				//we search the holder account form original report operation
				for (Object[] arrObject: lstReportHolderAccountOperation)
				{
					Long idHolderAccountOperation= new Long(arrObject[0].toString()); //ID original holder account operation from REPORT operation as seller in TERM PART
					Long idHolderAccount= new Long(arrObject[1].toString()); //ID holder account from REPORT operation as seller in TERM PART
					BigDecimal stockQuantity= new BigDecimal(arrObject[2].toString()); //stock quantity from original reporting holder account
					//if the reporting holder account from original REPORT operation is a seller in REPO operation
					BigDecimal termSettlementPrice = (BigDecimal)arrObject[4];
					if (idHolderAccount.equals(objSettlementHolderAccountTO.getIdHolderAccount())) 
					{
						//we add the original reporting holder account to be MODIFIED
						lstIdHolderAccountOperation.add(idHolderAccountOperation);
						// we reference original report
						//objSettlementHolderAccountTO.setIdRefRepoAccountOperation(idHolderAccountOperation);
						//it must save the guarantee operation to principal guarantee for this holder account operation. Guarantee opening
						
						//new stock quantity for the reporting holder account in the original REPORT operation
						BigDecimal newStockQuantity= stockQuantity.subtract(objSettlementHolderAccountTO.getStockQuantity());
						//First, the holder account must sell all the stock quantity from original position
						//objSettlementHolderAccountTO.setStockQuantity(stockQuantity);
						//we set the holder account balance information to settle the stocks in sale position
						HolderAccountBalanceTO objSellerHolderAccountBalanceTO = new HolderAccountBalanceTO();
						objSellerHolderAccountBalanceTO.setIdHolderAccount(objSettlementHolderAccountTO.getIdHolderAccount());
						objSellerHolderAccountBalanceTO.setIdParticipant(objSettlementHolderAccountTO.getIdParticipant());
						objSellerHolderAccountBalanceTO.setIdSecurityCode(objSettlementHolderAccountTO.getIdSecurityCode());
						objSellerHolderAccountBalanceTO.setStockQuantity(stockQuantity);
						createGuaranteeOperation(objSellerHolderAccountBalanceTO,idHolderAccountOperation);
//						if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
//							objSellerHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>(0));
//							List<AccountOperationMarketFact> objMarketFacts = settlementProcessServiceBean.getAccountOperationMarketfacts(objSettlementHolderAccountTO.getIdHolderAccountOperation());
//							for(AccountOperationMarketFact objMarketFact : objMarketFacts){
//								MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
//								marketFactAccountTO.setMarketDate(objMarketFact.getMarketDate());
//								marketFactAccountTO.setMarketRate(objMarketFact.getMarketRate());
//								marketFactAccountTO.setMarketPrice(objMarketFact.getMarketPrice());
//								marketFactAccountTO.setQuantity(objSellerHolderAccountBalanceTO.getStockQuantity());
//								objSellerHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
//							}
//						}
						lstSourceHolderAccountBalance.add(objSellerHolderAccountBalanceTO);
						//if the new stock quantity is equals to zero then the reporting holder account is selling all the stock. 
						//So we don't require to create a new holder account operation to the REPORT operation
						if (newStockQuantity.compareTo(BigDecimal.ZERO) > 0)
						{
							//the reporting holder account is selling just a part of all its original stock quantity
							//we create the new holder account operation from original REPORT operation with the new stock quantity
							Long idRefHolderAccount= new Long(arrObject[3].toString()); //ID holder account from REPORT operation as buyer in CASH PART
							HolderAccountOperation objHolderAccountOperation = populateHolderAccountOperation(objSettlementHolderAccountTO, idRefHolderAccount, 
																												objSettlementOperationTO.getIdReferencedOperation(), newStockQuantity,termSettlementPrice);
							//settlementProcessServiceBean.saveHolderAccountOperation(objHolderAccountOperation);
							this.create(objHolderAccountOperation);
							//we add the new holder account operation to be saved in the original REPORT operation as reporting
							lstNewReportAccountOperation.add(objHolderAccountOperation);
							//we add the holder account as buyer using the new stock quantity to generate the purchase movement
							HolderAccountBalanceTO objBuyerHolderAccountBalanceTO = new HolderAccountBalanceTO();
							objBuyerHolderAccountBalanceTO.setIdHolderAccount(objSettlementHolderAccountTO.getIdHolderAccount());
							objBuyerHolderAccountBalanceTO.setIdParticipant(objSettlementHolderAccountTO.getIdParticipant());
							objBuyerHolderAccountBalanceTO.setIdSecurityCode(objSettlementHolderAccountTO.getIdSecurityCode());
							objBuyerHolderAccountBalanceTO.setStockQuantity(newStockQuantity);
							createGuaranteeOperation(objBuyerHolderAccountBalanceTO,idHolderAccountOperation);
//							if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
//								objBuyerHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>(0));
//								List<AccountOperationMarketFact> objMarketFacts = settlementProcessServiceBean.getAccountOperationMarketfacts(objSettlementHolderAccountTO.getIdHolderAccountOperation());
//								for(AccountOperationMarketFact objMarketFact : objMarketFacts){
//									MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
//									marketFactAccountTO.setMarketDate(objMarketFact.getMarketDate());
//									marketFactAccountTO.setMarketRate(objMarketFact.getMarketRate());
//									marketFactAccountTO.setMarketPrice(objMarketFact.getMarketPrice());
//									marketFactAccountTO.setQuantity(objBuyerHolderAccountBalanceTO.getStockQuantity());
//									objBuyerHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
//								}
//							}
							lstTargetHolderAccountBalance.add(objBuyerHolderAccountBalanceTO);
						}
						
						//now we have to withdrawal the guarantees from delivered stock dividends to reporting holder account 
						//applies to reporting balance and RF operations
						if(ComponentConstant.ONE.equals(objSettlementOperationTO.getIndReportingBalance()) && 
								InstrumentType.FIXED_INCOME.getCode().equals(objSettlementOperationTO.getInstrumentType()))
						{
							//only the equities manage stock dividends guarantees. The fixed income doesn't have stock dividends guarantees
							BigDecimal movedGuaranteeBalance= guaranteesSettlement.generateWithdrawalReportingGuaranteBalance(
																														idHolderAccountOperation, idOperationPart, 
																														objSettlementOperationTO.getSettlementSchema(), 
																														objSettlementOperationTO.getIdModality(), 
																														stockQuantity, objSettlementHolderAccountTO.getStockQuantity());
							//we accumulate the moved guarantee from reporting holder accounts. This total guarantee will be distributed along buyers holder accounts
							totalMovedGuaranteeBalance = totalMovedGuaranteeBalance.add(movedGuaranteeBalance);
						}
						break;
					}
				}
			}
		}
		//we must create the new holder account from original report. They will be the buyers from REPO operation
		if (Validations.validateListIsNotNullAndNotEmpty(objSettlementOperationTO.getLstBuyerHolderAccounts()) && Validations.validateListIsNotNullAndNotEmpty(lstReportHolderAccountOperation))
		{
			int count=0; //count to know the position in the list lstBuyerRepoHolderAccounts
			BigDecimal deliveredGuaranteeQuantity= new BigDecimal(0); //accumulated quantity of delivered guarantees to new holder accounts from REPO operation
			for (SettlementHolderAccountTO objSettlementHolderAccountTO: objSettlementOperationTO.getLstBuyerHolderAccounts())
			{
				++count;
				BigDecimal newStockQuantity= objSettlementHolderAccountTO.getStockQuantity();
				Long idRefHolderAccount= null;
				BigDecimal termSettlementPrice = null;
				//we must verify if the buyer in REPO operation exists as reporting in the original REPORT operation
				for (Object[] arrObject: lstReportHolderAccountOperation)
				{
					Long idHolderAccountOperation= new Long(arrObject[0].toString());
					Long idHolderAccount= new Long(arrObject[1].toString());
					BigDecimal stockQuantity= new BigDecimal(arrObject[2].toString()); //stock quantity from original reporting holder account
					Long idTempRefHolderAccount= new Long(arrObject[3].toString()); //ID holder account from REPORT operation as buyer in CASH PART
					termSettlementPrice = (BigDecimal)arrObject[4];
					
					if (idHolderAccount.equals(objSettlementHolderAccountTO.getIdHolderAccount()))
					{
						//if the buyer holder account in REPO operation is equals to reporting in the original REPORT operation that's because it is buying part of other holder account. 
						//It is increasing its stocks at original REPORT operation, so we must add the stock quantity for the new Holder account to be saved 
						newStockQuantity = newStockQuantity.add(stockQuantity);
						//the old HolderAccountOperation as seller in TERM part must be MODIFIED
						lstIdHolderAccountOperation.add(idHolderAccountOperation);
						
						//this holder account will sell all the old stock quantity and then will buy the new stock quantity  
						HolderAccountBalanceTO objSellerHolderAccountBalanceTO = new HolderAccountBalanceTO();
						objSellerHolderAccountBalanceTO.setIdHolderAccount(objSettlementHolderAccountTO.getIdHolderAccount());
						objSellerHolderAccountBalanceTO.setIdParticipant(objSettlementHolderAccountTO.getIdParticipant());
						objSellerHolderAccountBalanceTO.setIdSecurityCode(objSettlementHolderAccountTO.getIdSecurityCode());
						objSellerHolderAccountBalanceTO.setStockQuantity(objSettlementHolderAccountTO.getStockQuantity());
						createGuaranteeOperation(objSellerHolderAccountBalanceTO,idHolderAccountOperation);
//						if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
//							objSellerHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>(0));
//							List<AccountOperationMarketFact> objMarketFacts = settlementProcessServiceBean.getAccountOperationMarketfacts(objSettlementHolderAccountTO.getIdHolderAccountOperation());
//							for(AccountOperationMarketFact objMarketFact : objMarketFacts){
//								MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
//								marketFactAccountTO.setMarketDate(objMarketFact.getMarketDate());
//								marketFactAccountTO.setMarketRate(objMarketFact.getMarketRate());
//								marketFactAccountTO.setMarketPrice(objMarketFact.getMarketPrice());
//								marketFactAccountTO.setQuantity(objSellerHolderAccountBalanceTO.getStockQuantity());
//								objSellerHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
//							}
//						}
						lstSourceHolderAccountBalance.add(objSellerHolderAccountBalanceTO);
						idRefHolderAccount = idTempRefHolderAccount; //this ID ref holder account operation must be saved in the new holder account operation in TERM PART
						break;
					}
				}
				//we create the new holder account from original REPORT operation with the new stock quantity
				HolderAccountOperation objHolderAccountOperation = populateHolderAccountOperation(objSettlementHolderAccountTO, idRefHolderAccount, 
																									objSettlementOperationTO.getIdReferencedOperation(), newStockQuantity,termSettlementPrice);
				this.create(objHolderAccountOperation);
				//we add the new holder account operation to be saved in the original REPORT operation as reporting
				lstNewReportAccountOperation.add(objHolderAccountOperation);
				//objSettlementHolderAccountTO.setIdRefRepoAccountOperation(objHolderAccountOperation.getIdHolderAccountOperationPk());
				//we add the holder account as buyer using the new stock quantity to generate the purchase movement
				HolderAccountBalanceTO objBuyerHolderAccountBalanceTO = new HolderAccountBalanceTO();
				objBuyerHolderAccountBalanceTO.setIdHolderAccount(objSettlementHolderAccountTO.getIdHolderAccount());
				objBuyerHolderAccountBalanceTO.setIdParticipant(objSettlementHolderAccountTO.getIdParticipant());
				objBuyerHolderAccountBalanceTO.setIdSecurityCode(objSettlementHolderAccountTO.getIdSecurityCode());
				objBuyerHolderAccountBalanceTO.setStockQuantity(newStockQuantity);
				createGuaranteeOperation(objBuyerHolderAccountBalanceTO,objHolderAccountOperation.getIdHolderAccountOperationPk());
//				if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
//					objBuyerHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>(0));
//					List<AccountOperationMarketFact> objMarketFacts = settlementProcessServiceBean.getAccountOperationMarketfacts(objSettlementHolderAccountTO.getIdHolderAccountOperation());
//					for(AccountOperationMarketFact objMarketFact : objMarketFacts){
//						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
//						marketFactAccountTO.setMarketDate(objMarketFact.getMarketDate());
//						marketFactAccountTO.setMarketRate(objMarketFact.getMarketRate());
//						marketFactAccountTO.setMarketPrice(objMarketFact.getMarketPrice());
//						marketFactAccountTO.setQuantity(objBuyerHolderAccountBalanceTO.getStockQuantity());
//						objBuyerHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
//					}
//				}
				lstTargetHolderAccountBalance.add(objBuyerHolderAccountBalanceTO);
				
				//now we distribute the moved stock dividends guarantee along the buyer holder accounts according their purchase quantities
				//if (NegotiationModalityType.REPORT_EQUITIES.getCode().equals(objSettlementOperationTO.getIdModality()))
				if(ComponentConstant.ONE.equals(objSettlementOperationTO.getIndReportingBalance()) && 
						InstrumentType.FIXED_INCOME.getCode().equals(objSettlementOperationTO.getInstrumentType()))
				{
					//only the equities manage stock dividends guarantees. The fixed income doesn't have stock dividends guarantees
					BigDecimal stockQuantity;
					if (count == objSettlementOperationTO.getLstBuyerHolderAccounts().size()) {
						//we got the last record from the list, so this holder account must receive the rest of guarantee quantity
						stockQuantity = totalMovedGuaranteeBalance.subtract(deliveredGuaranteeQuantity);
					} else {
						//we calculate the guarantee quantity to deliver to the new reporting holder account in REPO operation according the bought quantity
						stockQuantity = objSettlementHolderAccountTO.getStockQuantity().multiply(totalMovedGuaranteeBalance).
																				divide(objSettlementOperationTO.getStockQuantity()).setScale(0, BigDecimal.ROUND_HALF_UP);
					}
					guaranteesSettlement.generateDepositNewReportingGuaranteeBalance(objHolderAccountOperation.getIdHolderAccountOperationPk(), objSettlementHolderAccountTO, 
															 														stockQuantity, objSettlementOperationTO.getSettlementSchema(), 
															 														idOperationPart, objSettlementOperationTO.getIdModality());
					
					deliveredGuaranteeQuantity = deliveredGuaranteeQuantity.add(stockQuantity); //we accumulate the delivered stock dividends guarantee quantity
				}	
			}
		}
		//now we update the state of the old holder account as reporting from the original REPORT operation
		settlementProcessServiceBean.updateHolderAccountOperationState(lstIdHolderAccountOperation, 
														HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), loggerUser);
		
		//execute the component to REPO operation settlement
		List<SettlementHolderAccountTO> lstHolderAccountsOperation= new ArrayList<SettlementHolderAccountTO>();
		lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstBuyerHolderAccounts());
		lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstSellerHolderAccounts());
		List<Long> lstSellerAcountOperation= new ArrayList<Long>();
		List<Long> lstBuyerAcountOperation= new ArrayList<Long>();
		//NegotiationModality objModality = settlementProcessServiceBean.getNegotiationModality(objSettlementOperationTO.getIdModality());
		settlementProcessServiceBean.updateBalancesAccountOperation(objSettlementOperationTO, lstHolderAccountsOperation,
													lstSourceHolderAccountBalance, lstTargetHolderAccountBalance, 
													lstSellerAcountOperation, lstBuyerAcountOperation, 
													loggerUser,idepositarySetup.getIndMarketFact());
	}
	
		
	/**
	 * Creates the guarantee operation.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @param idHolderAccountOperation the id holder account operation
	 */
	private void createGuaranteeOperation(
			HolderAccountBalanceTO objHolderAccountBalanceTO,Long idHolderAccountOperation) {
		GuaranteeOperation objGuaranteeOperation = guaranteesSettlement.saveGuaranteeOperation(idHolderAccountOperation, 
				objHolderAccountBalanceTO.getIdParticipant(), objHolderAccountBalanceTO.getIdHolderAccount(), 
				objHolderAccountBalanceTO.getIdSecurityCode(), objHolderAccountBalanceTO.getStockQuantity(), 
				GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.PRINCIPAL.getCode(), null);
		objHolderAccountBalanceTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
	}


	/**
	 * Populate holder account operation.
	 *
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param idRefHolderAccountOperation the id ref holder account operation
	 * @param idReportOperation the id report operation
	 * @param newStockQuantity the new stock quantity
	 * @param termSettlementPrice the term settlement price
	 * @return the holder account operation
	 */
	private HolderAccountOperation populateHolderAccountOperation(SettlementHolderAccountTO objSettlementHolderAccountTO, Long idRefHolderAccountOperation, 
																	Long idReportOperation, BigDecimal newStockQuantity, BigDecimal termSettlementPrice)
	{
		HolderAccountOperation objHolderAccountOperation = new HolderAccountOperation();
		Participant objParticipant = new Participant();
		objParticipant.setIdParticipantPk(objSettlementHolderAccountTO.getIdParticipant());
		objHolderAccountOperation.setInchargeStockParticipant(objParticipant);
		objHolderAccountOperation.setInchargeFundsParticipant(objParticipant);
		
		HolderAccount objHolderAccount = new HolderAccount();
		objHolderAccount.setIdHolderAccountPk(objSettlementHolderAccountTO.getIdHolderAccount());
		objHolderAccountOperation.setHolderAccount(objHolderAccount);
		
		MechanismOperation objReportOperation = new MechanismOperation();
		objReportOperation.setIdMechanismOperationPk(idReportOperation);
		objHolderAccountOperation.setMechanismOperation(objReportOperation);
		
		if (Validations.validateIsNotNull(idRefHolderAccountOperation))
		{
			HolderAccountOperation objRefAccountOperation = new HolderAccountOperation();
			objRefAccountOperation.setIdHolderAccountOperationPk(idRefHolderAccountOperation);
			objHolderAccountOperation.setRefAccountOperation(objRefAccountOperation);
		}
		
		objHolderAccountOperation.setConfirmDate(CommonsUtilities.currentDateTime());
		objHolderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
		//objHolderAccountOperation.setStockReference(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		//objHolderAccountOperation.setStockBlockDate(CommonsUtilities.currentDateTime());
		objHolderAccountOperation.setIndAutomaticAsignment(ComponentConstant.ZERO);
		objHolderAccountOperation.setIndIncharge(ComponentConstant.ZERO);
		objHolderAccountOperation.setOperationPart(ComponentConstant.TERM_PART);
		objHolderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		objHolderAccountOperation.setRole(ComponentConstant.SALE_ROLE);
		objHolderAccountOperation.setStockQuantity(newStockQuantity);
		objHolderAccountOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(termSettlementPrice, newStockQuantity));
		
		return objHolderAccountOperation;
	}
	
}
