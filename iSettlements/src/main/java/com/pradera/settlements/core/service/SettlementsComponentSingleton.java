package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.jboss.ejb3.annotation.TransactionTimeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.PrimaryPlacementComponentTO;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.AssignmentRequest;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.ParticipantAssignment;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.McnCancelReasonType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.ParticipantAssignmentStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ChainedHolderOperation;
import com.pradera.model.settlement.HolderChainDetail;
import com.pradera.model.settlement.OperationSettlement;
import com.pradera.model.settlement.OperationUnfulfillment;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;

import com.pradera.model.settlement.SettlementReport;
import com.pradera.model.settlement.SettlementUnfulfillment;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.UnfulfillmentParameterType;
import com.pradera.negotiations.accountassignment.exception.AccountAssignmentException;
import com.pradera.negotiations.accountassignment.service.AccountAssignmentService;
import com.pradera.negotiations.mcnoperations.service.McnOperationServiceBean;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.settlements.chainedoperation.service.ChainedOperationsServiceBean;
import com.pradera.settlements.chainedoperation.to.ChainedOperationDataTO;
import com.pradera.settlements.chainedoperation.to.RegisterChainedOperationTO;
import com.pradera.settlements.core.to.NetParticipantPositionTO;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;
import com.pradera.settlements.core.to.NetSettlementOperationTO;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.settlements.core.to.SettlementUnfulfillmentTO;
import com.pradera.settlements.currencyexchange.service.CurrencySettlementService;
import com.pradera.settlements.notificator.SettlementNotificationEvent;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.webclient.SettlementServiceConsumer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementsComponentSingleton.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Singleton
public class SettlementsComponentSingleton extends CrudDaoServiceBean {
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(SettlementsComponentSingleton.class);
	
	/** The negotiation operation service bean. */
	@EJB
	NegotiationOperationServiceBean negotiationOperationServiceBean;
	
	/** The cancellation operation service. */
	@EJB
	CancellationOperationService cancellationOperationService;
	
	/** The mcn operation service bean. */
	@EJB
	McnOperationServiceBean mcnOperationServiceBean;
	
	/** The settlement process service bean. */
	@EJB
	SettlementProcessService settlementProcessServiceBean;
	
	/** The collection process service bean. */
	@EJB
	CollectionProcessService collectionProcessServiceBean;
	
	/** The funds component singleton. */
	@EJB
	FundsComponentSingleton fundsComponentSingleton;
	
	/** The report settlement. */
	@EJB
	ReportSettlement reportSettlement;
	
	/** The secondary repo settlement. */
	@EJB
	SecondaryRepoSettlement secondaryRepoSettlement;
	
	/** The primary placement settlement. */
	@EJB
	PrimaryPlacementSettlement primaryPlacementSettlement;
	
	/** The unfulfillment service. */
	@EJB
	UnfulfillmentService unfulfillmentService;
	
	/** The chained operations service bean. */
	@EJB
	ChainedOperationsServiceBean chainedOperationsServiceBean;
	
	/** The securities component service. */
	@Inject
    Instance<SecuritiesComponentService> securitiesComponentService;
	
	/** The securities remote service. */
	@Inject Instance<SecuritiesRemoteService> securitiesRemoteService;
	
	/** The parameter service bean. */
	@Inject
	ParameterServiceBean parameterServiceBean;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The batch service bean. */
	@EJB
	private BatchServiceBean batchServiceBean;
	
	/** The account assignment service. */
	@EJB
	private AccountAssignmentService accountAssignmentService;
	
	/** The currency settlement service. */
	@EJB
	private CurrencySettlementService currencySettlementService;
	
	/** The settlement service consumer. */
	@EJB
	private SettlementServiceConsumer settlementServiceConsumer;
	
	/** The notification event. */
	@Inject Event<SettlementNotificationEvent> notificationEvent;
	
	
	/**
	 * Execute gross settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @param operationId the operation id
	 * @throws Exception the exception
	 */
	//@AccessTimeout(unit=TimeUnit.MINUTES,value=8)
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=180)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public synchronized void executeGrossSettlementProcess(Long idMechanism, Long idModalityGroup, Integer idCurrency, 
			Date settlementDate, Long idSettlementProcess, Integer scheduleType, LoggerUser loggerUser, Long operationId) throws Exception {
		logger.info("executeGrossSettlementProcess: Starting the process ...");
		
		//we remove the operations from settlement process about blocked entities
		//settlementProcessServiceBean.removeOperationToSettlement(idMechanism, idModalityGroup, idCurrency, SettlementSchemaType.GROSS.getCode(), settlementDate,loggerUser);
		
		//settlement to CASH PART
		fundsComponentSingleton.collectMechanismOperations(idSettlementProcess, idMechanism, idModalityGroup, settlementDate, idCurrency, loggerUser);
		
		settleGrossMechanismOperations(idSettlementProcess, idMechanism, idModalityGroup, settlementDate, idCurrency, 
				ComponentConstant.CASH_PART, null, SettlementSchemaType.GROSS.getCode(), loggerUser,operationId, Boolean.FALSE);
		
		settleGrossMechanismOperations(idSettlementProcess, idMechanism, idModalityGroup, settlementDate, idCurrency, 
				ComponentConstant.TERM_PART, null, SettlementSchemaType.GROSS.getCode(),loggerUser,operationId, Boolean.FALSE);

		//we send the settlement information by web service
		Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, idSettlementProcess);
		BusinessProcess objBusinessProcess=new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.SEND_SETTLEMENT_OPERATION_WEBSERVICE.getCode());
		batchServiceBean.registerBatchTx(loggerUser.getUserName(), objBusinessProcess, processParameters);
		//sendSettlementOperationWebClient(lstSettlementOperationTOs, loggerUser);
		
		logger.info("executeGrossSettlementProcess: Finishing the process ...");
	}
	

	/**
	 * Begin settlement net process service.
	 *
	 * @param idMechanism the id mechanism
	 * @param settlementDate the settlement date
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param idSettlementProcess the id settlement process
	 * @param loggerUser the logger user
	 * @param scheduleType the schedule type
	 * @return the settlement process
	 * @throws Exception the exception
	 */
	//@AccessTimeout(unit=TimeUnit.MINUTES,value=8)
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=120)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public synchronized void executeNetSettlementProcess(Long idMechanism, Date settlementDate, Long idModalityGroup, Integer idCurrency, 
								Long idSettlementProcess, LoggerUser loggerUser, Integer scheduleType) throws Exception{

		logger.info("executeNetSettlementProcess: Starting the process ...");
		
		try {
			NetSettlementAttributesTO netSettlementAttributes = new NetSettlementAttributesTO();
			
			if(idSettlementProcess!=null){
				netSettlementAttributes.setSettlementSchedule(settlementProcessServiceBean.getSettlementSchedule(scheduleType, null, null, null));
				processNetParticipantPositions(netSettlementAttributes,idSettlementProcess,true);
				
			}else{
				netSettlementAttributes.setSettlementSchedule(settlementProcessServiceBean.getSettlementSchedule(scheduleType, null, ComponentConstant.ONE, null));
				// is a new execution, calculate participantPositions and operationSettlements
				calculateNetParticipantPositions(idMechanism,settlementDate, idModalityGroup,idCurrency,scheduleType, loggerUser,true, netSettlementAttributes);
				idSettlementProcess = netSettlementAttributes.getSettlementProcess().getIdSettlementProcessPk();
			}
			
			// collect net funds for negative position (debtors)
			fundsComponentSingleton.collectNetFundsParticipantOperation(netSettlementAttributes.getSettlementProcess(), 
																		idMechanism, idModalityGroup, idCurrency,loggerUser);
			
			//is necessary to chained operations, to update the settlement account market facts, provided that they have same rate and date, however, different market price 
			verifyChainedHolderMarkekfactBalance(idSettlementProcess, loggerUser);
			
			// we settle the operations
			settleNetMechanismOperations(idSettlementProcess, idMechanism, idModalityGroup, settlementDate, idCurrency,
														null, scheduleType, SettlementSchemaType.NET.getCode(), 
														loggerUser, null);

			//to deliver the funds to creditor participants from settlement process
			fundsComponentSingleton.deliverNetFundsToParticipant(idSettlementProcess, idMechanism, idModalityGroup, idCurrency,loggerUser);
			
			// unlock institution cashAccounts
			//settlementProcessServiceBean.lockCashAccounts(netSettlementAttributes, ComponentConstant.ZERO,loggerUser);										
			
			//we send the block balance process
			BusinessProcess objUpdateBalanceBusinessProcess= new BusinessProcess();
			objUpdateBalanceBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode());
			Map<String, Object> updateBalanceProcessParameters=new HashMap<String, Object>();
			updateBalanceProcessParameters.put(ComponentConstant.SETTLEMENT_SCHEMA, SettlementSchemaType.NET.getCode());
			batchServiceBean.registerBatchTx(loggerUser.getUserName(), objUpdateBalanceBusinessProcess, updateBalanceProcessParameters);
		} catch (Exception e) {
			
			notificationEvent.fire(getNotification(BooleanType.NO.getCode(), BooleanType.YES.getCode(), ComponentConstant.ZERO, BooleanType.YES.getCode(), null));
			
			logger.error("executeNetSettlementProcess : Error in processing ... ");
			logger.error("idSettlementProcess: " +idSettlementProcess + ", idMechanism: " +idMechanism +", idModalityGroup: " +idModalityGroup + ", currency: " +idCurrency);
			throw e;
		}
		
		logger.info("executeNetSettlementProcess: Finishing the process ...");
	}
	
	/**
	 * Gets the settlement process.
	 *
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @param settlementProcessId the settlement process id
	 * @param execute the execute
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	@AccessTimeout(unit=TimeUnit.MINUTES,value=2)
	public synchronized SettlementProcess  processNetParticipantPositions(NetSettlementAttributesTO netSettlementAttributesTO, Long settlementProcessId, boolean execute) throws ServiceException{
		SettlementProcess settlementProcess = settlementProcessServiceBean.getSettlementProcess(settlementProcessId);
		settlementProcess.setOperationSettlements(new ArrayList<OperationSettlement>());
		settlementProcess.setParticipantPositions(new ArrayList<ParticipantPosition>());
		
		Integer settlementType = null;
		if(execute){
			settlementType = SettlementType.DVP.getCode();
		}
		
		List<Object[]> operationSettlements = settlementProcessServiceBean.getListOperationSettlement(settlementProcessId,null,settlementType);
		
		List<Object[]> pendingOperationSettlements = settlementProcessServiceBean.getListNetSettlementOperations(settlementProcessId);
		
		Map<Long, NetParticipantPositionTO> mapParticipantOperations = new HashMap<Long, NetParticipantPositionTO>();
		
		List<NetSettlementOperationTO> operationsBySettlement = NegotiationUtils.populateSettlementOperationsAndParticipant(pendingOperationSettlements);
		
		init:
		for(NetSettlementOperationTO netSettlementOperation: operationsBySettlement){
			for(Object[] objOperation: operationSettlements){
				OperationSettlement operationSettlement = (OperationSettlement)objOperation[0];
				Long idSettlementOperation = (Long)objOperation[1];
				
				if(idSettlementOperation.equals(netSettlementOperation.getIdSettlementOperation())){
					netSettlementOperation.setOperationSettlement(operationSettlement);
					operationSettlement.setInitialState(operationSettlement.getOperationState());
					if(operationSettlement.getOperationState().equals(OperationSettlementSateType.WITHDRAWN_BY_GUARANTEES.getCode())){
						netSettlementAttributesTO.getLstGuaranteeRemovedOperations().add(netSettlementOperation);
					}else if(operationSettlement.getOperationState().equals(OperationSettlementSateType.WITHDRAWN_BY_SECURITIES.getCode())){
						netSettlementAttributesTO.getLstStockRemovedOperations().add(netSettlementOperation);
					}else if(operationSettlement.getOperationState().equals(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode())){
						netSettlementAttributesTO.getLstPendingSettlementOperations().add(netSettlementOperation);
					}else if (operationSettlement.getOperationState().equals(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode()) && !execute){
						netSettlementAttributesTO.getLstPendingSettlementOperations().add(netSettlementOperation);
					}
					
					settlementProcess.getOperationSettlements().add(operationSettlement);
					continue init;
				}
			}
		}
		netSettlementAttributesTO.setSettlementProcess(settlementProcess);
		mapParticipantOperations = NegotiationUtils.populateParticipantPositions(netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantOperations);
		
		List<ParticipantPosition> newParticipantPositions =  recalculateParticipantPositions(netSettlementAttributesTO, execute);

		if(execute){
			saveAll(newParticipantPositions);
			settlementProcessServiceBean.updateOperationSettlements(settlementProcess);
		}
		
		return settlementProcess;
	}
	
	/**
	 * Recalculate participant positions.
	 *
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @param autoRetire the auto retire
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=30)
	public synchronized List<ParticipantPosition> recalculateParticipantPositions(NetSettlementAttributesTO netSettlementAttributesTO, boolean autoRetire) throws ServiceException {
		logger.info(":::: recalculating participant positions");
		
		checkDebtorParticipantPosition(netSettlementAttributesTO.getMapParticipantPosition());
		
		SettlementProcess settlementProcess = netSettlementAttributesTO.getSettlementProcess();
		
		Map<Long,InstitutionCashAccount> mpCashAccounts = netSettlementAttributesTO.getMpCashAccounts();
		Long mechanismId = settlementProcess.getNegotiationMechanism().getIdNegotiationMechanismPk();
		Long modalityGroupId = settlementProcess.getModalityGroup().getIdModalityGroupPk();
		Integer currency = settlementProcess.getCurrency();
		
		Map<Long,NetParticipantPositionTO> mpNoCashAccounts = new HashMap<Long, NetParticipantPositionTO>(); 
		
		List<ParticipantPosition> newPartPositions = new ArrayList<ParticipantPosition>();
		Map<Long, NetParticipantPositionTO> mapInitialParticipantOperations= new HashMap<Long, NetParticipantPositionTO>();
		Map<Long, NetParticipantPositionTO> mapParticipantOperations= netSettlementAttributesTO.getMapParticipantPosition();
		List<Long> participantsList = new ArrayList<Long>(mapParticipantOperations.keySet());
		//first we get the entire list of participant operations
		List<NetSettlementOperationTO> lstTotalParticipantOperations = new ArrayList<NetSettlementOperationTO>();
		for (Long idParticipant: participantsList) {
			NetParticipantPositionTO participantPositionTO = mapParticipantOperations.get(idParticipant);
			try {
				mapInitialParticipantOperations.put(idParticipant, participantPositionTO.clone());
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			for (NetSettlementOperationTO objNetSettlementOperationTO: participantPositionTO.getLstSettlementOperation()) {
				if (!lstTotalParticipantOperations.contains(objNetSettlementOperationTO)) {
					lstTotalParticipantOperations.add(objNetSettlementOperationTO);
				}
			}
		}
		
		int i = 0;
		reinit:
		while(i < participantsList.size()){
			Long participantId = participantsList.get(i);
			NetParticipantPositionTO initialParticipantPositionTO = mapInitialParticipantOperations.get(participantId);
			NetParticipantPositionTO participantPositionTO = mapParticipantOperations.get(participantId);
			
			//obtains and cache institution cashAccounts for each participant
			InstitutionCashAccount institutionCashAccount = mpCashAccounts.get(participantId);
			BigDecimal availableCashAmount = BigDecimal.ZERO;
			if(institutionCashAccount == null){
				institutionCashAccount = collectionProcessServiceBean.getInstitutionCashAccount(
						mechanismId,modalityGroupId,participantId,currency,SettlementSchemaType.NET.getCode());
				mpCashAccounts.put(participantId, institutionCashAccount);
			}
			
			if(institutionCashAccount!=null){
				availableCashAmount = institutionCashAccount.getAvailableAmount();
			}else{
				availableCashAmount = BigDecimal.ZERO;
				mpNoCashAccounts.put(participantId, participantPositionTO);
			}
			
			BigDecimal buyerPosition = participantPositionTO.getPurchaseSettlementAmount();
			BigDecimal sellerPosition = participantPositionTO.getSaleSettlementAmount();
			
			participantPositionTO.setNetPosition(sellerPosition.subtract(buyerPosition));
			participantPositionTO.setAvailableCashAmount(availableCashAmount);
			participantPositionTO.setMissingAmount(BigDecimal.ZERO);
			
			BigDecimal missingAmount = participantPositionTO.getNetPosition().add(participantPositionTO.getAvailableCashAmount());
			if(missingAmount.compareTo(BigDecimal.ZERO) < 0){
				
				participantPositionTO.setMissingAmount(missingAmount);
				if(autoRetire){
					if(participantPositionTO.getMissingAmount().compareTo(BigDecimal.ZERO) < 0){
						List<NetSettlementOperationTO> pendingOperationsPerParticipant = participantPositionTO.getLstSettlementOperation();
						//find first operation and remove it by funds
						if(pendingOperationsPerParticipant.size() > 0){
							logger.info(":::: attempting operation removal");
							//we must remove the operations from current settlement process
							settlementProcessServiceBean.removeOperationSettlementProcessByFunds(participantPositionTO, 
																	pendingOperationsPerParticipant, mapParticipantOperations, ComponentConstant.PURCHARSE_ROLE);
							
							//we fill up the new participant positions keeping in mind the removed operations
							mapParticipantOperations = NegotiationUtils.populateParticipantPositions(lstTotalParticipantOperations,false);
							logger.info(":::: participant : "+ participantPositionTO.getIdParticipant()  + " size ops "+ participantPositionTO.getLstSettlementOperation().size());
							
							//we verify the participant positions
							/*
							mapParticipantOperations= verifyParticipantPositions(lstTotalParticipantOperations, participantPositionTO, 
																				initialParticipantPositionTO, mapParticipantOperations);
							*/
							participantsList = new ArrayList<Long>(mapParticipantOperations.keySet());
							//we restart the newPartPositions to start over with all participant positions
							newPartPositions = new ArrayList<ParticipantPosition>(); 
							
							logger.info(":::: operation removal finished, recalculating positions");
							i = 0;
							continue reinit;
						}
					}
				}
			}
			else if (initialParticipantPositionTO.isDebtor()) {
				if(autoRetire){
					if (participantPositionTO.getNetPosition().compareTo(BigDecimal.ZERO) > 0) {
						List<NetSettlementOperationTO> pendingOperationsPerParticipant = participantPositionTO.getLstSettlementOperation();
						settlementProcessServiceBean.removeOperationSettlementProcessByFunds(participantPositionTO, 
												pendingOperationsPerParticipant, mapParticipantOperations, ComponentConstant.SALE_ROLE);
						
						mapParticipantOperations = NegotiationUtils.populateParticipantPositions(lstTotalParticipantOperations,false);
						logger.info(":::: participant : "+ participantPositionTO.getIdParticipant()  + " size ops "+ participantPositionTO.getLstSettlementOperation().size());
						
						participantsList = new ArrayList<Long>(mapParticipantOperations.keySet());
						//we restart the newPartPositions to start over with all participant positions
						newPartPositions = new ArrayList<ParticipantPosition>(); 
						
						logger.info(":::: operation removal finished, recalculating positions");
						i = 0;
						continue reinit;
					}
				}
			} else if (!initialParticipantPositionTO.isDebtor()) {
				if(autoRetire){
					if (participantPositionTO.getNetPosition().compareTo(initialParticipantPositionTO.getInitialNetPosition()) > 0) {
						//the creditor position can't increase about the initial net position
						logger.info(" CHANGING CREDITOR PARTICIPANT POSITIONS ");
						logger.info("PARTICIPANT: "+ participantPositionTO.getIdParticipant() +
								"	INITIAL NET POSITION: "+ initialParticipantPositionTO.getInitialNetPosition() +
								" 	CURRENT NET POSITION: "+ participantPositionTO.getNetPosition());
						List<NetSettlementOperationTO> pendingOperationsPerParticipant = participantPositionTO.getLstSettlementOperation();
						settlementProcessServiceBean.removeOperationSettlementProcessByFunds(participantPositionTO, 
												pendingOperationsPerParticipant, mapParticipantOperations, ComponentConstant.SALE_ROLE);
						
						mapParticipantOperations = NegotiationUtils.populateParticipantPositions(lstTotalParticipantOperations,false);
						logger.info(":::: participant : "+ participantPositionTO.getIdParticipant()  + " size ops "+ participantPositionTO.getLstSettlementOperation().size());
						
						participantsList = new ArrayList<Long>(mapParticipantOperations.keySet());
						//we restart the newPartPositions to start over with all participant positions
						newPartPositions = new ArrayList<ParticipantPosition>(); 
						
						logger.info(":::: operation removal finished, recalculating positions");
						i = 0;
						continue reinit;
					}
				}
			}
			newPartPositions.add(populateParticipantPosition(participantPositionTO, settlementProcess));
			i++;
		}
		
		if(mpNoCashAccounts.values().size() > 0){
			List<NetParticipantPositionTO> lstNoCashAccParticipants = new ArrayList<NetParticipantPositionTO>(mpNoCashAccounts.values());
			StringBuilder sb = new StringBuilder(); 
			for (NetParticipantPositionTO participantPositionTO : lstNoCashAccParticipants) {
				sb.append(participantPositionTO.getDisplayCodeMnemonic()).append(GeneralConstants.STR_BR);
			}
			throw new ServiceException(ErrorServiceType.SETTLEMENT_CASH_ACCOUNT_NOT_EXIST, new Object[]{sb.toString()});
		}
		
		logger.info(":::: finishing participant positions reacalculation ");
		return newPartPositions;
	}

		
	/**
	 * Populate participant position.
	 *
	 * @param participantPositionTO the participant position to
	 * @param settlementProcess the settlement process
	 * @return the participant position
	 */
	private ParticipantPosition populateParticipantPosition(NetParticipantPositionTO participantPositionTO, SettlementProcess settlementProcess) {
		ParticipantPosition participantPosition = new ParticipantPosition();
		participantPosition.setParticipant(new Participant(participantPositionTO.getIdParticipant()));
		participantPosition.setSettlementProcess(settlementProcess);
		participantPosition.setBuyerPosition(participantPositionTO.getPurchaseSettlementAmount());
		participantPosition.setSellerPosition(participantPositionTO.getSaleSettlementAmount());
		participantPosition.setNetPosition(participantPositionTO.getNetPosition());
		participantPosition.setIndProcess(ComponentConstant.ONE);
		return participantPosition;
	}

	/**
	 * Calculate net participant positions.
	 *
	 * @param mechanismId the mechanism id
	 * @param settlementDate the settlement date
	 * @param modalityGroupId the modality group id
	 * @param currency the currency
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @param autoRetire the auto retire
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @throws ServiceException the service exception
	 */
	@AccessTimeout(unit=TimeUnit.MINUTES,value=2)
	public synchronized void calculateNetParticipantPositions(Long mechanismId, Date settlementDate, Long modalityGroupId, 
			Integer currency, Integer scheduleType, LoggerUser loggerUser, 
			boolean autoRetire,NetSettlementAttributesTO netSettlementAttributesTO) throws ServiceException {
		
		SettlementProcess settlementProcess= settlementProcessServiceBean.saveSettlementProcess(mechanismId, modalityGroupId, currency, 
				settlementDate, SettlementSchemaType.NET.getCode(), 0l, null, null,scheduleType);
		
		settlementProcess.setOperationSettlements(new ArrayList<OperationSettlement>());
		settlementProcess.setParticipantPositions(new ArrayList<ParticipantPosition>());
		
		//gather settlement operations for date and process
		netSettlementAttributesTO.setSettlementProcess(settlementProcess);
		settlementProcessServiceBean.populateNetSettlementOperations(settlementDate,mechanismId,modalityGroupId,currency,netSettlementAttributesTO);
		
		//populate participant positions and operations considering all operations
		Map<Long, NetParticipantPositionTO> mapTotalPositions = NegotiationUtils.populateParticipantPositions(
																				netSettlementAttributesTO.getAllSettlementOperations(),true);
		
		//we create initial participant positions, not processed
		settlementProcessServiceBean.createInitialParticipantPositions(settlementProcess, mapTotalPositions);
		
		//populate participant positions and operations WITHOUT considering retired operations
		Map<Long, NetParticipantPositionTO> mapParticipantPositions = NegotiationUtils.populateParticipantPositions(
																				netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		
		//we verify and remove the chained operation by stock 
		settlementProcessServiceBean.removeOperationSettlementProcessByStock(netSettlementAttributesTO);
		
		// we recalculate net positions and create new participantPositions
		List<ParticipantPosition> newPositions = recalculateParticipantPositions(netSettlementAttributesTO ,autoRetire);
		
		saveAll(settlementProcess.getOperationSettlements());
		
		update(settlementProcess);
		
		if(autoRetire){
			saveAll(newPositions);
			settlementProcess.setProcessState(SettlementProcessStateType.IN_PROCESS.getCode());
		}else{
			settlementProcess.setProcessState(SettlementProcessStateType.STARTED.getCode());
			
			settlementProcessServiceBean.lockCashAccounts(netSettlementAttributesTO, ComponentConstant.ONE,loggerUser);
		}
	}
	
	/**
	 * Check debtor participant position.
	 *
	 * @param mapParticipantPositions the map participant positions
	 */
	private void checkDebtorParticipantPosition(Map<Long, NetParticipantPositionTO> mapParticipantPositions) {
		List<Long> lstIdParticipants= new ArrayList<Long>(mapParticipantPositions.keySet());
		for (Long idParticipant: lstIdParticipants) {
			NetParticipantPositionTO netParticipantPositionTO = mapParticipantPositions.get(idParticipant);
			BigDecimal buyerPosition = netParticipantPositionTO.getPurchaseSettlementAmount();
			BigDecimal sellerPosition = netParticipantPositionTO.getSaleSettlementAmount();
			netParticipantPositionTO.setInitialNetPosition(sellerPosition.subtract(buyerPosition));
			if (BigDecimal.ZERO.compareTo(netParticipantPositionTO.getInitialNetPosition()) > 0) {
				netParticipantPositionTO.setDebtor(true);
			}
		}
	}
	
	/**
	 * Settle gross mechanism operations.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param settlementDate the settlement date
	 * @param idCurrency the id currency
	 * @param idOperationPart the id operation part
	 * @param scheduleType the schedule type
	 * @param settlementSchema the settlement schema
	 * @param loggerUser the logger user
	 * @param idOperation the id operation
	 * @throws ServiceException the service exception
	 */
	public void settleGrossMechanismOperations(Long idSettlementProcess, Long idMechanism, Long idModalityGroup, Date settlementDate, 
												Integer idCurrency, Integer idOperationPart,Integer scheduleType, 
												Integer settlementSchema, LoggerUser loggerUser, Long idOperation, Boolean excludeFOP) throws ServiceException
	{
		List<Object[]> lstMechanismOperations= null;
		List<SettlementOperationTO> lstSettlementOperations = null;
		List<Long> lstCashMechanismOperation= new ArrayList<Long>();
		List<Long> lstTermMechanismOperation= new ArrayList<Long>();
		List<Long> lstCashSettlementOperation= new ArrayList<Long>();
		List<Long> lstTermSettlementOperation= new ArrayList<Long>();
		//we get the list of account operations to be settled according the mechanism, modality group, settlement date , currency and operation part
		lstMechanismOperations= settlementProcessServiceBean.getListSettlementOperationToGrossSettlement(idSettlementProcess, settlementDate, 
																			idMechanism, idModalityGroup, idCurrency, idOperationPart, idOperation, excludeFOP);
		if (Validations.validateListIsNotNullAndNotEmpty(lstMechanismOperations)){
			lstSettlementOperations = settlementProcessServiceBean.populateSettlementOperations(lstMechanismOperations, idModalityGroup, 
														settlementSchema, idCurrency, idepositarySetup.getIndMarketFact(),
														lstCashSettlementOperation, lstTermSettlementOperation, lstCashMechanismOperation, lstTermMechanismOperation);
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementOperations)){
			
			for (SettlementOperationTO objSettlementOperationTO: lstSettlementOperations){
				try {
					//we settle the operation by STOCKS one by one
					settleGrossMechanismOperation(idSettlementProcess, objSettlementOperationTO, scheduleType, loggerUser);
				} catch (Exception ex) {
					ex.printStackTrace();;
				}
			}
		}
	}
	
	
	/**
	 * 
	 * @param idSettlementProcess
	 * @param idMechanism
	 * @param idModalityGroup
	 * @param settlementDate
	 * @param idCurrency
	 * @param idOperationPart
	 * @param scheduleType
	 * @param settlementSchema
	 * @param loggerUser
	 * @param idOperation
	 * @throws ServiceException
	 */
	  @LoggerAuditWeb
	  public void settleGrossMechanismOperationsUnfulfillment(Long idSettlementProcess, Long idMechanism, Long idModalityGroup, Date settlementDate, 
												Integer idCurrency, Integer idOperationPart,Integer scheduleType, 
												Integer settlementSchema, LoggerUser loggerUser, Long idOperation, Long idMechanismOperationPk, Integer indBuyerSeller) throws ServiceException
	{
		List<Object[]> lstMechanismOperations= null;
		List<SettlementOperationTO> lstSettlementOperations = null;
		List<Long> lstCashMechanismOperation= new ArrayList<Long>();
		List<Long> lstTermMechanismOperation= new ArrayList<Long>();
		List<Long> lstCashSettlementOperation= new ArrayList<Long>();
		List<Long> lstTermSettlementOperation= new ArrayList<Long>();
		
		
		//Listamos las operaciones que deseamos liquidar Solo debe ser 1 por operacion
		lstMechanismOperations= settlementProcessServiceBean.getListSettlementOperationToGrossSettlementUnfulfillment(null, settlementDate, 
																			idMechanism, idModalityGroup, idCurrency, idOperationPart, idOperation, idMechanismOperationPk);
		
		//Damos el formato necesario
		if (Validations.validateListIsNotNullAndNotEmpty(lstMechanismOperations)){
			lstSettlementOperations = settlementProcessServiceBean.populateSettlementOperationsUnfulfillment(lstMechanismOperations, idModalityGroup, 
														settlementSchema, idCurrency, idepositarySetup.getIndMarketFact(),
														lstCashSettlementOperation, lstTermSettlementOperation, lstCashMechanismOperation, lstTermMechanismOperation,
														indBuyerSeller);
		}
		
		//Si el formato es correcto debemos liquidar una a una
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementOperations)){
			
			for (SettlementOperationTO objSettlementOperationTO: lstSettlementOperations){
				try {
					
					//Aqui liquidamos 1 a 1 las operaciones
					settleGrossMechanismOperationUnfulfillment(idSettlementProcess, objSettlementOperationTO, scheduleType, loggerUser, indBuyerSeller);
					
				} catch (Exception ex) {
					ex.printStackTrace();;
				}
			}
		}
	}

	
	/**
	 * Settle net mechanism operations.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param settlementDate the settlement date
	 * @param idCurrency the id currency
	 * @param idOperationPart the id operation part
	 * @param scheduleType the schedule type
	 * @param settlementSchema the settlement schema
	 * @param loggerUser the logger user
	 * @param idOperation the id operation
	 * @throws ServiceException the service exception
	 */
	public void settleNetMechanismOperations(Long idSettlementProcess, Long idMechanism, Long idModalityGroup, Date settlementDate, 
												Integer idCurrency, Integer idOperationPart,Integer scheduleType, 
												Integer settlementSchema, LoggerUser loggerUser, Long idOperation) throws ServiceException
	{
		List<Object[]> lstMechanismOperations= null;
		List<SettlementOperationTO> lstSettlementOperations = null;
		List<Long> lstCashMechanismOperation= new ArrayList<Long>();
		List<Long> lstTermMechanismOperation= new ArrayList<Long>();
		List<Long> lstCashSettlementOperation= new ArrayList<Long>();
		List<Long> lstTermSettlementOperation= new ArrayList<Long>();
		//we get the list of account operations to be settled according the settlement process
		lstMechanismOperations= settlementProcessServiceBean.getListSettlementOperationToNetSettlement(idSettlementProcess);
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstMechanismOperations)){
			lstSettlementOperations = settlementProcessServiceBean.populateSettlementOperations(lstMechanismOperations, idModalityGroup, 
														settlementSchema, idCurrency, idepositarySetup.getIndMarketFact(),
														lstCashSettlementOperation, lstTermSettlementOperation, 
														lstCashMechanismOperation, lstTermMechanismOperation);
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementOperations)){
			List<Long> lstSellerAcountOperation= new ArrayList<Long>();
			List<Long> lstBuyerAcountOperation= new ArrayList<Long>();
			int count=0;
			
			Double totalProgress = new Double(lstSettlementOperations.size());
			Double currentProgress = new Double(ComponentConstant.ZERO);
			notificationEvent.fire(getNotification(BooleanType.YES.getCode(), BooleanType.NO.getCode(), lstSettlementOperations.size(), BooleanType.NO.getCode(), null));
			
			for (SettlementOperationTO objSettlementOperationTO: lstSettlementOperations){
				++count;
				//we settle the operation by STOCKS one by one
				settleNetMechanismOperation(idSettlementProcess, objSettlementOperationTO, scheduleType, 
												lstSellerAcountOperation, lstBuyerAcountOperation, loggerUser);
				logger.info("NRO OPERACIONES: "+count 
							+ "		ID_MECHANISM_OPERATION_PK: "+ objSettlementOperationTO.getIdMechanismOperation() 
							+ "		ID_SETTLEMENT_OPERATION_PK: "+ objSettlementOperationTO.getIdSettlementOperation());								
				
				Double percentage = (++currentProgress / totalProgress) * GeneralConstants.ONE_HUNDRED;				
				notificationEvent.fire(getNotification(BooleanType.YES.getCode(), BooleanType.NO.getCode(), lstSettlementOperations.size(), BooleanType.NO.getCode(), percentage.intValue()));
				
			}						
			
			updateSettledOperationIndicators(idSettlementProcess, lstCashSettlementOperation, lstTermSettlementOperation, lstCashMechanismOperation, 
											lstTermMechanismOperation, lstSellerAcountOperation, lstBuyerAcountOperation, loggerUser);
			//we update the term price for all term operation were settled inside the settlement process
			settlementProcessServiceBean.updateTermPriceOperations(idSettlementProcess);						
			
			notificationEvent.fire(getNotification(BooleanType.NO.getCode(), BooleanType.YES.getCode(), lstSettlementOperations.size(), BooleanType.NO.getCode(), GeneralConstants.ONE_HUNDRED.intValue()));
			
		}
	}

	/**
	 * Method to settle the mechanism operation according the operation part: CASH or TERM.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param scheduleType the schedule type
	 * @param lstSellerAccountOperation the lst seller account operation
	 * @param lstBuyerAccountOperation the lst buyer account operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void settleNetMechanismOperation(Long idSettlementProcess, SettlementOperationTO objSettlementOperationTO, Integer scheduleType, 
			List<Long> lstSellerAccountOperation, List<Long> lstBuyerAccountOperation, LoggerUser loggerUser) throws ServiceException
	{
		if (NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(objSettlementOperationTO.getIdModality()) ||
			 NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(objSettlementOperationTO.getIdModality())){
			
			secondaryRepoSettlement.settleSecondaryRepoOperation(objSettlementOperationTO, objSettlementOperationTO.getOperationPart(), loggerUser);
			
		} else { //the rest of modalities			
			settlementProcessServiceBean.updateSettlementOperationSettled(objSettlementOperationTO.getIdSettlementOperation(), 
					loggerUser, objSettlementOperationTO.getStockQuantity());
			
			if(ComponentConstant.ONE.equals(objSettlementOperationTO.getIndPartial())){
				settlementProcessServiceBean.updateSettlementOperationSettled(objSettlementOperationTO.getIdMechanismOperation(), 
						objSettlementOperationTO.getOperationPart(), loggerUser, objSettlementOperationTO.getStockQuantity());
			}
			
			//To primary placement is necessary update the security and issuance balances  
			if (ComponentConstant.ONE.equals(objSettlementOperationTO.getIndPrimaryPlacement()))
			{
				primaryPlacementSettlement.settlePrimaryPlacementOperation(objSettlementOperationTO,loggerUser,idepositarySetup.getIndMarketFact());
			}
			
			if(!ComponentConstant.ONE.equals(objSettlementOperationTO.getIndDematerialization())){
				//execute the component to operation settlement
				List<SettlementHolderAccountTO> lstHolderAccountsOperation= new ArrayList<SettlementHolderAccountTO>();
				lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstBuyerHolderAccounts());
				lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstSellerHolderAccounts());
				settlementProcessServiceBean.updateBalancesAccountOperation(objSettlementOperationTO, 
						lstHolderAccountsOperation, null, null, 
						lstSellerAccountOperation, lstBuyerAccountOperation,
						loggerUser, idepositarySetup.getIndMarketFact());
			}
			
			//To Report modality is necessary settle the delivered guarantees to reporting holder account, and free the cash guarantees or margin balances from reported holder account  
			if (ComponentConstant.ONE.equals(objSettlementOperationTO.getIndTermSettlement()) && //the modality has term settlement
				ComponentConstant.ONE.equals(objSettlementOperationTO.getIndMarginGuarantee()) && //the modality manages guarantees
				//ComponentConstant.ONE.equals(objModality.getIndReportingBalance()) && //the modality manages principal guarantees
				ComponentConstant.TERM_PART.equals(objSettlementOperationTO.getOperationPart()))
			{	
				reportSettlement.settleTermReportOperation(objSettlementOperationTO);
			}
		}
	}
	
	/**
	 * Method to settle the mechanism operation according the operation part: CASH or TERM.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@LoggerAuditWeb
	public void settleGrossMechanismOperation(Long idSettlementProcess, SettlementOperationTO objSettlementOperationTO, Integer scheduleType, LoggerUser loggerUser) throws ServiceException
	{
		List<Long> lstSellerAcountOperation= new ArrayList<Long>();
		List<Long> lstBuyerAcountOperation= new ArrayList<Long>();
		
		if (NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(objSettlementOperationTO.getIdModality()) ||
			 NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(objSettlementOperationTO.getIdModality())){
			
			secondaryRepoSettlement.settleSecondaryRepoOperation(objSettlementOperationTO, objSettlementOperationTO.getOperationPart(), loggerUser);
			
		} else { //the rest of modalities
					
			boolean blockTermStock= false;
			Integer settledState = null;
 			//update the state of the mechanism operation
			if (ComponentConstant.CASH_PART.equals(objSettlementOperationTO.getOperationPart())){
				settledState = MechanismOperationStateType.CASH_SETTLED.getCode();
				if (!ComponentConstant.ONE.equals(objSettlementOperationTO.getIndTermStockBlock()) && //the modality doesn't block stock in term part
					 ComponentConstant.ONE.equals(objSettlementOperationTO.getIndTermSettlement()) ){ //the modality must have term settlement{
					blockTermStock= true; //we must set VC in term part by default for these modalities
				}
				
				List<SettlementOperation> settlementOperations = (List<SettlementOperation>) settlementProcessServiceBean.getSettlementOperation(
						objSettlementOperationTO.getIdMechanismOperation(), null);	
				
				for(SettlementOperation settlementOperation : settlementOperations){
					if(settlementOperation.getIdSettlementOperationPk().equals(objSettlementOperationTO.getIdSettlementOperation())){
						settlementProcessServiceBean.updateSettlementOperationState(settlementOperation.getIdSettlementOperationPk(), 
								settledState , AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode(), loggerUser, true);
					}
					
					if(ComponentConstant.TERM_PART.equals(settlementOperation.getOperationPart())){
						Long stockReference = null;
						if(blockTermStock){
							stockReference = AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode();
						}
						settlementProcessServiceBean.updateSettlementOperationState(settlementOperation.getIdSettlementOperationPk(), 
								settledState, stockReference, loggerUser, true);
					}
				}
				
			}else if (ComponentConstant.TERM_PART.equals(objSettlementOperationTO.getOperationPart())){
				
				settledState = MechanismOperationStateType.TERM_SETTLED.getCode();
				
				settlementProcessServiceBean.updateSettlementOperationState(objSettlementOperationTO.getIdSettlementOperation(), 
						settledState, AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode(), loggerUser, true);
				
			}
			
			settlementProcessServiceBean.updateSettlementOperationSettled(objSettlementOperationTO.getIdSettlementOperation(), 
					loggerUser, objSettlementOperationTO.getStockQuantity());
			
			if(ComponentConstant.ONE.equals(objSettlementOperationTO.getIndPartial())){
				settlementProcessServiceBean.updateSettlementOperationSettled(objSettlementOperationTO.getIdMechanismOperation(), 
						objSettlementOperationTO.getOperationPart(), loggerUser, objSettlementOperationTO.getStockQuantity());
			}else{
				settlementProcessServiceBean.updateMechanismOperationState(objSettlementOperationTO.getIdMechanismOperation(), settledState, loggerUser);
			}
			
			//To primary placement is necessary update the security and issuance balances  
			if (ComponentConstant.ONE.equals(objSettlementOperationTO.getIndPrimaryPlacement()))
			{
				primaryPlacementSettlement.settlePrimaryPlacementOperation(objSettlementOperationTO,loggerUser,idepositarySetup.getIndMarketFact());
			}
			
			if(!ComponentConstant.ONE.equals(objSettlementOperationTO.getIndDematerialization())){
				//execute the component to operation settlement
				List<SettlementHolderAccountTO> lstHolderAccountsOperation= new ArrayList<SettlementHolderAccountTO>();
				lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstBuyerHolderAccounts());
				lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstSellerHolderAccounts());
				settlementProcessServiceBean.updateBalancesAccountOperation(objSettlementOperationTO, 
						lstHolderAccountsOperation, null, null,
						lstSellerAcountOperation, lstBuyerAcountOperation,
						loggerUser, idepositarySetup.getIndMarketFact());
			}
			
			//To Report modality is necessary settle the delivered guarantees to reporting holder account, and free the cash guarantees or margin balances from reported holder account  
			if (ComponentConstant.ONE.equals(objSettlementOperationTO.getIndTermSettlement()) && //the modality has term settlement
				//ComponentConstant.ONE.equals(objSettlementOperationTO.getIndMarginGuarantee()) && //the modality manages guarantees
				ComponentConstant.ONE.equals(objSettlementOperationTO.getIndReportingBalance()) && //the modality manages principal guarantees
				ComponentConstant.TERM_PART.equals(objSettlementOperationTO.getOperationPart()))
			{	
				reportSettlement.settleTermReportOperation(objSettlementOperationTO);
			}
		}
		
		//we have to send the funds to seller participants for DVP settlement type and GROSS settlement schema
		if (SettlementType.DVP.getCode().equals(objSettlementOperationTO.getSettlementType()) &&
			 SettlementSchemaType.GROSS.getCode().equals(objSettlementOperationTO.getSettlementSchema()))
		{
			fundsComponentSingleton.sendFundsParticipantOperations(objSettlementOperationTO, loggerUser);
		}
	
		//we have to update the settlement process with the new information about the current settlement
		settlementProcessServiceBean.updateSettlementProcess(idSettlementProcess, objSettlementOperationTO.getIdSettlementOperation(), OperationSettlementSateType.SETTLED.getCode());
	}
	
	

	/**
	 * Method to settle the mechanism operation according the operation part: CASH or TERM.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@LoggerAuditWeb
	public void settleGrossMechanismOperationUnfulfillment(Long idSettlementProcess, SettlementOperationTO objSettlementOperationTO, Integer scheduleType, LoggerUser loggerUser, Integer indbuyerSeller) throws ServiceException
	{
		List<Long> lstSellerAcountOperation= new ArrayList<Long>();
		List<Long> lstBuyerAcountOperation= new ArrayList<Long>();
		
		if (NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(objSettlementOperationTO.getIdModality()) ||
			 NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(objSettlementOperationTO.getIdModality())){
			
			secondaryRepoSettlement.settleSecondaryRepoOperation(objSettlementOperationTO, objSettlementOperationTO.getOperationPart(), loggerUser);
			
		} else { //the rest of modalities
					
			boolean blockTermStock= false;
			Integer settledState = null;
 			//update the state of the mechanism operation
			if (ComponentConstant.CASH_PART.equals(objSettlementOperationTO.getOperationPart())){
				settledState = MechanismOperationStateType.CASH_SETTLED.getCode();
				if (!ComponentConstant.ONE.equals(objSettlementOperationTO.getIndTermStockBlock()) && //the modality doesn't block stock in term part
					 ComponentConstant.ONE.equals(objSettlementOperationTO.getIndTermSettlement()) ){ //the modality must have term settlement{
					blockTermStock= true; //we must set VC in term part by default for these modalities
				}
				
				List<SettlementOperation> settlementOperations = (List<SettlementOperation>) settlementProcessServiceBean.getSettlementOperation(
						objSettlementOperationTO.getIdMechanismOperation(), null);	
				
				for(SettlementOperation settlementOperation : settlementOperations){
					if(settlementOperation.getIdSettlementOperationPk().equals(objSettlementOperationTO.getIdSettlementOperation())){
						settlementProcessServiceBean.updateSettlementOperationState(settlementOperation.getIdSettlementOperationPk(), 
								settledState , AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode(), loggerUser, true);
					}
					
					if(ComponentConstant.TERM_PART.equals(settlementOperation.getOperationPart())){
						Long stockReference = null;
						if(blockTermStock){
							stockReference = AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode();
						}
						settlementProcessServiceBean.updateSettlementOperationState(settlementOperation.getIdSettlementOperationPk(), 
								settledState, stockReference, loggerUser, true);
					}
				}
				
			}else if (ComponentConstant.TERM_PART.equals(objSettlementOperationTO.getOperationPart())){
				
				settledState = MechanismOperationStateType.TERM_SETTLED.getCode();
				
				settlementProcessServiceBean.updateSettlementOperationState(objSettlementOperationTO.getIdSettlementOperation(), 
						settledState, AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode(), loggerUser, true);
				
			}
			
			settlementProcessServiceBean.updateSettlementOperationSettled(objSettlementOperationTO.getIdSettlementOperation(), 
					loggerUser, objSettlementOperationTO.getStockQuantity());
			
			if(ComponentConstant.ONE.equals(objSettlementOperationTO.getIndPartial())){
				settlementProcessServiceBean.updateSettlementOperationSettled(objSettlementOperationTO.getIdMechanismOperation(), 
						objSettlementOperationTO.getOperationPart(), loggerUser, objSettlementOperationTO.getStockQuantity());
			}else{
				settlementProcessServiceBean.updateMechanismOperationState(objSettlementOperationTO.getIdMechanismOperation(), settledState, loggerUser);
			}
			
			//To primary placement is necessary update the security and issuance balances  
			if (ComponentConstant.ONE.equals(objSettlementOperationTO.getIndPrimaryPlacement()))
			{
				primaryPlacementSettlement.settlePrimaryPlacementOperation(objSettlementOperationTO,loggerUser,idepositarySetup.getIndMarketFact());
			}
			
			if(!ComponentConstant.ONE.equals(objSettlementOperationTO.getIndDematerialization())){
				//execute the component to operation settlement
				List<SettlementHolderAccountTO> lstHolderAccountsOperation= new ArrayList<SettlementHolderAccountTO>();
				lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstBuyerHolderAccounts());
				lstHolderAccountsOperation.addAll(objSettlementOperationTO.getLstSellerHolderAccounts());
				
				settlementProcessServiceBean.updateBalancesAccountOperation(objSettlementOperationTO, 
							lstHolderAccountsOperation, null, null,
							lstSellerAcountOperation, lstBuyerAcountOperation,
							loggerUser, idepositarySetup.getIndMarketFact());
			}
			
			//To Report modality is necessary settle the delivered guarantees to reporting holder account, and free the cash guarantees or margin balances from reported holder account  
			if (ComponentConstant.ONE.equals(objSettlementOperationTO.getIndTermSettlement()) && //the modality has term settlement
				ComponentConstant.ONE.equals(objSettlementOperationTO.getIndMarginGuarantee()) && //the modality manages guarantees
				//ComponentConstant.ONE.equals(objModality.getIndReportingBalance()) && //the modality manages principal guarantees
				ComponentConstant.TERM_PART.equals(objSettlementOperationTO.getOperationPart()))
			{	
				reportSettlement.settleTermReportOperation(objSettlementOperationTO);
			}
		}
		
		//we have to send the funds to seller participants for DVP settlement type and GROSS settlement schema
		if (SettlementType.DVP.getCode().equals(objSettlementOperationTO.getSettlementType()) &&
			 SettlementSchemaType.GROSS.getCode().equals(objSettlementOperationTO.getSettlementSchema()))
		{
			fundsComponentSingleton.sendFundsParticipantOperations(objSettlementOperationTO, loggerUser);
		}
	
		//we have to update the settlement process with the new information about the current settlement
		//settlementProcessServiceBean.updateSettlementProcess(idSettlementProcess, objSettlementOperationTO.getIdSettlementOperation(), OperationSettlementSateType.SETTLED.getCode());
	}
	
	
	
	
	
	
	/**
	 * Update settled operation indicators.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param lstCashSettlementOperation the lst cash settlement operation
	 * @param lstTermSettlementOperation the lst term settlement operation
	 * @param lstCashMechanismOperation the lst cash mechanism operation
	 * @param lstTermMechanismOperation the lst term mechanism operation
	 * @param lstSellerAcountOperation the lst seller acount operation
	 * @param lstBuyerAcountOperation the lst buyer acount operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void updateSettledOperationIndicators(Long idSettlementProcess, List<Long> lstCashSettlementOperation, 
													List<Long> lstTermSettlementOperation, List<Long> lstCashMechanismOperation, List<Long> lstTermMechanismOperation, 
													List<Long> lstSellerAcountOperation, List<Long> lstBuyerAcountOperation, 
													LoggerUser loggerUser) throws ServiceException {
		Integer settledState = null;
		Long stockReference = null;
		List<Long> lstTotalSettlementOperation= new ArrayList<Long>();
		//update the state of the settlement operation
		if (Validations.validateListIsNotNullAndNotEmpty(lstCashSettlementOperation)){
			settledState = MechanismOperationStateType.CASH_SETTLED.getCode();
			stockReference = AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode();
			
			settlementProcessServiceBean.updateSettlementOperationState(lstCashSettlementOperation, settledState, stockReference, loggerUser, true);
			lstTotalSettlementOperation.addAll(lstCashSettlementOperation);
			
			//update the mechanism operation state
			if (Validations.validateListIsNotNullAndNotEmpty(lstCashMechanismOperation)) {
				settlementProcessServiceBean.updateMechanismOperationState(lstCashMechanismOperation, settledState, loggerUser);
			}
			
			List<Long> lstCurrentTermSettlementOperation = settlementProcessServiceBean.getListTermSettlementOperation(lstCashSettlementOperation);
			if (Validations.validateListIsNotNullAndNotEmpty(lstCurrentTermSettlementOperation)) {
				stockReference = AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode();
				settlementProcessServiceBean.updateSettlementOperationState(lstCurrentTermSettlementOperation, settledState, stockReference, loggerUser, false);
			}
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstTermSettlementOperation)){
			settledState = MechanismOperationStateType.TERM_SETTLED.getCode();
			stockReference = AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode();
			settlementProcessServiceBean.updateSettlementOperationState(lstTermSettlementOperation, settledState, stockReference, loggerUser, true);
			lstTotalSettlementOperation.addAll(lstTermSettlementOperation);
			
			//update the mechanism operation state
			if (Validations.validateListIsNotNullAndNotEmpty(lstTermMechanismOperation)) {
				settlementProcessServiceBean.updateMechanismOperationState(lstTermMechanismOperation, settledState, loggerUser);
			}
		}
		
		//update the operation settlement state
		if (Validations.validateListIsNotNullAndNotEmpty(lstTotalSettlementOperation)) {
			Long countOperation= new Long(lstTotalSettlementOperation.size());
			if(countOperation>=1000){
				int recordCounter = 0;
				List<Long> lstTotalSettlementOperationAux= new ArrayList<Long>();
				for(int i = 0; recordCounter<=countOperation;i++ ){
					if(lstTotalSettlementOperationAux.size()<999&&recordCounter<countOperation){
						recordCounter++;
						lstTotalSettlementOperationAux.add(new Long(lstTotalSettlementOperation.get(i)));
					}else{
						if (lstTotalSettlementOperationAux.size()==0) 
							break;
						
						settlementProcessServiceBean.updateOperationSettlement(idSettlementProcess, lstTotalSettlementOperationAux, 
								OperationSettlementSateType.SETTLED.getCode(), loggerUser);
						
						lstTotalSettlementOperationAux = new ArrayList<Long>();
						i--;
					}
				}
			}else
				settlementProcessServiceBean.updateOperationSettlement(idSettlementProcess, lstTotalSettlementOperation, 
																	OperationSettlementSateType.SETTLED.getCode(), loggerUser);
			settlementProcessServiceBean.updateSettlementSettledCount(idSettlementProcess, countOperation, loggerUser);
		}
		
		//update the settlement accounts operation indicators
		if (Validations.validateListIsNotNullAndNotEmpty(lstBuyerAcountOperation)) {
			Long countOperation= new Long(lstBuyerAcountOperation.size());
			if(countOperation>=1000){
				int recordCounter = 0;
				List<Long> lstBuyerAcountOperationAux= new ArrayList<Long>();
				for(int i = 0; recordCounter<=countOperation;i++ ){
					if(lstBuyerAcountOperationAux.size()<999&&recordCounter<countOperation){
						recordCounter++;
						lstBuyerAcountOperationAux.add(new Long(lstBuyerAcountOperation.get(i)));
					}else{
						if (lstBuyerAcountOperationAux.size()==0) 
							break;
						settlementProcessServiceBean.updateStockSettlementAccountOperation(lstBuyerAcountOperationAux, 
								AccountOperationReferenceType.AVAILABLE_SECURITIES.getCode(), loggerUser);
						
						settlementProcessServiceBean.updateStockSettlementAccountTermOperation(lstBuyerAcountOperationAux, 
								AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode(), loggerUser);
						
						lstBuyerAcountOperationAux = new ArrayList<Long>();
						i--;
					}
				}
			}else{
				settlementProcessServiceBean.updateStockSettlementAccountOperation(lstBuyerAcountOperation, 
						AccountOperationReferenceType.AVAILABLE_SECURITIES.getCode(), loggerUser);
				settlementProcessServiceBean.updateStockSettlementAccountTermOperation(lstBuyerAcountOperation, 
						AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode(), loggerUser);
			}
		}
		if (Validations.validateListIsNotNullAndNotEmpty(lstSellerAcountOperation)) {
			Long countOperation= new Long(lstSellerAcountOperation.size());
			if(countOperation>=1000){
				int recordCounter = 0;
				List<Long> lstSellerAcountOperationAux= new ArrayList<Long>();
				for(int i = 0; recordCounter<=countOperation;i++ ){
					if(lstSellerAcountOperationAux.size()<999&&recordCounter<countOperation){
						recordCounter++;
						lstSellerAcountOperationAux.add(new Long(lstSellerAcountOperation.get(i)));
					}else{
						if (lstSellerAcountOperationAux.size()==0) 
							break;
						
						settlementProcessServiceBean.updateStockSettlementAccountOperation(lstSellerAcountOperationAux, 
								AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode(), loggerUser);
						settlementProcessServiceBean.updateStockSettlementAccountTermOperation(lstSellerAcountOperationAux, 
								AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode(), loggerUser);
						
						lstSellerAcountOperationAux = new ArrayList<Long>();
						i--;
					}
				}
			}else{
				settlementProcessServiceBean.updateStockSettlementAccountOperation(lstSellerAcountOperation, 
						AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode(), loggerUser);
				settlementProcessServiceBean.updateStockSettlementAccountTermOperation(lstSellerAcountOperation, 
						AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode(), loggerUser);
			}
		}
	}
	
	
	/**
	 
	 * Metodo para registrar la SEDIR
	 * @param settlementReport
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	public SettlementReport saveSettlementReport(SettlementReport settlementReport, LoggerUser loggerUser) throws ServiceException {
		
		create(settlementReport);
		
		return settlementReport;
	}
	
	/**
	 * Metodo para registrar la SOCI
	 * @param settlementReport
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	public SettlementUnfulfillment saveSettlementUnfulfillment(SettlementUnfulfillment settlementUnfulfillment, LoggerUser loggerUser) throws ServiceException {
		
		create(settlementUnfulfillment);
		
		return settlementUnfulfillment;
	}
	
	/**
	 * Register MCN Manually .
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param loggerUser the logger user
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public synchronized MechanismOperation saveMechanismOperation(MechanismOperation mechanismOperation, LoggerUser loggerUser) throws ServiceException {
		
		negotiationOperationServiceBean.validateMechanismOperation(mechanismOperation);
		
		Long mechanismId = mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		Long modalityId = mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk();
		
		// validamos el numero de operacion en mecanismos 
		/*
		if(mechanismOperation.getReferenceDate()!=null){
			Date refDate = CommonsUtilities.truncateDateTime(mechanismOperation.getReferenceDate());
			boolean exists = mcnOperationServiceBean.chkOperationNumberMechanism(mechanismId,mechanismOperation.getReferenceNumber(), refDate);
			if(exists){
				throw new ServiceException(ErrorServiceType.MECHANISM_REFERENCE_NUMBER_EXISTS);
			}
		}*/
		
		// Validate unique ballot/sequential
		boolean existsSq = mcnOperationServiceBean.chkOperationBallotSequential(mechanismOperation.getBallotNumber(),mechanismOperation.getSequential());
		if(existsSq){
			throw new ServiceException(ErrorServiceType.MECHANISM_BALLOT_SEQUENTIAL_EXISTS);
		}
		
		//check for data in ASSIGNMENT_PROCESS then save data
		
		ParticipantAssignment objSellerParticipantAssignment= null;
		ParticipantAssignment objBuyerParticipantAssignment= null;
		AssignmentProcess assignmentProcess = negotiationOperationServiceBean.findAssignmentProcess(mechanismId,modalityId,mechanismOperation.getCashSettlementDate());
		
		if(Validations.validateIsNull(assignmentProcess)){
			//we create the assignment process
			assignmentProcess = new AssignmentProcess();
			assignmentProcess.setMechanisnModality(mechanismOperation.getMechanisnModality());
			assignmentProcess.setRegisterDate(CommonsUtilities.currentDateTime());
			assignmentProcess.setSettlementDate(mechanismOperation.getCashSettlementDate());
			assignmentProcess.setAssignmentState(AssignmentProcessStateType.OPENED.getCode());
			create(assignmentProcess);
			//we create the participant assignment process to seller participant
			objSellerParticipantAssignment= createParticipantAssigment(assignmentProcess, mechanismOperation.getSellerParticipant());

			//if the participant are the same then we must create only one participant assignment
			if (!mechanismOperation.getSellerParticipant().getIdParticipantPk().equals(
					mechanismOperation.getBuyerParticipant().getIdParticipantPk())) {
				//we create the participant assignment process to buyer participant
				objBuyerParticipantAssignment= createParticipantAssigment(assignmentProcess, mechanismOperation.getBuyerParticipant());
			}else{
				objBuyerParticipantAssignment = objSellerParticipantAssignment;
			}
			
		} else {
			
			if (AssignmentProcessStateType.CLOSED.getCode().equals(assignmentProcess.getAssignmentState())) {
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_PROCESS_CLOSED,
						new Object[]{mechanismOperation.getMechanisnModality().getNegotiationMechanism().getMechanismName(),
									 mechanismOperation.getMechanisnModality().getNegotiationModality().getModalityName(),
									 mechanismOperation.getCashSettlementDate()});
			}
			
			//we verify if the buyer participant has assignment process
			objBuyerParticipantAssignment= negotiationOperationServiceBean.getParticipantAssignment(assignmentProcess.getIdAssignmentProcessPk(), 
					mechanismOperation.getBuyerParticipant().getIdParticipantPk());
			if (Validations.validateIsNull(objBuyerParticipantAssignment)) {
				objBuyerParticipantAssignment= createParticipantAssigment(assignmentProcess, mechanismOperation.getBuyerParticipant());
			}else{
				if(!ParticipantAssignmentStateType.OPENED.getCode().equals(objBuyerParticipantAssignment.getAssignmentState())){
					objBuyerParticipantAssignment.setAssignmentState(ParticipantAssignmentStateType.OPENED.getCode());
					update(objBuyerParticipantAssignment);
					
					Long participantId = mechanismOperation.getBuyerParticipant().getIdParticipantPk();
					Long businessProcessId = BusinessProcessType.PARTICIPANT_ASSIGNMENT_RESTART.getCode();
					mcnOperationServiceBean.sendOperationsNotification(participantId,businessProcessId,mechanismOperation,loggerUser);
				}
			}
			//we verify if the seller participant has assignment process
			objSellerParticipantAssignment= negotiationOperationServiceBean.getParticipantAssignment(assignmentProcess.getIdAssignmentProcessPk(), 
					mechanismOperation.getSellerParticipant().getIdParticipantPk());
			if (Validations.validateIsNull(objSellerParticipantAssignment)) {
				objSellerParticipantAssignment= createParticipantAssigment(assignmentProcess, mechanismOperation.getSellerParticipant());
			}else{
				if(!ParticipantAssignmentStateType.OPENED.getCode().equals(objSellerParticipantAssignment.getAssignmentState())){
					objSellerParticipantAssignment.setAssignmentState(ParticipantAssignmentStateType.OPENED.getCode());
					update(objSellerParticipantAssignment);
					
					//send reopen notification
					Long participantId = mechanismOperation.getSellerParticipant().getIdParticipantPk();
					Long businessProcessId = BusinessProcessType.PARTICIPANT_ASSIGNMENT_RESTART.getCode();
					mcnOperationServiceBean.sendOperationsNotification(participantId ,businessProcessId ,mechanismOperation,loggerUser);
				}
			}
		}
		
		/* Saving data in TRADE_OPERATION  */
		TradeOperation tradeOperation = new TradeOperation();
		tradeOperation.setRegisterUser(mechanismOperation.getRegisterUser());
		tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		tradeOperation.setOperationState(MechanismOperationStateType.REGISTERED_STATE.getCode());
		NegotiationModalityType modalityType = NegotiationModalityType.get(modalityId);
		tradeOperation.setOperationType(modalityType.getParameterOperationType());
		
		/* Saving data for Mechanism Operation */
		mechanismOperation.setAssignmentProcess(assignmentProcess);
		mechanismOperation.setTradeOperation(tradeOperation);
		mechanismOperation.setIdMechanismOperationPk(tradeOperation.getIdTradeOperationPk());
		mechanismOperation.setOperationState(MechanismOperationStateType.REGISTERED_STATE.getCode());
		mechanismOperation.setNominalValue(mechanismOperation.getSecurities().getCurrentNominalValue());
		mechanismOperation.setOperationNumber(mcnOperationServiceBean.getMcnUniqueNumber(mechanismOperation.getOperationDate(), mechanismId, modalityId));
		
		NegotiationModality modality = mechanismOperation.getMechanisnModality().getNegotiationModality();
		mechanismOperation.setIndReportingBalance(modality.getIndReportingBalance());
		mechanismOperation.setIndPrincipalGuarantee(modality.getIndPrincipalGuarantee());
		mechanismOperation.setIndMarginGuarantee(modality.getIndMarginGuarantee());
		mechanismOperation.setIndTermSettlement(modality.getIndTermSettlement());
		mechanismOperation.setIndCashStockBlock(modality.getIndCashStockBlock());
		mechanismOperation.setIndTermStockBlock(modality.getIndTermStockBlock());
		mechanismOperation.setIndPrimaryPlacement(modality.getIndPrimaryPlacement());
		
		//set cash settlements data 
		mechanismOperation.setIndCashSettlementExchange(ComponentConstant.ZERO);
		mechanismOperation.setCashSettlementAmount(mechanismOperation.getRealCashAmount());
		mechanismOperation.setCashSettlementPrice(mechanismOperation.getRealCashPrice());
		
		// change currency
		if(!mechanismOperation.getCurrency().equals(mechanismOperation.getCashSettlementCurrency())){
			mechanismOperation.setCashSettlementExChangeRate(mechanismOperation.getExchangeRate());	
			mechanismOperation.setCashSettlementAmount(NegotiationUtils.getSettlementAmount(mechanismOperation.getCashSettlementPrice(),mechanismOperation.getStockQuantity()).multiply(mechanismOperation.getExchangeRate()).setScale(2, RoundingMode.HALF_UP));
			mechanismOperation.setCashSettlementPrice(mechanismOperation.getCashSettlementAmount().divide(mechanismOperation.getStockQuantity(), 2, RoundingMode.HALF_UP));
		}
		
		SettlementOperation cashSettlementOperation = settlementProcessServiceBean.populateSettlementOperation(mechanismOperation,ComponentConstant.CASH_PART);
		SettlementOperation termSettlementOperation = null;
		
		//set term settlements data 
		mechanismOperation.setIndTermSettlementExchange(ComponentConstant.ZERO);
		if(ComponentConstant.ONE.equals(mechanismOperation.getIndTermSettlement())){
			
			mechanismOperation.setTermSettlementAmount(mechanismOperation.getTermAmount());
			mechanismOperation.setTermSettlementPrice(mechanismOperation.getTermPrice());
			
			// change currency
			if(!mechanismOperation.getCurrency().equals(mechanismOperation.getTermSettlementCurrency())){
				mechanismOperation.setTermSettlementExChangeRate(mechanismOperation.getExchangeRate());
				mechanismOperation.setTermSettlementAmount(NegotiationUtils.getSettlementAmount(mechanismOperation.getTermSettlementPrice(),mechanismOperation.getStockQuantity()).multiply(mechanismOperation.getExchangeRate()).setScale(2, RoundingMode.HALF_UP));
				mechanismOperation.setTermSettlementPrice(mechanismOperation.getTermSettlementAmount().divide(mechanismOperation.getStockQuantity(), 2, RoundingMode.HALF_UP));
			}
			
			termSettlementOperation = settlementProcessServiceBean.populateSettlementOperation(mechanismOperation,ComponentConstant.TERM_PART);
		}
		
		if (ComponentConstant.ONE.equals(mechanismOperation.getIndPrimaryPlacement())){
			
			//only for PRIMARY PLACEMENTS modalities with set the stock reference by default
			cashSettlementOperation.setStockReference(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
			
			// validate if bank shares are in primary placement
			Object[] issuerDetail= settlementProcessServiceBean.getIssuerDetails(mechanismOperation.getSecurities().getIdSecurityCodePk());
			Integer objEconomicActivity = (Integer)(issuerDetail[3] == null ? ComponentConstant.ZERO : issuerDetail[3]);
			Integer objSecurityClass = (Integer)(issuerDetail[4] == null ? ComponentConstant.ZERO : issuerDetail[4]);
			
			if(ComponentConstant.ONE.equals(objEconomicActivity) && ComponentConstant.ONE.equals(objSecurityClass)){
				cashSettlementOperation.setIndDematerialization(ComponentConstant.ONE);
			}
		}
		
		create(mechanismOperation);
		create(cashSettlementOperation);
		saveAll(cashSettlementOperation.getParticipantSettlements());
		if(termSettlementOperation!=null){
			create(termSettlementOperation);
			saveAll(termSettlementOperation.getParticipantSettlements());
		}
		
		if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
			for(MechanismOperationMarketFact mechaOpeMarketFact:mechanismOperation.getMechanismOperationMarketFacts()){
				mechaOpeMarketFact.setMechanismOperation(mechanismOperation);
				create(mechaOpeMarketFact);
			}
		}
		
		AssignmentRequest assignmentRequestSeller = createAssignmentRequest(objSellerParticipantAssignment, mechanismOperation, ComponentConstant.SALE_ROLE);
		
		AssignmentRequest assignmentRequestBuyer = createAssignmentRequest(objBuyerParticipantAssignment, mechanismOperation, ComponentConstant.PURCHARSE_ROLE);
		
		if(ComponentConstant.ONE.equals(mechanismOperation.getIndPrimaryPlacement())){//Si es colocacion primaria
			
			//validate if primary placement account exists
			HolderAccount sellerAccount = negotiationOperationServiceBean.getIssuerHolderAccount(
					mechanismOperation.getSecurities().getIdSecurityCodePk(), mechanismOperation.getSellerParticipant().getIdParticipantPk());
			if(sellerAccount == null){
				throw new ServiceException(ErrorServiceType.PRIMARY_PLACEMENT_ACCOUNT_NOT_EXIST);
			}else{
		    	
		    	saveHolderAccountAssignment(objSellerParticipantAssignment, sellerAccount, mechanismOperation,
		    			ComponentConstant.SALE_ROLE,assignmentRequestSeller,true,true);
			}
			
	    	PrimaryPlacementComponentTO primaryPlacementComponentTO = new PrimaryPlacementComponentTO();
	    	primaryPlacementComponentTO.setIdSecurityCode(mechanismOperation.getSecurities().getIdSecurityCodePk());
	    	primaryPlacementComponentTO.setIdPlacementParticipant(mechanismOperation.getSellerParticipant().getIdParticipantPk());
	    	primaryPlacementComponentTO.setMode(ComponentConstant.SUM);
	    	primaryPlacementComponentTO.setStockQuantity(mechanismOperation.getStockQuantity());
	    	primaryPlacementComponentTO.setValidateOpenPlacement(true);
	    	primaryPlacementComponentTO.setUpdateDate(CommonsUtilities.currentDate());
			securitiesComponentService.get().updatePrimaryPlacement(primaryPlacementComponentTO);
			
			/**If operation is primary placement*/
			Long participantCode = mechanismOperation.getSellerParticipant().getIdParticipantPk();
			String securityCode = mechanismOperation.getSecurities().getIdSecurityCodePk();
			Long operationCode= mechanismOperation.getIdMechanismOperationPk();
			BigDecimal quantit= mechanismOperation.getStockQuantity();
			securitiesRemoteService.get().addOperationPlacement(participantCode, securityCode, operationCode, quantit);
			
	    }
		
		return mechanismOperation;
	}

	/**
	 * Cancel mcn operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param indCancelMotive the ind cancel motive
	 * @param cancelMotiveOther the cancel motive other
	 * @param indPrimaryPlacement the ind primary placement
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void cancelMcnOperation(Long idMechanismOperation, Integer indCancelMotive, String cancelMotiveOther,  Integer indPrimaryPlacement, LoggerUser loggerUser) throws ServiceException {
		
		MechanismOperation objMechanismOperation = find(MechanismOperation.class,idMechanismOperation);
		
		if(!MechanismOperationStateType.REGISTERED_STATE.getCode().equals(objMechanismOperation.getOperationState())
				&& !MechanismOperationStateType.PENDING_CANCEL_STATE.getCode().equals(objMechanismOperation.getOperationState())){
			throw new ServiceException(ErrorServiceType.MECHANISM_OPERATION_STATE);
		}
		
		@SuppressWarnings("unchecked")
		List<SettlementOperation> settlementOperations = (List<SettlementOperation>) settlementProcessServiceBean.getSettlementOperation(idMechanismOperation, null);
		
		for (SettlementOperation settlementOperation : settlementOperations) {
			settlementProcessServiceBean.updateSettlementOperationState(settlementOperation.getIdSettlementOperationPk(), 
					MechanismOperationStateType.CANCELED_STATE.getCode(), null, loggerUser, false);
		}
		
		objMechanismOperation.getTradeOperation().setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
		objMechanismOperation.setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
		objMechanismOperation.setCancelMotive(indCancelMotive);
		objMechanismOperation.setCancelMotiveOther(cancelMotiveOther);
		update(objMechanismOperation);
		
		settlementProcessServiceBean.cancelPendingAssignmentRequests(idMechanismOperation, loggerUser);	
		
		//for primary placement modalities we have to subtract the request amount in the placement segment
		
	    if (ComponentConstant.ONE.equals(indPrimaryPlacement)) {
	    	PrimaryPlacementComponentTO primaryPlacementComponentTO = new PrimaryPlacementComponentTO();
	    	primaryPlacementComponentTO.setIdSecurityCode(objMechanismOperation.getSecurities().getIdSecurityCodePk());
	    	primaryPlacementComponentTO.setIdPlacementParticipant(objMechanismOperation.getSellerParticipant().getIdParticipantPk());
	    	primaryPlacementComponentTO.setMode(ComponentConstant.SUBTRACTION);
	    	primaryPlacementComponentTO.setStockQuantity(objMechanismOperation.getStockQuantity());
	    	primaryPlacementComponentTO.setValidateOpenPlacement(true);
	    	primaryPlacementComponentTO.setUpdateDate(CommonsUtilities.currentDate());
			securitiesComponentService.get().updatePrimaryPlacement(primaryPlacementComponentTO);
	    }
	}
	
	/**
	 * Method to cancel the mechanism operation. It is required reverse the blocked balances by sale position or purchase position 
	 *
	 * @param settlementUnfulfillmentTO the settlement unfulfillment to
	 * @param operationPart the operation part
	 * @param idCancellationReason the id cancellation reason
	 * @param otherReasons the other reasons
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void cancelMechanismOperation(SettlementUnfulfillmentTO settlementUnfulfillmentTO, Integer operationPart, Integer idCancellationReason, String otherReasons,LoggerUser loggerUser) throws ServiceException
	{
		if(!ComponentConstant.ONE.equals(settlementUnfulfillmentTO.getIndPartial())){
			//we have to unblock the balances from holder accounts operation
			if (ComponentConstant.CASH_PART.equals(operationPart)) {
				cancellationOperationService.unblockStockOperation(settlementUnfulfillmentTO, operationPart, loggerUser);
			}
		}else{
			repopulateSettlementOperation(settlementUnfulfillmentTO, operationPart, loggerUser);
		}
		
		//we cancel the operation
        if(ComponentConstant.CASH_PART.equals(operationPart)){
        	
        	settlementProcessServiceBean.updateSettlementOperationState(settlementUnfulfillmentTO.getIdSettlementOperation(), 
            		MechanismOperationStateType.CANCELED_STATE.getCode(), null, loggerUser, false);
        	
        	if(!ComponentConstant.ONE.equals(settlementUnfulfillmentTO.getIndPartial())){
	        	cancellationOperationService.updateCancellationOperation(settlementUnfulfillmentTO.getIdMechanismOperation(), 
	        			MechanismOperationStateType.CANCELED_STATE.getCode(), idCancellationReason, otherReasons, loggerUser);
        	}
        	
        	//check if there are pending assignment requests
        	settlementProcessServiceBean.cancelPendingAssignmentRequests(settlementUnfulfillmentTO.getIdMechanismOperation(), loggerUser);
        	
        }else if(ComponentConstant.TERM_PART.equals(operationPart)){
        	
        	settlementProcessServiceBean.updateSettlementOperationState(settlementUnfulfillmentTO.getIdSettlementOperation(), 
            		MechanismOperationStateType.CANCELED_TERM_STATE.getCode(), null, loggerUser , false);
        	
        	if(!ComponentConstant.ONE.equals(settlementUnfulfillmentTO.getIndPartial())){
        		cancellationOperationService.updateCancellationOperation(settlementUnfulfillmentTO.getIdMechanismOperation(), 
        				MechanismOperationStateType.CANCELED_TERM_STATE.getCode(), idCancellationReason, otherReasons, loggerUser);
        	}
        }
        
        
		//only for primary placement we have to restore the amount request from segment placement
		if (ComponentConstant.ONE.equals(settlementUnfulfillmentTO.getIndPrimaryPlacement())) 
		{
	    	PrimaryPlacementComponentTO primaryPlacementComponentTO = new PrimaryPlacementComponentTO();
	    	primaryPlacementComponentTO.setIdSecurityCode(settlementUnfulfillmentTO.getIdSecurityCode());
	    	primaryPlacementComponentTO.setIdPlacementParticipant(settlementUnfulfillmentTO.getIdSaleParticipant());
	    	primaryPlacementComponentTO.setMode(ComponentConstant.SUBTRACTION);
	    	primaryPlacementComponentTO.setStockQuantity(settlementUnfulfillmentTO.getStockQuantity());
	    	primaryPlacementComponentTO.setValidateOpenPlacement(true);
	    	primaryPlacementComponentTO.setUpdateDate(CommonsUtilities.currentDate());
			securitiesComponentService.get().updatePrimaryPlacement(primaryPlacementComponentTO);
		}
	}
	
	/**
	 * Identify settlement unfulfillment.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param loggerUser the logger user
	 */
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	public int identifySettlementUnfulfillment(Long idAssignmentProcess, Long idMechanism, Long idModality, LoggerUser loggerUser, Integer currency) 
	{
		logger.info("identifySettlementUnfulfillment(idAssignmentProcess: "+idAssignmentProcess+", idMechanism: "+idMechanism+", idModality: "+idModality);
		
		int quantity = 0;
		
		List<Object[]> lstUnfulfilledOperations= new ArrayList<Object[]>();
		if (idAssignmentProcess != null) {
			//we get the list of unfulfillment about assignment process
			lstUnfulfilledOperations = unfulfillmentService.getListAssigmentUnfulfilledOperations(idAssignmentProcess, currency);
			if (Validations.validateListIsNotNullAndNotEmpty(lstUnfulfilledOperations)) 
			{
				//the assignment unfulfillment is always in cash part 
				generateUnfulfillmentMechanismOperation(lstUnfulfilledOperations, ComponentConstant.CASH_PART, 
																				UnfulfillmentParameterType.PURCHASE_UNFULFILLMENT_TYPE.getCode(), 
																				UnfulfillmentParameterType.ASSIGNMENT_UNFULFILLMENT_MOTIVE.getCode(),loggerUser);
			}
		} else {
			Date unfulfillmentDate= CommonsUtilities.currentDate();
			//we get the list of unfulfillment for STOCKS in CASH part, role default
			lstUnfulfilledOperations = unfulfillmentService.getListStocksFundsUnfufilledOperations(idMechanism, idModality, ComponentConstant.CASH_PART, 
																				UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode(), unfulfillmentDate,null, currency);
			if (Validations.validateListIsNotNullAndNotEmpty(lstUnfulfilledOperations)) 
			{
				quantity = quantity + lstUnfulfilledOperations.size();
				generateUnfulfillmentMechanismOperation(lstUnfulfilledOperations, ComponentConstant.CASH_PART, 
																				UnfulfillmentParameterType.SALE_UNFULFILLMENT_TYPE.getCode(), 
																				UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode(),loggerUser);
			}
			
			//we get the list of unfulfillment for FUNDS in CASH part, role default
			lstUnfulfilledOperations = unfulfillmentService.getListStocksFundsUnfufilledOperations(idMechanism, idModality, ComponentConstant.CASH_PART, 
																				UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode(), unfulfillmentDate, null, currency);
			if (Validations.validateListIsNotNullAndNotEmpty(lstUnfulfilledOperations)) 
			{
				quantity = quantity + lstUnfulfilledOperations.size();
				generateUnfulfillmentMechanismOperation(lstUnfulfilledOperations, ComponentConstant.CASH_PART,
																				UnfulfillmentParameterType.PURCHASE_UNFULFILLMENT_TYPE.getCode(), 
																				UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode(),loggerUser);
			}
			
			//we get the list of unfulfillment for STOCKS in TERM part, role default
			lstUnfulfilledOperations = unfulfillmentService.getListStocksFundsUnfufilledOperations(idMechanism, idModality, ComponentConstant.TERM_PART, 
																				UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode(), unfulfillmentDate, null, currency);
			if (Validations.validateListIsNotNullAndNotEmpty(lstUnfulfilledOperations)) 
			{
				quantity = quantity + lstUnfulfilledOperations.size();
				generateUnfulfillmentMechanismOperation(lstUnfulfilledOperations, ComponentConstant.TERM_PART,
																				UnfulfillmentParameterType.SALE_UNFULFILLMENT_TYPE.getCode(), 
																				UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode(),loggerUser);
			}
			
			//we get the list of unfulfillment for FUNDS in TERM part, role default
			lstUnfulfilledOperations = unfulfillmentService.getListStocksFundsUnfufilledOperations(idMechanism, idModality, ComponentConstant.TERM_PART, 
																				UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode(), unfulfillmentDate, null, currency);
			if (Validations.validateListIsNotNullAndNotEmpty(lstUnfulfilledOperations)) 
			{
				quantity = quantity + lstUnfulfilledOperations.size();
				generateUnfulfillmentMechanismOperation(lstUnfulfilledOperations, ComponentConstant.TERM_PART,
																				UnfulfillmentParameterType.PURCHASE_UNFULFILLMENT_TYPE.getCode(), 
																				UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode(),loggerUser);
			}
			
			/******** we get the list of unfulfillment for GUARANTES in CASH part******/
			/** ROLE: SELLER, IS FOR REPORTO */
			lstUnfulfilledOperations = unfulfillmentService.getListStocksFundsUnfufilledOperations(idMechanism, idModality, ComponentConstant.CASH_PART, 
																				UnfulfillmentParameterType.GUARANTEES_UNFULFILLMENT_MOTIVE.getCode(), unfulfillmentDate, 
																				ComponentConstant.SALE_ROLE, currency);
			if (Validations.validateListIsNotNullAndNotEmpty(lstUnfulfilledOperations)) 
			{
				quantity = quantity + lstUnfulfilledOperations.size();
				generateUnfulfillmentMechanismOperation(lstUnfulfilledOperations, ComponentConstant.CASH_PART,
																				UnfulfillmentParameterType.SALE_UNFULFILLMENT_TYPE.getCode(), 
																				UnfulfillmentParameterType.GUARANTEES_UNFULFILLMENT_MOTIVE.getCode(),loggerUser);
			}

			/** ROLE: BUYER, IS FOR LOAN SECURITY */
			lstUnfulfilledOperations = unfulfillmentService.getListStocksFundsUnfufilledOperations(idMechanism, idModality, ComponentConstant.CASH_PART, 
																				UnfulfillmentParameterType.GUARANTEES_UNFULFILLMENT_MOTIVE.getCode(), unfulfillmentDate, 
																				ComponentConstant.PURCHARSE_ROLE, currency);
			if (Validations.validateListIsNotNullAndNotEmpty(lstUnfulfilledOperations)) 
			{
				quantity = quantity + lstUnfulfilledOperations.size();
				generateUnfulfillmentMechanismOperation(lstUnfulfilledOperations, ComponentConstant.CASH_PART,
																				UnfulfillmentParameterType.PURCHASE_UNFULFILLMENT_TYPE.getCode(), 
																				UnfulfillmentParameterType.GUARANTEES_UNFULFILLMENT_MOTIVE.getCode(),loggerUser);
			}						
		}
		return quantity;
	}

	/**
	 * Generate unfulfillment mechanism operation.
	 *
	 * @param lstOperations the lst operations
	 * @param idOperationPart the id operation part
	 * @param idUnfulfillmentType the id unfulfillment type
	 * @param idUnfulfillmentMotive the id unfulfillment motive
	 * @param loggerUser the logger user
	 */
	private void generateUnfulfillmentMechanismOperation(List<Object[]> lstOperations, Integer idOperationPart, Long idUnfulfillmentType, Long idUnfulfillmentMotive,LoggerUser loggerUser)
	{
		for (Object[] objOperation: lstOperations)
		{
			try {
				SettlementUnfulfillmentTO settlementUnfulfillment = populateSettlementUnfulfillment(objOperation);
				generateUnfulfillmentMechanismOperation(settlementUnfulfillment, idOperationPart, idUnfulfillmentType, idUnfulfillmentMotive,loggerUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Populate settlement unfulfillment.
	 *
	 * @param objOperation the obj operation
	 * @return the settlement unfulfillment to
	 */
	private SettlementUnfulfillmentTO populateSettlementUnfulfillment(Object[] objOperation) {
		SettlementUnfulfillmentTO settlementUnfulfillmentTO = new SettlementUnfulfillmentTO();
		settlementUnfulfillmentTO.setIdMechanismOperation((Long)objOperation[0]);
		settlementUnfulfillmentTO.setIdPurchaseParticipant((Long)objOperation[1]);
		settlementUnfulfillmentTO.setIdSaleParticipant((Long)objOperation[2]);
		settlementUnfulfillmentTO.setIdModality((Long)objOperation[3]);
		settlementUnfulfillmentTO.setIdSettlementOperation((Long)objOperation[4]);
		settlementUnfulfillmentTO.setSettlementDate((Date)objOperation[5]);
		settlementUnfulfillmentTO.setStockQuantity((BigDecimal)objOperation[6]);
		settlementUnfulfillmentTO.setSettlementAmount((BigDecimal)objOperation[7]);
		settlementUnfulfillmentTO.setIndPartial((Integer)objOperation[8]);
		settlementUnfulfillmentTO.setIndTermSettlement((Integer)objOperation[9]);
		settlementUnfulfillmentTO.setIndCashStockBlock((Integer)objOperation[10]);
		settlementUnfulfillmentTO.setIndTermStockBlock((Integer)objOperation[11]);
		settlementUnfulfillmentTO.setIndPrimaryPlacement((Integer)objOperation[12]);
		settlementUnfulfillmentTO.setIndReportingBalance((Integer)objOperation[13]);
		settlementUnfulfillmentTO.setIdSecurityCode((String)objOperation[14]);
		return settlementUnfulfillmentTO;
	}


	/**
	 * Method to check as unfulfilled the mechanism operation and holder accounts inside the operations. In addition, to cancel the mechanism operations   
	 *
	 * @param settlementUnfulfillment the settlement unfulfillment
	 * @param idOperationPart the id operation part
	 * @param idUnfulfillmentType the id unfulfillment type
	 * @param idUnfulfillmentMotive the id unfulfillment motive
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void generateUnfulfillmentMechanismOperation(SettlementUnfulfillmentTO settlementUnfulfillment, 
			Integer idOperationPart, Long idUnfulfillmentType, Long idUnfulfillmentMotive, LoggerUser loggerUser) throws ServiceException
	{
        
        Integer idCancellationReason= null;
		String otherReasons= null;
		
		if (UnfulfillmentParameterType.ASSIGNMENT_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			idCancellationReason= McnCancelReasonType.UNFULFILLMENT_ASSIGNMENT_ACCOUNTS.getCode();
		} else if (UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			idCancellationReason= McnCancelReasonType.UNFULFILLMENT_FUNDS.getCode();
		} else if (UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			idCancellationReason= McnCancelReasonType.UNFULFILLMENT_STOCKS.getCode();
		} else if (UnfulfillmentParameterType.GUARANTEES_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			idCancellationReason= McnCancelReasonType.UNFULFILLMENT_GUARANTEES.getCode();
		} 
		
		//we update the operation to unfulfillment
		unfulfillmentService.updateUnfulfillmentSettlementOperation(settlementUnfulfillment.getIdSettlementOperation(), loggerUser);
		
		//now we have to save the operation unfulfillment
		OperationUnfulfillment objOperationUnfulfillment= populateOperationUnfulfillment(settlementUnfulfillment, idUnfulfillmentType, idUnfulfillmentMotive, idOperationPart);
		create(objOperationUnfulfillment);
		
		//the assignment unfulfillment reason does not require to check in the holder account operation 
		if (!UnfulfillmentParameterType.ASSIGNMENT_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive))
		{
			//we have to update the account operation to unfulfillment from settlement operation
			List<Object[]> lstSettlementAccountOperations = unfulfillmentService.getListUnfulfilledAccountSettlementOperations(
					settlementUnfulfillment.getIdSettlementOperation(), idUnfulfillmentMotive);
			
			if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountOperations)) {
				unfulfillmentService.saveHolderAccountUnfulfillment(lstSettlementAccountOperations, objOperationUnfulfillment, loggerUser);
			}
		}
		
		//we cannot cancel the operations manage reporting and reported balances in term part
		cancelMechanismOperation(settlementUnfulfillment, idOperationPart, idCancellationReason, otherReasons, loggerUser);
	}
	
	/**
	 * Repopulate settlement operation.
	 *
	 * @param settlementUnfulfillment the settlement unfulfillment
	 * @param idOperationPart the id operation part
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void repopulateSettlementOperation(SettlementUnfulfillmentTO settlementUnfulfillment, Integer idOperationPart, LoggerUser loggerUser) throws ServiceException {
		
		//return stock to original term operation
		SettlementOperation originalSettOperation = (SettlementOperation) settlementProcessServiceBean.getSettlementOperation(settlementUnfulfillment.getIdMechanismOperation(),idOperationPart);
		originalSettOperation.setStockQuantity(originalSettOperation.getStockQuantity().add(settlementUnfulfillment.getStockQuantity()));
		originalSettOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(originalSettOperation.getSettlementPrice(), originalSettOperation.getStockQuantity()));
		settlementProcessServiceBean.update(originalSettOperation);
		
		//return stock to original sett accounts
		List<Object[]> arrAccountOperations = unfulfillmentService.getSettlementAccountsToRevert(
				settlementUnfulfillment.getIdSettlementOperation(),originalSettOperation.getIdSettlementOperationPk());
		
		for (Object[] objAccountOperation : arrAccountOperations) {
			SettlementAccountOperation originalSettAccountOperation = (SettlementAccountOperation)objAccountOperation[0];
			Long idSettlementAccountPk = (Long)objAccountOperation[1];
			BigDecimal stockQuantity= (BigDecimal)objAccountOperation[2];
			originalSettAccountOperation.setStockQuantity(originalSettAccountOperation.getStockQuantity().add(stockQuantity));
			originalSettAccountOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(originalSettOperation.getSettlementPrice(), originalSettAccountOperation.getStockQuantity()));
			if(originalSettAccountOperation.getOperationState().equals(HolderAccountOperationStateType.PREPAID.getCode())){
				originalSettAccountOperation.setOperationState(HolderAccountOperationStateType.CONFIRMED.getCode());
			}
			update(originalSettAccountOperation);
			
			List<SettlementAccountMarketfact> settlementAccMarketfacts = settlementProcessServiceBean.getSettlementAccountMarketfacts(idSettlementAccountPk);
			for (SettlementAccountMarketfact settlementAccountMarketfact : settlementAccMarketfacts) {
				Date marketDate = settlementAccountMarketfact.getMarketDate();
				BigDecimal marketRate = settlementAccountMarketfact.getMarketRate();
				BigDecimal marketPrice = settlementAccountMarketfact.getMarketPrice();
				SettlementAccountMarketfact originalSettMktFact = settlementProcessServiceBean.getSettlementAccountMarketfacts(
						originalSettAccountOperation.getIdSettlementAccountPk(),marketDate,marketRate,marketPrice);
				if(originalSettMktFact == null){
					detach(settlementAccountMarketfact);
					settlementAccountMarketfact.setSettlementAccountOperation(originalSettAccountOperation);
					create(settlementAccountMarketfact);
				}else{
					originalSettMktFact.setMarketQuantity(settlementAccountMarketfact.getMarketQuantity().add(originalSettMktFact.getMarketQuantity()));
					originalSettMktFact.setIndActive(ComponentConstant.ONE);
					update(originalSettMktFact);
				}
			}
			
			settlementProcessServiceBean.updateStockSettlementAccountOperation(idSettlementAccountPk, null, loggerUser);
			
		}
		
		//restore stock to original participantSettlement
		List<Object[]> arrPartSettlements = unfulfillmentService.getParticipantSettlementsToRevert(
				settlementUnfulfillment.getIdSettlementOperation(),originalSettOperation.getIdSettlementOperationPk());
		for (Object[] objParticipantSettlement : arrPartSettlements) {
			ParticipantSettlement originalPartSettlement = (ParticipantSettlement)objParticipantSettlement[0];
			BigDecimal stockQuantity = (BigDecimal)objParticipantSettlement[1];
			originalPartSettlement.setStockQuantity(originalPartSettlement.getStockQuantity().add(stockQuantity));
			originalPartSettlement.setSettlementAmount(NegotiationUtils.getSettlementAmount(originalSettOperation.getSettlementPrice(), originalPartSettlement.getStockQuantity()));
			update(originalPartSettlement);
		}
		
	}


	/**
	 * Method to populate the OperationUnfulfillment object according the unfulfillment type and reason.
	 *
	 * @param settlementUnfulfillment the settlement unfulfillment
	 * @param idUnfulfillmentType the id unfulfillment type
	 * @param idUnfulfillmentMotive the id unfulfillment motive
	 * @param idOperationPart the id operation part
	 * @return OperationUnfulfillment
	 * @throws ServiceException the service exception
	 */
	private OperationUnfulfillment populateOperationUnfulfillment(SettlementUnfulfillmentTO settlementUnfulfillment, Long idUnfulfillmentType, Long idUnfulfillmentMotive, Integer idOperationPart) throws ServiceException
	{
		OperationUnfulfillment objOperationUnfulfillment= new OperationUnfulfillment();
		
		SettlementOperation objSettlementOperation= new SettlementOperation(); 
		objSettlementOperation.setIdSettlementOperationPk(settlementUnfulfillment.getIdSettlementOperation());
		objOperationUnfulfillment.setSettlementOperation(objSettlementOperation);
		
		Participant objAffectedParticipant= new Participant();
		Participant objUnfulfilledParticipant= new Participant();
		
		if (UnfulfillmentParameterType.SALE_UNFULFILLMENT_TYPE.getCode().equals(idUnfulfillmentType)) {
			objUnfulfilledParticipant.setIdParticipantPk(settlementUnfulfillment.getIdSaleParticipant()); //seller participant
			objAffectedParticipant.setIdParticipantPk(settlementUnfulfillment.getIdPurchaseParticipant()); //buyer participant
		} else if (UnfulfillmentParameterType.PURCHASE_UNFULFILLMENT_TYPE.getCode().equals(idUnfulfillmentType)) {
			objUnfulfilledParticipant.setIdParticipantPk(settlementUnfulfillment.getIdPurchaseParticipant()); //buyer participant
			objAffectedParticipant.setIdParticipantPk(settlementUnfulfillment.getIdSaleParticipant()); //seller participant
		}
		
		objOperationUnfulfillment.setAffectedParticipant(objAffectedParticipant);
		objOperationUnfulfillment.setUnfulfilledParticipant(objUnfulfilledParticipant);
		objOperationUnfulfillment.setOperationPart(idOperationPart);
		objOperationUnfulfillment.setSettlementDate(settlementUnfulfillment.getSettlementDate());
		objOperationUnfulfillment.setUnfulfilledQuantity(settlementUnfulfillment.getStockQuantity());
		objOperationUnfulfillment.setUnfulfilledAmount(settlementUnfulfillment.getSettlementAmount());
		
		objOperationUnfulfillment.setUnfulfillmentDate(CommonsUtilities.currentDateTime());
		objOperationUnfulfillment.setUnfulfillmentType(idUnfulfillmentType);
		objOperationUnfulfillment.setUnfulfillmentReason(idUnfulfillmentMotive);
		
		return objOperationUnfulfillment;
	}
	
	/**
	 * To save Primary Placement Assignment.
	 *
	 * @param objParticipantAssignment the obj participant assignment
	 * @param holderAccount the holder account
	 * @param mechanismOperation the mechanism operation
	 * @param partRoleType the part role type
	 * @param assignmentRequest the assignment request
	 * @param complianceSecurities the compliance securities
	 * @param confirm the confirm
	 * @throws ServiceException the service exception
	 */
	private void saveHolderAccountAssignment(ParticipantAssignment objParticipantAssignment, HolderAccount holderAccount, 
												MechanismOperation mechanismOperation, Integer partRoleType, 
												AssignmentRequest assignmentRequest, boolean complianceSecurities, boolean confirm) throws ServiceException{	
		Participant participant = null;
		if(partRoleType.equals(ComponentConstant.SALE_ROLE)){
			participant = mechanismOperation.getSellerParticipant();
		}else if (partRoleType.equals(ComponentConstant.PURCHARSE_ROLE)){
			participant = mechanismOperation.getBuyerParticipant();
		}		
		
		if(confirm){
			assignmentRequest.setRequestState(AssignmentRequestStateType.CONFIRMED.getCode());
			assignmentRequest.setConfirmUser(mechanismOperation.getRegisterUser());
			assignmentRequest.setConfirmDate(CommonsUtilities.currentDateTime());
		}else{
			assignmentRequest.setRequestState(AssignmentRequestStateType.REGISTERED.getCode());
		}
		update(assignmentRequest);
		
		HolderAccountOperation holderAccountOperation = new HolderAccountOperation();
		holderAccountOperation.setAssignmentRequest(assignmentRequest);
		holderAccountOperation.setOperationPart(ComponentConstant.CASH_PART);//1 means CASH part
		holderAccountOperation.setRole(partRoleType); 
		holderAccountOperation.setStockQuantity(mechanismOperation.getStockQuantity());
		holderAccountOperation.setSettlementAmount(mechanismOperation.getCashAmount());
		holderAccountOperation.setInchargeStockParticipant(participant);
		holderAccountOperation.setInchargeFundsParticipant(participant);
		holderAccountOperation.setIndIncharge(ComponentConstant.ZERO);//Trader=Funds in charge=Securities in charge
		holderAccountOperation.setHolderAccount(holderAccount);
		holderAccountOperation.setIndAutomaticAsignment(ComponentConstant.ONE);
		holderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		
		if(confirm){
			holderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
			holderAccountOperation.setConfirmDate(CommonsUtilities.currentDateTime());
		}else{
			holderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
		}
		
		holderAccountOperation.setMechanismOperation(mechanismOperation);
		create(holderAccountOperation);
		
		SettlementAccountOperation settlementAccountOperation = settlementProcessServiceBean.populateSettlementAccountOperation(
				mechanismOperation.getIdMechanismOperationPk(), holderAccountOperation, null);
		
		if (ComponentConstant.SALE_ROLE.equals(partRoleType) && complianceSecurities){
			settlementAccountOperation.setStockReference(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		}
		
		create(settlementAccountOperation);

	}
	
	/**
	 * Creates the participant assigment.
	 *
	 * @param assignmentProcess the assignment process
	 * @param objParticipant the obj participant
	 * @return the participant assignment
	 * @throws ServiceException the service exception
	 */
	private ParticipantAssignment createParticipantAssigment(AssignmentProcess assignmentProcess, Participant objParticipant) throws ServiceException{
		ParticipantAssignment objParticipantAssignment = new ParticipantAssignment();
		objParticipantAssignment.setAssignmentProcess(assignmentProcess);
		objParticipantAssignment.setAssignmentState(ParticipantAssignmentStateType.OPENED.getCode());
		objParticipantAssignment.setParticipant(objParticipant);
		return create(objParticipantAssignment);
	}
	
	/**
	 * Creates the assignment request.
	 *
	 * @param objParticipantAssignment the obj participant assignment
	 * @param mechanismOperation the mechanism operation
	 * @param partRoleType the part role type
	 * @return the assignment request
	 */
	private AssignmentRequest createAssignmentRequest(ParticipantAssignment objParticipantAssignment, 
			MechanismOperation mechanismOperation, Integer partRoleType){
		AssignmentRequest assignmentRequest = new AssignmentRequest();
		assignmentRequest.setParticipantAssignment(objParticipantAssignment);
		assignmentRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		assignmentRequest.setRequestState(AssignmentRequestStateType.PENDING.getCode());
		assignmentRequest.setMechanismOperation(mechanismOperation);
		assignmentRequest.setRole(partRoleType);		
		return create(assignmentRequest);
	}


	/**
	 * Update settlement exchange rate operations.
	 *
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void updateSettlementExchangeRateOperations(Date settlementDate) throws ServiceException {

		Map<Integer,DailyExchangeRates> mapExchangeRates = new HashMap<Integer, DailyExchangeRates>();
		
		List<Object[]> arrSettlementOpertions = settlementProcessServiceBean.getDaySettlementOperations(settlementDate);
		for(Object[] objSettOperation : arrSettlementOpertions){
			SettlementOperation settlementOperation = (SettlementOperation)objSettOperation[0];
			MechanismOperation mechanismOperation = (MechanismOperation) objSettOperation[1];
			Integer originCurrency = (Integer)objSettOperation[2];
			
			DailyExchangeRates dailyExchangeRate = mapExchangeRates.get(originCurrency); 
			
			if(dailyExchangeRate == null){
				dailyExchangeRate = parameterServiceBean.getDayExchangeRate(settlementDate, originCurrency);
				if(dailyExchangeRate == null){
					throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST);
				}else{
					mapExchangeRates.put(dailyExchangeRate.getIdCurrency(), dailyExchangeRate);
				}
			}
			
			BigDecimal purchasePrice = dailyExchangeRate.getBuyPrice();
			BigDecimal newPrice = mechanismOperation.getCashPrice().setScale(2,RoundingMode.HALF_UP);
			BigDecimal newTermPrice = null;
			if(Validations.validateIsNotNull(settlementOperation.getAmountRate()) 
					&& !settlementOperation.getAmountRate().equals(BigDecimal.ZERO)){					
				newTermPrice = NegotiationUtils.getTermPrice(newPrice, settlementOperation.getAmountRate(), settlementOperation.getSettlementDays());
				BigDecimal newTermAmount = newTermPrice.multiply(settlementOperation.getStockQuantity()).setScale(2,RoundingMode.HALF_UP);
				newTermAmount =newTermAmount.multiply(purchasePrice).setScale(2,RoundingMode.HALF_UP);				
				settlementOperation.setSettlementAmount(newTermAmount);	
				
				BigDecimal newExchangeTermPrice =newTermPrice.multiply(purchasePrice).setScale(2,RoundingMode.HALF_UP);
				settlementOperation.setSettlementPrice(newExchangeTermPrice);
				settlementOperation.setInitialSettlementPrice(settlementOperation.getSettlementPrice());
				//BigDecimal newTermAmount = NegotiationUtils.getSettlementAmount(newTermPrice, settlementOperation.getStockQuantity());
			}else{					
				newTermPrice = newPrice;
				BigDecimal newTermAmount = newTermPrice.multiply(settlementOperation.getStockQuantity()).setScale(2,RoundingMode.HALF_UP); 
				newTermAmount =newTermAmount.multiply(purchasePrice).setScale(2,RoundingMode.HALF_UP);
				settlementOperation.setSettlementAmount(newTermAmount);
				//newTermAmount = NegotiationUtils.getSettlementAmount(newTermPrice, settlementOperation.getStockQuantity());
				
				BigDecimal newExchangeTermPrice =newTermPrice.multiply(purchasePrice).setScale(2,RoundingMode.HALF_UP);
				settlementOperation.setSettlementPrice(newExchangeTermPrice);
				settlementOperation.setInitialSettlementPrice(settlementOperation.getSettlementPrice());
			}
			
			mechanismOperation.setExchangeRate(purchasePrice);
			
			update(mechanismOperation);
			
			update(settlementOperation);
			
			List<Integer> states= new ArrayList<Integer>();
			states.add(HolderAccountOperationStateType.CONFIRMED.getCode());
			List<SettlementAccountOperation> accountOperations = settlementProcessServiceBean.
					getSettlementAccountOperations(settlementOperation.getIdSettlementOperationPk(),states);
			for (SettlementAccountOperation settlementAccountOperation : accountOperations) {
				BigDecimal newTermAmount= newTermPrice.multiply(settlementAccountOperation.getStockQuantity()).setScale(2,RoundingMode.HALF_UP);
				newTermAmount= newTermAmount.multiply(purchasePrice).setScale(2,RoundingMode.HALF_UP);
				settlementAccountOperation.setSettlementAmount(newTermAmount);
				update(settlementAccountOperation);
			}
			
			List<ParticipantSettlement> participantSettlements = (List<ParticipantSettlement>) settlementProcessServiceBean.
					getParticipantSettlements(settlementOperation.getIdSettlementOperationPk(), null , null);
			for (ParticipantSettlement participantSettlement : participantSettlements) {
				BigDecimal newTermAmount= newTermPrice.multiply(participantSettlement.getStockQuantity()).setScale(2,RoundingMode.HALF_UP);
				newTermAmount= newTermAmount.multiply(purchasePrice).setScale(2,RoundingMode.HALF_UP);
				participantSettlement.setSettlementAmount(newTermAmount);
				update(participantSettlement);
			}
			
		}
	}

	/**
	 * Creates the chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void createChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO, LoggerUser loggerUser) throws ServiceException{
		validateSettlementChainOperation(chainedOperationTO, null);
		chainedOperationsServiceBean.createChainedHolderOperation(chainedOperationTO, loggerUser);
	}
	
	/**
	 * Creates the chained holder operations automatic.
	 *
	 * @param settlementDate the settlement date
	 * @param idParticipant the id participant
	 * @param indExtended the ind extended
	 * @param instrumentType the instrument type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void createChainedHolderOperationsAutomatic(Date settlementDate, Long idParticipant, Integer indExtended, 
			Integer instrumentType, LoggerUser loggerUser) throws ServiceException{
		logger.info("::::::: STARTING CREATING OF CHAINED HOLDER OPERATIONS ::::::::");
		chainedOperationsServiceBean.createChainedHolderOperationsAutomatic(settlementDate, idParticipant, indExtended, instrumentType, loggerUser);		
		logger.info("::::::: FINISHING CREATING OF CHAINED HOLDER OPERATIONS ::::::::");
	}
	
	
	/**
	 * Cancel all chained holder operations.
	 *
	 * @param settlementDate the settlement date
	 * @param indExtended the ind extended
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void cancelAllChainedHolderOperations(Date settlementDate, Integer indExtended, LoggerUser loggerUser) throws ServiceException
	{
		logger.info("::::::: STARTING CANCELLATION OF CHAINED HOLDER OPERATIONS ::::::::");
		//we keep in mind the settlementAccountOperation for all participants, holder accounts and securities 
		Long idParticipant= null, idHolderAccount= null, idChainedHolderOperation= null;
		String idSecurityCode= null;
		chainedOperationsServiceBean.cancelChainedHolderOperations(settlementDate, indExtended, idParticipant, idHolderAccount, 
																	idSecurityCode, idChainedHolderOperation, loggerUser);
		logger.info("::::::: FINISHING CANCELLATION OF CHAINED HOLDER OPERATIONS ::::::::");
	}
	
	/**
	 * Confirm chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void confirmChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO, LoggerUser loggerUser) throws ServiceException
	{
		validateSettlementChainOperation(chainedOperationTO, ChainedOperationStateType.REGISTERED.getCode());
		chainedOperationsServiceBean.confirmChainedHolderOperation(chainedOperationTO, loggerUser);
	}
	
	
	/**
	 * Reject chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void rejectChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO, LoggerUser loggerUser) throws ServiceException
	{
		validateSettlementChainOperation(chainedOperationTO, ChainedOperationStateType.REGISTERED.getCode());
		//we update the chained holder operation state
		chainedOperationsServiceBean.updateStateChainedHolderOperation(chainedOperationTO.getIdChainedHolderOperation(), 
																		ChainedOperationStateType.REJECTED.getCode(), loggerUser);
	}
	
	/**
	 * Cancel chained holder operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void cancelChainedHolderOperation(RegisterChainedOperationTO chainedOperationTO, LoggerUser loggerUser) throws ServiceException
	{
		validateSettlementChainOperation(chainedOperationTO, ChainedOperationStateType.CONFIRMED.getCode());
		/*
		if (chainedOperationTO.getLstChainedOperationDataTO() != null) {
			List<ChainedOperationDataTO> lstChainedOperationOperation= chainedOperationTO.getLstChainedOperationDataTO().getDataList();
			for (ChainedOperationDataTO objChainedOperationDataTO: lstChainedOperationOperation) {
				//List<SettlementAccountOperation> lstSettlementAccountOperation= objChainedOperationDataTO.getLstSettlementAccountOperation();
				List<SettlementAccountMarketfact> lstSettlementAccountMarketfact= objChainedOperationDataTO.getLstSettlementAccountMarketfact();
				for (SettlementAccountMarketfact settlementAccountMarketfact: lstSettlementAccountMarketfact) {
					//we update the settlement account marketfact
					chainedOperationsServiceBean.updateChainedSettlementMarketFact(settlementAccountMarketfact.getIdSettAccountMarketfactPk(), 
																					settlementAccountMarketfact.getChainedQuantity(), 
																					ComponentConstant.SUBTRACTION, loggerUser);
					//we update the settlement account operation
					chainedOperationsServiceBean.updateChainedQuantitySettlementAccount(
																settlementAccountMarketfact.getSettlementAccountOperation().getIdSettlementAccountPk(), 
																settlementAccountMarketfact.getChainedQuantity(), ComponentConstant.SUBTRACTION, loggerUser);
				}
			}
		}
		//we update the chained holder operation state
		chainedOperationsServiceBean.updateStateChainedHolderOperation(chainedOperationTO.getIdChainedHolderOperation(), 
																		ChainedOperationStateType.CANCELLED.getCode(), loggerUser);
		*/																
		//we unblock the sale balance about normal part
		chainedOperationsServiceBean.cancelChainedHolderOperations(chainedOperationTO.getSettlementDate(), 
																	chainedOperationTO.getIndExtended().getParameterTablePk(), 
																	chainedOperationTO.getParticipant().getIdParticipantPk(), 
																	chainedOperationTO.getHolderAccount().getIdHolderAccountPk(), 
																	chainedOperationTO.getSecurity().getIdSecurityCodePk(), 
																	chainedOperationTO.getIdChainedHolderOperation(), loggerUser);
	}
	
	
	/**
	 * Validate settlement chain operation.
	 *
	 * @param chainedOperationTO the chained operation to
	 * @param chainState the chain state
	 * @throws ServiceException the service exception
	 */
	private void validateSettlementChainOperation(RegisterChainedOperationTO chainedOperationTO, Integer chainState) throws ServiceException
	{
		//we verify if not exists any settlement process at execution
		List<SettlementProcess> lstSettlementProcess= settlementProcessServiceBean.getListSettlementProcess(chainedOperationTO.getModalityGroup().getIdModalityGroupPk(), 
																									chainedOperationTO.getSettlementDate(), 
																									SettlementSchemaType.NET.getCode(), 
																									chainedOperationTO.getCurrency().getParameterTablePk());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementProcess)) {
			for (SettlementProcess objSettlementProcess: lstSettlementProcess) {
				if (!SettlementProcessStateType.FINISHED.getCode().equals(objSettlementProcess.getProcessState())) {
					throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_NOT_FINISH, ErrorServiceType.SETTLEMENT_PROCESS_NOT_FINISH.getMessage());
				}
			}
		}
		//we verify if some operation was settled before
		List<SettlementAccountOperation> lstSettlementAccountOperation= chainedOperationsServiceBean.
															getListSettlementAccountOperationByChain(chainedOperationTO.getIdChainedHolderOperation());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountOperation)) {
			for (SettlementAccountOperation objSettlementAccountOperation: lstSettlementAccountOperation) {
				if (MechanismOperationStateType.CASH_SETTLED.getCode().equals(objSettlementAccountOperation.getSettlementOperation().getOperationState()) &&
					OperationPartType.CASH_PART.getCode().equals(objSettlementAccountOperation.getSettlementOperation().getOperationPart())) {
					throw new ServiceException(ErrorServiceType.SETTLEMENT_CHAIN_OPERATIONS, ErrorServiceType.SETTLEMENT_CHAIN_OPERATIONS.getMessage());
				} else if (MechanismOperationStateType.TERM_SETTLED.getCode().equals(objSettlementAccountOperation.getSettlementOperation().getOperationState()) &&
					OperationPartType.TERM_PART.getCode().equals(objSettlementAccountOperation.getSettlementOperation().getOperationPart())) {
					throw new ServiceException(ErrorServiceType.SETTLEMENT_CHAIN_OPERATIONS, ErrorServiceType.SETTLEMENT_CHAIN_OPERATIONS.getMessage());
				}
			}
		}
		//we verify if the chain was confirmed before
		if (Validations.validateIsNotNull(chainState)) {
			ChainedHolderOperation chainedHolderOperation= chainedOperationsServiceBean.getChainedHolderOperation(chainedOperationTO.getIdChainedHolderOperation());
			if (!chainState.equals(chainedHolderOperation.getChaintState())) {
				throw new ServiceException(ErrorServiceType.SETTLEMENT_CHAIN_OPERATION_STATE, ErrorServiceType.SETTLEMENT_CHAIN_OPERATION_STATE.getMessage());
			}
		}
	}
	
	
//	/**
//	 * Identify forced purchase.
//	 *
//	 * @param settlementDate the settlement date
//	 * @param loggerUser the logger user
//	 * @throws ServiceException the service exception
//	 */
//	@Lock(LockType.READ)
//	public void identifyForcedPurchase(Date settlementDate, LoggerUser loggerUser) throws ServiceException
//	{
//		//we get the list of settlement operation to be settled as forced purchase
//		List<Long> lstIdSettlementOperation= settlementProcessServiceBean.getListSettlementOperationToForcedPurchase(settlementDate);
//		
//		if (Validations.validateListIsNotNullAndNotEmpty(lstIdSettlementOperation)) {
//			//we must update the settlement type to FOP and settlement schema to GROSS for each settlement operation
//			settlementProcessServiceBean.updateSettlementOperationToForcedPurchase(lstIdSettlementOperation, loggerUser);
//		}
//	}
	
	/**
	 * Save settlement process new tx.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param settlementSchema the settlement schema
	 * @param countPendingOperations the count pending operations
	 * @param pendingOperations the pending operations
	 * @param processType the process type
	 * @param scheduleType the schedule type
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public SettlementProcess saveSettlementProcessNewTx(Long idMechanism,
			Long idModalityGroup, Integer idCurrency, Date settlementDate, Integer settlementSchema, 
			Long countPendingOperations, List<Long> pendingOperations, Integer processType, Integer scheduleType) throws ServiceException {
		return settlementProcessServiceBean.saveSettlementProcess(idMechanism, idModalityGroup, idCurrency, settlementDate, settlementSchema, 
				countPendingOperations, pendingOperations, processType, scheduleType);
	}
	
	/**
	 * Gets the list settlement operation to.
	 *
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @return the list settlement operation to
	 */
	public List<SettlementOperationTO> getListSettlementOperationTO(Date settlementDate, Long idSettlementProcess) {
		List<SettlementOperationTO> lstSettlementOperationTOs= new ArrayList<SettlementOperationTO>();
		List<Long> lstCashMechanismOperation= new ArrayList<Long>();
		List<Long> lstTermMechanismOperation= new ArrayList<Long>();
		List<Long> lstCashSettlementOperation= new ArrayList<Long>();
		List<Long> lstTermSettlementOperation= new ArrayList<Long>();
		try {
			List<Object[]> lstSettlementOperation= settlementProcessServiceBean.getListSettlementOperationByProcessBatch(settlementDate, idSettlementProcess);
			if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementOperation)) {
				lstSettlementOperationTOs = settlementProcessServiceBean.populateSettlementOperations(lstSettlementOperation, 
																			null, null, null, BooleanType.NO.getCode(), lstCashSettlementOperation, 
																			lstTermSettlementOperation, lstCashMechanismOperation, lstTermMechanismOperation);
			}
		} catch (ServiceException e) {
			
		}
		return lstSettlementOperationTOs;
	}
	
	
	/**
	 * Verify chained holder markekfact balance.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void verifyChainedHolderMarkekfactBalance(Long idSettlementProcess, LoggerUser loggerUser) throws ServiceException{
		//we get all the chains of the settlement process
		List<HolderChainDetail> lstChainedHolderMarketfact= chainedOperationsServiceBean.getListChainedSettlementAccountMarketfact(idSettlementProcess);
		Map<Long, List<HolderChainDetail>> mpChainedHolderMarketfact= new HashMap<Long, List<HolderChainDetail>>();
		List<SettlementAccountMarketfact> lstNewSettlementAccountMarketfacts= new ArrayList<SettlementAccountMarketfact>();
		List<HolderChainDetail> lstNewHolderChainDetails= new ArrayList<HolderChainDetail>();
		List<Long> lstOldSettlementAccountMarketfacts= new ArrayList<Long>();
		if (Validations.validateListIsNotNullAndNotEmpty(lstChainedHolderMarketfact)) {
			for (HolderChainDetail objHolderChainDetail: lstChainedHolderMarketfact) {
				Long idChainedHolderOperation= objHolderChainDetail.getChainedHolderOperation().getIdChainedHolderOperationPk();
				if (Validations.validateIsNull(mpChainedHolderMarketfact.get(idChainedHolderOperation))) {
					mpChainedHolderMarketfact.put(idChainedHolderOperation, new LinkedList<HolderChainDetail>());
				}
				mpChainedHolderMarketfact.get(idChainedHolderOperation).add(objHolderChainDetail);
			}
			List<Long> lstChainedHolderOperation= new ArrayList<Long>(mpChainedHolderMarketfact.keySet());
			for (Long idChainedHolderOperation: lstChainedHolderOperation) {
				List<HolderChainDetail> lstHolderChainDetails= mpChainedHolderMarketfact.get(idChainedHolderOperation);
				List<HolderChainDetail> lstSaleHolderChainDetails= new LinkedList<HolderChainDetail>();
				List<HolderChainDetail> lstPurchaseHolderChainDetails= new LinkedList<HolderChainDetail>();
				BigDecimal saleQuantity= BigDecimal.ZERO;
				for (HolderChainDetail objHolderChainDetail: lstHolderChainDetails) {
					SettlementAccountMarketfact settlementAccountMarketfact= objHolderChainDetail.getSettlementAccountMarketfact();
					Integer role= settlementAccountMarketfact.getSettlementAccountOperation().getRole();
					
					if (ComponentConstant.SALE_ROLE.equals(role)) {
						lstSaleHolderChainDetails.add(objHolderChainDetail);
						saleQuantity= saleQuantity.add(settlementAccountMarketfact.getMarketQuantity());
					} else {
						lstPurchaseHolderChainDetails.add(objHolderChainDetail);
					}
				}
				//now we verify the sale settlement market fact
				int i=0;	
				restore:
				while (i<lstPurchaseHolderChainDetails.size()) {	
				//for (HolderChainDetail purchaseHolderChainDetail: lstPurchaseHolderChainDetails) {
					HolderChainDetail purchaseHolderChainDetail= lstPurchaseHolderChainDetails.get(i);
					if (purchaseHolderChainDetail.getSettlementAccountMarketfact().getChainedQuantity().compareTo(BigDecimal.ZERO) > 0 ) {
						SettlementAccountMarketfact purchaseSettlementAccountMarketfact= purchaseHolderChainDetail.getSettlementAccountMarketfact(); 
						em.detach(purchaseSettlementAccountMarketfact);
						for (HolderChainDetail saleHolderChainDetail: lstSaleHolderChainDetails) {
							if (saleHolderChainDetail.getSettlementAccountMarketfact().getMarketQuantity().compareTo(BigDecimal.ZERO) > 0 ) {
								SettlementAccountMarketfact saleSettlementAccountMarketfact= saleHolderChainDetail.getSettlementAccountMarketfact(); 
								HolderChainDetail newHolderChainDetail= null;
								SettlementAccountMarketfact newSettlementAccountMarketfact= null;
								try {
									newSettlementAccountMarketfact= saleHolderChainDetail.getSettlementAccountMarketfact().clone();
									newHolderChainDetail= saleHolderChainDetail.clone();
									newHolderChainDetail.setSettlementAccountMarketfact(newSettlementAccountMarketfact);
								} catch (CloneNotSupportedException e) { }
								BigDecimal deltaQuantity= purchaseSettlementAccountMarketfact.getChainedQuantity().
																		subtract(saleSettlementAccountMarketfact.getMarketQuantity());
								if (deltaQuantity.compareTo(BigDecimal.ZERO) > 0) {
									newSettlementAccountMarketfact.setMarketQuantity(saleSettlementAccountMarketfact.getChainedQuantity());
									newSettlementAccountMarketfact.setChainedQuantity(saleSettlementAccountMarketfact.getChainedQuantity());
									saleSettlementAccountMarketfact.setMarketQuantity(BigDecimal.ZERO);
									purchaseSettlementAccountMarketfact.setChainedQuantity(deltaQuantity);
								} else {
									newSettlementAccountMarketfact.setMarketQuantity(purchaseSettlementAccountMarketfact.getChainedQuantity());
									newSettlementAccountMarketfact.setChainedQuantity(purchaseSettlementAccountMarketfact.getChainedQuantity());
									saleSettlementAccountMarketfact.setMarketQuantity(saleSettlementAccountMarketfact.getMarketQuantity().
																				subtract(purchaseSettlementAccountMarketfact.getChainedQuantity()));
									saleSettlementAccountMarketfact.setChainedQuantity(saleSettlementAccountMarketfact.getChainedQuantity().
																				subtract(purchaseSettlementAccountMarketfact.getChainedQuantity()));
									purchaseSettlementAccountMarketfact.setChainedQuantity(BigDecimal.ZERO);
								}
								newSettlementAccountMarketfact.setIdSettAccountMarketfactPk(null);
								newSettlementAccountMarketfact.setMarketPrice(purchaseSettlementAccountMarketfact.getMarketPrice());
								lstNewSettlementAccountMarketfacts.add(newSettlementAccountMarketfact);
								newHolderChainDetail.setIdHolderChainDetailPk(null);
								newHolderChainDetail.setChainedQuantity(newSettlementAccountMarketfact.getChainedQuantity());
								lstNewHolderChainDetails.add(newHolderChainDetail);
								saleQuantity = saleQuantity.subtract(newSettlementAccountMarketfact.getChainedQuantity());
								if (!lstOldSettlementAccountMarketfacts.contains(saleSettlementAccountMarketfact.getIdSettAccountMarketfactPk())) {
									lstOldSettlementAccountMarketfacts.add(saleSettlementAccountMarketfact.getIdSettAccountMarketfactPk());
								}
								//at this point, if the purchase account market fact is finished, so we restart the iteration
								if (BigDecimal.ZERO.compareTo(purchaseSettlementAccountMarketfact.getChainedQuantity()) == 0) {
									i=0;
									continue restore;
								}
							}
						}
					}
					++i;
				}
			}
			//we update the old settlement account market fact
			if (Validations.validateListIsNotNullAndNotEmpty(lstOldSettlementAccountMarketfacts)) {
				settlementProcessServiceBean.updateSettlementAccountMarketfact(lstOldSettlementAccountMarketfacts, loggerUser);
			}
			//we create the new settlement account market fact
			if (Validations.validateListIsNotNullAndNotEmpty(lstNewSettlementAccountMarketfacts)) {
				settlementProcessServiceBean.saveAll(lstNewSettlementAccountMarketfacts);
			}
			//we create the new holder chain detail about new market facts
			if (Validations.validateListIsNotNullAndNotEmpty(lstNewHolderChainDetails)) {
				settlementProcessServiceBean.saveAll(lstNewHolderChainDetails);
			}
		}
	}
	
	/**
	 * Close general assignment process.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 * @throws AccountAssignmentException 
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void closeGeneralAssignmentProcess(Long assignmentProcessId, LoggerUser loggerUser) throws ServiceException, AccountAssignmentException{
		
		AssignmentProcess assignmentProcess= accountAssignmentService.find(AssignmentProcess.class, assignmentProcessId);
		if (AssignmentProcessStateType.OPENED.getCode().equals(assignmentProcess.getAssignmentState())) {
			/** 1 we reject all in charges not confirmed **/
			accountAssignmentService.rejectInchargesNotConfirmed(assignmentProcessId,loggerUser);
			
			/** 2 we must auto complete the assignment of the holder accounts not registered **/		
			accountAssignmentService.completeHolderAccountAssignment(assignmentProcessId, loggerUser);
			
			/** 3 we confirm the accounts recorded **/ 
			accountAssignmentService.confirmRecordedAssigments(assignmentProcessId, loggerUser);	
			
			/** 4 synchronize the assigned market facts about DPF operations **/
			//accountAssignmentService.synchronizeSettlementAccountMarketfact(assignmentProcessId, loggerUser);
			
			/** 5 we must close all the participant assignments for this process **/
			accountAssignmentService.closeParticipantAssignments(assignmentProcessId, loggerUser);
			
			/** 6 we must close the assignment process **/
			accountAssignmentService.closeAssignmentProcess(assignmentProcessId, loggerUser);		

			// NEW: we reuse component for unfulfillment 
			//settlementsComponentSingleton.identifySettlementUnfulfillment(assignmentProcessId, null, null, loggerUser);

			/** 7 cancel all currency settlement requests **/
			currencySettlementService.rejectAllCurrencySettlementRequest(CommonsUtilities.currentDate(), loggerUser);
			
		}				
		accountAssignmentService.flushTransaction();
	}
	
	/**
	 * Gets the notification.
	 *
	 * @param eventCode the event code
	 * @param processPk the process pk
	 * @param detailType the detail type
	 * @param indBegin the ind begin
	 * @param indFinish the ind finish
	 * @param rowQuantity the row quantity
	 * @param hasError the has error
	 * @param percentag the percentag
	 * @return the notification
	 */
	public SettlementNotificationEvent getNotification(Integer indBegin, Integer indFinish, Integer rowQuantity, Integer hasError, Integer percentag) {
		SettlementNotificationEvent settlementNotificationEvent = new SettlementNotificationEvent();				
		settlementNotificationEvent.setRowQuantity(rowQuantity.longValue());
		settlementNotificationEvent.setIndHasError(hasError);
		settlementNotificationEvent.setPercentage(percentag);		
		if(indBegin.equals(BooleanType.YES.getCode())){
			settlementNotificationEvent.setBeginTime(CommonsUtilities.currentDateTime());
			settlementNotificationEvent.setIndBegin(BooleanType.YES.getCode());
			settlementNotificationEvent.setIndFinish(BooleanType.NO.getCode());
			settlementNotificationEvent.setInExecution(BooleanType.YES.getCode());
		}else if(indBegin.equals(BooleanType.NO.getCode())){
			settlementNotificationEvent.setFinishTime(CommonsUtilities.currentDateTime());
			settlementNotificationEvent.setIndFinish(BooleanType.YES.getCode());
			settlementNotificationEvent.setInExecution(BooleanType.NO.getCode());
		}
		return settlementNotificationEvent;
	}
}

	