package com.pradera.settlements.core.service;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrimaryPlacementSettlement.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class GeneratedOperationNumberSendXmlService extends CrudDaoServiceBean{

	Logger logger = Logger.getLogger(GeneratedOperationNumberSendXmlService.class);
	
	/**
	 * Metodo que genera un numero secuencial para el envio de mensaje XML a un determinado EIF
	 * @param mNemonicEif Nemonico de una Entidad de Intermediacion Financiera
	 * @return
	 * @throws ServiceException
	 */
	public Long nextOperationNumber(String mNemonicEif) throws ServiceException{
		
		String sequenceName = "SQ_ID_SEND_MSG_EIF_" + mNemonicEif;
		
		StringBuilder query = new StringBuilder();
		
		query.append("  ");
		query.append(" select count(*) ");
		query.append(" from USER_SEQUENCES us ");
		query.append(" where us.SEQUENCE_NAME = :sequenceName ");
		
		BigDecimal numberSequence = BigDecimal.ZERO;
		
		try {
			
			Query consult = em.createNativeQuery(query.toString());
			consult.setParameter("sequenceName", sequenceName);
			
			numberSequence =  (BigDecimal) consult.getSingleResult();
			
		} catch (NoResultException e) {
		}		 
		
		logger.info("Secuenciador " + sequenceName + (numberSequence.equals(BigDecimal.ONE) ? numberSequence : "  NO Existe"));
		
		if(numberSequence.equals(BigDecimal.ZERO)){
			// El secuenciador no existe
			logger.info("Creando Secuenciador");			
			
			query = new StringBuilder();
			
			query.append("CREATE SEQUENCE ");
			query.append(sequenceName);
			query.append(" MINVALUE 1 MAXVALUE 9999999999 ");
			query.append(" INCREMENT BY 1 START WITH 100 NOCACHE ");
			
			int numRegisters = em.createNativeQuery(query.toString()).executeUpdate();
			
			logger.info("Sequence " + sequenceName + " creado con exito. " + numRegisters );
			
		} 
		
		query = new StringBuilder();
		query.append(" select ").append(sequenceName).append(".NEXTVAL ");
		query.append(" from dual ");

		
		try {
			numberSequence = (BigDecimal) em.createNativeQuery(query.toString()).getSingleResult();
		} catch (NoResultException e) {			
		}		
		
		logger.info(sequenceName + ".NEXTVAL: " + numberSequence);
		
		if(numberSequence.equals(BigDecimal.ZERO)){
			// error al generar secuenciador
			logger.error("Error al recuperar secuenciador: " + sequenceName);
			throw new ServiceException();
		}
		
		return numberSequence.longValue();
	}
	
}
