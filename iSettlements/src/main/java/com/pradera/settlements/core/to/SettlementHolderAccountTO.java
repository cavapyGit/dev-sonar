package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.MarketFactAccountTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementHolderAccountTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementHolderAccountTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement account operation. */
	private Long idSettlementAccountOperation;
	
	/** The id holder account operation. */
	private Long idHolderAccountOperation;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The id holder account. */
	private Long idHolderAccount;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/** The role. */
	private Integer role;
	
	/** The operation part. */
	private Integer operationPart;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The id ref account operation. */
	private Long idRefAccountOperation;
	
	/** The id ref settlement account operation. */
	private Long idRefSettlementAccountOperation;
//	private BigDecimal settledQuantity;
/** The ind complete settlement. */
//	private BigDecimal totalQuantity;
	private Integer indCompleteSettlement = ComponentConstant.ZERO;
	
	/** The chained quantity. */
	private BigDecimal chainedQuantity;
	//private Long idRefRepoAccountOperation;
	
	/** The market fact accounts. */
	private List<MarketFactAccountTO> marketFactAccounts;
	
	/**
	 * Instantiates a new settlement holder account to.
	 */
	public SettlementHolderAccountTO() {
		super();
	}

	/**
	 * Gets the id holder account operation.
	 *
	 * @return the id holder account operation
	 */
	public Long getIdHolderAccountOperation() {
		return idHolderAccountOperation;
	}

	/**
	 * Sets the id holder account operation.
	 *
	 * @param idHolderAccountOperation the new id holder account operation
	 */
	public void setIdHolderAccountOperation(Long idHolderAccountOperation) {
		this.idHolderAccountOperation = idHolderAccountOperation;
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the id holder account.
	 *
	 * @return the id holder account
	 */
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}

	/**
	 * Sets the id holder account.
	 *
	 * @param idHolderAccount the new id holder account
	 */
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}

	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the id ref account operation.
	 *
	 * @return the id ref account operation
	 */
	public Long getIdRefAccountOperation() {
		return idRefAccountOperation;
	}

	/**
	 * Sets the id ref account operation.
	 *
	 * @param idRefAccountOperation the new id ref account operation
	 */
	public void setIdRefAccountOperation(Long idRefAccountOperation) {
		this.idRefAccountOperation = idRefAccountOperation;
	}

	/**
	 * Gets the market fact accounts.
	 *
	 * @return the market fact accounts
	 */
	public List<MarketFactAccountTO> getMarketFactAccounts() {
		return marketFactAccounts;
	}

	/**
	 * Sets the market fact accounts.
	 *
	 * @param marketFactAccounts the new market fact accounts
	 */
	public void setMarketFactAccounts(List<MarketFactAccountTO> marketFactAccounts) {
		this.marketFactAccounts = marketFactAccounts;
	}
//
//	public Long getIdRefRepoAccountOperation() {
//		return idRefRepoAccountOperation;
//	}
//
//	public void setIdRefRepoAccountOperation(Long idRefRepoAccountOperation) {
//		this.idRefRepoAccountOperation = idRefRepoAccountOperation;
//	}

	/**
 * Gets the id settlement account operation.
 *
 * @return the id settlement account operation
 */
public Long getIdSettlementAccountOperation() {
		return idSettlementAccountOperation;
	}

	/**
	 * Sets the id settlement account operation.
	 *
	 * @param idSettlementAccountOperation the new id settlement account operation
	 */
	public void setIdSettlementAccountOperation(Long idSettlementAccountOperation) {
		this.idSettlementAccountOperation = idSettlementAccountOperation;
	}

	/**
	 * Gets the id ref settlement account operation.
	 *
	 * @return the id ref settlement account operation
	 */
	public Long getIdRefSettlementAccountOperation() {
		return idRefSettlementAccountOperation;
	}

	/**
	 * Sets the id ref settlement account operation.
	 *
	 * @param idRefSettlementAccountOperation the new id ref settlement account operation
	 */
	public void setIdRefSettlementAccountOperation(
			Long idRefSettlementAccountOperation) {
		this.idRefSettlementAccountOperation = idRefSettlementAccountOperation;
	}

	/**
	 * Gets the ind complete settlement.
	 *
	 * @return the ind complete settlement
	 */
	public Integer getIndCompleteSettlement() {
		return indCompleteSettlement;
	}

	/**
	 * Sets the ind complete settlement.
	 *
	 * @param indCompleteSettlement the new ind complete settlement
	 */
	public void setIndCompleteSettlement(Integer indCompleteSettlement) {
		this.indCompleteSettlement = indCompleteSettlement;
	}
	
	/**
	 * Gets the chained quantity.
	 *
	 * @return the chained quantity
	 */
	public BigDecimal getChainedQuantity() {
		return chainedQuantity;
	}

	/**
	 * Sets the chained quantity.
	 *
	 * @param chainedQuantity the new chained quantity
	 */
	public void setChainedQuantity(BigDecimal chainedQuantity) {
		this.chainedQuantity = chainedQuantity;
	}
	
	/**
	 * Checks for chain operation.
	 *
	 * @return true, if successful
	 */
	public boolean hasChainOperation() {
		if (this.chainedQuantity.compareTo(BigDecimal.ZERO) > 0) {
			return true;
		}
		return false;
	}
}
