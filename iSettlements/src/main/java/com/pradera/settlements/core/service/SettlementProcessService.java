package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
//import com.pradera.core.component.business.service.ExecutorComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.component.settlements.to.SecuritiesTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.ParticipantOperationStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.OperationSettlement;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.SettlementSchedule;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.negotiations.jaxb.dto.OperationTutXmlDTO;
import com.pradera.settlements.core.to.NetParticipantPositionTO;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;
import com.pradera.settlements.core.to.NetSettlementOperationTO;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.webclient.SettlementServiceConsumer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementProcessService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class SettlementProcessService extends CrudDaoServiceBean{
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(SettlementProcessService.class);
	
	/** The guarantees settlement. */
	@EJB
	private GuaranteesSettlement guaranteesSettlement;
	
	/** The collection process service bean. */
	@EJB
	private CollectionProcessService collectionProcessServiceBean;
	
	/** The accounts component service. */
	@Inject
	private Instance<AccountsComponentService> accountsComponentService;
	
	/** The holder balance movements service bean. */
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The settlement service consumer. */
	@EJB
	private SettlementServiceConsumer settlementServiceConsumer;
	
	/** The batch service bean. */
	@EJB
	private BatchServiceBean batchServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The Generator Sequence Send Message XML */
	@EJB
	private GeneratedOperationNumberSendXmlService generatedOperationNumberSendXmlService;
	
	public MechanismOperation findMechanismOperationBySecurityAndDate(String idSecurityPk, Date dateParam){
		List<MechanismOperation> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append(" select mo");
		sd.append(" from MechanismOperation mo ");
		sd.append(" inner join mo.securities s");
		sd.append(" where s.idSecurityCodePk = :idSecurityPk");
		sd.append(" and trunc(mo.operationDate) = :dateParam");
		//sd.append(" and rownum <= 1");
		sd.append(" order by mo.idMechanismOperationPk desc");
		
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("idSecurityPk", idSecurityPk);
		paramters.put("dateParam", dateParam);
		lst = findListByQueryString(sd.toString(), paramters);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			return lst.get(0);
		}else{
			return null;
		}
	}
	
	public HolderAccountMovement findHolderAccountMovementBySecurityAndDate(String idSecurityPk, Date dateParam){
		List<HolderAccountMovement> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select ham ");
		sd.append(" from HolderAccountMovement ham");
		sd.append(" inner join ham.movementType mt");
		sd.append(" inner join ham.holderAccountBalance hab ");
		sd.append(" where mt.idMovementTypePk in (");
		sd.append(" 		300037, 300039, 300041, 300043, 300045,");
		sd.append(" 		300047, 300049, 300051, 300053, 300055 )");
		sd.append(" and trunc(ham.movementDate) = :dateParam ");
		sd.append(" and hab.id.idSecurityCodePk = :idSecurityPk ");
		//sd.append(" and rownum <= 1");
		sd.append(" order by ham.idHolderAccountMovementPk desc");
		
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("dateParam", dateParam);
		paramters.put("idSecurityPk", idSecurityPk);
		lst = findListByQueryString(sd.toString(), paramters);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			return lst.get(0);
		}else{
			return null;
		}
		
	}
	
	/**
	 * Issue 1310: Valore que sufrieron movieminto de Cambio de Titularidad de un determinado emisor y fecha
	 * @param idIssuerPk
	 * @param dateParam
	 * @return
	 */
	public List<String> lstSecurituesWithMovementTransferHolder(String idIssuerPk, Date dateParam){
		List<String> lst=new ArrayList<>();
		
		StringBuilder sd=new StringBuilder();
		sd.append(" SELECT DISTINCT HAM.ID_SECURITY_CODE_FK                                         ");
		sd.append(" FROM HOLDER_ACCOUNT_MOVEMENT ham                                                ");
		sd.append(" INNER JOIN SECURITY s ON S.ID_SECURITY_CODE_PK = HAM.ID_SECURITY_CODE_FK        ");
		// son todos los movimientos relacionados a un CTT
		sd.append(" WHERE HAM.ID_MOVEMENT_TYPE_FK IN ( 300037, 300039, 300041, 300043, 300045,      ");
		sd.append(" 									300047, 300049, 300051, 300053, 300055 )    ");
		sd.append(" AND S.STATE_SECURITY = 131                                                      ");
		sd.append(" AND S.SECURITY_CLASS IN ( 420,1976 )                                            ");
		sd.append(" AND S.ID_ISSUER_FK = :idIssuerPk                                                ");
		sd.append(" AND TRUNC ( HAM.MOVEMENT_DATE ) = :dateParam 									");
		
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("idIssuerPk", idIssuerPk);
		paramters.put("dateParam", dateParam);
		lst = findByNativeQuery(sd.toString(), paramters);
		return lst;
	}
	
	/**
	 * obtiene todos los valores negociados en una determinada fecha (contado y plazo) de un determinado emisor
	 * @param idIssuerPk
	 * @param dateParam
	 * @return
	 */
	public List<Object[]> lstSecuritiesNegotiatedAndSettled(String idIssuerPk, Date dateParam) throws ServiceException{
		List<Object[]> lst = new ArrayList<>();
		StringBuilder sd =new StringBuilder();

		sd.append(" SELECT ");
		sd.append(" 	MO1.ID_SECURITY_CODE_FK                                                                                                                                                 ");
		sd.append(" FROM MECHANISM_OPERATION MO1                                                                                                                                                ");
		sd.append(" INNER JOIN MECHANISM_MODALITY MM1 ON MM1.ID_NEGOTIATION_MECHANISM_PK = MO1.ID_NEGOTIATION_MECHANISM_FK AND MM1.ID_NEGOTIATION_MODALITY_PK = MO1.ID_NEGOTIATION_MODALITY_FK  ");
		sd.append(" WHERE MO1.ID_MECHANISM_OPERATION_PK IN                                                                                                                                      ");
		// subQuery para obtener solo las ultimas operaciones negociadas, por ejemplo, un valor se negocia en reporto luego puede negociarce a compra en firme, entonces solo obtenemos la ultima operacion
		sd.append(" 	( SELECT DISTINCT MAX ( MO.ID_MECHANISM_OPERATION_PK ) OVER ( PARTITION BY MO.ID_NEGOTIATION_MECHANISM_FK,MO.ID_SECURITY_CODE_FK ) AS ID_MAX_MCN                        ");
		sd.append(" 	FROM MECHANISM_OPERATION MO                                                                                                                                             ");
		sd.append(" 	INNER JOIN SECURITY S            ON S.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                                                                                      ");
		sd.append(" 	INNER JOIN MECHANISM_MODALITY MM ON MM.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK AND MM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK   ");
		sd.append(" 	WHERE 0 = 0                                                                                                                                                             ");
		sd.append(" 	AND S.SECURITY_CLASS IN ( 420,1976 ) 			"); // DPF , DPA
		sd.append(" 	AND S.ID_ISSUER_FK = :idIssuerPk        		");
		sd.append(" 	AND MO.OPERATION_STATE IN ( 615,616 ) 			"); // LIQUDADA CONTADO, LIQUIDADA PLAZO
		sd.append(" 	AND TRUNC ( MO.OPERATION_DATE ) = :dateParam 	");
		sd.append(" 	)												");
		sd.append(" UNION ");
		sd.append(" SELECT ");
		sd.append(" 	MO.ID_SECURITY_CODE_FK                                                                          ");
		sd.append(" FROM MECHANISM_OPERATION MO                                                                         ");
		sd.append(" INNER JOIN SECURITY S              ON S.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                ");
		sd.append(" INNER JOIN SETTLEMENT_OPERATION so ON SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK   ");
		sd.append(" INNER JOIN MECHANISM_MODALITY MM   ON MM.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK AND MM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK");
		sd.append(" WHERE 0 = 0										");
		sd.append(" AND MO.ID_NEGOTIATION_MODALITY_FK IN ( 3,4 )	"); // REPORTO RF RV
		sd.append(" AND S.SECURITY_CLASS IN ( 420,1976 ) 			"); // DPF, DPA
		sd.append(" AND S.ID_ISSUER_FK = :idIssuerPk 				");
		sd.append(" AND MO.OPERATION_STATE IN ( 615,616 ) 			"); // LIQUDADA CONTADO, LIQUIDADA PLAZO
		sd.append(" AND TRUNC ( so.SETTLEMENT_DATE ) = :dateParam	");
		sd.append(" AND SO.OPERATION_PART = 2						"); // PARTE PLAZO
		
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("idIssuerPk", idIssuerPk);
		paramters.put("dateParam", dateParam);
		
		List<String> lstSecurituesNegotiated = findByNativeQuery(sd.toString(), paramters);
		if(Validations.validateListIsNotNullAndNotEmpty(lstSecurituesNegotiated)){
			List<Object[]> lstAux;
			for(String securityMcn:lstSecurituesNegotiated){
				lstAux=getLastMcnBySecurityAndDateOperation(securityMcn, dateParam);
				if(Validations.validateListIsNotNullAndNotEmpty(lstAux)){
					lst.addAll(lstAux);
				}
			}
		}
		return lst;
	}
	
	/**
	 * obtiene la ultima operacion MCN (BBV, OTC, SIRTEX) de un determinado valor y fecha
	 * @param idSecurityPk
	 * @param dateParam
	 * @return
	 * @throws ServiceException
	 */
	private List<Object []> getLastMcnBySecurityAndDateOperation(String idSecurityPk, Date dateParam) throws ServiceException {
		StringBuilder sd = new StringBuilder();
		sd.append(" SELECT 	SQ_MO.ID_NEGOTIATION_MECHANISM_FK, SQ_MO.ID_SECURITY_CODE_FK, ");
		sd.append(" 		SQ_MO.INTERFACE_TRANSFER_CODE, SQ_MO.ID_MECHANISM_OPERATION_PK ");
		sd.append(" FROM ");
		sd.append(" (SELECT MO.ID_NEGOTIATION_MECHANISM_FK,                                 ");
		sd.append(" 	MO.ID_SECURITY_CODE_FK,                                             ");
		sd.append(" 	MM.INTERFACE_TRANSFER_CODE,                                         ");
		sd.append(" 	MO.ID_MECHANISM_OPERATION_PK                                        ");
		sd.append(" FROM MECHANISM_OPERATION MO                                             ");
		sd.append(" INNER JOIN MECHANISM_MODALITY MM                                        ");
		sd.append(" 	ON MM.ID_NEGOTIATION_MECHANISM_PK = MO.ID_NEGOTIATION_MECHANISM_FK  ");
		sd.append(" 	AND MM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK   ");
		sd.append(" WHERE 0 = 0                                                             ");
		sd.append(" AND MO.OPERATION_STATE IN ( 615,616 )                                   ");
		sd.append(" AND MO.ID_SECURITY_CODE_FK = :idSecurityPk                              ");
		sd.append(" AND TRUNC ( MO.LAST_MODIFY_DATE ) = :dateParam                          ");
		sd.append(" ORDER BY MO.LAST_MODIFY_DATE DESC) SQ_MO								");
		sd.append(" WHERE ROWNUM = 1 ");
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("idSecurityPk", idSecurityPk);
		paramters.put("dateParam", dateParam);
		return findByNativeQuery(sd.toString(), paramters);
	}
	
	/**
	 *  obtiene los ID de todos los emisores en estado registrado
	 * @return
	 * @throws ServiceException
	 */
	public List<String> lstIdAllIssuer() throws ServiceException {
		String query = "SELECT I.ID_ISSUER_PK FROM ISSUER i WHERE I.STATE_ISSUER = :stateIssuer";
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("stateIssuer", IssuerStateType.REGISTERED.getCode());
		return findByNativeQuery(query, paramters);
	}
	
	/**
	 * obtiene los datos del ultimo titular de un determinado valor y fecha
	 * @param idSecurityPk
	 * @param dateParam
	 * @return List<HolderAccountDetail>
	 * @throws ServiceException
	 */
	public List<HolderAccountDetail> holderAccountDetailFullToBuildXmlLastHolder(String idSecurityPk, Date dateParam ) throws ServiceException {
		Long idHolderAccount = getIdHolderAccountBySecurity(idSecurityPk, dateParam);
		if(Validations.validateIsNotNullAndPositive(idHolderAccount)){
			StringBuilder sd = new StringBuilder();
			sd.append(" select had ");
			sd.append(" from HolderAccountDetail had ");
			sd.append(" inner join fetch had.holderAccount ha ");
			sd.append(" inner join fetch had.holder h");
			sd.append(" where ha.idHolderAccountPk = :idHolderAccount");
			Map<String, Object> paramters = new HashMap<String, Object>();
			paramters.put("idHolderAccount", idHolderAccount);
			return findListByQueryString(sd.toString(), paramters);
		}
		return null;
	}
	/**
	 * obtiene el id de HolderAccount a partir de un valor y fecha ingresados en la cartera
	 * @param idSecurityPk
	 * @param dateParam
	 * @return
	 * @throws ServiceException
	 */
	private Long getIdHolderAccountBySecurity(String idSecurityPk, Date dateParam) throws ServiceException {
		
		String dateString = CommonsUtilities.convertDatetoString(dateParam);
		
		StringBuilder sd =new StringBuilder();
		sd.append(" SELECT MFV.ID_HOLDER_ACCOUNT_PK             ");
		sd.append(" FROM MARKET_FACT_VIEW mfv                   ");
		sd.append(" WHERE 0 = 0                                 ");
		sd.append(" AND MFV.ID_SECURITY_CODE_PK = :idSecurityPk ");
		sd.append(" AND MFV.TOTAL_BALANCE > 0					");
		//sd.append(" AND MFV.CUT_DATE = :dateParam               ");
		sd.append(" AND MFV.CUT_DATE >= TRUNC (TO_DATE (:dateString, 'DD/MM/YYYY'), 'DD')");
		sd.append(" AND MFV.CUT_DATE < TRUNC  (TO_DATE (:dateString, 'DD/MM/YYYY'), 'DD') + 1");
		Map<String, Object> paramters = new HashMap<String, Object>();
		//paramters.put("dateParam", dateParam);
		paramters.put("dateString", dateString);
		paramters.put("idSecurityPk", idSecurityPk);
		List<Object> lstObj = findByNativeQuery(sd.toString(), paramters);
		if(Validations.validateListIsNotNullAndNotEmpty(lstObj)){
			return Long.parseLong(lstObj.get(0).toString());
		}else{
			return null;
		}
	}
	
	/**
	 * Gets the list settlement account operations to block balances.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idRole the id role
	 * @param blockDate the block date
	 * @param settlementSchema the settlement schema
	 * @return the list settlement account operations to block balances
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSettlementAccountOperationsToBlockBalances(Long idMechanism, Long idModality, Integer idRole, 
																			Date blockDate, Integer settlementSchema) throws ServiceException
	{
		List<Object[]> lstObjects = new ArrayList<Object[]>();	
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT DISTINCT MO.idMechanismOperationPk, "); // ID Mechanism operation 0
		stringBuilderSql.append(" HAO.idHolderAccountOperationPk, "); // ID Holder account operation 1
		stringBuilderSql.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // ID mechanism 2
		stringBuilderSql.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // ID modality 3
		stringBuilderSql.append(" HAO.inchargeStockParticipant.idParticipantPk, "); // ID participant stock in charge 4
		stringBuilderSql.append(" HAO.holderAccount.idHolderAccountPk, "); //ID holder account 5
		stringBuilderSql.append(" HAO.role, "); //ID Role holder account 6
		stringBuilderSql.append(" HAO.operationPart, "); //ID operation part 7
		stringBuilderSql.append(" HAO.stockQuantity, "); // stock quantity from holder account operation 8
		stringBuilderSql.append(" SE.idSecurityCodePk, "); // ID Security code 9
		stringBuilderSql.append(" 0, "); //indicator to know this doesn't use SwapOperation 10
		stringBuilderSql.append(" 0, "); //check chain 11
		stringBuilderSql.append(" NVL(SAO.chainedQuantity,0), "); //chained quantity 12
		stringBuilderSql.append(" SAO.stockQuantity, "); //settlement account quantity 13 
		stringBuilderSql.append(" SAO.idSettlementAccountPk, "); //settlement account 14
		stringBuilderSql.append(" SO.idSettlementOperationPk, "); //settlement operation 15
		stringBuilderSql.append(" SO.indDematerialization, "); //settlement operation 16
		stringBuilderSql.append(" MO.cashPrice, "); //cash price 17
		stringBuilderSql.append(" SE.instrumentType, "); //settlement price 18 
		stringBuilderSql.append(" MO.indReportingBalance "); //19
		stringBuilderSql.append(" FROM SettlementAccountOperation SAO inner join SAO.holderAccountOperation HAO ");
		stringBuilderSql.append(" inner join SAO.settlementOperation SO inner join SO.mechanismOperation MO ");
		stringBuilderSql.append(" inner join MO.securities SE ");
		stringBuilderSql.append(" WHERE "); 
		stringBuilderSql.append("     SAO.role = :idRole "); //role from holder account operation
		stringBuilderSql.append(" and HAO.holderAccountState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
		stringBuilderSql.append(" and SAO.operationState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
		stringBuilderSql.append(" and SAO.stockReference is null "); //do not blocks balance yet
		stringBuilderSql.append(" and SO.settlementSchema = :settlementSchema ");
		stringBuilderSql.append(" and SO.indDematerialization != :indicatorOne ");  // operations marked with dematerialization, are not blocked
		stringBuilderSql.append(" and ( ( SO.operationPart = :cashPart ");
		stringBuilderSql.append("         and SO.operationState = :assignedState ");
		stringBuilderSql.append("         and MO.indCashStockBlock = :indicatorOne ) or ");
		stringBuilderSql.append("       ( SO.operationPart = :termPart ");
		stringBuilderSql.append("         and SO.operationState = :cashSettledState ");
		stringBuilderSql.append("         and MO.indTermStockBlock = :indicatorOne ) ) ");

		if (idMechanism != null) {
			stringBuilderSql.append(" and MO.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism "); 
		}
		if (idModality != null) {
			stringBuilderSql.append(" and MO.mechanisnModality.id.idNegotiationModalityPk = :idModality ");
		} 
		stringBuilderSql.append(" and SO.settlementDate = :blockDate  ");
		if (ComponentConstant.SALE_ROLE.equals(idRole)) {
			//the accounts with full chain quantity doesn't block balance
			stringBuilderSql.append(" and (SAO.stockQuantity - NVL(SAO.chainedQuantity,0)) > 0 ");
			//we exclude the chained operations with REGISTERED state
			stringBuilderSql.append(" and not exists (Select HCD From HolderChainDetail HCD "
									+ " Where HCD.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = SAO.idSettlementAccountPk "
									+ " and HCD.chainedHolderOperation.chaintState = :chainState) ");
		}
		stringBuilderSql.append(" ORDER BY MO.mechanisnModality.id.idNegotiationMechanismPk, MO.mechanisnModality.id.idNegotiationModalityPk, MO.idMechanismOperationPk ");
		
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("idRole", idRole);
		paramters.put("blockDate", blockDate);
		paramters.put("indicatorOne", ComponentConstant.ONE);
		paramters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		paramters.put("settlementSchema", settlementSchema);
		paramters.put("cashPart", ComponentConstant.CASH_PART);
		paramters.put("termPart", ComponentConstant.TERM_PART);
		paramters.put("assignedState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		paramters.put("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		if (ComponentConstant.SALE_ROLE.equals(idRole)) {
			paramters.put("chainState", ChainedOperationStateType.REGISTERED.getCode());
		}
		if (idMechanism != null) {
			paramters.put("idMechanism", idMechanism);
		}
		if (idModality != null) {
			paramters.put("idModality", idModality);
		}
		
		lstObjects = findListByQueryString(stringBuilderSql.toString(), paramters);
		
		return lstObjects;
	}
	
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> lstOperationsSelid(){
	//Primero extraemos el valor de la operacion de negociacion
			StringBuilder querySql = new StringBuilder();
			
			querySql.append("   SELECT MO.ID_SECURITY_CODE_FK,MO.ID_MECHANISM_OPERATION_PK 							                ");
			querySql.append("   FROM SETTLEMENT_DATE_OPERATION SDO                                                                  ");
			querySql.append("   INNER JOIN SETTLEMENT_REQUEST SR on SDO.ID_SETTLEMENT_REQUEST_FK=SR.ID_SETTLEMENT_REQUEST_PK        ");
			querySql.append("   INNER JOIN SETTLEMENT_OPERATION SO on SDO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK  ");
			querySql.append("   INNER JOIN MECHANISM_OPERATION MO on SO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK      ");
			querySql.append("   WHERE 1 = 1                                                                                         ");
			querySql.append("   AND SR.REQUEST_TYPE = 2018                                                                          ");
//			querySql.append("   AND SR.ID_SETTLEMENT_REQUEST_PK = :idSettlementRequestPk                                            ");
			
			Query querySec = em.createNativeQuery(querySql.toString());
			//querySec.setParameter("idSettlementRequestPk", idSettlementRequestPk);
			//TODO Debe ser una lista Xq una solicitud puede tener varias operaciones
			List<Object[]>  result = (List<Object[]>)querySec.getResultList();
	
		return result;	
	}
	
	
	
	/**
	 * Extraemos la fecha para el bloqueo de Saldo SELID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBlockDateSelidOperation(){
	
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("   SELECT SDO.SETTLEMENT_DATE, MO.ID_MECHANISM_OPERATION_PK                                                                               ");
		querySql.append("   FROM SETTLEMENT_DATE_OPERATION SDO                                                                       ");
		querySql.append("   INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK           ");
		querySql.append("   INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK     ");
		querySql.append("   INNER JOIN MECHANISM_OPERATION MO ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK         ");
		querySql.append("   WHERE 1 = 1                                                                                              ");
		querySql.append("   AND SR.REQUEST_TYPE = 2018                                                                               ");
		querySql.append("   AND SR.REQUEST_STATE = 1543                                                                              ");
		querySql.append("   AND TRUNC(SR.REGISTER_DATE) = CASE WHEN SO.OPERATION_STATE = 614  THEN MO.CASH_SETTLEMENT_DATE           ");
		querySql.append("                                      WHEN SO.OPERATION_STATE = 615  THEN MO.TERM_SETTLEMENT_DATE END       ");
		querySql.append("   AND (   SO.OPERATION_PART = 1 and SO.OPERATION_STATE = 614                ");
		querySql.append("        or SO.OPERATION_PART = 2 and SO.OPERATION_STATE = 615              ");
		querySql.append("    )                                                                                                       ");
		//querySql.append("   AND ROWNUM = 1                                                                                           ");
		
		Query querySec = em.createNativeQuery(querySql.toString());
		//querySec.setParameter("idSettlementRequestPk", idSettlementRequestPk);
		
		List<Object[]>  result = (List<Object[]>)querySec.getResultList();
		
		return result;
			
	}
	
	/**
	 * Extraemos la fecha para el bloqueo de Saldo SELID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public void blockSalesHMHSelidOperation(String getUserName, Integer getIdPrivilegeOfSystem, Date getAuditTime, String getIpAddress ){
		//Primero extraemos el valor de la operacion de negociacion
		StringBuilder querySql = new StringBuilder();
				
		querySql.append("   SELECT MO.ID_SECURITY_CODE_FK,MO.ID_MECHANISM_OPERATION_PK                                               ");
		querySql.append("   FROM SETTLEMENT_DATE_OPERATION SDO                                                                       ");
		querySql.append("   INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK           ");
		querySql.append("   INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK     ");
		querySql.append("   INNER JOIN MECHANISM_OPERATION MO ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK         ");
		querySql.append("   WHERE 1 = 1                                                                                              ");
		querySql.append("   AND SR.REQUEST_TYPE = 2018                                                                               ");
		querySql.append("   AND SR.REQUEST_STATE = 1543                                                                              ");
		querySql.append("   AND SO.IND_EXTENDED = 1                                                                                  ");
		querySql.append("   AND SO.IND_UNFULFILLED <> 1                                                                              ");
		querySql.append("   AND (   SO.OPERATION_PART = 1 and SO.OPERATION_STATE = 614                                               ");
		querySql.append("        or SO.OPERATION_PART = 2 and SO.OPERATION_STATE = 615 )                                             ");
		querySql.append("   ORDER BY 1 DESC                                                                                          ");
				
		Query querySec = em.createNativeQuery(querySql.toString());
		//querySec.setParameter("idSettlementRequestPk", idSettlementRequestPk);
		
		List<Object[]>  resultList = (List<Object[]>)querySec.getResultList();
		
		//Si el resultado el diferente de nulo
		if( Validations.validateIsNotNullAndNotEmpty(resultList)){
			
			//recorremos las operaciones existentes
			for (Object[] result : resultList) {
	
				//extraccion de variables
				String security = (String)result[0];
				BigDecimal idMechanismOperationPK = (BigDecimal)result[1];
				
				//Extraemos informacion para luego realizar el update
				StringBuilder querySqlSao = new StringBuilder();
	
				querySqlSao.append("  SELECT                                                                                                                  ");
				querySqlSao.append("      sao.ID_SETTLEMENT_ACCOUNT_PK,                                                                                       ");                                                                                   
				querySqlSao.append("      SAO.STOCK_REFERENCE, 																								  ");
				querySqlSao.append("      HAO.ROLE,                                                                                                           ");
				querySqlSao.append("      HAO.ID_INCHARGE_STOCK_PARTICIPANT,                                                                                  ");
				querySqlSao.append("      HAO.ID_HOLDER_ACCOUNT_FK,                                                                                           ");
				querySqlSao.append("      SAM.MARKET_DATE,                                                                                                    ");
				querySqlSao.append("      SAM.MARKET_RATE,                                                                                                    ");
				querySqlSao.append("      SAM.MARKET_QUANTITY                                                                                                 ");
				querySqlSao.append("  FROM MECHANISM_OPERATION MO                                                                                             ");
				querySqlSao.append("  INNER JOIN SETTLEMENT_OPERATION SO on SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                       ");
				querySqlSao.append("  INNER JOIN HOLDER_ACCOUNT_OPERATION HAO on HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                 ");
				querySqlSao.append("  INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO on SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK  ");
				querySqlSao.append("  INNER JOIN SETTLEMENT_ACCOUNT_MARKETFACT SAM ON SAO.ID_SETTLEMENT_ACCOUNT_PK = SAM.ID_SETTLEMENT_ACCOUNT_FK             ");
				querySqlSao.append("  WHERE 1 = 1                                                                                                             ");
				querySqlSao.append("  AND MO.ID_MECHANISM_OPERATION_PK =:idMechanismOperationPK 					                                          ");
				querySqlSao.append("  AND SO.OPERATION_PART = 1                                                                                               ");
				querySqlSao.append("  AND HAO.OPERATION_PART = 1                                                                                              ");
				querySqlSao.append("  AND MO.OPERATION_STATE = 614                                                                                            ");
				querySqlSao.append("  AND HAO.HOLDER_ACCOUNT_STATE = 625                                                                                      ");
				querySqlSao.append("  AND SAO.OPERATION_STATE = 625                                                                                           ");
				//querySqlSao.append("  and SAM.IND_ACTIVE = 0                                                                                                  ");
				//querySqlSao.append("  AND TRUNC(SAM.LAST_MODIFY_DATE) =                                                                         ");
				
				querySqlSao.append("   AND (SAM.IND_ACTIVE = 0                                                         ");
				querySqlSao.append("        AND HAO.ROLE = 2                                                           ");
				querySqlSao.append("        AND TRUNC(SAM.LAST_MODIFY_DATE) = :dateOperation                             ");
				querySqlSao.append("                                                                                   ");
				querySqlSao.append("      OR (SAM.IND_ACTIVE = 1                                                       ");
				querySqlSao.append("          AND HAO.ROLE = 1 )                                                       ");
				querySqlSao.append("          )                                                                        ");
				
							
				Query querySAO = em.createNativeQuery(querySqlSao.toString());
				querySAO.setParameter("idMechanismOperationPK", idMechanismOperationPK.longValue());
				querySAO.setParameter("dateOperation", CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()));
				
				List<Object[]>  lstUpdate = (List<Object[]>) querySAO.getResultList();
	
				if(lstUpdate.size() > GeneralConstants.ZERO_VALUE_INTEGER){
					
					for(Object[] objectUpdate : lstUpdate){
						
						BigDecimal idSAOperation = (BigDecimal)objectUpdate[0];
						BigDecimal stockReference = (BigDecimal)objectUpdate[1];
						BigDecimal role = (BigDecimal)objectUpdate[2];
						BigDecimal idBuyerParticipant = (BigDecimal)objectUpdate[3]; 
						BigDecimal idHolderAccount = (BigDecimal)objectUpdate[4]; 
						Date marketDate = (Date)objectUpdate[5];
						BigDecimal marketRate = (BigDecimal)objectUpdate[6]; 
						BigDecimal marketQuantity = (BigDecimal)objectUpdate[7]; 
						
						//revertimos saldos dependiendo del rol del participante en la operacion
						if (Integer.valueOf(role.toString()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
							
							//rol compra, si el indicador de bloqueo esta correcto
							
							if(Validations.validateIsNotNullAndNotEmpty(stockReference)){
								
								if(Long.valueOf(stockReference.toString()).equals(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode())){
									
									//purchaseBalance HMB
									StringBuilder stringBufferPurchaseHMB = new StringBuilder();
									
									stringBufferPurchaseHMB.append(" UPDATE HOLDER_MARKETFACT_HISTORY set  ");
									stringBufferPurchaseHMB.append("   PURCHASE_BALANCE = PURCHASE_BALANCE + :marketQuantity ");
									stringBufferPurchaseHMB.append(" , LAST_MODIFY_APP = :lastModifyApp ");
									stringBufferPurchaseHMB.append(" , LAST_MODIFY_DATE = :lastModifyDate ");
									stringBufferPurchaseHMB.append(" , LAST_MODIFY_IP = :lastModifyIp ");
									stringBufferPurchaseHMB.append(" , LAST_MODIFY_USER = :lastModifyUser ");
									stringBufferPurchaseHMB.append(" WHERE 1 = 1");
									stringBufferPurchaseHMB.append(" and ID_SECURITY_CODE_FK = :idSecurity ");
									stringBufferPurchaseHMB.append(" and ID_PARTICIPANT_FK = :idParticipant ");
									stringBufferPurchaseHMB.append(" and ID_HOLDER_ACCOUNT_FK = :idHolderAccount ");
									stringBufferPurchaseHMB.append(" and MARKET_RATE = :marketRate  ");
									stringBufferPurchaseHMB.append(" and MARKET_DATE = :marketDate  ");
									stringBufferPurchaseHMB.append(" and TRUNC(PROCESS_DATE) = :processDate  ");
									
									Query queryPurchaseHMB = em.createNativeQuery(stringBufferPurchaseHMB.toString());
									
									queryPurchaseHMB.setParameter("idSecurity", security);
									queryPurchaseHMB.setParameter("idParticipant", Long.parseLong(idBuyerParticipant.toString()));
									queryPurchaseHMB.setParameter("idHolderAccount", Long.parseLong(idHolderAccount.toString()));
									queryPurchaseHMB.setParameter("marketQuantity", marketQuantity);
									queryPurchaseHMB.setParameter("marketRate", marketRate);
									queryPurchaseHMB.setParameter("marketDate", marketDate);
									queryPurchaseHMB.setParameter("lastModifyApp", getIdPrivilegeOfSystem);
									queryPurchaseHMB.setParameter("lastModifyDate", getAuditTime);
									queryPurchaseHMB.setParameter("lastModifyIp", getIpAddress);
									queryPurchaseHMB.setParameter("lastModifyUser", getUserName);
									queryPurchaseHMB.setParameter("processDate", CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()));
									
									queryPurchaseHMB.executeUpdate();	
								}
							}
							
						}else{
							//rol venta
							if(Integer.valueOf(role.toString()).equals(GeneralConstants.TWO_VALUE_INTEGER)){
								
								if(Validations.validateIsNotNullAndNotEmpty(stockReference)){
									
									if(Long.valueOf(stockReference.toString()).equals(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode())){
										
										//saleBalance HMB
										StringBuilder stringBufferSaleHMB = new StringBuilder();					
										
										stringBufferSaleHMB.append(" UPDATE HOLDER_MARKETFACT_HISTORY set  ");
										stringBufferSaleHMB.append("   AVAILABLE_BALANCE = AVAILABLE_BALANCE - :marketQuantity ");
										stringBufferSaleHMB.append(" , SALE_BALANCE = SALE_BALANCE + :marketQuantity ");
										stringBufferSaleHMB.append(" , LAST_MODIFY_APP = :lastModifyApp ");
										stringBufferSaleHMB.append(" , LAST_MODIFY_DATE = :lastModifyDate ");
										stringBufferSaleHMB.append(" , LAST_MODIFY_IP = :lastModifyIp ");
										stringBufferSaleHMB.append(" , LAST_MODIFY_USER = :lastModifyUser ");
										stringBufferSaleHMB.append(" WHERE 1 = 1");
										stringBufferSaleHMB.append(" and ID_SECURITY_CODE_FK = :idSecurity ");
										stringBufferSaleHMB.append(" and ID_PARTICIPANT_FK = :idParticipant ");
										stringBufferSaleHMB.append(" and ID_HOLDER_ACCOUNT_FK = :idHolderAccount ");
										stringBufferSaleHMB.append(" and MARKET_RATE = :marketRate  ");
										stringBufferSaleHMB.append(" and MARKET_DATE = :marketDate  ");
										stringBufferSaleHMB.append(" and TRUNC(PROCESS_DATE) = :processDate  ");
										
										Query querySaleHMB = em.createNativeQuery(stringBufferSaleHMB.toString());
										
										querySaleHMB.setParameter("idSecurity", security);
										querySaleHMB.setParameter("idParticipant", Long.parseLong(idBuyerParticipant.toString()));
										querySaleHMB.setParameter("idHolderAccount", Long.parseLong(idHolderAccount.toString()));
										querySaleHMB.setParameter("marketQuantity", marketQuantity);
										querySaleHMB.setParameter("marketRate", marketRate);
										querySaleHMB.setParameter("marketDate", marketDate);
										querySaleHMB.setParameter("lastModifyApp", getIdPrivilegeOfSystem);
										querySaleHMB.setParameter("lastModifyDate", getAuditTime);
										querySaleHMB.setParameter("lastModifyIp", getIpAddress);
										querySaleHMB.setParameter("lastModifyUser", getUserName);
										querySaleHMB.setParameter("processDate", CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()));
										
										querySaleHMB.executeUpdate();
									}
								}
							}
						}
					}
				}	
			}
		}
	}
	
	/**
	 * Gets the list settlement operation to net settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @return the list settlement operation to net settlement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSettlementOperationToNetSettlement(Long idSettlementProcess) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT MO.idMechanismOperationPk, "); // 0
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 2
		stringBuffer.append(" MO.indReportingBalance, "); //3
		stringBuffer.append(" MO.indPrincipalGuarantee, "); //4
		stringBuffer.append(" MO.indMarginGuarantee, "); //5
		stringBuffer.append(" MO.indTermSettlement, "); //6
		stringBuffer.append(" MO.indCashStockBlock, "); //7
		stringBuffer.append(" MO.indTermStockBlock, "); //8
		stringBuffer.append(" MO.indPrimaryPlacement, "); //9
		stringBuffer.append(" MO.stockQuantity, "); // 10
		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 11
		stringBuffer.append(" SO.settlementType, "); //FOP, DVP, DVD  12
		stringBuffer.append(" MO.referenceOperation.idMechanismOperationPk, "); // 13
		stringBuffer.append(" MO.securities.instrumentType, "); //14
		stringBuffer.append(" mo.securities.currentNominalValue, "); //15
		stringBuffer.append(" HAO.idHolderAccountOperationPk, "); // 16
		stringBuffer.append(" HAO.inchargeStockParticipant.idParticipantPk, "); // 17
		stringBuffer.append(" HAO.holderAccount.idHolderAccountPk, "); // 18
		stringBuffer.append(" HAO.role, "); // 19
		stringBuffer.append(" HAO.operationPart, "); // 20
		stringBuffer.append(" HAO.stockQuantity, "); // 21
		stringBuffer.append(" HAO.refAccountOperation.idHolderAccountOperationPk, "); // 22
		stringBuffer.append(" SO.idSettlementOperationPk, "); //23
		stringBuffer.append(" SO.stockQuantity, "); //24
		stringBuffer.append(" SO.settlementAmount, "); // 25
		stringBuffer.append(" SAO.idSettlementAccountPk, "); //26
		stringBuffer.append(" SAO.stockQuantity, "); //27
		stringBuffer.append(" SAO.settlementAccountOperation.idSettlementAccountPk, "); //28
		stringBuffer.append(" SAO.chainedQuantity, "); // 29
		stringBuffer.append(" SO.indPartial, "); // 30
		stringBuffer.append(" SO.indDematerialization, "); // 31
		stringBuffer.append(" SO.indForcedPurchase, "); // 32
		stringBuffer.append(" MO.mechanisnModality.interfaceTransferCode, "); //33
		stringBuffer.append(" MO.securities.securityClass, "); //34
		stringBuffer.append(" MO.operationNumber "); //35
		stringBuffer.append(" FROM MechanismOperation MO, HolderAccountOperation HAO, SettlementAccountOperation SAO, SettlementOperation SO, OperationSettlement OS ");
		stringBuffer.append(" WHERE OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		stringBuffer.append(" and OS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and OS.operationState = :operationSettlementState ");
		stringBuffer.append(" and MO.idMechanismOperationPk = HAO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and SAO.holderAccountOperation.idHolderAccountOperationPk = HAO.idHolderAccountOperationPk ");
		stringBuffer.append(" and SAO.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and SAO.operationState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
		stringBuffer.append(" and MO.operationState in (:lstOperationState) "); // ASSIGNED (CASH PART) - CASH SETTLED (TERM PART)
		stringBuffer.append(" and (SO.indRemoveSettlement != :indicatorOne or SO.indRemoveSettlement = :indicatorOne and SO.indReenterSettlement = :indicatorOne) ");
		stringBuffer.append(" and SO.fundsReference = :idFundsReference "); 
		stringBuffer.append(" and SO.settlementType = :idDVPSettlementType ");
		stringBuffer.append(" ORDER BY MO.mechanisnModality.id.idNegotiationModalityPk, MO.operationDate, MO.operationNumber, SAO.role ");
		
		parameters.put("idSettlementProcess", idSettlementProcess);
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("operationSettlementState", OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode());
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idDVPSettlementType", SettlementType.DVP.getCode());
		parameters.put("idFundsReference", AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
		List<Integer> lstOperationState= new ArrayList<Integer>();
		lstOperationState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		lstOperationState.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		parameters.put("lstOperationState", lstOperationState);
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	
	/**
	 * Gets the list settlement operation to gross settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param settlementDate the settlement date
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param idOperationPart the id operation part
	 * @param idSettlementOperation the id settlement operation
	 * @return the list settlement operation to gross settlement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSettlementOperationToGrossSettlement(Long idSettlementProcess, Date settlementDate, Long idMechanism, Long idModalityGroup, 
			Integer idCurrency, Integer idOperationPart, Long idSettlementOperation, Boolean excludeFOP) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT MO.idMechanismOperationPk, "); // 0
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 2
		stringBuffer.append(" MO.indReportingBalance, "); //3
		stringBuffer.append(" MO.indPrincipalGuarantee, "); //4
		stringBuffer.append(" MO.indMarginGuarantee, "); //5
		stringBuffer.append(" MO.indTermSettlement, "); //6
		stringBuffer.append(" MO.indCashStockBlock, "); //7
		stringBuffer.append(" MO.indTermStockBlock, "); //8
		stringBuffer.append(" MO.indPrimaryPlacement, "); //9
		stringBuffer.append(" MO.stockQuantity, "); // 10
		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 11
		stringBuffer.append(" SO.settlementType, "); //FOP, DVP, DVD  12
		stringBuffer.append(" MO.referenceOperation.idMechanismOperationPk, "); // 13
		stringBuffer.append(" MO.securities.instrumentType, "); //14
		stringBuffer.append(" mo.securities.currentNominalValue, "); //15
		stringBuffer.append(" HAO.idHolderAccountOperationPk, "); // 16
		stringBuffer.append(" HAO.inchargeStockParticipant.idParticipantPk, "); // 17
		stringBuffer.append(" HAO.holderAccount.idHolderAccountPk, "); // 18
		stringBuffer.append(" HAO.role, "); // 19
		stringBuffer.append(" HAO.operationPart, "); // 20
		stringBuffer.append(" HAO.stockQuantity, "); // 21
		stringBuffer.append(" HAO.refAccountOperation.idHolderAccountOperationPk, "); // 22
		stringBuffer.append(" SO.idSettlementOperationPk, "); //23
		stringBuffer.append(" SO.stockQuantity, "); //24
		stringBuffer.append(" SO.settlementAmount, "); // 25
		stringBuffer.append(" SAO.idSettlementAccountPk, "); //26
		stringBuffer.append(" SAO.stockQuantity, "); //27
		stringBuffer.append(" SAO.settlementAccountOperation.idSettlementAccountPk, "); //28
		stringBuffer.append(" SAO.chainedQuantity, "); // 29
		stringBuffer.append(" SO.indPartial, "); // 30
		stringBuffer.append(" SO.indDematerialization, "); // 31
		stringBuffer.append(" SO.indForcedPurchase, "); // 32
		stringBuffer.append(" MO.mechanisnModality.interfaceTransferCode, "); //33
		stringBuffer.append(" MO.securities.securityClass, "); //34
		stringBuffer.append(" MO.operationNumber "); //35
		stringBuffer.append(" FROM MechanismOperation MO, HolderAccountOperation HAO, SettlementAccountOperation SAO, SettlementOperation SO, ModalityGroupDetail MGD ");
		stringBuffer.append(" WHERE MO.idMechanismOperationPk = HAO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and MGD.negotiationMechanism.idNegotiationMechanismPk = MO.mechanisnModality.id.idNegotiationMechanismPk ");
		stringBuffer.append(" and MGD.negotiationModality.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
		stringBuffer.append(" and MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and SAO.holderAccountOperation.idHolderAccountOperationPk = HAO.idHolderAccountOperationPk ");
		stringBuffer.append(" and SAO.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = SO.settlementSchema ");
		stringBuffer.append(" and MO.mechanisnModality.id.idNegotiationMechanismPk= :idMechanism ");
		stringBuffer.append(" and SO.operationState = :idOperationState "); // ASSIGNED (CASH PART) - CASH SETTLED (TERM PART)
		stringBuffer.append(" and SO.operationPart = :idOperationPart ");
		stringBuffer.append(" and SAO.operationState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
		stringBuffer.append(" and (SO.indRemoveSettlement != :indicatorOne or SO.indRemoveSettlement = :indicatorOne and SO.indReenterSettlement = :indicatorOne) "); 
		stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		stringBuffer.append(" and SO.settlementDate = :settlementDate  ");
		stringBuffer.append(" and SO.settlementCurrency = :idCurrency ");
		stringBuffer.append(" and SO.stockReference = :idStockReference ");
		stringBuffer.append(" and ( (SO.fundsReference = :idFundsReference and SO.settlementType = :idDVPSettlementType) "); 
		stringBuffer.append("		or (SO.settlementType != :idDVPSettlementType) ) "); //FOP and DVD
		stringBuffer.append(" and SO.settlementSchema = :idSettlementSchema ");
		
		if(Validations.validateIsNotNull(excludeFOP) && excludeFOP) {
			stringBuffer.append("and SO.settlementType != :idFOPSettlementType");
		}
		
		if (ComponentConstant.CASH_PART.equals(idOperationPart))
		{
			stringBuffer.append(" and ( (MO.indMarginGuarantee = :indicatorOne and MO.marginReference = :idMarginReference) ");
			stringBuffer.append("		or (MO.indMarginGuarantee != :indicatorOne) )");
			
			parameters.put("idOperationState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
			parameters.put("idMarginReference", AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
		}
		else if (ComponentConstant.TERM_PART.equals(idOperationPart))
		{
			parameters.put("idOperationState", MechanismOperationStateType.CASH_SETTLED.getCode());
		}
		
		if(idSettlementOperation!=null){
			stringBuffer.append(" and SO.idSettlementOperationPk = :idSettlementOperation  ");
			parameters.put("idSettlementOperation", idSettlementOperation);
		}
		
		stringBuffer.append(" ORDER BY MO.mechanisnModality.id.idNegotiationModalityPk, MO.operationDate, MO.operationNumber, SAO.role ");
		
		
		parameters.put("idSettlementSchema", SettlementSchemaType.GROSS.getCode());
		parameters.put("idMechanism", idMechanism);
		parameters.put("idModalityGroup", idModalityGroup);
		
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("idCurrency", idCurrency);
		parameters.put("idOperationPart", idOperationPart);
		parameters.put("settlementDate", settlementDate);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idDVPSettlementType", SettlementType.DVP.getCode());
		if(Validations.validateIsNotNull(excludeFOP) && excludeFOP) {
			parameters.put("idFOPSettlementType", SettlementType.FOP.getCode());
		}
		parameters.put("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		parameters.put("idFundsReference", AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	/**
	 * 
	 * @param idSettlementProcess
	 * @param settlementDate
	 * @param idMechanism
	 * @param idModalityGroup
	 * @param idCurrency
	 * @param idOperationPart
	 * @param idSettlementOperation
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSettlementOperationToGrossSettlementUnfulfillment(Long idSettlementProcess, Date settlementDate, Long idMechanism, Long idModalityGroup, 
			Integer idCurrency, Integer idOperationPart, Long idSettlementOperation, Long idMechanismOperationPk) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		//StringBuffer stringBuffer = new StringBuffer();
		StringBuilder stringBuffer = new StringBuilder();
//		stringBuffer.append(" SELECT distinct MO.idMechanismOperationPk, "); // 0
//		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
//		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 2
//		stringBuffer.append(" MO.indReportingBalance, "); //3
//		stringBuffer.append(" MO.indPrincipalGuarantee, "); //4
//		stringBuffer.append(" MO.indMarginGuarantee, "); //5
//		stringBuffer.append(" MO.indTermSettlement, "); //6
//		stringBuffer.append(" MO.indCashStockBlock, "); //7
//		stringBuffer.append(" MO.indTermStockBlock, "); //8
//		stringBuffer.append(" MO.indPrimaryPlacement, "); //9
//		stringBuffer.append(" MO.stockQuantity, "); // 10
//		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 11
//		stringBuffer.append(" SO.settlementType, "); //FOP, DVP, DVD  12
//		stringBuffer.append(" MO.referenceOperation.idMechanismOperationPk, "); // 13
//		stringBuffer.append(" MO.securities.instrumentType, "); //14
//		stringBuffer.append(" mo.securities.currentNominalValue, "); //15
//		stringBuffer.append(" HAO.idHolderAccountOperationPk, "); // 16
//		stringBuffer.append(" HAO.inchargeStockParticipant.idParticipantPk, "); // 17
//		stringBuffer.append(" HAO.holderAccount.idHolderAccountPk, "); // 18
//		stringBuffer.append(" HAO.role, "); // 19
//		stringBuffer.append(" HAO.operationPart, "); // 20
//		stringBuffer.append(" HAO.stockQuantity, "); // 21
//		stringBuffer.append(" HAO.refAccountOperation.idHolderAccountOperationPk, "); // 22
//		stringBuffer.append(" SO.idSettlementOperationPk, "); //23
//		stringBuffer.append(" SO.stockQuantity, "); //24
//		stringBuffer.append(" SO.settlementAmount, "); // 25
//		stringBuffer.append(" SAO.idSettlementAccountPk, "); //26
//		stringBuffer.append(" SAO.stockQuantity, "); //27
//		stringBuffer.append(" SAO.settlementAccountOperation.idSettlementAccountPk, "); //28
//		stringBuffer.append(" SAO.chainedQuantity, "); // 29
//		stringBuffer.append(" SO.indPartial, "); // 30
//		stringBuffer.append(" SO.indDematerialization, "); // 31
//		stringBuffer.append(" SO.indForcedPurchase, "); // 32
//		stringBuffer.append(" MO.mechanisnModality.interfaceTransferCode, "); //33
//		stringBuffer.append(" MO.securities.securityClass, "); //34
//		stringBuffer.append(" MO.operationNumber "); //35
//		stringBuffer.append(" FROM MechanismOperation MO, HolderAccountOperation HAO, SettlementAccountOperation SAO, SettlementOperation SO, ModalityGroupDetail MGD ");
//		stringBuffer.append(" ,OperationUnfulfillment OU, SettlementUnfulfillment SU");
//		stringBuffer.append(" WHERE MO.idMechanismOperationPk = HAO.mechanismOperation.idMechanismOperationPk ");
//		
//		stringBuffer.append(" AND SO.idSettlementOperationPk = OU.settlementOperation.idSettlementOperationPk ");
//		stringBuffer.append(" AND SU.operationUnfulfillment.idOperationUnfulfillmentPk = OU.idOperationUnfulfillmentPk ");
//		
//		stringBuffer.append(" and MGD.negotiationMechanism.idNegotiationMechanismPk = MO.mechanisnModality.id.idNegotiationMechanismPk ");
//		stringBuffer.append(" and MGD.negotiationModality.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
//		stringBuffer.append(" and MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
//		stringBuffer.append(" and SAO.holderAccountOperation.idHolderAccountOperationPk = HAO.idHolderAccountOperationPk ");
//		stringBuffer.append(" and SAO.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
//		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = SO.settlementSchema ");
		
		stringBuffer.append("  select distinct                                                                  ");
		stringBuffer.append("      MO.ID_MECHANISM_OPERATION_PK ,                                               ");
		stringBuffer.append("      MO.ID_NEGOTIATION_MECHANISM_FK,                                              ");
		stringBuffer.append("      MO.ID_NEGOTIATION_MODALITY_FK ,                                              ");
		stringBuffer.append("      MO.IND_REPORTING_BALANCE ,                                                   ");
		stringBuffer.append("      MO.IND_PRINCIPAL_GUARANTEE,                                                  ");
		stringBuffer.append("      MO.IND_MARGIN_GUARANTEE ,                                                    ");
		stringBuffer.append("      MO.IND_TERM_SETTLEMENT ,                                                     ");
		stringBuffer.append("      MO.IND_CASH_STOCK_BLOCK,                                                     ");
		stringBuffer.append("      MO.IND_TERM_STOCK_BLOCK ,                                                    ");
		stringBuffer.append("      MO.IND_PRIMARY_PLACEMENT ,                                                   ");
		stringBuffer.append("      MO.STOCK_QUANTITY AS MOSTOCKQUANTITY,                                                          ");
		stringBuffer.append("      MO.ID_SECURITY_CODE_FK ,                                                     ");
		stringBuffer.append("      SO.SETTLEMENT_TYPE ,                                                         ");
		stringBuffer.append("      MO.ID_REFERENCE_OPERATION_FK ,                                               ");
		stringBuffer.append("      S1.INSTRUMENT_TYPE ,                                                         ");
		stringBuffer.append("      S2.CURRENT_NOMINAL_VALUE ,                                                   ");
		stringBuffer.append("      HAO.ID_HOLDER_ACCOUNT_OPERATION_PK ,                                         ");
		stringBuffer.append("      HAO.ID_INCHARGE_STOCK_PARTICIPANT,                                           ");
		stringBuffer.append("      HAO.ID_HOLDER_ACCOUNT_FK ,                                                   ");
		stringBuffer.append("      HAO.ROLE ,                                                                 ");
		stringBuffer.append("      HAO.OPERATION_PART,                                                          ");
		stringBuffer.append("      HAO.STOCK_QUANTITY AS HAOSTOCKQUANTITY,                                                         ");
		stringBuffer.append("      HAO.ID_REF_ACCOUNT_OPERATION_FK,                                             ");
		stringBuffer.append("      SO.ID_SETTLEMENT_OPERATION_PK,                                               ");
		stringBuffer.append("      SO.STOCK_QUANTITY AS SOSTOCKQUANTITY,                                                          ");
		stringBuffer.append("      SO.SETTLEMENT_AMOUNT ,                                                       ");
		stringBuffer.append("      SAO.ID_SETTLEMENT_ACCOUNT_PK,                                                ");
		stringBuffer.append("      SAO.STOCK_QUANTITY AS SAOSTOCKQUANTITY,                                                         ");
		stringBuffer.append("      SAO.ID_SETTLEMENT_ACCOUNT_FK ,                                               ");
		stringBuffer.append("      SAO.CHAINED_QUANTITY ,                                                       ");
		stringBuffer.append("      SO.IND_PARTIAL ,                                                             ");
		stringBuffer.append("      SO.IND_DEMATERIALIZATION ,                                                   ");
		stringBuffer.append("      SO.IND_FORCED_PURCHASE ,                                                     ");
		stringBuffer.append("      MM.INTERFACE_TRANSFER_CODE ,                                                 ");
		stringBuffer.append("      S1.SECURITY_CLASS ,                                                          ");
		stringBuffer.append("      MO.OPERATION_NUMBER                                                          ");
		stringBuffer.append("  from                                                                             ");
		stringBuffer.append("      MECHANISM_OPERATION MO,                                                      ");
		stringBuffer.append("      SECURITY S1,                                                                 ");
		stringBuffer.append("      SECURITY S2,                                                                 ");
		stringBuffer.append("      MECHANISM_MODALITY MM,                                                       ");
		stringBuffer.append("      HOLDER_ACCOUNT_OPERATION HAO,                                                ");
		stringBuffer.append("      SETTLEMENT_ACCOUNT_OPERATION SAO,                                            ");
		stringBuffer.append("      SETTLEMENT_OPERATION SO,                                                     ");
		stringBuffer.append("      MODALITY_GROUP_DETAIL MGD,                                                   ");
		stringBuffer.append("      OPERATION_UNFULFILLMENT OU,                                                  ");
		stringBuffer.append("      SETTLEMENT_UNFULFILLMENT SU,                                                 ");
		stringBuffer.append("      MODALITY_GROUP MG                                                            ");
		stringBuffer.append("  where                                                                            ");
		stringBuffer.append("      MO.ID_SECURITY_CODE_FK=S1.ID_SECURITY_CODE_PK                                ");
		stringBuffer.append("      and MO.ID_SECURITY_CODE_FK=S2.ID_SECURITY_CODE_PK                            ");
		stringBuffer.append("      and MO.ID_NEGOTIATION_MECHANISM_FK=MM.ID_NEGOTIATION_MECHANISM_PK            ");
		stringBuffer.append("      and MO.ID_NEGOTIATION_MODALITY_FK=MM.ID_NEGOTIATION_MODALITY_PK              ");
		stringBuffer.append("      and MGD.ID_MODALITY_GROUP_FK=MG.ID_MODALITY_GROUP_PK                         ");
		stringBuffer.append("      and MO.ID_MECHANISM_OPERATION_PK=HAO.ID_MECHANISM_OPERATION_FK               ");
		stringBuffer.append("      and SO.ID_SETTLEMENT_OPERATION_PK=OU.ID_SETTLEMENT_OPERATION_FK              ");
		stringBuffer.append("      and SU.ID_OPERATION_UNFULFILLMENT_FK=OU.ID_OPERATION_UNFULFILLMENT_PK        ");
		stringBuffer.append("      and MGD.ID_NEGOTIATION_MECHANISM_FK=MO.ID_NEGOTIATION_MECHANISM_FK           ");
		stringBuffer.append("      and MGD.ID_NEGOTIATION_MODALITY_FK=MO.ID_NEGOTIATION_MODALITY_FK             ");
		stringBuffer.append("      and MO.ID_MECHANISM_OPERATION_PK=SO.ID_MECHANISM_OPERATION_FK                ");
		stringBuffer.append("      and SAO.ID_HOLDER_ACCOUNT_OPERATION_FK=HAO.ID_HOLDER_ACCOUNT_OPERATION_PK    ");
		stringBuffer.append("      and SAO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK             ");
		stringBuffer.append("      and MG.SETTLEMENT_SCHEMA=SO.SETTLEMENT_SCHEMA                                ");
		stringBuffer.append("      and MO.ID_NEGOTIATION_MECHANISM_FK=:idMechanism                                         ");
		stringBuffer.append("      and SO.OPERATION_STATE=1849                                                  ");
		stringBuffer.append("      and SO.OPERATION_PART= :idOperationPart                                                      ");
		stringBuffer.append("      and SAO.OPERATION_STATE=625                                                  ");                                              
		stringBuffer.append("      and (                                                                        ");
		stringBuffer.append("          SO.IND_REMOVE_SETTLEMENT<>1                                              ");
		stringBuffer.append("          or SO.IND_REMOVE_SETTLEMENT=1                                            ");
		stringBuffer.append("          and SO.IND_REENTER_SETTLEMENT=1                                          ");
		stringBuffer.append("      )                                                                            ");
		stringBuffer.append("      and MGD.ID_MODALITY_GROUP_FK=:idModalityGroup                                               ");
		stringBuffer.append("      and SO.SETTLEMENT_CURRENCY=:idCurrency                                               ");
		stringBuffer.append("      and SO.STOCK_REFERENCE=:idStockReference                                                  ");
		stringBuffer.append("      and SO.SETTLEMENT_SCHEMA=:idSettlementSchema                                                   ");
		stringBuffer.append("      and MO.ID_MECHANISM_OPERATION_PK=:idMechanismOperationPk                                      ");
		stringBuffer.append("  order by                                                                 		");
		stringBuffer.append("      MO.ID_NEGOTIATION_MODALITY_FK,                                               ");                                               
		stringBuffer.append("      MO.OPERATION_NUMBER,                                                         ");
		stringBuffer.append("      HAO.ROLE                                                                     ");
				
//		stringBuffer.append(" and MO.mechanisnModality.id.idNegotiationMechanismPk= :idMechanism ");
//		stringBuffer.append(" and SO.operationState = :idOperationState "); // ASSIGNED (CASH PART) - CASH SETTLED (TERM PART)
//		stringBuffer.append(" and SO.operationPart = :idOperationPart ");
		//stringBuffer.append(" and SAO.operationState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
//		stringBuffer.append(" and (SO.indRemoveSettlement != :indicatorOne or SO.indRemoveSettlement = :indicatorOne and SO.indReenterSettlement = :indicatorOne) "); 
//		stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		//stringBuffer.append(" and SO.settlementDate = :settlementDate  ");
//		stringBuffer.append(" and SO.settlementCurrency = :idCurrency ");
//		stringBuffer.append(" and SO.stockReference = :idStockReference ");
		//stringBuffer.append(" and ( (SO.fundsReference = :idFundsReference and SO.settlementType = :idDVPSettlementType) "); 
		//stringBuffer.append("		or (SO.settlementType != :idDVPSettlementType) ) "); //FOP and DVD
//		stringBuffer.append(" and SO.settlementSchema = :idSettlementSchema ");
//		stringBuffer.append(" and MO.idMechanismOperationPk = :idMechanismOperationPk ");

//		parameters.put("idOperationState", MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
//		parameters.put("idMechanismOperationPk", idMechanismOperationPk);

//		if(idSettlementOperation!=null){
//			stringBuffer.append(" and SO.idSettlementOperationPk = :idSettlementOperation  ");
//			parameters.put("idSettlementOperation", idSettlementOperation);
//		}

////		stringBuffer.append(" ORDER BY MO.mechanisnModality.id.idNegotiationModalityPk, MO.operationDate, MO.operationNumber, SAO.role ");
//		parameters.put("idSettlementSchema", SettlementSchemaType.NET.getCode());
//		parameters.put("idMechanism", idMechanism);
//		parameters.put("idModalityGroup", idModalityGroup);
//		//parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
//		parameters.put("idCurrency", idCurrency);
//		parameters.put("idOperationPart", idOperationPart);
//		//parameters.put("settlementDate", settlementDate);
////		parameters.put("indicatorOne", ComponentConstant.ONE);
//		//parameters.put("idDVPSettlementType", SettlementType.DVP.getCode());
//		parameters.put("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
//		//parameters.put("idFundsReference", AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
		
//		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		Query query = em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("idSettlementSchema", SettlementSchemaType.NET.getCode());
		query.setParameter("idMechanism", idMechanism);
		query.setParameter("idModalityGroup", idModalityGroup);
		query.setParameter("idCurrency", idCurrency);
		query.setParameter("idOperationPart", idOperationPart);
		query.setParameter("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		
		lstObjects = query.getResultList();
		
		return lstObjects;
	}
	
	
	
	/**
	 * Method to get the list of holder accounts from swap operations. This according the settlement date
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSwapOperationToGrossSettlement(Long idMechanismOperation) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT MO.idMechanismOperationPk, "); // ID Mechanism operation
		stringBuffer.append(" HAO.idHolderAccountOperationPk, "); // ID Holder account operation
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // ID mechanism
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // ID modality
		stringBuffer.append(" HAO.inchargeStockParticipant.idParticipantPk, "); // ID participant stock in charge
		stringBuffer.append(" HAO.holderAccount.idHolderAccountPk, "); //ID holder account
		stringBuffer.append(" HAO.role, "); //ID Role holder account
		stringBuffer.append(" HAO.operationPart, "); //ID operation part
		stringBuffer.append(" HAO.stockQuantity, "); // stock quantity from holder account operation
		stringBuffer.append(" SO.securities.idSecurityCodePk, "); // ID ISIN code
		stringBuffer.append(" MO.stockQuantity, "); //stock quantity from mechanism operation
		stringBuffer.append(" MO.settlementType, "); //FOP, DVP, DVD 
		stringBuffer.append(" SO.cashAmount, ");
		stringBuffer.append(" MO.referenceOperation.idMechanismOperationPk, "); //ID referenced operation
		stringBuffer.append(" HAO.refAccountOperation.idHolderAccountOperationPk, ");
//		stringBuffer.append(" MO.programInterestCoupon.idProgramInterestPk, "); //ID program interest coupon
		stringBuffer.append(" 0, "); //ID program interest coupon
		stringBuffer.append(" SO.idSwapOperationPk, "); // ID SwapOperation
		stringBuffer.append(" MO.securities.instrumentType, "); //17
		stringBuffer.append(" mo.indReportingBalance, "); //18
		stringBuffer.append(" mo.indPrincipalGuarantee, "); //19
		stringBuffer.append(" mo.indMarginGuarantee, "); //20
		stringBuffer.append(" mo.indTermSettlement, "); //21
		stringBuffer.append(" mo.indCashStockBlock, "); //22
		stringBuffer.append(" mo.indTermStockBlock, "); //23
		stringBuffer.append(" mo.indPrimaryPlacement, "); //24
		stringBuffer.append(" mo.securities.currentNominalValue "); //25
		stringBuffer.append(" FROM MechanismOperation MO, HolderAccountOperation HAO, SwapOperation SO ");
		stringBuffer.append(" WHERE MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and HAO.swapOperation.idSwapOperationPk = SO.idSwapOperationPk ");
		stringBuffer.append(" and HAO.holderAccountState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
		stringBuffer.append(" and MO.idMechanismOperationPk = :idMechanismOperation");
		stringBuffer.append(" ORDER BY HAO.role ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("idMechanismOperation", idMechanismOperation);
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	
	/**
	 * Gets the settlement process.
	 *
	 * @param idModalityGroup the id modality group
	 * @param settlementDate the settlement date
	 * @param settlementSchema the settlement schema
	 * @param idCurrency the id currency
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess getSettlementProcess(Long idModalityGroup, Date settlementDate, Integer settlementSchema, Integer idCurrency) throws ServiceException
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SP ");
		stringBuffer.append(" FROM SettlementProcess SP ");
		stringBuffer.append(" WHERE SP.modalityGroup.idModalityGroupPk= :idModalityGroup ");
		stringBuffer.append(" and SP.currency = :idCurrency ");
		stringBuffer.append(" and SP.settlementSchema = :settlementSchema ");
		stringBuffer.append(" and SP.settlementDate = :settlementDate ");
		
		TypedQuery<SettlementProcess> typedQuery = em.createQuery(stringBuffer.toString(), SettlementProcess.class);
		typedQuery.setParameter("idModalityGroup", idModalityGroup);
		typedQuery.setParameter("idCurrency", idCurrency);
		typedQuery.setParameter("settlementSchema", settlementSchema);
		typedQuery.setParameter("settlementDate", settlementDate);
		
		try{
			return (SettlementProcess)typedQuery.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the list settlement process.
	 *
	 * @param idModalityGroup the id modality group
	 * @param settlementDate the settlement date
	 * @param settlementSchema the settlement schema
	 * @param idCurrency the id currency
	 * @return the list settlement process
	 */
	public List<SettlementProcess> getListSettlementProcess(Long idModalityGroup, Date settlementDate, Integer settlementSchema, Integer idCurrency)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SP ");
		stringBuffer.append(" FROM SettlementProcess SP ");
		stringBuffer.append(" WHERE SP.modalityGroup.idModalityGroupPk= :idModalityGroup ");
		stringBuffer.append(" and SP.currency = :idCurrency ");
		stringBuffer.append(" and SP.settlementSchema = :settlementSchema ");
		stringBuffer.append(" and SP.settlementDate = :settlementDate ");
		
		TypedQuery<SettlementProcess> typedQuery = em.createQuery(stringBuffer.toString(), SettlementProcess.class);
		typedQuery.setParameter("idModalityGroup", idModalityGroup);
		typedQuery.setParameter("idCurrency", idCurrency);
		typedQuery.setParameter("settlementSchema", settlementSchema);
		typedQuery.setParameter("settlementDate", settlementDate);
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Update settlement process state tx.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idProcessState the id process state
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @param pendingOperations the pending operations
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int updateSettlementProcessStateTx(Long idSettlementProcess, Integer idProcessState, Integer scheduleType, LoggerUser loggerUser, Long pendingOperations) throws ServiceException
	{
		return updateSettlementProcessState(idSettlementProcess, idProcessState, scheduleType, loggerUser, pendingOperations);
	}
	
	/**
	 * Update settlement process state.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idProcessState the id process state
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @param pendingOperations the pending operations
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateSettlementProcessState(Long idSettlementProcess, Integer idProcessState, Integer scheduleType, LoggerUser loggerUser, Long pendingOperations) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementProcess set processState= :idProcessState ");
		if (pendingOperations != null) {
			stringBuffer.append(" , totalOperation = :pendingOperations + settledOperations ");
			stringBuffer.append(" , pendingOperations = :pendingOperations ");
		}
		if(scheduleType != null){
			stringBuffer.append(" , scheduleType = :scheduleType ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementProcessPk = :idSettlementProcess ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idProcessState", idProcessState);
		query.setParameter("idSettlementProcess", idSettlementProcess);
		if (pendingOperations != null) {
			query.setParameter("pendingOperations", pendingOperations);
		}
		if(scheduleType != null){
			query.setParameter("scheduleType", scheduleType);
		}
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	
	/**
	 * Method to get the list of modality group and mechanism from GROSS settlement schema are pending to settle.
	 *
	 * @param settlementSchema the settlement schema
	 * @param settlementDate the settlement date
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListModalityGroups(Integer settlementSchema, Date settlementDate) throws ServiceException
	{
		List<Object[]> lstModalityGroup= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT DISTINCT MGD.modalityGroup.idModalityGroupPk, ");
		stringBuffer.append(" MGD.negotiationMechanism.idNegotiationMechanismPk, ");
		stringBuffer.append(" SO.settlementCurrency ");
		stringBuffer.append(" FROM ModalityGroupDetail MGD, MechanismOperation MO, SettlementOperation SO ");
		stringBuffer.append(" WHERE (    (SO.operationState = :assignedState and SO.operationPart = :cashPart ) ");
		stringBuffer.append("		or   (SO.operationState = :cashSettledState and SO.operationPart = :termPart) ) ");
		stringBuffer.append(" and MGD.negotiationMechanism.idNegotiationMechanismPk = MO.mechanisnModality.id.idNegotiationMechanismPk ");
		stringBuffer.append(" and MGD.negotiationModality.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = SO.settlementSchema ");
		stringBuffer.append(" and SO.settlementSchema = :settlementSchema  ");
		stringBuffer.append(" and SO.settlementDate = :settlementDate ");
		stringBuffer.append(" and MGD.modalityGroup.indAutoSettlement = :indicatorOne");
		stringBuffer.append(" ORDER BY MGD.negotiationMechanism.idNegotiationMechanismPk ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("settlementSchema", settlementSchema);
		parameters.put("settlementDate", settlementDate);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("termPart", ComponentConstant.TERM_PART);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("assignedState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		parameters.put("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		lstModalityGroup = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstModalityGroup;
	}
	
	
	/**
	 * Method to get the list of modalities from modality group and mechanism.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @return List<Long>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListModalitiesFromModalityGroupDetail(Long idMechanism, Long idModalityGroup) throws ServiceException
	{
		List<Long> lstModalities = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT id.idNegotiationModalityPk ");
		stringBuffer.append(" FROM ModalityGroupDetail ");
		stringBuffer.append(" WHERE id.idNegotiationMechanismPk = :idMechanism ");
		stringBuffer.append(" and id.idModalityGroupPk = :idModalityGroup ");
		stringBuffer.append(" and modGroupState = :idIndicatorOne "); //ACTIVE
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanism", idMechanism);
		query.setParameter("idModalityGroup", idModalityGroup);
		query.setParameter("idIndicatorOne", ComponentConstant.ONE);
		
		lstModalities = query.getResultList();
		return lstModalities;
	}
	
	/**
	 * Update mechanism operation state.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationState the operation state
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateMechanismOperationState(Long idMechanismOperation, Integer operationState, LoggerUser loggerUser) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE MechanismOperation set operationState = :idOperationState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idMechanismOperationPk = :idMechanismOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idOperationState", operationState);
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update mechanism operation state.
	 *
	 * @param lstMechanismOperation the lst mechanism operation
	 * @param operationState the operation state
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateMechanismOperationState(List<Long> lstMechanismOperation, Integer operationState, LoggerUser loggerUser) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE MechanismOperation set operationState = :idOperationState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idMechanismOperationPk in (:lstMechanismOperation) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idOperationState", operationState);
		query.setParameter("lstMechanismOperation", lstMechanismOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update settlement operation settled.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param loggerUser the logger user
	 * @param settledQuantity the settled quantity
	 * @return the int
	 */
	public int updateSettlementOperationSettled(Long idSettlementOperation, LoggerUser loggerUser, BigDecimal settledQuantity) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation set ");
		stringBuffer.append(" settledQuantity = settledQuantity + :settledQuantity  ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("settledQuantity", settledQuantity);
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update settlement operation settled.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationPart the operation part
	 * @param loggerUser the logger user
	 * @param settledQuantity the settled quantity
	 * @return the int
	 */
	public int updateSettlementOperationSettled(Long idMechanismOperation, Integer operationPart, LoggerUser loggerUser, BigDecimal settledQuantity) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation set ");
		stringBuffer.append(" settledQuantity = settledQuantity + :settledQuantity  ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and operationPart = :operationPart ");
		stringBuffer.append(" and indPartial = :indicatorOne ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("operationPart", operationPart);
		query.setParameter("indicatorOne", ComponentConstant.ZERO);
		query.setParameter("settledQuantity", settledQuantity);
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update settlement operation state.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param operationState the operation state
	 * @param stockReference the stock reference
	 * @param loggerUser the logger user
	 * @param setRealSettlementDate the set real settlement date
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateSettlementOperationState(Long idSettlementOperation, Integer operationState, 
			Long stockReference, LoggerUser loggerUser, boolean setRealSettlementDate) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation set operationState = :idOperationState ");
		if(stockReference!=null){
			stringBuffer.append(" , stockReference = :idStockReference ");
			if(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(stockReference)){
				stringBuffer.append(" , stockBlockDate = :lastModifyDate ");
			}
		}
		if(setRealSettlementDate){
			stringBuffer.append(" , realSettlementDate = :lastModifyDate ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idOperationState", operationState);
		if(stockReference!=null){
			query.setParameter("idStockReference",stockReference);
		}
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update settlement operation state.
	 *
	 * @param lstSettlementOperation the lst settlement operation
	 * @param operationState the operation state
	 * @param stockReference the stock reference
	 * @param loggerUser the logger user
	 * @param setRealSettlementDate the set real settlement date
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateSettlementOperationState(List<Long> lstSettlementOperation, Integer operationState, 
			Long stockReference, LoggerUser loggerUser, boolean setRealSettlementDate) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation set operationState = :idOperationState ");
		if(stockReference!=null){
			stringBuffer.append(" , stockReference = :idStockReference ");
			if(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(stockReference)){
				stringBuffer.append(" , stockBlockDate = :lastModifyDate ");
			}
		}
		if(setRealSettlementDate){
			stringBuffer.append(" , realSettlementDate = :lastModifyDate ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk in (:lstSettlementOperation) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idOperationState", operationState);
		if(stockReference!=null){
			query.setParameter("idStockReference",stockReference);
		}
		query.setParameter("lstSettlementOperation", lstSettlementOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}

	/**
	 * Update operation settlement type.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param settlementSchema the settlement schema
	 * @param settlementType the settlement type
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateOperationSettlementType(Long idSettlementOperation, Integer settlementSchema, Integer settlementType, LoggerUser loggerUser) throws ServiceException
	{			
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SettlementOperation set  ");
		stringBuffer.append(" settlementType = :settlementType ");
		stringBuffer.append(" , settlementSchema = :settlementSchema ");
		stringBuffer.append(" , indSettlementExchange = :indicatorOne ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("indicatorOne", ComponentConstant.ONE);
		query.setParameter("settlementType", settlementType);
		query.setParameter("settlementSchema", settlementSchema);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}	
	
	/**
	 * Update settlement operation date.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param indExtended the ind extended
	 * @param indPrepaid the ind prepaid
	 * @param settlementDate the settlement date
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateSettlementOperationDate(Long idSettlementOperation, 
			Integer indExtended, Integer indPrepaid, Date settlementDate, LoggerUser loggerUser) throws ServiceException
	{			
		Integer indChangeDate = null;
		
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SettlementOperation set  ");
		if(indExtended!=null){
			indChangeDate = indExtended;
			stringBuffer.append("   indExtended = :indChangeDate ");
		}
		if(indPrepaid!=null){
			indChangeDate = indPrepaid;
			stringBuffer.append("   indPrepaid = :indChangeDate ");
			
		}
		stringBuffer.append(" , settlementDate = :extendedDate ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("indChangeDate", indChangeDate);
		query.setParameter("extendedDate", settlementDate);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}

	/**
	 * Metodo para actualizar los saldos en cartera de forma temporal hasta el bloqueo de saldos en HMB
	 * @param idSecurity
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public void updateHolderMarketfactBalanceSelid(String getUserName, Integer getIdPrivilegeOfSystem, Date getAuditTime, String getIpAddress ) throws ServiceException {			
		
		//Primero extraemos el valor de la operacion de negociacion
		StringBuilder querySql = new StringBuilder();
				
		querySql.append("   SELECT MO.ID_SECURITY_CODE_FK,MO.ID_MECHANISM_OPERATION_PK                                               ");
		querySql.append("   FROM SETTLEMENT_DATE_OPERATION SDO                                                                       ");
		querySql.append("   INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK           ");
		querySql.append("   INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK     ");
		querySql.append("   INNER JOIN MECHANISM_OPERATION MO ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK         ");
		querySql.append("   WHERE 1 = 1                                                                                              ");
		querySql.append("   AND SR.REQUEST_TYPE = 2018                                                                               ");
		querySql.append("   AND SR.REQUEST_STATE = 1543                                                                              ");
		querySql.append("   AND SO.IND_EXTENDED = 1                                                                                  ");
		querySql.append("   AND SO.IND_UNFULFILLED <> 1                                                                              ");
		querySql.append("   AND (   SO.OPERATION_PART = 1 and SO.OPERATION_STATE = 614                                               ");
		querySql.append("        or SO.OPERATION_PART = 2 and SO.OPERATION_STATE = 615 )                                             ");
		querySql.append("   ORDER BY 1 DESC                                                                                          ");
				
		Query querySec = em.createNativeQuery(querySql.toString());
		//querySec.setParameter("idSettlementRequestPk", idSettlementRequestPk);
		
		List<Object[]>  resultList = (List<Object[]>)querySec.getResultList();
		
		//Si el resultado el diferente de nulo
		if( Validations.validateIsNotNullAndNotEmpty(resultList)){
			
			//recorremos las operaciones existentes
			for (Object[] result : resultList) {

				//extraccion de variables
				String security = (String)result[0];
				BigDecimal idMechanismOperationPK = (BigDecimal)result[1];
				
				//Extraemos informacion para luego realizar el update
				StringBuilder querySqlSao = new StringBuilder();
	
				querySqlSao.append("  SELECT                                                                                                                       ");
				querySqlSao.append("      sao.ID_SETTLEMENT_ACCOUNT_PK,                                                                                            ");
				querySqlSao.append("      SAO.STOCK_REFERENCE,                                                                                                     ");
				querySqlSao.append("      SAO.STOCK_BLOCK_DATE,                                                                                                    ");
				querySqlSao.append("      HAO.ROLE,                                                                                                                ");
				querySqlSao.append("      SAM.MARKET_DATE,                                                                                                         ");
				querySqlSao.append("      SAM.MARKET_RATE,                                                                                                         ");
				querySqlSao.append("      SAM.MARKET_QUANTITY,                                                                                                     ");
				querySqlSao.append("      MO.ID_BUYER_PARTICIPANT_FK,                                                                                              ");
				querySqlSao.append("      MO.ID_SELLER_PARTICIPANT_FK,                                                                                             ");
				querySqlSao.append("      HAO.ID_HOLDER_ACCOUNT_FK                                                                                                 ");
				querySqlSao.append("  FROM MECHANISM_OPERATION MO                                                                                                  ");
				querySqlSao.append("  INNER JOIN SETTLEMENT_OPERATION SO on SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                            ");
				querySqlSao.append("  INNER JOIN HOLDER_ACCOUNT_OPERATION HAO on HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                      ");
				querySqlSao.append("  INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO on SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK       ");
				querySqlSao.append("  INNER JOIN SETTLEMENT_ACCOUNT_MARKETFACT SAM ON SAO.ID_SETTLEMENT_ACCOUNT_PK = SAM.ID_SETTLEMENT_ACCOUNT_FK                  ");
				querySqlSao.append("  WHERE 1 = 1                                                                                                                  ");
				querySqlSao.append("  AND MO.ID_MECHANISM_OPERATION_PK = :idMechanismOperationPK                                                                   ");
				querySqlSao.append("  AND SO.OPERATION_PART = 1                                                                                                    ");
				querySqlSao.append("  AND HAO.OPERATION_PART = 1                                                                                                   ");
				querySqlSao.append("  AND MO.OPERATION_STATE = 614                                                                                                 ");
				querySqlSao.append("  AND HAO.HOLDER_ACCOUNT_STATE = 625                                                                                           ");
				querySqlSao.append("  AND SAO.OPERATION_STATE = 625                                                                                                ");
				querySqlSao.append("  AND SAM.IND_ACTIVE = 1                    		                                                                           ");
							
				Query querySAO = em.createNativeQuery(querySqlSao.toString());
				querySAO.setParameter("idMechanismOperationPK", idMechanismOperationPK.longValue());
				
				List<Object[]>  lstUpdate = (List<Object[]>) querySAO.getResultList();
	
				if(lstUpdate.size() > GeneralConstants.ZERO_VALUE_INTEGER){
					
					for(Object[] objectUpdate : lstUpdate){
						
						BigDecimal idSAOperation = (BigDecimal)objectUpdate[0];
						BigDecimal stockReference = (BigDecimal)objectUpdate[1];
						//Date stockBlockDate = (Date)objectUpdate[2];
						BigDecimal role = (BigDecimal)objectUpdate[3];
						Date marketDate = (Date)objectUpdate[4];
						BigDecimal marketRate = (BigDecimal)objectUpdate[5]; 
						BigDecimal marketQuantity = (BigDecimal)objectUpdate[6]; 
						BigDecimal idBuyerParticipant = (BigDecimal)objectUpdate[7]; 
						BigDecimal idSellerParticipant = (BigDecimal)objectUpdate[8]; 
						BigDecimal idHolderAccount = (BigDecimal)objectUpdate[9]; 
						
						//revertimos saldos dependiendo del rol del participante en la operacion
						if (Integer.valueOf(role.toString()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
							
							//rol compra, si el indicador de bloqueo esta correcto
							
							if(Validations.validateIsNotNullAndNotEmpty(stockReference)){
								
								if(Long.valueOf(stockReference.toString()).equals(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode())){
									
									//purchaseBalance HMB
									StringBuilder stringBufferPurchaseHMB = new StringBuilder();
									
									stringBufferPurchaseHMB.append(" UPDATE HolderMarketFactBalance set  ");
									stringBufferPurchaseHMB.append("   purchaseBalance = purchaseBalance - :marketQuantity ");
									stringBufferPurchaseHMB.append(" , lastModifyApp = :lastModifyApp ");
									stringBufferPurchaseHMB.append(" , lastModifyDate = :lastModifyDate ");
									stringBufferPurchaseHMB.append(" , lastModifyIp = :lastModifyIp ");
									stringBufferPurchaseHMB.append(" , lastModifyUser = :lastModifyUser ");
									stringBufferPurchaseHMB.append(" WHERE 1 = 1");
									stringBufferPurchaseHMB.append(" and security.idSecurityCodePk = :idSecurity ");
									stringBufferPurchaseHMB.append(" and participant.idParticipantPk = :idParticipant ");
									stringBufferPurchaseHMB.append(" and holderAccount.idHolderAccountPk = :idHolderAccount ");
									stringBufferPurchaseHMB.append(" and marketRate = :marketRate  ");
									stringBufferPurchaseHMB.append(" and marketDate = :marketDate  ");
									stringBufferPurchaseHMB.append(" and purchaseBalance >= :marketQuantity ");
									stringBufferPurchaseHMB.append(" and indActive = 1  ");
									
									Query queryPurchaseHMB = em.createQuery(stringBufferPurchaseHMB.toString());
									
									queryPurchaseHMB.setParameter("idSecurity", security);
									queryPurchaseHMB.setParameter("idParticipant", Long.parseLong(idBuyerParticipant.toString()));
									queryPurchaseHMB.setParameter("idHolderAccount", Long.parseLong(idHolderAccount.toString()));
									queryPurchaseHMB.setParameter("marketQuantity", marketQuantity);
									queryPurchaseHMB.setParameter("marketRate", marketRate);
									queryPurchaseHMB.setParameter("marketDate", marketDate);
									queryPurchaseHMB.setParameter("lastModifyApp", getIdPrivilegeOfSystem);
									queryPurchaseHMB.setParameter("lastModifyDate", getAuditTime);
									queryPurchaseHMB.setParameter("lastModifyIp", getIpAddress);
									queryPurchaseHMB.setParameter("lastModifyUser", getUserName);
									
									queryPurchaseHMB.executeUpdate();	
									
									//purchaseBalance HAB
									StringBuilder stringBufferPurchaseHAB = new StringBuilder();
									
									stringBufferPurchaseHAB.append(" UPDATE HolderAccountBalance set  ");
									stringBufferPurchaseHAB.append("   purchaseBalance = purchaseBalance - :marketQuantity  ");
									stringBufferPurchaseHAB.append(" , lastModifyApp = :lastModifyApp ");
									stringBufferPurchaseHAB.append(" , lastModifyDate = :lastModifyDate ");
									stringBufferPurchaseHAB.append(" , lastModifyIp = :lastModifyIp ");
									stringBufferPurchaseHAB.append(" , lastModifyUser = :lastModifyUser ");							
									stringBufferPurchaseHAB.append(" WHERE 1 = 1");
									stringBufferPurchaseHAB.append(" and security.idSecurityCodePk = :idSecurity ");
									stringBufferPurchaseHAB.append(" and participant.idParticipantPk = :idParticipant ");
									stringBufferPurchaseHAB.append(" and holderAccount.idHolderAccountPk = :idHolderAccount ");
									stringBufferPurchaseHAB.append(" and purchaseBalance >= :marketQuantity ");
									
									Query queryPurchaseHAB = em.createQuery(stringBufferPurchaseHAB.toString());
																
									queryPurchaseHAB.setParameter("idSecurity", security);
									queryPurchaseHAB.setParameter("idParticipant", Long.parseLong(idBuyerParticipant.toString()));
									queryPurchaseHAB.setParameter("idHolderAccount", Long.parseLong(idHolderAccount.toString()));
									queryPurchaseHAB.setParameter("marketQuantity", marketQuantity);
									queryPurchaseHAB.setParameter("lastModifyApp", getIdPrivilegeOfSystem);
									queryPurchaseHAB.setParameter("lastModifyDate", getAuditTime);
									queryPurchaseHAB.setParameter("lastModifyIp", getIpAddress);
									queryPurchaseHAB.setParameter("lastModifyUser", getUserName);
									
									queryPurchaseHAB.executeUpdate();	
								}
							}
							
						}else{
							//rol venta
							if(Integer.valueOf(role.toString()).equals(GeneralConstants.TWO_VALUE_INTEGER)){
								
								if(Validations.validateIsNotNullAndNotEmpty(stockReference)){
									
									if(Long.valueOf(stockReference.toString()).equals(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode())){
										
										//saleBalance HMB
										StringBuilder stringBufferSaleHMB = new StringBuilder();					
										
										stringBufferSaleHMB.append(" UPDATE HolderMarketFactBalance set  ");
										stringBufferSaleHMB.append("   availableBalance = availableBalance + :marketQuantity ");
										stringBufferSaleHMB.append(" , saleBalance = saleBalance - :marketQuantity ");
										stringBufferSaleHMB.append(" , lastModifyApp = :lastModifyApp ");
										stringBufferSaleHMB.append(" , lastModifyDate = :lastModifyDate ");
										stringBufferSaleHMB.append(" , lastModifyIp = :lastModifyIp ");
										stringBufferSaleHMB.append(" , lastModifyUser = :lastModifyUser ");
										stringBufferSaleHMB.append(" WHERE 1 = 1");
										stringBufferSaleHMB.append(" and security.idSecurityCodePk = :idSecurity ");
										stringBufferSaleHMB.append(" and participant.idParticipantPk = :idParticipant ");
										stringBufferSaleHMB.append(" and holderAccount.idHolderAccountPk = :idHolderAccount ");
										stringBufferSaleHMB.append(" and marketRate = :marketRate  ");
										stringBufferSaleHMB.append(" and marketDate = :marketDate  ");
										stringBufferSaleHMB.append(" and saleBalance >= :marketQuantity ");
										stringBufferSaleHMB.append(" and indActive = 1  ");
										
										Query querySaleHMB = em.createQuery(stringBufferSaleHMB.toString());
										
										querySaleHMB.setParameter("idSecurity", security);
										querySaleHMB.setParameter("idParticipant", Long.parseLong(idSellerParticipant.toString()));
										querySaleHMB.setParameter("idHolderAccount", Long.parseLong(idHolderAccount.toString()));
										querySaleHMB.setParameter("marketQuantity", marketQuantity);
										querySaleHMB.setParameter("marketRate", marketRate);
										querySaleHMB.setParameter("marketDate", marketDate);
										querySaleHMB.setParameter("lastModifyApp", getIdPrivilegeOfSystem);
										querySaleHMB.setParameter("lastModifyDate", getAuditTime);
										querySaleHMB.setParameter("lastModifyIp", getIpAddress);
										querySaleHMB.setParameter("lastModifyUser", getUserName);
										
										querySaleHMB.executeUpdate();
										
										//saleBalance HAB
										StringBuilder stringBufferSaleHAB = new StringBuilder();
										
										stringBufferSaleHAB.append(" UPDATE HolderAccountBalance set  ");
										stringBufferSaleHAB.append("   availableBalance = availableBalance + :marketQuantity ");
										stringBufferSaleHAB.append(" , saleBalance = saleBalance - :marketQuantity ");
										stringBufferSaleHAB.append(" , lastModifyApp = :lastModifyApp ");
										stringBufferSaleHAB.append(" , lastModifyDate = :lastModifyDate ");
										stringBufferSaleHAB.append(" , lastModifyIp = :lastModifyIp ");
										stringBufferSaleHAB.append(" , lastModifyUser = :lastModifyUser ");
										stringBufferSaleHAB.append(" WHERE 1 = 1 ");
										stringBufferSaleHAB.append(" and security.idSecurityCodePk = :idSecurity ");
										stringBufferSaleHAB.append(" and participant.idParticipantPk = :idParticipant ");
										stringBufferSaleHAB.append(" and holderAccount.idHolderAccountPk = :idHolderAccount ");	
										stringBufferSaleHAB.append(" and saleBalance >= :marketQuantity ");
										
										Query querySaleHAB = em.createQuery(stringBufferSaleHAB.toString());
									
										querySaleHAB.setParameter("idSecurity", security);
										querySaleHAB.setParameter("idParticipant", Long.parseLong(idSellerParticipant.toString()));
										querySaleHAB.setParameter("idHolderAccount", Long.parseLong(idHolderAccount.toString()));
										querySaleHAB.setParameter("marketQuantity", marketQuantity);
										querySaleHAB.setParameter("lastModifyApp", getIdPrivilegeOfSystem);
										querySaleHAB.setParameter("lastModifyDate", getAuditTime);
										querySaleHAB.setParameter("lastModifyIp", getIpAddress);
										querySaleHAB.setParameter("lastModifyUser", getUserName);
										
										querySaleHAB.executeUpdate();
									}
								}
							}
						}
						
						//actualizamos SettlementAccountOperation, el bloqueo de saldos podra volver a bloquear saldos despues de valorar			
						StringBuilder stringBufferSAO = new StringBuilder();
						
						stringBufferSAO.append(" UPDATE SettlementAccountOperation SET  ");
						stringBufferSAO.append("   stockReference = null ");
						stringBufferSAO.append(" , stockBlockDate = null ");
						stringBufferSAO.append(" , lastModifyApp = :lastModifyApp ");
						stringBufferSAO.append(" , lastModifyDate = :lastModifyDate ");
						stringBufferSAO.append(" , lastModifyIp = :lastModifyIp ");
						stringBufferSAO.append(" , lastModifyUser = :lastModifyUser ");
						stringBufferSAO.append(" WHERE idSettlementAccountPk = :idSettlementAccountOperationPk ");
						
						Query query = em.createQuery(stringBufferSAO.toString());
						query.setParameter("idSettlementAccountOperationPk", Long.parseLong(idSAOperation.toString()) );
						query.setParameter("lastModifyApp", getIdPrivilegeOfSystem);
						query.setParameter("lastModifyDate", getAuditTime);
						query.setParameter("lastModifyIp", getIpAddress);
						query.setParameter("lastModifyUser", getUserName);
						
						query.executeUpdate();	
					}
					
					//seleccionamos los datos de bloqueo antiguos
					StringBuilder stringBufferDel = new StringBuilder();
					stringBufferDel.append("     select HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK                                                 ");
					stringBufferDel.append("     from HOLDER_ACCOUNT_MOVEMENT HAM                                                         ");
					stringBufferDel.append("     inner join MOVEMENT_TYPE MT on MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK          ");
					stringBufferDel.append("     where 1 = 1                                                                              ");
					stringBufferDel.append("     and HAM.ID_MOVEMENT_TYPE_FK in (300234,300237,300250,300246)                             ");
					stringBufferDel.append("     and HAM.ID_TRADE_OPERATION_FK = :idMechanismPk                                           ");
					Query queryDel = em.createNativeQuery(stringBufferDel.toString());
					queryDel.setParameter("idMechanismPk", Long.parseLong(idMechanismOperationPK.toString()));
					List<Long[]>  resultListDel = (List<Long[]>)queryDel.getResultList();
					
					//Solo se elimina si es q existen movimientos de ese tipo
					if(resultListDel.size() > GeneralConstants.ZERO_VALUE_INTEGER) {
						//eliminamos movimientos de bloqueo de saldo referencial a nivel de HMM
						StringBuilder stringBufferHMM = new StringBuilder();
						stringBufferHMM.append(" DELETE FROM HOLDER_MARKETFACT_MOVEMENT                                                       ");
						stringBufferHMM.append(" where ID_HOLDER_ACCOUNT_MOVEMENT_FK in (:lstMechanismPk)                                     ");
						Query queryHMM = em.createNativeQuery(stringBufferHMM.toString());
						queryHMM.setParameter("lstMechanismPk", resultListDel);
						queryHMM.executeUpdate();	
						
						//eliminamos movimientos de bloqueo de saldo referencial a nivel HAM
						StringBuilder stringBufferHAM = new StringBuilder();
						stringBufferHAM.append(" DELETE FROM HOLDER_ACCOUNT_MOVEMENT                                                          ");
						stringBufferHAM.append(" WHERE ID_HOLDER_ACCOUNT_MOVEMENT_PK IN (:lstMechanismPk)                                     ");
						Query queryHAM = em.createNativeQuery(stringBufferHAM.toString());
						queryHAM.setParameter("lstMechanismPk", resultListDel);
						queryHAM.executeUpdate();
					}
				}	
			}
		}
	}
	
	/**
	 * Update stock settlement account operation.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @param idStockReference the id stock reference
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStockSettlementAccountOperation(Long idSettlementAccountOperation, Long idStockReference, LoggerUser loggerUser) throws ServiceException
	{			
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SettlementAccountOperation SET stockReference = :idStockReference ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk = :idSettlementAccountOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idStockReference", idStockReference);
		query.setParameter("idSettlementAccountOperation", idSettlementAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}
	
	/**
	 * Update stock settlement account operation.
	 *
	 * @param lstSettlementAccountOperation the lst settlement account operation
	 * @param idStockReference the id stock reference
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStockSettlementAccountOperation(List<Long> lstSettlementAccountOperation, Long idStockReference, LoggerUser loggerUser) throws ServiceException
	{			
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SettlementAccountOperation SET stockReference = :idStockReference ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk in (:lstSettlementAccountOperation) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idStockReference", idStockReference);
		query.setParameter("lstSettlementAccountOperation", lstSettlementAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}
	
	/**
	 * Update stock settlement account term operation.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @param idStockReference the id stock reference
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStockSettlementAccountTermOperation(Long idSettlementAccountOperation, Long idStockReference, LoggerUser loggerUser) throws ServiceException
	{			
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SettlementAccountOperation SET stockReference = :idStockReference ");
		if (AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(idStockReference)) {
			stringBuffer.append(" , stockBlockDate = :lastModifyDate ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementAccountOperation.idSettlementAccountPk = :idSettlementAccountOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idStockReference", idStockReference);
		query.setParameter("idSettlementAccountOperation", idSettlementAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}
	
	/**
	 * Update stock settlement account term operation.
	 *
	 * @param lstSettlementAccountOperation the lst settlement account operation
	 * @param idStockReference the id stock reference
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateStockSettlementAccountTermOperation(List<Long> lstSettlementAccountOperation, Long idStockReference, LoggerUser loggerUser)
	{			
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SettlementAccountOperation SET stockReference = :idStockReference ");
		if (AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(idStockReference)) {
			stringBuffer.append(" , stockBlockDate = :lastModifyDate ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementAccountOperation.idSettlementAccountPk in (:lstSettlementAccountOperation) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idStockReference", idStockReference);
		query.setParameter("lstSettlementAccountOperation", lstSettlementAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}
	
	/**
	 * Update stock reference participant operation.
	 *
	 * @param idParticipantOperation the id participant operation
	 * @param idStockReference the id stock reference
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStockReferenceParticipantOperation(Long idParticipantOperation, Long idStockReference, LoggerUser loggerUser) throws ServiceException
	{
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE ParticipantOperation SET stockReference = :idStockReference ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idParticipantOperationPk = :idParticipantOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idStockReference", idStockReference);
		query.setParameter("idParticipantOperation", idParticipantOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Method to get the list of participant operation ID related to the mechanism operation in STOCKS in charge for CASH or TERM part.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param idOperationPart the id operation part
	 * @param inchargeType the incharge type
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListIdParticipantOperation(Long idMechanismOperation, Integer idOperationPart, Integer inchargeType) throws ServiceException
	{
		List<Object[]> lstIdParticipantOperation= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT idParticipantOperationPk, ");
		stringBuffer.append(" role ");
		stringBuffer.append(" FROM ParticipantOperation ");
		stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and operationPart = :idOperationPart ");
		stringBuffer.append(" and inchargeType = :inchargeType ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("idOperationPart", idOperationPart);
		parameters.put("inchargeType", inchargeType);
		
		lstIdParticipantOperation = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstIdParticipantOperation;
	}
	
	
	/**
	 * Method to get the list of participant operation ID from swap operation in STOCK in charge.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param inchargeType the incharge type
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListIdParticipantSwapOperation(Long idMechanismOperation, Integer inchargeType) throws ServiceException
	{
		List<Object[]> lstIdParticipantOperation= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT PO.idParticipantOperationPk, ");
		stringBuffer.append(" PO.role ");
		stringBuffer.append(" FROM ParticipantOperation PO, SwapOperation SO ");
		stringBuffer.append(" WHERE SO.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and PO.swapOperation.idSwapOperationPk = SO.idSwapOperationPk ");
		stringBuffer.append(" and PO.operationPart = :idOperationPart ");
		stringBuffer.append(" and PO.inchargeType = :inchargeType ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("idOperationPart", ComponentConstant.CASH_PART);
		parameters.put("inchargeType", inchargeType);
		
		lstIdParticipantOperation = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstIdParticipantOperation;
	}
	
	
	/**
	 * Method to get the list of holder account from original REPORT operation in TERM part as sellers	.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListReportAccountOperation(Long idMechanismOperation) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT HAO.idHolderAccountOperationPk, ");
		stringBuffer.append(" HAO.holderAccount.idHolderAccountPk, ");
		stringBuffer.append(" HAO.stockQuantity, ");
		stringBuffer.append(" HAO.refAccountOperation.idHolderAccountOperationPk, ");
		stringBuffer.append(" HAO.mechanismOperation.termSettlementPrice ");
		stringBuffer.append(" FROM HolderAccountOperation HAO ");
		stringBuffer.append(" WHERE HAO.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and HAO.operationPart = :idOperationPart ");
		stringBuffer.append(" and HAO.role = :idRole ");
		stringBuffer.append(" and HAO.holderAccountState = :idHolderAccountState ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("idOperationPart", ComponentConstant.TERM_PART);
		parameters.put("idRole", ComponentConstant.SALE_ROLE);
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		lstObjects= findListByQueryString(stringBuffer.toString(), parameters);
		return lstObjects;
	}
	
	/**
	 * Update holder account operation state.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param idHolderAccountState the id holder account state
	 * @param inchargeState the incharge state
	 * @param loggerUser the logger user
	 * @param part the part
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateHolderAccountOperationState(Long idHolderAccountOperation, Integer idHolderAccountState,  Integer inchargeState,
			LoggerUser loggerUser, Integer part) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE HolderAccountOperation set holderAccountState= :idHolderAccountState ");
		if(inchargeState!=null){
			stringBuffer.append(" , inchargeState= :inchargeState ");
		}
		if(idHolderAccountState.equals(HolderAccountOperationStateType.CONFIRMED.getCode())){
			stringBuffer.append(" , confirmDate = :lastModifyDate ");
		}
		if(idHolderAccountState.equals(HolderAccountOperationStateType.CANCELLED.getCode())){
			stringBuffer.append(" , indIncharge = 0 ");
			stringBuffer.append(" , inchargeState = null ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		if(ComponentConstant.CASH_PART.equals(part)){
			stringBuffer.append(" WHERE idHolderAccountOperationPk = :lstIdHolderAccountOperation ");
		}else if (ComponentConstant.TERM_PART.equals(part)){
			stringBuffer.append(" WHERE refAccountOperation.idHolderAccountOperationPk = :lstIdHolderAccountOperation ");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idHolderAccountState", idHolderAccountState);
		query.setParameter("lstIdHolderAccountOperation", idHolderAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		if(inchargeState!=null){
			query.setParameter("inchargeState", inchargeState);
		}
		
		return query.executeUpdate();
	}
	
	/**
	 * Update settlement account by hao state.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param idHolderAccountState the id holder account state
	 * @param loggerUser the logger user
	 * @param part the part
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateSettlementAccountByHAOState(Long idHolderAccountOperation, Integer idHolderAccountState,  
			LoggerUser loggerUser, Integer part) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountOperation set operationState= :idHolderAccountState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		
		if(ComponentConstant.CASH_PART.equals(part)){
			stringBuffer.append(" WHERE holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
		}else if (ComponentConstant.TERM_PART.equals(part)){
			stringBuffer.append(" WHERE holderAccountOperation.idHolderAccountOperationPk in ( ");
			stringBuffer.append(" 			select hao.idHolderAccountOperationPk from HolderAccountOperation hao ");
			stringBuffer.append(" 			where hao.refAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
			stringBuffer.append(" 		) ");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idHolderAccountState", idHolderAccountState);
		query.setParameter("idHolderAccountOperation", idHolderAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update settlement account operation state.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @param idHolderAccountState the id holder account state
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateSettlementAccountOperationState(Long idSettlementAccountOperation, Integer idHolderAccountState, LoggerUser loggerUser) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountOperation set operationState= :idHolderAccountState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk = :idSettlementAccountOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idHolderAccountState", idHolderAccountState);
		query.setParameter("idSettlementAccountOperation", idSettlementAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update holder account operation state.
	 *
	 * @param lstIdHolderAccountOperation the lst id holder account operation
	 * @param idHolderAccountState the id holder account state
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateHolderAccountOperationState(List<Long> lstIdHolderAccountOperation, Integer idHolderAccountState, LoggerUser loggerUser) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE HolderAccountOperation set holderAccountState= :idHolderAccountState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idHolderAccountOperationPk in (:lstIdHolderAccountOperation) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idHolderAccountState", idHolderAccountState);
		query.setParameter("lstIdHolderAccountOperation", lstIdHolderAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	
	/**
	 * Method to update the stock reference for the participant operation.
	 *
	 * @param lstIdParticipantOperation the lst id participant operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStockReferenceParticipantOperation(List<Object[]> lstIdParticipantOperation, LoggerUser loggerUser) throws ServiceException
	{
		for (Object[] arrObject: lstIdParticipantOperation)
		{
			Long idParticipantOperation = new Long(arrObject[0].toString());
			Integer idRole= new Integer(arrObject[1].toString());
			Long idStockReference= null;
			if (ComponentConstant.SALE_ROLE.equals(idRole)) {
				idStockReference = AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode();
			} else if (ComponentConstant.PURCHARSE_ROLE.equals(idRole)) {
				idStockReference = AccountOperationReferenceType.AVAILABLE_SECURITIES.getCode();
			}
			updateStockReferenceParticipantOperation(idParticipantOperation, idStockReference, loggerUser);
		}
	}
	
	
	/**
	 * Method to update the balances from the holder account from the mechanism operation.
	 *
	 * @param settlementOperationTO the settlement operation to
	 * @param lstHolderAccountsOperation the lst holder accounts operation
	 * @param lstSourceHolderAccountBalance the lst source holder account balance
	 * @param lstTargetHolderAccountBalance the lst target holder account balance
	 * @param lstSellerAccountOperation the lst seller account operation
	 * @param lstBuyerAccountOperation the lst buyer account operation
	 * @param loggerUser the logger user
	 * @param indMarkerFact the ind marker fact
	 * @throws ServiceException the service exception
	 */
	public void updateBalancesAccountOperation(SettlementOperationTO settlementOperationTO,
												List<SettlementHolderAccountTO> lstHolderAccountsOperation,
												List<HolderAccountBalanceTO> lstSourceHolderAccountBalance,
												List<HolderAccountBalanceTO> lstTargetHolderAccountBalance,
												List<Long> lstSellerAccountOperation,
												List<Long> lstBuyerAccountOperation,
												LoggerUser loggerUser, Integer indMarkerFact) throws ServiceException
	{
		Long idStockReference= null;
		//we use temporal lists to fill the holder account balance 
		List<HolderAccountBalanceTO> lstTempSourceHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> lstTempTargetHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		//we use list to fill the holder account balance are chained
		List<HolderAccountBalanceTO> lstChainedSourceHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> lstChainedTargetHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		
		for (SettlementHolderAccountTO objSettlementHolderAccountTO: lstHolderAccountsOperation)
		{
			collectionProcessServiceBean.updateSettlementAccountSettled(objSettlementHolderAccountTO.getIdSettlementAccountOperation(), 
					objSettlementHolderAccountTO.getStockQuantity(), loggerUser);
			
			if(ComponentConstant.ONE.equals(settlementOperationTO.getIndPartial())){
				collectionProcessServiceBean.updateOrigSettlementAccountSettled(
						settlementOperationTO.getIdMechanismOperation(),
						objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
						objSettlementHolderAccountTO.getStockQuantity(), loggerUser);
			}
			
			//we update the stock reference for the holder accounts operation
			if (ComponentConstant.SALE_ROLE.equals(objSettlementHolderAccountTO.getRole())){
				idStockReference= AccountOperationReferenceType.DELIVERIED_SECURITIES.getCode();
				lstSellerAccountOperation.add(objSettlementHolderAccountTO.getIdSettlementAccountOperation());
			}else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementHolderAccountTO.getRole())){
				idStockReference= AccountOperationReferenceType.AVAILABLE_SECURITIES.getCode();
				lstBuyerAccountOperation.add(objSettlementHolderAccountTO.getIdSettlementAccountOperation());
			}
			if (SettlementSchemaType.GROSS.getCode().equals(settlementOperationTO.getSettlementSchema())) {
				updateStockSettlementAccountOperation(objSettlementHolderAccountTO.getIdSettlementAccountOperation(),idStockReference,loggerUser);
			}
			
			//we update the stock reference for holder accounts in term part only if the modality doesn't require block stock in term part
			if (ComponentConstant.ZERO.equals(settlementOperationTO.getIndTermStockBlock()) &&
				 ComponentConstant.ONE.equals(settlementOperationTO.getIndTermSettlement()) && //the modality must have term settlement
				 ComponentConstant.CASH_PART.equals(settlementOperationTO.getOperationPart()))
			{	
				if (ComponentConstant.SALE_ROLE.equals(objSettlementHolderAccountTO.getRole())){
					idStockReference= AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode();
				}else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementHolderAccountTO.getRole())){
					idStockReference= AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode();
				}
				if (SettlementSchemaType.GROSS.getCode().equals(settlementOperationTO.getSettlementSchema())) {
					updateStockSettlementAccountTermOperation(objSettlementHolderAccountTO.getIdSettlementAccountOperation(),idStockReference,loggerUser);
				}
			}
			
			if (!NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(settlementOperationTO.getIdModality()) &&
				!NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(settlementOperationTO.getIdModality())) 
			{
				HolderAccountBalanceTO objChainHolderAccountBalanceTO = null; //only for chain holder account
				//we set the holder account balance information to settle the stocks
				HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
				objHolderAccountBalanceTO.setIdHolderAccount(objSettlementHolderAccountTO.getIdHolderAccount());
				objHolderAccountBalanceTO.setIdParticipant(objSettlementHolderAccountTO.getIdParticipant());
				objHolderAccountBalanceTO.setIdSecurityCode(objSettlementHolderAccountTO.getIdSecurityCode());
				//we consider the normal quantity only
				objHolderAccountBalanceTO.setStockQuantity(objSettlementHolderAccountTO.getStockQuantity().subtract(objSettlementHolderAccountTO.getChainedQuantity()));
				if (objSettlementHolderAccountTO.getChainedQuantity().compareTo(BigDecimal.ZERO) > 0) {
					objChainHolderAccountBalanceTO = new HolderAccountBalanceTO();
					objChainHolderAccountBalanceTO.setIdHolderAccount(objSettlementHolderAccountTO.getIdHolderAccount());
					objChainHolderAccountBalanceTO.setIdParticipant(objSettlementHolderAccountTO.getIdParticipant());
					objChainHolderAccountBalanceTO.setIdSecurityCode(objSettlementHolderAccountTO.getIdSecurityCode());
					objChainHolderAccountBalanceTO.setStockQuantity(objSettlementHolderAccountTO.getChainedQuantity());
				}
				if(ComponentConstant.ONE.equals(indMarkerFact)){
					try {
						List<MarketFactAccountTO> lstChainedMarketFacts= new ArrayList<MarketFactAccountTO>();
						List<MarketFactAccountTO> lstNormalMarketFacts= new ArrayList<MarketFactAccountTO>();
						for (MarketFactAccountTO objMarketFactAccountTO: objSettlementHolderAccountTO.getMarketFactAccounts()) {
							BigDecimal normalQuantity= objMarketFactAccountTO.getQuantity().subtract(objMarketFactAccountTO.getChainedQuantity());
							if (normalQuantity.compareTo(BigDecimal.ZERO) > 0) {
								MarketFactAccountTO objNormalMarketFact= objMarketFactAccountTO.clone();
								objNormalMarketFact.setQuantity(normalQuantity);
								lstNormalMarketFacts.add(objNormalMarketFact);
							}
							if (objMarketFactAccountTO.getChainedQuantity().compareTo(BigDecimal.ZERO) > 0) {
								MarketFactAccountTO objChainedMarketFact= objMarketFactAccountTO.clone();
								objChainedMarketFact.setQuantity(objMarketFactAccountTO.getChainedQuantity());
								lstChainedMarketFacts.add(objChainedMarketFact);
							}
						}
						if (Validations.validateListIsNotNullAndNotEmpty(lstNormalMarketFacts)) {
							objHolderAccountBalanceTO.setLstMarketFactAccounts(lstNormalMarketFacts);
						}
						if (Validations.validateListIsNotNullAndNotEmpty(lstChainedMarketFacts)) {
							objChainHolderAccountBalanceTO.setLstMarketFactAccounts(lstChainedMarketFacts);
						}
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
						throw new ServiceException();
					}
				}
			
				if (ComponentConstant.SALE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					//we verify if this modality manage guarantees (Reported holder account)
					if (ComponentConstant.ONE.equals(settlementOperationTO.getIndReportingBalance()) && 
						ComponentConstant.TERM_PART.equals(objSettlementHolderAccountTO.getOperationPart())) 
					{	
						Long idHolderAccountOperation = objSettlementHolderAccountTO.getIdHolderAccountOperation();
						
						//it must save the guarantee operation to principal guarantee for this holder account operation. Guarantee finishing 
						GuaranteeOperation objGuaranteeOperation = guaranteesSettlement.saveGuaranteeOperation(idHolderAccountOperation, 
																										objSettlementHolderAccountTO.getIdParticipant(), objSettlementHolderAccountTO.getIdHolderAccount(), 
																										objSettlementHolderAccountTO.getIdSecurityCode(), objSettlementHolderAccountTO.getStockQuantity(), 
																										GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.PRINCIPAL.getCode(), null);
						if (objSettlementHolderAccountTO.getChainedQuantity().compareTo(BigDecimal.ZERO) > 0) {
							//it has a chained quantity, so the guarantee operation will be related to the chain settlement movement 
							objChainHolderAccountBalanceTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
						} else {
							//it doesn't belong a chain operation
							objHolderAccountBalanceTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
						}
					}
					if (objHolderAccountBalanceTO.getStockQuantity().compareTo(BigDecimal.ZERO) > 0) {
						if (objSettlementHolderAccountTO.getChainedQuantity().compareTo(BigDecimal.ZERO) > 0) {
							//it belongs a chain operation with normal quantity
							objHolderAccountBalanceTO.setNormalQuantity(true);
						}
						lstTempSourceHolderAccountBalance.add(objHolderAccountBalanceTO);
					}
					//this is the list to settle the chain quantity. This require a different settlement movement
					if (Validations.validateIsNotNull(objChainHolderAccountBalanceTO)) {
						lstChainedSourceHolderAccountBalance.add(objChainHolderAccountBalanceTO);
					}
				}
				else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					//we verify if this modality manage guarantees (Reported holder account)
					if (ComponentConstant.ONE.equals(settlementOperationTO.getIndReportingBalance()) && 
						ComponentConstant.CASH_PART.equals(settlementOperationTO.getOperationPart())) 
					{
						//we get the term holder account operation. this will save the principal guarantee
						Long idHolderAccountOperation = getIdRefHolderAccountOperation(objSettlementHolderAccountTO.getIdHolderAccountOperation());
						//it must save the guarantee operation to principal guarantee for this holder account operation. Guarantee opening
						GuaranteeOperation objGuaranteeOperation = guaranteesSettlement.saveGuaranteeOperation(idHolderAccountOperation, 
																										objSettlementHolderAccountTO.getIdParticipant(), objSettlementHolderAccountTO.getIdHolderAccount(), 
																										objSettlementHolderAccountTO.getIdSecurityCode(), objSettlementHolderAccountTO.getStockQuantity(), 
																										GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.PRINCIPAL.getCode(), null);
						if (objSettlementHolderAccountTO.getChainedQuantity().compareTo(BigDecimal.ZERO) > 0) {
							//it has a chained quantity, so the guarantee operation will be related to the chain settlement movement 
							objChainHolderAccountBalanceTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
						} else {
							//it doesn't belong a chain operation
							objHolderAccountBalanceTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
						}
					}
					if (objHolderAccountBalanceTO.getStockQuantity().compareTo(BigDecimal.ZERO) > 0) {
						if (objSettlementHolderAccountTO.getChainedQuantity().compareTo(BigDecimal.ZERO) > 0) {
							//it belongs a chain operation with normal quantity
							objHolderAccountBalanceTO.setNormalQuantity(true);
						}
						lstTempTargetHolderAccountBalance.add(objHolderAccountBalanceTO);
					}
					//this is the list to settle the chain quantity. This require a different settlement movement
					if (Validations.validateIsNotNull(objChainHolderAccountBalanceTO)) {
						lstChainedTargetHolderAccountBalance.add(objChainHolderAccountBalanceTO);
					}
				}
			}			
		}
		//only if the lstSourceHolderAccountBalance is null we set the temporal list
		if (lstSourceHolderAccountBalance == null){
			lstSourceHolderAccountBalance = lstTempSourceHolderAccountBalance;
		}
		//only if the lstTargetHolderAccountBalance is null we set the temporal list
		if (lstTargetHolderAccountBalance == null){
			lstTargetHolderAccountBalance = lstTempTargetHolderAccountBalance;
		}
		
		//we execute the component to move the balances. without chain or with normal quantity
		if (Validations.validateListIsNotNullAndNotEmpty(lstSourceHolderAccountBalance) ||
			Validations.validateListIsNotNullAndNotEmpty(lstTargetHolderAccountBalance))
		{
			AccountsComponentTO objAccountsComponentTO = populateAccountComponentMechanism(settlementOperationTO.getIdSettlementOperation(), 
														settlementOperationTO.getIdModality(), settlementOperationTO.getSettlementSchema(), 
														settlementOperationTO.getOperationPart(),indMarkerFact, false);
			if(!ComponentConstant.ONE.equals(settlementOperationTO.getIndPrimaryPlacement())){
				//the primary placement modalities in sale position doesn't move stocks in holder account balance. They didn't block any balances
				objAccountsComponentTO.setLstSourceAccounts(lstSourceHolderAccountBalance);
			}
			objAccountsComponentTO.setLstTargetAccounts(lstTargetHolderAccountBalance);
			accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
			
			//we verify if the operation has forced purchase
			if (ComponentConstant.ONE.equals(settlementOperationTO.getIndForcedPurchase())) {
				executeForcedPurchase(settlementOperationTO.getIdSettlementOperation(),
									settlementOperationTO.getOperationPart(), indMarkerFact,
									lstSourceHolderAccountBalance, lstTargetHolderAccountBalance);
			}
		}
		//if we have chain quantity, we execute the component to move the balance 
		if (Validations.validateListIsNotNullAndNotEmpty(lstChainedSourceHolderAccountBalance) ||
			Validations.validateListIsNotNullAndNotEmpty(lstChainedTargetHolderAccountBalance))
		{
			AccountsComponentTO objAccountsComponentTO = populateAccountComponentMechanism(settlementOperationTO.getIdSettlementOperation(), 
					settlementOperationTO.getIdModality(), settlementOperationTO.getSettlementSchema(), 
					settlementOperationTO.getOperationPart(),indMarkerFact, true);
			objAccountsComponentTO.setLstSourceAccounts(lstChainedSourceHolderAccountBalance);
			objAccountsComponentTO.setLstTargetAccounts(lstChainedTargetHolderAccountBalance); 
			accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
		}
	}

	/**
	 * Populate account component mechanism.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idModality the id modality
	 * @param settlementSchema the settlement schema
	 * @param idOperationPart the id operation part
	 * @param indMarketFact the ind market fact
	 * @param isChainSettlement the is chain settlement
	 * @return the accounts component to
	 */
	public AccountsComponentTO populateAccountComponentMechanism(Long idSettlementOperation, Long idModality, Integer settlementSchema, 
			Integer idOperationPart, Integer indMarketFact, boolean isChainSettlement)
	{
		AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
		Long idBusinessProcess= null;
		if (ComponentConstant.CASH_PART.equals(idOperationPart) && SettlementSchemaType.GROSS.getCode().equals(settlementSchema)) {
			idBusinessProcess= BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode();
		} else if (ComponentConstant.TERM_PART.equals(idOperationPart) && SettlementSchemaType.GROSS.getCode().equals(settlementSchema)) {
			idBusinessProcess= BusinessProcessType.GROSS_OPERATIONS_SETTLE_TERM_PART.getCode();
		} else if (ComponentConstant.CASH_PART.equals(idOperationPart) && SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
			idBusinessProcess= BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode();
		} else if (ComponentConstant.TERM_PART.equals(idOperationPart) && SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
			idBusinessProcess= BusinessProcessType.NET_OPERATIONS_SETTLE_TERM_PART.getCode();
		}
		
		Long operationType = null;
		if (!isChainSettlement) {
			operationType= NegotiationModalityType.get(idModality).getParameterOperationType();
		} else {
			operationType = NegotiationModalityType.get(idModality).getChainOperationType();
		}
		/*
		if (NegotiationModalityType.REPORT_EQUITIES.getCode().equals(idModality) || NegotiationModalityType.REPORT_FIXED_INCOME.getCode().equals(idModality)) {
			if (OperationPartType.CASH_PART.getCode().equals(idOperationPart)) {
				if (isChainSettlement) {
					operationType = ParameterOperationType.CHAIN_CASH_SETTLEMENT.getCode();
				}
			}
		}*/
		
		objAccountsComponentTO.setIdBusinessProcess(idBusinessProcess);
		objAccountsComponentTO.setIdOperationType(operationType);
		objAccountsComponentTO.setIdSettlementOperation(idSettlementOperation);
		objAccountsComponentTO.setIndMarketFact(indMarketFact);
		objAccountsComponentTO.setOperationPart(idOperationPart);
		return objAccountsComponentTO;
	}
	
	
	/**
	 * Execute forced purchase.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idOperationPart the id operation part
	 * @param indMarketFact the ind market fact
	 * @param lstSourceHolderAccountBalance the lst source holder account balance
	 * @param lstTargetHolderAccountBalance the lst target holder account balance
	 * @throws ServiceException the service exception
	 */
	private void executeForcedPurchase(Long idSettlementOperation, Integer idOperationPart, Integer indMarketFact,
										List<HolderAccountBalanceTO> lstSourceHolderAccountBalance, 
										List<HolderAccountBalanceTO> lstTargetHolderAccountBalance) throws ServiceException
	{
		AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
		objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_SETTLE_TERM_PART.getCode());
		objAccountsComponentTO.setIdOperationType(ParameterOperationType.FORCED_PURCHASE_SETTLEMENT.getCode());
		objAccountsComponentTO.setIdSettlementOperation(idSettlementOperation);
		objAccountsComponentTO.setIndMarketFact(indMarketFact);
		objAccountsComponentTO.setOperationPart(idOperationPart);
		objAccountsComponentTO.setLstSourceAccounts(lstTargetHolderAccountBalance); //rol de inversa
		objAccountsComponentTO.setLstTargetAccounts(lstSourceHolderAccountBalance); //rol de inversa
		
		accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
	}
	
	/**
	 * Gets the operation settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idSettlementOperation the id settlement operation
	 * @return the operation settlement
	 * @throws ServiceException the service exception
	 */
	public OperationSettlement getOperationSettlement(Long idSettlementProcess, Long idSettlementOperation) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		OperationSettlement objOperationSettlement= null;
		try {
			stringBuffer.append(" SELECT OS ");
			stringBuffer.append(" FROM OperationSettlement OS ");
			stringBuffer.append(" WHERE OS.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
			stringBuffer.append(" and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idSettlementOperation", idSettlementOperation);
			query.setParameter("idSettlementProcess", idSettlementProcess);
			
			objOperationSettlement= (OperationSettlement) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return objOperationSettlement;
	}
	
	/**
	 * Gets the list settlement process.
	 *
	 * @param settlementSchema the settlement schema
	 * @param settlementDate the settlement date
	 * @return the list settlement process
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementProcess> getListSettlementProcess(Integer settlementSchema, Date settlementDate)
	{
		List<SettlementProcess> lstSettlementProcesses= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SP FROM SettlementProcess SP ");
		stringBuffer.append(" WHERE SP.settlementSchema = :settlementSchema ");
		stringBuffer.append(" and SP.settlementDate = :settlementDate ");
		stringBuffer.append(" and SP.processState = :processState ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("settlementSchema", settlementSchema);
		query.setParameter("settlementDate", settlementDate);
		query.setParameter("processState", SettlementProcessStateType.WAITING.getCode());
		
		lstSettlementProcesses = query.getResultList();
		return lstSettlementProcesses;
	}
	
	
	/**
	 * Gets the list participant position.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idParticipant the id participant
	 * @param indPosition the ind position
	 * @param indSentFunds the ind sent funds
	 * @param dtSettlementDate the dt settlement date
	 * @return the list participant position
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipantPosition> getListParticipantPosition(Long idSettlementProcess, Long idParticipant, Integer indPosition, 
																Integer indSentFunds, Date dtSettlementDate) throws ServiceException
	{
		List<ParticipantPosition> lstParticipantPosition= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT PP FROM ParticipantPosition PP ");
		stringBuffer.append(" inner join fetch PP.participant p ");
		stringBuffer.append(" inner join fetch PP.settlementProcess sp ");
		stringBuffer.append(" inner join fetch sp.negotiationMechanism nme ");
		stringBuffer.append(" inner join fetch sp.modalityGroup mgr ");
		stringBuffer.append(" WHERE PP.indProcess = :indicatorOne ");
		stringBuffer.append(" and PP.indSentFunds = :indSentFunds ");
		if (SettlementConstant.NEGATIVE_POSITION.equals(indPosition)) {
			stringBuffer.append(" and PP.netPosition < 0 ");
		} else if (SettlementConstant.POSITIVE_POSITION.equals(indPosition)) {
			stringBuffer.append(" and PP.netPosition > 0 ");
		}
		if (Validations.validateIsNotNull(idSettlementProcess)) {
			stringBuffer.append(" and sp.idSettlementProcessPk = :idSettlementProcess ");
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			stringBuffer.append(" and p.idParticipantPk = :idParticipant ");
		}
		
		if(Validations.validateIsNotNull(dtSettlementDate)){
			stringBuffer.append(" and trunc(sp.settlementDate) = trunc(:dtSettlementDate) ");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indicatorOne", ComponentConstant.ONE);
		query.setParameter("indSentFunds", indSentFunds);
		if (Validations.validateIsNotNull(idSettlementProcess)) {
			query.setParameter("idSettlementProcess", idSettlementProcess);
		}
		if (Validations.validateIsNotNull(idParticipant)) {
			query.setParameter("idParticipant", idParticipant);
		}
		if(Validations.validateIsNotNull(dtSettlementDate)){
			query.setParameter("dtSettlementDate", dtSettlementDate);
		}
		lstParticipantPosition = query.getResultList();
		return lstParticipantPosition;
	}
	
	/**
	 * Update operation settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param lstSettlementOperation the lst settlement operation
	 * @param operationState the operation state
	 * @param loggerUser the logger user
	 */
	public void updateOperationSettlement(Long idSettlementProcess, List<Long> lstSettlementOperation, Integer operationState, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE OperationSettlement ");
		stringBuffer.append(" SET operationState = :operationState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementOperation.idSettlementOperationPk in (:lstSettlementOperation) ");
		stringBuffer.append(" and settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("operationState", operationState);
		query.setParameter("lstSettlementOperation", lstSettlementOperation);
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update settlement settled count.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param countOperation the count operation
	 * @param loggerUser the logger user
	 */
	public void updateSettlementSettledCount(Long idSettlementProcess, Long countOperation, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementProcess set settledOperations = settledOperations + :countOperation ");
		stringBuffer.append(" , pendingOperations = pendingOperations - :countOperation ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementProcessPk = :idSettlementProcess ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.setParameter("countOperation", countOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update settlement settled count.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @throws ServiceException the service exception
	 */
	public void updateSettlementSettledCount(Long idSettlementProcess) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementProcess set settledOperations = settledOperations +1, pendingOperations = pendingOperations -1 ");
		stringBuffer.append(" WHERE idSettlementProcessPk = :idSettlementProcess ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.executeUpdate();
	}
	
	/**
	 * Update settlement pending count.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @throws ServiceException the service exception
	 */
	public void updateSettlementPendingCount(Long idSettlementProcess) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementProcess set pendingOperations = pendingOperations +1, removedOperations = removedOperations -1 ");
		stringBuffer.append(" WHERE idSettlementProcessPk = :idSettlementProcess ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.executeUpdate();
	}
	
	/**
	 * Update settlement removed count.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @throws ServiceException the service exception
	 */
	public void updateSettlementRemovedCount(Long idSettlementProcess) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementProcess set removedOperations = removedOperations +1, pendingOperations = pendingOperations -1 ");
		stringBuffer.append(" WHERE idSettlementProcessPk = :idSettlementProcess ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.executeUpdate();
	}
	
	/**
	 * Gets the count pending settlement operations.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param settlementSchema the settlement schema
	 * @return the count pending settlement operations
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getCountPendingSettlementOperations(Long idMechanism, Long idModalityGroup, Integer idCurrency, Date settlementDate, Integer settlementSchema)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SO.idSettlementOperationPk ");
		stringBuffer.append(" FROM ModalityGroupDetail MGD, MechanismOperation MO , SettlementOperation SO ");
		stringBuffer.append(" WHERE MO.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism ");
		stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		stringBuffer.append(" and MGD.negotiationMechanism.idNegotiationMechanismPk = MO.mechanisnModality.id.idNegotiationMechanismPk ");
		stringBuffer.append(" and MGD.negotiationModality.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = SO.settlementSchema ");
		stringBuffer.append(" and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and   (( SO.operationPart = :cashPart and SO.operationState in (:cashStates)) ");
		stringBuffer.append("     or ( SO.operationPart = :termPart and SO.operationState in (:termStates))) ");
		stringBuffer.append(" and SO.settlementCurrency =:idCurrency ");
		stringBuffer.append(" and SO.settlementDate =:settlementDate ");
		stringBuffer.append(" and SO.settlementSchema = :settlementSchema ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanism", idMechanism);
		query.setParameter("idModalityGroup", idModalityGroup);
		query.setParameter("idCurrency", idCurrency);
		query.setParameter("settlementDate", settlementDate);
		query.setParameter("settlementSchema", settlementSchema);
		query.setParameter("cashPart", ComponentConstant.CASH_PART);
		query.setParameter("termPart", ComponentConstant.TERM_PART);
		List<Integer> lstCashState= new ArrayList<Integer>();
		lstCashState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("cashStates", lstCashState);
		query.setParameter("termStates", MechanismOperationStateType.CASH_SETTLED.getCode());
		List<Long> countPending = (List<Long>) query.getResultList();
		return countPending;
	}
	
	
	/**
	 * Removes the operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param removeMotive the remove motive
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void removeOperation(Long idMechanismOperation, Long removeMotive, LoggerUser loggerUser) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE MechanismOperation SET indRemoveSettlement = :indRemoveSettlement ");
		stringBuffer.append(" , removeSettlementMotive = :removeMotive ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idMechanismOperationPk = :idMechanismOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indRemoveSettlement", ComponentConstant.ONE);
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("removeMotive", removeMotive);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Gets the settlement process.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @return the settlement process
	 */
	public SettlementProcess getSettlementProcess(Long idSettlementProcess)
	{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT sp from SettlementProcess sp inner join fetch sp.negotiationMechanism nme ");
		stringBuffer.append(" inner join fetch sp.modalityGroup mg ");
		stringBuffer.append(" where sp.idSettlementProcessPk = :idSettlementProcess ");
		
		parameters.put("idSettlementProcess", idSettlementProcess);
		
		return (SettlementProcess) findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the list id operation settlement.
	 *
	 * @param idSetlementProcess the id setlement process
	 * @param mechanismOperationState the mechanism operation state
	 * @return the list id operation settlement
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListIdOperationSettlement(Long idSetlementProcess, Integer mechanismOperationState)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT settlementOperation.idSettlementOperationPk, settlementOperation.mechanismOperation.idMechanismOperationPk, ");
		stringBuffer.append(" ( CASE WHEN settlementOperation.mechanismOperation.settledQuantity = settlementOperation.mechanismOperation.stockQuantity THEN 1 ELSE 0 END) "); // 2
		stringBuffer.append(" FROM OperationSettlement ");
		stringBuffer.append(" WHERE settlementProcess.idSettlementProcessPk = :idSetlementProcess ");
		stringBuffer.append(" and operationState = :operationState ");
		stringBuffer.append(" and settlementOperation.operationState = :mechanismOperationState ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSetlementProcess", idSetlementProcess);
		query.setParameter("operationState", OperationSettlementSateType.SETTLED.getCode());
		query.setParameter("mechanismOperationState", mechanismOperationState);
		
		return query.getResultList();
	}
	
	/**
	 * Gets the list operation settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param operationSettlementState the operation settlement state
	 * @param settlementType the settlement type
	 * @return the list operation settlement
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListOperationSettlement(Long idSettlementProcess, Integer operationSettlementState, Integer settlementType)
	{
		StringBuffer stringBuffer = new StringBuffer();
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		stringBuffer.append(" SELECT OS, SO.idSettlementOperationPk "); // 0 1 
		stringBuffer.append(" FROM OperationSettlement OS inner join OS.settlementOperation SO ");
		stringBuffer.append(" WHERE OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		if(operationSettlementState!=null){
			stringBuffer.append(" and OS.operationState = :operationSettlementState ");
			parameters.put("operationSettlementState", operationSettlementState);
		}
		if(settlementType!=null){
			stringBuffer.append(" and SO.settlementType = :settlementType ");
			parameters.put("settlementType", settlementType);
		}
		stringBuffer.append(" ORDER BY SO.settlementAmount ASC ");
		parameters.put("idSettlementProcess", idSettlementProcess);
		
		return (List<Object[]>)findListByQueryString(stringBuffer.toString(),parameters);
	}
	
	/**
	 * Gets the operation settlement details.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param operationSettlementState the operation settlement state
	 * @return the operation settlement details
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getOperationSettlementDetails(Long idSettlementProcess, Integer operationSettlementState)
	{
		StringBuffer stringBuffer = new StringBuffer();
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		stringBuffer.append(" SELECT SO.idSettlementOperationPk, SO.mechanismOperation.idMechanismOperationPk "); 
		stringBuffer.append(" FROM 	SettlementOperation SO , OperationSettlement OS ");
		stringBuffer.append(" WHERE OS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");	
		stringBuffer.append(" and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		stringBuffer.append(" and SO.settlementType in (:dvpType ) ");
		stringBuffer.append(" and OS.operationState = :operationSettlementState ");
		stringBuffer.append(" order by SO.mechanismOperation.idMechanismOperationPk");
		
		parameters.put("operationSettlementState", operationSettlementState);
		parameters.put("dvpType", SettlementType.DVP.getCode());
		parameters.put("idSettlementProcess", idSettlementProcess);
		
		return (List<Object[]>)findListByQueryString(stringBuffer.toString(),parameters);
	}

	/**
	 * Method to get the list of operations pending to block stock in cash and term part.
	 *
	 * @param settlementDate the settlement date
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param byStock the by stock
	 * @param byGuarantees the by guarantees
	 * @param byFunds the by funds
	 * @param settlement the settlement
	 * @param extended the extended
	 * @return the list excluded operations by stocks
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListNetPendingOperations(Date settlementDate, 
			Long idMechanism, Long idModalityGroup, Integer currency, boolean byStock, boolean byGuarantees, boolean byFunds, 
			boolean settlement, boolean extended, Long idParticipantPk) throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT MO.idMechanismOperationPk, "); //0
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
		stringBuffer.append(" MO.mechanisnModality.negotiationMechanism.mechanismName, "); // 2
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 3
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.modalityName, "); // 4
		stringBuffer.append(" MO.sellerParticipant.idParticipantPk, "); //5
		stringBuffer.append(" MO.sellerParticipant.description, "); // 6 
		stringBuffer.append(" MO.sellerParticipant.mnemonic, "); // 7 
		stringBuffer.append(" MO.buyerParticipant.idParticipantPk, "); // 8
		stringBuffer.append(" MO.buyerParticipant.description, "); // 9
		stringBuffer.append(" MO.buyerParticipant.mnemonic, "); //10
		stringBuffer.append(" MO.operationDate, "); //11
		stringBuffer.append(" MO.operationNumber, "); // 12 
		stringBuffer.append(" MO.ballotNumber, "); // 13
		stringBuffer.append(" MO.sequential, "); // 14
		stringBuffer.append(" MO.operationState, "); // 15
		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 16
		stringBuffer.append(" SO.idSettlementOperationPk, "); //17
		stringBuffer.append(" SO.stockQuantity, "); //18
		stringBuffer.append(" SO.settlementAmount, "); //19
		stringBuffer.append(" SO.settlementDate, "); //20
		stringBuffer.append(" SO.operationPart, "); //21
		stringBuffer.append(" ps.idParticipantSettlementPk, "); //22
		stringBuffer.append(" ps.participant.idParticipantPk, "); //23
		stringBuffer.append(" ps.participant.description, "); //24
		stringBuffer.append(" ps.participant.mnemonic, "); //25
		stringBuffer.append(" ps.role, "); //26
		stringBuffer.append(" ps.stockQuantity, "); //27
		stringBuffer.append(" ps.settlementAmount, "); //28
		stringBuffer.append(" 0 "); //29 IND HAS CHAIN HOLDER OPERATION
		stringBuffer.append(" FROM ParticipantSettlement PS, SettlementOperation SO, MechanismOperation MO, ModalityGroupDetail MGD ");
		stringBuffer.append(" WHERE MO.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism ");
		
		if(byStock){
			stringBuffer.append(" and ( (SO.operationPart = :cashPart and MO.indCashStockBlock = :indicatorOne and SO.operationState = :assignedState)");
			stringBuffer.append("       or (SO.operationPart = :termPart and MO.indTermStockBlock = :indicatorOne and SO.operationState = :cashSettledState) )");
			stringBuffer.append(" and SO.stockReference is null ");
			// Error  // stringBuffer.append(" and SAO.stockReference is null ");
			
			parameters.put("termPart", ComponentConstant.TERM_PART);
			parameters.put("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		}
		
		if(byGuarantees){
			stringBuffer.append(" and (SO.operationPart = :cashPart and SO.operationState = :assignedState) ");
			stringBuffer.append(" and SO.stockReference = :idStockReference ");
			stringBuffer.append(" and (SO.marginReference is null and MO.indMarginGuarantee = :indicatorOne ) ");
			parameters.put("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		}
		
		if(byFunds){
			stringBuffer.append(" and ( (SO.operationPart = :cashPart and SO.operationState = :assignedState)");
			stringBuffer.append("       or (SO.operationPart = :termPart and SO.operationState = :cashSettledState) )");
			stringBuffer.append(" and (SO.marginReference = :marginReference and MO.indMarginGuarantee = :indicatorOne or ");
			stringBuffer.append("			SO.marginReference is null and MO.indMarginGuarantee != :indicatorOne) ");
			stringBuffer.append(" and SO.stockReference = :idStockReference ");
		
			parameters.put("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
			parameters.put("marginReference", AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
			parameters.put("termPart", ComponentConstant.TERM_PART);
			parameters.put("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		}
		
		if(!settlement && extended){
			stringBuffer.append(" and SO.indExtended = :indicatorOne ");
		}
		if(settlement && !extended){
			stringBuffer.append(" and SO.indExtended <> :indicatorOne ");	
		}
		
		//we exclude the chained operations, because they have a special query
		stringBuffer.append(" and not exists (Select HCD From HolderChainDetail HCD "
							+ " Where HCD.settlementAccountMarketfact.settlementAccountOperation.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk "
							+ " and HCD.chainedHolderOperation.chaintState in (:lstChainState)) "); //registered and confirmed
	
		List<Integer> lstChainState= new ArrayList<Integer>();
		//lstChainState.add(ChainedOperationStateType.REGISTERED.getCode());
		lstChainState.add(ChainedOperationStateType.CONFIRMED.getCode());
		parameters.put("lstChainState", lstChainState);
		
		// Error // stringBuffer.append(" and SO.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk");
		stringBuffer.append(" and SO.settlementDate = :settlementDate");
		stringBuffer.append(" and SO.settlementCurrency = :idCurrency ");
		stringBuffer.append(" and MGD.mechanismModality.id.idNegotiationMechanismPk = MO.mechanisnModality.id.idNegotiationMechanismPk ");
		stringBuffer.append(" and MGD.mechanismModality.id.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
		stringBuffer.append(" and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and PS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = :idSettlementSchema ");
		stringBuffer.append(" and SO.settlementSchema = :idSettlementSchema ");
		stringBuffer.append(" and SO.indRemoveSettlement != :indicatorOne ");
		stringBuffer.append(" and PS.operationState = :confirmedPartState ");
		// Error // stringBuffer.append(" AND ps.role=2 "); // solo parte venta
		
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk)) {
			stringBuffer.append(" and ps.participant.idParticipantPk = :idParticipant");
			
		}
		
		stringBuffer.append(" ORDER BY ps.role, SO.settlementAmount asc ");
		
		parameters.put("settlementDate", settlementDate);
		parameters.put("confirmedPartState", ParticipantOperationStateType.CONFIRM.getCode());
		parameters.put("idMechanism", idMechanism);
		parameters.put("idModalityGroup", idModalityGroup);
		parameters.put("idSettlementSchema", SettlementSchemaType.NET.getCode());
		parameters.put("idCurrency", currency);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("assignedState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk)) {
			parameters.put("idParticipant", idParticipantPk);
		}
		
		return (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	
	/**
	 * Gets the list chained operation.
	 *
	 * @param settlementDate the settlement date
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param settlement the settlement
	 * @param extended the extended
	 * @return the list chained operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListChainedOperation(Date settlementDate,Long idMechanism, Long idModalityGroup, Integer currency, boolean settlement, boolean extended) throws ServiceException
	{
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT DISTINCT MO.idMechanismOperationPk, "); //0
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
		stringBuffer.append(" MO.mechanisnModality.negotiationMechanism.mechanismName, "); // 2
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 3
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.modalityName, "); // 4
		stringBuffer.append(" MO.sellerParticipant.idParticipantPk, "); //5
		stringBuffer.append(" MO.sellerParticipant.description, "); // 6 
		stringBuffer.append(" MO.sellerParticipant.mnemonic, "); // 7 
		stringBuffer.append(" MO.buyerParticipant.idParticipantPk, "); // 8
		stringBuffer.append(" MO.buyerParticipant.description, "); // 9
		stringBuffer.append(" MO.buyerParticipant.mnemonic, "); //10
		stringBuffer.append(" MO.operationDate, "); //11
		stringBuffer.append(" MO.operationNumber, "); // 12 
		stringBuffer.append(" MO.ballotNumber, "); // 13
		stringBuffer.append(" MO.sequential, "); // 14
		stringBuffer.append(" MO.operationState, "); // 15
		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 16
		stringBuffer.append(" SO.idSettlementOperationPk, "); //17
		stringBuffer.append(" SO.stockQuantity, "); //18
		stringBuffer.append(" SO.settlementAmount, "); //19
		stringBuffer.append(" SO.settlementDate, "); //20
		stringBuffer.append(" SO.operationPart, "); //21
		stringBuffer.append(" ps.idParticipantSettlementPk, "); //22
		stringBuffer.append(" ps.participant.idParticipantPk, "); //23
		stringBuffer.append(" ps.participant.description, "); //24
		stringBuffer.append(" ps.participant.mnemonic, "); //25
		stringBuffer.append(" ps.role, "); //26
		stringBuffer.append(" ps.stockQuantity, "); //27
		stringBuffer.append(" ps.settlementAmount, "); //28
		stringBuffer.append(" 1 "); //29
		stringBuffer.append(" FROM ParticipantSettlement PS, SettlementOperation SO, MechanismOperation MO, ModalityGroupDetail MGD, HolderChainDetail HCD ");
		stringBuffer.append(" WHERE MO.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism ");
		stringBuffer.append(" and MGD.mechanismModality.id.idNegotiationMechanismPk = MO.mechanisnModality.id.idNegotiationMechanismPk ");
		stringBuffer.append(" and MGD.mechanismModality.id.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
		stringBuffer.append(" and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and PS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and HCD.settlementAccountMarketfact.settlementAccountOperation.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and HCD.chainedHolderOperation.chaintState = :chainState ");//confirmed
		stringBuffer.append(" and HCD.chainedHolderOperation.stockReference in (:lstStockReference) "); //VC - VR
		stringBuffer.append(" and (SO.operationState = :assignedState and SO.operationPart = :cashPart "
								+ " or SO.operationState = :cashSettledState and SO.operationPart = :termPart) ");
		stringBuffer.append(" and SO.settlementDate = :settlementDate ");
		stringBuffer.append(" and SO.settlementCurrency = :idCurrency ");
		stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		stringBuffer.append(" and MGD.modalityGroup.settlementSchema = :idSettlementSchema ");
		stringBuffer.append(" and MO.settlementSchema = :idSettlementSchema ");
		stringBuffer.append(" and SO.indRemoveSettlement != :indicatorOne ");
		if(!settlement && extended){
			stringBuffer.append(" and SO.indExtended = :indicatorOne ");
		}
		if(settlement && !extended){
			stringBuffer.append(" and SO.indExtended <> :indicatorOne ");	
		}
		stringBuffer.append(" ORDER BY ps.role, SO.settlementAmount asc ");
		
		parameters.put("chainState", ChainedOperationStateType.CONFIRMED.getCode());
		List<Long> lstStockReference= new ArrayList<Long>();
		lstStockReference.add(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		lstStockReference.add(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		parameters.put("lstStockReference", lstStockReference);
		parameters.put("settlementDate", settlementDate);
		parameters.put("idMechanism", idMechanism);
		parameters.put("idModalityGroup", idModalityGroup);
		parameters.put("idSettlementSchema", SettlementSchemaType.NET.getCode());
		parameters.put("idCurrency", currency);
		parameters.put("assignedState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		parameters.put("cashSettledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		parameters.put("cashPart", OperationPartType.CASH_PART.getCode());
		parameters.put("termPart", OperationPartType.TERM_PART.getCode());
		parameters.put("indicatorOne", ComponentConstant.ONE);
		//parameters.put("role", ComponentConstant.SALE_ROLE);
		
		return (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the list chain settlement operation.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idChainHolderOperation the id chain holder operation
	 * @return the list chain settlement operation
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListChainSettlementOperation(Long idSettlementOperation, Long idChainHolderOperation)
	{		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT HCD.chainedHolderOperation.idChainedHolderOperationPk "); //0
		stringBuffer.append(" FROM HolderChainDetail HCD ");
		stringBuffer.append(" WHERE HCD.settlementAccountMarketfact.settlementAccountOperation.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and HCD.chainedHolderOperation.chaintState = :chainState ");
		if (Validations.validateIsNotNull(idChainHolderOperation)) {
			stringBuffer.append(" and HCD.chainedHolderOperation.idChainedHolderOperationPk != :idChainHolderOperation ");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("chainState", ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		if (Validations.validateIsNotNull(idChainHolderOperation)) {
			query.setParameter("idChainHolderOperation", idChainHolderOperation);
		}
		return query.getResultList();
	}
	
	/**
	 * Gets the list settlement operation by chain.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param idSettlementOperation the id settlement operation
	 * @return the list settlement operation by chain
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListSettlementOperationByChain(Long idChainedHolderOperation, Long idSettlementOperation)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT HCD.settlementAccountMarketfact.settlementAccountOperation.settlementOperation.idSettlementOperationPk "); //0
		stringBuffer.append(" FROM HolderChainDetail HCD ");
		stringBuffer.append(" WHERE HCD.chainedHolderOperation.idChainedHolderOperationPk = :idChainedHolderOperation ");
		stringBuffer.append(" and HCD.chainedHolderOperation.chaintState = :chainState ");
		if (Validations.validateIsNotNull(idSettlementOperation)) {
			stringBuffer.append(" and HCD.settlementAccountMarketfact.settlementAccountOperation.settlementOperation.idSettlementOperationPk != :idSettlementOperation ");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("chainState", ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("idChainedHolderOperation", idChainedHolderOperation);
		if (Validations.validateIsNotNull(idSettlementOperation)) {
			query.setParameter("idSettlementOperation", idSettlementOperation);
		}
		return query.getResultList();
	}
	
	/**
	 * Gets the operations by id.
	 * method to get operations with participant by Id
	 *
	 * @param settlementProcessId the settlement process id
	 * @return the operations by id
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListNetSettlementOperations(Long settlementProcessId) throws ServiceException{
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT MO.idMechanismOperationPk, "); //0
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
		stringBuffer.append(" MO.mechanisnModality.negotiationMechanism.mechanismName, "); // 2
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 3
		stringBuffer.append(" MO.mechanisnModality.negotiationModality.modalityName, "); // 4
		stringBuffer.append(" MO.sellerParticipant.idParticipantPk, "); //5
		stringBuffer.append(" MO.sellerParticipant.description, "); // 6 
		stringBuffer.append(" MO.sellerParticipant.mnemonic, "); // 7 
		stringBuffer.append(" MO.buyerParticipant.idParticipantPk, "); // 8
		stringBuffer.append(" MO.buyerParticipant.description, "); // 9
		stringBuffer.append(" MO.buyerParticipant.mnemonic, "); //10
		stringBuffer.append(" MO.operationDate, "); //11
		stringBuffer.append(" MO.operationNumber, "); // 12 
		stringBuffer.append(" MO.ballotNumber, "); // 13
		stringBuffer.append(" MO.sequential, "); // 14
		stringBuffer.append(" MO.operationState, "); // 15
		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 16
		stringBuffer.append(" SO.idSettlementOperationPk, "); //17
		stringBuffer.append(" SO.stockQuantity, "); //18
		stringBuffer.append(" SO.settlementAmount, "); //19
		stringBuffer.append(" SO.settlementDate, "); //20
		stringBuffer.append(" SO.operationPart, "); //21
		stringBuffer.append(" ps.idParticipantSettlementPk, "); //22
		stringBuffer.append(" ps.participant.idParticipantPk, "); //23
		stringBuffer.append(" ps.participant.description, "); //24
		stringBuffer.append(" ps.participant.mnemonic, "); //25
		stringBuffer.append(" ps.role, "); //26
		stringBuffer.append(" ps.stockQuantity, "); //27
		stringBuffer.append(" ps.settlementAmount, "); //28
		stringBuffer.append(" (Select count(HCD.chainedHolderOperation.idChainedHolderOperationPk) "
							+ " From HolderChainDetail HCD "
							+ " Where HCD.settlementAccountMarketfact.settlementAccountOperation.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk "
							+ " and HCD.chainedHolderOperation.chaintState = :chainState "
							+ " and HCD.chainedHolderOperation.stockReference in (:lstStockReference)) "); //29
		stringBuffer.append(" FROM ParticipantSettlement PS, SettlementOperation SO, MechanismOperation MO, OperationSettlement OS ");
		stringBuffer.append(" WHERE ");
		stringBuffer.append(" SO.settlementDate = OS.settlementProcess.settlementDate ");
		stringBuffer.append(" and SO.idSettlementOperationPk = OS.settlementOperation.idSettlementOperationPk ");
		stringBuffer.append(" and SO.settlementCurrency = OS.settlementProcess.currency  ");
		stringBuffer.append(" and OS.settlementProcess.idSettlementProcessPk = :settlementId ");
		stringBuffer.append(" and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and PS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and PS.operationState = :confirmedPartState ");
		stringBuffer.append(" ORDER BY ps.role, SO.settlementAmount asc ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("confirmedPartState", ParticipantOperationStateType.CONFIRM.getCode());
		parameters.put("chainState", ChainedOperationStateType.CONFIRMED.getCode());
		List<Long> lstStockReference= new ArrayList<Long>();
		lstStockReference.add(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		lstStockReference.add(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		parameters.put("lstStockReference", lstStockReference);
		parameters.put("settlementId", settlementProcessId);	
		
		return (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the id ref holder account operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @return the id ref holder account operation
	 */
	public Long getIdRefHolderAccountOperation(Long idHolderAccountOperation)
	{
		Long idRefHolderAccountOperation= null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT HAO2.idHolderAccountOperationPk ");
			stringBuffer.append(" FROM HolderAccountOperation HAO1, HolderAccountOperation HAO2 ");
			stringBuffer.append(" WHERE HAO1.idHolderAccountOperationPk = HAO2.refAccountOperation.idHolderAccountOperationPk ");
			stringBuffer.append(" and HAO1.idHolderAccountOperationPk = :idHolderAccountOperation ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idHolderAccountOperation", idHolderAccountOperation);
			idRefHolderAccountOperation = (Long) query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return idRefHolderAccountOperation;
	}
	
	/**
	 * Gets the modality group.
	 *
	 * @param modalityGroupId the modality group id
	 * @return the modality group
	 * @throws ServiceException the service exception
	 */
	public ModalityGroup getModalityGroup(Long modalityGroupId) throws ServiceException{
		ModalityGroup modalityGroup = this.find(ModalityGroup.class, modalityGroupId);
		return modalityGroup;
	}
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @param mechanismOpeListId the mechanism ope list id
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> getMechanismOperation(List<Long> mechanismOpeListId) throws ServiceException{
		String Query = "Select new com.pradera.model.negotiation.MechanismOperation(mec.idMechanismOperationPk, mec.operationNumber, mec.operationState) " +
				"		From MechanismOperation mec Where mec.idMechanismOperationPk in :mechanismId";
		Query query = em.createQuery(Query);
		query.setParameter("mechanismId", mechanismOpeListId);
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<MechanismOperation>();
		}
	}
	
	/**
	 * Creates the initial participant positions.
	 *
	 * @param settlementProcess the settlement process
	 * @param mapParticipantOperations the map participant operations
	 * @throws ServiceException the service exception
	 */
	public void createInitialParticipantPositions(SettlementProcess settlementProcess, Map<Long, NetParticipantPositionTO> mapParticipantOperations) throws ServiceException {
		
		//we iterate operations by participant to create new ParticipantPositions wich indProcess is 0
		
		for(Long idParticipant: mapParticipantOperations.keySet()){
			
			ParticipantPosition participantPosition = new ParticipantPosition();
			participantPosition.setParticipant(new Participant(idParticipant));
			participantPosition.setSettlementProcess(settlementProcess);
			
			NetParticipantPositionTO netParticipantPositionTO = mapParticipantOperations.get(idParticipant);
			
			BigDecimal buyerPosition = netParticipantPositionTO.getPurchaseSettlementAmount();
			BigDecimal sellerPosition = netParticipantPositionTO.getSaleSettlementAmount();
			
			participantPosition.setBuyerPosition(buyerPosition);
			participantPosition.setSellerPosition(sellerPosition);
			participantPosition.setNetPosition(sellerPosition.subtract(buyerPosition));
			
			create(participantPosition);
		}
	}
	
	/**
	 * Save settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param settlementSchema the settlement schema
	 * @param countPendingOperations the count pending operations
	 * @param pendingOperations the pending operations
	 * @param processType the process type
	 * @param scheduleType the schedule type
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess saveSettlementProcess(Long idMechanism,
			Long idModalityGroup, Integer idCurrency, Date settlementDate, Integer settlementSchema, 
			Long countPendingOperations, List<Long> pendingOperations, Integer processType, Integer scheduleType) throws ServiceException {

		SettlementProcess objSettlementProcess = new SettlementProcess();

		ModalityGroup objModalityGroup = new ModalityGroup();
		objModalityGroup.setIdModalityGroupPk(idModalityGroup);
		objSettlementProcess.setModalityGroup(objModalityGroup);

		NegotiationMechanism objNegotiationMechanism = new NegotiationMechanism();
		objNegotiationMechanism.setIdNegotiationMechanismPk(idMechanism);
		objSettlementProcess.setNegotiationMechanism(objNegotiationMechanism);

		objSettlementProcess.setCurrency(idCurrency);
		objSettlementProcess.setSettlementDate(settlementDate);
		objSettlementProcess.setSettlementSchema(settlementSchema);
		objSettlementProcess.setPendingOperations(countPendingOperations);
		objSettlementProcess.setSettledOperations(new Long(0));
		objSettlementProcess.setCanceledOperations(new Long(0));
		objSettlementProcess.setTotalOperation(countPendingOperations);
		objSettlementProcess.setProcessNumber(getCorrelativeSettlementProcessNumber(scheduleType));
		objSettlementProcess.setProcessType(processType);
		objSettlementProcess.setProcessState(SettlementProcessStateType.IN_PROCESS.getCode());
		objSettlementProcess.setScheduleType(scheduleType);
			
		create(objSettlementProcess);
		
		if(pendingOperations!=null){
			for(Long idSettlementOperation : pendingOperations){
				updateSettlementProcess(objSettlementProcess.getIdSettlementProcessPk(), idSettlementOperation, OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode());
			}
		}

		return objSettlementProcess;
	}
	
	
	/**
	 * Gets the correlative settlement process number.
	 *
	 * @param scheduleType the schedule type
	 * @return the correlative settlement process number
	 */
	private Long getCorrelativeSettlementProcessNumber(Integer scheduleType) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT nvl(max(sp.processNumber),0) + 1 ");
		stringBuffer.append(" FROM SettlementProcess sp ");
		stringBuffer.append(" WHERE sp.scheduleType = :scheduleType ");
		stringBuffer.append(" and sp.processState = :processState ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("scheduleType", scheduleType);
		query.setParameter("processState", SettlementProcessStateType.FINISHED.getCode());
		
		return (Long)query.getSingleResult();
	}

	/**
	 * Gets the settlement schedule.
	 *
	 * @param scheduleType the schedule type
	 * @param currentTime the current time
	 * @param indSettlement the ind settlement
	 * @param indExtension the ind extension
	 * @return the settlement schedule
	 * @throws ServiceException the service exception
	 */
	public SettlementSchedule getSettlementSchedule(Integer scheduleType, Date currentTime, Integer indSettlement, Integer indExtension) throws ServiceException{
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT ss, (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ss.scheduleType ) ");
		stringBuffer.append(" FROM SettlementSchedule ss ");
		stringBuffer.append(" WHERE 1=1 ");
		if(scheduleType!=null){
			stringBuffer.append(" and ss.scheduleType = :scheduleType ");
		}
		if(currentTime!=null){
			stringBuffer.append(" and TO_NUMBER(TO_CHAR(:currentTime,'HH24MI')) BETWEEN TO_NUMBER(TO_CHAR(ss.startDate,'HH24MI')) AND TO_NUMBER(TO_CHAR(ss.endDate,'HH24MI'))");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		if(scheduleType!=null){
			query.setParameter("scheduleType", scheduleType);
		}
		if(currentTime!=null){
			query.setParameter("currentTime", currentTime,TemporalType.TIME);
		}
		
		try{
			Object[] objs = (Object[])query.getSingleResult();
			
			SettlementSchedule settlementSchedule = (SettlementSchedule)objs[0];
			settlementSchedule.setScheduleTypeDescription((String)objs[1]);
			
			if(indSettlement!=null){
				if(!settlementSchedule.getIndSettlement().equals(indSettlement)){
					if(!settlementSchedule.getIndExtendSettlement().equals(indSettlement)){
						throw new ServiceException(ErrorServiceType.SETTLEMENT_SCHEDULE_SETTLEMENT_NOT_PERMITTED,new Object[]{settlementSchedule.getScheduleTypeDescription()});
					}
				}
			}
			
			if(indExtension!=null && !settlementSchedule.getIndExtend().equals(indExtension)){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_SCHEDULE_EXTENSION_NOT_PERMITTED,new Object[]{settlementSchedule.getScheduleTypeDescription()});
			}
			
			return settlementSchedule;
		}catch(NoResultException ex){
			if (SettlementScheduleType.MELID.getCode().equals(scheduleType)) {
				throw new ServiceException(ErrorServiceType.SETTLEMENT_SCHEDULE_MELID_NOT_PERMITTED);
			}
			throw new ServiceException(ErrorServiceType.SETTLEMENT_SCHEDULE_NOT_FOUND);
		}catch(NonUniqueResultException ex){
			throw new ServiceException(ErrorServiceType.SETTLEMENT_SCHEDULE_MORE_THAN_ONE);
		}
	}
	
	/**
	 * Populate settlement operations.
	 *
	 * @param lstObjects the lst objects
	 * @param idModalityGroup the id modality group
	 * @param settlementSchema the settlement schema
	 * @param currency the currency
	 * @param indMarketFact the ind market fact
	 * @param lstCashSettlementOperation the lst cash settlement operation
	 * @param lstTermSettlementOperation the lst term settlement operation
	 * @param lstCashMechanismOperation the lst cash mechanism operation
	 * @param lstTermMechanismOperation the lst term mechanism operation
	 * @return the list
	 */
	public List<SettlementOperationTO> populateSettlementOperations(List<Object[]> lstObjects, Long idModalityGroup, Integer settlementSchema, 
										Integer currency, Integer indMarketFact, List<Long> lstCashSettlementOperation, 
										List<Long> lstTermSettlementOperation, List<Long> lstCashMechanismOperation, List<Long> lstTermMechanismOperation)
	{
		Map<Long,SettlementOperationTO> settlementOperations = new HashMap<Long, SettlementOperationTO>();
		List<SettlementOperationTO> listSettlementOperations = new ArrayList<>();
		
		for (Object[] arrObject: lstObjects)
		{
			Long idMechanismOperation = (Long)arrObject[0];
			Integer indPartial = (Integer)arrObject[30];
			Long idSettlementOperation = (Long)arrObject[23];
			SettlementOperationTO objSettlementOperationTO = settlementOperations.get(idSettlementOperation);
			if(objSettlementOperationTO == null){
				objSettlementOperationTO = setOperationData(arrObject, idModalityGroup, settlementSchema, currency);
				objSettlementOperationTO.setLstBuyerHolderAccounts(new ArrayList<SettlementHolderAccountTO>());
				objSettlementOperationTO.setLstSellerHolderAccounts(new ArrayList<SettlementHolderAccountTO>());
				
				settlementOperations.put(idSettlementOperation, objSettlementOperationTO);
				listSettlementOperations.add(objSettlementOperationTO);
				
				if (OperationPartType.CASH_PART.getCode().equals(objSettlementOperationTO.getOperationPart())) {
					lstCashSettlementOperation.add(idSettlementOperation);
					lstCashMechanismOperation.add(idMechanismOperation);
				} else if (OperationPartType.TERM_PART.getCode().equals(objSettlementOperationTO.getOperationPart())) {
					lstTermSettlementOperation.add(idSettlementOperation);
					if (BooleanType.NO.getCode().equals(indPartial)) {
						lstTermMechanismOperation.add(idMechanismOperation);
					}
				}
			}
			
			SettlementHolderAccountTO objSettlementHolderAccountTO = setHolderAccountData(arrObject,indMarketFact);
			
			//verificando si no es compra forzosa
			if(objSettlementOperationTO.getIndForcedPurchase() != 1) {
				if (ComponentConstant.SALE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstSellerHolderAccounts().add(objSettlementHolderAccountTO);
				} else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstBuyerHolderAccounts().add(objSettlementHolderAccountTO);
				}
			}else{
				if (ComponentConstant.SALE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstBuyerHolderAccounts().add(objSettlementHolderAccountTO);
				} else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstSellerHolderAccounts().add(objSettlementHolderAccountTO);
				}
			}
			
		}
		return listSettlementOperations;
	}

	
	/**
	 * 
	 * @param lstObjects
	 * @param idModalityGroup
	 * @param settlementSchema
	 * @param currency
	 * @param indMarketFact
	 * @param lstCashSettlementOperation
	 * @param lstTermSettlementOperation
	 * @param lstCashMechanismOperation
	 * @param lstTermMechanismOperation
	 * @return
	 */
	public List<SettlementOperationTO> populateSettlementOperationsUnfulfillment(List<Object[]> lstObjects, Long idModalityGroup, Integer settlementSchema, 
										Integer currency, Integer indMarketFact, List<Long> lstCashSettlementOperation, 
										List<Long> lstTermSettlementOperation, List<Long> lstCashMechanismOperation, List<Long> lstTermMechanismOperation,
										Integer indBuyerSeller)
	{
		Map<Long,SettlementOperationTO> settlementOperations = new HashMap<Long, SettlementOperationTO>();
		List<SettlementOperationTO> listSettlementOperations = new ArrayList<>();
		
		for (Object[] arrObject: lstObjects)
		{
			Long idMechanismOperation = Long.valueOf(arrObject[0].toString());
			Integer indPartial = Integer.valueOf(arrObject[30].toString());
			Long idSettlementOperation = Long.valueOf(arrObject[23].toString());
			SettlementOperationTO objSettlementOperationTO = settlementOperations.get(idSettlementOperation);
			if(objSettlementOperationTO == null){
				objSettlementOperationTO = setOperationDataUnfulfillment(arrObject, idModalityGroup, settlementSchema, currency);
				objSettlementOperationTO.setLstBuyerHolderAccounts(new ArrayList<SettlementHolderAccountTO>());
				objSettlementOperationTO.setLstSellerHolderAccounts(new ArrayList<SettlementHolderAccountTO>());
				
				objSettlementOperationTO.setSettlementType(SettlementType.FOP.getCode());
				
				if(indBuyerSeller.equals(GeneralConstants.ZERO_VALUE_INTEGER)) {
					objSettlementOperationTO.setIndForcedPurchase(GeneralConstants.ONE_VALUE_INTEGER);
				}else {
					objSettlementOperationTO.setIndForcedPurchase(GeneralConstants.ZERO_VALUE_INTEGER);
				}
				
				settlementOperations.put(idSettlementOperation, objSettlementOperationTO);
				listSettlementOperations.add(objSettlementOperationTO);
				
				if (OperationPartType.CASH_PART.getCode().equals(objSettlementOperationTO.getOperationPart())) {
					lstCashSettlementOperation.add(idSettlementOperation);
					lstCashMechanismOperation.add(idMechanismOperation);
				} else if (OperationPartType.TERM_PART.getCode().equals(objSettlementOperationTO.getOperationPart())) {
					lstTermSettlementOperation.add(idSettlementOperation);
					if (BooleanType.NO.getCode().equals(indPartial)) {
						lstTermMechanismOperation.add(idMechanismOperation);
					}
				}
			}
			
			SettlementHolderAccountTO objSettlementHolderAccountTO = setHolderAccountDataUnfulfillment(arrObject,indMarketFact);
			
			//verificando si no es compra forzosa
			if(objSettlementOperationTO.getIndForcedPurchase() != 1) {
				if (ComponentConstant.SALE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstSellerHolderAccounts().add(objSettlementHolderAccountTO);
				} else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstBuyerHolderAccounts().add(objSettlementHolderAccountTO);
				}
			}else{
				if (ComponentConstant.SALE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstBuyerHolderAccounts().add(objSettlementHolderAccountTO);
				} else if (ComponentConstant.PURCHARSE_ROLE.equals(objSettlementHolderAccountTO.getRole())) {
					objSettlementOperationTO.getLstSellerHolderAccounts().add(objSettlementHolderAccountTO);
				}
			}
			
		}
		return listSettlementOperations;
	}
	
	
	
	
	/**
	 * Sets the operation data.
	 *
	 * @param arrObject the arr object
	 * @param idModalityGroup the id modality group
	 * @param settlementSchema the settlement schema
	 * @param currency the currency
	 * @return the settlement operation to
	 */
	private SettlementOperationTO setOperationData(Object[] arrObject, Long idModalityGroup, Integer settlementSchema, Integer currency)
	{
		SettlementOperationTO objSettlementOperationTO = new SettlementOperationTO();
		objSettlementOperationTO.setIdMechanismOperation((Long)arrObject[0]);
		objSettlementOperationTO.setIdMechanism((Long)arrObject[1]);
		objSettlementOperationTO.setIdModality((Long)arrObject[2]);
		objSettlementOperationTO.setIndReportingBalance((Integer)arrObject[3]);
		objSettlementOperationTO.setIndPrincipalGuarantee((Integer)arrObject[4]);
		objSettlementOperationTO.setIndMarginGuarantee((Integer)arrObject[5]);
		objSettlementOperationTO.setIndTermSettlement((Integer)arrObject[6]);
		objSettlementOperationTO.setIndCashStockBlock((Integer)arrObject[7]);
		objSettlementOperationTO.setIndTermStockBlock((Integer)arrObject[8]);
		objSettlementOperationTO.setIndPrimaryPlacement((Integer)arrObject[9]);
		objSettlementOperationTO.setIdSecurityCode((String)arrObject[11]);
		objSettlementOperationTO.setSettlementType((Integer)arrObject[12]);
		if (Validations.validateIsNotNull(arrObject[13])){
			objSettlementOperationTO.setIdReferencedOperation(new Long(arrObject[13].toString()));
		}
		objSettlementOperationTO.setInstrumentType((Integer)arrObject[14]);
		objSettlementOperationTO.setCurrentNominalValue(arrObject[15] == null ? BigDecimal.ZERO : (BigDecimal)arrObject[15]);
		objSettlementOperationTO.setOperationPart((Integer)arrObject[20]);
		objSettlementOperationTO.setIdSettlementOperation((Long)arrObject[23]);
		objSettlementOperationTO.setStockQuantity((BigDecimal)arrObject[24]);
		objSettlementOperationTO.setOperationAmount((BigDecimal)arrObject[25]);
		objSettlementOperationTO.setIndPartial((Integer)arrObject[30]);
		objSettlementOperationTO.setIndDematerialization((Integer)arrObject[31]);
		objSettlementOperationTO.setIndForcedPurchase((Integer)arrObject[32]);
		if (Validations.validateIsNotNull(arrObject[33])) {
			objSettlementOperationTO.setInterfaceTransferCode(arrObject[33].toString());
		}
		objSettlementOperationTO.setSecurityClass(new Integer(arrObject[34].toString()));
		objSettlementOperationTO.setOperationNumber(new Long(arrObject[35].toString()));
		objSettlementOperationTO.setSettlementSchema(settlementSchema);
		objSettlementOperationTO.setCurrency(currency);
		objSettlementOperationTO.setIdModalityGroup(idModalityGroup);
		
		return objSettlementOperationTO;
	}
	
	/**
	 * Sets the operation data.
	 *
	 * @param arrObject the arr object
	 * @param idModalityGroup the id modality group
	 * @param settlementSchema the settlement schema
	 * @param currency the currency
	 * @return the settlement operation to
	 */
	private SettlementOperationTO setOperationDataUnfulfillment(Object[] arrObject, Long idModalityGroup, Integer settlementSchema, Integer currency)
	{
		SettlementOperationTO objSettlementOperationTO = new SettlementOperationTO();
		objSettlementOperationTO.setIdMechanismOperation(Long.valueOf(arrObject[0].toString()));
		objSettlementOperationTO.setIdMechanism(Long.valueOf(arrObject[1].toString()));
		objSettlementOperationTO.setIdModality(Long.valueOf(arrObject[2].toString()));
		objSettlementOperationTO.setIndReportingBalance(Integer.valueOf(arrObject[3].toString()));
		objSettlementOperationTO.setIndPrincipalGuarantee(Integer.valueOf(arrObject[4].toString()));
		objSettlementOperationTO.setIndMarginGuarantee(Integer.valueOf(arrObject[5].toString()));
		objSettlementOperationTO.setIndTermSettlement(Integer.valueOf(arrObject[6].toString()));
		objSettlementOperationTO.setIndCashStockBlock(Integer.valueOf(arrObject[7].toString()));
		objSettlementOperationTO.setIndTermStockBlock(Integer.valueOf(arrObject[8].toString()));
		objSettlementOperationTO.setIndPrimaryPlacement(Integer.valueOf(arrObject[9].toString()));
		objSettlementOperationTO.setIdSecurityCode(arrObject[11].toString());
		objSettlementOperationTO.setSettlementType(Integer.valueOf(arrObject[12].toString()));
		if (Validations.validateIsNotNull(arrObject[13])){
			objSettlementOperationTO.setIdReferencedOperation(new Long(arrObject[13].toString()));
		}
		objSettlementOperationTO.setInstrumentType(Integer.valueOf(arrObject[14].toString()));
		objSettlementOperationTO.setCurrentNominalValue(arrObject[15] == null ? BigDecimal.ZERO : (BigDecimal)arrObject[15]);
		objSettlementOperationTO.setOperationPart(Integer.valueOf(arrObject[20].toString()));
		objSettlementOperationTO.setIdSettlementOperation(Long.valueOf(arrObject[23].toString()));
		objSettlementOperationTO.setStockQuantity((BigDecimal)arrObject[24]);
		objSettlementOperationTO.setOperationAmount((BigDecimal)arrObject[25]);
		objSettlementOperationTO.setIndPartial(Integer.valueOf(arrObject[30].toString()));
		objSettlementOperationTO.setIndDematerialization(Integer.valueOf(arrObject[31].toString()));
		objSettlementOperationTO.setIndForcedPurchase(Integer.valueOf(arrObject[32].toString()));
		if (Validations.validateIsNotNull(arrObject[33])) {
			objSettlementOperationTO.setInterfaceTransferCode(arrObject[33].toString());
		}
		objSettlementOperationTO.setSecurityClass(new Integer(arrObject[34].toString()));
		objSettlementOperationTO.setOperationNumber(new Long(arrObject[35].toString()));
		objSettlementOperationTO.setSettlementSchema(settlementSchema);
		objSettlementOperationTO.setCurrency(currency);
		objSettlementOperationTO.setIdModalityGroup(idModalityGroup);
		
		return objSettlementOperationTO;
	}
	
	/**
	 * Sets the holder account data.
	 *
	 * @param arrObject the arr object
	 * @param indMarketFact the ind market fact
	 * @return the settlement holder account to
	 */
	@SuppressWarnings("unused")
	private SettlementHolderAccountTO setHolderAccountData(Object[] arrObject, Integer indMarketFact)
	{
		SettlementHolderAccountTO objSettlementHolderAccountTO = new SettlementHolderAccountTO();
		objSettlementHolderAccountTO.setIdSecurityCode((String)arrObject[11]);
		objSettlementHolderAccountTO.setIdHolderAccountOperation((Long)arrObject[16]);
		objSettlementHolderAccountTO.setIdParticipant((Long)arrObject[17]);
		objSettlementHolderAccountTO.setIdHolderAccount((Long)arrObject[18]);
		objSettlementHolderAccountTO.setRole((Integer)arrObject[19]);
		objSettlementHolderAccountTO.setOperationPart((Integer)arrObject[20]);
		if (Validations.validateIsNotNull(arrObject[22])){
			objSettlementHolderAccountTO.setIdRefAccountOperation((Long)arrObject[22]);
		}
		objSettlementHolderAccountTO.setIdSettlementAccountOperation((Long)arrObject[26]);		
		objSettlementHolderAccountTO.setStockQuantity((BigDecimal)arrObject[27]);
		if (Validations.validateIsNotNull(arrObject[28])){
			objSettlementHolderAccountTO.setIdRefSettlementAccountOperation((Long)arrObject[28]);
		}
		objSettlementHolderAccountTO.setChainedQuantity((BigDecimal)arrObject[29]);
		if(ComponentConstant.ONE.equals(indMarketFact)){
			objSettlementHolderAccountTO.setMarketFactAccounts(new ArrayList<MarketFactAccountTO>(0));
			List<SettlementAccountMarketfact> objMarketFacts = getSettlementAccountMarketfacts(objSettlementHolderAccountTO.getIdSettlementAccountOperation());
			for(SettlementAccountMarketfact objMarketFact : objMarketFacts){
				MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
				marketFactAccountTO.setMarketDate(objMarketFact.getMarketDate());
				Integer intrumentType = (Integer)arrObject[14];
				//if(intrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
					marketFactAccountTO.setMarketRate(objMarketFact.getMarketRate());
				//}else if(intrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
					marketFactAccountTO.setMarketPrice(objMarketFact.getMarketPrice());
				//}
				marketFactAccountTO.setQuantity(objMarketFact.getMarketQuantity());
				marketFactAccountTO.setChainedQuantity(objMarketFact.getChainedQuantity());
				objSettlementHolderAccountTO.getMarketFactAccounts().add(marketFactAccountTO);
			}
		}
		return objSettlementHolderAccountTO;
	}
	
	/**
	 * Sets the holder account data.
	 *
	 * @param arrObject the arr object
	 * @param indMarketFact the ind market fact
	 * @return the settlement holder account to
	 */
	@SuppressWarnings("unused")
	private SettlementHolderAccountTO setHolderAccountDataUnfulfillment(Object[] arrObject, Integer indMarketFact)
	{
		SettlementHolderAccountTO objSettlementHolderAccountTO = new SettlementHolderAccountTO();
		objSettlementHolderAccountTO.setIdSecurityCode(arrObject[11].toString());
		objSettlementHolderAccountTO.setIdHolderAccountOperation(Long.valueOf(arrObject[16].toString()));
		objSettlementHolderAccountTO.setIdParticipant(Long.valueOf(arrObject[17].toString()));
		objSettlementHolderAccountTO.setIdHolderAccount(Long.valueOf(arrObject[18].toString()));
		objSettlementHolderAccountTO.setRole(Integer.valueOf(arrObject[19].toString()));
		objSettlementHolderAccountTO.setOperationPart(Integer.valueOf(arrObject[20].toString()));
		if (Validations.validateIsNotNull(arrObject[22])){
			objSettlementHolderAccountTO.setIdRefAccountOperation(Long.valueOf(arrObject[22].toString()));
		}
		objSettlementHolderAccountTO.setIdSettlementAccountOperation(Long.valueOf(arrObject[26].toString()));		
		objSettlementHolderAccountTO.setStockQuantity((BigDecimal)arrObject[27]);
		if (Validations.validateIsNotNull(arrObject[28])){
			objSettlementHolderAccountTO.setIdRefSettlementAccountOperation(Long.valueOf(arrObject[28].toString()));
		}
		objSettlementHolderAccountTO.setChainedQuantity((BigDecimal)arrObject[29]);
		if(ComponentConstant.ONE.equals(indMarketFact)){
			objSettlementHolderAccountTO.setMarketFactAccounts(new ArrayList<MarketFactAccountTO>(0));
			List<SettlementAccountMarketfact> objMarketFacts = getSettlementAccountMarketfacts(objSettlementHolderAccountTO.getIdSettlementAccountOperation());
			for(SettlementAccountMarketfact objMarketFact : objMarketFacts){
				MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
				marketFactAccountTO.setMarketDate(objMarketFact.getMarketDate());
				Integer intrumentType = Integer.valueOf(arrObject[14].toString());
				//if(intrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
					marketFactAccountTO.setMarketRate(objMarketFact.getMarketRate());
				//}else if(intrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
					marketFactAccountTO.setMarketPrice(objMarketFact.getMarketPrice());
				//}
				marketFactAccountTO.setQuantity(objMarketFact.getMarketQuantity());
				marketFactAccountTO.setChainedQuantity(objMarketFact.getChainedQuantity());
				objSettlementHolderAccountTO.getMarketFactAccounts().add(marketFactAccountTO);
			}
		}
		return objSettlementHolderAccountTO;
	}
	
	/**
	 * Gets the mechanism operation market facts.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the mechanism operation market facts
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationMarketFact> getMechanismOperationMarketFacts(Long idMechanismOperation) {
		List<MechanismOperationMarketFact> lstObjects = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select mom "); // ID Mechanism operation
		stringBuffer.append(" from MechanismOperationMarketFact mom where ");
		stringBuffer.append(" mom.mechanismOperation.idMechanismOperationPk = :idMechanismOperation "); 
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	/**
	 * Gets the settlement account marketfacts.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @return the settlement account marketfacts
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementAccountMarketfact> getSettlementAccountMarketfacts(Long idSettlementAccountOperation) {
		List<SettlementAccountMarketfact> lstObjects = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select sam "); // ID Mechanism operation
		stringBuffer.append(" from SettlementAccountMarketfact sam where ");
		stringBuffer.append(" sam.indActive = :indicatorOne and ");
		stringBuffer.append(" sam.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccount "); 
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idSettlementAccount", idSettlementAccountOperation);
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	/**
	 * Gets the settlement account marketfacts.
	 *
	 * @param idSettlementAccount the id settlement account
	 * @param marketDate the market date
	 * @param marketRate the market rate
	 * @param marketPrice the market price
	 * @return the settlement account marketfacts
	 */
	public SettlementAccountMarketfact getSettlementAccountMarketfacts(Long idSettlementAccount, Date marketDate,BigDecimal marketRate, BigDecimal marketPrice) {
		StringBuffer stringBuffer = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		stringBuffer.append(" select sam "); // ID Mechanism operation
		stringBuffer.append(" from SettlementAccountMarketfact sam where ");
		stringBuffer.append(" sam.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccount ");
		stringBuffer.append(" and trunc(sam.marketDate) = trunc(:marketDate) ");
		stringBuffer.append(" and sam.indActive = :indActive ");
		if(marketRate!=null){
			stringBuffer.append(" and sam.marketRate = :marketRate ");
			parameters.put("marketRate", marketRate);
		}else{
			stringBuffer.append(" and sam.marketRate is null ");
		}
		if(marketPrice!=null){
			stringBuffer.append(" and sam.marketPrice = :marketPrice ");
			parameters.put("marketPrice", marketPrice);
		}else{
			stringBuffer.append(" and sam.marketPrice is null ");
		}
		
		parameters.put("idSettlementAccount", idSettlementAccount);
		parameters.put("marketDate", marketDate);
		parameters.put("indActive", BooleanType.YES.getCode());
		
		return (SettlementAccountMarketfact) findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the account operation marketfacts.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @return the account operation marketfacts
	 */
	@SuppressWarnings("unchecked")
	public List<AccountOperationMarketFact> getAccountOperationMarketfacts(Long idHolderAccountOperation) {
		List<AccountOperationMarketFact> lstObjects = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select aom "); // ID Mechanism operation
		stringBuffer.append(" from AccountOperationMarketFact aom where ");
		stringBuffer.append(" aom.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation "); 
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}

	/**
	 * Update settlement process.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idSettlementOperation the id settlement operation
	 * @param idOperationSettlementState the id operation settlement state
	 * @throws ServiceException the service exception
	 */
	public void updateSettlementProcess(Long idSettlementProcess, Long idSettlementOperation, Integer idOperationSettlementState) throws ServiceException
	{
		try {
			//we get the operation settlement information
			OperationSettlement objOperationSettlement = getOperationSettlement(idSettlementProcess, idSettlementOperation);
			if (Validations.validateIsNull(objOperationSettlement)) {
				//we create the settlement information about this mechanism operation
				objOperationSettlement = createOperationSettlement(idSettlementProcess, idSettlementOperation, idOperationSettlementState);
			} else {
				objOperationSettlement.setOperationState(idOperationSettlementState);
				update(objOperationSettlement);
			}
			//if the operation was settled then we update the count
			if (OperationSettlementSateType.SETTLED.getCode().equals(idOperationSettlementState)) {
				updateSettlementSettledCount(idSettlementProcess);
			}
		} catch (ServiceException e) {
			throw e;
		}
	}
	
	/**
	 * Creates the operation settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idSettlementOperation the id settlement operation
	 * @param idOperationSettlementState the id operation settlement state
	 * @return the operation settlement
	 */
	public OperationSettlement createOperationSettlement(Long idSettlementProcess, Long idSettlementOperation, Integer idOperationSettlementState){
		//we create the settlement information about this mechanism operation
		OperationSettlement objOperationSettlement = new OperationSettlement();
		objOperationSettlement.setOperationState(idOperationSettlementState);
		SettlementOperation settlementOperation = new SettlementOperation();
		settlementOperation.setIdSettlementOperationPk(idSettlementOperation);
		objOperationSettlement.setSettlementOperation(settlementOperation);
		SettlementProcess objSettlementProcess = new SettlementProcess();
		objSettlementProcess.setIdSettlementProcessPk(idSettlementProcess);
		objOperationSettlement.setSettlementProcess(objSettlementProcess);
		create(objOperationSettlement);
		return objOperationSettlement;
	}
	
	/**
	 * Populate net settlement operations.
	 *
	 * @param settlementDate the settlement date
	 * @param mechanismId the mechanism id
	 * @param modalityGroupId the modality group id
	 * @param currency the currency
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @throws ServiceException the service exception
	 */
	public void populateNetSettlementOperations(Date settlementDate, Long mechanismId, 
			Long modalityGroupId, Integer currency, NetSettlementAttributesTO netSettlementAttributesTO) throws ServiceException {
		
		//remove operations by securities
		SettlementProcess settlementProcess = netSettlementAttributesTO.getSettlementProcess();
		boolean extended = false;
		if(ComponentConstant.ONE.equals(netSettlementAttributesTO.getSettlementSchedule().getIndExtendSettlement())){
			extended = true;
		}
		boolean settlement = false;
		if(ComponentConstant.ONE.equals(netSettlementAttributesTO.getSettlementSchedule().getIndSettlement())){
			settlement = true;
		}
		
		List<Object[]> pendingByStockObjects = getListNetPendingOperations(settlementDate, mechanismId, modalityGroupId, currency,true,false,false,settlement,extended, null);
		
		List<NetSettlementOperationTO> operationsExcludedByStocks = NegotiationUtils.populateSettlementOperationsAndParticipant(pendingByStockObjects);
		
		for(NetSettlementOperationTO settlementOperation: operationsExcludedByStocks){
			OperationSettlement operationSettlement = new OperationSettlement();
			operationSettlement.setSettlementProcess(settlementProcess);
			operationSettlement.setOperationState(OperationSettlementSateType.WITHDRAWN_BY_SECURITIES.getCode());
			operationSettlement.setInitialState(operationSettlement.getOperationState());
			operationSettlement.setSettlementOperation(new SettlementOperation(settlementOperation.getIdSettlementOperation()));
			settlementOperation.setOperationSettlement(operationSettlement);
			netSettlementAttributesTO.getLstStockRemovedOperations().add(settlementOperation);
			settlementProcess.getOperationSettlements().add(operationSettlement);
		}
		
		//remove operations by guarantees
		List<Object[]> pendingByGuaranteesObjects = getListNetPendingOperations(settlementDate, mechanismId, modalityGroupId, currency, false,true,false,settlement,extended, null);
		List<NetSettlementOperationTO> operationsExcludedByGuarantees = NegotiationUtils.populateSettlementOperationsAndParticipant(pendingByGuaranteesObjects);
		
		for(NetSettlementOperationTO settlementOperation: operationsExcludedByGuarantees){
			OperationSettlement operationSettlement = new OperationSettlement();
			operationSettlement.setSettlementProcess(settlementProcess);
			operationSettlement.setOperationState(OperationSettlementSateType.WITHDRAWN_BY_GUARANTEES.getCode());
			operationSettlement.setInitialState(operationSettlement.getOperationState());
			operationSettlement.setSettlementOperation(new SettlementOperation(settlementOperation.getIdSettlementOperation()));
			settlementOperation.setOperationSettlement(operationSettlement);
			netSettlementAttributesTO.getLstGuaranteeRemovedOperations().add(settlementOperation);
			settlementProcess.getOperationSettlements().add(operationSettlement);
		}
		
		// get funds pending operations
		List<Object[]> pendingByFundsObjects = getListNetPendingOperations(settlementDate, mechanismId,modalityGroupId, currency, false,false,true,settlement,extended, null);
		//we add the chained operations
		pendingByFundsObjects.addAll(getListChainedOperation(settlementDate, mechanismId, modalityGroupId, currency,settlement,extended));
		List<NetSettlementOperationTO> operationsExcludedByFunds = NegotiationUtils.populateSettlementOperationsAndParticipant(pendingByFundsObjects);
		
		for(NetSettlementOperationTO settlementOperation: operationsExcludedByFunds){
			OperationSettlement operationSettlement = new OperationSettlement();
			operationSettlement.setSettlementProcess(settlementProcess);
			operationSettlement.setOperationState(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode());
			operationSettlement.setInitialState(operationSettlement.getOperationState());
			operationSettlement.setSettlementOperation(new SettlementOperation(settlementOperation.getIdSettlementOperation()));
			settlementOperation.setOperationSettlement(operationSettlement);
			netSettlementAttributesTO.getLstPendingSettlementOperations().add(settlementOperation);
			settlementProcess.getOperationSettlements().add(operationSettlement);
		}
		
		Long pending = (long) operationsExcludedByFunds.size();
		Long removed = (long) (operationsExcludedByGuarantees.size()+operationsExcludedByStocks.size());
		Long total = pending + removed;
		
		settlementProcess.setCanceledOperations(0l);
		settlementProcess.setPendingOperations(pending);
		settlementProcess.setRemovedOperations(removed);
		settlementProcess.setSettledOperations(0l);
		settlementProcess.setTotalOperation(total);
		
		return;
	}

	/**
	 * Validate account market fact consistency.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @return the list
	 */
	public List<String> validateAccountMarketFactConsistency(Long assignmentProcessId,Long participantAssignmentId){
		
		List<Object[]> accountOperations = getHolderAccountOperationsData(assignmentProcessId,participantAssignmentId);
		
		Map<Long,String> inconsistencies = new HashMap<Long,String>();
		for(Object[] objAccountOperation : accountOperations){
			Long idHolderAccountOperation = (Long)objAccountOperation[0];
			boolean inconsistent = true;
			
			Integer instrumentType= (Integer)objAccountOperation[1];
			Long idMechanismOperation= (Long)objAccountOperation[2];
			Long ballotNumber= (Long)objAccountOperation[4];
			Long sequential= (Long)objAccountOperation[5];
			String modalityName = (String)objAccountOperation[6];
			//Long idSettlementAccount = (Long)objAccountOperation[7];
			Long operationNumber = (Long)objAccountOperation[3];
			
			List<AccountOperationMarketFact> accountMarketFacts = getAccountOperationMarketfacts(idHolderAccountOperation);
			
			inconsistent = isInconsistentAccountOperation(idHolderAccountOperation,accountMarketFacts,instrumentType);
			
			if(inconsistent){
				StringBuilder sb = new StringBuilder();
				sb.append(modalityName).append(GeneralConstants.BLANK_SPACE);
				if(ballotNumber != null && sequential != null){
					sb.append(ballotNumber.toString()).append(GeneralConstants.SLASH).append(sequential.toString());
				}else{
					sb.append(operationNumber.toString());
				}
				
				inconsistencies.put(idMechanismOperation,sb.toString());
			}
		}
		return new ArrayList<String>(inconsistencies.values());
	}
		
	/**
	 * Checks if is inconsistent account operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param accountMarketfacts the account marketfacts
	 * @param instrumentType the instrument type
	 * @return true, if is inconsistent account operation
	 */
	public boolean isInconsistentAccountOperation(
			Long idHolderAccountOperation, List<AccountOperationMarketFact> accountMarketfacts , Integer instrumentType) {
		boolean inconsistent = true;
		List<Object[]> accountBalances = getBalanceFromAccountOperation(idHolderAccountOperation,instrumentType);
		if(Validations.validateListIsNotNullAndNotEmpty(accountBalances)){
			if(accountMarketfacts.size()<accountBalances.size()){
				for(AccountOperationMarketFact accountOperationMarketFact: accountMarketfacts){
					for(Object[] accountMarketFactBalance : accountBalances){					
						{
							Date marketDateBal = (Date) accountMarketFactBalance[2];
							BigDecimal marketRateBal = (BigDecimal) accountMarketFactBalance[3];
							BigDecimal marketPriceBal = (BigDecimal) accountMarketFactBalance[4];
							if(accountOperationMarketFact.getMarketDate().compareTo(marketDateBal) == 0 && 
								((InstrumentType.FIXED_INCOME.getCode().equals(instrumentType) && accountOperationMarketFact.getMarketRate().compareTo(marketRateBal) == 0) ||
								(InstrumentType.VARIABLE_INCOME.getCode().equals(instrumentType) && accountOperationMarketFact.getMarketPrice().compareTo(marketPriceBal) == 0 ))){
								inconsistent = false;
								break;
							}
						}
					}
				}
			}else{
				Map<Long,Boolean> mpValidateInconsistent = new HashMap<Long,Boolean>();
				for(AccountOperationMarketFact accountOperationMarketFact: accountMarketfacts){
					mpValidateInconsistent.put(accountOperationMarketFact.getIdAccountOperMarketFactPk(), Boolean.TRUE);
				}
				for(AccountOperationMarketFact accountOperationMarketFact: accountMarketfacts){
					for(Object[] accountMarketFactBalance : accountBalances){					
						{
							Date marketDateBal = (Date) accountMarketFactBalance[2];
							BigDecimal marketRateBal = (BigDecimal) accountMarketFactBalance[3];
							BigDecimal marketPriceBal = (BigDecimal) accountMarketFactBalance[4];
							if(accountOperationMarketFact.getMarketDate().compareTo(marketDateBal) == 0 && 
								((InstrumentType.FIXED_INCOME.getCode().equals(instrumentType) && accountOperationMarketFact.getMarketRate().compareTo(marketRateBal) == 0) ||
								(InstrumentType.VARIABLE_INCOME.getCode().equals(instrumentType) && accountOperationMarketFact.getMarketPrice().compareTo(marketPriceBal) == 0 ))){
								mpValidateInconsistent.put(accountOperationMarketFact.getIdAccountOperMarketFactPk(), Boolean.FALSE);
								break;
							}
						}
					}
				}
				if(mpValidateInconsistent.containsValue(Boolean.TRUE));
				else{
					inconsistent = false;
				}
			}			
		}
		return inconsistent;
	}


	/**
	 * Gets the holder account operations data.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @return the holder account operations data
	 */
	@SuppressWarnings("unchecked")
	private List<Object[]> getHolderAccountOperationsData(Long assignmentProcessId, Long participantAssignmentId ) {
		StringBuilder sbQuery =  new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT hao.idHolderAccountOperationPk,  mo.securities.instrumentType, ");
		sbQuery.append("	mo.idMechanismOperationPk, mo.operationNumber, mo.ballotNumber, mo.sequential , mo.mechanisnModality.negotiationModality.modalityName ");
		sbQuery.append("	FROM HolderAccountOperation hao , MechanismOperation mo , AssignmentProcess ap ");
			
		if(participantAssignmentId!=null){
			sbQuery.append(" , ParticipantAssignment pa ");
			sbQuery.append("	where pa.assignmentProcess.idAssignmentProcessPk = ap.idAssignmentProcessPk ");
			sbQuery.append("	and pa.idParticipantAssignmentPk = :idParticipantAssignmentPk ");
			sbQuery.append("	and pa.participant.idParticipantPk =  mo.sellerParticipant.idParticipantPk ");
			sbQuery.append("    and "); 
			parameters.put("idParticipantAssignmentPk", participantAssignmentId);
		}else{
			sbQuery.append(" where "); 
		}
		
		sbQuery.append("    ap.idAssignmentProcessPk = :idAssignmentProcessPk");
		sbQuery.append("	and mo.assignmentProcess.idAssignmentProcessPk =  ap.idAssignmentProcessPk ");
		sbQuery.append("	and hao.mechanismOperation.idMechanismOperationPk =  mo.idMechanismOperationPk ");
		sbQuery.append("	and hao.holderAccountState = :stateRegistered");
		sbQuery.append("	and mo.operationState = :operationState");
		sbQuery.append("	and hao.operationPart = :cashPart");
		sbQuery.append("	and hao.role = :role ");
		sbQuery.append("	and mo.securities.securityClass not in (:lstSecurityClass) ");
		sbQuery.append("	and not exists ( ");
		sbQuery.append("		select hcd.idHolderChainDetailPk from HolderChainDetail hcd ");
		sbQuery.append("		inner join hcd.chainedHolderOperation cho ");
		sbQuery.append("		where hcd.settlementAccountMarketfact.settlementAccountOperation.holderAccountOperation.idHolderAccountOperationPk = hao.idHolderAccountOperationPk ");
		sbQuery.append("		and cho.chaintState in  (:includedStates)  ) ");
		
		parameters.put("idAssignmentProcessPk", assignmentProcessId);
		parameters.put("stateRegistered", HolderAccountOperationStateType.REGISTERED.getCode());
		parameters.put("operationState", MechanismOperationStateType.REGISTERED_STATE.getCode());
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("role", ComponentConstant.SALE_ROLE);
		List<Integer> includedStates= new ArrayList<Integer>();
		includedStates.add(ChainedOperationStateType.REGISTERED.getCode());
		includedStates.add(ChainedOperationStateType.CONFIRMED.getCode());
		parameters.put("includedStates",includedStates);
		List<Integer> lstSecurityClass= new ArrayList<Integer>();
		lstSecurityClass.add(SecurityClassType.DPA.getCode());
		lstSecurityClass.add(SecurityClassType.DPF.getCode());
		parameters.put("lstSecurityClass", lstSecurityClass);
		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the balance from account operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param instrumentType the instrument type
	 * @return the balance from account operation
	 */
	@SuppressWarnings("unchecked")
	private List<Object[]> getBalanceFromAccountOperation(Long idHolderAccountOperation, Integer instrumentType) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT hab.availableBalance,hmb.availableBalance, "); //0 1
		stringBuffer.append(" 		 hmb.marketDate,hmb.marketRate, hmb.marketPrice "); //0 1 
		stringBuffer.append(" from HolderAccountBalance hab , HolderMarketFactBalance hmb , MechanismOperation mo, HolderAccountOperation hao ");
		stringBuffer.append(" where mo.idMechanismOperationPk = hao.mechanismOperation.idMechanismOperationPk "); 
		stringBuffer.append(" and hab.id.idHolderAccountPk = hao.holderAccount.idHolderAccountPk ");
		stringBuffer.append(" and hab.id.idParticipantPk = hao.inchargeStockParticipant.idParticipantPk "); 
		stringBuffer.append(" and hab.id.idSecurityCodePk = mo.securities.idSecurityCodePk "); 
		stringBuffer.append(" and hab.id.idHolderAccountPk = hmb.holderAccount.idHolderAccountPk ");
		stringBuffer.append(" and hab.id.idParticipantPk = hmb.participant.idParticipantPk "); 
		stringBuffer.append(" and hab.id.idSecurityCodePk = hmb.security.idSecurityCodePk ");
		stringBuffer.append(" and hmb.indActive = :indicatorOne ");
		stringBuffer.append(" and hao.idHolderAccountOperationPk = :idHolderAccountOperation ");
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		
		return (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}

	/**
	 * Gets the pending assignment operations.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @param role the role
	 * @return the pending assignment operations
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getPendingAssignmentOperations(Long assignmentProcessId, Long participantAssignmentId, Integer role) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT distinct mo.operationNumber, mo.ballotNumber, mo.sequential , mo.mechanisnModality.negotiationModality.modalityName "); //0 1  2
		sbQuery.append(" from AssignmentRequest ar , MechanismOperation mo , AssignmentProcess ap ");
		
		if(participantAssignmentId!=null){
			sbQuery.append(" , ParticipantAssignment pa ");
			sbQuery.append("	where pa.assignmentProcess.idAssignmentProcessPk = ap.idAssignmentProcessPk ");
			sbQuery.append("	and ar.participantAssignment.idParticipantAssignmentPk = pa.idParticipantAssignmentPk ");
			sbQuery.append("	and pa.idParticipantAssignmentPk = :idParticipantAssignmentPk ");
			sbQuery.append(" 	and ");
			parameters.put("idParticipantAssignmentPk", participantAssignmentId);
		}else{
			sbQuery.append(" where "); 
		}
		
		sbQuery.append(" ap.idAssignmentProcessPk = mo.assignmentProcess.idAssignmentProcessPk ");
		sbQuery.append(" and ar.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk ");
		sbQuery.append(" and ar.requestState = :pendingState ");
		sbQuery.append(" and ar.role = :role ");
		sbQuery.append(" and ap.idAssignmentProcessPk = :idAssignmentProcessPk ");
		parameters.put("pendingState", AssignmentRequestStateType.PENDING.getCode());
		parameters.put("role", role);
		parameters.put("idAssignmentProcessPk", assignmentProcessId);
		
		return (List<Object[]>) findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the IncompleteSecurities operations.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @param role the role
	 * @return assignment operations
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getIncompleteSecurities(Long negotiationModality, Long participantAssignmentId, Integer role, Long idParticipant, Date dateSelected) {
		
		// Oscar
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT distinct mo.ballotNumber, mo.sequential , mo.mechanisnModality.negotiationModality.modalityName, " //0 1  2
				+ "mo.securities.idSecurityCodePk, hao.holderAccount.idHolderAccountPk, " //3 4
				+ "sam.marketDate, sam.marketRate, sam.marketQuantity ,hao.inchargeStockParticipant.idParticipantPk, sam.chainedQuantity, mo.securities.securityClass "); // 5 6 7 8 9
		sbQuery.append(" from HolderAccountOperation hao, MechanismOperation mo ,AssignmentRequest ar ,ParticipantAssignment pa, AssignmentProcess ap,  "
				+ "SettlementAccountOperation sao, SettlementAccountMarketfact sam");		
		sbQuery.append(" where 1=1"); 
//		sbQuery.append(" and ap.idAssignmentProcessPk = mo.assignmentProcess.idAssignmentProcessPk ");
		sbQuery.append(" and mo.sellerParticipant.idParticipantPk = hao.inchargeStockParticipant.idParticipantPk ");
		sbQuery.append(" and ar.idAssignmentRequestPk = hao.assignmentRequest.idAssignmentRequestPk ");
		sbQuery.append(" and ar.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk ");
		sbQuery.append(" and hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk ");
		sbQuery.append(" and sao.holderAccountOperation.idHolderAccountOperationPk = hao.idHolderAccountOperationPk ");
		sbQuery.append(" and sam.settlementAccountOperation.idSettlementAccountPk = sao.idSettlementAccountPk ");
//		sbQuery.append(" and mo.securities.securityClass not in (:lstSecurityClass) ");
		sbQuery.append(" and trunc(mo.registerDate) = :dateSelected ");
		sbQuery.append(" and ar.role = hao.role ");
		sbQuery.append(" and ar.role = :role ");
		sbQuery.append(" and ar.requestState = :registerState ");
//		sbQuery.append(" and ap.idAssignmentProcessPk = :idAssignmentProcessPk ");
		sbQuery.append(" and mo.mechanisnModality.negotiationModality.idNegotiationModalityPk =:negotiationModality ");
		
		
		if(Validations.validateIsNotNull(idParticipant)){
			sbQuery.append(" and hao.inchargeStockParticipant.idParticipantPk=:idParticipant ");
		}
		parameters.put("dateSelected", dateSelected);
		parameters.put("registerState", AssignmentRequestStateType.REGISTERED.getCode());
		parameters.put("role", role);
		parameters.put("negotiationModality", negotiationModality);
		
		if(Validations.validateIsNotNull(idParticipant)){
			parameters.put("idParticipant", idParticipant);
		}
//		List<Integer> lstSecurityClass= new ArrayList<Integer>();
//		lstSecurityClass.add(SecurityClassType.DPA.getCode());
//		lstSecurityClass.add(SecurityClassType.DPF.getCode());
//		parameters.put("lstSecurityClass", lstSecurityClass);
		
		return (List<Object[]>) findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the settlement operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationPart the operation part
	 * @return the settlement operation
	 */
	@SuppressWarnings("unchecked")
	public Object getSettlementOperation(Long idMechanismOperation, Integer operationPart) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT so from SettlementOperation so where"); //0 1  2
		stringBuffer.append("     so.mechanismOperation.idMechanismOperationPk  = :idMechanismOperation ");
		stringBuffer.append(" and so.indPartial = :indPartial");
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("indPartial", ComponentConstant.ZERO);
		
		if(operationPart!=null){ //issue 677
			stringBuffer.append(" and so.operationPart = :operationPart ");
			parameters.put("operationPart", operationPart);
			return (SettlementOperation) findObjectByQueryString(stringBuffer.toString(), parameters);
		}
		
		return (List<SettlementOperation>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the list settlement operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the list settlement operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSettlementOperation(Long idMechanismOperation) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SO.idSettlementOperationPk, ");
		stringBuffer.append(" SO.operationPart ");
		stringBuffer.append(" FROM SettlementOperation SO ");
		stringBuffer.append(" WHERE SO.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and SO.indPartial = :indPartial ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("indPartial", ComponentConstant.ZERO);
		
		return query.getResultList();
	}
	
	public Integer getUnsettledListSettlementOperation(Long idMechanismOperation) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT so.operationState ");
		stringBuffer.append(" FROM SettlementOperation so");
		stringBuffer.append(" WHERE so.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" AND so.operationPart = :cashPart");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		
		query.setMaxResults(1);
		
		return Integer.valueOf(query.getSingleResult().toString());
	}
	
	/**
	 * Gets the list term settlement operation.
	 *
	 * @param lstCashSettlementOperation the lst cash settlement operation
	 * @return the list term settlement operation
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListTermSettlementOperation(List<Long> lstCashSettlementOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SO_TERM.idSettlementOperationPk ");
		stringBuffer.append(" FROM SettlementOperation SO_CASH, SettlementOperation SO_TERM ");
		stringBuffer.append(" WHERE SO_CASH.mechanismOperation.idMechanismOperationPk = SO_TERM.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and SO_CASH.idSettlementOperationPk IN (:lstCashSettlementOperation) ");
		stringBuffer.append(" and SO_CASH.operationPart = :cashPart ");
		stringBuffer.append(" and SO_TERM.operationPart = :termPart ");
		stringBuffer.append(" and SO_TERM.mechanismOperation.indTermSettlement = :indTermSettlement ");
		stringBuffer.append(" and SO_TERM.mechanismOperation.indTermStockBlock = :indTermStockBlock ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("lstCashSettlementOperation", lstCashSettlementOperation);
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		query.setParameter("termPart", OperationPartType.TERM_PART.getCode());
		query.setParameter("indTermSettlement", BooleanType.YES.getCode());
		query.setParameter("indTermStockBlock", BooleanType.NO.getCode());
		
		return query.getResultList();
	}
	
	/**
	 * Gets the settlement operations.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationPart the operation part
	 * @param indPartial the ind partial
	 * @return the settlement operations
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementOperation> getSettlementOperations(Long idMechanismOperation, Integer operationPart, Integer indPartial) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT so from SettlementOperation so where"); //0 1  2
		stringBuffer.append("     so.mechanismOperation.idMechanismOperationPk  = :idMechanismOperation ");
		stringBuffer.append(" and so.indPartial = :indPartial");
		stringBuffer.append(" and so.operationPart = :operationPart ");
		stringBuffer.append(" order by so.settlementDate desc");
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("indPartial", indPartial);
		parameters.put("operationPart", operationPart);
		
		return (List<SettlementOperation>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the settlement account operation ref.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @return the settlement account operation ref
	 */
	public SettlementAccountOperation getSettlementAccountOperationRef(Long idSettlementAccountOperation) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT sao from SettlementAccountOperation sao where"); //0 1  2
		stringBuffer.append(" sao.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccount ");
		parameters.put("idSettlementAccount", idSettlementAccountOperation);
		
		return (SettlementAccountOperation) findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the settlement account operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @return the settlement account operation
	 */
	public SettlementAccountOperation getSettlementAccountOperation(Long idHolderAccountOperation) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT sao from SettlementAccountOperation sao where"); //0 1  2
		stringBuffer.append(" sao.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		
		return (SettlementAccountOperation) findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the participant settlements.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param role the role
	 * @param idParticipant the id participant
	 * @return the participant settlements
	 */
	@SuppressWarnings("unchecked")
	public Object getParticipantSettlements(Long idSettlementOperation, Integer role,Long idParticipant) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT ps from ParticipantSettlement ps");
		stringBuffer.append("	inner join fetch ps.participant p ");
		stringBuffer.append(" where ps.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and ps.operationState = :state ");
		parameters.put("idSettlementOperation", idSettlementOperation);
		parameters.put("state", ParticipantOperationStateType.CONFIRM.getCode());
		
		if(role!=null && idParticipant!=null){
			stringBuffer.append(" and ps.role = :role ");
			stringBuffer.append(" and ps.participant.idParticipantPk = :idParticipant ");
			parameters.put("role", role);
			parameters.put("idParticipant", idParticipant);
			return (ParticipantSettlement) findObjectByQueryString(stringBuffer.toString(), parameters);
		}
		
		return (List<ParticipantSettlement>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the participant settlement data.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @return the participant settlement data
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getParticipantSettlementData(Long idSettlementOperation) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT ps, ps.participant.idParticipantPk from ParticipantSettlement ps");
		stringBuffer.append(" where ps.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		parameters.put("idSettlementOperation", idSettlementOperation);
		
		return (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the settlement account operations.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param states the states
	 * @return the settlement account operations
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementAccountOperation> getSettlementAccountOperations(Long idSettlementOperation, List<Integer> states) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT sao from SettlementAccountOperation sao ");
		stringBuffer.append(" where sao.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		if(states!=null){
			stringBuffer.append(" and sao.operationState in ( :states )");
			parameters.put("states", states);
		}
		parameters.put("idSettlementOperation", idSettlementOperation);
		
		return (List<SettlementAccountOperation>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the settlement account operations by role.
	 *
	 * @param operationId the operation id
	 * @param role the role
	 * @param part the part
	 * @return the settlement account operations by role
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementAccountOperation> getSettlementAccountOperationsByRole(Long operationId, Integer role, Integer part) throws ServiceException{
		HashMap<String, Object> parameters = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT sao FROM SettlementAccountOperation sao");
		sbQuery.append("	inner join sao.holderAccountOperation hao ");
		sbQuery.append("	inner join hao.holderAccount ha ");
		sbQuery.append("	WHERE hao.mechanismOperation.idMechanismOperationPk = :operationId ");
		sbQuery.append("	and hao.operationPart = :operationPart ");
		sbQuery.append("	and hao.holderAccountState in (:accountState) ");
		sbQuery.append("	and hao.role = :role order by ha.accountNumber desc ");
		parameters.put("operationId", operationId);
		parameters.put("operationPart", part);
		parameters.put("role", role);
		parameters.put("accountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		return (List<SettlementAccountOperation>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Update settlement account operations.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param indTermSettlement the ind term settlement
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateSettlementAccountOperations(Long idMechanismOperation, Integer indTermSettlement, LoggerUser loggerUser) throws ServiceException {

		List<SettlementAccountOperation> purchaseAccountOperations = getSettlementAccountOperationsByRole(idMechanismOperation, ComponentConstant.PURCHARSE_ROLE, ComponentConstant.CASH_PART);
		
		List<SettlementAccountOperation> saleAccountOperations = getSettlementAccountOperationsByRole(idMechanismOperation, ComponentConstant.SALE_ROLE, ComponentConstant.CASH_PART);
		
		// create term marketfacts part for sale accounts
		for(SettlementAccountOperation saleAccountOperation : saleAccountOperations){
			
			updateSettlementAccountOperationState(saleAccountOperation.getIdSettlementAccountPk(), 
					HolderAccountOperationStateType.CONFIRMED.getCode(), loggerUser);
			
			if(ComponentConstant.ONE.equals(indTermSettlement)){
				SettlementAccountOperation termSaleAccount = getSettlementAccountOperationRef(saleAccountOperation.getIdSettlementAccountPk());
				if(termSaleAccount!=null){
					updateSettlementAccountOperationState(termSaleAccount.getIdSettlementAccountPk(), 
							HolderAccountOperationStateType.CONFIRMED.getCode(), loggerUser);
				}	
				if(termSaleAccount!=null){
					termSaleAccount.setSettlementAccountMarketfacts(new ArrayList<SettlementAccountMarketfact>());
					saleAccountOperation.setSettlementAccountMarketfacts(getSettlementAccountMarketfacts(saleAccountOperation.getIdSettlementAccountPk()));
					for(SettlementAccountMarketfact accountOperationMarketFact : saleAccountOperation.getSettlementAccountMarketfacts()){
						em.detach(accountOperationMarketFact);
						accountOperationMarketFact.setChainedQuantity(BigDecimal.ZERO);
						accountOperationMarketFact.setIdSettAccountMarketfactPk(null);
						accountOperationMarketFact.setSettlementAccountOperation(termSaleAccount);
						create(accountOperationMarketFact);
					}
				}
			}
		}
		
		// create term marketfacts part for account accounts (save cash market facts first)
		for(SettlementAccountOperation purchaseAccountOperation : purchaseAccountOperations){
			//saveAll(purchaseAccountOperation.getSettlementAccountMarketfacts());
			
			purchaseAccountOperation.setOperationState(HolderAccountOperationStateType.CONFIRMED.getCode());
			update(purchaseAccountOperation);
			
			if(ComponentConstant.ONE.equals(indTermSettlement)){
				SettlementAccountOperation termPurchaseAccount = getSettlementAccountOperationRef(purchaseAccountOperation.getIdSettlementAccountPk());
				if(termPurchaseAccount!=null){
					termPurchaseAccount.setOperationState(HolderAccountOperationStateType.CONFIRMED.getCode());		
				}				
				update(termPurchaseAccount);
				
				if(termPurchaseAccount!=null){
					termPurchaseAccount.setSettlementAccountMarketfacts(new ArrayList<SettlementAccountMarketfact>());
					purchaseAccountOperation.setSettlementAccountMarketfacts(getSettlementAccountMarketfacts(purchaseAccountOperation.getIdSettlementAccountPk()));
					for(SettlementAccountMarketfact accountOperationMarketFact : purchaseAccountOperation.getSettlementAccountMarketfacts()){
						em.detach(accountOperationMarketFact);
						accountOperationMarketFact.setChainedQuantity(BigDecimal.ZERO);
						accountOperationMarketFact.setIdSettAccountMarketfactPk(null);
						accountOperationMarketFact.setSettlementAccountOperation(termPurchaseAccount);
						create(accountOperationMarketFact);
					}
				}
			}
		}
	}
	
	/**
	 * Populate settlement operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param operationPart the operation part
	 * @return the settlement operation
	 */
	public SettlementOperation populateSettlementOperation(MechanismOperation mechanismOperation, Integer operationPart) {
		Long idSaleParticipant = null;
		Long idPurchaseParticipant = null;
		SettlementOperation settlementOperation = new SettlementOperation();
		settlementOperation.setMechanismOperation(mechanismOperation);
		settlementOperation.setOperationPart(operationPart);
		settlementOperation.setOperationState(mechanismOperation.getOperationState());
		settlementOperation.setInitialStockQuantity(mechanismOperation.getStockQuantity());
		settlementOperation.setStockQuantity(mechanismOperation.getStockQuantity());
		settlementOperation.setAmountRate(mechanismOperation.getAmountRate());
		settlementOperation.setExchangeRate(mechanismOperation.getExchangeRate());
		settlementOperation.setSettlementType(mechanismOperation.getSettlementType());
		settlementOperation.setSettlementSchema(mechanismOperation.getSettlementSchema());
		if(ComponentConstant.CASH_PART.equals(operationPart)){
			settlementOperation.setSettlementDate(mechanismOperation.getCashSettlementDate());
			settlementOperation.setSettlementPrice(mechanismOperation.getCashSettlementPrice());
			settlementOperation.setSettlementAmount(mechanismOperation.getCashSettlementAmount());
			settlementOperation.setSettlementCurrency(mechanismOperation.getCashSettlementCurrency());
			settlementOperation.setSettlementDays(mechanismOperation.getCashSettlementDays());
			idSaleParticipant = mechanismOperation.getSellerParticipant().getIdParticipantPk();
			idPurchaseParticipant = mechanismOperation.getBuyerParticipant().getIdParticipantPk();
		}else if(ComponentConstant.TERM_PART.equals(operationPart)){
			settlementOperation.setSettlementCurrency(mechanismOperation.getTermSettlementCurrency());
			settlementOperation.setSettlementAmount(mechanismOperation.getTermSettlementAmount());
			settlementOperation.setSettlementPrice(mechanismOperation.getTermSettlementPrice());
			settlementOperation.setSettlementDate(mechanismOperation.getTermSettlementDate());
			settlementOperation.setSettlementDays(mechanismOperation.getTermSettlementDays());
			idSaleParticipant = mechanismOperation.getBuyerParticipant().getIdParticipantPk();
			idPurchaseParticipant = mechanismOperation.getSellerParticipant().getIdParticipantPk();
		}
		
		settlementOperation.setInitialSettlementCurrency(settlementOperation.getSettlementCurrency());
		settlementOperation.setInitialSettlementPrice(settlementOperation.getSettlementPrice());
		
		ParticipantSettlement saleParticipant = populateParticipantSettlement(
				settlementOperation, settlementOperation.getStockQuantity(), idSaleParticipant, 
				ComponentConstant.SALE_ROLE,ComponentConstant.ZERO);
		ParticipantSettlement purchaseParticipant = populateParticipantSettlement(
				settlementOperation, settlementOperation.getStockQuantity(), idPurchaseParticipant,
				ComponentConstant.PURCHARSE_ROLE,ComponentConstant.ZERO);
		settlementOperation.setParticipantSettlements(new ArrayList<ParticipantSettlement>());
		settlementOperation.getParticipantSettlements().add(saleParticipant);
		settlementOperation.getParticipantSettlements().add(purchaseParticipant);
		
		return settlementOperation;
	}
	
	/**
	 * Populate participant settlement.
	 *
	 * @param settlementOperation the settlement operation
	 * @param stockQuantity the stock quantity
	 * @param idParticipant the id participant
	 * @param role the role
	 * @param indIncharge the ind incharge
	 * @return the participant settlement
	 */
	public ParticipantSettlement populateParticipantSettlement(SettlementOperation settlementOperation, 
			BigDecimal stockQuantity, Long idParticipant, Integer role, Integer indIncharge) {
		ParticipantSettlement participantSettlement = new ParticipantSettlement();
		participantSettlement.setStockQuantity(stockQuantity);
		participantSettlement.setSettlementCurrency(settlementOperation.getSettlementCurrency());
		participantSettlement.setSettlementAmount(settlementOperation.getSettlementAmount());
		participantSettlement.setRole(role);
		participantSettlement.setIndIncharge(indIncharge);
		participantSettlement.setOperationState(ParticipantOperationStateType.CONFIRM.getCode());
		participantSettlement.setSettlementOperation(settlementOperation);
		participantSettlement.setParticipant(new Participant(idParticipant));
		return participantSettlement;
	}

	/**
	 * Populate settlement account operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param accountOperation the account operation
	 * @param cashSettAccountOperation the cash sett account operation
	 * @return the settlement account operation
	 */
	public SettlementAccountOperation populateSettlementAccountOperation(Long idMechanismOperation, HolderAccountOperation accountOperation, SettlementAccountOperation cashSettAccountOperation) {
		SettlementAccountOperation settlementAccountOperation = new SettlementAccountOperation();
		settlementAccountOperation.setOperationState(accountOperation.getHolderAccountState());
		settlementAccountOperation.setChainedQuantity(BigDecimal.ZERO);
		settlementAccountOperation.setInitialStockQuantity(accountOperation.getStockQuantity());
		settlementAccountOperation.setStockQuantity(accountOperation.getStockQuantity());
		settlementAccountOperation.setRole(accountOperation.getRole());
		settlementAccountOperation.setHolderAccountOperation(accountOperation);
		
		SettlementOperation settlementOperation = (SettlementOperation)getSettlementOperation(idMechanismOperation,accountOperation.getOperationPart());
		settlementAccountOperation.setSettlementOperation(settlementOperation);
		if(accountOperation.getSettlementAmount()==null){
			settlementAccountOperation.setSettlementAmount(settlementOperation.getSettlementAmount().divide(settlementAccountOperation.getStockQuantity(), 2, RoundingMode.HALF_UP));
		}else{
			settlementAccountOperation.setSettlementAmount(accountOperation.getSettlementAmount());
		}
		
		if(ComponentConstant.TERM_PART.equals(accountOperation.getOperationPart())){
			settlementAccountOperation.setSettlementAccountOperation(cashSettAccountOperation);
		}
		
		return settlementAccountOperation;
	}

	/**
	 * Populate settlement account market fact.
	 *
	 * @param holAccOpeMarketFact the hol acc ope market fact
	 * @param settlementAccountOperation the settlement account operation
	 * @return the settlement account marketfact
	 */
	public SettlementAccountMarketfact populateSettlementAccountMarketFact(AccountOperationMarketFact holAccOpeMarketFact, SettlementAccountOperation settlementAccountOperation) {
		SettlementAccountMarketfact settlementAccountMarketfact = new SettlementAccountMarketfact();
		settlementAccountMarketfact.setMarketDate(holAccOpeMarketFact.getMarketDate());
//		if(holAccOpeMarketFact.getMarketRate()== null){
//			settlementAccountMarketfact.setMarketRate(BigDecimal.ZERO);
//		}else{
//			settlementAccountMarketfact.setMarketRate(holAccOpeMarketFact.getMarketRate());
//		}
		settlementAccountMarketfact.setMarketRate(holAccOpeMarketFact.getMarketRate());
		settlementAccountMarketfact.setMarketPrice(holAccOpeMarketFact.getMarketPrice());
		settlementAccountMarketfact.setMarketQuantity(holAccOpeMarketFact.getOperationQuantity());
		settlementAccountMarketfact.setSettlementAccountOperation(settlementAccountOperation);
		return settlementAccountMarketfact;
	}
	
	/**
	 * Populate purchase settlement market facts.
	 *
	 * @param settlementAccountOperation the settlement account operation
	 * @param idMechanismOperation the id mechanism operation
	 * @return the settlement account marketfact
	 */
	public SettlementAccountMarketfact populatePurchaseSettlementMarketFacts(SettlementAccountOperation settlementAccountOperation, Long idMechanismOperation) {
		
		List<MechanismOperationMarketFact> operationMarketFacts = getMechanismOperationMarketFacts(idMechanismOperation);
		
		SettlementAccountMarketfact settlementAccountMarketFact = new SettlementAccountMarketfact();
		settlementAccountMarketFact.setSettlementAccountOperation(settlementAccountOperation);
		for(MechanismOperationMarketFact mechanismOperationMarketFact:operationMarketFacts){
			settlementAccountMarketFact.setMarketDate(mechanismOperationMarketFact.getMarketDate());
			settlementAccountMarketFact.setMarketPrice(mechanismOperationMarketFact.getMarketPrice());
//			if(mechanismOperationMarketFact.getMarketRate()== null){
//				settlementAccountMarketFact.setMarketRate(BigDecimal.ZERO);
//			}else{
//				settlementAccountMarketFact.setMarketRate(mechanismOperationMarketFact.getMarketRate());
//			}
			settlementAccountMarketFact.setMarketRate(mechanismOperationMarketFact.getMarketRate());
		}
		settlementAccountMarketFact.setMarketQuantity(settlementAccountOperation.getStockQuantity());
		return settlementAccountMarketFact;
	}

	/**
	 * Creates the purchase settlement market facts.
	 *
	 * @param settlementAccountOperation the settlement account operation
	 * @param idMechanismOperation the id mechanism operation
	 */
	public void createPurchaseSettlementMarketFacts(SettlementAccountOperation settlementAccountOperation, Long idMechanismOperation) {
		
		List<MechanismOperationMarketFact> operationMarketFacts = getMechanismOperationMarketFacts(idMechanismOperation);
		
		SettlementAccountMarketfact settlementAccountMarketFact = new SettlementAccountMarketfact();
		settlementAccountMarketFact.setSettlementAccountOperation(settlementAccountOperation);
		for(MechanismOperationMarketFact mechanismOperationMarketFact:operationMarketFacts){
			settlementAccountMarketFact.setMarketDate(mechanismOperationMarketFact.getMarketDate());
			settlementAccountMarketFact.setMarketPrice(mechanismOperationMarketFact.getMarketPrice());
			settlementAccountMarketFact.setMarketRate(mechanismOperationMarketFact.getMarketRate());
		}
		settlementAccountMarketFact.setMarketQuantity(settlementAccountOperation.getStockQuantity());
		create(settlementAccountMarketFact);
	}
	
	/**
	 * Update operation settlements.
	 *
	 * @param settlementProcess the settlement process
	 * @throws ServiceException the service exception
	 */
	public void updateOperationSettlements(SettlementProcess settlementProcess) throws ServiceException {
		
		for(OperationSettlement operationSettlement : settlementProcess.getOperationSettlements()){
			if(!operationSettlement.getInitialState().equals(operationSettlement.getOperationState())){
				update(operationSettlement);
				if(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode().equals(operationSettlement.getOperationState())){
					updateSettlementRemovedCount(settlementProcess.getIdSettlementProcessPk());
				}
				if(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode().equals(operationSettlement.getOperationState())){
					updateSettlementPendingCount(settlementProcess.getIdSettlementProcessPk());
				}
			}
		}
	}
	
	
	/**
	 * Metodo para eliminar 
	 * @param idSettlementProcessPk
	 * @throws ServiceException
	 */
	
	public void deleteSettlementProcess(Long idSettlementProcessPk) throws ServiceException {
		
		StringBuilder queryUpdateOpSet = new StringBuilder();
		
		queryUpdateOpSet.append(" DELETE FROM  OPERATION_SETTLEMENT   							");
		queryUpdateOpSet.append(" WHERE  ID_SETTLMENT_PROCESS_FK = :idSettlementProcessPk		");
		
		Query queryToUpdateOpSet = em.createNativeQuery(queryUpdateOpSet.toString());
		queryToUpdateOpSet.setParameter("idSettlementProcessPk", idSettlementProcessPk);
		queryToUpdateOpSet.executeUpdate();
		
		
		StringBuilder queryUpdateParPos = new StringBuilder();
		
		queryUpdateParPos.append(" DELETE FROM  PARTICIPANT_POSITION   							");
		queryUpdateParPos.append(" WHERE  ID_SETTLEMENT_PROCESS_FK = :idSettlementProcessPk		");
		
		Query queryToUpdateParPos = em.createNativeQuery(queryUpdateParPos.toString());
		queryToUpdateParPos.setParameter("idSettlementProcessPk", idSettlementProcessPk);
		queryToUpdateParPos.executeUpdate();
		
		
		StringBuilder queryUpdateSetPro = new StringBuilder();
		
		queryUpdateSetPro.append(" DELETE FROM  SETTLEMENT_PROCESS   							");
		queryUpdateSetPro.append(" WHERE  ID_SETTLEMENT_PROCESS_PK = :idSettlementProcessPk		");
		
		Query queryToUpdateSetPro = em.createNativeQuery(queryUpdateSetPro.toString());
		queryToUpdateSetPro.setParameter("idSettlementProcessPk", idSettlementProcessPk);
		queryToUpdateSetPro.executeUpdate();
		
	}
	

	/**
	 * Update cash account ind process.
	 *
	 * @param idInstitutionCashAccount the id institution cash account
	 * @param indicator the indicator
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateCashAccountIndProcess(Long idInstitutionCashAccount, Integer indicator, LoggerUser loggerUser) {
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE InstitutionCashAccount SET indSettlementProcess = :indicator ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idInstitutionCashAccountPk = :idInstitutionCashAccount ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indicator", indicator);
		query.setParameter("idInstitutionCashAccount", idInstitutionCashAccount);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}

	/**
	 * Lock cash accounts.
	 *
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @param indicator the indicator
	 * @param loggerUser the logger user
	 */
	public void lockCashAccounts(NetSettlementAttributesTO netSettlementAttributesTO, Integer indicator, LoggerUser loggerUser) {
		//lock institution cashAccounts
		if(netSettlementAttributesTO.getMpCashAccounts().values().size() >0){
			List<InstitutionCashAccount> lstCashAccParticipants = new ArrayList<InstitutionCashAccount>(netSettlementAttributesTO.getMpCashAccounts().values());
			for (InstitutionCashAccount cashAccountParticipant : lstCashAccParticipants) {
				updateCashAccountIndProcess(cashAccountParticipant.getIdInstitutionCashAccountPk(),indicator, loggerUser);
			}
		}
	}


	/**
	 * Gets the day settlement operations.
	 *
	 * @param settlementDate the settlement date
	 * @return the day settlement operations
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getDaySettlementOperations(Date settlementDate) {
		StringBuffer stringBuffer= new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		stringBuffer.append(" SELECT so , mo , pt.indicator4 , pt.indicator5 ");
		stringBuffer.append(" FROM SettlementOperation so , MechanismOperation mo , ParameterTable pt ");
		stringBuffer.append(" where so.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk ");
		stringBuffer.append("   and pt.parameterTablePk = mo.currency ");
		stringBuffer.append("   and pt.indicator2 = :indicator ");
		stringBuffer.append("   and pt.indicator4 != pt.indicator5 ");
		stringBuffer.append("   and trunc(:settlementDate) <= trunc(so.settlementDate) ");
		stringBuffer.append("   and so.operationPart = :termPart ");
		stringBuffer.append("   and so.operationState =:cashSettled ");
		
		parameters.put("settlementDate", settlementDate);
		parameters.put("indicator", ComponentConstant.ZERO.toString());
		parameters.put("termPart", ComponentConstant.TERM_PART);
		parameters.put("cashSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
		
		return (List<Object[]>)findListByQueryString(stringBuffer.toString(),parameters);
	}

	
	
	/**
	 * Removes the operation settlement process by stock.
	 *
	 * @param netSettlementAttributesTO the net settlement attributes to
	 */
	public void removeOperationSettlementProcessByStock(NetSettlementAttributesTO netSettlementAttributesTO)
	{
		SettlementProcess settlementProcess = netSettlementAttributesTO.getSettlementProcess();
		Map<Long, NetParticipantPositionTO> mapParticipantOperations= netSettlementAttributesTO.getMapParticipantPosition();
		List<Long> participantsList = new ArrayList<Long>(mapParticipantOperations.keySet());
		//first we get the entire list of participant operations
		List<NetSettlementOperationTO> lstTotalParticipantOperations = new ArrayList<NetSettlementOperationTO>();
		for (Long idParticipant: participantsList) {
			NetParticipantPositionTO participantPositionTO = mapParticipantOperations.get(idParticipant);
			for (NetSettlementOperationTO objNetSettlementOperationTO: participantPositionTO.getLstSettlementOperation()) {
				if (!lstTotalParticipantOperations.contains(objNetSettlementOperationTO)) {
					lstTotalParticipantOperations.add(objNetSettlementOperationTO);
				}
			}
		}
		
		int countRemovedOperations=0;
		nextSettlementOperation:
		for (NetSettlementOperationTO objNetSettlementOperationTO: lstTotalParticipantOperations) {
			//if the operation is pending to settlement only
			if(objNetSettlementOperationTO.getOperationSettlement().getOperationState().equals(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode())){
				//if this operation belongs a chain
				if (BooleanType.YES.getValue().equals(objNetSettlementOperationTO.getIndChainedHolderOperation())) {
					//we get the list of sale settlement account operation of current operation 
					List<SettlementAccountOperation> lstSettlementAccountOperation= getListSettlementAccountByOperation(
																						objNetSettlementOperationTO.getIdSettlementOperation());
					if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountOperation)) {
						for (SettlementAccountOperation objSettlementAccountOperation: lstSettlementAccountOperation) {
							//if the sale settlement account operation lacks chained quantity, then it had to block at sale balance  
							if (objSettlementAccountOperation.getChainedQuantity().compareTo(BigDecimal.ZERO) == 0) {
								if (Validations.validateIsNull(objSettlementAccountOperation.getStockReference())) {
									//it did not block at sale balance, so it must be removed from settlement process
									List<Long> lstIdChainedHolderOperation= getListChainSettlementOperation(
																				objNetSettlementOperationTO.getIdSettlementOperation(), null);
									if (Validations.validateListIsNotNullAndNotEmpty(lstIdChainedHolderOperation)) {
										for (Long idRemoveChainedHolderOperation: lstIdChainedHolderOperation) {
											logger.info("::::  STARTING THE CHAIN REMOVE");
											List<NetSettlementOperationTO> lstNetSettlementOperationTO= new ArrayList<NetSettlementOperationTO>();
											countRemovedOperations+= removeChainedOperations(idRemoveChainedHolderOperation, null, mapParticipantOperations,
																							OperationSettlementSateType.WITHDRAWN_BY_SECURITIES.getCode(),
																							lstNetSettlementOperationTO);
											logger.info(":::: FINISHING THE CHAIN REMOVE");
											//update the list of removed operation by stock
											if (Validations.validateListIsNotNullAndNotEmpty(lstNetSettlementOperationTO)) {
												for (NetSettlementOperationTO settlementOperation: lstNetSettlementOperationTO) {
													netSettlementAttributesTO.getLstStockRemovedOperations().add(settlementOperation);
													netSettlementAttributesTO.getLstPendingSettlementOperations().remove(settlementOperation);
												}
											}
											//we fill up the new participant positions keeping in mind the removed operations
											mapParticipantOperations = NegotiationUtils.populateParticipantPositions(
																		netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
											netSettlementAttributesTO.setMapParticipantPosition(mapParticipantOperations);
										}
									}
									continue nextSettlementOperation;
								}
							}
						}
					}
				}
			}
		}
		//if we removed some operations, then we update the counters
		if (countRemovedOperations > 0) {
			settlementProcess.setRemovedOperations(settlementProcess.getRemovedOperations() + new Long(countRemovedOperations));
			settlementProcess.setPendingOperations(settlementProcess.getPendingOperations() - new Long(countRemovedOperations));
		}
	}
	
	/**
	 * Method to remove the settlement operations by funds. At the moment to remove an operation is mandatory does not affect the creditor position of participant
	 * - First: remove operations doesn't belong a chain
	 * - Second: removes chain with least size 
	 *
	 * @param participantPositionTO the participant position to
	 * @param lstPendingOperationsPerParticipant the lst pending operations per participant
	 * @param mapParticipantOperations the map participant operations
	 * @param participantRole the participant role
	 */
	public void removeOperationSettlementProcessByFunds(NetParticipantPositionTO participantPositionTO, 
																	List<NetSettlementOperationTO> lstPendingOperationsPerParticipant,
																	Map<Long, NetParticipantPositionTO> mapParticipantOperations,
																	Integer participantRole)
	{
		Long idBuyerParticipant= participantPositionTO.getIdParticipant();
		Map<Long,BigDecimal> mpChainedHolderOperation = new HashMap<Long,BigDecimal>(); //idChainedHolderOperation, settlement amount
		for (NetSettlementOperationTO objNetSettlementOperationTO: lstPendingOperationsPerParticipant) {
			String indChainedHolderOperation= objNetSettlementOperationTO.getIndChainedHolderOperation();
			//if the operation doesn't belong to chain, then we can removed
			if (StringUtils.equalsIgnoreCase(BooleanType.NO.getValue(),indChainedHolderOperation)) {
				//we don't considerer the crossed operation
				if (!NegotiationUtils.isCrossedOperation(objNetSettlementOperationTO, idBuyerParticipant)) {
					//only if the operation is pending to be settled 
					if (OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode().equals(objNetSettlementOperationTO.getOperationSettlement().getOperationState())) {
						if (participantRole.equals(objNetSettlementOperationTO.getParticipantRole())) { //seller or buyer
							objNetSettlementOperationTO.getOperationSettlement().setOperationState(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode());
							NegotiationUtils.updateParticipantNetPosition(participantPositionTO, objNetSettlementOperationTO);
							logger.info(":::: removed settlement operation "+ objNetSettlementOperationTO.getIdSettlementOperation());
							return;
						}
					}
				}
			} else { //we store the chains to identify the least one to be removed
				if (!NegotiationUtils.isCrossedOperation(objNetSettlementOperationTO, idBuyerParticipant)) {
					if (OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode().equals(objNetSettlementOperationTO.getOperationSettlement().getOperationState())) {
						List<Long> lstIdChainedHolderOperation= getListChainSettlementOperation(
																		objNetSettlementOperationTO.getIdSettlementOperation(), null);
						for (Long idChainedHolderOperation: lstIdChainedHolderOperation)
						{
							//it belongs a chain
							if (mpChainedHolderOperation.containsKey(idChainedHolderOperation)) {
								mpChainedHolderOperation.put(idChainedHolderOperation, mpChainedHolderOperation.get(idChainedHolderOperation).
																						add(objNetSettlementOperationTO.getSettlementAmount()));
							} else {
								mpChainedHolderOperation.put(idChainedHolderOperation, objNetSettlementOperationTO.getSettlementAmount());
							}
						}
					}
				}
			}
		}
		//if we didn't find out any single operation to be retired, then we must removed an entire chain
		if (!mpChainedHolderOperation.isEmpty()) {
			//first we must order the chains according their size 
			HashMap<Long,BigDecimal> mpOrderChains = new LinkedHashMap<Long,BigDecimal>();
			List<Long> misMapKeys = new ArrayList<Long>(mpChainedHolderOperation.keySet());
			List<BigDecimal> misMapValues = new ArrayList<BigDecimal>(mpChainedHolderOperation.values());
			TreeSet<BigDecimal> conjuntoOrdenado = new TreeSet<BigDecimal>(misMapValues);
			Object[] arrayOrdenado = conjuntoOrdenado.toArray();
			for (int i=0; i<arrayOrdenado.length; i++) {
				mpOrderChains.put(misMapKeys.get(misMapValues.indexOf(arrayOrdenado[i])),new BigDecimal(arrayOrdenado[i].toString()));
			}
			//we got the chain order by size 
			Iterator<Long> it = mpOrderChains.keySet().iterator();
			Long idRemoveChainedHolderOperation= it.next(); //this is the least chain
			logger.info("::::  STARTING THE CHAIN REMOVE");
			List<NetSettlementOperationTO> lstNetSettlementOperationTO= new ArrayList<NetSettlementOperationTO>();
			removeChainedOperations(idRemoveChainedHolderOperation, null, mapParticipantOperations, 
									OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode(), lstNetSettlementOperationTO);
			logger.info(":::: FINISHING THE CHAIN REMOVE");
		}
	}
	
	/**
	 * Removes the chained operations.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param idSettlementOperation the id settlement operation
	 * @param mapParticipantOperations the map participant operations
	 * @param operationSettlementState the operation settlement state
	 * @param lstNetSettlementOperationTO the lst net settlement operation to
	 * @return the int
	 */
	private int removeChainedOperations(Long idChainedHolderOperation, Long idSettlementOperation, Map<Long, NetParticipantPositionTO> mapParticipantOperations,
										Integer operationSettlementState, List<NetSettlementOperationTO> lstNetSettlementOperationTO)
	{
		logger.info(" REMOVING CHAIN: "+idChainedHolderOperation);
		int countRemovedOperations=0;
		List<Long> lstIdSettlementOperationToRemove= getListSettlementOperationByChain(idChainedHolderOperation, idSettlementOperation);
		//we removed all operation of this chain
		Iterator<Long> itPositions= mapParticipantOperations.keySet().iterator();
		//we look up for all operations in all participant positions 
		while (itPositions.hasNext()) {
			Long idParticipant= itPositions.next();
			List<NetSettlementOperationTO> lstParticipantOperations= mapParticipantOperations.get(idParticipant).getLstSettlementOperation();
			//for each participant position we get the chained operation to be removed 
			for (NetSettlementOperationTO objNetSettlementOperationTO: lstParticipantOperations) {
				for (Long idSettlementOperationToRemove: lstIdSettlementOperationToRemove) {
					//we look up the chained operation inside the participant position
					if (idSettlementOperationToRemove.equals(objNetSettlementOperationTO.getIdSettlementOperation())) {
						//only if the operation is pending to be settled 
						if (OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode().equals(
							objNetSettlementOperationTO.getOperationSettlement().getOperationState())) 
						{
							objNetSettlementOperationTO.getOperationSettlement().setOperationState(operationSettlementState);
							++countRemovedOperations;
							lstNetSettlementOperationTO.add(objNetSettlementOperationTO); //we add the removed operation to the list
							logger.info(" --- REMOVING OPERATION: "+idChainedHolderOperation+"-"+idSettlementOperationToRemove);
							//now we verify if this operation belongs to others different chain
							List<Long> lstIdChainedHolderOperation= getListChainSettlementOperation(
																						idSettlementOperationToRemove, idChainedHolderOperation);
							if (Validations.validateListIsNotNullAndNotEmpty(lstIdChainedHolderOperation)) {
								for (Long idChainedHolderOperationToRemove: lstIdChainedHolderOperation) {
									//we remove the operation of these chains
									countRemovedOperations+= removeChainedOperations(idChainedHolderOperationToRemove, idSettlementOperationToRemove, 
																				mapParticipantOperations, operationSettlementState, lstNetSettlementOperationTO);
								}
							}
						}
					}
				}
			}
		}
		return countRemovedOperations;
	}
	
	/**
	 * Gets the settlement operation funds.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationPart the operation part
	 * @return the settlement operation funds
	 */
	@SuppressWarnings("unchecked")
	public Object getSettlementOperationFunds(Long idMechanismOperation, Integer operationPart) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT so from SettlementOperation so where"); //0 1  2
		stringBuffer.append("     so.mechanismOperation.idMechanismOperationPk  = :idMechanismOperation ");
		//stringBuffer.append(" and so.settlementDate = :settlementDate");
		parameters.put("idMechanismOperation", idMechanismOperation);
		//parameters.put("settlementDate", CommonsUtilities.truncateDateTime(new Date()));
		
		if(operationPart!=null){
			stringBuffer.append(" and so.operationPart = :operationPart ");
			parameters.put("operationPart", operationPart);
			return (SettlementOperation) findObjectByQueryString(stringBuffer.toString(), parameters);
		}
		
		return (List<SettlementOperation>) findListByQueryString(stringBuffer.toString(), parameters);
	}


	/**
	 * Check pending settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void checkPendingSettlementProcess(Long idMechanism, Long idModality,Integer idCurrency, Date settlementDate) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT sp.idSettlementProcessPk ");
		sbQuery.append("	FROM SettlementProcess sp ");
		sbQuery.append("	WHERE sp.negotiationMechanism.idNegotiationMechanismPk = :idMechanism ");
		sbQuery.append("	AND sp.currency = :idCurrency ");
		sbQuery.append("	AND sp.settlementDate = :settlementDate ");
		sbQuery.append("	AND sp.processState in ( :state ) ");
		sbQuery.append("	AND sp.modalityGroup.idModalityGroupPk in ( ");
		sbQuery.append("		select distinct mg.idModalityGroupPk from ModalityGroupDetail mgd  ");
		sbQuery.append("		inner join mgd.modalityGroup mg inner join mgd.mechanismModality mm ");
		sbQuery.append("		where mm.id.idNegotiationMechanismPk = :idMechanism ");
		sbQuery.append("		and mm.id.idNegotiationModalityPk = :idModality ");
		sbQuery.append("		and mm.settlementSchema = mg.settlementSchema ) ");
		
		parameters.put("idMechanism", idMechanism);
		parameters.put("idModality", idModality);
		parameters.put("settlementDate", settlementDate);
		parameters.put("idCurrency", idCurrency);
		List<Integer> states = new ArrayList<Integer>();
		states.add(SettlementProcessStateType.IN_PROCESS.getCode());
		states.add(SettlementProcessStateType.STARTED.getCode());
		parameters.put("state", states);
		
		Long result = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
		if(result !=null){
			throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_NOT_EXECUTED_FINISHED);
		}
	}
	
	/**
	 * Check finished settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param scheduleType the schedule type
	 * @throws ServiceException the service exception
	 */
	public void checkFinishedSettlementProcess(Long idMechanism, Long idModality,Integer idCurrency, Date settlementDate, Integer scheduleType) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT sp.idSettlementProcessPk , ");
		sbQuery.append("    (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = sp.scheduleType ) "); 
		sbQuery.append("	FROM SettlementProcess sp ");
		sbQuery.append("	WHERE sp.negotiationMechanism.idNegotiationMechanismPk = :idMechanism ");
		sbQuery.append("	AND sp.currency = :idCurrency ");
		sbQuery.append("	AND sp.settlementDate = :settlementDate ");
		sbQuery.append("	AND sp.processState = :state ");
		sbQuery.append("	AND sp.scheduleType = :scheduleType ");
		sbQuery.append("	AND sp.modalityGroup.idModalityGroupPk in ( ");
		sbQuery.append("		select distinct mg.idModalityGroupPk from ModalityGroupDetail mgd  ");
		sbQuery.append("		inner join mgd.modalityGroup mg inner join mgd.mechanismModality mm ");
		sbQuery.append("		where mm.id.idNegotiationMechanismPk = :idMechanism ");
		sbQuery.append("		and mm.id.idNegotiationModalityPk = :idModality ");
		sbQuery.append("		and mm.settlementSchema = mg.settlementSchema ) ");
		
		parameters.put("idMechanism", idMechanism);
		parameters.put("idModality", idModality);
		parameters.put("settlementDate", settlementDate);
		parameters.put("idCurrency", idCurrency);
		parameters.put("scheduleType", scheduleType);
		parameters.put("state", SettlementProcessStateType.FINISHED.getCode());
		
		Object[] result = (Object[])findObjectByQueryString(sbQuery.toString(), parameters);
		if(result !=null){
			String scheduleName = (String) result[1];
			throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_SCHEDULE_FINISHED, new Object[]{scheduleName});
		}
	}
	
	/**
	 * Check exist chained account.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @throws ServiceException the service exception
	 */
	public void checkExistChainedAccount(Long idSettlementOperation) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT distinct mo.ballotNumber , mo.sequential , mo.operationNumber, mo.operationDate ");
		sbQuery.append("	from SettlementAccountOperation sao ");
		sbQuery.append("	inner join sao.settlementOperation so ");
		sbQuery.append("	inner join so.mechanismOperation mo ");
		sbQuery.append("	where so.idSettlementOperationPk = :idSettlementOperation  ");
		sbQuery.append("	and exists ( ");
		sbQuery.append("		select hcd.idHolderChainDetailPk from HolderChainDetail hcd inner join hcd.chainedHolderOperation cho ");
		sbQuery.append("		where hcd.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = sao.idSettlementAccountPk  ");
		sbQuery.append("		and cho.chaintState in  (:includedStates)  ) ");
		
		parameters.put("idSettlementOperation", idSettlementOperation);
		List<Integer> includedStates= new ArrayList<Integer>();
		includedStates.add(ChainedOperationStateType.REGISTERED.getCode());
		includedStates.add(ChainedOperationStateType.CONFIRMED.getCode());
		parameters.put("includedStates",includedStates);
		
		Object[] result = (Object[])findObjectByQueryString(sbQuery.toString(), parameters);
		if(result != null){
			String ballotNumber = result[0].toString();
			String sequential = result[1].toString();
			StringBuffer operation = new StringBuffer();
			if(ballotNumber != null && sequential !=null){
				operation.append(ballotNumber).append(GeneralConstants.SLASH).append(sequential);
			}else{
				Long operationNumber = new Long(result[2].toString());
				Date date = (Date)result[3];
				operation.append(operationNumber).append(GeneralConstants.DASH).append(date);
			}
			throw new ServiceException(ErrorServiceType.CHAINED_OPERATIONS_OPERATION_EXISTS_AS_CHAIN, new Object[]{operation.toString()});
		}
	}
	
	/**
	 * Gets the issuer details.
	 *
	 * @param idSecurityCode the id security code
	 * @return the issuer details
	 */
	public Object[] getIssuerDetails(String idSecurityCode){
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select SEC.alternativeCode,SEC.issuanceDate,ISS.idIssuerPk,a.indicator4,c.indicator4");  // 0 1 2 3 4
		stringBuffer.append(" from Security SEC, Issuer ISS, ParameterTable a, ParameterTable c "); 
		stringBuffer.append(" where SEC.issuer.idIssuerPk = ISS.idIssuerPk "); 
		stringBuffer.append(" and ISS.economicActivity = a.parameterTablePk ");
		stringBuffer.append(" and SEC.securityClass = c.parameterTablePk ");
		stringBuffer.append(" and SEC.idSecurityCodePk = :idSecurityCode ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSecurityCode", idSecurityCode);
		
		return (Object[])findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Gets the list settlement account by operation.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @return the list settlement account by operation
	 */
	public List<SettlementAccountOperation> getListSettlementAccountByOperation(Long idSettlementOperation)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SAO FROM SettlementAccountOperation SAO ");
		stringBuffer.append(" WHERE SAO.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and SAO.role = :role ");
		stringBuffer.append(" and SAO.operationState = :settlementAccountState ");
		stringBuffer.append(" and SAO.settlementOperation.operationPart = :operationPart ");
		
		TypedQuery<SettlementAccountOperation> typedQuery= em.createQuery(stringBuffer.toString(), SettlementAccountOperation.class);
		typedQuery.setParameter("idSettlementOperation", idSettlementOperation);
		typedQuery.setParameter("role", ComponentConstant.SALE_ROLE);
		typedQuery.setParameter("settlementAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		typedQuery.setParameter("operationPart", OperationPartType.CASH_PART.getCode());
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Gets the list settlement operation to forced purchase.
	 *
	 * @param settlementDate the settlement date
	 * @return the list settlement operation to forced purchase
	 */
	public List<Long> getListSettlementOperationToForcedPurchase(Date settlementDate)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT idSettlementOperationPk FROM SettlementOperation ");
		stringBuffer.append(" WHERE settlementDate = :settlementDate ");
		stringBuffer.append(" and mechanismOperation.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = :mechanismBBV ");
		stringBuffer.append(" and operationState = :operationState ");
		stringBuffer.append(" and operationPart = :operationPart ");
		stringBuffer.append(" and settlementDate >= mechanismOperation.securities.expirationDate ");
		
		TypedQuery<Long> typedQuery= em.createQuery(stringBuffer.toString(),Long.class);
		typedQuery.setParameter("settlementDate", settlementDate);
		typedQuery.setParameter("mechanismBBV", NegotiationMechanismType.BOLSA.getCode());
		typedQuery.setParameter("operationState", MechanismOperationStateType.CASH_SETTLED.getCode());
		typedQuery.setParameter("operationPart", OperationPartType.TERM_PART.getCode());
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Update settlement operation to forced purchase.
	 *
	 * @param lstIdSettlementOperation the lst id settlement operation
	 * @param loggerUser the logger user
	 */
	public void updateSettlementOperationToForcedPurchase(List<Long> lstIdSettlementOperation, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation ");
		stringBuffer.append(" SET settlementType = :settlementType ");
		stringBuffer.append(" , settlementSchema = :settlementSchema ");
		stringBuffer.append(" , indForcedPurchase = :indForcedPurchase ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk in (:lstIdSettlementOperation) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("settlementType", SettlementType.FOP.getCode());
		query.setParameter("settlementSchema", SettlementSchemaType.GROSS.getCode());
		query.setParameter("indForcedPurchase", BooleanType.YES.getCode());
		query.setParameter("lstIdSettlementOperation", lstIdSettlementOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}

	/**
	 * Gets the unfullfillment participants.
	 *
	 * @param currentDate the current date
	 * @param type the type
	 * @return the unfullfillment participants
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getUnfullfillmentParticipants(Date currentDate, Long type) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT pa.idParticipantPk, pa.description from OperationUnfulfillment ou "); 
		if(type.equals(ComponentConstant.SOURCE)){
			stringBuffer.append(" 	inner join ou.unfulfilledParticipant pa "); 
		}else if(type.equals(ComponentConstant.TARGET)){
			stringBuffer.append(" 	inner join ou.affectedParticipant pa ");
		}
		stringBuffer.append(" where trunc(ou.unfulfillmentDate) = :settlementDate ");
		stringBuffer.append(" group by pa.idParticipantPk, pa.description ");
		
		parameters.put("settlementDate", currentDate);
		
		return (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Count unfullfillment operations.
	 *
	 * @param currentDate the current date
	 * @param idMechanism the id mechanism
	 * @return the long
	 */
	public Long countUnfullfillmentOperations(Date currentDate,Long idMechanism, Integer currency) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select nvl(count(ou.idOperationUnfulfillmentPk),0) from OperationUnfulfillment ou inner join ou.settlementOperation so ");
		stringBuffer.append(" inner join so.mechanismOperation mo where mo.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism ");
		stringBuffer.append(" and trunc(ou.unfulfillmentDate) = :settlementDate ");
		
		if(Validations.validateIsNotNullAndNotEmpty(currency)){
			stringBuffer.append(" and MO.securities.currency = :currency ");
			parameters.put("currency", currency);
		}
		
		parameters.put("settlementDate", currentDate);
		parameters.put("idMechanism", idMechanism);
		
		return (Long) findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Count unfullfillment operations.
	 *
	 * @param currentDate the current date
	 * @param idMechanism the id mechanism
	 * @return the long
	 */
	public Boolean verifyOperationExist(Date dateOperation) {
		
		Boolean indError = false;
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("   SELECT MO.ID_MECHANISM_OPERATION_PK                                                                                ");
		querySql.append("   FROM MECHANISM_OPERATION MO                                                                                        ");
		querySql.append("   WHERE 1 = 1                                                                                                        ");
		querySql.append("   AND (TRUNC(MO.TERM_SETTLEMENT_DATE) = :processDate                                                                 ");
		querySql.append("       OR TRUNC(MO.CASH_SETTLEMENT_DATE) = :processDate )                                                             ");
		querySql.append("   AND MO.OPERATION_STATE in ( 612, 614 )                                                                             ");
		querySql.append("   AND MO.ID_NEGOTIATION_MECHANISM_FK = 1                                                                             ");
		querySql.append("   AND MO.ID_MECHANISM_OPERATION_PK NOT IN (                                                                          ");
		querySql.append("            SELECT MO_SUB.ID_MECHANISM_OPERATION_PK                                                                   ");     
		querySql.append("            FROM SETTLEMENT_DATE_OPERATION SDO                                                                        ");
		querySql.append("            INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK            ");
		querySql.append("            INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK      ");
		querySql.append("            INNER JOIN MECHANISM_OPERATION MO_SUB ON MO_SUB.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK  ");
		querySql.append("            WHERE 1 = 1                                                                                               ");
		querySql.append("            AND SR.REQUEST_TYPE = 2018                                                                                ");
		querySql.append("            AND SR.REQUEST_STATE = 1543                                                                               ");
		querySql.append("            AND TRUNC(SR.REGISTER_DATE) = :processDate                                                                ");
		querySql.append("       )                                                                                                              ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("processDate", dateOperation);
		List<BigDecimal> test =  (List<BigDecimal>)query.getResultList();
		if(test.size() > GeneralConstants.ZERO_VALUE_INTEGER) {
			indError = true;
		}
		
		return indError;
	}
	

	/**
	 * Cancel pending assignment requests.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param loggerUser the logger user
	 */
	public void cancelPendingAssignmentRequests(Long idMechanismOperation, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE AssignmentRequest ");
		stringBuffer.append(" SET requestState = :cancelledState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and requestState in (:pendingState,:registeredState) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("cancelledState", AssignmentRequestStateType.CANCLED.getCode());
		query.setParameter("pendingState", AssignmentRequestStateType.PENDING.getCode());
		query.setParameter("registeredState", AssignmentRequestStateType.REGISTERED.getCode());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Gets the list settlement operation by process.
	 *
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @return the list settlement operation by process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	
	public List<Object[]> getListSettlementOperationByProcess(Date settlementDate, Long idSettlementProcess) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT MO.idMechanismOperationPk, "); // 0
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 2
		stringBuffer.append(" MO.indReportingBalance, "); //3
		stringBuffer.append(" MO.indPrincipalGuarantee, "); //4
		stringBuffer.append(" MO.indMarginGuarantee, "); //5
		stringBuffer.append(" MO.indTermSettlement, "); //6
		stringBuffer.append(" MO.indCashStockBlock, "); //7
		stringBuffer.append(" MO.indTermStockBlock, "); //8
		stringBuffer.append(" MO.indPrimaryPlacement, "); //9
		stringBuffer.append(" MO.stockQuantity, "); // 10
		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 11
		stringBuffer.append(" SO.settlementType, "); //FOP, DVP, DVD  12
		stringBuffer.append(" MO.referenceOperation.idMechanismOperationPk, "); // 13
		stringBuffer.append(" MO.securities.instrumentType, "); //14
		stringBuffer.append(" mo.securities.currentNominalValue, "); //15
		stringBuffer.append(" HAO.idHolderAccountOperationPk, "); // 16
		stringBuffer.append(" HAO.inchargeStockParticipant.idParticipantPk, "); // 17
		stringBuffer.append(" HAO.holderAccount.idHolderAccountPk, "); // 18
		stringBuffer.append(" HAO.role, "); // 19
		stringBuffer.append(" HAO.operationPart, "); // 20
		stringBuffer.append(" HAO.stockQuantity, "); // 21
		stringBuffer.append(" HAO.refAccountOperation.idHolderAccountOperationPk, "); // 22
		stringBuffer.append(" SO.idSettlementOperationPk, "); //23
		stringBuffer.append(" SO.stockQuantity, "); //24
		stringBuffer.append(" SO.settlementAmount, "); // 25
		stringBuffer.append(" SAO.idSettlementAccountPk, "); //26
		stringBuffer.append(" SAO.stockQuantity, "); //27
		stringBuffer.append(" SAO.settlementAccountOperation.idSettlementAccountPk, "); //28
		stringBuffer.append(" SAO.chainedQuantity, "); // 29
		stringBuffer.append(" SO.indPartial, "); // 30
		stringBuffer.append(" SO.indDematerialization, "); // 31
		stringBuffer.append(" SO.indForcedPurchase, "); // 32
		stringBuffer.append(" MO.mechanisnModality.interfaceTransferCode, "); //33
		stringBuffer.append(" MO.securities.securityClass, "); //34
		stringBuffer.append(" MO.operationNumber "); //35
		stringBuffer.append(" FROM MechanismOperation MO, HolderAccountOperation HAO, SettlementAccountOperation SAO, SettlementOperation SO, OperationSettlement OS ");
		stringBuffer.append(" WHERE OS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and MO.idMechanismOperationPk = HAO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and SAO.holderAccountOperation.idHolderAccountOperationPk = HAO.idHolderAccountOperationPk ");
		stringBuffer.append(" and SAO.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and trunc(OS.settlementProcess.settlementDate) = :settlementDate ");
		stringBuffer.append(" and OS.operationState = :operationSettlementState ");
		stringBuffer.append(" and OS.settlementOperation.indSentInterface = :indSentInterface ");
		stringBuffer.append(" and SAO.operationState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
		stringBuffer.append(" and MO.securities.securityClass in (:lstSecurityClass) ");
		if (Validations.validateIsNotNull(idSettlementProcess)) {
			stringBuffer.append(" and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		}
		stringBuffer.append(" ORDER BY MO.mechanisnModality.id.idNegotiationModalityPk desc, MO.operationDate, MO.operationNumber, SAO.role ");
		
		parameters.put("settlementDate", settlementDate);
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("operationSettlementState", OperationSettlementSateType.SETTLED.getCode());
		parameters.put("indSentInterface", BooleanType.NO.getCode());
		List<Integer> lstSecurityClass= new ArrayList<Integer>();
		lstSecurityClass.add(SecurityClassType.DPA.getCode());
		lstSecurityClass.add(SecurityClassType.DPF.getCode());
		parameters.put("lstSecurityClass", lstSecurityClass);
		if (Validations.validateIsNotNull(idSettlementProcess)) {
			parameters.put("idSettlementProcess", idSettlementProcess);
		}
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		return lstObjects;
	}
	
	/**
	 * issue 1197, cambio de order by
	 *
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @return the list settlement operation by process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSettlementOperationByProcessBatch(Date settlementDate, Long idSettlementProcess) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT MO.idMechanismOperationPk, "); // 0
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationMechanismPk, "); // 1
		stringBuffer.append(" MO.mechanisnModality.id.idNegotiationModalityPk, "); // 2
		stringBuffer.append(" MO.indReportingBalance, "); //3
		stringBuffer.append(" MO.indPrincipalGuarantee, "); //4
		stringBuffer.append(" MO.indMarginGuarantee, "); //5
		stringBuffer.append(" MO.indTermSettlement, "); //6
		stringBuffer.append(" MO.indCashStockBlock, "); //7
		stringBuffer.append(" MO.indTermStockBlock, "); //8
		stringBuffer.append(" MO.indPrimaryPlacement, "); //9
		stringBuffer.append(" MO.stockQuantity, "); // 10
		stringBuffer.append(" MO.securities.idSecurityCodePk, "); // 11
		stringBuffer.append(" SO.settlementType, "); //FOP, DVP, DVD  12
		stringBuffer.append(" MO.referenceOperation.idMechanismOperationPk, "); // 13
		stringBuffer.append(" MO.securities.instrumentType, "); //14
		stringBuffer.append(" mo.securities.currentNominalValue, "); //15
		stringBuffer.append(" HAO.idHolderAccountOperationPk, "); // 16
		stringBuffer.append(" HAO.inchargeStockParticipant.idParticipantPk, "); // 17
		stringBuffer.append(" HAO.holderAccount.idHolderAccountPk, "); // 18
		stringBuffer.append(" HAO.role, "); // 19
		stringBuffer.append(" HAO.operationPart, "); // 20
		stringBuffer.append(" HAO.stockQuantity, "); // 21
		stringBuffer.append(" HAO.refAccountOperation.idHolderAccountOperationPk, "); // 22
		stringBuffer.append(" SO.idSettlementOperationPk, "); //23
		stringBuffer.append(" SO.stockQuantity, "); //24
		stringBuffer.append(" SO.settlementAmount, "); // 25
		stringBuffer.append(" SAO.idSettlementAccountPk, "); //26
		stringBuffer.append(" SAO.stockQuantity, "); //27
		stringBuffer.append(" SAO.settlementAccountOperation.idSettlementAccountPk, "); //28
		stringBuffer.append(" SAO.chainedQuantity, "); // 29
		stringBuffer.append(" SO.indPartial, "); // 30
		stringBuffer.append(" SO.indDematerialization, "); // 31
		stringBuffer.append(" SO.indForcedPurchase, "); // 32
		stringBuffer.append(" MO.mechanisnModality.interfaceTransferCode, "); //33
		stringBuffer.append(" MO.securities.securityClass, "); //34
		stringBuffer.append(" MO.operationNumber "); //35
		stringBuffer.append(" FROM MechanismOperation MO, HolderAccountOperation HAO, SettlementAccountOperation SAO, SettlementOperation SO, OperationSettlement OS ");
		stringBuffer.append(" WHERE OS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and MO.idMechanismOperationPk = HAO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and SAO.holderAccountOperation.idHolderAccountOperationPk = HAO.idHolderAccountOperationPk ");
		stringBuffer.append(" and SAO.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		stringBuffer.append(" and trunc(OS.settlementProcess.settlementDate) = :settlementDate ");
		stringBuffer.append(" and OS.operationState = :operationSettlementState ");
		stringBuffer.append(" and OS.settlementOperation.indSentInterface = :indSentInterface ");
		stringBuffer.append(" and SAO.operationState = :idHolderAccountState "); //Holder accounts in CONFIRMED state
		stringBuffer.append(" and MO.securities.securityClass in (:lstSecurityClass) ");
		if (Validations.validateIsNotNull(idSettlementProcess)) {
			stringBuffer.append(" and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		}
		stringBuffer.append(" ORDER BY MO.idMechanismOperationPk,MO.mechanisnModality.id.idNegotiationModalityPk desc, MO.operationDate, MO.operationNumber, SAO.role ");
		
		parameters.put("settlementDate", settlementDate);
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("operationSettlementState", OperationSettlementSateType.SETTLED.getCode());
		parameters.put("indSentInterface", BooleanType.NO.getCode());
		List<Integer> lstSecurityClass= new ArrayList<Integer>();
		lstSecurityClass.add(SecurityClassType.DPA.getCode());
		lstSecurityClass.add(SecurityClassType.DPF.getCode());
		parameters.put("lstSecurityClass", lstSecurityClass);
		if (Validations.validateIsNotNull(idSettlementProcess)) {
			parameters.put("idSettlementProcess", idSettlementProcess);
		}
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		return lstObjects;
	}
	
	/**
	 * Generate securities transfer register to.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> generateSecuritiesTransferRegisterTO(SettlementOperationTO objSettlementOperationTO, Long idInterfaceProcess, 
				LoggerUser loggerUser) throws ServiceException{
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		
		SecuritiesTransferRegisterTO securitiesTransferRegisterTO= getSecuritiesTransferRegisterTO(objSettlementOperationTO);
		securitiesTransferRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(securitiesTransferRegisterTO, 
																		GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		interfaceComponentServiceBean.get().saveInterfaceTransaction(securitiesTransferRegisterTO, BooleanType.YES.getCode(), loggerUser);
		return lstRecordValidationTypes;
	}
	
	private Long getIdParticipantByNemonic(String mnemonic) {
		StringBuilder sd =new StringBuilder();
		sd.append(" select p.idParticipantPk from Participant p where p.mnemonic = :mnemonic");
		try {
			Query q = em.createQuery(sd.toString());
			q.setParameter("mnemonic", mnemonic);
			Object obj = q.getSingleResult();
			if(Validations.validateIsNotNull(obj)){
				return Long.parseLong(obj.toString());
			}else{
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<RecordValidationType> registerInterfaceTrasaction(OperationTutXmlDTO operationTutXmlDTO, Long idInterfaceProcess, 
			LoggerUser loggerUser) throws ServiceException{
	List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
	
	// construir el OperationInterfaceTO para registrar el InterfaceTransaction
	OperationInterfaceTO operationInterfaceTO = new OperationInterfaceTO();
	operationInterfaceTO.setOperationNumber(operationTutXmlDTO.getNumOperation());
	operationInterfaceTO.setIdInterfaceProcess(idInterfaceProcess);
	// seteando el emisor
	operationInterfaceTO.setIdParticipant(getIdParticipantByNemonic(operationTutXmlDTO.getEEFF()));
	
	
	RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(operationInterfaceTO, 
																	GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
	lstRecordValidationTypes.add(objRecordValidationType);
	interfaceComponentServiceBean.get().saveInterfaceTransaction(operationInterfaceTO, BooleanType.YES.getCode(), loggerUser);
	return lstRecordValidationTypes;
}
	
	/**
	 * Generate funds transfer register to.
	 *
	 * @param objFundsTransferRegisterTO the obj funds transfer register to
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the funds transfer register to
	 * @throws ServiceException the service exception
	 */
	public FundsTransferRegisterTO generateFundsTransferRegisterTO(FundsTransferRegisterTO objFundsTransferRegisterTO, Long idInterfaceProcess, 
			LoggerUser loggerUser) throws ServiceException{
		objFundsTransferRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		Long idInterfaceTransaction = interfaceComponentServiceBean.get().saveInterfaceTransactionTx(objFundsTransferRegisterTO, BooleanType.YES.getCode(), loggerUser);
		objFundsTransferRegisterTO.setIdInterfaceTransaction(idInterfaceTransaction);
		return objFundsTransferRegisterTO;
	}
	
	/**
	 * Gets the securities transfer register to.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @return the securities transfer register to
	 */
	public SecuritiesTransferRegisterTO getSecuritiesTransferRegisterTO(SettlementOperationTO objSettlementOperationTO) {
		Security securities= find(Security.class, objSettlementOperationTO.getIdSecurityCode());
		HolderAccount holderAccount= getHolderAccount(objSettlementOperationTO.getLstBuyerHolderAccounts().get(0).getIdHolderAccount());
		
		HolderAccountObjectTO holderAccountObjectTO= new HolderAccountObjectTO();
		holderAccountObjectTO.setAccountNumber(holderAccount.getAccountNumber());
		
		ParameterTable parameterTable= parameterServiceBean.getParameterDetail(holderAccount.getAccountType());
		holderAccountObjectTO.setAccountTypeCode(parameterTable.getText1());
		
		List<HolderAccountDetail> lstHolderAccountDetails= holderAccount.getHolderAccountDetails();
		//if there are a list of holders then is a co-ownership of natural persons
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails) && lstHolderAccountDetails.size() > 1) {
			holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
		} else {
			Holder holder= lstHolderAccountDetails.get(0).getHolder();
			if (PersonType.NATURAL.getCode().equals(holder.getHolderType())) {
				holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				holderAccountObjectTO.setHolderType(GeneralConstants.TWO_VALUE_INTEGER);
			}
		}
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails)) {
			List<NaturalHolderObjectTO> lstNaturalHolderObjectTO= new ArrayList<NaturalHolderObjectTO>();
			for (HolderAccountDetail holderAccountDetail: lstHolderAccountDetails) {
			if (PersonType.JURIDIC.getCode().equals(holderAccountDetail.getHolder().getHolderType())) {
				JuridicHolderObjectTO juridicHolderObjectTO= holderBalanceMovementsServiceBean.populateJuridicHolderObjectTO(
											holderAccountDetail.getHolder(), true);
				holderAccountObjectTO.setJuridicHolderObjectTO(juridicHolderObjectTO);
				} else {
					NaturalHolderObjectTO naturalHolderObjectTO= holderBalanceMovementsServiceBean.populateNaturalHolderObjectTO(
												holderAccountDetail.getHolder(), true);
					lstNaturalHolderObjectTO.add(naturalHolderObjectTO);
				}
			}
			holderAccountObjectTO.setLstNaturalHolderObjectTOs(lstNaturalHolderObjectTO);
		}		
		SecurityObjectTO securityObjectTO= holderBalanceMovementsServiceBean.populateSecurityObjectTO(securities);		
		SecuritiesTransferRegisterTO securitiesTransferRegisterTO = new SecuritiesTransferRegisterTO();		
		securitiesTransferRegisterTO.setTransferType(objSettlementOperationTO.getInterfaceTransferCode());
		securitiesTransferRegisterTO.setSecurityObjectTO(securityObjectTO);
		securitiesTransferRegisterTO.setHolderAccountObjectTO(holderAccountObjectTO);		
		securitiesTransferRegisterTO.setIdTradeOperation(objSettlementOperationTO.getIdMechanismOperation());
		securitiesTransferRegisterTO.setOperationCode(ComponentConstant.INTERFACE_SECURITIES_TRANSFER_DPF);
		securitiesTransferRegisterTO.setOperationDate(CommonsUtilities.currentDate());		
		Participant objParticipant = participantServiceBean.getParticipantIsIssuer(securities.getIssuer().getIdIssuerPk());		
		if(objParticipant != null){
			securitiesTransferRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			securitiesTransferRegisterTO.setEntityNemonic(objParticipant.getMnemonic());
		}								
		securitiesTransferRegisterTO.setOperationNumber(objSettlementOperationTO.getOperationNumber());		
		return securitiesTransferRegisterTO;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param idHolderAccount the id holder account
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount(Long idHolderAccount) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HA ");
		stringBuffer.append(" FROM HolderAccount HA ");
		stringBuffer.append(" INNER JOIN FETCH HA.participant PA ");
		stringBuffer.append(" INNER JOIN FETCH HA.holderAccountDetails HAD ");
		stringBuffer.append(" INNER JOIN FETCH HAD.holder HO ");
		stringBuffer.append(" WHERE HA.idHolderAccountPk = :idHolderAccount ");
		
		TypedQuery<HolderAccount> typedQuery= em.createQuery(stringBuffer.toString(), HolderAccount.class);
		typedQuery.setParameter("idHolderAccount", idHolderAccount);
		return typedQuery.getSingleResult();
	}
	
	/**
	 * Exists trade operation at transaction.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return true, if successful
	 */
	@SuppressWarnings("rawtypes")
	public boolean existsTradeOperationAtTransaction(Long idMechanismOperation) {
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT.idInterfaceTransactionPk ");
		stringBuffer.append(" FROM InterfaceTransaction IT ");
		stringBuffer.append(" WHERE IT.tradeOperation.idTradeOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("transactionState", BooleanType.YES.getCode());
		
		List lstObject= query.getResultList();
		if (Validations.validateListIsNotNullAndNotEmpty(lstObject)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Update settlement operation sent interface.
	 *
	 * @param idSettlementOperation the id settlement operation
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateSettlementOperationSentInterface(Long idSettlementOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation ");
		stringBuffer.append(" SET indSentInterface = :indSentInterface ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("indSentInterface", BooleanType.YES.getCode());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.executeUpdate();
	}
	
	/**
	 * Update settlement account marketfact.
	 *
	 * @param lstSettlementAccountMarketfact the lst settlement account marketfact
	 * @param loggerUser the logger user
	 */
	public void updateSettlementAccountMarketfact(List<Long> lstSettlementAccountMarketfact, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountMarketfact ");
		stringBuffer.append(" SET indActive = :indActive ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettAccountMarketfactPk in (:lstSettlementAccountMarketfact) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indActive", BooleanType.NO.getCode());
		query.setParameter("lstSettlementAccountMarketfact", lstSettlementAccountMarketfact);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	public void sendInformationLastHolderDPFWebClient(OperationTutXmlDTO operationTutXmlDTO, LoggerUser loggerUser) {
		if(Validations.validateIsNotNull(operationTutXmlDTO)
				&& Validations.validateIsNotNullAndNotEmpty(operationTutXmlDTO.getEEFF())){
			try {
				operationTutXmlDTO.setNumOperation(generatedOperationNumberSendXmlService.nextOperationNumber(operationTutXmlDTO.getEEFF()));
				settlementServiceConsumer.sendInformationLastHolderDPFWebClient(operationTutXmlDTO, loggerUser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Send settlement operation web client.
	 *
	 * @param lstSettlementOperationTOs the lst settlement operation t os
	 * @param loggerUser the logger user
	 */
	public void sendSettlementOperationWebClient(List<SettlementOperationTO> lstSettlementOperationTOs, LoggerUser loggerUser) {
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementOperationTOs)) {
			for (SettlementOperationTO settlementOperationTO: lstSettlementOperationTOs) {
				try {
					//just for DPFs and DPAs
					if (SecurityClassType.DPA.getCode().equals(settlementOperationTO.getSecurityClass()) || 
						SecurityClassType.DPF.getCode().equals(settlementOperationTO.getSecurityClass())) 
					{					
						//we verify this operation doesn't exist at INTERFACE TRANSACTION
						// comentando validacion para que envie tambien el retorno de vencimiento de reportos
//						if (!existsTradeOperationAtTransaction(settlementOperationTO.getIdMechanismOperation())) {
							//settlementOperationTO.setOperationNumber(generatedOperationNumberSendXmlService.nextOperationNumber(getEifMNemonic(settlementOperationTO.getIdSecurityCode()))); 
							//settlementServiceConsumer.sendSecuritiesTransferOperationWebClient(settlementOperationTO, loggerUser);
							updateSettlementOperationSentInterface(settlementOperationTO.getIdSettlementOperation());
//						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Metodo que optiene el nemonico del emisor del DPF
	 * 
	 * @param idSecurityCode
	 * @return
	 */
	private String getEifMNemonic(String idSecurityCode){
		
		String mNemonicEIF = null;
		// Recupernado Nemonico del Emisor
		if(idSecurityCode != null && idSecurityCode.length() == 16){//--DPF-BMEN12345617
			mNemonicEIF = idSecurityCode.substring(4,7);
		}
		
		return mNemonicEIF;
	}
	
	/**
	 * Send settlement funds sending process web client.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param loggerUser the logger user
	 * @throws Exception the exception
	 */
	public void sendSettlementFundsSendingProcessWebClient(Long idSettlementProcess, Long idMechanism, 
			Long idModalityGroup, Integer idCurrency, LoggerUser loggerUser) throws Exception{
		try {		
			logger.info("sendSettlementFundsSendingProcessWebClient: Start the process ...");
			//we send funds to creditor participant according the last net positions
			Map<String, Object> fundsParameters=new HashMap<String, Object>();
			fundsParameters.put(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, idSettlementProcess);
			fundsParameters.put(SettlementConstant.MECHANISM_PARAMETER, idMechanism);
			fundsParameters.put(SettlementConstant.MODALITY_GROUP_PARAMETER, idModalityGroup);
			fundsParameters.put(SettlementConstant.CURRENCY_PARAMETER, idCurrency);
			BusinessProcess fundsBusinessProcess=new BusinessProcess();
			fundsBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.SETTLEMENT_FUNDS_SENDING_PROCESS.getCode());
			batchServiceBean.registerBatchTx(loggerUser.getUserName(), fundsBusinessProcess, fundsParameters);			
		}  catch (Exception e) {
			logger.error("sendSettlementFundsSendingProcessWebClient : Error in processing ... ");
			logger.error("idSettlementProcess: " +idSettlementProcess+ ", idMechanism: " +idMechanism+", idModalityGroup: " +idModalityGroup+ ", currency: " +idCurrency);
			throw e;
		}
		logger.info("sendSettlementFundsSendingProcessWebClient: Finish the process ... ");		
	}
	
	/**
	 * Update term price operations.
	 *
	 * @param idSettlementProcess the id settlement process
	 */
	public void updateTermPriceOperations(Long idSettlementProcess) {
		List<Object[]> lstSettledAccountMarketfactOperation= getListSettledAccountMarketfactOperations(idSettlementProcess);
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettledAccountMarketfactOperation)) {
			List<SettlementAccountMarketfact> lstUpdateTermPrice= new ArrayList<SettlementAccountMarketfact>();
			for (Object[] objAccountMarketfact: lstSettledAccountMarketfactOperation) {
				SettlementAccountMarketfact settlementAccountMarketfact= (SettlementAccountMarketfact) objAccountMarketfact[0];
				if (Validations.validateIsNotNull(objAccountMarketfact[1])) {
					BigDecimal marketPrice= new BigDecimal(objAccountMarketfact[1].toString());
					settlementAccountMarketfact.setMarketPrice(marketPrice);
					lstUpdateTermPrice.add(settlementAccountMarketfact);
				}
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstUpdateTermPrice)) {
				this.updateList(lstUpdateTermPrice);
			}
		}
	}
	
	/**
	 * Gets the list settled account marketfact operations.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @return the list settled account marketfact operations
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSettledAccountMarketfactOperations(Long idSettlementProcess) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SAM_TERM, HMB.marketPrice ");
		stringBuffer.append(" FROM SettlementAccountMarketfact SAM, HolderMarketFactBalance HMB, SettlementAccountMarketfact SAM_TERM, "
								+ " OperationSettlement OS, MechanismOperation MO ");
		stringBuffer.append(" WHERE SAM.settlementAccountOperation.settlementOperation.idSettlementOperationPk = OS.settlementOperation.idSettlementOperationPk ");
		stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.operationState = :operationState ");
		stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.operationPart = :operationPart ");
		stringBuffer.append(" and SAM.settlementAccountOperation.role = :role ");
		stringBuffer.append(" and SAM.settlementAccountOperation.holderAccountOperation.operationPart = :operationPart ");
		stringBuffer.append(" and SAM_TERM.marketPrice is null ");
		stringBuffer.append(" and SAM_TERM.marketDate = HMB.marketDate ");
		stringBuffer.append(" and SAM_TERM.marketRate = HMB.marketRate ");
		stringBuffer.append(" and SAM_TERM.indActive = :indActive ");
		stringBuffer.append(" and SAM_TERM.settlementAccountOperation.settlementAccountOperation.idSettlementAccountPk = SAM.settlementAccountOperation.idSettlementAccountPk ");
		stringBuffer.append(" and SAM_TERM.settlementAccountOperation.holderAccountOperation.holderAccount.idHolderAccountPk = HMB.holderAccount.idHolderAccountPk ");
		stringBuffer.append(" and MO.securities.idSecurityCodePk = HMB.security.idSecurityCodePk ");
		stringBuffer.append(" and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("operationState", MechanismOperationStateType.CASH_SETTLED.getCode());
		query.setParameter("operationPart", OperationPartType.CASH_PART.getCode());
		query.setParameter("role", ComponentConstant.SALE_ROLE);
		query.setParameter("indActive", BooleanType.YES.getCode());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		
		return query.getResultList();
	}
	
	public String verifyMechanismPendingSettlementOperation(Date settlementDate) {
		StringBuilder stringBuilder= new StringBuilder(); 
		List<String> lstMechanismPendingSettlementOperation= getMechanismPendingSettlementOperation(settlementDate);
		if (Validations.validateListIsNotNullAndNotEmpty(lstMechanismPendingSettlementOperation)) {
			for (String strMechanism: lstMechanismPendingSettlementOperation) {
				if (Validations.validateIsNotNullAndNotEmpty(stringBuilder.toString())) {
					stringBuilder.append(GeneralConstants.STR_COMMA).append(strMechanism);
				} else {
					stringBuilder.append(strMechanism);
				}
			}
		}
		return stringBuilder.toString();
	}
	
	@SuppressWarnings("unchecked")
	private List<String> getMechanismPendingSettlementOperation(Date settlementDate) {
		StringBuilder stringBuilder= new StringBuilder();
		stringBuilder.append(" SELECT DISTINCT SO.mechanismOperation.mechanisnModality.negotiationMechanism.mechanismName ");
		stringBuilder.append(" FROM SettlementOperation SO ");
		stringBuilder.append(" WHERE SO.settlementDate = :settlementDate ");
		stringBuilder.append(" and (SO.operationPart = :cashPart and SO.operationState in (:lstCashState) "
								+ "	or SO.operationPart = :termPart and SO.operationState = :termState ) ");
		
		Query query= em.createQuery(stringBuilder.toString());
		query.setParameter("settlementDate", settlementDate);
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		List<Integer> lstCashState= new ArrayList<Integer>();
		lstCashState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstCashState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("lstCashState", lstCashState);
		query.setParameter("termPart", OperationPartType.TERM_PART.getCode());
		query.setParameter("termState", MechanismOperationStateType.CASH_SETTLED.getCode());
		
		return query.getResultList();
	}
	
	/**
	 * Update settlement account marketfact.
	 *
	 * @param idSettAccountMarketfactPk the id sett account marketfact pk
	 * @param marketDate the market date
	 * @param marketRate the market rate
	 * @param marketPrice the market price
	 * @param loggerUser the logger user
	 */
	public void updateReassignmentRequestSettlementAccountMarketfact(Long idSettAccountMarketfactPk,
			Date marketDate, BigDecimal marketRate, BigDecimal marketPrice, boolean isFixedIncome, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountMarketfact ");
		stringBuffer.append(" SET marketDate = :marketDate ");
		if(isFixedIncome){
			stringBuffer.append(" , marketRate = :marketRate ");
		} else {
			stringBuffer.append(" , marketPrice = :marketPrice ");
		}								
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettAccountMarketfactPk = :idSettAccountMarketfactPk ");		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("marketDate", marketDate);
		if(isFixedIncome){
			query.setParameter("marketRate", marketRate);
		} else {
			query.setParameter("marketPrice", marketPrice);
		}				
		query.setParameter("idSettAccountMarketfactPk", idSettAccountMarketfactPk);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());		
		query.executeUpdate();
	}
	
	/**
	 * Update settlement account marketfact.
	 *
	 * @param idHolderAccountOperationPk the id holder account operation pk
	 * @param marketDate the market date
	 * @param marketRate the market rate
	 * @param marketPrice the market price
	 * @param isFixedIncome the is fixed income
	 * @param loggerUser the logger user
	 */
	public void updateReassignmentRequestAccountOperationMarketFact(Long idHolderAccountOperationPk, Date marketDate, 
			BigDecimal marketRate, BigDecimal marketPrice, boolean isFixedIncome, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE AccountOperationMarketFact aomf ");
		stringBuffer.append(" SET aomf.marketDate = :marketDate ");
		if(isFixedIncome){
			stringBuffer.append(" , aomf.marketRate = :marketRate ");
		} else {
			stringBuffer.append(" , aomf.marketPrice = :marketPrice ");
		}								
		stringBuffer.append(" , aomf.lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , aomf.lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , aomf.lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , aomf.lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE aomf.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperationPk ");		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("marketDate", marketDate);
		if(isFixedIncome){
			query.setParameter("marketRate", marketRate);
		} else {
			query.setParameter("marketPrice", marketPrice);
		}				
		query.setParameter("idHolderAccountOperationPk", idHolderAccountOperationPk);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());		
		query.executeUpdate();
	}
	
	/**
	 * Gets the negotiation mechanism service.
	 *
	 * @return the negotiation mechanism service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getNegotiationMechanismService() throws ServiceException{
		String querySql = "SELECT negMec FROM NegotiationMechanism negMec";
		Query query = em.createQuery(querySql);
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<NegotiationMechanism>();
		}
	}
	
	public void calculateIssuerAmountAutomatic(Date expirationDate, LoggerUser loggerUser) {
		List<Object[]> listCorporativesByExpirationDate= getListCorporativesByExpirationDate(expirationDate);
		if (Validations.validateListIsNotNullAndNotEmpty(listCorporativesByExpirationDate)) {
			for (Object[] arrObject: listCorporativesByExpirationDate) {
				Long idCorporativeOperation= new Long(arrObject[0].toString());
				BigDecimal stockQuantity= new BigDecimal(arrObject[1].toString());
				BigDecimal couponAmount= null;
				if (Validations.validateIsNotNull(arrObject[2])) {
					couponAmount= new BigDecimal(arrObject[2].toString());
				} else if (Validations.validateIsNotNull(arrObject[3])) {
					couponAmount= new BigDecimal(arrObject[3].toString());
				}
				BigDecimal issuerAmount= stockQuantity.multiply(couponAmount);
				updateCorporativeIssuerAmount(idCorporativeOperation, issuerAmount, loggerUser);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getListCorporativesByExpirationDate(Date expirationDate) {
		StringBuilder stringBuilder= new StringBuilder();
		stringBuilder.append(" SELECT co.idCorporativeOperationPk, ");
		stringBuilder.append(" co.securities.desmaterializedBalance, ");
		stringBuilder.append(" pic.couponAmount, ");
		stringBuilder.append(" pac.couponAmount ");
		stringBuilder.append(" FROM CorporativeOperation co ");
		stringBuilder.append(" LEFT JOIN co.programInterestCoupon pic ");
		stringBuilder.append(" LEFT JOIN co.programAmortizationCoupon pac ");
		stringBuilder.append(" WHERE (pic.experitationDate = :expirationDate or pac.expirationDate = :expirationDate) ");
		stringBuilder.append(" and co.corporativeEventType.corporativeEventType in (:lstCorporativeEventType) ");
		stringBuilder.append(" and co.securities.indPaymentBenefit = :indPaymentBenefit ");
		
		Query query= em.createQuery(stringBuilder.toString());
		query.setParameter("expirationDate", expirationDate);
		List<Integer> lstCorporativeEventType= new ArrayList<Integer>();
		lstCorporativeEventType.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		lstCorporativeEventType.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		query.setParameter("lstCorporativeEventType", lstCorporativeEventType);
		query.setParameter("indPaymentBenefit", BooleanType.YES.getCode());
		
		return query.getResultList();
	}
	
	public void updateCorporativeIssuerAmount(Long idCorporativeOperation, BigDecimal issuerAmount, LoggerUser loggerUser) {
		StringBuilder stringBuilder= new StringBuilder();
		stringBuilder.append(" UPDATE CorporativeOperation ");
		stringBuilder.append(" SET issuerConfirmedAmount = :issuerAmount ");
		stringBuilder.append(" , lastModifyApp = :lastModifyApp ");
		stringBuilder.append(" , lastModifyDate = :lastModifyDate ");
		stringBuilder.append(" , lastModifyIp = :lastModifyIp ");
		stringBuilder.append(" , lastModifyUser = :lastModifyUser ");
		stringBuilder.append(" WHERE idCorporativeOperationPk = :idCorporativeOperation ");
		
		Query query= em.createQuery(stringBuilder.toString());
		query.setParameter("issuerAmount", issuerAmount);
		query.setParameter("idCorporativeOperation", idCorporativeOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Identificar compras forzosas, fuera del singleton
	 * @param settlementDate
	 * @param loggerUser
	 * @throws ServiceException
	 */
	public void identifyForcedPurchase(Date settlementDate, LoggerUser loggerUser) throws ServiceException
	{
		//we get the list of settlement operation to be settled as forced purchase
		List<Long> lstIdSettlementOperation= getListSettlementOperationToForcedPurchase(settlementDate);
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstIdSettlementOperation)) {
			//we must update the settlement type to FOP and settlement schema to GROSS for each settlement operation
			updateSettlementOperationToForcedPurchase(lstIdSettlementOperation, loggerUser);
		}
	}
	
}