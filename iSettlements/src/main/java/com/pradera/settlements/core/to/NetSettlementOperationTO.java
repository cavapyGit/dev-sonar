package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.constants.Constants;
import com.pradera.model.settlement.OperationSettlement;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NetSettlementOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class NetSettlementOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The id modality. */
	private Long idModality;
	
	/** The modality name. */
	private String modalityName;
	
	/** The id mechanism. */
	private Long idMechanism;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The id sale participant. */
	private Long idSaleParticipant;
	
	/** The sale participant description. */
	private String saleParticipantDescription;
	
	/** The sale participant nemonic. */
	private String saleParticipantNemonic;
	
	/** The id purchase participant. */
	private Long idPurchaseParticipant;
	
	/** The purchase participant description. */
	private String purchaseParticipantDescription;
	
	/** The purchase participant nemonic. */
	private String purchaseParticipantNemonic;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The operation state. */
	private Integer operationState;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The operation part. */
	private Integer operationPart;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The settlement amount. */
	private BigDecimal settlementAmount;
	
	/** The participant purchase amount. */
	private BigDecimal participantPurchaseAmount;
	
	/** The participant sale amount. */
	private BigDecimal participantSaleAmount;
	
	/** The disabled. */
	private boolean disabled;
	
	/** The selected. */
	private boolean selected;
	
	/** The crossed. */
	private boolean crossed;
	
	/** The sale participant code nemonic. */
	private String saleParticipantCodeNemonic;
	
	/** The purchase participant code nemonic. */
	private String purchaseParticipantCodeNemonic;
	
	/** The ballot sequential. */
	private String ballotSequential;
	
	/** The operation settlement. */
	private OperationSettlement operationSettlement;
	
	/** The lst participant settlements. */
	private List<FundsParticipantSettlementTO> lstParticipantSettlements;
	
	/** The ind chained holder operation. */
	private String indChainedHolderOperation;
	
	/** The participant role. */
	private Integer participantRole;
	
	/**
	 * Instantiates a new net settlement operation to.
	 */
	public NetSettlementOperationTO() {
		participantPurchaseAmount = BigDecimal.ZERO;
		participantSaleAmount = BigDecimal.ZERO;
	}
	
	/**
	 * Gets the sale participant code nemonic.
	 *
	 * @return the sale participant code nemonic
	 */
	public String getSaleParticipantCodeNemonic(){
    	
		if(Validations.validateIsNullOrEmpty(saleParticipantCodeNemonic)){
    		if(Validations.validateIsNotNullAndPositive(idSaleParticipant) && Validations.validateIsNotNull(saleParticipantNemonic)){
    			StringBuffer sb = new StringBuffer();
        		sb.append(saleParticipantNemonic);
        		sb.append(Constants.DASH);
        		sb.append(idSaleParticipant);    		
        		saleParticipantCodeNemonic = sb.toString();
        	}
    	}
    	return saleParticipantCodeNemonic;
    }
	
	/**
	 * Gets the purchase participant code nemonic.
	 *
	 * @return the purchase participant code nemonic
	 */
	public String getPurchaseParticipantCodeNemonic(){
    	if(Validations.validateIsNullOrEmpty(purchaseParticipantCodeNemonic)){
    		if(Validations.validateIsNotNullAndPositive(idPurchaseParticipant) && Validations.validateIsNotNullAndNotEmpty(purchaseParticipantNemonic)){
    			StringBuffer sb = new StringBuffer();
        		sb.append(purchaseParticipantNemonic);
        		sb.append(Constants.DASH);
        		sb.append(idPurchaseParticipant);    		
        		purchaseParticipantCodeNemonic = sb.toString();
        	}
    	}
    	return purchaseParticipantCodeNemonic;
    }
	
	/**
	 * Gets the ballot sequential.
	 *
	 * @return the ballot sequential
	 */
	public String getBallotSequential(){
		if(ballotSequential == null){
			StringBuilder sbOpNumber = new StringBuilder();
			if(ballotNumber!=null && sequential!=null){
				sbOpNumber.append(this.ballotNumber).append(GeneralConstants.SLASH).append(this.sequential);
			}
			ballotSequential = sbOpNumber.toString();
		}
		return ballotSequential;
	}
	
	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}
	
	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}
	
	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}
	
	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}
	
	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}
	
	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}
	
	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return idMechanism;
	}
	
	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}
	
	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}
	
	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}
	
	/**
	 * Gets the sale participant description.
	 *
	 * @return the sale participant description
	 */
	public String getSaleParticipantDescription() {
		return saleParticipantDescription;
	}
	
	/**
	 * Sets the sale participant description.
	 *
	 * @param saleParticipantDescription the new sale participant description
	 */
	public void setSaleParticipantDescription(String saleParticipantDescription) {
		this.saleParticipantDescription = saleParticipantDescription;
	}
	
	/**
	 * Gets the sale participant nemonic.
	 *
	 * @return the sale participant nemonic
	 */
	public String getSaleParticipantNemonic() {
		return saleParticipantNemonic;
	}
	
	/**
	 * Sets the sale participant nemonic.
	 *
	 * @param saleParticipantNemonic the new sale participant nemonic
	 */
	public void setSaleParticipantNemonic(String saleParticipantNemonic) {
		this.saleParticipantNemonic = saleParticipantNemonic;
	}
	
	/**
	 * Gets the id sale participant.
	 *
	 * @return the id sale participant
	 */
	public Long getIdSaleParticipant() {
		return idSaleParticipant;
	}
	
	/**
	 * Sets the id sale participant.
	 *
	 * @param idSaleParticipant the new id sale participant
	 */
	public void setIdSaleParticipant(Long idSaleParticipant) {
		this.idSaleParticipant = idSaleParticipant;
	}
	
	/**
	 * Gets the id purchase participant.
	 *
	 * @return the id purchase participant
	 */
	public Long getIdPurchaseParticipant() {
		return idPurchaseParticipant;
	}
	
	/**
	 * Sets the id purchase participant.
	 *
	 * @param idPurchaseParticipant the new id purchase participant
	 */
	public void setIdPurchaseParticipant(Long idPurchaseParticipant) {
		this.idPurchaseParticipant = idPurchaseParticipant;
	}
	
	/**
	 * Gets the purchase participant description.
	 *
	 * @return the purchase participant description
	 */
	public String getPurchaseParticipantDescription() {
		return purchaseParticipantDescription;
	}
	
	/**
	 * Sets the purchase participant description.
	 *
	 * @param purchaseParticipantDescription the new purchase participant description
	 */
	public void setPurchaseParticipantDescription(
			String purchaseParticipantDescription) {
		this.purchaseParticipantDescription = purchaseParticipantDescription;
	}
	
	/**
	 * Gets the purchase participant nemonic.
	 *
	 * @return the purchase participant nemonic
	 */
	public String getPurchaseParticipantNemonic() {
		return purchaseParticipantNemonic;
	}
	
	/**
	 * Sets the purchase participant nemonic.
	 *
	 * @param purchaseParticipantNemonic the new purchase participant nemonic
	 */
	public void setPurchaseParticipantNemonic(String purchaseParticipantNemonic) {
		this.purchaseParticipantNemonic = purchaseParticipantNemonic;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}
	
	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}
	
	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	
	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	
	/**
	 * Gets the id settlement operation.
	 *
	 * @return the id settlement operation
	 */
	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}
	
	/**
	 * Sets the id settlement operation.
	 *
	 * @param idSettlementOperation the new id settlement operation
	 */
	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}
	
	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}
	
	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}
	
	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	
	/**
	 * Gets the operation settlement.
	 *
	 * @return the operation settlement
	 */
	public OperationSettlement getOperationSettlement() {
		return operationSettlement;
	}
	
	/**
	 * Sets the operation settlement.
	 *
	 * @param operationSettlement the new operation settlement
	 */
	public void setOperationSettlement(OperationSettlement operationSettlement) {
		this.operationSettlement = operationSettlement;
	}
	
	/**
	 * Gets the participant purchase amount.
	 *
	 * @return the participant purchase amount
	 */
	public BigDecimal getParticipantPurchaseAmount() {
		return participantPurchaseAmount;
	}
	
	/**
	 * Sets the participant purchase amount.
	 *
	 * @param participantPurchaseAmount the new participant purchase amount
	 */
	public void setParticipantPurchaseAmount(BigDecimal participantPurchaseAmount) {
		this.participantPurchaseAmount = participantPurchaseAmount;
	}
	
	/**
	 * Gets the participant sale amount.
	 *
	 * @return the participant sale amount
	 */
	public BigDecimal getParticipantSaleAmount() {
		return participantSaleAmount;
	}
	
	/**
	 * Sets the participant sale amount.
	 *
	 * @param participantSaleAmount the new participant sale amount
	 */
	public void setParticipantSaleAmount(BigDecimal participantSaleAmount) {
		this.participantSaleAmount = participantSaleAmount;
	}
	
	/**
	 * Checks if is disabled.
	 *
	 * @return true, if is disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}
	
	/**
	 * Sets the disabled.
	 *
	 * @param disabled the new disabled
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	/**
	 * Sets the sale participant code nemonic.
	 *
	 * @param saleParticipantCodeNemonic the new sale participant code nemonic
	 */
	public void setSaleParticipantCodeNemonic(String saleParticipantCodeNemonic) {
		this.saleParticipantCodeNemonic = saleParticipantCodeNemonic;
	}

	/**
	 * Sets the purchase participant code nemonic.
	 *
	 * @param purchaseParticipantCodeNemonic the new purchase participant code nemonic
	 */
	public void setPurchaseParticipantCodeNemonic(
			String purchaseParticipantCodeNemonic) {
		this.purchaseParticipantCodeNemonic = purchaseParticipantCodeNemonic;
	}

	/**
	 * Sets the ballot sequential.
	 *
	 * @param ballotSequential the new ballot sequential
	 */
	public void setBallotSequential(String ballotSequential) {
		this.ballotSequential = ballotSequential;
	}

	/**
	 * Gets the lst participant settlements.
	 *
	 * @return the lst participant settlements
	 */
	public List<FundsParticipantSettlementTO> getLstParticipantSettlements() {
		return lstParticipantSettlements;
	}

	/**
	 * Sets the lst participant settlements.
	 *
	 * @param lstParticipantSettlements the new lst participant settlements
	 */
	public void setLstParticipantSettlements(
			List<FundsParticipantSettlementTO> lstParticipantSettlements) {
		this.lstParticipantSettlements = lstParticipantSettlements;
	}

	/**
	 * Gets the ind chained holder operation.
	 *
	 * @return the ind chained holder operation
	 */
	public String getIndChainedHolderOperation() {
		return indChainedHolderOperation;
	}

	/**
	 * Sets the ind chained holder operation.
	 *
	 * @param indChainedHolderOperation the new ind chained holder operation
	 */
	public void setIndChainedHolderOperation(String indChainedHolderOperation) {
		this.indChainedHolderOperation = indChainedHolderOperation;
	}

	/**
	 * Checks if is crossed.
	 *
	 * @return true, if is crossed
	 */
	public boolean isCrossed() {
		return crossed;
	}

	/**
	 * Sets the crossed.
	 *
	 * @param crossed the new crossed
	 */
	public void setCrossed(boolean crossed) {
		this.crossed = crossed;
	}

	/**
	 * Gets the participant role.
	 *
	 * @return the participant role
	 */
	public Integer getParticipantRole() {
		return participantRole;
	}

	/**
	 * Sets the participant role.
	 *
	 * @param participantRole the new participant role
	 */
	public void setParticipantRole(Integer participantRole) {
		this.participantRole = participantRole;
	}

	
}
