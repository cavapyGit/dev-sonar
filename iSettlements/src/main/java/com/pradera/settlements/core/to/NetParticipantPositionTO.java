package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.constants.Constants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NetParticipantPositionTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class NetParticipantPositionTO implements Serializable, Cloneable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The participant description. */
	private String participantDescription;
	
	/** The participant nemonic. */
	private String participantNemonic;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The modality name. */
	private String modalityName;
	
	/** The sale settlement amount. */
	private BigDecimal saleSettlementAmount;
	
	/** The purchase settlement amount. */
	private BigDecimal purchaseSettlementAmount;
	
	/** The net position. */
	private BigDecimal netPosition;
	
	/** The initial net position. */
	private BigDecimal initialNetPosition;
	
	/** The available cash amount. */
	private BigDecimal availableCashAmount;
	
	/** The missing amount. */
	private BigDecimal missingAmount;
	
	/** The display code mnemonic. */
	private String displayCodeMnemonic;
	
	/** The display code description. */
	private String displayCodeDescription;
	
	/** The is debtor. */
	private boolean isDebtor;
	
	/** The check all. */
	private boolean checkAll;
	
	/** The lst settlement operation. */
	private List<NetSettlementOperationTO> lstSettlementOperation;
	
	/** The net position. */
	private BigDecimal netPositionTemp;
	
	/**
	 * Update check all settlement editions.
	 */
	public void updateCheckAllSettlementEditions(){
		checkAll = false;
		if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementOperation)){
			for (NetSettlementOperationTO netSettlementOperationTO : lstSettlementOperation) {
				if(!netSettlementOperationTO.isSelected()){
					return;
				}
			}
			checkAll = true;
		}
	}
	
	/**
	 * Instantiates a new net participant position to.
	 */
	public NetParticipantPositionTO() {
		super();
		this.saleSettlementAmount = BigDecimal.ZERO;
		this.purchaseSettlementAmount = BigDecimal.ZERO;
		this.lstSettlementOperation = new LinkedList<NetSettlementOperationTO>();
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the display code mnemonic.
	 *
	 * @param displayCodeMnemonic the new display code mnemonic
	 */
	public void setDisplayCodeMnemonic(String displayCodeMnemonic){
    	this.displayCodeMnemonic = displayCodeMnemonic;
    }
    
    /**
     * Gets the display code mnemonic.
     *
     * @return the display code mnemonic
     */
    public String getDisplayCodeMnemonic(){
    	if(Validations.validateIsNullOrEmpty(displayCodeMnemonic)){
    		if(Validations.validateIsNotNull(idParticipant) && Validations.validateIsNotNull(participantNemonic)){
    			StringBuffer sb = new StringBuffer();
        		sb.append(participantNemonic);
        		sb.append(Constants.DASH);
        		sb.append(idParticipant);    		
        		displayCodeMnemonic = sb.toString();
        	}
    	}
    	return displayCodeMnemonic;
    }
    
    /**
     * Gets the display code description.
     *
     * @return the display code description
     */
    public String getDisplayCodeDescription(){
    	if(Validations.validateIsNullOrEmpty(displayCodeDescription)){
	    	if(Validations.validateIsNotNull(idParticipant) && Validations.validateIsNotNull(participantDescription) && Validations.validateIsNotNull(participantNemonic)){
	    		StringBuffer sb = new StringBuffer();
	    		sb.append(participantNemonic).append(Constants.DASH).append(idParticipant).append(Constants.DASH).append(participantDescription);
	    		displayCodeDescription = sb.toString();
	    	}
    	}
    	return displayCodeDescription;
    }

	/**
	 * Sets the display code description.
	 *
	 * @param displayCodeDescription the new display code description
	 */
	public void setDisplayCodeDescription(String displayCodeDescription) {
		this.displayCodeDescription = displayCodeDescription;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return participantDescription;
	}

	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}

	/**
	 * Gets the participant nemonic.
	 *
	 * @return the participant nemonic
	 */
	public String getParticipantNemonic() {
		return participantNemonic;
	}

	/**
	 * Sets the participant nemonic.
	 *
	 * @param participantNemonic the new participant nemonic
	 */
	public void setParticipantNemonic(String participantNemonic) {
		this.participantNemonic = participantNemonic;
	}

	/**
	 * Gets the lst settlement operation.
	 *
	 * @return the lst settlement operation
	 */
	public List<NetSettlementOperationTO> getLstSettlementOperation() {
		return lstSettlementOperation;
	}

	/**
	 * Sets the lst settlement operation.
	 *
	 * @param lstSettlementOperation the new lst settlement operation
	 */
	public void setLstSettlementOperation(
			List<NetSettlementOperationTO> lstSettlementOperation) {
		this.lstSettlementOperation = lstSettlementOperation;
	}

	/**
	 * Gets the sale settlement amount.
	 *
	 * @return the sale settlement amount
	 */
	public BigDecimal getSaleSettlementAmount() {
		return saleSettlementAmount;
	}

	/**
	 * Sets the sale settlement amount.
	 *
	 * @param saleSettlementAmount the new sale settlement amount
	 */
	public void setSaleSettlementAmount(BigDecimal saleSettlementAmount) {
		this.saleSettlementAmount = saleSettlementAmount;
	}

	/**
	 * Gets the purchase settlement amount.
	 *
	 * @return the purchase settlement amount
	 */
	public BigDecimal getPurchaseSettlementAmount() {
		return purchaseSettlementAmount;
	}

	/**
	 * Sets the purchase settlement amount.
	 *
	 * @param purchaseSettlementAmount the new purchase settlement amount
	 */
	public void setPurchaseSettlementAmount(BigDecimal purchaseSettlementAmount) {
		this.purchaseSettlementAmount = purchaseSettlementAmount;
	}

	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}

	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}

	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}

	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}

	/**
	 * Gets the net position.
	 *
	 * @return the net position
	 */
	public BigDecimal getNetPosition() {
		return netPosition;
	}

	/**
	 * Sets the net position.
	 *
	 * @param netPosition the new net position
	 */
	public void setNetPosition(BigDecimal netPosition) {
		this.netPosition = netPosition;
	}

	/**
	 * Gets the available cash amount.
	 *
	 * @return the available cash amount
	 */
	public BigDecimal getAvailableCashAmount() {
		return availableCashAmount;
	}

	/**
	 * Sets the available cash amount.
	 *
	 * @param availableCashAmount the new available cash amount
	 */
	public void setAvailableCashAmount(BigDecimal availableCashAmount) {
		this.availableCashAmount = availableCashAmount;
	}

	/**
	 * Gets the missing amount.
	 *
	 * @return the missing amount
	 */
	public BigDecimal getMissingAmount() {
		return missingAmount;
	}

	/**
	 * Sets the missing amount.
	 *
	 * @param missingAmount the new missing amount
	 */
	public void setMissingAmount(BigDecimal missingAmount) {
		this.missingAmount = missingAmount;
	}

	/**
	 * Checks if is check all.
	 *
	 * @return true, if is check all
	 */
	public boolean isCheckAll() {
		return checkAll;
	}

	/**
	 * Sets the check all.
	 *
	 * @param checkAll the new check all
	 */
	public void setCheckAll(boolean checkAll) {
		this.checkAll = checkAll;
	}

	/**
	 * Checks if is debtor.
	 *
	 * @return true, if is debtor
	 */
	public boolean isDebtor() {
		return isDebtor;
	}

	/**
	 * Sets the debtor.
	 *
	 * @param isDebtor the new debtor
	 */
	public void setDebtor(boolean isDebtor) {
		this.isDebtor = isDebtor;
	}

	/**
	 * Gets the initial net position.
	 *
	 * @return the initial net position
	 */
	public BigDecimal getInitialNetPosition() {
		return initialNetPosition;
	}

	/**
	 * Sets the initial net position.
	 *
	 * @param initialNetPosition the new initial net position
	 */
	public void setInitialNetPosition(BigDecimal initialNetPosition) {
		this.initialNetPosition = initialNetPosition;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public NetParticipantPositionTO clone() throws CloneNotSupportedException {
        return (NetParticipantPositionTO) super.clone();
    }

	public BigDecimal getNetPositionTemp() {
		return netPositionTemp;
	}

	public void setNetPositionTemp(BigDecimal netPositionTemp) {
		this.netPositionTemp = netPositionTemp;
	}
	
	
}
