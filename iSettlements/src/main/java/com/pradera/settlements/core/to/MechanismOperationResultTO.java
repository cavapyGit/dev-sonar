/*
 * 
 */
package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

//import com.pradera.model.negotiation.type.NegotiationOperationStateType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class MechanismOperationResultTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The id repo settlement reques pk. */
	private Long idRepoSettlementRequesPk;
	
	/** The id repo settlement status. */
	private Long idRepoSettlementStatus;
	
	/** The id mechanism operation request pk. */
	private Long idMechanismOperationRequestPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The modality name. */
	private String modalityName;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id part seller. */
	private Long idPartSeller;
	
	/** The mnemo part seller. */
	private String mnemoPartSeller;
	
	/** The id part buyer. */
	private Long idPartBuyer;
	
	/** The mnemo part buyer. */
	private String mnemoPartBuyer;
	
	/** The id part in charge. */
	private Long idPartInCharge;
	
	/** The mnemo part in charge. */
	private String mnemoPartInCharge;
	
	/** The settlement cash date. */
	private Date settlementCashDate;
	
	/** The settlement term date. */
	private Date settlementTermDate;
	
	/** The isin code. */
	private String idSecurityCode;
	
	/** The isin description. */
	private String isinDescription;
	
	/** The operation state. */
	private Integer operationState;
	
	/** The state description. */
	private String stateDescription;
	
	/** The request date. */
	private Date requestDate;

	/**
	 * Gets the id mechanism operation request pk.
	 *
	 * @return the id mechanism operation request pk
	 */
	public Long getIdMechanismOperationRequestPk() {
		return idMechanismOperationRequestPk;
	}

	/**
	 * Sets the id mechanism operation request pk.
	 *
	 * @param idMechanismOperationRequestPk the new id mechanism operation request pk
	 */
	public void setIdMechanismOperationRequestPk(Long idMechanismOperationRequestPk) {
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the id part seller.
	 *
	 * @return the id part seller
	 */
	public Long getIdPartSeller() {
		return idPartSeller;
	}

	/**
	 * Sets the id part seller.
	 *
	 * @param idPartSeller the new id part seller
	 */
	public void setIdPartSeller(Long idPartSeller) {
		this.idPartSeller = idPartSeller;
	}

	/**
	 * Gets the mnemo part seller.
	 *
	 * @return the mnemo part seller
	 */
	public String getMnemoPartSeller() {
		return mnemoPartSeller;
	}

	/**
	 * Sets the mnemo part seller.
	 *
	 * @param mnemoPartSeller the new mnemo part seller
	 */
	public void setMnemoPartSeller(String mnemoPartSeller) {
		this.mnemoPartSeller = mnemoPartSeller;
	}

	/**
	 * Gets the id part buyer.
	 *
	 * @return the id part buyer
	 */
	public Long getIdPartBuyer() {
		return idPartBuyer;
	}

	/**
	 * Sets the id part buyer.
	 *
	 * @param idPartBuyer the new id part buyer
	 */
	public void setIdPartBuyer(Long idPartBuyer) {
		this.idPartBuyer = idPartBuyer;
	}

	/**
	 * Gets the mnemo part buyer.
	 *
	 * @return the mnemo part buyer
	 */
	public String getMnemoPartBuyer() {
		return mnemoPartBuyer;
	}

	/**
	 * Sets the mnemo part buyer.
	 *
	 * @param mnemoPartBuyer the new mnemo part buyer
	 */
	public void setMnemoPartBuyer(String mnemoPartBuyer) {
		this.mnemoPartBuyer = mnemoPartBuyer;
	}

	/**
	 * Gets the id part in charge.
	 *
	 * @return the id part in charge
	 */
	public Long getIdPartInCharge() {
		return idPartInCharge;
	}

	/**
	 * Sets the id part in charge.
	 *
	 * @param idPartInCharge the new id part in charge
	 */
	public void setIdPartInCharge(Long idPartInCharge) {
		this.idPartInCharge = idPartInCharge;
	}

	/**
	 * Gets the mnemo part in charge.
	 *
	 * @return the mnemo part in charge
	 */
	public String getMnemoPartInCharge() {
		return mnemoPartInCharge;
	}

	/**
	 * Sets the mnemo part in charge.
	 *
	 * @param mnemoPartInCharge the new mnemo part in charge
	 */
	public void setMnemoPartInCharge(String mnemoPartInCharge) {
		this.mnemoPartInCharge = mnemoPartInCharge;
	}

	/**
	 * Gets the settlement cash date.
	 *
	 * @return the settlement cash date
	 */
	public Date getSettlementCashDate() {
		return settlementCashDate;
	}

	/**
	 * Sets the settlement cash date.
	 *
	 * @param settlementCashDate the new settlement cash date
	 */
	public void setSettlementCashDate(Date settlementCashDate) {
		this.settlementCashDate = settlementCashDate;
	}

	/**
	 * Gets the settlement term date.
	 *
	 * @return the settlement term date
	 */
	public Date getSettlementTermDate() {
		return settlementTermDate;
	}

	/**
	 * Sets the settlement term date.
	 *
	 * @param settlementTermDate the new settlement term date
	 */
	public void setSettlementTermDate(Date settlementTermDate) {
		this.settlementTermDate = settlementTermDate;
	}

	/**
	 * Gets the isin code.
	 *
	 * @return the isin code
	 */
	public String getIsinCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the new isin code
	 */
	public void setIsinCode(String isinCode) {
		this.idSecurityCode = isinCode;
	}

	/**
	 * Gets the isin description.
	 *
	 * @return the isin description
	 */
	public String getIsinDescription() {
		return isinDescription;
	}

	/**
	 * Sets the isin description.
	 *
	 * @param isinDescription the new isin description
	 */
	public void setIsinDescription(String isinDescription) {
		this.isinDescription = isinDescription;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}

	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}

	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}

	/**
	 * Instantiates a new otc operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 */
	public MechanismOperationResultTO(Long idMechanismOperationRequestPk) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
	}

	/**
	 * Instantiates a new otc operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 * @param modalityName the modality name
	 */
	public MechanismOperationResultTO(Long idMechanismOperationRequestPk,String modalityName) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
		this.modalityName = modalityName;
	}

	/**
	 * Instantiates a new mechanism operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 * @param modalityName the modality name
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @param idPartSeller the id part seller
	 * @param mnemoPartSeller the mnemo part seller
	 * @param idPartBuyer the id part buyer
	 * @param mnemoPartBuyer the mnemo part buyer
	 */
	public MechanismOperationResultTO(Long idMechanismOperationRequestPk,
			String modalityName, Date operationDate, Long operationNumber,
			Long idPartSeller, String mnemoPartSeller, Long idPartBuyer,
			String mnemoPartBuyer) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
		this.modalityName = modalityName;
		this.operationDate = operationDate;
		this.operationNumber = operationNumber;
		this.idPartSeller = idPartSeller;
		this.mnemoPartSeller = mnemoPartSeller;
		this.idPartBuyer = idPartBuyer;
		this.mnemoPartBuyer = mnemoPartBuyer;
	}

	/**
	 * Instantiates a new mechanism operation result to.
	 *
	 * @param idMechanismOperationRequestPk the id mechanism operation request pk
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 * @param modalityName the modality name
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @param idPartSeller the id part seller
	 * @param mnemoPartSeller the mnemo part seller
	 * @param idPartBuyer the id part buyer
	 * @param mnemoPartBuyer the mnemo part buyer
	 * @param settlementCashDate the settlement cash date
	 * @param settlementTermDate the settlement term date
	 * @param isinCode the isin code
	 * @param state the state
	 */
	public MechanismOperationResultTO(Long idMechanismOperationRequestPk, Long idNegotiationModalityPk,
			String modalityName, Date operationDate, Long operationNumber,
			Long idPartSeller, String mnemoPartSeller, Long idPartBuyer,
			String mnemoPartBuyer, Date settlementCashDate,
			Date settlementTermDate, String isinCode, Integer state) {
		super();
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
		this.idNegotiationModalityPk = idNegotiationModalityPk;
		this.modalityName = modalityName;
		this.operationDate = operationDate;
		this.operationNumber = operationNumber;
		this.idPartSeller = idPartSeller;
		this.mnemoPartSeller = mnemoPartSeller;
		this.idPartBuyer = idPartBuyer;
		this.mnemoPartBuyer = mnemoPartBuyer;
		this.settlementCashDate = settlementCashDate;
		this.settlementTermDate = settlementTermDate;
		this.idSecurityCode = isinCode;
		this.operationState = state;
	}
	
	/**
	 * Instantiates a new repo operation result to.
	 *
	 * @param requestDate the request date
	 * @param idRepoSettlementRequesPk the id repo settlement reques pk
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 * @param modalityName the modality name
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @param idPartSeller the id part seller
	 * @param mnemoPartSeller the mnemo part seller
	 * @param idPartBuyer the id part buyer
	 * @param mnemoPartBuyer the mnemo part buyer
	 * @param settlementTermDate the settlement term date
	 * @param isinCode the isin code
	 * @param state the state
	 * @param mechanismName the mechanism name
	 * @param idMechanismOperationPk the id mechanism operation pk
	 */
	public MechanismOperationResultTO(Date requestDate, Long idRepoSettlementRequesPk, Long idNegotiationModalityPk,
			String modalityName, Date operationDate, Long operationNumber,
			Long idPartSeller, String mnemoPartSeller, Long idPartBuyer,
			String mnemoPartBuyer, Date settlementTermDate,
			String isinCode, Long state, String mechanismName, Long idMechanismOperationPk) {
		super();
		this.requestDate = requestDate;
		this.idRepoSettlementRequesPk = idRepoSettlementRequesPk;
		this.idNegotiationModalityPk = idNegotiationModalityPk;
		this.modalityName = modalityName;
		this.operationDate = operationDate;
		this.operationNumber = operationNumber;
		this.idPartSeller = idPartSeller;
		this.mnemoPartSeller = mnemoPartSeller;
		this.idPartBuyer = idPartBuyer;
		this.mnemoPartBuyer = mnemoPartBuyer;
		this.settlementTermDate = settlementTermDate;
		this.idSecurityCode = isinCode;
		this.idRepoSettlementStatus = state;
		this.mechanismName = mechanismName;
		this.idMechanismOperationRequestPk = idMechanismOperationPk;
	}

	/**
	 * Instantiates a new mechanism operation result to.
	 */
	public MechanismOperationResultTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
	 * Gets the id repo settlement reques pk.
	 *
	 * @return the id repo settlement reques pk
	 */
	public Long getIdRepoSettlementRequesPk() {
		return idRepoSettlementRequesPk;
	}

	/**
	 * Sets the id repo settlement reques pk.
	 *
	 * @param idRepoSettlementRequesPk the new id repo settlement reques pk
	 */
	public void setIdRepoSettlementRequesPk(Long idRepoSettlementRequesPk) {
		this.idRepoSettlementRequesPk = idRepoSettlementRequesPk;
	}

	/**
	 * Gets the id repo settlement status.
	 *
	 * @return the id repo settlement status
	 */
	public Long getIdRepoSettlementStatus() {
		return idRepoSettlementStatus;
	}
	
	/**
	 * Gets the id repo settlement status description.
	 *
	 * @return the id repo settlement status description
	 */
	public String getIdRepoSettlementStatusDescription() {
		return SettlementDateRequestStateType.lookup.get(new Integer(idRepoSettlementStatus.toString())).getDescription();
	}

	/**
	 * Sets the id repo settlement status.
	 *
	 * @param idRepoSettlementStatus the new id repo settlement status
	 */
	public void setIdRepoSettlementStatus(Long idRepoSettlementStatus) {
		this.idRepoSettlementStatus = idRepoSettlementStatus;
	}

	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}

	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}

	/**
	 * Gets the request date.
	 *
	 * @return the request date
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * Sets the request date.
	 *
	 * @param requestDate the new request date
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
}