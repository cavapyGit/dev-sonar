package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
//import com.pradera.core.component.business.service.ExecutorComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.guarantees.GuaranteeMarketFactOperation;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeFundsStateType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GuaranteesSettlement.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class GuaranteesSettlement extends CrudDaoServiceBean{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(GuaranteesSettlement.class);
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	
	/**
	 * Generate withdrawal reporting guarante balance.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param operationPart the operation part
	 * @param settlementSchema the settlement schema
	 * @param idModality the id modality
	 * @param originalQuantity the original quantity
	 * @param saleQuantity the sale quantity
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal generateWithdrawalReportingGuaranteBalance(Long idHolderAccountOperation, Integer operationPart, Integer settlementSchema, Long idModality, 
																											BigDecimal originalQuantity, BigDecimal saleQuantity) throws ServiceException
	{
		BigDecimal movedGuaranteeQuantity= BigDecimal.ZERO;
		//we get the list of guarantee accounts for the reporting holder account operation. It is necessary only stock dividends from principal 
		List<Object[]> principalStockDividendsBalance = getListGuarateesHolderAccount(idHolderAccountOperation,GuaranteeClassType.SECURITIES.getCode(),
																														GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode());
		if (Validations.validateListIsNotNullAndNotEmpty(principalStockDividendsBalance))
		{
			for (Object[] arrObject: principalStockDividendsBalance)
			{
				Long idParticipant = new Long(arrObject[2].toString());
				Long idHolderAccount = new Long(arrObject[3].toString());
				String idSecurityCode = arrObject[4].toString();
				BigDecimal guaranteeQuantity = new BigDecimal(arrObject[5].toString());
				if (NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(idModality) ||
					 NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(idModality))
					{
						//we calculate the guarantee quantity to move in rate to sale quantity in REPO operation
						movedGuaranteeQuantity = guaranteeQuantity.multiply(saleQuantity).divide(originalQuantity).setScale(0, BigDecimal.ROUND_HALF_UP);
					}
					else {
						//we withdrawal all the stock for REPORT operations
						movedGuaranteeQuantity = guaranteeQuantity;
					}
					if (movedGuaranteeQuantity.compareTo(BigDecimal.ZERO) > 0)
					{
						//is necessary create a guarantee operation to move the principal stock dividends guarantee
						GuaranteeOperation objGuaranteeOperation= saveGuaranteeOperation(idHolderAccountOperation, idParticipant, idHolderAccount, idSecurityCode, movedGuaranteeQuantity,
																												GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), null);
						
						//now we execute the component to remove the guarantees balances from original holder account
						AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
						Long idBusinessProcess= null;
						if (NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(idModality) ||
							 NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(idModality))
						{
							if (ComponentConstant.CASH_PART.equals(operationPart) && SettlementSchemaType.GROSS.getCode().equals(settlementSchema)) {
								idBusinessProcess= BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode();
							} else if (ComponentConstant.TERM_PART.equals(operationPart) && SettlementSchemaType.GROSS.getCode().equals(settlementSchema)) {
								idBusinessProcess= BusinessProcessType.GROSS_OPERATIONS_SETTLE_TERM_PART.getCode();
							} else if (ComponentConstant.CASH_PART.equals(operationPart) && SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
								idBusinessProcess= BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode();
							} else if (ComponentConstant.TERM_PART.equals(operationPart) && SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
								idBusinessProcess= BusinessProcessType.NET_OPERATIONS_SETTLE_TERM_PART.getCode();
							}
						} else {
							idBusinessProcess= BusinessProcessType.GUARANTEE_RETIREMENT_PROCESS.getCode();
						}
						objAccountsComponentTO.setIdBusinessProcess(idBusinessProcess);
						objAccountsComponentTO.setIdOperationType(ParameterOperationType.GUARANTEES_DIVIDENDS_PRINCIPAL_FINISHING.getCode());
						objAccountsComponentTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
						List<HolderAccountBalanceTO> lstHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
						HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
						objHolderAccountBalanceTO.setIdParticipant(idParticipant);
						objHolderAccountBalanceTO.setIdHolderAccount(idHolderAccount);
						objHolderAccountBalanceTO.setIdSecurityCode(idSecurityCode);
						objHolderAccountBalanceTO.setStockQuantity(movedGuaranteeQuantity);
						lstHolderAccountBalance.add(objHolderAccountBalanceTO);
						objAccountsComponentTO.setLstSourceAccounts(lstHolderAccountBalance);
						//execute the component
						accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
					}
			}
		}
		return movedGuaranteeQuantity;
	}
	
	
	/**
	 * Generate deposit new reporting guarantee balance.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param stockQuantity the stock quantity
	 * @param settlementSchema the settlement schema
	 * @param operationPart the operation part
	 * @param idModality the id modality
	 * @throws ServiceException the service exception
	 */
	public void generateDepositNewReportingGuaranteeBalance(Long idHolderAccountOperation, SettlementHolderAccountTO objSettlementHolderAccountTO, 
																	BigDecimal stockQuantity, Integer settlementSchema, Integer operationPart, Long idModality) throws ServiceException
	{
		Long idParticipant = objSettlementHolderAccountTO.getIdParticipant();
		Long idHolderAccount = objSettlementHolderAccountTO.getIdHolderAccount();
		String idSecurityCode = objSettlementHolderAccountTO.getIdSecurityCode();
		
		GuaranteeOperation objGuaranteeOperation= saveGuaranteeOperation(idHolderAccountOperation, idParticipant, idHolderAccount, idSecurityCode, stockQuantity,
																									GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), null);
		
		//now we execute the component to remove the guarantees balances from original holder account
		AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
		Long idBusinessProcess= null;
		if (NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(idModality) ||
			 NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(idModality))
		{
			if (ComponentConstant.CASH_PART.equals(operationPart) && SettlementSchemaType.GROSS.getCode().equals(settlementSchema)) {
				idBusinessProcess= BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode();
			} else if (ComponentConstant.TERM_PART.equals(operationPart) && SettlementSchemaType.GROSS.getCode().equals(settlementSchema)) {
				idBusinessProcess= BusinessProcessType.GROSS_OPERATIONS_SETTLE_TERM_PART.getCode();
			} else if (ComponentConstant.CASH_PART.equals(operationPart) && SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
				idBusinessProcess= BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode();
			} else if (ComponentConstant.TERM_PART.equals(operationPart) && SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
				idBusinessProcess= BusinessProcessType.NET_OPERATIONS_SETTLE_TERM_PART.getCode();
			}
		} else {
			idBusinessProcess= BusinessProcessType.GUARANTEE_RETIREMENT_PROCESS.getCode();
		}
		objAccountsComponentTO.setIdBusinessProcess(idBusinessProcess);
		objAccountsComponentTO.setIdOperationType(ParameterOperationType.GUARANTEES_DIVIDENDS_PRINCIPAL_FINISHING.getCode());
		objAccountsComponentTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
		List<HolderAccountBalanceTO> lstHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
		objHolderAccountBalanceTO.setIdParticipant(idParticipant);
		objHolderAccountBalanceTO.setIdHolderAccount(idHolderAccount);
		objHolderAccountBalanceTO.setIdSecurityCode(idSecurityCode);
		objHolderAccountBalanceTO.setStockQuantity(stockQuantity);
		lstHolderAccountBalance.add(objHolderAccountBalanceTO);
		objAccountsComponentTO.setLstTargetAccounts(lstHolderAccountBalance);
		//execute the component
		accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
	}
	


	/**
	 * Save guarantee operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param indMarketFact the ind market fact
	 * @param guaranteeClass the guarantee class
	 * @param guaranteeType the guarantee type
	 * @param currency the currency
	 * @return the guarantee operation
	 */
	public GuaranteeOperation saveGuaranteeOperation(Long idHolderAccountOperation,
			SettlementHolderAccountTO objSettlementHolderAccountTO, Integer indMarketFact, Integer guaranteeClass, Integer guaranteeType, Integer currency) {
		
		Long idParticipant = objSettlementHolderAccountTO.getIdParticipant();
		Long idHolderAccount = objSettlementHolderAccountTO.getIdHolderAccount();
		String idSecurityCode = objSettlementHolderAccountTO.getIdSecurityCode();
		BigDecimal guaranteeBalance = objSettlementHolderAccountTO.getStockQuantity();
		
		GuaranteeOperation guaranteeOperation = saveGuaranteeOperation(idHolderAccountOperation, idParticipant, 
				idHolderAccount, idSecurityCode, guaranteeBalance, guaranteeClass, guaranteeType, currency);

		if(ComponentConstant.ONE.equals(indMarketFact)){
			for(MarketFactAccountTO marketFactAccountTO : objSettlementHolderAccountTO.getMarketFactAccounts()){
				GuaranteeMarketFactOperation guaranteeMarketFactOperation = new GuaranteeMarketFactOperation();
				guaranteeMarketFactOperation.setGuaranteeOperation(guaranteeOperation);
				guaranteeMarketFactOperation.setMarketDate(marketFactAccountTO.getMarketDate());
				guaranteeMarketFactOperation.setMarketPrice(marketFactAccountTO.getMarketPrice());
				guaranteeMarketFactOperation.setMarketDate(marketFactAccountTO.getMarketDate());
				create(guaranteeMarketFactOperation);
			}
		}
		
		return guaranteeOperation;
	}
	
	/**
	 * Save guarantee operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCode the id security code
	 * @param guaranteeBalance the guarantee balance
	 * @param guaranteeClass the guarantee class
	 * @param guaranteeType the guarantee type
	 * @param currency the currency
	 * @return the guarantee operation
	 */
	public GuaranteeOperation saveGuaranteeOperation(Long idHolderAccountOperation, Long idParticipant, Long idHolderAccount, String idSecurityCode, 
																						BigDecimal guaranteeBalance, Integer guaranteeClass, Integer guaranteeType, Integer currency)
	{
		GuaranteeOperation objGuaranteeOperation = new GuaranteeOperation();
		objGuaranteeOperation.setOperationType(1l);
		objGuaranteeOperation.setGuaranteeClass(guaranteeClass);
		objGuaranteeOperation.setGuaranteeType(guaranteeType);
		objGuaranteeOperation.setGuaranteeBalance(guaranteeBalance);
		objGuaranteeOperation.setOperationDate(CommonsUtilities.currentDateTime());
		objGuaranteeOperation.setCommissionAmount(BigDecimal.ZERO);
		objGuaranteeOperation.setCurrency(currency);
		objGuaranteeOperation.setInterestAmount(BigDecimal.ZERO);
		objGuaranteeOperation.setPendingAmount(guaranteeBalance);
		objGuaranteeOperation.setTaxAmount(BigDecimal.ZERO);
		
		Participant objParticipant = new Participant();
		objParticipant.setIdParticipantPk(idParticipant);
		objGuaranteeOperation.setParticipant(objParticipant);
		
		HolderAccount objHolderAccount = new HolderAccount();
		objHolderAccount.setIdHolderAccountPk(idHolderAccount);
		objGuaranteeOperation.setHolderAccount(objHolderAccount);
		
		if (idSecurityCode != null) {
			Security objSecurity = new Security();
			objSecurity.setIdSecurityCodePk(idSecurityCode);
			objGuaranteeOperation.setSecurities(objSecurity);
		}
		
		HolderAccountOperation objHolderAccountOperation = new HolderAccountOperation(); //find(HolderAccountOperation.class, idHolderAccountOperation);
		objHolderAccountOperation.setIdHolderAccountOperationPk(idHolderAccountOperation);
		objGuaranteeOperation.setHolderAccountOperation(objHolderAccountOperation);
		
		//we create the new guarantee operation
		this.create(objGuaranteeOperation);
		
		return objGuaranteeOperation;
	}
	
	
	/**
	 * Gets the list guaratees holder account.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param guaranteeClass the guarantee class
	 * @param guaranteeType the guarantee type
	 * @return the list guaratees holder account
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListGuarateesHolderAccount(Long idHolderAccountOperation, Integer guaranteeClass, Integer guaranteeType) throws ServiceException
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT holderAccountOperation.idHolderAccountOperationPk, ");
		stringBuffer.append(" guaranteeClass, ");
		stringBuffer.append(" participant.idParticipantPk, ");
		stringBuffer.append(" holderAccount.idHolderAccountPk, ");
		stringBuffer.append(" securities.idSecurityCodePk, ");
		if (guaranteeType != null && GuaranteeType.PRINCIPAL.getCode().equals(guaranteeType)) {
			stringBuffer.append(" principalGuarantee, ");
			stringBuffer.append(GuaranteeType.PRINCIPAL.getCode().toString());
		}
		else if (guaranteeType != null && GuaranteeType.MARGIN.getCode().equals(guaranteeType)) {
			stringBuffer.append(" marginGuarantee, ");
			stringBuffer.append(GuaranteeType.MARGIN.getCode().toString());
		}
		else if (guaranteeType != null && GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode().equals(guaranteeType)) {
			stringBuffer.append(" principalDividendsGuarantee, ");
			stringBuffer.append(GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode().toString());
		}
		else if (guaranteeType != null && GuaranteeType.STOCK_DIVIDENDS_MARGIN.getCode().equals(guaranteeType)) {
			stringBuffer.append(" marginDividendsGuarantee, ");
			stringBuffer.append(GuaranteeType.STOCK_DIVIDENDS_MARGIN.getCode().toString());
		}
		else if (guaranteeType != null && GuaranteeType.FUNDS_INTEREST_MARGIN.getCode().equals(guaranteeType)) {
			stringBuffer.append(" interestMarginGuarantee, ");
			stringBuffer.append(GuaranteeType.FUNDS_INTEREST_MARGIN.getCode().toString());
		}
		if (guaranteeClass != null && GuaranteeClassType.FUNDS.getCode().equals(guaranteeClass)) {
			stringBuffer.append(" ,institutionCashAccount.idInstitutionCashAccountPk ");
		}
		stringBuffer.append(" FROM AccountGuaranteeBalance ");
		stringBuffer.append(" WHERE holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
		if (guaranteeClass != null)
			stringBuffer.append(" and guaranteeClass = :guaranteeClass ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		if (guaranteeClass != null)
			parameters.put("guaranteeClass", guaranteeClass);
		
		lstObjects= findListByQueryString(stringBuffer.toString(), parameters);
		return lstObjects; 
	}
	
	
	/**
	 * Generate withdrawal reported securities guarantees.
	 *
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param guaranteeType the guarantee type
	 * @param idSecurityCode the id security code
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	public void generateWithdrawalReportedSecuritiesGuarantees(SettlementHolderAccountTO objSettlementHolderAccountTO, 
																									Integer guaranteeType, String idSecurityCode, BigDecimal stockQuantity) throws ServiceException
	{
		Long idHolderAccountOperation = objSettlementHolderAccountTO.getIdHolderAccountOperation();
		Long idParticipant = objSettlementHolderAccountTO.getIdParticipant();
		Long idHolderAccount = objSettlementHolderAccountTO.getIdHolderAccount();
		
		GuaranteeOperation objGuaranteeOperation= saveGuaranteeOperation(idHolderAccountOperation, idParticipant, idHolderAccount, idSecurityCode, stockQuantity,
																													GuaranteeClassType.SECURITIES.getCode(), guaranteeType, null);
		
		//now we execute the component to remove the guarantees balances from original holder account
		AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
		objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.GUARANTEE_RETIREMENT_PROCESS.getCode());
		if (GuaranteeType.MARGIN.getCode().equals(guaranteeType))
			objAccountsComponentTO.setIdOperationType(ParameterOperationType.GUARANTEES_MARGIN_FINISHING.getCode());
		else if (GuaranteeType.STOCK_DIVIDENDS_MARGIN.getCode().equals(guaranteeType))
			objAccountsComponentTO.setIdOperationType(ParameterOperationType.GUARANTEES_DIVIDENDS_MARGIN_FINISHING.getCode());
		objAccountsComponentTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
		List<HolderAccountBalanceTO> lstHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
		objHolderAccountBalanceTO.setIdParticipant(idParticipant);
		objHolderAccountBalanceTO.setIdHolderAccount(idHolderAccount);
		objHolderAccountBalanceTO.setIdSecurityCode(idSecurityCode);
		objHolderAccountBalanceTO.setStockQuantity(stockQuantity);
		lstHolderAccountBalance.add(objHolderAccountBalanceTO);
		objAccountsComponentTO.setLstSourceAccounts(lstHolderAccountBalance);
		//execute the component
		accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
	}
	
	
	/**
	 * Generate deposit reported securities guarantees.
	 *
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	public void generateDepositReportedSecuritiesGuarantees(SettlementHolderAccountTO objSettlementHolderAccountTO, BigDecimal stockQuantity) throws ServiceException
	{
		Long idHolderAccountOperation = objSettlementHolderAccountTO.getIdHolderAccountOperation();
		Long idParticipant = objSettlementHolderAccountTO.getIdParticipant();
		Long idHolderAccount = objSettlementHolderAccountTO.getIdHolderAccount();
		String idSecurityCode = objSettlementHolderAccountTO.getIdSecurityCode();
		
		GuaranteeOperation objGuaranteeOperation= saveGuaranteeOperation(idHolderAccountOperation, idParticipant, idHolderAccount, idSecurityCode, stockQuantity,
																										GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), null);
		
		//now we execute the component to remove the guarantees balances from original holder account
		AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
		objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.GUARANTEE_RETIREMENT_PROCESS.getCode());
		objAccountsComponentTO.setIdOperationType(ParameterOperationType.GUARANTEES_DIVIDENDS_PRINCIPAL_FINISHING.getCode());
		objAccountsComponentTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
		List<HolderAccountBalanceTO> lstHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
		objHolderAccountBalanceTO.setIdParticipant(idParticipant);
		objHolderAccountBalanceTO.setIdHolderAccount(idHolderAccount);
		objHolderAccountBalanceTO.setIdSecurityCode(idSecurityCode);
		objHolderAccountBalanceTO.setStockQuantity(stockQuantity);
		lstHolderAccountBalance.add(objHolderAccountBalanceTO);
		objAccountsComponentTO.setLstTargetAccounts(lstHolderAccountBalance);
		//execute the component
		accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
	}

	
	/**
	 * Gets the list funds guarantees.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @return the list funds guarantees
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListFundsGuarantees(Long idHolderAccountOperation)
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT idGuaranteeOperationPk, ");
		stringBuffer.append(" holderAccountOperation.idHolderAccountOperationPk, ");
		stringBuffer.append(" participant.idParticipantPk, ");
		stringBuffer.append(" holderAccount.idHolderAccountPk, ");
		stringBuffer.append(" operationDate, ");
		stringBuffer.append(" currency, ");
		stringBuffer.append(" guaranteeAmount, ");
		stringBuffer.append(" pendingAmount ");
		stringBuffer.append(" FROM GuaranteeOperation ");
		stringBuffer.append(" WHERE holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
		stringBuffer.append(" and guaranteeClass = :idGuaranteeClass ");
		stringBuffer.append(" and fundsState = :idFundsState ");
		stringBuffer.append(" ORDER BY idGuaranteeOperationPk ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		parameters.put("idGuaranteeClass", GuaranteeClassType.FUNDS.getCode());
		parameters.put("idFundsState", GuaranteeFundsStateType.PENDING_REFUNDS.getCode());
		
		lstObjects= findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	/**
	 * Gets the institution cash account.
	 *
	 * @param idParticipant the id participant
	 * @param currency the currency
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount getInstitutionCashAccount(Long idParticipant, Integer currency) throws ServiceException
	{
		InstitutionCashAccount objInstitutionCashAccount= null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT ICA FROM InstitutionCashAccount ICA ");
			stringBuffer.append(" WHERE ICA.accountType = :idAccountType "); 
			stringBuffer.append(" and ICA.participant.idParticipantPk = :idParticipant ");
			stringBuffer.append(" and ICA.currency = :currency ");
			stringBuffer.append(" and ICA.accountState = :idAccountState ");
			//stringBuffer.append(" and ICA.indAutomaticProcess = :indAutomaticProcess ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idParticipant", idParticipant);
			query.setParameter("idAccountType", AccountCashFundsType.GUARANTEES.getCode());
			query.setParameter("currency", currency);
			query.setParameter("idAccountState", CashAccountStateType.ACTIVATE.getCode());
			//query.setParameter("indAutomaticProcess", ComponentConstant.ONE);
			
			objInstitutionCashAccount= (InstitutionCashAccount) query.getSingleResult();
		} catch (NoResultException e) {
			logger.info("idParticipant: " +idParticipant	
							+", idAccountType: " +AccountCashFundsType.GUARANTEES.getCode()
							+", currency: " +currency
							+", idAccountState: " +CashAccountStateType.ACTIVATE.getCode());
			e.printStackTrace();
		}
		return objInstitutionCashAccount;
	}
	
}
