package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
import com.pradera.core.component.business.to.LipMessageHistoryTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.bank.accounts.service.ManageBankAccountsService;
import com.pradera.funds.fundsoperations.deposit.facade.ManageDepositServiceFacade;
import com.pradera.funds.fundsoperations.deposit.service.ManageDepositServiceBean;
import com.pradera.funds.fundsoperations.deposit.to.RegisterDepositFundsTO;
import com.pradera.funds.fundsoperations.retirement.facade.RetirementFundsOperationServiceFacade;
import com.pradera.funds.fundsoperations.retirement.service.RetirementFundsOperationServiceBean;
import com.pradera.funds.fundsoperations.retirement.to.HolderFundsOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.FundsInternationalOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.LipMessageHistory;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsRetirementType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.funds.type.HolderCashMovementStateType;
import com.pradera.model.funds.type.HolderFundsOperationStateType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroupDetail;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.InternationalOperationTransferType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.settlements.core.to.FundsParticipantSettlementTO;
import com.pradera.settlements.core.to.FundsSettlementOperationTO;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.webclient.SettlementServiceConsumer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FundsComponentSingleton.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Singleton
public class FundsComponentSingleton extends CrudDaoServiceBean {
	
	/** The funds component service. */
	@Inject
    Instance<FundsComponentService> fundsComponentService;

	/** The manage deposit service bean. */
	@EJB
	ManageDepositServiceBean manageDepositServiceBean;
	
	/** The manage deposit service facade. */
	@EJB
	ManageDepositServiceFacade manageDepositServiceFacade;
	
	@EJB
	ManageBankAccountsService manageBankAccountsService;
	
	/** The collection process service bean. */
	@Inject
	CollectionProcessService collectionProcessServiceBean;
	
	/** The settlement process service bean. */
	@EJB
	SettlementProcessService settlementProcessServiceBean;
	
	/** The guarantees settlement. */
	@EJB
	private GuaranteesSettlement guaranteesSettlement;
	
	/** The reception service. */
	@EJB
	AutomaticReceptionFundsService receptionService;
	
	/** The deposit service bean. */
	@Inject
	private ManageDepositServiceBean depositServiceBean;
	
	/** The funds operation service facade. */
	@EJB
	private RetirementFundsOperationServiceFacade fundsOperationServiceFacade;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** The settlement service consumer. */
	@EJB
	private SettlementServiceConsumer settlementServiceConsumer;
	
	/** The funds operation service bean. */
	@Inject
	private RetirementFundsOperationServiceBean fundsOperationServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
			
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Collect mechanism operations.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param settlementDate the settlement date
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void collectMechanismOperations(Long idSettlementProcess, Long idMechanism, Long idModalityGroup, Date settlementDate, Integer currency, LoggerUser loggerUser) throws ServiceException
	{
		//we get the list of participant operation for all the operations are completed the deposits of funds
		
		List<Object[]> lstFundsParticipantSettlement = collectionProcessServiceBean.getListMechanismOperationWithFunds(idMechanism, idModalityGroup, currency, settlementDate);
		
		List<FundsSettlementOperationTO> settlementOperationsTO = collectionProcessServiceBean.populateFundsParticipantSettlementOperations(lstFundsParticipantSettlement);
		
		if (Validations.validateListIsNotNullAndNotEmpty(settlementOperationsTO)){
			
			for(FundsSettlementOperationTO fundsSettlementOperationTO : settlementOperationsTO){
				
				Long idMechanismOperation = fundsSettlementOperationTO.getIdMechanismOperation();
				Long idSettlementOperation = fundsSettlementOperationTO.getIdSettlementOperation();
				
				for (FundsParticipantSettlementTO participantSettlementTO: fundsSettlementOperationTO.getParticipantSettlements()){
					Long idParticipant= participantSettlementTO.getIdParticipant();
					BigDecimal totalAmount= participantSettlementTO.getSettlementAmount();

					//we collect the funds to the participant from the current operation
					collectionProcessServiceBean.collectGrossMechanismOperation(idMechanismOperation, idMechanism, idModalityGroup, idParticipant, totalAmount, currency, loggerUser);
					
				}
				
				collectionProcessServiceBean.updateFundsSettlementAccountOperation(idSettlementOperation, 
						AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode(), ComponentConstant.PURCHARSE_ROLE, loggerUser);
				
				collectionProcessServiceBean.updateFundsSettlementOperation(idSettlementOperation, 
						AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode(), loggerUser);
				
				//if we collected all funds of buyer participants, then we have to deliver the funds to the seller participants
				collectionProcessServiceBean.deliverFundsMechanismOperation(idMechanism, idModalityGroup, idSettlementOperation, idMechanismOperation, currency, loggerUser);
				
			}

		}
	}
	
	/**
	 * Collect net funds participant operation.
	 *
	 * @param settlementProcess the settlement process
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void collectNetFundsParticipantOperation(SettlementProcess settlementProcess, Long idMechanism, Long idModalityGroup, Integer idCurrency, LoggerUser loggerUser) throws ServiceException {
		
		Long idSettlementProcess = settlementProcess.getIdSettlementProcessPk();

		List<ParticipantPosition> lstParticipantPosition= settlementProcessServiceBean.getListParticipantPosition(
				idSettlementProcess, null, SettlementConstant.NEGATIVE_POSITION, BooleanType.NO.getCode(), null);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantPosition)){
			for(ParticipantPosition participantPosition : lstParticipantPosition){
				collectionProcessServiceBean.collectNetParticipantPosition(participantPosition, settlementProcess, idMechanism, idModalityGroup, idCurrency, loggerUser);
			}
		}
		collectionProcessServiceBean.updateFundsSettlementAccountOperation(idSettlementProcess, 
				OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode(), ComponentConstant.PURCHARSE_ROLE,
				AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode(), loggerUser);
		
		collectionProcessServiceBean.updateFundsParticipantSettlement(idSettlementProcess, 
				OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode(), ComponentConstant.PURCHARSE_ROLE,
				AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode(), loggerUser);
		
		collectionProcessServiceBean.updateFundsSettlementOperation(idSettlementProcess, 
				OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode(),
				AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode(), loggerUser);
	}
	
	/**
	 * Deliver net funds to participant.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void deliverNetFundsToParticipant(Long idSettlementProcess, Long idMechanism, Long idModalityGroup, Integer currency,LoggerUser loggerUser) throws ServiceException
	{
		try {
			//we settle the funds of the creditor participant positions
			//we get the list of participant in positive position that EDV hasn't sent the funds 
			List<ParticipantPosition> lstParticipantPosition= settlementProcessServiceBean.getListParticipantPosition(idSettlementProcess, null, 
																								SettlementConstant.POSITIVE_POSITION, BooleanType.NO.getCode(), null);
			
			if (Validations.validateListIsNotNullAndNotEmpty(lstParticipantPosition))
			{
				for (ParticipantPosition objParticipantPosition: lstParticipantPosition)
				{
					/*
					* STEP 01: To deliver the funds to creditor participant in their own cash accounts
					*/
					//ingreso de fondos al acreedor
					collectionProcessServiceBean.deliverFundsCreditorParticipant(idSettlementProcess, objParticipantPosition.getParticipant().getIdParticipantPk(), idMechanism, 
																		idModalityGroup, currency, objParticipantPosition.getNetPosition(),loggerUser);
				}
			}
			/*
			 * STEP 02: To update the cash reference indicator from holder account operation and mechanism operation
			 */
			
			collectionProcessServiceBean.updateFundsSettlementAccountOperation(idSettlementProcess, 
					OperationSettlementSateType.SETTLED.getCode(), ComponentConstant.SALE_ROLE,
					AccountOperationReferenceType.SENT_FUNDS.getCode(), loggerUser);
			
			collectionProcessServiceBean.updateFundsParticipantSettlement(idSettlementProcess, 
					OperationSettlementSateType.SETTLED.getCode(), ComponentConstant.SALE_ROLE, 
					AccountOperationReferenceType.SENT_FUNDS.getCode(), loggerUser);
			
			collectionProcessServiceBean.updateFundsSettlementOperation(idSettlementProcess, 
					OperationSettlementSateType.SETTLED.getCode(), AccountOperationReferenceType.SENT_FUNDS.getCode(), loggerUser);
			
		} catch (Exception e) {
			throw e;
		} 
	}


	/**
	 * Send funds participant positions tx.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idParticipant the id participant
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendFundsParticipantPositionsTx(Long idSettlementProcess, Long idParticipant, Long idMechanism, Long idModalityGroup, Integer currency,
												LoggerUser loggerUser) throws ServiceException {
		//we get the list of participant in positive position that EDV hasn't sent the funds 
		log.info(":::::::: Start method sendFundsParticipantPositionsTx ::::::::");
		log.info(":::::::: getListParticipantPosition ::::::::");
		List<ParticipantPosition> lstParticipantPosition= settlementProcessServiceBean.getListParticipantPosition(idSettlementProcess, idParticipant, 
																							SettlementConstant.POSITIVE_POSITION, BooleanType.NO.getCode(), null);
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstParticipantPosition))
		{
			log.info(":::::::: Size List lstParticipantPosition ::::::::" + lstParticipantPosition.size());
			for (ParticipantPosition objParticipantPosition: lstParticipantPosition)
			{
				// salida desde el acreedor a su cuenta del BC
				// Temp: la cuenta se queda en la cuenta efectivo 	
				// Cada registro sera una trasaccion
				log.info(":::::::: Information sendFundsParticipantPositionsTx ::::::::");
				log.info(":::::::: idMechanism ::::::::" + idMechanism);
				log.info(":::::::: idModalityGroup ::::::::" + idModalityGroup);
				log.info(":::::::: currency ::::::::" + currency);
				try {
					collectionProcessServiceBean.sendFundsParticipantPositionsTx(objParticipantPosition, idMechanism,  idModalityGroup, currency, loggerUser);
					
					// Send Notification 
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.SETTLEMENT_FUNDS_SENDING_PROCESS.getCode());
					Object[] parameters = new Object[] {objParticipantPosition.getParticipant().getMnemonic(), objParticipantPosition.getNetPosition()};
					notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, null, parameters);
					
				} catch (Exception ex) {
					log.info(":::::::: Exception sendFundsParticipantPositionsTx ::::::::");
				}				
			}
		}
	}
	
	/**
	 * Send funds participant positions.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idParticipant the id participant
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendFundsParticipantPositions(Long idSettlementProcess, Long idParticipant, Long idMechanism, Long idModalityGroup, Integer currency,
			LoggerUser loggerUser) throws ServiceException {
		//we get the list of participant in positive position that EDV hasn't sent the funds 
		log.info(":::::::: Start method sendFundsParticipantPositions ::::::::");		
		List<ParticipantPosition> lstParticipantPosition= settlementProcessServiceBean.getListParticipantPosition(idSettlementProcess, idParticipant, 
																SettlementConstant.POSITIVE_POSITION, BooleanType.NO.getCode(), null);		
		if (Validations.validateListIsNotNullAndNotEmpty(lstParticipantPosition)) {
			log.info(":::::::: Size List lstParticipantPosition ::::::::" + lstParticipantPosition.size());
			for (ParticipantPosition objParticipantPosition: lstParticipantPosition){				
				log.info(":::::::: Information sendFundsParticipantPositionsTx ::::::::");
				log.info(":::::::: idMechanism ::::::::" + idMechanism);
				log.info(":::::::: idModalityGroup ::::::::" + idModalityGroup);
				log.info(":::::::: currency ::::::::" + currency);
				collectionProcessServiceBean.sendFundsCreditorParticipant(objParticipantPosition, idMechanism,  idModalityGroup, currency, loggerUser);				
			}
		}
	}
		
	/**
	 * Send funds participant operations.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendFundsParticipantOperations(SettlementOperationTO objSettlementOperationTO, LoggerUser loggerUser) throws ServiceException
	{
		List<Object[]> lstParticipantOperation= collectionProcessServiceBean.getListSellerPaticipantSettlement(objSettlementOperationTO.getIdSettlementOperation());
				
		if (Validations.validateListIsNotNullAndNotEmpty(lstParticipantOperation))
		{
			for (Object[] objParticipantOperation: lstParticipantOperation){
				Long idParticipant = (Long)objParticipantOperation[0];
				BigDecimal settlementAmount = (BigDecimal)objParticipantOperation[1];
				Integer currency = (Integer)objParticipantOperation[2];
				Long idParticipantSettlement = (Long)objParticipantOperation[3];
				collectionProcessServiceBean.sendFundsParticipantSettlement(idParticipant, settlementAmount, currency, objSettlementOperationTO, loggerUser);
				
				//now we update the funds reference to this participant operation to Sent Funds (FE)
				collectionProcessServiceBean.updateFundsParticipantSettlement(idParticipantSettlement, 
																		AccountOperationReferenceType.SENT_FUNDS.getCode(), loggerUser);
			}
			
			for(SettlementHolderAccountTO settlementHolderAccountTO: objSettlementOperationTO.getLstBuyerHolderAccounts()){

				collectionProcessServiceBean.updateFundsSettlementAccountOperation(settlementHolderAccountTO.getIdSettlementAccountOperation(), 
																		AccountOperationReferenceType.SENT_FUNDS.getCode(), loggerUser);
			}
			
			//now we update the funds reference to this operation to Sent Funds (FE)
			collectionProcessServiceBean.updateFundsSettlementOperation(objSettlementOperationTO.getIdSettlementOperation(), 
																		AccountOperationReferenceType.SENT_FUNDS.getCode(), loggerUser);
		}
	}
	
	/**
	 * Deliver interest funds guarantees.
	 *
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @throws ServiceException the service exception
	 */
	public void deliverInterestFundsGuarantees(SettlementHolderAccountTO objSettlementHolderAccountTO, 
													SettlementOperationTO objSettlementOperationTO) throws ServiceException
	{
		//we get the list of accumulated interest margin funds in the account guarantee
		List<Object[]> lstInterestMarginGuarantees = guaranteesSettlement.getListGuarateesHolderAccount(objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
																				GuaranteeClassType.FUNDS.getCode(), GuaranteeType.FUNDS_INTEREST_MARGIN.getCode());
		if (Validations.validateListIsNotNullAndNotEmpty(lstInterestMarginGuarantees))
		{
			for (Object [] arrObject: lstInterestMarginGuarantees)
			{
				Long idInstitutionCashAccount= new Long(arrObject[7].toString());
				BigDecimal interestAmount= new BigDecimal(arrObject[5].toString());
				
				//if interest Amount is geather than 0
				if(interestAmount.compareTo(BigDecimal.ZERO) > 0){
					//we create the guarantee operation to withdrawal the interest margin funds
					GuaranteeOperation objGuaranteeOperation= guaranteesSettlement.saveGuaranteeOperation(objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
																objSettlementHolderAccountTO.getIdParticipant(), objSettlementHolderAccountTO.getIdHolderAccount(), 
																null, interestAmount, GuaranteeClassType.FUNDS.getCode(), GuaranteeType.FUNDS_INTEREST_MARGIN.getCode(), 
																objSettlementOperationTO.getCurrency());
					
					//we create the funds operation from participant guarantee cash account 
					FundsOperation objFundsOperation= collectionProcessServiceBean.generateFundsOperation(objGuaranteeOperation, 
																	ParameterFundsOperationType.INTEREST_MARGIN_GAURANTEE_END.getCode(),
																	FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), idInstitutionCashAccount);
					
					//we execute the funds component to move the balances
					FundsComponentTO objFundsComponentTO= new FundsComponentTO();
					objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.GUARANTEE_RETIREMENT_PROCESS.getCode());
					objFundsComponentTO.setIdFundsOperationType(ParameterFundsOperationType.INTEREST_MARGIN_GAURANTEE_END.getCode());
					objFundsComponentTO.setCashAmount(objFundsOperation.getOperationAmount());
					objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
					objFundsComponentTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
					objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
				}
			}
		}
	}
	
	/**
	 * Unblock funds guarantees.
	 *
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @throws ServiceException the service exception
	 */
	public void unblockFundsGuarantees(SettlementHolderAccountTO objSettlementHolderAccountTO, SettlementOperationTO objSettlementOperationTO) throws ServiceException
	{
		//we get the list of accumulated margin funds in the account guarantee
		List<Object[]> lstFundsGuarantees = guaranteesSettlement.getListGuarateesHolderAccount(objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
																									GuaranteeClassType.FUNDS.getCode(), GuaranteeType.MARGIN.getCode());
		if (Validations.validateListIsNotNullAndNotEmpty(lstFundsGuarantees))
		{
			for (Object[] arrObject: lstFundsGuarantees)
			{
				Long idInstitutionCashAccount= new Long(arrObject[7].toString());
				BigDecimal guaranteeAmount= new BigDecimal(arrObject[5].toString());
				if (guaranteeAmount.compareTo(BigDecimal.ZERO) >0)
				{
					//we create the guarantee operation to withdrawal the margin funds
					GuaranteeOperation objGuaranteeOperation= guaranteesSettlement.saveGuaranteeOperation(objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
																								objSettlementHolderAccountTO.getIdParticipant(), objSettlementHolderAccountTO.getIdHolderAccount(), 
																								null, guaranteeAmount, GuaranteeClassType.FUNDS.getCode(), GuaranteeType.MARGIN.getCode(), 
																								objSettlementOperationTO.getCurrency());
					
					//we create the funds operation from participant guarantee cash account 
					FundsOperation objFundsOperation= collectionProcessServiceBean.generateFundsOperation(objGuaranteeOperation, ParameterFundsOperationType.GAURANTEE_END.getCode(),
																										FundsType.IND_FUND_OPERATION_CLASS_TRANSFER.getCode(), idInstitutionCashAccount);
					
					//we execute the funds component to move the balances
					FundsComponentTO objFundsComponentTO= new FundsComponentTO();
					objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.GUARANTEE_RETIREMENT_PROCESS.getCode());
					objFundsComponentTO.setIdFundsOperationType(ParameterFundsOperationType.GAURANTEE_END.getCode());
					objFundsComponentTO.setCashAmount(objFundsOperation.getOperationAmount());
					objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
					objFundsComponentTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
					objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
				}
			}
		}
	}
	
	/**
	 * Confirm deposit multiple.
	 *
	 * @param lstFundOperation the lst fund operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void confirmDepositMultiple(List<FundsOperation> lstFundOperation, LoggerUser loggerUser) throws ServiceException{
		if(lstFundOperation != null && !lstFundOperation.isEmpty()){
			for(FundsOperation objFundsOperation : lstFundOperation){
				confirmDeposit(objFundsOperation, objFundsOperation.getBankReference(), loggerUser);
			}
		}
	}
	
	/**
	 * Confirm deposit.
	 *
	 * @param fundOperation the fund operation
	 * @param bankReference the bank reference
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void confirmDeposit(FundsOperation fundOperation, String bankReference, LoggerUser loggerUser) throws ServiceException{
		
		SettlementOperation objSettlementOperation=null;
		List<ParticipantSettlement> lstParticipantSettlement=null;
		FundsOperation fundOpeTemp = manageDepositServiceBean.find(fundOperation.getIdFundsOperationPk(), FundsOperation.class);
		if(!MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode().equals(fundOpeTemp.getOperationState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = fundOpeTemp.getIdFundsOperationPk().toString();
			throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_REQUEST_STATE_MODIFY, bodyData);
		}
		boolean isRelated= manageDepositServiceFacade.validateDayIsOpen(fundOpeTemp);
		if(isRelated){
			if(!isDaysIsOpen()){
				throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
			}
		}
		
		if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(fundOpeTemp.getFundsOperationGroup())) {
			if(Validations.validateIsNotNull(fundOpeTemp.getMechanismOperation())){
				SettlementOperation tempOp = (SettlementOperation) manageDepositServiceFacade.getSettlementOperationFunds(
						fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), fundOpeTemp.getOperationPart());
				if(Validations.validateIsNull(tempOp.getStockReference())) {
					throw new ServiceException(ErrorServiceType.FUNDS_OPERATION_STOCK_REFERENCE_NULL);
				}
			}
		}
		
		fundOpeTemp.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(bankReference)){
			fundOpeTemp.setBankReference(bankReference);
		}
			
		manageDepositServiceBean.update(fundOpeTemp);
		if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(fundOpeTemp.getFundsOperationGroup())){
			if(Validations.validateIsNotNull(fundOpeTemp.getMechanismOperation())){
				if(Validations.validateIsNotNullAndNotEmpty(fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk())){
					objSettlementOperation = (SettlementOperation) manageDepositServiceFacade.getSettlementOperationFunds(
							fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), fundOpeTemp.getOperationPart());
					lstParticipantSettlement=
							(List<ParticipantSettlement>)manageDepositServiceFacade.getParticipantSettlements(objSettlementOperation.getIdSettlementOperationPk(), 
																					ParticipantRoleType.BUY.getCode(), null);
					for(ParticipantSettlement partiSettlement:lstParticipantSettlement){
						if(partiSettlement.getParticipant().getIdParticipantPk().equals(fundOpeTemp.getParticipant().getIdParticipantPk())
								&& ParticipantRoleType.BUY.getCode().equals(partiSettlement.getRole())
								&& fundOpeTemp.getOperationPart().equals(fundOpeTemp.getOperationPart())){
							
							//Se valida si el participante ya tiene FC o FD
							if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(partiSettlement.getFundsReference())
									|| AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode().equals(partiSettlement.getFundsReference())){
								Object[] bodyData = new Object[1];
								bodyData[0] = fundOperation.getMechanismOperation().getOperationNumber().toString();
								throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_OPERATION_PAYED, bodyData);
							}
							if(Validations.validateIsNullOrEmpty(partiSettlement.getDepositedAmount()))	
								partiSettlement.setDepositedAmount(fundOpeTemp.getOperationAmount());
							else{
								BigDecimal depositAmount = partiSettlement.getDepositedAmount();
								partiSettlement.setDepositedAmount(depositAmount.add(fundOpeTemp.getOperationAmount()));	
							}
							//MONTO DEPOSITADO MAYOR O IGUAL
							if(partiSettlement.getDepositedAmount().compareTo(partiSettlement.getSettlementAmount()) == 0 
									|| partiSettlement.getDepositedAmount().compareTo(partiSettlement.getSettlementAmount()) == 1){
								partiSettlement.setFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
								fundOpeTemp.getMechanismOperation().getHolderAccountOperations().size();
								for(HolderAccountOperation holAccOperation:fundOpeTemp.getMechanismOperation().getHolderAccountOperations()){
									if(partiSettlement.getParticipant().getIdParticipantPk().equals(holAccOperation.getInchargeFundsParticipant().getIdParticipantPk())
											&& ParticipantRoleType.BUY.getCode().equals(holAccOperation.getRole())
											&& holAccOperation.getOperationPart().equals(fundOpeTemp.getOperationPart())
											&& HolderAccountOperationStateType.CONFIRMED.getCode().equals(holAccOperation.getHolderAccountState())){
										holAccOperation.setFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
									}
								}
							}
							break;
						}
					}
					fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOpeTemp,	BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode()));
					/**
					 * SI TODOS LOS ParticipantOperation DEL MechanismOperation
					 * TIENEN FD EL MechanismOperation CAMBIA A FD
					 */					
					Integer totalDeposited = null;
					Integer totalCashParticipant = null;
					Long idMechanism = null;
					Long idModalityGroup = null;
					if(OperationPartType.CASH_PART.getCode().equals(fundOpeTemp.getOperationPart())){
						totalDeposited = manageDepositServiceBean.getTotalDeposited(fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), OperationPartType.CASH_PART.getCode());
						totalCashParticipant = manageDepositServiceBean.getCashParticipant(fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), OperationPartType.CASH_PART.getCode());
						if(Validations.validateIsNotNullAndNotEmpty(totalDeposited)
								&& Validations.validateIsNotNullAndNotEmpty(totalCashParticipant)){
							if(totalDeposited.equals(totalCashParticipant) && !fundOpeTemp.getMechanismOperation().getOperationState().equals(MechanismOperationStateType.CANCELED_STATE.getCode())){
								objSettlementOperation = (SettlementOperation) manageDepositServiceFacade.getSettlementOperationFunds(
										fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), fundOpeTemp.getOperationPart());
								//objSettlementOperation.setCashFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
								if(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(objSettlementOperation.getStockReference())){
									/**
									* COBRO AUTOMATICO
									*/	
									lstParticipantSettlement=
											(List<ParticipantSettlement>)manageDepositServiceFacade.getParticipantSettlements(objSettlementOperation.getIdSettlementOperationPk(), 
																									ParticipantRoleType.BUY.getCode(), null);
									for(ParticipantSettlement partiSettlement:lstParticipantSettlement){
										if(ParticipantRoleType.BUY.getCode().equals(partiSettlement.getRole())
											&& partiSettlement.getSettlementOperation().getOperationPart().equals(fundOpeTemp.getOperationPart())){
											idMechanism = partiSettlement.getSettlementOperation().getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk();
											for(ModalityGroupDetail objModalityGroupDetail: partiSettlement.getSettlementOperation().getMechanismOperation().getMechanisnModality().getModalityGroupDetailList()){
												if(objModalityGroupDetail.getModalityGroup().getSettlementSchema().intValue()==fundOpeTemp.getInstitutionCashAccount().getSettlementSchema().intValue()){
													idModalityGroup=objModalityGroupDetail.getModalityGroup().getIdModalityGroupPk();
													break;
												}
											}
											collectionProcessServiceBean.collectGrossMechanismOperation(partiSettlement.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk(), 
																								idMechanism, 
																								idModalityGroup, 
																								partiSettlement.getParticipant().getIdParticipantPk(), 
																								partiSettlement.getSettlementAmount(), 
																								fundOpeTemp.getCurrency(),
																								//OperationPartType.TERM_PART.getCode(),
																								loggerUser);
											/*Despues de realizar el cobro se marca los indicadores a FC*/
											partiSettlement.setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
											partiSettlement.getSettlementOperation().setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
											depositServiceBean.update(partiSettlement.getSettlementOperation());
											depositServiceBean.update(partiSettlement);
											for(HolderAccountOperation holAccOperation:fundOpeTemp.getMechanismOperation().getHolderAccountOperations()){
												if(partiSettlement.getParticipant().getIdParticipantPk().equals(holAccOperation.getInchargeFundsParticipant().getIdParticipantPk())
														&& ParticipantRoleType.BUY.getCode().equals(holAccOperation.getRole())
														&& holAccOperation.getOperationPart().equals(fundOpeTemp.getOperationPart())
														&& HolderAccountOperationStateType.CONFIRMED.getCode().equals(holAccOperation.getHolderAccountState())){
													holAccOperation.setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
												}
											}
										}
									}
									collectionProcessServiceBean.deliverFundsMechanismOperation(idMechanism, idModalityGroup, objSettlementOperation.getIdSettlementOperationPk(),
																											fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk()
																											//, OperationPartType.CASH_PART.getCode()
																											, fundOpeTemp.getCurrency(),loggerUser
																											);
									//fundOpeTemp.getMechanismOperation().setCashFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
								}
							}
						}
					}else if(OperationPartType.TERM_PART.getCode().equals(fundOpeTemp.getOperationPart())){
						totalDeposited = manageDepositServiceBean.getTotalDeposited(fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), OperationPartType.TERM_PART.getCode());
						totalCashParticipant = manageDepositServiceBean.getCashParticipant(fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), OperationPartType.TERM_PART.getCode());
						if(Validations.validateIsNotNullAndNotEmpty(totalDeposited)
								&& Validations.validateIsNotNullAndNotEmpty(totalCashParticipant)){
							if(totalDeposited.equals(totalCashParticipant) && !fundOpeTemp.getMechanismOperation().getOperationState().equals(MechanismOperationStateType.CANCELED_TERM_STATE.getCode())){
								//fundOpeTemp.getMechanismOperation().setTermFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
								objSettlementOperation = (SettlementOperation) manageDepositServiceFacade.getSettlementOperationFunds(
										fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(), fundOpeTemp.getOperationPart());
								if(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(objSettlementOperation.getStockReference())){
									/**
									 * COBRO AUTOMATICO
									 */
									
									lstParticipantSettlement=
											(List<ParticipantSettlement>)manageDepositServiceFacade.getParticipantSettlements(objSettlementOperation.getIdSettlementOperationPk(), 
																									ParticipantRoleType.BUY.getCode(), null);
									for(ParticipantSettlement partiSettlement:lstParticipantSettlement){
										if(ParticipantRoleType.BUY.getCode().equals(partiSettlement.getRole())
											&& partiSettlement.getSettlementOperation().getOperationPart().equals(fundOpeTemp.getOperationPart())){
											idMechanism = partiSettlement.getSettlementOperation().getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk();
											for(ModalityGroupDetail objModalityGroupDetail: partiSettlement.getSettlementOperation().getMechanismOperation().getMechanisnModality().getModalityGroupDetailList()){
												if(objModalityGroupDetail.getModalityGroup().getSettlementSchema().intValue()==fundOpeTemp.getInstitutionCashAccount().getSettlementSchema().intValue()){
													idModalityGroup=objModalityGroupDetail.getModalityGroup().getIdModalityGroupPk();
													break;
												}
											}
											collectionProcessServiceBean.collectGrossMechanismOperation(partiSettlement.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk(), 
																								idMechanism, idModalityGroup, 
																								partiSettlement.getParticipant().getIdParticipantPk(), 
																								partiSettlement.getSettlementAmount(), 
																								fundOpeTemp.getCurrency(),
																								loggerUser);
											/*Despues de realizar el cobro se marca los indicadores a FC*/
											partiSettlement.setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
											partiSettlement.getSettlementOperation().setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
											depositServiceBean.update(partiSettlement.getSettlementOperation());
											depositServiceBean.update(partiSettlement);
											for(HolderAccountOperation holAccOperation:fundOpeTemp.getMechanismOperation().getHolderAccountOperations()){
												if(partiSettlement.getParticipant().getIdParticipantPk().equals(holAccOperation.getInchargeFundsParticipant().getIdParticipantPk())
														&& ParticipantRoleType.BUY.getCode().equals(holAccOperation.getRole())
														&& holAccOperation.getOperationPart().equals(fundOpeTemp.getOperationPart())
														&& HolderAccountOperationStateType.CONFIRMED.getCode().equals(holAccOperation.getHolderAccountState())){
													holAccOperation.setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
												}
											}
										}
									}
									
									collectionProcessServiceBean.deliverFundsMechanismOperation(idMechanism, idModalityGroup, objSettlementOperation.getIdSettlementOperationPk(),
																										fundOpeTemp.getMechanismOperation().getIdMechanismOperationPk(),
																										//, OperationPartType.TERM_PART.getCode(), 
																										fundOpeTemp.getCurrency(),loggerUser
																										);
									//fundOpeTemp.getMechanismOperation().setTermFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
								}
							}
						}
					}					
				}
			}else{
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOpeTemp,	BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode()));
			}
			
			
		}else if(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(fundOpeTemp.getFundsOperationGroup())){
			fundOpeTemp.getLstFundsInternationalOperation().size();
			for(FundsInternationalOperation fundsIntOpe:fundOpeTemp.getLstFundsInternationalOperation()){
				fundsIntOpe.getInternationalOperation().getIdInternationalOperationPk();
				InternationalOperation interOpe = fundsIntOpe.getInternationalOperation();
				//Se valida si ya tiene FC o FD
				if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(interOpe.getFundsReference())
						|| AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode().equals(interOpe.getFundsReference())){
					Object[] bodyData = new Object[1];
					bodyData[0] = interOpe.getOperationNumber().toString();
					throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_OPERATION_PAYED, bodyData);
				}
				if(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode().equals(interOpe.getTransferType())){
					//Para envio
					interOpe.setDepositAmount(interOpe.getSettlementAmount());				
				}else if(InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode().equals(interOpe.getTransferType())){
					//Para recepcion
					if(Validations.validateIsNullOrEmpty(interOpe.getDepositAmount()))
						interOpe.setDepositAmount(fundOpeTemp.getOperationAmount());
					else{
						BigDecimal depositAmount = interOpe.getDepositAmount();
						interOpe.setDepositAmount(depositAmount.add(fundOpeTemp.getOperationAmount()));
					}
				}
				if(interOpe.getDepositAmount().compareTo(interOpe.getSettlementAmount()) == 0 
						|| interOpe.getDepositAmount().compareTo(interOpe.getSettlementAmount()) == 1)//MONTO DEPOSITADO MAYOR O IGUAL
					interOpe.setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
				
			}
			fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOpeTemp,	BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode()));
		}else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(fundOpeTemp.getFundsOperationGroup())){
			//******************* Process for RETURN GROUP OPERATIONS
			fundOpeTemp.getHolderFundsOperations().size();
			fundOpeTemp.getBank().getIdBankPk();
			List<HolderFundsOperation> lstHolderFundsOperation = fundOpeTemp.getHolderFundsOperations();
			HolderCashMovement holderCashMovement=null;					 
				for(HolderFundsOperation tempHolderFundsOper: lstHolderFundsOperation){
					tempHolderFundsOper.getHolder().getIdHolderPk();
					tempHolderFundsOper.getHolderAccountBank().getIdHolderAccountBankPk();
					tempHolderFundsOper.getHolderAccountBank().getBank().getIdBankPk();
					tempHolderFundsOper.getFundsOperationType().getIdFundsOperationTypePk();						
					holderCashMovement = new HolderCashMovement();
 					holderCashMovement.setBankAccountNumber(tempHolderFundsOper.getHolderAccountBank().getBankAccountNumber());
					holderCashMovement.setBankAccountType(tempHolderFundsOper.getBankAccountType());
					holderCashMovement.setCurrency(fundOpeTemp.getCurrency());
					holderCashMovement.setDocumentHolder(tempHolderFundsOper.getHolderDocumentNumber());
					holderCashMovement.setFullName(tempHolderFundsOper.getFullNameHolder());
					holderCashMovement.setHolder(tempHolderFundsOper.getHolder());
	 				holderCashMovement.setBank(tempHolderFundsOper.getHolderAccountBank().getBank());
					holderCashMovement.setFundsOperation(fundOpeTemp);
					holderCashMovement.setHolderAccountBank(tempHolderFundsOper.getHolderAccountBank());
					holderCashMovement.setHolderAmount(tempHolderFundsOper.getHolderOperationAmount());
					holderCashMovement.setMovementDate(CommonsUtilities.currentDateTime());
					FundsOperationType tempFoType = new FundsOperationType(fundOpeTemp.getFundsOperationType());
					holderCashMovement.setFundsOperationType(tempFoType);
					holderCashMovement.setMovementState(HolderCashMovementStateType.RESERVED.getCode());					
					manageDepositServiceBean.create(holderCashMovement);
				}				
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOpeTemp,	BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode())); 			
		} else
			fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOpeTemp,	BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode()));
		
		
		if(isRelated){
			if(BooleanType.NO.getCode().equals(fundOpeTemp.getIndCentralized()))
				confirmMirrorOperationForCentralAccount(fundOpeTemp);
		}
	}
		
	
	
	
	/**
	 * Confirm deposit.
	 *
	 * @param fundOperation the fund operation
	 * @param bankReference the bank reference
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void confirmTransferCentral(FundsOperation fundOperation,LoggerUser loggerUser) throws ServiceException{
		
		FundsOperation fundOpeTemp = manageDepositServiceBean.find(fundOperation.getIdFundsOperationPk(), FundsOperation.class);
		if(!MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode().equals(fundOpeTemp.getOperationState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = fundOpeTemp.getIdFundsOperationPk().toString();
			throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_REQUEST_STATE_MODIFY, bodyData);
		}
		boolean isRelated= manageDepositServiceFacade.validateDayIsOpen(fundOpeTemp);
		if(isRelated){
			if(!isDaysIsOpen()){
				throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
			}
		}
		fundOpeTemp.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_TRANSFERRED_TO_CENTRALIZER.getCode());
		fundOpeTemp.setIndTransferCentralizing(BooleanType.YES.getCode());
		manageDepositServiceBean.update(fundOpeTemp);
		if(BooleanType.NO.getCode().equals(fundOpeTemp.getIndCentralized()))
			createServiceWithdrawal(fundOpeTemp, loggerUser.getUserName());
	}
		
	private FundsOperation createServiceWithdrawal(FundsOperation fundOperation, String userName) throws ServiceException{
		FundsOperation centralFundsOperation = new FundsOperation();
		centralFundsOperation.setInstitutionCashAccount(receptionService.getActivatedCentralizingAccount(fundOperation.getCurrency()));
		centralFundsOperation.setCurrency(fundOperation.getCurrency());
		centralFundsOperation.setOperationAmount(fundOperation.getOperationAmount());
		centralFundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralFundsOperation.setRegistryUser(userName);
		centralFundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		centralFundsOperation.setIndCentralized(BooleanType.YES.getCode());
		centralFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralFundsOperation.setFundsOperationClass(fundOperation.getFundsOperationClass());
		centralFundsOperation.setFundsOperationGroup(fundOperation.getFundsOperationGroup());
		centralFundsOperation.setFundsOperationType(fundOperation.getFundsOperationType());
		manageDepositServiceBean.create(centralFundsOperation);
		executeFundComponent(centralFundsOperation, BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_DEPOSIT_PROCESS.getCode());
		
		return centralFundsOperation;
	}
	
	/**
	 * Metodo que ejecuta componente de fondos
	 *
	 * @param centralizerFundOperation the centralizer fund operation
	 * @param idBusinessProcess the id business process
	 * @throws ServiceException the service exception
	 */
	private void executeFundComponent(FundsOperation centralizerFundOperation, Long idBusinessProcess) throws ServiceException{
		FundsComponentTO objComponent = new FundsComponentTO();
		objComponent.setIdBusinessProcess(idBusinessProcess);
		objComponent.setIdFundsOperationType(centralizerFundOperation.getFundsOperationType());
		objComponent.setIdFundsOperation(centralizerFundOperation.getIdFundsOperationPk());
		objComponent.setIdInstitutionCashAccount(centralizerFundOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		objComponent.setCashAmount(centralizerFundOperation.getOperationAmount());
		fundsComponentService.get().executeFundsComponent(objComponent);
	}
	
	/**
	 * Confirm mirror operation for central account.
	 *
	 * @param fundOpeOrigin the fund ope origin
	 * @throws ServiceException the service exception
	 */
	private void confirmMirrorOperationForCentralAccount(FundsOperation fundOpeOrigin) throws ServiceException{
		FundsOperation fundOperationMirror = manageDepositServiceBean.getFundOperationMirror(fundOpeOrigin.getFundsOperation().getIdFundsOperationPk());
		fundOperationMirror.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		manageDepositServiceBean.update(fundOperationMirror);
		fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOperationMirror,
				BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode()));
	}		
	
	
	/**
	 * Reject deposit multiple.
	 *
	 * @param lstFundOperation the lst fund operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void rejectDepositMultiple(List<FundsOperation> lstFundOperation,LoggerUser loggerUser) throws ServiceException{
		if(lstFundOperation != null && !lstFundOperation.isEmpty()){
			for(FundsOperation objFundsOperation : lstFundOperation){
				rejectDeposit(objFundsOperation, loggerUser);
			}
		}
	}
	
	
	/**
	 * Reject deposit.
	 *
	 * @param fundOperation the fund operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void rejectDeposit(FundsOperation fundOperation,LoggerUser loggerUser) throws ServiceException{
		FundsOperation fundOpeTemp = manageDepositServiceBean.find(fundOperation.getIdFundsOperationPk(), FundsOperation.class);
		
		if(BooleanType.YES.getCode().equals(fundOpeTemp.getIndAutomatic())){
			if(!MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode().equals(fundOpeTemp.getOperationState())){
				Object[] bodyData = new Object[1];
				bodyData[0] = fundOpeTemp.getIdFundsOperationPk().toString();
				throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_REQUEST_STATE_MODIFY, bodyData);
			}
			fundOpeTemp.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode());
			manageDepositServiceBean.update(fundOpeTemp);
			FundsOperation fundsOpeRetire = new FundsOperation();
			fundsOpeRetire.setSenderBicCode(fundOpeTemp.getSenderBicCode());
			fundsOpeRetire.setInstitutionCashAccount(fundOpeTemp.getInstitutionCashAccount());
			fundsOpeRetire.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
			fundsOpeRetire.setFundsOperationGroup(fundOpeTemp.getFundsOperationGroup());
			if(com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_RECEPTION_COMMENTED.getCode().equals(fundOpeTemp.getFundsOperationType())){
				fundsOpeRetire.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_WITHDRAWAL_COMMENTED.getCode());
				fundsOpeRetire.setIssuer(fundOpeTemp.getIssuer());
				fundsOpeRetire.setBank(fundOpeTemp.getBank());
				fundsOpeRetire.setParticipant(fundOpeTemp.getParticipant());
			}
			else if(com.pradera.model.component.type.ParameterFundsOperationType.REFUNDS_RECEPTION_COMMENTED.getCode().equals(fundOpeTemp.getFundsOperationType())){
				fundsOpeRetire.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.REFUNDS_WITHDRAWAL_COMMENTED.getCode());
				fundsOpeRetire.setBank(fundOpeTemp.getBank());
				fundsOpeRetire.setParticipant(fundOpeTemp.getParticipant());
			} 
			else if(com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_SUPPLY_DEPOSIT.getCode().equals(fundOpeTemp.getFundsOperationType())){
				fundsOpeRetire.setIssuer(fundOpeTemp.getIssuer());
				fundsOpeRetire.setBank(fundOpeTemp.getBank());
				fundsOpeRetire.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_BENEFITS.getCode());
			} 
			else if(com.pradera.model.component.type.ParameterFundsOperationType.SETTLEMENT_OPERATIONS_FUND_DEPOSITS.getCode().equals(fundOpeTemp.getFundsOperationType())){
				fundsOpeRetire.setParticipant(fundOpeTemp.getParticipant());
				fundsOpeRetire.setPaymentReference(fundOpeTemp.getPaymentReference());
				fundsOpeRetire.setMechanismOperation(fundOpeTemp.getMechanismOperation());
				fundsOpeRetire.setOperationPart(fundOpeTemp.getOperationPart());
				fundsOpeRetire.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_SETTLEMENT.getCode());
			}
			fundsOpeRetire.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
			fundsOpeRetire.setRegistryDate(CommonsUtilities.currentDateTime());
			fundsOpeRetire.setRegistryUser(loggerUser.getUserName());
			fundsOpeRetire.setIndAutomatic(BooleanType.YES.getCode());
			fundsOpeRetire.setIndCentralized(BooleanType.YES.getCode());
			fundsOpeRetire.setRetirementType(FundsRetirementType.BY_TOTAL.getCode());
			fundsOpeRetire.setCurrency(fundOpeTemp.getCurrency());			
			fundsOpeRetire.setOperationAmount(fundOpeTemp.getOperationAmount());
			fundsOpeRetire.setFundsOperation(fundOpeTemp);
			manageDepositServiceBean.create(fundsOpeRetire);
			fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOpeRetire,
										BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_RETIREMENT_PROCESS.getCode()));
		}else{		
			if(!MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode().equals(fundOpeTemp.getOperationState())){
				Object[] bodyData = new Object[1];
				bodyData[0] = fundOpeTemp.getIdFundsOperationPk().toString();
				throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_REQUEST_STATE_MODIFY, bodyData);
			}
			
			boolean isRelated= manageDepositServiceFacade.validateDayIsOpen(fundOpeTemp);
			
			fundOpeTemp.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode());
			manageDepositServiceBean.update(fundOpeTemp);
			fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOpeTemp,
					BusinessProcessType.FUNDS_DEPOSIT_REJECT.getCode()));
			if(isRelated){
				if(BooleanType.NO.getCode().equals(fundOpeTemp.getIndCentralized()))
					rejectMirrorOperationForCentralAccount(fundOpeTemp);
			}
		}
	}
	
	/**
	 * Reject mirror operation for central account.
	 *
	 * @param fundOpeOrigin the fund ope origin
	 * @throws ServiceException the service exception
	 */
	private void rejectMirrorOperationForCentralAccount(FundsOperation fundOpeOrigin) throws ServiceException{
		FundsOperation fundOperationMirror = manageDepositServiceBean.getFundOperationMirror(fundOpeOrigin.getFundsOperation().getIdFundsOperationPk());
		fundOperationMirror.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode());
		manageDepositServiceBean.update(fundOperationMirror);
		fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundOperationMirror,
				BusinessProcessType.FUNDS_DEPOSIT_REJECT.getCode()));
	}
	
	/**
	 * Gets the funds component to.
	 *
	 * @param fundsOperation the funds operation
	 * @param idBusinessProcess the id business process
	 * @return the funds component to
	 */
	private FundsComponentTO getFundsComponentTO(FundsOperation fundsOperation, Long idBusinessProcess){
		FundsComponentTO fundsComponentTO = new FundsComponentTO();
		fundsComponentTO.setIdBusinessProcess(idBusinessProcess);
		fundsComponentTO.setIdInstitutionCashAccount(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		fundsComponentTO.setIdFundsOperation(fundsOperation.getIdFundsOperationPk());
		fundsComponentTO.setIdFundsOperationType(fundsOperation.getFundsOperationType());
		fundsComponentTO.setCashAmount(fundsOperation.getOperationAmount());
		fundsComponentTO.setOperationState(fundsOperation.getOperationState());
		return fundsComponentTO;
	}
	
	
	/**
	 * Save deposit.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @param loggerUser the logger user
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long saveDeposit(RegisterDepositFundsTO registerDepositFundsTO,LoggerUser loggerUser) throws ServiceException{
		
		//Create the fund operation for the specific cash account
		FundsOperation fundsOperationOrigin = fillFundOperation(registerDepositFundsTO, loggerUser, null);
		boolean isRelated = isRelatedCentral(fundsOperationOrigin.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		if(isRelated){
			//Get the cash account for central account
			InstitutionCashAccount insCashAccount = manageDepositServiceBean.getCashAccountForCentralAccount(fundsOperationOrigin.getCurrency());
			//Create a mirror for the first fund operation but with the central account cash account
			FundsOperation fundsOperationMirror = fillFundOperation(registerDepositFundsTO, loggerUser, insCashAccount);
			fundsOperationOrigin.setFundsOperation(fundsOperationMirror);
			manageDepositServiceBean.update(fundsOperationOrigin);
		}
		return Validations.validateIsNotNullAndNotEmpty(fundsOperationOrigin.getIdFundsOperationPk())?fundsOperationOrigin.getIdFundsOperationPk():null;
	}
	
	/**
	 * Fill fund operation.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @param loggerUser the logger user
	 * @param instCashAccount the inst cash account
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	private FundsOperation fillFundOperation(RegisterDepositFundsTO registerDepositFundsTO, LoggerUser loggerUser, InstitutionCashAccount instCashAccount) throws ServiceException{
		//Datos genericos de la operacion de fondos
		FundsOperation fundsOperation = new FundsOperation();
		fundsOperation.setFundsOperationGroup(registerDepositFundsTO.getFundOperationGroupSelected());
		fundsOperation.setFundsOperationType(registerDepositFundsTO.getOperationTypeSelected());	
		fundsOperation.setFundsOperationClass(OperationClassType.RECEPTION_FUND.getCode());
		fundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundsOperation.setRegistryUser(loggerUser.getUserName());
		fundsOperation.setBankMovement(registerDepositFundsTO.getBankMovement());
		if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
			fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
		else
			fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundsOperation.setIndAutomatic(BooleanType.NO.getCode());
		if(Validations.validateIsNullOrEmpty(instCashAccount))
			fundsOperation.setIndCentralized(BooleanType.NO.getCode());
		else
			fundsOperation.setIndCentralized(BooleanType.YES.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getCashAccountDetail())){
			if(registerDepositFundsTO.isBlIssuer()){
				fundsOperation.setIssuer(registerDepositFundsTO.getIssuer());
				if(Validations.validateIsNullOrEmpty(instCashAccount)){
					//registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().setDepositAmount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().getDepositAmount());
					fundsOperation.setInstitutionCashAccount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount());
					boolean isRelated = isRelatedCentral(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					if(isRelated){
						if(!isDaysIsOpen())
							throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
					}
				}else
				{	//instCashAccount.setDepositAmount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().getDepositAmount());
					fundsOperation.setInstitutionCashAccount(instCashAccount);
				}
				fundsOperation.setBank(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getProviderBank());
				fundsOperation.setBankAccountNumber(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getAccountNumber());
				fundsOperation.setCurrency(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getCurrency());
				fundsOperation.setOperationAmount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().getDepositAmount());
				manageDepositServiceBean.create(fundsOperation);	
				//manageDepositServiceBean.update(fundsOperation.getInstitutionCashAccount());
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
			}else if(registerDepositFundsTO.isBlEDV()){
				Participant participant = new Participant();
				participant.setIdParticipantPk(idepositarySetup.getIdParticipantDepositary());
				fundsOperation.setParticipant(participant);
				if(Validations.validateIsNullOrEmpty(instCashAccount)){
					fundsOperation.setInstitutionCashAccount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount());
					boolean isRelated = isRelatedCentral(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					if(isRelated){
						if(!isDaysIsOpen())
							throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
					}
				}else
					fundsOperation.setInstitutionCashAccount(instCashAccount);
				fundsOperation.setBank(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getProviderBank());
				fundsOperation.setBankAccountNumber(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getAccountNumber());
				fundsOperation.setCurrency(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getCurrency());				
				fundsOperation.setOperationAmount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().getDepositAmount());
				manageDepositServiceBean.create(fundsOperation);
				//manageDepositServiceBean.update(fundsOperation.getInstitutionCashAccount());
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
			}else if(registerDepositFundsTO.isBlParticipant()){
				Participant participant = new Participant();
				participant.setIdParticipantPk(registerDepositFundsTO.getParticipantSelected().longValue());
				fundsOperation.setParticipant(participant);
				if(Validations.validateIsNullOrEmpty(instCashAccount)){
					fundsOperation.setInstitutionCashAccount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount());
					boolean isRelated = isRelatedCentral(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					if(isRelated){
						if(!isDaysIsOpen())
							throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
					}
				}else
					fundsOperation.setInstitutionCashAccount(instCashAccount);
				fundsOperation.setBank(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getProviderBank());
				fundsOperation.setBankAccountNumber(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getAccountNumber());
				fundsOperation.setCurrency(registerDepositFundsTO.getCashAccountDetail().getInstitutionBankAccount().getCurrency());
				fundsOperation.setOperationAmount(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().getDepositAmount());
				manageDepositServiceBean.create(fundsOperation);
				//manageDepositServiceBean.update(fundsOperation.getInstitutionCashAccount());
				
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
			}
		}
		else 
			if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getReturnsDepositTO())){
			List<HolderFundsOperation> listHolderFundsOperations = new ArrayList<HolderFundsOperation>();
			if(Validations.validateIsNullOrEmpty(instCashAccount))
				fundsOperation.setInstitutionCashAccount(registerDepositFundsTO.getReturnsDepositTO().getCashAccountDetail().getInstitutionCashAccount());
			else
				fundsOperation.setInstitutionCashAccount(instCashAccount);
			fundsOperation.setCurrency(fundsOperation.getInstitutionCashAccount().getCurrency());
			boolean isRelated = isRelatedCentral(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
			if(isRelated){
				if(!isDaysIsOpen())
					throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
			}
			
			if(BooleanType.NO.getCode().equals(fundsOperation.getIndCentralized())){
				for(HolderFundsOperation holFundOpeTemp:registerDepositFundsTO.getReturnsDepositTO().getLstHolderFundsOperation()){
					if(holFundOpeTemp.isSelected()){
						holFundOpeTemp.setFundsOperation(fundsOperation);
						holFundOpeTemp.setHolderOperationState(HolderFundsOperationStateType.REGISTERED.getCode());
	  					listHolderFundsOperations.add(holFundOpeTemp);					 
					}
				}
				fundsOperation.setHolderFundsOperations(listHolderFundsOperations);
	 			Integer nextOperationNumber = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), fundsOperation.getFundsOperationGroup());
	 			fundsOperation.setFundOperationNumber(nextOperationNumber);
			}

 			fundsOperation.setBank(registerDepositFundsTO.getReturnsDepositTO().getCashAccountDetail().getInstitutionBankAccount().getProviderBank());
			fundsOperation.setBankAccountNumber(registerDepositFundsTO.getReturnsDepositTO().getCashAccountDetail().getInstitutionBankAccount().getAccountNumber());
			fundsOperation.setOperationAmount(registerDepositFundsTO.getReturnsDepositTO().getDepositAmount());			
			manageDepositServiceBean.create(fundsOperation);	
			//manageDepositServiceBean.update(fundsOperation.getInstitutionCashAccount());
			fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
		}else  if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getSettlementGroupTO())){			
			if(registerDepositFundsTO.getSettlementGroupTO().isBlSettlementNet()){
				validateSettlementFundsDeposit(registerDepositFundsTO);
				if(Validations.validateIsNullOrEmpty(instCashAccount)){
					fundsOperation.setInstitutionCashAccount(registerDepositFundsTO.getSettlementGroupTO().getCashAccountDetail().getInstitutionCashAccount());
					fundsOperation.setParticipant(new Participant(registerDepositFundsTO.getParticipantSelected().longValue()));
					boolean isRelated = isRelatedCentral(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					if(isRelated){
						if(!isDaysIsOpen())
							throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
					}
				}else
				{
					//instCashAccount.setDepositAmount(registerDepositFundsTO.getSettlementGroupTO().getCashAccountDetail().getInstitutionCashAccount().getDepositAmount());
					fundsOperation.setInstitutionCashAccount(instCashAccount);
				}
				fundsOperation.setOperationAmount(registerDepositFundsTO.getSettlementGroupTO().getCashAccountDetail().getInstitutionCashAccount().getDepositAmount());
				fundsOperation.setCurrency(registerDepositFundsTO.getSettlementGroupTO().getCashAccountDetail().getInstitutionCashAccount().getCurrency());
				manageDepositServiceBean.create(fundsOperation);
				//manageDepositServiceBean.update(fundsOperation.getInstitutionCashAccount());
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
			}else  if(registerDepositFundsTO.getSettlementGroupTO().isBlSettlementGross()){
				if(Validations.validateIsNullOrEmpty(instCashAccount)){
					InstitutionCashAccount instCashAcc = manageDepositServiceBean.find(InstitutionCashAccount.class, registerDepositFundsTO.getSettlementGroupTO().getIdInstitutionCashAccountPk());
					//instCashAcc.setDepositAmount(registerDepositFundsTO.getSettlementGroupTO().getDepositAmount());				
					fundsOperation.setInstitutionCashAccount(instCashAcc);
					fundsOperation.setParticipant(new Participant(registerDepositFundsTO.getParticipantSelected().longValue()));
					boolean isRelated = isRelatedCentral(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					if(isRelated){
						if(!isDaysIsOpen())
							throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
					}
				}else
				{
					//InstitutionCashAccount instCashAcc = manageDepositServiceBean.find(InstitutionCashAccount.class, registerDepositFundsTO.getSettlementGroupTO().getIdInstitutionCashAccountPk());
					fundsOperation.setInstitutionCashAccount(instCashAccount);
					//instCashAccount.setDepositAmount(registerDepositFundsTO.getSettlementGroupTO().getDepositAmount());	
					//fundsOperation.setInstitutionCashAccount(instCashAccount);
				}
				fundsOperation.setCurrency(registerDepositFundsTO.getSettlementGroupTO().getCurrency());
				fundsOperation.setOperationAmount(registerDepositFundsTO.getSettlementGroupTO().getDepositAmount());
				/*MechanismOperation mechanismOperation = new MechanismOperation();
				mechanismOperation.setIdMechanismOperationPk(registerDepositFundsTO.getSettlementGroupTO().getMechanismOperationPk());*/
				fundsOperation.setMechanismOperation(new MechanismOperation(registerDepositFundsTO.getSettlementGroupTO().getMechanismOperationPk()));
				SettlementOperation settlementOperation=new SettlementOperation();
				settlementOperation.setIdSettlementOperationPk(registerDepositFundsTO.getSettlementGroupTO().getSettlementOperationPk());
				fundsOperation.setSettlementOperation(settlementOperation);
				fundsOperation.setPaymentReference(registerDepositFundsTO.getSettlementGroupTO().getReferencePayment().trim());
				fundsOperation.setOperationPart(registerDepositFundsTO.getSettlementGroupTO().getOperationPartSelected());
				manageDepositServiceBean.create(fundsOperation);
				//manageDepositServiceBean.update(fundsOperation.getInstitutionCashAccount());
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
			}			
		} else if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getInternationalDepositTO())){			
			if(Validations.validateIsNullOrEmpty(instCashAccount)){
				fundsOperation.setInstitutionCashAccount(registerDepositFundsTO.getInternationalDepositTO().getCashAccountDetail().getInstitutionCashAccount());
				fundsOperation.setParticipant(new Participant(registerDepositFundsTO.getParticipantSelected().longValue()));
				boolean isRelated = isRelatedCentral(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
				if(isRelated){
					if(!isDaysIsOpen())
						throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
				}
			}else
				fundsOperation.setInstitutionCashAccount(instCashAccount);
			List<FundsInternationalOperation> lstFundsInterOperation = new ArrayList<FundsInternationalOperation>();
			if(registerDepositFundsTO.getInternationalDepositTO().isBlReception()){
				InternationalOperation internationalOpe = new InternationalOperation();
				internationalOpe.setIdInternationalOperationPk(registerDepositFundsTO.getInternationalDepositTO().getIdInternationalOperationPk());
				FundsInternationalOperation fundsIntOpe = new FundsInternationalOperation();
				fundsIntOpe.setInternationalOperation(internationalOpe);
				fundsIntOpe.setFundsOperation(fundsOperation);
				manageDepositServiceBean.create(fundsIntOpe);
				lstFundsInterOperation.add(fundsIntOpe);
				fundsOperation.setCurrency(registerDepositFundsTO.getInternationalDepositTO().getCurrency());
				fundsOperation.setOperationAmount(registerDepositFundsTO.getInternationalDepositTO().getDepositAmount());
				fundsOperation.setLstFundsInternationalOperation(lstFundsInterOperation);
				manageDepositServiceBean.create(fundsOperation);
				//manageDepositServiceBean.update(fundsOperation.getInstitutionCashAccount());
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
			}else if(registerDepositFundsTO.getInternationalDepositTO().isBlSend()){
				for(InternationalOperation interOpe:registerDepositFundsTO.getInternationalDepositTO().getLstInternationalOperation()){
					if(interOpe.isBlCheck()){						
						FundsInternationalOperation fundsIntOpe = new FundsInternationalOperation();
						fundsIntOpe.setInternationalOperation(interOpe);
						fundsIntOpe.setFundsOperation(fundsOperation);
						manageDepositServiceBean.create(fundsIntOpe);
						lstFundsInterOperation.add(fundsIntOpe);
					}
				}
				fundsOperation.setCurrency(registerDepositFundsTO.getInternationalDepositTO().getCurrencySelected());
				fundsOperation.setOperationAmount(registerDepositFundsTO.getInternationalDepositTO().getDepositAmount());
				fundsOperation.setLstFundsInternationalOperation(lstFundsInterOperation);
				manageDepositServiceBean.create(fundsOperation);
				fundsComponentService.get().executeFundsComponent(getFundsComponentTO(fundsOperation,	BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode()));
			}
			}
			return fundsOperation;
			
		}
	
	/**
	 * Validate settlement funds deposit.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @throws ServiceException the service exception
	 */
	private void validateSettlementFundsDeposit(RegisterDepositFundsTO registerDepositFundsTO) throws ServiceException {
		List<SettlementProcess> lstSettlementProcess= settlementProcessServiceBean.getListSettlementProcess(
																			registerDepositFundsTO.getSettlementGroupTO().getModaGroupSelected(), 
																			CommonsUtilities.currentDate(), SettlementSchemaType.NET.getCode(), 
																			registerDepositFundsTO.getSettlementGroupTO().getCurrency());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementProcess)) {
			for (SettlementProcess objSettlementProcess: lstSettlementProcess) {
				if (!SettlementProcessStateType.FINISHED.getCode().equals(objSettlementProcess.getProcessState())) {
					throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_NOT_FINISH, ErrorServiceType.SETTLEMENT_PROCESS_NOT_FINISH.getMessage());
				}
			}
		}
	}
	
	/**
	 * Checks if is days is open.
	 *
	 * @return true, if is days is open
	 * @throws ServiceException the service exception
	 */
	public boolean isDaysIsOpen() throws ServiceException{
		BeginEndDay beginEndDay = manageDepositServiceBean.getBeginEndDay();
		if(Validations.validateIsNotNullAndNotEmpty(beginEndDay))
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if is related central.
	 *
	 * @param idInstitutionCashAccountPk the id institution cash account pk
	 * @return true, if is related central
	 * @throws ServiceException the service exception
	 */
	public boolean isRelatedCentral(Long idInstitutionCashAccountPk) throws ServiceException{
		Integer indRelatedBCRD = manageDepositServiceBean.getIndRelatedBCRD(idInstitutionCashAccountPk);
		if(BooleanType.YES.getCode().equals(indRelatedBCRD))
			return true;
		else
			return false;
	}
	

	/**
	 * Confirm retirement.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void confirmRetirement(FundsOperation objFundsOperation,LoggerUser loggerUser) throws ServiceException{
		// GET INDICATOR SEND LIP
		boolean indSendLip = objFundsOperation.isIndSendLip();
		String bankMovement = objFundsOperation.getBankMovement();
        //Finding All object of funds operation
        objFundsOperation = depositServiceBean.getFundOperationDetail(objFundsOperation.getIdFundsOperationPk());
        //objFundsOperation = this.fundsOperationServiceBean.find(FundsOperation.class, objFundsOperation.getIdFundsOperationPk());
        //Setting confirmed status
        objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
        objFundsOperation.setBankMovement(bankMovement);
        // SET INDICATOR SEND LIP
        objFundsOperation.setIndSendLip(indSendLip);
        //Validating if there is a funds operation related to execute retirement component
        if(Validations.validateIsNotNull(objFundsOperation.getFundsOperation())
        	&& Validations.validateIsNotNull(objFundsOperation.getFundsOperation().getIdFundsOperationPk())	){
        	
        	Long idRelatedFundsOperation= objFundsOperation.getFundsOperation().getIdFundsOperationPk();
	        FundsOperation objRelatedFundsOperation = fundsOperationServiceBean.find(FundsOperation.class, idRelatedFundsOperation);
	        objRelatedFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
	        objRelatedFundsOperation.setBankMovement(bankMovement);
	        
	        /** EXECUTING COMPONENT FOR RELATED FUNDS OPERATION **/
	        executeComponenteService(objRelatedFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_CONFIRM.getCode());

	        fundsOperationServiceBean.update(objRelatedFundsOperation);
        }
        if(objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())){
        	//VALIDATING IF THE DEVOLUTION IS TO BCRD
//            if(objFundsOperation.getIndBcrdPayback().equals(BooleanType.NO.getCode())){
//	        	FundsOperation depositFundsOperation = this.fundsOperationServiceBean.searchFundsOperationRelated(objFundsOperation.getIdFundsOperationPk());            
//	        	//now we have to execute the funds component to deposit
//	            FundsComponentTO secondObjFundsComponentTO = new FundsComponentTO();
//	            secondObjFundsComponentTO.setIdFundsOperation(depositFundsOperation.getIdFundsOperationPk());
//	            secondObjFundsComponentTO.setIdBusinessProcess(BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode());
//	            secondObjFundsComponentTO.setIdFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_DEPOSIT.getCode());
//	            secondObjFundsComponentTO.setCashAmount(depositFundsOperation.getOperationAmount());
//	            secondObjFundsComponentTO.setIdInstitutionCashAccount(depositFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
//	            fundsComponentService.get().executeFundsComponent(secondObjFundsComponentTO);
//            }
            
        }else if(objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())
 			  ||objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())
   			  ||objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
        	HolderFundsOperationTO holderFundsOperationTO = new HolderFundsOperationTO();
        	holderFundsOperationTO.setIdHolderFundsOperationPk(objFundsOperation.getIdFundsOperationPk());
        	
        	//Calling service to get holder funds operation related
        	List<HolderFundsOperation> objHolderFundsOperation = fundsOperationServiceBean.searchHolderFundsOperationsLst(holderFundsOperationTO);
        	List<HolderCashMovement> holderCashMovementsLst = new ArrayList<HolderCashMovement>();
        	for(HolderFundsOperation holderFundsOperationObj : objHolderFundsOperation){
	        	//Creating retirement holder cash movement
	        	HolderCashMovement retirementHolderMovement = createRetirementHolderCashMovement(holderFundsOperationObj);
	    		//Adding Retirement Cash Movement to list
	        	holderCashMovementsLst.add(retirementHolderMovement);
	        	//Setting paid status to related holder cash movement
	        	holderFundsOperationObj.getRelatedHolderCashMovement().setMovementState(HolderCashMovementStateType.PAID.getCode());
				
											
				holderFundsOperationObj.setHolderOperationState(HolderFundsOperationStateType.CONFIRMED.getCode());
				
				this.fundsOperationServiceBean.create(retirementHolderMovement);
				this.fundsOperationServiceBean.update(holderFundsOperationObj);
				this.fundsOperationServiceBean.update(holderFundsOperationObj.getRelatedHolderCashMovement());
        	}
        	
        	//objFundsOperation.setHolderCashMovement(holderCashMovementsLst);
        	
    	}else if(objFundsOperation.getFundsOperationGroup().equals(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode())){
    		if(Validations.validateIsNotNull(objFundsOperation.getMechanismOperation()) && Validations.validateIsNotNull(objFundsOperation.getMechanismOperation().getIdMechanismOperationPk())){
    			MechanismOperation mo = objFundsOperation.getMechanismOperation();
    			Integer settlementSchema = (mo.getSettlementSchema()!=null) ? mo.getSettlementSchema() : objFundsOperation.getInstitutionCashAccount().getSettlementSchema();
    			//VALIDATING SETTLEMENT SCHEMA TYPE IF GROSS THE UPDATE PARTICIPANT OPERATION
    			if(settlementSchema.equals(SettlementSchemaType.GROSS.getCode())){
    				//GETTING MECHANISM OPERATION PK
    				Long idMechanismOperationFk = mo.getIdMechanismOperationPk();
    				//GETTING PARTICIPANT PK
					Long idParticipantPk = objFundsOperation.getParticipant().getIdParticipantPk();
					//GETTING OPERATION PART
					Integer operationPart = objFundsOperation.getOperationPart();
					//FINDING PARTICIPANT OPERATION
					ParticipantSettlement pSettlement = fundsOperationServiceFacade.findParticipantSettlement(idMechanismOperationFk, idParticipantPk, operationPart);
					pSettlement = fundsOperationServiceBean.find(ParticipantSettlement.class, pSettlement.getIdParticipantSettlementPk());

					//Substract the excedent amount!
					BigDecimal depositedAmount = pSettlement.getDepositedAmount().subtract(objFundsOperation.getOperationAmount());
					pSettlement.setDepositedAmount(depositedAmount);

					//SETTING NULL BECAUSE ALL FUNDS HAS BEEN RETIRED
					if(objFundsOperation.getRetirementType().equals(FundsRetirementType.BY_TOTAL.getCode()))					
						pSettlement.setFundsReference(null);
					
					/** UPDATING PARTICIPANT OPERATION **/
					fundsOperationServiceBean.update(pSettlement);
					
					//Update mechanismOperation set null funds reference
					//collectionProcessServiceBean.updateFundsMechanismOperation(idMechanismOperationFk, operationPart, null, loggerUser);
				}
    		}
    		
    	} 
        /** EXECUTING COMPONENT FUNDS OPERATION **/
        executeComponenteService(objFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_CONFIRM.getCode()); 
        
        if(objFundsOperation.getFundsOperationType().equals(ParameterFundsOperationType.WITHDRAWL_FUND_BENEFITS.getCode())&&
        		objFundsOperation.getIndTransferCentralizing().equals(BooleanType.YES.getCode())){
        		FundsOperation centralFundsOperation = new FundsOperation();
        		centralFundsOperation.setInstitutionCashAccount(receptionService.getActivatedCentralizingAccount(objFundsOperation.getCurrency()));
        		centralFundsOperation.setCurrency(objFundsOperation.getCurrency());
        		centralFundsOperation.setOperationAmount(objFundsOperation.getOperationAmount());
        		centralFundsOperation.setIndAutomatic(BooleanType.YES.getCode());
        		centralFundsOperation.setRegistryUser(loggerUser.getUserName());
        		centralFundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
        		centralFundsOperation.setIndCentralized(BooleanType.NO.getCode());
        		centralFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
        		centralFundsOperation.setFundsOperationClass(objFundsOperation.getFundsOperationClass());
        		centralFundsOperation.setFundsOperationGroup(objFundsOperation.getFundsOperationGroup());
        		centralFundsOperation.setFundsOperationType(objFundsOperation.getFundsOperationType());
        		manageDepositServiceBean.create(centralFundsOperation);
        		executeFundComponent(centralFundsOperation, BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_RETIREMENT_PROCESS.getCode());
        		
        }
        
        if(objFundsOperation.getFundsOperationType().equals(
    			com.pradera.model.component.type.ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT_MANUAL.getCode()) ){
    		
    		//ENVIAR WEBSERVICES
    		
        	try {
        		
        		// if the idepositary lip parameter is active
        		
    			if(objFundsOperation.isIndSendLip() && idepositarySetup.getIndFundsThridPart().equals(GeneralConstants.ONE_VALUE_INTEGER)){				
    				settlementServiceConsumer.sendFundsLipWebClient(objFundsOperation, loggerUser);
    				fundsOperationServiceBean.updateParticipantPosition(objFundsOperation.getSettlementProcess().getIdSettlementProcessPk(),
    						objFundsOperation.getParticipant().getIdParticipantPk(), GeneralConstants.ONE_VALUE_INTEGER, loggerUser);
    						
    			}  else {
    				fundsOperationServiceBean.updateParticipantPosition(objFundsOperation.getSettlementProcess().getIdSettlementProcessPk(),
    						objFundsOperation.getParticipant().getIdParticipantPk(), GeneralConstants.ZERO_VALUE_INTEGER, loggerUser);
    			}
        		
        	}  catch(Exception ex) {
				ex.printStackTrace();
				if (ex instanceof com.pradera.integration.exception.ServiceException) {
					ServiceException se= (ServiceException) ex;
					throw se;
				}
				throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
			}    		
    	}
        
        if(objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.FUNDS_SETTLEMENT_WITHDRAWL.getCode())){
        	objFundsOperation.setIndBankRetirement(BooleanType.YES.getCode());
        	try {
        		if(objFundsOperation.isIndSendLip()){	
            		settlementServiceConsumer.sendFundsLipWebClient(objFundsOperation, loggerUser);
            	}
        	}  catch(Exception ex) {
				ex.printStackTrace();
				if (ex instanceof com.pradera.integration.exception.ServiceException) {
					ServiceException se= (ServiceException) ex;
					throw se;
				}
				throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
			}         	
        }
        
        fundsOperationServiceBean.update(objFundsOperation);
	}
	
	/**
	 * Execute componente service.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @param businessProcess the business process
	 * @throws ServiceException the service exception
	 */
	private void executeComponenteService(FundsOperation objFundsOperation, Long businessProcess)throws ServiceException{
		FundsComponentTO objFundsComponentTO = new FundsComponentTO();
        objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
        objFundsComponentTO.setIdBusinessProcess(businessProcess);
        objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
        objFundsComponentTO.setCashAmount(objFundsOperation.getOperationAmount());
        objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
        objFundsComponentTO.setOperationState(objFundsOperation.getOperationState());
        //Executing component
        fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
	}
	
	/**
	 * Creates the retirement holder cash movement.
	 *
	 * @param objHolderFundsOperation the obj holder funds operation
	 * @return the holder cash movement
	 */
	private HolderCashMovement createRetirementHolderCashMovement(HolderFundsOperation objHolderFundsOperation){
		HolderCashMovement retirementHolderMovement = new HolderCashMovement();
		FundsOperation fundsOperation = new FundsOperation();
		fundsOperation.setIdFundsOperationPk(objHolderFundsOperation.getFundsOperation().getIdFundsOperationPk());
		
		retirementHolderMovement.setFundsOperation(fundsOperation);
		retirementHolderMovement.setHolder(objHolderFundsOperation.getHolder());
		retirementHolderMovement.setMovementDate(CommonsUtilities.currentDate());
		retirementHolderMovement.setCurrency(objHolderFundsOperation.getCurrency());								
		retirementHolderMovement.setFullName(objHolderFundsOperation.getFullNameHolder());
		retirementHolderMovement.setMovementState(HolderCashMovementStateType.PAID.getCode());
		retirementHolderMovement.setBank(objHolderFundsOperation.getHolderAccountBank().getBank());
		retirementHolderMovement.setHolderAmount(objHolderFundsOperation.getHolderOperationAmount());	
		retirementHolderMovement.setDocumentHolder(objHolderFundsOperation.getHolderDocumentNumber());				
		retirementHolderMovement.setHolderAccountBank(objHolderFundsOperation.getHolderAccountBank());
		retirementHolderMovement.setFundsOperationType(objHolderFundsOperation.getFundsOperationType());				
		retirementHolderMovement.setBankAccountType(objHolderFundsOperation.getHolderAccountBank().getBankAccountType());
		retirementHolderMovement.setBankAccountNumber(objHolderFundsOperation.getHolderAccountBank().getBankAccountNumber());
		
		return retirementHolderMovement;
	}
	
	/**
	 * Validate and save deposit of funds lip tx.
	 *
	 * @param fundsTransferRegisterTO the funds transfer register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveDepositOfFundsLipTx(FundsTransferRegisterTO fundsTransferRegisterTO, 
			LoggerUser loggerUser) throws ServiceException {
		
		Object objResult = validateAndSaveDepositOfFundsLip(fundsTransferRegisterTO, loggerUser);
		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(fundsTransferRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}		
	}
	
	/**
	 * Validate and save deposit of funds lip.
	 *
	 * @param fundsTransferRegisterTO the funds transfer register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveDepositOfFundsLip(FundsTransferRegisterTO fundsTransferRegisterTO, 
			LoggerUser loggerUser) throws ServiceException{
		
		AutomaticSendType automaticSendTypeTO = fundsTransferRegisterTO.getAutomaticSendTypeTO();
		
		// validate if participant entity is allowed for ACV interface
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(automaticSendTypeTO.getTargetParticipantCode());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		
		return null;
		
	}
	
	/**
	 * Gets the institution cash account information.
	 *
	 * @param objInstitutionCashAccountTO the obj institution cash account to
	 * @return the institution cash account information
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> getInstitutionCashAccountInformation(InstitutionCashAccountTO objInstitutionCashAccountTO) throws ServiceException{
		return manageDepositServiceBean.getInstitutionCashAccountInformation(objInstitutionCashAccountTO);
	}
	
	/**
	 * Send deposit lip tx.
	 *
	 * @param objFundsTransferRegisterTO the obj funds transfer register to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public void sendDepositLipTx(FundsTransferRegisterTO objFundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException {
		try {
			sendDepositLip(objFundsTransferRegisterTO, loggerUser);
		} catch (Exception e) {
			throw e;
		}		
	}
	
	/**
	 * Send deposit lip.
	 *
	 * @param objFundsTransferRegisterTO the obj funds transfer register to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendDepositLip(FundsTransferRegisterTO objFundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException{		
							
		try {
			
			// OBTENER MOVIMIENTOS POR EXTRACTO
			List<AutomaticSendType> lstAutomaticSendType = settlementServiceConsumer.sendFundsExtractLip(objFundsTransferRegisterTO, loggerUser);					
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstAutomaticSendType)){
												
				for(AutomaticSendType objAutomaticSendType : lstAutomaticSendType){
					try {
						savOperationDeposit(objAutomaticSendType, loggerUser);						
					} catch (Exception e) {						
					}
												
				}								
			}				
		} catch (Exception e){
			throw e;
		}				
	}
	
	/**
	 * Sav operation deposit.
	 *
	 * @param objAutomaticSendType the obj automatic send type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public void savOperationDeposit(AutomaticSendType objAutomaticSendType, LoggerUser loggerUser) throws ServiceException {
		FundsOperation objFundsOperationOrigen = new FundsOperation();
		Participant participantSearch = null;
		InstitutionBankAccount institutionBankAccountSearch = null;
		try {
			
			// OBTENER TIPO DE OPERACION
			
			String strOperationType = objAutomaticSendType.getCodTypeOperation();
			
			// verificando que operacion se va a ejecutar Operacion E73
//			if(StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E73, strOperationType)){
				// buscando participante por codigo de BCB
				Participant participantFilter = new Participant();
				participantFilter.setIdBcbCode(objAutomaticSendType.getOriginParticipantCode());
					
				participantSearch = participantServiceBean.findParticipantByFilters(participantFilter);//ObjectByNamedQuery(Participant.PARTICIPANT_ID_BCB_CODE, mapaParametros);
					
				
				// verificando la existencia del participante con el Codigo BCB
				if(participantSearch != null){
					// cargando nemonico
					objAutomaticSendType.setMnemonicParticipant(participantSearch.getMnemonic());
				} else{
					String mNemonic = objAutomaticSendType.getMnemonicParticipant();
					mNemonic = mNemonic == null ? mNemonic : mNemonic.length() >= 3 ? mNemonic.substring(0, 3) : mNemonic;
					objAutomaticSendType.setMnemonicParticipant(mNemonic);
				} 			
//			} 
			
			objAutomaticSendType.setIndRecepFunds(ComponentConstant.ZERO);
			Integer currency = null;
			
			// OBTENER LA MONEDA DE LA TRANSFERENCIA
			
			if(Validations.validateIsNotNullAndNotEmpty(objAutomaticSendType.getOriginCurrency())){
				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				parameterTableTO.setShortInteger(new Integer(objAutomaticSendType.getOriginCurrency()));
				ParameterTable objParameterTable = new ParameterTable();
				List<ParameterTable> lstParameterTable = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstParameterTable)){
					objParameterTable = lstParameterTable.get(0);
					currency = objParameterTable.getParameterTablePk();
				}												
				
			}
			
			// VALIDAR SI YA EXISTE LA OPERACION DE DEPOSITO
			
			LipMessageHistoryTO objLipMessageHistoryTO = new LipMessageHistoryTO();	
			objLipMessageHistoryTO.setRequestNumber(objAutomaticSendType.getNumberRequest());
			objLipMessageHistoryTO.setAccountNumber(objAutomaticSendType.getOriginBankAccountCode());
			objLipMessageHistoryTO.setCurrency(currency);
			objLipMessageHistoryTO.setOperationType(strOperationType);
			objLipMessageHistoryTO.setOperationDate(objAutomaticSendType.getDate());
			
			objLipMessageHistoryTO.setDescription(objAutomaticSendType.getMnemonicParticipant());
			
//			if(StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E02, strOperationType)){	
//				objLipMessageHistoryTO.setDescription(objAutomaticSendType.getMnemonicParticipant());	
//			} else if(StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E73, strOperationType)){
//				
//			} else {
//				objLipMessageHistoryTO.setDescription(objAutomaticSendType.getTextDescription());
//			}							
			List<LipMessageHistory> lstLipMessageHistory = manageDepositServiceBean.getLipMessageHistoryValidate(objLipMessageHistoryTO);					
			if(Validations.validateListIsNullOrEmpty(lstLipMessageHistory)){										
				
				// GENERAR OPERACIONES DE DEPOSITO SI ES OPERACION : E02 E73 M12T
				
				if(StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E02, strOperationType)
						|| StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E73, strOperationType)
						// issue 1313: Para a recepcion de fondos se incluye tambien la operacion Debito Automatico
						|| StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_M12T, strOperationType)){	
					
					objFundsOperationOrigen = saveFundOperation(objAutomaticSendType, currency, null);		
					
					boolean isRelated = isRelatedCentral(objFundsOperationOrigen.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					
					if(isRelated){
						
						InstitutionCashAccount insCashAccount = manageDepositServiceBean.getCashAccountForCentralAccount(objFundsOperationOrigen.getCurrency());
						FundsOperation fundsOperationCentral = saveFundOperation(objAutomaticSendType, currency, insCashAccount);						
						fundsOperationCentral.setRegistryUser(loggerUser.getUserName());
						fundsOperationCentral.setRegistryDate(CommonsUtilities.currentDateTime());						
							
						manageDepositServiceBean.create(fundsOperationCentral);
						
						executeComponenteService(fundsOperationCentral, BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_DEPOSIT_PROCESS.getCode());
						
						objFundsOperationOrigen.setFundsOperation(fundsOperationCentral);
						objAutomaticSendType.setIndRecepFunds(ComponentConstant.ONE);
					}
					
					if(StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E73, strOperationType)){
						// verificando que operacion se va a ejecutar Operacion E73
						// buscando objeto institution_bank_account por 
						// codigo de participante, moneda y estado
						Map<String, Object> mapaParametros = new HashMap<String, Object>();
						mapaParametros.put("idParticipantPk", participantSearch == null ? null : participantSearch.getIdParticipantPk());
						mapaParametros.put("currency", currency);
						mapaParametros.put("state", BankAccountsStateType.REGISTERED.getCode());				
							
						try {
							institutionBankAccountSearch = manageBankAccountsService.findObjectByNamedQuery(InstitutionBankAccount.INSTITUTION_BANK_ACCOUNT_FOR_ID_PARTICIPANT_CURRENCY_STATE, mapaParametros);	
						} catch (NoResultException e) {
						} catch (EJBException e) {
						} 
						
						if(institutionBankAccountSearch != null){
							// seteando institutionBancAccount encontrado
							objFundsOperationOrigen.setInstitutionBankAccount(institutionBankAccountSearch);
						}
					}
															
					objFundsOperationOrigen.setRegistryUser(loggerUser.getUserName());
					objFundsOperationOrigen.setRegistryDate(CommonsUtilities.currentDateTime());
					
					manageDepositServiceBean.create(objFundsOperationOrigen);
					
					executeComponenteService(objFundsOperationOrigen, BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_DEPOSIT_PROCESS.getCode());			
				}
				
				// GRABAR OPOERACIONES E02 Y E03, E73
				
				saveLipMessageHistory(objAutomaticSendType, currency);
				
			} else {
				
				LipMessageHistory objLipMessageHistory = lstLipMessageHistory.get(0);
				
				if(ComponentConstant.ZERO.equals(objLipMessageHistory.getIndRecepFunds())){
					
				}
			}
			
		} catch (Exception e) {
			throw e;
		}		
	}
	
	/**
	 * Save fund operation.
	 *
	 * @param objAutomaticSendType the obj automatic send type
	 * @param currency the currency
	 * @param instCashAccount the inst cash account
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	private FundsOperation saveFundOperation(AutomaticSendType objAutomaticSendType, Integer currency, InstitutionCashAccount instCashAccount) throws ServiceException{		
		FundsOperation fundOperation = new FundsOperation();		
		BigDecimal bgAmountTransaction = BigDecimal.ZERO;
		
		// OBTENER TIPO DE OPERACION
		
		String strOperationType = objAutomaticSendType.getCodTypeOperation();
		
		if(Validations.validateIsNotNullAndNotEmpty(strOperationType)){
			
			// OBTENER LA MONEDA DE LA TRANSFERENCIA
			
			fundOperation.setCurrency(currency);			
			
			// OBTENER CUENTA EFECTIVO Y CUENTA BANCARIA
			
			InstitutionCashAccount objInstitutionCashAccount = new InstitutionCashAccount();
			Participant objParticipant = null;
			if(Validations.validateIsNullOrEmpty(instCashAccount)){				
				InstitutionCashAccountTO objInstitutionCashAccountTO = new InstitutionCashAccountTO();			
				//objInstitutionCashAccountTO.setIdBcbCode(new Long(objAutomaticSendType.getOriginParticipantCode()));								
				if(Validations.validateIsNotNullAndNotEmpty(objAutomaticSendType.getMnemonicParticipant())){
					objParticipant = new Participant();
					objParticipant.setMnemonic(objAutomaticSendType.getMnemonicParticipant().trim());
					objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
				} else {
					throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_RECEPTION_PARTICIPANT);
				}
				
				if(objParticipant == null){
					throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_RECEPTION_PARTICIPANT);
				}
				
				objInstitutionCashAccountTO.setIdParticipantFk(objParticipant.getIdParticipantPk());			
				objInstitutionCashAccountTO.setCurrency(fundOperation.getCurrency());				
				List<InstitutionCashAccount> lstInstitutionCashAccount = getInstitutionCashAccountInformation(objInstitutionCashAccountTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionCashAccount)){
					objInstitutionCashAccount = lstInstitutionCashAccount.get(0);
				} else {
					throw new ServiceException(ErrorServiceType.SPECIAL_PAYMENT_CASH_ACCOUNT_NOT_EXIST);
				}
				boolean isRelated = isRelatedCentral(objInstitutionCashAccount.getIdInstitutionCashAccountPk());
				if(isRelated){
					if(!isDaysIsOpen())
						throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
				}
				fundOperation.setParticipant(find(Participant.class, objParticipant.getIdParticipantPk()));
				fundOperation.setIndCentralized(BooleanType.NO.getCode());
			} else {
				objInstitutionCashAccount = instCashAccount;
				fundOperation.setIndCentralized(BooleanType.YES.getCode());
			}																	
			fundOperation.setInstitutionCashAccount(objInstitutionCashAccount);			
			fundOperation.setFundsOperationGroup(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
			fundOperation.setFundsOperationClass(OperationClassType.RECEPTION_FUND.getCode());	
			fundOperation.setIndAutomatic(BooleanType.YES.getCode());
			
			// OBTENER EL MONTO DE TRANSACCION
			
			if(StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E02, strOperationType) ||
					StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E73, strOperationType)
					|| StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_M12T, strOperationType)
					){
				
				// MONTO DE ABONO
				
				bgAmountTransaction = objAutomaticSendType.getAmount();
				
			} else if(StringUtils.equalsIgnoreCase(ComponentConstant.LIP_BCB_OPERATION_TYPE_E03, strOperationType)){
				
				// MONTO DE RETIRO
				
				bgAmountTransaction = objAutomaticSendType.getRetirementAmount();
				
			}
			fundOperation.setOperationAmount(bgAmountTransaction);
			fundOperation.setFundsOperationType(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_FUND_DEPOSITS.getCode());			
			fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
			fundOperation.setSenderBicCode(objAutomaticSendType.getTargetParticipantCode());
			Integer nextOperationNumber = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), fundOperation.getFundsOperationGroup());
			fundOperation.setFundOperationNumber(nextOperationNumber);	
			if(objParticipant != null){
				fundOperation.setPaymentReference(objParticipant.getMnemonic());
			} else {
				fundOperation.setPaymentReference(objAutomaticSendType.getMnemonicParticipant());
			}
		}		
		return fundOperation;	
	}	
	
	/**
	 * Save lip message history.
	 *
	 * @param objAutomaticSendType the obj automatic send type
	 * @param currency the currency
	 * @throws ServiceException the service exception
	 */
	public void saveLipMessageHistory(AutomaticSendType objAutomaticSendType, Integer currency) throws ServiceException{
		LipMessageHistory objLipMessageHistory = new LipMessageHistory();
		objLipMessageHistory.setAccountNumber(objAutomaticSendType.getOriginBankAccountCode());
		objLipMessageHistory.setCurrency(currency);		
		objLipMessageHistory.setParticipantSourceCode(new Long(objAutomaticSendType.getOriginParticipantCode()));
		objLipMessageHistory.setDepartamentCodeSource(objAutomaticSendType.getOriginDepartment());
		objLipMessageHistory.setPartSourceDescription(objAutomaticSendType.getOriginParticipantDesc());		
		objLipMessageHistory.setParticipantTargetCode(new Long(objAutomaticSendType.getTargetParticipantCode()));
		objLipMessageHistory.setDepartamentCodeTarget(objAutomaticSendType.getTargetDepartment());
		objLipMessageHistory.setPartTargetDescription(objAutomaticSendType.getTargetParticipantDesc());			
		objLipMessageHistory.setOperationDate(objAutomaticSendType.getDate());
		objLipMessageHistory.setOperationType(objAutomaticSendType.getCodTypeOperation());
		objLipMessageHistory.setOperTypeDescription(objAutomaticSendType.getDescTypeOperation());
		objLipMessageHistory.setRequestNumber(objAutomaticSendType.getNumberRequest());
		objLipMessageHistory.setIndRecepFunds(objAutomaticSendType.getIndRecepFunds());
		objLipMessageHistory.setOperationHour(objAutomaticSendType.getHour());
		if(ComponentConstant.LIP_BCB_OPERATION_TYPE_E02.compareTo(objLipMessageHistory.getOperationType()) == 0
				|| ComponentConstant.LIP_BCB_OPERATION_TYPE_E73.compareTo(objLipMessageHistory.getOperationType()) == 0
				|| ComponentConstant.LIP_BCB_OPERATION_TYPE_M12T.compareTo(objLipMessageHistory.getOperationType()) == 0){						
			objLipMessageHistory.setTransactionAmount(objAutomaticSendType.getAmount());		
			objLipMessageHistory.setDescription(objAutomaticSendType.getMnemonicParticipant());
		} else if(ComponentConstant.LIP_BCB_OPERATION_TYPE_E03.compareTo(objLipMessageHistory.getOperationType()) == 0){			
			objLipMessageHistory.setTransactionAmount(objAutomaticSendType.getRetirementAmount());		
			objLipMessageHistory.setDescription(objAutomaticSendType.getTextDescription());
		}		
		manageDepositServiceBean.create(objLipMessageHistory);
	}

}
