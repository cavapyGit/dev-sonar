package com.pradera.settlements.core.view;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class MonitoringAmountsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/12/2013
 */
public class MonitoringAmountsTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The total amount. */
	private BigDecimal totalAmount;
	
	/** The pendient amount. */
	private BigDecimal pendientAmount;
	
	/** The settlem amount. */
	private BigDecimal settlemAmount;
	
	/** The remov amount. */
	private BigDecimal removAmount;
	
	/** The canceled amount. */
	private BigDecimal canceledAmount;

	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * Sets the total amount.
	 *
	 * @param totalAmount the new total amount
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * Gets the pendient amount.
	 *
	 * @return the pendient amount
	 */
	public BigDecimal getPendientAmount() {
		return pendientAmount;
	}

	/**
	 * Sets the pendient amount.
	 *
	 * @param pendientAmount the new pendient amount
	 */
	public void setPendientAmount(BigDecimal pendientAmount) {
		this.pendientAmount = pendientAmount;
	}

	/**
	 * Gets the settlem amount.
	 *
	 * @return the settlem amount
	 */
	public BigDecimal getSettlemAmount() {
		return settlemAmount;
	}

	/**
	 * Sets the settlem amount.
	 *
	 * @param settlemAmount the new settlem amount
	 */
	public void setSettlemAmount(BigDecimal settlemAmount) {
		this.settlemAmount = settlemAmount;
	}

	/**
	 * Gets the remov amount.
	 *
	 * @return the remov amount
	 */
	public BigDecimal getRemovAmount() {
		return removAmount;
	}

	/**
	 * Sets the remov amount.
	 *
	 * @param removAmount the new remov amount
	 */
	public void setRemovAmount(BigDecimal removAmount) {
		this.removAmount = removAmount;
	}

	/**
	 * Gets the canceled amount.
	 *
	 * @return the canceled amount
	 */
	public BigDecimal getCanceledAmount() {
		return canceledAmount;
	}

	/**
	 * Sets the canceled amount.
	 *
	 * @param canceledAmount the new canceled amount
	 */
	public void setCanceledAmount(BigDecimal canceledAmount) {
		this.canceledAmount = canceledAmount;
	}
}