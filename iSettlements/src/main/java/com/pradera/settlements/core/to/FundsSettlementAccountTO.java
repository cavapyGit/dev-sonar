package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FundsSettlementAccountTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class FundsSettlementAccountTO implements Serializable {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id settlement account. */
	private Long idSettlementAccount;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The settled quantity. */
	private BigDecimal settledQuantity;
	
	/** The total quantity. */
	private BigDecimal totalQuantity;
	
	/** The role. */
	private Integer role;
	
	/** The id holder account operation. */
	private Long idHolderAccountOperation;
	
	
	/**
	 * Gets the id settlement account.
	 *
	 * @return the id settlement account
	 */
	public Long getIdSettlementAccount() {
		return idSettlementAccount;
	}

	/**
	 * Sets the id settlement account.
	 *
	 * @param idSettlementAccount the new id settlement account
	 */
	public void setIdSettlementAccount(Long idSettlementAccount) {
		this.idSettlementAccount = idSettlementAccount;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the settled quantity.
	 *
	 * @return the settled quantity
	 */
	public BigDecimal getSettledQuantity() {
		return settledQuantity;
	}

	/**
	 * Sets the settled quantity.
	 *
	 * @param settledQuantity the new settled quantity
	 */
	public void setSettledQuantity(BigDecimal settledQuantity) {
		this.settledQuantity = settledQuantity;
	}

	/**
	 * Gets the total quantity.
	 *
	 * @return the total quantity
	 */
	public BigDecimal getTotalQuantity() {
		return totalQuantity;
	}

	/**
	 * Sets the total quantity.
	 *
	 * @param totalQuantity the new total quantity
	 */
	public void setTotalQuantity(BigDecimal totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the id holder account operation.
	 *
	 * @return the id holder account operation
	 */
	public Long getIdHolderAccountOperation() {
		return idHolderAccountOperation;
	}

	/**
	 * Sets the id holder account operation.
	 *
	 * @param idHolderAccountOperation the new id holder account operation
	 */
	public void setIdHolderAccountOperation(Long idHolderAccountOperation) {
		this.idHolderAccountOperation = idHolderAccountOperation;
	}
	
	
	
}
