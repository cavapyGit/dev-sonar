/*
 * 
 */
package com.pradera.settlements.core.view;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class MonitorSettlementAttributes.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/09/2013
 */
public class ModalitySettlementTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The settled operations. */
	private Integer settledOperations;
	
	/** The pending operations. */
	private Integer pendingOperations;
	
	/** The total operations. */
	private Integer totalOperations;
	
	/** The cancelled operations. */
	private Integer cancelledOperations;
	
	/** The removed operations. */
	private Integer removedOperations;
	
	/** The lst pending operations. */
	private List<SettlementOperation> lstPendingOperations;
	
	/** The lst settled operations. */
	private List<SettlementOperation> lstSettledOperations;
	
	/** The lst total operations. */
	private List<SettlementOperation> lstTotalOperations;
	
	/** The lst cancelled operations. */
	private List<SettlementOperation> lstCancelledOperations;
	
	/** The lst removed operations. */
	private List<SettlementOperation> lstRemovedOperations;
	
	/** The mechanism id. */
	private Long mechanismId;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The settlement scheme id. */
	private Integer settlementSchemeId;
	
	/** The settlement scheme name. */
	private String settlementSchemeName;
	
	/** The modality group id. */
	private Long modalityGroupId;
	
	/** The modality group name. */
	private String modalityGroupName;
	
	/** The currency. */
	private Integer currency;
	
	/** The id settlement process. */
	private Long idSettlementProcess;
	
	/** The id participant lib. */
	private Long idParticipantLIB;
	
	/** The sett process list. */
	private List<SettlementProcess> settProcessList;
	
	/** The net process. */
	private boolean netProcess;
	
	/**
	 * Gets the settled operations.
	 *
	 * @return the settled operations
	 */
	public Integer getSettledOperations() {
		return settledOperations;
	}

	/**
	 * Sets the settled operations.
	 *
	 * @param settledOperations the new settled operations
	 */
	public void setSettledOperations(Integer settledOperations) {
		this.settledOperations = settledOperations;
	}

	/**
	 * Gets the pending operations.
	 *
	 * @return the pending operations
	 */
	public Integer getPendingOperations() {
		return pendingOperations;
	}

	/**
	 * Sets the pending operations.
	 *
	 * @param pendingOperations the new pending operations
	 */
	public void setPendingOperations(Integer pendingOperations) {
		this.pendingOperations = pendingOperations;
	}

	/**
	 * Gets the total operations.
	 *
	 * @return the total operations
	 */
	public Integer getTotalOperations() {
		return totalOperations;
	}

	/**
	 * Sets the total operations.
	 *
	 * @param totalOperations the new total operations
	 */
	public void setTotalOperations(Integer totalOperations) {
		this.totalOperations = totalOperations;
	}

	/**
	 * Gets the cancelled operations.
	 *
	 * @return the cancelled operations
	 */
	public Integer getCancelledOperations() {
		return cancelledOperations;
	}

	/**
	 * Sets the cancelled operations.
	 *
	 * @param cancelledOperations the new cancelled operations
	 */
	public void setCancelledOperations(Integer cancelledOperations) {
		this.cancelledOperations = cancelledOperations;
	}

	/**
	 * Gets the removed operations.
	 *
	 * @return the removed operations
	 */
	public Integer getRemovedOperations() {
		return removedOperations;
	}

	/**
	 * Sets the removed operations.
	 *
	 * @param removedOperations the new removed operations
	 */
	public void setRemovedOperations(Integer removedOperations) {
		this.removedOperations = removedOperations;
	}

	/**
	 * Gets the lst removed operations.
	 *
	 * @return the lst removed operations
	 */
	public List<SettlementOperation> getLstRemovedOperations() {
		return lstRemovedOperations;
	}

	/**
	 * Sets the lst removed operations.
	 *
	 * @param lstRemovedOperations the new lst removed operations
	 */
	public void setLstRemovedOperations(
			List<SettlementOperation> lstRemovedOperations) {
		this.lstRemovedOperations = lstRemovedOperations;
	}

	/**
	 * Gets the lst pending operations.
	 *
	 * @return the lst pending operations
	 */
	public List<SettlementOperation> getLstPendingOperations() {
		return lstPendingOperations;
	}

	/**
	 * Sets the lst pending operations.
	 *
	 * @param lstPendingOperations the new lst pending operations
	 */
	public void setLstPendingOperations(
			List<SettlementOperation> lstPendingOperations) {
		this.lstPendingOperations = lstPendingOperations;
	}

	/**
	 * Gets the lst settled operations.
	 *
	 * @return the lst settled operations
	 */
	public List<SettlementOperation> getLstSettledOperations() {
		return lstSettledOperations;
	}

	/**
	 * Sets the lst settled operations.
	 *
	 * @param lstSettledOperations the new lst settled operations
	 */
	public void setLstSettledOperations(
			List<SettlementOperation> lstSettledOperations) {
		this.lstSettledOperations = lstSettledOperations;
	}

	/**
	 * Gets the lst total operations.
	 *
	 * @return the lst total operations
	 */
	public List<SettlementOperation> getLstTotalOperations() {
		return lstTotalOperations;
	}

	/**
	 * Sets the lst total operations.
	 *
	 * @param lstTotalOperations the new lst total operations
	 */
	public void setLstTotalOperations(List<SettlementOperation> lstTotalOperations) {
		this.lstTotalOperations = lstTotalOperations;
	}

	/**
	 * Gets the lst cancelled operations.
	 *
	 * @return the lst cancelled operations
	 */
	public List<SettlementOperation> getLstCancelledOperations() {
		return lstCancelledOperations;
	}

	/**
	 * Sets the lst cancelled operations.
	 *
	 * @param lstCancelledOperations the new lst cancelled operations
	 */
	public void setLstCancelledOperations(
			List<SettlementOperation> lstCancelledOperations) {
		this.lstCancelledOperations = lstCancelledOperations;
	}

	/**
	 * Gets the mechanism id.
	 *
	 * @return the mechanism id
	 */
	public Long getMechanismId() {
		return mechanismId;
	}

	/**
	 * Sets the mechanism id.
	 *
	 * @param mechanismId the new mechanism id
	 */
	public void setMechanismId(Long mechanismId) {
		this.mechanismId = mechanismId;
	}

	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}

	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}

	/**
	 * Gets the settlement scheme id.
	 *
	 * @return the settlement scheme id
	 */
	public Integer getSettlementSchemeId() {
		return settlementSchemeId;
	}

	/**
	 * Sets the settlement scheme id.
	 *
	 * @param settlementSchemeId the new settlement scheme id
	 */
	public void setSettlementSchemeId(Integer settlementSchemeId) {
		this.settlementSchemeId = settlementSchemeId;
	}

	/**
	 * Gets the settlement scheme name.
	 *
	 * @return the settlement scheme name
	 */
	public String getSettlementSchemeName() {
		return settlementSchemeName;
	}

	/**
	 * Sets the settlement scheme name.
	 *
	 * @param settlementSchemeName the new settlement scheme name
	 */
	public void setSettlementSchemeName(String settlementSchemeName) {
		this.settlementSchemeName = settlementSchemeName;
	}

	/**
	 * Gets the modality group id.
	 *
	 * @return the modality group id
	 */
	public Long getModalityGroupId() {
		return modalityGroupId;
	}

	/**
	 * Sets the modality group id.
	 *
	 * @param modalityGroupId the new modality group id
	 */
	public void setModalityGroupId(Long modalityGroupId) {
		this.modalityGroupId = modalityGroupId;
	}

	/**
	 * Gets the modality group name.
	 *
	 * @return the modality group name
	 */
	public String getModalityGroupName() {
		return modalityGroupName;
	}

	/**
	 * Sets the modality group name.
	 *
	 * @param modalityGroupName the new modality group name
	 */
	public void setModalityGroupName(String modalityGroupName) {
		this.modalityGroupName = modalityGroupName;
	}

	/**
	 * Instantiates a new modality settlement to.
	 */
	public ModalitySettlementTO() {
		super();

	}

	/**
	 * Gets the sett process list.
	 *
	 * @return the sett process list
	 */
	public List<SettlementProcess> getSettProcessList() {
		return settProcessList;
	}
	/**
	 * Sets the sett process list.
	 *
	 * @param settProcessList the new sett process list
	 */
	public void setSettProcessList(List<SettlementProcess> settProcessList) {
		this.settProcessList = settProcessList;
	}

	/**
	 * Checks if is net process.
	 *
	 * @return true, if is net process
	 */
	public boolean isNetProcess() {
		return netProcess;
	}

	/**
	 * Sets the net process.
	 *
	 * @param netProcess the new net process
	 */
	public void setNetProcess(boolean netProcess) {
		this.netProcess = netProcess;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the id settlement process.
	 *
	 * @return the id settlement process
	 */
	public Long getIdSettlementProcess() {
		return idSettlementProcess;
	}

	/**
	 * Sets the id settlement process.
	 *
	 * @param idSettlementProcess the new id settlement process
	 */
	public void setIdSettlementProcess(Long idSettlementProcess) {
		this.idSettlementProcess = idSettlementProcess;
	}

	/**
	 * Gets the id participant lib.
	 *
	 * @return the id participant lib
	 */
	public Long getIdParticipantLIB() {
		return idParticipantLIB;
	}

	/**
	 * Sets the id participant lib.
	 *
	 * @param idParticipantLIB the new id participant lib
	 */
	public void setIdParticipantLIB(Long idParticipantLIB) {
		this.idParticipantLIB = idParticipantLIB;
	}
	
}