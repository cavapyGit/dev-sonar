package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FundsParticipantSettlementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class FundsParticipantSettlementTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant settlement. */
	private Long idParticipantSettlement;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The Participant description. */
	private String ParticipantDescription;
	
	/** The Participant nemonic. */
	private String ParticipantNemonic;
	
	/** The role. */
	private Integer role;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The settlement amount. */
	private BigDecimal settlementAmount;
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return ParticipantDescription;
	}
	
	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		ParticipantDescription = participantDescription;
	}
	
	/**
	 * Gets the participant nemonic.
	 *
	 * @return the participant nemonic
	 */
	public String getParticipantNemonic() {
		return ParticipantNemonic;
	}
	
	/**
	 * Sets the participant nemonic.
	 *
	 * @param participantNemonic the new participant nemonic
	 */
	public void setParticipantNemonic(String participantNemonic) {
		ParticipantNemonic = participantNemonic;
	}
	
	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}
	
	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}
	
	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	
	/**
	 * Gets the id participant settlement.
	 *
	 * @return the id participant settlement
	 */
	public Long getIdParticipantSettlement() {
		return idParticipantSettlement;
	}
	
	/**
	 * Sets the id participant settlement.
	 *
	 * @param idParticipantSettlement the new id participant settlement
	 */
	public void setIdParticipantSettlement(Long idParticipantSettlement) {
		this.idParticipantSettlement = idParticipantSettlement;
	}
	
}
