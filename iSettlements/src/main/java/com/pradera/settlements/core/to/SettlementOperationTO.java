package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.integration.component.business.ComponentConstant;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The id referenced operation. */
	private Long idReferencedOperation;
	
	/** The id mechanism. */
	private Long idMechanism;
	
	/** The id modality group. */
	private Long idModalityGroup;
	
	/** The id modality. */
	private Long idModality;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The settlement type. */
	private Integer settlementType;
	
	/** The settlement schema. */
	private Integer settlementSchema;
	
	/** The currency. */
	private Integer currency;
	
	/** The operation amount. */
	private BigDecimal operationAmount;
	
	/** The ind reporting balance. */
	private Integer indReportingBalance;
	
	/** The ind principal guarantee. */
	private Integer indPrincipalGuarantee;
	
	/** The ind margin guarantee. */
	private Integer indMarginGuarantee;
	
	/** The ind term settlement. */
	private Integer indTermSettlement;
	
	/** The ind cash stock block. */
	private Integer indCashStockBlock;
	
	/** The ind term stock block. */
	private Integer indTermStockBlock;
	
	/** The ind primary placement. */
	private Integer indPrimaryPlacement;
	
	/** The ind dematerialization. */
	private Integer indDematerialization;
	
	/** The current nominal value. */
	private BigDecimal currentNominalValue;
	
	/** The operation part. */
	private Integer operationPart;
	
	/** The ind partial. */
	private Integer indPartial;
	
	/** The ind forced purchase. */
	private Integer indForcedPurchase;
	
	/** The interface transfer code. */
	private String interfaceTransferCode;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The lst buyer holder accounts. */
	private List<SettlementHolderAccountTO> lstBuyerHolderAccounts;
	
	/** The lst seller holder accounts. */
	private List<SettlementHolderAccountTO> lstSellerHolderAccounts;
	
	/** The ind complete settlement. */
	private Integer indCompleteSettlement = ComponentConstant.ZERO;
	
	/**
	 * Instantiates a new settlement operation to.
	 */
	public SettlementOperationTO() {
		super();
		indDematerialization = ComponentConstant.ZERO;
	}

	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}

	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idOperation) {
		this.idMechanismOperation = idOperation;
	}

	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return idMechanism;
	}

	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}

	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}

	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	/**
	 * Gets the settlement type.
	 *
	 * @return the settlement type
	 */
	public Integer getSettlementType() {
		return settlementType;
	}

	/**
	 * Sets the settlement type.
	 *
	 * @param settlementType the new settlement type
	 */
	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public Integer getSettlementSchema() {
		return settlementSchema;
	}

	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the new settlement schema
	 */
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	/**
	 * Gets the lst buyer holder accounts.
	 *
	 * @return the lst buyer holder accounts
	 */
	public List<SettlementHolderAccountTO> getLstBuyerHolderAccounts() {
		return lstBuyerHolderAccounts;
	}

	/**
	 * Sets the lst buyer holder accounts.
	 *
	 * @param lstBuyerHolderAccounts the new lst buyer holder accounts
	 */
	public void setLstBuyerHolderAccounts(
			List<SettlementHolderAccountTO> lstBuyerHolderAccounts) {
		this.lstBuyerHolderAccounts = lstBuyerHolderAccounts;
	}

	/**
	 * Gets the lst seller holder accounts.
	 *
	 * @return the lst seller holder accounts
	 */
	public List<SettlementHolderAccountTO> getLstSellerHolderAccounts() {
		return lstSellerHolderAccounts;
	}

	/**
	 * Sets the lst seller holder accounts.
	 *
	 * @param lstSellerHolderAccounts the new lst seller holder accounts
	 */
	public void setLstSellerHolderAccounts(
			List<SettlementHolderAccountTO> lstSellerHolderAccounts) {
		this.lstSellerHolderAccounts = lstSellerHolderAccounts;
	}

	/**
	 * Gets the operation amount.
	 *
	 * @return the operation amount
	 */
	public BigDecimal getOperationAmount() {
		return operationAmount;
	}

	/**
	 * Sets the operation amount.
	 *
	 * @param operationAmount the new operation amount
	 */
	public void setOperationAmount(BigDecimal operationAmount) {
		this.operationAmount = operationAmount;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the id modality group.
	 *
	 * @return the id modality group
	 */
	public Long getIdModalityGroup() {
		return idModalityGroup;
	}

	/**
	 * Sets the id modality group.
	 *
	 * @param idModalityGroup the new id modality group
	 */
	public void setIdModalityGroup(Long idModalityGroup) {
		this.idModalityGroup = idModalityGroup;
	}

	/**
	 * Gets the id referenced operation.
	 *
	 * @return the id referenced operation
	 */
	public Long getIdReferencedOperation() {
		return idReferencedOperation;
	}

	/**
	 * Sets the id referenced operation.
	 *
	 * @param idReferencedOperation the new id referenced operation
	 */
	public void setIdReferencedOperation(Long idReferencedOperation) {
		this.idReferencedOperation = idReferencedOperation;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the ind reporting balance.
	 *
	 * @return the ind reporting balance
	 */
	public Integer getIndReportingBalance() {
		return indReportingBalance;
	}

	/**
	 * Sets the ind reporting balance.
	 *
	 * @param indReportingBalance the new ind reporting balance
	 */
	public void setIndReportingBalance(Integer indReportingBalance) {
		this.indReportingBalance = indReportingBalance;
	}

	/**
	 * Gets the ind principal guarantee.
	 *
	 * @return the ind principal guarantee
	 */
	public Integer getIndPrincipalGuarantee() {
		return indPrincipalGuarantee;
	}

	/**
	 * Sets the ind principal guarantee.
	 *
	 * @param indPrincipalGuarantee the new ind principal guarantee
	 */
	public void setIndPrincipalGuarantee(Integer indPrincipalGuarantee) {
		this.indPrincipalGuarantee = indPrincipalGuarantee;
	}

	/**
	 * Gets the ind margin guarantee.
	 *
	 * @return the ind margin guarantee
	 */
	public Integer getIndMarginGuarantee() {
		return indMarginGuarantee;
	}

	/**
	 * Sets the ind margin guarantee.
	 *
	 * @param indMarginGuarantee the new ind margin guarantee
	 */
	public void setIndMarginGuarantee(Integer indMarginGuarantee) {
		this.indMarginGuarantee = indMarginGuarantee;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the ind term settlement.
	 *
	 * @return the ind term settlement
	 */
	public Integer getIndTermSettlement() {
		return indTermSettlement;
	}

	/**
	 * Sets the ind term settlement.
	 *
	 * @param indTermSettlement the new ind term settlement
	 */
	public void setIndTermSettlement(Integer indTermSettlement) {
		this.indTermSettlement = indTermSettlement;
	}

	/**
	 * Gets the ind cash stock block.
	 *
	 * @return the ind cash stock block
	 */
	public Integer getIndCashStockBlock() {
		return indCashStockBlock;
	}

	/**
	 * Sets the ind cash stock block.
	 *
	 * @param indCashStockBlock the new ind cash stock block
	 */
	public void setIndCashStockBlock(Integer indCashStockBlock) {
		this.indCashStockBlock = indCashStockBlock;
	}

	/**
	 * Gets the ind term stock block.
	 *
	 * @return the ind term stock block
	 */
	public Integer getIndTermStockBlock() {
		return indTermStockBlock;
	}

	/**
	 * Sets the ind term stock block.
	 *
	 * @param indTermStockBlock the new ind term stock block
	 */
	public void setIndTermStockBlock(Integer indTermStockBlock) {
		this.indTermStockBlock = indTermStockBlock;
	}

	/**
	 * Gets the ind primary placement.
	 *
	 * @return the ind primary placement
	 */
	public Integer getIndPrimaryPlacement() {
		return indPrimaryPlacement;
	}

	/**
	 * Sets the ind primary placement.
	 *
	 * @param indPrimaryPlacement the new ind primary placement
	 */
	public void setIndPrimaryPlacement(Integer indPrimaryPlacement) {
		this.indPrimaryPlacement = indPrimaryPlacement;
	}

	/**
	 * Gets the ind dematerialization.
	 *
	 * @return the ind dematerialization
	 */
	public Integer getIndDematerialization() {
		return indDematerialization;
	}

	/**
	 * Sets the ind dematerialization.
	 *
	 * @param indDematerialization the new ind dematerialization
	 */
	public void setIndDematerialization(Integer indDematerialization) {
		this.indDematerialization = indDematerialization;
	}

	/**
	 * Gets the current nominal value.
	 *
	 * @return the current nominal value
	 */
	public BigDecimal getCurrentNominalValue() {
		return currentNominalValue;
	}

	/**
	 * Sets the current nominal value.
	 *
	 * @param currentNominalValue the new current nominal value
	 */
	public void setCurrentNominalValue(BigDecimal currentNominalValue) {
		this.currentNominalValue = currentNominalValue;
	}

	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}

	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	/**
	 * Gets the id settlement operation.
	 *
	 * @return the id settlement operation
	 */
	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}

	/**
	 * Sets the id settlement operation.
	 *
	 * @param idSettlementOperation the new id settlement operation
	 */
	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}

	/**
	 * Gets the ind complete settlement.
	 *
	 * @return the ind complete settlement
	 */
	public Integer getIndCompleteSettlement() {
		return indCompleteSettlement;
	}

	/**
	 * Sets the ind complete settlement.
	 *
	 * @param indCompleteSettlement the new ind complete settlement
	 */
	public void setIndCompleteSettlement(Integer indCompleteSettlement) {
		this.indCompleteSettlement = indCompleteSettlement;
	}

	/**
	 * Gets the ind partial.
	 *
	 * @return the ind partial
	 */
	public Integer getIndPartial() {
		return indPartial;
	}

	/**
	 * Sets the ind partial.
	 *
	 * @param indPartial the new ind partial
	 */
	public void setIndPartial(Integer indPartial) {
		this.indPartial = indPartial;
	}

	/**
	 * Gets the ind forced purchase.
	 *
	 * @return the ind forced purchase
	 */
	public Integer getIndForcedPurchase() {
		return indForcedPurchase;
	}

	/**
	 * Sets the ind forced purchase.
	 *
	 * @param indForcedPurchase the new ind forced purchase
	 */
	public void setIndForcedPurchase(Integer indForcedPurchase) {
		this.indForcedPurchase = indForcedPurchase;
	}

	/**
	 * Gets the interface transfer code.
	 *
	 * @return the interface transfer code
	 */
	public String getInterfaceTransferCode() {
		return interfaceTransferCode;
	}

	/**
	 * Sets the interface transfer code.
	 *
	 * @param interfaceTransferCode the new interface transfer code
	 */
	public void setInterfaceTransferCode(String interfaceTransferCode) {
		this.interfaceTransferCode = interfaceTransferCode;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
}
