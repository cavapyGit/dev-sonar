package com.pradera.settlements.core.facade;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jboss.ejb3.annotation.TransactionTimeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.billing.service.BillingComponentService;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.funds.webservices.ServiceAPIClients;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.SalePurchaseUpdateComponentService;
import com.pradera.integration.component.business.to.AccountOperationTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.SettlementProcessType;
import com.pradera.negotiations.jaxb.dto.JuridicHolderXmlDTO;
import com.pradera.negotiations.jaxb.dto.LastHolderXmlDTO;
import com.pradera.negotiations.jaxb.dto.NaturalHolderXmlDTO;
import com.pradera.negotiations.jaxb.dto.NegotiationXmlDTO;
import com.pradera.negotiations.jaxb.dto.OperationTutXmlDTO;
import com.pradera.negotiations.jaxb.dto.SecurityXmlDTO;
import com.pradera.settlements.core.service.FundsComponentSingleton;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementProcessFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class SettlementProcessFacade {

	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(SettlementProcessFacade.class);
	
	@Inject
	private PraderaLogger log;
	
	/** The settlements component singleton. */
	@EJB
	private SettlementsComponentSingleton settlementsComponentSingleton;
	
	/** The funds component singleton. */
	@EJB
	private FundsComponentSingleton fundsComponentSingleton;
	
	/** The settlement process service bean. */
	@EJB
	private SettlementProcessService settlementProcessServiceBean;
	
	/** The notification service. */
	@EJB
	private NotificationServiceBean notificationService;
	
	@EJB
	private ServiceAPIClients serviceAPIClients;
	
	/** The report service. */
	@Inject
 	Instance<ReportGenerationService> reportService;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sale purchase update component service. */
	@Inject
	Instance<SalePurchaseUpdateComponentService> salePurchaseUpdateComponentService;
	
	/** The billing component service. */
	@EJB
	private BillingComponentService billingComponentService;

	/**
	 * obtiene todos los emisores de los valores negociados en una determinada fecha (contado y plazo)
	 * 
	 * @param date
	 * @return
	 * @throws ServiceException
	 */
	public List<String> lstIdAllIssuer() throws ServiceException {
		return settlementProcessServiceBean.lstIdAllIssuer();
	}
	
	/**
	 * issue 1310: obtiene los DPFs negociados (SIRTEX, OTC, BBV, CTT) de un determinado emisor y fecha (contado y plazo) 
	 * @param idIssuerPk
	 * @param date
	 * @return
	 * @throws ServiceException
	 */
	private List<Object[]> getLstInformationOfSecuritiesNegotiatedToSendIssuer(String idIssuerPk, Date date) throws ServiceException {
		
		List<Object[]> lst = settlementProcessServiceBean.lstSecuritiesNegotiatedAndSettled(idIssuerPk, date);
		List<String> lstSecuritiesFromHAM = settlementProcessServiceBean.lstSecurituesWithMovementTransferHolder(idIssuerPk, date);
		
		List<Object[]> lstAux = new ArrayList<>();
		
		String idSecurityPk;
		
		// caso 1: lst y lstSecuritiesFromHAM con data
		if(Validations.validateListIsNotNullAndNotEmpty(lst)
				&& Validations.validateListIsNotNullAndNotEmpty(lstSecuritiesFromHAM)){
			
			boolean isMatchSecurity=false;
			
			for (String securityFromHam : lstSecuritiesFromHAM) {
				for (Object[] row : lst) {
					idSecurityPk = row[1].toString();
					if (Validations.validateIsNotNullAndNotEmpty(idSecurityPk)) {
						if (securityFromHam.equals(idSecurityPk)) {
							isMatchSecurity = true;
							HolderAccountMovement ham = settlementProcessServiceBean.findHolderAccountMovementBySecurityAndDate(securityFromHam, date);
							MechanismOperation mo = settlementProcessServiceBean.findMechanismOperationBySecurityAndDate(idSecurityPk, date);
							// comparar el que se realizo ultimo
							if (Validations.validateIsNotNull(ham) && Validations.validateIsNotNull(ham.getMovementType()) && Validations.validateIsNotNull(ham.getMovementDate())
									&& Validations.validateIsNotNull(mo) && Validations.validateIsNotNull(mo.getLastModifyDate())) {
								// si el CTT (cambio de Titularidad) es mayor a la operacion MCN entonces ponemos los datos del CTT en lugar de MCN
								// si el MCN es mayor se deja tal cual la lista, no pasa nada
								if (ham.getMovementDate().compareTo(mo.getLastModifyDate()) > 0) {
									row[0] = null; // se asigna null porque CTT no pertenece aningun mecanismo de negociacion
									row[2] = "CTT";
								}
							}
							break;
						}
					}
				}
				// si NO hubo matcheo en las lista, entonces se adicona a la lista: lstAux
				if (!isMatchSecurity) {
					Object[] dataCTT;
					dataCTT = new Object[3];
					dataCTT[0] = null; // se asigna null porque CTT no pertenece aningun mecanismo de negociacion
					dataCTT[1] = securityFromHam;
					dataCTT[2] = "CTT"; // modalidad de negociacion (de acuerdo a la documentacion tecnica, metodo TUT)

					lstAux.add(dataCTT);
				}
				// colocando false en cada iteracion
				isMatchSecurity = false;
			}
			// si lstAux tiene data entonces lo adiconamos a lst para retornar la lista completa
			if(Validations.validateListIsNotNullAndNotEmpty(lstAux)){
				lst.addAll(lstAux);
			}
			return lst;
		}
		// caso 2: lst con data y lstSecuritiesFromHAM vacio
		else if(Validations.validateListIsNotNullAndNotEmpty(lst)
				&& Validations.validateListIsNullOrEmpty(lstSecuritiesFromHAM)){
			//lstAux = lst;
			return lst;
		}
		// caso 3: lst vacio y lstSecuritiesFromHAM con data
		else if(Validations.validateListIsNullOrEmpty(lst)
				&& Validations.validateListIsNotNullAndNotEmpty(lstSecuritiesFromHAM)){
			// asignando datos complementarios para retornar la lista final
			Object [] finalData;
			for(String idSecurity: lstSecuritiesFromHAM){
				finalData = new Object[3];
				finalData[0] = null; // se asigna null porque CTT no pertenece aningun mecanismo de negociacion
				finalData[1] = idSecurity;
				finalData[2] = "CTT"; // modalidad de negociacion (de acuerdo a la documentacion tecnica, metodo TUT)
				
				lstAux.add(finalData);
			}
			return lstAux;
		}
		// caso 4: se asume que ambas listas estan vacias o nulas, por lo tanto se retorna null
		else{
			return null;
		}
	}
	
	/**
	 * Construye el XML con informacion sobre los ultimos titutlares de los DPF emitidos por un EMISOR
	 * @param idIssuerPk
	 * @param date
	 * @return
	 * @throws ServiceException
	 * @throws JAXBException
	 */
	public OperationTutXmlDTO buildXmlToSendIssuer(String idIssuerPk, Date date) throws ServiceException, JAXBException{
		List<Object[]> lst = getLstInformationOfSecuritiesNegotiatedToSendIssuer(idIssuerPk, date);
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			
			Issuer iss = settlementProcessServiceBean.find(Issuer.class, idIssuerPk);
			
			// construyendo el xml
			OperationTutXmlDTO operationTut = new OperationTutXmlDTO();
			operationTut.setNumOperation(0L);
			operationTut.setOperationName("TUT");
			operationTut.setDateOPeration(CommonsUtilities.convertDatetoString(date));
			operationTut.setEEFF(iss.getMnemonic());
			
			List<NegotiationXmlDTO> lstNegotiation = new ArrayList<>();
			NegotiationXmlDTO negotiation;
			Long idNegotiationMechanismPk;
			String idSecurityPk;
			String operTrans="";
			Security security=null;
			for(Object[] row:lst){
				
				if(Validations.validateIsNotNull(row[0])){
					// es una operacion MCN
					idNegotiationMechanismPk = Long.parseLong(row[0].toString());
				}else{
					// es una operacion CTT
					idNegotiationMechanismPk = null;
				}
				idSecurityPk = row[1].toString();
				operTrans = row[2] == null ? "" : row[2].toString(); 
				
				if(Validations.validateIsNotNullAndNotEmpty(idSecurityPk)){
					security = settlementProcessServiceBean.find(Security.class, idSecurityPk);
				}else{
					// informar que no se pudo obtener el mecanismo de negociacion o el valor de esta fila
					log.warn("::: El mecanismo de negociacion o el vaor de la fila estan nulos");
					//return;
					continue;
				}
				
				// datos de la negociacion
				negotiation = new NegotiationXmlDTO();
				if(Validations.validateIsNotNullAndPositive(idNegotiationMechanismPk)){
					// es un MCN (podria ser BBV, SIRTEX, OTC)
					negotiation.setMecanismo(NegotiationMechanismType.get(idNegotiationMechanismPk).getDescripcion());
				}else{
					// caso especial al tratarse de un CTT
					negotiation.setMecanismo("CUSTODIA"); //Se coloca mecanismo: CUSTODIA de acuerdo a la documentacion tecnica, (ver metodo TUT)
				}
				negotiation.setOperTrans(operTrans);
				
				// datos del valor
				SecurityXmlDTO secXml = new SecurityXmlDTO();
				secXml.setSecurityClassText(SecurityClassType.get(security.getSecurityClass()).getText1());
				secXml.setSecurityOnlyCode(security.getIdSecurityCodeOnly());
				secXml.setCodeAlternate(security.getAlternativeCode() == null ? "":security.getAlternativeCode());
				negotiation.setSecurity(secXml);
				
				// Obtener datos de la cuenta
				List<HolderAccountDetail> lstHad = settlementProcessServiceBean.holderAccountDetailFullToBuildXmlLastHolder(security.getIdSecurityCodePk(), date);
				HolderAccount ha=null;
				ParameterTable ptHolderAccount=null;
				if(Validations.validateListIsNotNullAndNotEmpty(lstHad)){
					ha = lstHad.get(0).getHolderAccount();
					try {
						ptHolderAccount = settlementProcessServiceBean.find(ParameterTable.class, ha.getAccountType());
						if(Validations.validateIsNull(ptHolderAccount)
								|| Validations.validateIsNull(ptHolderAccount.getText1())){
							// informar que el se pudo encotrar el de PTable
							log.warn("::: No se pudo encontrar el ParameterTable con ID "+ha.getAccountType());
							//return;
							continue;
						}
					} catch (Exception e) {
						log.info("::: error al buscar ParameterTable con el ID "+ha.getAccountType()+" datalle: "+e.getMessage());
						continue;
					}
				}else{
					// informar de que no se pudo encontrar los titulares
					log.warn("::: No se pudo encontrar los titulares para el valor: "+security.getIdSecurityCodePk());
					continue;
				}
				
				JuridicHolderXmlDTO juridicHolder=null;
				List<NaturalHolderXmlDTO> lstNaturalHolder = null;;
				NaturalHolderXmlDTO naturalHolder;
				
				// determinando el tipo de titular a travez del HolderAccountType
				Integer indHolderType=null;
				if ( ha.getAccountType().equals(HolderAccountType.NATURAL.getCode())
					 || ha.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode()) ) {
					indHolderType = 1;

					// construyendo titular/es natural/es
					lstNaturalHolder = new ArrayList<>();
					for (HolderAccountDetail had : lstHad) {
						naturalHolder = new NaturalHolderXmlDTO();
						naturalHolder.setIdHolderPk(had.getHolder().getIdHolderPk());
						//naturalHolder.setAccountNumber(ha.getAccountNumber());
						naturalHolder.setDocumentNumber(had.getHolder().getDocumentNumber());
						naturalHolder.setFullName(had.getHolder().getFullName());
						lstNaturalHolder.add(naturalHolder);
					}

				} else if (ha.getAccountType().equals(HolderAccountType.JURIDIC.getCode())) {
					indHolderType = 2;
					// construyendo al titular juridico
					for (HolderAccountDetail had : lstHad) {
						juridicHolder = new JuridicHolderXmlDTO();
						juridicHolder.setIdHolderPk(had.getHolder().getIdHolderPk());
						//juridicHolder.setAccountNumber(ha.getAccountNumber());
						juridicHolder.setDocumentNumber(had.getHolder().getDocumentNumber());
						juridicHolder.setBusinessName(had.getHolder().getFullName());
						break;
					}
				}
				
				// datos del ultimo titular
				LastHolderXmlDTO lastHolder=new LastHolderXmlDTO();
				lastHolder.setAccountTypeText(ptHolderAccount.getText1());
				lastHolder.setHolderType(indHolderType);
				if( Validations.validateIsNotNull(indHolderType)
					&& indHolderType.intValue() ==1 ){
					// natural
					lastHolder.setLstNaturalHolder(lstNaturalHolder);
				}else if ( Validations.validateIsNotNull(indHolderType)
							&& indHolderType.intValue() == 2 ){
					// juridico
					lastHolder.setJuridicHolder(juridicHolder);
				}
				negotiation.setLastHolder(lastHolder);
				
				lstNegotiation.add(negotiation);
			}
			operationTut.setLstNegotiation(lstNegotiation);
			
			StringWriter xmltoSend = new StringWriter();
			JAXBContext contextObj = JAXBContext.newInstance(OperationTutXmlDTO.class);
			Marshaller marshallerObj = contextObj.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty("com.sun.xml.bind.xmlDeclaration", false);
			marshallerObj.marshal(operationTut, xmltoSend);
			log.info("::: XML para enviar:\n" + xmltoSend.toString());
			
			return operationTut;
		}else{
			log.info("No existen valores negociados para el emisor: "+idIssuerPk);
			return null;
		}
	}
	
	/**
	 * envio de la informacion al emisior por medio del BUS
	 * @param operationTutXmlDTO
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=180)
	public void sendInformationLastHolderDPFWebClient(OperationTutXmlDTO operationTutXmlDTO) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        settlementProcessServiceBean.sendInformationLastHolderDPFWebClient(operationTutXmlDTO, loggerUser);
	}
	
	/**
	 * Gets the modality group facade.
	 *
	 * @param modalityGroupId the modality group id
	 * @return the modality group facade
	 * @throws ServiceException the service exception
	 */
	public ModalityGroup getModalityGroupFacade(Long modalityGroupId) throws ServiceException{
		return settlementProcessServiceBean.getModalityGroup(modalityGroupId);
	}
	
	/**
	 * Gets the mechanism operation facade.
	 *
	 * @param mechanismOpeListId the mechanism ope list id
	 * @return the mechanism operation facade
	 * @throws ServiceException the service exception
	 */
	public List<MechanismOperation> getMechanismOperationFacade(List<Long> mechanismOpeListId) throws ServiceException{
		return settlementProcessServiceBean.getMechanismOperation(mechanismOpeListId);
	}
	
	/**
	 * Start gros settlement process.
	 *
	 * @param settlementDate the settlement date
	 * @param idUserAccount the id user account
	 * @throws ServiceException the service exception
	 */
	public void startGrosSettlementProcess(Date settlementDate, String idUserAccount) throws ServiceException
	{	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		try {
			
			Long idMechanism= null;
			Long idModalityGroup= null;
			Integer currency= null;
			Integer settlementSchema = SettlementSchemaType.GROSS.getCode();
			
			//settlementProcessServiceBean.removeOperationToSettlement(idMechanism, idModalityGroup, currency, settlementSchema, settlementDate,loggerUser);
			
			//we get the list of modality group detail to execute the settlement process for each mechanism and modality 
			List<Object[]> lstModalityGroups = settlementProcessServiceBean.getListModalityGroups(settlementSchema, settlementDate);
			
			if (Validations.validateListIsNotNullAndNotEmpty(lstModalityGroups))
			{
				//we execute the settlement process for each modality group
				for (Object[] arrObject: lstModalityGroups)
				{
					idModalityGroup = new Long(arrObject[0].toString());
					idMechanism = new Long(arrObject[1].toString());
					currency = new Integer(arrObject[2].toString());
					//we execute the settlement process for each currency
					boolean executeProcess= false; //flag to know if we must execute the batch process or not
					//we verify the existence of the settlement process
					SettlementProcess objSettlementProcess = settlementProcessServiceBean.getSettlementProcess(idModalityGroup, settlementDate, settlementSchema, currency);
					//we verify the state of the settlement process
					if (Validations.validateIsNotNull(objSettlementProcess) && 
						 SettlementProcessStateType.WAITING.getCode().equals(objSettlementProcess.getProcessState()))
					{
						//we can execute only the settlement process in WAITING state. It is PROCESSING now 
						executeProcess= true;
					} else if (Validations.validateIsNull(objSettlementProcess)) {
						//if does not exist the settlement process then we have to register it
						executeProcess= true;
					}
					Long idSettlementProcess= new Long(0);
					if (objSettlementProcess != null) {
						idSettlementProcess= objSettlementProcess.getIdSettlementProcessPk();
					}
					//we execute the batch process only if the current process has WAITING state or doesn't exist one
					if (executeProcess)
					{
						//now we have to execute the settlement process to GROSS settlement process
						executeGrossSettlementProcess(idMechanism, idModalityGroup, currency, settlementDate, idSettlementProcess, false, null, loggerUser, null);
					}
				}
			}
			
		} catch (ServiceException objException){
			logger.error(("startGrosSettlementProcess"));
			logger.debug(objException.getMessage());
			throw objException;
		}
				
	}

	/**
	 * Execute gross settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @param closeSettlementProcess the close settlement process
	 * @param scheduleType the schedule type
	 * @param operationId the operation id
	 * @throws ServiceException the service exception
	 */
	public void executeGrossSettlementProcess(Long idMechanism,
			Long idModalityGroup, Integer idCurrency, Date settlementDate, Long idSettlementProcess, boolean closeSettlementProcess, Integer scheduleType, Long operationId) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		executeGrossSettlementProcess(idMechanism, idModalityGroup, idCurrency, settlementDate, idSettlementProcess, closeSettlementProcess,scheduleType ,loggerUser,operationId);
	}
	

	/**
	 * Execute gross settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @param closeSettlementProcess the close settlement process
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @param operationId the operation id
	 * @throws ServiceException the service exception
	 */
	public void executeGrossSettlementProcess(Long idMechanism,
			Long idModalityGroup, Integer idCurrency, Date settlementDate,Long idSettlementProcess, 
			boolean closeSettlementProcess, Integer scheduleType, LoggerUser loggerUser, Long operationId) throws ServiceException {
		
		boolean continueProcess= true; //flag to identify if we continue with the process or skips the execution

		try {
			List<Long> pendingOperations= settlementProcessServiceBean.getCountPendingSettlementOperations(
					idMechanism, idModalityGroup, idCurrency, settlementDate, SettlementSchemaType.GROSS.getCode());
			
			Long countPending = new Long(pendingOperations.size());
			
			SettlementProcess objSettlementProcess= null;
			if (Validations.validateIsNotNullAndPositive(idSettlementProcess)) {
				objSettlementProcess = settlementProcessServiceBean.find(SettlementProcess.class, idSettlementProcess);
				if (SettlementProcessStateType.WAITING.getCode().equals(objSettlementProcess.getProcessState())) {
					//we can execute only the settlement process in WAITING state. It is PROCESSING now
					settlementProcessServiceBean.updateSettlementProcessStateTx(idSettlementProcess, SettlementProcessStateType.IN_PROCESS.getCode(), scheduleType, loggerUser, countPending);
				} else {
					continueProcess= false;
				}
			} else {
				//if does not exist the settlement process then we have to register it
				objSettlementProcess= settlementsComponentSingleton.saveSettlementProcessNewTx(idMechanism, idModalityGroup, idCurrency, settlementDate, 
																						 SettlementSchemaType.GROSS.getCode(), countPending, pendingOperations, 
																						 SettlementProcessType.PARTIAL.getCode(),scheduleType);
				idSettlementProcess= objSettlementProcess.getIdSettlementProcessPk();
			}
			
			if (continueProcess){
			
				settlementsComponentSingleton.executeGrossSettlementProcess(idMechanism, idModalityGroup, idCurrency, 
						settlementDate, idSettlementProcess, scheduleType, loggerUser, operationId);
				
				if (!closeSettlementProcess) {
					//we update the current settlement process to WAITING state
					settlementProcessServiceBean.updateSettlementProcessStateTx(idSettlementProcess, SettlementProcessStateType.WAITING.getCode(),null, loggerUser, null);
				} else {
					//we update the current settlement process to FINISHED state
					settlementProcessServiceBean.updateSettlementProcessStateTx(idSettlementProcess, SettlementProcessStateType.FINISHED.getCode(),null, loggerUser, null);
				}
			}
			
		} catch (Exception e) {
			if(e instanceof ServiceException){
				ServiceException se = (ServiceException)e;
				
				if(se.getErrorService()!=null){
					String exceptions = NegotiationUtils.concatenateParameters(se.getConcatenatedParams());
					BusinessProcess businessProcess = new BusinessProcess(BusinessProcessType.GROSS_SETTLEMENT_PROCESS_MANUAL_START.getCode(), null);
					notificationService.sendNotification(loggerUser, loggerUser.getUserName(), businessProcess, null, new Object[]{
						PropertiesUtilities.getExceptionMessage(se.getMessage(),se.getParams(),new Locale("es")),exceptions});
				}
			}
			
			settlementProcessServiceBean.updateSettlementProcessStateTx(idSettlementProcess, SettlementProcessStateType.WAITING.getCode(),null, loggerUser, null);
		}
		
	}
	
	/**
	 * Metodo para verificar la existencia de operaciones sin liquidar
	 * @return
	 * @throws ServiceException
	 */
	public Boolean verifyOperationExist(Date dateOPeration) throws ServiceException {
		
		return settlementProcessServiceBean.verifyOperationExist(dateOPeration);
	}
	
	/**
	 * Execute net settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @param scheduleType the schedule type
	 * @throws ServiceException the service exception
	 */
	public boolean executeNetSettlementProcess(Long idMechanism,
			Long idModalityGroup, Integer idCurrency, Date settlementDate, Long idSettlementProcess, Integer scheduleType) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		boolean blError = Boolean.FALSE;
		//we can execute only the settlement process in WAITING state. It is PROCESSING now
		settlementProcessServiceBean.updateSettlementProcessStateTx(idSettlementProcess, SettlementProcessStateType.IN_PROCESS.getCode(),null, loggerUser, null);
		
		try {
			
			settlementsComponentSingleton.executeNetSettlementProcess(idMechanism, settlementDate, idModalityGroup,idCurrency,  idSettlementProcess,loggerUser,scheduleType);
			
			logger.info("Metodh:executeNetSettlementProcess :::::::: sendSettlementFundsSendingProcessWebClient");
			//we send funds to creditor participant according the last net positions
			settlementProcessServiceBean.sendSettlementFundsSendingProcessWebClient(idSettlementProcess, idMechanism, idModalityGroup, idCurrency, loggerUser);
			
			// update settlement process state to finished
			settlementProcessServiceBean.updateSettlementProcessStateTx(idSettlementProcess, SettlementProcessStateType.FINISHED.getCode(),null, loggerUser, null);
			
		} catch (Exception e) {
			blError = Boolean.TRUE;
			if(e instanceof ServiceException){
				ServiceException se = (ServiceException)e;
				
				if(se.getErrorService()!=null){
					String exceptions = NegotiationUtils.concatenateParameters(se.getConcatenatedParams());
					BusinessProcess businessProcess = new BusinessProcess(BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode(), null);
					
					List<UserAccountSession> lstUserAccount = billingComponentService.getListUserByBusinessProcess(BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode());
					String message = GeneralConstants.STR_MESSAGE_ERROR_NET_SETTLEMENT_01;
					message = message + PropertiesUtilities.getExceptionMessage(se.getMessage(),se.getParams(),new Locale("es"));
					message = message + GeneralConstants.STR_MESSAGE_ERROR_NET_SETTLEMENT_02 + exceptions;
					
					notificationService.sendNotificationManual(loggerUser, loggerUser.getUserName(), businessProcess, lstUserAccount,
							GeneralConstants.NOTIFICATION_DEFAULT_HEADER, message, NotificationType.EMAIL);
					
					//notificationService.sendNotification(loggerUser, loggerUser.getUserName(), businessProcess, null, new Object[]{
						//PropertiesUtilities.getExceptionMessage(se.getMessage(),se.getParams(),new Locale("es")),exceptions});
				}
			}
			
			// update settlement process state to waiting / error
			settlementProcessServiceBean.updateSettlementProcessStateTx(idSettlementProcess, SettlementProcessStateType.ERROR.getCode(),null, loggerUser, null);
		}
		return blError;
	}

	/**
	 * Update settlement exchange rate operations.
	 *
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void updateSettlementExchangeRateOperations(Date settlementDate) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        settlementsComponentSingleton.updateSettlementExchangeRateOperations(settlementDate);
        
	}
	
	/**
	 * Identify settlement unfulfillment.
	 *
	 * @param settlementDate the settlement date
	 * @param idAssignmentProcess the id assignment process
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	public int identifySettlementUnfulfillment(Date settlementDate, Long idAssignmentProcess,Long idMechanism, Long idModality, Integer currency) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
        
        settlementsComponentSingleton.cancelAllChainedHolderOperations(settlementDate, BooleanType.NO.getCode(), loggerUser);
        
        return settlementsComponentSingleton.identifySettlementUnfulfillment(idAssignmentProcess, idMechanism, idModality, loggerUser, currency);
        
	}
	
	/**
	 * Send ope unful reports.
	 *
	 * @param settlementDate the settlement date
	 * @param idMechanism the id mechanism
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	public void sendOpeUnfulReports(Date settlementDate,Long idMechanism, Integer currency) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
        try {
        	if(idMechanism==null){
        		idMechanism = NegotiationMechanismType.BOLSA.getCode();
        	}
        	if(idMechanism.equals(NegotiationMechanismType.BOLSA.getCode())){
            	Long unfulfillmentCount = settlementProcessServiceBean.countUnfullfillmentOperations(settlementDate,idMechanism, currency);
            	if(unfulfillmentCount.compareTo(0l) > 0){
            		this.serviceAPIClients.sendCoreOpeUnfullFillmentReport(loggerUser, settlementDate, idMechanism, InstitutionType.BOLSA.getCode());
            		List<Object[]> lstUnfulParticipant = settlementProcessServiceBean.getUnfullfillmentParticipants(CommonsUtilities.currentDate(),ComponentConstant.SOURCE);
                	for(Object[] unfullPartObj : lstUnfulParticipant){
                		Long participantCode = (Long)unfullPartObj[0];
                		this.serviceAPIClients.sendUnfullfilledParticipantOpeUnfullFillmentReport(loggerUser, settlementDate, 
                				idMechanism, (String)unfullPartObj[1], participantCode);
                	}
                	List<Object[]> lstAffectParticipant = settlementProcessServiceBean.getUnfullfillmentParticipants(CommonsUtilities.currentDate(),ComponentConstant.TARGET);
                	for(Object[] affectPartObj : lstAffectParticipant){
                    	Long participantCode = (Long)affectPartObj[0];                    	
                    	this.serviceAPIClients.sendAffectedParticipantOpeUnfullFillmentReport(loggerUser, settlementDate, 
                    			idMechanism, (String)affectPartObj[1], participantCode);
                	}
                	/*
                	Map<String,String> parameters = new HashMap<String, String>();
                	Map<String,String> parametersAfecct = new HashMap<String, String>();
            		parameters.put("settlement_date",CommonsUtilities.convertDatetoString(settlementDate));
            		parameters.put("mechanism_type",idMechanism.toString());
            		parametersAfecct.put("settlement_date",CommonsUtilities.convertDatetoString(settlementDate));
            		parametersAfecct.put("mechanism_type",idMechanism.toString());
                	parameters.put("institution","CAJA DE VALORES DEL PARAGUAY S.A.");
                	sendUnfulfillmentReports(loggerUser,parameters, null,InstitutionType.BOLSA.getCode());
                	List<Object[]> lstUnfulParticipant = settlementProcessServiceBean.getUnfullfillmentParticipants(CommonsUtilities.currentDate(),ComponentConstant.SOURCE);
                	for(Object[] unfullPartObj : lstUnfulParticipant){
                    	Long participantCode = (Long)unfullPartObj[0];
                    	parameters.put("unfulfillment_part_id",participantCode.toString());
                    	parameters.put("institution",(String)unfullPartObj[1]);
                    	sendUnfulfillmentReports(loggerUser,parameters,participantCode,null);
                	}
                	List<Object[]> lstAffectParticipant = settlementProcessServiceBean.getUnfullfillmentParticipants(CommonsUtilities.currentDate(),ComponentConstant.TARGET);
                	for(Object[] affectPartObj : lstAffectParticipant){
                    	Long participantCode = (Long)affectPartObj[0];
                    	parametersAfecct.put("affected_part_id",participantCode.toString());
                    	parametersAfecct.put("institution",(String)affectPartObj[1]);
                    	sendUnfulfillmentReports(loggerUser,parametersAfecct,participantCode,null);
                	}*/
            	}
        	}
		} catch (ServiceException e) {
			logger.error("Error while sending unfulfillment reports, check log ");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Send unfulfillment reports.
	 *
	 * @param loggerUser the logger user
	 * @param parameters the parameters
	 * @param participantCode the participant code
	 * @param institutionType the institution type
	 * @throws ServiceException the service exception
	 */
	private void sendUnfulfillmentReports(LoggerUser loggerUser, Map<String, String> parameters, Long participantCode,Integer institutionType) throws ServiceException {
		ReportUser user = new ReportUser();
		if(participantCode!=null){
			user.setPartcipantCode(participantCode);
		}else{
			user.setUserName(loggerUser.getUserName());
		}
		if (institutionType!=null){
			user.setInstitutionType(institutionType);
		}
		user.setReportFormat(ReportFormatType.PDF.getCode());
		reportService.get().saveReportExecution(197l,parameters , null, user, loggerUser);
	}

	/**
	 * Identify forced purchase.
	 *
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void identifyForcedPurchase(Date settlementDate) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        settlementProcessServiceBean.identifyForcedPurchase(settlementDate, loggerUser);
	}
	
	/**
	 * Block sale purchase balances tx.
	 *
	 * @param objAccountOperationTO the obj account operation to
	 * @param idBusinessProcess the id business process
	 * @param indMarketFact the ind market fact
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void blockSalePurchaseBalancesTx(AccountOperationTO objAccountOperationTO, Long idBusinessProcess, Integer indMarketFact) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		try {
	        salePurchaseUpdateComponentService.get().blockSalePurchaseBalances(objAccountOperationTO, idBusinessProcess, indMarketFact);
		} catch (Exception e) {
			logger.error("Error while blocking balances ...");
			logger.error("idMechanismOperation: "+objAccountOperationTO.getIdMechanismOperation()
							+", idHolderAccountOperation: "+ objAccountOperationTO.getIdHolderAccountOperation()
							+", operationPart: "+ objAccountOperationTO.getOperationPart()
							+", idParticipant: "+ objAccountOperationTO.getIdParticipant()
							+", idHolderAccount: "+ objAccountOperationTO.getIdHolderAccount()
							+", idIsinCode: "+ objAccountOperationTO.getIdSecurityCode()
							+", stockQuantity: "+ objAccountOperationTO.getStockQuantity());
		}
	}
	
	
	/**
	 * Gets the list settlement operation by process.
	 *
	 * @param settlementDate the settlement date
	 * @param idSettlementProcess the id settlement process
	 * @return the list settlement operation by process
	 */
	public List<SettlementOperationTO> getListSettlementOperationByProcess(Date settlementDate, Long idSettlementProcess) {
		return settlementsComponentSingleton.getListSettlementOperationTO(settlementDate, idSettlementProcess);
	}
	
	/**
	 * Send settlement operation web client.
	 *
	 * @param lstSettlementOperationTOs the lst settlement operation t os
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=180)
	public void sendSettlementOperationWebClient(List<SettlementOperationTO> lstSettlementOperationTOs) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        settlementProcessServiceBean.sendSettlementOperationWebClient(lstSettlementOperationTOs, loggerUser);
	}
	
	/**
	 * Send funds participant positions tx.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idParticipant the id participant
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=30)
	public void sendFundsParticipantPositionsTx(Long idSettlementProcess, Long idParticipant, Long idMechanism, Long idModalityGroup, Integer currency) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        fundsComponentSingleton.sendFundsParticipantPositionsTx(idSettlementProcess, idParticipant, idMechanism, idModalityGroup, currency, loggerUser);
	}	
	
	/**
	 * Send funds participant positions.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idParticipant the id participant
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=30)
	public void sendFundsParticipantPositions(Long idSettlementProcess, Long idParticipant, Long idMechanism, Long idModalityGroup, Integer currency) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        fundsComponentSingleton.sendFundsParticipantPositions(idSettlementProcess, idParticipant, idMechanism, idModalityGroup, currency, loggerUser);
	}
	
	/**
	 * Gets the list participant position.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idParticipant the id participant
	 * @param indPosition the ind position
	 * @param indSentFunds the ind sent funds
	 * @param dtSettlementDate the dt settlement date
	 * @return the list participant position
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantPosition> getListParticipantPosition(Long idSettlementProcess, Long idParticipant, Integer indPosition, 
			Integer indSentFunds, Date dtSettlementDate) throws ServiceException{
		return settlementProcessServiceBean.getListParticipantPosition(idSettlementProcess, idParticipant, indPosition, indSentFunds, dtSettlementDate);
	}
	
	
	/**
	 * Verify mechanism pending settlement operation.
	 *
	 * @param settlementDate the settlement date
	 * @return the string
	 */
	public String verifyMechanismPendingSettlementOperation(Date settlementDate) {
		return settlementProcessServiceBean.verifyMechanismPendingSettlementOperation(settlementDate);
	}
	
	/**
	 * Calculate issuer amount automatic.
	 *
	 * @param expirationDate the expiration date
	 * @throws ServiceException the service exception
	 */
	public void calculateIssuerAmountAutomatic(Date expirationDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		settlementProcessServiceBean.calculateIssuerAmountAutomatic(expirationDate, loggerUser);
	}
}
