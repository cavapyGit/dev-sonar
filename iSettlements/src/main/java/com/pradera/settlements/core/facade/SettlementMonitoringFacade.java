package com.pradera.settlements.core.facade;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.accounts.Participant;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.OperationSettlement;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.SettlementSchedule;
import com.pradera.model.settlement.type.MonitoringSettCountType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.settlements.core.service.SettlementMonitoringService;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;
import com.pradera.settlements.core.view.ModalitySettlementTO;
import com.pradera.settlements.core.view.MonitorSettlementTO;
import com.pradera.settlements.core.view.MonitoringAmountsTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementMonitoringFacade.
 *
 * @author Pradera Technologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SettlementMonitoringFacade {
	
	/** The monitoring settlement process service. */
	@EJB
	private SettlementMonitoringService monitorProcessService;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The settlements component singleton. */
	@EJB
	SettlementsComponentSingleton settlementsComponentSingleton;
	
	/** The batch service bean. */
	@EJB
	private BatchServiceBean batchServiceBean;
	
	@EJB
	private SettlementMonitoringService settlementMonitoringService;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	public MechanismOperation getMcnWithParticipantsAndSecurity(Long idMechanismOperationPk) throws ServiceException{
		return monitorProcessService.getMcnWithParticipantsAndSecurity(idMechanismOperationPk);
	}
	
	public List<Participant> getLstParticipant() throws ServiceException{
		return monitorProcessService.getLstParticipant();
	}
	
	public boolean finishProcessForcedPurchaseIdentification() throws ServiceException{
		return monitorProcessService.finishProcessForcedPurchaseIdentification();
	}

	/**
	 * Begin settlement net process facade.
	 *
	 * @param idCurrency the id currency
	 * @param idModalityGroup the id modality group
	 * @param idMechanism the id mechanism
	 * @param settlementDate the settlement date
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void startNetSettlementProcess(Integer idCurrency, Long idModalityGroup, Long idMechanism, 
			Date settlementDate, NetSettlementAttributesTO netSettlementAttributesTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		SettlementSchedule settlementSchedule = netSettlementAttributesTO.getSettlementSchedule();
		
		monitorProcessService.validateSettlementScheduleSequence(idCurrency, idModalityGroup, idMechanism,settlementDate,
				settlementSchedule.getScheduleType(), settlementSchedule.getExecutionTimes().longValue(),false);
		
		if(settlementSchedule.getScheduleTypeRef()!=null){
			monitorProcessService.validateSettlementScheduleSequence(idCurrency, idModalityGroup, 
					idMechanism,settlementDate,settlementSchedule.getScheduleTypeRef(), settlementSchedule.getExecutionTimes().longValue(),true);
		}
		settlementsComponentSingleton.calculateNetParticipantPositions(idMechanism, settlementDate, idModalityGroup, idCurrency, 
				settlementSchedule.getScheduleType(), loggerUser, false, netSettlementAttributesTO);
	}
	
	/**
	 * Restart settlement process.
	 *
	 * @param settlementProcParam the settlement proc param
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess restartSettlementProcess(SettlementProcess settlementProcParam) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		return monitorProcessService.restartSettlementProcess(settlementProcParam);
	}
	
	/**
	 * Stop settlement process.
	 *
	 * @param settlementProcParam the settlement proc param
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess stopSettlementProcess(SettlementProcess settlementProcParam) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		return monitorProcessService.stopSettlementProcess(settlementProcParam);
	}
	
	/**
	 * Calculate net position facade.
	 *
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @param autoRetire the auto retire
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=30)
	public List<ParticipantPosition> endEditAndRecalculateNetPositions(NetSettlementAttributesTO netSettlementAttributesTO, boolean autoRetire) throws ServiceException{
		return settlementsComponentSingleton.recalculateParticipantPositions(netSettlementAttributesTO, autoRetire);
	}
	
	/**
	 * Withdrawn Funds Automatic.
	 *
	 * @throws ServiceException the Service Exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=30)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void withdrawnFundsAutomatic(NetSettlementAttributesTO netSettlementAttributesTO, SettlementProcess settlementProcessNetSession) throws ServiceException{
		settlementMonitoringService.withdrawnFundsAutomatic(netSettlementAttributesTO, settlementProcessNetSession);
	}
	
	/**
	 * Gets the settlement process started facade.
	 *
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @param settlementProcessId the settlement process id
	 * @return the settlement process started facade
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess restartNetSettlementProcess(NetSettlementAttributesTO netSettlementAttributesTO, Long settlementProcessId) throws ServiceException{
		return settlementsComponentSingleton.processNetParticipantPositions(netSettlementAttributesTO,settlementProcessId, false);
	}
	
	/**
	 * Validate begin net settlement facade.
	 *
	 * @param mechanismId the mechanism id
	 * @param settlementDate the settlement date
	 * @param modalityGroupId the modality group id
	 * @param schemaId the schema id
	 * @param currency the currency
	 * @throws ServiceException the service exception
	 */
	public void validateStartSettlementProcessFacade(Long mechanismId, Date settlementDate , 
			Long modalityGroupId, Integer schemaId, Integer currency)throws ServiceException{
		monitorProcessService.validateExistingSettlementProcess(mechanismId,settlementDate,modalityGroupId,schemaId,currency);
	}
	
	/**
	 * validate Settlemet State Request.
	 *
	 * @param settlementDate settlement date
	 * @param currency currency
	 * @throws ServiceException the Service Exception
	 */
	public void validateSettlemetStateRequest( Date settlementDate, Integer currency) throws ServiceException{
		monitorProcessService.validateSettlemetStateRequest(settlementDate,currency);
	}
	
	/**
	 * Validate close settlement facade.
	 *
	 * @param settlementProcess the settlement process
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long validateCloseSettlementFacade(SettlementProcess settlementProcess)throws ServiceException{
		return monitorProcessService.validateCloseSettlement(settlementProcess);
	}
	
	/**
	 * Gets the modality settlement facade.
	 *
	 * @param monitorSettlement the monitor settlement
	 * @return the modality settlement facade
	 * @throws ServiceException the service exception
	 */
	public List<ModalitySettlementTO> getModalitySettlementFacade(MonitorSettlementTO monitorSettlement) throws ServiceException{
		return monitorProcessService.getModalitySettlementService(monitorSettlement);
	}
	
	/**
	 * Gets the settlement process.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityGroupId the modality group id
	 * @param settlementDate the settlement date
	 * @param currency the currency
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public List<SettlementProcess> getSettlementProcess(Long mechanismId, Long modalityGroupId, Date settlementDate, Integer currency, Long partCode) throws ServiceException{
		List<SettlementProcess> settProcessList = monitorProcessService.getSettlementProcess(mechanismId,modalityGroupId, settlementDate, currency,partCode);
		if(partCode!=null){
			for(SettlementProcess settlementProcess: settProcessList){
				monitorProcessService.detach(settlementProcess);
				Long pendient = 0L, withDrawn = 0L, settled = 0L, total = 0L;
				for(OperationSettlement operationSettlement: settlementProcess.getOperationSettlements()){
					switch(OperationSettlementSateType.get(operationSettlement.getOperationState())){
						case SETTLED: settled++;break;
						case SETTLEMENT_PENDIENT: pendient++;break;
						case WITHDRAWN_BY_FUNDS: case WITHDRAWN_BY_GUARANTEES: case WITHDRAWN_BY_SECURITIES: withDrawn++;break;
						default: continue;
					}
				}
				total = settled+pendient+withDrawn;
				settlementProcess.setTotalOperation(total);
				settlementProcess.setSettledOperations(settled);
				settlementProcess.setPendingOperations(pendient);
				settlementProcess.setRemovedOperations(withDrawn);
			}
		}
		return settProcessList;
	}
	
	/**
	 * Gets the participant positions.
	 *
	 * @param settlementProcessId the settlement process id
	 * @return the participant positions
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantPosition> getParticipantPositions(Long settlementProcessId,Long partCode) throws ServiceException{
		return monitorProcessService.getParticipantPositions(settlementProcessId,partCode);
	}
	
	/**
	 * Sets the operations set process facade.
	 *
	 * @param setProcess the set process
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess setOperationsSetProcessFacade(SettlementProcess setProcess) throws ServiceException{
		return monitorProcessService.setOperationsSetProcess(setProcess);
	}
	
	/**
	 * Gets the amounts by state.
	 *
	 * @param modalityList the modality list
	 * @param monitorSettlement the monitor settlement
	 * @return the amounts by state
	 * @throws ServiceException the service exception
	 */
	public MonitoringAmountsTO getAmountsByState(List<ModalitySettlementTO> modalityList, MonitorSettlementTO monitorSettlement) throws ServiceException{
		Map<Long, BigDecimal> mapDataByStates = new HashMap<Long, BigDecimal>();
		
		mapDataByStates.put(MonitoringSettCountType.CANCEL_OPERATION.getCode(), BigDecimal.ZERO);
		mapDataByStates.put(MonitoringSettCountType.PENDIENT_OPERATION.getCode(), BigDecimal.ZERO);
		mapDataByStates.put(MonitoringSettCountType.REMOVED_OPERATION.getCode(), BigDecimal.ZERO);
		mapDataByStates.put(MonitoringSettCountType.SETTLEMENT_OPERATION.getCode(), BigDecimal.ZERO);
		
		for(ModalitySettlementTO modalitySettlement: modalityList){
			
			//sum amount operations canceled
			for(SettlementOperation cancelledOperation: modalitySettlement.getLstCancelledOperations()){
				BigDecimal countAmountCancel = mapDataByStates.get(MonitoringSettCountType.CANCEL_OPERATION.getCode()).add(cancelledOperation.getSettlementAmount());
				mapDataByStates.put(MonitoringSettCountType.CANCEL_OPERATION.getCode(),countAmountCancel);
			}
			
			//sum amount operations pendient
			for(SettlementOperation operationsPendient: modalitySettlement.getLstPendingOperations()){
				BigDecimal amountToSum = BigDecimal.ZERO;
//				if(operationsPendient.getOperationState().equals(MechanismOperationStateType.ASSIGNED_STATE.getCode())){
//					amountToSum = operationsPendient.getSettlementAmount();
//				}else if(operationsPendient.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode())){
					amountToSum = operationsPendient.getSettlementAmount();	
//				}
				BigDecimal countAmountPendient = mapDataByStates.get(MonitoringSettCountType.PENDIENT_OPERATION.getCode()).add(amountToSum);
				mapDataByStates.put(MonitoringSettCountType.PENDIENT_OPERATION.getCode(),countAmountPendient);
			}
			
			//sum amount operations settlement
			for(SettlementOperation operationsSettlem: modalitySettlement.getLstSettledOperations()){
				BigDecimal amountToSum = BigDecimal.ZERO;
//				if(operationsSettlem.getRealCashSettlementDate()!=null && operationsSettlem.getRealTermSettlementDate()!=null){
//					if(CommonsUtilities.truncateDateTime(operationsSettlem.getRealTermSettlementDate()).equals(
//					   CommonsUtilities.truncateDateTime(operationsSettlem.getRealCashSettlementDate()))){//if cashSettlement equals than termSettlement
//						amountToSum = operationsSettlem.getCashSettlementAmount().add(operationsSettlem.getTermSettlementAmount());
//					}
//				}
				
				if(amountToSum.equals(BigDecimal.ZERO)){
//					if(operationsSettlem.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode())){
//						amountToSum = operationsSettlem.getCashSettlementAmount();
//					}else if(operationsSettlem.getOperationState().equals(MechanismOperationStateType.TERM_SETTLED.getCode())){
//						amountToSum = operationsSettlem.getTermSettlementAmount();
//					}
					amountToSum = operationsSettlem.getSettlementAmount();	
				}
				BigDecimal countAmountSettlem = mapDataByStates.get(MonitoringSettCountType.SETTLEMENT_OPERATION.getCode()).add(amountToSum);
				mapDataByStates.put(MonitoringSettCountType.SETTLEMENT_OPERATION.getCode(),countAmountSettlem);
			}
			
			//sum amount operation removed
			for(SettlementOperation operationsRemov: modalitySettlement.getLstRemovedOperations()){
				BigDecimal amountToSum = BigDecimal.ZERO;
//				if(operationsRemov.getOperationState().equals(MechanismOperationStateType.ASSIGNED_STATE.getCode())){
//					amountToSum = operationsRemov.getCashSettlementAmount();
//				}else if(operationsRemov.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode())){
//					amountToSum = operationsRemov.getTermSettlementAmount();	
//				}
				amountToSum = operationsRemov.getSettlementAmount();
				
				BigDecimal countAmountRemov = mapDataByStates.get(MonitoringSettCountType.REMOVED_OPERATION.getCode()).add(amountToSum);
				mapDataByStates.put(MonitoringSettCountType.REMOVED_OPERATION.getCode(),countAmountRemov);
			}
		}
		MonitoringAmountsTO amountsTO = new MonitoringAmountsTO();
		for(MonitoringSettCountType monitorCountType: MonitoringSettCountType.list){
			switch (monitorCountType) {
				case CANCEL_OPERATION:amountsTO.setCanceledAmount(mapDataByStates.get(monitorCountType.getCode()));break;
				case SETTLEMENT_OPERATION:amountsTO.setSettlemAmount(mapDataByStates.get(monitorCountType.getCode()));break;
				case PENDIENT_OPERATION:amountsTO.setPendientAmount(mapDataByStates.get(monitorCountType.getCode()));break;
				case REMOVED_OPERATION:amountsTO.setRemovAmount(mapDataByStates.get(monitorCountType.getCode()));break;
			}
		}
		amountsTO.setTotalAmount(amountsTO.getCanceledAmount().add(amountsTO.getPendientAmount()).add(amountsTO.getSettlemAmount()).add(amountsTO.getRemovAmount()));
		return amountsTO;
	}
	
	/**
	 * Register batch.
	 *
	 * @param userName the user name
	 * @param process the process
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void registerBatch(String userName,BusinessProcess process, Map<String,Object> parameters) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if (loggerUser == null || loggerUser.getIdPrivilegeOfSystem() == null) {
			if (loggerUser == null) {
				loggerUser = new LoggerUser();
				loggerUser.setUserAction(new UserAcctions());
			}
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		}
		//loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		batchServiceBean.registerBatchTx(userName, process, parameters);
	}
	
	/**
	 * Gets the begin end day facade.
	 *
	 * @param settlementDate the settlement date
	 * @return the begin end day facade
	 * @throws ServiceException the service exception
	 */
	public BeginEndDay getBeginEndDayFacade(Date settlementDate) throws ServiceException{
		if(!CommonsUtilities.currentDate().equals(settlementDate)){
			return null;
		}
		BeginEndDay beginEndDay = monitorProcessService.getBeginEndDayService();
		if(beginEndDay==null){
			throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_DAY_NOT_START);
		}
		return beginEndDay;
	}
	
	/**
	 * Gets the settlement schedule.
	 *
	 * @param settlementSchedule the settlement schedule
	 * @param indSettlement the ind settlement
	 * @return the settlement schedule
	 * @throws ServiceException the service exception
	 */
	public SettlementSchedule getSettlementSchedule(Integer settlementSchedule,Integer indSettlement) throws ServiceException {
		// TODO Auto-generated method stub
		return settlementProcessService.getSettlementSchedule(settlementSchedule,null,indSettlement,null);
	}
	
	/**
	 * Update operation settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param operationSettlement the operation settlement
	 * @throws ServiceException the service exception
	 */
	public void updateOperationSettlement(Long idSettlementProcess , OperationSettlement operationSettlement) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		settlementProcessService.update(operationSettlement);
		if(operationSettlement.getOperationState().equals(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode())){
			settlementProcessService.updateSettlementRemovedCount(idSettlementProcess);
		}
		
	}
	
	/**
	 * Update operation settlements.
	 *
	 * @param settlementProcess the settlement process
	 * @throws ServiceException the service exception
	 */
	public void updateOperationSettlements(SettlementProcess settlementProcess) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		settlementProcessService.updateOperationSettlements(settlementProcess);
	}
	
	/**
	 * para eliminar un proceso de liquidacion
	 * @param idSettlementProcessPk
	 * @throws ServiceException
	 */
	public void deleteSettlementProcess(Long idSettlementProcessPk) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		settlementProcessService.deleteSettlementProcess(idSettlementProcessPk);
	}
	
	/**
	 * Gets the list chain settlement operation.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idChainHolderOperation the id chain holder operation
	 * @return the list chain settlement operation
	 * @throws ServiceException the service exception
	 */
	public List<Long> getListChainSettlementOperation(Long idSettlementOperation, Long idChainHolderOperation) throws ServiceException {
		return settlementProcessService.getListChainSettlementOperation(idSettlementOperation, idChainHolderOperation);
	}
	
	/**
	 * Gets the list settlement operation by chain.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param idSettlementOperation the id settlement operation
	 * @return the list settlement operation by chain
	 * @throws ServiceException the service exception
	 */
	public List<Long> getListSettlementOperationByChain(Long idChainedHolderOperation, Long idSettlementOperation) throws ServiceException {
		return settlementProcessService.getListSettlementOperationByChain(idChainedHolderOperation, idSettlementOperation);
	}
	
}