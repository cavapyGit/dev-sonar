package com.pradera.settlements.core.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementUnfulfillmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementUnfulfillmentTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The id purchase participant. */
	private Long idPurchaseParticipant;
	
	/** The id sale participant. */
	private Long idSaleParticipant;
	
	/** The id modality. */
	private Long idModality;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The ind partial. */
	private Integer indPartial;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The settlement amount. */
	private BigDecimal settlementAmount;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/** The ind term settlement. */
	private Integer indTermSettlement;
	
	/** The ind cash stock block. */
	private Integer indCashStockBlock;
	
	/** The ind term stock block. */
	private Integer indTermStockBlock;
	
	/** The ind primary placement. */
	private Integer indPrimaryPlacement;
	
	/** The ind reporting balance. */
	private Integer indReportingBalance;
	
	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}
	
	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}
	
	/**
	 * Gets the id settlement operation.
	 *
	 * @return the id settlement operation
	 */
	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}
	
	/**
	 * Sets the id settlement operation.
	 *
	 * @param idSettlementOperation the new id settlement operation
	 */
	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}
	
	/**
	 * Gets the id purchase participant.
	 *
	 * @return the id purchase participant
	 */
	public Long getIdPurchaseParticipant() {
		return idPurchaseParticipant;
	}
	
	/**
	 * Sets the id purchase participant.
	 *
	 * @param idPurchaseParticipant the new id purchase participant
	 */
	public void setIdPurchaseParticipant(Long idPurchaseParticipant) {
		this.idPurchaseParticipant = idPurchaseParticipant;
	}
	
	/**
	 * Gets the id sale participant.
	 *
	 * @return the id sale participant
	 */
	public Long getIdSaleParticipant() {
		return idSaleParticipant;
	}
	
	/**
	 * Sets the id sale participant.
	 *
	 * @param idSaleParticipant the new id sale participant
	 */
	public void setIdSaleParticipant(Long idSaleParticipant) {
		this.idSaleParticipant = idSaleParticipant;
	}
	
	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}
	
	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}
	
	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	
	/**
	 * Gets the ind term settlement.
	 *
	 * @return the ind term settlement
	 */
	public Integer getIndTermSettlement() {
		return indTermSettlement;
	}
	
	/**
	 * Sets the ind term settlement.
	 *
	 * @param indTermSettlement the new ind term settlement
	 */
	public void setIndTermSettlement(Integer indTermSettlement) {
		this.indTermSettlement = indTermSettlement;
	}
	
	/**
	 * Gets the ind cash stock block.
	 *
	 * @return the ind cash stock block
	 */
	public Integer getIndCashStockBlock() {
		return indCashStockBlock;
	}
	
	/**
	 * Sets the ind cash stock block.
	 *
	 * @param indCashStockBlock the new ind cash stock block
	 */
	public void setIndCashStockBlock(Integer indCashStockBlock) {
		this.indCashStockBlock = indCashStockBlock;
	}
	
	/**
	 * Gets the ind primary placement.
	 *
	 * @return the ind primary placement
	 */
	public Integer getIndPrimaryPlacement() {
		return indPrimaryPlacement;
	}
	
	/**
	 * Sets the ind primary placement.
	 *
	 * @param indPrimaryPlacement the new ind primary placement
	 */
	public void setIndPrimaryPlacement(Integer indPrimaryPlacement) {
		this.indPrimaryPlacement = indPrimaryPlacement;
	}
	
	/**
	 * Gets the ind term stock block.
	 *
	 * @return the ind term stock block
	 */
	public Integer getIndTermStockBlock() {
		return indTermStockBlock;
	}
	
	/**
	 * Sets the ind term stock block.
	 *
	 * @param indTermStockBlock the new ind term stock block
	 */
	public void setIndTermStockBlock(Integer indTermStockBlock) {
		this.indTermStockBlock = indTermStockBlock;
	}
	
	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	
	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	
	/**
	 * Gets the ind partial.
	 *
	 * @return the ind partial
	 */
	public Integer getIndPartial() {
		return indPartial;
	}
	
	/**
	 * Sets the ind partial.
	 *
	 * @param indPartial the new ind partial
	 */
	public void setIndPartial(Integer indPartial) {
		this.indPartial = indPartial;
	}
	
	/**
	 * Gets the ind reporting balance.
	 *
	 * @return the ind reporting balance
	 */
	public Integer getIndReportingBalance() {
		return indReportingBalance;
	}
	
	/**
	 * Sets the ind reporting balance.
	 *
	 * @param indReportingBalance the new ind reporting balance
	 */
	public void setIndReportingBalance(Integer indReportingBalance) {
		this.indReportingBalance = indReportingBalance;
	}

	
	
}
