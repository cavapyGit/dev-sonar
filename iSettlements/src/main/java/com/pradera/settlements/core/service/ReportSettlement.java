package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;


//import com.pradera.core.component.business.service.ExecutorComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReportSettlement.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class ReportSettlement extends CrudDaoServiceBean{

	/** The guarantees settlement. */
	@EJB
	private GuaranteesSettlement guaranteesSettlement;
	
	/** The funds component singleton. */
	@EJB
	private FundsComponentSingleton fundsComponentSingleton;
	
	
	/**
	 * Sets the tle term report operation.
	 *
	 * @param objSettlementOperationTO the new tle term report operation
	 * @throws ServiceException the service exception
	 */
	public void settleTermReportOperation(SettlementOperationTO objSettlementOperationTO) throws ServiceException
	{
		/*
		 * STEP 01:
		 * RETURNING DELIVERED GUARANTEES TO REPORTED HOLDER ACCOUNTS 
		 * - Return to reported holder accounts the delivered guarantees because stock dividends from principal guarantee.
		 *   We have to get the delivered guarantees for each SELLER holder account in TERM PART  
		 */
		BigDecimal totalReportingGuarantee= new BigDecimal(0);
		List<SettlementHolderAccountTO> lstReportingHolderAccounts = objSettlementOperationTO.getLstSellerHolderAccounts();
		if (Validations.validateListIsNotNullAndNotEmpty(lstReportingHolderAccounts))
		{
			for (SettlementHolderAccountTO objSettlementHolderAccountTO: lstReportingHolderAccounts)
			{
				//now we have to withdrawal the guarantees from delivered stock dividends to reporting holder account 
				BigDecimal movedGuaranteeBalance= guaranteesSettlement.generateWithdrawalReportingGuaranteBalance(objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
																																	null, null, objSettlementOperationTO.getIdModality(), null, null);
				//we accumulate the moved guarantee from reporting holder accounts. This total guarantee will be distributed along reported holder accounts
				totalReportingGuarantee = totalReportingGuarantee.add(movedGuaranteeBalance);
			}
		}
		
		/*
		 * STEP 02:
		 * UNBLOCKING PRESENTED SECURITIES AND FUNDS GUARANTEES BY REPORTED HOLDER ACCOUNTS. 
		 * - To unblock the MARGIN balances and STOCK DIVIDENDS MARGIN
		 * - To deliver the STOCK DIVIDENDS PRINCIPAL from reporting holder accounts 
		 * - To deliver daily earned interests by the presented FUNDS guarantees 
		 * - To unblock FUNDS guarantees 
		 */
		List<SettlementHolderAccountTO> lstReportedHolderAccounts = objSettlementOperationTO.getLstBuyerHolderAccounts();
		if (Validations.validateListIsNotNullAndNotEmpty(lstReportedHolderAccounts))
		{
			BigDecimal deliveredReportingGuarantees= new BigDecimal(0); //total of delivered reporting guarantees to reported holder account
			int count=0; //count to know the position inside the list lstReportedHolderAccounts
			for (SettlementHolderAccountTO objSettlementHolderAccountTO: lstReportedHolderAccounts)
			{
				++count;
				 // - To unblock the MARGIN balances and STOCK DIVIDENDS MARGIN
				unblockMarginGuarantees(objSettlementHolderAccountTO, objSettlementOperationTO.getIdMechanismOperation());
				
				// - To deliver the STOCK DIVIDENDS PRINCIPAL to reported holder account from reporting holder accounts
				deliverPrincipalStockDividends(objSettlementHolderAccountTO, objSettlementOperationTO, totalReportingGuarantee, 
																deliveredReportingGuarantees, count, lstReportedHolderAccounts.size());
				
				// - To deliver daily earned interests by the presented FUNDS guarantees
				fundsComponentSingleton.deliverInterestFundsGuarantees(objSettlementHolderAccountTO, objSettlementOperationTO);
				
				// - To unblock FUNDS guarantees
				fundsComponentSingleton.unblockFundsGuarantees(objSettlementHolderAccountTO, objSettlementOperationTO);
			}
		}
	}
	
	
	/**
	 * Unblock margin guarantees.
	 *
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param idMechanismOperation the id mechanism operation
	 * @throws ServiceException the service exception
	 */
	private void unblockMarginGuarantees(SettlementHolderAccountTO objSettlementHolderAccountTO, Long idMechanismOperation) throws ServiceException
	{
		List<Object[]> lstStockGuarantees= new ArrayList<Object[]>();
		List<Object[]> lstSecuritiesGuarantees = guaranteesSettlement.getListGuarateesHolderAccount(objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
																							GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.MARGIN.getCode());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSecuritiesGuarantees))
			lstStockGuarantees.addAll(lstSecuritiesGuarantees);
		lstSecuritiesGuarantees = guaranteesSettlement.getListGuarateesHolderAccount(objSettlementHolderAccountTO.getIdHolderAccountOperation(), 
																				GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.STOCK_DIVIDENDS_MARGIN.getCode());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSecuritiesGuarantees))
			lstStockGuarantees.addAll(lstSecuritiesGuarantees);
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstStockGuarantees))
		{
			//we have to unblock all stocks for each security in MARGIN or STOCK DIVIDENDS MARGIN
			for (Object[] arrObject: lstStockGuarantees)
			{
				Integer guaranteeType= new Integer(arrObject[6].toString());
				String idIsinCode= arrObject[4].toString();
				BigDecimal stockQuantity = new BigDecimal(arrObject[5].toString());
				if (stockQuantity.compareTo(BigDecimal.ZERO) >0)
				{
					//we generate the withdrawal of securities guarantees for each guarantee type: MARGIN, STOCK DIVIDENDS MARGIN
					guaranteesSettlement.generateWithdrawalReportedSecuritiesGuarantees(objSettlementHolderAccountTO, guaranteeType, idIsinCode, stockQuantity);
				}
			}
		}
	}
	
	/**
	 * Deliver principal stock dividends.
	 *
	 * @param objSettlementHolderAccountTO the obj settlement holder account to
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param totalReportingGuarantee the total reporting guarantee
	 * @param deliveredReportingGuarantees the delivered reporting guarantees
	 * @param count the count
	 * @param lstReportedSize the lst reported size
	 * @throws ServiceException the service exception
	 */
	private void deliverPrincipalStockDividends(SettlementHolderAccountTO objSettlementHolderAccountTO, SettlementOperationTO objSettlementOperationTO, 
													BigDecimal totalReportingGuarantee, BigDecimal deliveredReportingGuarantees, int count, int lstReportedSize) throws ServiceException
	{
		if (totalReportingGuarantee.compareTo(BigDecimal.ZERO) >0)
		{
			BigDecimal stockQuantity= null;
			if (count == lstReportedSize)
				//To the last holder account we deliver the rest of the total reporting guarantee
				stockQuantity = totalReportingGuarantee.subtract(deliveredReportingGuarantees);
			else {
				//we deliver the reporting guarantee according the ratio of holder account stock quantity in the report operation
				stockQuantity = totalReportingGuarantee.multiply(objSettlementHolderAccountTO.getStockQuantity()).
																	divide(objSettlementOperationTO.getStockQuantity()).setScale(0, BigDecimal.ROUND_HALF_UP);
			}
			guaranteesSettlement.generateDepositReportedSecuritiesGuarantees(objSettlementHolderAccountTO, stockQuantity);
//			deliveredReportingGuarantees = deliveredReportingGuarantees.add(stockQuantity); //we accumulate the delivered guarantee quantity
		}
	}
	
}
