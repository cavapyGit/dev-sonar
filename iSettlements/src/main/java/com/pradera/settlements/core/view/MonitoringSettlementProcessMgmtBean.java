package com.pradera.settlements.core.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.funds.fundoperations.monitoring.facade.MonitoringFundOperationsServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.OperationSettlement;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.SettlementSchedule;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.negotiations.operations.view.OperationBusinessBean;
import com.pradera.settlements.core.facade.SettlementMonitoringFacade;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.core.service.CollectionProcessService;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.to.NetParticipantPositionTO;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;
import com.pradera.settlements.core.to.NetSettlementOperationTO;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitoringSettlementProcessMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MonitoringSettlementProcessMgmtBean extends GenericBaseBean implements Serializable {

/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(MonitoringSettlementProcessMgmtBean.class);
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The monitor process facade. */
	@EJB
	private SettlementMonitoringFacade monitorProcessFacade;
	
	/** The mcn operation service facade. */
	@EJB
	private McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The settlement process service bean. */
	@EJB
	SettlementProcessService settlementProcessServiceBean;
	
	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	/** The monitor settlement attributes. */
	private MonitorSettlementAttributes monitorSettlementAttributes;
	
	/** The net settlement attributes to. */
	private NetSettlementAttributesTO netSettlementAttributesTO;
	
	/** The monitor settlement to. */
	private MonitorSettlementTO monitorSettlementTO;
	
	/** The monitor sett selected. */
	private MonitorSettlementTO monitorSettSelected;//object selected on datatable's search
	
	/** The settlement process session. */
	private SettlementProcess settlementProcessNetSession;
	
	/** The collection process service bean. */
	@EJB
	CollectionProcessService collectionProcessServiceBean;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The operation business comp. */
	@Inject
	OperationBusinessBean operationBusinessComp;
	
	/** The mechanism operacion id. */
	private Long mechanismOperacionId;
	
	/** The lst id chained holder operation. */
	private List<Long> lstIdChainedHolderOperation;
	
	/** The net settlement operation to. */
	private List<NetSettlementOperationTO> netSettlementOperationTO = new ArrayList<NetSettlementOperationTO>();
	
	/** The removed operation. */
	private boolean removedOperation;
	
	/** The bl progress bar. */
	private boolean blProgressBar;
	
	/** The modality settlement selected. */
	private ModalitySettlementTO modalitySettlementSelected;
	
	/** The participant selected. */
	private String participantSelected;
	
	/**Contrasena de usuario*/
	private String password;
	
	/** The monitor cash account facade. */
	@EJB
	private MonitoringFundOperationsServiceFacade monitorCashAccountFacade;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		monitorSettlementAttributes = new MonitorSettlementAttributes();
		monitorSettlementTO = new MonitorSettlementTO();
		try{
			monitorSettlementTO.setSettlementDate(CommonsUtilities.currentDate());
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				monitorSettlementTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			}
			
			loadParameterLists();
			loadMechanismOperationList();
			loadModalityGroupList();
			loadDailyExchangeRate();
			monitorSettlementTO.setNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
			changeMechanisnModality();
			monitorSettlementTO.setSettlementSchemePk(SettlementSchemaType.NET.getCode());
			monitorSettlementTO.setCurrencyPk(CurrencyType.PYG.getCode());	
			
			modalitySettlementSelected = new ModalitySettlementTO();
			
		}catch (ServiceException e) {
			 excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load parameter lists.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParameterLists() throws ServiceException {
		
		// lista participantes
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		monitorSettlementAttributes.setParticipantList(mcnOperationServiceFacade.getLstParticipants(null));

		// parametros
		ParameterTableTO paramTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.CURRENCY.getCode()); //currencies
			masterPks.add(MasterTableType.MASTER_TABLE_SETTLEMENT_PROCESS_TYPE.getCode()); //stados proc liq
			masterPks.add(MasterTableType.ESTADOS_OPERACION_MCN.getCode()); //estados mcn
			masterPks.add(MasterTableType.MASTER_TABLE_REMOVE_SETTLEMENT.getCode());  // motivos de retiro de operaciones
			masterPks.add(MasterTableType.PARAMETER_TABLE_SCHED_SETTLEMENT.getCode());//
		paramTO.setLstMasterTableFk(masterPks);
		paramTO.setOrderbyParameterTableCd(ComponentConstant.ONE);
		
		List<ParameterTable> lst = generalParametersFacade.getListParameterTableServiceBean(paramTO);
		for(ParameterTable pt : lst ){
			monitorSettlementAttributes.getMapParameters().put(pt.getParameterTablePk(), pt);
			if(pt.getParameterState().equals(ComponentConstant.ONE)){
				if(pt.getMasterTable().getMasterTablePk().equals(MasterTableType.CURRENCY.getCode()) && GeneralConstants.ONE_VALUE_STRING.equals(pt.getIndicator2() )){
					monitorSettlementAttributes.getCurrencyList().add(pt);
				}else if(pt.getMasterTable().getMasterTablePk().equals(MasterTableType.ESTADOS_OPERACION_MCN.getCode())){
					monitorSettlementAttributes.getMechanismOperationStateList().add(pt);
				}else if(pt.getMasterTable().getMasterTablePk().equals(MasterTableType.MASTER_TABLE_REMOVE_SETTLEMENT.getCode())){
					monitorSettlementAttributes.getRemoveMotiveList().add(pt);
				}else if(pt.getMasterTable().getMasterTablePk().equals(MasterTableType.PARAMETER_TABLE_SCHED_SETTLEMENT.getCode())){
					if(!pt.getParameterTablePk().equals(SettlementScheduleType.MELID.getCode())){
						monitorSettlementAttributes.getNetProcessTypeList().add(pt);
					}					
				}
			}
		}
		ParameterTable parameterTableYes= new ParameterTable();
		parameterTableYes.setParameterTablePk(BooleanType.YES.getCode());
		parameterTableYes.setParameterName(BooleanType.YES.getValue());
		monitorSettlementAttributes.getMapParameters().put(parameterTableYes.getParameterTablePk(), parameterTableYes);
		ParameterTable parameterTableNO= new ParameterTable();
		parameterTableNO.setParameterTablePk(BooleanType.NO.getCode());
		parameterTableNO.setParameterName(BooleanType.NO.getValue());
		monitorSettlementAttributes.getMapParameters().put(parameterTableNO.getParameterTablePk(), parameterTableNO);
	}

	/**
	 * Load mechanism operation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadMechanismOperationList() throws ServiceException{
		List<NegotiationMechanism> negoList = mcnOperationServiceFacade.getLstNegotiationMechanism(null);
		this.monitorSettlementAttributes.setNegotiationMechanismList(negoList);
	}
	/**
	 * Load modality group list.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadModalityGroupList() throws ServiceException{
		List<ModalityGroup> modalityGroupList = mcnOperationServiceFacade.getModalityGroups(null,null);
		this.monitorSettlementAttributes.setModalityGroupList(modalityGroupList);
		this.monitorSettlementAttributes.setModalityGroupBySettSchemaList(new ArrayList<ModalityGroup>());
	}

	/**
	 * Load daily exchange rate.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadDailyExchangeRate() throws ServiceException{
		DailyExchangeRates dailyExchangeRates = generalParametersFacade.getdailyExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());
		monitorSettlementAttributes.setDailyExchangeRate(dailyExchangeRates);
	}
	/**
	 * Gets the settlement schema type list.
	 *
	 * @return the settlement schema type list
	 */
	public List<SettlementSchemaType> getSettlementSchemaTypeList(){
		return SettlementSchemaType.list;
	}

	/**
	 * Change mechanisn modality.
	 */
	public void changeMechanisnModality(){
		Long idNegotiationMechanism = monitorSettlementTO.getNegotiationMechanismPk();
		Integer settlementSchemaType = monitorSettlementTO.getSettlementSchemePk();
		if(idNegotiationMechanism !=null){
			monitorSettlementAttributes.setModalityGroupBySettSchemaList(mcnOperationServiceFacade.getModalityGroups(idNegotiationMechanism,settlementSchemaType));
		}
		
	}
	/**
	 * Change settlement schenma.
	 */
	public void changeSettlementSchenma(){
		Long idNegotiationMechanism = monitorSettlementTO.getNegotiationMechanismPk();
		Integer settlementSchemaType = monitorSettlementTO.getSettlementSchemePk();
		if(idNegotiationMechanism!=null){
			if(settlementSchemaType!=null){
				monitorSettlementAttributes.setModalityGroupBySettSchemaList(mcnOperationServiceFacade.getModalityGroups(idNegotiationMechanism,settlementSchemaType));
			}
		}
	}
	/**
	 * Search process modality.
	 * method to find all modality group with his settlemen process
	 */
	public void searchSettlementProcesses(){
		monitorSettlementAttributes.setSettlementProcSelected(null);
		monitorSettlementAttributes.setPartPositionList(null);
		monitorSettlementAttributes.setModSettlementSelected(null);
		monitorSettlementAttributes.setSelectedOperations(null);//clean operations selected
		monitorSettlementAttributes.setDisableSettlement(true);
		
		MonitorSettlementTO monitorSettlement = new MonitorSettlementTO();
		monitorSettlement.setSettlementDate(monitorSettlementTO.getSettlementDate());
		monitorSettlement.setNegotiationMechanismPk(monitorSettlementTO.getNegotiationMechanismPk());
		monitorSettlement.setCurrencyPk(monitorSettlementTO.getCurrencyPk());
		monitorSettlement.setModalityGroupPk(monitorSettlementTO.getModalityGroupPk());
		monitorSettlement.setSettlementSchemePk(monitorSettlementTO.getSettlementSchemePk());
		monitorSettlement.setIdParticipantPk(monitorSettlementTO.getIdParticipantPk());
		
		List<ModalitySettlementTO> modalitySettlementList = null;
		MonitoringAmountsTO monitoringAmounts = null;
		BeginEndDay beginEndDay = null;
		try {
			
			beginEndDay = monitorProcessFacade.getBeginEndDayFacade(monitorSettlementTO.getSettlementDate());

			if(monitorSettlement.getSettlementDate().equals(CommonsUtilities.currentDate()) && monitorSettlementTO.getIdParticipantPk()==null && beginEndDay!=null){
				if(beginEndDay.getIndSituation().equals(BooleanType.YES.getCode())){
					monitorSettlementAttributes.setDisableSettlement(false);
				}
			}
			
			modalitySettlementList = monitorProcessFacade.getModalitySettlementFacade(monitorSettlement);
			monitoringAmounts = monitorProcessFacade.getAmountsByState(modalitySettlementList, monitorSettlement);
			
			monitorSettlementAttributes.setModalitySettlementList(modalitySettlementList);
			monitorSettlementAttributes.setAmountsByModality(monitoringAmounts);
			
			if(beginEndDay!=null && !beginEndDay.getIndSituation().equals(BooleanType.YES.getCode())){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_DAY_CLOSE);
			}
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
			//e.printStackTrace();
		}
	}
	

	/**
	 * Gets the settlement description.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @return the settlement description
	 */
	private Object[] getSettlementDescription(Long idSettlementProcess) {
		Object[] params= null;
		if(idSettlementProcess!=null){
			params = new Object[]{idSettlementProcess.toString()};
		}else{
			String settlementSchema = monitorSettlementAttributes.getModSettlementSelected().getSettlementSchemeName(); 
			String mechanismName = monitorSettlementAttributes.getModSettlementSelected().getMechanismName();
			String modalityGroupName = monitorSettlementAttributes.getModSettlementSelected().getModalityGroupName();
			
			String currencyName = null;
			ParameterTable currency = monitorSettlementAttributes.getMapParameters().get(monitorSettlementTO.getCurrencyPk());
			if(currency!=null){
				currencyName = currency.getParameterName();
			}
			params = new Object[] {settlementSchema,mechanismName,modalityGroupName,currencyName};
		}
		
		return params;
	}
	
	/**
	 * method to restart settlement process
	 * Before restar settl process.
	 * method when click on restart process
	 *
	 * @param settlementProcess the settlement process
	 */
	public void beforeRestarSettlProcess(SettlementProcess settlementProcess){
		
		if(settlementProcess!=null){
			monitorSettlementAttributes.setSettlementProcSelected(settlementProcess);
			if(settlementProcess.getSettlementSchema().equals(SettlementSchemaType.GROSS.getCode())){ 
			   if(settlementProcess.getProcessState().equals(SettlementProcessStateType.STOPPED.getCode()) || settlementProcess.getProcessState().equals(SettlementProcessStateType.WAITING.getCode())){
				   showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
							PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_RESTART_CONFIRM,getSettlementDescription(settlementProcess.getIdSettlementProcessPk())));
					JSFUtilities.executeJavascriptFunction("PF('restartSettlementProcessDlgW').show()");
					return;
				}
			}else if(settlementProcess.getSettlementSchema().equals(SettlementSchemaType.NET.getCode())){
				if(settlementProcess.getProcessState().equals(SettlementProcessStateType.STARTED.getCode()) || settlementProcess.getProcessState().equals(SettlementProcessStateType.ERROR.getCode())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
							PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_RESTART_CONFIRM,getSettlementDescription(settlementProcess.getIdSettlementProcessPk())));
					JSFUtilities.executeJavascriptFunction("PF('restartSettlementProcessDlgW').show()");
					return;
				}
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_INCORRECT_PROCESS));
			JSFUtilities.showSimpleValidationDialog();
			
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Restar settlement process.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String restarSettlementProcess(){
		SettlementProcess settProcess = monitorSettlementAttributes.getSettlementProcSelected();
		ModalitySettlementTO modSelected = monitorSettlementAttributes.getModSettlementSelected();
		try{
			if(settProcess !=null && settProcess.getSettlementSchema().equals(SettlementSchemaType.GROSS.getCode())){
				
				//Call batch process to restart settlement process
				Map<String, Object> processParameters=new HashMap<String, Object>();
				processParameters.put(SettlementConstant.CURRENCY_PARAMETER, settProcess.getCurrency());
				processParameters.put(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, settProcess.getIdSettlementProcessPk());
				processParameters.put(SettlementConstant.MECHANISM_PARAMETER, modSelected.getMechanismId());
				processParameters.put(SettlementConstant.MODALITY_GROUP_PARAMETER, modSelected.getModalityGroupId());
				
				//change state "stoped" gross settlement process before to call batch settlement
				monitorProcessFacade.restartSettlementProcess(settProcess);
				
				BusinessProcess objBusinessProcess=new BusinessProcess();
				objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());
				//to send the batch process to start settlement
				monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);

				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_RESTART_SUCCESS,getSettlementDescription(settProcess.getIdSettlementProcessPk())));
				JSFUtilities.showSimpleValidationDialog();

				loadSettlementProcesses(monitorSettlementAttributes.getModSettlementSelected());
			}else if(settProcess !=null && settProcess.getSettlementSchema().equals(SettlementSchemaType.NET.getCode())){
//				if(settProcess != null && SettlementProcessStateType.WAITING.getCode().equals(settProcess.getProcessState())){
//					//now we have to execute the settlement process to GROSS settlement process for last time
//					BusinessProcess objBusinessProcess = new BusinessProcess();
//					objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode());
//					Map<String,Object> mpParameters= new HashMap<String,Object>();
//					mpParameters.put(SettlementConstant.MECHANISM_PARAMETER, modSelected.getMechanismId());
//					mpParameters.put(SettlementConstant.MODALITY_GROUP_PARAMETER, modSelected.getModalityGroupId());
//					mpParameters.put(SettlementConstant.CURRENCY_PARAMETER, settProcess.getCurrency());
//					mpParameters.put(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, settProcess.getIdSettlementProcessPk());
//					monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, mpParameters);
//
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
//							PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_RESTART_SUCCESS,getSettlementDescription(settProcess.getIdSettlementProcessPk())));
//					JSFUtilities.showSimpleValidationDialog();
//					
//				} else 
				if(settProcess != null && (SettlementProcessStateType.STARTED.getCode().equals(settProcess.getProcessState()) ||
						SettlementProcessStateType.ERROR.getCode().equals(settProcess.getProcessState())) ){
					netSettlementAttributesTO = new NetSettlementAttributesTO();
					Long settlementProcessId = settProcess.getIdSettlementProcessPk();
					SettlementProcess settlementProcess = monitorProcessFacade.restartNetSettlementProcess(netSettlementAttributesTO,settlementProcessId);
					loadParticipantByModalities();
					if(settlementProcess!=null){
						settlementProcessNetSession = settlementProcess;
						setViewOperationType(ViewOperationsType.MODIFY.getCode());
						return "beginNetSettlementRule";
					}
				} 
			}
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		return "";
	}
	
	/**
	 * Before stop settl process.
	 * method when user do click on stop process
	 *
	 * @param settlementProcess the settlement process
	 */
	public void beforeStopSettlProcess(SettlementProcess settlementProcess){
		if(settlementProcess!=null){
			if(settlementProcess.getSettlementSchema().equals(SettlementSchemaType.GROSS.getCode()) && 
			   settlementProcess.getProcessState().equals(SettlementProcessStateType.WAITING.getCode())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_STOP_CONFIRM, 
								getSettlementDescription(settlementProcess.getIdSettlementProcessPk())));
				JSFUtilities.executeJavascriptFunction("PF('stopSettlementProcessDlgW').show()");
				monitorSettlementAttributes.setSettlementProcSelected(settlementProcess);
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_INCORRECT_PROCESS));
			JSFUtilities.showSimpleValidationDialog();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	

	/**
	 * Metodo para renderizar o no la columna con el boton eliminar
	 * @return
	 */
	public Boolean deleteVerifyDate(){
		if(monitorSettlementTO.getSettlementDate().equals(CommonsUtilities.currentDate())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * boton eliminar proceso de Liquidacion
	 * @param settlementProcess
	 */
	
	public void beforeDeleteProcess(SettlementProcess settlementProcess){
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_DELETE_CONFIRM, 
							settlementProcess.getIdSettlementProcessPk().toString()));
			JSFUtilities.executeJavascriptFunction("PF('deleteSettlementProcessW').show()");
			monitorSettlementAttributes.setSettlementProcSelected(settlementProcess);
		return;
	}
	
	/**
	 * Borrar proceso de liquidacion y actualizar pantalla
	 */
	@LoggerAuditWeb
	public void deleteSettlementProcess(){
		SettlementProcess settProcess = monitorSettlementAttributes.getSettlementProcSelected();
		
			try {
				monitorProcessFacade.deleteSettlementProcess(settProcess.getIdSettlementProcessPk());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_DELETE_SUCCESS,
								settProcess.getIdSettlementProcessPk().toString()));
				JSFUtilities.showSimpleValidationDialog();
				loadSettlementProcesses(monitorSettlementAttributes.getModSettlementSelected());
			} catch (Exception e) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						"Error desconocido, verifique el log del servidor."
								);
				JSFUtilities.showSimpleValidationDialog();
			}
	}
	
	/**
	 * Before stop settlement process.
	 * method when user do click on stop process
	 *
	 * @param settlementProcess the settlement process
	 */
	public void beforeSendReportSettlProcess(SettlementProcess settlementProcess){
		if(settlementProcess != null){
			boolean isNetSettlements = Boolean.FALSE;
			if(monitorSettlementTO.getSettlementSchemePk() != null && SettlementSchemaType.NET.getCode().equals(monitorSettlementTO.getSettlementSchemePk() )) {
				isNetSettlements = Boolean.TRUE;
			}
			if(isNetSettlements) {
				if(settlementProcess.getProcessState().equals(SettlementProcessStateType.FINISHED.getCode())){					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
							PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_SENDREPORT_AUTOMATIC, 
									new Object[]{settlementProcess.getIdSettlementProcessPk(), settlementProcess.getScheduleTypeDescription()}));
					JSFUtilities.executeJavascriptFunction("PF('sendReportsDlgW').show()");
					monitorSettlementAttributes.setSettlementProcSelected(settlementProcess);
					return;					
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_INCORRECT_PROCESS));
					JSFUtilities.showSimpleValidationDialog();
				}
			} else {
				String strEtapa = GeneralConstants.EMPTY_STRING;
				if(Validations.validateIsNotNullAndNotEmpty(settlementProcess.getScheduleTypeDescription())) {
					strEtapa = settlementProcess.getScheduleTypeDescription();
				} else {
					strEtapa = GeneralConstants.N_A;
				}
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_SENDREPORT_AUTOMATIC, 
								new Object[]{settlementProcess.getIdSettlementProcessPk(), strEtapa}));
				JSFUtilities.executeJavascriptFunction("PF('sendReportsDlgW').show()");
				monitorSettlementAttributes.setSettlementProcSelected(settlementProcess);
				return;
			}			
			
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Send Reports Automatic settlement process.
	 */
	@LoggerAuditWeb
	public void sendReportsAutomaticSettlementProcess(){
		SettlementProcess settProcess = monitorSettlementAttributes.getSettlementProcSelected();
		try {			
			boolean isNetSettlements = Boolean.FALSE;
			if(monitorSettlementTO.getSettlementSchemePk() != null && SettlementSchemaType.NET.getCode().equals(monitorSettlementTO.getSettlementSchemePk() )) {
				isNetSettlements = Boolean.TRUE;
			}			
			if(isNetSettlements) {
				SettlementReportTO objSettlementReportTO = new SettlementReportTO();
				objSettlementReportTO.setCurrency(settProcess.getCurrency().toString());
				objSettlementReportTO.setDate(CommonsUtilities.convertDatetoString(settProcess.getSettlementDate()));
				objSettlementReportTO.setScheduleType(settProcess.getScheduleType().toString());	
										
				List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.netPositionsOperationsBagPreQuery(objSettlementReportTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
					logger.info("SEND :::: REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA ");
					settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.NET_POSITIONS_OPERATIONS_BAG_PRE.getCode());
				}
				
				List<SettlementReportTO> lstSettlementReportTODso= settlementReportFacade.detailSettledOperationsQuery(objSettlementReportTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTODso)){
					logger.info("SEND :::: REPORTE DE DETALLE DE OPERACIONES LIQUIDADAS ");
					settlementReportFacade.sendReports(lstSettlementReportTODso, ReportIdType.DETAILS_SETTLED_OPERATIONS.getCode());
				}
				
				List<SettlementReportTO> lstSettlementReportTOEsofb = settlementReportFacade.exlackSecurOperationsFundQuery(objSettlementReportTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTOEsofb)){
					logger.info("SEND :::: REPORTE DE EXCLUSION POR FALTA DE VALORES Y/O FONDOS DE OPERACION DE BOLSA ");
					settlementReportFacade.sendReports(lstSettlementReportTOEsofb, ReportIdType.EXLACK_SECURITY_OPER_FUND_BAG.getCode());
				}
				
				logger.info("INICIO :::: REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA REGISTRO DE OPERACIONES DIFERIDAS ");
				SettlementReportTO filter = new SettlementReportTO();
				filter.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1)));
				List<SettlementReportTO> lstSettlementReportTOPder = settlementReportFacade.netPositionsOperationsDefExRegQuery(filter);
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
					logger.info("SEND :::: REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA REGISTRO DE OPERACIONES DIFERIDAS ");
					logger.info("SEND :::: cantidadreportes :" + lstSettlementReportTOPder.size());
					settlementReportFacade.sendReports(lstSettlementReportTOPder, ReportIdType.NET_POSITIONS_OPERATIONS_DEF_EX_REG.getCode());
				}
				logger.info("FIN :::: REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA REGISTRO DE OPERACIONES DIFERIDAS ");
			} else {
				SettlementReportTO filter = new SettlementReportTO();
				filter.setInitialDate(CommonsUtilities.convertDatetoString(settProcess.getSettlementDate()));
				filter.setEndDate(CommonsUtilities.convertDatetoString(settProcess.getSettlementDate()));
				List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.purchaseForceExpirationSettledQuery(filter);
				logger.info("INICIO :::: REPORTE DE LIQUIDACIONES DE COMPRAS FORZOSAS, SELVE Y/O CAMBIO DE TIPO DE LIQUIDACION ESPECIAL ");
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
					logger.info("SEND :::: REPORTE DE LIQUIDACIONES DE COMPRAS FORZOSAS, SELVE Y/O CAMBIO DE TIPO DE LIQUIDACION ESPECIAL ");
					settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.PURCHASE_FORCE_EXPIRATION_SETTLED.getCode());
					
					lstSettlementReportTO = new ArrayList<SettlementReportTO>();
					SettlementReportTO objSettlementReportTO = new SettlementReportTO();
					objSettlementReportTO.setOnlyUser(Boolean.TRUE);
					objSettlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(settProcess.getSettlementDate()));
					objSettlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(settProcess.getSettlementDate()));
					lstSettlementReportTO.add(objSettlementReportTO);
					if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
						settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.PURCHASE_FORCE_EXPIRATION_SETTLED.getCode());
					}
				}
				logger.info("FIN :::: REPORTE DE LIQUIDACIONES DE COMPRAS FORZOSAS, SELVE Y/O CAMBIO DE TIPO DE LIQUIDACION ESPECIAL ");
				
				logger.info("INICIO :::: REPORTE LIQUIDADAS POR ESQUEMA BRUTO ");
				filter = new SettlementReportTO();
				filter.setDate(CommonsUtilities.convertDatetoString(settProcess.getSettlementDate()));
				List<SettlementReportTO> lstSettlementReportTOGrossSchema = settlementReportFacade.grossSchemeSettlementQuery(filter);
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTOGrossSchema)){
					logger.info("SEND :::: REPORTE LIQUIDADAS POR ESQUEMA BRUTO ");
					settlementReportFacade.sendReports(lstSettlementReportTOGrossSchema, ReportIdType.GROSS_SCHEME_SETTLEMENT.getCode());
				}
				logger.info("FIN :::: REPORTE LIQUIDADAS POR ESQUEMA BRUTO ");				
			}									
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_SUCCESS_SENDER_REPORT,
							new Object[]{settProcess.getIdSettlementProcessPk(), settProcess.getScheduleTypeDescription()}));
			JSFUtilities.showSimpleValidationDialog();
			
		} catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	
	/**
	 * Before unfulfillment process.
	 */
	public void beforeUnfulfillmentProcess(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_UNFULFILLMENT_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('unfulfillmentProcessDlgW').show()");
	}
	
	/**
	 * Before begin settlement process.
	 *
	 * @param modalitySettlement the modality settlement
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String beforeBeginSettlementProcess(ModalitySettlementTO modalitySettlement) throws ServiceException{
		monitorSettlementAttributes.setModSettlementSelected(modalitySettlement);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_START_CONFIRM,getSettlementDescription(null)));
		JSFUtilities.executeJavascriptFunction("PF('startSettlementProcessDlgW').show()");
		return null;
	}

	/**
	 * Begin edit settlement process.
	 *
	 * @param modalitySettlement the modality settlement
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beginEditSettlementProcess(ModalitySettlementTO modalitySettlement){
		netSettlementAttributesTO = null;
		monitorSettlementAttributes.setModSettlementSelected(modalitySettlement);
		try{
			if(monitorSettlementAttributes.getDailyExchangeRate()==null){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_EXCHANGE_RATE);
			}
					
			BigDecimal exchangeRate = monitorSettlementAttributes.getDailyExchangeRate().getBuyPrice();
			Date settlementDate = monitorSettlementTO.getSettlementDate();
			Integer currency = monitorSettlementTO.getCurrencyPk();
			Long mechanismId = modalitySettlement.getMechanismId();
			Long modalityGroupId = modalitySettlement.getModalityGroupId();
			Integer settlementSchema = modalitySettlement.getSettlementSchemeId();
			
			//validate if exist some settlement state not confirm request
			monitorProcessFacade.validateSettlemetStateRequest(settlementDate,currency);
			
			//validate if exist some settlement process pendit of finish
			monitorProcessFacade.validateStartSettlementProcessFacade(mechanismId,settlementDate,modalityGroupId,settlementSchema,currency);
			
			SettlementProcess settlementProcess = new SettlementProcess();
			settlementProcess.setSettlementDate(settlementDate);
			settlementProcess.setExchangeRate(exchangeRate);
			settlementProcess.setCurrency(currency);
			settlementProcess.setNegotiationMechanism(new NegotiationMechanism(mechanismId));
			settlementProcess.setModalityGroup(new ModalityGroup(modalityGroupId));
			settlementProcess.setSettlementSchema(settlementSchema);
			
			settlementProcessNetSession = settlementProcess;
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			return "beginNetSettlementRule";
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		return null;
	}
	
	/**
	 * to start the gross Settlement Process.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beginGrossSettlementProcess(){
		ModalitySettlementTO modSettlement = monitorSettlementAttributes.getModSettlementSelected();
		try{
			if(monitorSettlementAttributes.getDailyExchangeRate()==null){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_EXCHANGE_RATE);
			}
			
			Date settlementDate = monitorSettlementTO.getSettlementDate();
			Integer currency = monitorSettlementTO.getCurrencyPk();
			Long mechanismId = modSettlement.getMechanismId();
			Long modalityGroupId = modSettlement.getModalityGroupId();
			Integer settlementSchema = modSettlement.getSettlementSchemeId();
			
			if(settlementSchema.equals(SettlementSchemaType.GROSS.getCode())){//If settlement process is GROSS- Bruta
			
				//validate if exist some settlement process pendit of finish
				monitorProcessFacade.validateStartSettlementProcessFacade(mechanismId,settlementDate,modalityGroupId,settlementSchema,currency);
				
				Map<String, Object> processParameters=new HashMap<String, Object>();
				processParameters.put(SettlementConstant.MECHANISM_PARAMETER, mechanismId);
				processParameters.put(SettlementConstant.CURRENCY_PARAMETER, currency);
				processParameters.put(SettlementConstant.MODALITY_GROUP_PARAMETER, modalityGroupId);
				//processParameters.put(SettlementConstant.SCHEDULE_TYPE, null);
				
				BusinessProcess objBusinessProcess=new BusinessProcess();
				objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());	
				
				monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_START_SUCCESS,getSettlementDescription(null)));
			JSFUtilities.showSimpleValidationDialog();
			loadSettlementProcesses(modSettlement);
			
			}catch(ServiceException e){
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		return StringUtils.EMPTY;
	}
	
	 
	/**
	 * Stop settlement process.
	 */
	@LoggerAuditWeb
	public void stopSettlementProcess(){
		SettlementProcess settProcess = monitorSettlementAttributes.getSettlementProcSelected();
		try{
			settProcess = monitorProcessFacade.stopSettlementProcess(settProcess);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_STOP_SUCCESS,getSettlementDescription(settProcess.getIdSettlementProcessPk())));
			JSFUtilities.showSimpleValidationDialog();
			loadSettlementProcesses(monitorSettlementAttributes.getModSettlementSelected());
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Before close settl process.
	 *
	 * @param modSettlement the mod settlement
	 */
	public void beforeCloseSettlProcess(ModalitySettlementTO modSettlement){
		if(modSettlement!=null){
			if(modSettlement.getSettlementSchemeId().equals(SettlementSchemaType.GROSS.getCode())){
				monitorSettlementAttributes.setModSettlementSelected(modSettlement);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_CLOSE_CONFIRM,getSettlementDescription(null)));
				JSFUtilities.executeJavascriptFunction("PF('closeSettlementProcessDlgW').show()");
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_INCORRECT_PROCESS));
			JSFUtilities.showSimpleValidationDialog();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	
	/**
	 * Close settlement process.
	 */
	@LoggerAuditWeb
	public void closeSettlementProcess(){
		ModalitySettlementTO modSettlement = monitorSettlementAttributes.getModSettlementSelected();
		try{
			SettlementProcess settlementProcess = new SettlementProcess();
			
			settlementProcess.setSettlementDate(monitorSettlementTO.getSettlementDate());
			settlementProcess.setCurrency(monitorSettlementTO.getCurrencyPk());

			//Set some parameters from modality settlement
			settlementProcess.setNegotiationMechanism(new NegotiationMechanism(modSettlement.getMechanismId()));
			settlementProcess.setModalityGroup(new ModalityGroup(modSettlement.getModalityGroupId()));
			settlementProcess.setSettlementSchema(modSettlement.getSettlementSchemeId());
			
			Long idSettlementProcess = monitorProcessFacade.validateCloseSettlementFacade(settlementProcess);
			
			//now we have to execute the settlement process to GROSS settlement process for last time
			BusinessProcess objBusinessProcess = new BusinessProcess();
			objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());
			Map<String,Object> mpParameters= new HashMap<String,Object>();
			mpParameters.put(SettlementConstant.MECHANISM_PARAMETER, settlementProcess.getNegotiationMechanism().getIdNegotiationMechanismPk());
			mpParameters.put(SettlementConstant.MODALITY_GROUP_PARAMETER, settlementProcess.getModalityGroup().getIdModalityGroupPk());
			mpParameters.put(SettlementConstant.CURRENCY_PARAMETER, settlementProcess.getCurrency());
			mpParameters.put(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, idSettlementProcess);
			mpParameters.put(SettlementConstant.CLOSSING_SETTLEMENT_PROCESS, SettlementConstant.CLOSSING_SETTLEMENT_PROCESS);
			
			monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, mpParameters);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_CLOSE_SUCCESS,getSettlementDescription(null)));
			JSFUtilities.showSimpleValidationDialog();
			loadSettlementProcesses(modSettlement);//call method to reload Data on datagrid
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Before validate begin net sett process.
	 * method to begin settlement netted process
	 */
	public void beforeValidateBeginNetSettProcess(){//dialog to begin settlement net process
		try {
			
			monitorProcessFacade.validateStartSettlementProcessFacade(
					settlementProcessNetSession.getNegotiationMechanism().getIdNegotiationMechanismPk(),
					settlementProcessNetSession.getSettlementDate(),
					settlementProcessNetSession.getModalityGroup().getIdModalityGroupPk(),
					settlementProcessNetSession.getSettlementSchema(),
					settlementProcessNetSession.getCurrency());
			
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_START_CONFIRM,getSettlementDescription(null)));
		JSFUtilities.executeJavascriptFunction("PF('wvConfBeginNetProcess').show()");
	}
	
	/**
	 * Begin bet settlement process.
	 */
	@LoggerAuditWeb
	public void beginNetSettlementProcess(){
		try {
			
			netSettlementAttributesTO = new NetSettlementAttributesTO();
			
			Integer idCurrency =  settlementProcessNetSession.getCurrency();
			Long idModalityGroup = settlementProcessNetSession.getModalityGroup().getIdModalityGroupPk();
			Long idMechanism = settlementProcessNetSession.getNegotiationMechanism().getIdNegotiationMechanismPk();
			Date settlementDate = settlementProcessNetSession.getSettlementDate();
			Integer scheduleType = settlementProcessNetSession.getScheduleType();
			
			SettlementSchedule schedule = monitorProcessFacade.getSettlementSchedule(scheduleType, ComponentConstant.ONE);
			netSettlementAttributesTO.setSettlementSchedule(schedule);
			
			monitorProcessFacade.startNetSettlementProcess(idCurrency,idModalityGroup,idMechanism,settlementDate,netSettlementAttributesTO);
			
			settlementProcessNetSession = netSettlementAttributesTO.getSettlementProcess();
			
			loadParticipantByModalities();
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_START_SUCCESS,getSettlementDescription(null)));
			JSFUtilities.showSimpleValidationDialog();
			
		} catch (ServiceException e) {
			netSettlementAttributesTO = null;
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
			//e.printStackTrace();
		}
	}

	/**
	 * Before settlement net process.
	 */
	@LoggerAuditWeb
	public void beforeExecuteNetProcess(){
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_NET_EXECUTION_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('idConfNetProcessW').show()");
	}
	
	/**
	 * Execute net process.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void executeNetProcess() throws ServiceException{
		
		monitorProcessFacade.updateOperationSettlements(netSettlementAttributesTO.getSettlementProcess());
		
		Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(SettlementConstant.MECHANISM_PARAMETER, settlementProcessNetSession.getNegotiationMechanism().getIdNegotiationMechanismPk());
		processParameters.put(SettlementConstant.MODALITY_GROUP_PARAMETER, settlementProcessNetSession.getModalityGroup().getIdModalityGroupPk());
		processParameters.put(SettlementConstant.CURRENCY_PARAMETER, settlementProcessNetSession.getCurrency());
		processParameters.put(SettlementConstant.SCHEDULE_TYPE, settlementProcessNetSession.getScheduleType());
		processParameters.put(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, settlementProcessNetSession.getIdSettlementProcessPk());
		
		BusinessProcess objBusinessProcess=new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode());
		monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_NET_EXECUTION_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		
		cleanMonitorSettlement();
		
	}
	
	/**
	 * Before end edition.
	 *
	 * @param participantPosition the participant position
	 */
	
	/**
	 * Edits the net process handler.
	 *
	 * @param participantPosition the participant position
	 */
	public void beginEditNetPosition(NetParticipantPositionTO participantPosition) {
		
		netSettlementAttributesTO.setParticipantPositionToEdit(participantPosition);
		
		for(NetSettlementOperationTO settlementOperationTO : participantPosition.getLstSettlementOperation()){
			
			BigDecimal saleAmountOnOperation = NegotiationUtils.getAmountOnOperation(settlementOperationTO,participantPosition.getIdParticipant(),ComponentConstant.SALE_ROLE);
			BigDecimal purchaseAmountOnOperation = NegotiationUtils.getAmountOnOperation(settlementOperationTO,participantPosition.getIdParticipant(),ComponentConstant.PURCHARSE_ROLE);
			
			settlementOperationTO.setParticipantSaleAmount(saleAmountOnOperation);
			settlementOperationTO.setParticipantPurchaseAmount(purchaseAmountOnOperation);
			
			if(settlementOperationTO.getParticipantSaleAmount().compareTo(settlementOperationTO.getParticipantPurchaseAmount()) > 0){
				settlementOperationTO.setDisabled(Boolean.TRUE);
			}else{
				settlementOperationTO.setDisabled(Boolean.FALSE);
			}
			
			if(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode().equals(settlementOperationTO.getOperationSettlement().getOperationState())){
				settlementOperationTO.setSelected(Boolean.TRUE);
			}else{
				settlementOperationTO.setSelected(Boolean.FALSE);
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(participantPosition.getDisplayCodeDescription())){
			participantSelected = participantPosition.getDisplayCodeDescription();
		}
	}
	
	/**
	 * before Process Lip.
	 *
	 * @param objParticipantPosition the obj participant position
	 */
	public void beforeProcessLip(ParticipantPosition objParticipantPosition){
		monitorSettlementAttributes.getModSettlementSelected().setIdParticipantLIB(objParticipantPosition.getParticipant().getIdParticipantPk());
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_PROCESS_LIP_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('idProcessLipW').show()");
	}
	
	/**
	 * Process Lip.
	 *
	 * @return the string
	 * @throws ServiceException the Service Exception
	 */
	@LoggerAuditWeb
	public String processLip()throws ServiceException{
		try{
			settlementProcessFacade.sendFundsParticipantPositions(monitorSettlementAttributes.getModSettlementSelected().getIdSettlementProcess(), 
					monitorSettlementAttributes.getModSettlementSelected().getIdParticipantLIB(), 
					monitorSettlementAttributes.getModSettlementSelected().getMechanismId(), 
					monitorSettlementAttributes.getModSettlementSelected().getModalityGroupId(),
					monitorSettlementAttributes.getModSettlementSelected().getCurrency());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_PROCESS_LIP_SUCCESS));
			JSFUtilities.showSimpleValidationDialog();

		}catch (ServiceException e) {
			if(e.getErrorService()!=null){
				if(GeneralConstants.ASTERISK_STRING.equals(e.getMessage().substring(0, 1))){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR) , e.getMessage());
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				}
				JSFUtilities.showSimpleValidationDialog();
			}
		}		
		return StringUtils.EMPTY;
	}
	
	/**
	 * Withdrawn Funds Automatic.
	 *
	 * @throws ServiceException the Service Exception
	 */
	public void withdrawnFundsAutomatic() throws ServiceException{
		monitorProcessFacade.withdrawnFundsAutomatic(netSettlementAttributesTO, settlementProcessNetSession);
	}
	
	/**
	 * Withdrawn Funds Automatic.
	 * Version Original
	 *
	 * @throws ServiceException the Service Exception
	 */
	public void withdrawnFundsAutomatic2() throws ServiceException{
		
		Map<Long, NetParticipantPositionTO> mapParticipantPositions = NegotiationUtils.populateParticipantPositions(netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
		netSettlementAttributesTO.setLstParticipantPosition(null);
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		
		List<ParticipantPosition> newParticipantPositions = monitorProcessFacade.endEditAndRecalculateNetPositions(netSettlementAttributesTO, true);
		settlementProcessNetSession.setParticipantPositions(newParticipantPositions);
		netSettlementAttributesTO.setParticipantPositionToEdit(null);	
				
		if(Validations.validateListIsNotNullAndNotEmpty(netSettlementAttributesTO.getLstPendingSettlementOperations())){
			int countWithdrawn = 0;
			int countPending = netSettlementAttributesTO.getLstPendingSettlementOperations().size();
			for(NetSettlementOperationTO objNetSettlementOperationTO : netSettlementAttributesTO.getLstPendingSettlementOperations() ){				
				if(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode().equals(objNetSettlementOperationTO.getOperationSettlement().getOperationState())){
					countWithdrawn ++;
				}				
			}
			countPending = countPending - countWithdrawn;				
			netSettlementAttributesTO.getSettlementProcess().setPendingOperations(new Long(countPending));
		}		
		
		for(ParticipantPosition objParticipantPosition : settlementProcessNetSession.getParticipantPositions()){
			BigDecimal availableCashAmount = BigDecimal.ZERO , missingAmount = BigDecimal.ZERO;
			NetParticipantPositionTO objNetParticipantPositionTO = netSettlementAttributesTO.getMapParticipantPosition().get(objParticipantPosition.getParticipant().getIdParticipantPk());
			objNetParticipantPositionTO.setNetPosition(objParticipantPosition.getNetPosition());
			InstitutionCashAccount institutionCashAccount = collectionProcessServiceBean.getInstitutionCashAccount(
					settlementProcessNetSession.getNegotiationMechanism().getIdNegotiationMechanismPk(),settlementProcessNetSession.getModalityGroup().getIdModalityGroupPk()
					,objParticipantPosition.getParticipant().getIdParticipantPk(),settlementProcessNetSession.getCurrency(),SettlementSchemaType.NET.getCode());
			if(institutionCashAccount!=null){
				availableCashAmount = institutionCashAccount.getAvailableAmount();
			}
			objNetParticipantPositionTO.setAvailableCashAmount(availableCashAmount);	
			objNetParticipantPositionTO.setMissingAmount(BigDecimal.ZERO);
			missingAmount = objNetParticipantPositionTO.getNetPosition().add(objNetParticipantPositionTO.getAvailableCashAmount());
			if(missingAmount.compareTo(BigDecimal.ZERO) < 0){
				objNetParticipantPositionTO.setMissingAmount(missingAmount);
			}
		}
		
	}
	
	/**
	 * Beforewithdrawn funds automatic.
	 */
	public void beforewithdrawnFundsAutomatic(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_FUNDS_RETIREMENT_AUTOMATIC_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('idConfNetEditionProcessAutomaticW').show()");
	}
	
	/**
	 * Before end net edition.
	 */
	public void beforeEndNetEdition(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_NET_EDITION_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('idConfNetEditionProcessW').show()");
	}
	
	/**
	 * Cancel end edition.
	 */
	public void cancelEditNetPosition(){
		netSettlementAttributesTO.setParticipantPositionToEdit(null);
	}
	
	/**
	 * Net process end edition.
	 *
	 * @throws ServiceException the service exception
	 */
	public void endEditNetPosition() throws ServiceException{
		
		Map<Long, NetParticipantPositionTO> mapParticipantPosition= netSettlementAttributesTO.getMapParticipantPosition();
		Iterator<Long> it= mapParticipantPosition.keySet().iterator();
		while (it.hasNext()) {
			Long idParticipant= it.next();
			//we get the operation for each participant position
			List<NetSettlementOperationTO> lstParticipantOperations= mapParticipantPosition.get(idParticipant).getLstSettlementOperation();
			for(NetSettlementOperationTO settlementOperation: lstParticipantOperations){
				OperationSettlement operationSettlement = settlementOperation.getOperationSettlement();
				if(settlementOperation.isSelected()){
					operationSettlement.setOperationState(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode());
				}else{
					operationSettlement.setOperationState(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode());
				}
			}
		}
		
		Map<Long, NetParticipantPositionTO> mapParticipantPositions = NegotiationUtils.populateParticipantPositions(netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
		netSettlementAttributesTO.setLstParticipantPosition(null);
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		
		List<ParticipantPosition> nwParticipantPositions =  monitorProcessFacade.endEditAndRecalculateNetPositions(netSettlementAttributesTO,false);
		settlementProcessNetSession.setParticipantPositions(nwParticipantPositions);
		
		netSettlementAttributesTO.setParticipantPositionToEdit(null);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_NET_EDITION_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Load participant by modalities.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipantByModalities() throws ServiceException{
		
		Map<String,NetParticipantPositionTO> mapParticipantModality = new HashMap<String, NetParticipantPositionTO>();
		
		for(NetSettlementOperationTO settlementOperationTO : netSettlementAttributesTO.getLstGuaranteeRemovedOperations()){
			String key = new StringBuffer().append(settlementOperationTO.getIdModality()).append(settlementOperationTO.getIdSaleParticipant()).toString();
			NetParticipantPositionTO netParticipantPositionTO = mapParticipantModality.get(key); 
			if(netParticipantPositionTO == null){
				netParticipantPositionTO = new NetParticipantPositionTO();
				mapParticipantModality.put(key, netParticipantPositionTO);
			}
			netParticipantPositionTO.setIdParticipant(settlementOperationTO.getIdSaleParticipant());
			netParticipantPositionTO.setParticipantDescription(settlementOperationTO.getSaleParticipantDescription());
			netParticipantPositionTO.setParticipantNemonic(settlementOperationTO.getSaleParticipantNemonic());
			netParticipantPositionTO.setMechanismName(settlementOperationTO.getMechanismName());
			netParticipantPositionTO.setModalityName(settlementOperationTO.getModalityName());
			netParticipantPositionTO.getLstSettlementOperation().add(settlementOperationTO);
		}
		netSettlementAttributesTO.setLstGuaranteeParticipantPosition(new ArrayList<NetParticipantPositionTO>(mapParticipantModality.values()));
		
		mapParticipantModality = new HashMap<String, NetParticipantPositionTO>();
		
		for(NetSettlementOperationTO settlementOperationTO : netSettlementAttributesTO.getLstStockRemovedOperations()){
			String key = new StringBuffer().append(settlementOperationTO.getIdModality()).append(settlementOperationTO.getIdSaleParticipant()).toString();
			NetParticipantPositionTO netParticipantPositionTO = mapParticipantModality.get(key); 
			if(netParticipantPositionTO == null){
				netParticipantPositionTO = new NetParticipantPositionTO();
				mapParticipantModality.put(key, netParticipantPositionTO);
			}
			netParticipantPositionTO.setIdParticipant(settlementOperationTO.getIdSaleParticipant());
			netParticipantPositionTO.setParticipantDescription(settlementOperationTO.getSaleParticipantDescription());
			netParticipantPositionTO.setParticipantNemonic(settlementOperationTO.getSaleParticipantNemonic());
			netParticipantPositionTO.setMechanismName(settlementOperationTO.getMechanismName());
			netParticipantPositionTO.setModalityName(settlementOperationTO.getModalityName());
			netParticipantPositionTO.getLstSettlementOperation().add(settlementOperationTO);
		}
		netSettlementAttributesTO.setLstStockParticipantPosition(new ArrayList<NetParticipantPositionTO>(mapParticipantModality.values()));
		
	}
	
	/**
	 * Load operations selected.
	 *
	 * @param operationsListSelected the operations list selected
	 * @param modalitySettlement the modality settlement
	 */
	public void loadOperationsSelected(List<SettlementOperation> operationsListSelected, ModalitySettlementTO modalitySettlement){
		monitorSettlementAttributes.setSettlementProcSelected(null);
		//monitorSettlementAttributes.setPartPositionList(null);
		monitorSettlementAttributes.setModSettlementSelected(modalitySettlement);
		monitorSettlementAttributes.setShowSettlementProcess(Boolean.FALSE);
		monitorSettlementAttributes.setSelectedOperations(operationsListSelected);
	}
	
	/**
	 * Load operations remove selected.
	 *
	 * @param operationsListSelected the operations list selected
	 */
	public void loadOperationsRemoveSelected(List<SettlementOperation> operationsListSelected){
		//monitorSettlementAttributes.setPartPositionList(null);
		monitorSettlementAttributes.setSettlementProcSelected(null);
		monitorSettlementAttributes.setModSettlementSelected(null);
		monitorSettlementAttributes.setSelectedOperations(operationsListSelected);
		//monitorSettlementAttributes.setShowMotiveRemove(Boolean.TRUE);
	}
	
	/**
	 * Load total operation.
	 * method to load total operations from settlement Process
	 *
	 * @param settlementProcess the settlement process
	 */
	public void loadTotalOperation(SettlementProcess settlementProcess){
		removedOperation=false;
		if(settlementProcess.getTotalSettlementOperations()==null){
			try {
				settlementProcess = monitorProcessFacade.setOperationsSetProcessFacade(settlementProcess);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		monitorSettlementAttributes.setSelectedOperations(settlementProcess.getTotalSettlementOperations());
	}
	
	/**
	 * Load pendient operation.
	 * method to load pendient operations from settlement process
	 *
	 * @param settlementProcess the settlement process
	 */
	public void loadPendientOperation(SettlementProcess settlementProcess){
		removedOperation=false;
		if(settlementProcess.getTotalSettlementOperations()==null){
			try {
				settlementProcess = monitorProcessFacade.setOperationsSetProcessFacade(settlementProcess);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		monitorSettlementAttributes.setSelectedOperations(settlementProcess.getPendingSettlementOperations());
	}
	
	/**
	 * Load settlement operation.
	 *
	 * @param settlementProcess the settlement process
	 */
	public void loadSettlementOperation(SettlementProcess settlementProcess){
		removedOperation=false;
		if(settlementProcess.getTotalSettlementOperations()==null){
			try {
				settlementProcess = monitorProcessFacade.setOperationsSetProcessFacade(settlementProcess);	
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		monitorSettlementAttributes.setSelectedOperations(settlementProcess.getSettledSettlementOperations());
	}
	/**
	 * Load removed operation.
	 *
	 * @param settlementProcess the settlement process
	 */
	public void loadRemovedOperation(SettlementProcess settlementProcess){
		removedOperation=true;
		if(settlementProcess.getTotalSettlementOperations()==null){
			try {
				settlementProcess = monitorProcessFacade.setOperationsSetProcessFacade(settlementProcess);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		monitorSettlementAttributes.setSelectedOperations(settlementProcess.getRemovedSettlementOperations());
	}
	
	/**
	 * Load mec operations pendient by part.
	 *
	 * @param selectedPosition the selected position
	 */
	public void loadMecOperationsPendientByPart(NetParticipantPositionTO selectedPosition){
		netSettlementAttributesTO.setParticipantRemovedPosition(getAmountForView(selectedPosition));
	}
	
	/**
	 * Get Amount For View.
	 *
	 * @param selectedPosition selected Position
	 * @return Amount For View
	 */
	public NetParticipantPositionTO getAmountForView(NetParticipantPositionTO selectedPosition){		
		Long idParticipantAux = selectedPosition.getIdParticipant();
		for(NetSettlementOperationTO objNetSettlementOperationTO :selectedPosition.getLstSettlementOperation()){
			if(objNetSettlementOperationTO.getIdPurchaseParticipant().equals(idParticipantAux) && 
					objNetSettlementOperationTO.getIdSaleParticipant().equals(idParticipantAux)){
				objNetSettlementOperationTO.setParticipantPurchaseAmount(objNetSettlementOperationTO.getSettlementAmount());
				objNetSettlementOperationTO.setParticipantSaleAmount(objNetSettlementOperationTO.getSettlementAmount());
			}
			else if(objNetSettlementOperationTO.getIdPurchaseParticipant().equals(idParticipantAux)){
				if(objNetSettlementOperationTO.getOperationPart().equals(OperationPartType.CASH_PART.getCode())){
					objNetSettlementOperationTO.setParticipantPurchaseAmount(objNetSettlementOperationTO.getSettlementAmount());
					objNetSettlementOperationTO.setParticipantSaleAmount(BigDecimal.ZERO);
				}else{
					objNetSettlementOperationTO.setParticipantPurchaseAmount(BigDecimal.ZERO);
					objNetSettlementOperationTO.setParticipantSaleAmount(objNetSettlementOperationTO.getSettlementAmount());
				}				
			}else if(objNetSettlementOperationTO.getIdSaleParticipant().equals(idParticipantAux)){
				if(objNetSettlementOperationTO.getOperationPart().equals(OperationPartType.CASH_PART.getCode())){
					objNetSettlementOperationTO.setParticipantSaleAmount(objNetSettlementOperationTO.getSettlementAmount());
					objNetSettlementOperationTO.setParticipantPurchaseAmount(BigDecimal.ZERO);
				}else{
					objNetSettlementOperationTO.setParticipantSaleAmount(BigDecimal.ZERO);
					objNetSettlementOperationTO.setParticipantPurchaseAmount(objNetSettlementOperationTO.getSettlementAmount());
				}			
			}
		}
		return selectedPosition;		
	}
	/**
	 * Sets the monitor settlement attributes.
	 *
	 * @param monitorSettlementAttributes the new monitor settlement attributes
	 */
	public void setMonitorSettlementAttributes(MonitorSettlementAttributes monitorSettlementAttributes) {
		this.monitorSettlementAttributes = monitorSettlementAttributes;
	}
	
	public String showConfirmDialogUserPassword(){
		if(monitorCashAccountFacade.isSupervisorOrManagerProfile(userInfo.getUserAccountSession().getUserName())){
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogConfirmUser').show();");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage("text.lbl.message.user.no.autorized"));
			JSFUtilities.showSimpleValidationDialog();
		}
		return StringUtils.EMPTY;
	}
	
	@LoggerAuditWeb
	public String verifyUserAccount() throws MalformedURLException{
		if(monitorCashAccountFacade.isSupervisorOrManagerProfile(userInfo.getUserAccountSession().getUserName())){
			try {
				if(!monitorCashAccountFacade.validateUser(userInfo.getUserAccountSession().getUserName(),password)){
					this.password = null;
					//Lanzar dialogo de que la contreana no es correcta
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage("text.lbl.message.no.correct"));
					JSFUtilities.showSimpleValidationDialog();
					return null;
				}
				this.password = null;
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		executeUnfulfillmentProcess();
		return null;
	}
	
	/**
	 * Execute unfulfillment process.
	 */
	@LoggerAuditWeb
	public void executeUnfulfillmentProcess()
	{
		try {
			Long idMechanism= monitorSettlementTO.getNegotiationMechanismPk();
			//We execute the unfulfillment process for each modality
			BusinessProcess objBusinessProcess = new BusinessProcess();
			objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.UNFULFILLMENT_IDENTIFICATION.getCode());
			Map<String,Object> mpParameters= new HashMap<String,Object>();
			if(idMechanism!=null){
				mpParameters.put(SettlementConstant.MECHANISM_PARAMETER, idMechanism);
				mpParameters.put(SettlementConstant.CURRENCY_PARAMETER, monitorSettlementTO.getCurrencyPk());
			}
			monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, mpParameters);
			
			//rechaza solicitudes de liquidacion con moneda distinta		
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_AUTOMATIC_REJECT.getCode());
			monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, new HashMap<String, Object>());
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_UNFULFILLMENT_SUCCESS));
			JSFUtilities.showSimpleValidationDialog();
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Monitore mod process.
	 *
	 * @param modalitySettlement the modality settlement
	 */
	public void loadSettlementProcesses(ModalitySettlementTO modalitySettlement){
		Integer currency = monitorSettlementTO.getCurrencyPk();
		Date settlementDate = monitorSettlementTO.getSettlementDate();

		Long participantCode = monitorSettlementTO.getIdParticipantPk();
		
		List<SettlementProcess> settlementProcList = null;
		try{
			settlementProcList = monitorProcessFacade.getSettlementProcess(modalitySettlement.getMechanismId(), modalitySettlement.getModalityGroupId(), settlementDate, currency,participantCode);
			
			for(SettlementProcess processSett: settlementProcList){
				processSett.setParticipantCode(participantCode);
				ParameterTable param = null;
				param =  monitorSettlementAttributes.getMapParameters().get(processSett.getScheduleType());
				if(param!=null){
					processSett.setScheduleTypeDescription(param.getParameterName());
				}
				
				param =  monitorSettlementAttributes.getMapParameters().get(processSett.getProcessState());
				if(param!=null){
					processSett.setStateDescription(param.getParameterName());
				}
			}
			
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
		modalitySettlement.setSettProcessList(settlementProcList);
		modalitySettlement.setCurrency(currency);
		monitorSettlementAttributes.setModSettlementSelected(modalitySettlement);
		monitorSettlementAttributes.setSelectedOperations(null);
		monitorSettlementAttributes.setShowSettlementProcess(Boolean.TRUE);
		
		if(modalitySettlement.getSettlementSchemeId().equals(SettlementSchemaType.GROSS.getCode())){
			monitorSettlementAttributes.setIsNetSettlement(Boolean.FALSE);
		}else if(modalitySettlement.getSettlementSchemeId().equals(SettlementSchemaType.NET.getCode())){
			monitorSettlementAttributes.setIsNetSettlement(Boolean.TRUE);
		}
		
		// Verify update progress bar
		blProgressBar = verifyUpdateProgressBar();	
		modalitySettlementSelected = modalitySettlement;
		
	}
		
	/**
	 * Verify update progress bar.
	 *
	 * @return true, if successful
	 */
	public boolean verifyUpdateProgressBar() {
		boolean blStatus = false;
		if(monitorSettlementAttributes != null && monitorSettlementAttributes.getModSettlementSelected() != null 
				&& Validations.validateListIsNotNullAndNotEmpty(monitorSettlementAttributes.getModSettlementSelected().getSettProcessList())) {
			for(SettlementProcess objSettlementProcess : monitorSettlementAttributes.getModSettlementSelected().getSettProcessList()) {
				if(SettlementProcessStateType.IN_PROCESS.getCode().equals(objSettlementProcess.getProcessState())) {
					blStatus = true;
					break;
				}
			}
		}
		return blStatus;
	}
		
	
	/**
	 * On complete.
	 */
	public void onComplete() {
		if(modalitySettlementSelected != null) {
			loadSettlementProcesses(modalitySettlementSelected);			
		}				
	}
	
	/**
	 * Load participant positions.
	 *
	 * @param settlementProcess the settlement process
	 * @throws ServiceException the service exception
	 */
	public void loadParticipantPositions(SettlementProcess settlementProcess) throws ServiceException{
		//clean operations on fieldSet
		monitorSettlementAttributes.setSelectedOperations(null);
		
		Long partCode = monitorSettlementTO.getIdParticipantPk();
		
		//if, is null, participant position is not in the object
		List<ParticipantPosition> partPositionList = monitorProcessFacade.getParticipantPositions(settlementProcess.getIdSettlementProcessPk(),partCode);

		monitorSettlementAttributes.setPartPositionList(partPositionList);
		
		monitorSettlementAttributes.getModSettlementSelected().setIdSettlementProcess(settlementProcess.getIdSettlementProcessPk());
	}
	/**
	 * Load mechanism operation.
	 *
	 * @param mechanismOperationId the mechanism operation id
	 */
	public void loadMechanismOperation(Long mechanismOperationId){
		operationBusinessComp.setIdMechanismOperationPk(mechanismOperationId);
		operationBusinessComp.setShowAssignments(true);
		operationBusinessComp.searchMechanismOperation();
	}
	/**
	 * Clean operations detail net process.
	 * method to clean, operation on reteirement net process
	 */
	public void cleanOperationsDetailNetProcess(){
		netSettlementAttributesTO.setParticipantRemovedPosition(null);
		//monitorSettlementAttributes.setMechanismOperationsByPart(null);
	}
	
	/**
	 * Verify chain on multiple operations.
	 *
	 * @param objNetSettlementOperationTO the obj net settlement operation to
	 */
	public void verifyChainOnMultipleOperations(NetSettlementOperationTO objNetSettlementOperationTO){
		if(Validations.validateIsNull(objNetSettlementOperationTO)){
			for (NetSettlementOperationTO objSettlementOperationTO : netSettlementAttributesTO.getParticipantPositionToEdit().getLstSettlementOperation()) {
				
				if(netSettlementAttributesTO.getParticipantPositionToEdit().isCheckAll()){
					if(!objSettlementOperationTO.isSelected() && !objSettlementOperationTO.isDisabled()){
						objSettlementOperationTO.setSelected(true);
						verifyChainOperation(objSettlementOperationTO,true);
						if(lstIdChainedHolderOperation!=null){
							List<String> ballotSequentials = new ArrayList<String>();
							for(NetSettlementOperationTO selectedSettlementOperationTO : netSettlementOperationTO){
								ballotSequentials.add(selectedSettlementOperationTO.getBallotSequential());
							}
							
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
									PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_CHAIN_OPERATION_CONFIRM, 
											new Object[]{CommonsUtilities.concatWithBreakLine(ballotSequentials.toArray(), 5)} ) );
							JSFUtilities.executeJavascriptFunction("PF('idVerifyChainOperationW').show()");
						}
					}
				} else {
					if(objSettlementOperationTO.isSelected() && !objSettlementOperationTO.isDisabled()){
						objSettlementOperationTO.setSelected(false);
						verifyChainOperation(objSettlementOperationTO,true);
					}
				}
			}
		} else {
			verifyChainOperation(objNetSettlementOperationTO,false);
		}
	}
	
	/**
	 * Verify chain operation.
	 *
	 * @param objNetSettlementOperationTO the obj net settlement operation to
	 * @param checkAll the check all
	 */
	public void verifyChainOperation(NetSettlementOperationTO objNetSettlementOperationTO, boolean checkAll) 
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			if (objNetSettlementOperationTO.isSelected()) {
				if (StringUtils.equalsIgnoreCase(BooleanType.YES.getValue(), objNetSettlementOperationTO.getIndChainedHolderOperation())) {
					netSettlementOperationTO.add(objNetSettlementOperationTO);
					if(!checkAll){
						//this operation belongs to chain, the we get the chains to be removed
						lstIdChainedHolderOperation= monitorProcessFacade.getListChainSettlementOperation(objNetSettlementOperationTO.getIdSettlementOperation(),null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_CHAIN_OPERATION_CONFIRM, new Object[]{objNetSettlementOperationTO.getBallotSequential()}));
						JSFUtilities.executeJavascriptFunction("PF('idVerifyChainOperationW').show()");
					}else{
						List<Long> tmpLstIdChainedHolderOperation= monitorProcessFacade.getListChainSettlementOperation(objNetSettlementOperationTO.getIdSettlementOperation(),null);
						if(lstIdChainedHolderOperation!=null){
							lstIdChainedHolderOperation.addAll(tmpLstIdChainedHolderOperation);
						}else{
							lstIdChainedHolderOperation = tmpLstIdChainedHolderOperation;
						}
					}
				}
			} else {
				if (StringUtils.equalsIgnoreCase(BooleanType.YES.getValue(), objNetSettlementOperationTO.getIndChainedHolderOperation())) {
					//we get the chains inside the operation
					lstIdChainedHolderOperation= monitorProcessFacade.getListChainSettlementOperation(objNetSettlementOperationTO.getIdSettlementOperation(),null);
					for (Long idChainedHolderOperation: lstIdChainedHolderOperation) {
						//we update the selection of the operations for each chain
						updateChainedOperations(idChainedHolderOperation, null, netSettlementAttributesTO.getMapParticipantPosition(), false);
					}
				}
				lstIdChainedHolderOperation= null;
				netSettlementAttributesTO.getParticipantPositionToEdit().updateCheckAllSettlementEditions();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the chain operation selection.
	 */
	public void removeChainOperationSelection()
	{
		try {
			for (Long idChainedHolderOperation: lstIdChainedHolderOperation) {
				//we remove the operations for each chain
				updateChainedOperations(idChainedHolderOperation, null, netSettlementAttributesTO.getMapParticipantPosition(), true);
			}
			lstIdChainedHolderOperation= null;
			netSettlementOperationTO.clear();
			netSettlementAttributesTO.getParticipantPositionToEdit().updateCheckAllSettlementEditions();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean selected net settlement operation.
	 */
	public void cleanSelectedNetSettlementOperation() 
	{
		for(NetSettlementOperationTO objSettlementOperationTO : netSettlementOperationTO){
			objSettlementOperationTO.setSelected(false);
		}
		lstIdChainedHolderOperation = null;
		netSettlementOperationTO.clear();
		netSettlementAttributesTO.getParticipantPositionToEdit().updateCheckAllSettlementEditions();
	}
	
	
	/**
	 * Update chained operations.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param idSettlementOperation the id settlement operation
	 * @param mapParticipantOperations the map participant operations
	 * @param selectState the select state
	 * @throws ServiceException the service exception
	 */
	private void updateChainedOperations(Long idChainedHolderOperation, Long idSettlementOperation, 
										Map<Long, NetParticipantPositionTO> mapParticipantOperations, boolean selectState) throws ServiceException
	{
		logger.info(" REMOVING CHAIN: "+idChainedHolderOperation);
		List<Long> lstIdSettlementOperationToRemove= monitorProcessFacade.getListSettlementOperationByChain(idChainedHolderOperation, idSettlementOperation);
		//we removed all operation of this chain
		Iterator<Long> itPositions= mapParticipantOperations.keySet().iterator();
		//we look up for all operations in all participant positions 
		while (itPositions.hasNext()) {
			Long idParticipant= itPositions.next();
			List<NetSettlementOperationTO> lstParticipantOperations= mapParticipantOperations.get(idParticipant).getLstSettlementOperation();
			//for each participant position we get the chained operation to be removed 
			for (NetSettlementOperationTO objNetSettlementOperationTO: lstParticipantOperations) {
				for (Long idSettlementOperationToRemove: lstIdSettlementOperationToRemove) {
					//we look up the chained operation inside the participant position
					if (idSettlementOperationToRemove.equals(objNetSettlementOperationTO.getIdSettlementOperation())) {
						if (objNetSettlementOperationTO.isSelected() != selectState) {
							objNetSettlementOperationTO.setSelected(selectState);
							logger.info(" --- REMOVING OPERATION: "+idChainedHolderOperation+"-"+idSettlementOperationToRemove);
							//now we verify if this operation belongs to others different chain
							List<Long> lstIdChainedHolderOperation= monitorProcessFacade.getListChainSettlementOperation(
																						idSettlementOperationToRemove, idChainedHolderOperation);
							if (Validations.validateListIsNotNullAndNotEmpty(lstIdChainedHolderOperation)) {
								for (Long idChainedHolderOperationToRemove: lstIdChainedHolderOperation) {
									//we remove the operation of these chains
									updateChainedOperations(idChainedHolderOperationToRemove, idSettlementOperationToRemove, mapParticipantOperations, selectState);
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	/**
	 * Clean monitor settlement.
	 */
	public void cleanMonitorSettlement(){
		monitorSettlementTO = new MonitorSettlementTO();
		monitorSettlementTO.setSettlementDate(CommonsUtilities.currentDate());
		monitorSettlementTO.setNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
		monitorSettlementTO.setSettlementSchemePk(SettlementSchemaType.NET.getCode());
		monitorSettlementTO.setCurrencyPk(CurrencyType.PYG.getCode());
		monitorSettlementAttributes.setSettlementProcSelected(null);
		monitorSettlementAttributes.setModSettlementSelected(null);
		monitorSettlementAttributes.setSelectedOperations(null);//clean operations selected
		monitorSettlementAttributes.setDisableSettlement(true);
		monitorSettlementAttributes.setModalitySettlementList(null);
		monitorSettlementAttributes.setAmountsByModality(null);
		monitorSettlementAttributes.setPartPositionList(null);
	}

	/**
	 * Gets the mechanism operacion id.
	 *
	 * @return the mechanism operacion id
	 */
	public Long getMechanismOperacionId() {
		return mechanismOperacionId;
	}

	/**
	 * Sets the mechanism operacion id.
	 *
	 * @param mechanismOperacionId the new mechanism operacion id
	 */
	public void setMechanismOperacionId(Long mechanismOperacionId) {
		this.mechanismOperacionId = mechanismOperacionId;
	}
	
	/**
	 * Gets the settlement process net session.
	 *
	 * @return the settlement process net session
	 */
	public SettlementProcess getSettlementProcessNetSession() {
		return settlementProcessNetSession;
	}
	/**
	 * Sets the settlement process net session.
	 *
	 * @param settlementProcessNetSession the new settlement process net session
	 */
	public void setSettlementProcessNetSession(SettlementProcess settlementProcessNetSession) {
		this.settlementProcessNetSession = settlementProcessNetSession;
	}
	/**
	 * Gets the monitor sett selected.
	 *
	 * @return the monitor sett selected
	 */
	public MonitorSettlementTO getMonitorSettSelected() {
		return monitorSettSelected;
	}
	/**
	 * Sets the monitor sett selected.
	 *
	 * @param monitorSettSelected the new monitor sett selected
	 */
	public void setMonitorSettSelected(MonitorSettlementTO monitorSettSelected) {
		this.monitorSettSelected = monitorSettSelected;
	}
	/**
	 * Gets the monitor settlement attributes.
	 *
	 * @return the monitor settlement attributes
	 */
	public MonitorSettlementAttributes getMonitorSettlementAttributes() {
		return monitorSettlementAttributes;
	}	
	/**
	 * Gets the monitor settlement to.
	 *
	 * @return the monitor settlement to
	 */
	public MonitorSettlementTO getMonitorSettlementTO() {
		return monitorSettlementTO;
	}	
	/**
	 * Sets the monitor settlement to.
	 *
	 * @param monitorSettlementTO the new monitor settlement to
	 */
	public void setMonitorSettlementTO(MonitorSettlementTO monitorSettlementTO) {
		this.monitorSettlementTO = monitorSettlementTO;
	}

	/**
	 * Gets the net settlement attributes to.
	 *
	 * @return the net settlement attributes to
	 */
	public NetSettlementAttributesTO getNetSettlementAttributesTO() {
		return netSettlementAttributesTO;
	}

	/**
	 * Sets the net settlement attributes to.
	 *
	 * @param netSettlementAttributesTO the new net settlement attributes to
	 */
	public void setNetSettlementAttributesTO(
			NetSettlementAttributesTO netSettlementAttributesTO) {
		this.netSettlementAttributesTO = netSettlementAttributesTO;
	}

	/**
	 * Gets the lst id chained holder operation.
	 *
	 * @return the lst id chained holder operation
	 */
	public List<Long> getLstIdChainedHolderOperation() {
		return lstIdChainedHolderOperation;
	}

	/**
	 * Sets the lst id chained holder operation.
	 *
	 * @param lstIdChainedHolderOperation the new lst id chained holder operation
	 */
	public void setLstIdChainedHolderOperation(
			List<Long> lstIdChainedHolderOperation) {
		this.lstIdChainedHolderOperation = lstIdChainedHolderOperation;
	}

	/**
	 * Checks if is removed operation.
	 *
	 * @return true, if is removed operation
	 */
	public boolean isRemovedOperation() {
		return removedOperation;
	}

	/**
	 * Sets the removed operation.
	 *
	 * @param removedOperation the new removed operation
	 */
	public void setRemovedOperation(boolean removedOperation) {
		this.removedOperation = removedOperation;
	}

	/**
	 * Checks if is bl progress bar.
	 *
	 * @return the blProgressBar
	 */
	public boolean isBlProgressBar() {
		return blProgressBar;
	}

	/**
	 * Sets the bl progress bar.
	 *
	 * @param blProgressBar the blProgressBar to set
	 */
	public void setBlProgressBar(boolean blProgressBar) {
		this.blProgressBar = blProgressBar;
	}

	/**
	 * Gets the modality settlement selected.
	 *
	 * @return the modalitySettlementSelected
	 */
	public ModalitySettlementTO getModalitySettlementSelected() {
		return modalitySettlementSelected;
	}

	/**
	 * Sets the modality settlement selected.
	 *
	 * @param modalitySettlementSelected the modalitySettlementSelected to set
	 */
	public void setModalitySettlementSelected(
			ModalitySettlementTO modalitySettlementSelected) {
		this.modalitySettlementSelected = modalitySettlementSelected;
	}

	/**
	 * Gets the participant selected.
	 *
	 * @return the participantSelected
	 */
	public String getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the participantSelected to set
	 */
	public void setParticipantSelected(String participantSelected) {
		this.participantSelected = participantSelected;
	}	
	
	/**Pasword*/
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}	
	
}