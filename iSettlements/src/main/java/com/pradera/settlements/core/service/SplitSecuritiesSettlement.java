package com.pradera.settlements.core.service;
//package com.pradera.settlements.service;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.ejb.EJB;
//import javax.ejb.Stateless;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.pradera.commons.type.BusinessProcessType;
//import com.pradera.commons.utils.CommonsUtilities;
//import com.pradera.core.component.business.service.ExecutorComponentServiceBean;
//import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
//import com.pradera.core.component.securities.service.SecuritiesServiceBean;
//import com.pradera.integration.common.type.BooleanType;
//import com.pradera.integration.common.validation.Validations;
//import com.pradera.integration.component.business.ComponentConstant;
//import com.pradera.integration.component.business.to.AccountsComponentTO;
//import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
//import com.pradera.integration.component.business.to.SecuritiesComponentTO;
//import com.pradera.integration.contextholder.LoggerUser;
//import com.pradera.integration.exception.ServiceException;
//import com.pradera.model.component.SecuritiesOperation;
//import com.pradera.model.component.type.ParameterOperationType;
//import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
//import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
//import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
//import com.pradera.model.issuancesecuritie.Security;
//import com.pradera.model.issuancesecuritie.SecurityForeignDepository;
//import com.pradera.model.issuancesecuritie.SecurityInterestCoupon;
//import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
//import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;
//import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
//import com.pradera.model.issuancesecuritie.type.SecurityStateType;
//import com.pradera.model.negotiation.TradeOperation;
//import com.pradera.model.negotiation.type.MechanismOperationStateType;
//import com.pradera.settlements.to.SettlementHolderAccountTO;
//import com.pradera.settlements.to.SettlementOperationTO;
//
//@Stateless
//public class SplitSecuritiesSettlement extends CrudDaoServiceBean {
//
//	private static final  Logger logger = LoggerFactory.getLogger(SplitSecuritiesSettlement.class);
//	
//	private static final String WITHOUT_COUPON= " (SIN C";
//	private static final String CLOSE_PARENTESIS= ")";
//	private static final String COUPON= " - CUPON ";
//	
//	@EJB
//	private SecuritiesServiceBean securitiesServiceBean;
//	
//	@EJB
//	private SettlementProcessServiceBean settlementProcessServiceBean;
//	
//	@EJB
//	private ExecutorComponentServiceBean executorComponentServiceBean;
//	
//	
//	public void settleSplitCouponOperation(SettlementOperationTO objSettlementOperationTO, LoggerUser loggerUser) throws ServiceException
//	{
//		try {
//			/*
//			 * STEP 01: TO CREATE THE CHILD SECURITIES FROM PARENT SECURITIES
//			 * - to update the indicator to has split from securities
//			 * - to create the capital securities
//			 * - to create the coupon securities
//			 */
//			Security objSourceSecurities = securitiesServiceBean.getSecuritiesById(objSettlementOperationTO.getIdIsinCode());
//			String idParentIsinCode= null;
//			ProgramInterestCoupon objProgramInterestCoupon= securitiesServiceBean.getProgramInterestCoupon(objSettlementOperationTO.getIdProgramInterestCoupon());
//			//we allocate the parent security
//			if (objSourceSecurities.getSecurity() != null) {
//				//this is a capital securities
//				idParentIsinCode= objSourceSecurities.getSecurity().getIdIsinCodePk();
//			} else {
//				//this is the main security
//				idParentIsinCode= objSourceSecurities.getIdIsinCodePk();
//			}
//			// - to update the indicator to has split from securities
//			securitiesServiceBean.updateSplitParentSecurities(objSourceSecurities.getIdIsinCodePk(), loggerUser);
//			
//			// - to create the capital securities
//			Security objCapitalSecurities= createSplitSecurities(objSourceSecurities, objProgramInterestCoupon, idParentIsinCode, 
//																		objSettlementOperationTO.getStockQuantity(), ComponentConstant.ZERO, loggerUser);
//			
//			//- to create the coupon securities
//			Security objCouponSecurities= null;
//			//we create the coupon securities only if it wasn't created before
//			if (Validations.validateIsNull(objProgramInterestCoupon.getSplitSecurities())) {
//				objCouponSecurities= createSplitSecurities(objSourceSecurities, objProgramInterestCoupon, idParentIsinCode, 
//																objSettlementOperationTO.getStockQuantity(), ComponentConstant.ONE, loggerUser);
//				objProgramInterestCoupon.setSplitSecurities(objCouponSecurities);
//				this.update(objProgramInterestCoupon);
//			} else {
//				objCouponSecurities= objProgramInterestCoupon.getSplitSecurities();
//			}
//			
//			/*	
//			 * STEP 02: TO EXECUTE SETTLEMENT SPLIT TO GENERATE THE WITHDRAWAL OF PARENT SECURITIES
//			 * - to execute the withdrawal by detachment from parent securities (seller)
//			 */
//			executeDetachmentSecurities(objSettlementOperationTO, objSourceSecurities, ComponentConstant.SOURCE);
//			
//			/*
//			 * STEP 03: TO EXECUTE THE SETTLEMENT SPLIT TO GENERATE THE MOVEMENTS OF SPLIT SECURITES
//			 * - to execute the income by detachment from capital securities (seller)
//			 * - to execute the income by detachment from coupon securities (seller)
//			 * - to execute the withdrawal by settlement from coupon securities (seller)
//			 * - to execute the income by settlement from coupon securities (buyer)
//			 */
//			
//			//- to execute the income by detachment from capital securities (seller)
//			executeDetachmentSecurities(objSettlementOperationTO, objCapitalSecurities, ComponentConstant.TARGET);
//			
//			//- to execute the income by detachment from coupon securities (seller)
//			executeDetachmentSecurities(objSettlementOperationTO, objCouponSecurities, ComponentConstant.TARGET);
//			
//			//- to execute the withdrawal by settlement from coupon securities (seller)
//			//- to execute the income by settlement from coupon securities (buyer)
//			executeSettlementCouponSecurities(objSettlementOperationTO, objCouponSecurities.getIdIsinCodePk());
//			
//			settlementProcessServiceBean.updateMechanisOperationState(objSettlementOperationTO.getIdMechanismOperation(), 
//																	MechanismOperationStateType.CASH_SETTLED.getCode(), loggerUser, false);
//		} catch (ServiceException e) {
//			logger.error("idMechanismOperation: " +objSettlementOperationTO.getIdMechanismOperation()
//								+", idProgramInterestCoupon: " +objSettlementOperationTO.getIdProgramInterestCoupon());
//			throw e;
//		}
//	}
//	
//	private void executeDetachmentSecurities(SettlementOperationTO objSettlementOperationTO, Security objSplitSecurity, Long indSourceTarget) throws ServiceException
//	{
//		//FIRST execute the account component to affect the balances from holder account
//		AccountsComponentTO objAccountsComponentTO= new AccountsComponentTO();
//		objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());
//		objAccountsComponentTO.setIdOperationType(ParameterOperationType.SPLIT_SECURITIES_DETACHMENT.getCode());
//		objAccountsComponentTO.setIdTradeOperation(objSettlementOperationTO.getIdMechanismOperation());
//		List<HolderAccountBalanceTO> lstSellerAccountBalanceTO = populateHolderAccountBalance(objSettlementOperationTO.getLstSellerHolderAccounts(), 
//																											objSplitSecurity.getIdIsinCodePk());
//		if (ComponentConstant.SOURCE.equals(indSourceTarget))
//			objAccountsComponentTO.setLstSourceAccounts(lstSellerAccountBalanceTO);
//		else if (ComponentConstant.TARGET.equals(indSourceTarget))
//			objAccountsComponentTO.setLstTargetAccounts(lstSellerAccountBalanceTO);
//		executorComponentServiceBean.executeAccountsComponent(objAccountsComponentTO);
//		
//		//SECOND execute the securities component to affect the balance from securities
//		SecuritiesOperation objSecuritiesOperation = new SecuritiesOperation();
//		objSecuritiesOperation.setCashAmount(objSplitSecurity.getCurrentNominalValue().multiply(objSettlementOperationTO.getStockQuantity()));
//		objSecuritiesOperation.setOperationDate(CommonsUtilities.currentDateTime());
//		objSecuritiesOperation.setOperationType(ParameterOperationType.SEC_SPLIT_SECURITIES_DETACHMENT.getCode());
//		objSecuritiesOperation.setSecurities(objSplitSecurity);
//		objSecuritiesOperation.setStockQuantity(objSettlementOperationTO.getStockQuantity());
//		TradeOperation objTradeOperation= new TradeOperation();
//		objTradeOperation.setIdTradeOperationPk(objSettlementOperationTO.getIdMechanismOperation());
//		objSecuritiesOperation.setTradeOperation(objTradeOperation);
//		this.create(objSecuritiesOperation);
//		
//		SecuritiesComponentTO objSecuritiesComponentTO = new SecuritiesComponentTO();
//		objSecuritiesComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());
//		objSecuritiesComponentTO.setIdOperationType(ParameterOperationType.SEC_SPLIT_SECURITIES_DETACHMENT.getCode());
//		objSecuritiesComponentTO.setIdIsinCode(objSplitSecurity.getIdIsinCodePk());
//		objSecuritiesComponentTO.setDematerializedBalance(objSettlementOperationTO.getStockQuantity());
//		objSecuritiesComponentTO.setPhysicalBalance(objSettlementOperationTO.getStockQuantity());
//		objSecuritiesComponentTO.setIdSecuritiesOperation(objSecuritiesOperation.getIdSecuritiesOperationPk());
//		objSecuritiesComponentTO.setIndSourceTarget(indSourceTarget);
//		executorComponentServiceBean.executeSecuritiesComponent(objSecuritiesComponentTO);
//	}
//	
//	private void executeSettlementCouponSecurities(SettlementOperationTO objSettlementOperationTO, String idCouponIsinCode) throws ServiceException
//	{
//		AccountsComponentTO objAccountsComponentTO= new AccountsComponentTO();
//		objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());
//		objAccountsComponentTO.setIdOperationType(ParameterOperationType.COUPON_SPLIT_FIXED_INCOME.getCode());
//		objAccountsComponentTO.setIdTradeOperation(objSettlementOperationTO.getIdMechanismOperation());
//		List<HolderAccountBalanceTO> lstSellerAccountBalanceTO = populateHolderAccountBalance(objSettlementOperationTO.getLstSellerHolderAccounts(), idCouponIsinCode);
//		List<HolderAccountBalanceTO> lstBuyerAccountBalanceTO = populateHolderAccountBalance(objSettlementOperationTO.getLstBuyerHolderAccounts(), idCouponIsinCode);
//		objAccountsComponentTO.setLstSourceAccounts(lstSellerAccountBalanceTO);
//		objAccountsComponentTO.setLstTargetAccounts(lstBuyerAccountBalanceTO);
//		executorComponentServiceBean.executeAccountsComponent(objAccountsComponentTO);
//	}
//	
//	private List<HolderAccountBalanceTO> populateHolderAccountBalance(List<SettlementHolderAccountTO> lstHolderAccountsOperation, String idIsinCode)
//	{
//		List<HolderAccountBalanceTO> lstHolderAccountBalanceTO= new ArrayList<HolderAccountBalanceTO>();
//		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountsOperation))
//		{
//			for (SettlementHolderAccountTO objSettlementHolderAccountTO: lstHolderAccountsOperation)
//			{
//				//we set the holder account balance information to settle the stocks
//				HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
//				objHolderAccountBalanceTO.setIdHolderAccount(objSettlementHolderAccountTO.getIdHolderAccount());
//				objHolderAccountBalanceTO.setIdParticipant(objSettlementHolderAccountTO.getIdParticipant());
//				objHolderAccountBalanceTO.setIdSecurities(idIsinCode);
//				objHolderAccountBalanceTO.setStockQuantity(objSettlementHolderAccountTO.getStockQuantity());
//				lstHolderAccountBalanceTO.add(objHolderAccountBalanceTO);
//			}
//		}
//		return lstHolderAccountBalanceTO;
//	}
//	
//	private Security createSplitSecurities(Security objSourceSecurity, ProgramInterestCoupon objProgramInterestCoupon, String idParentIsinCode, 
//																			BigDecimal stockQuantity, Integer indIsCoupon, LoggerUser loggerUser) throws ServiceException
//	{
//		Security objSecurities = new Security();
//		objSecurities.setAmortizationAmount(BigDecimal.ZERO);
//		objSecurities.setAmortizationFactor(objSourceSecurity.getAmortizationFactor());
//		objSecurities.setAmortizationOn(objSourceSecurity.getAmortizationOn());
//		objSecurities.setAmortizationPaymentSchedule(null);
//		objSecurities.setAmortizationPeriodicity(objSourceSecurity.getAmortizationPeriodicity());
//		objSecurities.setAmortizationType(objSourceSecurity.getAmortizationType());
//		objSecurities.setCalendarDays(objSourceSecurity.getCalendarDays());
//		objSecurities.setCalendarMonth(objSourceSecurity.getCalendarMonth());
//		objSecurities.setCalendarType(objSourceSecurity.getCalendarType());
//		objSecurities.setCapitalPaymentModality(objSourceSecurity.getCapitalPaymentModality());
//		objSecurities.setCashNominal(objSourceSecurity.getCashNominal());
//		objSecurities.setCfiCode(objSourceSecurity.getCfiCode());
//		objSecurities.setCirculationAmount(BigDecimal.ZERO);
//		objSecurities.setCirculationBalance(stockQuantity);
//		objSecurities.setCorporativeProcessDays(objSourceSecurity.getCorporativeProcessDays());
//		objSecurities.setCurrency(objSourceSecurity.getCurrency());
//		objSecurities.setCurrentNominalValue(objSourceSecurity.getCurrentNominalValue());
//		objSecurities.setDepositRegistryDate(objSourceSecurity.getDepositRegistryDate());
//		objSecurities.setDescription(objSourceSecurity.getDescription());
//		objSecurities.setDesmaterializedBalance(BigDecimal.ZERO);
//		objSecurities.setEconomicActivity(objSourceSecurity.getEconomicActivity());
//		if (ComponentConstant.ONE.equals(indIsCoupon))
//			objSecurities.setExpirationDate(objProgramInterestCoupon.getPaymentDate());
//		else objSecurities.setExpirationDate(objSourceSecurity.getExpirationDate());
//		objSecurities.setFinancialIndex(objSourceSecurity.getFinancialIndex());
//		objSecurities.setIdGroupFk(objSourceSecurity.getIdGroupFk());
//		objSecurities.setIndAmortizationType(objSourceSecurity.getIndAmortizationType());
//		objSecurities.setIndCapitalizableInterest(objSourceSecurity.getIndCapitalizableInterest());
//		objSecurities.setIndConvertibleStock(objSourceSecurity.getIndConvertibleStock());
//		objSecurities.setIndEarlyRedemption(objSourceSecurity.getIndEarlyRedemption());
//		objSecurities.setIndexed(objSourceSecurity.getIndexed());
//		objSecurities.setIndHolderDetail(objSourceSecurity.getIndHolderDetail());
//		objSecurities.setIndIsCoupon(indIsCoupon);
//		objSecurities.setIndHasSplitSecurities(ComponentConstant.ZERO);
//		if (ComponentConstant.ONE.equals(indIsCoupon))
//			objSecurities.setIndIsDetached(ComponentConstant.ZERO);
//		else objSecurities.setIndIsDetached(ComponentConstant.ONE);
//		objSecurities.setIndIsFractionable(objSourceSecurity.getIndIsFractionable());
//		objSecurities.setIndIssuanceManagement(objSourceSecurity.getIndIssuanceManagement());
//		objSecurities.setIndPaymentBenefit(objSourceSecurity.getIndPaymentBenefit());
//		objSecurities.setIndReceivableCustody(objSourceSecurity.getIndReceivableCustody());
//		objSecurities.setIndSecuritization(objSourceSecurity.getIndSecuritization());
//		if (ComponentConstant.ONE.equals(indIsCoupon))
//			objSecurities.setIndSplitCoupon(ComponentConstant.ZERO);
//		else objSecurities.setIndSplitCoupon(objSourceSecurity.getIndSplitCoupon());
//		objSecurities.setIndTaxExempt(objSourceSecurity.getIndTaxExempt());
//		objSecurities.setIndTraderSecondary(objSourceSecurity.getIndTraderSecondary());
//		objSecurities.setInitialNominalValue(objSourceSecurity.getInitialNominalValue());
//		objSecurities.setInscriptionDate(objSourceSecurity.getInscriptionDate());
//		objSecurities.setInstrumentType(objSourceSecurity.getInstrumentType());
//		objSecurities.setInterestFactor(objSourceSecurity.getInterestFactor());
//		objSecurities.setInterestPaymentModality(objSourceSecurity.getInterestPaymentModality());
//		objSecurities.setInterestPaymentSchedule(null);
//		objSecurities.setInterestRate(objSourceSecurity.getInterestRate());
//		objSecurities.setInterestRatePeriodicity(objSourceSecurity.getInterestRatePeriodicity());
//		objSecurities.setInterestType(objSourceSecurity.getInterestType());
//		objSecurities.setIssuance(objSourceSecurity.getIssuance());
//		objSecurities.setIssuanceCountry(objSourceSecurity.getIssuanceCountry());
//		objSecurities.setIssuanceDate(objSourceSecurity.getIssuanceDate());
//		objSecurities.setIssuanceForm(objSourceSecurity.getIssuanceForm());
//		objSecurities.setIssuer(objSourceSecurity.getIssuer());
//		objSecurities.setMaximumInvesment(objSourceSecurity.getMaximumInvesment());
//		objSecurities.setMaximumRate(objSourceSecurity.getMaximumRate());
//		objSecurities.setMinimumInvesment(objSourceSecurity.getMinimumInvesment());
//		objSecurities.setMinimumRate(objSourceSecurity.getMinimumInvesment());
//		objSecurities.setMinimumRate(objSourceSecurity.getMinimumRate());
//		objSecurities.setMnemonic(objSourceSecurity.getMnemonic());
//		objSecurities.setOperationType(objSourceSecurity.getOperationType());
//		objSecurities.setPeriodicity(objSourceSecurity.getPeriodicity());
//		objSecurities.setPhysicalBalance(BigDecimal.ZERO);
//		objSecurities.setPlacedAmount(BigDecimal.ZERO);
//		objSecurities.setPlacedBalance(BigDecimal.ZERO);
//		objSecurities.setRateType(objSourceSecurity.getRateType());
//		objSecurities.setRegistryDate(CommonsUtilities.currentDateTime());
//		objSecurities.setRegistryUser(loggerUser.getUserName());
//		objSecurities.setRetirementDate(objSourceSecurity.getRetirementDate());
//		objSecurities.setSecurityClass(objSourceSecurity.getSecurityClass());
//		objSecurities.setSecurityInvestor(objSourceSecurity.getSecurityInvestor());
//		objSecurities.setSecurityMonthsTerm(objSourceSecurity.getSecurityMonthsTerm());
//		Security objParentSecurity = new Security();
//		objParentSecurity.setIdIsinCodePk(idParentIsinCode);
//		objSecurities.setSecurity(objParentSecurity);
//		objSecurities.setSecuritySerial(objSourceSecurity.getSecuritySerial());
//		objSecurities.setSecuritySource(objSourceSecurity.getSecuritySource());
//		objSecurities.setSecurityType(objSourceSecurity.getSecurityType());
//		objSecurities.setSerialProgram(objSourceSecurity.getSerialProgram());
//		objSecurities.setShareBalance(stockQuantity);
//		objSecurities.setShareCapital(stockQuantity.multiply(objSourceSecurity.getCurrentNominalValue()));
//		objSecurities.setSpread(objSourceSecurity.getSpread());
//		objSecurities.setStateSecurity(SecurityStateType.REGISTERED.getCode());
//		objSecurities.setStockRegistryDays(objSourceSecurity.getStockRegistryDays());
//		objSecurities.setYield(objSourceSecurity.getYield());
//		
//		List<ProgramInterestCoupon> lstProgramInterestCoupons= securitiesServiceBean.getListProgramInterestCoupon(objSourceSecurity.getIdIsinCodePk());
//		if (Validations.validateListIsNotNullAndNotEmpty(lstProgramInterestCoupons))
//			objSecurities.setNumberCoupons(lstProgramInterestCoupons.size()-1);
//		
//		objSecurities.setIdIsinCodePk(securitiesServiceBean.securityIsinGenerate(objSecurities));
//		objSecurities.setAlternativeCode(securitiesServiceBean.alternativeCodeGenerate(objSecurities));
//		this.create(objSecurities);
//		
//		//we set the list of Negotiation mechanism to this securities
//		List<SecurityNegotiationMechanism> lstSecurityNegotiationMechanisms= securitiesServiceBean.getListSecurityNegotiationMechanism(idParentIsinCode);
//		if (Validations.validateListIsNotNullAndNotEmpty(lstSecurityNegotiationMechanisms)) {
//			List<SecurityNegotiationMechanism> lstNewSecurityNegotiationMechanisms= new ArrayList<SecurityNegotiationMechanism>();
//			for (SecurityNegotiationMechanism objSecurityNegotiationMechanism: lstSecurityNegotiationMechanisms)
//			{
//				SecurityNegotiationMechanism objNewSecurityNegotiationMechanism= null;
//				try {
//					objNewSecurityNegotiationMechanism= objSecurityNegotiationMechanism.clone();
//				} catch (CloneNotSupportedException e) {
//					e.printStackTrace();
//				}
//				objNewSecurityNegotiationMechanism.setSecurity(objSecurities);
//				objNewSecurityNegotiationMechanism.setIdSecurityNegMechPk(null);
//				this.create(objNewSecurityNegotiationMechanism);
//				lstNewSecurityNegotiationMechanisms.add(objNewSecurityNegotiationMechanism);
//			}
//			objSecurities.setSecurityNegotiationMechanisms(lstNewSecurityNegotiationMechanisms);
//		}
//		
//		//we set the list of foreign depositaries to this securities 
//		List<SecurityForeignDepository> lstSecurityForeignDepositories= securitiesServiceBean.getListSecurityForeignDepositories(idParentIsinCode);
//		if (Validations.validateListIsNotNullAndNotEmpty(lstSecurityForeignDepositories)) {
//			for (SecurityForeignDepository objSecurityForeignDepository: lstSecurityForeignDepositories)
//			{
//				SecurityForeignDepository objNewSecurityForeignDepository= null;
//				try {
//					objNewSecurityForeignDepository= objSecurityForeignDepository.clone();
//				} catch (CloneNotSupportedException e) {
//					e.printStackTrace();
//				}
//				objNewSecurityForeignDepository.getId().setIdIsinCodePk(objSecurities.getIdIsinCodePk());
//				objNewSecurityForeignDepository.setRegistryDate(CommonsUtilities.currentDateTime());
//				this.create(objNewSecurityForeignDepository);
//			}
//		}
//		
//		//we create the Amortization corporative operation to coupon securities
//		if (ComponentConstant.ONE.equals(indIsCoupon)) {
//			//we create the AmortizationPaymentSchedule to coupon securities
//			createAmortizationPaymentSchedule(objSecurities, objProgramInterestCoupon, loggerUser);
//			//we create the Amortization corporative operation to coupon securities
//			securitiesServiceBean.createAmortizationCorporativeOperation(objSecurities, loggerUser);
//		}
//		
//		//we create the new references between the ProgramInterestCoupon and the split securities
//		createSplitSecuritiesInterestCoupon(objSecurities, objSourceSecurity.getIdIsinCodePk(), objProgramInterestCoupon, lstProgramInterestCoupons, indIsCoupon);
//				
//		//we update the security description
//		String splitDescription= generateSplitSecuritiesDescription(objSecurities, objSourceSecurity.getDescription(), objProgramInterestCoupon);
//		securitiesServiceBean.updateSplitSecuritiesDescription(objSecurities.getIdIsinCodePk(), splitDescription);
//		return objSecurities;
//	}
//	
//	private AmortizationPaymentSchedule createAmortizationPaymentSchedule(Security objSecurities, ProgramInterestCoupon objProgramInterestCoupon, LoggerUser loggerUser)
//	{
//		//we create a AmortizationPaymentSchedule to this coupon securities
//		AmortizationPaymentSchedule objAmortizationPaymentSchedule= new AmortizationPaymentSchedule();
//		objAmortizationPaymentSchedule.setAmortizationFactor(new BigDecimal(100)); //it is total amortization
//		objAmortizationPaymentSchedule.setAmortizationType(objSecurities.getAmortizationType());
//		objAmortizationPaymentSchedule.setCalendarType(objSecurities.getCalendarType());
//		objAmortizationPaymentSchedule.setCorporativeDays(objSecurities.getCorporativeProcessDays());
//		objAmortizationPaymentSchedule.setPeriodicity(objSecurities.getPeriodicity());
//		objAmortizationPaymentSchedule.setRegistryDays(objSecurities.getStockRegistryDays());
//		objAmortizationPaymentSchedule.setScheduleState(PaymentScheduleStateType.REGTERED.getCode());
//		objAmortizationPaymentSchedule.setSecurity(objSecurities);
//		this.create(objAmortizationPaymentSchedule);
//		
//		//we create a ProgramAmortizationCoupon to this coupon securities
//		List<ProgramAmortizationCoupon> lstProgramAmortizationCoupons= new ArrayList<ProgramAmortizationCoupon>();
//		ProgramAmortizationCoupon objProgramAmortizationCoupon = new ProgramAmortizationCoupon();
//		objProgramAmortizationCoupon.setAmortizationAmount(BigDecimal.ZERO);
//		objProgramAmortizationCoupon.setAmortizationFactor(objAmortizationPaymentSchedule.getAmortizationFactor());
//		objProgramAmortizationCoupon.setAmortizationPaymentSchedule(objAmortizationPaymentSchedule);
//		objProgramAmortizationCoupon.setBeginingDate(objProgramInterestCoupon.getBeginingDate());
//		objProgramAmortizationCoupon.setCorporativeDate(objProgramInterestCoupon.getCorporativeDate());
//		objProgramAmortizationCoupon.setCouponNumber(objProgramInterestCoupon.getCouponNumber());
//		objProgramAmortizationCoupon.setCurrency(objSecurities.getCurrency());
//		objProgramAmortizationCoupon.setCutoffDate(objProgramInterestCoupon.getCutoffDate());
//		objProgramAmortizationCoupon.setExpirationDate(objProgramInterestCoupon.getExperitationDate());
//		objProgramAmortizationCoupon.setIndRounding(objProgramInterestCoupon.getIndRounding());
//		objProgramAmortizationCoupon.setPaymentDate(objProgramInterestCoupon.getPaymentDate());
//		objProgramAmortizationCoupon.setRegisterDate(objProgramInterestCoupon.getCutoffDate());
//		objProgramAmortizationCoupon.setRegistryDate(CommonsUtilities.currentDateTime());
//		objProgramAmortizationCoupon.setRegistryUser(loggerUser.getUserName());
//		objProgramAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());
//		this.create(objProgramAmortizationCoupon);
//		
//		lstProgramAmortizationCoupons.add(objProgramAmortizationCoupon);
//		objAmortizationPaymentSchedule.setProgramAmortizationCoupons(lstProgramAmortizationCoupons);
//		objSecurities.setAmortizationPaymentSchedule(objAmortizationPaymentSchedule);
//		
//		return objAmortizationPaymentSchedule;
//	}
//	
//	
//	private void createSplitSecuritiesInterestCoupon(Security objSplitSecurities, String idParentIsinCode, ProgramInterestCoupon objSplitInterestCoupon,
//																					List<ProgramInterestCoupon> lstProgramInterestCoupons, Integer indIsCoupon)
//	{
//		List<SecurityInterestCoupon> lstSecurityInterestCoupons = new ArrayList<SecurityInterestCoupon>();
//		if (ComponentConstant.ONE.equals(indIsCoupon)) {
//			//to coupon securities we create a unique relationship between split ProgramInterestCoupon and the new coupon securities
//			List<ProgramInterestCoupon> lstNewProgramInterestCoupons= new ArrayList<ProgramInterestCoupon>();
//			lstNewProgramInterestCoupons.add(objSplitInterestCoupon);
//			createSplitSecuritiesInterestCoupon(objSplitSecurities.getIdIsinCodePk(), lstNewProgramInterestCoupons);
//		} else {
//			//we get the list of SecurityInterestCoupon from parent securities
//			lstSecurityInterestCoupons = securitiesServiceBean.getListSecurityInterestCoupons(idParentIsinCode);
//			if (Validations.validateListIsNullOrEmpty(lstSecurityInterestCoupons)) {
//				//if the parent securities doesn't have SecurityInterestCoupon (the first split) then we create them  
//				lstSecurityInterestCoupons= createSplitSecuritiesInterestCoupon(idParentIsinCode, lstProgramInterestCoupons);
//			}
//		}
//		
//		if (Validations.validateListIsNotNullAndNotEmpty(lstSecurityInterestCoupons)) {
//			for (SecurityInterestCoupon objSecurityInterestCoupon: lstSecurityInterestCoupons)
//			{
//				Long idProgramInterestCoupon= objSecurityInterestCoupon.getProgramInterestCoupon().getIdProgramInterestPk(); //ProgramInterestCoupon from parent securities
//				//we exclude the ProgramInterestCoupon was split from parent securities
//				if (!idProgramInterestCoupon.equals(objSplitInterestCoupon.getIdProgramInterestPk())) {
//					SecurityInterestCoupon objNewSecurityInterestCoupon= null;
//					try {
//						objNewSecurityInterestCoupon= objSecurityInterestCoupon.clone();
//					} catch (CloneNotSupportedException e) {
//						e.printStackTrace();
//					}
//					objNewSecurityInterestCoupon.setIdSecurityInterestCouponPk(null);
//					objNewSecurityInterestCoupon.setSecurities(objSplitSecurities);
//					//we save the new relationship between split securities and ProgramInterestCoupon without split coupon 
//					this.create(objNewSecurityInterestCoupon);
//				}
//			}
//		}
//	}
//	
//	private List<SecurityInterestCoupon> createSplitSecuritiesInterestCoupon(String idIsinCode, List<ProgramInterestCoupon> lstProgramInterestCoupons)
//	{
//		List<SecurityInterestCoupon> lstSecurityInterestCoupons = new ArrayList<SecurityInterestCoupon>();
//		if (Validations.validateListIsNotNullAndNotEmpty(lstProgramInterestCoupons)) {
//			for (ProgramInterestCoupon objProgramInterestCoupon: lstProgramInterestCoupons)
//			{
//				SecurityInterestCoupon objSecurityInterestCoupon= new SecurityInterestCoupon();
//				objSecurityInterestCoupon.setProgramInterestCoupon(objProgramInterestCoupon);
//				Security objSecurities= new Security();
//				objSecurities.setIdIsinCodePk(idIsinCode);
//				objSecurityInterestCoupon.setSecurities(objSecurities);
//				objSecurityInterestCoupon.setCouponState(objProgramInterestCoupon.getStateProgramInterest());
//				this.create(objSecurityInterestCoupon);
//				lstSecurityInterestCoupons.add(objSecurityInterestCoupon);
//			}
//		}
//		return lstSecurityInterestCoupons;
//	}
//	
//	private String generateSplitSecuritiesDescription(Security objSplitSecurities, String strOldDescription, ProgramInterestCoupon objSplitInterestCoupon)
//	{
//		String strNewDescription= strOldDescription;
//		if (BooleanType.YES.getCode().equals(objSplitSecurities.getIndIsCoupon())) {
//			strNewDescription+= COUPON + objSplitInterestCoupon.getCouponNumber();
//		} else {
//			strNewDescription+= WITHOUT_COUPON + objSplitInterestCoupon.getCouponNumber() + CLOSE_PARENTESIS;
//		}
//		return strNewDescription;
//	}
//	
//}
