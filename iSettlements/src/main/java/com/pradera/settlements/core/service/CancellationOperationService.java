package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentDetailStateType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.settlements.core.to.SettlementUnfulfillmentTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CancellationOperationService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class CancellationOperationService extends CrudDaoServiceBean{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(CancellationOperationService.class);

    /** The accounts component service. */
    @Inject
    Instance<AccountsComponentService> accountsComponentService;
    
    /** The settlement process service. */
    @EJB
    SettlementProcessService settlementProcessService;
    
	
	/**
	 * Unblock stock operation.
	 *
	 * @param settlementUnfulfillmentTO the settlement unfulfillment to
	 * @param operationPart the operation part
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void unblockStockOperation(SettlementUnfulfillmentTO settlementUnfulfillmentTO, Integer operationPart, LoggerUser loggerUser) throws ServiceException
	{
		List<Object[]> lstMechanismBlockedHolderAccount= null;
//		List<Object[]> lstSwapBlockedHolderAccount= null;
		List<Long> lstIdSettlementAccountOperation= new ArrayList<Long>(); //list to know which holderAccountOperation update the stock reference
		try {
			//we configure the component to cancel the movements
			AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
			if (ComponentConstant.CASH_PART.equals(operationPart)) {
				objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.UNFULFILL_OPERATIONS_CANCEL.getCode());
			} else if (ComponentConstant.TERM_PART.equals(operationPart)) {
				objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.OPERATION_CANCEL_TERM_PART.getCode());
			}
			objAccountsComponentTO.setOperationPart(operationPart);
			objAccountsComponentTO.setIdSettlementOperation(settlementUnfulfillmentTO.getIdSettlementOperation());
			objAccountsComponentTO.setIdOperationType(NegotiationModalityType.get(settlementUnfulfillmentTO.getIdModality()).getParameterOperationType());
			objAccountsComponentTO.setIndMarketFact(ComponentConstant.ZERO);
			List<HolderAccountBalanceTO> lstBuyerHolderAccount= new ArrayList<HolderAccountBalanceTO>();
			List<HolderAccountBalanceTO> lstSellerHolderAccount= new ArrayList<HolderAccountBalanceTO>();
			
			//we get the list of holder accounts for mechanism operation
			lstMechanismBlockedHolderAccount= getListBlockedAccountMechanismOperations(settlementUnfulfillmentTO.getIdSettlementOperation(), ComponentConstant.PURCHARSE_ROLE);
			if (Validations.validateListIsNotNullAndNotEmpty(lstMechanismBlockedHolderAccount)) {
				populateAccountsComponent(lstBuyerHolderAccount, lstSellerHolderAccount, lstMechanismBlockedHolderAccount, settlementUnfulfillmentTO.getIndReportingBalance(), operationPart);
				populateListIdHolderAccountOperation(lstIdSettlementAccountOperation, lstMechanismBlockedHolderAccount);
			}
			
			lstMechanismBlockedHolderAccount= getListBlockedAccountMechanismOperations(settlementUnfulfillmentTO.getIdSettlementOperation(), ComponentConstant.SALE_ROLE);
			if (Validations.validateListIsNotNullAndNotEmpty(lstMechanismBlockedHolderAccount)) {
				populateAccountsComponent(lstBuyerHolderAccount, lstSellerHolderAccount, lstMechanismBlockedHolderAccount, settlementUnfulfillmentTO.getIndReportingBalance(), operationPart);
				populateListIdHolderAccountOperation(lstIdSettlementAccountOperation, lstMechanismBlockedHolderAccount);
			}
			
//			//only for CASH PART we verify the block holder accounts to unblock the balances 
//			if (ComponentConstant.CASH_PART.equals(operationPart))
//			{
//				//we have to the same steps for swap operation
//				if (NegotiationModalityType.SWAP_EQUITIES.getCode().equals(settlementUnfulfillmentTO.getIdModality()) || 
//					 NegotiationModalityType.SWAP_FIXED_INCOME.getCode().equals(settlementUnfulfillmentTO.getIdModality())) 
//				{
//					//we get the list of holder account for swap operation
//					lstSwapBlockedHolderAccount= getListBlockedAccountSwapOperations(idMechanismOperation, ComponentConstant.PURCHARSE_ROLE);
//					if (Validations.validateListIsNotNullAndNotEmpty(lstSwapBlockedHolderAccount)) {
//						populateAccountsComponent(lstBuyerHolderAccount, lstSellerHolderAccount, lstSwapBlockedHolderAccount, objNegotiationModality, operationPart);
//						populateListIdHolderAccountOperation(lstIdSettlementAccountOperation, lstSwapBlockedHolderAccount);
//					}
//					lstSwapBlockedHolderAccount= getListBlockedAccountSwapOperations(idMechanismOperation, ComponentConstant.SALE_ROLE);
//					if (Validations.validateListIsNotNullAndNotEmpty(lstSwapBlockedHolderAccount)) {
//						populateAccountsComponent(lstBuyerHolderAccount, lstSellerHolderAccount, lstSwapBlockedHolderAccount, objNegotiationModality, operationPart);
//						populateListIdHolderAccountOperation(lstIdSettlementAccountOperation, lstSwapBlockedHolderAccount);
//					}
//				}
//			}
			//we execute the component to revert the movements
			if (Validations.validateListIsNotNullAndNotEmpty(lstSellerHolderAccount)) {
				objAccountsComponentTO.setLstSourceAccounts(lstSellerHolderAccount);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstBuyerHolderAccount)) {
				objAccountsComponentTO.setLstTargetAccounts(lstBuyerHolderAccount);
			}
			
			if (Validations.validateListIsNotNullAndNotEmpty(lstSellerHolderAccount) || Validations.validateListIsNotNullAndNotEmpty(lstBuyerHolderAccount)) {
				accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
			}
			
			//we have to set in "null" the stock reference for the holder account operation 
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdSettlementAccountOperation)) {
				updateStockSettlementAccountOperation(lstIdSettlementAccountOperation, loggerUser);
			}
			
		} catch (ServiceException e) {
			logger.error("idMechanismOperation: " +settlementUnfulfillmentTO.getIdMechanismOperation()
								+ ", idModality: " +settlementUnfulfillmentTO.getIdModality()
								+ ", operationPart: " +operationPart);
			throw e;
		}
	}
	
	
	/**
	 * Method to get the list of holder accounts to unblock the balances in sale or purchase position for this mechanism operation .
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idRole the id role
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListBlockedAccountMechanismOperations(Long idSettlementOperation, Integer idRole) throws ServiceException
	{
		StringBuilder stringBuilderSql = new StringBuilder();
		Map<String, Object> paramters = new HashMap<String, Object>();
		stringBuilderSql.append(" SELECT mo.idMechanismOperationPk, ");
		stringBuilderSql.append(" mo.securities.idSecurityCodePk, ");//1
		stringBuilderSql.append(" hao.idHolderAccountOperationPk, ");//2
		stringBuilderSql.append(" hao.inchargeStockParticipant.idParticipantPk, ");//3
		stringBuilderSql.append(" hao.holderAccount.idHolderAccountPk, ");//4
		stringBuilderSql.append(" SAO.idSettlementAccountPk, ");//5
		stringBuilderSql.append(" SAO.role, ");//6
		stringBuilderSql.append(" SAO.stockQuantity ");//7
		stringBuilderSql.append(" FROM SettlementAccountOperation SAO, HolderAccountOperation HAO, SettlementOperation SO, MechanismOperation mo ");
		stringBuilderSql.append(" WHERE hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk ");
		stringBuilderSql.append(" and SAO.holderAccountOperation.idHolderAccountOperationPk = HAO.idHolderAccountOperationPk ");
		stringBuilderSql.append(" and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk  ");
		stringBuilderSql.append(" and SO.idSettlementOperationPk = :idSettlementOperation ");
		if (ComponentConstant.SALE_ROLE.equals(idRole)) {
			//we exclude the list of following modalities (Primary Placement)
			stringBuilderSql.append(" and (mo.indPrimaryPlacement != :indicatorOne and SO.operationPart = :cashPart ) ");
			paramters.put("cashPart", ComponentConstant.CASH_PART);
			paramters.put("indicatorOne", ComponentConstant.ONE);
		}
		stringBuilderSql.append(" and SAO.operationState = :idAccountState ");
		stringBuilderSql.append(" and SAO.role = :idRole ");
		stringBuilderSql.append(" and SAO.stockReference = :idStockReference ");
		
		paramters.put("idRole", idRole);
		paramters.put("idSettlementOperation", idSettlementOperation);
		paramters.put("idAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		if (ComponentConstant.SALE_ROLE.equals(idRole)) {
			paramters.put("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		}
		else if (ComponentConstant.PURCHARSE_ROLE.equals(idRole)) {
			paramters.put("idStockReference", AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		}
		
		return  findListByQueryString(stringBuilderSql.toString(), paramters);
		
	}
	
	
	/**
	 * Method to get the list of holder accounts to unblock the balances in sale or purchase position for this swap operation .
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param idRole the id role
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListBlockedAccountSwapOperations(Long idMechanismOperation,  Integer idRole) throws ServiceException
	{
		List<Object[]> lstObjects = new ArrayList<Object[]>();
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT HAO.mechanismOperation.idMechanismOperationPk, "); // ID Mechanism Operation
		stringBuilderSql.append(" HAO.idHolderAccountOperationPk, ");
		stringBuilderSql.append(" SO.mechanismOperation.mechanisnModality.id.idNegotiationModalityPk, ");
		stringBuilderSql.append(" HAO.role, ");
		stringBuilderSql.append(" HAO.inchargeStockParticipant.idParticipantPk, ");
		stringBuilderSql.append(" HAO.holderAccount.idHolderAccountPk, ");
		stringBuilderSql.append(" SO.securities.idIsinCodePk, ");
		stringBuilderSql.append(" HAO.stockQuantity ");
		stringBuilderSql.append(" FROM HolderAccountOperation HAO, SwapOperation SO ");
		stringBuilderSql.append(" WHERE SO.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuilderSql.append(" and HAO.swapOperation.idSwapOperationPk = SO.idSwapOperationPk ");
		stringBuilderSql.append(" and HAO.holderAccountState = :idAccountState ");
		stringBuilderSql.append(" and HAO.operationPart = :idOperationPart ");
		stringBuilderSql.append(" and HAO.role = :idRole ");
		stringBuilderSql.append(" and HAO.stockReference = :idStockReference ");
		
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("idRole", idRole);
		paramters.put("idMechanismOperation", idMechanismOperation);
		paramters.put("idOperationPart", ComponentConstant.CASH_PART);
		paramters.put("idAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		if (ComponentConstant.SALE_ROLE.equals(idRole)) {
			paramters.put("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		} else if (ComponentConstant.PURCHARSE_ROLE.equals(idRole)) {
			paramters.put("idStockReference", AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode());
		}
		
		lstObjects = findListByQueryString(stringBuilderSql.toString(), paramters);
		
		return lstObjects;
	}
	
	
	/**
	 * Method to get the list of participants that have blocked balance in their holder accounts .
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param idOperationPart the id operation part
	 * @return List<ParticipantOperation>
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListIdBlockedParticipantOperation(Long idMechanismOperation, Integer idOperationPart)
	{
		List<Long> lstIdParticipantOperations = null;
		
		StringBuffer strStringBuffer= new StringBuffer();
		strStringBuffer.append(" SELECT PO.idParticipantOperationPk ");
		strStringBuffer.append(" FROM ParticipantOperation PO ");
		strStringBuffer.append(" WHERE PO.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		strStringBuffer.append(" and PO.operationPart = :idOperationPart ");
		strStringBuffer.append(" and PO.stockReference = :idStockReference ");
		strStringBuffer.append(" and PO.role = :idRole ");
		
		Query objQuery= em.createQuery(strStringBuffer.toString());
		objQuery.setParameter("idMechanismOperation", idMechanismOperation);
		objQuery.setParameter("idOperationPart", idOperationPart);
		objQuery.setParameter("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		objQuery.setParameter("idRole", ComponentConstant.SALE_ROLE);
		
		lstIdParticipantOperations= objQuery.getResultList();
		
		return lstIdParticipantOperations;
	}
	
	
	/**
	 * Method to get the list of participants that have blocked balance in their holder accounts for swap operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return List<ParticipantOperation>
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListIdBlockedParticipantSwapOperation(Long idMechanismOperation)
	{
		List<Long> lstIdParticipantOperations = null;
		
		StringBuffer strStringBuffer= new StringBuffer();
		strStringBuffer.append(" SELECT PO.idParticipantOperationPk ");
		strStringBuffer.append(" FROM ParticipantOperation PO, SwapOperation SO ");
		strStringBuffer.append(" WHERE PO.swapOperation.idSwapOperationPk = SO.idSwapOperationPk ");
		strStringBuffer.append(" and SO.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		strStringBuffer.append(" and PO.operationPart = :idOperationPart ");
		strStringBuffer.append(" and PO.stockReference = :idStockReference ");
		strStringBuffer.append(" and PO.role = :idRole ");
		
		Query objQuery= em.createQuery(strStringBuffer.toString());
		objQuery.setParameter("idMechanismOperation", idMechanismOperation);
		objQuery.setParameter("idOperationPart", ComponentConstant.CASH_PART);
		objQuery.setParameter("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		objQuery.setParameter("idRole", ComponentConstant.SALE_ROLE);
		
		lstIdParticipantOperations= objQuery.getResultList();
		
		return lstIdParticipantOperations;
	}
	
	
	/**
	 * Update cancellation operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param state the state
	 * @param idCancellationReason the id cancellation reason
	 * @param otherReason the other reason
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateCancellationOperation(Long idMechanismOperation, Integer state, Integer idCancellationReason, String otherReason, LoggerUser loggerUser) throws ServiceException
	{
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE MechanismOperation SET operationState = :operationState ");
		stringBuffer.append(" , cancelMotive = :idCancellationReason ");
		stringBuffer.append(" , cancelMotiveOther = :otherReason ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idMechanismOperationPk = :idMechanismOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("operationState", state);
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("idCancellationReason", idCancellationReason);
		query.setParameter("otherReason", otherReason);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();
	}
	
		
	/**
	 * Method to update the stock reference from HolderAccountOperation. This happens when cancel the operation
	 *
	 * @param lstIdSettlementAccountOperation the lst id settlement account operation
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateStockSettlementAccountOperation(List<Long> lstIdSettlementAccountOperation, LoggerUser loggerUser)
	{			
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE SettlementAccountOperation SET stockReference = null ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk in (:lstIdSettlementAccountOperation) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdSettlementAccountOperation", lstIdSettlementAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();	
	}
	
	
	/**
	 * Gets the placement segment detail.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the placement segment detail
	 */
	public PlacementSegmentDetail getPlacementSegmentDetail(Long idMechanismOperation)
	{
		PlacementSegmentDetail objPlacementSegmentDetail= null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT PSD FROM PlacementSegmentDetail PSD, MechanismOperation MO ");
			stringBuffer.append(" WHERE PSD.security.idIsinCodePk = MO.securities.idIsinCodePk ");
			stringBuffer.append(" and MO.idMechanismOperationPk = :idMechanismOperation ");
			stringBuffer.append(" and PSD.statePlacementSegmentDet = :stateSegmentDetail ");
			stringBuffer.append(" and PSD.placementSegment.placementSegmentState = :stateSegment ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idMechanismOperation", idMechanismOperation);
			query.setParameter("stateSegmentDetail", PlacementSegmentDetailStateType.REGISTERED.getCode());
			query.setParameter("stateSegment", PlacementSegmentStateType.OPENED.getCode());
			objPlacementSegmentDetail= (PlacementSegmentDetail) query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
				
		return objPlacementSegmentDetail;
	}
	
	/**
	 * Gets the stock quantity operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the stock quantity operation
	 */
	public BigDecimal getStockQuantityOperation(Long idMechanismOperation)
	{
		BigDecimal stockQuantity= null;
		StringBuffer stringBuffer= new StringBuffer();
		try {
			stringBuffer.append(" SELECT stockQuantity FROM MechanismOperation ");
			stringBuffer.append(" WHERE idMechanismOperationPk = :idMechanismOperation ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idMechanismOperation", idMechanismOperation);
			stockQuantity = (BigDecimal) query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return stockQuantity;
	}
	
	/**
	 * Gets the negotiation modality.
	 *
	 * @param idNegotiationModality the id negotiation modality
	 * @return the negotiation modality
	 */
	public NegotiationModality getNegotiationModality(Long idNegotiationModality)
	{
		return this.find(NegotiationModality.class, idNegotiationModality);
	}
	
	/**
	 * Populate list id holder account operation.
	 *
	 * @param lstIdHolderAccountOperation the lst id holder account operation
	 * @param lstBlockedAccountOperations the lst blocked account operations
	 */
	private void populateListIdHolderAccountOperation(List<Long> lstIdHolderAccountOperation, List<Object[]> lstBlockedAccountOperations)
	{
		for (Object[] arrObject: lstBlockedAccountOperations)
		{
			Long idHolderAccountOperation= (Long)arrObject[5];
			lstIdHolderAccountOperation.add(idHolderAccountOperation);
		}
	}
	
	
	/**
	 * Populate accounts component.
	 *
	 * @param lstBuyerHolderAccount the lst buyer holder account
	 * @param lstSellerHolderAccount the lst seller holder account
	 * @param lstHolderAccountOperation the lst holder account operation
	 * @param reportingBalance the reporting balance
	 * @param operationPart the operation part
	 */
	private void populateAccountsComponent(List<HolderAccountBalanceTO> lstBuyerHolderAccount, List<HolderAccountBalanceTO> lstSellerHolderAccount, 
																	List<Object[]> lstHolderAccountOperation, Integer reportingBalance, Integer operationPart)
	{
		for (Object[] arrObject: lstHolderAccountOperation)
		{
			String idIsinCode= (String)arrObject[1];
			Long idHolderAccountOperation = (Long)arrObject[2];
			Long idParticipant= (Long)arrObject[3];
			Long idHolderAccount= (Long)arrObject[4];
			Long idSettlementAccount = (Long)arrObject[5];
			Integer role= (Integer)arrObject[6];
			BigDecimal stockQuantity= (BigDecimal)arrObject[7];
			
			HolderAccountBalanceTO objHolderAccountBalanceTO= new HolderAccountBalanceTO();
			objHolderAccountBalanceTO.setIdHolderAccount(idHolderAccount);
			objHolderAccountBalanceTO.setStockQuantity(stockQuantity);
			objHolderAccountBalanceTO.setIdSecurityCode(idIsinCode);
			objHolderAccountBalanceTO.setIdParticipant(idParticipant);
			
			List<SettlementAccountMarketfact> settlementAccountMarketfacts = settlementProcessService.getSettlementAccountMarketfacts(idSettlementAccount);
			if(Validations.validateListIsNotNullAndNotEmpty(settlementAccountMarketfacts)){
				objHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for(SettlementAccountMarketfact settlementAccountMarketfact : settlementAccountMarketfacts){
					MarketFactAccountTO objMarketFactAccountTO = new MarketFactAccountTO();
					objMarketFactAccountTO.setMarketDate(settlementAccountMarketfact.getMarketDate());
					objMarketFactAccountTO.setMarketPrice(settlementAccountMarketfact.getMarketPrice());
					objMarketFactAccountTO.setMarketRate(settlementAccountMarketfact.getMarketRate());
					objMarketFactAccountTO.setQuantity(settlementAccountMarketfact.getMarketQuantity());
					objHolderAccountBalanceTO.getLstMarketFactAccounts().add(objMarketFactAccountTO);
				}
			}
			
			if (ComponentConstant.SALE_ROLE.equals(role)) {
				//if the cancellation is to term part we must save a guarantee operation to withdrawal the principal guarantee from reporting holder account
				if (ComponentConstant.ONE.equals(reportingBalance) && ComponentConstant.TERM_PART.equals(operationPart)) {
					GuaranteeOperation objGuaranteeOperation = saveGuaranteeOperation(idHolderAccountOperation, idParticipant, idHolderAccount, idIsinCode, stockQuantity, 
																											GuaranteeClassType.SECURITIES.getCode(), GuaranteeType.PRINCIPAL.getCode(), null);
					objHolderAccountBalanceTO.setIdGuaranteeOperation(objGuaranteeOperation.getIdGuaranteeOperationPk());
				}
				lstSellerHolderAccount.add(objHolderAccountBalanceTO);
			} else if (ComponentConstant.PURCHARSE_ROLE.equals(role)) {
				lstBuyerHolderAccount.add(objHolderAccountBalanceTO);
			}
		}
	}
	
	/**
	 * Save guarantee operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idIsinCode the id isin code
	 * @param guaranteeBalance the guarantee balance
	 * @param guaranteeClass the guarantee class
	 * @param guaranteeType the guarantee type
	 * @param currency the currency
	 * @return the guarantee operation
	 */
	public GuaranteeOperation saveGuaranteeOperation(Long idHolderAccountOperation, Long idParticipant, Long idHolderAccount, String idIsinCode, 
			BigDecimal guaranteeBalance, Integer guaranteeClass, Integer guaranteeType, Integer currency)
	{
		GuaranteeOperation objGuaranteeOperation = new GuaranteeOperation();
		objGuaranteeOperation.setGuaranteeClass(guaranteeClass);
		objGuaranteeOperation.setGuaranteeType(guaranteeType);
		objGuaranteeOperation.setGuaranteeBalance(guaranteeBalance);
		objGuaranteeOperation.setOperationDate(CommonsUtilities.currentDateTime());
		objGuaranteeOperation.setCommissionAmount(BigDecimal.ZERO);
		objGuaranteeOperation.setCurrency(currency);
		objGuaranteeOperation.setInterestAmount(BigDecimal.ZERO);
		objGuaranteeOperation.setPendingAmount(guaranteeBalance);
		objGuaranteeOperation.setTaxAmount(BigDecimal.ZERO);
		
		Participant objParticipant = new Participant();
		objParticipant.setIdParticipantPk(idParticipant);
		objGuaranteeOperation.setParticipant(objParticipant);
		
		HolderAccount objHolderAccount = new HolderAccount();
		objHolderAccount.setIdHolderAccountPk(idHolderAccount);
		objGuaranteeOperation.setHolderAccount(objHolderAccount);
		
		if (idIsinCode != null) {
			Security objSecurity = new Security();
			objSecurity.setIdIsinCode(idIsinCode);
			objGuaranteeOperation.setSecurities(objSecurity);
		}
		
		HolderAccountOperation objHolderAccountOperation = find(HolderAccountOperation.class, idHolderAccountOperation);
		objGuaranteeOperation.setHolderAccountOperation(objHolderAccountOperation);
		
		//we create the new guarantee operation
		this.create(objGuaranteeOperation);
		return objGuaranteeOperation;
	}
}
