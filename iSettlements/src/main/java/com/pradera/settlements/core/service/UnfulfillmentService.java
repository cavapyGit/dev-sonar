package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.guarantees.AccountOperationValorization;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.HolderAccountUnfulfillment;
import com.pradera.model.settlement.OperationUnfulfillment;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.type.UnfulfillmentParameterType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UnfulfillmentService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class UnfulfillmentService extends CrudDaoServiceBean{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(UnfulfillmentService.class);
	
	
	/**
	 * Method to get the list of mechanism operation that did not complete the holder account assignment. This is for a specific assignment process 
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @return List<MechanismOperation>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListAssigmentUnfulfilledOperations(Long idAssignmentProcess, Integer currency) 
	{
		logger.info("getListAssigmentUnfulfilledOperations(idAssignmentProcess: "+idAssignmentProcess+")");
		
		List<Object[]> lstObjects = new ArrayList<Object[]>();
		StringBuilder stringBuilderSql = new StringBuilder();
		List<Integer> lstRequestStates= new ArrayList<Integer>();
		
		stringBuilderSql.append(" SELECT ");
		stringBuilderSql.append(" MO.idMechanismOperationPk, "); 
		stringBuilderSql.append(" MO.buyerParticipant.idParticipantPk, ");
		stringBuilderSql.append(" MO.sellerParticipant.idParticipantPk, ");
		stringBuilderSql.append(" MO.mechanisnModality.id.idNegotiationModalityPk, ");
		stringBuilderSql.append(" SO.idSettlementOperationPk, ");
		stringBuilderSql.append(" SO.settlementDate, "); //5
		stringBuilderSql.append(" SO.stockQuantity, ");  //6
		stringBuilderSql.append(" SO.settlementAmount, ");
		stringBuilderSql.append(" SO.indPartial, ");//8
		stringBuilderSql.append(" MO.indTermSettlement, ");//9
		stringBuilderSql.append(" MO.indCashStockBlock, ");
		stringBuilderSql.append(" MO.indTermStockBlock, ");//11
		stringBuilderSql.append(" MO.indPrimaryPlacement, ");//12
		stringBuilderSql.append(" MO.indReportingBalance, ");//13
		stringBuilderSql.append(" MO.securities.idSecurityCodePk");//14
		stringBuilderSql.append(" FROM SettlementOperation SO, MechanismOperation MO ");
		stringBuilderSql.append(" WHERE MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuilderSql.append(" and MO.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcess ");
		stringBuilderSql.append(" and SO.operationPart = :cashPart ");
		stringBuilderSql.append(" and not exists (select AR from AssignmentRequest AR " + 
																	" where AR.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk " +
																	" and AR.role = :idRole and AR.requestState in (:lstRequestStates)) ");
		
		if(Validations.validateIsNotNullAndNotEmpty(currency)) {
			stringBuilderSql.append(" and MO.securities.currency = :currency  ");
		}
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		//only consider purchase position
		parameters.put("idRole", ComponentConstant.PURCHARSE_ROLE);
		//search only for the present assignment process
		parameters.put("idAssignmentProcess", idAssignmentProcess);
		//do not consider assignment request in CONFIRMED and CANCELED state
		lstRequestStates.add(AssignmentRequestStateType.CONFIRMED.getCode());
		lstRequestStates.add(AssignmentRequestStateType.CANCLED.getCode());
		parameters.put("lstRequestStates", lstRequestStates);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		
		if(Validations.validateIsNotNullAndNotEmpty(currency)) {
			parameters.put("currency", currency);
		}
		
		lstObjects =findListByQueryString(stringBuilderSql.toString(), parameters);
		
		return lstObjects;
	}
	
	
	/**
	 * Method to get the list of unfulfilled operations in Funds or Stocks in Cash part or Term part considering a unfulfillment date.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idOperationPart the id operation part
	 * @param idUnfulfillmentMotive the id unfulfillment motive
	 * @param unfulfillmentDate the unfulfillment date
	 * @param role the role
	 * @return List<MechanismOperation>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListStocksFundsUnfufilledOperations(Long idMechanism, Long idModality, Integer idOperationPart,
																									Long idUnfulfillmentMotive, Date unfulfillmentDate, Integer role, Integer currency)
	{
		logger.info("getListStocksFundsUnfufilledOperations(idOperationPart: "+idOperationPart+ ", idUnfulfillmentMotive: "+idUnfulfillmentMotive+ 
															", unfulfillmentDate: "+unfulfillmentDate.toString()+", role: "+role+")");
		
		List<Object[]> lstObjects = new ArrayList<Object[]>();
		StringBuilder stringBuilderSql = new StringBuilder();
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		stringBuilderSql.append(" SELECT ");
		stringBuilderSql.append(" MO.idMechanismOperationPk, "); 
		stringBuilderSql.append(" MO.buyerParticipant.idParticipantPk, ");
		stringBuilderSql.append(" MO.sellerParticipant.idParticipantPk, ");
		stringBuilderSql.append(" MO.mechanisnModality.id.idNegotiationModalityPk, ");
		stringBuilderSql.append(" SO.idSettlementOperationPk, ");
		stringBuilderSql.append(" SO.settlementDate, "); //5
		stringBuilderSql.append(" SO.stockQuantity, ");  //6
		stringBuilderSql.append(" SO.settlementAmount, ");
		stringBuilderSql.append(" SO.indPartial, ");//8
		stringBuilderSql.append(" MO.indTermSettlement, ");//9
		stringBuilderSql.append(" MO.indCashStockBlock, ");
		stringBuilderSql.append(" MO.indTermStockBlock, ");//11
		stringBuilderSql.append(" MO.indPrimaryPlacement, ");//12
		stringBuilderSql.append(" MO.indReportingBalance, ");//13
		stringBuilderSql.append(" MO.securities.idSecurityCodePk");//14
		stringBuilderSql.append(" FROM SettlementOperation SO, MechanismOperation MO ");
		stringBuilderSql.append(" WHERE MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuilderSql.append(" and SO.operationPart = :idOperationPart ");
		stringBuilderSql.append(" and SO.operationState in (:lstOperationStates) ");
		stringBuilderSql.append(" and SO.settlementDate = :unfulfillmentDate ");
		stringBuilderSql.append(" and SO.indUnfulfilled != :indicatorOne ");
		
		if(Validations.validateIsNotNullAndNotEmpty(currency)) {
			stringBuilderSql.append(" and MO.securities.currency = :currency");
			parameters.put("currency", currency);
		}
		
		if (idMechanism != null) {
			stringBuilderSql.append(" and MO.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism ");
			parameters.put("idMechanism", idMechanism);
		}
		if (idModality != null) {
			stringBuilderSql.append(" and MO.mechanisnModality.id.idNegotiationModalityPk = :idModality ");
			parameters.put("idModality", idModality);
		}
		//rules to get an unfulfillment in cash part 
		if (ComponentConstant.CASH_PART.equals(idOperationPart))
		{
			//To unfulfillment in stocks we have to consider indicator about stocks
			if (UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive))
			{
				stringBuilderSql.append(" and SO.stockReference is null "); //did not completed to block stock balance in the operation
				//we have to consider only modalities that block stocks in cash part
				stringBuilderSql.append(" and MO.indCashStockBlock = :indicatorOne ");
			}
			//To unfulfillment in funds we have to consider indicator about funds
			else if (UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive))
			{
				stringBuilderSql.append(" and SO.fundsReference is null "); //did not completed to deposit the 
				//we have to consider only DVP settlement type
				stringBuilderSql.append(" and SO.settlementType = :idSettlementType ");
				parameters.put("idSettlementType", SettlementType.DVP.getCode()); //only if the settlement type of this operation is DVP
			}
			/****** To unfulfillment in Guarantes we have to consider indicator about Guarantes */
			else if (UnfulfillmentParameterType.GUARANTEES_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive))
			{
				stringBuilderSql.append(" and SO.marginReference is null "); //did not completed to deposit the 
				//we have to consider only DVP settlement type
				stringBuilderSql.append(" and SO.settlementType = :idSettlementType ");
				parameters.put("idSettlementType", SettlementType.DVP.getCode()); //only if the settlement type of this operation is DVP
			}
		}
		//rules to get an unfulfillment in term part 
		else if (ComponentConstant.TERM_PART.equals(idOperationPart))
		{
			//To unfulfillment in stocks we have to consider indicator about stocks
			if (UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive))
			{
				stringBuilderSql.append(" and SO.stockReference is null "); //did not completed to block stock balance in the operation
				//we have to consider only modalities that block stocks in term part
				stringBuilderSql.append(" and MO.indTermStockBlock = :indicatorOne ");
			}
			//To unfulfillment in funds we have to consider indicator about funds
			else if (UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive))
			{
				stringBuilderSql.append(" and SO.fundsReference is null "); //did not completed to deposit the 
				//we have to consider only DVP settlement type
				stringBuilderSql.append(" and SO.settlementType = :idSettlementType ");
				parameters.put("idSettlementType", SettlementType.DVP.getCode());
			}
		}
		
		parameters.put("idOperationPart", idOperationPart);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("unfulfillmentDate", unfulfillmentDate);

		List<Integer> lstOperationStates= new ArrayList<Integer>();
		if (ComponentConstant.CASH_PART.equals(idOperationPart)) {
			//to cash part we have to take only mechanism operations in REGISTERED and ASSIGNED states
			lstOperationStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
			lstOperationStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		} else if (ComponentConstant.TERM_PART.equals(idOperationPart)) {
			//to term part we have to take only mechanism operations in CASH SETTLED state
			lstOperationStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		}
		parameters.put("lstOperationStates", lstOperationStates);
		
		lstObjects = findListByQueryString(stringBuilderSql.toString(), parameters);

		return lstObjects;
	}
	
	
	
	
	/**
	 * Method to get the list of unfulfilled holder accounts from the unfulfilled mechanism operation .
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idUnfulfillmentMotive the id unfulfillment motive
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListUnfulfilledAccountSettlementOperations(Long idSettlementOperation, Long idUnfulfillmentMotive)
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT idSettlementAccountPk, ");
		stringBuffer.append(" stockQuantity, ");
		stringBuffer.append(" settlementAmount, ");
		stringBuffer.append(" 0 "); //ID Account valorization. It is not required for this 
		stringBuffer.append(" FROM SettlementAccountOperation ");
		stringBuffer.append(" WHERE settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and role = :idRole ");
		stringBuffer.append(" and operationState = :idHolderAccountState ");
		if (UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			stringBuffer.append(" and stockReference is null "); //did not completed to block stock balance in the operation
		} else if (UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			stringBuffer.append(" and fundsReference is null "); //did not completed the deposit 
		}
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementOperation", idSettlementOperation);
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		if (UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			parameters.put("idRole", ComponentConstant.SALE_ROLE);
		} else if (UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			parameters.put("idRole", ComponentConstant.PURCHARSE_ROLE);
		}
		
		lstObjects =findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	
	/**
	 * Method to get the list of unfulfilled holder accounts from the unfulfilled swap operation .
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param idOperationPart the id operation part
	 * @param idUnfulfillmentMotive the id unfulfillment motive
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListUnfulfilledAccountSwapOperations(Long idMechanismOperation, Integer idOperationPart, Long idUnfulfillmentMotive)
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HAO.idHolderAccountOperationPk, ");
		stringBuffer.append(" HAO.stockQuantity, ");
		stringBuffer.append(" HAO.cashAmount, ");
		stringBuffer.append(" 0 "); //ID Account valorization. It is not required for this 
		stringBuffer.append(" FROM HolderAccountOperation HAO, SwapOperation SO ");
		stringBuffer.append(" WHERE SO.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and HAO.swapOperation.idSwapOperationPk = SO.idSwapOperationPk ");
		stringBuffer.append(" and HAO.operationPart = :idOperationPart ");
		stringBuffer.append(" and HAO.role = :idRole ");
		stringBuffer.append(" and HAO.holderAccountState = :idHolderAccountState ");
		if (UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			stringBuffer.append(" and HAO.stockReference is null "); //did not completed to block stock balance in the operation
		} else if (UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			stringBuffer.append(" and HAO.fundsReference is null "); //did not completed the deposit 
		}
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("idOperationPart", idOperationPart);
		parameters.put("idHolderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		if (UnfulfillmentParameterType.STOCKS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			parameters.put("idRole", ComponentConstant.SALE_ROLE);
		} else if (UnfulfillmentParameterType.FUNDS_UNFULFILLMENT_MOTIVE.getCode().equals(idUnfulfillmentMotive)) {
			parameters.put("idRole", ComponentConstant.PURCHARSE_ROLE);
		}
		
		lstObjects =findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	
	/**
	 * Gets the list margin unfulfilled operations.
	 *
	 * @return the list margin unfulfilled operations
	 */
	public List<Object[]> getListMarginUnfulfilledOperations()
	{
		List<Object[]> lstObjects = new ArrayList<Object[]>();
		
		return lstObjects;
	}
	
	
	/**
	 * Update unfulfillment settlement operation.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateUnfulfillmentSettlementOperation(Long idSettlementOperation, LoggerUser loggerUser) throws ServiceException
	{
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" UPDATE SettlementOperation SET indUnfulfilled = :indicatorOne ");
		//stringBuffer.append(" , indUnfulfilled = :lastModifyDate ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indicatorOne", ComponentConstant.ONE);
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	
	/**
	 * Update unfulfillment holder account operation.
	 *
	 * @param lstIdHolderAccountOperation the lst id holder account operation
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateUnfulfillmentHolderAccountOperation(List<Long> lstIdHolderAccountOperation, LoggerUser loggerUser) throws ServiceException
	{
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" UPDATE HolderAccountOperation SET indUnfulfilled = :indicatorOne ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idHolderAccountOperationPk in (:lstIdHolderAccountOperation) ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indicatorOne", ComponentConstant.ONE);
		query.setParameter("lstIdHolderAccountOperation", lstIdHolderAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	
	/**
	 * Method to save the holder account unfulfillment and update the unfulfilled holder accounts.
	 *
	 * @param lstObject the lst object
	 * @param objOperationUnfulfillment the obj operation unfulfillment
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void saveHolderAccountUnfulfillment(List<Object[]> lstObject, OperationUnfulfillment objOperationUnfulfillment, LoggerUser loggerUser) throws ServiceException
	{
		List<Long> lstHolderAccountOperations= new ArrayList<Long>();
		for (Object[] arrObject: lstObject)
		{
			HolderAccountUnfulfillment objHolderAccountUnfulfillment= new HolderAccountUnfulfillment();
			
			//ID holder account operation
			lstHolderAccountOperations.add(new Long(arrObject[0].toString()));
			
			SettlementAccountOperation objSettlementAccountOperation = new SettlementAccountOperation();
			objSettlementAccountOperation.setIdSettlementAccountPk(new Long(arrObject[0].toString()));
			objHolderAccountUnfulfillment.setSettlementAccountOperation(objSettlementAccountOperation);
			
			objHolderAccountUnfulfillment.setOperationUnfulfillment(objOperationUnfulfillment);
			
			if (Validations.validateIsNotNull(arrObject[1]))
				objHolderAccountUnfulfillment.setUnfulfilledQuantity(new BigDecimal(arrObject[1].toString()));
			
			if (Validations.validateIsNotNull(arrObject[2]))
				objHolderAccountUnfulfillment.setUnfulfilledAmount(new BigDecimal(arrObject[2].toString()));
			
			//this is only if we have guarantees unfulfillment
			if (Validations.validateIsNotNullAndPositive(new Long(arrObject[3].toString())))
			{
				AccountOperationValorization objAccountOperationValorization = new AccountOperationValorization();
				objAccountOperationValorization.setIdAccountValorizationPk(new Long(arrObject[3].toString()));
				objHolderAccountUnfulfillment.setAccountOperationValorization(objAccountOperationValorization);
			}
			
			//we save the holder account unfulfillment
			create(objHolderAccountUnfulfillment);
		}
		//finally we update the indicator abot unfulfillment in the holder accounts operation
		updateUnfulfillmentHolderAccountOperation(lstHolderAccountOperations, loggerUser);
	}


	/**
	 * Gets the settlement account data.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @return the settlement account data
	 */
	public List<Object[]> getSettlementAccountData(Long idSettlementOperation) {
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT SAO.idSettlementAccountPk, ");
		stringBuffer.append(" SAO.holderAccountOperation.idHolderAccountOperationPk ");
		stringBuffer.append(" FROM SettlementAccountOperation SAO ");
		stringBuffer.append(" WHERE SAO.settlementOperation.idSettlementOperationPk  = :idSettlementOperation ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementOperation", idSettlementOperation);
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	/**
	 * Gets the settlement accounts to revert.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idInitialSettlementOperation the id initial settlement operation
	 * @return the settlement accounts to revert
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getSettlementAccountsToRevert(Long idSettlementOperation, Long idInitialSettlementOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT OSAO, SAO.idSettlementAccountPk, SAO.stockQuantity ");
		stringBuffer.append(" FROM SettlementAccountOperation OSAO, SettlementAccountOperation SAO ");
		stringBuffer.append(" WHERE OSAO.holderAccountOperation.idHolderAccountOperationPk = SAO.holderAccountOperation.idHolderAccountOperationPk ");
		stringBuffer.append(" and SAO.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and OSAO.settlementOperation.idSettlementOperationPk = :idInitialSettlementOperation ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementOperation", idSettlementOperation);
		parameters.put("idInitialSettlementOperation", idInitialSettlementOperation);
		
		return  (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}


	/**
	 * Gets the participant settlements to revert.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idInitialSettlementOperation the id initial settlement operation
	 * @return the participant settlements to revert
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getParticipantSettlementsToRevert(Long idSettlementOperation, Long idInitialSettlementOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT OPS, PS.stockQuantity ");
		stringBuffer.append(" FROM ParticipantSettlement OPS, ParticipantSettlement PS ");
		stringBuffer.append(" WHERE OPS.participant.idParticipantPk = PS.participant.idParticipantPk ");
		stringBuffer.append(" and OPS.role = PS.role ");
		stringBuffer.append(" and OPS.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and PS.settlementOperation.idSettlementOperationPk = :idInitialSettlementOperation ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementOperation", idSettlementOperation);
		parameters.put("idInitialSettlementOperation", idInitialSettlementOperation);
		
		return  (List<Object[]>) findListByQueryString(stringBuffer.toString(), parameters);
	}
	
}
