package com.pradera.settlements.core.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class MonitorSettlementAttributes.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/09/2013
 */
public class MonitorSettlementAttributes implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The negotiation mechanism list. */
	private List<NegotiationMechanism> negotiationMechanismList;
	
	/** The disable settlement. */
	private boolean disableSettlement;
	
	/** The modality group list. */
	private List<ModalityGroup> modalityGroupList;
	
	/** The modality group by sett schema list. */
	private List<ModalityGroup> modalityGroupBySettSchemaList;
	
	/** The net process type list. */
	private List<ParameterTable> netProcessTypeList = new ArrayList<ParameterTable>(0);
	
	/** The remove motive list. */
	private List<ParameterTable> removeMotiveList = new ArrayList<ParameterTable>(0);
	
	/** The mechanism operation state list. */
	private List<ParameterTable> mechanismOperationStateList = new ArrayList<ParameterTable>(0);
	
	/** The mechanism operation state list. */
	private List<ParameterTable> currencyList = new ArrayList<ParameterTable>(0);
	
	/** The monitor sett detail list. */
	private List<MonitorSettDetailTO> monitorSettDetailList;//List to handle modalities with operations and numbers
	
	/** The exchange current rate. */
	private DailyExchangeRates dailyExchangeRate;//Attribute to handle current exchange rate
	
	/** The is net settlement. */
	private Boolean isNetSettlement;
	
	/** The show settlement process. */
	private Boolean showSettlementProcess;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The modality settlement list. */
	private List<ModalitySettlementTO> modalitySettlementList;
	
	/** The settlement process list. */
	private ModalitySettlementTO modSettlementSelected;
	
	/** The settlement proc selected. */
	private SettlementProcess settlementProcSelected;
	
	/** The operations by modality. */
	private List<SettlementOperation> selectedOperations;//List to handle operations by modality
	
	/** The amounts by modality. */
	private MonitoringAmountsTO amountsByModality;//object to handle sum of amounts on modalityList
	
	/** The part position list. */
	private List<ParticipantPosition> partPositionList;//list to handle part Position list when net process is link for detail
	
	/** The map parameters. */
	private Map<Integer,ParameterTable> mapParameters = new HashMap<Integer,ParameterTable>();

	/**
	 * Gets the negotiation mechanism list.
	 *
	 * @return the negotiation mechanism list
	 */
	public List<NegotiationMechanism> getNegotiationMechanismList() {
		return negotiationMechanismList;
	}
	/**
	 * Sets the negotiation mechanism list.
	 *
	 * @param negotiationMechanismList the new negotiation mechanism list
	 */
	public void setNegotiationMechanismList(
			List<NegotiationMechanism> negotiationMechanismList) {
		this.negotiationMechanismList = negotiationMechanismList;
	}

	/**
	 * Gets the modality group list.
	 *
	 * @return the modality group list
	 */
	public List<ModalityGroup> getModalityGroupList() {
		return modalityGroupList;
	}
	/**
	 * Sets the modality group list.
	 *
	 * @param modalityGroupList the new modality group list
	 */
	public void setModalityGroupList(List<ModalityGroup> modalityGroupList) {
		this.modalityGroupList = modalityGroupList;
	}
	
	
	
	/**
	 * Gets the modality group by sett schema list.
	 *
	 * @return the modality group by sett schema list
	 */
	public List<ModalityGroup> getModalityGroupBySettSchemaList() {
		return modalityGroupBySettSchemaList;
	}
	
	/**
	 * Sets the modality group by sett schema list.
	 *
	 * @param modalityGroupBySettSchemaList the new modality group by sett schema list
	 */
	public void setModalityGroupBySettSchemaList(
			List<ModalityGroup> modalityGroupBySettSchemaList) {
		this.modalityGroupBySettSchemaList = modalityGroupBySettSchemaList;
	}
	/**
	 * Gets the net process type list.
	 *
	 * @return the net process type list
	 */
	public List<ParameterTable> getNetProcessTypeList() {
		return netProcessTypeList;
	}
	/**
	 * Sets the net process type list.
	 *
	 * @param netProcessTypeList the new net process type list
	 */
	public void setNetProcessTypeList(List<ParameterTable> netProcessTypeList) {
		this.netProcessTypeList = netProcessTypeList;
	}
	/**
	 * Gets the daily exchange rate.
	 *
	 * @return the daily exchange rate
	 */
	public DailyExchangeRates getDailyExchangeRate() {
		return dailyExchangeRate;
	}
	/**
	 * Sets the daily exchange rate.
	 *
	 * @param dailyExchangeRate the new daily exchange rate
	 */
	public void setDailyExchangeRate(DailyExchangeRates dailyExchangeRate) {
		this.dailyExchangeRate = dailyExchangeRate;
	}

	/**
	 * Gets the monitor sett detail list.
	 *
	 * @return the monitor sett detail list
	 */
	public List<MonitorSettDetailTO> getMonitorSettDetailList() {
		return monitorSettDetailList;
	}
	/**
	 * Sets the monitor sett detail list.
	 *
	 * @param monitorSettDetailList the new monitor sett detail list
	 */
	public void setMonitorSettDetailList(List<MonitorSettDetailTO> monitorSettDetailList) {
		this.monitorSettDetailList = monitorSettDetailList;
	}

	
	/**
	 * Gets the checks if is net settlement.
	 *
	 * @return the checks if is net settlement
	 */
	public Boolean getIsNetSettlement() {
		return isNetSettlement;
	}
	
	/**
	 * Sets the checks if is net settlement.
	 *
	 * @param isNetSettlement the new checks if is net settlement
	 */
	public void setIsNetSettlement(Boolean isNetSettlement) {
		this.isNetSettlement = isNetSettlement;
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}
	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}
	/**
	 * Gets the modality settlement list.
	 *
	 * @return the modality settlement list
	 */
	public List<ModalitySettlementTO> getModalitySettlementList() {
		return modalitySettlementList;
	}
	/**
	 * Sets the modality settlement list.
	 *
	 * @param modalitySettlementList the new modality settlement list
	 */
	public void setModalitySettlementList(List<ModalitySettlementTO> modalitySettlementList) {
		this.modalitySettlementList = modalitySettlementList;
	}
	
	/**
	 * Gets the mod settlement selected.
	 *
	 * @return the mod settlement selected
	 */
	public ModalitySettlementTO getModSettlementSelected() {
		return modSettlementSelected;
	}
	
	/**
	 * Sets the mod settlement selected.
	 *
	 * @param modSettlementSelected the new mod settlement selected
	 */
	public void setModSettlementSelected(ModalitySettlementTO modSettlementSelected) {
		this.modSettlementSelected = modSettlementSelected;
	}
	
	/**
	 * Gets the settlement proc selected.
	 *
	 * @return the settlement proc selected
	 */
	public SettlementProcess getSettlementProcSelected() {
		return settlementProcSelected;
	}
	
	/**
	 * Sets the settlement proc selected.
	 *
	 * @param settlementProcSelected the new settlement proc selected
	 */
	public void setSettlementProcSelected(SettlementProcess settlementProcSelected) {
		this.settlementProcSelected = settlementProcSelected;
	}
	
	/**
	 * Gets the selected operations.
	 *
	 * @return the selected operations
	 */
	public List<SettlementOperation> getSelectedOperations() {
		return selectedOperations;
	}
	
	/**
	 * Sets the selected operations.
	 *
	 * @param selectedOperations the new selected operations
	 */
	public void setSelectedOperations(
			List<SettlementOperation> selectedOperations) {
		this.selectedOperations = selectedOperations;
	}
	
	/**
	 * Gets the removes the motive list.
	 *
	 * @return the removes the motive list
	 */
	public List<ParameterTable> getRemoveMotiveList() {
		return removeMotiveList;
	}
	
	/**
	 * Sets the removes the motive list.
	 *
	 * @param removeMotiveList the new removes the motive list
	 */
	public void setRemoveMotiveList(List<ParameterTable> removeMotiveList) {
		this.removeMotiveList = removeMotiveList;
	}
	
	/**
	 * Gets the mechanism operation state list.
	 *
	 * @return the mechanism operation state list
	 */
	public List<ParameterTable> getMechanismOperationStateList() {
		return mechanismOperationStateList;
	}
	
	/**
	 * Sets the mechanism operation state list.
	 *
	 * @param mechanismOperationStateList the new mechanism operation state list
	 */
	public void setMechanismOperationStateList(
			List<ParameterTable> mechanismOperationStateList) {
		this.mechanismOperationStateList = mechanismOperationStateList;
	}
	
	/**
	 * Gets the amounts by modality.
	 *
	 * @return the amounts by modality
	 */
	public MonitoringAmountsTO getAmountsByModality() {
		return amountsByModality;
	}
	
	/**
	 * Sets the amounts by modality.
	 *
	 * @param amountsByModality the new amounts by modality
	 */
	public void setAmountsByModality(MonitoringAmountsTO amountsByModality) {
		this.amountsByModality = amountsByModality;
	}
	
	/**
	 * Gets the show settlement process.
	 *
	 * @return the show settlement process
	 */
	public Boolean getShowSettlementProcess() {
		return showSettlementProcess;
	}
	
	/**
	 * Sets the show settlement process.
	 *
	 * @param showSettlementProcess the new show settlement process
	 */
	public void setShowSettlementProcess(Boolean showSettlementProcess) {
		this.showSettlementProcess = showSettlementProcess;
	}
	
	/**
	 * Gets the map parameters.
	 *
	 * @return the map parameters
	 */
	public Map<Integer,ParameterTable> getMapParameters() {
		return mapParameters;
	}
	
	/**
	 * Sets the map parameters.
	 *
	 * @param mapParameters the map parameters
	 */
	public void setMapParameters(Map<Integer,ParameterTable> mapParameters) {
		this.mapParameters = mapParameters;
	}
	
	/**
	 * Gets the currency list.
	 *
	 * @return the currency list
	 */
	public List<ParameterTable> getCurrencyList() {
		return currencyList;
	}
	
	/**
	 * Sets the currency list.
	 *
	 * @param currencyList the new currency list
	 */
	public void setCurrencyList(List<ParameterTable> currencyList) {
		this.currencyList = currencyList;
	}
	
	/**
	 * Gets the part position list.
	 *
	 * @return the part position list
	 */
	public List<ParticipantPosition> getPartPositionList() {
		return partPositionList;
	}
	
	/**
	 * Sets the part position list.
	 *
	 * @param partPositionList the new part position list
	 */
	public void setPartPositionList(List<ParticipantPosition> partPositionList) {
		this.partPositionList = partPositionList;
	}
	
	/**
	 * Checks if is disable settlement.
	 *
	 * @return true, if is disable settlement
	 */
	public boolean isDisableSettlement() {
		return disableSettlement;
	}
	
	/**
	 * Sets the disable settlement.
	 *
	 * @param disableSettlement the new disable settlement
	 */
	public void setDisableSettlement(boolean disableSettlement) {
		this.disableSettlement = disableSettlement;
	}
}