/*
 * 
 */
package com.pradera.settlements.core.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.settlement.type.SettlementProcessStateType;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class MonitorSettlementAttributes.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/09/2013
 */
public class MonitorSettDetailTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The settlement date. */
	private Date settlementDate;
	
	/** The settlement scheme pk. */
	private Integer settlementSchemePk;
	
	/** The negotiation mechanism pk. */
	private Long negotiationMechanismPk;
	
	/** The modality group pk. */
	private Long modalityGroupPk;
	
	/** The process type pk. */
	private Long processTypePk;
	
	/** The operations quantity settlemented. */
	private Long operationsQuantitySettlemented;
	
	/** The operations settlement. */
	private List<MechanismOperation> operationsSettlement;
	
	/** The operations quantity pendiented. */
	private Long operationsQuantityPendiented;
	
	/** The operations pendient. */
	private List<MechanismOperation> operationsPendient;
	
	/** The operations total operations. */
	private Long operationsTotalOperations;
	
	/** The mechanism operations. */
	private List<MechanismOperation> mechanismOperations;
	
	/** The operations quantity canceled. */
	private Long operationsQuantityCanceled;
	
	/** The operations canceled. */
	private List<MechanismOperation> operationsCanceled;
	
	/** The monitoring settlement pk. */
	private Long monitoringSettlementPk;
	
	/** The negotitaionmodality pk. */
	private Long negotitaionmodalityPk;
	
	/** The currency pk. */
	private Integer currencyPk;
	
	/** The negotiation mechanism name. */
	private String negotiationMechanismName;
	
	/** The modality group name. */
	private String modalityGroupName;
	
	/** The negotiation modality name. */
	private String negotiationModalityName;
	
	/** The id process state. */
	private Long idProcessState;
	
	/** The id settlement process pk. */
	private Long idSettlementProcessPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The account reference type. */
	private Long accountReferenceType;
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	/**
	 * Gets the settlement scheme pk.
	 *
	 * @return the settlement scheme pk
	 */
	public Integer getSettlementSchemePk() {
		return settlementSchemePk;
	}
	/**
	 * Sets the settlement scheme pk.
	 *
	 * @param settlementSchemePk the new settlement scheme pk
	 */
	public void setSettlementSchemePk(Integer settlementSchemePk) {
		this.settlementSchemePk = settlementSchemePk;
	}
	/**
	 * Gets the negotiation mechanism pk.
	 *
	 * @return the negotiation mechanism pk
	 */
	public Long getNegotiationMechanismPk() {
		return negotiationMechanismPk;
	}
	/**
	 * Sets the negotiation mechanism pk.
	 *
	 * @param negotiationMechanismPk the new negotiation mechanism pk
	 */
	public void setNegotiationMechanismPk(Long negotiationMechanismPk) {
		this.negotiationMechanismPk = negotiationMechanismPk;
	}
	/**
	 * Gets the modality group pk.
	 *
	 * @return the modality group pk
	 */
	public Long getModalityGroupPk() {
		return modalityGroupPk;
	}
	/**
	 * Sets the modality group pk.
	 *
	 * @param modalityGroupPk the new modality group pk
	 */
	public void setModalityGroupPk(Long modalityGroupPk) {
		this.modalityGroupPk = modalityGroupPk;
	}
	/**
	 * Gets the monitoring settlement pk.
	 *
	 * @return the monitoring settlement pk
	 */
	public Long getMonitoringSettlementPk() {
		return monitoringSettlementPk;
	}
	/**
	 * Sets the monitoring settlement pk.
	 *
	 * @param monitoringSettlementPk the new monitoring settlement pk
	 */
	public void setMonitoringSettlementPk(Long monitoringSettlementPk) {
		this.monitoringSettlementPk = monitoringSettlementPk;
	}
	/**
	 * Gets the negotitaionmodality pk.
	 *
	 * @return the negotitaionmodality pk
	 */
	public Long getNegotitaionmodalityPk() {
		return negotitaionmodalityPk;
	}
	/**
	 * Sets the negotitaionmodality pk.
	 *
	 * @param negotitaionmodalityPk the new negotitaionmodality pk
	 */
	public void setNegotitaionmodalityPk(Long negotitaionmodalityPk) {
		this.negotitaionmodalityPk = negotitaionmodalityPk;
	}
	/**
	 * Gets the currency pk.
	 *
	 * @return the currency pk
	 */
	public Integer getCurrencyPk() {
		return currencyPk;
	}
	
	/**
	 * Gets the currency pk description.
	 *
	 * @return the currency pk description
	 */
	public String getCurrencyPkDescription() {
		return CurrencyType.get(currencyPk).getValue();
	}
	/**
	 * Sets the currency pk.
	 *
	 * @param currencyPk the new currency pk
	 */
	public void setCurrencyPk(Integer currencyPk) {
		this.currencyPk = currencyPk;
	}
	/**
	 * Gets the negotiation mechanism name.
	 *
	 * @return the negotiation mechanism name
	 */
	public String getNegotiationMechanismName() {
		return negotiationMechanismName;
	}
	/**
	 * Sets the negotiation mechanism name.
	 *
	 * @param negotiationMechanismName the new negotiation mechanism name
	 */
	public void setNegotiationMechanismName(String negotiationMechanismName) {
		this.negotiationMechanismName = negotiationMechanismName;
	}
	/**
	 * Gets the modality group name.
	 *
	 * @return the modality group name
	 */
	public String getModalityGroupName() {
		return modalityGroupName;
	}
	/**
	 * Sets the modality group name.
	 *
	 * @param modalityGroupName the new modality group name
	 */
	public void setModalityGroupName(String modalityGroupName) {
		this.modalityGroupName = modalityGroupName;
	}
	/**
	 * Gets the negotiation modality name.
	 *
	 * @return the negotiation modality name
	 */
	public String getNegotiationModalityName() {
		return negotiationModalityName;
	}
	/**
	 * Sets the negotiation modality name.
	 *
	 * @param negotiationModalityName the new negotiation modality name
	 */
	public void setNegotiationModalityName(String negotiationModalityName) {
		this.negotiationModalityName = negotiationModalityName;
	}
	/**
	 * Gets the process type pk.
	 *
	 * @return the process type pk
	 */
	public Long getProcessTypePk() {
		return processTypePk;
	}
	/**
	 * Sets the process type pk.
	 *
	 * @param processTypePk the new process type pk
	 */
	public void setProcessTypePk(Long processTypePk) {
		this.processTypePk = processTypePk;
	}
	/**
	 * Gets the operations quantity settlemented.
	 *
	 * @return the operations quantity settlemented
	 */
	public Long getOperationsQuantitySettlemented() {
		return operationsQuantitySettlemented;
	}
	/**
	 * Sets the operations quantity settlemented.
	 *
	 * @param operationsQuantitySettlemented the new operations quantity settlemented
	 */
	public void setOperationsQuantitySettlemented(Long operationsQuantitySettlemented) {
		this.operationsQuantitySettlemented = operationsQuantitySettlemented;
	}
	/**
	 * Gets the operations quantity pendiented.
	 *
	 * @return the operations quantity pendiented
	 */
	public Long getOperationsQuantityPendiented() {
		return operationsQuantityPendiented;
	}
	/**
	 * Sets the operations quantity pendiented.
	 *
	 * @param operationsQuantityPendiented the new operations quantity pendiented
	 */
	public void setOperationsQuantityPendiented(Long operationsQuantityPendiented) {
		this.operationsQuantityPendiented = operationsQuantityPendiented;
	}
	/**
	 * Gets the id process state.
	 *
	 * @return the id process state
	 */
	public Long getIdProcessState() {
		return idProcessState;
	}
	
	/**
	 * Gets the id process state description.
	 *
	 * @return the id process state description
	 */
	public String getIdProcessStateDescription() {
		return SettlementProcessStateType.lookup.get(new Integer(idProcessState.toString())).getDescription();
	} 
	/**
	 * Sets the id process state.
	 *
	 * @param idProcessState the new id process state
	 */
	public void setIdProcessState(Long idProcessState) {
		this.idProcessState = idProcessState;
	}
	
	/**
	 * Instantiates a new monitor sett detail to.
	 *
	 * @param idSettlementProcessPk the id settlement process pk
	 * @param currencyPk the currency pk
	 * @param idModalityGroup the id modality group
	 * @param modalityGroupName the modality group name
	 * @param negotiationModalityName the negotiation modality name
	 * @param idProcessState the id process state
	 */
	public MonitorSettDetailTO(Long idSettlementProcessPk, Integer currencyPk, Long idModalityGroup, String modalityGroupName,
							   String negotiationModalityName, Long idProcessState) {
		this.idSettlementProcessPk = idSettlementProcessPk;
		this.currencyPk = currencyPk;
		this.modalityGroupPk = idModalityGroup;
		this.modalityGroupName = modalityGroupName;
		this.negotiationModalityName = negotiationModalityName;
		this.idProcessState = idProcessState;
	}
	
	/**
	 * Instantiates a new monitor settlement to.
	 *
	 * @param idSettlementProcessPk the id settlement process pk
	 * @param currencyPk the currency pk
	 * @param idModalityGroup the id modality group
	 * @param modalityGroupName the modality group name
	 * @param idProcessState the id process state
	 */
	public MonitorSettDetailTO(Long idSettlementProcessPk, Integer currencyPk, Long idModalityGroup, String modalityGroupName,
					   		   Long idProcessState) {
		this.idSettlementProcessPk = idSettlementProcessPk;
		this.currencyPk = currencyPk;
		this.modalityGroupPk = idModalityGroup;
		this.modalityGroupName = modalityGroupName;
		this.idProcessState = idProcessState;
	}
	
	/**
	 * Instantiates a new monitor sett detail to.
	 */
	public MonitorSettDetailTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * Gets the id settlement process pk.
	 *
	 * @return the id settlement process pk
	 */
	public Long getIdSettlementProcessPk() {
		return idSettlementProcessPk;
	}
	/**
	 * Sets the id settlement process pk.
	 *
	 * @param idSettlementProcessPk the new id settlement process pk
	 */
	public void setIdSettlementProcessPk(Long idSettlementProcessPk) {
		this.idSettlementProcessPk = idSettlementProcessPk;
	}
	
	/**
	 * Gets the operations total operations.
	 *
	 * @return the operations total operations
	 */
	public Long getOperationsTotalOperations() {
		return operationsTotalOperations;
	}
	
	/**
	 * Sets the operations total operations.
	 *
	 * @param operationsTotalOperations the new operations total operations
	 */
	public void setOperationsTotalOperations(Long operationsTotalOperations) {
		this.operationsTotalOperations = operationsTotalOperations;
	}
	
	/**
	 * Gets the operations quantity canceled.
	 *
	 * @return the operations quantity canceled
	 */
	public Long getOperationsQuantityCanceled() {
		return operationsQuantityCanceled;
	}
	
	/**
	 * Sets the operations quantity canceled.
	 *
	 * @param operationsQuantityCanceled the new operations quantity canceled
	 */
	public void setOperationsQuantityCanceled(Long operationsQuantityCanceled) {
		this.operationsQuantityCanceled = operationsQuantityCanceled;
	}
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	/**
	 * Gets the account reference type.
	 *
	 * @return the account reference type
	 */
	public Long getAccountReferenceType() {
		return accountReferenceType;
	}
	/**
	 * Sets the account reference type.
	 *
	 * @param accountReferenceType the new account reference type
	 */
	public void setAccountReferenceType(Long accountReferenceType) {
		this.accountReferenceType = accountReferenceType;
	}
	/**
	 * Gets the mechanism operations.
	 *
	 * @return the mechanism operations
	 */
	public List<MechanismOperation> getMechanismOperations() {
		return mechanismOperations;
	}
	/**
	 * Sets the mechanism operations.
	 *
	 * @param mechanismOperations the new mechanism operations
	 */
	public void setMechanismOperations(List<MechanismOperation> mechanismOperations) {
		this.mechanismOperations = mechanismOperations;
	}
	
	/**
	 * Gets the operations settlement.
	 *
	 * @return the operations settlement
	 */
	public List<MechanismOperation> getOperationsSettlement() {
		return operationsSettlement;
	}
	
	/**
	 * Sets the operations settlement.
	 *
	 * @param operationsSettlement the new operations settlement
	 */
	public void setOperationsSettlement(
			List<MechanismOperation> operationsSettlement) {
		this.operationsSettlement = operationsSettlement;
	}
	
	/**
	 * Gets the operations pendient.
	 *
	 * @return the operations pendient
	 */
	public List<MechanismOperation> getOperationsPendient() {
		return operationsPendient;
	}
	
	/**
	 * Sets the operations pendient.
	 *
	 * @param operationsPendient the new operations pendient
	 */
	public void setOperationsPendient(List<MechanismOperation> operationsPendient) {
		this.operationsPendient = operationsPendient;
	}
	
	/**
	 * Gets the operations canceled.
	 *
	 * @return the operations canceled
	 */
	public List<MechanismOperation> getOperationsCanceled() {
		return operationsCanceled;
	}
	
	/**
	 * Sets the operations canceled.
	 *
	 * @param operationsCanceled the new operations canceled
	 */
	public void setOperationsCanceled(List<MechanismOperation> operationsCanceled) {
		this.operationsCanceled = operationsCanceled;
	}
}