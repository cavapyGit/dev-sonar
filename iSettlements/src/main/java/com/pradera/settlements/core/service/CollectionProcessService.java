package com.pradera.settlements.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.swift.facade.SwiftWithdrawlProcess;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.InChargeNegotiationType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantOperationStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.settlements.core.to.FundsParticipantSettlementTO;
import com.pradera.settlements.core.to.FundsSettlementOperationTO;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.webclient.SettlementServiceConsumer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionProcessService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class CollectionProcessService extends CrudDaoServiceBean{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(CollectionProcessService.class);
	
	/** The funds component service. */
	@Inject
	private Instance<FundsComponentService> fundsComponentService;
	
	/** The swift withdrawl process. */
	@EJB
	private SwiftWithdrawlProcess swiftWithdrawlProcess;
	
	/** The settlement process service bean. */
	@EJB
	private SettlementProcessService settlementProcessServiceBean;

	/** The settlement service consumer. */
	@EJB
	private SettlementServiceConsumer settlementServiceConsumer;
	
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/**
	 * To get the list of settlement broker in purchase position pending to collect funds in the mechanism operation.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param settlementDate the settlement date
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListMechanismOperationWithFunds(Long idMechanism, Long idModalityGroup, Integer currency, Date settlementDate)
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT SO.idSettlementOperationPk, "); 
		stringBuffer.append(" SO.operationPart, ");
		stringBuffer.append(" SO.stockQuantity, ");
		stringBuffer.append(" MO.idMechanismOperationPk, ");
		stringBuffer.append(" MO.stockQuantity, ");
		stringBuffer.append(" PS.idParticipantSettlementPk, ");//5
		stringBuffer.append(" PS.participant.idParticipantPk, ");//6
		stringBuffer.append(" PS.stockQuantity, ");//7
		stringBuffer.append(" PS.settlementAmount, ");//8
		stringBuffer.append(" PS.role ");//9
		stringBuffer.append(" FROM SettlementOperation SO, ParticipantSettlement PS, MechanismOperation MO, ModalityGroupDetail MGD ");
		stringBuffer.append(" where MGD.negotiationMechanism.idNegotiationMechanismPk = MO.mechanisnModality.id.idNegotiationMechanismPk ");
		stringBuffer.append(" and MGD.negotiationModality.idNegotiationModalityPk =  MO.mechanisnModality.id.idNegotiationModalityPk ");
		stringBuffer.append(" and MO.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		stringBuffer.append(" and SO.idSettlementOperationPk = PS.settlementOperation.idSettlementOperationPk ");
		stringBuffer.append(" and MO.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism ");
		stringBuffer.append(" and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		stringBuffer.append(" and ( (SO.operationPart = :cashPart ");
		stringBuffer.append("    	 and SO.operationState = :cashState ");
		stringBuffer.append(" 		 and ((MO.mechanisnModality.negotiationModality.indCashStockBlock = :indicatorOne and SO.stockReference = :idStockReference) or ");
		stringBuffer.append("             (MO.mechanisnModality.negotiationModality.indCashStockBlock != :indicatorOne)) )");
		stringBuffer.append("    or (SO.operationPart = :termPart");
		stringBuffer.append("    	 and SO.operationState = :termState ");
		stringBuffer.append("        and ((MO.mechanisnModality.negotiationModality.indTermStockBlock = :indicatorOne and SO.stockReference = :idStockReference) or ");
		stringBuffer.append("             (MO.mechanisnModality.negotiationModality.indTermStockBlock != :indicatorOne)) )  )");
		stringBuffer.append(" and SO.settlementType = :settlementType ");
		stringBuffer.append(" and SO.fundsReference = :idFundsReference ");
		stringBuffer.append(" and SO.settlementDate = :settlementDate ");
		stringBuffer.append(" and SO.settlementCurrency = :currency	");
		stringBuffer.append(" and PS.role = :idRole ");
		stringBuffer.append(" and PS.operationState = :participantState ");
		stringBuffer.append(" ORDER BY SO.idSettlementOperationPk, SO.operationPart ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanism", idMechanism);
		parameters.put("settlementType", SettlementType.DVP.getCode());
		parameters.put("idModalityGroup", idModalityGroup);
		parameters.put("currency", currency);
		parameters.put("idRole", ComponentConstant.PURCHARSE_ROLE);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("termPart", ComponentConstant.TERM_PART);
		parameters.put("settlementDate", settlementDate);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idStockReference", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		parameters.put("idFundsReference", AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
		parameters.put("cashState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		parameters.put("termState", MechanismOperationStateType.CASH_SETTLED.getCode());
		parameters.put("participantState", ParticipantOperationStateType.CONFIRM.getCode());
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		
		return lstObjects;
	}
	
	/**
	 * Populate funds participant settlement operations.
	 *
	 * @param lstOperationSettlements the lst operation settlements
	 * @return the list
	 */
	public List<FundsSettlementOperationTO> populateFundsParticipantSettlementOperations(List<Object[]> lstOperationSettlements) {
		Map<Long,FundsSettlementOperationTO> settlementOperations = new LinkedHashMap<Long, FundsSettlementOperationTO>();
		for (Object[] arrObject: lstOperationSettlements)
		{
			Long idSettlementOperation = (Long)arrObject[0];
			FundsSettlementOperationTO objSettlementOperationTO = settlementOperations.get(idSettlementOperation);
			if(objSettlementOperationTO == null){
				objSettlementOperationTO = new FundsSettlementOperationTO();
				objSettlementOperationTO.setIdSettlementOperation((Long)arrObject[0]);
				objSettlementOperationTO.setOperationPart((Integer)arrObject[1]);
				objSettlementOperationTO.setStockQuantity((BigDecimal)arrObject[2]);
				objSettlementOperationTO.setIdMechanismOperation((Long)arrObject[3]);
				objSettlementOperationTO.setTotalQuantity((BigDecimal)arrObject[4]);
				settlementOperations.put(idSettlementOperation, objSettlementOperationTO);
			}
			
			FundsParticipantSettlementTO objParticipantSettlementTO = new FundsParticipantSettlementTO();
			objParticipantSettlementTO.setIdParticipantSettlement((Long)arrObject[5]);
			objParticipantSettlementTO.setIdParticipant((Long)arrObject[6]);
			objParticipantSettlementTO.setStockQuantity((BigDecimal)arrObject[7]);
			objParticipantSettlementTO.setSettlementAmount((BigDecimal)arrObject[8]);
			objParticipantSettlementTO.setRole((Integer)arrObject[9]);
			objSettlementOperationTO.getParticipantSettlements().add(objParticipantSettlementTO);
		}
		return new ArrayList<FundsSettlementOperationTO>(settlementOperations.values());
	}
	
	/**
	 * Method to collect the buyer participant in the respective cash account.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idParticipant the id participant
	 * @param totalAmount the total amount
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void collectGrossMechanismOperation(Long idMechanismOperation, Long idMechanism, Long idModalityGroup, Long idParticipant, 
																		BigDecimal totalAmount, Integer currency, LoggerUser loggerUser) throws ServiceException
	{
		InstitutionCashAccount  objInstitutionCashAccount = getInstitutionCashAccount(idMechanism, idModalityGroup, idParticipant, currency,SettlementSchemaType.GROSS.getCode());
		if (Validations.validateIsNull(objInstitutionCashAccount)) {
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS,
																PropertiesUtilities.getExceptionMessage(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage()));
		}
		//we create the Funds Operation
		FundsOperation objFundsOperation= new FundsOperation();
		objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
		objFundsOperation.setRegistryUser(loggerUser.getUserName());
		objFundsOperation.setRegistryDate(CommonsUtilities.currentDate());
		objFundsOperation.setCurrency(currency);
		objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
		objFundsOperation.setIndCentralized(ComponentConstant.ZERO);
		objFundsOperation.setOperationAmount(totalAmount);
		objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		objFundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_TRANSFER.getCode());
		objFundsOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode());
		objFundsOperation.setFundsOperationType(ParameterFundsOperationType.OPERATIONS_COLLECTION_WITHDRAWL.getCode());
		objFundsOperation.setIndCentralized(BooleanType.NO.getCode());
		objFundsOperation.setRegistryUser(loggerUser.getUserName());
		objFundsOperation.setParticipant(new Participant(idParticipant));
		objFundsOperation.setMechanismOperation(new MechanismOperation(idMechanismOperation));
		
		Map<String,Object> paramat= new HashMap<String,Object>();		
		paramat.put("idMechanismOperationPkParam", idMechanismOperation);
		MechanismOperation mo = findObjectByNamedQuery(MechanismOperation.PAYMENT_REFERENCE, paramat);
		objFundsOperation.setPaymentReference(mo.getPaymentReference());
		
		this.create(objFundsOperation);
		
		//now we have to execute the funds component
		FundsComponentTO objFundsComponentTO = new FundsComponentTO();
		objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
		objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_COLLECT_PROCESS.getCode());
		objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
		objFundsComponentTO.setCashAmount(totalAmount);
		objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
	}
	
	
	/**
	 * Method to collect the debtor participant position from its cash account .
	 *
	 * @param objParticipantPosition the obj participant position
	 * @param settlementProcess the settlement process
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idCurrency the id currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void collectNetParticipantPosition(ParticipantPosition objParticipantPosition, SettlementProcess settlementProcess,
			Long idMechanism, Long idModalityGroup, Integer idCurrency, LoggerUser loggerUser) throws ServiceException
	{
		//we have to get the Cash account from participant to collect the funds
		InstitutionCashAccount  objInstitutionCashAccount = getInstitutionCashAccount(idMechanism, idModalityGroup, 
																						objParticipantPosition.getParticipant().getIdParticipantPk(), 
																						idCurrency, settlementProcess.getSettlementSchema());
		if (Validations.validateIsNull(objInstitutionCashAccount)) {
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage());
		}
		//we create the Funds Operation
		FundsOperation objFundsOperation= new FundsOperation();
		objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
		objFundsOperation.setRegistryDate(CommonsUtilities.currentDate());
		objFundsOperation.setRegistryUser(loggerUser.getUserName());
		objFundsOperation.setCurrency(idCurrency);
		objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
		objFundsOperation.setIndCentralized(ComponentConstant.ZERO);
		objFundsOperation.setOperationAmount(objParticipantPosition.getNetPosition().abs());
		objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		objFundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_TRANSFER.getCode());
		objFundsOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode());
		objFundsOperation.setFundsOperationType(ParameterFundsOperationType.OPERATIONS_COLLECTION_WITHDRAWL.getCode());
		objFundsOperation.setIndCentralized(BooleanType.NO.getCode());
		objFundsOperation.setParticipant(objParticipantPosition.getParticipant());
		objFundsOperation.setSettlementProcess(settlementProcess);
		
		this.create(objFundsOperation);
		
		//now we have to execute the funds component
		FundsComponentTO objFundsComponentTO = new FundsComponentTO();
		objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
		objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.NET_OPERATIONS_COLLECT_PROCESS.getCode());
		objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
		objFundsComponentTO.setCashAmount(objParticipantPosition.getNetPosition().abs());
		objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
	}
	
	
	/**
	 * Method to deliver the funds to Seller participant by mechanism operation in its cash account.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idSettlementOperation the id settlement operation
	 * @param idMechanismOperation the id mechanism operation
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> deliverFundsMechanismOperation(Long idMechanism, Long idModalityGroup, Long idSettlementOperation, Long idMechanismOperation,
															Integer currency, LoggerUser loggerUser) throws ServiceException
	{
		List<Object[]> lstSellerParticipants = getListSellerPaticipantSettlement(idSettlementOperation);
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstSellerParticipants))
		{
			for (Object[] arrObject: lstSellerParticipants)
			{
				Long idParticipant = (Long)arrObject[0];
				BigDecimal totalAmount = (BigDecimal)arrObject[1];
				
				//we get the cash account from seller participant
				InstitutionCashAccount  objInstitutionCashAccount = getInstitutionCashAccount(idMechanism, idModalityGroup, idParticipant, currency,SettlementSchemaType.GROSS.getCode());
				if (Validations.validateIsNull(objInstitutionCashAccount)) {
					throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_SELLER_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_SELLER_NOTEXISTS.getMessage());
				}
				//we create the Funds Operation
				FundsOperation objFundsOperation= new FundsOperation();
				objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
				objFundsOperation.setCurrency(currency);
				objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
				objFundsOperation.setIndCentralized(ComponentConstant.ZERO);
				objFundsOperation.setOperationAmount(totalAmount);
				objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
				objFundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_TRANSFER.getCode());
				objFundsOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode());
				objFundsOperation.setFundsOperationType(ParameterFundsOperationType.OPERATIONS_COLLECTION_DEPOSIT.getCode());
				objFundsOperation.setParticipant(new Participant(idParticipant));
				objFundsOperation.setMechanismOperation(new MechanismOperation(idMechanismOperation));
				objFundsOperation.setRegistryUser(loggerUser.getUserName());
				objFundsOperation.setRegistryDate(new Date());

				Map<String,Object> paramat= new HashMap<String,Object>();		
				paramat.put("idMechanismOperationPkParam", idMechanismOperation);
				MechanismOperation mo = findObjectByNamedQuery(MechanismOperation.PAYMENT_REFERENCE, paramat);
				objFundsOperation.setPaymentReference(mo.getPaymentReference());				
				
				this.create(objFundsOperation);
				
				//now we have to execute the funds component
				FundsComponentTO objFundsComponentTO = new FundsComponentTO();
				objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
				objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_COLLECT_PROCESS.getCode());
				objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
				objFundsComponentTO.setCashAmount(totalAmount);
				objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
				fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
			}
		} else {
			lstSellerParticipants = getListSellerPaticipantSettlementByMechanismOperation(idMechanismOperation);
			if (Validations.validateListIsNotNullAndNotEmpty(lstSellerParticipants)){
				for (Object[] arrObject: lstSellerParticipants){
					Long idParticipant = (Long)arrObject[0];
					BigDecimal totalAmount = (BigDecimal)arrObject[1];
					
					//we get the cash account from seller participant
					InstitutionCashAccount  objInstitutionCashAccount = getInstitutionCashAccount(idMechanism, idModalityGroup, idParticipant, currency,SettlementSchemaType.GROSS.getCode());
					if (Validations.validateIsNull(objInstitutionCashAccount)) {
						throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_SELLER_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_SELLER_NOTEXISTS.getMessage());
					}
					//we create the Funds Operation
					FundsOperation objFundsOperation= new FundsOperation();
					objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
					objFundsOperation.setCurrency(currency);
					objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
					objFundsOperation.setIndCentralized(ComponentConstant.ZERO);
					objFundsOperation.setOperationAmount(totalAmount);
					objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
					objFundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_TRANSFER.getCode());
					objFundsOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode());
					objFundsOperation.setFundsOperationType(ParameterFundsOperationType.OPERATIONS_COLLECTION_DEPOSIT.getCode());
					objFundsOperation.setParticipant(new Participant(idParticipant));
					objFundsOperation.setMechanismOperation(new MechanismOperation(idMechanismOperation));
					objFundsOperation.setRegistryUser(loggerUser.getUserName());
					objFundsOperation.setRegistryDate(new Date());
					
					this.create(objFundsOperation);
					
					//now we have to execute the funds component
					FundsComponentTO objFundsComponentTO = new FundsComponentTO();
					objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
					objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_COLLECT_PROCESS.getCode());
					objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
					objFundsComponentTO.setCashAmount(totalAmount);
					objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
				}
			}
		}
		return lstSellerParticipants;
	}
	
	
	/**
	 * Method to deliver the funds to creditor participant by net position in its cash account.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param idParticipant the id participant
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param totalAmount the total amount
	 * @param loggerUSer the logger u ser
	 * @throws ServiceException the service exception
	 */
	public void deliverFundsCreditorParticipant(Long idSettlementProcess, Long idParticipant, Long idMechanism, Long idModalityGroup, 
																		Integer currency, BigDecimal totalAmount,LoggerUser loggerUSer) throws ServiceException
	{
		try {
			//we get the cash account from seller participant
			InstitutionCashAccount  objInstitutionCashAccount = getInstitutionCashAccount(idMechanism, idModalityGroup, idParticipant, currency,SettlementSchemaType.NET.getCode());
			
			if (Validations.validateIsNull(objInstitutionCashAccount)) {
				throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_SELLER_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_SELLER_NOTEXISTS.getMessage());
			}
			//we create the Funds Operation
			FundsOperation objFundsOperation= new FundsOperation();
			objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
			objFundsOperation.setCurrency(currency);
			objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
			objFundsOperation.setIndCentralized(ComponentConstant.ZERO);
			objFundsOperation.setOperationAmount(totalAmount);
			objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
			objFundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_TRANSFER.getCode());
			objFundsOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode());
			objFundsOperation.setFundsOperationType(ParameterFundsOperationType.OPERATIONS_COLLECTION_DEPOSIT.getCode());
			
			Participant objParticipant = new Participant();
			objParticipant.setIdParticipantPk(idParticipant);
			objFundsOperation.setParticipant(objParticipant);
			
			SettlementProcess objSettlementProcess= new SettlementProcess();
			objSettlementProcess.setIdSettlementProcessPk(idSettlementProcess);
			objFundsOperation.setSettlementProcess(objSettlementProcess);
			objFundsOperation.setRegistryDate(new Date());
			objFundsOperation.setRegistryUser(loggerUSer.getUserName());
			
			this.create(objFundsOperation);
			
			//now we have to execute the funds component
			FundsComponentTO objFundsComponentTO = new FundsComponentTO();
			objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
			objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.NET_OPERATIONS_COLLECT_PROCESS.getCode());
			objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
			objFundsComponentTO.setCashAmount(totalAmount);
			objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
			fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
		} catch (ServiceException e) {
			logger.error("idSettlementProcess: " +idSettlementProcess
								+", idMechanism: " +idMechanism
								+", idModalityGroup: " +idModalityGroup
								+", currency: " +currency
								+", idParticipant: " +idParticipant);
			e.printStackTrace();
			throw e;
		}
	}
	
	
	/**
	 * Gets the list seller paticipant settlement.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @return the list seller paticipant settlement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListSellerPaticipantSettlement(Long idSettlementOperation) throws ServiceException 
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT participant.idParticipantPk, ");
		stringBuffer.append(" settlementAmount, ");
		stringBuffer.append(" settlementCurrency, ");
		stringBuffer.append(" idParticipantSettlementPk ");
		stringBuffer.append(" FROM ParticipantSettlement ");
		stringBuffer.append(" WHERE settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and role = :role ");
		stringBuffer.append(" and operationState = :state ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSettlementOperation", idSettlementOperation);
		parameters.put("role", ComponentConstant.SALE_ROLE);
		parameters.put("state", ParticipantOperationStateType.CONFIRM.getCode());
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		return lstObjects;
	}
	
	public List<Object[]> getListSellerPaticipantSettlementByMechanismOperation(Long idMechanismOperation) throws ServiceException 
	{
		List<Object[]> lstObjects= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT participant.idParticipantPk, ");
		stringBuffer.append(" settlementAmount, ");
		stringBuffer.append(" settlementCurrency, ");
		stringBuffer.append(" idParticipantSettlementPk ");
		stringBuffer.append(" FROM ParticipantSettlement ");
		stringBuffer.append(" WHERE settlementOperation.mechanismOperation.tradeOperation.idTradeOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and role = :role ");
		stringBuffer.append(" and operationState = :state ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("role", ComponentConstant.SALE_ROLE);
		parameters.put("state", ParticipantOperationStateType.CONFIRM.getCode());
		
		lstObjects = findListByQueryString(stringBuffer.toString(), parameters);
		return lstObjects;
	}
	/**
	 * Update funds settlement account operation.
	 *
	 * @param idSettlementAccount the id settlement account
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsSettlementAccountOperation(Long idSettlementAccount, Long idFundsReference, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountOperation SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk = :idSettlementAccount ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccount", idSettlementAccount);
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update funds settlement account operation.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idFundsReference the id funds reference
	 * @param role the role
	 * @param loggerUser the logger user
	 */
	public void updateFundsSettlementAccountOperation(Long idSettlementOperation, Long idFundsReference, Integer role, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountOperation SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and role = :role");
		stringBuffer.append(" and operationState =:state ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("role", role);
		query.setParameter("state", HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update funds settlement account operation.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param operationSettlementState the operation settlement state
	 * @param role the role
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsSettlementAccountOperation(Long idSettlementProcess, Integer operationSettlementState, Integer role, Long idFundsReference, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountOperation SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk in ( ");
		stringBuffer.append("   SELECT SAO.idSettlementAccountPk "); 
		stringBuffer.append("   FROM 	SettlementAccountOperation SAO , OperationSettlement OS ");
		stringBuffer.append("   WHERE OS.settlementOperation.idSettlementOperationPk = SAO.settlementOperation.idSettlementOperationPk ");	
		stringBuffer.append("   and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		stringBuffer.append("   and SAO.settlementOperation.settlementType in (:dvpType) ");
		stringBuffer.append("   and OS.operationState = :operationSettlementState ");
		stringBuffer.append("   and SAO.role = :role ");
		stringBuffer.append("   and SAO.operationState = :state ");
		stringBuffer.append("  ) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("operationSettlementState", operationSettlementState);
		query.setParameter("dvpType", SettlementType.DVP.getCode());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.setParameter("role", role);
		query.setParameter("state", HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update settlement account settled.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @param settledQuantity the settled quantity
	 * @param loggerUser the logger user
	 */
	public void updateSettlementAccountSettled(Long idSettlementAccountOperation, BigDecimal settledQuantity, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountOperation SET settledQuantity = settledQuantity + :settledQuantity, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementAccountPk = :idSettlementAccount ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("settledQuantity", settledQuantity);
		query.setParameter("idSettlementAccount",idSettlementAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update orig settlement account settled.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param idHolderAccountOperation the id holder account operation
	 * @param settledQuantity the settled quantity
	 * @param loggerUser the logger user
	 */
	public void updateOrigSettlementAccountSettled(Long idMechanismOperation, Long idHolderAccountOperation, BigDecimal settledQuantity, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountOperation SET settledQuantity = settledQuantity + :settledQuantity, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation ");
		stringBuffer.append(" and settlementOperation.idSettlementOperationPk in  ");
		stringBuffer.append(" ( select idSettlementOperationPk from SettlementOperation ");
		stringBuffer.append("    where mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append("      and indPartial = :indicatorZero ) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("settledQuantity", settledQuantity);
		query.setParameter("indicatorZero", ComponentConstant.ZERO);
		query.setParameter("idMechanismOperation",idMechanismOperation);
		query.setParameter("idHolderAccountOperation",idHolderAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update funds holder account operation.
	 *
	 * @param lstIdMechanismOperation the lst id mechanism operation
	 * @param role the role
	 * @param operationPart the operation part
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsHolderAccountOperation(List<Long> lstIdMechanismOperation, Integer role, Integer operationPart, Long idFundsReference, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE HolderAccountOperation SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk in (:lstIdMechanismOperation) ");
		stringBuffer.append(" and role = :role ");
		stringBuffer.append(" and operationPart = :operationPart ");
		stringBuffer.append(" and holderAccountState = :holderAccountState ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lstIdMechanismOperation", lstIdMechanismOperation);
		query.setParameter("role", role);
		query.setParameter("operationPart", operationPart);
		query.setParameter("holderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update funds settlement operation.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsSettlementOperation(Long idSettlementOperation, Long idFundsReference, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation SET fundsReference = :idFundsReference, ");
		
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk = :idSettlementOperation ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		
		query.executeUpdate();
	}
	
	/**
	 * Update funds settlement operation.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param operationSettlementState the operation settlement state
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsSettlementOperation(Long idSettlementProcess, Integer operationSettlementState, Long idFundsReference, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettlementOperationPk in ( ");
		stringBuffer.append("  SELECT SO.idSettlementOperationPk "); 
		stringBuffer.append("  FROM 	SettlementOperation SO , OperationSettlement OS ");
		stringBuffer.append("  WHERE OS.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");	
		stringBuffer.append("  and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		stringBuffer.append("  and SO.settlementType in (:dvpType ) ");
		stringBuffer.append("  and OS.operationState = :operationSettlementState ");
		stringBuffer.append(" )");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.setParameter("operationSettlementState", operationSettlementState);
		query.setParameter("dvpType", SettlementType.DVP.getCode());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		
		query.executeUpdate();
	}
	
	/**
	 * Gets the institution cash account.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idParticipant the id participant
	 * @param currency the currency
	 * @param settlementSchema the settlement schema
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount getInstitutionCashAccount(Long idMechanism, Long idModalityGroup, Long 
			idParticipant, Integer currency, Integer settlementSchema) throws ServiceException{
		InstitutionCashAccount objInstitutionCashAccount= null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT ICA FROM InstitutionCashAccount ICA ");
			stringBuffer.append(" WHERE ICA.accountType = :idAccountType "); 
			stringBuffer.append(" and ICA.participant.idParticipantPk = :idParticipant ");
			stringBuffer.append(" and ICA.settlementSchema = :settlementSchema ");
			stringBuffer.append(" and ICA.currency = :currency ");
			stringBuffer.append(" and ICA.modalityGroup.idModalityGroupPk = :idModalityGroup ");
			stringBuffer.append(" and ICA.negotiationMechanism.idNegotiationMechanismPk = :idMechanism ");
			stringBuffer.append(" and ICA.accountState = :idAccountState ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idParticipant", idParticipant);
			query.setParameter("idAccountType", AccountCashFundsType.SETTLEMENT.getCode());
			query.setParameter("settlementSchema", settlementSchema);
			query.setParameter("currency", currency);
			query.setParameter("idModalityGroup", idModalityGroup);
			query.setParameter("idMechanism", idMechanism);
			query.setParameter("idAccountState", CashAccountStateType.ACTIVATE.getCode());
			
			objInstitutionCashAccount= (InstitutionCashAccount) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(e.getMessage());
			logger.info("idParticipant: " +idParticipant	
							+", idAccountType: " +AccountCashFundsType.SETTLEMENT.getCode()
							+", idMechanism: " +idMechanism
							+", idModalityGroup: " +idModalityGroup
							+", settlementSchema: " +settlementSchema
							+", currency: " +currency
							+", idAccountState: " +CashAccountStateType.ACTIVATE.getCode());
			//e.printStackTrace();
		}
		return objInstitutionCashAccount;
	}
	
	/**
	 * Exists participant collection pending.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationPart the operation part
	 * @return true, if successful
	 */
	public boolean existsParticipantCollectionPending(Long idMechanismOperation, Integer operationPart)
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT COUNT(*) ");
		stringBuffer.append(" FROM ParticipantOperation ");
		stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and role = :role ");
		stringBuffer.append(" and inchargeType = :inchargeType ");
		stringBuffer.append(" and operationPart = :operationPart ");
		stringBuffer.append(" and (fundsReference = :fundsReference or fundsReference is null) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("role", ComponentConstant.PURCHARSE_ROLE);
		query.setParameter("inchargeType", InChargeNegotiationType.INCHARGE_FUNDS.getCode());
		query.setParameter("fundsReference", AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
		query.setParameter("operationPart", operationPart);
		
		int count = ((Integer) query.getSingleResult()).intValue();
		if (count>0)
			return true;
		
		return false;
	}

	/**
	 * Send funds participant settlement.
	 *
	 * @param idParticipant the id participant
	 * @param settlementAmount the settlement amount
	 * @param currency the currency
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendFundsParticipantSettlement(Long idParticipant, BigDecimal settlementAmount, Integer currency, SettlementOperationTO objSettlementOperationTO, 
			LoggerUser loggerUser) throws ServiceException
	{
		InstitutionCashAccount objInstitutionCashAccount = getInstitutionCashAccount(objSettlementOperationTO.getIdMechanism(), 
									objSettlementOperationTO.getIdModalityGroup(), idParticipant,
									currency,objSettlementOperationTO.getSettlementSchema());
		if (Validations.validateIsNull(objInstitutionCashAccount)) {
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage());
		}
		InstitutionCashAccount objBcrdCashAccount = objInstitutionCashAccount.getInstitutionCashAccount(); //cash account related to BCRD
		if (Validations.validateIsNull(objBcrdCashAccount)) {
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS, "BCB "+ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage());
		}
		
		//we create the withdrawal funds operation from BCRD
		//FundsOperation objRefFundsOperation= createFundsOperationGrossSettlement(idParticipant, objSettlementOperationTO.getIdMechanismOperation(), 
			//	currency, settlementAmount, objSettlementOperationTO.getOperationPart(), objBcrdCashAccount, loggerUser.getUserName(), ComponentConstant.ZERO, null);
		//now we create the referential funds operation to participant cash account
		FundsOperation objFundsOperation= createFundsOperationGrossSettlement(idParticipant, objSettlementOperationTO.getIdMechanismOperation(), 
				currency, settlementAmount, objSettlementOperationTO.getOperationPart(), objInstitutionCashAccount, loggerUser.getUserName(), ComponentConstant.ZERO, null);
		
		// component is not ready
		// swiftWithdrawlProcess.executeSwiftWithdrawl(objFundsOperation, null, null);
	}
	
	/**
	 * Creates the funds operation gross settlement.
	 *
	 * @param idParticipant the id participant
	 * @param idMechanismOperation the id mechanism operation
	 * @param currency the currency
	 * @param amount the amount
	 * @param operationPart the operation part
	 * @param objInstitutionCashAccount the obj institution cash account
	 * @param userName the user name
	 * @param indCentralized the ind centralized
	 * @param objRefFundsOperation the obj ref funds operation
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	public FundsOperation createFundsOperationGrossSettlement(Long idParticipant, Long idMechanismOperation, Integer currency, BigDecimal amount, 
			Integer operationPart, InstitutionCashAccount objInstitutionCashAccount, 
			String userName, Integer indCentralized, FundsOperation objRefFundsOperation) throws ServiceException
	{
		FundsOperation objFundsOperation= new FundsOperation();	
		objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
		objFundsOperation.setParticipant(new Participant(idParticipant));
		objFundsOperation.setCurrency(currency);
		objFundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		objFundsOperation.setFundsOperationGroup(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		objFundsOperation.setFundsOperationType(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT.getCode());
		objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
		objFundsOperation.setIndCentralized(indCentralized);
		objFundsOperation.setMechanismOperation(new MechanismOperation(idMechanismOperation));
		objFundsOperation.setOperationAmount(amount);
		objFundsOperation.setOperationPart(operationPart);
		objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		objFundsOperation.setRegistryDate(new Date());
		objFundsOperation.setRegistryUser(userName);
		objFundsOperation.setFundsOperation(objRefFundsOperation);
		objFundsOperation.setIndBankRetirement(BooleanType.YES.getCode());

		Map<String,Object> paramat= new HashMap<String,Object>();		
		paramat.put("idMechanismOperationPkParam", idMechanismOperation);
		MechanismOperation mo = findObjectByNamedQuery(MechanismOperation.PAYMENT_REFERENCE, paramat);
		objFundsOperation.setPaymentReference(mo.getPaymentReference());
		
		 create(objFundsOperation);
		 
		//we execute the funds component
		FundsComponentTO objFundsComponentTO= new FundsComponentTO();
		objFundsComponentTO.setCashAmount(objFundsOperation.getOperationAmount());
		objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
		objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());
		objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
		fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
		
		return objFundsOperation;
	}
	
	
	/**
	 * Creates the funds operation net settlement.
	 *
	 * @param objParticipantPosition the obj participant position
	 * @param objInstitutionCashAccount the obj institution cash account
	 * @param loggerUser the logger user
	 * @param indCentralized the ind centralized
	 * @param objRefFundsOperation the obj ref funds operation
	 * @param paymentReference the payment reference
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	public FundsOperation createFundsOperationNetSettlement(ParticipantPosition objParticipantPosition, InstitutionCashAccount objInstitutionCashAccount, 
															LoggerUser loggerUser, Integer indCentralized, FundsOperation objRefFundsOperation, String paymentReference) throws ServiceException
	{
		FundsOperation objFundsOperation= new FundsOperation();	
		objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
		objFundsOperation.setParticipant(objParticipantPosition.getParticipant());
		objFundsOperation.setCurrency(objInstitutionCashAccount.getCurrency());
		objFundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		objFundsOperation.setFundsOperationGroup(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		objFundsOperation.setFundsOperationType(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT.getCode());
		objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
		objFundsOperation.setIndCentralized(indCentralized);
		objFundsOperation.setSettlementProcess(objParticipantPosition.getSettlementProcess());
		objFundsOperation.setOperationAmount(objParticipantPosition.getNetPosition());
		objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		objFundsOperation.setRegistryDate(new Date());
		objFundsOperation.setRegistryUser(loggerUser.getUserName());
		objFundsOperation.setFundsOperation(objRefFundsOperation);
		objFundsOperation.setPaymentReference(paymentReference);
		create(objFundsOperation, loggerUser);
		
		//we execute the funds component
		FundsComponentTO objFundsComponentTO= new FundsComponentTO();
		objFundsComponentTO.setCashAmount(objFundsOperation.getOperationAmount());
		objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
		objFundsComponentTO.setIdBusinessProcess(BusinessProcessType.NET_OPERATIONS_SETTLE_CASH_PART.getCode());
		objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
		fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
		
		return objFundsOperation;
	}
	
	/**
	 * Send funds participant positions tx.
	 *
	 * @param objParticipantPosition the obj participant position
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public void sendFundsParticipantPositionsTx(ParticipantPosition objParticipantPosition, Long idMechanism, Long idModalityGroup, 
			Integer currency, LoggerUser loggerUser)  throws ServiceException {
		try {
			sendFundsCreditorParticipant(objParticipantPosition, idMechanism, idModalityGroup, currency, loggerUser);
		} catch (Exception e) {
			throw e;
		}		
	}
	
	/**
	 * Send funds creditor participant.
	 *
	 * @param objParticipantPosition the obj participant position
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param currency the currency
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendFundsCreditorParticipant(ParticipantPosition objParticipantPosition, Long idMechanism, Long idModalityGroup, 
																	Integer currency, LoggerUser loggerUser) throws ServiceException
	{
		logger.info("::::::::: Start metod sendFundsCreditorParticipant :::::::::");
		SettlementProcess objSettlementProcess= objParticipantPosition.getSettlementProcess();
		try {
			InstitutionCashAccount objInstitutionCashAccount = getInstitutionCashAccount(idMechanism, idModalityGroup, 
																							objParticipantPosition.getParticipant().getIdParticipantPk(),
																							currency,
																							objSettlementProcess.getSettlementSchema());
			if (Validations.validateIsNull(objInstitutionCashAccount)) {
				throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage());
			}
			InstitutionCashAccount objBcrdCashAccount = objInstitutionCashAccount.getInstitutionCashAccount(); //cash account related to BCB
			if (Validations.validateIsNull(objBcrdCashAccount)) {
				throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage());
			}
			
			//we create payment reference
			StringBuilder paymentReference = new StringBuilder();
			paymentReference.append(idMechanism).append(idModalityGroup);
			
			//we create the withdrawal funds operation from BCB
			//salida desde la cuenta acreedora en el BC
			FundsOperation objRefFundsOperation= createFundsOperationNetSettlement(objParticipantPosition, objBcrdCashAccount, 
																	loggerUser, ComponentConstant.ONE, null,null);
			//now we create the referential funds operation to participant cash account
			//salida desde la cuenta acreedora efectivo
			FundsOperation objFundsOperation= createFundsOperationNetSettlement(objParticipantPosition, objInstitutionCashAccount, 
																	loggerUser, ComponentConstant.ZERO, objRefFundsOperation,paymentReference.toString());
			
			// if the idepositary lip parameter is active
			if(idepositarySetup.getIndFundsThridPart().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				settlementServiceConsumer.sendFundsLipWebClient(objFundsOperation, loggerUser);
			}
			
			//we update the sent funds indicator
			objParticipantPosition.setIndSentFunds(BooleanType.YES.getCode());
			objParticipantPosition.setIndAutomaticSent(BooleanType.YES.getCode());
			update(objParticipantPosition);
		} catch (ServiceException e) {
			logger.error("idSettlementProcess: " +objSettlementProcess.getIdSettlementProcessPk()
								+", idMechanism: " +idMechanism
								+", idModalityGroup: " +idModalityGroup
								+", currency: " +currency
								+", idParticipant: " +objParticipantPosition.getParticipant().getIdParticipantPk());
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * Generate funds operation.
	 *
	 * @param objGuaranteeOperation the obj guarantee operation
	 * @param idFundsOperationType the id funds operation type
	 * @param fundsOperationClass the funds operation class
	 * @param idInstitutionCashAccount the id institution cash account
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	public FundsOperation generateFundsOperation(GuaranteeOperation objGuaranteeOperation,Long idFundsOperationType, Integer fundsOperationClass,
			Long idInstitutionCashAccount) throws ServiceException {
		FundsOperation objFundsOperation = new FundsOperation();
		// we get the cash account from participant

		InstitutionCashAccount objInstitutionCashAccount = find(InstitutionCashAccount.class, idInstitutionCashAccount);
		// InstitutionCashAccount objInstitutionCashAccount =
		// guaranteesSettlement.getInstitutionCashAccount(objGuaranteeOperation.getParticipant().getIdParticipantPk(),
		// objGuaranteeOperation.getCurrency());

		if (Validations.validateIsNull(objInstitutionCashAccount)) {
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS,
					ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage());
		}

		objFundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
		objFundsOperation.setCurrency(objGuaranteeOperation.getCurrency());
		objFundsOperation.setFundsOperationClass(fundsOperationClass);
		objFundsOperation.setFundsOperationGroup(FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode());
		objFundsOperation.setFundsOperationType(idFundsOperationType);
		objFundsOperation.setGuaranteeOperation(objGuaranteeOperation);
		objFundsOperation.setIndAutomatic(ComponentConstant.ONE);
		objFundsOperation.setIndCentralized(ComponentConstant.ZERO);
		objFundsOperation.setOperationAmount(objGuaranteeOperation.getGuaranteeBalance());
		objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		objFundsOperation.setParticipant(objGuaranteeOperation.getParticipant());
		objFundsOperation.setRegistryUser(objGuaranteeOperation.getLastModifyUser());
		objFundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		this.create(objFundsOperation);
		return objFundsOperation;
	}
	
	/**
	 * Update funds participant settlement.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param role the role
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsParticipantSettlement(Long idSettlementOperation, Integer role, Long idFundsReference, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE ParticipantSettlement SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		stringBuffer.append(" and role = :role ");
		stringBuffer.append(" and operationState = :state ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementOperation", idSettlementOperation);
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("role", role);
		query.setParameter("state", ParticipantOperationStateType.CONFIRM.getCode());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update funds participant settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param operationSettlementState the operation settlement state
	 * @param role the role
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsParticipantSettlement(Long idSettlementProcess, Integer operationSettlementState, Integer role, Long idFundsReference, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE ParticipantSettlement SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idParticipantSettlementPk in ( ");
		stringBuffer.append("   SELECT PS.idParticipantSettlementPk "); 
		stringBuffer.append("   FROM 	ParticipantSettlement PS , OperationSettlement OS ");
		stringBuffer.append("   WHERE OS.settlementOperation.idSettlementOperationPk = PS.settlementOperation.idSettlementOperationPk ");	
		stringBuffer.append("   and OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
		stringBuffer.append("   and PS.settlementOperation.settlementType in (:dvpType) ");
		stringBuffer.append("   and OS.operationState = :operationSettlementState ");
		stringBuffer.append("   and PS.role = :role ");
		stringBuffer.append("   and PS.operationState = :state ");
		stringBuffer.append(" )");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("operationSettlementState", operationSettlementState);
		query.setParameter("dvpType", SettlementType.DVP.getCode());
		query.setParameter("idSettlementProcess", idSettlementProcess);
		query.setParameter("role", role);
		query.setParameter("state", ParticipantOperationStateType.CONFIRM.getCode());
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update funds participant settlement.
	 *
	 * @param idParticipantSettlement the id participant settlement
	 * @param idFundsReference the id funds reference
	 * @param loggerUser the logger user
	 */
	public void updateFundsParticipantSettlement(Long idParticipantSettlement, Long idFundsReference, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE ParticipantSettlement SET fundsReference = :idFundsReference, ");
		stringBuffer.append(" lastModifyApp = :lastModifyApp, ");
		stringBuffer.append(" lastModifyDate = :lastModifyDate, ");
		stringBuffer.append(" lastModifyIp = :lastModifyIp, ");
		stringBuffer.append(" lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idParticipantSettlementPk = :idParticipantSettlement ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idParticipantSettlement", idParticipantSettlement);
		query.setParameter("idFundsReference", idFundsReference);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}

}
