package com.pradera.settlements.core.service;

import static com.pradera.model.settlement.type.OperationSettlementSateType.SETTLED;
import static com.pradera.model.settlement.type.OperationSettlementSateType.SETTLEMENT_PENDIENT;
import static com.pradera.model.settlement.type.OperationSettlementSateType.WITHDRAWN_BY_FUNDS;
import static com.pradera.model.settlement.type.OperationSettlementSateType.WITHDRAWN_BY_GUARANTEES;
import static com.pradera.model.settlement.type.OperationSettlementSateType.WITHDRAWN_BY_SECURITIES;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsProcessesType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.RequestStateType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.type.ProcessLoggerStateType;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.ReassignmentRequestStateType;
import com.pradera.model.settlement.type.SettlementCurrencyRequestStateType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.settlements.core.to.NetParticipantPositionTO;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;
import com.pradera.settlements.core.to.NetSettlementOperationTO;
import com.pradera.settlements.core.view.ModalitySettlementTO;
import com.pradera.settlements.core.view.MonitorSettlementTO;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementMonitoringService.
 *
 * @author Pradera Technologies
 */
@Stateless
public class SettlementMonitoringService extends CrudDaoServiceBean {

	/** The settlement process service bean. */
	@EJB
	private SettlementProcessService settlementProcessServiceBean;
	
	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The collection process service bean. */
	@EJB
	private CollectionProcessService collectionProcessServiceBean;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The settlements component singleton. */
	@EJB
	SettlementsComponentSingleton settlementsComponentSingleton;
	
	/**
	 * Gets the modality settlement service.
	 *
	 * @param monitorSettlement the monitor settlement
	 * @return the modality settlement service
	 * @throws ServiceException the service exception
	 */
	public List<ModalitySettlementTO> getModalitySettlementService(MonitorSettlementTO monitorSettlement) throws ServiceException{
		
		//List<Object[]> objects = getModalityGroupOperations(monitorSettlement);
		List<Object[]> objects = getModalityGroupSettlementOperations(monitorSettlement);
		List<ModalitySettlementTO> modSettlementList = new ArrayList<ModalitySettlementTO>();
		
		for(Object[] object : objects){
			ModalitySettlementTO modSettlement = new ModalitySettlementTO();
			modSettlement.setModalityGroupId((Long)object[0]);
			modSettlement.setModalityGroupName((String)object[1]);
			modSettlement.setSettlementSchemeId((Integer)object[2]);
			modSettlement.setSettlementSchemeName(SettlementSchemaType.get((Integer)object[2]).getValue());
			modSettlement.setMechanismId((Long)object[3]);
			modSettlement.setMechanismName((String)object[4]);
			modSettlement.setNetProcess(SettlementSchemaType.NET.getCode().equals(modSettlement.getSettlementSchemeId())?true:false);
			
			Integer idCurrency = monitorSettlement.getCurrencyPk();
			Date settlementDate = monitorSettlement.getSettlementDate();
			Long idParticipant = monitorSettlement.getIdParticipantPk();
			
			Long idModalityGroup = (Long)object[0];
			Integer idSettlementSchema = (Integer)object[2];
			Long idMechanism = (Long)object[3];
			
			List<SettlementOperation> pendingOperations = null;
			pendingOperations = setForcedPurchase(getDateOperationSettlementProcesses(idMechanism,idModalityGroup, null,idCurrency, idParticipant, settlementDate, idSettlementSchema, true , false, false, false));
			modSettlement.setLstPendingOperations(pendingOperations);
			modSettlement.setPendingOperations(pendingOperations.size());
			
			//get operations settled
			List<SettlementOperation> settledOperations = null;
			settledOperations = setForcedPurchase(getDateOperationSettlementProcesses(idMechanism,idModalityGroup, null,idCurrency, idParticipant, settlementDate, idSettlementSchema, false, true ,false , false));
			modSettlement.setLstSettledOperations(settledOperations);
			modSettlement.setSettledOperations(settledOperations.size());
			
			//get operations canceled
			List<SettlementOperation> cancelledOperations = null;
			cancelledOperations = setForcedPurchase(getDateOperationSettlementProcesses(idMechanism,idModalityGroup, null,idCurrency, idParticipant, settlementDate, idSettlementSchema, false, false, true , false));
			modSettlement.setLstCancelledOperations(cancelledOperations);
			modSettlement.setCancelledOperations(cancelledOperations.size());
			
			//get operations removed by IND_REMOVED
			List<SettlementOperation> removedOperations = null;
			removedOperations = setForcedPurchase(getDateOperationSettlementProcesses(idMechanism,idModalityGroup, null,idCurrency, idParticipant, settlementDate, idSettlementSchema, false,false, false , true));
			modSettlement.setLstRemovedOperations(removedOperations);
			modSettlement.setRemovedOperations(removedOperations.size());

//			//get total operations
			List<SettlementOperation> operationTotal = new ArrayList<SettlementOperation>();
			operationTotal.addAll(cancelledOperations);
			operationTotal.addAll(pendingOperations);
			operationTotal.addAll(settledOperations);
			operationTotal.addAll(removedOperations);
			modSettlement.setTotalOperations(operationTotal.size());
			
			if(modSettlement.getTotalOperations() > 0){
				modSettlement.setLstTotalOperations(operationTotal);
				modSettlementList.add(modSettlement);
			}
		}
		return modSettlementList;
	}
	
	public MechanismOperation getMcnWithParticipantsAndSecurity(Long idMechanismOperationPk) throws ServiceException {
		StringBuilder sd = new StringBuilder();
		sd.append(" select mo");
		sd.append(" from MechanismOperation mo");
		sd.append(" inner join fetch mo.buyerParticipant pbuy");
		sd.append(" inner join fetch mo.sellerParticipant psell");
		sd.append(" inner join fetch mo.securities s");
		sd.append(" where mo.idMechanismOperationPk = :idMechanismOperationPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperationPk", idMechanismOperationPk);
		List<MechanismOperation> lst = findListByQueryString(sd.toString(), parameters);
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			return lst.get(0);
		}else{
			return null;
		}
	}
	
	public List<Participant> getLstParticipant() throws ServiceException {
		StringBuilder sd = new StringBuilder();
		sd.append(" select p");
		sd.append(" from Participant p");
		Map<String, Object> parameters = new HashMap<String, Object>();
		return findListByQueryString(sd.toString(), parameters);
	}
	
	/**
	 * verifica si el proceso de identificacion de compra forzosa ya termino
	 * @return
	 * @throws ServiceException
	 */
	public boolean finishProcessForcedPurchaseIdentification()throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" select pl");
		sd.append(" from ProcessLogger pl");
		sd.append(" inner join pl.businessProcess bp");
		sd.append(" where 0=0");
		sd.append(" and trunc(pl.processDate) = :date");
		sd.append(" and bp.idBusinessProcessPk = :idBusinessProcessPk");
		sd.append(" order by pl.idProcessLoggerPk desc");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("date", CommonsUtilities.currentDate());
	    parameters.put("idBusinessProcessPk", BusinessProcessType.IDENTIFY_FORCED_PURCHASE.getCode());
	    
	    List<ProcessLogger> lst = findListByQueryString(sd.toString(),parameters);
	    if(Validations.validateListIsNotNullAndNotEmpty(lst)){
	    	ProcessLogger pl = lst.get(0);
	    	if( Validations.validateIsNotNull(pl)
	    		&& Validations.validateIsNotNullAndPositive(pl.getLoggerState())
	    		&& pl.getLoggerState().equals(ProcessLoggerStateType.FINISHED.getCode())){
	    		return true;
	    	}else{
	    		return false;
	    	}
	    }else{
	    	return false;
	    }
	}
	
	
	/**
	 * Gets the date operation settlement processes.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModalityGroup the id modality group
	 * @param idModality the id modality
	 * @param idCurrency the id currency
	 * @param idParticipant the id participant
	 * @param settlementDate the settlement date
	 * @param idSettlementSchema the id settlement schema
	 * @param pending the pending
	 * @param settled the settled
	 * @param cancelled the cancelled
	 * @param removed the removed
	 * @return the date operation settlement processes
	 */
	@SuppressWarnings("unchecked")
	private List<SettlementOperation> getDateOperationSettlementProcesses(
			Long idMechanism, Long idModalityGroup, Long idModality, Integer idCurrency, Long idParticipant, Date settlementDate, Integer idSettlementSchema,
			boolean pending, boolean settled, boolean cancelled, boolean removed){
		
		StringBuffer stringBuilderSql = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		stringBuilderSql.append(" 	select distinct new SettlementOperation ( " );
		stringBuilderSql.append("		MO.idMechanismOperationPk, ");
		stringBuilderSql.append("		mm.negotiationMechanism.idNegotiationMechanismPk, ");
		stringBuilderSql.append("		mm.negotiationMechanism.mechanismName, ");
		stringBuilderSql.append("		mm.negotiationModality.idNegotiationModalityPk, ");
		stringBuilderSql.append("		mm.negotiationModality.modalityName, ");
		stringBuilderSql.append("		MO.operationDate, ");
		stringBuilderSql.append("		MO.operationNumber, ");
		stringBuilderSql.append("		MO.ballotNumber, ");
		stringBuilderSql.append("		MO.sequential, ");
		stringBuilderSql.append("		MO.buyerParticipant.idParticipantPk, ");
		stringBuilderSql.append("		MO.buyerParticipant.mnemonic, ");
		stringBuilderSql.append("		MO.sellerParticipant.idParticipantPk, ");
		stringBuilderSql.append("		MO.sellerParticipant.mnemonic, ");
		stringBuilderSql.append("		MO.securities.idSecurityCodePk, ");
		stringBuilderSql.append("		MO.operationState, ");
		stringBuilderSql.append("		SO.operationState, ");
		stringBuilderSql.append("		SO.stockQuantity, ");
		stringBuilderSql.append("		SO.settlementAmount, ");
		stringBuilderSql.append("		SO.settlementPrice, ");
		stringBuilderSql.append("		SO.amountRate, ");
		stringBuilderSql.append("		SO.settlementDate, ");
		stringBuilderSql.append("		SO.operationPart, ");
		stringBuilderSql.append("	    SO.removeSettlementMotive, ");
		stringBuilderSql.append("		SO.indRemoveSettlement, ");
		stringBuilderSql.append("		SO.indReenterSettlement,  ");
		stringBuilderSql.append("		(Select pt.parameterName from ParameterTable pt where pt.parameterTablePk = SO.operationState ), ");
		stringBuilderSql.append("		SO.indSentInterface , ");
		stringBuilderSql.append("		(select count(sos) from SettlementOperation sos where sos.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk and sos.indForcedPurchase = :indForcedPurchase),  ");
		stringBuilderSql.append("		(Select pt.parameterName from ParameterTable pt where pt.parameterTablePk = SO.operationState )) ");
	    stringBuilderSql.append(" 	from SettlementOperation SO, MechanismOperation MO , MechanismModality mm , ModalityGroupDetail MGD ");
	    stringBuilderSql.append(" 	where MO.mechanisnModality.id.idNegotiationMechanismPk = mm.id.idNegotiationMechanismPk ");
	    stringBuilderSql.append(" 	and   MO.mechanisnModality.id.idNegotiationModalityPk = mm.id.idNegotiationModalityPk ");
	    stringBuilderSql.append(" 	and   MGD.mechanismModality.id.idNegotiationMechanismPk = mm.id.idNegotiationMechanismPk ");
	    stringBuilderSql.append(" 	and   MGD.mechanismModality.id.idNegotiationModalityPk = mm.id.idNegotiationModalityPk ");
	    //stringBuilderSql.append("	and PO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
	    stringBuilderSql.append("	and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
	    stringBuilderSql.append(" 	and mm.id.idNegotiationMechanismPk = :idMechanism ");
	    stringBuilderSql.append(" 	and SO.settlementSchema = :idSettlementSchema ");
	    stringBuilderSql.append(" 	and MGD.modalityGroup.settlementSchema = :idSettlementSchema ");
	    stringBuilderSql.append(" 	and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
	    
	    List<Integer> cashStates = new ArrayList<Integer>();
	    List<Integer> termStates = new ArrayList<Integer>();
	    
	    parameters.put("indForcedPurchase", BooleanType.YES.getCode());
	    if(idParticipant!=null){
	    	stringBuilderSql.append(" and ( mo.sellerParticipant.idParticipantPk = :idParticipant ");
			stringBuilderSql.append("       or mo.buyerParticipant.idParticipantPk = :idParticipant )");
	    	parameters.put("idParticipant", idParticipant);
	    }
	    
	    if(idModality!=null){
	    	stringBuilderSql.append(" 	and mm.id.idNegotiationModalityPk = :idModality ");
	    	parameters.put("idModality", idModality);
	    }
	    stringBuilderSql.append("	and SO.settlementCurrency = :idCurrency ");
		stringBuilderSql.append("	and SO.settlementDate = :settlementDate ");
		
		stringBuilderSql.append("	and ( ( SO.operationPart = :cashPart and SO.operationState in (:cashStates) ) ");
		stringBuilderSql.append("	   or ( SO.operationPart = :termPart and SO.operationState in (:termStates) ) ) ");
		
		if(pending || removed){
			
			cashStates = new ArrayList<Integer>();
			cashStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		    cashStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		    
		    termStates = new ArrayList<Integer>();
		    termStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		    
		}
		
		if(settled){
			cashStates = new ArrayList<Integer>();
			cashStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
			
			termStates = new ArrayList<Integer>();
		    termStates.add(MechanismOperationStateType.TERM_SETTLED.getCode());
		}
		
		if(cancelled){
			cashStates = new ArrayList<Integer>();
			cashStates.add(MechanismOperationStateType.CANCELED_STATE.getCode());
			
			termStates = new ArrayList<Integer>();
		    termStates.add(MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
		}
		
		parameters.put("cashStates", cashStates);
		parameters.put("termStates", termStates);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
	    parameters.put("termPart", ComponentConstant.TERM_PART);
	    
		
		if(removed){
			stringBuilderSql.append(" 	and SO.indRemoveSettlement = :indicatorOne and SO.indReenterSettlement != :indicatorOne");
		}else{
			stringBuilderSql.append("   and   ( ( SO.indRemoveSettlement != :indicatorOne and SO.indReenterSettlement = :indicatorOne) ");
			stringBuilderSql.append("        or ( SO.indRemoveSettlement != :indicatorOne and SO.indReenterSettlement != :indicatorOne) ) ");
		}
    
	    stringBuilderSql.append(" 	and ( ( MO.indMarginGuarantee = :indicatorOne and MO.marginReference = :complianceMargin ) ");
	    stringBuilderSql.append(" 	  or  ( MO.indMarginGuarantee != :indicatorOne and MO.marginReference is null ) ");
	    stringBuilderSql.append(" 	  or  ( MO.indMarginGuarantee = :indicatorOne and MO.marginReference is null ) ) ");
	    
	    parameters.put("complianceMargin", AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
	    parameters.put("indicatorOne", ComponentConstant.ONE);
	    parameters.put("idSettlementSchema",idSettlementSchema);
	    parameters.put("idMechanism", idMechanism);
	    parameters.put("idCurrency", idCurrency);
	    parameters.put("settlementDate", settlementDate);
	    parameters.put("idModalityGroup", idModalityGroup);
	    
	    return (List<SettlementOperation>) findListByQueryString(stringBuilderSql.toString(),parameters);
	}
	
	/**
	 * Gets the date operation settlement.
	 *
	 * @param idSettlementProcess the id settlement process
	 * @param states the states
	 * @return the date operation settlement
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementOperation> getDateOperationSettlement(Long idSettlementProcess , List<Integer> states, Long participantCode){
		
		StringBuffer stringBuilderSql = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		stringBuilderSql.append(" 	select distinct new SettlementOperation (  " );
		stringBuilderSql.append("		MO.idMechanismOperationPk, ");
		stringBuilderSql.append("		MO.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk, ");
		stringBuilderSql.append("		MO.mechanisnModality.negotiationMechanism.mechanismName, ");
		stringBuilderSql.append("		MO.mechanisnModality.negotiationModality.idNegotiationModalityPk, ");
		stringBuilderSql.append("		MO.mechanisnModality.negotiationModality.modalityName, ");
		stringBuilderSql.append("		MO.operationDate, ");
		stringBuilderSql.append("		MO.operationNumber, ");
		stringBuilderSql.append("		MO.ballotNumber, ");
		stringBuilderSql.append("		MO.sequential, ");
		stringBuilderSql.append("		MO.buyerParticipant.idParticipantPk, ");
		stringBuilderSql.append("		MO.buyerParticipant.mnemonic, ");
		stringBuilderSql.append("		MO.sellerParticipant.idParticipantPk, ");
		stringBuilderSql.append("		MO.sellerParticipant.mnemonic, ");
		stringBuilderSql.append("		MO.securities.idSecurityCodePk, ");
		stringBuilderSql.append("		MO.operationState, ");
		stringBuilderSql.append("		SO.operationState, ");
		stringBuilderSql.append("		SO.stockQuantity, ");
		stringBuilderSql.append("		SO.settlementAmount, ");
		stringBuilderSql.append("		SO.settlementPrice, ");
		stringBuilderSql.append("		SO.amountRate, ");
		stringBuilderSql.append("		SO.settlementDate, ");
		stringBuilderSql.append("		SO.operationPart, ");
		stringBuilderSql.append("	    SO.removeSettlementMotive, ");
		stringBuilderSql.append("		SO.indRemoveSettlement, ");
		stringBuilderSql.append("		SO.indReenterSettlement , ");
		stringBuilderSql.append("		(Select pt.parameterName from ParameterTable pt where pt.parameterTablePk = SO.operationState ), ");
		stringBuilderSql.append("		SO.indSentInterface , ");
		stringBuilderSql.append("		(select count(sos) from SettlementOperation sos where sos.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk and sos.indForcedPurchase = :indForcedPurchase),  ");
		stringBuilderSql.append("		(Select pt.parameterName from ParameterTable pt where pt.parameterTablePk = OS.operationState )) ");
	    stringBuilderSql.append(" 	from OperationSettlement OS inner join OS.settlementOperation SO inner join SO.mechanismOperation MO ");
	    stringBuilderSql.append(" 	where OS.operationState in (:states) ");
	    stringBuilderSql.append(" 	and   OS.settlementProcess.idSettlementProcessPk = :idSettlementProcess ");
	    
	    if(participantCode!=null){
	    	stringBuilderSql.append(" 	and (MO.sellerParticipant.idParticipantPk = :partCode	Or 										 ");
	    	stringBuilderSql.append(" 		 MO.buyerParticipant.idParticipantPk  = :partCode											)");
	    	parameters.put("partCode", participantCode);
	    }
	    
	    parameters.put("indForcedPurchase", BooleanType.YES.getCode());
	    parameters.put("states", states);
	    parameters.put("idSettlementProcess", idSettlementProcess);
	    
	    return (List<SettlementOperation>) findListByQueryString(stringBuilderSql.toString(),parameters);
	}
	
	/**
	 * Restart settlement process.
	 *
	 * @param settlementProcParam the settlement proc param
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess restartSettlementProcess(SettlementProcess settlementProcParam) throws ServiceException{
		SettlementProcess settlementProcess = em.find(SettlementProcess.class, settlementProcParam.getIdSettlementProcessPk());
		
		if(settlementProcess.getProcessState().equals(SettlementProcessStateType.WAITING.getCode())){
			return settlementProcess;
		}else if(settlementProcess.getProcessState().equals(SettlementProcessStateType.STOPPED.getCode())){
			settlementProcess.setProcessState(SettlementProcessStateType.WAITING.getCode());
			update(settlementProcess);
			return settlementProcess;
		}else{	
			throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_STATE_INCORRECT);
		}
	}
	
	/**
	 * Stop settlement process.
	 *
	 * @param settlementProcParam the settlement proc param
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess stopSettlementProcess(SettlementProcess settlementProcParam) throws ServiceException{
		SettlementProcess settlementProcess = em.find(SettlementProcess.class, settlementProcParam.getIdSettlementProcessPk());
		if(settlementProcess.getProcessState().equals(SettlementProcessStateType.WAITING.getCode())){
			settlementProcess.setProcessState(SettlementProcessStateType.STOPPED.getCode());
			update(settlementProcess);
			return settlementProcess;
		}else{
			throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_STATE_INCORRECT);
		}
	}
	
	/**
	 * Withdrawn Funds Automatic.
	 * 
	 * @param netSettlementAttributesTO
	 * @param settlementProcessNetSession
	 * @throws ServiceException
	 */
	public void withdrawnFundsAutomatic(NetSettlementAttributesTO netSettlementAttributesTO, SettlementProcess settlementProcessNetSession) throws ServiceException{
		
		Map<Long, NetParticipantPositionTO> mapParticipantPositions = NegotiationUtils.populateParticipantPositions(netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
		netSettlementAttributesTO.setLstParticipantPosition(null);
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		
		//  Calculate net position, con retiro automatico
//		List<ParticipantPosition> newParticipantPositions = monitorProcessFacade.endEditAndRecalculateNetPositions(netSettlementAttributesTO, true);
		List<ParticipantPosition> newParticipantPositions =  settlementsComponentSingleton.recalculateParticipantPositions(netSettlementAttributesTO, true);
		settlementProcessNetSession.setParticipantPositions(newParticipantPositions);
		netSettlementAttributesTO.setParticipantPositionToEdit(null);	
				
		if(Validations.validateListIsNotNullAndNotEmpty(netSettlementAttributesTO.getLstPendingSettlementOperations())){
			int countWithdrawn = 0;
			int countPending = netSettlementAttributesTO.getLstPendingSettlementOperations().size();
			for(NetSettlementOperationTO objNetSettlementOperationTO : netSettlementAttributesTO.getLstPendingSettlementOperations() ){				
				if(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode().equals(objNetSettlementOperationTO.getOperationSettlement().getOperationState())){
					countWithdrawn ++;
				}				
			}
			countPending = countPending - countWithdrawn;				
			netSettlementAttributesTO.getSettlementProcess().setPendingOperations(new Long(countPending));
		}		
		
		for(ParticipantPosition objParticipantPosition : settlementProcessNetSession.getParticipantPositions()){
			BigDecimal availableCashAmount = BigDecimal.ZERO , missingAmount = BigDecimal.ZERO;
			NetParticipantPositionTO objNetParticipantPositionTO = netSettlementAttributesTO.getMapParticipantPosition().get(objParticipantPosition.getParticipant().getIdParticipantPk());
			objNetParticipantPositionTO.setNetPosition(objParticipantPosition.getNetPosition());
			InstitutionCashAccount institutionCashAccount = collectionProcessServiceBean.getInstitutionCashAccount(
					settlementProcessNetSession.getNegotiationMechanism().getIdNegotiationMechanismPk(),settlementProcessNetSession.getModalityGroup().getIdModalityGroupPk()
					,objParticipantPosition.getParticipant().getIdParticipantPk(),settlementProcessNetSession.getCurrency(),SettlementSchemaType.NET.getCode());
			if(institutionCashAccount!=null){
				availableCashAmount = institutionCashAccount.getAvailableAmount();
			}
			objNetParticipantPositionTO.setAvailableCashAmount(availableCashAmount);	
			objNetParticipantPositionTO.setMissingAmount(BigDecimal.ZERO);
			missingAmount = objNetParticipantPositionTO.getNetPosition().add(objNetParticipantPositionTO.getAvailableCashAmount());
			if(missingAmount.compareTo(BigDecimal.ZERO) < 0){
				objNetParticipantPositionTO.setMissingAmount(missingAmount);
			}
		}
		
	}
	
	/**
	 * Validate begin net settlement.
	 *
	 * @param mechanismId the mechanism id
	 * @param settlementDate the settlement date
	 * @param modalityGroupId the modality group id
	 * @param schemaId the schema id
	 * @param currency the currency
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void validateExistingSettlementProcess(Long mechanismId, Date settlementDate , Long modalityGroupId, Integer schemaId, Integer currency) throws ServiceException{
		StringBuffer settlementQuery = new StringBuffer(); 
		settlementQuery.append("SELECT sett FROM SettlementProcess sett ");
		settlementQuery.append(" WHERE trunc(sett.settlementDate) = trunc(:dateParam) ");
		settlementQuery.append(" and sett.negotiationMechanism.idNegotiationMechanismPk = :mechanismIdParam ");
		settlementQuery.append(" and sett.settlementSchema = :schemeParam ");
		settlementQuery.append(" and sett.currency = :currencyParam  ");
		settlementQuery.append(" AND sett.modalityGroup.idModalityGroupPk = :modalityParam" );
		
		Query query = em.createQuery(settlementQuery.toString());
		query.setParameter("dateParam", settlementDate);
		query.setParameter("mechanismIdParam", mechanismId);
		query.setParameter("schemeParam", schemaId);
		query.setParameter("currencyParam", currency);
		query.setParameter("modalityParam", modalityGroupId);
		
		List<SettlementProcess> settlementList = null;
		try{
			settlementList =  query.getResultList();
		}catch(NoResultException ex){
		}
		if(settlementList!=null){
			for(SettlementProcess settlementProcess: settlementList){
				if(!settlementProcess.getProcessState().equals(SettlementProcessStateType.FINISHED.getCode())){
					throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_NOT_FINISH);
				}
			}
		}		
	}
	
	/**
	 * validate Settlemet State Request.
	 *
	 * @param settlementDate settlement date
	 * @param currency currency
	 * @throws ServiceException the Service Exception
	 */
	public void validateSettlemetStateRequest(Date settlementDate, Integer currency) throws ServiceException{
		List<String> msjAll = new ArrayList<String>();
		msjAll.addAll(validateReassignmentRequest(settlementDate));
		msjAll.addAll(validateChainedHolderOperation(settlementDate));
		msjAll.addAll(validateSettlementRequest(settlementDate));
		msjAll.addAll(validateFundsOperation(settlementDate,currency));
		String cadAll = "";
		if(Validations.validateListIsNotNullAndNotEmpty(msjAll)){	
			boolean one = true;
			for(String cadAux : msjAll){
				if(one){
					cadAll = cadAll.concat(cadAux);
					one = false;
				}else{
					cadAll = cadAll.concat(GeneralConstants.STR_COMMA_WITHOUT_SPACE).concat(cadAux);
				}				
			}			
		}
		if(Validations.validateIsNotEmpty(cadAll)){
			throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_NOT_VALID_REQUEST, new Object[]{cadAll});
		}
	}
	
	/**
	 * Validate Settlement Request.
	 *
	 * @param settlementDate settlement date
	 * @return Validate Settlemet Date Request
	 * @throws ServiceException the Service Exception
	 */
	public List validateSettlementRequest(Date settlementDate)throws ServiceException{
		List<String> msj = new ArrayList<String>();
		
		StringBuffer settlementQuery = new StringBuffer(); 
		settlementQuery.append("SELECT DISTINCT sett.requestType FROM SettlementDateOperation sdo ");
		settlementQuery.append(" ,SettlementRequest sett ,SettlementOperation so , MechanismOperation mo ");
		settlementQuery.append(" WHERE sdo.settlementRequest.idSettlementRequestPk = sett.idSettlementRequestPk ");
		settlementQuery.append(" and sdo.settlementOperation.idSettlementOperationPk = so.idSettlementOperationPk ");
		settlementQuery.append(" and so.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk ");
		settlementQuery.append(" and mo.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk ");
		settlementQuery.append(" and trunc(sett.registerDate) = trunc(:dateParam) ");
		settlementQuery.append(" and sett.requestState not in ( :state1 , :state2 , :state3 , :state4 , :state5 , :state6 ) ");
				
		Query query = em.createQuery(settlementQuery.toString());
		query.setParameter("dateParam", settlementDate);
		query.setParameter("state1", SettlementDateRequestStateType.ANNULATE.getCode());
		query.setParameter("state2", SettlementDateRequestStateType.REJECTED.getCode());
		query.setParameter("state3", SettlementDateRequestStateType.AUTHORIZE.getCode());	
		query.setParameter("state4", SettlementCurrencyRequestStateType.CANCELLED.getCode());
		query.setParameter("state5", SettlementCurrencyRequestStateType.REJECTED.getCode());
		query.setParameter("state6", SettlementCurrencyRequestStateType.AUTHORIZED.getCode());
		query.setParameter("idNegotiationMechanismPk", NegotiationMechanismType.BOLSA.getCode());
		
		Object obj = query.getResultList();
		if(obj!=null){
			List<Integer> settlementList = (List<Integer>)obj;
			for(Integer requestType : settlementList){
				if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode())){
					msj.add(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getDescription());
				}else if(requestType.equals(SettlementRequestType.SETTLEMENT_EXTENSION.getCode())){
					msj.add(SettlementRequestType.SETTLEMENT_EXTENSION.getDescription());
				}else if(requestType.equals(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode())){
					msj.add(SettlementRequestType.SETTLEMENT_ANTICIPATION.getDescription());
				}else if(requestType.equals(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getCode())){
					msj.add(SettlementRequestType.SETTLEMENT_TYPE_EXCHANGE.getDescription());
				}else if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode())){
					msj.add(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getDescription());
				}else if(requestType.equals(SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getCode())){
					msj.add(SettlementRequestType.SETTLEMENT_SPECIAL_TYPE_EXCHANGE.getDescription());
				}
			}
		}		
		
		return msj;
	}
	
	/**
	 * validate Reassignment Request.
	 *
	 * @param settlementDate settlement Date
	 * @return validate Reassignment Request
	 * @throws ServiceException the Service Exception
	 */
	public List validateReassignmentRequest(Date settlementDate)throws ServiceException{
		List<String> msj = new ArrayList<String>();
		
		StringBuffer settlementQuery = new StringBuffer(); 
		settlementQuery.append("SELECT count(sett) FROM ReassignmentRequest sett ");
		settlementQuery.append(" WHERE trunc(sett.registerDate) = trunc(:dateParam) ");
		settlementQuery.append(" and sett.requestState not in ( :state1 , :state3  )");
				
		Query query = em.createQuery(settlementQuery.toString());
		query.setParameter("dateParam", settlementDate);
		query.setParameter("state1", ReassignmentRequestStateType.REJECTED.getCode());
		query.setParameter("state3", ReassignmentRequestStateType.AUTHORIZED.getCode());

		Object obj = query.getSingleResult();
		if(obj!=null && Long.valueOf(obj.toString())>0){
			msj.add(SettlementConstant.MSG_REASSIGNMENT_REQUEST);
		}
		return msj;
	}
	
	/**
	 * validate Chained Holder Operation.
	 *
	 * @param settlementDate settlement Date
	 * @return validate Chained Holder Operation
	 * @throws ServiceException the Service Exception
	 */
	public List validateChainedHolderOperation(Date settlementDate)throws ServiceException{
		List<String> msj = new ArrayList<String>();
		
		StringBuffer settlementQuery = new StringBuffer(); 
		settlementQuery.append("SELECT count(sett) FROM ChainedHolderOperation sett ");
		settlementQuery.append(" WHERE trunc(sett.registerDate) = trunc(:dateParam) ");
		settlementQuery.append(" and sett.chaintState not in ( :state1 , :state2, :state3  )");
				
		Query query = em.createQuery(settlementQuery.toString());
		query.setParameter("dateParam", settlementDate);
		query.setParameter("state1", ChainedOperationStateType.CANCELLED.getCode());
		query.setParameter("state2", ChainedOperationStateType.REJECTED.getCode());
		query.setParameter("state3", ChainedOperationStateType.CONFIRMED.getCode());

		Object obj = query.getSingleResult();
		if(obj!=null && Long.valueOf(obj.toString())>0){
			msj.add(SettlementConstant.MSG_CHAINED_HOLDER_OPERATION);
		}
		
		return msj;
	}
	
	/**
	 * validate Funds Operation.
	 *
	 * @param settlementDate settlement Date
	 * @param currency currency
	 * @return validate Funds Operation
	 * @throws ServiceException the Service Exception
	 */
	public List validateFundsOperation(Date settlementDate,Integer currency)throws ServiceException{
		List<String> msj = new ArrayList<String>();
		
		StringBuffer settlementQuery = new StringBuffer(); 
		settlementQuery.append("SELECT DISTINCT sett.currency FROM FundsOperation sett ");
		settlementQuery.append(" WHERE trunc(sett.registryDate) = trunc(:dateParam) ");
		settlementQuery.append(" and sett.currency = :currency ");
		settlementQuery.append(" and sett.operationState not in ( :state1 , :state2 )");
		settlementQuery.append(" and sett.fundsOperationType = :fundsOperationType ");
				
		Query query = em.createQuery(settlementQuery.toString());
		query.setParameter("dateParam", settlementDate);
		query.setParameter("currency", currency);
		query.setParameter("state1", MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		query.setParameter("state2", MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode());	
		query.setParameter("fundsOperationType", ParameterFundsOperationType.SETTLEMENT_OPERATIONS_FUND_DEPOSITS.getCode());
		
		Object obj = query.getResultList();
		if(obj!=null){
			List<Integer> currencyList = (List<Integer>)obj;
			for(Integer currencyaux : currencyList){
				if(currencyaux.equals(CurrencyType.PYG.getCode())){
					msj.add(SettlementConstant.MSG_FUNDS_OPERATION_BOB);
				}else if(currencyaux.equals(CurrencyType.USD.getCode())){
					msj.add(SettlementConstant.MSG_FUNDS_OPERATION_USD);
				}else{
					msj.add(SettlementConstant.MSG_FUNDS_OPERATION);
				}
			}
		}
		return msj;
	}
	
	/**
	 * Validate close settlement.
	 *
	 * @param settlementProcess the settlement process
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long validateCloseSettlement(SettlementProcess settlementProcess)throws ServiceException{
		String settlementQuery = "SELECT sett FROM SettlementProcess sett " +
				"					WHERE trunc(sett.settlementDate) = trunc(:dateParam) AND " +
				"						  sett.negotiationMechanism.idNegotiationMechanismPk = :mechanismIdParam AND" +
				"						  sett.settlementSchema = :schemeParam AND" +
				"						  sett.currency = :currencyParam";
		if(settlementProcess.getModalityGroup().getIdModalityGroupPk()!=null){
			settlementQuery+="		AND	  sett.modalityGroup.idModalityGroupPk = :modalityParam";
		}
		
		Query query = em.createQuery(settlementQuery);
		query.setParameter("dateParam", settlementProcess.getSettlementDate());
		query.setParameter("mechanismIdParam", settlementProcess.getNegotiationMechanism().getIdNegotiationMechanismPk());
		query.setParameter("schemeParam", settlementProcess.getSettlementSchema());
		query.setParameter("currencyParam", settlementProcess.getCurrency());
		if(settlementProcess.getModalityGroup().getIdModalityGroupPk()!=null){
			query.setParameter("modalityParam", settlementProcess.getModalityGroup().getIdModalityGroupPk());
		}
		
		List<SettlementProcess> settlementList = null;
		try{
			settlementList =  query.getResultList();
		}catch(NoResultException ex){
			settlementList = new ArrayList<SettlementProcess>();
		}
		
		for(SettlementProcess _settlementProcess: settlementList){
			if(!_settlementProcess.getProcessState().equals(SettlementProcessStateType.FINISHED.getCode())){
				return _settlementProcess.getIdSettlementProcessPk();
			}
		}
		throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_NOT_AVAILABLE);
	}
	
	/**
	 * Gets the settlement process.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityGroupId the modality group id
	 * @param settlementDate the settlement date
	 * @param currency the currency
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementProcess> getSettlementProcess(Long mechanismId, Long modalityGroupId, Date settlementDate, Integer currency, Long partCode) throws ServiceException{
		StringBuffer stbQuery = new StringBuffer();
		stbQuery.append(" Select Distinct setPro From SettlementProcess setPro 									");
		stbQuery.append(" Left Outer Join setPro.participantPositions posit										");
		
		if(partCode!=null){
			stbQuery.append(" Left Outer Join Fetch setPro.operationSettlements operat							");
		}
		
		stbQuery.append(" WHERE setPro.negotiationMechanism.idNegotiationMechanismPk = :mechanismParam AND 		");
		stbQuery.append(" 		setPro.settlementDate = :settlementDateParam AND 								");
		stbQuery.append(" 		setPro.modalityGroup.idModalityGroupPk = :modalityGroupParam AND 				");
		stbQuery.append(" 		setPro.currency = :currencyParam 												");
		
		if(partCode!=null){
			stbQuery.append(" 	And posit.participant.idParticipantPk = :pCode									");
			stbQuery.append(" 	And (operat.settlementOperation.mechanismOperation.buyerParticipant = :pCode Or	");
			stbQuery.append(" 		 operat.settlementOperation.mechanismOperation.sellerParticipant = :pCode )	");
		}
		
		stbQuery.append(" order by setPro.idSettlementProcessPk desc 											");
		Query query = em.createQuery(stbQuery.toString());
		query.setParameter("mechanismParam", mechanismId);
		query.setParameter("settlementDateParam", settlementDate);
		query.setParameter("modalityGroupParam", modalityGroupId);
		query.setParameter("currencyParam", currency);
		
		if(partCode!=null){
			query.setParameter("pCode", partCode);
		}
		
		return query.getResultList();
	}
	
	/**
	 * Gets the participant positions.
	 *
	 * @param settlementProcessId the settlement process id
	 * @return the participant positions
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipantPosition> getParticipantPositions(Long settlementProcessId,Long partCode) throws ServiceException{
		StringBuffer stbQuery = new StringBuffer();
		stbQuery.append(" Select pp From ParticipantPosition pp 								");
		stbQuery.append(" inner join fetch pp.participant part  where 							");
		stbQuery.append(" pp.indProcess = :indicatorOne  										");
		stbQuery.append(" and pp.settlementProcess.idSettlementProcessPk  = :settlementProcessId ");
		
		if(partCode!=null){
			stbQuery.append(" and part.idParticipantPk = :partCode								");
		}
		
		stbQuery.append(" Order By part.mnemonic Asc ");
		Query query = em.createQuery(stbQuery.toString());
		query.setParameter("settlementProcessId", settlementProcessId);
		query.setParameter("indicatorOne", ComponentConstant.ONE);
		
		if(partCode!=null){
			query.setParameter("partCode", partCode);
		}
		
		return query.getResultList();
	}
	
	/**
	 * Sets the operations set process.
	 *
	 * @param setProcess the set process
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess setOperationsSetProcess(SettlementProcess setProcess) throws ServiceException{
		List<Integer> statePendient = Arrays.asList(SETTLEMENT_PENDIENT.getCode());
		List<Integer> stateSettlem = Arrays.asList(SETTLED.getCode());
		List<Integer> stateRemoved = Arrays.asList(WITHDRAWN_BY_FUNDS.getCode(),WITHDRAWN_BY_GUARANTEES.getCode(),WITHDRAWN_BY_SECURITIES.getCode());
		
		Long processId = setProcess.getIdSettlementProcessPk();
		Long participantCode = setProcess.getParticipantCode();
		
		List<SettlementOperation> pendientOperationList = getDateOperationSettlement(processId, statePendient,participantCode);
		List<SettlementOperation> settOperationList = getDateOperationSettlement(processId, stateSettlem,participantCode);
		List<SettlementOperation> removedOperationList = getDateOperationSettlement(processId, stateRemoved,participantCode);
		List<SettlementOperation> totalOperation = new ArrayList<SettlementOperation>();
		
		totalOperation.addAll(removedOperationList);
		totalOperation.addAll(settOperationList);
		totalOperation.addAll(pendientOperationList);
		
		setProcess.setTotalSettlementOperations(totalOperation);
		setProcess.setPendingSettlementOperations(pendientOperationList);
		setProcess.setRemovedSettlementOperations(removedOperationList);
		setProcess.setSettledSettlementOperations(settOperationList);
		
		return setProcess;
	}
	
	/**
	 * Set Forced Purchase.
	 *
	 * @param lstSettlementOperation List Settlement Operation
	 * @return List Settlement Operation
	 */
	public List<SettlementOperation> setForcedPurchase(List<SettlementOperation> lstSettlementOperation){
		List<SettlementOperation> lstSettlementOperationAux = new ArrayList<SettlementOperation>();
		for(SettlementOperation objSettlementOperation : lstSettlementOperation){
			if(Validations.validateIsNotNull(objSettlementOperation.getLngForcedPurchase())){
				if(Integer.valueOf(objSettlementOperation.getLngForcedPurchase().toString()).equals(BooleanType.NO.getCode())){
					objSettlementOperation.setForcedPurchase(Boolean.FALSE);
				}else{
					objSettlementOperation.setForcedPurchase(Boolean.TRUE);
				}
			}else{
				objSettlementOperation.setForcedPurchase(Boolean.FALSE);
			}
			lstSettlementOperationAux.add(objSettlementOperation);
		}
		return lstSettlementOperationAux;
	}
	/**
	 * Gets the begin end day service.
	 *
	 * @return the begin end day service
	 * @throws ServiceException the service exception
	 */
	public BeginEndDay getBeginEndDayService() throws ServiceException{
		String querySql = "Select be From BeginEndDay be Where trunc(be.processDay) = trunc(:currentDateParam)";
		Query query = em.createQuery(querySql);
		query.setParameter("currentDateParam", CommonsUtilities.currentDate());
		try{
			return (BeginEndDay) query.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}catch(NonUniqueResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the modality group operations.
	 *
	 * @param monitorSettlement the monitor settlement
	 * @return the modality group operations
	 * @throws ServiceException the service exception
	 */
	public List<Object[]>  getModalityGroupSettlementOperations(MonitorSettlementTO monitorSettlement) throws ServiceException{
		
		StringBuffer stringBuilderSql = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		stringBuilderSql.append("select distinct"); 
		stringBuilderSql.append(" 		mg.idModalityGroupPk,");//0
		stringBuilderSql.append(" 		mg.groupName,");//1
		stringBuilderSql.append(" 		mg.settlementSchema,");//2
		stringBuilderSql.append(" 		nme.idNegotiationMechanismPk,");//3
		stringBuilderSql.append(" 		nme.mechanismName ");//4
		stringBuilderSql.append(" 	From ModalityGroup mg , ModalityGroupDetail mgd, MechanismModality mm , MechanismOperation mo , SettlementOperation so, ");
		stringBuilderSql.append("       NegotiationMechanism nme , NegotiationModality nmo  ");
		stringBuilderSql.append("   where mo.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = mm.id.idNegotiationMechanismPk ");
		stringBuilderSql.append("   and   mo.mechanisnModality.negotiationModality.idNegotiationModalityPk = mm.id.idNegotiationModalityPk ");
		stringBuilderSql.append("   and   mgd.negotiationMechanism.idNegotiationMechanismPk = mm.id.idNegotiationMechanismPk ");
		stringBuilderSql.append("   and   mgd.negotiationModality.idNegotiationModalityPk = mm.id.idNegotiationModalityPk ");
		stringBuilderSql.append("   and   nme.idNegotiationMechanismPk = mm.id.idNegotiationMechanismPk ");
		stringBuilderSql.append("   and   nmo.idNegotiationModalityPk = mm.id.idNegotiationModalityPk ");
		stringBuilderSql.append("   and   mgd.modalityGroup.idModalityGroupPk = mg.idModalityGroupPk ");
		stringBuilderSql.append("   and   mo.idMechanismOperationPk = so.mechanismOperation.idMechanismOperationPk  ");
		stringBuilderSql.append("   and   SO.settlementDate  = :settlementDate  ");
	    stringBuilderSql.append(" and  (   (MO.indMarginGuarantee = :indicatorOne and MO.marginReference = :complianceMargin )");
	    stringBuilderSql.append(" 		or (MO.indMarginGuarantee != :indicatorOne and MO.marginReference is null ) ");
	    stringBuilderSql.append(" 		or (MO.indMarginGuarantee = :indicatorOne and MO.marginReference is null ) ");
	    stringBuilderSql.append(" 		)");
	    
		if(monitorSettlement.getIdParticipantPk()!=null){
			stringBuilderSql.append(" and ( mo.sellerParticipant.idParticipantPk = :idParticipant ");
			stringBuilderSql.append("       or mo.buyerParticipant.idParticipantPk = :idParticipant )");
			parameters.put("idParticipant",monitorSettlement.getIdParticipantPk());
		}
		
		if(monitorSettlement.getNegotiationMechanismPk()!=null){
			stringBuilderSql.append(" and nme.idNegotiationMechanismPk = :idNegotiationMechanismPk");
			parameters.put("idNegotiationMechanismPk",monitorSettlement.getNegotiationMechanismPk());
		}
		if(monitorSettlement.getSettlementSchemePk()!=null){
			stringBuilderSql.append(" and MG.settlementSchema = :settlementSchema");
			stringBuilderSql.append(" and SO.settlementSchema = :settlementSchema");
			parameters.put("settlementSchema",monitorSettlement.getSettlementSchemePk());
		}
		if(monitorSettlement.getModalityGroupPk()!=null){
			stringBuilderSql.append(" and mg.idModalityGroupPk = :idModalityGroupPk");
			parameters.put("idModalityGroupPk",monitorSettlement.getModalityGroupPk());
		}
		
		parameters.put("settlementDate",monitorSettlement.getSettlementDate());
	    
	    stringBuilderSql.append(" group by"); 
	    stringBuilderSql.append(" 	mg.idModalityGroupPk,");
	    stringBuilderSql.append(" 	mg.groupName,");
	    stringBuilderSql.append(" 	mg.settlementSchema,");
	    stringBuilderSql.append(" 	nme.idNegotiationMechanismPk,");
	    stringBuilderSql.append(" 	nme.mechanismName ");
	    
	    parameters.put("complianceMargin", AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
	    parameters.put("indicatorOne", ComponentConstant.ONE);
	//    parameters.put("indicatorZero", ComponentConstant.ZERO);
	    
	    return (List<Object[]>) findListByQueryString(stringBuilderSql.toString(),parameters);
	}
	
	
	/**
	 * Validate settlement schedule sequence.
	 *
	 * @param idCurrency the id currency
	 * @param idModalityGroup the id modality group
	 * @param idMechanism the id mechanism
	 * @param settlementDate the settlement date
	 * @param scheduleType the schedule type
	 * @param execTimes the exec times
	 * @param checkPrevious the check previous
	 * @throws ServiceException the service exception
	 */
	public void validateSettlementScheduleSequence(Integer idCurrency, Long idModalityGroup, Long idMechanism, 
			Date settlementDate, Integer scheduleType, Long execTimes, boolean checkPrevious) throws ServiceException {
		
		StringBuffer stbQuery = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();
		stbQuery.append(" Select count(sp.idSettlementProcessPk) From SettlementProcess sp");
		stbQuery.append(" WHERE sp.negotiationMechanism.idNegotiationMechanismPk = :mechanism AND ");
		stbQuery.append(" sp.settlementDate = :settlementDate AND ");
		stbQuery.append(" sp.modalityGroup.idModalityGroupPk = :modalityGroup AND ");
		stbQuery.append(" sp.currency = :currency and ");
		stbQuery.append(" sp.scheduleType = :scheduleType ");
		//stbQuery.append(" sp.processNumber = :processNumber ");
		
		parameters.put("mechanism", idMechanism);
		parameters.put("settlementDate", settlementDate);
		parameters.put("modalityGroup", idModalityGroup);
		parameters.put("currency", idCurrency);
		parameters.put("scheduleType", scheduleType);
		//parameters.put("processNumber", execTimes);
		
	    Long count = (Long) findObjectByQueryString(stbQuery.toString(),parameters);
	    if(checkPrevious){
	    	if(count == 0l){
		    	throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_PENDING_SCHEDULE);
		    }
	    }else{
	    	if(count >= execTimes){
		    	throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_MAX_EXEC_SCHEDULE);
		    }
	    }
	    
	}
	
}