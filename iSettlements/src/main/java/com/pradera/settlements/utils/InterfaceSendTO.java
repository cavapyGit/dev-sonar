package com.pradera.settlements.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceSendTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18-dic-2013
 */
public class InterfaceSendTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The Id. */
	private Long id;
	
	/** The interface id. */
	private Long interfaceId;
	
	/** The interface name. */
	private String interfaceName;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The institution type value. */
	private String institutionTypeValue;
	
	/** The participant id. */
	private Long participantId;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The execution type. */
	private Integer sendType;
	
	/** The execution type value. */
	private String sendTypeValue;
	
	/** The registry date. */
	private Date registryDate;

	/** The number send. */
	private Integer numberSend;
	
	/** The number send ok. */
	private Integer numberSendOk;
	
	/** The number send fail. */
	private Integer numberSendFail;
	
	/** The issuer request. */
	private Issuer issuerRequest; 
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The holder code. */
	private Long holderCode;

	/** The query sql. */
	private String querySql;
	
	/** The interface to generate id. */
	private Long interfaceToGenerateId;
	
	/** The list id security code pk. */
	private List<String> listIdSecurityCodePk;
	
	/** The send ftp. */
	private boolean sendFtp;
	
	/**
	 * Instantiates a new interface send to.
	 */
	public InterfaceSendTO() {
		initialDate = new Date();
		finalDate = new Date();
		issuerRequest = new Issuer();
	}
	
	/**
	 * Instantiates a new interface send to.
	 *
	 * @param id the id
	 * @param sendType the send type
	 * @param institutionType the institution type
	 * @param interfaceId the interface id
	 * @param interfaceName the interface name
	 * @param registryDate the registry date
	 * @param numberSend the number send
	 * @param numberSendOk the number send ok
	 * @param numberSendFail the number send fail
	 */
	public InterfaceSendTO(	Long id, 
							Integer sendType, 
							Integer institutionType, 
							Long interfaceId,
							String interfaceName,
							Date registryDate,
							Integer numberSend,
							Integer numberSendOk,
							Integer numberSendFail){
		this.id = id;
		this.sendType = sendType;
		this.institutionType = institutionType;
		this.interfaceId = interfaceId;
		this.interfaceName = interfaceName;
		this.registryDate = registryDate;
		this.numberSend = numberSend;
		this.numberSendOk = numberSendOk;
		this.numberSendFail = numberSendFail;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}
	
	/**
	 * Gets the interface name.
	 *
	 * @return the interface name
	 */
	public String getInterfaceName() {
		return interfaceName;
	}

	/**
	 * Sets the interface name.
	 *
	 * @param interfaceName the new interface name
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	/**
	 * Gets the institution type value.
	 *
	 * @return the institution type value
	 */
	public String getInstitutionTypeValue() {
		return institutionTypeValue;
	}

	/**
	 * Sets the institution type value.
	 *
	 * @param institutionTypeValue the new institution type value
	 */
	public void setInstitutionTypeValue(String institutionTypeValue) {
		this.institutionTypeValue = institutionTypeValue;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the number send.
	 *
	 * @return the number send
	 */
	public Integer getNumberSend() {
		return numberSend;
	}

	/**
	 * Sets the number send.
	 *
	 * @param numberSend the new number send
	 */
	public void setNumberSend(Integer numberSend) {
		this.numberSend = numberSend;
	}

	/**
	 * Gets the number send ok.
	 *
	 * @return the number send ok
	 */
	public Integer getNumberSendOk() {
		return numberSendOk;
	}

	/**
	 * Sets the number send ok.
	 *
	 * @param numberSendOk the new number send ok
	 */
	public void setNumberSendOk(Integer numberSendOk) {
		this.numberSendOk = numberSendOk;
	}

	/**
	 * Gets the number send fail.
	 *
	 * @return the number send fail
	 */
	public Integer getNumberSendFail() {
		return numberSendFail;
	}

	/**
	 * Sets the number send fail.
	 *
	 * @param numberSendFail the new number send fail
	 */
	public void setNumberSendFail(Integer numberSendFail) {
		this.numberSendFail = numberSendFail;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the interface id.
	 *
	 * @return the interface id
	 */
	public Long getInterfaceId() {
		return interfaceId;
	}

	/**
	 * Sets the interface id.
	 *
	 * @param interfaceId the new interface id
	 */
	public void setInterfaceId(Long interfaceId) {
		this.interfaceId = interfaceId;
	}

	/**
	 * Gets the send type.
	 *
	 * @return the send type
	 */
	public Integer getSendType() {
		return sendType;
	}

	/**
	 * Sets the send type.
	 *
	 * @param sendType the new send type
	 */
	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

	/**
	 * Gets the send type value.
	 *
	 * @return the send type value
	 */
	public String getSendTypeValue() {
		return sendTypeValue;
	}

	/**
	 * Sets the send type value.
	 *
	 * @param sendTypeValue the new send type value
	 */
	public void setSendTypeValue(String sendTypeValue) {
		this.sendTypeValue = sendTypeValue;
	}

	/**
	 * Gets the participant id.
	 *
	 * @return the participant id
	 */
	public Long getParticipantId() {
		return participantId;
	}

	/**
	 * Sets the participant id.
	 *
	 * @param participantId the new participant id
	 */
	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}

	/**
	 * Gets the issuer request.
	 *
	 * @return the issuer request
	 */
	public Issuer getIssuerRequest() {
		return issuerRequest;
	}

	/**
	 * Sets the issuer request.
	 *
	 * @param issuerRequest the new issuer request
	 */
	public void setIssuerRequest(Issuer issuerRequest) {
		this.issuerRequest = issuerRequest;
	}

	/**
	 * Gets the interface to generate id.
	 *
	 * @return the interface to generate id
	 */
	public Long getInterfaceToGenerateId() {
		return interfaceToGenerateId;
	}

	/**
	 * Sets the interface to generate id.
	 *
	 * @param interfaceToGenerateId the new interface to generate id
	 */
	public void setInterfaceToGenerateId(Long interfaceToGenerateId) {
		this.interfaceToGenerateId = interfaceToGenerateId;
	}

	/**
	 * Gets the query sql.
	 *
	 * @return the querySql
	 */
	public String getQuerySql() {
		return querySql;
	}

	/**
	 * Sets the query sql.
	 *
	 * @param querySql the querySql to set
	 */
	public void setQuerySql(String querySql) {
		this.querySql = querySql;
	}

	/**
	 * Gets the list id security code pk.
	 *
	 * @return the listIdSecurityCodePk
	 */
	public List<String> getListIdSecurityCodePk() {
		return listIdSecurityCodePk;
	}

	/**
	 * Sets the list id security code pk.
	 *
	 * @param listIdSecurityCodePk the listIdSecurityCodePk to set
	 */
	public void setListIdSecurityCodePk(List<String> listIdSecurityCodePk) {
		this.listIdSecurityCodePk = listIdSecurityCodePk;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	/**
	 * Gets the holder code.
	 *
	 * @return the holderCode
	 */
	public Long getHolderCode() {
		return holderCode;
	}

	/**
	 * Sets the holder code.
	 *
	 * @param holderCode the holderCode to set
	 */
	public void setHolderCode(Long holderCode) {
		this.holderCode = holderCode;
	}

	/**
	 * @return the sendFtp
	 */
	public boolean isSendFtp() {
		return sendFtp;
	}

	/**
	 * Sets the send ftp.
	 *
	 * @param sendFtp the sendFtp to set
	 */
	public void setSendFtp(boolean sendFtp) {
		this.sendFtp = sendFtp;
	}
	
}
