package com.pradera.settlements.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.constants.Constants;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.sirtex.to.SirtexOperationResultTO;
import com.pradera.settlements.core.to.FundsParticipantSettlementTO;
import com.pradera.settlements.core.to.NetParticipantPositionTO;
import com.pradera.settlements.core.to.NetSettlementOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NegotiationUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class NegotiationUtils {
	
	/**
	 * Mechanism operation is split.
	 * metdo para validar si la operacion es SPlit
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsSplit(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = NegotiationModalityType.listSomeElements(NegotiationModalityType.COUPON_SPLIT_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es prestamo de valores
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Mechanism operation is repurchase.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsRepurchase(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = NegotiationModalityType.listSomeElements(NegotiationModalityType.REPURCHASE_EQUITIES,NegotiationModalityType.REPURCHASE_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es prestamo de valores
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Mechanism operation is securities loan.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsSecuritiesLoan(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = NegotiationModalityType.listSomeElements(NegotiationModalityType.SECURITIES_LOAN_EQUITIES,NegotiationModalityType.SECURITIES_LOAN_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es prestamo de valores
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Mechanism operation is primary placement.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsPrimaryPlacement(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = NegotiationModalityType.listSomeElements(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES,NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es colocacion Primaria
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Mechanism operation is swap.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsSwap(MechanismOperation otcOperation){
		if(otcOperation!= null && otcOperation.getMechanisnModality()!= null && otcOperation.getMechanisnModality().getId()!= null && otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk() != null) {
			NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
			List<NegotiationModalityType> negModTypeList = NegotiationModalityType.listSomeElements(NegotiationModalityType.SWAP_EQUITIES,NegotiationModalityType.SWAP_FIXED_INCOME);
			if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es permuta
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Mechanism operation is report.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsReport(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = 
				NegotiationModalityType.listSomeElements(NegotiationModalityType.REPORT_EQUITIES,
														 NegotiationModalityType.REPORT_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es Reporto
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Mechanism operation is view report.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsViewReport(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = 
				NegotiationModalityType.listSomeElements(NegotiationModalityType.VIEW_REPO_EQUITIES,
														 NegotiationModalityType.VIEW_REPO_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es Reporto a la Vista
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Mechanism operation is secundary report.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsSecundaryReport(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = 
				NegotiationModalityType.listSomeElements(NegotiationModalityType.SECUNDARY_REPO_EQUITIES,
														 NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es Reporto a la Vista
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Mechanism operation is cash.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsCash(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = NegotiationModalityType.listSomeElements(NegotiationModalityType.CASH_EQUITIES,NegotiationModalityType.CASH_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es colocacion Primaria
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Mechanism operation is foward.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean mechanismOperationIsFoward(MechanismOperation otcOperation){
		NegotiationModality negotiationModality = new NegotiationModality(otcOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		List<NegotiationModalityType> negModTypeList = NegotiationModalityType.listSomeElements(NegotiationModalityType.FOWARD_SALE_EQUITIES,NegotiationModalityType.FOWARD_SALE_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(negotiationModality.getIdNegotiationModalityPk()))){//La modalidad es Foward
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Calculate valued amount.
	 *
	 * @param balance the balance
	 * @param factor the factor
	 * @param quotePrice the quote price
	 * @return the big decimal
	 */
	public static BigDecimal calculateValuedAmount(BigDecimal balance, BigDecimal factor, BigDecimal quotePrice){
		BigDecimal valuedAmount = balance.multiply(factor).multiply(quotePrice);
		return valuedAmount;
	}
	

	/**
	 * Gets the efective tasa.
	 * metodo para calcular la tasa efectiva,
	 * Formula TE = ((MP-MC)/MC) x 100
	 * TE: Tasa Efectiva (efectiveTasa)
	 * MP: Monto Plazo (termAmount)
	 * MC: Monto Contado (cashAmount)
	 * @param termAmount the term amount
	 * @param cashAmount the cash amount
	 * @return the efective tasa
	 */
	public static BigDecimal getEfectiveRate(BigDecimal termAmount, BigDecimal cashAmount){
		if(termAmount==null || cashAmount==null){
			return BigDecimal.ZERO;
		}
		BigDecimal efectiveRate = ((termAmount.subtract(cashAmount)).divide(cashAmount,8,BigDecimal.ROUND_HALF_UP)).multiply(new BigDecimal(100));
		return efectiveRate;
	}
	
	/**
	 * Gets the settlement amount.
	 *
	 * @param price the price
	 * @param stockQuantity the stock quantity
	 * @return the settlement amount
	 */
	public static BigDecimal getSettlementAmount(BigDecimal price, BigDecimal stockQuantity){
		BigDecimal roundPrice= price.setScale(2,RoundingMode.HALF_UP);
		BigDecimal amount = roundPrice.multiply(stockQuantity).setScale(2,RoundingMode.HALF_UP);
		return amount;
	}
	
	/**
	 * Gets the term price.
	 *
	 * @param cashPrice the cash price
	 * @param rate the rate
	 * @param settlementDays the settlement days
	 * @return the term price
	 */
	public static BigDecimal getTermPrice(BigDecimal cashPrice, BigDecimal rate, Long settlementDays){
		if(rate==null || cashPrice==null){
			return BigDecimal.ZERO;
		}
		BigDecimal roundCashPrice = cashPrice.setScale(2,RoundingMode.HALF_UP);
		BigDecimal days = BigDecimal.valueOf(settlementDays);
		BigDecimal _360days= BigDecimal.valueOf(360);	
		BigDecimal _100= BigDecimal.valueOf(100);
		BigDecimal pl = days.divide(_360days,MathContext.DECIMAL128);
		BigDecimal plmul = (rate.divide(_100,MathContext.DECIMAL128)).multiply(pl);
		BigDecimal pre_price = roundCashPrice.multiply(BigDecimal.ONE.add(plmul)); 
		// need to lose precision (ej. 4.814999999.. is scaled to 4.81 using BigDecimal and scale 2;
		// converting to double first, the value is 4.815 and then scaled to 4.82 using scale 2, which is needed)
		BigDecimal termPrice = (BigDecimal.valueOf(pre_price.doubleValue())).setScale(2,RoundingMode.HALF_UP); 
		return termPrice;
	}
	

	
	/**
	 * Gets the mechanism operation from to.
	 *
	 * @param operationObjectList the par me operation to list
	 * @return the mechanism operation from to
	 */
	public static List<NetSettlementOperationTO> populateSettlementOperationsAndParticipant(List<Object[]> operationObjectList){
		Map<Long, NetSettlementOperationTO> mapSettlementOperations  = new LinkedHashMap<Long, NetSettlementOperationTO>();
		for(Object[] objOperation: operationObjectList){
			
			Long idSettlementOperation = (Long)objOperation[17];
			NetSettlementOperationTO settlementOperationTO = mapSettlementOperations.get(idSettlementOperation);
			
			if(settlementOperationTO == null){
				settlementOperationTO = new NetSettlementOperationTO();
				settlementOperationTO.setLstParticipantSettlements(new ArrayList<FundsParticipantSettlementTO>());
				settlementOperationTO.setIdMechanismOperation((Long)objOperation[0]);
				settlementOperationTO.setIdMechanism((Long)objOperation[1]);
				settlementOperationTO.setMechanismName((String)objOperation[2]);
				settlementOperationTO.setIdModality((Long)objOperation[3]);
				settlementOperationTO.setModalityName((String)objOperation[4]);
				settlementOperationTO.setIdSaleParticipant((Long)objOperation[5]);
				settlementOperationTO.setSaleParticipantDescription((String)objOperation[6]);
				settlementOperationTO.setSaleParticipantNemonic((String)objOperation[7]);
				settlementOperationTO.setIdPurchaseParticipant((Long)objOperation[8]);
				settlementOperationTO.setPurchaseParticipantDescription((String)objOperation[9]);
				settlementOperationTO.setPurchaseParticipantNemonic((String)objOperation[10]);
				settlementOperationTO.setOperationDate((Date)objOperation[11]);
				settlementOperationTO.setOperationNumber((Long)objOperation[12]);
				settlementOperationTO.setBallotNumber((Long)objOperation[13]);
				settlementOperationTO.setSequential((Long)objOperation[14]);
				settlementOperationTO.setOperationState((Integer)objOperation[15]);
				settlementOperationTO.setIdSecurityCode((String)objOperation[16]);
				settlementOperationTO.setIdSettlementOperation((Long)objOperation[17]);
				settlementOperationTO.setStockQuantity((BigDecimal)objOperation[18]);
				settlementOperationTO.setSettlementAmount((BigDecimal)objOperation[19]);
				settlementOperationTO.setSettlementDate((Date)objOperation[20]);
				settlementOperationTO.setOperationPart((Integer)objOperation[21]);
				if (BooleanType.NO.getCode().equals(new Integer(objOperation[29].toString()))) {
					settlementOperationTO.setIndChainedHolderOperation(BooleanType.NO.getValue());
				} else {
					settlementOperationTO.setIndChainedHolderOperation(BooleanType.YES.getValue());
				}
				mapSettlementOperations.put(idSettlementOperation, settlementOperationTO);
			}
			
			FundsParticipantSettlementTO settlementParticipantTO = new FundsParticipantSettlementTO();
			settlementParticipantTO.setIdParticipantSettlement((Long)objOperation[22]);
			settlementParticipantTO.setIdParticipant((Long)objOperation[23]);
			settlementParticipantTO.setParticipantDescription((String)objOperation[24]);
			settlementParticipantTO.setParticipantNemonic((String)objOperation[25]);
			settlementParticipantTO.setRole((Integer)objOperation[26]);
			settlementParticipantTO.setStockQuantity((BigDecimal)objOperation[27]);
			settlementParticipantTO.setSettlementAmount((BigDecimal)objOperation[28]);
			
			settlementOperationTO.getLstParticipantSettlements().add(settlementParticipantTO);
	
		}
		List<NetSettlementOperationTO> lstNetSettlementOperationTOs = new ArrayList<NetSettlementOperationTO>();
		List<Long> lstIdSettlementOperation= new LinkedList<Long>(mapSettlementOperations.keySet());
		for (Long idSettlementOperation: lstIdSettlementOperation) {
			lstNetSettlementOperationTOs.add(mapSettlementOperations.get(idSettlementOperation));
		}
		
		return lstNetSettlementOperationTOs;
	}
	
	/**
	 * Checks if is crossed operation.
	 *
	 * @param settlementOperation the settlement operation
	 * @param idParticipant the id participant
	 * @return true, if is crossed operation
	 */
	public static boolean isCrossedOperation(NetSettlementOperationTO settlementOperation, Long idParticipant) {
		BigDecimal saleAmount= BigDecimal.ZERO;
		BigDecimal purchaseAmount= BigDecimal.ZERO;
		for (FundsParticipantSettlementTO fundsParticipantSettlementTO : settlementOperation.getLstParticipantSettlements()) {
			if(fundsParticipantSettlementTO.getIdParticipant().equals(idParticipant)){
				if(ComponentConstant.SALE_ROLE.equals(fundsParticipantSettlementTO.getRole())){
					saleAmount = saleAmount .add(fundsParticipantSettlementTO.getSettlementAmount());
				}else if(ComponentConstant.PURCHARSE_ROLE.equals(fundsParticipantSettlementTO.getRole())){
					purchaseAmount = purchaseAmount .add(fundsParticipantSettlementTO.getSettlementAmount());
				}
			}
		}
		if(saleAmount.compareTo(purchaseAmount) == 0){
			return true;
		} else if (saleAmount.compareTo(purchaseAmount) > 0) {
			settlementOperation.setParticipantRole(ComponentConstant.SALE_ROLE);
		} else {
			settlementOperation.setParticipantRole(ComponentConstant.PURCHARSE_ROLE);
		}
		return false;
	}

	/**
	 * Gets the operation description.
	 *
	 * @param mechanismName the mechanism name
	 * @param modalityName the modality name
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @return the operation description
	 */
	public static String getOperationDescription(String mechanismName, String modalityName, Date operationDate, Long operationNumber){
		StringBuilder sb = new StringBuilder();
		sb.append(mechanismName).append(GeneralConstants.SLASH_SPACE).append(modalityName).append(GeneralConstants.SLASH_SPACE);
		sb.append(CommonsUtilities.convertDatetoString(operationDate)).append(GeneralConstants.SLASH_SPACE).append(operationNumber.toString());
		return sb.toString();
	}
	
	/**
	 * Validate mechanism number.
	 *
	 * @param referenceNumber the reference number
	 * @param referenceDate the reference date
	 * @return true, if successful
	 */
	public static boolean validateMechanismNumber(String referenceNumber, Date referenceDate) {
		
		String numberDateString = referenceNumber.substring(0, 6);
		String referenceDateString = CommonsUtilities.convertDateToString(referenceDate,CommonsUtilities.DATE_PATTERN_MINIMAL_REV);
		if(!numberDateString.equals(referenceDateString)){
			return false;
		}
		return true;
	}
	
	/** The lst date type. */
	private static List<SelectItem> lstDateType;
	
	/** The lst roles. */
	private static List<SelectItem> lstRoles;
	
	/** The lst settlement type. */
	private static List<SelectItem> lstSettlementType;
	
	/** The lst settlement schema. */
	private static List<SelectItem> lstSettlementSchema;
	
	/** The lst operation part. */
	private static List<SelectItem> lstOperationPart;
	
	/**
	 * Gets the lst roles.
	 *
	 * @return the lst roles
	 */
	public static List<SelectItem> getLstRoles() {	
		lstRoles = new ArrayList<SelectItem>();
		lstRoles.add(new SelectItem(ComponentConstant.PURCHARSE_ROLE,PropertiesUtilities.getMessage("lbl.operation.role.purchase")));
		lstRoles.add(new SelectItem(ComponentConstant.SALE_ROLE,PropertiesUtilities.getMessage("lbl.operation.role.sale")));	
		return lstRoles;
	}	
	
	/**
	 * Gets the lst operation part.
	 *
	 * @return the lst operation part
	 */
	public static List<SelectItem> getLstOperationPart() {		
		lstOperationPart = new ArrayList<SelectItem>();
		lstOperationPart.add(new SelectItem(ComponentConstant.CASH_PART,PropertiesUtilities.getMessage("lbl.operation.cash.part")));
		lstOperationPart.add(new SelectItem(ComponentConstant.TERM_PART,PropertiesUtilities.getMessage("lbl.operation.term.part")));
		return lstOperationPart;
	}	
	
	/**
	 * Gets the date types.
	 *
	 * @return the date types
	 */
	public static List<SelectItem> getDateTypes() {		
		lstDateType = new ArrayList<SelectItem>();
		lstDateType.add(new SelectItem(ComponentConstant.CASH_PART,PropertiesUtilities.getMessage("lbl.operation.cash.settlement")));
		lstDateType.add(new SelectItem(ComponentConstant.TERM_PART,PropertiesUtilities.getMessage("lbl.operation.term.settlement")));	
		return lstDateType;
	}	
	
	/**
	 * Gets the settlement types.
	 *
	 * @return the settlement types
	 */
	public static List<SelectItem> getSettlementTypes() {	
		lstSettlementType = new ArrayList<SelectItem>();
		lstSettlementType.add(new SelectItem(SettlementType.DVP.getCode(),SettlementType.DVP.getValue()));
		lstSettlementType.add(new SelectItem(SettlementType.FOP.getCode(),SettlementType.FOP.getValue()));
		lstSettlementType.add(new SelectItem(SettlementType.ECE.getCode(),SettlementType.ECE.getValue()));		
		return lstSettlementType;
	}
	
	/**
	 * Gets the settlement schemas.
	 *
	 * @return the settlement schemas
	 */
	public static List<SelectItem> getSettlementSchemas() {		
		lstSettlementSchema = new ArrayList<SelectItem>();
		lstSettlementSchema.add(new SelectItem(SettlementSchemaType.NET.getCode(),SettlementSchemaType.NET.getValue()));
		lstSettlementSchema.add(new SelectItem(SettlementSchemaType.GROSS.getCode(),SettlementSchemaType.GROSS.getValue()));	
		return lstSettlementSchema;
	}

	/**
	 * Contains participant.
	 *
	 * @param lstParticipants the lst participants
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 */
	public static Participant containsParticipant(
			List<Participant> lstParticipants, Long idParticipantPk) {
		for(Participant participant : lstParticipants){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}

	/**
	 * Populate search operations.
	 *
	 * @param objOperation the obj operation
	 * @return the mechanism operation to
	 */
	public static MechanismOperationTO populateSearchOperations(Object[] objOperation) {
		MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
		mechanismOperationTO.setIdMechanism((Long)objOperation[0]);
		mechanismOperationTO.setMechanismName((String)objOperation[1]);
		mechanismOperationTO.setIdModality((Long)objOperation[2]);
		mechanismOperationTO.setModalityName((String)objOperation[3]);
		mechanismOperationTO.setIdMechanismOperationPk((Long)objOperation[4]);
		mechanismOperationTO.setOperationNumber((Long)objOperation[5]);
		mechanismOperationTO.setOperationDate((Date)objOperation[6]);
		mechanismOperationTO.setBallotNumber((Long)objOperation[7]);
		mechanismOperationTO.setSequential((Long)objOperation[8]);
		mechanismOperationTO.setIdSecurityCodePk((String)objOperation[9]);
		mechanismOperationTO.setStockQuantity((BigDecimal)objOperation[10]);
		mechanismOperationTO.setCashSettlementDate((Date)objOperation[11]);
		mechanismOperationTO.setTermSettlementDate((Date)objOperation[12]);
		mechanismOperationTO.setIdPurchaseParticipant((Long)objOperation[13]);
		mechanismOperationTO.setPurchaseParticipantNemonic((String)objOperation[14]);
		mechanismOperationTO.setIdSaleParticipant((Long)objOperation[15]);
		mechanismOperationTO.setSaleParticipantNemonic((String)objOperation[16]);
		mechanismOperationTO.setOperationState((Integer)objOperation[17]);
		mechanismOperationTO.setStateDescription((String)objOperation[18]);
		mechanismOperationTO.setIdProcessFile((Long)objOperation[19]);
		mechanismOperationTO.setCurrencyDescription((String)objOperation[20]);
		if(Integer.valueOf(objOperation[21].toString()).equals(BooleanType.NO.getCode())){
			mechanismOperationTO.setForcedPurchase(Boolean.FALSE);						
		}else{
			mechanismOperationTO.setForcedPurchase(Boolean.TRUE);
		}
		mechanismOperationTO.setTermSettlementDays((Long)objOperation[22]);
		mechanismOperationTO.setAmountRate((BigDecimal)objOperation[23]);
		mechanismOperationTO.setCashSettlementPrice((BigDecimal)objOperation[24]);
		return mechanismOperationTO;
	}
	
	/**
	 * Populate search operations.
	 *
	 * @param objOperation the obj operation
	 * @return the mechanism operation to
	 */
	public static MechanismOperationTO populateSearchOperationsUnfulfillment(Object[] objOperation) {
		MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
		mechanismOperationTO.setIdMechanism((Long)objOperation[0]);
		mechanismOperationTO.setMechanismName((String)objOperation[1]);
		mechanismOperationTO.setIdModality((Long)objOperation[2]);
		mechanismOperationTO.setModalityName((String)objOperation[3]);
		mechanismOperationTO.setIdMechanismOperationPk((Long)objOperation[4]);
		mechanismOperationTO.setOperationNumber((Long)objOperation[5]);
		mechanismOperationTO.setOperationDate((Date)objOperation[6]);
		mechanismOperationTO.setBallotNumber((Long)objOperation[7]);
		mechanismOperationTO.setSequential((Long)objOperation[8]);
		mechanismOperationTO.setIdSecurityCodePk((String)objOperation[9]);
		mechanismOperationTO.setStockQuantity((BigDecimal)objOperation[10]);
		mechanismOperationTO.setCashSettlementDate((Date)objOperation[11]);
		mechanismOperationTO.setTermSettlementDate((Date)objOperation[12]);
		mechanismOperationTO.setIdPurchaseParticipant((Long)objOperation[13]);
		mechanismOperationTO.setPurchaseParticipantNemonic((String)objOperation[14]);
		mechanismOperationTO.setIdSaleParticipant((Long)objOperation[15]);
		mechanismOperationTO.setSaleParticipantNemonic((String)objOperation[16]);
		mechanismOperationTO.setOperationState((Integer)objOperation[17]);
		mechanismOperationTO.setStateDescription((String)objOperation[18]);
		mechanismOperationTO.setIdProcessFile((Long)objOperation[19]);
		mechanismOperationTO.setCurrencyDescription((String)objOperation[20]);
		if(Integer.valueOf(objOperation[21].toString()).equals(BooleanType.NO.getCode())){
			mechanismOperationTO.setForcedPurchase(Boolean.FALSE);						
		}else{
			mechanismOperationTO.setForcedPurchase(Boolean.TRUE);
		}
		mechanismOperationTO.setTermSettlementDays((Long)objOperation[22]);
		mechanismOperationTO.setAmountRate((BigDecimal)objOperation[23]);
		mechanismOperationTO.setCashSettlementPrice((BigDecimal)objOperation[24]);
		mechanismOperationTO.setIdOperationUnfulfillment((Long)objOperation[25]);
		return mechanismOperationTO;
	}
	
	/**
	 * Populate search sirtex to.
	 *
	 * @param data the data
	 * @return the sirtex operation result to
	 */
	public static SirtexOperationResultTO populateSearchSirtexTO(Object[] data){
		SirtexOperationResultTO sirtexResultTO = new SirtexOperationResultTO();
		sirtexResultTO.setIdMechanismOperationRequestPk(Long.valueOf(data[0].toString()));
		sirtexResultTO.setIdNegotiationModalityPk(Long.valueOf(data[1].toString()));
		sirtexResultTO.setModalityName((String) data[2]);
		sirtexResultTO.setOperationDate((Date) data[3]);
		sirtexResultTO.setIdPartSeller(Long.valueOf(data[4].toString()));
		sirtexResultTO.setMnemoPartSeller((String) data[5]);
		sirtexResultTO.setIdPartBuyer(Long.valueOf(data[6].toString()));
		sirtexResultTO.setMnemoPartBuyer((String) data[7]);
		sirtexResultTO.setSettlementCashDate((Date) data[8]);
		sirtexResultTO.setSettlementTermDate((Date) data[9]);
		sirtexResultTO.setSecurityCode((String) data[10]);
		sirtexResultTO.setOperationState(Integer.valueOf(data[11].toString()));
		sirtexResultTO.setRegistryUser((String)data[12]);
		sirtexResultTO.setStockQuantity(new BigDecimal(data[13].toString()));
		sirtexResultTO.setTermSettlementDays(data[14]!=null ? (Long.valueOf(data[14].toString())) : null );
		sirtexResultTO.setSettlementTermDate(data[15]!=null ? (Date)data[15] : null );
		//sirtexResultTO.setTermSettlementDaysReal(data[16]!=null ? (Long.valueOf(data[16].toString())) : null );
		//sirtexResultTO.setCoupon((Integer.valueOf(data[18].toString()))== GeneralConstants.ONE_VALUE_INTEGER ? (BooleanType.YES.getValue()) : null );
		return sirtexResultTO;
	}

	/**
	 * Populate participant positions.
	 *
	 * @param lstSettlementOperations the lst settlement operations
	 * @param allOperations the all operations
	 * @return the map
	 */
	public static Map<Long, NetParticipantPositionTO> populateParticipantPositions(List<NetSettlementOperationTO> lstSettlementOperations, boolean allOperations) {
		Map<Long,NetParticipantPositionTO> mapParticipantPosition = new HashMap<Long, NetParticipantPositionTO>();
		for(NetSettlementOperationTO settlementOperationTO : lstSettlementOperations){
			for(FundsParticipantSettlementTO settlementParticipantTO : settlementOperationTO.getLstParticipantSettlements()){
				NetParticipantPositionTO participantPositionTO = mapParticipantPosition.get(settlementParticipantTO.getIdParticipant());
				if(participantPositionTO == null){
					participantPositionTO = new NetParticipantPositionTO();
					participantPositionTO.setIdParticipant(settlementParticipantTO.getIdParticipant());
					participantPositionTO.setParticipantDescription(settlementParticipantTO.getParticipantDescription());
					participantPositionTO.setParticipantNemonic(settlementParticipantTO.getParticipantNemonic());
					mapParticipantPosition.put(participantPositionTO.getIdParticipant(), participantPositionTO);
				}
				
				if(allOperations || settlementOperationTO.getOperationSettlement().getOperationState().equals(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode())){
					if(ComponentConstant.SALE_ROLE.equals(settlementParticipantTO.getRole())){
						participantPositionTO.setSaleSettlementAmount(participantPositionTO.getSaleSettlementAmount().add(settlementParticipantTO.getSettlementAmount()));
					}else if(ComponentConstant.PURCHARSE_ROLE.equals(settlementParticipantTO.getRole())){
						participantPositionTO.setPurchaseSettlementAmount(participantPositionTO.getPurchaseSettlementAmount().add(settlementParticipantTO.getSettlementAmount()));
					}
				}
				
				if(!participantPositionTO.getLstSettlementOperation().contains(settlementOperationTO)){
					participantPositionTO.getLstSettlementOperation().add(settlementOperationTO);
				}
				
				participantPositionTO.updateCheckAllSettlementEditions();
			}
		}
		
		//issue?
		/*for (NetParticipantPositionTO name : mapParticipantPosition.values()) {
			BigDecimal posnet = name.getSaleSettlementAmount().subtract(name.getPurchaseSettlementAmount());
			name.setNetPositionTemp(posnet);
		}
		
		Map<Long,NetParticipantPositionTO> mapParticipantPosition2 = sortByValue(mapParticipantPosition);
		
		mapParticipantPosition = mapParticipantPosition2;*/
		
		return mapParticipantPosition;
	}
	
	/**
	 * Metodo para ordenar un map en base a un criterio
	 * @param unsortMap
	 * @return
	 */
	private static Map<Long, NetParticipantPositionTO> sortByValue(Map<Long, NetParticipantPositionTO> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<Long, NetParticipantPositionTO>> list =
                new LinkedList<Map.Entry<Long, NetParticipantPositionTO>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<Long, NetParticipantPositionTO>>() {
            public int compare(Map.Entry<Long, NetParticipantPositionTO> o1,
                               Map.Entry<Long, NetParticipantPositionTO> o2) {
                return (o2.getValue().getNetPositionTemp()).compareTo(o1.getValue().getNetPositionTemp());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<Long, NetParticipantPositionTO> sortedMap = new LinkedHashMap<Long, NetParticipantPositionTO>();
        for (Map.Entry<Long, NetParticipantPositionTO> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        /*
        //classic iterator example
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }*/


        return sortedMap;
    }
	

	/**
	 * Update participant net position.
	 *
	 * @param participantPositionTO the participant position to
	 * @param settlementOperationTO the settlement operation to
	 */
	public static void updateParticipantNetPosition(NetParticipantPositionTO participantPositionTO, NetSettlementOperationTO settlementOperationTO) {
		for(FundsParticipantSettlementTO settlementParticipantTO : settlementOperationTO.getLstParticipantSettlements()){
			if (participantPositionTO.getIdParticipant().equals(settlementParticipantTO.getIdParticipant())) {
				if(ComponentConstant.SALE_ROLE.equals(settlementParticipantTO.getRole())) {
					participantPositionTO.setNetPosition(participantPositionTO.getNetPosition().subtract(settlementParticipantTO.getSettlementAmount()));
				} else if (ComponentConstant.PURCHARSE_ROLE.equals(settlementParticipantTO.getRole())) {
					participantPositionTO.setNetPosition(participantPositionTO.getNetPosition().add(settlementParticipantTO.getSettlementAmount()));
				}
			}
		}
	}
	
	/**
	 * Gets the amount on operation.
	 *
	 * @param settlementOperationTO the settlement operation to
	 * @param idParticipant the id participant
	 * @param role the role
	 * @return the amount on operation
	 */
	public static BigDecimal getAmountOnOperation(NetSettlementOperationTO settlementOperationTO, Long idParticipant,Integer role) {
		BigDecimal amount= BigDecimal.ZERO;
		for (FundsParticipantSettlementTO settlementAccountTO : settlementOperationTO.getLstParticipantSettlements()) {
			if(settlementAccountTO.getIdParticipant().equals(idParticipant)){
				if(role.equals(settlementAccountTO.getRole())){
					amount = amount .add (settlementAccountTO.getSettlementAmount() );
				}
			}
		}
		return amount;
	}
	
	/**
	 * Validate quantity negotitation.
	 *
	 * @param indPrimaryPlacement the ind primary placement
	 * @param objSecurity the obj security
	 * @param stockQuantity the stock quantity
	 * @return the string
	 */
	public static String validateQuantityNegotitation(Integer indPrimaryPlacement, Security objSecurity, BigDecimal stockQuantity){
		if(ComponentConstant.ONE.equals(indPrimaryPlacement)){
			BigDecimal amountEntered = stockQuantity.multiply(objSecurity.getCurrentNominalValue());
			BigDecimal maxInvesment = objSecurity.getMaximumInvesment();
			BigDecimal minInvesment = objSecurity.getMinimumInvesment();
		  	if(minInvesment!=null && amountEntered.compareTo(minInvesment) < 0){
		  		return PropertiesConstants.OTC_OPERATION_VALIDATION_RANGE_AMOUTNS;
		  	}else if(maxInvesment!=null &&  maxInvesment.compareTo(BigDecimal.ZERO) > 0 &&  amountEntered.compareTo(maxInvesment) > 0){
		  		return PropertiesConstants.OTC_OPERATION_VALIDATION_RANGE_MAX_AMOUTNS;
		    }else{
			   return null;
		    }
		}
		return null;
	}	
	
	/**
	 * Gets the cui holder.
	 *
	 * @param holderAccount the holder account
	 * @return the cui holder
	 */
	public static String getCuiHolder(HolderAccount holderAccount){
		if(holderAccount!=null){
			StringBuilder cuiHolder = new StringBuilder();
			if (Validations.validateListIsNotNullAndNotEmpty(holderAccount.getHolderAccountDetails())) {				
				for (HolderAccountDetail holderAccountDetail:holderAccount.getHolderAccountDetails()) {
					if(Validations.validateIsNotNullAndNotEmpty(holderAccountDetail.getHolder())){
						cuiHolder.append(holderAccountDetail.getHolder().getIdHolderPk());
						cuiHolder.append(Constants.BR);
					}
				}
			}
			return cuiHolder.toString();
		}
		return null;
	}
	
	/**
	 * Convert to settlement account market facts.
	 *
	 * @param accountMarketfacts the account marketfacts
	 * @param holderAccountOperation the holder account operation
	 * @return the list
	 */
	public static List<AccountOperationMarketFact> convertToSettlementAccountMarketFacts(List<MarketFactAccountTO> accountMarketfacts, HolderAccountOperation holderAccountOperation) {
		List<AccountOperationMarketFact> lstSettlementMarketFactAccounts = new ArrayList<AccountOperationMarketFact>();
		if(accountMarketfacts!=null){
			for (MarketFactAccountTO marketFactAccountTO : accountMarketfacts) {
				AccountOperationMarketFact settlementAccountMarketfact = new AccountOperationMarketFact();
				settlementAccountMarketfact.setMarketPrice(marketFactAccountTO.getMarketPrice());
				settlementAccountMarketfact.setOperationQuantity(marketFactAccountTO.getQuantity());
				settlementAccountMarketfact.setMarketRate(marketFactAccountTO.getMarketRate());
				settlementAccountMarketfact.setMarketDate(marketFactAccountTO.getMarketDate());
				settlementAccountMarketfact.setHolderAccountOperation(holderAccountOperation);
				lstSettlementMarketFactAccounts.add(settlementAccountMarketfact);
			}
		}
		return lstSettlementMarketFactAccounts;
	}

	/**
	 * Convert to market fact accounts.
	 *
	 * @param accountMarketfacts the account marketfacts
	 * @return the list
	 */
	public static List<MarketFactAccountTO> convertToMarketFactAccounts(List<AccountOperationMarketFact> accountMarketfacts) {
		List<MarketFactAccountTO> lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();
		if(accountMarketfacts!=null){
			for (AccountOperationMarketFact settlementAccountMarketfact : accountMarketfacts) {
				MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
				marketFactAccountTO.setMarketPrice(settlementAccountMarketfact.getMarketPrice());
				marketFactAccountTO.setQuantity(settlementAccountMarketfact.getOperationQuantity());
				marketFactAccountTO.setMarketRate(settlementAccountMarketfact.getMarketRate());
				marketFactAccountTO.setMarketDate(settlementAccountMarketfact.getMarketDate());
				lstMarketFactAccounts.add(marketFactAccountTO);
			}
		}
		return lstMarketFactAccounts;
	}
	
	/**
	 * Convert sett to market fact accounts.
	 *
	 * @param settlementAccountMarketfacts the settlement account marketfacts
	 * @return the list
	 */
	public static List<MarketFactAccountTO> convertSettToMarketFactAccounts(List<SettlementAccountMarketfact> settlementAccountMarketfacts) {
		List<MarketFactAccountTO> lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();
		if(settlementAccountMarketfacts!=null){
			for (SettlementAccountMarketfact settlementAccountMarketfact : settlementAccountMarketfacts) {
				MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
				marketFactAccountTO.setMarketPrice(settlementAccountMarketfact.getMarketPrice());
				marketFactAccountTO.setQuantity(settlementAccountMarketfact.getMarketQuantity());
				marketFactAccountTO.setMarketRate(settlementAccountMarketfact.getMarketRate());
				marketFactAccountTO.setMarketDate(settlementAccountMarketfact.getMarketDate());
				marketFactAccountTO.setChainedQuantity(settlementAccountMarketfact.getMarketQuantity());
				lstMarketFactAccounts.add(marketFactAccountTO);
			}
		}
		return lstMarketFactAccounts;
	}

	/**
	 * Concatenate parameters.
	 *
	 * @param params the params
	 * @return the string
	 */
	public static String concatenateParameters(List<Object> params) {
		StringBuffer sb = new StringBuffer();
		for (Object object : params) {
			if(object instanceof String){
				sb.append((String)object + GeneralConstants.STR_BR);
			}
			if(object instanceof AccountsComponentTO){
				AccountsComponentTO accountsComponentTO = (AccountsComponentTO)object;
				sb.append("Id proc. negocio : " + accountsComponentTO.getIdBusinessProcess() + GeneralConstants.STR_BR);
				sb.append("Id tipo oper. : " + accountsComponentTO.getIdOperationType() + GeneralConstants.STR_BR);
				if(accountsComponentTO.getIdTradeOperation()!=null){
					sb.append("Id operacion negociacion: " + accountsComponentTO.getIdTradeOperation() + GeneralConstants.STR_BR);
				}
				if(accountsComponentTO.getIdSettlementOperation()!=null){
					sb.append("Id operacion liquidacion: " + accountsComponentTO.getIdSettlementOperation() + GeneralConstants.STR_BR);
				}
			}
			
			if(object instanceof HolderAccountBalanceTO){
				HolderAccountBalanceTO holderAccountBalanceTO = (HolderAccountBalanceTO)object;
				sb.append("Id cuenta titular: " + holderAccountBalanceTO.getIdHolderAccount() + GeneralConstants.STR_BR);
				sb.append("Id participante: " +  holderAccountBalanceTO.getIdParticipant() + GeneralConstants.STR_BR);
				sb.append("Clase - clave valor: " +  holderAccountBalanceTO.getIdSecurityCode() + GeneralConstants.STR_BR);
				sb.append("Cantidad: " +  holderAccountBalanceTO.getStockQuantity() + GeneralConstants.STR_BR);
			}
			
			if(object instanceof MarketFactAccountTO){
				MarketFactAccountTO marketFactAccountTO = (MarketFactAccountTO)object;
				sb.append("Fecha HM: " +  marketFactAccountTO.getMarketDate() + GeneralConstants.STR_BR);
				sb.append("Tasa HM: " +  marketFactAccountTO.getMarketRate() + GeneralConstants.STR_BR);
				sb.append("Precio HM: " +  marketFactAccountTO.getMarketPrice() + GeneralConstants.STR_BR);
				sb.append("Cantidad HM: " + marketFactAccountTO.getQuantity() + GeneralConstants.STR_BR);
				sb.append("Cantidad Encadenada HM: " + marketFactAccountTO.getChainedQuantity() + GeneralConstants.STR_BR);
			}
		}
		return sb.toString();
	}
	
}
