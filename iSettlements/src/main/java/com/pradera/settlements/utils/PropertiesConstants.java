package com.pradera.settlements.utils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/09/2013
 */
public class PropertiesConstants {

	
	/**
	 * Instantiates a new properties constants.
	 */
	private PropertiesConstants() {
		super();
	}
	
	/** The Constant ERROR_RECORD_NOT_SELECTED. */
	public static final String ERROR_RECORD_NOT_SELECTED="error.record.not.selected";
	
	/** The Constant MESSAGES_SUCCESFUL. */
	public static final String MESSAGES_SUCCESFUL="messages.succesful";
	
	/** The Constant MESSAGES_SUCCESFUL. */
	public static final String MESSAGES_UNSUCCESFUL="messages.unsuccesful";
	
	/** The Constant MESSAGES_ALERT. */
	public static final String MESSAGES_ALERT="messages.alert";
	
	/** The Constant MSG_CHAINS_SALE_OPERATION_NO_SELECTED. */
	public static final String MSG_CHAINS_SALE_OPERATION_NO_SELECTED= "msg.chains.sale.operation.no.selected";
	
	/** The Constant MSG_CHAINS_PURCHASE_OPERATION_NO_SELECTED. */
	public static final String MSG_CHAINS_PURCHASE_OPERATION_NO_SELECTED= "msg.chains.purchase.operation.no.selected";
	
	/** The Constant MSG_CHAINS_SALE_NP_GREATER. */
	public static final String MSG_CHAINS_SALE_NP_GREATER="msg.chains.sale.np.greater";
	
	/** The Constant MSG_CHAINS_PURCHASE_NP_GREATER. */
	public static final String MSG_CHAINS_PURCHASE_NP_GREATER="msg.chains.purchase.np.greater";

	/** The Constant MSG_CHAINS_ACCOUNTS_NOT_SELECTED. */
	public static final String MSG_CHAINS_ACCOUNTS_NOT_SELECTED = "msg.chains.accounts.not.selected";
	
	/** The Constant MSG_CHAINS_REGISTER_CONFIRM. */
	public static final String MSG_CHAINS_REGISTER_CONFIRM = "msg.chains.register.confirm";
	
	/** The Constant MSG_CHAINS_REGISTER_SUCCESS. */
	public static final String MSG_CHAINS_REGISTER_SUCCESS = "msg.chains.register.success";
	
	/** The Constant MSG_CHAINS_CONFIRM_SUCCESS. */
	public static final String MSG_CHAINS_CONFIRM_SUCCESS = "msg.chains.confirm.success";
	
	/** The Constant MSG_CHAINS_REJECT_SUCCESS. */
	public static final String MSG_CHAINS_REJECT_SUCCESS = "msg.chains.reject.success";
	
	public static final String DOES_NOT_HAVE_SECURITITES ="error.message.security.no.have";
	
	/** The Constant MSG_CHAINS_CANCEL_SUCCESS. */
	public static final String MSG_CHAINS_CANCEL_SUCCESS = "msg.chains.cancel.success";
	
	/** The Constant MSG_CHAINS_GENERATE_CONFIRM. */
	public static final String MSG_CHAINS_GENERATE_CONFIRM = "msg.chains.generate.confirm";
	
	/** The Constant MSG_CHAINS_GENERATE_SUCCESS. */
	public static final String MSG_CHAINS_GENERATE_SUCCESS = "msg.chains.generate.success";
	
	/** The Constant MSG_CHAINS_NO_REGISTERED. */
	public static final String MSG_CHAINS_NO_REGISTERED = "msg.chains.no.registered";
	
	/** The Constant MSG_CHAINS_NO_CONFIRMED. */
	public static final String MSG_CHAINS_NO_CONFIRMED = "msg.chains.no.confirmed";
	
	/** The Constant MSG_CHAINS_OPERATION_CONFIRM. */
	public static final String MSG_CHAINS_OPERATION_CONFIRM = "msg.chains.operation.confirm";
	
	/** The Constant MSG_CHAINS_OPERATION_REJECT. */
	public static final String MSG_CHAINS_OPERATION_REJECT = "msg.chains.operation.reject";
	
	/** The Constant MSG_CHAINS_OPERATION_CANCEL. */
	public static final String MSG_CHAINS_OPERATION_CANCEL = "msg.chains.operation.cancel";
	
	/** The Constant MSG_CHAINS_OPERATION_CONFIRM MULTIPLE. */
	public static final String MSG_CHAINS_OPERATION_CONFIRM_MULTIPLE = "msg.chains.operation.confirm.multiple";
	
	/** The Constant MSG_CHAINS_OPERATION_REJECT_MULTIPLE. */
	public static final String MSG_CHAINS_OPERATION_REJECT_MULTIPLE = "msg.chains.operation.reject.multiple";
	
	/** The Constant MSG_CHAINS_OPERATION_CANCEL_MULTIPLE. */
	public static final String MSG_CHAINS_OPERATION_CANCEL_MULTIPLE = "msg.chains.operation.cancel.multiple";
	
	// settlement
	
	/** The Constant MSG_SETTLEMENT_PROCESS_NET_LABEL. */
	public static final String MSG_SETTLEMENT_PROCESS_NET_LABEL="msg.settlementProcess.netLabel";
	
	/** The Constant MSG_SETTLEMENT_PROCESS_GROSS_LABEL. */
	public static final String MSG_SETTLEMENT_PROCESS_GROSS_LABEL="msg.settlementProcess.grossLabel";
	
	/** The Constant MSG_SETTLEMENT_EXCHANGE_RATE_REQUIRED. */
	public static final String MSG_SETTLEMENT_EXCHANGE_RATE_REQUIRED="msg.settlementProcess.CurrentExchangeRate.required";
	
	/** The Constant MSG_SETTLEMENT_NET_EXECUTION_CONFIRM. */
	public static final String MSG_SETTLEMENT_NET_EXECUTION_CONFIRM= "msg.settlementProcess.confirmation.executeSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_NET_EXECUTION_SUCCESS. */
	public static final String MSG_SETTLEMENT_NET_EXECUTION_SUCCESS= "msg.settlementProcess.success.executeSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_START_CONFIRM. */
	public static final String MSG_SETTLEMENT_START_CONFIRM= "msg.settlementProcess.confirm.startSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_START_SUCCESS. */
	public static final String MSG_SETTLEMENT_START_SUCCESS= "msg.settlementProcess.success.startSettlementProcess";

	/** The Constant MSG_SETTLEMENT_STOP_CONFIRM. */
	public static final String MSG_SETTLEMENT_STOP_CONFIRM= "msg.settlementProcess.confirmation.stopSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_DELETE_CONFIRM. */
	public static final String MSG_SETTLEMENT_DELETE_CONFIRM= "msg.settlementProcess.confirmation.deleteSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_STOP_CONFIRM. */
	public static final String MSG_SETTLEMENT_SENDREPORT_AUTOMATIC = "msg.settlementProcess.sendreport.automatic";
	
	/** The Constant MSG_SETTLEMENT_STOP_SUCCESS. */
	public static final String MSG_SETTLEMENT_STOP_SUCCESS= "msg.settlementProcess.success.stopSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_DELETE_SUCCESS. */
	public static final String MSG_SETTLEMENT_DELETE_SUCCESS= "msg.settlementProcess.success.deleteSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_SUCCESS_SENDER_REPORT. */
	public static final String MSG_SETTLEMENT_SUCCESS_SENDER_REPORT = "msg.settlementProcess.success.sendreport.automatic";
	
	/** The Constant MSG_SETTLEMENT_RESTART_CONFIRM. */
	public static final String MSG_SETTLEMENT_RESTART_CONFIRM= "msg.settlementProcess.confirmation.restartSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_RESTART_SUCCESS. */
	public static final String MSG_SETTLEMENT_RESTART_SUCCESS= "msg.settlementProcess.success.restartSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_CLOSE_CONFIRM. */
	public static final String MSG_SETTLEMENT_CLOSE_CONFIRM= "msg.settlementProcess.confirmation.closeSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_CLOSE_SUCCESS. */
	public static final String MSG_SETTLEMENT_CLOSE_SUCCESS= "msg.settlementProcess.success.closeSettlementProcess";
	
	/** The Constant MSG_SETTLEMENT_INCORRECT_PROCESS. */
	public static final String MSG_SETTLEMENT_INCORRECT_PROCESS="msg.settlementProcess.incorrectProcess";
	
	/** The Constant MSG_SETTLEMENT_NET_NO_PENDING_OPERATIONS. */
	public static final String MSG_SETTLEMENT_NET_NO_PENDING_OPERATIONS="msg.settlementProcess.netEdition.noPendingOperations";
	
	/** The Constant MSG_SETTLEMENT_NET_EDITION_CONFIRM. */
	public static final String MSG_SETTLEMENT_NET_EDITION_CONFIRM="msg.settlementProcess.netEdition.confirm";
	
	/** The Constant MSG_SETTLEMENT_FUNDS_RETIREMENT_AUTOMATIC_CONFIRM. */
	public static final String MSG_SETTLEMENT_FUNDS_RETIREMENT_AUTOMATIC_CONFIRM="msg.settlementProcess.netEdition.retirement.automatic.confirm";
	
	/** The Constant MSG_SETTLEMENT_PROCESS_LIP_CONFIRM. */
	public static final String MSG_SETTLEMENT_PROCESS_LIP_CONFIRM="msg.settlementProcess.procces.lip";
	
	/** The Constant MSG_SETTLEMENT_PROCESS_LIP_SUCCESS. */
	public static final String MSG_SETTLEMENT_PROCESS_LIP_SUCCESS="msg.settlementProcess.procces.lip.sent";
	
	/** The Constant MSG_SETTLEMENT_NET_EDITION_SUCCESS. */
	public static final String MSG_SETTLEMENT_NET_EDITION_SUCCESS="msg.settlementProcess.netEdition.success";
	
	/** The Constant MSG_SETTLEMENT_UNFULFILLMENT_CONFIRM. */
	public static final String MSG_SETTLEMENT_UNFULFILLMENT_CONFIRM="msg.settlementProcess.unfulfillment.confirm";
	
	/** The Constant MSG_SETTLEMENT_UNFULFILLMENT_SUCCESS. */
	public static final String MSG_SETTLEMENT_UNFULFILLMENT_SUCCESS="msg.settlementProcess.unfulfillment.success";
	
	/** The Constant MSG_SETTLEMENT_CHAIN_OPERATION_CONFIRM. */
	public static final String MSG_SETTLEMENT_CHAIN_OPERATION_CONFIRM="msg.settlementProcess.chainOperation.confirm";
	
	/** The Constant ERROR_SELECT_NO_REQUESTS. */
	public static final String ERROR_SELECT_NO_REQUESTS="prepaidextended.settlementDate.msg.error.selectNorequests";
	
	/** The Constant CONFIRMATION_CONFIRM_STTLEMENTREQUESTDATE. */
	public static final String CONFIRMATION_CONFIRM_STTLEMENTREQUESTDATE="prepaidextended.settlementDate.msg.confirmation.confirmSettlementDateRequest";
	
	/** The Constant CONFIRMATION_REJECT_STTLEMENTREQUESTDATE. */
	public static final String CONFIRMATION_REJECT_STTLEMENTREQUESTDATE="prepaidextended.settlementDate.msg.confirmation.rejectSettlementDateRequest";
	
	/** The Constant ERROR_CONFIRM_STTLEMENTREQUESTDATE_ALREADY_CONFIRMED. */
	public static final String ERROR_CONFIRM_STTLEMENTREQUESTDATE_ALREADY_CONFIRMED="prepaidextended.settlementDate.msg.error.confirmSettlementDateRequest.alreadyConfirmed";
	
	/** The Constant ERROR_CONFIRM_STTLEMENTREQUESTDATE_ALREADY_UPDATED. */
	public static final String ERROR_CONFIRM_STTLEMENTREQUESTDATE_ALREADY_UPDATED="prepaidextended.settlementDate.msg.error.confirmSettlementDateRequest.alreadyUpdated";
	
	/** The Constant ERROR_REJECT_STTLEMENTREQUESTDATE_ALREADY_REJECTED. */
	public static final String ERROR_REJECT_STTLEMENTREQUESTDATE_ALREADY_REJECTED="prepaidextended.settlementDate.msg.error.rejectSettlementDateRequest.alreadyRejected";
	
	/** The Constant ERROR_REJECT_STTLEMENTREQUESTDATE_ALREADY_UPDATED. */
	public static final String ERROR_REJECT_STTLEMENTREQUESTDATE_ALREADY_UPDATED="prepaidextended.settlementDate.msg.error.rejectSettlementDateRequest.alreadyUpdated";
	
	/** The Constant SECCESS_CONFIRM_STTLEMENTREQUESTDATE. */
	public static final String SECCESS_CONFIRM_STTLEMENTREQUESTDATE="prepaidextended.settlementDate.msg.success.confirmSettlementDateRequest";
	
	/** The Constant SECCESS_REJECT_STTLEMENTREQUESTDATE. */
	public static final String SECCESS_REJECT_STTLEMENTREQUESTDATE="prepaidextended.settlementDate.msg.success.rejectSettlementDateRequest";
	
	/** The Constant ERROR_SETTLEMENT_DATE_EXCEEDED. */
	public static final String ERROR_SETTLEMENT_DATE_EXCEEDED="prepaidextended.settlementDate.msg.error.settlementDate.exceeded";
	
	/** The Constant ERROR_SETTLEMENT_DATE_SAME_AS_TODAY. */
	public static final String ERROR_SETTLEMENT_DATE_SAME_AS_TODAY="prepaidextended.settlementDate.msg.error.settlementDate.sameAsToday";
	
	/** The Constant CONFIRMATION_REGISTER_STTLEMENTREQUESTDATE. */
	public static final String CONFIRMATION_REGISTER_STTLEMENTREQUESTDATE="prepaidextended.settlementDate.msg.confirmation.registerSettlementDateRequest";
	
	/** The Constant SUCCESS_REGISTER_STTLEMENTREQUESTDATE. */
	public static final String SUCCESS_REGISTER_STTLEMENTREQUESTDATE="prepaidextended.settlementDate.msg.success.registerSettlementDateRequest";
	
	/** The Constant REQUIRED_NEW_SETTLEMENT_DATE. */
	public static final String REQUIRED_NEW_SETTLEMENT_DATE="prepaidextended.settlementDate.msg.error.requiredNewDate";
	
	/** The Constant SELECT_ATLEAST_ONE_OPERATION. */
	public static final String SELECT_ATLEAST_ONE_OPERATION="prepaidextended.settlementDate.msg.error.selectAtleastOneOperation";
	
	/** The Constant INVALID_DATE_SELECTED. */
	public static final String INVALID_DATE_SELECTED="prepaidextended.settlementDate.msg.error.invalidDate";
	
	/** The Constant NON_WORKING_DAY_SELECTED. */
	public static final String NON_WORKING_DAY_SELECTED="prepaidextended.settlementDate.msg.error.nonWorkingDay";
	
	/** The Constant PENDING. */
	public static final String PENDING ="monitor.settlementProcess.lbl.Pending";
	
	/** The Constant SETTLED. */
	public static final String SETTLED ="monitor.settlementProcess.lbl.Settled";
	
	/** The Constant CANCELED. */
	public static final String CANCELED ="monitor.settlementProcess.lbl.Cancelled";
	
	//MESSAGES FOR REPO SETTLEMENT REQUEST TERM
	
	/** The Constant OTC_OPERATION_VALIDATION_LENGHT_EFECTIVETAZA. */
	public static final String REPO_OPERATION_VALIDATION_LENGHT_EFECTIVETAZA="repo.operation.validation.lengt.efectivetaza";
	
	/** The Constant REPO_OPERATION_VALIDATION_SETTLEMENT_DAYS. */
	public static final String REPO_OPERATION_VALIDATION_SETTLEMENT_DAYS="repo.operation.validation.settlement.days";
	
	/** The Constant REPO_OPERATION_VALIDATION_REQUEST_EXIST. */
	public static final String REPO_OPERATION_VALIDATION_REQUEST_EXIST="repo.operation.validation.request.exist";
	
	/** The Constant REPO_OPERATION_VALIDATION_TERMPLAZE_EXIST. */
	public static final String REPO_OPERATION_VALIDATION_TERMPLAZE_EXIST="repo.operation.validation.termplaze.exist";
	
	/** The Constant REPO_OPERATION_VALIDATION_MODALITY_INCONSISTENT. */
	public static final String REPO_OPERATION_VALIDATION_MODALITY_INCONSISTENT="repo.operation.validation.modality.incosistent";
	
	/** The Constant REPO_OPERATION_VALIDATION_SETTLEMENT_TERM. */
	public static final String REPO_OPERATION_VALIDATION_SETTLEMENT_TERM="repo.operation.validation.settlement.term";
	
	/** The Constant REPO_OPERATION_VALIDATION_DATE_BEFORE. */
	public static final String REPO_OPERATION_VALIDATION_DATE_BEFORE="repo.operation.validation.date.before";
	
	/** The Constant REPO_OPERATION_VALIDATION_TERMAMOUNT_CASHAMOUNT. */
	public static final String REPO_OPERATION_VALIDATION_TERMAMOUNT_CASHAMOUNT="repo.operation.validation.termamount.chasamount";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_REGISTRY. */
	public static final String REPO_OPERATION_SETTLEMENT_REGISTRY="repo.operation.settlement.registry";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_REGISTRY_OK. */
	public static final String REPO_OPERATION_SETTLEMENT_REGISTRY_OK="repo.operation.settlement.registry.ok";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_APPROVE. */
	public static final String REPO_OPERATION_SETTLEMENT_APPROVE="repo.operation.settlement.approve";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_APPROVE_OK. */
	public static final String REPO_OPERATION_SETTLEMENT_APPROVE_OK="repo.operation.settlement.approve.ok";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_ANNULATE. */
	public static final String REPO_OPERATION_SETTLEMENT_ANNULATE="repo.operation.settlement.annulate";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_ANNULATE_OK. */
	public static final String REPO_OPERATION_SETTLEMENT_ANNULATE_OK="repo.operation.settlement.annulate.ok";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_REVIEW. */
	public static final String REPO_OPERATION_SETTLEMENT_REVIEW="repo.operation.settlement.review";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_REVIEW_OK. */
	public static final String REPO_OPERATION_SETTLEMENT_REVIEW_OK="repo.operation.settlement.review.ok";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_REJECT. */
	public static final String REPO_OPERATION_SETTLEMENT_REJECT="repo.operation.settlement.reject";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_REJECT_OK. */
	public static final String REPO_OPERATION_SETTLEMENT_REJECT_OK="repo.operation.settlement.reject.ok";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_CONFIRM. */
	public static final String REPO_OPERATION_SETTLEMENT_CONFIRM="repo.operation.settlement.confirm";
	
	/** The Constant REPO_OPERATION_SETTLEMENT_CONFIRM_OK. */
	public static final String REPO_OPERATION_SETTLEMENT_CONFIRM_OK="repo.operation.settlement.confirm.ok";
	
	/** The Constant REPO_OPERATION_VALIDATION_STATE_INCORRECT. */
	public static final String REPO_OPERATION_VALIDATION_STATE_INCORRECT="repo.operation.validation.state.incorrect";
	
	/** The Constant REPO_OPERATION_VALIDATION_EXPIRATION_DAYS. */
	public static final String REPO_OPERATION_VALIDATION_EXPIRATION_DAYS = "repo.operation.validation.expiration.days";
	
	/** The Constant CONFIRM_REGISTER_SANCTION_PENALTY. */
	public static final String CONFIRM_REGISTER_SANCTION_PENALTY="confirm.register.sanction.penalty";
	
	/** The Constant SUCCESS_REGISTER_SANCTION_PENALTY. */
	public static final String SUCCESS_REGISTER_SANCTION_PENALTY="success.register.sanction.penalty";
	
	/** The Constant CONFIRM_MODIFY_SANCTION_PENALTY. */
	public static final String CONFIRM_MODIFY_SANCTION_PENALTY="confirm.modify.sanction.penalty";
	
	/** The Constant SUCCESS_MODIFY_SANCTION_PENALTY. */
	public static final String SUCCESS_MODIFY_SANCTION_PENALTY="success.modify.sanction.penalty";
	
	/** The Constant CONFIRM_CONFIRM_SANCTION_PENALTY. */
	public static final String CONFIRM_CONFIRM_SANCTION_PENALTY="confirm.confirm.sanction.penalty";
	
	/** The Constant SUCCESS_CONFIRM_SANCTION_PENALTY. */
	public static final String SUCCESS_CONFIRM_SANCTION_PENALTY="success.confirm.sanction.penalty";
	
	/** The Constant CONFIRM_DELETE_SANCTION_PENALTY. */
	public static final String CONFIRM_DELETE_SANCTION_PENALTY="confirm.delete.sanction.penalty";
	
	/** The Constant SUCCESS_DELETE_SANCTION_PENALTY. */
	public static final String SUCCESS_DELETE_SANCTION_PENALTY="success.delete.sanction.penalty";
	
	/** The Constant CONFIRM_APPEAL_SANCTION_PENALTY. */
	public static final String CONFIRM_APPEAL_SANCTION_PENALTY="confirm.appeal.sanction.penalty";
	
	/** The Constant SUCCESS_APPEAL_SANCTION_PENALTY. */
	public static final String SUCCESS_APPEAL_SANCTION_PENALTY="success.appeal.sanction.penalty";
	
	/** The Constant WARNING_OPERATION_MODIFY_INVALID. */
	public static final String WARNING_OPERATION_MODIFY_INVALID="warning.operation.modify.invalid";
	
	/** The Constant WARNING_OPERATION_APPEAL_INVALID. */
	public static final String WARNING_OPERATION_APPEAL_INVALID="warning.operation.appeal.invalid";
	
	/** The Constant MSG_SANCTION_OPERATION_REQUIRED. */
	public static final String MSG_SANCTION_OPERATION_REQUIRED="msg.sanction.mechanism.operation.required";
	
	/** The Constant MSG_SANCTION_SANCTION_MOTIVE_REQUIRED. */
	public static final String MSG_SANCTION_SANCTION_MOTIVE_REQUIRED="msg.sanction.sanction.motive.required";
	
	/** The Constant MSG_SANCTION_SANCTION_MOTIVE_REPEAT. */
	public static final String MSG_SANCTION_SANCTION_MOTIVE_REPEAT="msg.sanction.sanction.motive.not.create";
	
	/** The Constant MSG_SANCTION_SANCTION_AMOUNT_FAILED. */
	public static final String MSG_SANCTION_SANCTION_AMOUNT_FAILED="msg.sanction.sanction.amount.failed";
	
	
	/** The Constant MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REGISTER_CONFIRM. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REGISTER_CONFIRM="msg.currency.settlement.request.reversion.register.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST APPROVE CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_APPROVE_CONFIRM="msg.currency.settlement.request.reversion.approve.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST ANNUL CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_ANNUL_CONFIRM="msg.currency.settlement.request.reversion.annul.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REVIEW CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REVIEW_CONFIRM="msg.currency.settlement.request.reversion.review.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST CONFIRM CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_CONFIRM_CONFIRM="msg.currency.settlement.request.reversion.confirm.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REJECT CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REJECT_CONFIRM="msg.currency.settlement.request.reversion.reject.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST AUTHORIZE CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_AUTHORIZE_CONFIRM="msg.currency.settlement.request.reversion.authorize.confirm";
	
	
	/** The Constant MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REGISTER_SUCCESS. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REGISTER_SUCCESS="msg.currency.settlement.request.reversion.register.success";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST APPROVE OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_APPROVE_SUCCESS="msg.currency.settlement.request.reversion.approve.success";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST ANNUL OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_ANNUL_SUCCESS="msg.currency.settlement.request.reversion.annul.success";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REVIEW OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REVIEW_SUCCESS="msg.currency.settlement.request.reversion.review.success";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST CONFIRM OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_CONFIRM_SUCCESS="msg.currency.settlement.request.reversion.confirm.success";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REJECT OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REJECT_SUCCESS="msg.currency.settlement.request.reversion.reject.success";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST AUTHORIZE OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_AUTHORIZE_SUCCESS="msg.currency.settlement.request.reversion.authorize.success";
	
	
	/**  The Constant MESSAGE CODE VALUE NOT EXIST. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_SECURITY_NOT_EXIST="mcn.msg.cordigoValor.NotExist";
	
	/** The Constant MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_SECURITY_NOT_REGISTER. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_SECURITY_NOT_REGISTER="currency.settlement.security.not.register";
	
	/**  The Constant MESSAGE MECHANISM OPERATION NOT FOUND. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_MECHANISM_OPERATION_NOT_FOUND="currency.settlement.msg.mechanism.operation.not.found";
	
	/** The Constant MSG CURRENCY SETTLEMENT REQUEST OPERATIONS NOT SELECTED. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_OPERATION_NOT_SELECTED="msg.currency.settlement.request.operations.not.selected";
	
	/** The Constant MSG_CURRENCY_SETTLEMENT_REQUEST_EXISTS. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_EXISTS="msg.currency.settlement.request.exists";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST VALIDATION REGISTERED . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_STATE = "msg.currency.settlement.request.validation.state";
	
	/** The Constant MSG_CURRENCY_SETTLEMENT_REQUEST_REGISTER. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REGISTER="msg.currency.settlement.request.register.confirm";
	
	/** The Constant MSG_CURRENCY_SETTLEMENT_REQUEST_REGISTER_OK. */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REGISTER_OK="msg.currency.settlement.request.register.ok";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST APPROVE CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_APPROVE_CONFIRM="msg.currency.settlement.request.approve.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST ANNUL CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_ANNUL_CONFIRM="msg.currency.settlement.request.annul.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REVIEW CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVIEW_CONFIRM="msg.currency.settlement.request.review.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST CONFIRM CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_CONFIRM_CONFIRM="msg.currency.settlement.request.confirm.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REJECT CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REJECT_CONFIRM="msg.currency.settlement.request.reject.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST AUTHORIZE CONFIRM . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_AUTHORIZE_CONFIRM="msg.currency.settlement.request.authorize.confirm";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST APPROVE OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_APPROVE_OK="msg.currency.settlement.request.approve.ok";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST ANNUL OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_ANNUL_OK="msg.currency.settlement.request.annul.ok";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REVIEW OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REVIEW_OK="msg.currency.settlement.request.review.ok";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST CONFIRM OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_CONFIRM_OK="msg.currency.settlement.request.confirm.ok";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST REJECT OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_REJECT_OK="msg.currency.settlement.request.reject.ok";
	
	/** The Constant CURRENCY SETTLEMENT REQUEST AUTHORIZE OK . */
	public static final String MSG_CURRENCY_SETTLEMENT_REQUEST_AUTHORIZE_OK="msg.currency.settlement.request.authorize.ok";
	//FINAL -- MESSAGES FOR CURRENCY SETTLEMENT
	

	/** The Constant HEADER_MESSAGE_ALERT. */
	public static final String HEADER_MESSAGE_ALERT = "header.message.alert";
	
	/** Holds Message Validate punishment factor value. */
	public static final String MSG_GVC_PUNISHMENT_FACTOR_ERROR = "msg.gvc.punishment.factor.error";

	/** The Constant MSG_GVC_PUNISHMENT_FACTOR_MODIFY_CONF. */
	public static final String MSG_GVC_PUNISHMENT_FACTOR_MODIFY_CONF = "msg.gvc.punishment.factor.modify.conf";
	
	/** The Constant MSG_GVC_PUNISHMENT_FACTOR_UPDATE_CONF. */
	public static final String MSG_GVC_PUNISHMENT_FACTOR_UPDATE_CONF = "msg.gvc.punishment.factor.update.conf";
	
	/** The Constant MSG_GVC_DELETE_SECURITY_GROUP_CONF. */
	public static final String MSG_GVC_DELETE_SECURITY_GROUP_CONF = "msg.gvc.delete.security.group.conf";
	
	/** The Constant MSG_GVC_DUPLICATE_SECURITY_INSERTED. */
	public static final String MSG_GVC_DUPLICATE_SECURITY_INSERTED = "msg.gvc.duplicate.security.insert";		

	/** The Constant MSG_GVC_LOAD_EXISTING_GROUP_CONFIRM. */
	public static final String MSG_GVC_LOAD_EXISTING_GROUP_CONFIRM = "msg.gvc.group.exists.confirm";
	
	/** The Constant MSG_GVC_SAVE_CONFIRM. */
	public static final String MSG_GVC_SAVE_CONFIRM = "msg.gvc.save.confirm";

	/** The Constant MSG_GVC_SECURITY_LIST_EMPTY. */
	public static final String MSG_GVC_SECURITY_LIST_EMPTY = "msg.gvc.security.list.empty";

	/** The Constant MSG_GVC_SECURITY_NOT_REGISTERED. */
	public static final String MSG_GVC_SECURITY_NOT_REGISTERED = "msg.gvc.security.not.registered";
	
	/** The Constant MSG_GVC_ISSUANCE_NOT_REGISTERED. */
	public static final String MSG_GVC_ISSUANCE_NOT_REGISTERED = "msg.gvc.issuance.not.registered";

	/** The Constant MSG_GVC_SECURITY_ALREADY_GROUP. */
	public static final String MSG_GVC_SECURITY_ALREADY_GROUP = "msg.gvc.security.already.group";

	/** The Constant MSG_GVC_INSTRUMENT_TYPE_NOT_MODALITY. */
	public static final String MSG_GVC_INSTRUMENT_TYPE_NOT_MODALITY = "msg.gvc.instrumenttype.not.modality";

	/** The Constant MSG_GVC_SECURITY_CLASS_NOT_GROUP. */
	public static final String MSG_GVC_SECURITY_CLASS_NOT_GROUP = "msg.gvc.securityclass.not.group.class";
	
	/** The Constant MSG_GVC_PUNISHMENT_FACTOR_UPDATE_SUCCESS. */
	public static final String MSG_GVC_PUNISHMENT_FACTOR_UPDATE_SUCCESS = "msg.gvc.punishment.factor.update.succ";
	
	/** The Constant MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_REMOVE. */
	public static final String MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_REMOVE = "msg.gvc.punishment.factor.update.succ.remove";
	
	/** The Constant MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_INSERT. */
	public static final String MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_INSERT = "msg.gvc.punishment.factor.update.succ.insert";
	
	/** The Constant MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_ALL. */
	public static final String MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_ALL = "msg.gvc.punishment.factor.update.succ.all";
	
	// guarantees registration
	
	/** The Constant MSG_GUARANTEES_CUMULATIVE_GREATER_AVAILABLE_BAL. */
	public static final String MSG_GUARANTEES_CUMULATIVE_GREATER_AVAILABLE_BAL = "msg.guarantees.cumulative.greater.available.bal";
	
	/** The Constant MSG_GUARANTEES_CUMULATIVE_GREATER_AVAILABLE_AMO. */
	public static final String MSG_GUARANTEES_CUMULATIVE_GREATER_AVAILABLE_AMO = "msg.guarantees.cumulative.greater.available.amount";
	
	/** The Constant MSG_GUARANTEES_AMOUNT_SUM_ZERO. */
	public static final String MSG_GUARANTEES_AMOUNT_SUM_ZERO = "msg.guarantees.amount.sum.zero";
	
	/** The Constant MSG_GUARANTEES_AMOUNT_WITHDRAW_INVALID. */
	public static final String MSG_GUARANTEES_AMOUNT_WITHDRAW_INVALID = "msg.guarantees.amount.withdraw.invalid";
	
	/** The Constant MSG_GUARANTEES_REGISTER_INITIAL_CONFIRM. */
	public static final String MSG_GUARANTEES_REGISTER_INITIAL_CONFIRM = "msg.guarantees.register.initial.confirm";
	
	/** The Constant MSG_GUARANTEES_REGISTER_INITIAL_SUCESS. */
	public static final String MSG_GUARANTEES_REGISTER_INITIAL_SUCESS = "msg.guarantees.register.initial.success";
	
	/** The Constant MSG_GUARANTEES_REGISTER_RECOVER_CONFIRM. */
	public static final String MSG_GUARANTEES_REGISTER_RECOVER_CONFIRM = "msg.guarantees.register.recover.confirm";
	
	/** The Constant MSG_GUARANTEES_REGISTER_RECOVER_SUCCESS. */
	public static final String MSG_GUARANTEES_REGISTER_RECOVER_SUCCESS = "msg.guarantees.register.recover.success";
	
	/** The Constant MSG_GUARANTEES_REGISTER_ADITIONAL_CONFIRM. */
	public static final String MSG_GUARANTEES_REGISTER_ADITIONAL_CONFIRM = "msg.guarantees.register.aditional.confirm";
	
	/** The Constant MSG_GUARANTEES_REGISTER_ADITIONAL_SUCCESS. */
	public static final String MSG_GUARANTEES_REGISTER_ADITIONAL_SUCCESS = "msg.guarantees.register.aditional.success";

	/** The Constant MSG_GUARANTEES_REGISTER_WITHDRAWAL_CONFIRM. */
	public static final String MSG_GUARANTEES_REGISTER_WITHDRAWAL_CONFIRM = "msg.guarantees.register.withdrawal.confirm";
	
	/** The Constant MSG_GUARANTEES_REGISTER_WITHDRAWAL_SUCCESS. */
	public static final String MSG_GUARANTEES_REGISTER_WITHDRAWAL_SUCCESS = "msg.guarantees.register.withdrawal.success";
	
	/** The Constant MSG_GUARANTEES_NO_FUNDS_NOR_SECURITIES_BALANCES. */
	public static final String MSG_GUARANTEES_NO_FUNDS_NOR_SECURITIES_BALANCES = "msg.guarantees.register.no.funds.or.securities.balances";
	
	/** The Constant MSG_GUARANTEES_VALID_MARKETFACT. */
	public static final String MSG_GUARANTEES_VALID_MARKETFACT = "msg.guarantees.valid.marketfact";
	
	public static final String MSG_SIRTEX_SPLIT_REPORTED_VALIDATION = "msg.sirtex.split.reported.validation";
	
	public static final String MSG_CUI_IS_NOT_ENABLED_TO_NEGOTIATE_SIRTEX="msg.cui.is.not.enabled.to.negotiate.sirtex";	
	
	/** The Constant CONFIRM_HEADER_RECORD. */
	public static final String CONFIRM_HEADER_RECORD = "lbl.confirm.header.record";
	
	// negotiation operation registration
	
	/** The Constant MCN_MASSIVE_REGISTER_CONFIRM. */
	public static final String MCN_MASSIVE_REGISTER_CONFIRM = "mcn.msg.massive.register.confirm";	
	
	/** The Constant MCN_MASSIVE_REGISTER_SUCCESS. */
	public static final String MCN_MASSIVE_REGISTER_SUCCESS = "mcn.msg.massive.register.success";
	
	/** The Constant MCN_MANUAL_REGISTER_SUCCESS. */
	public static final String MCN_MANUAL_REGISTER_SUCCESS = "mcn.msg.register.success";
	
	/** The Constant MCN_MASSIVE_ERROR_DOWNLOADING_FILE. */
	public static final String MCN_MASSIVE_ERROR_DOWNLOADING_FILE="mcn.msg.massive.error.downloading.file";
	
	/** The Constant MCN_MASSIVE_EXCHANGE_RATE_NOT_FOUND. */
	public static final String MCN_MASSIVE_EXCHANGE_RATE_NOT_FOUND="mcn.msg.massive.exchange.rate.not.found";
	
	/** The Constant MCN_MASSIVE_OPER_NUMBER_EXISTS. */
	public static final String MCN_MASSIVE_OPER_NUMBER_EXISTS="mcn.msg.massive.operation.number.exists";
	
	/** The Constant MCN_MASSIVE_OPER_NUMBER_EXISTS. */
	public static final String MCN_MASSIVE_OPER_BALLOT_SEQ_EXISTS="mcn.msg.massive.operation.ballot.seq.exists";
	
	/** The Constant MCN_MASSIVE_OPER_FIS_NOT_VALID. */
	public static final String MCN_MASSIVE_OPER_FIS_NOT_VALID="mcn.msg.massive.operation.invalid.fis";
	
	/** The Constant MCN_MASSIVE_OPER_NUMBER_INVALID. */
	public static final String MCN_MASSIVE_OPER_NUMBER_INVALID="mcn.msg.massive.operation.invalid.referenceNumber";
	
	/** The Constant MCN_MASSIVE_CURRENCY_NOT_FOUND. */
	public static final String MCN_MASSIVE_CURRENCY_NOT_FOUND="mcn.msg.massive.currency.not.found";
	
	/** The Constant MCN_MASSIVE_CURRENCY_NOT_FOUND. */
	public static final String MCN_MASSIVE_SECURITY_CLASS_NOT_FOUND="mcn.msg.massive.securityclass.not.found";
	
	/** The Constant MCN_MASSIVE_CURRENCY_NOT_FOUND. */
	public static final String MCN_MASSIVE_SECURITY_CLASS_NOT_VALID="mcn.msg.massive.securityclass.not.valid";
	
	/** The Constant MCN_MASSIVE_MODALITY_NOT_PERMITTED. */
	public static final String MCN_MASSIVE_MODALITY_NOT_PERMITTED="mcn.msg.massive.modality.not.permitted";
	
	/** The Constant MCN_MASSIVE_MODALITY_CURRENCY_DIFFERS. */
	public static final String MCN_MASSIVE_MODALITY_CURRENCY_DIFFERS="mcn.msg.massive.modality.currency.differs";
	
	public static final String MCN_MASSIVE_MODALITY_SELL_FLAG_INCORRECT="mcn.msg.massive.sell.flag.incorrect";
	
	/** The Constant MCN_MASSIVE_TRANS_DATE_INVALID. */
	public static final String MCN_MASSIVE_TRANS_DATE_INVALID="mcn.msg.massive.transaction.date.invalid";
	
	/** The Constant MCN_MASSIVE_TRANS_DATE_NW_DAY. */
	public static final String MCN_MASSIVE_TRANS_DATE_NW_DAY = "mcn.msg.massive.transaction.date.not.working.day";
	
	/** The Constant MCN_MASSIVE_SETTLE_DATE_GREATER_TODAY. */
	public static final String MCN_MASSIVE_SETTLE_DATE_GREATER_TODAY="mcn.msg.massive.settlement.date.greater.than.today";
	
	/** The Constant MCN_MASSIVE_SETTLE_DATE_NW_DAY. */
	public static final String MCN_MASSIVE_SETTLE_DATE_NW_DAY="mcn.msg.massive.settlement.date.not.working.day";
	
	/** The Constant MCN_MASSIVE_SETTLE_DATE_EXPIRATION. */
	public static final String MCN_MASSIVE_SETTLE_DATE_EXPIRATION ="mcn.msg.massive.settlement.date.gt.expiration";
	
	/** The Constant MCN_MASSIVE_SECURITY_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_SECURITY_NOT_REGISTERED="mcn.msg.massive.security.not.registered";
	
	/** The Constant MCN_MASSIVE_SECURITY_NOT_FOUND. */
	public static final String MCN_MASSIVE_SECURITY_NOT_FOUND="mcn.msg.massive.security.not.found";
	
	/** The Constant MCN_MASSIVE_SECURITY_NOT_IN_MODALITY. */
	public static final String MCN_MASSIVE_SECURITY_NOT_IN_MODALITY="mcn.msg.massive.security.not.in.modality";
	
	/** The Constant MCN_MASSIVE_SECURITY_NO_ISSUER_ACCOUNT. */
	public static final String MCN_MASSIVE_SECURITY_NO_ISSUER_ACCOUNT="mcn.msg.massive.security.not.issuer.account";
	
	/** The Constant MCN_MASSIVE_SECURITY_ISSUER_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_SECURITY_ISSUER_NOT_REGISTERED="mcn.msg.massive.security.issuer.not.registered";
	
	/** The Constant MCN_MASSIVE_SECURITY_ISSUANCE_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_SECURITY_ISSUANCE_NOT_REGISTERED="mcn.msg.massive.security.issuance.not.registered";
	
	/** The Constant MCN_MASSIVE_SECURITY_INS_TYPE_NOT_VALID. */
	public static final String MCN_MASSIVE_SECURITY_INS_TYPE_NOT_VALID="mcn.msg.massive.security.instype.not.valid";
	
	/** The Constant MCN_MASSIVE_SECURITY_ISSUER_ACC_NOT_FOUND. */
	public static final String MCN_MASSIVE_SECURITY_ISSUER_ACC_NOT_FOUND="mcn.msg.massive.security.issuer.acc.not.found";
	
	/** The Constant MCN_MASSIVE_SECURITY_CURRENCY_DIFF_ISSUANCE. */
	public static final String MCN_MASSIVE_SECURITY_CURRENCY_DIFF_ISSUANCE="mcn.msg.massive.security.currency.diff.issuance";
	
	/** The Constant MCN_MASSIVE_SECURITY_CURRENCY_DIFF_OP. */
	public static final String MCN_MASSIVE_SECURITY_CURRENCY_DIFF_OP="mcn.msg.massive.security.currency.diff.op";
	
	/** The Constant MCN_MASSIVE_SELL_ACCOUNT_NOT_FOUND. */
	public static final String MCN_MASSIVE_SELL_ACCOUNT_NOT_FOUND="mcn.msg.massive.sell.account.not.found";
	
	/** The Constant MCN_MASSIVE_SELL_ACCOUNT_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_SELL_ACCOUNT_NOT_REGISTERED="mcn.msg.massive.sell.account.not.registered";
	
	/** The Constant MCN_MASSIVE_SELL_ACCOUNT_HOLDER_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_SELL_ACCOUNT_HOLDER_NOT_REGISTERED="mcn.msg.massive.sell.account.holder.not.registered";
	
	/** The Constant MCN_MASSIVE_SELL_ACCOUNT_NOT_ISSUER. */
	public static final String MCN_MASSIVE_SELL_ACCOUNT_NOT_ISSUER="mcn.msg.massive.sell.account.not.issuer";
	
	/** The Constant MCN_MASSIVE_SELL_NEEDS_PART_AND_ACCOUNT. */
	public static final String MCN_MASSIVE_SELL_NEEDS_PART_AND_ACCOUNT="mcn.msg.massive.sell.participant.and.account";
	
	/** The Constant MCN_MASSIVE_SELL_ACCOUNT_NOT_IN_PART. */
	public static final String MCN_MASSIVE_SELL_ACCOUNT_NOT_IN_PART="mcn.msg.massive.sell.account.not.in.participant";
	
	/** The Constant MCN_MASSIVE_BUY_ACCOUNT_NOT_FOUND. */
	public static final String MCN_MASSIVE_BUY_ACCOUNT_NOT_FOUND="mcn.msg.massive.buy.account.not.found";
	
	/** The Constant MCN_MASSIVE_BUY_ACCOUNT_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_BUY_ACCOUNT_NOT_REGISTERED="mcn.msg.massive.buy.account.not.registered";
	
	/** The Constant MCN_MASSIVE_BUY_ACCOUNT_HOLDER_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_BUY_ACCOUNT_HOLDER_NOT_REGISTERED="mcn.msg.massive.buy.account.holder.not.registered";
	
	/** The Constant MCN_MASSIVE_BUY_ACCOUNT_NOT_NULL. */
	public static final String MCN_MASSIVE_BUY_ACCOUNT_NOT_NULL="mcn.msg.massive.buy.account.not.null";
	
	/** The Constant MCN_MASSIVE_SELL_ACCOUNT_NOT_NULL. */
	public static final String MCN_MASSIVE_SELL_ACCOUNT_NOT_NULL="mcn.msg.massive.sell.account.not.null";
	
	/** The Constant MCN_MASSIVE_BUY_ACCOUNT_NOT_IN_PART. */
	public static final String MCN_MASSIVE_BUY_ACCOUNT_NOT_IN_PART="mcn.msg.massive.buy.account.not.in.participant";
	
	/** The Constant MCN_MASSIVE_BUY_ACCOUNT_NEEDED_CROSSED. */
	public static final String MCN_MASSIVE_BUY_ACCOUNT_NEEDED_CROSSED="mcn.msg.massive.buy.account.needed.in.crossed";
	
	/** The Constant MCN_MASSIVE_BUY_ACCOUNT_NEEDED_REVISION. */
	public static final String MCN_MASSIVE_BUY_ACCOUNT_NEEDED_REVISION="mcn.msg.massive.buy.account.needed.in.revision";
	
	/** The Constant MCN_MASSIVE_SECURITY_CODE_NOT_OPERATION. */
	public static final String MCN_MASSIVE_SECURITY_CODE_NOT_OPERATION="mcn.msg.massive.security.not.in.operation";
	
	/** The Constant MCN_MASSIVE_PARTICIPANT_NOT_FOUND. */
	public static final String MCN_MASSIVE_PARTICIPANT_NOT_FOUND="mcn.msg.massive.participant.not.found";
	
	public static final String MCN_MASSIVE_PARTICIPANT_NON_UNIQUE_RESULT="mcn.msg.massive.participant.non.unique.result";
	
	/** The Constant MCN_MASSIVE_PARTICIPANT_STRUCT_NOT_FOUND. */
	public static final String MCN_MASSIVE_PARTICIPANT_STRUCT_NOT_FOUND="mcn.msg.massive.participant.struct.not.found";
	
	/** The Constant MCN_MASSIVE_PARTICIPANT_NOT_REGISTERED. */
	public static final String MCN_MASSIVE_PARTICIPANT_NOT_REGISTERED="mcn.msg.massive.participant.not.registered";
	
	/** The Constant MCN_MASSIVE_PARTICIPANT_NOT_IN_MODALITY. */
	public static final String MCN_MASSIVE_PARTICIPANT_NOT_IN_MODALITY="mcn.msg.massive.participant.not.in.modality";
	
	/** The Constant MCN_MASSIVE_PARTICIPANT_OTC_NOT_VALID. */
	public static final String MCN_MASSIVE_PARTICIPANT_OTC_NOT_VALID="mcn.msg.massive.participant.otc.not.valid";
	
	/** The Constant MCN_MASSIVE_AMOUNT_LARGER_THAN_PLACEMENT. */
	public static final String MCN_MASSIVE_AMOUNT_LARGER_THAN_PLACEMENT="mcn.msg.massive.amount.larger.than.placement";
	
	/** The Constant MCN_MASSIVE_RATE_LARGER_THAN_PERMITTED. */
	public static final String MCN_MASSIVE_RATE_LARGER_THAN_PERMITTED="mcn.msg.massive.rate.larger.than.permitted";

	/** The Constant MCN_MASSIVE_RATE_LARGER_THAN_PERMITTED. */
	public static final String MCN_MASSIVE_REAL_CASH_PRICE_LARGER_THAN_PERMITTED="mcn.msg.massive.real.cash.price.larger.than.permitted";
	
	/** The Constant MCN_MASSIVE_INVALID_TERM_PRICE. */
	public static final String MCN_MASSIVE_INVALID_TERM_PRICE = "mcn.msg.massive.invalid.term.price";
	
	/** The Constant MCN_MASSIVE_TERM_AMOUNT_GREATER_THAN_CASH. */
	public static final String MCN_MASSIVE_TERM_AMOUNT_GREATER_THAN_CASH = "mcn.msg.massive.term.amount.greater.than.cash";

	/** The Constant MCN_MASSIVE_OPERATION_TYPE_NOT_FOUND. */
	public static final String MCN_MASSIVE_OPERATION_TYPE_NOT_FOUND = "mcn.msg.massive.operation.type.notfound";
	
	/** The Constant MCN_MASSIVE_OPERATION_CLASS_NOT_FOUND. */
	public static final String MCN_MASSIVE_OPERATION_CLASS_NOT_FOUND = "mcn.msg.massive.operation.class.notfound";

	/** The Constant MCN_MASSIVE_OPERATION_ASSIGN_NOT_FOUND. */
	public static final String MCN_MASSIVE_OPERATION_ASSIGN_NOT_FOUND = "mcn.msg.massive.assign.operation.notfound";
	
	/** The Constant MCN_MASSIVE_OPERATION_CLASS_INVALID. */
	public static final String MCN_MASSIVE_OPERATION_CLASS_INVALID = "mcn.msg.massive.operation.class.invalid";
	
	/** The Constant MCN_MASSIVE_OPERATION_TYPE_INVALID. */
	public static final String MCN_MASSIVE_OPERATION_TYPE_INVALID = "mcn.msg.massive.operation.type.invalid";
	
	/** The Constant MCN_MASSIVE_PARTICIPANT_INVALID_SALE_ROLE. */
	public static final String MCN_MASSIVE_PARTICIPANT_INVALID_SALE_ROLE = "mcn.msg.massive.participant.sale.role.invalid";
	
	/** The Constant MCN_MASSIVE_PARTICIPANT_INVALID_PURCH_ROLE. */
	public static final String MCN_MASSIVE_PARTICIPANT_INVALID_PURCH_ROLE = "mcn.msg.massive.participant.purchase.role.invalid";
	
	/** The Constant MCN_MASSIVE_PART_ASSIGNMENT_CLOSED. */
	public static final String MCN_MASSIVE_PART_ASSIGNMENT_CLOSED = "mcn.msg.massive.participant.assignment.closed";
	
	/** The Constant MCN_MASSIVE_HOLDER_ACCOUNT_NOT_FOUND. */
	public static final String MCN_MASSIVE_HOLDER_ACCOUNT_NOT_FOUND = "mcn.msg.massive.holder.account.not.found";
	
	/** The Constant MCN_MASSIVE_ASSIGN_MARKET_DATE_NULL. */
	public static final String MCN_MASSIVE_ASSIGN_MARKET_DATE_NULL = "mcn.msg.massive.market.date.null";
	
	/** The Constant MCN_MASSIVE_ASSIGN_MARKET_RATE_NULL. */
	public static final String MCN_MASSIVE_ASSIGN_MARKET_RATE_NULL= "mcn.msg.massive.market.rate.null";
	
	/** The Constant MCN_MASSIVE_ASSIGN_MARKET_PRICE_NULL. */
	public static final String MCN_MASSIVE_ASSIGN_MARKET_PRICE_NULL = "mcn.msg.massive.market.price.null";
	
	/** The Constant MCN_MASSIVE_ASSIGN_SUCCESS. */
	public static final String MCN_MASSIVE_ASSIGN_SUCCESS = "mcn.msg.massive.assigment.success";
	
	/** The Constant MCN_MASSIVE_ASSIGN_FILE_IN_PROCESS. */
	public static final String MCN_MASSIVE_ASSIGN_FILE_IN_PROCESS = "mcn.msg.massive.assigment.file.in.process";
	
	/** The Constant MCN_ISSUANCE_DATE_GREATHER_CURRENT. */
	public static final String MCN_ISSUANCE_DATE_GREATHER_CURRENT = "mcn.msg.cordigoValor.issuanceDate";
	
	/** The Constant MCN_CORDIGOVALOR_NOTENTERED. */
	public static final String MCN_CORDIGOVALOR_NOTENTERED = "mcn.msg.cordigoValor.NotEntered";
	
	/** The Constant MCN_CORDIGOVALOR_NOTREGISTERED. */
	public static final String MCN_CORDIGOVALOR_NOTREGISTERED = "mcn.msg.codigoValorRegis";
	
	/** The Constant MCN_CANCEL_NOT_REGISTERED. */
	public static final String MCN_CANCEL_NOT_REGISTERED = "mcn.msg.notRegistered";
	
	/** The Constant MCN_CANCEL_SUCCESS. */
	public static final String MCN_CANCEL_SUCCESS = "mcn.msg.cancelSuccess";
	
	/** The Constant MCN_CANCEL_CONFIRMER. */
	public static final String MCN_CANCEL_CONFIRMER = "mcn.msg.cancel.confirm";
	
	/** The Constant CLOSE_ASSIGNMENT_ERROR. */
	public static final String CLOSE_ASSIGNMENT_ERROR = "manage.holderAccount.closeAssProcess.error";
	
	/** The Constant CLOSE_ASSIGNMENT_PROCESS. */
	public static final String CLOSE_ASSIGNMENT_PROCESS = "manage.holderAccount.closeAssProcess";
	
	/** The Constant CLOSE_ASSIGNMENT_PROCESS_SUCCESS. */
	public static final String CLOSE_ASSIGNMENT_PROCESS_SUCCESS = "manage.holderAccount.closeAssProcSuccess";
	
	/** The Constant RESTART_ASSIGNMENT_ERROR. */
	public static final String RESTART_ASSIGNMENT_ERROR = "manage.holderAccount.restartAssProc.error";
	
	/** The Constant RESTART_ASSIGNMENT_PROCESS. */
	public static final String RESTART_ASSIGNMENT_PROCESS = "manage.holderAccount.restartAssProc";
	
	/** The Constant RESTART_ASSIGNMENT_PROCESS_SUCCESS. */
	public static final String RESTART_ASSIGNMENT_PROCESS_SUCCESS = "manage.holderAccount.restartAssProcSuccess";
	
	/** The Constant CLOSE_GNERAL_ASSIGNMENT. */
	public static final String CLOSE_GNERAL_ASSIGNMENT = "manage.holderAccount.closeGeneralAssignment";
	
	/** The Constant CLOSE_GENERAL_ASSIGNMENT_SUCCESS. */
	public static final String CLOSE_GENERAL_ASSIGNMENT_SUCCESS = "manage.holderAccount.closeGeneralAssignmentSuccess";
	
	/** The Constant MCN_CORDIGOVALOR_INSTRUMENT_TYPE. */
	public static final String MCN_SECURITY_NOTALLOWED_MODALITY = "mcn.msg.codigoValorRegis.notallowedmodality";
	
	/** The Constant MCN_PARTICIPANT_NOTALLOWED_MODALITY. */
	public static final String MCN_PARTICIPANT_NOTALLOWED_MODALITY = "mcn.msg.participant.notallowedmodality";
	
	/** The Constant MCN_CORDIGOVALOR_MODALITY. */
	public static final String MCN_CORDIGOVALOR_MODALITY = "mcn.msg.codigoValorRegis.modality";
	
	/** The Constant MCN_SETTLEMENT_DATE_REPO. */
	public static final String MCN_SETTLEMENT_DATE_REPO = "mcn.msg.settlementDate.repo";
	
	/** The Constant MCN_CASH_DAYS_GREATER_TERM_DAYS. */
	public static final String MCN_CASH_DAYS_GREATER_TERM_DAYS = "mcn.cashdays.greater.termdays";
	
	/** The Constant MCN_OPERATION_REPORT. */
	public static final String MCN_TERM_SETTLEMENT_DAYS_NOUTIL = "mcn.term.settlement.days.noutil";
	
	/** The Constant MCN_INVALID_OPERATION_NUMBER_BY_MECHANISM. */
	public static final String MCN_INVALID_OPERATION_NUMBER_BY_MECHANISM = "mcn.operation.numberByMechanism.not.exists";

	public static final String MCN_INVALID_OPERATION_NUMBER_WRONG_REGEX = "mcn.operation.numberByMechanism.not.numbers";
	
	/** The Constant MCN_CASH_INVALID_SETTLEMENT_DAYS. */
	public static final String MCN_CASH_INVALID_SETTLEMENT_DAYS = "mcn.cash.invalid.settlement.days";
	
	/** The Constant MCN_STOCK_QUANTITY_NOT_EXISTS. */
	public static final String MCN_STOCK_QUANTITY_GREATER_THAN_REPORT = "mcn.stock.quantity.greater.than.report";
	
	/** The Constant MCN_NOT_MULTIPLE_OF_NOMINAL_VALUE. */
	public static final String MCN_NOT_MULTIPLE_OF_NOMINAL_VALUE = "mcn.not.multiple.of.nominal.value";
	
	/** The Constant MCN_REGISTRATION_FAILED. */
	public static final String MCN_REGISTRATION_FAILED = "mcn.msg.register.failed";
	
	/** The Constant MCN_PRIMARY_PLACEMENT_NOT_EXISTS. */
	public static final String MCN_PRIMARY_PLACEMENT_NOT_EXISTS = "mcn.primary.placement.not.exists";
	
	/** The Constant MCN_STOCKQUANTITY_NOT_NULL. */
	public static final String MCN_STOCKQUANTITY_NOT_NULL = "mcn.msg.stockQuantity.notNull";
	
	/** The Constant MCN_STOCKQUANTITY_NOT_NULL. */
	public static final String MCN_RATE_NOT_NULL = "mcn.msg.rate.notNull";
	
	/** The Constant MCN_RATE_NOT_NULL_OR_ZERO. */
	public static final String MCN_RATE_NOT_NULL_OR_ZERO = "mcn.msg.rate.notNullOrZero";
	
	/** The Constant MCN_NO_CASH_SETTLEMENT_DATE. */
	public static final String MCN_NO_CASH_SETTLEMENT_DATE = "mcn.msg.cash.settlementdate.notNull";
	
	/** The Constant MCN_MECHANISM_NULL. */
	public static final String MCN_MECHANISM_NULL = "mcn.msg.mechanism.notExists";
	
	/** The Constant MCN_MCNFILE_NULL. */
	public static final String MCN_MCNFILE_NULL = "mcn.msg.mcnFile.notExists";
	

	/** The Constant MCN_REAL_CASH_AMOUNT_VAL. */
	public static final String MCN_REAL_CASH_AMOUNT_VAL = "mcn.realcashamount.greater";
	
	/** The Constant QUANTITY_EXCEEDED. */
	public static final String QUANTITY_EXCEEDED="reg.holderAccount.error.quantityExceed";
	
	/** The Constant NOT_EXCEEDED_OPERATION_QUANTITY. */
	public static final String NOT_EXCEEDED_OPERATION_QUANTITY="reg.holderAccount.error.notExceedOperationQuantity";
	
	/** The Constant ATLEAST_ONE_HOLDERACCOUNT. */
	public static final String ATLEAST_ONE_HOLDERACCOUNT="reg.holderAccount.error.atleastOneholderAccount";
	
	/** The Constant SAVE_ASSIGNMENT. */
	public static final String SAVE_ASSIGNMENT="manage.holderAccount.error.saveAssignment";
	
	/** The Constant SUCCESS_SAVE_ASSIGNMENT. */
	public static final String SUCCESS_SAVE_ASSIGNMENT="reg.holderAccount.msg.successSaveAssignment";
	
	/** The Constant SUCEESS_CONFIRM_ASSIGNMENT. */
	public static final String SUCEESS_CONFIRM_ASSIGNMENT="confirm.holderAccount.msg.successAssignment";
	
	/** The Constant TOTAL_QUANTITY_NOT_MATCH. */
	public static final String TOTAL_QUANTITY_NOT_MATCH="confirm.holderAccount.error.totalQuantity";
	
	/** The Constant TOTAL_QUANTITY_NOT_MATCH_TO_CONFIRM. */
	public static final String TOTAL_QUANTITY_NOT_MATCH_TO_CONFIRM="confirm.holderAccount.error.totalQuantityNotMatch";
	
	/** The Constant ALREADY_EXIST_ACCOUNT. */
	public static final String ALREADY_EXIST_ACCOUNT="reg.holderAccount.error.alredyExistAccount";
	
	/** The Constant ALREADY_EXIST_SECURITIES_INCHARGE. */
	public static final String ALREADY_EXIST_SECURITIES_INCHARGE="reg.holderAccount.error.alredyExistSecuritiesIncharge";
	
	/** The Constant INCHARGERS_WRONG_COMBINATION. */
	public static final String INCHARGERS_WRONG_COMBINATION="reg.holderAccount.error.inchargersWrongCombination";
	
	/** The Constant NO_REGISTERED_ACCOUNT. */
	public static final String NO_REGISTERED_ACCOUNT="reg.holderAccount.error.noRegisteredAccount";
	
	/** The Constant ACCOUNT_NOT_EXIST. */
	public static final String ACCOUNT_NOT_EXIST="reg.holderAccount.error.accountNotExist";
	
	/** The Constant MECHANISM_REQUIRED_FOR_SEARCH. */
	public static final String MECHANISM_REQUIRED_FOR_SEARCH="manage.holderAccount.error.mechanism.required";
	
	/** The Constant CONFIRM_CONFIRMATION_ASSIGNMENT. */
	public static final String CONFIRM_CONFIRMATION_ASSIGNMENT="confirm.holderAccount.msg.confirmAssignment";
	
	/** The Constant CONFIRMATION_PENDING_ROLE_CONTINUE. */
	public static final String CONFIRMATION_PENDING_ROLE_CONTINUE="confirm.holderAccount.msg.confirmAssignmentForContinue";
	
	/** The Constant CONFIRM_SAVE_ASSIGNMENT. */
	public static final String CONFIRM_SAVE_ASSIGNMENT="reg.holderAccount.msg.confirmSaveAssignment";
	
	/** The Constant HOLDERaCCOUNT_REQIRED. */
	public static final String HOLDERaCCOUNT_REQIRED="manage.incharge.error.holderAccountRequired";
	
	/** The Constant INTERNATIONAL_OPERATION_OPERATION_DATE. */
	public static final String INTERNATIONAL_OPERATION_OPERATION_DATE = "international.operation.date";
	
	/** The Constant REQUEST_EMPTY_NOT_SELECTED. */
	public static final String REQUEST_EMPTY_NOT_SELECTED = "request.empty.not.selected";
	
	/** The Constant INTERNATIONAL_OPERATION_SETTLEMENT_DATE_. */
	public static final String INTERNATIONAL_OPERATION_SETTLEMENT_DATE_= "international.settlement.date";
	
	/** The Constant INTERNATIONAL_OPERATION_SETTLEMENT_DATE. */
	public static final String INTERNATIONAL_OPERATION_SETTLEMENT_DATE = "international.system.date";
	
	/** The Constant INTERNATIONAL_OPERATION_ISINCODE_NOTEXISTS. */
	public static final String INTERNATIONAL_OPERATION_ISINCODE_NOTEXISTS = "international.isincode.notexists";
	
	/** The Constant INTERNATIONAL_OPERATION_ISINCODE_NOT_REGISTERED. */
	public static final String INTERNATIONAL_OPERATION_ISINCODE_NOT_REGISTERED = "international.isincode.notregistered";
	
	/** The Constant LOCAL_PARTICIPANT_NOT_SELECTED. */
	public static final String LOCAL_PARTICIPANT_NOT_SELECTED = "international.participant.not.selected";
	
	/** The Constant HOLDER_ACCOUNT_NOT_EXISTS. */
	public static final String HOLDER_ACCOUNT_NOT_EXISTS = "international.holderAcc.not.exists";
	
	/** The Constant HOLDER_ACCOUNT_NOT_REGISTERED. */
	public static final String HOLDER_ACCOUNT_NOT_REGISTERED = "international.holderAcc.not.registered";
	
	/** The Constant SENDING_INTERNATIONAL_SECURITIES. */
	public static final String SENDING_INTERNATIONAL_SECURITIES = "international.sending.type";
	
	/** The Constant SELECT_INTERNATIONAL_OPERATION_TYPE. */
	public static final String SELECT_INTERNATIONAL_OPERATION_TYPE = "international.select.operation.type";
	
	/** The Constant DECIMAL_GREATER_THAN_ZERO. */
	public static final String DECIMAL_GREATER_THAN_ZERO = "international.decimalGreaterThanZero";
	
	/** The Constant WHOLE_NUMBER_GREATER_THAN_ZERO. */
	public static final String WHOLE_NUMBER_GREATER_THAN_ZERO = "international.wholeNumberGreaterThanZero";
	
	/** The Constant ISIN_CODE_DOES_NOT_EXISTS. */
	public static final String ISIN_CODE_DOES_NOT_EXISTS = "international.isinCodeNotExist";
	
	/** The Constant INTERNATIONAL_OPERATION_REGISTER_SUCCESS. */
	public static final String INTERNATIONAL_OPERATION_REGISTER_SUCCESS = "international.operation.register.success";
	
	/** The Constant INTERNATIONAL_OPERATION_REGISTRATION_START_DATE. */
	public static final String INTERNATIONAL_OPERATION_REGISTRATION_START_DATE = "international.registration.start.date";
	
	/** The Constant INTERNATIONAL_OPERATION_REGISTRATION_END_DATE. */
	public static final String INTERNATIONAL_OPERATION_REGISTRATION_END_DATE = "international.registration.end.date";
	
	/** The Constant INTER_OPERATION_MODIFY_MORE_RECORD_SELECTED. */
	public static final String INTER_OPERATION_MODIFY_MORE_RECORD_SELECTED = "inter.operation.modify.moreRecordsSelected";
	
	/** The Constant INTER_OPERATION_NO_RECORD_SELECTED. */
	public static final String INTER_OPERATION_NO_RECORD_SELECTED ="international.operation.noRecordSelected";
	
	/** The Constant INTER_OPERATION_MODIFY_NOT_REGISTERED. */
	public static final String INTER_OPERATION_MODIFY_NOT_REGISTERED = "international.operation.modify.notRegistered";
	
	/** The Constant INTER_REGISTER_CONFIRM. */
	public static final String INTER_REGISTER_CONFIRM = "international.msg.register.confirm";
	
	/** The Constant INTER_MODIFY_CONFIRM. */
	public static final String INTER_MODIFY_CONFIRM = "international.msg.modify.confirm";
	
	/** The Constant INTER_REVERSE_CONFIRM. */
	public static final String INTER_REVERSE_CONFIRM = "international.msg.reverse.confirm";
	
	/** The Constant INTER_REJECT_CONFIRM. */
	public static final String INTER_REJECT_CONFIRM = "international.msg.reject.confirm";
	
	/** The Constant INTER_CONFIRM_CONFIRM. */
	public static final String INTER_CONFIRM_CONFIRM = "international.msg.confirm.confirm";
	
	/** The Constant INTER_CANCEL_CONFIRM. */
	public static final String INTER_CANCEL_CONFIRM = "international.msg.cancel.confirm";
	
	/** The Constant INTER_SETTLE_CONFIRM. */
	public static final String INTER_SETTLE_CONFIRM = "international.msg.settle.confirm";	
	
	/** The Constant INTERNATIONAL_OPERATION_MODIFY_SUCCESS. */
	public static final String INTERNATIONAL_OPERATION_MODIFY_SUCCESS = "international.msg.modify.success";
	
	/** The Constant INTER_APPROVE_CONFIRM. */
	public static final String INTER_APPROVE_CONFIRM = "international.msg.approve.confirm";
	
	/** The Constant INTER_APPROVE_SUCCESS. */
	public static final String INTER_APPROVE_SUCCESS = "international.msg.approve.success";
	
	/** The Constant INTER_REVERSE_SUCCESS. */
	public static final String INTER_REVERSE_SUCCESS = "international.msg.reverse.success";
	
	/** The Constant INTER_REJECT_SUCCESS. */
	public static final String INTER_REJECT_SUCCESS = "international.msg.reject.success";
	
	/** The Constant INTER_CANCEL_SUCCESS. */
	public static final String INTER_CANCEL_SUCCESS = "international.msg.cancel.success";
	
	/** The Constant INTER_CONFIRM_SUCCESS. */
	public static final String INTER_CONFIRM_SUCCESS = "international.msg.confirm.success";
	
	/** The Constant INTER_SETTLEMENT_SUCCESS. */
	public static final String INTER_SETTLEMENT_SUCCESS = "international.msg.settle.success";
	
	/** The Constant START_DATE_GREATER_END_DATE. */
	public static final String START_DATE_GREATER_END_DATE = "international.msg.StartEndDate"; 
	
	/** The Constant INTERNATIONAL_MSG_VALID_SECURITY. */
	public static final String INTERNATIONAL_MSG_VALID_SECURITY = "international.msg.valid.security";
	
	/** The Constant INTERNATIONAL_MSG_VALID_HOLDER. */
	public static final String INTERNATIONAL_MSG_VALID_HOLDER = "international.msg.valid.holder";
	
	/** The Constant INTERNATIONAL_MSG_VALID_HOLDER_ACCOUNT. */
	public static final String INTERNATIONAL_MSG_VALID_HOLDER_ACCOUNT = "international.msg.valid.holder.account";
	
	/** The Constant INTERNATIONAL_MSG_NEGATIVE_BALANCE. */
	public static final String INTERNATIONAL_MSG_NEGATIVE_BALANCE = "international.msg.negative.balance";
	
	// PROPERTIES CONSTANST FOR OTC OPERATIONS
	/** The Constant OTC_OPERATIONS_VALIDATION_SECURITY_STATE. */
	public static final String OTC_OPERATIONS_VALIDATION_SECURITY_STATE="otc.operation.validation.security.state";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_CASH_AMOUNT_NULL. */
	public static final String OTC_OPERATIONS_VALIDATION_CASH_AMOUNT_NULL="otc.operation.validation.cashamount.null";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_USER_INCORRECT. */
	public static final String OTC_OPERATIONS_VALIDATION_USER_INCORRECT="otc.operation.validation.user.incorrect";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS. */
	public static final String OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS="otc.operation.validation.fixed.expiration.days";

	/** The Constant OTC_OPERATIONS_VALIDATION_MAX_SETTLEMENT_DATE. */
	public static final String OTC_OPERATIONS_VALIDATION_MAX_SETTLEMENT_DATE="otc.operation.validation.max.settlement.date";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_SPLIT_PAYMENT_DATE. */
	public static final String OTC_OPERATIONS_VALIDATION_SPLIT_PAYMENT_DATE="otc.operation.validation.split.payment.date";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_SECURITY_NULL. */
	public static final String OTC_OPERATIONS_VALIDATION_SECURITY_NULL="otc.operation.validation.security.null";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_SECURITY_INSTRUMENT. */
	public static final String OTC_OPERATIONS_VALIDATION_SECURITY_INSTRUMENT="otc.operation.validation.security.instrument";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_RATES. */
	public static final String OTC_OPERATIONS_VALIDATION_RATES="otc.operation.validation.rates";
	
	/** The Constant OTC_OPERATIONS_VALIDATION_MULTIPLE_STOCK. */
	public static final String OTC_OPERATIONS_VALIDATION_MULTIPLE_STOCK="otc.operation.validation.multiple.stock";
	
	/** The Constant OTC_OPERATION_VALIDATION_SETTLEMENT_DAYS. */
	public static final String OTC_OPERATION_VALIDATION_SETTLEMENT_DAYS="otc.operation.validation.settlement.days";
	
	/** The Constant OTC_OPERATION_VALIDATION_SETTLEMENT_CASH_EMPTY. */
	public static final String OTC_OPERATION_VALIDATION_SETTLEMENT_CASH_EMPTY="otc.operation.validation.settlement.cash.empty";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDERACCOUNT. */
	public static final String OTC_OPERATION_VALIDATION_HOLDERACCOUNT="otc.operation.validation.holderaccount";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDERACCOUNT_STATE. */
	public static final String OTC_OPERATION_VALIDATION_HOLDERACCOUNT_STATE="otc.operation.validation.holderaccount.state";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDERACCOUNT_HOLDER_STATE. */
	public static final String OTC_OPERATION_VALIDATION_HOLDERACCOUNT_HOLDER_STATE="otc.operation.validation.holderaccount.holder.state";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDERACCOUNT_PARTICIPANTCHARGE. */
	public static final String OTC_OPERATION_VALIDATION_HOLDERACCOUNT_PARTICIPANTCHARGE="otc.operation.validation.holderaccount.participantcharge";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDERACCOUNT_STATE_PARTICIPANTCHARGE. */
	public static final String OTC_OPERATION_VALIDATION_HOLDERACCOUNT_STATE_PARTICIPANTCHARGE="otc.operation.validation.holderaccount.state.participantcharge";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDERACCOUNT_HOLDER_STATE_PARTICIPANTCHARGE. */
	public static final String OTC_OPERATION_VALIDATION_HOLDERACCOUNT_HOLDER_STATE_PARTICIPANTCHARGE="otc.operation.validation.holderaccount.holder.state.participantcharge";
	
	/** The Constant OTC_OPERATION_VALIDATION_ISSUANCE_FACTORNEGOTIATION. */
	public static final String OTC_OPERATION_VALIDATION_ISSUANCE_FACTORNEGOTIATION="otc.operation.validation.issuance.factornegotiation";
	
	/** The Constant OTC_OPERATION_VALIDATION_QUANTITY_SELL. */
	public static final String OTC_OPERATION_VALIDATION_QUANTITY_SELL="otc.operation.validation.quantity.sell";
	
	/** The Constant OTC_OPERATION_VALIDATION_QUANTITY_OPERATION. */
	public static final String OTC_OPERATION_VALIDATION_QUANTITY_OPERATION="otc.operation.validation.quantity.operation";
	
	public static final String OTC_OPERATION_VALIDATION_MESSAGE = "otc.operation.validation";
	
	/** The Constant OTC_OPERATION_VALIDATION_QUANTITY_OPERATION_MENOR. */
	public static final String OTC_OPERATION_VALIDATION_QUANTITY_OPERATION_MENOR="otc.operation.validation.quantity.operation.menor";
	
	/** The Constant MESSAGES_ALERT_CONFIRM. */
	public static final String MESSAGES_ALERT_CONFIRM="messages.alert.confirm";
	
	/** The Constant OTC_OPERATION_WARNING_DELETE_ACCOUNT. */
	public static final String OTC_OPERATION_WARNING_DELETE_ACCOUNT="otc.operation.warning.delete.account";
	
	/** The Constant OTC_OPERATION_VALIDATION_QUANTITY_SUM. */
	public static final String OTC_OPERATION_VALIDATION_QUANTITY_SUM="otc.operation.validation.quantity.sum";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT="otc.operation.validation.security.inconsistent";
	
	/** The Constant INTERNATIONAL_VALIDATION_SETTLEMENT_DATE. */
	public static final String INTERNATIONAL_VALIDATION_SETTLEMENT_DATE="international.validation.settlement.date";
	
	/** The Constant INTERNATIONAL_VALIDATION_SECURITY_FOREIGN. */
	public static final String INTERNATIONAL_VALIDATION_SECURITY_FOREIGN="international.validation.security.foreign";
	
	/** The Constant OTC_OPERATION_VALIDATION_INCHARGE. */
	public static final String OTC_OPERATION_VALIDATION_INCHARGE="otc.operation.validation.incharge";
	
	/** The Constant OTC_OPERATION_REGISTER_CONFIRM. */
	public static final String OTC_OPERATION_REGISTER_CONFIRM="otc.operation.register.confirm";

	/** The Constant OTC_OPERATION_REGISTER_CONFIRM_OK. */
	public static final String OTC_OPERATION_REGISTER_CONFIRM_OK="otc.operation.register.confirm.ok";
	
	/** The Constant OTC_OPERATION_CONFIRM_CONFIRM. */
	public static final String OTC_OPERATION_CONFIRM_CONFIRM="otc.operation.confirm.confirm";

	public static final String OTC_OPERATION_SETTLEMENT_CONFIRM="otc.operation.settlement.confirm";
	
	public static final String OTC_OPERATION_AUTHORIZE_CONFIRM="otc.operation.authorize.confirm";
	/** The Constant OTC_OPERATION_CANCEL_CONFIRM. */
	public static final String OTC_OPERATION_CANCEL_CONFIRM="otc.operation.cancel.confirm";
	
	/** The Constant OTC_OPERATION_CANCEL_CONFIRM_OK. */
	public static final String OTC_OPERATION_CANCEL_CONFIRM_OK="otc.operation.cancel.confirm.ok";

	/** The Constant OTC_OPERATION_CONFIRM_CONFIRM_OK. */
	public static final String OTC_OPERATION_CONFIRM_CONFIRM_OK="otc.operation.confirm.confirm.ok";
	
	public static final String OTC_OPERATION_SETTLEMENT_CONFIRM_OK="otc.operation.settlement.confirm.ok";
	
	public static final String OTC_OPERATION_AUTHORIZE_CONFIRM_OK="otc.operation.authorize.confirm.ok";
	
	/** The Constant OTC_OPERATION_REVIEW_CONFIRM. */
	public static final String OTC_OPERATION_REVIEW_CONFIRM="otc.operation.review.confirm";
	
	/** The Constant OTC_OPERATION_REVIEW_CONFIRM_OK. */
	public static final String OTC_OPERATION_REVIEW_CONFIRM_OK="otc.operation.review.confirm.ok";
	
	/** The Constant OTC_OPERATION_APPROVE_CONFIRM. */
	public static final String OTC_OPERATION_APPROVE_CONFIRM="otc.operation.approve.confirm";
	
	/** The Constant OTC_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String OTC_OPERATION_APPROVE_CONFIRM_OK="otc.operation.approve.confirm.ok";
	
	/** The Constant OTC_OPERATION_ANNULATE_CONFIRM. */
	public static final String OTC_OPERATION_ANNULATE_CONFIRM="otc.operation.annulate.confirm";
	
	/** The Constant OTC_OPERATION_ANNULATE_CONFIRM_OK. */
	public static final String OTC_OPERATION_ANNULATE_CONFIRM_OK="otc.operation.annulate.confirm.ok";
	
	/** The Constant OTC_OPERATION_REJECT_CONFIRM. */
	public static final String OTC_OPERATION_REJECT_CONFIRM="otc.operation.reject.confirm";
	
	/** The Constant OTC_OPERATION_REJECT_CONFIRM_OK. */
	public static final String OTC_OPERATION_REJECT_CONFIRM_OK="otc.operation.reject.confirm.ok";
	
	/** The Constant OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT. */
	public static final String OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT="otc.operation.validation.incharge.pendient";
	
	/** The Constant OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT. */
	public static final String OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT="otc.operation.validation.participation.incorrect";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDERACCOUNT_DUPLICATE. */
	public static final String OTC_OPERATION_VALIDATION_HOLDERACCOUNT_DUPLICATE="otc.operation.validation.holderaccount.duplicated";
	
	/** The Constant OTC_OPERATION_VALIDATION_HOLDER_DUPLICATE. */
	public static final String OTC_OPERATION_VALIDATION_HOLDER_DUPLICATE="otc.operation.validation.holder.duplicated";
	
	/** The Constant OTC_OPERATION_VALIDATION_PARTICIPANT_ASSIGNATION. */
	public static final String OTC_OPERATION_VALIDATION_PARTICIPANT_ASSIGNATION="otc.operation.validation.participant.assignation";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_ISSUER. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_ISSUER="otc.operation.validation.security.issuer";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_ISSUANCE. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_ISSUANCE="otc.operation.validation.security.issuance";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT="otc.operation.validation.security.placement";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_CARACTERS. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_CARACTERS="otc.operation.validation.quantity.caracters";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_PARTICIPANT. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_PARTICIPANT="otc.operation.validation.security.placement.participant";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_PARTICIPANT_HOLDERACCOUNT. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_PARTICIPANT_HOLDERACCOUNT="otc.operation.validation.security.placement.participant.holderaccount";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_PARTICIPANT_HOLDERACCOUNT_STATE. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_PARTICIPANT_HOLDERACCOUNT_STATE="otc.operation.validation.security.placement.participant.holderaccount.state";
	
	/** The Constant OTC_OPERATION_VALIDATION_FOWARD. */
	public static final String OTC_OPERATION_VALIDATION_FOWARD="otc.operation.validation.foward";
	
	/** The Constant OTC_OPERATION_VALIDATION_SETTLEMENT_TERM. */
	public static final String OTC_OPERATION_VALIDATION_SETTLEMENT_TERM="otc.operation.validation.settlement.term";
	
	/** The Constant OTC_OPERATION_VALIDATION_SETTLEMENT_CASH. */
	public static final String OTC_OPERATION_VALIDATION_SETTLEMENT_CASH="otc.operation.validation.settlement.cash";
	
	/** The Constant OTC_OPERATION_VALIDATION_MODALITY_INCONSISTENT. */
	public static final String OTC_OPERATION_VALIDATION_MODALITY_INCONSISTENT="otc.operation.validation.modality.incosistent";
	
	/** The Constant OTC_OPERATION_REPOOPERATION_EXIST. */
	public static final String OTC_OPERATION_REPOOPERATION_EXIST="otc.operation.repooperation.exist";
	
	/** The Constant OTC_OPERATION_VALIDATION_REPO_SECUNDARY_QUANTITY. */
	public static final String OTC_OPERATION_VALIDATION_REPO_SECUNDARY_QUANTITY="otc.operation.validation.repo.secundary.quantity";
	
	/** The Constant OTC_OPERATION_REPOOPERATION_SETLEMENT_CONTADO. */
	public static final String OTC_OPERATION_REPOOPERATION_SETLEMENT_CONTADO="otc.operation.repooperation.setlement.contado";
	
	/** The Constant OTC_OPERATION_VALIDATION_SEC_REPO_QUANTITY. */
	public static final String OTC_OPERATION_VALIDATION_SEC_REPO_QUANTITY="otc.operation.validation.sec.repo.quantity";
	
	/** The Constant OTC_OPERATION_VALIDATION_SEC_REPO_EMPTY. */
	public static final String OTC_OPERATION_VALIDATION_SEC_REPO_EMPTY="otc.operation.validation.sec.repo.empty";
	
	/** The Constant OTC_OPERATION_VALIDATION_ISIN_MECHANISM. */
	public static final String OTC_OPERATION_VALIDATION_ISIN_MECHANISM="otc.operation.validation.isin.mechanism";
	
	/** The Constant OTC_OPERATION_VALIDATION_ISIN_SWAP_EXIST_EXIST. */
	public static final String OTC_OPERATION_VALIDATION_ISIN_SWAP_EXIST="otc.operation.validation.isin.swap.exist";
	
	/** The Constant OTC_OPERATION_VALIDATION_PARTICIPANT. */
	public static final String OTC_OPERATION_VALIDATION_PARTICIPANT="otc.operation.validation.participant";
	
	/** The Constant OTC_OPERATION_VALIDATION_COUPON_EMPTY. */
	public static final String OTC_OPERATION_VALIDATION_COUPON_EMPTY="otc.operation.validation.coupon.empty";
	
	/** The Constant OTC_OPERATION_VALIDATION_COUPON_PENDIENT. */
	public static final String OTC_OPERATION_VALIDATION_COUPON_PENDIENT="otc.operation.validation.coupon.pendient";
	
	/** The Constant OTC_OPERATION_VALIDATION_ACCOUNT_SELL. */
	public static final String OTC_OPERATION_VALIDATION_ACCOUNT_SELL="otc.operation.validation.holderaccount.sell";
	
	/** The Constant OTC_OPERATION_VALIDATION_ACCOUNT_SELL_EMPTY. */
	public static final String OTC_OPERATION_VALIDATION_ACCOUNT_SELL_EMPTY="otc.operation.validation.holderaccount.sell.empty";
	
	/** The Constant OTC_OPERATION_VALIDATION_ACCOUNT_BUY. */
	public static final String OTC_OPERATION_VALIDATION_ACCOUNT_BUY="otc.operation.validation.holderaccount.buy";
	
	/** The Constant OTC_OPERATION_VALIDATION_ACCOUNT_SELL_BUY. */
	public static final String OTC_OPERATION_VALIDATION_ACCOUNT_BUY_EMPTY="otc.operation.validation.holderaccount.buy.empty";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_AMOUNT. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_PLACEMENT_AMOUNT="otc.operation.validation.security.placement.amount";
	
	/** The Constant OTC_OPERATION_VALIDATION_SECURITY_CIRCULATION. */
	public static final String OTC_OPERATION_VALIDATION_SECURITY_CIRCULATION="otc.operation.validation.security.circulation";
	
	/** The Constant OTC_OPERATION_VALIDATION_LENGHT_EFECTIVETAZA. */
	public static final String OTC_OPERATION_VALIDATION_LENGHT_EFECTIVETAZA="otc.operation.validation.lengt.efectivetaza";
	
	/** The Constant OTC_OPERATION_VALIDATION_TERM_AMOUNT_LESS. */
	public static final String OTC_OPERATION_VALIDATION_TERM_AMOUNT_LESS="otc.operation.validation.term.amount.less";
	
	/** The Constant OTC_OPERATION_VALIDATION_CASH_AMOUNT_EMPTY. */
	public static final String OTC_OPERATION_VALIDATION_CASH_AMOUNT_EMPTY="otc.operation.validation.cash.amount.empty";
	
	/** The Constant OPERATION_VALIDATION_CASH_PRICE_EMPTY. */
	public static final String OPERATION_VALIDATION_CASH_PRICE_EMPTY="operation.validation.cash.price.empty";
	
	/** The Constant OTC_OPERATION_VALIDATION_DIRTY_AMOUNT_LESS. */
	public static final String OTC_OPERATION_VALIDATION_DIRTY_AMOUNT_LESS="otc.operation.validation.dirty.amount.less";
	
	/** The Constant OTC_OPERATION_VALIDATION_CLEAN_AMOUNT_GATHER. */
	public static final String OTC_OPERATION_VALIDATION_CLEAN_AMOUNT_GATHER="otc.operation.validation.clean.amount.gather";
	
	/** The Constant OTC_OPERATION_VALIDATION_PRIMARYPLACEMENT_FINISHED. */
	public static final String OTC_OPERATION_VALIDATION_PRIMARYPLACEMENT_FINISHED="otc.operation.validation.primaryplacement.finished";
	
	/** The Constant OTC_OPERATION_VALIDATION_RANGE_AMOUTNS. */
	public static final String OTC_OPERATION_VALIDATION_RANGE_AMOUTNS="otc.operation.validation.range.amounts";
	
	/** The Constant OTC_OPERATION_VALIDATION_RANGE_MAX_AMOUTNS. */
	public static final String OTC_OPERATION_VALIDATION_RANGE_MAX_AMOUTNS="otc.operation.validation.range.max.amounts";
	
	/** The Constant OTC_OPERATION_VALIDATION_SWAP_STOCK. */
	public static final String OTC_OPERATION_VALIDATION_SWAP_STOCK="otc.operation.validation.swap.stockquantity";
	
	/** The Constant OTC_OPERATION_SWAP_REALCASH_AMOUNT. */
	public static final String OTC_OPERATION_SWAP_REALCASH_AMOUNT="otc.operation.swap.realcash.amount";
	
	/** The Constant OTC_OPERATION_SWAP_REALCASH_AMOUNT_EMPTY. */
	public static final String OTC_OPERATION_SWAP_REALCASH_AMOUNT_EMPTY="otc.operation.swap.realcash.amount.empty";
	
	public static final String OTC_OPERATION_AVAILABLE_BALANCE="otc.operation.available.balance";
	
	public static final String OTC_OPERATION_SELL_PENDING_BALANCE="otc.operation.sell.pending";	

	public static final String OTC_OPERATION_ALREADY_SETTLED="otc.operation.already.settled";
	
	public static final String OTC_OPERATION_NOT_FOP="otc.operation.not.fop";
	// END - PROPERTIES CONSTANST FOR OTC OPERATIONS
	
	/** The Constant MSG_INCHARGE_AGREEMENT_REGISTER. */
	public static final String MSG_INCHARGE_AGREEMENT_REGISTER="msg.manage.incharge.agreement.register.confirm";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_REGISTER_OK. */
	public static final String MSG_INCHARGE_AGREEMENT_REGISTER_OK="msg.manage.incharge.agreement.register.ok";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED. */
	public final static String MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED="msg.manage.incharge.agreement.validation.error.record.required";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_VALIDATION_REGISTER. */
	public final static String MSG_INCHARGE_AGREEMENT_VALIDATION_REGISTER="msg.manage.incharge.agreement.validation.register";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_VALIDATION_CONFIRM. */
	public final static String MSG_INCHARGE_AGREEMENT_VALIDATION_CONFIRM="msg.manage.incharge.agreement.validation.confirm";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_CONFIRM. */
	public static final String MSG_INCHARGE_AGREEMENT_CONFIRM="msg.manage.incharge.agreement.confirm.confirm";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_REJECT. */
	public static final String MSG_INCHARGE_AGREEMENT_REJECT="msg.manage.incharge.agreement.confirm.reject";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_CANCEL. */
	public static final String MSG_INCHARGE_AGREEMENT_CANCEL="msg.manage.incharge.agreement.confirm.cancel";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_CONFIRM_OK. */
	public static final String MSG_INCHARGE_AGREEMENT_CONFIRM_OK="msg.manage.incharge.agreement.confirm.ok";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_REJECT_OK. */
	public static final String MSG_INCHARGE_AGREEMENT_REJECT_OK="msg.manage.incharge.agreement.reject.ok";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_CANCEL_OK. */
	public static final String MSG_INCHARGE_AGREEMENT_CANCEL_OK="msg.manage.incharge.agreement.cancel.ok";
	
	/** The Constant MSG_INCHARGE_AGREEMENT_DATA_EXIST. */
	public static final String MSG_INCHARGE_AGREEMENT_DATA_EXIST="msg.manage.incharge.agreement.data.exist";
	
	/** The Constant DELETE_ACCOUNT_CONFIRM. */
	public static final String DELETE_ACCOUNT_CONFIRM= "reg.holderAccount.msg.deleteAccountConfirm";
	
	/** The Constant CONFIRMATION_FOR_CONFIRM. */
	public static final String CONFIRMATION_FOR_CONFIRM="manage.incharge.confirmMsg.confirmation";
	
	/** The Constant CONFIRMATION_SUCCESS. */
	public static final String CONFIRMATION_SUCCESS="manage.incharge.confirmMsg.confirm.success";
	
	/** The Constant CONFIRMATION_FOR_REJECT. */
	public static final String CONFIRMATION_FOR_REJECT="manage.incharge.confirmMsg.reject";
	
	/** The Constant REJECT_SUCCESS. */
	public static final String REJECT_SUCCESS="manage.incharge.confirmMsg.reject.success";
	
	/** The Constant ERROR_REJECT. */
	public static final String ERROR_REJECT="manage.incharge.confirmMsg.reject.error";
	
	/** The Constant ATLEAST_SELECT_ONE_RECORD. */
	public static final String ATLEAST_SELECT_ONE_RECORD="manage.incharge.error.atleastSelectOneRecord";
	
	/** The Constant INCHARGE_PARTICIPANT_REQUIRED. */
	public static final String INCHARGE_PARTICIPANT_REQUIRED="manage.incharge.error.inchgargeParticipantRequired";
	
	/** The Constant TRADER_PARTICIPANT_REQUIRED. */
	public static final String TRADER_PARTICIPANT_REQUIRED="manage.incharge.error.TradingParticipantRequired";
	
	/** The Constant INVALID_RECORDS_SELECTED. */
	public static final String INVALID_RECORDS_SELECTED="manage.incharge.invalidrecordselected.error";
	
	
	/** The Constant SECURITY_DEPOSITARY_EXIST. */
	public static final String SECURITY_DEPOSITARY_EXIST="security.depositary.exist";
	
	/** The Constant HOLDERACCCOUNT_BALANCE_LESS. */
	public static final String HOLDERACCCOUNT_BALANCE_LESS="holderaccount.balance.less";
	
	/** The Constant INTERNATIONAL_VALIDATION_PHYSICAL_QUANTITY. */
	public static final String INTERNATIONAL_VALIDATION_PHYSICAL_QUANTITY="international.validation.physical.quantity";
	
	/** The Constant INTERNATIONAL_STOCK_QUANTITY. */
	public static final String INTERNATIONAL_STOCK_QUANTITY="international.stockquantity";
	
	/** The Constant INTERNATIONAL_CASH_AMOUNT_INVALID. */
	public static final String INTERNATIONAL_CASH_AMOUNT_INVALID="international.cashAmount.invalid";
	
	/** The Constant INTERNATIONAL_MSG_VALID_MARKETFACT. */
	public static final String INTERNATIONAL_MSG_VALID_MARKETFACT="international.msg.valid.marketfact";

	/** The Constant MECHANISM_REQUIRED. */
	public static final String MECHANISM_REQUIRED="manage.incharge.error.Mecanismo.required";
	
	/** The Constant MODALITY_REQUIRED. */
	public static final String MODALITY_REQUIRED="manage.incharge.error.Modalidad.required";
	
	/** The Constant PARTICIPANT_ENCARGADO_REQUIRED. */
	public static final String PARTICIPANT_ENCARGADO_REQUIRED="manage.incharge.error.ParticipanteEncargado.required";
	
	/** The Constant SECURITY_ISIN_LENGTH. */
	public static final String SECURITY_ISIN_LENGTH = "security.isin.length";
	
	/** The Constant DEPOSITORY_REQUIRED. */
	public static final String DEPOSITORY_REQUIRED="msg.interpart.deposit.required";
	
	/** The Constant PARTICIPANT_ACCOUNT_NUMBER_REPEATED. */
	public static final String PARTICIPANT_ACCOUNT_NUMBER_REPEATED="msg.interpart.account.repeated";
	
	/** The Constant MSG_PARTICIPANT_SAVE. */
	public static final String MSG_PARTICIPANT_SAVE="msg.interpart.save";
	
	/** The Constant MSG_PARTICIPANT_SAVE_SUCC. */
	public static final String MSG_PARTICIPANT_SAVE_SUCC="msg.interpart.save.succ";
	
	/** The Constant MSG_PARTICIPANT_BACK. */
	public static final String MSG_PARTICIPANT_BACK="msg.interpart.back";
	
	/** The Constant MSG_PARTICIPANT_EMPTY_SEARCH. */
	public static final String MSG_PARTICIPANT_EMPTY_SEARCH="msg.interpart.empty.search";
	
	/** The Constant MSG_PARTICIPANT_MODIFY_NOTSEL. */
	public static final String MSG_PARTICIPANT_MODIFY_NOTSEL="msg.interpart.modify.notsel";
	
	/** The Constant MSG_PARTICIPANT_MODIFY_NOTMODIFY. */
	public static final String MSG_PARTICIPANT_MODIFY_NOTMODIFY="msg.interpart.modify.notmodify";
	
	/** The Constant MSG_PARTICIPANT_MODIFY_CONFIRM. */
	public static final String MSG_PARTICIPANT_MODIFY_CONFIRM="msg.interpart.modify.confirm";
	
	/** The Constant MSG_PARTICIPANT_MODIFY_SUCC. */
	public static final String MSG_PARTICIPANT_MODIFY_SUCC="msg.interpart.modify.succ";
	
	/** Holds the Invalid user login message. */
	public static final String MSG_INVALID_USER="msg.user.not.logged";
	
	/** The Constant MESSAGE_DATA_REQUIRED. */
	public static final String MESSAGE_DATA_REQUIRED = "message.data.required";
	
	/** The Constant DATOS_ADJUNTOS. */
	public static final String MESSAGE_DATA_REQUIRED_ADJUNTOS = "message.data.required.adjuntos";
	
	/** The Constant EXECUTED_PROCESS_SUCCESFUL. */
	public static final String EXECUTED_PROCESS_SUCCESFUL= "executed.process.succesful";
	
	/** The Constant INTERNATIONAL_PARTICIPANT_HOLDERACCOUNT_EMPTY. */
	public static final String INTERNATIONAL_PARTICIPANT_HOLDERACCOUNT_EMPTY="international.participant.holderaccount.empty";
	
	/** The Constant for displaying the session expired message on Idle time. */
	public static final String IDLE_TIME_EXPIRED = "idle.session.expired";

	/** The Constant OTHER_MOTIVE_EMPTY. */
	public static final String OTHER_MOTIVE_EMPTY = "mcn.error.motive.other.empty";
	
	/** The Constant MOTIVE_NOT_SELECTED. */
	public static final String MOTIVE_NOT_SELECTED = "mcn.error.motive";
	
	/** The Constant MSG VALIDATION CUI NOT FOUND. */
	public static final String MSG_VALIDATION_CUI_NOT_FOUND="msg.loanable.securities.validation.cui.not.found";
	
	/** The Constant MSG VALIDATION HOLDER ACCOUNT NOT REGISTER. */
	public static final String MSG_VALIDATION_HOLDER_NOT_REGISTER="msg.loanable.securities.validation.holder.not.register";
	
	/** The Constant MSG VALIDATION CUI NOT FOUND. */
	public static final String MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND="msg.loanable.securities.validation.holder.account.not.found";
	
	/** The Constant MSG VALIDATION HOLDER ACCOUNT NOT REGISTER. */
	public static final String MSG_VALIDATION_HOLDER_ACCOUNT_NOT_REGISTER="msg.loanable.securities.validation.holder.account.not.register";
	
	/** The Constant MSG VALIDATION  NOT FOUND. */
	public static final String MSG_VALIDATION_NOT_FOUND="message.data.required";
	
	/** The Constant MSG VALIDATION  BALANCES NOT FOUND. */
	public static final String MSG_VALIDATION_BALANCES_NOT_FOUND="msg.loanable.securities.validation.balances.not.found";
	
	/** The Constant MSG VALIDATION  BALANCES LOANABLE NOT FOUND. */
	public static final String MSG_VALIDATION_BALANCES_LOANABLE_NOT_FOUND="msg.loanable.securities.validation.balances.loanable.not.found";
	
	/** The Constant MSG VALIDATION  TO PAY BALANCES ZERO. */
	public static final String MSG_VALIDATION_TO_PAY_BALANCE_ZERO="msg.loanable.securities.validation.to.pay.balance.zero";
	
	/** The Constant MSG VALIDATION  RETIREMENT BALANCES ZERO. */
	public static final String MSG_VALIDATION_RETIREMENT_BALANCE_ZERO="msg.loanable.securities.validation.retirement.balance.zero";
	
	/** The Constant MSG VALIDATION  TO PAY BALANCES. */
	public static final String MSG_VALIDATION_TO_PAY_BALANCE_AVAILABLE_BALANCE="msg.loanable.securities.validation.to.pay.balance.available.balance";
	
	/** The Constant MSG VALIDATION  TO PAY BALANCES. */
	public static final String MSG_VALIDATION_TO_PAY_BALANCE_LOANABLE_BALANCE="msg.loanable.securities.validation.to.pay.balance.loanable.balance";
	
	/** The Constant MSG LOANABLE SECURITIES REGISTER. */
	public static final String MSG_LOANABLE_SECURITIES_REGISTER="msg.loanable.securities.register.confirm";
	
	/** The Constant MSG LOANABLE SECURITIES REGISTER RETIREMENT. */
	public static final String MSG_LOANABLE_SECURITIES_REGISTER_RETIREMENT="msg.loanable.securities.register.retirement.confirm";
	
	/** The Constant MSG LOANABLE SECURITIES REGISTER OK. */
	public static final String MSG_LOANABLE_SECURITIES_REGISTER_OK="msg.loanable.securities.register.ok";
	
	/** The Constant MSG LOANABLE SECURITIES REGISTER RETIREMENT OK. */
	public static final String MSG_LOANABLE_SECURITIES_REGISTER_RETIREMENT_OK="msg.loanable.securities.register.retirement.ok";
	
	/** The Constant MSG LOANABLE SECURITIES ERROR RECORD REQUIRED. */
	public final static String MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED="msg.loanable.securities.validation.error.record.required";
	
	/** The Constant MSG LOANABLE SECURITIES VALIDATION REGISTERED. */
	public final static String MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED="msg.loanable.securities.validation.registered";
	
	/** The Constant MSG LOANABLE SECURITIES APPROVE. */
	public static final String MSG_LOANABLE_SECURITIES_APPROVE="msg.loanable.securities.approve.confirm";
	
	/** The Constant MSG LOANABLE SECURITIES ANNULAR. */
	public static final String MSG_LOANABLE_SECURITIES_ANNULAR="msg.loanable.securities.annular.confirm";
	
	/** The Constant MSG LOANABLE SECURITIES ANNULAR OK. */
	public static final String MSG_LOANABLE_SECURITIES_ANNULAR_OK="msg.loanable.securities.annular.ok";
	
	/** The Constant MSG LOANABLE SECURITIES APPROVE OK. */
	public static final String MSG_LOANABLE_SECURITIES_APPROVE_OK="msg.loanable.securities.approve.ok";
	
	/** The Constant MSG LOANABLE SECURITIES VALIDATION APPROVE. */
	public final static String MSG_LOANABLE_SECURITIES_VALIDATION_APPROVE="msg.loanable.securities.validation.approve";
	
	/** The Constant MSG LOANABLE SECURITIES REJECT. */
	public static final String MSG_LOANABLE_SECURITIES_REJECT="msg.loanable.securities.reject.confirm";
	
	/** The Constant MSG RETIREMENT LOANABLE SECURITIES REJECT. */
	public static final String MSG_RETIREMENT_LOANABLE_SECURITIES_REJECT="msg.loanable.securities.reject.retirement.confirm";
	
	/** The Constant MSG LOANABLE SECURITIES CONFIRM. */
	public static final String MSG_LOANABLE_SECURITIES_CONFIRM="msg.loanable.securities.confirm.confirm";
	
	/** The Constant MSG RETIREMENT LOANABLE SECURITIES CONFIRM. */
	public static final String MSG_RETIREMENT_LOANABLE_SECURITIES_CONFIRM="msg.loanable.securities.confirm.retirement.confirm";
	
	/** The Constant MSG LOANABLE SECURITIES REJECT OK. */
	public static final String MSG_LOANABLE_SECURITIES_REJECT_OK="msg.loanable.securities.reject.ok";
	
	/** The Constant MSG RETIREMENT LOANABLE SECURITIES REJECT OK. */
	public static final String MSG_RETIREMENT_LOANABLE_SECURITIES_REJECT_OK="msg.loanable.securities.reject.retirement.ok";
	
	/** The Constant MSG LOANABLE SECURITIES CONFIRM OK. */
	public static final String MSG_LOANABLE_SECURITIES_CONFIRM_OK="msg.loanable.securities.confirm.ok";
	
	/** The Constant MSG RETIREMENT LOANABLE SECURITIES CONFIRM OK. */
	public static final String MSG_RETIREMENT_LOANABLE_SECURITIES_CONFIRM_OK="msg.loanable.securities.confirm.retirement.ok";
	
	/** The Constant MSG LOANABLE SECURITIES INSUFFFICIENT BALANCE LOANABLE OPERATION. */
	public final static String MSG_LOANABLE_SECURITIES_INSUFFFICIENT_BALANCE_LOANABLE_OPERATION="msg.loanable.securities.validation.insufficient.balance.loanable.operation";
	
	/**  START ASSIGNMENT ACCOUNT *. */
	/** The Constant ASSIGNMENT_ACCOUNT EXPIRED SETTLEMENT DATE. */
	public static final String ASSIGNMENT_ACCOUNT_EXPIRED_SETTLEMENT_DATE = "msg.assignment.account.expired.settlement.date";
	
	/** The Constant ASSIGNMENT_ACCOUNT_AP_INVALID_CLOSE. */
	public static final String ASSIGNMENT_ACCOUNT_AP_INVALID_CLOSE = "msg.assignment.account.assignmentprocess.invalid.close";
	
	/** The Constant ASSIGNMENT_ACCOUNT_AP_CLOSED_STATE. */
	public static final String ASSIGNMENT_ACCOUNT_AP_CLOSED_STATE = "msg.assignment.account.assignmentprocess.closed.state";
	
	/** The Constant ASSIGNMENT_ACCOUNT_AP_RECORD_NOT_SELECTED. */
	public static final String ASSIGNMENT_ACCOUNT_AP_RECORD_NOT_SELECTED = "msg.assignment.account.assignmentprocess.record.not.selected";
	
	/** The Constant ASSIGNMENT_ACCOUNT_AP_ASK_CLOSE. */
	public static final String ASSIGNMENT_ACCOUNT_AP_ASK_CLOSE = "msg.assignment.account.assignmentprocess.askClose";
	
	/** The Constant ASSIGNMENT_ACCOUNT_AP_SUCCESS_CLOSE. */
	public static final String ASSIGNMENT_ACCOUNT_AP_SUCCESS_CLOSE = "msg.assignment.account.assignmentprocess.close";
	
	/** The Constant ASSIGNMENT_ACCOUNT_INCONSISTENCES. */
	public static final String ASSIGNMENT_ACCOUNT_INCONSISTENCES = "msg.assignment.account.inconsistences";
	
	/** The Constant ASSIGNMENT_ACCOUNT_INCONSISTENCES. */
	public static final String ASSIGNMENT_ACCOUNT_INCONSISTENCES_GENERAL = "msg.assignment.account.inconsistences.general";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PA_CLOSED_STATE. */
	public static final String ASSIGNMENT_ACCOUNT_PA_CLOSED_STATE = "msg.assignment.account.participantassignment.closed.state";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PA_OPENED_STATE. */
	public static final String ASSIGNMENT_ACCOUNT_PA_OPENED_STATE = "msg.assignment.account.participantassignment.opened.state";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PA_ASK_CLOSE. */
	public static final String ASSIGNMENT_ACCOUNT_PA_ASK_CLOSE = "msg.assignment.account.participantassignment.askClose";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PA_ASK_OPEN. */
	public static final String ASSIGNMENT_ACCOUNT_PA_ASK_OPEN = "msg.assignment.account.participantassignment.askOpen";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PA_SUCCESS_CLOSE. */
	public static final String ASSIGNMENT_ACCOUNT_PA_SUCCESS_CLOSE = "msg.assignment.account.participantassignment.close";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PA_SUCCESS_OPEN. */
	public static final String ASSIGNMENT_ACCOUNT_PA_SUCCESS_OPEN = "msg.assignment.account.participantassignment.open";
	
	/** The Constant ASSIGNMENT_ACCOUNT_HOLDER_BLOCKED. */
	public static final String ASSIGNMENT_ACCOUNT_HOLDER_BLOCKED = "msg.assignment.account.holder.blocked";
	
	/** The Constant ASSIGNMENT_ACCOUNT_HOLDER_SELECT. */
	public static final String ASSIGNMENT_ACCOUNT_HOLDER_SELECT = "msg.assignment.account.holder.select";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PARTICIPANT_REQUIRED. */
	public static final String ASSIGNMENT_ACCOUNT_PARTICIPANT_REQUIRED = "msg.assignment.account.participant.required";
	
	/** The Constant ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS. */
	public static final String ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS = "msg.assignment.account.holder.error.accounts";
	
	/** The Constant ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE. */
	public static final String ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE = "msg.assignment.account.holder.trade.error.accounts";
	
	/** The Constant ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE. */
	public static final String ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE = "msg.assignment.account.holder.incharge.error.accounts";
	
	/** The Constant ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE_AGREEMENT. */
	public static final String ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE_AGREEMENT = "msg.assignment.account.holder.trade.error.accounts.agreement";
	
	/** The Constant ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE_AGREEMENT. */
	public static final String ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE_AGREEMENT = "msg.assignment.account.holder.incharge.error.accounts.agreement";
	
	/** The Constant ASSIGNMENT_ACCOUNT_NOT_SELECTED. */
	public static final String ASSIGNMENT_ACCOUNT_NOT_SELECTED = "msg.assignment.account.not.selected";
	
	/** The Constant ASSIGNMENT_ACCOUNT_SAME_PARTICIPANTS. */
	public static final String ASSIGNMENT_ACCOUNT_SAME_PARTICIPANTS = "msg.assignment.account.same.participants";
	
	/** The Constant ASSIGNMENT_ACCOUNT_PARTICIPANT_INCHARGE_ROLE_EXIST. */
	public static final String ASSIGNMENT_ACCOUNT_PARTICIPANT_INCHARGE_ROLE_EXIST = "msg.assignment.account.participant.incharge.role.exist";
	
	/** The Constant ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_COMBINATION. */
	public static final String ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_COMBINATION = "msg.assignment.account.invalid.incharge.combination";
	
	/** The Constant ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_PARTICIPANT_COMBINATION. */
	public static final String ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_PARTICIPANT_COMBINATION = "msg.assignment.account.invalid.incharge.participant.combination";
	
	/** The Constant ASSIGNMENT_ACCOUNT_EXCEEDED_QUANTITY. */
	public static final String ASSIGNMENT_ACCOUNT_EXCEEDED_QUANTITY = "msg.assignment.account.exceeded.quantity";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ASK_REMOVE. */
	public static final String ASSIGNMENT_ACCOUNT_ASK_REMOVE = "msg.assignment.account.askRemove";
	
	/** The Constant ASSIGNMENT_ACCOUNT_REMOVE_NOT_SELECTED. */
	public static final String ASSIGNMENT_ACCOUNT_REMOVE_NOT_SELECTED = "msg.assignment.account.remove.not.selected";
	
	/** The Constant ASSIGNMENT_ACCOUNT_NOT_SAME_QUANTITY_OPERATION. */
	public static final String ASSIGNMENT_ACCOUNT_NOT_SAME_QUANTITY_OPERATION = "msg.assignment.account.not.same.quantity.operation";
	
	/** The Constant ASSIGNMENT_ACCOUNT_NOT_SAME_QUANTITY_OPERATION. */
	public static final String ASSIGNMENT_ACCOUNT_NOT_MARKEFACT_PARTICIPANT = "msg.assignment.account.marketfact.not.holder";
	
	/** The Constant ASSIGNMENT_ACCOUNT_INVALID_QUANTITY_OPERATION. */
	public static final String ASSIGNMENT_ACCOUNT_INVALID_QUANTITY_OPERATION = "msg.assignment.account.invalid.quantity.operation";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ASK_REGISTER. */
	public static final String ASSIGNMENT_ACCOUNT_ASK_REGISTER = "msg.assignment.account.askRegister";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ASK_MODIFY. */
	public static final String ASSIGNMENT_ACCOUNT_ASK_MODIFY = "msg.assignment.account.askModify";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ASK_CONFIRM. */
	public static final String ASSIGNMENT_ACCOUNT_ASK_CONFIRM = "msg.assignment.account.askConfirm";
	
	/** The Constant ASSIGNMENT_ACCOUNT_SUCCESS_REGISTER. */
	public static final String ASSIGNMENT_ACCOUNT_SUCCESS_REGISTER = "msg.assignment.account.register";
	
	/** The Constant ASSIGNMENT_ACCOUNT_SUCCESS_MODIFY. */
	public static final String ASSIGNMENT_ACCOUNT_SUCCESS_MODIFY = "msg.assignment.account.modify";
	
	/** The Constant ASSIGNMENT_ACCOUNT_SUCCESS_CONFIRM. */
	public static final String ASSIGNMENT_ACCOUNT_SUCCESS_CONFIRM = "msg.assignment.account.confirm";
	
	/** The Constant ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNTS. */
	public static final String ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNTS = "msg.assignment.account.remove.accounts"; 
	
	/** The Constant ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNT. */
	public static final String ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNT = "msg.assignment.account.remove.account";
	
	/** The Constant ASSIGNMENT_ACCOUNT_INCHARGE_CONFIRMED. */
	public static final String ASSIGNMENT_ACCOUNT_INCHARGE_CONFIRMED = "msg.assignment.account.incharge.confirm";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ROLE_EXIST. */
	public static final String ASSIGNMENT_ACCOUNT_ROLE_EXIST = "msg.assignment.account.role.exist";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ACCOUNT_EXIST. */
	public static final String ASSIGNMENT_ACCOUNT_ACCOUNT_EXIST = "msg.assignment.account.account.exist";
	
	/** The Constant ASSIGNMENT_ACCOUNT_MARKETFACT_QUANTITY_NOT_ENOUGH. */
	public static final String ASSIGNMENT_ACCOUNT_MARKETFACT_QUANTITY_NOT_ENOUGH = "msg.assignment.account.marketfact.quantity.not.enough";
	
	/** The Constant ASSIGNMENT_ACCOUNT_MARKETFACT_QUANTITY_EXCEEDED. */
	public static final String ASSIGNMENT_ACCOUNT_MARKETFACT_QUANTITY_EXCEEDED = "msg.assignment.account.marketfact.quantity.exceeded";
	
	/** The Constant ASSIGNMENT_ACCOUNT_MARKETFACT_PRICE_ZERO. */
	public static final String ASSIGNMENT_ACCOUNT_MARKETFACT_PRICE_ZERO= "msg.assignment.account.marketfact.price.zero";
	
	/** The Constant ASSIGNMENT_ACCOUNT_MARKETFACT_RATE_ZERO. */
	public static final String ASSIGNMENT_ACCOUNT_MARKETFACT_RATE_ZERO= "msg.assignment.account.marketfact.rate.zero";
	
	/** The Constant ASSIGNMENT_ACCOUNT_QUANTITY_EXCEEDED. */
	public static final String ASSIGNMENT_ACCOUNT_QUANTITY_EXCEEDED = "msg.assignment.account.quantity.exceeded";
	
	/** The Constant ASSIGNMENT_ACCOUNT_REPOSEC_QUANTITY_EXCEEDED. */
	public static final String ASSIGNMENT_ACCOUNT_REPOSEC_QUANTITY_EXCEEDED = "msg.assignment.account.repoSec.quantity.exceeded";
	
	/** The Constant ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY. */
	public static final String ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY = "msg.assignment.account.marketfact.empty";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ASKCHAINED_MASSIVE_EXITS. */
	public static final String ASSIGNMENT_ACCOUNT_ASKCHAINED_MASSIVE_EXITS = "msg.assignment.account.askChained.massive.exits";
	
	/** The Constant ASSIGNMENT_ACCOUNT_ASKCHAINED_ONE_EXITS. */
	public static final String ASSIGNMENT_ACCOUNT_ASKCHAINED_ONE_EXITS = "msg.assignment.account.askChained.one.exits";
	
	public static final String ASSIGNMENT_ACCOUNT_IN_CHARGE_PARTICIPANT_NO_EXISTS = "msg.assignment.account.in.charge.participant.no.exits";

	public static final String ASSIGNMENT_ACCOUNT_IN_CHARGE_ORIGIN_HOLDER_ACCOUNT_NO_EXISTS = "msg.assignment.account.in.charge.origin.holder.account.no.exits";
	
	public static final String ASSIGNMENT_ACCOUNT_IN_CHARGE_PARTICIPANT_HOLDER_ACCOUNT_NO_EXISTS = "msg.assignment.account.in.charge.participant.holder.account.no.exits";
	/**  END ASSIGNMENT ACCOUNT *. */
	
	/** START ASSIGNMENT REQUEST (INCHARGE) **/
	/** The Constant ASSIGNMENT REQUEST INVALID CONFIRM. */
	public static final String ASSIGNMENT_REQUEST_INVALID_CONFIRM = "alt.manage.incharge.invalidConfirm";
	
	/** The Constant ASSIGNMENT_REQUEST_INVALID_REJECT. */
	public static final String ASSIGNMENT_REQUEST_INVALID_REJECT = "alt.manage.incharge.invalidReject";
	
	/** The Constant ASSIGNMENT_REQUEST_RECORD_NOT_SELECTED. */
	public static final String ASSIGNMENT_REQUEST_RECORD_NOT_SELECTED = "alt.manage.incharge.record.not.selected";
	
	/** The Constant ASSIGNMENT_REQUEST_ASK_CONFIRM. */
	public static final String ASSIGNMENT_REQUEST_ASK_CONFIRM = "alt.manage.incharge.askConfirm";
	
	/** The Constant ASSIGNMENT_REQUEST_MASSIVE_ASK_CONFIRM. */
	public static final String ASSIGNMENT_REQUEST_MASSIVE_ASK_CONFIRM = "alt.manage.incharge.massive.askConfirm";
	
	/** The Constant ASSIGNMENT_REQUEST_ASK_REJECT. */
	public static final String ASSIGNMENT_REQUEST_ASK_REJECT = "alt.manage.incharge.askReject";
	
	/** The Constant ASSIGNMENT_REQUEST_MASSIVE_ASK_REJECT.  */
	public static final String ASSIGNMENT_REQUEST_MASSIVE_ASK_REJECT = "alt.manage.incharge.massive.askReject";
	
	/** The Constant ASSIGNMENT_REQUEST_SUCCESS_CONFIRM. */
	public static final String ASSIGNMENT_REQUEST_SUCCESS_CONFIRM = "alt.manage.incharge.confirm";
	
	/** The Constant ASSIGNMENT_REQUEST_SUCCESS_REJECT. */
	public static final String ASSIGNMENT_REQUEST_SUCCESS_REJECT = "alt.manage.incharge.reject";
	
	/** The Constant ASSIGNMENT_REQUEST_SUCCESS_CONFIRM. */
	public static final String ASSIGNMENT_MASSIVE_REQUEST_SUCCESS_CONFIRM = "alt.manage.incharge.massive.confirm";
	
	/** The Constant ASSIGNMENT_REQUEST_SUCCESS_REJECT. */
	public static final String ASSIGNMENT_MASSIVE_REQUEST_SUCCESS_REJECT = "alt.manage.incharge.massive.reject";
	
	/** The Constant ASSIGNMENT_REQUEST_HOLDER_BLOCKED. */
	public static final String ASSIGNMENT_REQUEST_HOLDER_BLOCKED = "alt.manage.incharge.holder.blocked";
	
	/** The Constant ASSIGNMENT_REQUEST_PARTICIPANT_REQUIRED. */
	public static final String ASSIGNMENT_REQUEST_PARTICIPANT_REQUIRED = "alt.manage.incharge.participant.required";
	
	/** The Constant ASSIGNMENT_REQUEST_HOLDER_NOT_ACCOUNTS. */
	public static final String ASSIGNMENT_REQUEST_HOLDER_NOT_ACCOUNTS = "alt.manage.incharge.holder.error.accounts";
	
	/** The Constant ASSIGNMENT_REQUEST_ACCOUNT_NOT_SELECTED. */
	public static final String ASSIGNMENT_REQUEST_ACCOUNT_NOT_SELECTED = "alt.manage.incharge.account.not.selected";
	
	/** The Constant ASSIGNMENT_REQUEST_NOT_SAME_HOLDERS. */
	public static final String ASSIGNMENT_REQUEST_NOT_SAME_HOLDERS = "alt.manage.incharge.not.same.holders";
	
	/**  END ASSIGNMENT REQUEST (INCHARGE) *. */
	
	public static final String REASSIGNMENT_REQUEST_NO_MODIFICATION = "msg.reassignment.no.modifications";
	
	/** The Constant REASSIGNMENT_REQUEST_NO_OPERATION_SELECTED. */
	public static final String REASSIGNMENT_REQUEST_NO_OPERATION_SELECTED = "msg.reassignment.no.operation.selected";
	
	/** The Constant REASSIGNMENT_REQUEST_NO_OPERATION_SELECTED. */
	public static final String REASSIGNMENT_REQUEST_NO_MARKETFACT_SELECTED = "msg.reassignment.no.marketfact.selected";
	
	/** The Constant REASSIGNMENT_REQUEST_NO_OPERATION_SELECTED. */
	public static final String REASSIGNMENT_REQUEST_NO_CHANGES_MARKETFACT = "msg.reassignment.no.changes.marketfact";
	
	/** The Constant REASSIGNMENT_REQUEST_INCORRECT_STATE. */
	public static final String REASSIGNMENT_REQUEST_INCORRECT_STATE = "msg.reassignment.incorrect.state";
	
	/** The Constant REASSIGNMENT_REQUEST_REGISTER_CONFIRM. */
	public static final String REASSIGNMENT_REQUEST_REGISTER_CONFIRM = "msg.reassignment.registration.confirm";
	
	/** The Constant REASSIGNMENT_REQUEST_REGISTER_SUCCESS. */
	public static final String REASSIGNMENT_REQUEST_REGISTER_SUCCESS = "msg.reassignment.registration.success";
	
	/** The Constant REASSIGNMENT_REQUEST_REJECT_CONFIRM. */
	public static final String REASSIGNMENT_REQUEST_REJECT_CONFIRM = "msg.reassignment.reject.confirm";
	
	/** The Constant REASSIGNMENT_REQUEST_REJECT_SUCCESS. */
	public static final String REASSIGNMENT_REQUEST_REJECT_SUCCESS = "msg.reassignment.reject.success";
	
	/** The Constant REASSIGNMENT_REQUEST_CONFIRM_CONFIRM. */
	public static final String REASSIGNMENT_REQUEST_CONFIRM_CONFIRM = "msg.reassignment.confirm.confirm";
	
	/** The Constant REASSIGNMENT_REQUEST_CONFIRM_CONFIRM. */
	public static final String REASSIGNMENT_REQUEST_CONFIRM_AUTHORIZE = "msg.reassignment.confirm.authorize";
	
	/** The Constant REASSIGNMENT_REQUEST_CONFIRM_SUCCESS. */
	public static final String REASSIGNMENT_REQUEST_CONFIRM_SUCCESS = "msg.reassignment.confirm.success";
	
	/** The Constant REASSIGNMENT_REQUEST_AUTHORIZE_SUCCESS. */
	public static final String REASSIGNMENT_REQUEST_AUTHORIZE_SUCCESS = "msg.reassignment.authorize.success";
	
	/** The Constant SIRTEX_OPERATION_REGISTER_CONFIRM. */
	/*START MESSAGES FOR SIRTEX OPERATION*/	
	public static final String SIRTEX_OPERATION_REGISTER_CONFIRM="sirtex.operation.register.confirm";

	/** The Constant SIRTEX_OPERATION_REGISTER_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_REGISTER_CONFIRM_OK="sirtex.operation.register.confirm.ok";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SIRTEX_OPERATION_APPROVE_CONFIRM="sirtex.operation.approve.confirm";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SIRTEX_OPERATION_APPROVE_CONFIRM_MULTIPLE="sirtex.operation.approve.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_APPROVE_CONFIRM_OK="sirtex.operation.approve.confirm.ok";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_APPROVE_CONFIRM_OK_MULTIPLE="sirtex.operation.approve.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_ANNULATE_CONFIRM. */
	public static final String SIRTEX_OPERATION_ANNULATE_CONFIRM="sirtex.operation.annulate.confirm";
	
	/** The Constant SIRTEX_OPERATION_ANNULATE_CONFIRM. */
	public static final String SIRTEX_OPERATION_ANNULATE_CONFIRM_MULTIPLE="sirtex.operation.annulate.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_ANNULATE_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_ANNULATE_CONFIRM_OK="sirtex.operation.annulate.confirm.ok";
	
	/** The Constant SIRTEX_OPERATION_ANNULATE_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_ANNULATE_CONFIRM_OK_MULTIPLE="sirtex.operation.annulate.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_REVIEW_CONFIRM. */
	public static final String SIRTEX_OPERATION_REVIEW_CONFIRM="sirtex.operation.review.confirm";
	
	/** The Constant SIRTEX_OPERATION_REVIEW_CONFIRM. */
	public static final String SIRTEX_OPERATION_REVIEW_CONFIRM_MULTIPLE="sirtex.operation.review.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_REVIEW_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_REVIEW_CONFIRM_OK="sirtex.operation.review.confirm.ok";
	
	/** The Constant SIRTEX_OPERATION_REVIEW_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_REVIEW_CONFIRM_OK_MULTIPLE="sirtex.operation.review.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_REJECT_CONFIRM. */
	public static final String SIRTEX_OPERATION_REJECT_CONFIRM="sirtex.operation.reject.confirm";
	
	/** The Constant SIRTEX_OPERATION_REJECT_CONFIRM. */
	public static final String SIRTEX_OPERATION_REJECT_CONFIRM_MULTIPLE="sirtex.operation.reject.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_REJECT_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_REJECT_CONFIRM_OK="sirtex.operation.reject.confirm.ok";
	
	/** The Constant SIRTEX_OPERATION_REJECT_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_REJECT_CONFIRM_OK_MULTIPLE="sirtex.operation.reject.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_CONFIRM_CONFIRM. */
	public static final String SIRTEX_OPERATION_CONFIRM_CONFIRM="sirtex.operation.confirm.confirm";
	
	/** The Constant SIRTEX_OPERATION_CONFIRM_CONFIRM. */
	public static final String SIRTEX_OPERATION_CONFIRM_CONFIRM_MULTIPLE="sirtex.operation.confirm.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_CONFIRM_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_CONFIRM_CONFIRM_OK="sirtex.operation.confirm.confirm.ok";
	
	/** The Constant SIRTEX_OPERATION_CONFIRM_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_CONFIRM_CONFIRM_OK_MULTIPLE="sirtex.operation.confirm.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_SETTLEMENT_CONFIRM. */
	public static final String SIRTEX_OPERATION_SETTLEMENT_CONFIRM="sirtex.operation.settlement.confirm";
	
	/** The Constant SIRTEX_OPERATION_SETTLEMENT_CONFIRM. */
	public static final String SIRTEX_OPERATION_SETTLEMENT_CONFIRM_MULTIPLE="sirtex.operation.settlement.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_SETTLEMENT_CONFIRM. */
	public static final String SIRTEX_OPERATION_SETTLEMENT_CONFIRM_MULTIPLE_UP="sirtex.operation.settlement.confirm.multiple.up";
	
	/** The Constant SIRTEX_OPERATION_SETTLEMENT_CONFIRM. */
	public static final String SIRTEX_OPERATION_SETTLEMENT_CONFIRM_MULTIPLE_EQUAL="sirtex.operation.settlement.confirm.multiple.equal";
	
	/** The Constant SIRTEX_OPERATION_SETTLEMENT_CONFIRM_SECURITY. */
	public static final String SIRTEX_OPERATION_SETTLEMENT_CONFIRM_SECURITY="sirtex.operation.settlement.confirm.security";
	
	/** The Constant SIRTEX_OPERATION_SETTLEMENT_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_SETTLEMENT_CONFIRM_OK="sirtex.operation.settlement.confirm.ok";
	
	/** The Constant SIRTEX_OPERATION_SETTLEMENT_CONFIRM_OK. */
	public static final String SIRTEX_OPERATION_SETTLEMENT_CONFIRM_OK_MULTIPLE="sirtex.operation.settlement.confirm.ok.multiple";

	/** The Constant SIRTEX_SETTLEMENT_ANTICIPATED_CONFIRM. */
	public static final String SIRTEX_SETTLEMENT_ANTICIPATED_CONFIRM="sirtex.settlement.anticipated.confirm";
	
	/** The Constant SIRTEX_SETTLEMENT_ANTICIPATED_CONFIRM_OK. */
	public static final String SIRTEX_SETTLEMENT_ANTICIPATED_CONFIRM_OK="sirtex.settlement.anticipated.confirm.ok";	
	
	/** The Constant SIRTEX_SETTLEMENT_ANTICIPATED_CONFIRM_OK. */
	public static final String SIRTEX_SETTLEMENT_CUI_PERSON_TYPE="operation.sirtex.holderaccount.validation.holder.type";	
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SELAR_OPERATION_APPROVE_CONFIRM_MULTIPLE="selar.operation.approve.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SELAR_OPERATION_ANNULATE_CONFIRM_MULTIPLE="selar.operation.annulate.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SELAR_OPERATION_REVIEW_CONFIRM_MULTIPLE="selar.operation.review.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SELAR_OPERATION_REJECT_CONFIRM_MULTIPLE="selar.operation.reject.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SELAR_OPERATION_CONFIRM_CONFIRM_MULTIPLE="selar.operation.confirm.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM. */
	public static final String SELAR_OPERATION_AUTHORIZED_CONFIRM_MULTIPLE="selar.operation.authorized.confirm.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SELAR_OPERATION_APPROVE_CONFIRM_OK_MULTIPLE="selar.operation.approve.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SELAR_OPERATION_ANNULATE_CONFIRM_OK_MULTIPLE="selar.operation.annulate.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SELAR_OPERATION_REVIEW_CONFIRM_OK_MULTIPLE="selar.operation.review.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SELAR_OPERATION_REJECT_CONFIRM_OK_MULTIPLE="selar.operation.reject.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SELAR_OPERATION_CONFIRM_CONFIRM_OK_MULTIPLE="selar.operation.confirm.confirm.ok.multiple";
	
	/** The Constant SIRTEX_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String SELAR_OPERATION_AUTHORIZED_CONFIRM_OK_MULTIPLE="selar.operation.authorized.confirm.ok.multiple";
	
	/** The Constant SELAR_OPERATION_REVIEW_ERROR_MULTIPLE. */
	public static final String SELAR_OPERATION_REVIEW_ERROR_MULTIPLE="selar.operation.review.confirm.error.multiple";
	
	/** The Constant RENEW_SIRTEX_REGISTER_CONFIRM. */
	public static final String RENEW_SIRTEX_REGISTER_CONFIRM="renew.sirtex.register.confirm";

	/** The Constant RENEW_SIRTEX_REGISTER_CONFIRM_OK. */
	public static final String RENEW_SIRTEX_REGISTER_CONFIRM_OK="renew.sirtex.register.confirm.ok";
	
	/** The Constant RENEW_SIRTEX_APPROVE_CONFIRM. */
	public static final String RENEW_SIRTEX_APPROVE_CONFIRM="renew.sirtex.approve.confirm";
	
	/** The Constant RENEW_SIRTEX_APPROVE_CONFIRM_OK. */
	public static final String RENEW_SIRTEX_APPROVE_CONFIRM_OK="renew.sirtex.approve.confirm.ok";
	
	/** The Constant RENEW_SIRTEX_ANNULATE_CONFIRM. */
	public static final String RENEW_SIRTEX_ANNULATE_CONFIRM="renew.sirtex.annulate.confirm";
	
	/** The Constant RENEW_SIRTEX_ANNULATE_CONFIRM_OK. */
	public static final String RENEW_SIRTEX_ANNULATE_CONFIRM_OK="renew.sirtex.annulate.confirm.ok";
	
	/** The Constant RENEW_SIRTEX_REVIEW_CONFIRM. */
	public static final String RENEW_SIRTEX_REVIEW_CONFIRM="renew.sirtex.review.confirm";
	
	/** The Constant RENEW_SIRTEX_REVIEW_CONFIRM_OK. */
	public static final String RENEW_SIRTEX_REVIEW_CONFIRM_OK="renew.sirtex.review.confirm.ok";
	
	/** The Constant RENEW_SIRTEX_REJECT_CONFIRM. */
	public static final String RENEW_SIRTEX_REJECT_CONFIRM="renew.sirtex.reject.confirm";
	
	/** The Constant RENEW_SIRTEX_REJECT_CONFIRM_OK. */
	public static final String RENEW_SIRTEX_REJECT_CONFIRM_OK="renew.sirtex.reject.confirm.ok";
	
	/** The Constant RENEW_SIRTEX_CONFIRM_CONFIRM. */
	public static final String RENEW_SIRTEX_CONFIRM_CONFIRM="renew.sirtex.confirm.confirm";
	
	/** The Constant RENEW_SIRTEX_CONFIRM_CONFIRM_OK. */
	public static final String RENEW_SIRTEX_CONFIRM_CONFIRM_OK="renew.sirtex.confirm.confirm.ok";
	
	/** Messages for OTC dpf operation. */
	public static final String OTC_DPF_OPERATION_REGISTER_CONFIRM="otcdpf.operation.register.confirm";

	/** The Constant OTC_DPF_OPERATION_REGISTER_CONFIRM_OK. */
	public static final String OTC_DPF_OPERATION_REGISTER_CONFIRM_OK="otcdpf.operation.register.confirm.ok";
	
	/** The Constant OTC_DPF_OPERATION_APPROVE_CONFIRM. */
	public static final String OTC_DPF_OPERATION_APPROVE_CONFIRM="otcdpf.operation.approve.confirm";
	
	/** The Constant OTC_DPF_OPERATION_APPROVE_CONFIRM_OK. */
	public static final String OTC_DPF_OPERATION_APPROVE_CONFIRM_OK="otcdpf.operation.approve.confirm.ok";
	
	/** The Constant OTC_DPF_OPERATION_ANNULATE_CONFIRM. */
	public static final String OTC_DPF_OPERATION_ANNULATE_CONFIRM="otcdpf.operation.annulate.confirm";
	
	/** The Constant OTC_DPF_OPERATION_ANNULATE_CONFIRM_OK. */
	public static final String OTC_DPF_OPERATION_ANNULATE_CONFIRM_OK="otcdpf.operation.annulate.confirm.ok";
	
	/** The Constant OTC_DPF_OPERATION_REVIEW_CONFIRM. */
	public static final String OTC_DPF_OPERATION_REVIEW_CONFIRM="otcdpf.operation.review.confirm";
	
	/** The Constant OTC_DPF_OPERATION_REVIEW_CONFIRM_OK. */
	public static final String OTC_DPF_OPERATION_REVIEW_CONFIRM_OK="otcdpf.operation.review.confirm.ok";
	
	/** The Constant OTC_DPF_OPERATION_REJECT_CONFIRM. */
	public static final String OTC_DPF_OPERATION_REJECT_CONFIRM="otcdpf.operation.reject.confirm";
	
	/** The Constant OTC_DPF_OPERATION_REJECT_CONFIRM_OK. */
	public static final String OTC_DPF_OPERATION_REJECT_CONFIRM_OK="otcdpf.operation.reject.confirm.ok";
	
	/** The Constant OTC_DPF_OPERATION_CONFIRM_CONFIRM. */
	public static final String OTC_DPF_OPERATION_CONFIRM_CONFIRM="otcdpf.operation.confirm.confirm";
	
	/** The Constant OTC_DPF_OPERATION_CONFIRM_CONFIRM_OK. */
	public static final String OTC_DPF_OPERATION_CONFIRM_CONFIRM_OK="otcdpf.operation.confirm.confirm.ok";
	
	/** End Messages for OTC dpf operation. */
	
	
	/** START BANK **/
	/** The Constant BANK MNEMONIC SIZE MINIMUN. */
	public static final String BANK_MNEMONIC_SIZE_MINIMUN = "msg.banks.mnemonic.size.minimun";
	
	/** The Constant BANK MNEMONIC REGISTER. */
	public static final String BANK_MNEMONIC_REGISTER = "msg.banks.mnemonic.register";
	
	/** The Constant BANK TYPE REGISTER. */
	public static final String BANK_TYPE_REGISTER = "msg.banks.type.register";
	
	/** The Constant BANK BIC CODE REGISTER. */
	public static final String BANK_BIC_CODE_REGISTER = "msg.banks.bic.code.register";
	
	/** The Constant  BANK VALIDATION PHONE ONLY ZERO. */
	public static final String BANK_VALIDATION_PHONE_ONLY_ZERO = "msg.banks.validation.phone.only.zero";
	
	/** The Constant  BANK VALIDATION WEB PAGE FORMAT. */
	public static final String BANK_VALIDATION_WEB_PAGE_FORMAT = "msg.banks.validation.web.page.format";
	
	/** The Constant BANK DOCUMENT REGISTER. */
	public static final String BANK_DOCUMENT_REGISTER = "msg.banks.document.register";
	
	/** The Constant BANK VALIDATION ADDRESS FORMAT. */
	public static final String BANK_VALIDATION_ADDRESS_FORMAT = "msg.banks.validation.address.format";
	
	/** The Constant  BANK VALIDATION FAX ONLY ZERO. */
	public static final String BANK_VALIDATION_FAX_ONLY_ZERO = "msg.banks.validation.fax.only.zero";
	
	/** The Constant  BANK VALIDATION CONTACT EMAIL FORMAT. */
	public static final String BANK_VALIDATION_CONTACT_EMAIL_FORMAT = "msg.banks.validation.contactemail.format";
	
	/** The Constant BANK REGISTER. */
	public static final String BANK_REGISTER="msg.banks.register.confirm";
	
	/** The Constant BANK MODIFY. */
	public static final String BANK_MODIFY="msg.banks.modify.confirm";
	
	/** The Constant BANK BLOCK. */
	public static final String BANK_BLOCK_CONFIRM="msg.banks.block.confirm";
	
	/** The Constant BANK UNBLOCK. */
	public static final String BANK_UNBLOCK_CONFIRM="msg.banks.unblock.confirm";
	
	/** The Constant BANK REGISTER OK. */
	public static final String BANK_REGISTER_OK="msg.banks.register.ok";
	
	/** The Constant BANK REGISTER OK. */
	public static final String BANK_BLOCK_OK="msg.banks.block.ok";
	
	/** The Constant BANK REGISTER OK. */
	public static final String BANK_UNBLOCK_OK="msg.banks.unblock.ok";
	
	/** The Constant BANK MODIFY OK. */
	public static final String BANK_MODIFY_OK="msg.banks.modify.ok";
	
	/** The Constant BANK VALIDATION REGISTERED . */
	public static final String BANK_VALIDATION_REGISTERED="msg.banks.validation.registered";
	
	/** The Constant BANK VALIDATION BLOCKED . */
	public static final String BANK_VALIDATION_BLOCKED="msg.banks.validation.blocked";
	
	/**  END BANK *. */
	
	// SETTLEMENT REQUEST
	
	
	public static final String MSG_SETTLEMENT_REQUEST_ACCOUNT_LESS_QTY="msg.settlement.dateExchange.account.lessQty";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_OPERATION_CROSS_DIFFERS. */
	public static final String MSG_SETTLEMENT_REQUEST_OPERATION_CROSS_DIFFERS="msg.settlement.dateExchange.operation.cross.differs";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_ACCOUNT_NOT_SELECTED. */
	public static final String MSG_SETTLEMENT_REQUEST_ACCOUNT_NOT_SELECTED="msg.settlement.dateExchange.accounts.not.selected";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_ACCOUNT_QTY_ZERO. */
	public static final String MSG_SETTLEMENT_REQUEST_ACCOUNT_QTY_ZERO="msg.settlement.dateExchange.account.quantity.zero";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_ACCOUNT_QTY_ZERO_REVIEW. */
	public static final String MSG_SETTLEMENT_REQUEST_ACCOUNT_QTY_ZERO_REVIEW="msg.settlement.dateExchange.account.quantity.zero.review"; 
	
	/** The Constant MSG_SETTLEMENT_REQUEST_DAYS_EMPTY. */
	public static final String MSG_SETTLEMENT_REQUEST_DAYS_EMPTY="msg.settlement.dateExchange.days.empty";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_NEW_SETT_DATE_GREATER_CASH. */
	public static final String MSG_SETTLEMENT_REQUEST_NEW_SETT_DATE_GREATER_CASH="msg.settlement.dateExchange.newSettDate.greater.cash";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_NEW_SETT_DATE_GREATER. */
	public static final String MSG_SETTLEMENT_REQUEST_NEW_SETT_DATE_GREATER="msg.settlement.dateExchange.newSettDate.greater";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_ACCOUNT_INVALID_QTY. */
	public static final String MSG_SETTLEMENT_REQUEST_ACCOUNT_INVALID_QTY="msg.settlement.dateExchange.account.invQuantity";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_OPERATION_NOT_SELECTED. */
	public static final String MSG_SETTLEMENT_REQUEST_OPERATION_NOT_SELECTED="msg.settlement.dateExchange.operation.not.selected";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_CONFIRM_REGISTER. */
	public static final String MSG_SETTLEMENT_REQUEST_CONFIRM_REGISTER="msg.settlement.dateExchange.askRegister";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_CONFIRM_CONFIRM. */
	public static final String MSG_SETTLEMENT_REQUEST_CONFIRM_CONFIRM="msg.settlement.dateExchange.askConfirm";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_CONFIRM_ANNUL. */
	public static final String MSG_SETTLEMENT_REQUEST_CONFIRM_ANNUL="msg.settlement.dateExchange.askAnnul";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_CONFIRM_APPROVAL. */
	public static final String MSG_SETTLEMENT_REQUEST_CONFIRM_APPROVAL="msg.settlement.dateExchange.askApprove";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_CONFIRM_AUTHORIZE. */
	public static final String MSG_SETTLEMENT_REQUEST_CONFIRM_AUTHORIZE="msg.settlement.dateExchange.askAuthorize";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_CONFIRM_REJECT. */
	public static final String MSG_SETTLEMENT_REQUEST_CONFIRM_REJECT="msg.settlement.dateExchange.askReject";
	
	/** The Constant MSG_SETTLEMENT_REQUEST_CONFIRM_REVIEW. */
	public static final String MSG_SETTLEMENT_REQUEST_CONFIRM_REVIEW="msg.settlement.dateExchange.askReview";
	
	/**  START BANK ACCOUNTS*. */
	
	/** The Constant ISSUER VALIDATION REGISTERED . */
	public static final String ISSUER_VALIDATION_REGISTERED="msg.banks.accounts.issuer.validation.registered";
	
	/** The Constant BANK ACCOUNTS REGISTER. */
	public static final String BANK_ACCOUNTS_REGISTER="msg.banks.accounts.register.confirm";
	
	/** The Constant BANK ACCOUNTS MODIFY. */
	public static final String BANK_ACCOUNTS_MODIFY="msg.banks.accounts.modify.confirm";
	
	/** The Constant BANK ACCOUNTS VALIDATE ACCOUNT CURRENCY. */
	public static final String BANK_ACCOUNTS_VALIDATE_ACCOUNT_CURRENCY="msg.banks.accounts.validate.account.currency";
	
	/** The Constant BANK ACCOUNTS VALIDATE ACCOUNT NUMBER CURRENCY. */
	public static final String BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER_CURRENCY="msg.banks.accounts.validate.account.number.currency";
	
	/** The Constant BANK ACCOUNTS VALIDATE BANK CENTRALIZING. */
	public static final String BANK_ACCOUNTS_VALIDATE_BANK_CENTRALIZING="msg.banks.accounts.validate.bank.centralizing";
	
	/** The Constant BANK ACCOUNTS REGISTER OK. */
	public static final String BANK_ACCOUNTS_REGISTER_OK="msg.banks.accounts.register.ok";
	
	/** The Constant BANK ACCOUNTS REGISTER OK. */
	public static final String BANK_ACCOUNTS_BLOCK_OK="msg.banks.accounts.block.ok";
	
	/** The Constant BANK ACCOUNTS REGISTER OK. */
	public static final String BANK_ACCOUNTS_UNBLOCK_OK="msg.banks.accounts.unblock.ok";
	
	/** The Constant BANK ACCOUNTS MODIFY OK. */
	public static final String BANK_ACCOUNTS_MODIFY_OK="msg.banks.accounts.modify.ok";
	
	/** The Constant BANK ACCOUNTS VALIDATION REGISTERED . */
	public static final String BANK_ACCOUNTS_VALIDATION_REGISTERED="msg.banks.accounts.validation.registered";
	
	/** The Constant BANK ACCOUNTS VALIDATION BLOCKED . */
	public static final String BANK_ACCOUNTS_VALIDATION_BLOCKED="msg.banks.accounts.validation.blocked";
	
	/** The Constant BANK ACCOUNTS BLOCK. */
	public static final String BANK_ACCOUNTS_BLOCK_CONFIRM="msg.banks.accounts.block.confirm";
	
	/** The Constant BANK ACCOUNTS UNBLOCK. */
	public static final String BANK_ACCOUNTS_UNBLOCK_CONFIRM="msg.banks.accounts.unblock.confirm";
	
	/**  END BANK ACCOUNTS*. */
	/** START EFFECTIVE ACCOUNTS**/
	/** The Constant EFFECTIVE ACCOUNTS CURRENCY NOT SELECTED . */
	public static final String EFFECTIVE_ACCOUNTS_CURRENCY_NOT_SELECTED="msg.effective.accounts.currency.not.selected";
	
	/** The Constant BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER. */
	public static final String BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER="msg.banks.accounts.validate.account.number";
	
	/**  START EFFECTIVE ACCOUNTS*. */
	/** The Constant ERROR_MESSAGE_INVALID_USER_CONFIRM. */
	public static final String ERROR_MESSAGE_INVALID_USER_CONFIRM = "error.message.invalid.user.confirm";
	/** The Constant ERROR_MESSAGE_INVALID_USER_ANNUL. */
	public static final String ERROR_MESSAGE_INVALID_USER_ANNUL = "error.message.invalid.user.annul";
	/** The Constant ERROR_MESSAGE_INVALID_USER_REVISE. */
	public static final String ERROR_MESSAGE_INVALID_USER_REVISE = "error.message.invalid.user.revise";
	/** The Constant ERROR_MESSAGE_INVALID_USER_APPROVE. */
	public static final String ERROR_MESSAGE_INVALID_USER_APPROVE = "error.message.invalid.user.approve";
	/** The Constant ERROR_MESSAGE_INVALID_USER_MODIFY. */
	public static final String ERROR_MESSAGE_INVALID_USER_MODIFY = "error.message.invalid.user.modify";
	/** The Constant ERROR_MESSAGE_INVALID_USER_ACTIVE. */
	public static final String ERROR_MESSAGE_INVALID_USER_ACTIVE = "error.message.invalid.user.active";
	/** The Constant ERROR_MESSAGE_INVALID_USER_DESACTIVE. */
	public static final String ERROR_MESSAGE_INVALID_USER_DESACTIVE = "error.message.invalid.user.desactive";
	/** The Constant ERROR_MESSAGE_INVALID_USER_REJECT. */
	public static final String ERROR_MESSAGE_INVALID_USER_REJECT = "error.message.invalid.user.reject";
	/** The Constant ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT. */
	public static final String ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT = "error.message.issuer.dpf.not.have.participant";
	
	/** The Constant ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_HOLDER_BUYER. */
	public static final String ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_HOLDER_BUYER = "error.message.issuer.dpf.not.have.holder.buyer";
	
	/** The Constant ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_HOLDER_SALE. */
	public static final String ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_HOLDER_SALE = "error.message.issuer.dpf.not.have.holder.sale";
	
	/** The Constant ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_SECURITY_PK. */
	public static final String ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_SECURITY_PK = "error.message.issuer.dpf.not.have.security.pk";
	
	/**  START SETTLEMENT SCHEDULE*. */
	public static final String ALT_SCHEDULES_SELECTED_WRONG="schedule.settlement.alt.schedules.wrong";
	
	/** The Constant ALT_SCHEDULE_ALREADY_EXISTS. */
	public static final String ALT_SCHEDULE_ALREADY_EXISTS="schedule.settlement.alt.schedule.exists";
	
	/** The Constant ALT_SCHEDULE_REGISTER_SUCCESS. */
	public static final String ALT_SCHEDULE_REGISTER_SUCCESS="schedule.settlement.alt.schedule.register.success";
	
	/** The Constant ALT_SCHEDULE_MODIFY_SUCCESS. */
	public static final String ALT_SCHEDULE_MODIFY_SUCCESS="schedule.settlement.alt.schedule.modify.success";
	
	/** The Constant MSG_SCHEDULE_PRE_CONFIRM. */
	public static final String MSG_SCHEDULE_PRE_CONFIRM="schedule.settlement.msg.pre.confirm";
	
	/** The Constant MSG_SCHEDULE_PRE_MODIFY. */
	public static final String MSG_SCHEDULE_PRE_MODIFY="schedule.settlement.msg.pre.modify";
	
	/** The Constant MSG_SCHEDULE_SAME_DATES. */
	public static final String MSG_SCHEDULE_SAME_DATES="schedule.settlement.alt.same.dates";
	
	/** The Constant MSG_SCHEDULE_FINAL_BEFORE_INITIAL_DATES. */
	public static final String MSG_SCHEDULE_FINAL_BEFORE_INITIAL_DATES="schedule.settlement.alt.dtFinal.before.dtInitial";
	
	/** The Constant MSG_SCHEDULE_INITIAL_AFTER_FINAL_DATES. */
	public static final String MSG_SCHEDULE_INITIAL_AFTER_FINAL_DATES="schedule.settlement.alt.dtInitial.after.dtFinal";
	
	/** The Constant MSG_SCHEDULE_FINAL_AFTER_INITIAL_DATES_CROSS. */
	public static final String MSG_SCHEDULE_FINAL_AFTER_INITIAL_DATES_CROSS="schedule.settlement.alt.dtFinal.after.dtInitial.cross";
	
	/** The Constant MSG_SCHEDULE_INITIAL_AFTER_FINAL_DATES_CROSS2. */
	public static final String MSG_SCHEDULE_INITIAL_AFTER_FINAL_DATES_CROSS2="schedule.settlement.alt.dtInitial.before.dtFinal.cross2";

	/** The Constant INTERNATIONAL_VALIDATION_DEMATERIALIZED_QUANTITY. */
	public static final String INTERNATIONAL_VALIDATION_DEMATERIALIZED_QUANTITY="international.validation.dematerializated.quantity";
	
	/**  END SETTLEMENT SCHEDULE*. */
	
	/** The Constant OTC_OPERATION_ACCOUNT_SELLER_CLOSED. */
	public static final String OTC_OPERATION_ACCOUNT_SELLER_CLOSED = "otc.operation.account.seller.closed";
	
	/** The Constant OTC_OPERATION_NO_ACCOUNTS_FOR_PARTICIPANT. */
	public static final String OTC_OPERATION_NO_ACCOUNTS_FOR_PARTICIPANT = "otc.operation.no.accounts.for.participant";
	
	/** The Constant MCN_MASSIVE_PRIMARY_PLACEMENT_ROLE_SALE. */
	public static final String MCN_MASSIVE_PRIMARY_PLACEMENT_ROLE_SALE = "mcn.msg.massive.primary.placement.role.sale";
	
	/** The Constant NOTIFICATION_MESSAGE_BCB_OPERATION_SIRTEX_PENDING. */
	public static final String NOTIFICATION_MESSAGE_BCB_OPERATION_SIRTEX_PENDING = "notification.message.bcb.operation.sirtex.pending";
	
	/** The Constant CHAINS_QUANTITY_CHAINED. */
	public static final String CHAINS_QUANTITY_CHAINED = "lbl.chains.quantity.chained";
	
	//TODO issue 1161
	public static final String MESSAGES_WARNING="messages.warning";
	public static final String MESSAGES_CONFIRM = "messages.confirm";
	public static final String MSG_CONFIRM_REGISTER_ASSIGNMENTOPEN="msg.confirm.register.assignmentOpen";
	public static final String MSG_CONFIRM_REGISTER_ASSIGNMENTOPEN_WITHOUTMCN="msg.confirm.register.assignmentOpen.withoutMCN"; 
	public static final String MSG_SUCCESFUL_REGISTER_ASSIGNMENTOPEN="msg.succesful.register.assignmentOpen";
	public static final String MSG_SEARCH_TO_REGISTER_VALIDATION="msg.search.to.register.validation";
	public static final String MSG_ERROR_ASSIGNMENTOPEN_REQUIRED_SELECTED="msg.error.assignmentOpen.required.selected";
	public static final String MSG_ERROR_ASSIGNMENTOPEN_REQUIRED_STATEREGISTERED="msg.error.assignmentOpen.required.stateRegistered";
	public static final String LBL_MSG_CONFIRM_REJECT_ASSIGNMENTOPEN="lbl.msg.confirm.reject.assignmentOpen";
	public static final String MSG_SUCCESFUL_REJECT_ASSIGNMENTOPEN="msg.succesful.reject.assignmentOpen";
	public static final String LBL_MSG_CONFIRM_CONFIRM_ASSIGNMENTOPEN="lbl.msg.confirm.confirm.assignmentOpen";
	public static final String MSG_SUCCESFUL_CONFIRM_ASSIGNMENTOPEN="msg.succesful.confirm.assignmentOpen";
	public static final String MSG_VALIDATION_REQUIRED_UPLOADFILE="msg.validation.required.uploadFile";
	public static final String MSG_ERROR_REQUIRED_CURRENDDATE_FOR_CONFIRMED="msg.error.required.currendDate.for.confirmed";
	public static final String MSG_ALERT_INFO_CHAINS_OPERATIONMCN="msg.alert.info.chains.operationMcn";
	public static final String MSG_ALERT_INFO_CHAINS_OPERATIONMCNDETAIL="msg.alert.info.chains.operationMcnDetail";
	public static final String MSG_ERROR_REQUIRED_CANCELCHAINED="msg.error.required.cancelChained";
	
	public static final String MSG_MAXIMUM_EXTENSION_DAYS = "msg.settlement.dateExchange.account.maximumExtensionDays";
}