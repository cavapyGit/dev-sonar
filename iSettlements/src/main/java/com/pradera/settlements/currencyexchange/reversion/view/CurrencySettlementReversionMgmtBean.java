package com.pradera.settlements.currencyexchange.reversion.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.CurrencySettlementOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.CurrencySettlementMotiveAnnulType;
import com.pradera.model.settlement.type.CurrencySettlementMotiveRejectType;
import com.pradera.model.settlement.type.SettlementCurrencyRequestStateType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.settlements.currencyexchange.facade.CurrencySettlementFacade;
import com.pradera.settlements.currencyexchange.to.SearchCurrencySettlementRequestTO;
import com.pradera.settlements.currencyexchange.to.SearchSettlementOperationTO;
import com.pradera.settlements.currencyexchange.to.SettlementRequestDetailTO;
import com.pradera.settlements.currencyexchange.to.SettlementRequestTO;
import com.pradera.settlements.utils.MotiveController;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CurrencySettlementReversionMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class CurrencySettlementReversionMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The selected currency settlement request. */
	private SettlementRequestTO selectedCurrencySettlementRequest;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The general parameter facade. */
	@EJB
	GeneralParametersFacade generalParameterFacade;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The currency settlement facade. */
	@EJB
	private CurrencySettlementFacade currencySettlementFacade; 
	
	/** The mcn operation service facade. */
	@EJB
	private McnOperationServiceFacade mcnOperationServiceFacade;

	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The search currency Settlement Request Data Model. */
	private GenericDataModel<SettlementRequestTO> currencySettlementRequestsDataModel;
		
	/** The search Holder Account Balance Data Model. */
	private GenericDataModel<SearchSettlementOperationTO> settlementOperationsDataModel;
		
	/** The search Currency Settlement Request Header TO. */
	private SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO;
	
	/** The search Currency Settlement Request TO. */
	private SearchCurrencySettlementRequestTO searchCurrencySettlementRequestHeaderTO;
	
	/** The lst selected settlement operations. */
	private List<SearchSettlementOperationTO> lstSelectedSettlementOperations;
	
	/** The mp parameters. */
	private Map<Integer,String> mpParameters = new HashMap<Integer, String>(); 
	
	/** The motive controller annul. */
	private MotiveController motiveControllerAnnul; 
	
	/** The motive controller reject. */
	private MotiveController motiveControllerReject;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The exchange rate. */
	private DailyExchangeRates exchangeRate;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		createObject();
		try {
			loadNegotiationMechanism();
			loadParticipantApplicantAndCounterpart();
			loadSecurityClass();
			loadCurrency();
			loadState();
			loadMotiveAnnul();
			loadMotiveReject();
			loadHolidays();
			loadUserValidation();
			loadRequestTypes();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load request types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadRequestTypes() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		//parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SETTLEMENT_REQUEST_TYPE.getCode());	
		List<ParameterTable> lstRequestTypes = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);	
		for(ParameterTable param : lstRequestTypes){
			mpParameters.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Load User Validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnReview(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		privilegeComponent.setBtnAuthorize(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
		}
	}
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParameterFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Load cbo modality and participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadNegotiationMechanism() throws ServiceException{
		// TODO Auto-generated method stub
		List<NegotiationMechanism> lstNegotiationMechanismAux = new ArrayList<NegotiationMechanism>();
		List<NegotiationMechanism> lstNegotiationMechanism = (List<NegotiationMechanism>)mcnOperationServiceFacade.getLstNegotiationMechanism(null);
		for(NegotiationMechanism negotiationMechanism : lstNegotiationMechanism){
			if(negotiationMechanism.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.BOLSA.getCode())
					||negotiationMechanism.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.OTC.getCode())){
				lstNegotiationMechanismAux.add(negotiationMechanism);
			}
		}
		searchCurrencySettlementRequestTO.setLstNegotiationMechanism(lstNegotiationMechanismAux);
	}
	
	/**
	 * Change Negotitation Mechanism.
	 */
	public void changeNegotitationMechanism(){
		JSFUtilities.hideGeneralDialogues();		
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())){
			cleanDataModel();
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestTO.getIdMechanismNegotiation())){
				try {
					searchCurrencySettlementRequestTO.setIdModalityNegotiation(null);
					 if(userInfo.getUserAccountSession().isParticipantInstitucion()){
						 searchCurrencySettlementRequestTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
									searchCurrencySettlementRequestTO.getIdMechanismNegotiation(), 
									searchCurrencySettlementRequestTO.getIdParticipant()));	
					 }else{
						 searchCurrencySettlementRequestTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModality(searchCurrencySettlementRequestTO.getIdMechanismNegotiation()));
					 }
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}else {
				searchCurrencySettlementRequestTO.setLstNegotiationModality(null);
			}
			
		}
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
			cleanModelRegister();
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestHeaderTO.getIdMechanismNegotiation())){
				try {
					searchCurrencySettlementRequestHeaderTO.setIdModalityNegotiation(null);
					 if(userInfo.getUserAccountSession().isParticipantInstitucion()) 
								searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
										searchCurrencySettlementRequestHeaderTO.getIdMechanismNegotiation(), 
										searchCurrencySettlementRequestHeaderTO.getIdParticipant()));				 
					 else{
						 searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModality(searchCurrencySettlementRequestHeaderTO.getIdMechanismNegotiation()));
					 }
							 
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}else searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(null);
			
		}
	}
	
	/**
	 * Create Object.
	 */
	public void createObject()
	{
		searchCurrencySettlementRequestTO = new SearchCurrencySettlementRequestTO(getCurrentSystemDate(),getCurrentSystemDate());
		searchCurrencySettlementRequestTO.setSecurity(new Security());
		searchCurrencySettlementRequestTO.setLstNegotiationMechanism(new ArrayList<NegotiationMechanism>());
		searchCurrencySettlementRequestTO.setLstNegotiationModality(new ArrayList<NegotiationModality>());
		searchCurrencySettlementRequestTO.setLstParticipant(new ArrayList<Participant>());
		searchCurrencySettlementRequestTO.setLstParticipantCounterpart(new ArrayList<Participant>());
		searchCurrencySettlementRequestTO.setLstSecurityClass(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestTO.setLstCurrency(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestTO.setLstState(new ArrayList<ParameterTable>());
		motiveControllerAnnul = new MotiveController();
		motiveControllerReject = new MotiveController();
	}
	
	/**
	 * Registry Currency Settlement Request Handler.
	 *
	 * @return the string
	 */
	public String registryCurrencySettlementRequestHandler()
	{
		try {
			exchangeRate = generalParameterFacade.getdailyExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());
			if(exchangeRate == null){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_EXCHANGE_RATE);
			}
			
			executeAction();
			cleanModelRegister();
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			createObjectRegister();
		
			searchCurrencySettlementRequestHeaderTO.setLstNegotiationMechanism(searchCurrencySettlementRequestTO.getLstNegotiationMechanism());
			searchCurrencySettlementRequestHeaderTO.setLstParticipant(searchCurrencySettlementRequestTO.getLstParticipant());
			searchCurrencySettlementRequestHeaderTO.setLstParticipantCounterpart(searchCurrencySettlementRequestTO.getLstParticipantCounterpart());
			searchCurrencySettlementRequestHeaderTO.setLstSecurityClass(searchCurrencySettlementRequestTO.getLstSecurityClass());
			searchCurrencySettlementRequestHeaderTO.setLstCurrency(searchCurrencySettlementRequestTO.getLstCurrency());
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));	
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestHeaderTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
			searchCurrencySettlementRequestHeaderTO.setIndUserParticipant(BooleanType.YES.getCode());
		}else {
			searchCurrencySettlementRequestHeaderTO.setIndUserParticipant(BooleanType.NO.getCode());
		}
		return "registerCurrencySettlementReversion";
	}
	
	/**
	 * Validate Securities.
	 */
	public void validateSecurities()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		cleanModelRegister();
		if (Validations.validateIsNullOrEmpty(searchCurrencySettlementRequestHeaderTO.getSecurity().getIdSecurityCodePk())) {
			searchCurrencySettlementRequestHeaderTO.getSecurity().setIdSecurityCodePk(null);		  
		   return;
		  }	
		// Get security from database.
		  Security security = null;
		  try {
			  security = currencySettlementFacade.getSecuritiesById(searchCurrencySettlementRequestHeaderTO.getSecurity().getIdSecurityCodePk());			 
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(security)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_SECURITY_NOT_EXIST) );			 
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
			  return;
		  }
		  if (!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_SECURITY_NOT_REGISTER) );			
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
			  return;
		  }
		  
		  searchCurrencySettlementRequestHeaderTO.setSecurity(security);
	}
	
	/**
	 * Validate Securities search.
	 */
	public void validateSecuritiesSearch()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		cleanModelRegister();
		if (Validations.validateIsNullOrEmpty(searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk())) {
			searchCurrencySettlementRequestTO.getSecurity().setIdSecurityCodePk(null);		  
		   return;
		  }	
		// Get security from database.
		  Security security = null;
		  try {
			  security = currencySettlementFacade.getSecuritiesById(searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk());			 
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(security)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_SECURITY_NOT_EXIST) );			 
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestTO.setSecurity(new Security());
		   return;
		  }
		  if (!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_SECURITY_NOT_REGISTER) );			
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestTO.setSecurity(new Security());
		   return;
		  }
		  
		  searchCurrencySettlementRequestTO.setSecurity(security);
	}
	
	/**
	 * Load Participant Role.
	 *
	 * @return the participant role
	 */
	public List<ParticipantRoleType> getParticipantRole(){	
		return ParticipantRoleType.list;	
	}
	
	/**
	 * Load State.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadState()throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY_SETTLEMENT_STATE.getCode());	
		searchCurrencySettlementRequestTO.setLstState(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));	
		
		for(ParameterTable state : searchCurrencySettlementRequestTO.getLstState()){
			mpParameters.put(state.getParameterTablePk(), state.getParameterName());
		}
	}
	
	/**
	 * Load Motive Annul.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveAnnul()throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY_SETTLEMENT_ANNUL_MOTIVE.getCode());	
		motiveControllerAnnul.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * Load Motive Reject.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveReject()throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY_SETTLEMENT_REJECT_MOTIVE.getCode());	
		motiveControllerReject.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * load Currency.
	 *
	 * @throws ServiceException  the service exception
	 */
	private void loadCurrency() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		List<ParameterTable> lstParameterTable = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstParameterTable){
			if(parameterTable.getParameterState().equals(ParameterTableStateType.REGISTERED.getCode()) && parameterTable.getIndicator2().equals(GeneralConstants.ONE_VALUE_STRING)){
				lstParameterTableAux.add(parameterTable);
			}
			mpParameters.put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
		}
		searchCurrencySettlementRequestTO.setLstCurrency(lstParameterTableAux);
		
	}
	
	/**
	 * load Security Class.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityClass() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		searchCurrencySettlementRequestTO.setLstSecurityClass(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
	}
	/**
	 * Load Participant Buyer And Seller.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipantApplicantAndCounterpart() throws ServiceException{
		searchCurrencySettlementRequestTO.setLstParticipant(mcnOperationServiceFacade.getLstParticipants(null));
		searchCurrencySettlementRequestTO.setLstParticipantCounterpart(mcnOperationServiceFacade.getLstParticipants(null));
	}
	
	/**
	 * Create Object Register.
	 */
	public void createObjectRegister()
	{
		searchCurrencySettlementRequestHeaderTO = new SearchCurrencySettlementRequestTO(getCurrentSystemDate());
		searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
		searchCurrencySettlementRequestHeaderTO.setLstNegotiationMechanism(new ArrayList<NegotiationMechanism>());
		searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(new ArrayList<NegotiationModality>());
		searchCurrencySettlementRequestHeaderTO.setLstParticipant(new ArrayList<Participant>());
		searchCurrencySettlementRequestHeaderTO.setLstParticipantCounterpart(new ArrayList<Participant>());
		searchCurrencySettlementRequestHeaderTO.setLstSecurityClass(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestHeaderTO.setLstCurrency(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestHeaderTO.setSettlementDate(CommonsUtilities.currentDate());
	}

	
	/**
	 * Search Mechanism Operation Handler.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchSettlementOperationForReversionHandler() throws ServiceException
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		lstSelectedSettlementOperations = null;
		
		List<SearchSettlementOperationTO> lstMechanismOperation = currencySettlementFacade.getCurrencyExchangedSettlementOperation(searchCurrencySettlementRequestHeaderTO);
		
		settlementOperationsDataModel = new GenericDataModel<SearchSettlementOperationTO>(lstMechanismOperation);		 
	}
	
	/**
	 * Gets the list currency type.
	 *
	 * @return the list currency type
	 */
	public List<CurrencyType> getListCurrencyType(){
		executeAction();
		return CurrencyType.listSomeElements(CurrencyType.PYG,CurrencyType.USD);
	}
	
	/**
	 * Before save.
	 */
	public void beforeSave(){
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  if(Validations.validateIsNullOrEmpty(searchCurrencySettlementRequestHeaderTO.getSettlementDate())
					|| Validations.validateIsNullOrNotPositive(searchCurrencySettlementRequestHeaderTO.getIdParticipant())
					|| Validations.validateIsNull(lstSelectedSettlementOperations)){
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  
		 		  
		  if(Validations.validateListIsNullOrEmpty(lstSelectedSettlementOperations)){
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_OPERATION_NOT_SELECTED));			 
			  JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  
		  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REGISTER_CONFIRM));	
		  JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
	}
	
	/**
	 * Clean Currency Settlement Request Handler.
	 */
	public void cleanCurrencySettlementRequestHandler(){
		executeAction();
		searchCurrencySettlementRequestHeaderTO.setIdMechanismNegotiation(null);
		searchCurrencySettlementRequestHeaderTO.setIdModalityNegotiation(null);
		searchCurrencySettlementRequestHeaderTO.setInitialDate(getCurrentSystemDate());
		searchCurrencySettlementRequestHeaderTO.setParticipantRole(null);
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestHeaderTO.setIdParticipant(null);
		}
		searchCurrencySettlementRequestHeaderTO.setIdParticipantCounterpart(null);
		searchCurrencySettlementRequestHeaderTO.setBallotNumber(null);
		searchCurrencySettlementRequestHeaderTO.setSequentialNumber(null);
		searchCurrencySettlementRequestHeaderTO.setCurrency(null);
		searchCurrencySettlementRequestHeaderTO.setSecurityClass(null);
		searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
		cleanModelRegister();
	}
	
	/**
	 * Clean Currency Settlement Request Search Handler.
	 */
	public void cleanCurrencySettlementRequestSearchHandler(){
		executeAction();
		searchCurrencySettlementRequestTO.setIdMechanismNegotiation(null);
		searchCurrencySettlementRequestTO.setIdModalityNegotiation(null);
		searchCurrencySettlementRequestTO.setCurrency(null);
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestTO.setIdParticipant(null);
		}
		searchCurrencySettlementRequestTO.setIdParticipantCounterpart(null);
		searchCurrencySettlementRequestTO.setStateRequest(null);
		searchCurrencySettlementRequestTO.setBallotNumber(null);
		searchCurrencySettlementRequestTO.setSequentialNumber(null);
		searchCurrencySettlementRequestTO.setSecurityClass(null);
		searchCurrencySettlementRequestTO.setSecurity(new Security());
		searchCurrencySettlementRequestTO.setNumberRequest(null);
		searchCurrencySettlementRequestTO.setInitialDate(getCurrentSystemDate());
		searchCurrencySettlementRequestTO.setFinalDate(getCurrentSystemDate());
		cleanDataModel();
	}
	
	/**
	 * Save Currency Settlement Request Handler.
	 */
	@LoggerAuditWeb
	public void saveCurrencySettlementReversionHandler(){
		
		List<SettlementRequest> lstSettlementRequests = null;
		Map<Long,SettlementRequest> requestByCounterPart = new HashMap<Long, SettlementRequest>();
		
		try {
								
			for(SearchSettlementOperationTO searchMechanismOperationTO : lstSelectedSettlementOperations){
					 
				Long idMechanism = searchMechanismOperationTO.getIdMechanism();
				Long idModality = searchMechanismOperationTO.getIdModality();
				String settlementDate = CommonsUtilities.convertDatetoString(searchCurrencySettlementRequestHeaderTO.getSettlementDate());
				AssignmentProcess assignmentProcess = currencySettlementFacade.findAssignmentProcess(idMechanism,idModality,searchCurrencySettlementRequestHeaderTO.getSettlementDate());
				if(assignmentProcess!=null && AssignmentProcessStateType.CLOSED.getCode().equals( assignmentProcess.getAssignmentState() )){
					throw new ServiceException(ErrorServiceType.ASSIGNMENT_PROCESS_CLOSED,
						 new Object[]{searchMechanismOperationTO.getMechanismName(), searchMechanismOperationTO.getModalityName(),settlementDate});
				}
				 
				SettlementRequest settlementRequest = requestByCounterPart.get(searchMechanismOperationTO.getIdParticipantCounterpart());
				
				if(settlementRequest == null){
					settlementRequest = new SettlementRequest();
					settlementRequest.setSourceParticipant(new Participant(searchCurrencySettlementRequestHeaderTO.getIdParticipant()));
					settlementRequest.setTargetParticipant(new Participant(searchMechanismOperationTO.getIdParticipantCounterpart()));
					settlementRequest.setRequestType(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode());
					settlementRequest.setLstCurrencySettlementDetail(new ArrayList<CurrencySettlementOperation>());
					requestByCounterPart.put(settlementRequest.getTargetParticipant().getIdParticipantPk(), settlementRequest);
				}
				 
				CurrencySettlementOperation currencySettlementRequest = new CurrencySettlementOperation();
				currencySettlementRequest.setSettlementOperation(new SettlementOperation(searchMechanismOperationTO.getIdSettlementOperation()));				 
				currencySettlementRequest.setOperationPart(searchMechanismOperationTO.getOperationPart());
				currencySettlementRequest.setInitialSettlementCurrency(searchMechanismOperationTO.getInitialSettlementCurrency());
				currencySettlementRequest.setSettlementCurrency(searchMechanismOperationTO.getSettlementCurrency());
				currencySettlementRequest.setExchangeRate(exchangeRate.getBuyPrice());
				currencySettlementRequest.setSettlementPrice(searchMechanismOperationTO.getNewSettlementPrice());
				currencySettlementRequest.setSettlementAmount(searchMechanismOperationTO.getNewSettlementAmount());
				currencySettlementRequest.setInitialSettlementPrice(searchMechanismOperationTO.getSettlementPrice());
				currencySettlementRequest.setInitialSettlementAmount(searchMechanismOperationTO.getSettlementAmount());
				settlementRequest.getLstCurrencySettlementDetail().add(currencySettlementRequest);
					 
			}
			
			lstSettlementRequests = new ArrayList<SettlementRequest>(requestByCounterPart.values());
		
			currencySettlementFacade.registryCurrencySettlementRequestReversionFacade(lstSettlementRequests,searchCurrencySettlementRequestHeaderTO.getIndUserParticipant());
			
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				cleanCurrencySettlementRequestSearchHandler();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		} 
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementRequests)){			
			//Sending notification
			for(SearchSettlementOperationTO searchMechanismOperationTO : lstSelectedSettlementOperations){
				 SettlementRequest settlementRequest = requestByCounterPart.get(searchMechanismOperationTO.getIdParticipantCounterpart());
				 BusinessProcess businessProcessNotification = new BusinessProcess();					
				 businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REGISTER.getCode());
				 Object[] parameters = new Object[]{
							mpParameters.get(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode()),
							settlementRequest.getIdSettlementRequestPk(),
							searchMechanismOperationTO.getMechanismName(),
							searchMechanismOperationTO.getModalityName(),
							searchMechanismOperationTO.getOperationDate(),
							searchMechanismOperationTO.getBallotNumber() == null ? 
								searchMechanismOperationTO.getOperationNumber():
								searchMechanismOperationTO.getBallotNumber() + GeneralConstants.SLASH + searchMechanismOperationTO.getSequential() };								
				 notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
							businessProcessNotification, settlementRequest.getSourceParticipant().getIdParticipantPk(), parameters);
			}
		}							
		setViewOperationType(ViewOperationsType.CONSULT.getCode());	
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REGISTER_SUCCESS));						
		JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		cleanCurrencySettlementRequestSearchHandler();
	}
	
	/**
	 * Before Approve action.
	 *
	 * @return the string
	 */
	public String validateApproveAction(){
		return validateAction(SettlementCurrencyRequestStateType.APPROVED.getCode());
	}
	
	/**
	 * Validate annular action.
	 *
	 * @return the string
	 */
	public String validateAnnularAction(){
		return validateAction(SettlementCurrencyRequestStateType.CANCELLED.getCode());
	}
	
	/**
	 * Validate review action.
	 *
	 * @return the string
	 */
	public String validateReviewAction(){
		return validateAction(SettlementCurrencyRequestStateType.REVISED.getCode());
	}
	
	/**
	 * Validate confirm action.
	 *
	 * @return the string
	 */
	public String validateConfirmAction(){
		return validateAction(SettlementCurrencyRequestStateType.CONFIRMED.getCode());
	}
	
	/**
	 * Validate reject action.
	 *
	 * @return the string
	 */
	public String validateRejectAction(){
		return validateAction(SettlementCurrencyRequestStateType.REJECTED.getCode());
	}
	
	/**
	 * Validate authorize action.
	 *
	 * @return the string
	 */
	public String validateAuthorizeAction(){
		return validateAction(SettlementCurrencyRequestStateType.AUTHORIZED.getCode());
	}
	
	/**
	 * Validate action.
	 *
	 * @param newState the new state
	 * @return the string
	 */
	public String validateAction(Integer newState){
		executeAction();
		cleanModelRegister();	
		List<Integer> expectedStates = new ArrayList<Integer>();
		List<Long> expectedParticipants = new ArrayList<Long>();
		
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(SettlementCurrencyRequestStateType.APPROVED.getCode().equals(newState)){	
			expectedParticipants.add(selectedCurrencySettlementRequest.getSourceParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.REGISTERED.getCode());
			setViewOperationType(ViewOperationsType.APPROVE.getCode());
		}else if(SettlementCurrencyRequestStateType.CANCELLED.getCode().equals(newState)){			
			expectedParticipants.add(selectedCurrencySettlementRequest.getSourceParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.REGISTERED.getCode());
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
		}else if(SettlementCurrencyRequestStateType.REJECTED.getCode().equals(newState)){
			expectedParticipants.add(selectedCurrencySettlementRequest.getTargetParticipant().getIdParticipantPk());
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				expectedStates.add(SettlementCurrencyRequestStateType.REVISED.getCode());
				expectedStates.add(SettlementCurrencyRequestStateType.APPROVED.getCode());
			}else{
				expectedStates.add(SettlementCurrencyRequestStateType.REVISED.getCode());
				expectedStates.add(SettlementCurrencyRequestStateType.APPROVED.getCode());
				expectedStates.add(SettlementCurrencyRequestStateType.CONFIRMED.getCode());
			}
			setViewOperationType(ViewOperationsType.REJECT.getCode());
		}else if(SettlementCurrencyRequestStateType.REVISED.getCode().equals(newState)){
			expectedParticipants.add(selectedCurrencySettlementRequest.getTargetParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.APPROVED.getCode());
			setViewOperationType(ViewOperationsType.REVIEW.getCode());
		}else if(SettlementCurrencyRequestStateType.CONFIRMED.getCode().equals(newState)){	
			expectedParticipants.add(selectedCurrencySettlementRequest.getTargetParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.REVISED.getCode());
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		}else if(SettlementCurrencyRequestStateType.AUTHORIZED.getCode().equals(newState)){			
			expectedStates.add(SettlementCurrencyRequestStateType.CONFIRMED.getCode());
			setViewOperationType(ViewOperationsType.AUTHORIZE.getCode());
		}
			
		if(userInfo.getUserAccountSession().isParticipantInstitucion() 
				&& searchCurrencySettlementRequestTO.getIdParticipant()!=null 
				&& !expectedParticipants.contains(searchCurrencySettlementRequestTO.getIdParticipant())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
					, PropertiesUtilities.getMessage("msg.settlement.dateExchange.invalidAction.participant"));
			JSFUtilities.showValidationDialog();
			return null;
		}
		
		if(!expectedStates.contains(selectedCurrencySettlementRequest.getRequestState())){
			
			List<String> stringStates = new ArrayList<String>();
			for(Integer state : expectedStates){
				stringStates.add(mpParameters.get(state));
			}
			
			Object[] bodyData = new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest().toString(),
					StringUtils.join(stringStates.toArray(),GeneralConstants.STR_COMMA_WITHOUT_SPACE)};
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_STATE, bodyData));
			JSFUtilities.showValidationDialog();
			return null;
		}
		
		return "viewCurrencySettlementReversion";
	}
	
	/**
	 * Search Currency Settlement Request Handler.
	 */
	@LoggerAuditWeb
	public void searchCurrencySettlementRequestHandler(){
		selectedCurrencySettlementRequest = null;
		JSFUtilities.hideGeneralDialogues(); 		
		List <SettlementRequestTO> lstCurrencySettlementRequest=null;		
		try{			
			searchCurrencySettlementRequestTO.setRequestType(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode());
			lstCurrencySettlementRequest =  currencySettlementFacade.getListCurrencySettlementRequestFilter(searchCurrencySettlementRequestTO);
			currencySettlementRequestsDataModel = new GenericDataModel<SettlementRequestTO>(lstCurrencySettlementRequest);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Before do action.
	 */
	public void beforeDoAction(){
		executeAction();
		String message = null;
		String header = null;
		
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(isViewOperationApprove()){
			header = GeneralConstants.LBL_HEADER_ALERT_APPROVE;
			message = PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_APPROVE_CONFIRM;
		}else if(isViewOperationReview()){
			header = GeneralConstants.LBL_HEADER_ALERT_REVIEW;
			message = PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REVIEW_CONFIRM;
		}else if(isViewOperationAnnul()){
			motiveControllerAnnul.setIdMotivePK(0);
			motiveControllerAnnul.setMotiveText(null);
			motiveControllerAnnul.setShowMotiveText(false);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL),null);	
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
			return;
		}else if(isViewOperationReject()){
			motiveControllerAnnul.setIdMotivePK(0);
			motiveControllerAnnul.setMotiveText(null);
			motiveControllerAnnul.setShowMotiveText(false);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT),null);	
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
			return;
		}else if (isViewOperationConfirm()){
			header = GeneralConstants.LBL_HEADER_ALERT_CONFIRM;
			message = PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_CONFIRM_CONFIRM;
		}else if (isViewOperationAuthorize()){
			header = GeneralConstants.LBL_HEADER_ALERT_AUTHORIZE;
			message = PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_AUTHORIZE_CONFIRM;
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(header)
			      , PropertiesUtilities.getMessage(message,
			    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );			
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
		
		return;
	}

	/**
	 * Change action currency settlement request handler.
	 */
	@LoggerAuditWeb
	public void doActionHandler(){
		switch(ViewOperationsType.lookup.get(getViewOperationType())){
			case APPROVE : approveCurrencySettlementRequest();break;
			case ANULATE: annularCurrencySettlementRequest();break;
			case REVIEW: reviewCurrencySettlementRequest();break;
			case CONFIRM : confirmCurrencySettlementRequest();break;
			case REJECT: rejectCurrencySettlementRequest();break;
			case AUTHORIZE: authorizeCurrencySettlementRequest();break;
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		cleanCurrencySettlementRequestSearchHandler();
	}

	
	/**
	 * Send change state notification.
	 *
	 * @param currencySettlementRequest the currency settlement request
	 * @param idBusinessProcess the id business process
	 */
	private void sendChangeStateNotification(SettlementRequestTO currencySettlementRequest, Long idBusinessProcess) {
		//Sending notification
		BusinessProcess businessProcessNotification = new BusinessProcess();					
		businessProcessNotification.setIdBusinessProcessPk(idBusinessProcess);		
		for(SettlementRequestDetailTO currencySettlementOperation : currencySettlementRequest.getSettlementRequestDetail()){
			Object[] parameters = new Object[]{
					mpParameters.get(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode()),
					currencySettlementRequest.getIdSettlementRequest(),
					currencySettlementOperation.getMechanismName(),
					currencySettlementOperation.getModalityName(),
					CommonsUtilities.convertDatetoString(currencySettlementOperation.getOperationDate()),
					currencySettlementOperation.getBallotNumber() == null ? 
						currencySettlementOperation.getOperationNumber():
						currencySettlementOperation.getBallotNumberSequential() };
			if(BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_APPROVE.getCode().equals(idBusinessProcess) ||
					BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_REVIEW.getCode().equals(idBusinessProcess)) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,currencySettlementRequest.getTargetParticipant().getIdParticipantPk(), parameters);	
			} else if(BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_CONFIRM.getCode().equals(idBusinessProcess)) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcessNotification, null, parameters);
			} else {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,currencySettlementRequest.getSourceParticipant().getIdParticipantPk(), parameters);
			}			
		}
		
	}
	
	/**
	 * Annul Currency settlement request.
	 */
	@LoggerAuditWeb
	private void annularCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		selectedCurrencySettlementRequest.setAnnulMotive(Integer.parseInt(""+motiveControllerAnnul.getIdMotivePK()));
		selectedCurrencySettlementRequest.setAnnulOtherMotive(motiveControllerAnnul.getMotiveText());
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.annulCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_CANCEL.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_ANNUL_SUCCESS,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	
	/**
	 * Reject currency settlement request.
	 */
	@LoggerAuditWeb
	private void rejectCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues(); 
		selectedCurrencySettlementRequest.setRejectMotive(Integer.parseInt(""+motiveControllerReject.getIdMotivePK()));
		selectedCurrencySettlementRequest.setRejectOtherMotive(motiveControllerReject.getMotiveText());
		SettlementRequest currencySettlementRequest = null;
		try {
			if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.APPROVED.getCode()))
				currencySettlementRequest = currencySettlementFacade.rejectCurrencySettlementRequestApproveFacade(selectedCurrencySettlementRequest);
			else 	{
				if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.REVISED.getCode()))
					currencySettlementRequest = currencySettlementFacade.rejectCurrencySettlementRequestReviewFacade(selectedCurrencySettlementRequest);
				else {
					if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.CONFIRMED.getCode()))
						currencySettlementRequest = currencySettlementFacade.rejectCurrencySettlementRequestConfirmFacade(selectedCurrencySettlementRequest);
				}
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_REJECT.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REJECT_SUCCESS,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	/**
	 * Approve currency settlement request.
	 */
	@LoggerAuditWeb
	private void approveCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.approveCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_APPROVE.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_APPROVE_SUCCESS,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	/**
	 * Review currency settlement request.
	 */
	@LoggerAuditWeb
	private void reviewCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		
				
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.reviewCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_REVIEW.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REVIEW_SUCCESS,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	/**
	 * Confirm currency settlement request.
	 */
	@LoggerAuditWeb
	private void confirmCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  						
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.confirmCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_CONFIRM.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_CONFIRM_SUCCESS,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	/**
	 * Authorize currency settlement request.
	 */
	@LoggerAuditWeb
	private void authorizeCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  				
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.authorizeCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REVERSION_AUTHORIZE.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_AUTHORIZE_SUCCESS,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	
	/**
	 * Select Currency Settlement Request Handler.
	 *
	 * @param currencySettlementRequest object currency Settlement Request
	 */
	public void selectCurrencySettlementRequestHandler(SettlementRequestTO currencySettlementRequest){
		executeAction();
		cleanModelRegister();	
		
		selectedCurrencySettlementRequest = currencySettlementRequest;

		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveAnnulHandler(){
		if(this.getMotiveControllerAnnul().getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveAnnulType.OTHER.getCode().toString())){
			this.getMotiveControllerAnnul().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerAnnul().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Change motive Reject handler.
	 */
	public void changeMotiveRejectHandler(){
		if(this.getMotiveControllerReject().getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveRejectType.OTHER.getCode().toString())){
			this.getMotiveControllerReject().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerReject().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveAnnulHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerAnnul.getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveAnnulType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerAnnul().getMotiveText())){	
				setViewOperationType(ViewOperationsType.ANULATE.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_ANNUL_CONFIRM,
			    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );	
		setViewOperationType(ViewOperationsType.ANULATE.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	/**
	 * Confirm motive Reject handler.
	 */
	public void confirmMotiveRejectHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerReject.getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveRejectType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerReject().getMotiveText())){
				setViewOperationType(ViewOperationsType.REJECT.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVERSION_REJECT_CONFIRM,
			    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );	
		setViewOperationType(ViewOperationsType.REJECT.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	
	/**
	 * Clean Model Register.
	 */
	public void cleanModelRegister()
	{
		settlementOperationsDataModel=null;
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		currencySettlementRequestsDataModel = null;
	}
	
	/**
	 * Gets the search currency settlement request to.
	 *
	 * @return the search currency settlement request to
	 */
	public SearchCurrencySettlementRequestTO getSearchCurrencySettlementRequestTO() {
		return searchCurrencySettlementRequestTO;
	}
	
	/**
	 * Sets the search currency settlement request to.
	 *
	 * @param searchCurrencySettlementRequestTO the new search currency settlement request to
	 */
	public void setSearchCurrencySettlementRequestTO(
			SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO) {
		this.searchCurrencySettlementRequestTO = searchCurrencySettlementRequestTO;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Gets the search currency settlement request header to.
	 *
	 * @return the search currency settlement request header to
	 */
	public SearchCurrencySettlementRequestTO getSearchCurrencySettlementRequestHeaderTO() {
		return searchCurrencySettlementRequestHeaderTO;
	}
	
	/**
	 * Sets the search currency settlement request header to.
	 *
	 * @param searchCurrencySettlementRequestHeaderTO the new search currency settlement request header to
	 */
	public void setSearchCurrencySettlementRequestHeaderTO(
			SearchCurrencySettlementRequestTO searchCurrencySettlementRequestHeaderTO) {
		this.searchCurrencySettlementRequestHeaderTO = searchCurrencySettlementRequestHeaderTO;
	}
	
	/**
	 * Gets the settlement operations data model.
	 *
	 * @return the settlement operations data model
	 */
	public GenericDataModel<SearchSettlementOperationTO> getSettlementOperationsDataModel() {
		return settlementOperationsDataModel;
	}
	
	/**
	 * Sets the settlement operations data model.
	 *
	 * @param settlementOperationsDataModel the new settlement operations data model
	 */
	public void setSettlementOperationsDataModel(
			GenericDataModel<SearchSettlementOperationTO> settlementOperationsDataModel) {
		this.settlementOperationsDataModel = settlementOperationsDataModel;
	}
	
	/**
	 * Gets the currency settlement requests data model.
	 *
	 * @return the currency settlement requests data model
	 */
	public GenericDataModel<SettlementRequestTO> getCurrencySettlementRequestsDataModel() {
		return currencySettlementRequestsDataModel;
	}
	
	/**
	 * Sets the currency settlement requests data model.
	 *
	 * @param currencySettlementRequestsDataModel the new currency settlement requests data model
	 */
	public void setCurrencySettlementRequestsDataModel(
			GenericDataModel<SettlementRequestTO> currencySettlementRequestsDataModel) {
		this.currencySettlementRequestsDataModel = currencySettlementRequestsDataModel;
	}
	
	/**
	 * Gets the motive controller annul.
	 *
	 * @return the motive controller annul
	 */
	public MotiveController getMotiveControllerAnnul() {
		return motiveControllerAnnul;
	}
	
	/**
	 * Sets the motive controller annul.
	 *
	 * @param motiveControllerAnnul the new motive controller annul
	 */
	public void setMotiveControllerAnnul(MotiveController motiveControllerAnnul) {
		this.motiveControllerAnnul = motiveControllerAnnul;
	}
	
	/**
	 * Gets the motive controller reject.
	 *
	 * @return the motive controller reject
	 */
	public MotiveController getMotiveControllerReject() {
		return motiveControllerReject;
	}
	
	/**
	 * Sets the motive controller reject.
	 *
	 * @param motiveControllerReject the new motive controller reject
	 */
	public void setMotiveControllerReject(MotiveController motiveControllerReject) {
		this.motiveControllerReject = motiveControllerReject;
	}
	
	/**
	 * Gets the mp parameters.
	 *
	 * @return the mp parameters
	 */
	public Map<Integer, String> getMpParameters() {
		return mpParameters;
	}
	
	/**
	 * Sets the mp parameters.
	 *
	 * @param mpParameters the mp parameters
	 */
	public void setMpParameters(Map<Integer, String> mpParameters) {
		this.mpParameters = mpParameters;
	}
	
	/**
	 * Gets the selected currency settlement request.
	 *
	 * @return the selected currency settlement request
	 */
	public SettlementRequestTO getSelectedCurrencySettlementRequest() {
		return selectedCurrencySettlementRequest;
	}
	
	/**
	 * Sets the selected currency settlement request.
	 *
	 * @param selectedCurrencySettlementRequest the new selected currency settlement request
	 */
	public void setSelectedCurrencySettlementRequest(
			SettlementRequestTO selectedCurrencySettlementRequest) {
		this.selectedCurrencySettlementRequest = selectedCurrencySettlementRequest;
	}
	
	/**
	 * Gets the lst selected settlement operations.
	 *
	 * @return the lst selected settlement operations
	 */
	public List<SearchSettlementOperationTO> getLstSelectedSettlementOperations() {
		return lstSelectedSettlementOperations;
	}
	
	/**
	 * Sets the lst selected settlement operations.
	 *
	 * @param lstSelectedSettlementOperations the new lst selected settlement operations
	 */
	public void setLstSelectedSettlementOperations(
			List<SearchSettlementOperationTO> lstSelectedSettlementOperations) {
		this.lstSelectedSettlementOperations = lstSelectedSettlementOperations;
	}
	
		
}
