package com.pradera.settlements.currencyexchange.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchSettlementOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchSettlementOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The id modality negotiation. */
	private Long idModality;
	
	/** The description modality negotiation. */
	private String modalityName;
	
	/** The id mechanism. */
	private Long idMechanism;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The ballot Number. */
	private Long ballotNumber;
	
	/** The sequential Number. */
	private Long sequential;
	
	/** The id Participant counterpart. */
	private Long idParticipantCounterpart;
	
	/** The description Participant counterpart. */
	private String descriptionParticipantCounterpart;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The description security class. */
	private String descriptionSecurityClass;
	
	/** The code security. */
	private String idSecurityCode;
	
	/** The currency. */
	private Integer currency;
	
	/** The description currency. */
	private String descriptionCurrency;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The market Date. */
	private Date marketDate; 
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The Settlement Amount. */
	private BigDecimal settlementAmount;
	
	/** The Settlement Currency. */
	private Integer settlementCurrency;
	
	/** The initial settlement currency. */
	private Integer initialSettlementCurrency;
	
	/**  the settlement Price. */
	private BigDecimal settlementPrice;
	
	/** The exchange rate. */
	private BigDecimal initialExchangeRate;
	
	/** The exchange rate. */
	private BigDecimal exchangeRate;
	
	/**  the new settlement Price. */
	private BigDecimal newSettlementPrice;
	
	/** The new Settlement Amount. */
	private BigDecimal newSettlementAmount;
	
	/** The operation Part. */
	private Integer operationPart;
	
	/** The indicator  Extended. */
	private Integer indExtended;

	/** The indicator  Prepaid. */
	private Integer indPrepaid;
	
	/** The indicator Remove Settlement. */
	private Integer indRemoveSettlement;
	
	/** The indicator Reenter Settlement. */
	private Integer indReenterSettlement;
	
	/** The indicator Mechanism Modality  Settlement Exchange. */
	private Integer indCurrencyExchange;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The selected. */
	private Boolean selected = false;
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}


	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}


	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}


	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Instantiates a new search settlement operation to.
	 */
	public SearchSettlementOperationTO(){
		
	}

	
	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}


	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}


	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}


	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}


	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return idMechanism;
	}


	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}


	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}


	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}


	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}

	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}


	/**
	 * Gets the id participant counterpart.
	 *
	 * @return the id participant counterpart
	 */
	public Long getIdParticipantCounterpart() {
		return idParticipantCounterpart;
	}

	/**
	 * Sets the id participant counterpart.
	 *
	 * @param idParticipantCounterpart the new id participant counterpart
	 */
	public void setIdParticipantCounterpart(Long idParticipantCounterpart) {
		this.idParticipantCounterpart = idParticipantCounterpart;
	}

	/**
	 * Gets the description participant counterpart.
	 *
	 * @return the description participant counterpart
	 */
	public String getDescriptionParticipantCounterpart() {
		return descriptionParticipantCounterpart;
	}

	/**
	 * Sets the description participant counterpart.
	 *
	 * @param descriptionParticipantCounterpart the new description participant counterpart
	 */
	public void setDescriptionParticipantCounterpart(
			String descriptionParticipantCounterpart) {
		this.descriptionParticipantCounterpart = descriptionParticipantCounterpart;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the description security class.
	 *
	 * @return the description security class
	 */
	public String getDescriptionSecurityClass() {
		return descriptionSecurityClass;
	}

	/**
	 * Sets the description security class.
	 *
	 * @param descriptionSecurityClass the new description security class
	 */
	public void setDescriptionSecurityClass(String descriptionSecurityClass) {
		this.descriptionSecurityClass = descriptionSecurityClass;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the description currency.
	 *
	 * @return the description currency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}

	/**
	 * Sets the description currency.
	 *
	 * @param descriptionCurrency the new description currency
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	/**
	 * Gets the settlement currency.
	 *
	 * @return the settlement currency
	 */
	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}

	/**
	 * Sets the settlement currency.
	 *
	 * @param settlementCurrency the new settlement currency
	 */
	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	/**
	 * Gets the exchange rate.
	 *
	 * @return the exchange rate
	 */
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	/**
	 * Sets the exchange rate.
	 *
	 * @param exchangeRate the new exchange rate
	 */
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	/**
	 * Gets the new settlement amount.
	 *
	 * @return the new settlement amount
	 */
	public BigDecimal getNewSettlementAmount() {
		return newSettlementAmount;
	}

	/**
	 * Sets the new settlement amount.
	 *
	 * @param newSettlementAmount the new new settlement amount
	 */
	public void setNewSettlementAmount(BigDecimal newSettlementAmount) {
		this.newSettlementAmount = newSettlementAmount;
	}

	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}

	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}

	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public Boolean getSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}

	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	/**
	 * Gets the settlement price.
	 *
	 * @return the settlement price
	 */
	public BigDecimal getSettlementPrice() {
		return settlementPrice;
	}

	/**
	 * Sets the settlement price.
	 *
	 * @param settlementPrice the new settlement price
	 */
	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	/**
	 * Gets the new settlement price.
	 *
	 * @return the new settlement price
	 */
	public BigDecimal getNewSettlementPrice() {
		return newSettlementPrice;
	}

	/**
	 * Sets the new settlement price.
	 *
	 * @param newSettlementPrice the new new settlement price
	 */
	public void setNewSettlementPrice(BigDecimal newSettlementPrice) {
		this.newSettlementPrice = newSettlementPrice;
	}
	
	/**
	 * Gets the ind remove settlement.
	 *
	 * @return the ind remove settlement
	 */
	public Integer getIndRemoveSettlement() {
		return indRemoveSettlement;
	}

	/**
	 * Sets the ind remove settlement.
	 *
	 * @param indRemoveSettlement the new ind remove settlement
	 */
	public void setIndRemoveSettlement(Integer indRemoveSettlement) {
		this.indRemoveSettlement = indRemoveSettlement;
	}

	/**
	 * Gets the ind reenter settlement.
	 *
	 * @return the ind reenter settlement
	 */
	public Integer getIndReenterSettlement() {
		return indReenterSettlement;
	}

	/**
	 * Sets the ind reenter settlement.
	 *
	 * @param indReenterSettlement the new ind reenter settlement
	 */
	public void setIndReenterSettlement(Integer indReenterSettlement) {
		this.indReenterSettlement = indReenterSettlement;
	}

	/**
	 * Gets the ind extended.
	 *
	 * @return the ind extended
	 */
	public Integer getIndExtended() {
		return indExtended;
	}

	/**
	 * Sets the ind extended.
	 *
	 * @param indExtended the new ind extended
	 */
	public void setIndExtended(Integer indExtended) {
		this.indExtended = indExtended;
	}

	/**
	 * Gets the ind prepaid.
	 *
	 * @return the ind prepaid
	 */
	public Integer getIndPrepaid() {
		return indPrepaid;
	}

	/**
	 * Sets the ind prepaid.
	 *
	 * @param indPrepaid the new ind prepaid
	 */
	public void setIndPrepaid(Integer indPrepaid) {
		this.indPrepaid = indPrepaid;
	}

	/**
	 * Gets the id settlement operation.
	 *
	 * @return the id settlement operation
	 */
	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}


	/**
	 * Sets the id settlement operation.
	 *
	 * @param idSettlementOperation the new id settlement operation
	 */
	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}


	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}


	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}


	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}


	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}


	/**
	 * Gets the ind currency exchange.
	 *
	 * @return the ind currency exchange
	 */
	public Integer getIndCurrencyExchange() {
		return indCurrencyExchange;
	}


	/**
	 * Sets the ind currency exchange.
	 *
	 * @param indCurrencyExchange the new ind currency exchange
	 */
	public void setIndCurrencyExchange(Integer indCurrencyExchange) {
		this.indCurrencyExchange = indCurrencyExchange;
	}


	/**
	 * Gets the initial exchange rate.
	 *
	 * @return the initial exchange rate
	 */
	public BigDecimal getInitialExchangeRate() {
		return initialExchangeRate;
	}


	/**
	 * Sets the initial exchange rate.
	 *
	 * @param initialExchangeRate the new initial exchange rate
	 */
	public void setInitialExchangeRate(BigDecimal initialExchangeRate) {
		this.initialExchangeRate = initialExchangeRate;
	}


	/**
	 * Gets the initial settlement currency.
	 *
	 * @return the initial settlement currency
	 */
	public Integer getInitialSettlementCurrency() {
		return initialSettlementCurrency;
	}


	/**
	 * Sets the initial settlement currency.
	 *
	 * @param initialSettlementCurrency the new initial settlement currency
	 */
	public void setInitialSettlementCurrency(Integer initialSettlementCurrency) {
		this.initialSettlementCurrency = initialSettlementCurrency;
	}


		
}
