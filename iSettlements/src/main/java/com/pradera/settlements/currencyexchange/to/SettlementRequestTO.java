package com.pradera.settlements.currencyexchange.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementRequestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementRequestTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id settlement request. */
	private Long idSettlementRequest;
	
	/** The state description. */
	private String stateDescription;
	
	/** The request state. */
	private Integer requestState;
	
	/** The participant role. */
	private Integer participantRole;
	
	/** The source participant. */
	private Participant sourceParticipant;
	
	/** The target participant. */
	private Participant targetParticipant;
	
	/** The register date. */
	private Date registerDate;
	
	/** The annul motive. */
	private Integer annulMotive;
	
	/** The annul other motive. */
	private String annulOtherMotive;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The reject other motive. */
	private String rejectOtherMotive;
	
	/** The request type. */
	private Integer requestType;
	
	/** The request type description. */
	private String requestTypeDescription;
	
	/** The counter part role. */
	private Integer counterPartRole;
	
	/** The anticipation type. */
	private Integer anticipationType;
	
	/** The anticipation type description. */
	private String anticipationTypeDescription;
	
	/** The settlement request detail. */
	private List<SettlementRequestDetailTO> settlementRequestDetail;

	/**
	 * Gets the anticipation type.
	 *
	 * @return the anticipation type
	 */
	public Integer getAnticipationType() {
		return anticipationType;
	}

	/**
	 * Sets the anticipation type.
	 *
	 * @param anticipationType the new anticipation type
	 */
	public void setAnticipationType(Integer anticipationType) {
		this.anticipationType = anticipationType;
	}
	
	/**
	 * Gets the id settlement request.
	 *
	 * @return the id settlement request
	 */
	public Long getIdSettlementRequest() {
		return idSettlementRequest;
	}

	/**
	 * Sets the id settlement request.
	 *
	 * @param idSettlementRequest the new id settlement request
	 */
	public void setIdSettlementRequest(Long idSettlementRequest) {
		this.idSettlementRequest = idSettlementRequest;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/**
	 * Gets the participant role.
	 *
	 * @return the participant role
	 */
	public Integer getParticipantRole() {
		return participantRole;
	}

	/**
	 * Sets the participant role.
	 *
	 * @param participantRole the new participant role
	 */
	public void setParticipantRole(Integer participantRole) {
		this.participantRole = participantRole;
	}

	/**
	 * Gets the source participant.
	 *
	 * @return the source participant
	 */
	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	/**
	 * Sets the source participant.
	 *
	 * @param sourceParticipant the new source participant
	 */
	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}

	/**
	 * Gets the target participant.
	 *
	 * @return the target participant
	 */
	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	/**
	 * Sets the target participant.
	 *
	 * @param targetParticipant the new target participant
	 */
	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the settlement request detail.
	 *
	 * @return the settlement request detail
	 */
	public List<SettlementRequestDetailTO> getSettlementRequestDetail() {
		return settlementRequestDetail;
	}

	/**
	 * Sets the settlement request detail.
	 *
	 * @param currencySettlementDetail the new settlement request detail
	 */
	public void setSettlementRequestDetail(
			List<SettlementRequestDetailTO> currencySettlementDetail) {
		this.settlementRequestDetail = currencySettlementDetail;
	}

	/**
	 * Gets the annul motive.
	 *
	 * @return the annul motive
	 */
	public Integer getAnnulMotive() {
		return annulMotive;
	}

	/**
	 * Sets the annul motive.
	 *
	 * @param annulMotive the new annul motive
	 */
	public void setAnnulMotive(Integer annulMotive) {
		this.annulMotive = annulMotive;
	}

	/**
	 * Gets the annul other motive.
	 *
	 * @return the annul other motive
	 */
	public String getAnnulOtherMotive() {
		return annulOtherMotive;
	}

	/**
	 * Sets the annul other motive.
	 *
	 * @param annulOtherMotive the new annul other motive
	 */
	public void setAnnulOtherMotive(String annulOtherMotive) {
		this.annulOtherMotive = annulOtherMotive;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the request type description.
	 *
	 * @return the request type description
	 */
	public String getRequestTypeDescription() {
		return requestTypeDescription;
	}

	/**
	 * Sets the request type description.
	 *
	 * @param requestTypeDescription the new request type description
	 */
	public void setRequestTypeDescription(String requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}

	/**
	 * Gets the counter part role.
	 *
	 * @return the counter part role
	 */
	public Integer getCounterPartRole() {
		return counterPartRole;
	}

	/**
	 * Sets the counter part role.
	 *
	 * @param counterPartRole the new counter part role
	 */
	public void setCounterPartRole(Integer counterPartRole) {
		this.counterPartRole = counterPartRole;
	}

	/**
	 * Gets the anticipation type description.
	 *
	 * @return the anticipation type description
	 */
	public String getAnticipationTypeDescription() {
		return anticipationTypeDescription;
	}

	/**
	 * Sets the anticipation type description.
	 *
	 * @param anticipationTypeDescription the new anticipation type description
	 */
	public void setAnticipationTypeDescription(String anticipationTypeDescription) {
		this.anticipationTypeDescription = anticipationTypeDescription;
	}

	
	
	

}
