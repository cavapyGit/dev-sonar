package com.pradera.settlements.currencyexchange.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.CurrencySettlementOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.CurrencySettlementMotiveAnnulType;
import com.pradera.model.settlement.type.CurrencySettlementMotiveRejectType;
import com.pradera.model.settlement.type.SettlementCurrencyRequestStateType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.settlements.currencyexchange.facade.CurrencySettlementFacade;
import com.pradera.settlements.currencyexchange.to.SearchCurrencySettlementRequestTO;
import com.pradera.settlements.currencyexchange.to.SearchMechanismOperationMarketFactTO;
import com.pradera.settlements.currencyexchange.to.SearchSettlementOperationTO;
import com.pradera.settlements.currencyexchange.to.SettlementRequestDetailTO;
import com.pradera.settlements.currencyexchange.to.SettlementRequestTO;
import com.pradera.settlements.utils.MotiveController;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CurrencySettlementRequestMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class CurrencySettlementRequestMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The settlement request session. */
	private SettlementRequest settlementRequestSession;
	
	/** The selected currency settlement request. */
	private SettlementRequestTO selectedCurrencySettlementRequest;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The general parameter facade. */
	@EJB
	GeneralParametersFacade generalParameterFacade;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The currency settlement facade. */
	@EJB
	private CurrencySettlementFacade currencySettlementFacade; 
	
	/** The mcn operation service facade. */
	@EJB
	private McnOperationServiceFacade mcnOperationServiceFacade;

	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The search currency Settlement Request Data Model. */
	private GenericDataModel<SettlementRequestTO> currencySettlementRequestsDataModel;
		
	/** The search Holder Account Balance Data Model. */
	private GenericDataModel<SearchSettlementOperationTO> settlementOperationsDataModel;
		
	/** The search Currency Settlement Request Header TO. */
	private SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO;
	
	/** The search Currency Settlement Request TO. */
	private SearchCurrencySettlementRequestTO searchCurrencySettlementRequestHeaderTO;
	
	/** The Search Mechanism Operation MarketFactTO. */
	private SearchMechanismOperationMarketFactTO searchMechanismOperationMarketFactTO;
	
	/** The mp parameters. */
	private Map<Integer,Object> mpParameters = new HashMap<Integer, Object>(); 
	
	/** The motive controller annul. */
	private MotiveController motiveControllerAnnul; 
	
	/** The motive controller reject. */
	private MotiveController motiveControllerReject;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		createObject();
		try {
			loadNegotiationMechanism();
			loadParticipantApplicantAndCounterpart();
			loadSecurityClass();
			loadCurrency();
			loadState();
			loadMotiveAnnul();
			loadMotiveReject();
			loadHolidays();
			loadUserValidation();
			loadRequestTypes();
			searchCurrencySettlementRequestTO.setIdMechanismNegotiation(NegotiationMechanismType.BOLSA.getCode());
			changeNegotitationMechanism();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load request types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadRequestTypes() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		//parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SETTLEMENT_REQUEST_TYPE.getCode());	
		List<ParameterTable> lstRequestTypes = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);	
		for(ParameterTable param : lstRequestTypes){
			mpParameters.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Load User Validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnReview(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		privilegeComponent.setBtnAuthorize(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
		}
	}
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParameterFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Load cbo modality and participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadNegotiationMechanism() throws ServiceException{
		// TODO Auto-generated method stub
		List<NegotiationMechanism> lstNegotiationMechanismAux = new ArrayList<NegotiationMechanism>();
		List<NegotiationMechanism> lstNegotiationMechanism = (List<NegotiationMechanism>)mcnOperationServiceFacade.getLstNegotiationMechanism(null);
		for(NegotiationMechanism negotiationMechanism : lstNegotiationMechanism){
			if(negotiationMechanism.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.BOLSA.getCode())
					||negotiationMechanism.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.OTC.getCode())){
				lstNegotiationMechanismAux.add(negotiationMechanism);
			}
		}
		searchCurrencySettlementRequestTO.setLstNegotiationMechanism(lstNegotiationMechanismAux);
	}
	
	/**
	 * Change Negotitation Mechanism.
	 */
	public void changeNegotitationMechanism(){
		JSFUtilities.hideGeneralDialogues();		
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())){
			cleanDataModel();
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestTO.getIdMechanismNegotiation())){
				try {
					searchCurrencySettlementRequestTO.setIdModalityNegotiation(null);
					 if(userInfo.getUserAccountSession().isParticipantInstitucion()){
						 searchCurrencySettlementRequestTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
									searchCurrencySettlementRequestTO.getIdMechanismNegotiation(), 
									searchCurrencySettlementRequestTO.getIdParticipant()));	
					 }else{
						 searchCurrencySettlementRequestTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModality(searchCurrencySettlementRequestTO.getIdMechanismNegotiation()));
					 }
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}else {
				searchCurrencySettlementRequestTO.setLstNegotiationModality(null);
			}
			
		}
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
			cleanModelRegister();
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestHeaderTO.getIdMechanismNegotiation())){
				try {
					searchCurrencySettlementRequestHeaderTO.setIdModalityNegotiation(null);
					 if(userInfo.getUserAccountSession().isParticipantInstitucion()) 
								searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
										searchCurrencySettlementRequestHeaderTO.getIdMechanismNegotiation(), 
										searchCurrencySettlementRequestHeaderTO.getIdParticipant()));				 
					 else{
						 searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(mcnOperationServiceFacade.getLstNegotiationModality(searchCurrencySettlementRequestHeaderTO.getIdMechanismNegotiation()));
					 }
							 
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}else searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(null);
			
		}
	}
	
	/**
	 * Create Object.
	 */
	public void createObject()
	{
		searchCurrencySettlementRequestTO = new SearchCurrencySettlementRequestTO(getCurrentSystemDate(),getCurrentSystemDate());
		searchCurrencySettlementRequestTO.setSecurity(new Security());
		searchCurrencySettlementRequestTO.setLstNegotiationMechanism(new ArrayList<NegotiationMechanism>());
		searchCurrencySettlementRequestTO.setLstNegotiationModality(new ArrayList<NegotiationModality>());
		searchCurrencySettlementRequestTO.setLstParticipant(new ArrayList<Participant>());
		searchCurrencySettlementRequestTO.setLstSecurityClass(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestTO.setLstCurrency(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestTO.setLstState(new ArrayList<ParameterTable>());
		motiveControllerAnnul = new MotiveController();
		motiveControllerReject = new MotiveController();
	}
	
	/**
	 * Registry Currency Settlement Request Handler.
	 *
	 * @return the string
	 */
	public String registryCurrencySettlementRequestHandler()
	{
		try {
			DailyExchangeRates exchangeRate = generalParameterFacade.getdailyExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());
			if(exchangeRate == null){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_EXCHANGE_RATE);
			}
			
			executeAction();
			cleanModelRegister();
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			createObjectMechanismOperationSession();
			createObjectRegister();
		
			searchCurrencySettlementRequestHeaderTO.setLstNegotiationMechanism(searchCurrencySettlementRequestTO.getLstNegotiationMechanism());
			searchCurrencySettlementRequestHeaderTO.setLstParticipant(searchCurrencySettlementRequestTO.getLstParticipant());
			searchCurrencySettlementRequestHeaderTO.setLstParticipantCounterpart(searchCurrencySettlementRequestTO.getLstParticipant());
			searchCurrencySettlementRequestHeaderTO.setLstSecurityClass(searchCurrencySettlementRequestTO.getLstSecurityClass());
			searchCurrencySettlementRequestHeaderTO.setLstCurrency(searchCurrencySettlementRequestTO.getLstCurrency());
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));	
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestHeaderTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
			searchCurrencySettlementRequestHeaderTO.setIndUserParticipant(BooleanType.YES.getCode());
		}else {
			searchCurrencySettlementRequestHeaderTO.setIndUserParticipant(BooleanType.NO.getCode());
		}
		searchCurrencySettlementRequestHeaderTO.setIdMechanismNegotiation(NegotiationMechanismType.BOLSA.getCode());
		changeNegotitationMechanism();
		return "registerCurrenncySettlementRequest";
	}
	
	/**
	 * Validate Securities.
	 */
	public void validateSecurities()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		cleanModelRegister();
		if (Validations.validateIsNullOrEmpty(searchCurrencySettlementRequestHeaderTO.getSecurity().getIdSecurityCodePk())) {
			searchCurrencySettlementRequestHeaderTO.getSecurity().setIdSecurityCodePk(null);		  
		   return;
		  }	
		// Get security from database.
		  Security security = null;
		  try {
			  security = currencySettlementFacade.getSecuritiesById(searchCurrencySettlementRequestHeaderTO.getSecurity().getIdSecurityCodePk());			 
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(security)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_SECURITY_NOT_EXIST) );			 
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
			  return;
		  }
		  if (!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_SECURITY_NOT_REGISTER) );			
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
			  return;
		  }
		  
		  searchCurrencySettlementRequestHeaderTO.setSecurity(security);
	}
	
	/**
	 * Validate Securities search.
	 */
	public void validateSecuritiesSearch()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		cleanModelRegister();
		if (Validations.validateIsNullOrEmpty(searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk())) {
			searchCurrencySettlementRequestTO.getSecurity().setIdSecurityCodePk(null);		  
		   return;
		  }	
		// Get security from database.
		  Security security = null;
		  try {
			  security = currencySettlementFacade.getSecuritiesById(searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk());			 
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(security)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_SECURITY_NOT_EXIST) );			 
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestTO.setSecurity(new Security());
		   return;
		  }
		  if (!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_SECURITY_NOT_REGISTER) );			
			  JSFUtilities.showSimpleValidationDialog();
			  searchCurrencySettlementRequestTO.setSecurity(new Security());
		   return;
		  }
		  
		  searchCurrencySettlementRequestTO.setSecurity(security);
	}
	
	/**
	 * Load Participant Role.
	 *
	 * @return the participant role
	 */
	public List<ParticipantRoleType> getParticipantRole(){	
		return ParticipantRoleType.list;	
	}
	
	/**
	 * Load State.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadState()throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY_SETTLEMENT_STATE.getCode());	
		searchCurrencySettlementRequestTO.setLstState(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));	
		for(ParameterTable param : searchCurrencySettlementRequestTO.getLstState()){
			mpParameters.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Load Motive Annul.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveAnnul()throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY_SETTLEMENT_ANNUL_MOTIVE.getCode());	
		motiveControllerAnnul.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * Load Motive Reject.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveReject()throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY_SETTLEMENT_REJECT_MOTIVE.getCode());	
		motiveControllerReject.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * load Currency.
	 *
	 * @throws ServiceException  the service exception
	 */
	private void loadCurrency() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		List<ParameterTable> lstParameterTable = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstParameterTable){
			if(parameterTable.getParameterState().equals(ParameterTableStateType.REGISTERED.getCode()) && parameterTable.getIndicator2().equals(GeneralConstants.ONE_VALUE_STRING)){
				lstParameterTableAux.add(parameterTable);
			}
			mpParameters.put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
		}
		searchCurrencySettlementRequestTO.setLstCurrency(lstParameterTableAux);
		
	}
	
	/**
	 * load Security Class.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityClass() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		searchCurrencySettlementRequestTO.setLstSecurityClass(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
	}
	/**
	 * Load Participant Buyer And Seller.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipantApplicantAndCounterpart() throws ServiceException{
		searchCurrencySettlementRequestTO.setLstParticipant(mcnOperationServiceFacade.getLstParticipants(null));
		searchCurrencySettlementRequestTO.setLstParticipantCounterpart(mcnOperationServiceFacade.getLstParticipants(null));
	}
	
	/**
	 * Create Object Register.
	 */
	public void createObjectRegister()
	{
		searchCurrencySettlementRequestHeaderTO = new SearchCurrencySettlementRequestTO(getCurrentSystemDate());
		searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
		searchCurrencySettlementRequestHeaderTO.setLstNegotiationMechanism(new ArrayList<NegotiationMechanism>());
		searchCurrencySettlementRequestHeaderTO.setLstNegotiationModality(new ArrayList<NegotiationModality>());
		searchCurrencySettlementRequestHeaderTO.setLstParticipant(new ArrayList<Participant>());
		searchCurrencySettlementRequestHeaderTO.setLstParticipantCounterpart(new ArrayList<Participant>());
		searchCurrencySettlementRequestHeaderTO.setLstSecurityClass(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestHeaderTO.setLstCurrency(new ArrayList<ParameterTable>());
		searchCurrencySettlementRequestHeaderTO.setSettlementDate(CommonsUtilities.currentDate());
	}
	
	/**
	 * create Object Mechanism Operation Session.
	 */
	public void createObjectMechanismOperationSession()
	{
		settlementRequestSession = new SettlementRequest();		
		settlementRequestSession.setSourceParticipant(new Participant());
		settlementRequestSession.setTargetParticipant(new Participant());
		settlementRequestSession.setLstCurrencySettlementDetail(new ArrayList<CurrencySettlementOperation>());
	}
	
	/**
	 * Search Mechanism Operation Handler.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchMechanismOperationHandler() throws ServiceException
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues(); 		
		if(Validations.validateIsNullOrEmpty(searchCurrencySettlementRequestHeaderTO.getSettlementDate())
				|| Validations.validateIsNullOrNotPositive(searchCurrencySettlementRequestHeaderTO.getIdParticipant())
				|| Validations.validateIsNullOrNotPositive(searchCurrencySettlementRequestHeaderTO.getIdParticipantCounterpart())
				|| Validations.validateIsNullOrNotPositive(searchCurrencySettlementRequestHeaderTO.getParticipantRole())){
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		}
		
		List<SearchSettlementOperationTO> lstMechanismOperation = currencySettlementFacade.getMechanismOperationByFilterServiceBean(searchCurrencySettlementRequestHeaderTO);
		
		settlementOperationsDataModel = new GenericDataModel<SearchSettlementOperationTO>(lstMechanismOperation);		 
	}
	
	/**
	 * Gets the list currency type.
	 *
	 * @return the list currency type
	 */
	public List<CurrencyType> getListCurrencyType(){
		executeAction();
		return CurrencyType.listSomeElements(CurrencyType.PYG,CurrencyType.USD);
	}
	
	/**
	 * lean New Price And New Amount.
	 *
	 * @param searchMechanismOperationTO search Mechanism Operation
	 */
	public void onSelectSettlementOperation(SearchSettlementOperationTO searchMechanismOperationTO){
		if(!searchMechanismOperationTO.getSelected()){
			searchMechanismOperationTO.setSettlementCurrency(searchMechanismOperationTO.getInitialSettlementCurrency());
			searchMechanismOperationTO.setExchangeRate(searchMechanismOperationTO.getInitialExchangeRate());
			searchMechanismOperationTO.setNewSettlementAmount(null);
			searchMechanismOperationTO.setNewSettlementPrice(null);
		}else{
			CurrencySettlementOperation currencySettlementDetail=null;

			try{
				currencySettlementDetail=currencySettlementFacade.requestExistsCurrencySettlement(
						searchMechanismOperationTO.getIdSettlementOperation(), searchMechanismOperationTO.getOperationPart(), SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(currencySettlementDetail)){
				searchMechanismOperationTO.setSelected(Boolean.FALSE);
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_EXISTS));			 
				  JSFUtilities.showSimpleValidationDialog();
			}				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * lean New Price And New Amount Selected.
	 *
	 * @param searchMechanismOperationTO search Mechanism Operation
	 */
	public void cleanNewPriceAndNewAmountSelected(SearchSettlementOperationTO searchMechanismOperationTO){
		if(searchMechanismOperationTO.getSelected()){
			if(Validations.validateIsNotNullAndNotEmpty(searchMechanismOperationTO.getExchangeRate()) && searchMechanismOperationTO.getExchangeRate().compareTo(BigDecimal.ZERO)==0){
				searchMechanismOperationTO.setExchangeRate(null);
			}
			searchMechanismOperationTO.setNewSettlementAmount(null);
			searchMechanismOperationTO.setNewSettlementPrice(null);			
		}
	}
	
	/**
	 * On change settlement currency.
	 *
	 * @param searchMechanismOperationTO the search mechanism operation to
	 */
	/*
	 * Select Currency Column
	 */
	public void onChangeSettlementCurrency(SearchSettlementOperationTO searchMechanismOperationTO){
		if(Validations.validateIsNotNullAndPositive(searchMechanismOperationTO.getSettlementCurrency()) && searchMechanismOperationTO.getSelected()){
			if(searchMechanismOperationTO.getInitialSettlementCurrency().equals(CurrencyType.PYG.getCode())){
				searchMechanismOperationTO.setSettlementCurrency(CurrencyType.USD.getCode());
				calculateNewPriceAndNewAmount(searchMechanismOperationTO);
			}else if(searchMechanismOperationTO.getInitialSettlementCurrency().equals(CurrencyType.USD.getCode())){
					searchMechanismOperationTO.setSettlementCurrency(CurrencyType.PYG.getCode());
					calculateNewPriceAndNewAmount(searchMechanismOperationTO);
			}											
		}else {
			cleanNewPriceAndNewAmountSelected(searchMechanismOperationTO);				
		}
	}
	
	/**
	 * Calculate New Price.
	 *
	 * @param searchMechanismOperationTO object search Mechanism Operation
	 */
	public void calculateNewPriceAndNewAmount(SearchSettlementOperationTO searchMechanismOperationTO){
		if(Validations.validateIsNotNullAndNotEmpty(searchMechanismOperationTO.getExchangeRate()) 
				&& Validations.validateIsNotNullAndNotEmpty(searchMechanismOperationTO.getSettlementCurrency())
				&& searchMechanismOperationTO.getSelected()){
			if((searchMechanismOperationTO.getExchangeRate().compareTo(BigDecimal.ZERO)==1)){
				BigDecimal roundedPrice = searchMechanismOperationTO.getSettlementPrice().setScale(2, RoundingMode.HALF_UP);
				if(searchMechanismOperationTO.getSettlementCurrency().equals(CurrencyType.PYG.getCode())){
					searchMechanismOperationTO.setNewSettlementPrice(roundedPrice.multiply(searchMechanismOperationTO.getExchangeRate()).setScale(8, RoundingMode.HALF_UP));
				}
				if(searchMechanismOperationTO.getSettlementCurrency().equals(CurrencyType.USD.getCode())){
					searchMechanismOperationTO.setNewSettlementPrice(roundedPrice.divide(searchMechanismOperationTO.getExchangeRate(), 8, RoundingMode.HALF_UP));
				}
				// -- FUCK THIS SHIT -> do not use NegotiationUtils.getSettlementAmount(a,b) because newSettlementPrice results from a rounded price.
				searchMechanismOperationTO.setNewSettlementAmount(NegotiationUtils.getSettlementAmount(searchMechanismOperationTO.getNewSettlementPrice(),searchMechanismOperationTO.getStockQuantity()));
				//searchMechanismOperationTO.setNewSettlementAmount(searchMechanismOperationTO.getNewSettlementPrice().multiply(searchMechanismOperationTO.getStockQuantity()));
			}
			else {
				cleanNewPriceAndNewAmountSelected(searchMechanismOperationTO);
			}
		}
		else {
			cleanNewPriceAndNewAmountSelected(searchMechanismOperationTO);
		}
	}
	
	/**
	 * Before Currency Settlemen tRequest Handler.
	 */
	public void beforeSaveCurrencySettlementRequest(){
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  if(Validations.validateIsNullOrEmpty(searchCurrencySettlementRequestHeaderTO.getSettlementDate())
					|| Validations.validateIsNullOrNotPositive(searchCurrencySettlementRequestHeaderTO.getIdParticipant())
					|| Validations.validateIsNullOrNotPositive(searchCurrencySettlementRequestHeaderTO.getIdParticipantCounterpart())
					|| Validations.validateIsNullOrNotPositive(searchCurrencySettlementRequestHeaderTO.getParticipantRole())
					|| Validations.validateIsNull(settlementOperationsDataModel))
			{
				  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
				   JSFUtilities.showSimpleValidationDialog();
				   return;
			}
		  Boolean validate=false;
		  int cont=0;
		  for(SearchSettlementOperationTO searchMechanismOperationTO : settlementOperationsDataModel.getDataList()){
			  if(searchMechanismOperationTO.getSelected()){
				  cont++;
				  if(Validations.validateIsNullOrEmpty(searchMechanismOperationTO.getSettlementCurrency())||Validations.validateIsNullOrNotPositive(searchMechanismOperationTO.getSettlementCurrency())
						  ||Validations.validateIsNullOrEmpty(searchMechanismOperationTO.getExchangeRate())||searchMechanismOperationTO.getExchangeRate().compareTo(BigDecimal.ZERO)==0
						  ||Validations.validateIsNullOrEmpty(searchMechanismOperationTO.getNewSettlementAmount())||searchMechanismOperationTO.getNewSettlementAmount().compareTo(BigDecimal.ZERO)==0
						  ||Validations.validateIsNullOrEmpty(searchMechanismOperationTO.getNewSettlementPrice())||searchMechanismOperationTO.getNewSettlementPrice().compareTo(BigDecimal.ZERO)==0) {
					  validate=Boolean.TRUE;
					  break;
				  }				 
			  }
		  }
		  
		  if(cont==0){
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_OPERATION_NOT_SELECTED));			 
			  JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  else{
			  if(validate) {			 
				  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));			 
				  JSFUtilities.showSimpleValidationDialog();
				   return;
			  }
		  }
		  
		  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REGISTER));	
		  JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
	}
	
	/**
	 * Clean Currency Settlement Request Handler.
	 */
	public void cleanCurrencySettlementRequestHandler(){
		executeAction();
		searchCurrencySettlementRequestHeaderTO.setIdMechanismNegotiation(null);
		searchCurrencySettlementRequestHeaderTO.setIdModalityNegotiation(null);
		searchCurrencySettlementRequestHeaderTO.setInitialDate(getCurrentSystemDate());
		searchCurrencySettlementRequestHeaderTO.setParticipantRole(null);
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestHeaderTO.setIdParticipant(null);
		}
		searchCurrencySettlementRequestHeaderTO.setIdParticipantCounterpart(null);
		searchCurrencySettlementRequestHeaderTO.setBallotNumber(null);
		searchCurrencySettlementRequestHeaderTO.setSequentialNumber(null);
		searchCurrencySettlementRequestHeaderTO.setCurrency(null);
		searchCurrencySettlementRequestHeaderTO.setSecurityClass(null);
		searchCurrencySettlementRequestHeaderTO.setSecurity(new Security());
		cleanModelRegister();
	}
	
	/**
	 * Clean Currency Settlement Request Search Handler.
	 */
	public void cleanCurrencySettlementRequestSearchHandler(){
		executeAction();
		searchCurrencySettlementRequestTO.setIdMechanismNegotiation(null);
		searchCurrencySettlementRequestTO.setIdModalityNegotiation(null);
		searchCurrencySettlementRequestTO.setCurrency(null);
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchCurrencySettlementRequestTO.setIdParticipant(null);
		}
		searchCurrencySettlementRequestTO.setIdParticipantCounterpart(null);
		searchCurrencySettlementRequestTO.setStateRequest(null);
		searchCurrencySettlementRequestTO.setBallotNumber(null);
		searchCurrencySettlementRequestTO.setSequentialNumber(null);
		searchCurrencySettlementRequestTO.setSecurityClass(null);
		searchCurrencySettlementRequestTO.setSecurity(new Security());
		searchCurrencySettlementRequestTO.setNumberRequest(null);
		searchCurrencySettlementRequestTO.setInitialDate(getCurrentSystemDate());
		searchCurrencySettlementRequestTO.setFinalDate(getCurrentSystemDate());
		cleanDataModel();
	}
	
	/**
	 * Save Currency Settlement Request Handler.
	 */
	@LoggerAuditWeb
	public void saveCurrencySettlementRequestHandler(){
		SettlementRequest settlementRequest=null;
		settlementRequestSession.setParticipantRole(searchCurrencySettlementRequestHeaderTO.getParticipantRole());
		Participant participantApplicant = new Participant();
		participantApplicant.setIdParticipantPk(searchCurrencySettlementRequestHeaderTO.getIdParticipant());	
		settlementRequestSession.setSourceParticipant(participantApplicant);
		Participant participantCounterpart = new Participant();
		participantCounterpart.setIdParticipantPk(searchCurrencySettlementRequestHeaderTO.getIdParticipantCounterpart());		
		settlementRequestSession.setTargetParticipant(participantCounterpart);
		List <CurrencySettlementOperation> lstCurrencySettOperation = new ArrayList<CurrencySettlementOperation>();
		try{
		
			for(SearchSettlementOperationTO searchMechanismOperationTO : settlementOperationsDataModel.getDataList()){
				 if(searchMechanismOperationTO.getSelected()){
					 
					 Long idMechanism = searchMechanismOperationTO.getIdMechanism();
					 Long idModality = searchMechanismOperationTO.getIdModality();
					 String settlementDate = CommonsUtilities.convertDatetoString(searchCurrencySettlementRequestHeaderTO.getSettlementDate());
					 AssignmentProcess assignmentProcess = currencySettlementFacade.findAssignmentProcess(idMechanism,idModality,searchCurrencySettlementRequestHeaderTO.getSettlementDate());
					 if(assignmentProcess!=null && AssignmentProcessStateType.CLOSED.getCode().equals( assignmentProcess.getAssignmentState() )){
						 throw new ServiceException(ErrorServiceType.ASSIGNMENT_PROCESS_CLOSED,
								 new Object[]{searchMechanismOperationTO.getMechanismName(), searchMechanismOperationTO.getModalityName(),settlementDate});
					 }
					 
					 CurrencySettlementOperation currencySettlementRequest = new CurrencySettlementOperation();
					 currencySettlementRequest.setSettlementOperation(new SettlementOperation(searchMechanismOperationTO.getIdSettlementOperation()));				 
					 currencySettlementRequest.setOperationPart(searchMechanismOperationTO.getOperationPart());
					 currencySettlementRequest.setInitialSettlementCurrency(searchMechanismOperationTO.getInitialSettlementCurrency());
					 currencySettlementRequest.setSettlementCurrency(searchMechanismOperationTO.getSettlementCurrency());
					 currencySettlementRequest.setExchangeRate(searchMechanismOperationTO.getExchangeRate());
					 currencySettlementRequest.setSettlementPrice(searchMechanismOperationTO.getNewSettlementPrice());
					 currencySettlementRequest.setSettlementAmount(searchMechanismOperationTO.getNewSettlementAmount());
					 currencySettlementRequest.setInitialSettlementPrice(searchMechanismOperationTO.getSettlementPrice());
					 currencySettlementRequest.setInitialSettlementAmount(searchMechanismOperationTO.getSettlementAmount());
					 lstCurrencySettOperation.add(currencySettlementRequest);
					 
				 }
			}
			settlementRequestSession.setLstCurrencySettlementDetail(lstCurrencySettOperation);
			settlementRequestSession.setRequestType(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode());
			
			settlementRequest = currencySettlementFacade.registryCurrencySettlementRequestFacade(settlementRequestSession,
					searchCurrencySettlementRequestHeaderTO.getIndUserParticipant());
			
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				cleanCurrencySettlementRequestSearchHandler();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		} 
		if(Validations.validateIsNotNullAndNotEmpty(settlementRequest)){
			
			//Sending notification
			for(SearchSettlementOperationTO searchMechanismOperationTO : settlementOperationsDataModel.getDataList()){
				 if(searchMechanismOperationTO.getSelected()){
					 BusinessProcess businessProcessNotification = new BusinessProcess();					
						businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REGISTER.getCode());
						Object[] parameters = new Object[]{
								mpParameters.get(settlementRequest.getRequestType()),
								settlementRequest.getIdSettlementRequestPk(),
								searchMechanismOperationTO.getMechanismName(),
								searchMechanismOperationTO.getModalityName(),
								searchMechanismOperationTO.getOperationDate(),
								searchMechanismOperationTO.getBallotNumber() == null ? 
									searchMechanismOperationTO.getOperationNumber():
									searchMechanismOperationTO.getBallotNumber() + GeneralConstants.SLASH + searchMechanismOperationTO.getSequential() };
								
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
								businessProcessNotification,settlementRequest.getSourceParticipant().getIdParticipantPk(), parameters);
				 }
			}
			
				
			setViewOperationType(ViewOperationsType.CONSULT.getCode());	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REGISTER_OK));						
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}	
		cleanCurrencySettlementRequestSearchHandler();
	}
	
	/**
	 * View Marketfact.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param idSecurityCode the id security code
	 */
	public void viewMarketfact(Long idMechanismOperation, String idSecurityCode)
	{
		searchMechanismOperationMarketFactTO = new SearchMechanismOperationMarketFactTO();
		MechanismOperationMarketFact mechanismOperationMarketFact=null;
		Security security = null;
		try{
		security = currencySettlementFacade.getSecuritiesById(idSecurityCode);
		if(Validations.validateIsNotNull(security)){
			searchMechanismOperationMarketFactTO.setIdSecurityCodePk(security.getIdSecurityCodePk());
			searchMechanismOperationMarketFactTO.setSecurityDescription(security.getDescription());
			searchMechanismOperationMarketFactTO.setInstrumentType(security.getInstrumentType());
			searchMechanismOperationMarketFactTO.setInstrumentDescription(security.getInstrumentTypeDescription());
		}		
		mechanismOperationMarketFact = currencySettlementFacade.getMechanismOperationMarketFact(idMechanismOperation);
		if(Validations.validateIsNotNull(mechanismOperationMarketFact)){
			searchMechanismOperationMarketFactTO.setMarketDate(mechanismOperationMarketFact.getMarketDate());
			searchMechanismOperationMarketFactTO.setMarketPrice(mechanismOperationMarketFact.getMarketPrice());
			searchMechanismOperationMarketFactTO.setMarketRate(mechanismOperationMarketFact.getMarketRate());
		}		
		JSFUtilities.executeJavascriptFunction("PF('dialogWMarketfactBalance').show()");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Validate approve action.
	 *
	 * @return the string
	 */
	public String validateApproveAction(){
		return validateAction(SettlementCurrencyRequestStateType.APPROVED.getCode());
	}
	
	/**
	 * Validate annular action.
	 *
	 * @return the string
	 */
	public String validateAnnularAction(){
		return validateAction(SettlementCurrencyRequestStateType.CANCELLED.getCode());
	}
	
	/**
	 * Validate review action.
	 *
	 * @return the string
	 */
	public String validateReviewAction(){
		return validateAction(SettlementCurrencyRequestStateType.REVISED.getCode());
	}
	
	/**
	 * Validate confirm action.
	 *
	 * @return the string
	 */
	public String validateConfirmAction(){
		return validateAction(SettlementCurrencyRequestStateType.CONFIRMED.getCode());
	}
	
	/**
	 * Validate reject action.
	 *
	 * @return the string
	 */
	public String validateRejectAction(){
		return validateAction(SettlementCurrencyRequestStateType.REJECTED.getCode());
	}
	
	/**
	 * Validate authorize action.
	 *
	 * @return the string
	 */
	public String validateAuthorizeAction(){
		return validateAction(SettlementCurrencyRequestStateType.AUTHORIZED.getCode());
	}
	
	/**
	 * Validate action.
	 *
	 * @param newState the new state
	 * @return the string
	 */
	public String validateAction(Integer newState){
		executeAction();
		List<Integer> expectedStates = new ArrayList<Integer>();
		List<Long> expectedParticipants = new ArrayList<Long>();
		
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(SettlementCurrencyRequestStateType.APPROVED.getCode().equals(newState)){		
			expectedParticipants.add(selectedCurrencySettlementRequest.getSourceParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.REGISTERED.getCode());
			setViewOperationType(ViewOperationsType.APPROVE.getCode());
		}else if(SettlementCurrencyRequestStateType.CANCELLED.getCode().equals(newState)){	
			expectedParticipants.add(selectedCurrencySettlementRequest.getSourceParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.REGISTERED.getCode());
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
		}else if(SettlementCurrencyRequestStateType.REJECTED.getCode().equals(newState)){		
			expectedParticipants.add(selectedCurrencySettlementRequest.getTargetParticipant().getIdParticipantPk());
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				expectedStates.add(SettlementCurrencyRequestStateType.REVISED.getCode());
				expectedStates.add(SettlementCurrencyRequestStateType.APPROVED.getCode());
			}else{
				expectedStates.add(SettlementCurrencyRequestStateType.REVISED.getCode());
				expectedStates.add(SettlementCurrencyRequestStateType.APPROVED.getCode());
				expectedStates.add(SettlementCurrencyRequestStateType.CONFIRMED.getCode());
			}
			setViewOperationType(ViewOperationsType.REJECT.getCode());
		}else if(SettlementCurrencyRequestStateType.REVISED.getCode().equals(newState)){
			expectedParticipants.add(selectedCurrencySettlementRequest.getTargetParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.APPROVED.getCode());
			setViewOperationType(ViewOperationsType.REVIEW.getCode());
		}else if(SettlementCurrencyRequestStateType.CONFIRMED.getCode().equals(newState)){	
			expectedParticipants.add(selectedCurrencySettlementRequest.getTargetParticipant().getIdParticipantPk());
			expectedStates.add(SettlementCurrencyRequestStateType.REVISED.getCode());
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		}else if(SettlementCurrencyRequestStateType.AUTHORIZED.getCode().equals(newState)){			
			expectedStates.add(SettlementCurrencyRequestStateType.CONFIRMED.getCode());
			setViewOperationType(ViewOperationsType.AUTHORIZE.getCode());
		}
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion() 
				&& searchCurrencySettlementRequestTO.getIdParticipant()!=null 
				&& !expectedParticipants.contains(searchCurrencySettlementRequestTO.getIdParticipant())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
					, PropertiesUtilities.getMessage("msg.settlement.dateExchange.invalidAction.participant"));
			JSFUtilities.showValidationDialog();
			return null;
		}
			
		if(!expectedStates.contains(selectedCurrencySettlementRequest.getRequestState())){
			
			List<String> stringStates = new ArrayList<String>();
			for(Integer state : expectedStates){
				stringStates.add((String) mpParameters.get(state));
			}
			
			Object[] bodyData = new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest().toString(),
					StringUtils.join(stringStates.toArray(),GeneralConstants.STR_COMMA_WITHOUT_SPACE)};
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_VALIDATION_STATE, bodyData));
			JSFUtilities.showValidationDialog();
			return null;
		}
		
		cleanModelRegister();			
		return "viewCurrenncySettlementRequest";
	}
	
	/**
	 * Search Currency Settlement Request Handler.
	 */
	@LoggerAuditWeb
	public void searchCurrencySettlementRequestHandler(){
		selectedCurrencySettlementRequest = null;
		JSFUtilities.hideGeneralDialogues(); 		
		List <SettlementRequestTO> lstCurrencySettlementRequest=null;		
		try{			
			searchCurrencySettlementRequestTO.setRequestType(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode());
			lstCurrencySettlementRequest =  currencySettlementFacade.getListCurrencySettlementRequestFilter(searchCurrencySettlementRequestTO);
			currencySettlementRequestsDataModel = new GenericDataModel<SettlementRequestTO>(lstCurrencySettlementRequest);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	/**
	 * Before approve handler.
	 */
	public void beforeApproveHandler(){
		executeAction();
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_APPROVE_CONFIRM,
				    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );			
			this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
	}
	/**
	 * Before Review handler.
	 */
	public void beforeReviewHandler(){
		executeAction();
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.APPROVED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REVIEW)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVIEW_CONFIRM,
				    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );			
			this.setViewOperationType(ViewOperationsType.REVIEW.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
	}
	/**
	 * Before annular handler.
	 */
	public void beforeAnnularHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.REGISTERED.getCode())){
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
			motiveControllerAnnul.setIdMotivePK(0);
			motiveControllerAnnul.setMotiveText(null);
			motiveControllerAnnul.setShowMotiveText(false);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL),null);	
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
			return;
		}
	}
	/**
	 * Before reject handler.
	 */
	public void beforeRejectHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.APPROVED.getCode())
				|| selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.REVISED.getCode())
				|| selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.CONFIRMED.getCode())){
			setViewOperationType(ViewOperationsType.REJECT.getCode());
			motiveControllerAnnul.setIdMotivePK(0);
			motiveControllerAnnul.setMotiveText(null);
			motiveControllerAnnul.setShowMotiveText(false);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT),null);	
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
			return;
		}
	}
	/**
	 * Before confirm handler.
	 */
	public void beforeConfirmHandler()
	{
		executeAction();
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.REVISED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_CONFIRM_CONFIRM,
				    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );			
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
	}
	/**
	 * Before Authorize handler.
	 */
	public void beforeAuthorizeHandler()
	{
		executeAction();
		if(selectedCurrencySettlementRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.CONFIRMED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_AUTHORIZE)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_AUTHORIZE_CONFIRM,
				    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );			
			this.setViewOperationType(ViewOperationsType.ACTIVATE.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
	}
	/**
	 * Change action currency settlement request handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		switch(ViewOperationsType.lookup.get(getViewOperationType())){
			case APPROVE : approveCurrencySettlementRequest();break;
			case ANULATE: annularCurrencySettlementRequest();break;
			case REVIEW: reviewCurrencySettlementRequest();break;
			case CONFIRM : confirmCurrencySettlementRequest();break;
			case REJECT: rejectCurrencySettlementRequest();break;
			case ACTIVATE: authorizeCurrencySettlementRequest();break;
		}
		cleanCurrencySettlementRequestSearchHandler();
	}
	/**
	 * Test batch process.
	 */
	@LoggerAuditWeb
	public void testBatchProcess(){
//		JSFUtilities.hideGeneralDialogues();  
//		BusinessProcess businessProcess = new BusinessProcess();
//		businessProcess.setIdBusinessProcessPk(BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_AUTOMATIC_REJECT.getCode());
//		try {
//			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, new HashMap<String, Object>());
//		} catch (ServiceException e) {
//			e.printStackTrace();
//		}
//		JSFUtilities.executeJavascriptFunction("batchWOk.show()");
	}
	
	/**
	 * Send change state notification.
	 *
	 * @param currencySettlementRequest the currency settlement request
	 * @param idBusinessProcess the id business process
	 */
	private void sendChangeStateNotification(SettlementRequestTO currencySettlementRequest, Long idBusinessProcess) {
		//Sending notification
		BusinessProcess businessProcessNotification = new BusinessProcess();					
		businessProcessNotification.setIdBusinessProcessPk(idBusinessProcess);
		
		for(SettlementRequestDetailTO currencySettlementOperation : currencySettlementRequest.getSettlementRequestDetail()){
			Object[] parameters = new Object[]{
					mpParameters.get(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode()),
					currencySettlementRequest.getIdSettlementRequest(),
					currencySettlementOperation.getMechanismName(),
					currencySettlementOperation.getModalityName(),
					CommonsUtilities.convertDatetoString(currencySettlementOperation.getOperationDate()),
					currencySettlementOperation.getBallotNumber() == null ? 
						currencySettlementOperation.getOperationNumber():
						currencySettlementOperation.getBallotNumberSequential() };
			if(BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_APPROVE.getCode().equals(idBusinessProcess)) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,currencySettlementRequest.getTargetParticipant().getIdParticipantPk(), parameters);
			} else if(BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_CONFIRM.getCode().equals(idBusinessProcess)) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcessNotification, null, parameters);
			} else if(BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REVIEW.getCode().equals(idBusinessProcess)) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,currencySettlementRequest.getTargetParticipant().getIdParticipantPk(), parameters);
			} else {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,currencySettlementRequest.getSourceParticipant().getIdParticipantPk(), parameters);
			}				
		}		
	}
	
	/**
	 * Annul Currency settlement request.
	 */
	@LoggerAuditWeb
	private void annularCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		selectedCurrencySettlementRequest.setAnnulMotive(Integer.parseInt(""+motiveControllerAnnul.getIdMotivePK()));
		selectedCurrencySettlementRequest.setAnnulOtherMotive(motiveControllerAnnul.getMotiveText());
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.annulCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_CANCEL.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_ANNUL_OK,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Reject currency settlement request.
	 */
	@LoggerAuditWeb
	private void rejectCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues(); 
		selectedCurrencySettlementRequest.setRejectMotive(Integer.parseInt(""+motiveControllerReject.getIdMotivePK()));
		selectedCurrencySettlementRequest.setRejectOtherMotive(motiveControllerReject.getMotiveText());
		SettlementRequest currencySettlementRequest = null;
		try {
			if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.APPROVED.getCode()))
				currencySettlementRequest = currencySettlementFacade.rejectCurrencySettlementRequestApproveFacade(selectedCurrencySettlementRequest);
			else 	{
				if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.REVISED.getCode()))
					currencySettlementRequest = currencySettlementFacade.rejectCurrencySettlementRequestReviewFacade(selectedCurrencySettlementRequest);
				else {
					if(selectedCurrencySettlementRequest.getRequestState().equals(SettlementCurrencyRequestStateType.CONFIRMED.getCode()))
						currencySettlementRequest = currencySettlementFacade.rejectCurrencySettlementRequestConfirmFacade(selectedCurrencySettlementRequest);
				}
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REJECT.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REJECT_OK,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Approve currency settlement request.
	 */
	@LoggerAuditWeb
	private void approveCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.approveCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_APPROVE.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_APPROVE_OK,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Review currency settlement request.
	 */
	@LoggerAuditWeb
	private void reviewCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		
				
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.reviewCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REVIEW.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REVIEW_OK,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Confirm currency settlement request.
	 */
	@LoggerAuditWeb
	private void confirmCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  						
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.confirmCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_CONFIRM.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_CONFIRM_OK,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Authorize currency settlement request.
	 */
	@LoggerAuditWeb
	private void authorizeCurrencySettlementRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  				
		SettlementRequest currencySettlementRequest = null;
		try {
			currencySettlementRequest = currencySettlementFacade.authorizeCurrencySettlementRequestFacade(selectedCurrencySettlementRequest);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(currencySettlementRequest!=null){
			
			//Sending notification
			sendChangeStateNotification(selectedCurrencySettlementRequest, BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_AUTHORIZE.getCode());
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_AUTHORIZE_OK,
				    		  new Object[]{currencySettlementRequest.getIdSettlementRequestPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Select Currency Settlement Request Handler.
	 *
	 * @param currencySettlementRequest object currency Settlement Request
	 */
	public void selectCurrencySettlementRequestHandler(SettlementRequestTO currencySettlementRequest){
		executeAction();
		cleanModelRegister();	
		
		selectedCurrencySettlementRequest = currencySettlementRequest;

		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveAnnulHandler(){
		if(this.getMotiveControllerAnnul().getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveAnnulType.OTHER.getCode().toString())){
			this.getMotiveControllerAnnul().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerAnnul().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Change motive Reject handler.
	 */
	public void changeMotiveRejectHandler(){
		if(this.getMotiveControllerReject().getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveRejectType.OTHER.getCode().toString())){
			this.getMotiveControllerReject().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerReject().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveAnnulHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerAnnul.getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveAnnulType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerAnnul().getMotiveText())){	
				setViewOperationType(ViewOperationsType.ANULATE.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_ANNUL_CONFIRM,
			    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );	
		setViewOperationType(ViewOperationsType.ANULATE.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	/**
	 * Confirm motive Reject handler.
	 */
	public void confirmMotiveRejectHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerReject.getIdMotivePK()==Long.parseLong(CurrencySettlementMotiveRejectType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerReject().getMotiveText())){
				setViewOperationType(ViewOperationsType.REJECT.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_CURRENCY_SETTLEMENT_REQUEST_REJECT_CONFIRM,
			    		  new Object[]{selectedCurrencySettlementRequest.getIdSettlementRequest()}) );	
		setViewOperationType(ViewOperationsType.REJECT.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	
	/**
	 * Clean Model Register.
	 */
	public void cleanModelRegister()
	{
		settlementOperationsDataModel=null;
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		currencySettlementRequestsDataModel = null;
	}
	
	/**
	 * Gets the search currency settlement request to.
	 *
	 * @return the search currency settlement request to
	 */
	public SearchCurrencySettlementRequestTO getSearchCurrencySettlementRequestTO() {
		return searchCurrencySettlementRequestTO;
	}
	
	/**
	 * Sets the search currency settlement request to.
	 *
	 * @param searchCurrencySettlementRequestTO the new search currency settlement request to
	 */
	public void setSearchCurrencySettlementRequestTO(
			SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO) {
		this.searchCurrencySettlementRequestTO = searchCurrencySettlementRequestTO;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Gets the search currency settlement request header to.
	 *
	 * @return the search currency settlement request header to
	 */
	public SearchCurrencySettlementRequestTO getSearchCurrencySettlementRequestHeaderTO() {
		return searchCurrencySettlementRequestHeaderTO;
	}
	
	/**
	 * Sets the search currency settlement request header to.
	 *
	 * @param searchCurrencySettlementRequestHeaderTO the new search currency settlement request header to
	 */
	public void setSearchCurrencySettlementRequestHeaderTO(
			SearchCurrencySettlementRequestTO searchCurrencySettlementRequestHeaderTO) {
		this.searchCurrencySettlementRequestHeaderTO = searchCurrencySettlementRequestHeaderTO;
	}
	
	/**
	 * Gets the settlement operations data model.
	 *
	 * @return the settlement operations data model
	 */
	public GenericDataModel<SearchSettlementOperationTO> getSettlementOperationsDataModel() {
		return settlementOperationsDataModel;
	}
	
	/**
	 * Sets the settlement operations data model.
	 *
	 * @param settlementOperationsDataModel the new settlement operations data model
	 */
	public void setSettlementOperationsDataModel(
			GenericDataModel<SearchSettlementOperationTO> settlementOperationsDataModel) {
		this.settlementOperationsDataModel = settlementOperationsDataModel;
	}
	
	/**
	 * Gets the search mechanism operation market fact to.
	 *
	 * @return the search mechanism operation market fact to
	 */
	public SearchMechanismOperationMarketFactTO getSearchMechanismOperationMarketFactTO() {
		return searchMechanismOperationMarketFactTO;
	}
	
	/**
	 * Sets the search mechanism operation market fact to.
	 *
	 * @param searchMechanismOperationMarketFactTO the new search mechanism operation market fact to
	 */
	public void setSearchMechanismOperationMarketFactTO(
			SearchMechanismOperationMarketFactTO searchMechanismOperationMarketFactTO) {
		this.searchMechanismOperationMarketFactTO = searchMechanismOperationMarketFactTO;
	}
	
	/**
	 * Gets the currency settlement requests data model.
	 *
	 * @return the currency settlement requests data model
	 */
	public GenericDataModel<SettlementRequestTO> getCurrencySettlementRequestsDataModel() {
		return currencySettlementRequestsDataModel;
	}
	
	/**
	 * Sets the currency settlement requests data model.
	 *
	 * @param currencySettlementRequestsDataModel the new currency settlement requests data model
	 */
	public void setCurrencySettlementRequestsDataModel(
			GenericDataModel<SettlementRequestTO> currencySettlementRequestsDataModel) {
		this.currencySettlementRequestsDataModel = currencySettlementRequestsDataModel;
	}
	
	/**
	 * Gets the motive controller annul.
	 *
	 * @return the motive controller annul
	 */
	public MotiveController getMotiveControllerAnnul() {
		return motiveControllerAnnul;
	}
	
	/**
	 * Sets the motive controller annul.
	 *
	 * @param motiveControllerAnnul the new motive controller annul
	 */
	public void setMotiveControllerAnnul(MotiveController motiveControllerAnnul) {
		this.motiveControllerAnnul = motiveControllerAnnul;
	}
	
	/**
	 * Gets the motive controller reject.
	 *
	 * @return the motive controller reject
	 */
	public MotiveController getMotiveControllerReject() {
		return motiveControllerReject;
	}
	
	/**
	 * Sets the motive controller reject.
	 *
	 * @param motiveControllerReject the new motive controller reject
	 */
	public void setMotiveControllerReject(MotiveController motiveControllerReject) {
		this.motiveControllerReject = motiveControllerReject;
	}
	
	/**
	 * Gets the mp parameters.
	 *
	 * @return the mp parameters
	 */
	public Map<Integer, Object> getMpParameters() {
		return mpParameters;
	}
	
	/**
	 * Sets the mp parameters.
	 *
	 * @param mpParameters the mp parameters
	 */
	public void setMpParameters(Map<Integer, Object> mpParameters) {
		this.mpParameters = mpParameters;
	}
	
	/**
	 * Gets the selected currency settlement request.
	 *
	 * @return the selected currency settlement request
	 */
	public SettlementRequestTO getSelectedCurrencySettlementRequest() {
		return selectedCurrencySettlementRequest;
	}
	
	/**
	 * Sets the selected currency settlement request.
	 *
	 * @param selectedCurrencySettlementRequest the new selected currency settlement request
	 */
	public void setSelectedCurrencySettlementRequest(
			SettlementRequestTO selectedCurrencySettlementRequest) {
		this.selectedCurrencySettlementRequest = selectedCurrencySettlementRequest;
	}
	
	/**
	 * Gets the settlement request session.
	 *
	 * @return the settlement request session
	 */
	public SettlementRequest getSettlementRequestSession() {
		return settlementRequestSession;
	}
	
	/**
	 * Sets the settlement request session.
	 *
	 * @param settlementRequestSession the new settlement request session
	 */
	public void setSettlementRequestSession(
			SettlementRequest settlementRequestSession) {
		this.settlementRequestSession = settlementRequestSession;
	}
		
}
