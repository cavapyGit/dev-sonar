package com.pradera.settlements.currencyexchange.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchCurrencySettlementRequestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchCurrencySettlementRequestTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The request type. */
	private Integer requestType;
	/** The id Currency Settlement Request. */
	private Long idCurrencySettlementRequest;
	
	/** The id mechanism negotiation. */
	private Long idMechanismNegotiation;
	
	/** The id modality negotiation. */
	private Long idModalityNegotiation;
	
	/** The id Participant role. */
	private Integer participantRole;
	
	/** The id Participant applicant. */
	private Long idParticipant;
	
	/** The id participant counterpart. */
	private Long idParticipantCounterpart;
	
	/** The ballot Number. */
	private Long ballotNumber;
	
	/** The sequential Number. */
	private Long sequentialNumber;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The security code. */
	private Security security;
	
	/** The currency. */
	private Integer currency;
	
	/** The settlement Date. */
	private Date settlementDate;
	
	/** The initial Date. */
	private Date initialDate;
	
	/** The final Date. */
	private Date finalDate;
	
	/** The number Request. */
	private Long numberRequest;
	
	/** The state request. */
	private Integer stateRequest;
	
	/** The annul motive. */
	private Integer annulMotive;
	
	/** The annul other motive. */
	private String annulOtherMotive;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The reject other motive. */
	private String rejectOtherMotive;
	
	/** The mechanism negotiation list. */
	private List<NegotiationMechanism> lstNegotiationMechanism;
	
	/** The modality negotiation list. */
	private List<NegotiationModality> lstNegotiationModality;
	
	/** The participant Applicant List. */
	private List<Participant> lstParticipant;
	
	/** The lst participant counterpart. */
	private List<Participant> lstParticipantCounterpart;
	
	/** The Security Class List. */
	private List<ParameterTable> lstSecurityClass;
	
	/** The Currency List. */
	private List<ParameterTable> lstCurrency;
	
	/** The State List. */
	private List<ParameterTable> lstState;
	
	/** The Mechanism Operation. */
	private SearchSettlementOperationTO searchMechanismOperationTO;
	
	/** The ind User Participant. */
	private Integer indUserParticipant;
	
	
	/**
	 * Instantiates a new search currency settlement request to.
	 */
	public SearchCurrencySettlementRequestTO() {

	}
	
	/**
	 * Instantiates a new search currency settlement request to.
	 *
	 * @param settlementDate the settlement date
	 */
	public SearchCurrencySettlementRequestTO(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Instantiates a new search currency settlement request to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 */
	public SearchCurrencySettlementRequestTO(Date initialDate, Date finalDate) {
		this.initialDate = initialDate;
		this.finalDate = finalDate;
	}

	/**
	 * Gets the id mechanism negotiation.
	 *
	 * @return the id mechanism negotiation
	 */
	public Long getIdMechanismNegotiation() {
		return idMechanismNegotiation;
	}

	/**
	 * Sets the id mechanism negotiation.
	 *
	 * @param idMechanismNegotiation the new id mechanism negotiation
	 */
	public void setIdMechanismNegotiation(Long idMechanismNegotiation) {
		this.idMechanismNegotiation = idMechanismNegotiation;
	}

	/**
	 * Gets the id modality negotiation.
	 *
	 * @return the id modality negotiation
	 */
	public Long getIdModalityNegotiation() {
		return idModalityNegotiation;
	}

	/**
	 * Sets the id modality negotiation.
	 *
	 * @param idModalityNegotiation the new id modality negotiation
	 */
	public void setIdModalityNegotiation(Long idModalityNegotiation) {
		this.idModalityNegotiation = idModalityNegotiation;
	}

	/**
	 * Gets the participant role.
	 *
	 * @return the participant role
	 */
	public Integer getParticipantRole() {
		return participantRole;
	}

	/**
	 * Sets the participant role.
	 *
	 * @param participantRole the new participant role
	 */
	public void setParticipantRole(Integer participantRole) {
		this.participantRole = participantRole;
	}

	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}

	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}

	/**
	 * Gets the sequential number.
	 *
	 * @return the sequential number
	 */
	public Long getSequentialNumber() {
		return sequentialNumber;
	}

	/**
	 * Sets the sequential number.
	 *
	 * @param sequentialNumber the new sequential number
	 */
	public void setSequentialNumber(Long sequentialNumber) {
		this.sequentialNumber = sequentialNumber;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}


	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}

	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the number request.
	 *
	 * @return the number request
	 */
	public Long getNumberRequest() {
		return numberRequest;
	}

	/**
	 * Sets the number request.
	 *
	 * @param numberRequest the new number request
	 */
	public void setNumberRequest(Long numberRequest) {
		this.numberRequest = numberRequest;
	}

	/**
	 * Gets the state request.
	 *
	 * @return the state request
	 */
	public Integer getStateRequest() {
		return stateRequest;
	}

	/**
	 * Sets the state request.
	 *
	 * @param stateRequest the new state request
	 */
	public void setStateRequest(Integer stateRequest) {
		this.stateRequest = stateRequest;
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}

	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}

	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}

	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(
			List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}

	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	/**
	 * Gets the search mechanism operation to.
	 *
	 * @return the search mechanism operation to
	 */
	public SearchSettlementOperationTO getSearchMechanismOperationTO() {
		return searchMechanismOperationTO;
	}

	/**
	 * Sets the search mechanism operation to.
	 *
	 * @param searchMechanismOperationTO the new search mechanism operation to
	 */
	public void setSearchMechanismOperationTO(
			SearchSettlementOperationTO searchMechanismOperationTO) {
		this.searchMechanismOperationTO = searchMechanismOperationTO;
	}

	/**
	 * Gets the ind user participant.
	 *
	 * @return the ind user participant
	 */
	public Integer getIndUserParticipant() {
		return indUserParticipant;
	}

	/**
	 * Sets the ind user participant.
	 *
	 * @param indUserParticipant the new ind user participant
	 */
	public void setIndUserParticipant(Integer indUserParticipant) {
		this.indUserParticipant = indUserParticipant;
	}

	/**
	 * Gets the id currency settlement request.
	 *
	 * @return the id currency settlement request
	 */
	public Long getIdCurrencySettlementRequest() {
		return idCurrencySettlementRequest;
	}

	/**
	 * Sets the id currency settlement request.
	 *
	 * @param idCurrencySettlementRequest the new id currency settlement request
	 */
	public void setIdCurrencySettlementRequest(Long idCurrencySettlementRequest) {
		this.idCurrencySettlementRequest = idCurrencySettlementRequest;
	}

	/**
	 * Gets the annul motive.
	 *
	 * @return the annul motive
	 */
	public Integer getAnnulMotive() {
		return annulMotive;
	}

	/**
	 * Sets the annul motive.
	 *
	 * @param annulMotive the new annul motive
	 */
	public void setAnnulMotive(Integer annulMotive) {
		this.annulMotive = annulMotive;
	}

	/**
	 * Gets the annul other motive.
	 *
	 * @return the annul other motive
	 */
	public String getAnnulOtherMotive() {
		return annulOtherMotive;
	}

	/**
	 * Sets the annul other motive.
	 *
	 * @param annulOtherMotive the new annul other motive
	 */
	public void setAnnulOtherMotive(String annulOtherMotive) {
		this.annulOtherMotive = annulOtherMotive;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the lst participant counterpart.
	 *
	 * @return the lst participant counterpart
	 */
	public List<Participant> getLstParticipantCounterpart() {
		return lstParticipantCounterpart;
	}

	/**
	 * Sets the lst participant counterpart.
	 *
	 * @param lstParticipantCounterpart the new lst participant counterpart
	 */
	public void setLstParticipantCounterpart(
			List<Participant> lstParticipantCounterpart) {
		this.lstParticipantCounterpart = lstParticipantCounterpart;
	}

	/**
	 * Gets the id participant counterpart.
	 *
	 * @return the id participant counterpart
	 */
	public Long getIdParticipantCounterpart() {
		return idParticipantCounterpart;
	}

	/**
	 * Sets the id participant counterpart.
	 *
	 * @param idParticipantCounterpart the new id participant counterpart
	 */
	public void setIdParticipantCounterpart(Long idParticipantCounterpart) {
		this.idParticipantCounterpart = idParticipantCounterpart;
	}
	
}
