package com.pradera.settlements.currencyexchange.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.settlement.CurrencySettlementOperation;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.CurrencySettlementMotiveRejectType;
import com.pradera.model.settlement.type.SettlementCurrencyRequestStateType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.currencyexchange.to.SearchCurrencySettlementRequestTO;
import com.pradera.settlements.currencyexchange.to.SearchSettlementOperationTO;
import com.pradera.settlements.currencyexchange.to.SettlementRequestDetailTO;
import com.pradera.settlements.currencyexchange.to.SettlementRequestTO;
import com.pradera.settlements.utils.NegotiationUtils;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CurrencySettlementService.
 *
 * @author Pradera Technologies
 */
@Stateless
public class CurrencySettlementService extends CrudDaoServiceBean{
	
	/** The settlement process service. */
	@EJB
	SettlementProcessService settlementProcessService;
	
	/**
	 * Gets the securities by id.
	 *
	 * @param idIsinCode the id isin code
	 * @return the securities by id
	 * @throws ServiceException the service exception
	 */
	public Security getSecuritiesById(String idIsinCode) throws ServiceException{
		try{
		Security objSecurity = null;
		try {
			objSecurity = this.find(Security.class, idIsinCode);
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return objSecurity;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Mechanism Operation By Filter Service Bean.
	 *
	 * @param searchCurrencySettlementRequestTO filter Currency Settlement
	 * @return List Mechanism Operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SearchSettlementOperationTO> getMechanismOperationByFilterServiceBean(SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();			
			stringBuilder.append("	select mo.idMechanismOperationPk,	");//0
			stringBuilder.append("	mo.ballotNumber, mo.sequential,	"); //1,2
			if(searchCurrencySettlementRequestTO.getParticipantRole().equals(ComponentConstant.PURCHARSE_ROLE)){
				stringBuilder.append("	sp.idParticipantPk,sp.description,	"); //3,4
			}
			if(searchCurrencySettlementRequestTO.getParticipantRole().equals(ComponentConstant.SALE_ROLE)){
				stringBuilder.append("	bp.idParticipantPk,bp.description,	"); 
			}
			stringBuilder.append("	se.securityClass, (Select p1.parameterName From ParameterTable p1 Where p1.parameterTablePk = se.securityClass), se.idSecurityCodePk,	");	//5 6 7
			stringBuilder.append("	se.currency,(Select p2.parameterName From ParameterTable p2 Where p2.parameterTablePk = se.currency),	");	//8 9
			stringBuilder.append("	so.stockQuantity, so.settlementCurrency, so.settlementPrice, so.settlementAmount, "); // 10 11 12 13
			stringBuilder.append("	(select p3.buyPrice From DailyExchangeRates p3 Where TRUNC(p3.dateRate) = so.settlementDate  AND p3.idCurrency = :idCurrencyDOL and p3.idSourceinformation =  "+InformationSourceType.BCRD.getCode()+"),   ");
			stringBuilder.append("	so.operationState, so.indRemoveSettlement , so.indReenterSettlement, ");	//15 16 17
			stringBuilder.append("  so.indCurrencyExchange, so.indPrepaid , so.indExtended, ");//18 19 20
			stringBuilder.append("  nmo.idNegotiationModalityPk, nmo.modalityName, "); // 21 22
			stringBuilder.append("  nme.idNegotiationMechanismPk,nme.mechanismName,	");//23 24
			stringBuilder.append("  mo.operationNumber, mo.operationDate, ");//25 26
			stringBuilder.append("  so.idSettlementOperationPk, ");//27
			stringBuilder.append("  so.operationPart ");//28
			stringBuilder.append("	from SettlementOperation so inner join so.mechanismOperation mo ");
			stringBuilder.append("	inner join mo.mechanisnModality mm	");
			stringBuilder.append("	inner join mm.negotiationModality nmo	");	
			stringBuilder.append("	inner join mm.negotiationMechanism nme	");			
			stringBuilder.append("	inner join mo.sellerParticipant sp	");			
			stringBuilder.append("	inner join mo.buyerParticipant bp	");	
			stringBuilder.append("	inner join mo.securities se	");	
			stringBuilder.append("	Where ((so.operationPart = :cashPart AND so.operationState in (:cashStates) AND mm.indCashCurrencyExchange = :indicatorOne) or ");
			stringBuilder.append("         (so.operationPart = :termPart AND so.operationState in (:termStates) AND mm.indTermCurrencyExchange = :indicatorOne) )  ");
			stringBuilder.append("  and so.indSettlementExchange != :indicatorOne  ");
			stringBuilder.append("  and so.indExtended != :indicatorOne  ");
			stringBuilder.append("  and so.indPartial != :indicatorOne  ");
			
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestTO.getSettlementDate())){
				stringBuilder.append(" and SO.settlementDate = :settlementDate ");
				parameters.put("settlementDate", searchCurrencySettlementRequestTO.getSettlementDate());
			}
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getIdMechanismNegotiation())){
				stringBuilder.append("	AND	nme.idNegotiationMechanismPk = :idNegotiationMechanismPk	");
				parameters.put("idNegotiationMechanismPk", searchCurrencySettlementRequestTO.getIdMechanismNegotiation());							
			}	
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getIdModalityNegotiation())){
				stringBuilder.append("	AND	nmo.idNegotiationModalityPk = :idNegotiationModalityPk	");
				parameters.put("idNegotiationModalityPk", searchCurrencySettlementRequestTO.getIdModalityNegotiation());							
			}	
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getBallotNumber())){
				stringBuilder.append("	AND	mo.ballotNumber = :ballotNumber	");
				parameters.put("ballotNumber", searchCurrencySettlementRequestTO.getBallotNumber());							
			}	
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getSequentialNumber())){
				stringBuilder.append("	AND	mo.sequential = :sequential	");
				parameters.put("sequential",searchCurrencySettlementRequestTO.getSequentialNumber());							
			}	
							
			stringBuilder.append("	and ( (so.operationPart = :cashPart AND bp.idParticipantPk = :buyerParticipant and sp.idParticipantPk = :sellerParticipant ) or");
			stringBuilder.append("	      (so.operationPart = :termPart AND bp.idParticipantPk = :sellerParticipant and sp.idParticipantPk = :buyerParticipant ) )");

			if(searchCurrencySettlementRequestTO.getParticipantRole().equals(ComponentConstant.PURCHARSE_ROLE)){
				parameters.put("buyerParticipant", searchCurrencySettlementRequestTO.getIdParticipant());
				parameters.put("sellerParticipant", searchCurrencySettlementRequestTO.getIdParticipantCounterpart());							
			}else if(searchCurrencySettlementRequestTO.getParticipantRole().equals(ComponentConstant.SALE_ROLE)){		
				parameters.put("sellerParticipant", searchCurrencySettlementRequestTO.getIdParticipant());
				parameters.put("buyerParticipant", searchCurrencySettlementRequestTO.getIdParticipantCounterpart());		
			}
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getSecurityClass())){
				stringBuilder.append("	AND	se.securityClass = :securityClass	");
				parameters.put("securityClass", searchCurrencySettlementRequestTO.getSecurityClass());							
			}	
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk())){
				stringBuilder.append("	AND	se.idSecurityCodePk = :idSecurityCodePk	");
				parameters.put("idSecurityCodePk", searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk());		
			}
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getCurrency())){
				stringBuilder.append("	AND	SO.settlementCurrency = :settlementCurrency  ");
				parameters.put("settlementCurrency", searchCurrencySettlementRequestTO.getCurrency());		
			}	
			
			List<Integer> cashStates = new ArrayList<Integer>();
			cashStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
			cashStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());

			List<Integer> termStates = new ArrayList<Integer>();
			termStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
			
			parameters.put("cashStates", cashStates);
			parameters.put("termStates", termStates);
			parameters.put("cashPart", ComponentConstant.CASH_PART);
			parameters.put("termPart", ComponentConstant.TERM_PART);
			parameters.put("idCurrencyDOL", CurrencyType.USD.getCode());
			parameters.put("indicatorOne",ComponentConstant.ONE);	
			
			List<Object[]> lstMechanismOperation =  findListByQueryString(stringBuilder.toString(), parameters);
			List<SearchSettlementOperationTO> settlementOperations = new ArrayList<SearchSettlementOperationTO>();
		    for(Object[] object: lstMechanismOperation)
		    {
				 SearchSettlementOperationTO searchMechanismOperationTO = new SearchSettlementOperationTO();
				 searchMechanismOperationTO.setIdMechanismOperation((Long)object[0]);
				 searchMechanismOperationTO.setBallotNumber((Long)object[1]);
				 searchMechanismOperationTO.setSequential((Long)object[2]);
				 
				 searchMechanismOperationTO.setIdParticipantCounterpart((Long)object[3]);
				 searchMechanismOperationTO.setDescriptionParticipantCounterpart((String)object[4]);
				 
				 searchMechanismOperationTO.setSecurityClass((Integer)object[5]);
				 searchMechanismOperationTO.setDescriptionSecurityClass((String)object[6]);
				 searchMechanismOperationTO.setIdSecurityCode((String)object[7]);
				 
				 searchMechanismOperationTO.setCurrency((Integer)object[8]);
				 searchMechanismOperationTO.setDescriptionCurrency((String)object[9]);
				 
				 searchMechanismOperationTO.setStockQuantity((BigDecimal)object[10]);
				 searchMechanismOperationTO.setInitialSettlementCurrency((Integer)object[11]);
				 searchMechanismOperationTO.setSettlementCurrency((Integer)object[11]);
				 searchMechanismOperationTO.setSettlementPrice((BigDecimal)object[12]);
				 searchMechanismOperationTO.setSettlementAmount((BigDecimal)object[13]);
				 
				 searchMechanismOperationTO.setExchangeRate((BigDecimal)object[14]);
				 searchMechanismOperationTO.setInitialExchangeRate(searchMechanismOperationTO.getExchangeRate());
				 searchMechanismOperationTO.setIndRemoveSettlement((Integer)object[16]);
				 searchMechanismOperationTO.setIndReenterSettlement((Integer)object[17]);
				 
				 searchMechanismOperationTO.setIndCurrencyExchange(((Integer)object[18]));
				 searchMechanismOperationTO.setIndPrepaid((Integer)object[19]);
				 searchMechanismOperationTO.setIndExtended((Integer)object[20]);
				 
				 searchMechanismOperationTO.setIdModality((Long)object[21]);
				 searchMechanismOperationTO.setModalityName((String)object[22]);	
				 
				 searchMechanismOperationTO.setIdMechanism((Long)object[23]);
				 searchMechanismOperationTO.setMechanismName((String)object[24]);
				 
				 searchMechanismOperationTO.setOperationNumber((Long)object[25]);
				 searchMechanismOperationTO.setOperationDate((Date)object[26]);
				 searchMechanismOperationTO.setIdSettlementOperation((Long)object[27]);
				 searchMechanismOperationTO.setOperationPart((Integer)object[28]);

				 settlementOperations.add(searchMechanismOperationTO);
		    }
		    
		    return settlementOperations;
		    
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	

	/**
	 * Get Mechanism Operation By Filter Service Bean.
	 *
	 * @param searchCurrencySettlementRequestTO filter Currency Settlement
	 * @return List Mechanism Operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SearchSettlementOperationTO> getCurrencyExchangedSettlementOperation(SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();			
			stringBuilder.append("	select mo.idMechanismOperationPk,	");//0
			stringBuilder.append("	mo.ballotNumber, mo.sequential,	"); //1,2
			stringBuilder.append("  mo.operationNumber, mo.operationDate, ");//3 4
			stringBuilder.append("	se.securityClass,(Select p1.parameterName From ParameterTable p1 Where p1.parameterTablePk = se.securityClass), se.idSecurityCodePk,	");	//5 6 7
			stringBuilder.append("	se.currency,(Select p2.parameterName From ParameterTable p2 Where p2.parameterTablePk = se.currency),	");	//8 9
			stringBuilder.append("	so.stockQuantity, so.settlementCurrency, so.settlementPrice, so.settlementAmount, "); // 10 11 12 13
			stringBuilder.append("  so.operationPart, ");//14
			stringBuilder.append("	so.operationState, so.indRemoveSettlement , so.indReenterSettlement, ");	//15 16 17
			stringBuilder.append("  so.indCurrencyExchange, so.indPrepaid , so.indExtended, ");//18 19 20
			stringBuilder.append("  nmo.idNegotiationModalityPk, nmo.modalityName, "); // 21 22
			stringBuilder.append("  nme.idNegotiationMechanismPk,nme.mechanismName,	");//23 24
			stringBuilder.append("  so.idSettlementOperationPk, ");//25
			stringBuilder.append("  (case when bp.idParticipantPk = :idParticipant then sp ");
			stringBuilder.append("  	  when sp.idParticipantPk = :idParticipant then bp end) ,");//26
			stringBuilder.append("  so.initialSettlementCurrency, ");//27
			stringBuilder.append("  so.initialSettlementPrice ");//28
			stringBuilder.append("	from SettlementOperation so inner join so.mechanismOperation mo ");
			stringBuilder.append("	inner join mo.mechanisnModality mm	");
			stringBuilder.append("	inner join mm.negotiationModality nmo	");	
			stringBuilder.append("	inner join mm.negotiationMechanism nme	");			
			stringBuilder.append("	inner join mo.sellerParticipant sp	");			
			stringBuilder.append("	inner join mo.buyerParticipant bp	");	
			stringBuilder.append("	inner join mo.securities se	");	
			stringBuilder.append("	Where ((so.operationPart = :cashPart AND so.operationState in (:cashStates)) or ");
			stringBuilder.append("         (so.operationPart = :termPart AND so.operationState in (:termStates)) )  ");
			stringBuilder.append("  and so.indCurrencyExchange = :indicatorOne  ");
			stringBuilder.append("	and ( bp.idParticipantPk = :idParticipant or sp.idParticipantPk = :idParticipant ) ");
			
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestTO.getSettlementDate())){
				stringBuilder.append(" and SO.settlementDate = :settlementDate ");
				parameters.put("settlementDate", searchCurrencySettlementRequestTO.getSettlementDate());
			}
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getIdMechanismNegotiation())){
				stringBuilder.append("	AND	nme.idNegotiationMechanismPk = :idNegotiationMechanismPk	");
				parameters.put("idNegotiationMechanismPk", searchCurrencySettlementRequestTO.getIdMechanismNegotiation());							
			}	
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getIdModalityNegotiation())){
				stringBuilder.append("	AND	nmo.idNegotiationModalityPk = :idNegotiationModalityPk	");
				parameters.put("idNegotiationModalityPk", searchCurrencySettlementRequestTO.getIdModalityNegotiation());							
			}	
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getBallotNumber())){
				stringBuilder.append("	AND	mo.ballotNumber = :ballotNumber	");
				parameters.put("ballotNumber", searchCurrencySettlementRequestTO.getBallotNumber());							
			}	
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getSequentialNumber())){
				stringBuilder.append("	AND	mo.sequential = :sequential	");
				parameters.put("sequential",searchCurrencySettlementRequestTO.getSequentialNumber());							
			}	
							
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getSecurityClass())){
				stringBuilder.append("	AND	se.securityClass = :securityClass	");
				parameters.put("securityClass", searchCurrencySettlementRequestTO.getSecurityClass());							
			}	
			if(Validations.validateIsNotNullAndNotEmpty(searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk())){
				stringBuilder.append("	AND	se.idSecurityCodePk = :idSecurityCodePk	");
				parameters.put("idSecurityCodePk", searchCurrencySettlementRequestTO.getSecurity().getIdSecurityCodePk());		
			}
			if(Validations.validateIsNotNullAndPositive(searchCurrencySettlementRequestTO.getCurrency())){
				stringBuilder.append("	AND	SO.settlementCurrency = :settlementCurrency  ");
				parameters.put("settlementCurrency", searchCurrencySettlementRequestTO.getCurrency());		
			}	
			
			List<Integer> cashStates = new ArrayList<Integer>();
			cashStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
			cashStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());

			List<Integer> termStates = new ArrayList<Integer>();
			termStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
			
			parameters.put("cashStates", cashStates);
			parameters.put("termStates", termStates);
			parameters.put("cashPart", ComponentConstant.CASH_PART);
			parameters.put("termPart", ComponentConstant.TERM_PART);
			parameters.put("indicatorOne",ComponentConstant.ONE);
			parameters.put("idParticipant",searchCurrencySettlementRequestTO.getIdParticipant());
			
			List<Object[]> lstMechanismOperation =  findListByQueryString(stringBuilder.toString(), parameters);
			List<SearchSettlementOperationTO> settlementOperations = new ArrayList<SearchSettlementOperationTO>();
		    for(Object[] object: lstMechanismOperation)
		    {
				 SearchSettlementOperationTO searchMechanismOperationTO = new SearchSettlementOperationTO();
				 searchMechanismOperationTO.setIdMechanismOperation((Long)object[0]);
				 searchMechanismOperationTO.setBallotNumber((Long)object[1]);
				 searchMechanismOperationTO.setSequential((Long)object[2]);
				 searchMechanismOperationTO.setOperationNumber((Long)object[3]);
				 searchMechanismOperationTO.setOperationDate((Date)object[4]);
				 
				 searchMechanismOperationTO.setSecurityClass((Integer)object[5]);
				 searchMechanismOperationTO.setDescriptionSecurityClass((String)object[6]);
				 searchMechanismOperationTO.setIdSecurityCode((String)object[7]);
				 
				 searchMechanismOperationTO.setCurrency((Integer)object[8]);
				 searchMechanismOperationTO.setDescriptionCurrency((String)object[9]);
				 
				 searchMechanismOperationTO.setStockQuantity((BigDecimal)object[10]);
				 searchMechanismOperationTO.setInitialSettlementCurrency((Integer)object[11]);
				 searchMechanismOperationTO.setSettlementPrice((BigDecimal)object[12]);
				 searchMechanismOperationTO.setSettlementAmount((BigDecimal)object[13]);
				 
				 searchMechanismOperationTO.setOperationPart((Integer)object[14]);
				 searchMechanismOperationTO.setIndRemoveSettlement((Integer)object[16]);
				 searchMechanismOperationTO.setIndReenterSettlement((Integer)object[17]);
				 
				 searchMechanismOperationTO.setIndCurrencyExchange(((Integer)object[18]));
				 searchMechanismOperationTO.setIndPrepaid((Integer)object[19]);
				 searchMechanismOperationTO.setIndExtended((Integer)object[20]);
				 
				 searchMechanismOperationTO.setIdModality((Long)object[21]);
				 searchMechanismOperationTO.setModalityName((String)object[22]);	
				 
				 searchMechanismOperationTO.setIdMechanism((Long)object[23]);
				 searchMechanismOperationTO.setMechanismName((String)object[24]);
				 
				 searchMechanismOperationTO.setIdSettlementOperation((Long)object[25]);
				 Participant counterPart = (Participant)object[26];
				 searchMechanismOperationTO.setIdParticipantCounterpart(counterPart.getIdParticipantPk());
				 searchMechanismOperationTO.setDescriptionParticipantCounterpart(counterPart.getDisplayDescriptionCode());
				 
				 searchMechanismOperationTO.setSettlementCurrency((Integer)object[27]);
				 searchMechanismOperationTO.setNewSettlementPrice((BigDecimal)object[28]);
				 searchMechanismOperationTO.setNewSettlementAmount(searchMechanismOperationTO.getNewSettlementPrice().multiply(searchMechanismOperationTO.getStockQuantity()));

				 settlementOperations.add(searchMechanismOperationTO);
		    }
		    
		    return settlementOperations;
		    
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Request Exists Currency Settlement.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param operationPart operation Part
	 * @param requestType the request type
	 * @return object Currency Settlement Detail
	 * @throws ServiceException the service exception
	 */	
	public CurrencySettlementOperation requestExistsCurrencySettlement(Long idSettlementOperation,Integer operationPart, Integer requestType) throws ServiceException{
		try{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT cs FROM CurrencySettlementOperation cs WHERE cs.settlementOperation.idSettlementOperationPk = :idSettlementOperation ");
		sbQuery.append(" AND cs.operationPart = :operationPart");
		sbQuery.append(" AND trunc(cs.settlementRequest.registerDate) = :currentDate ");
		sbQuery.append(" AND cs.settlementRequest.requestType = :requestType ");
		if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode())){
			sbQuery.append(" AND ( (cs.settlementOperation.indCurrencyExchange = 0 and cs.settlementRequest.requestState not in (:states)) or ");
			sbQuery.append("        cs.settlementOperation.indCurrencyExchange = 1 ) ");
		}else if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode())){
			sbQuery.append(" AND ( cs.settlementOperation.indCurrencyExchange = 1 and cs.settlementRequest.requestState not in (:states) ) ");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		List<Integer> states = new ArrayList<Integer>();
		states.add(SettlementCurrencyRequestStateType.CANCELLED.getCode());
		states.add(SettlementCurrencyRequestStateType.REJECTED.getCode());
		states.add(SettlementCurrencyRequestStateType.AUTHORIZED.getCode());
		query.setParameter("states",  states);
		query.setParameter("idSettlementOperation",  idSettlementOperation);
		query.setParameter("operationPart", operationPart);
		query.setParameter("currentDate", CommonsUtilities.currentDate());
		query.setParameter("requestType", requestType);
		CurrencySettlementOperation currencySettlementOperation = (CurrencySettlementOperation)query.getSingleResult();
		
		return currencySettlementOperation;
		
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Mechanism Operation MarketFact.
	 *
	 * @param idMechanismOperation id Mechanism Operation
	 * @return object mechanism Operation MarketFact
	 * @throws ServiceException the service exception
	 */
	public MechanismOperationMarketFact getMechanismOperationMarketFact(Long idMechanismOperation)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM MechanismOperationMarketFact cs	WHERE cs.mechanismOperation.tradeOperation.idTradeOperationPk = :idTradeOperationPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("idTradeOperationPk",  idMechanismOperation);
			MechanismOperationMarketFact mechanismOperationMarketFact = (MechanismOperationMarketFact)query.getSingleResult();
			
			return mechanismOperationMarketFact;
			
			}catch(NoResultException ex){
				   return null;
			} catch(NonUniqueResultException ex){
				   return null;
			}	
	}
	 
	/**
	 * Get List Currency Settlement Request Filter.
	 *
	 * @param currencySettlementRequestTO object search Currency Settlement Request
	 * @return object Currency Settlement Request
	 * @throws ServiceException the service exception
	 */
	public List<SettlementRequestTO> getListCurrencySettlementRequestFilter(SearchCurrencySettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();	
		stringBuilder.append("	Select sr.idSettlementRequestPk,	");//0
		stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = sr.requestState),sr.requestState,	"); //1 2
		stringBuilder.append("	sr.participantRole,	"); //3 
		stringBuilder.append("	ps.idParticipantPk,ps.mnemonic, pt.idParticipantPk,pt.mnemonic,	"); //4 5 6 7
		stringBuilder.append("	cso.idCurrencySettlementOperPk,"); //8
		stringBuilder.append("	mo.ballotNumber, mo.sequential, mo.operationState,	"); // 9 10 11
		stringBuilder.append("	mo.cashSettlementDate, mo.termSettlementDate,"); // 12 13
		stringBuilder.append("  mo.securities.securityClass,"); // 14
		stringBuilder.append("  (Select p.text1 From ParameterTable p Where p.parameterTablePk = mo.securities.securityClass),"); // 15
		stringBuilder.append("  cso.settlementCurrency, "); //16
		stringBuilder.append("  (Select p.parameterName From ParameterTable p Where p.parameterTablePk = cso.settlementCurrency),");// 17
		stringBuilder.append("  cso.exchangeRate, cso.settlementAmount, "); // 18 19
		stringBuilder.append("  mo.operationNumber, "); // 20
		stringBuilder.append("  mo.currency, "); // 21
		stringBuilder.append("  (Select p.parameterName From ParameterTable p Where p.parameterTablePk = mo.currency),"); // 22
		stringBuilder.append("  mo.securities.idSecurityCodePk, "); // 23
		stringBuilder.append("  nme.idNegotiationMechanismPk , nme.mechanismName, "); // 24 25
		stringBuilder.append("  nmo.idNegotiationModalityPk , nmo.modalityName, "); // 26 27
		stringBuilder.append("  ps.description, pt.description, "); // 28 29
		stringBuilder.append("  sr.registerDate , "); // 30
		stringBuilder.append("  mo.stockQuantity, "); // 31 
		stringBuilder.append("  cso.settlementPrice,mo.idMechanismOperationPk, "); //32 33 
		stringBuilder.append("  cso.operationPart, "); // 34
		stringBuilder.append("  mo.operationDate,so.idSettlementOperationPk ,"); // 35 36
		stringBuilder.append("  cso.initialSettlementPrice, cso.initialSettlementAmount, cso.initialSettlementCurrency "); // 37 38 39 
		stringBuilder.append("	from CurrencySettlementOperation cso ");
		stringBuilder.append("	inner join cso.settlementRequest sr ");
		stringBuilder.append("	inner join cso.settlementOperation so ");
		stringBuilder.append("	inner join so.mechanismOperation mo ");
		stringBuilder.append("	inner join mo.mechanisnModality mm ");
		stringBuilder.append("	inner join mm.negotiationMechanism nme ");
		stringBuilder.append("	inner join mm.negotiationModality nmo ");
		stringBuilder.append("	inner join sr.sourceParticipant ps	");		
		stringBuilder.append("	inner join sr.targetParticipant pt	");
		stringBuilder.append("	Where sr.requestType = :currencyExchangeReq	");	
		
		parameters.put("currencyExchangeReq", currencySettlementRequestTO.getRequestType());		
		
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getIdMechanismNegotiation()) ){
			stringBuilder.append("	AND	mo.mechanisnModality.id.idNegotiationMechanismPk = :idMechanism	");
			parameters.put("idMechanism", currencySettlementRequestTO.getIdMechanismNegotiation());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getIdModalityNegotiation()) ){
			stringBuilder.append("	AND	mo.mechanisnModality.id.idNegotiationModalityPk = :idModality	");
			parameters.put("idModality", currencySettlementRequestTO.getIdModalityNegotiation());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getBallotNumber()) ){
			stringBuilder.append("	AND	mo.ballotNumber = :idBallotNumber	");
			parameters.put("idBallotNumber", currencySettlementRequestTO.getBallotNumber());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getSequentialNumber()) ){
			stringBuilder.append("	AND	mo.sequential  = :sequential	");
			parameters.put("sequential", currencySettlementRequestTO.getSequentialNumber());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getSecurityClass()) ){
			stringBuilder.append("	AND	mo.securities.securityClass  = :securityClass ");
			parameters.put("securityClass", currencySettlementRequestTO.getSecurityClass());							
		}
		if(Validations.validateIsNotNull(currencySettlementRequestTO.getSecurity().getIdSecurityCodePk())){
			stringBuilder.append("	AND	mo.securities.idSecurityCodePk  = :securityCode ");
			parameters.put("securityCode", currencySettlementRequestTO.getSecurity().getIdSecurityCodePk());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getIdParticipant()) ){
			stringBuilder.append("	AND	( ps.idParticipantPk = :idParticipant or pt.idParticipantPk = :idParticipant ) ");
			parameters.put("idParticipant", currencySettlementRequestTO.getIdParticipant());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getCurrency())){
			stringBuilder.append("	AND	cso.settlementCurrency = :currency	");
			parameters.put("currency", currencySettlementRequestTO.getCurrency());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getStateRequest())){
			stringBuilder.append("	AND	sr.requestState = :requestState	");
			parameters.put("requestState", currencySettlementRequestTO.getStateRequest());							
		}
		if(Validations.validateIsNotNullAndPositive(currencySettlementRequestTO.getNumberRequest()) ){
			stringBuilder.append("	AND	sr.idSettlementRequestPk = :idSettlementRequestPk	");
			parameters.put("idSettlementRequestPk", currencySettlementRequestTO.getNumberRequest());							
		}
		if(Validations.validateIsNotNull(currencySettlementRequestTO.getInitialDate()) && Validations.validateIsNotNull(currencySettlementRequestTO.getFinalDate())){
			stringBuilder.append(" and  TRUNC(sr.registerDate) between :dateIni and :dateEnd ");
			parameters.put("dateIni", currencySettlementRequestTO.getInitialDate());
			parameters.put("dateEnd", currencySettlementRequestTO.getFinalDate());			
		}
		@SuppressWarnings("unchecked")
		List<Object[]> lstCurrencySettlementOperations =  findListByQueryString(stringBuilder.toString(), parameters);
		Map<Long,SettlementRequestTO> mapCurrencySettlementRequest = new LinkedHashMap<Long, SettlementRequestTO>();
		for(Object[] object: lstCurrencySettlementOperations){
			Long idSettlementRequest = (Long)object[0];
			SettlementRequestTO settlementRequest = mapCurrencySettlementRequest.get(idSettlementRequest);
			if(settlementRequest == null){
				settlementRequest = new SettlementRequestTO();
				settlementRequest.setSettlementRequestDetail(new ArrayList<SettlementRequestDetailTO>());
				settlementRequest.setIdSettlementRequest(idSettlementRequest);
				settlementRequest.setStateDescription((String)object[1]);
				settlementRequest.setRequestState((Integer)object[2]);
				settlementRequest.setParticipantRole((Integer)object[3]);
				settlementRequest.setSourceParticipant(new Participant((Long)object[4],(String)object[5]));
				settlementRequest.getSourceParticipant().setDescription((String)object[28]);
				settlementRequest.setTargetParticipant(new Participant((Long)object[6],(String)object[7]));
				settlementRequest.getTargetParticipant().setDescription((String)object[29]);
				settlementRequest.setRegisterDate((Date)object[30]);
				mapCurrencySettlementRequest.put(idSettlementRequest, settlementRequest);
			}
			SettlementRequestDetailTO currencySettlementDetail = new SettlementRequestDetailTO();
			currencySettlementDetail.setIdCurrencySettlement((Long)object[8]);
			currencySettlementDetail.setIdMechanismOperation((Long)object[33]);
			currencySettlementDetail.setBallotNumber((Long)object[9]);
			currencySettlementDetail.setSequential((Long)object[10]);
			currencySettlementDetail.setOperationState((Integer)object[11]);
			currencySettlementDetail.setCashSettlementDate((Date)object[12]);
			currencySettlementDetail.setTermSettlementDate((Date)object[13]);
			currencySettlementDetail.setCurrency((Integer)object[21]);
			currencySettlementDetail.setCurrencyDescription((String)object[22]);
			currencySettlementDetail.setIdMechanism((Long)object[24]);
			currencySettlementDetail.setMechanismName((String)object[25]);
			currencySettlementDetail.setIdModality((Long)object[26]);
			currencySettlementDetail.setModalityName((String)object[27]);
			currencySettlementDetail.setIdSecurityCode((String)object[23]);
			currencySettlementDetail.setSecurityClass((Integer)object[14]);
			currencySettlementDetail.setSecurityClassDescription((String)object[15]);
			currencySettlementDetail.setOperationNumber((Long)object[20]);
			currencySettlementDetail.setStockQuantity((BigDecimal)object[31]);
			currencySettlementDetail.setSettlementCurrency((Integer)object[16]);
			currencySettlementDetail.setSettlementCurrencyDescription((String)object[17]);
			currencySettlementDetail.setExchangeRate((BigDecimal)object[18]);
			currencySettlementDetail.setSettlementAmount((BigDecimal)object[19]);
			currencySettlementDetail.setSettlementPrice((BigDecimal) object[32]);
			currencySettlementDetail.setOperationPart((Integer)object[34]);
			currencySettlementDetail.setOperationDate((Date)object[35]);
			currencySettlementDetail.setIdSettlementOperation((Long)object[36]);
			
			currencySettlementDetail.setInitialSettlementPrice((BigDecimal)object[37]);
			currencySettlementDetail.setInitialSettlementAmount((BigDecimal)object[38]);
			currencySettlementDetail.setInitialSettlementCurrency((Integer)object[39]);
			
			if(ComponentConstant.CASH_PART.equals(currencySettlementDetail.getOperationPart())){
				currencySettlementDetail.setCashPart(true);
			}else if(ComponentConstant.TERM_PART.equals(currencySettlementDetail.getOperationPart())){
				currencySettlementDetail.setTermPart(true);
			}
			
			settlementRequest.getSettlementRequestDetail().add(currencySettlementDetail);
		}
		return new ArrayList<SettlementRequestTO>(mapCurrencySettlementRequest.values());
	}
		
	/**
	 * Registry Currency Settlement Request Facade.
	 *
	 * @param currencySettlementRequest object currency Settlement Request
	 * @param logger the logger
	 * @param indUserParticipant the ind user participant
	 * @return object currency Settlement Request
	 * @throws ServiceException the service exception
	 */
	public SettlementRequest registryCurrencySettlementRequest(SettlementRequest currencySettlementRequest, LoggerUser logger, Integer indUserParticipant)throws ServiceException{
		try{
			
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("idParticipantPkParam", currencySettlementRequest.getSourceParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE,params);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}	
			currencySettlementRequest.setSourceParticipant(participant);			
			params.put("idParticipantPkParam", currencySettlementRequest.getTargetParticipant().getIdParticipantPk());
			Participant participantAux = findObjectByNamedQuery(Participant.PARTICIPANT_STATE,params);
			if(!participantAux.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}				
			currencySettlementRequest.setTargetParticipant(participantAux);	
			currencySettlementRequest.setRequestState(SettlementCurrencyRequestStateType.REGISTERED.getCode());				
			currencySettlementRequest.setRegisterUser(logger.getUserName());
			currencySettlementRequest.setRegisterDate(CommonsUtilities.currentDateTime());
			
			if(ComponentConstant.ONE.equals(indUserParticipant)){
				if(currencySettlementRequest.getSourceParticipant().getIdParticipantPk().equals(currencySettlementRequest.getTargetParticipant().getIdParticipantPk()))	{
					currencySettlementRequest.setRequestState(SettlementCurrencyRequestStateType.REVISED.getCode());		
					currencySettlementRequest.setReviewUser(logger.getUserName());
					currencySettlementRequest.setReviewDate(CommonsUtilities.currentDateTime());		
					currencySettlementRequest.setApprovalUser(logger.getUserName());
					currencySettlementRequest.setApprovalDate(CommonsUtilities.currentDateTime());					
				}
			}		
			
			create(currencySettlementRequest);
			
			for(CurrencySettlementOperation currencySettlementOperation : currencySettlementRequest.getLstCurrencySettlementDetail()){

				currencySettlementOperation.setSettlementRequest(currencySettlementRequest);
				
				settlementProcessService.checkExistChainedAccount(
						currencySettlementOperation.getSettlementOperation().getIdSettlementOperationPk());
				
				if(Validations.validateIsNotNullAndNotEmpty(requestExistsCurrencySettlement(
						currencySettlementOperation.getSettlementOperation().getIdSettlementOperationPk(), 
						currencySettlementOperation.getOperationPart(),currencySettlementRequest.getRequestType()))){
					if(currencySettlementRequest.getRequestType().equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode())){
						throw new ServiceException(ErrorServiceType.SETTLEMENT_REQUEST_CURRENCY_EXCHANGE_DUPLICATE);
					}
					if(currencySettlementRequest.getRequestType().equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode())){
						throw new ServiceException(ErrorServiceType.SETTLEMENT_REQUEST_CURRENCY_EXCHANGE_REVERSION_DUPLICATE);
					}
				}
				
				create(currencySettlementOperation);
			}
			return currencySettlementRequest;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Validate request.
	 *
	 * @param settlementRequest the settlement request
	 * @param newState the new state
	 * @throws ServiceException the service exception
	 */
	private void validateRequest(SettlementRequest settlementRequest, Integer newState) throws ServiceException {
		
		Map<String,Object> parama = new HashMap<String,Object>();		
		parama.put("idParticipantPkParam", settlementRequest.getSourceParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
		if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}	
		Map<String,Object> paramaAux = new HashMap<String,Object>();		
		paramaAux.put("idParticipantPkParam", settlementRequest.getTargetParticipant().getIdParticipantPk());
		Participant participantAux = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramaAux);
		if(!participantAux.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}			

		if(SettlementCurrencyRequestStateType.APPROVED.getCode().equals(newState)){
			if(!SettlementCurrencyRequestStateType.REGISTERED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}else if(SettlementCurrencyRequestStateType.CANCELLED.getCode().equals(newState)){
			if(!SettlementCurrencyRequestStateType.REGISTERED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}else if(SettlementCurrencyRequestStateType.REVISED.getCode().equals(newState)){
			if(!SettlementCurrencyRequestStateType.APPROVED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}else if(SettlementCurrencyRequestStateType.REJECTED.getCode().equals(newState)){
			if(!SettlementCurrencyRequestStateType.APPROVED.getCode().equals(settlementRequest.getRequestState()) &&
					!SettlementCurrencyRequestStateType.REVISED.getCode().equals(settlementRequest.getRequestState()) &&
						!SettlementCurrencyRequestStateType.CONFIRMED.getCode().equals(settlementRequest.getRequestState()) ){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}else if(SettlementCurrencyRequestStateType.CONFIRMED.getCode().equals(newState)){
			if(!SettlementCurrencyRequestStateType.REVISED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}else if(SettlementCurrencyRequestStateType.AUTHORIZED.getCode().equals(newState)){
			if(!SettlementCurrencyRequestStateType.CONFIRMED.getCode().equals(settlementRequest.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
		}
		
	}
	
	/**
	 * Change operation state.
	 *
	 * @param settlementRequest the settlement request
	 * @param newState the new state
	 * @param loggerUser the logger user
	 * @return the settlement request
	 * @throws ServiceException the service exception
	 */
	public SettlementRequest changeOperationState(SettlementRequestTO settlementRequest, Integer newState, LoggerUser loggerUser) throws ServiceException{
		
		SettlementRequest currencySettlementRequest = getCurrencySettlementRequest(settlementRequest.getIdSettlementRequest());
		
		validateRequest(currencySettlementRequest,newState);
		
		if(SettlementCurrencyRequestStateType.APPROVED.getCode().equals(newState)){
			currencySettlementRequest.setApprovalUser(loggerUser.getUserName());
			currencySettlementRequest.setApprovalDate(CommonsUtilities.currentDateTime());
		}else if(SettlementCurrencyRequestStateType.CANCELLED.getCode().equals(newState)){
			currencySettlementRequest.setAnnulMotive(settlementRequest.getAnnulMotive());
			currencySettlementRequest.setAnnulOtherMotive(settlementRequest.getAnnulOtherMotive());
			currencySettlementRequest.setAnnulUser(loggerUser.getUserName());
			currencySettlementRequest.setAnnulDate(CommonsUtilities.currentDateTime());
		}else if(SettlementCurrencyRequestStateType.REVISED.getCode().equals(newState)){
			currencySettlementRequest.setReviewUser(loggerUser.getUserName());
			currencySettlementRequest.setReviewDate(CommonsUtilities.currentDateTime());
		}else if(SettlementCurrencyRequestStateType.REJECTED.getCode().equals(newState)){
			currencySettlementRequest.setRejectMotive(settlementRequest.getRejectMotive());
			currencySettlementRequest.setRejectOtherMotive(settlementRequest.getRejectOtherMotive());
			currencySettlementRequest.setRejectUser(loggerUser.getUserName());
			currencySettlementRequest.setRejectDate(CommonsUtilities.currentDateTime());
		}else if(SettlementCurrencyRequestStateType.CONFIRMED.getCode().equals(newState)){
			currencySettlementRequest.setConfirmUser(loggerUser.getUserName());
			currencySettlementRequest.setConfirmDate(CommonsUtilities.currentDateTime());
		}else if(SettlementCurrencyRequestStateType.AUTHORIZED.getCode().equals(newState)){
			currencySettlementRequest.setAuthorizeUser(loggerUser.getUserName());
			currencySettlementRequest.setAuthorizeDate(CommonsUtilities.currentDateTime());
		}
		
		currencySettlementRequest.setRequestState(newState);
		update(currencySettlementRequest);
		
		if(SettlementCurrencyRequestStateType.AUTHORIZED.getCode().equals(currencySettlementRequest.getRequestState())){
			for(SettlementRequestDetailTO currencySettlementDetail : settlementRequest.getSettlementRequestDetail() ){
					
				updateSettlementOperationData(currencySettlementDetail,currencySettlementRequest.getRequestType(), loggerUser);
			}
		}
		
		return currencySettlementRequest;
	}
	
	/**
	 * Update settlement operation data.
	 *
	 * @param currencySettlementDetail the currency settlement detail
	 * @param requestType the request type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	private void updateSettlementOperationData(SettlementRequestDetailTO currencySettlementDetail, Integer requestType, LoggerUser loggerUser) throws ServiceException {
		SettlementOperation settlementOperation = find(SettlementOperation.class, currencySettlementDetail.getIdSettlementOperation());
		if(settlementOperation.getOperationState().equals(MechanismOperationStateType.CANCELED_STATE.getCode())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS); 
		}
		
		if(settlementOperation.getIndPartial().equals(ComponentConstant.ZERO) && settlementOperation.getOperationPart().equals(ComponentConstant.CASH_PART)){
			updateMechanismOperationSettlementData(currencySettlementDetail.getIdMechanismOperation(),currencySettlementDetail.getOperationPart(),
					currencySettlementDetail.getSettlementCurrency(),currencySettlementDetail.getSettlementPrice(),
					currencySettlementDetail.getSettlementAmount(),currencySettlementDetail.getExchangeRate(), loggerUser);
		}
		
		if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode())){
			settlementOperation.setIndCurrencyExchange(ComponentConstant.ONE);
		}else if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode())){
			settlementOperation.setIndCurrencyExchange(ComponentConstant.ZERO);
		}
		
		if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode())){
			settlementOperation.setInitialSettlementPrice(settlementOperation.getSettlementPrice());
			settlementOperation.setInitialSettlementCurrency(settlementOperation.getSettlementCurrency());
		}else if(requestType.equals(SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode())){
			settlementOperation.setInitialSettlementPrice(BigDecimal.ZERO);
		}
		
		settlementOperation.setSettlementCurrency(currencySettlementDetail.getSettlementCurrency());
		settlementOperation.setExchangeRate(currencySettlementDetail.getExchangeRate());
		settlementOperation.setSettlementPrice(currencySettlementDetail.getSettlementPrice());
		settlementOperation.setSettlementAmount(currencySettlementDetail.getSettlementAmount());	
		
		update(settlementOperation);	
		
		Long idSettlementOperation = settlementOperation.getIdSettlementOperationPk();
		List<Integer> states = new ArrayList<Integer>();
		states.add(HolderAccountOperationStateType.REGISTERED.getCode());
		states.add(HolderAccountOperationStateType.CONFIRMED.getCode());
		states.add(HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode());
		List<SettlementAccountOperation> settlementAccountOperations = settlementProcessService.getSettlementAccountOperations(idSettlementOperation,states);
		
		if(Validations.validateListIsNotNullAndNotEmpty(settlementAccountOperations)){
			for (SettlementAccountOperation settlementAccountOperation : settlementAccountOperations) {
				settlementAccountOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), settlementAccountOperation.getStockQuantity()));
				update(settlementAccountOperation);
				
				HolderAccountOperation holderAccountOperation = settlementAccountOperation.getHolderAccountOperation();
				holderAccountOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), holderAccountOperation.getStockQuantity()));
				updateAccountOperationSettlementData(settlementAccountOperation.getHolderAccountOperation().getIdHolderAccountOperationPk(),
						holderAccountOperation.getSettlementAmount(),loggerUser);
			}
		}
		
		List<ParticipantSettlement> participantSettlements = (List<ParticipantSettlement>) settlementProcessService.getParticipantSettlements(idSettlementOperation,null,null);
		
		if(Validations.validateListIsNotNullAndNotEmpty(participantSettlements)){
			for (ParticipantSettlement participantSettlement : participantSettlements) {
				participantSettlement.setSettlementCurrency(settlementOperation.getSettlementCurrency());
				participantSettlement.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), participantSettlement.getStockQuantity()));
				update(participantSettlement);
			}
		}
		
	}
	
	/**
	 * Update mechanism operation settlement data.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationPart the operation part
	 * @param currency the currency
	 * @param price the price
	 * @param amount the amount
	 * @param exchangeRate the exchange rate
	 * @param loggerUser the logger user
	 * @return the int
	 */
	private int updateMechanismOperationSettlementData(Long idMechanismOperation, Integer operationPart, 
			Integer currency, BigDecimal price, BigDecimal amount, BigDecimal exchangeRate,LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE MechanismOperation set ");
		if(ComponentConstant.CASH_PART.equals(operationPart)){
			stringBuffer.append(" indCashSettlementExchange = :indicatorOne ");
			stringBuffer.append(" , cashSettlementCurrency = :settlementCurrency ");
			stringBuffer.append(" , cashSettlementAmount = :settlementAmount ");
			stringBuffer.append(" , cashSettlementPrice = :settlementPrice ");
			stringBuffer.append(" , termSettlementExChangeRate = :settlementExChangeRate ");
		}else if (ComponentConstant.TERM_PART.equals(operationPart)){
			stringBuffer.append(" indTermSettlementExchange = :indicatorOne ");
			stringBuffer.append(" , termSettlementCurrency = :settlementCurrency ");
			stringBuffer.append(" , termSettlementAmount = :settlementAmount ");
			stringBuffer.append(" , termSettlementPrice = :settlementPrice ");
			stringBuffer.append(" , termSettlementExChangeRate = :settlementExChangeRate ");
		}
		
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idMechanismOperationPk = :idMechanismOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("indicatorOne", ComponentConstant.ONE);
		query.setParameter("settlementCurrency", currency);
		query.setParameter("settlementPrice", price);
		query.setParameter("settlementAmount", amount);
		query.setParameter("settlementExChangeRate", exchangeRate);
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Update account operation settlement data.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param amount the amount
	 * @param loggerUser the logger user
	 * @return the int
	 */
	private int updateAccountOperationSettlementData(Long idHolderAccountOperation, 
			BigDecimal amount, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE HolderAccountOperation set ");
		stringBuffer.append("   settlementAmount = :settlementAmount ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idHolderAccountOperationPk = :idHolderAccountOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("settlementAmount", amount);
		query.setParameter("idHolderAccountOperation", idHolderAccountOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Gets the currency sett operations.
	 *
	 * @param idSettlementRequest the id settlement request
	 * @return the currency sett operations
	 */
	@SuppressWarnings("unchecked")
	public List<CurrencySettlementOperation> getCurrencySettOperations(Long idSettlementRequest){

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select csr From CurrencySettlementOperation csr ");
		sbQuery.append(" where csr.settlementRequest.idSettlementRequestPk = " + idSettlementRequest);					
		Query query = em.createQuery(sbQuery.toString());
		return (List<CurrencySettlementOperation>) query.getResultList();
		
	}
	
	/**
	 * Gets the currency settlement request.
	 *
	 * @param idSettlementRequest the id settlement request
	 * @return the currency settlement request
	 */
	@SuppressWarnings("unchecked")
	public SettlementRequest getCurrencySettlementRequest(Long idSettlementRequest){

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select cr From SettlementRequest cr ");
		sbQuery.append(" inner join fetch cr.sourceParticipant ");
		sbQuery.append(" inner join fetch cr.targetParticipant ");
		//sbQuery.append(" inner join fetch cr.lstCurrencySettlementDetail scr ");
		sbQuery.append(" where cr.idSettlementRequestPk = " + idSettlementRequest);					
		Query query = em.createQuery(sbQuery.toString());
		return (SettlementRequest) query.getSingleResult();
		
	}

	
	/**
	 * Gets the holder account operations.
	 *
	 * @param operationId the operation id
	 * @param operationPart the operation part
	 * @param role the role
	 * @return the holder account operations
	 */
	@SuppressWarnings("unchecked")
	private List<HolderAccountOperation> getHolderAccountOperations(Long operationId, Integer operationPart, Integer role) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();		
		sbQuery.append("Select hao From HolderAccountOperation hao ");
		sbQuery.append(" where hao.mechanismOperation.idMechanismOperationPk = :operationId");	
		sbQuery.append(" AND (hao.holderAccountState IN (:accountState) ) ");
		sbQuery.append(" AND hao.operationPart = :operationPart ");
		sbQuery.append(" AND hao.role = :role ");
		sbQuery.append(" ORDER BY  hao.mechanismOperation.idMechanismOperationPk");
		
		parameters.put("operationId", operationId);
		parameters.put("operationPart", operationPart);
		parameters.put("role", role);
		parameters.put("accountState", Arrays.asList(HolderAccountOperationStateType.CONFIRMED.getCode(),
													 HolderAccountOperationStateType.REGISTERED.getCode()));
		
		return (List<HolderAccountOperation>) findListByQueryString(sbQuery.toString(),parameters);
	}

	/**
	 * Reject All Currency Settlement Request.
	 *
	 * @param registerDate register Date for request
	 * @param loggerUser   logger User
	 * @throws ServiceException the service exception
	 */
	public void rejectAllCurrencySettlementRequest(Date registerDate,LoggerUser loggerUser)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM SettlementRequest cs	WHERE  TRUNC(cs.registerDate) = TRUNC( :dateIni ) AND cs.requestState not in ( :requestState ) ");
			sbQuery.append(" and cs.requestType  in ( :currencyRequestType , :currencyReversionType ) ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("dateIni",  registerDate);
			List<Integer> states = new ArrayList<Integer>();
			states.add(SettlementCurrencyRequestStateType.AUTHORIZED.getCode());
			states.add(SettlementCurrencyRequestStateType.REJECTED.getCode());
			states.add(SettlementCurrencyRequestStateType.CANCELLED.getCode());
			query.setParameter("requestState", states );
			query.setParameter("currencyRequestType", SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE.getCode() );
			query.setParameter("currencyReversionType", SettlementRequestType.SETTLEMENT_CURRENCY_EXCHANGE_REVERSION.getCode() );
			List<SettlementRequest> lstCurrencySettlementRequest = (List<SettlementRequest>)query.getResultList();
			for(SettlementRequest currencySettlementRequest : lstCurrencySettlementRequest){
				currencySettlementRequest.setRequestState(SettlementCurrencyRequestStateType.REJECTED.getCode());
				currencySettlementRequest.setRejectMotive(CurrencySettlementMotiveRejectType.OTHER.getCode());
				currencySettlementRequest.setRejectOtherMotive("PENDIENTE DE AUTORIZAR");
				currencySettlementRequest.setRejectUser(loggerUser.getUserName());
				currencySettlementRequest.setRejectDate(CommonsUtilities.currentDateTime());
				
				update(currencySettlementRequest);
			}
		} catch (Exception ex) {
			if (ex instanceof ServiceException) {
				throw ex;
			}
			throw new ServiceException(ErrorServiceType.REJECT_ALL_CURRENCY_SETTLEMENT_REQUEST);
		}	
	}
}
