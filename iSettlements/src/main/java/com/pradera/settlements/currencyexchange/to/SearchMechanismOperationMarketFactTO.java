package com.pradera.settlements.currencyexchange.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchMechanismOperationMarketFactTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchMechanismOperationMarketFactTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The Id security code pk. */
	private String IdSecurityCodePk;
	
	/** The security description. */
	private String securityDescription;
	
	/** The instrument description. */
	private String instrumentDescription;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The market date. */
	private Date marketDate;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/**
	 * Instantiates a new search mechanism operation market fact to.
	 */
	public SearchMechanismOperationMarketFactTO(){
		
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return IdSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		IdSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the security description.
	 *
	 * @return the security description
	 */
	public String getSecurityDescription() {
		return securityDescription;
	}

	/**
	 * Sets the security description.
	 *
	 * @param securityDescription the new security description
	 */
	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}

	/**
	 * Gets the instrument description.
	 *
	 * @return the instrument description
	 */
	public String getInstrumentDescription() {
		return instrumentDescription;
	}

	/**
	 * Sets the instrument description.
	 *
	 * @param instrumentDescription the new instrument description
	 */
	public void setInstrumentDescription(String instrumentDescription) {
		this.instrumentDescription = instrumentDescription;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
	
}
