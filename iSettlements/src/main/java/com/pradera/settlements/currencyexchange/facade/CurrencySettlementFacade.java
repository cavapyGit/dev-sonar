package com.pradera.settlements.currencyexchange.facade;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.settlement.CurrencySettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.SettlementCurrencyRequestStateType;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.settlements.currencyexchange.service.CurrencySettlementService;
import com.pradera.settlements.currencyexchange.to.SettlementRequestTO;
import com.pradera.settlements.currencyexchange.to.SearchCurrencySettlementRequestTO;
import com.pradera.settlements.currencyexchange.to.SearchSettlementOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CurrencySettlementFacade.
 *
 * @author Pradera Technologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class CurrencySettlementFacade {

	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** The currency Settlement Service. */
	
	@EJB
	private CurrencySettlementService currencySettlementService;
	
	/** The negotiation operation service bean. */
	@EJB
	private NegotiationOperationServiceBean negotiationOperationServiceBean;
		
	/**
	 * Gets the securities by id.
	 *
	 * @param idIsinCode the id isin code
	 * @return the securities by id
	 * @throws ServiceException the service exception
	 */
	public Security getSecuritiesById(String idIsinCode)throws ServiceException{
		return currencySettlementService.getSecuritiesById(idIsinCode);
	}
	
	/**
	 * Get Mechanism Operation By Filter Service Bean.
	 *
	 * @param searchCurrencySettlementRequestTO filter Currency Settlement
	 * @return List Mechanism Operation
	 * @throws ServiceException the service exception
	 */
	public List<SearchSettlementOperationTO> getMechanismOperationByFilterServiceBean(SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO)throws ServiceException{
		return currencySettlementService.getMechanismOperationByFilterServiceBean(searchCurrencySettlementRequestTO);
	}
	
	/**
	 * Gets the currency exchanged settlement operation.
	 *
	 * @param searchCurrencySettlementRequestTO the search currency settlement request to
	 * @return the currency exchanged settlement operation
	 * @throws ServiceException the service exception
	 */
	public List<SearchSettlementOperationTO> getCurrencyExchangedSettlementOperation(SearchCurrencySettlementRequestTO searchCurrencySettlementRequestTO)throws ServiceException{
		return currencySettlementService.getCurrencyExchangedSettlementOperation(searchCurrencySettlementRequestTO);
	}
	
	/**
	 * Get Mechanism Operation MarketFact.
	 *
	 * @param idMechanismOperation id Mechanism Operation
	 * @return object mechanism Operation MarketFact
	 * @throws ServiceException the service exception
	 */
	public MechanismOperationMarketFact getMechanismOperationMarketFact(Long idMechanismOperation)throws ServiceException{
		return currencySettlementService.getMechanismOperationMarketFact(idMechanismOperation);
	}
	
	/**
	 * Request Exists Currency Settlement.
	 *
	 * @param idMechanismOperation id Mechanism Operation
	 * @param operationPart operation Part
	 * @param requestType the request type
	 * @return object Currency Settlement Detail
	 * @throws ServiceException the service exception
	 */
	public CurrencySettlementOperation requestExistsCurrencySettlement(Long idMechanismOperation,Integer operationPart , Integer requestType)throws ServiceException{
		return currencySettlementService.requestExistsCurrencySettlement(idMechanismOperation, operationPart, requestType);
	}
	
	/**
	 * Registry Currency Settlement Request Facade.
	 *
	 * @param currencySettlementRequest object currency Settlement Request
	 * @param indUserParticipant the ind user participant
	 * @return object currency Settlement Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REGISTER)
	public SettlementRequest registryCurrencySettlementRequestFacade(SettlementRequest currencySettlementRequest, Integer indUserParticipant)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());     
		return currencySettlementService.registryCurrencySettlementRequest(currencySettlementRequest, loggerUser, indUserParticipant);
	}
	
	/**
	 * Registry currency settlement request reversion facade.
	 *
	 * @param currencySettlementRequests the currency settlement requests
	 * @param indUserParticipant the ind user participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SettlementRequest> registryCurrencySettlementRequestReversionFacade(List<SettlementRequest> currencySettlementRequests, Integer indUserParticipant)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());     
        
        for (SettlementRequest settlementRequest : currencySettlementRequests) {
        	currencySettlementService.registryCurrencySettlementRequest(settlementRequest, loggerUser, indUserParticipant);
		}
		return currencySettlementRequests;
	}
	
	/**
	 * Get List Currency Settlement Request Filter.
	 *
	 * @param searchCurrencySettlementRequestHeaderTO object search Currency Settlement Request
	 * @return object Currency Settlement Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_QUERY)
	public List<SettlementRequestTO> getListCurrencySettlementRequestFilter(SearchCurrencySettlementRequestTO searchCurrencySettlementRequestHeaderTO)throws ServiceException{
		return currencySettlementService.getListCurrencySettlementRequestFilter(searchCurrencySettlementRequestHeaderTO);
	}
	
	/**
	 * Approve Currency Settlement Request.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_APPROVE)
	public SettlementRequest approveCurrencySettlementRequestFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());   
		return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.APPROVED.getCode(), loggerUser);
	}
	
	/**
	 * Annul Currency Settlement Request.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_CANCEL)
	public SettlementRequest annulCurrencySettlementRequestFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular()); 
		return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.CANCELLED.getCode(), loggerUser);
	}
	
	/**
	 * Review Currency Settlement Request.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REVIEW)
	public SettlementRequest reviewCurrencySettlementRequestFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());   
		return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.REVISED.getCode(), loggerUser);
	}
	
	/**
	 * Reject Currency Settlement Request for Approve.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REJECT)
	public SettlementRequest rejectCurrencySettlementRequestApproveFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());   
		return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.REJECTED.getCode(), loggerUser);
	}
	
	/**
	 * Reject Currency Settlement Request for Review.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REJECT)
	public SettlementRequest rejectCurrencySettlementRequestReviewFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());   
        return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.REJECTED.getCode(), loggerUser);
	}
	
	/**
	 * Reject Currency Settlement Request for Confirm.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_REJECT)
	public SettlementRequest rejectCurrencySettlementRequestConfirmFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());   
        return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.REJECTED.getCode(), loggerUser);
	}
	
	/**
	 * Confirm Currency Settlement Request.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_CONFIRM)
	public SettlementRequest confirmCurrencySettlementRequestFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());   
        return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.CONFIRMED.getCode(), loggerUser);
	}
	
	/**
	 * Authorize Currency Settlement Request.
	 *
	 * @param currencySettlementRequestTO the currency settlement request to
	 * @return object currency SettlementRequest
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.CURRENCY_SETTLEMENT_REQUEST_AUTHORIZE)
	public SettlementRequest authorizeCurrencySettlementRequestFacade(SettlementRequestTO currencySettlementRequestTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAuthorize());   
        return currencySettlementService.changeOperationState(currencySettlementRequestTO,SettlementCurrencyRequestStateType.AUTHORIZED.getCode(), loggerUser);
	}
	
	/**
	 * Reject All Currency Settlement Request.
	 *
	 * @param registerDate register Date for request
	 * @throws ServiceException the service exception
	 */
	public void rejectAllCurrencySettlementRequest(Date registerDate)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
        currencySettlementService.rejectAllCurrencySettlementRequest(registerDate, loggerUser);
	}

	/**
	 * Find assignment process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param settlementDate the settlement date
	 * @return the assignment process
	 */
	public AssignmentProcess findAssignmentProcess(Long idMechanism,Long idModality, Date settlementDate) {
		return negotiationOperationServiceBean.findAssignmentProcess(idMechanism, idModality, settlementDate);
	}
}
