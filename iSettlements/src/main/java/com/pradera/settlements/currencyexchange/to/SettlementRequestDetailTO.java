package com.pradera.settlements.currencyexchange.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.GeneralConstants;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementRequestDetailTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementRequestDetailTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id currency settlement. */
	private Long idCurrencySettlement;
	
	/** The id settlement date operation. */
	private Long idSettlementDateOperation;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The operation state. */
	private Integer operationState;
	
	/** The state description. */
	private String stateDescription;
	
	/** The cash settlement date. */
	private Date cashSettlementDate;
	
	/** The term settlement date. */
	private Date termSettlementDate;
	
	/** The currency. */
	private Integer currency;
	
	/** The currency description. */
	private String currencyDescription;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The security class description. */
	private String securityClassDescription;
	
	/** The id mechanism. */
	private Long idMechanism;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The id modality. */
	private Long idModality;
	
	/** The modality name. */
	private String modalityName;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The cash price. */
	private BigDecimal cashPrice;
	
	/** The cash amount. */
	private BigDecimal cashAmount;
	
	/** The term price. */
	private BigDecimal termPrice;
	
	/** The term amount. */
	private BigDecimal termAmount;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The initial settlement currency. */
	private Integer initialSettlementCurrency;
	
	/** The initial settlement price. */
	private BigDecimal initialSettlementPrice;
	
	/** The initial settlement amount. */
	private BigDecimal initialSettlementAmount;
	
	/** The settlement currency. */
	private Integer settlementCurrency;
	
	/** The settlement price. */
	private BigDecimal settlementPrice;
	
	/** The settlement amount. */
	private BigDecimal settlementAmount;
	
	/** The settlement currency description. */
	private String settlementCurrencyDescription;
	
	/** The exchange rate. */
	private BigDecimal exchangeRate;
	
	/** The operation part. */
	private Integer operationPart;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The ballot number sequential. */
	private String ballotNumberSequential;
	
	/** The cash part. */
	private boolean cashPart;
	
	/** The term part. */
	private boolean termPart;
	
	/**
	 * Gets the id currency settlement.
	 *
	 * @return the id currency settlement
	 */
	public Long getIdCurrencySettlement() {
		return idCurrencySettlement;
	}
	
	/**
	 * Sets the id currency settlement.
	 *
	 * @param idCurrencySettlement the new id currency settlement
	 */
	public void setIdCurrencySettlement(Long idCurrencySettlement) {
		this.idCurrencySettlement = idCurrencySettlement;
	}
	
	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}
	
	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}
	
	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the cash settlement date.
	 *
	 * @return the cash settlement date
	 */
	public Date getCashSettlementDate() {
		return cashSettlementDate;
	}
	
	/**
	 * Sets the cash settlement date.
	 *
	 * @param cashSettlementDate the new cash settlement date
	 */
	public void setCashSettlementDate(Date cashSettlementDate) {
		this.cashSettlementDate = cashSettlementDate;
	}
	
	/**
	 * Gets the term settlement date.
	 *
	 * @return the term settlement date
	 */
	public Date getTermSettlementDate() {
		return termSettlementDate;
	}
	
	/**
	 * Sets the term settlement date.
	 *
	 * @param termSettlementDate the new term settlement date
	 */
	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	
	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	
	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}
	
	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	
	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the security class description.
	 *
	 * @return the security class description
	 */
	public String getSecurityClassDescription() {
		return securityClassDescription;
	}
	
	/**
	 * Sets the security class description.
	 *
	 * @param securityClassDsescription the new security class description
	 */
	public void setSecurityClassDescription(String securityClassDsescription) {
		this.securityClassDescription = securityClassDsescription;
	}
	
	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return idMechanism;
	}
	
	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}
	
	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}
	
	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}
	
	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}
	
	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}
	
	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}
	
	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the cash price.
	 *
	 * @return the cash price
	 */
	public BigDecimal getCashPrice() {
		return cashPrice;
	}
	
	/**
	 * Sets the cash price.
	 *
	 * @param cashPrice the new cash price
	 */
	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}
	
	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}
	
	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}
	
	/**
	 * Gets the term price.
	 *
	 * @return the term price
	 */
	public BigDecimal getTermPrice() {
		return termPrice;
	}
	
	/**
	 * Sets the term price.
	 *
	 * @param termPrice the new term price
	 */
	public void setTermPrice(BigDecimal termPrice) {
		this.termPrice = termPrice;
	}
	
	/**
	 * Gets the term amount.
	 *
	 * @return the term amount
	 */
	public BigDecimal getTermAmount() {
		return termAmount;
	}
	
	/**
	 * Sets the term amount.
	 *
	 * @param termAmount the new term amount
	 */
	public void setTermAmount(BigDecimal termAmount) {
		this.termAmount = termAmount;
	}
	
	/**
	 * Gets the settlement currency.
	 *
	 * @return the settlement currency
	 */
	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}
	
	/**
	 * Sets the settlement currency.
	 *
	 * @param settlementCurrency the new settlement currency
	 */
	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}
	
	/**
	 * Gets the settlement price.
	 *
	 * @return the settlement price
	 */
	public BigDecimal getSettlementPrice() {
		return settlementPrice;
	}
	
	/**
	 * Sets the settlement price.
	 *
	 * @param settlementPrice the new settlement price
	 */
	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}
	
	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}
	
	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	
	/**
	 * Gets the settlement currency description.
	 *
	 * @return the settlement currency description
	 */
	public String getSettlementCurrencyDescription() {
		return settlementCurrencyDescription;
	}
	
	/**
	 * Sets the settlement currency description.
	 *
	 * @param settlementCurrencyDescription the new settlement currency description
	 */
	public void setSettlementCurrencyDescription(
			String settlementCurrencyDescription) {
		this.settlementCurrencyDescription = settlementCurrencyDescription;
	}
	
	/**
	 * Gets the exchange rate.
	 *
	 * @return the exchange rate
	 */
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	
	/**
	 * Sets the exchange rate.
	 *
	 * @param exchangeRate the new exchange rate
	 */
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}
	
	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the ballot number sequential.
	 *
	 * @return the ballot number sequential
	 */
	public String getBallotNumberSequential() {
		if(ballotNumberSequential == null){
			StringBuilder sbOpNumber = new StringBuilder();
			if(ballotNumber!=null && sequential!=null){
				sbOpNumber.append(this.ballotNumber).append(GeneralConstants.SLASH).append(this.sequential);
			}else{
				sbOpNumber.append(operationNumber.toString());
			}
			ballotNumberSequential = sbOpNumber.toString();
		}
		return ballotNumberSequential;
	}
	
	/**
	 * Checks if is cash part.
	 *
	 * @return true, if is cash part
	 */
	public boolean isCashPart() {
		return cashPart;
	}
	
	/**
	 * Sets the cash part.
	 *
	 * @param cashPart the new cash part
	 */
	public void setCashPart(boolean cashPart) {
		this.cashPart = cashPart;
	}
	
	/**
	 * Checks if is term part.
	 *
	 * @return true, if is term part
	 */
	public boolean isTermPart() {
		return termPart;
	}
	
	/**
	 * Sets the term part.
	 *
	 * @param termPart the new term part
	 */
	public void setTermPart(boolean termPart) {
		this.termPart = termPart;
	}
	
	/**
	 * Gets the id settlement operation.
	 *
	 * @return the id settlement operation
	 */
	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}
	
	/**
	 * Sets the id settlement operation.
	 *
	 * @param idSettlementOperation the new id settlement operation
	 */
	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}
	
	/**
	 * Gets the id settlement date operation.
	 *
	 * @return the id settlement date operation
	 */
	public Long getIdSettlementDateOperation() {
		return idSettlementDateOperation;
	}
	
	/**
	 * Sets the id settlement date operation.
	 *
	 * @param idSettlementDateOperation the new id settlement date operation
	 */
	public void setIdSettlementDateOperation(Long idSettlementDateOperation) {
		this.idSettlementDateOperation = idSettlementDateOperation;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the initial settlement currency.
	 *
	 * @return the initial settlement currency
	 */
	public Integer getInitialSettlementCurrency() {
		return initialSettlementCurrency;
	}
	
	/**
	 * Sets the initial settlement currency.
	 *
	 * @param initialSettlementCurrency the new initial settlement currency
	 */
	public void setInitialSettlementCurrency(Integer initialSettlementCurrency) {
		this.initialSettlementCurrency = initialSettlementCurrency;
	}
	
	/**
	 * Gets the initial settlement price.
	 *
	 * @return the initial settlement price
	 */
	public BigDecimal getInitialSettlementPrice() {
		return initialSettlementPrice;
	}
	
	/**
	 * Sets the initial settlement price.
	 *
	 * @param initialSettlementPrice the new initial settlement price
	 */
	public void setInitialSettlementPrice(BigDecimal initialSettlementPrice) {
		this.initialSettlementPrice = initialSettlementPrice;
	}
	
	/**
	 * Gets the initial settlement amount.
	 *
	 * @return the initial settlement amount
	 */
	public BigDecimal getInitialSettlementAmount() {
		return initialSettlementAmount;
	}
	
	/**
	 * Sets the initial settlement amount.
	 *
	 * @param initialSettlementAmount the new initial settlement amount
	 */
	public void setInitialSettlementAmount(BigDecimal initialSettlementAmount) {
		this.initialSettlementAmount = initialSettlementAmount;
	}
	
}
