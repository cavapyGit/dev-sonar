package com.pradera.funds.banks.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.funds.banks.to.SearchBanksTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageBanksService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class ManageBanksService extends CrudDaoServiceBean{

	/**
	 * Get Bank For Mnemonic.
	 *
	 * @param mnemonic the mnemonic
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank getBankForMnemonic(String mnemonic)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	WHERE cs.mnemonic = :mnemonic ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("mnemonic",  mnemonic);
			Bank bank = (Bank)query.getSingleResult();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Bank For Bic Code.
	 *
	 * @param biccode the biccode
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank getBankForBicCode(String biccode)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	WHERE cs.bicCode = :bicCode ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("bicCode",  biccode);
			Bank bank = (Bank)query.getSingleResult();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	 
 	/**
 	 * Get Bank For Document.
 	 *
 	 * @param documentType document type
 	 * @param documentNumber document number
 	 * @return  object bank
 	 * @throws ServiceException the Service Exception
 	 */ 
	public Bank getBankForDocument(Long documentType,String documentNumber)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	WHERE cs.documentType = :documentType AND cs.documentNumber = :documentNumber");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("documentType",  documentType);
			query.setParameter("documentNumber",  documentNumber);
			Bank bank = (Bank)query.getSingleResult();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Bank For Bank Type.
	 *
	 * @param bankType bank Type
	 * @return  object bank
	 * @throws ServiceException the Service Exception
	 */ 
	public Bank getBankForBankType(Integer bankType)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	WHERE cs.bankType = :bankType ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("bankType",  bankType);
			Bank bank = (Bank)query.getSingleResult();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Bank For getBankForId.
	 *
	 * @param idBank id Bank
	 * @return  object bank
	 * @throws ServiceException the Service Exception
	 */ 
	public Bank getBankForId(Long idBank)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	WHERE cs.idBankPk = :idBankPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("idBankPk",  idBank);
			Bank bank = (Bank)query.getSingleResult();	
			if(Validations.validateIsNotNull(bank)){
				if(Validations.validateIsNotNull(bank.getCountry())){
					bank.getCountry().getName();
				}
				if(Validations.validateIsNotNull(bank.getDepartment())){
					bank.getDepartment().getName();
				}
				if(Validations.validateIsNotNull(bank.getProvince())){
					bank.getProvince().getName();
				}
				if(Validations.validateIsNotNull(bank.getDistrict())){
					bank.getDistrict().getName();
				}
			}			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Registry Bank Facade.
	 *
	 * @param bank object bank
	 * @param loggerUser the logger user
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank registryBankFacade(Bank bank,LoggerUser loggerUser)throws ServiceException{
		try{
			Bank bankMnemonic = getBankForMnemonic(bank.getMnemonic());
			if(Validations.validateIsNotNullAndNotEmpty(bankMnemonic))
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			Bank bankBicCode = getBankForBicCode(bank.getBicCode());
			if(Validations.validateIsNotNullAndNotEmpty(bankBicCode))
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			Bank bankDocument = getBankForDocument(bank.getDocumentType(),bank.getDocumentNumber());
			if(Validations.validateIsNotNullAndNotEmpty(bankDocument))
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			if(bank.getBankType().equals(BankType.CENTRALIZING.getCode())){
				Bank bankType = getBankForBankType(bank.getBankType());
				if(Validations.validateIsNotNullAndNotEmpty(bankType))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			bank.setState(BankStateType.REGISTERED.getCode());
			bank.setRegistryUser(loggerUser.getUserName());
			bank.setRegistryDate(CommonsUtilities.currentDateTime());
			
			if(Validations.validateIsNull(bank.getDistrict().getIdGeographicLocationPk())
					||bank.getDistrict().getIdGeographicLocationPk().equals(GeneralConstants.ZERO_VALUE_INTEGER))
				bank.setDistrict(null);
			create(bank);
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Modify Bank Facade.
	 *
	 * @param bank object bank
	 * @param loggerUser the logger user
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank modifyBankFacade(Bank bank,LoggerUser loggerUser)throws ServiceException{
		try{
			Bank bankBicCode = getBankForBicCode(bank.getBicCode());
			if(bankBicCode!=null)
				if(!(bankBicCode.getIdBankPk().equals(bank.getIdBankPk())))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);		
			Bank bankId = find(Bank.class, bank.getIdBankPk());
			if(bankId.getLastModifyDate().compareTo(bank.getLastModifyDate())==1)
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);		
			if(Validations.validateIsNull(bank.getDistrict().getIdGeographicLocationPk())
					||bank.getDistrict().getIdGeographicLocationPk().equals(GeneralConstants.ZERO_VALUE_INTEGER))
				bank.setDistrict(null);
			update(bank);
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get List Bank Filter.
	 *
	 * @param searchBanksTO   search Banks TO
	 * @return List Bank Filter
	 * @throws ServiceException the Service Exception
	 */
	public List<Bank> getListBankFilter(SearchBanksTO searchBanksTO)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();	
			stringBuilder.append("	Select cs.idBankPk,cs.documentType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.documentType),	");//0,1,2
			stringBuilder.append("	cs.documentNumber,cs.description,cs.mnemonic,cs.bicCode,	"); //3,4,5,6
			stringBuilder.append("	cs.state, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.state)	"); //7,8
			stringBuilder.append("	from Bank cs");
			stringBuilder.append("	Where 1 = 1	");	
			if(Validations.validateIsNotNullAndNotEmpty(searchBanksTO.getMnemonic())){
				stringBuilder.append("	AND	cs.mnemonic = :mnemonic	");
				parameters.put("mnemonic", searchBanksTO.getMnemonic());							
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBanksTO.getBicCode())){
				stringBuilder.append("	AND	cs.bicCode = :bicCode	");
				parameters.put("bicCode", searchBanksTO.getBicCode());							
			}
			if(Validations.validateIsNotNull(searchBanksTO.getState()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBanksTO.getState())))){
				stringBuilder.append("	AND	cs.state = :state	");
				parameters.put("state", searchBanksTO.getState());							
			}
			if(Validations.validateIsNotNull(searchBanksTO.getDocumentType()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBanksTO.getDocumentType())))){
				stringBuilder.append("	AND	cs.documentType = :documentType	");
				parameters.put("documentType", searchBanksTO.getDocumentType());							
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBanksTO.getDocumentNumber())){
				stringBuilder.append("	AND	cs.documentNumber = :documentNumber	");
				parameters.put("documentNumber", searchBanksTO.getDocumentNumber());							
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBanksTO.getDescription())){
				stringBuilder.append("	AND	cs.description  like :description	");
				parameters.put("description",GeneralConstants.PERCENTAGE_STRING + searchBanksTO.getDescription() + GeneralConstants.PERCENTAGE_STRING );							
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBanksTO.getInitialDate())){
				stringBuilder.append(" and  TRUNC(cs.registryDate) >= :dateIni ");
				parameters.put("dateIni", searchBanksTO.getInitialDate());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBanksTO.getFinalDate()))
			{
				stringBuilder.append(" and  TRUNC(cs.registryDate) <= :dateEnd ");
				parameters.put("dateEnd", searchBanksTO.getFinalDate());
			}
			stringBuilder.append("	order by cs.idBankPk desc	");	
			List<Object[]> lstBank =  findListByQueryString(stringBuilder.toString(), parameters);
			Map<Long,Bank> bankPk = new HashMap<Long, Bank>();
			for(Object[] object: lstBank){
				Bank bank = new Bank();
				bank.setIdBankPk(new Long(object[0].toString()));
				if(Validations.validateIsNotNull((object[1])))
					bank.setDocumentType(new Long(object[1].toString()));
				if(Validations.validateIsNotNull((object[2])))
					bank.setDocumentTypeDescription(object[2].toString());
				if(Validations.validateIsNotNull((object[3])))
					bank.setDocumentNumber(object[3].toString());
				if(Validations.validateIsNotNull((object[4])))
					bank.setDescription(object[4].toString());
				if(Validations.validateIsNotNull((object[5])))
					bank.setMnemonic(object[5].toString());
				if(Validations.validateIsNotNull((object[6])))
					bank.setBicCode(object[6].toString());
				if(Validations.validateIsNotNull((object[7])))
					bank.setState(new Integer(object[7].toString()));
				if(Validations.validateIsNotNull((object[8])))
					bank.setStateDescription(object[8].toString());
				bankPk.put(bank.getIdBankPk(), bank);
			}
			return new ArrayList<Bank>(bankPk.values());
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Block Bank Facade.
	 *
	 * @param bank object bank
	 * @param loggerUser the logger user
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank blockBank(Bank bank,LoggerUser loggerUser)throws ServiceException{
		try{
		Map<String,Object> mapParams = new HashMap<String, Object>();
		mapParams.put("idBankPkParam", bank.getIdBankPk());
		Bank bankAux = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, mapParams);
		if(!BankStateType.REGISTERED.getCode().equals(bankAux.getState())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		}
		Bank bankUpdate = find(Bank.class, bank.getIdBankPk());
		bankUpdate.setState(BankStateType.BLOCKED.getCode());
		bankUpdate.setBlockMotive(bank.getBlockMotive());
		bankUpdate.setBlockOtherMotive(bank.getBlockOtherMotive());
		bankUpdate.setBlockUser(loggerUser.getUserName());
		bankUpdate.setBlockDate(CommonsUtilities.currentDateTime());
		
		update(bankUpdate);
		
		return bankUpdate;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * UnBlock Bank Facade.
	 *
	 * @param bank object bank
	 * @param loggerUser the logger user
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank unblockBank(Bank bank,LoggerUser loggerUser)throws ServiceException{
		try{
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idBankPkParam", bank.getIdBankPk());
			Bank bankAux = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, mapParams);
			if(!BankStateType.BLOCKED.getCode().equals(bankAux.getState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			Bank bankUpdate = find(Bank.class, bank.getIdBankPk());
			bankUpdate.setState(BankStateType.REGISTERED.getCode());
			bankUpdate.setUnblockMotive(bank.getUnblockMotive());
			bankUpdate.setUnblockOtherMotive(bank.getUnblockOtherMotive());
			bankUpdate.setUnblockUser(loggerUser.getUserName());
			bankUpdate.setUnblockDate(CommonsUtilities.currentDateTime());
			
			update(bankUpdate);
			
			return bankUpdate;
			}catch(NoResultException ex){
				   return null;
			} catch(NonUniqueResultException ex){
				   return null;
			}	
	}
}
