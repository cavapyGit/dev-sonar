package com.pradera.funds.banks.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchBanksTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchBanksTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The state. */
	private Integer state;
	
	/** The document Type. */
	private Long documentType;
	
	/** The document Number. */
	private String documentNumber;
	
	/** The bic Code. */
	private String bicCode;
	
	/** The description. */
	private String description;
	
	/** The current Date. */
	private Date currentDate;
	
	/** The initial Date. */
	private Date initialDate;
	
	/** The final Date. */
	private Date finalDate;
	
	/** The List state. */
	private List<ParameterTable> lstState;
	
	/** The List document Type. */
	private List<ParameterTable> lstDocumentType;
	
	/** The List Bank Type. */
	private List<ParameterTable> lstBankType;
	
	/** The List Bank Origin. */
	private List<ParameterTable> lstBankOrigin;
	
	/** The List country. */
	private List<GeographicLocation> lstCountry;
	
	/** The List department. */
	private List<GeographicLocation> lstDepartment;
	
	/** The List province. */
	private List<GeographicLocation> lstProvince;
	
	/** The List district. */
	private List<GeographicLocation> lstDistrict;

	/**
	 * Instantiates a new search banks to.
	 */
	public SearchBanksTO(){
		
	}
	
	/**
	 * Instantiates a new search banks to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 */
	public SearchBanksTO(Date initialDate, Date finalDate) {
		this.initialDate = initialDate;
		this.finalDate = finalDate;
	}
	
	/**
	 * Instantiates a new search banks to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 * @param currentDate the current date
	 */
	public SearchBanksTO(Date initialDate, Date finalDate,Date currentDate) {
		this.initialDate = initialDate;
		this.finalDate = finalDate;
		this.currentDate = currentDate;
	}
	
	/**
	 * Instantiates a new search banks to.
	 *
	 * @param currentDate the current date
	 */
	public SearchBanksTO(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}
	
	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Long getDocumentType() {
		return documentType;
	}
	
	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Long documentType) {
		this.documentType = documentType;
	}
	
	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}
	
	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	
	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	
	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public List<ParameterTable> getLstDocumentType() {
		return lstDocumentType;
	}
	
	/**
	 * Sets the lst document type.
	 *
	 * @param lstDocumentType the new lst document type
	 */
	public void setLstDocumentType(List<ParameterTable> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}
	
	/**
	 * Gets the lst bank type.
	 *
	 * @return the lst bank type
	 */
	public List<ParameterTable> getLstBankType() {
		return lstBankType;
	}
	
	/**
	 * Sets the lst bank type.
	 *
	 * @param lstBankType the new lst bank type
	 */
	public void setLstBankType(List<ParameterTable> lstBankType) {
		this.lstBankType = lstBankType;
	}
	
	/**
	 * Gets the lst bank origin.
	 *
	 * @return the lst bank origin
	 */
	public List<ParameterTable> getLstBankOrigin() {
		return lstBankOrigin;
	}
	
	/**
	 * Sets the lst bank origin.
	 *
	 * @param lstBankOrigin the new lst bank origin
	 */
	public void setLstBankOrigin(List<ParameterTable> lstBankOrigin) {
		this.lstBankOrigin = lstBankOrigin;
	}
	
	/**
	 * Gets the lst country.
	 *
	 * @return the lst country
	 */
	public List<GeographicLocation> getLstCountry() {
		return lstCountry;
	}
	
	/**
	 * Sets the lst country.
	 *
	 * @param lstCountry the new lst country
	 */
	public void setLstCountry(List<GeographicLocation> lstCountry) {
		this.lstCountry = lstCountry;
	}
	
	/**
	 * Gets the lst department.
	 *
	 * @return the lst department
	 */
	public List<GeographicLocation> getLstDepartment() {
		return lstDepartment;
	}
	
	/**
	 * Sets the lst department.
	 *
	 * @param lstDepartment the new lst department
	 */
	public void setLstDepartment(List<GeographicLocation> lstDepartment) {
		this.lstDepartment = lstDepartment;
	}
	
	/**
	 * Gets the lst province.
	 *
	 * @return the lst province
	 */
	public List<GeographicLocation> getLstProvince() {
		return lstProvince;
	}
	
	/**
	 * Sets the lst province.
	 *
	 * @param lstProvince the new lst province
	 */
	public void setLstProvince(List<GeographicLocation> lstProvince) {
		this.lstProvince = lstProvince;
	}
	
	/**
	 * Gets the lst district.
	 *
	 * @return the lst district
	 */
	public List<GeographicLocation> getLstDistrict() {
		return lstDistrict;
	}
	
	/**
	 * Sets the lst district.
	 *
	 * @param lstDistrict the new lst district
	 */
	public void setLstDistrict(List<GeographicLocation> lstDistrict) {
		this.lstDistrict = lstDistrict;
	}
	
	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}
	
	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	
}
