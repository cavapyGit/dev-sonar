package com.pradera.funds.banks.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.funds.banks.service.ManageBanksService;
import com.pradera.funds.banks.to.SearchBanksTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.Bank;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageBanksFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageBanksFacade {

	/** Transaction Registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** Manage Banks Service. */
	@EJB
	ManageBanksService manageBanksService;
	
	/**
	 * Get Bank For Mnemonic.
	 *
	 * @param mnemonic the mnemonic
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank getBankForMnemonic(String mnemonic)throws ServiceException{
		return manageBanksService.getBankForMnemonic(mnemonic);
	}
	
	/**
	 * Get Bank For Bic Code.
	 *
	 * @param biccode the biccode
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank getBankForBicCode(String biccode)throws ServiceException{
		return manageBanksService.getBankForBicCode(biccode);
	}
	 
 	/**
 	 * Get Bank For Document.
 	 *
 	 * @param documentType document type
 	 * @param documentNumber document number
 	 * @return  object bank
 	 * @throws ServiceException the Service Exception
 	 */ 
	public Bank getBankForDocument(Long documentType,String documentNumber)throws ServiceException{
		return manageBanksService.getBankForDocument(documentType, documentNumber);
	}
	
	/**
	 * Get Bank For BankT ype.
	 *
	 * @param bankType bank Type
	 * @return  object bank
	 * @throws ServiceException the Service Exception
	 */ 
	public Bank getBankForBankType(Integer bankType)throws ServiceException{
		return manageBanksService.getBankForBankType(bankType);
	}
	
	/**
	 * Get Bank For getBankForId.
	 *
	 * @param idBank id Bank
	 * @return  object bank
	 * @throws ServiceException the Service Exception
	 */ 
	public Bank getBankForId(Long idBank)throws ServiceException{
		return manageBanksService.getBankForId(idBank);
	}
	
	/**
	 * Registry Bank Facade.
	 *
	 * @param bank object bank
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_REGISTRATION)
	public Bank registryBankFacade(Bank bank)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());    
		return manageBanksService.registryBankFacade(bank,loggerUser);
	}
	
	/**
	 * Modify Bank Facade.
	 *
	 * @param bank object bank
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_MODIFICATION)
	public Bank modifyBankFacade(Bank bank)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify()); 
		return manageBanksService.modifyBankFacade(bank,loggerUser);
	}
	
	/**
	 * Get List Bank Filter.
	 *
	 * @param searchBanksTO   search Banks TO
	 * @return List Bank Filter
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_QUERY)
	public List<Bank> getListBankFilter(SearchBanksTO searchBanksTO)throws ServiceException{
		return manageBanksService.getListBankFilter(searchBanksTO);
	}
	
	/**
	 * Block Bank Facade.
	 *
	 * @param bank object bank
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_BLOCK)
	public Bank blockBankFacade(Bank bank)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock()); 
		return manageBanksService.blockBank(bank,loggerUser);
	}
	
	/**
	 * UnBlock Bank Facade.
	 *
	 * @param bank object bank
	 * @return object bank
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_UNBLOCK)
	public Bank unblockBankFacade(Bank bank)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock()); 
		return manageBanksService.unblockBank(bank,loggerUser);
	}
}
