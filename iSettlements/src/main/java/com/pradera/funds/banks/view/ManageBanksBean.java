package com.pradera.funds.banks.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.banks.facade.ManageBanksFacade;
import com.pradera.funds.banks.to.SearchBanksTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.funds.type.BankDocumentType;
import com.pradera.model.funds.type.BankMotiveBlockType;
import com.pradera.model.funds.type.BankMotiveUnBlockType;
import com.pradera.model.funds.type.BankOriginType;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.settlements.utils.MotiveController;
import com.pradera.settlements.utils.PropertiesConstants;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageBanksBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ManageBanksBean  extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The general parameters facade. */
    @EJB
    GeneralParametersFacade generalParametersFacade;
    
    /** The manage Banks Facade. */
    @EJB
    ManageBanksFacade manageBanksFacade;
    
    /** The bank Session. */
	private Bank bankSession;
		 
    /** The bank to. */
	private SearchBanksTO searchBanksRegisterTO; 
	
	/** The bank to. */
	private SearchBanksTO searchBanksTO; 
	
	/** The search currency Settlement Request Data Model. */
	private GenericDataModel<Bank> searchBankTODataModel;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	 /** Document search. */
    private DocumentTO documentTORegister;
    
    /** Document Validate. */
    @Inject
    private DocumentValidator documentValidatorRegister;  
    
	/** The motive controller Block. */
	private MotiveController motiveControllerBlock; 
	
	/** The motive controller reject. */
	private MotiveController motiveControllerUnBlock;
	
	/** The minimum size Mnemonic. */
	private int tamMinMnemonic=3;
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		createObject();
		try {			
			loadDocumentType();
			loadState();
			loadMotiveBlock();
			loadMotiveUnBlock();
			loadHolidays();
			loadUserValidation();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load Motive Block.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveBlock()throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.BANK_BLOCK_MOTIVE.getCode());	
		motiveControllerBlock.setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * Load Motive UnBlock.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveUnBlock()throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.BANK_UNBLOCK_MOTIVE.getCode());	
		motiveControllerUnBlock.setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * Load State.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadState()throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_STATE_TYPE.getCode());	
		searchBanksTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create Object.
	 */
	public void createObject(){
		searchBanksTO = new SearchBanksTO(CommonsUtilities.currentDate());
		searchBanksTO.setLstDocumentType(new ArrayList<ParameterTable>());
		searchBanksTO.setLstState(new ArrayList<ParameterTable>());
		motiveControllerBlock = new MotiveController();
		motiveControllerUnBlock = new MotiveController();
		createObjectBankSession();
	}
	
	/**
	 * Load User Validation.
	 */
	private void loadUserValidation() {

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);				
	}
	
	/**
	 * Registry Bank Handler.
	 */
	public void registryBankHandler()
	{
		executeAction();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		createObjectBankSession();
		createObjectRegister();
		try {
			loadBankType();
			loadBankOrigin();
			loadDocumentType();
		} catch (ServiceException e) {
			e.printStackTrace();
		}			
	}
	
	/**
	 * Validate Mnemonic.
	 */
	public void validateMnemonic(){
		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getMnemonic())){
			if(bankSession.getMnemonic().length() < tamMinMnemonic){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_MNEMONIC_SIZE_MINIMUN) );			
				  JSFUtilities.showSimpleValidationDialog();
				  bankSession.setMnemonic(null);
			}else{
				try{
					Bank bank = manageBanksFacade.getBankForMnemonic(bankSession.getMnemonic());
					if(Validations.validateIsNotNullAndNotEmpty(bank)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_MNEMONIC_REGISTER,
							    		  new Object[]{bank.getIdBankPk().toString()}) );	
						JSFUtilities.showSimpleValidationDialog();
						bankSession.setMnemonic(null);
					}			
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}
		}		
	}
	
	/**
	 * Validate Bic Code.
	 */
	public void validateBicCode(){
		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getBicCode())){
			try{
				if(Validations.validateIsNotNullAndNotEmpty(bankSession.getIdBankPk())){
					Bank bank = manageBanksFacade.getBankForBicCode(bankSession.getBicCode());
					if(Validations.validateIsNotNullAndNotEmpty(bank) && !(bank.getIdBankPk().equals(bankSession.getIdBankPk()))){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_BIC_CODE_REGISTER,
							    		  new Object[]{bank.getIdBankPk().toString()}) );	
						JSFUtilities.showSimpleValidationDialog();
						bankSession.setBicCode(null);
					}
				}else{
					Bank bank = manageBanksFacade.getBankForBicCode(bankSession.getBicCode());
					if(Validations.validateIsNotNullAndNotEmpty(bank)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_BIC_CODE_REGISTER,
							    		  new Object[]{bank.getIdBankPk().toString()}) );	
						JSFUtilities.showSimpleValidationDialog();
						bankSession.setBicCode(null);
					}
				}
				bankSession.setMnemonic(bankSession.getBicCode());
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * lean For Bank Type.
	 */
	public void cleanForBankType(){
		bankSession.setBankOrigin(null);	
		bankSession.setDocumentType(null);
		bankSession.setDocumentNumber(null);
		bankSession.getCountry().setIdGeographicLocationPk(null);
		bankSession.getDepartment().setIdGeographicLocationPk(null);
		bankSession.getProvince().setIdGeographicLocationPk(null);
		bankSession.getDistrict().setIdGeographicLocationPk(null);
		searchBanksRegisterTO.setLstCountry(new ArrayList<GeographicLocation>());
		searchBanksRegisterTO.setLstDepartment(new ArrayList<GeographicLocation>());			
		searchBanksRegisterTO.setLstProvince(new ArrayList<GeographicLocation>());			
		searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());
	}
	
	/**
	 * Validate Bank Type.
	 */
	public void validateBankType(){
		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getBankType()))
		{
			if(bankSession.getBankType().equals(BankType.CENTRALIZING.getCode())){
				try{					
					Bank bank = manageBanksFacade.getBankForBankType(bankSession.getBankType());
					if(Validations.validateIsNotNullAndNotEmpty(bank)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_TYPE_REGISTER,
							    		  new Object[]{bank.getIdBankPk().toString()}) );	
						JSFUtilities.showSimpleValidationDialog();
						bankSession.setBankType(null);
						cleanForBankType();
					}
					else{
						bankSession.setBankOrigin(BankOriginType.NATIONAL.getCode());
						validateBankOrigin();
					}
				} catch (ServiceException e) {
					e.printStackTrace();
				}				
			}else {
				cleanForBankType();
			}
		}else {
			cleanForBankType();
		}
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())
				||getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
			JSFUtilities.resetComponent(":frmRegBank:bankOrigin");
			JSFUtilities.resetComponent(":frmRegBank:cboCountryAux");
			JSFUtilities.resetComponent(":frmRegBank:cboDepartment");
			JSFUtilities.resetComponent(":frmRegBank:cboProv");
			JSFUtilities.resetComponent(":frmRegBank:txtDocumentNumber");
		}		
	}
	
	/**
	 * Clean Bank Handler.
	 */
	public void cleanBankHandler(){
		executeAction();
		bankSession = new Bank();
		bankSession.setCountry(new GeographicLocation());
		bankSession.setDepartment(new GeographicLocation());
		bankSession.setProvince(new GeographicLocation());
		bankSession.setDistrict(new GeographicLocation());
	}
	
	/**
	 * Before Bank Handler.
	 */
	public void beforeBankHandler(){
		
		  if(Validations.validateIsNullOrEmpty(bankSession.getMnemonic()) || Validations.validateIsNullOrEmpty(bankSession.getBicCode()) || Validations.validateIsNullOrEmpty(bankSession.getDescription())
					|| Validations.validateIsNullOrNotPositive(bankSession.getBankType()) || Validations.validateIsNullOrNotPositive(bankSession.getBankOrigin())
					|| Validations.validateIsNullOrNotPositive(bankSession.getCountry().getIdGeographicLocationPk()) || Validations.validateIsNullOrNotPositive(bankSession.getDocumentType())
					|| Validations.validateIsNullOrEmpty(bankSession.getDocumentNumber()) || Validations.validateIsNullOrEmpty(bankSession.getPhoneNumber())|| Validations.validateIsNullOrEmpty(bankSession.getWebsite())
					|| Validations.validateIsNullOrEmpty(bankSession.getContactName()) || Validations.validateIsNullOrEmpty(bankSession.getAddress())		
					|| Validations.validateIsNullOrNotPositive(bankSession.getDepartment().getIdGeographicLocationPk())|| Validations.validateIsNullOrNotPositive(bankSession.getProvince().getIdGeographicLocationPk()))
			{
				  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
				   JSFUtilities.showSimpleValidationDialog();
				   return;
			}
		  if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage(PropertiesConstants.BANK_REGISTER));	
		  
		  if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode()))
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage(PropertiesConstants.BANK_MODIFY,new Object[]{bankSession.getIdBankPk().toString()}));	
		  		  
		  JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
	}
	
	/**
	 * Save Bank Handler.
	 */
	@LoggerAuditWeb
	public void saveBankHandler(){
		Bank bank=null;
		try{
			if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
				bank = manageBanksFacade.registryBankFacade(bankSession);
			if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode()))			
				bank = manageBanksFacade.modifyBankFacade(bankSession);	
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				cleanBankSearchHandler();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(Validations.validateIsNotNullAndNotEmpty(bank)){
			
			if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
				//Sending notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_REGISTRATION.getCode());
					Object[] parameters = new Object[]{bank.getIdBankPk().toString()};
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
							businessProcessNotification,bank.getIdBankPk(), parameters);
			}
			if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
				//Sending notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_MODIFICATION.getCode());
					Object[] parameters = new Object[]{bank.getIdBankPk().toString()};
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
							businessProcessNotification,bank.getIdBankPk(), parameters);
			}
				
			if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_REGISTER_OK, new Object[]{bank.getIdBankPk().toString()}));	
			if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode()))
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_MODIFY_OK, new Object[]{bank.getIdBankPk().toString()}));	
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}	
		cleanBankSearchHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());	
	}
	
	/**
	 * Clean Bank Search Handler.
	 */
	public void cleanBankSearchHandler(){
		executeAction();
		searchBanksTO.setMnemonic(null);
		searchBanksTO.setBicCode(null);
		searchBanksTO.setState(null);
		searchBanksTO.setDocumentType(null);
		searchBanksTO.setDocumentNumber(null);
		searchBanksTO.setDescription(null);
		searchBanksTO.setInitialDate(null);
		searchBanksTO.setFinalDate(null);
		cleanDataModel();
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			JSFUtilities.resetComponent(":frmBank:state");
	}
	
	/**
	 * Search Bank Handler.
	 */
	@LoggerAuditWeb
	public void searchBankHandler(){
		List <Bank> lstBank=null;		
		try{			
			lstBank = manageBanksFacade.getListBankFilter(searchBanksTO);
			searchBankTODataModel= new GenericDataModel<Bank>(lstBank);			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	/**
	 * Change selected country.
	 */
	public void changeSelectedCountry(){
		try{
			if(!Validations.validateIsNullOrNotPositive(bankSession.getCountry().getIdGeographicLocationPk()))
			{
				searchBanksRegisterTO.setLstDepartment(getLstDepartments(bankSession.getCountry().getIdGeographicLocationPk()));
				searchBanksRegisterTO.setLstProvince(new ArrayList<GeographicLocation>());			
				searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());
			}				
			else
			{				
				searchBanksRegisterTO.setLstDepartment(new ArrayList<GeographicLocation>());			
				searchBanksRegisterTO.setLstProvince(new ArrayList<GeographicLocation>());			
				searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load Province Department.
	 */
	public void loadProvinceDepartment()
	{
		try{
			if(!Validations.validateIsNullOrNotPositive(bankSession.getDepartment().getIdGeographicLocationPk()))
			{
				if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){	
					GeographicLocation geographicLocation = new GeographicLocation();
					geographicLocation.setIdGeographicLocationPk(bankSession.getDepartment().getIdGeographicLocationPk());
					bankSession.setDepartment(geographicLocation);
					bankSession.setProvince(new GeographicLocation());
					bankSession.setDistrict(new GeographicLocation());
				}
				searchBanksRegisterTO.setLstProvince(getLstProvinces(bankSession.getDepartment().getIdGeographicLocationPk()));
				searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());
			}
			else
			{								
				searchBanksRegisterTO.setLstProvince(new ArrayList<GeographicLocation>());			
				searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	/**
	 * Validate address format.
	 */
	public void validateAddressFormat(){
    	if(Validations.validateIsNotNullAndNotEmpty(bankSession.getAddress())){    		
    		if(!CommonsUtilities.matchAddress(bankSession.getAddress())){    		
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_ADDRESS_FORMAT));
    			JSFUtilities.showSimpleValidationDialog();    					
    			bankSession.setAddress(null);
    		}
    	}
	}
	/**
	 * Validate fax number format.
	 */
	public void validateFaxNumberFormat(){
		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getFaxNumber())){    			
    		if(!validateNotZero(bankSession.getFaxNumber())){    			
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_FAX_ONLY_ZERO));
    			JSFUtilities.showSimpleValidationDialog();				
    			bankSession.setFaxNumber(null);
    		}
    	}
	}
	/**
	 * Validate contact email format.
	 */
	public void validateContactEmailFormat(){	   
		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getContactEmail())){    		
    		if(!CommonsUtilities.matchEmail(bankSession.getContactEmail())){
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_CONTACT_EMAIL_FORMAT));
    			JSFUtilities.showSimpleValidationDialog();    	
    			bankSession.setContactEmail(null);
    		}
    	}
	}
	
	/**
	 * Load District Province.
	 */
	public void loadDistrictProvince()
	{
		try{
			if(!Validations.validateIsNullOrNotPositive(bankSession.getProvince().getIdGeographicLocationPk())){
				if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){	
					GeographicLocation geographicLocation = new GeographicLocation();
					geographicLocation.setIdGeographicLocationPk(bankSession.getProvince().getIdGeographicLocationPk());
					bankSession.setProvince(geographicLocation);
					bankSession.setDistrict(new GeographicLocation());
				}
				List<GeographicLocation> lstDistricts = getLstDistricts(bankSession.getProvince().getIdGeographicLocationPk());
				if(Validations.validateListIsNotNullAndNotEmpty(lstDistricts))
					searchBanksRegisterTO.setLstDistrict(lstDistricts);
				else searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());	
			}				
			else						
				searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	/**
	 * Before Block handler.
	 */
	public void beforeBlockHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(this.getBankSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getBankSession().getState().equals(BankStateType.REGISTERED.getCode())){
			setViewOperationType(ViewOperationsType.BLOCK.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Before Un Block handler.
	 */
	public void beforeUnBlockHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(this.getBankSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getBankSession().getState().equals(BankStateType.BLOCKED.getCode())){
			setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveUnBlock').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_BLOCKED));	
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Load Districts.
	 *
	 * @param referenceFk the reference fk
	 * @return List Districts
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getLstDistricts(Integer referenceFk) throws ServiceException{		
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return  generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);	
	}
	
	/**
	 * Load Provinces.
	 *
	 * @param referenceFk the reference fk
	 * @return List Provinces
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getLstProvinces(Integer referenceFk) throws ServiceException{		
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return  generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);	
	}
	
	/**
	 * Load Departments.
	 *
	 * @param referenceFk the reference fk
	 * @return List Departments
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getLstDepartments(Integer referenceFk) throws ServiceException{		
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return  generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);	
	}
	
	/**
	 * Validate Bank Origin.
	 */
	public void validateBankOrigin(){
		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getBankOrigin())){
			try{
				if(bankSession.getBankOrigin().equals(BankOriginType.NATIONAL.getCode())){
					loadCountries(BooleanType.YES.getCode());
					loadIssuerDocumentTypeNational();	
					bankSession.setCountry(new GeographicLocation(countryResidence));
					bankSession.setDocumentType(new Long(BankDocumentType.RUC.getCode()));
					changeSelectedDocumentTypeRegister();
					changeSelectedCountry();
				}else{
					loadCountries(BooleanType.NO.getCode());
					bankSession.getCountry().setIdGeographicLocationPk(null);
					changeSelectedCountry();
					loadIssuerDocumentTypeForeign();
					bankSession.setDocumentType(new Long(BankDocumentType.DIO.getCode()));
					changeSelectedDocumentTypeRegister();
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}else{
			bankSession.setDocumentType(null);
			bankSession.setDocumentNumber(null);
			bankSession.getCountry().setIdGeographicLocationPk(null);
			searchBanksRegisterTO.setLstCountry(new ArrayList<GeographicLocation>());
			changeSelectedCountry();
		}	
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())
				||getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
			JSFUtilities.resetComponent(":frmRegBank:cboCountryAux");
			JSFUtilities.resetComponent(":frmRegBank:txtDocumentNumber");
		}		
	}
	
	/**
	 * Load Issuer Document Type National.
	 */
	public void loadIssuerDocumentTypeNational()
	{
		PersonTO person=new PersonTO();
		person.setFlagIssuerIndicator(true);
		person.setFlagNational(true);
		person.setPersonType(PersonType.JURIDIC.getCode());
		searchBanksRegisterTO.setLstDocumentType(documentValidatorRegister.getDocumentTypeResult(person));
	}
	
	/**
	 * Load Issuer Document Type Foreign.
	 */
	public void loadIssuerDocumentTypeForeign()
	{
		PersonTO person=new PersonTO();
		person.setFlagIssuerIndicator(true);
		person.setFlagForeign(true);
		person.setPersonType(PersonType.JURIDIC.getCode());
		searchBanksRegisterTO.setLstDocumentType(documentValidatorRegister.getDocumentTypeResult(person));
	}
	
	/**
	 * Is Bank Type Centralizing.
	 *
	 * @return validate Bank Type Centralizing
	 */
	public boolean isIndBankTypeCentralizing(){
		return Validations.validateIsNotNull(bankSession.getBankType()) &&
				bankSession.getBankType() > 0 &&
				(BankType.CENTRALIZING.getCode().equals(bankSession.getBankType()));
	}
	
	/**
	 * Validate Document.
	 */
	public void validateDocument(){
		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getDocumentType())
				&& Validations.validateIsNotNullAndNotEmpty(bankSession.getDocumentNumber())){
			try{
				Bank bank = manageBanksFacade.getBankForDocument(bankSession.getDocumentType(),bankSession.getDocumentNumber());
				if(Validations.validateIsNotNullAndNotEmpty(bank)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_DOCUMENT_REGISTER,
						    		  new Object[]{bank.getIdBankPk().toString()}) );
					JSFUtilities.showSimpleValidationDialog();
					bankSession.setDocumentNumber(null);
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Validate web page format.
	 */
	public void validateWebPageFormat(){
    	if(Validations.validateIsNotNullAndNotEmpty(bankSession.getWebsite())){
    		if(!CommonsUtilities.matchWebPage(bankSession.getWebsite())){
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_WEB_PAGE_FORMAT));
    			JSFUtilities.showSimpleValidationDialog();		
    			bankSession.setWebsite(null);
    		}
    	}
	}
	/**
	 * Validate phone number format.
	 */
	public void validatePhoneNumberFormat(){
    	if(Validations.validateIsNotNullAndNotEmpty(bankSession.getPhoneNumber())){      			
    		if(!validateNotZero(bankSession.getPhoneNumber())){    		
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_PHONE_ONLY_ZERO));
    			JSFUtilities.showSimpleValidationDialog();			
    			bankSession.setPhoneNumber(null);
    		}
    	}

		if(Validations.validateIsNotNullAndNotEmpty(bankSession.getContactPhoneNumber())){      			
    		if(!validateNotZero(bankSession.getContactPhoneNumber())){    		
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_PHONE_ONLY_ZERO));
    			JSFUtilities.showSimpleValidationDialog();			
    			bankSession.setContactPhoneNumber(null);
    		}
    	}
    	
	}
	
	/**
	 * Zero to validate string.
	 *
	 * @param str  validate string
	 * @return string to validate
	 */
	public boolean validateNotZero(String str)
	{
		boolean charNotZero = false;
		for (char charStr : str.toCharArray()) {
			if(charStr!='0'){
				charNotZero = true;
				break;
			}
		} 
		return charNotZero;
	}
	
	/**
	 * Is Bank Origin National.
	 *
	 * @return validate Bank Origin National
	 */
	public boolean isIndBankOriginNational(){
		return Validations.validateIsNotNull(bankSession.getBankOrigin()) &&
				bankSession.getBankOrigin() > 0 &&
				(BankOriginType.NATIONAL.getCode().equals(bankSession.getBankOrigin()));
	}
	
	/**
	 * Load Document Type.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadDocumentType()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(DocumentType.RUC.getCode());
		lstParameterTablePk.add(DocumentType.DIO.getCode());
		filterParameterTable.setLstParameterTablePk(lstParameterTablePk);
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			searchBanksRegisterTO.setLstDocumentType(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			searchBanksTO.setLstDocumentType(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Load Bank Origin.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadBankOrigin()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ORIGIN.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		searchBanksRegisterTO.setLstBankOrigin(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Load Bank Type.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadBankType()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		searchBanksRegisterTO.setLstBankType(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Load Countries.
	 *
	 * @param indLocalCountry indicator local country
	 * @throws ServiceException the Service Exception
	 */
	private void loadCountries(Integer indLocalCountry) throws ServiceException{
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocationTO.setIndLocalCountry(indLocalCountry);
		searchBanksRegisterTO.setLstCountry(generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO));
	}
	
	/**
	 * Create Object Register.
	 */
	private void createObjectRegister(){
		searchBanksRegisterTO = new SearchBanksTO();
		searchBanksRegisterTO.setLstBankOrigin(new ArrayList<ParameterTable>());
		searchBanksRegisterTO.setLstBankType(new ArrayList<ParameterTable>());
		searchBanksRegisterTO.setLstDocumentType(new ArrayList<ParameterTable>());
		searchBanksRegisterTO.setLstCountry(new ArrayList<GeographicLocation>());
		searchBanksRegisterTO.setLstDepartment(new ArrayList<GeographicLocation>());
		searchBanksRegisterTO.setLstProvince(new ArrayList<GeographicLocation>());
		searchBanksRegisterTO.setLstDistrict(new ArrayList<GeographicLocation>());
	}
	
	/**
	 * Create Object Bank Session.
	 */
	private void createObjectBankSession(){
		bankSession = new Bank();
		bankSession.setCountry(new GeographicLocation());
		bankSession.setDepartment(new GeographicLocation());
		bankSession.setProvince(new GeographicLocation());
		bankSession.setDistrict(new GeographicLocation());
	}
	/**
	 * Change selected document type register.
	 */
	public void changeSelectedDocumentTypeRegister() { 
		bankSession.setDocumentNumber(null);
		if(Validations.validateIsNotNullAndPositive(bankSession.getDocumentType()))
			documentTORegister=documentValidatorRegister.loadMaxLenghtType(new Integer(bankSession.getDocumentType().toString()));
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())
				||getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
			JSFUtilities.resetComponent(":frmRegBank:txtDocumentNumber");
			JSFUtilities.resetComponent(":frmRegBank:msgTxtDocumentNumber");
		}		
	}
	/**
	 * Change selected document type search.
	 */
	public void changeSelectedDocumentTypeSearch() {
		cleanDataModel();
		bankSession.setDocumentNumber(null);
		if(Validations.validateIsNotNullAndPositive(searchBanksTO.getDocumentType()))
			documentTORegister=documentValidatorRegister.loadMaxLenghtType(new Integer(searchBanksTO.getDocumentType().toString()));
	}
	
	/**
	 * Before Modify Action.
	 *
	 * @return the string
	 */
	public String beforeModifyAction(){
		Bank bank = this.getBankSession(); 
		executeAction();
		if(bank==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(bank.getState().equals(BankStateType.REGISTERED.getCode())){			
			selectBankHandler(bank);
			this.setViewOperationType(ViewOperationsType.MODIFY.getCode());
			bankSession.setLastModifyDate(CommonsUtilities.currentDateTime());
			return "registerBank";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	
	/**
	 * before Block Action.
	 *
	 * @return the string
	 */
	public String beforeBlockAction(){
		Bank bank = this.getBankSession(); 
		executeAction();
		if(bank==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(bank.getState().equals(BankStateType.REGISTERED.getCode())){			
			selectBankHandler(bank);
			this.setViewOperationType(ViewOperationsType.BLOCK.getCode());
			return "viewBank";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	
	/**
	 * before Unblock Action.
	 *
	 * @return the string
	 */
	public String beforeUnblockAction(){
		Bank bank = this.getBankSession(); 
		executeAction();
		if(bank==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(bank.getState().equals(BankStateType.BLOCKED.getCode())){			
			selectBankHandler(bank);
			this.setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
			return "viewBank";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_VALIDATION_BLOCKED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	/**
	 * Change action currency settlement request handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		switch(ViewOperationsType.lookup.get(getViewOperationType())){
			case BLOCK : blockBank();break;
			case UNBLOCK:unblockBank();break;			
		}
		cleanBankSearchHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());	
	}
	
	/**
	 * block Bank.
	 */
	@LoggerAuditWeb
	private void blockBank(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		bankSession.setBlockMotive(Integer.parseInt(""+motiveControllerBlock.getIdMotivePK()));
		bankSession.setBlockOtherMotive(motiveControllerBlock.getMotiveText());
		Bank bank = null;
		try {
			bank = manageBanksFacade.blockBankFacade(bankSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(bank!=null){
			
			//Sending notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_BLOCK.getCode());
				Object[] parameters = new Object[]{bank.getIdBankPk().toString()};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,bank.getIdBankPk(), parameters);
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_BLOCK_OK,
				    		  new Object[]{bank.getIdBankPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveBlockHandler(){
		if(this.getMotiveControllerBlock().getIdMotivePK()==Long.parseLong(BankMotiveBlockType.OTHER.getCode().toString())){
			this.getMotiveControllerBlock().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerBlock().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveUnBlockHandler(){
		if(this.getMotiveControllerUnBlock().getIdMotivePK()==Long.parseLong(BankMotiveUnBlockType.OTHER.getCode().toString())){
			this.getMotiveControllerUnBlock().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerUnBlock().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveBlockHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerBlock.getIdMotivePK()==Long.parseLong(BankMotiveBlockType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerBlock().getMotiveText())){	
				setViewOperationType(ViewOperationsType.BLOCK.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
			      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_BLOCK_CONFIRM,
			    		  new Object[]{this.getBankSession().getIdBankPk()}) );	
		setViewOperationType(ViewOperationsType.BLOCK.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveUnBlockHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerUnBlock.getIdMotivePK()==Long.parseLong(BankMotiveUnBlockType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerUnBlock().getMotiveText())){	
				setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
			      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_UNBLOCK_CONFIRM,
			    		  new Object[]{this.getBankSession().getIdBankPk()}) );	
		setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	
	/**
	 * unblock Bank.
	 */
	@LoggerAuditWeb
	private void unblockBank(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		bankSession.setUnblockMotive(Integer.parseInt(""+motiveControllerUnBlock.getIdMotivePK()));
		bankSession.setUnblockOtherMotive(motiveControllerUnBlock.getMotiveText());
		Bank bank = null;
		try {
			bank = manageBanksFacade.unblockBankFacade(bankSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(bank!=null){
			
			//Sending notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_UNBLOCK.getCode());
				Object[] parameters = new Object[]{bank.getIdBankPk().toString()};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,bank.getIdBankPk(), parameters);
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_UNBLOCK_OK,
				    		  new Object[]{bank.getIdBankPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	
	/**
	 * Select Bank Handler.
	 *
	 * @param bank object bank
	 */
	public void selectBankHandler(Bank bank){
		executeAction();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		createObjectRegister();
		try {
			loadBankType();
			loadBankOrigin();
			loadDocumentType();
			bankSession = manageBanksFacade.getBankForId(bank.getIdBankPk());
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			if(bankSession.getBankOrigin().equals(BankOriginType.NATIONAL.getCode()))
				loadCountries(BooleanType.YES.getCode());
			else
				loadCountries(BooleanType.NO.getCode());			
			changeSelectedCountry();
			loadProvinceDepartment();
			loadDistrictProvince();
			if(Validations.validateIsNull(bankSession.getDistrict()))
				bankSession.setDistrict(new GeographicLocation());
		} catch (ServiceException e) {
			e.printStackTrace();
		}			
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel(){
		searchBankTODataModel=null;
	}
	
	/**
	 * Get User Information.
	 *
	 * @return User Information
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Set User Information.
	 *
	 * @param userInfo User Information
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Get Bank Session.
	 *
	 * @return Bank Session
	 */
	public Bank getBankSession() {
		return bankSession;
	}
	
	/**
	 * Set Bank Session.
	 *
	 * @param bankSession Bank Session
	 */
	public void setBankSession(Bank bankSession) {
		this.bankSession = bankSession;
	}
	
	/**
	 * Get Search Banks Register TO.
	 *
	 * @return Search Banks Register TO
	 */
	public SearchBanksTO getSearchBanksRegisterTO() {
		return searchBanksRegisterTO;
	}
	
	/**
	 * Set Search Banks Register TO.
	 *
	 * @param searchBanksRegisterTO Search Banks Register TO
	 */
	public void setSearchBanksRegisterTO(SearchBanksTO searchBanksRegisterTO) {
		this.searchBanksRegisterTO = searchBanksRegisterTO;
	}
	
	/**
	 * Get Document TO Register.
	 *
	 * @return Document TO Register
	 */
	public DocumentTO getDocumentTORegister() {
		return documentTORegister;
	}
	
	/**
	 * Set Document TO Register.
	 *
	 * @param documentTORegister Document TO Register
	 */
	public void setDocumentTORegister(DocumentTO documentTORegister) {
		this.documentTORegister = documentTORegister;
	}
	
	/**
	 * Get Document Validator Register.
	 *
	 * @return  Document Validator Register
	 */
	public DocumentValidator getDocumentValidatorRegister() {
		return documentValidatorRegister;
	}
	
	/**
	 * Set  Document Validator Register.
	 *
	 * @param documentValidatorRegister  Document Validator Register
	 */
	public void setDocumentValidatorRegister(
			DocumentValidator documentValidatorRegister) {
		this.documentValidatorRegister = documentValidatorRegister;
	}
	
	/**
	 * Get Search Banks TO.
	 *
	 * @return Search Banks TO
	 */
	public SearchBanksTO getSearchBanksTO() {
		return searchBanksTO;
	}
	
	/**
	 * Set Search Banks TO.
	 *
	 * @param searchBanksTO Search Banks TO
	 */
	public void setSearchBanksTO(SearchBanksTO searchBanksTO) {
		this.searchBanksTO = searchBanksTO;
	}
	
	/**
	 * Get Search Bank Data Model.
	 *
	 * @return Search Bank Data Model
	 */
	public GenericDataModel<Bank> getSearchBankTODataModel() {
		return searchBankTODataModel;
	}
	
	/**
	 * Set Search Bank Data Model.
	 *
	 * @param searchBankTODataModel Search Bank Data Model
	 */
	public void setSearchBankTODataModel(
			GenericDataModel<Bank> searchBankTODataModel) {
		this.searchBankTODataModel = searchBankTODataModel;
	}
	
	/**
	 * Get Motive Controller Block.
	 *
	 * @return Motive Controller Block
	 */
	public MotiveController getMotiveControllerBlock() {
		return motiveControllerBlock;
	}
	
	/**
	 * Set Motive Controller Block.
	 *
	 * @param motiveControllerBlock Motive Controller Block
	 */
	public void setMotiveControllerBlock(MotiveController motiveControllerBlock) {
		this.motiveControllerBlock = motiveControllerBlock;
	}
	
	/**
	 * Get Motive Controller UnBlock.
	 *
	 * @return Motive Controller UnBlock
	 */
	public MotiveController getMotiveControllerUnBlock() {
		return motiveControllerUnBlock;
	}
	
	/**
	 * Set Motive Controller UnBlock.
	 *
	 * @param motiveControllerUnBlock Motive Controller UnBlock
	 */
	public void setMotiveControllerUnBlock(MotiveController motiveControllerUnBlock) {
		this.motiveControllerUnBlock = motiveControllerUnBlock;
	}
	
}
