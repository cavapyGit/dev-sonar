package com.pradera.funds.fundoperations.monitoring.facade;

import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.effective.accounts.service.InstitutionCashAccountService;
import com.pradera.funds.fundoperations.monitoring.to.MonitoringFundOperationsTO;
import com.pradera.funds.fundoperations.util.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.constant.FundsConstants;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountStateType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BeginEndDaySituationType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsRetirementType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;

// TODO: Auto-generated Javadoc
/**
 * The Class InstitutionCashAccountServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class MonitoringFundOperationsServiceFacade {
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The param service bean. */
	@EJB
	private ParameterServiceBean paramServiceBean;
	
	/** The manage cash account service. */
	@EJB
	private InstitutionCashAccountService manageCashAccountService;
	

    /** The executor component service bean. */
    @Inject
    Instance<FundsComponentService> executorComponentServiceBean;
	
	/** The automatic reception service. */
	@EJB
	AutomaticReceptionFundsService automaticReceptionService;
	
	/** The holiday service. */
	@Inject
	private HolidayQueryServiceBean holidayService;
	
	/** The cash account service. */
	@EJB
	CashAccountManagementService cashAccountService;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	/**
	 * Find.
	 *
	 * @param <T> the generic type
	 * @param type the type
	 * @param id the id
	 * @return the t
	 */
	public <T> T find(Class<T> type, Object id) {
		return (T) manageCashAccountService.find(type, id);
	}
	
	/**
	 * Creates the.
	 *
	 * @param <T> the generic type
	 * @param object the object
	 * @return the t
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public <T> T create(Object object) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		return (T) manageCashAccountService.create(object);
	}
	
	/**
	 * Update.
	 *
	 * @param <T> the generic type
	 * @param object the object
	 * @return the t
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public <T> T update(Object object) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return (T) manageCashAccountService.update(object,loggerUser);
	}
	
	
	/**
	 * Begin day.
	 *
	 * @param beginEndDay the begin end day
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BEGIN_DAY_MANUAL)
	public void beginDay(BeginEndDay beginEndDay) throws ServiceException{
		//Setting the Begin Day
		beginEndDay.setRegistryDate(CommonsUtilities.currentDate());
		beginEndDay.setIndSituation(BeginEndDaySituationType.OPEN.getCode());
		beginEndDay.setOpenDay(CommonsUtilities.currentDateTime());
		create(beginEndDay);
		//WE UPDATE THE SITUATION OF ALL THE CASH ACCOUNTS IN ACTIVATE STATE
		manageCashAccountService.updateInstitutionCashAccountSituation(BeginEndDaySituationType.OPEN.getCode());
		//UPDATE THE SITUATION OF CENTRALIZER CASH ACCONT TO OPEN
		manageCashAccountService.updateCentralizerCashAccountSituation(BeginEndDaySituationType.OPEN.getCode());
	}
	/**
	 * Gets the parameter list.
	 *
	 * @param type the type
	 * @return the parameter list
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterList(MasterTableType type) throws ServiceException {
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setMasterTableFk(type.getCode());
		parameterFilter.setState(ParameterTableStateType.REGISTERED.getCode());
		if(type.equals(MasterTableType.CURRENCY))
			parameterFilter.setIndicator2(FundsConstants.CURRENCY_FUNDS_INDICATOR_ON);
		List<ParameterTable> parameterList = new ArrayList<ParameterTable>();	
		parameterList = this.paramServiceBean.getListParameterTableServiceBean(parameterFilter);	
		return parameterList;
	}
	
	/**
	 * Search last begin end day.
	 *
	 * @param date the date
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public BeginEndDay searchBeginEnd(Date date) throws ServiceException{
		return manageCashAccountService.searchBeginEnd(date);
	}
	
	/**
	 * Search institution cash accounts.
	 *
	 * @param objMonitoringFundOperationsTO the obj monitoring fund operations to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BEGIN_END_DAY_MONITORING)
	public List<InstitutionCashAccount> searchInstitutionCashAccounts(MonitoringFundOperationsTO objMonitoringFundOperationsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<InstitutionCashAccount> lstResult = new ArrayList<InstitutionCashAccount>();
		//Setting search data from view
		InstitutionCashAccount objInstitutionCashAccount=new InstitutionCashAccount();
		objInstitutionCashAccount.setAccountType(objMonitoringFundOperationsTO.getAccountType().intValue());
		if(objMonitoringFundOperationsTO.getCurrency()!=null && !objMonitoringFundOperationsTO.getCurrency().equals(GeneralConstants.ZERO_VALUE_LONG))
		{
			objInstitutionCashAccount.setCurrency(objMonitoringFundOperationsTO.getCurrency().intValue());
		}
		if(objMonitoringFundOperationsTO.getParticipantSelected()!=null && !objMonitoringFundOperationsTO.getParticipantSelected().equals(GeneralConstants.ZERO_VALUE_LONG))
		{
			Participant objParticipant=new Participant();
			objParticipant.setIdParticipantPk(objMonitoringFundOperationsTO.getParticipantSelected());
			objInstitutionCashAccount.setParticipant(objParticipant);
		}
		if(Validations.validateIsNotNull(objMonitoringFundOperationsTO.getIssuer()) &&
				Validations.validateIsNotNull(objMonitoringFundOperationsTO.getIssuer().getIdIssuerPk()) && 
					!objMonitoringFundOperationsTO.getIssuer().getIdIssuerPk().equals(GeneralConstants.EMPTY_STRING))
		{
			Issuer objIssuer=new Issuer();
			objIssuer.setIdIssuerPk(objMonitoringFundOperationsTO.getIssuer().getIdIssuerPk());
			objInstitutionCashAccount.setIssuer(objIssuer);
		} 
		
		
		List<Object[]> lstCashAccounts = manageCashAccountService.searchInstitutionCashAccounts(objInstitutionCashAccount);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccounts)){
			for(Object[] obj: lstCashAccounts){
				InstitutionCashAccount cashAccount = new InstitutionCashAccount();
				cashAccount.setAccountType(Integer.parseInt(obj[7].toString()));
				cashAccount.setAccountTypeDescription(obj[0].toString());
				cashAccount.setCurrencyTypeDescription(obj[1].toString());				
				cashAccount.setTotalAmount(new BigDecimal(obj[2].toString()));
				cashAccount.setAvailableAmount(new BigDecimal(obj[3].toString()));
				cashAccount.setRetirementAmount(new BigDecimal(obj[5].toString()));
				cashAccount.setDepositAmount(new BigDecimal(obj[4].toString()));
				if(BeginEndDaySituationType.OPEN.getCode().equals(Integer.valueOf(obj[6].toString()))){
					cashAccount.setSituationDescription(BeginEndDaySituationType.OPEN.getValue());
				}else if(BeginEndDaySituationType.CLOSE.getCode().equals(Integer.valueOf(obj[6].toString()))){
					cashAccount.setSituationDescription(BeginEndDaySituationType.CLOSE.getValue());
				}
				cashAccount.setCurrency(Integer.valueOf(obj[8].toString()));
				Integer indCentralizer = Integer.valueOf(obj[9].toString());
				if(BooleanType.YES.getCode().equals(indCentralizer)){
					cashAccount.setIndCentralizer(BooleanType.YES.getValue());
				}else {
					cashAccount.setIndCentralizer(BooleanType.NO.getValue());
				}
				lstResult.add(cashAccount);
			}
		}
		return lstResult;
	}
	
	/**
	 * Search bank description.
	 *
	 * @param idInstitutionCashAccount the id institution cash account
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String searchBankDescription(Long idInstitutionCashAccount) throws ServiceException{
		return manageCashAccountService.searchBankDescription(idInstitutionCashAccount);
	}
	
	/**
	 * Search efective accounts.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> searchEfectiveAccounts()  throws ServiceException{
		return manageCashAccountService.searchEfectiveAccounts();
	}
	
	/**
	 * Search registered funds operations.
	 *
	 * @param processDay the process day
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperation> searchRegisteredFundsOperations(Date processDay) throws ServiceException{
		return manageCashAccountService.searchRegisteredFundsOperations(processDay);
	}

	/**
	 * Gets the observed fund operations.
	 *
	 * @param processDay the process day
	 * @return the observed fund operations
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperation> getObservedFundOperations(Date processDay) throws ServiceException{
		return manageCashAccountService.getObservedFundOperations(processDay);
	}

	/**
	 * Gets the monitoring cash account detail.
	 *
	 * @param accountType the account type
	 * @param currency the currency
	 * @param participantPk the participant pk
	 * @param issuer the issuer
	 * @return the monitoring cash account detail
	 */
	public List<InstitutionCashAccount> getMonitoringCashAccountDetail(Integer accountType,Integer currency,Long participantPk,Issuer issuer){
		List<InstitutionCashAccount> lstReturnDetail = new ArrayList<InstitutionCashAccount>();

		if(AccountCashFundsType.SETTLEMENT.getCode().equals(accountType)){
			List<Object[]> lstAccounts = manageCashAccountService.settlementCashAccountMonitoring(currency,participantPk,issuer);
			if(Validations.validateListIsNotNullAndNotEmpty(lstAccounts)){
				for(Object[] obj : lstAccounts){
					InstitutionCashAccount cashAccount = new InstitutionCashAccount();
					cashAccount.setParticipant(new Participant());
					cashAccount.getParticipant().setIdParticipantPk(Long.valueOf(obj[0].toString()));
					cashAccount.getParticipant().setDescription(obj[1].toString());
					if(SettlementSchemaType.GROSS.getCode().equals(Integer.valueOf(obj[2].toString()))){
						cashAccount.setSettlementSchemaDescription(SettlementSchemaType.GROSS.getValue());
					}else if(SettlementSchemaType.NET.getCode().equals(Integer.valueOf(obj[2].toString()))){
						cashAccount.setSettlementSchemaDescription(SettlementSchemaType.NET.getValue());
					}
					cashAccount.setNegotiationMechanism(new NegotiationMechanism());
					cashAccount.getNegotiationMechanism().setDescription(obj[3].toString());
					cashAccount.setModalityGroup(new ModalityGroup());
					cashAccount.getModalityGroup().setDescription(obj[4].toString());
					cashAccount.setTotalAmount(new BigDecimal(obj[5].toString()));
					lstReturnDetail.add(cashAccount);
				}
			}
		}else if(AccountCashFundsType.BENEFIT.getCode().equals(accountType)){
			List<Object[]> lstAccounts = manageCashAccountService.benefitCashAccountMonitoring(currency, issuer);
			if(Validations.validateListIsNotNullAndNotEmpty(lstAccounts)){
				for(Object[] obj : lstAccounts){
					InstitutionCashAccount cashAccount = new InstitutionCashAccount();
					cashAccount.setIssuer(new Issuer());
					cashAccount.getIssuer().setIdIssuerPk(obj[0].toString());
					cashAccount.getIssuer().setDescription(obj[1].toString());
					cashAccount.getIssuer().setMnemonic(obj[7].toString());
					cashAccount.setBankDescription(obj[2]!=null ?obj[2].toString():"");
					cashAccount.setTotalAmount(new BigDecimal(obj[3].toString()));
					cashAccount.setIdInstitutionCashAccountPk(new Long(obj[4].toString()));					
					cashAccount.setAccountNumber(obj[5]!=null?obj[5].toString():"");
					cashAccount.setAvailableAmount(new BigDecimal(obj[6].toString()));
					lstReturnDetail.add(cashAccount);
				}
			}
		}else{
			List<Object[]> lstAccounts = manageCashAccountService.otherCashAccountMonitoring(accountType,currency);
			if(Validations.validateListIsNotNullAndNotEmpty(lstAccounts)){
				for(Object[] obj : lstAccounts){
					InstitutionCashAccount cashAccount = new InstitutionCashAccount();
					cashAccount.setParticipant(new Participant());
					cashAccount.getParticipant().setIdParticipantPk(Long.valueOf(obj[0].toString()));
					cashAccount.getParticipant().setDescription(obj[1].toString());
					cashAccount.setBankDescription(obj[2].toString());
					cashAccount.setTotalAmount(new BigDecimal(obj[3].toString()));
					cashAccount.setAccountNumber(obj[4].toString());
					cashAccount.setIdInstitutionCashAccountPk(new Long(obj[5].toString()));					
					lstReturnDetail.add(cashAccount);
				}
			}
		}
		return lstReturnDetail;
	}
	
	/**
	 * Validate close day.
	 *
	 * @param processDay the process day
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateCloseDay(Date processDay) throws ServiceException
	{
		String strMessage= null;
		BeginEndDay beginEndDay = searchBeginEnd(processDay);
		if(Validations.validateIsNotNull(beginEndDay)){
			if(BeginEndDaySituationType.OPEN.getCode().equals(beginEndDay.getIndSituation())){
				List<FundsOperation> lstFundsOperation = searchRegisteredFundsOperations(processDay);
				//WE VALIDATE IF EXIST OPERATIONS NOT CONFIRMED OR REJECTED
				if(Validations.validateListIsNotNullAndNotEmpty(lstFundsOperation)){
					strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.FUNDS_OPERATION_REGISTERED_ERROR);
				}else{
					//VALIDATE IF EXIST FUND OPERATIONS IN OBSERVED STATE
					List<FundsOperation> lstObservedFundOperations = getObservedFundOperations(processDay);
					if(Validations.validateListIsNullOrEmpty(lstObservedFundOperations)){
						//Validating if efective accounts balances are greater than 0
						String strAccountBalanceMessage=searchEfectiveAccountsBalance();
						if(strAccountBalanceMessage!=null)
							return strAccountBalanceMessage;
					}else{
						strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.FUNDS_OPERATION_OBSERVED_STATE);
					}
				}
			}else{
				//IF THE DAY IS CLOSED, WE CANNOT DO IT AGAIN
				strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.FUNDS_CASH_ACCOUNT_IS_NOT_OPENED_TO_CLOSE);
			}
		}else{
			strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.FUNDS_OPERATIONS_NOT_OPENED_TO_END);
		}
		return strMessage;
	}
	
	
	/**
	 * Search efective accounts balance.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String searchEfectiveAccountsBalance() throws ServiceException{
		String strMessage= null;
			//Searching efectives accounts
			List<InstitutionCashAccount> institutionCashEfectiveAccountsWithoutBalances = searchEfectiveAccounts();			
			for (InstitutionCashAccount institutionCashAccountTmp : institutionCashEfectiveAccountsWithoutBalances) {
				if(institutionCashAccountTmp.getTotalAmount().compareTo(new BigDecimal(0)) > 0){
					strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.FUNDS_OPERATIONS_CONFIRM_EXISTAMOUNT_RELATEDCENTRAL);
					break;
				}
			}
		return strMessage;
	}
	
	
	/**
	 * Metodo para enviar el proceso batch de Precio Curva
	 */
	@LoggerAuditWeb
	public void registerUnblockBalanceSelidBatch(){
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());           
		
		Schedulebatch scheduleBatch = new Schedulebatch();
        scheduleBatch.setIdBusinessProcess(Long.valueOf(40866));
        scheduleBatch.setUserName(loggerUser.getUserName());
        scheduleBatch.setPrivilegeCode(loggerUser.getUserAction().getIdPrivilegeRegister());
        scheduleBatch.setParameters(new HashMap<String, String>());
    	clientRestService.excuteRemoteBatchProcess(scheduleBatch,"iSecurities");

	}
	
	/**
	 * End day.
	 *
	 * @param processDay the process day
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CLOSE_DAY_MANUAL)
	public void endDay(Date processDay) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());           
        
        /**Validate marketfact in reporting operation*/
        List<Object> operationInconsist = cashAccountService.getReportingInconsist();
        if(operationInconsist!=null && !operationInconsist.isEmpty()){
        	throw new ServiceException(ErrorServiceType.BEGIN_END_DAY_REPORTING_MARKETFACT,new Object[]{StringUtils.join(operationInconsist,",")});
        }
        
		//WE UPDATE THE SITUATION OF THE BEGIN END DAY PROCESS
		BeginEndDay endDayProcess = manageCashAccountService.searchBeginEnd(processDay);
		endDayProcess.setIndSituation(BeginEndDaySituationType.CLOSE.getCode());
		endDayProcess.setCloseDay(CommonsUtilities.currentDateTime());
		manageCashAccountService.update(endDayProcess);
		//WE UPDATE THE SITUATION OF ALL THE CASH ACCOUNTS IN CLOSED STATE
		manageCashAccountService.updateInstitutionCashAccountSituation(BeginEndDaySituationType.CLOSE.getCode());
		//UPDATE THE SITUATION OF CENTRALIZER CASH ACCONT TO CLOSED
		manageCashAccountService.updateCentralizerCashAccountSituation(BeginEndDaySituationType.CLOSE.getCode());
		//WE GENERATE THE WITHDRAWL FOR SETTLEMENTS GROUPED BY PARTICIPANT ACCORDING TO SETTLEMENT TYPE
		withdrawlSettlementFunds(processDay);
		 //Validate if exist operations w/o settlement process closed
        if(Validations.validateListIsNotNullAndNotEmpty(manageCashAccountService.getOperationsWOCloseSettlementProcess(processDay, ComponentConstant.CASH_PART))
        		|| Validations.validateListIsNotNullAndNotEmpty(manageCashAccountService.getOperationsWOCloseSettlementProcess(processDay, ComponentConstant.TERM_PART))){
        	throw new ServiceException(ErrorServiceType.BEGIN_END_DAY_PROCESS_END_ERROR);
        }    
		if(manageCashAccountService.getCashAccountAmount().compareTo(BigDecimal.ZERO) == 1
				|| manageCashAccountService.getCentralizerAccountAmount().compareTo(BigDecimal.ZERO) == 1){
			throw new ServiceException(ErrorServiceType.BEGIN_END_DAY_EXIST_AMOUNT_ERROR);
		}
	}
	
	
	/**
	 * End day automatic.
	 *
	 * @param processDay the process day
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CLOSE_DAY_AUTOMATIC)
	public void endDayAutomatic(Date processDay) throws ServiceException{
		String strMessage=null;
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());           
		//WE UPDATE THE SITUATION OF THE BEGIN END DAY PROCESS
		BeginEndDay endDayProcess = manageCashAccountService.searchBeginEnd(processDay);
		endDayProcess.setIndSituation(BeginEndDaySituationType.CLOSE.getCode());
		endDayProcess.setCloseDay(CommonsUtilities.currentDateTime());
		manageCashAccountService.update(endDayProcess);
		//WE UPDATE THE SITUATION OF ALL THE CASH ACCOUNTS IN CLOSED STATE
		manageCashAccountService.updateInstitutionCashAccountSituation(BeginEndDaySituationType.CLOSE.getCode());
		//UPDATE THE SITUATION OF CENTRALIZER CASH ACCONT TO CLOSED
		manageCashAccountService.updateCentralizerCashAccountSituation(BeginEndDaySituationType.CLOSE.getCode());
		//WE GENERATE THE WITHDRAWL FOR SETTLEMENTS GROUPED BY PARTICIPANT ACCORDING TO SETTLEMENT TYPE
		withdrawlSettlementFunds(processDay);
		 //Validate if exist operations w/o settlement process closed
        if(Validations.validateListIsNotNullAndNotEmpty(manageCashAccountService.getOperationsWOCloseSettlementProcess(processDay, ComponentConstant.CASH_PART))
        		|| Validations.validateListIsNotNullAndNotEmpty(manageCashAccountService.getOperationsWOCloseSettlementProcess(processDay, ComponentConstant.TERM_PART))){
        	strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), ErrorServiceType.BEGIN_END_DAY_PROCESS_END_ERROR);
        }    
		if(manageCashAccountService.getCashAccountAmount().compareTo(BigDecimal.ZERO) == 1
				|| manageCashAccountService.getCentralizerAccountAmount().compareTo(BigDecimal.ZERO) == 1){
			strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), ErrorServiceType.BEGIN_END_DAY_EXIST_AMOUNT_ERROR);
		}
		
		if(strMessage!=null)
		{
			// Start Notification Error Manual
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CLOSE_DAY_AUTOMATIC.getCode());
			UserFilterTO  userFilter=new UserFilterTO();
			userFilter.setUserName(loggerUser.getUserName());
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);
			/*notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcessNotification, lstUserAccount,
						GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.MESSAGE);*/
			notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcessNotification, lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.EMAIL);
		//	 End Notification
		}
		else
		{
			// Start Sucess Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CLOSE_DAY_AUTOMATIC.getCode());
			Object[] parameters = new Object[]{CommonsUtilities.convertDatetoString(processDay)};
			notificationServiceFacade.sendNotification(loggerUser.getUserName(),
					businessProcessNotification,CommonsUtilities.convertDatetoString(processDay), parameters);
			// End Sucess Notification
		}
	}

	/**
	 * METHOD WHICH WILL GENERATE THE WITHDRAWL BY PARTICIPANT - SETTLEMENT TYPE.
	 *
	 * @param processDay the process day
	 * @throws ServiceException the service exception
	 */
	private void withdrawlSettlementFunds(Date processDay) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		String userName = loggerUser.getUserName();
		//GET UNFULFILL CASH OPERATIONS
		List<ParticipantSettlement> unfulfillCashOperations = manageCashAccountService.getWithdrawlUnfulfillOperations(processDay, ComponentConstant.CASH_PART);
		//GET UNFULFILL TERM OPERATIONS
		List<ParticipantSettlement> unfulfillTermOperations = manageCashAccountService.getWithdrawlUnfulfillOperations(processDay, ComponentConstant.TERM_PART);
		//GET CASH SETTLED OPERATIONS WITH EXCEDENT AMOUNT
		List<ParticipantSettlement> cashSettledOperations = manageCashAccountService.getWithdrawlSettledOperations(processDay, ComponentConstant.CASH_PART);
		//GET TERM SETTLED OPERATIONS WITH EXCEDENT AMOUNT
		List<ParticipantSettlement> termSettledOperations = manageCashAccountService.getWithdrawlSettledOperations(processDay, ComponentConstant.TERM_PART);
		//GET SETTLEMENTS ACCOUNT FOR NET SCHEMA WITH AVAILABLE BALANCE
		InstitutionCashAccountTO filterAux = new InstitutionCashAccountTO();
		filterAux.setAccountType(AccountCashFundsType.SETTLEMENT.getCode());
		filterAux.setSettlementSchema(SettlementSchemaType.NET.getCode());
		filterAux.setAccountState(CashAccountStateType.ACTIVATE.getCode());
		filterAux.setBankState(BankAccountStateType.ACTIVATE.getCode());
		List<InstitutionCashAccount> lstCashAccount = manageCashAccountService.getWidthdrawlCashAccounts(filterAux);
		
		//GENERATE THE WITHDRAWL FOR INSTITUTION CASH ACCOUNT WITH AVAILABLE BALANCE
		if(Validations.validateIsNotNullAndNotEmpty(lstCashAccount)){
			validateBankAccountWithdraw(lstCashAccount);
			for(InstitutionCashAccount instCashAccount: lstCashAccount){
				withdrawlOperations(instCashAccount,userName);
			}
		}

		//GENERATE THE WITHDRAWL FOR UNFULFILL CASH OPERATION
		if(Validations.validateListIsNotNullAndNotEmpty(unfulfillCashOperations)){
			for(ParticipantSettlement unfulfillCashOperation : unfulfillCashOperations){
				unfulfillCashOperation.setIndPayedBack(BooleanType.YES.getCode());
				unfulfillCashOperation.setFundsReference(null);
				unfulfillCashOperation.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk();
				unfulfillCashOperation.getSettlementOperation().setFundsReference(null);
				manageCashAccountService.update(unfulfillCashOperation);

				withdrawlOperations(unfulfillCashOperation,true,userName);
			}
		}

		//GENERATE THE WITHDRAWL FOR UNFULFILL TERM OPERATION
		if(Validations.validateListIsNotNullAndNotEmpty(unfulfillTermOperations)){
			for(ParticipantSettlement unfulfillTermOperation : unfulfillTermOperations){
				unfulfillTermOperation.setIndPayedBack(BooleanType.YES.getCode());
				unfulfillTermOperation.setFundsReference(null);
				unfulfillTermOperation.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk();
				unfulfillTermOperation.getSettlementOperation().setFundsReference(null);
				manageCashAccountService.update(unfulfillTermOperation);

				withdrawlOperations(unfulfillTermOperation,true,userName);
			}
		}

		//GENERATE THE WITHDRAWL FOR CASH SETTLED OPERATIONS WITH EXCEDENT
		if(Validations.validateListIsNotNullAndNotEmpty(cashSettledOperations)){
			for(ParticipantSettlement cashSettledOperation : cashSettledOperations){
				cashSettledOperation.setIndPayedBack(BooleanType.YES.getCode());
				manageCashAccountService.update(cashSettledOperation);

				withdrawlOperations(cashSettledOperation,false,userName);
			}
		}

		//GENERATE THE WITHDRAWL FOR TERM SETTLED OPERATIONS WITH EXCEDENT
		if(Validations.validateListIsNotNullAndNotEmpty(termSettledOperations)){
			for(ParticipantSettlement termSettledOperation : termSettledOperations){
				termSettledOperation.setIndPayedBack(BooleanType.YES.getCode());
				manageCashAccountService.update(termSettledOperation);

				withdrawlOperations(termSettledOperation,false,userName);
			}
		}
	}
	
	/**
	 * Validate bank account withdraw.
	 *
	 * @param lstCashAccount the lst cash account
	 * @throws ServiceException the service exception
	 */
	private void validateBankAccountWithdraw(List<InstitutionCashAccount> lstCashAccount) throws ServiceException
	{
		for(InstitutionCashAccount instCashAccount: lstCashAccount){
			List<CashAccountDetail> lstCashAccountDetail=instCashAccount.getCashAccountDetailResults();
			if(lstCashAccountDetail!=null && lstCashAccountDetail.size()>0 )
			{
				for(CashAccountDetail objCashAccountDetail: lstCashAccountDetail){
					InstitutionBankAccount objInstitutionBankAccount=objCashAccountDetail.getInstitutionBankAccount();
					if(objInstitutionBankAccount==null)
					{
						ParameterTable objParameter= paramServiceBean.getParameterDetail(instCashAccount.getCurrency());
						Object[] parameters = new Object[]{instCashAccount.getAccountTypeDescription(),objParameter.getParameterName()};
						throw new ServiceException(ErrorServiceType.BEGIN_END_DAY_NOT_EXIST_BANK_ACCOUNT,parameters);
					}
					else
					{
						if(!objInstitutionBankAccount.getState().equals(BankAccountsStateType.REGISTERED.getCode().intValue()))
						{
							ParameterTable objParameter= paramServiceBean.getParameterDetail(objInstitutionBankAccount.getCurrency());
							Object[] parameters = new Object[]{instCashAccount.getAccountTypeDescription(),objParameter.getParameterName()};
							throw new ServiceException(ErrorServiceType.BEGIN_END_DAY_NOT_EXIST_BANK_ACCOUNT,parameters);
						}
					}
				}
			}
			else
			{
				ParameterTable objParameter= paramServiceBean.getParameterDetail(instCashAccount.getCurrency());
				Object[] parameters = new Object[]{instCashAccount.getAccountTypeDescription(),objParameter.getParameterName()};
				throw new ServiceException(ErrorServiceType.BEGIN_END_DAY_NOT_EXIST_BANK_ACCOUNT,parameters);
			}
				
		}
	}

	/**
	 * METHOD WHICH WILL GENERATE FUNDS WITHDRAWL BY PARTICIPANT OPERATION.
	 *
	 * @param unfulfillOperation the unfulfill operation
	 * @param blUnfulfill the bl unfulfill
	 * @param userName the user name
	 * @throws ServiceException the service exception
	 */
	private void withdrawlOperations(ParticipantSettlement unfulfillOperation,boolean blUnfulfill,String userName) throws ServiceException{
		//REGISTER FUND CENTRAL WITHDRAWL OPERATIONS
		//FundsOperation centralFundOperation = saveWithdrawlFundOperation(unfulfillOperation,null,true,blUnfulfill,userName);
		//EXECUTE CENTRAL FUNDS COMPONENT
		//executeFundComponent(centralFundOperation);
		//REGISTER FUND WITHDRAWL OPERATIONS
		FundsOperation fundOperation = new FundsOperation();
		//fundOperation.setFundsOperation(centralFundOperation);
		fundOperation = saveWithdrawlFundOperation(unfulfillOperation,null,false,blUnfulfill,userName);
		//EXECUTE FUNDS COMPONENT
		executeFundComponent(fundOperation);
		//EXECUTE SWIFT COMPONENT
		//swiftWithdrawl.executeSwiftWithdrawl(fundOperation,null,null);
	}
	
	/**
	 * METHOD WHICH WILL GENERATE FUNDS WITHDRAWL BY INSTITUTION_CASH_ACCOUNT.
	 *
	 * @param institutionCashAccount the institution cash account
	 * @param userName the user name
	 * @throws ServiceException the service exception
	 */
	private void withdrawlOperations(InstitutionCashAccount institutionCashAccount, String userName) throws ServiceException{
		//REGISTER FUND CENTRAL WITHDRAWL OPERATIONS
		//FundsOperation centralFundOperation = saveWithdrawlFundOperation(institutionCashAccount,null,true,userName);
		//EXECUTE CENTRAL FUNDS COMPONENT
		//executeFundComponent(centralFundOperation);
		//REGISTER FUND WITHDRAWL OPERATIONS
		FundsOperation fundOperation = new FundsOperation();
 		fundOperation = saveWithdrawlFundOperation(institutionCashAccount,null,false,userName);
		//EXECUTE FUNDS COMPONENT
		executeFundComponent(fundOperation);
		//EXECUTE SWIFT COMPONENT
		//swiftWithdrawl.executeSwiftWithdrawl(fundOperation,null,null);
	}


	/**
	 * METHOD FOR SAVING THE WITHDRAWL FUND OPERATION FOR PARTICIPANT OR CENTRAL CASH ACCOUT .
	 *
	 * @param unfulfillOperation the unfulfill operation
	 * @param centralOperation the central operation
	 * @param centralAccount the central account
	 * @param blUnfulFill the bl unful fill
	 * @param userName the user name
	 * @return the funds operation
	 */
	private FundsOperation saveWithdrawlFundOperation(ParticipantSettlement unfulfillOperation, FundsOperation centralOperation, boolean centralAccount, 
			boolean blUnfulFill,String userName){
		FundsOperation fundOperation = new FundsOperation();
		MechanismOperation operation = unfulfillOperation.getSettlementOperation().getMechanismOperation();
		InstitutionCashAccount cashAccount = new InstitutionCashAccount();
		Long participantPK = unfulfillOperation.getParticipant().getIdParticipantPk();
		Long mechanism = operation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
		Long modality = operation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		Long modalityGroup = automaticReceptionService.getModalityGroup(mechanism, modality);
		if(centralAccount){
			cashAccount = automaticReceptionService.getActivatedCentralizingAccount(operation.getCurrency());
			fundOperation.setInstitutionCashAccount(cashAccount);
			fundOperation.setIndCentralized(BooleanType.YES.getCode());
		}else{
			cashAccount = manageCashAccountService.getWithdrawlCashAccount(unfulfillOperation.getParticipant().getIdParticipantPk(),mechanism,modalityGroup,operation.getCurrency());
			fundOperation.setInstitutionCashAccount(cashAccount);
			fundOperation.setIndCentralized(BooleanType.NO.getCode());
		}
		fundOperation.setFundsOperation(centralOperation);
		Participant participant = manageCashAccountService.find(participantPK,Participant.class);
		fundOperation.setParticipant(participant);
		fundOperation.setPaymentReference(operation.getPaymentReference());
		fundOperation.setBank(null);
		fundOperation.setBenefitPaymentAllocation(null);
		fundOperation.setCurrency(operation.getCurrency());
		fundOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		fundOperation.setFundsOperationGroup(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		fundOperation.setFundsOperationType(ParameterFundsOperationType.FUNDS_SETTLEMENT_WITHDRAWL.getCode());
		fundOperation.setGuaranteeOperation(null);
		fundOperation.setIndAutomatic(BooleanType.YES.getCode());
		fundOperation.setIssuer(null);
		fundOperation.setMechanismOperation(operation);
		if(blUnfulFill){
			//WITHDRAWL BY UNFULFILLED OPERATIONS
			fundOperation.setOperationAmount(unfulfillOperation.getDepositedAmount());
			fundOperation.setRetirementType(FundsRetirementType.BY_TOTAL.getCode());
		}else{
			//WITHDRAWL BY OPERATIONS WITH EXCEDENT DEPOSITED
			BigDecimal operationAmount = unfulfillOperation.getSettlementAmount();
			BigDecimal depositedAmount = unfulfillOperation.getDepositedAmount();
			fundOperation.setOperationAmount(depositedAmount.subtract(operationAmount));
			fundOperation.setRetirementType(FundsRetirementType.BY_OVERPAID.getCode());
		}
		fundOperation.setOperationPart(unfulfillOperation.getSettlementOperation().getOperationPart());
		fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundOperation.setRegistryUser(userName);
		fundOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundOperation.setSettlementProcess(null);
		Integer fundOperationNumber = automaticReceptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), 
																							FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		fundOperation.setFundOperationNumber(fundOperationNumber);
		fundOperation.setIndBankRetirement(BooleanType.YES.getCode());
		manageCashAccountService.create(fundOperation);

		return fundOperation;
	}
	
	
	/**
	 * METHOD FOR SAVING THE WITHDRAWL FUND OPERATION FOR PARTICIPANT OR CENTRAL CASH ACCOUT .
	 *
	 * @param institutuionCashAccount the institutuion cash account
	 * @param centralFundsOperation the central funds operation
	 * @param centralAccount the central account
	 * @param userName the user name
	 * @return the funds operation
	 */
	private FundsOperation saveWithdrawlFundOperation(InstitutionCashAccount institutuionCashAccount,FundsOperation centralFundsOperation,  boolean centralAccount,
			String userName){
		FundsOperation fundOperation = new FundsOperation();
 		InstitutionCashAccount cashAccount = new InstitutionCashAccount();
   
		if(centralAccount){
			cashAccount = automaticReceptionService.getActivatedCentralizingAccount(institutuionCashAccount.getCurrency());
			fundOperation.setInstitutionCashAccount(cashAccount);
			fundOperation.setIndCentralized(BooleanType.YES.getCode());
		}else{
 			fundOperation.setInstitutionCashAccount(institutuionCashAccount);
 			fundOperation.setFundsOperation(centralFundsOperation);
 			fundOperation.setIndCentralized(BooleanType.NO.getCode());
		}
 		fundOperation.setParticipant(institutuionCashAccount.getParticipant());
		fundOperation.setBank(null);
		fundOperation.setBenefitPaymentAllocation(null);
		fundOperation.setCurrency(institutuionCashAccount.getCurrency());
		fundOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		fundOperation.setFundsOperationGroup(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		fundOperation.setFundsOperationType(ParameterFundsOperationType.FUNDS_SETTLEMENT_WITHDRAWL.getCode());
		fundOperation.setGuaranteeOperation(null);
		fundOperation.setIndAutomatic(BooleanType.YES.getCode());
		fundOperation.setRetirementType(FundsRetirementType.BY_TOTAL.getCode());
		fundOperation.setIssuer(null);		
 		fundOperation.setOperationAmount(institutuionCashAccount.getAvailableAmount());
 		fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
 		fundOperation.setRegistryUser(userName);
 		fundOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundOperation.setSettlementProcess(null);
		Integer fundOperationNumber = automaticReceptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), 
																							FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		fundOperation.setFundOperationNumber(fundOperationNumber);
		fundOperation.setIndBankRetirement(BooleanType.YES.getCode());
		
		manageCashAccountService.create(fundOperation);

		return fundOperation;
	}

	/**
	 * METHOD TO EXECUTE THE FUND OPERATION.
	 *
	 * @param fundOperation the fund operation
	 * @throws ServiceException the service exception
	 */
	private void executeFundComponent(FundsOperation fundOperation) throws ServiceException{

		FundsComponentTO objComponent = new FundsComponentTO();
		objComponent.setIdBusinessProcess(BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_RETIREMENT_PROCESS.getCode());
		objComponent.setIdFundsOperationType(fundOperation.getFundsOperationType());
		objComponent.setIdFundsOperation(fundOperation.getIdFundsOperationPk());
		objComponent.setIdInstitutionCashAccount(fundOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		objComponent.setCashAmount(fundOperation.getOperationAmount());

		executorComponentServiceBean.get().executeFundsComponent(objComponent);
	}

	/**
	 * METHOD WHICH WILL VALIDATE IF THE CENTRALIZER CASH ACCOUNT HAS AMOUNTS.
	 *
	 * @return the integer
	 */
	public Integer validateCentralizingAccountAmounts(){
		Integer cashAmount = GeneralConstants.ZERO_VALUE_INTEGER;
		try{
			BigDecimal amountCashAccounts = manageCashAccountService.getCashAccountAmount();
			if(amountCashAccounts==null || BigDecimal.ZERO.compareTo(amountCashAccounts) == 0){
				BigDecimal centralizerAccountAmount = manageCashAccountService.getCentralizerAccountAmount();
				if(Validations.validateIsNull(centralizerAccountAmount))
				{
					cashAmount=GeneralConstants.ONE_VALUE_INTEGER;
				}
				else if(BigDecimal.ZERO.compareTo(centralizerAccountAmount) == 0){
					cashAmount=GeneralConstants.TWO_VALUE_INTEGER;
				}
			}
		}catch(ServiceException x){
			x.printStackTrace();
		}

		return cashAmount;
	}

	/**
	 * Gets the last process day.
	 *
	 * @return the last process day
	 * @throws ServiceException the service exception
	 */
	public Date getLastProcessDay() throws ServiceException{
		return manageCashAccountService.getLastProcessDay();
	}
	
	/**
	 * Gets the last open day.
	 *
	 * @return the last open day
	 * @throws ServiceException the service exception
	 */
	public Date getLastOpenDay() throws ServiceException{
		return manageCashAccountService.getLastOpenDay();
	}
	
	/**
	 * Gets the last close day.
	 *
	 * @return the last close day
	 * @throws ServiceException the service exception
	 */
	public Date getLastCloseDay() throws ServiceException{
		return manageCashAccountService.getLastCloseDay();
	}
	
	/**
	 * Gets the accounts validate begin day.
	 *
	 * @return the accounts validate begin day
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> getAccountsValidateBeginDay() throws ServiceException{
		return manageCashAccountService.getAccountsValidateBeginDay();
	}
	

	/**
	 * Gets the account type related central.
	 *
	 * @param currency the currency
	 * @return the account type related central
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> getAccountTypeRelatedCentral(Integer currency) throws ServiceException{
		List<InstitutionCashAccount> lstResult = new ArrayList<InstitutionCashAccount>();
		List<Object[]> lstCashAccounts = manageCashAccountService.getAccountTypeRelatedCentral(currency);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccounts)){
			for(Object[] obj: lstCashAccounts){
				InstitutionCashAccount cashAccount = new InstitutionCashAccount();
				cashAccount.setAccountType(Integer.parseInt(obj[6].toString()));
				cashAccount.setAccountTypeDescription(obj[0].toString());
				cashAccount.setCurrencyTypeDescription(obj[1].toString());				
				cashAccount.setTotalAmount(new BigDecimal(obj[2].toString()));
				cashAccount.setAvailableAmount(new BigDecimal(obj[3].toString()));
				cashAccount.setDepositAmount(new BigDecimal(obj[4].toString()));
				cashAccount.setRetirementAmount(new BigDecimal(obj[5].toString()));				
				cashAccount.setCurrency(Integer.valueOf(obj[7].toString()));
				lstResult.add(cashAccount);
			}
		}
		return lstResult;
	}
	
	/**
	 * Validate begin day.
	 *
	 * @param processDay the process day
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateBeginDay(Date processDay) throws ServiceException
	{
		String strMessage= null;
		Date previousDate = holidayService.getCalculateDate(processDay, 1, 0,null);
		BeginEndDay beginEndPreviousDay = searchBeginEnd(previousDate);
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(Validations.validateIsNotNull(beginEndPreviousDay))
		{
			if(BeginEndDaySituationType.CLOSE.getCode().equals(beginEndPreviousDay.getIndSituation()))
			{
				BeginEndDay beginEndToday = searchBeginEnd(processDay);
				if(Validations.validateIsNotNull(beginEndToday))
				{
					strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.FUNDS_CASH_ACCOUNT_IS_NOT_CLOSED_TO_INIT);
				}
				else
				{
					List<InstitutionCashAccount> lstAccounts=getAccountsValidateBeginDay();
					if(lstAccounts!=null && lstAccounts.size()>0)
					{
						strMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.CASH_ACCOUNT_START_DAY_EXIST_AMOUNT_RELATEDCENTRALACCOUNT);
					}
					
					else
					{
						Integer iAccountAmounts=validateCentralizingAccountAmounts();
						if(iAccountAmounts.equals(GeneralConstants.ZERO_VALUE_INTEGER))
						{
							strMessage= PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.CASH_ACCOUNT_START_DAY_EXIST_CENTRALACCOUNT_HASAMOUNT);
						}
						else if(iAccountAmounts.equals(GeneralConstants.ONE_VALUE_INTEGER))
						{
							strMessage= PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.CASH_ACCOUNT_START_DAY_NOT_EXIST_CENTRALACCOUNT);
						}
					}
				}
			} else {
				strMessage= PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_CLOSED);
			}
		} else {
			strMessage= PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_MESSAGES, Locale.getDefault(), PropertiesConstants.CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_OPEN);
		}
		if(strMessage!=null){
			// Start Notification Error Manual
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BEGIN_DAY_AUTOMATIC.getCode());
			UserFilterTO  userFilter=new UserFilterTO();
			userFilter.setUserName(loggerUser.getUserName());
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);
			/*notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcessNotification, lstUserAccount,
						GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.MESSAGE);*/
			notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcessNotification, lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.EMAIL);
		//	 End Notification
		}
		return strMessage;
	}
	
	/**
	 * Start begin day.
	 *
	 * @param processDay the process day
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BEGIN_DAY_AUTOMATIC)
	public void startBeginDay(Date processDay) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		BeginEndDay beginEndDay = new BeginEndDay();
		beginEndDay.setRegistryUser(loggerUser.getUserName());
		beginEndDay.setProcessDay(processDay);
		beginEndDay.setRegistryDate(CommonsUtilities.currentDate());
		beginEndDay.setIndSituation(BeginEndDaySituationType.OPEN.getCode());
		beginEndDay.setOpenDay(CommonsUtilities.currentDateTime());
		manageCashAccountService.create(beginEndDay);
		//WE UPDATE THE SITUATION OF ALL THE CASH ACCOUNTS IN ACTIVATE STATE
		manageCashAccountService.updateInstitutionCashAccountSituation(BeginEndDaySituationType.OPEN.getCode());
		//UPDATE THE SITUATION OF CENTRALIZER CASH ACCONT TO OPEN
		manageCashAccountService.updateCentralizerCashAccountSituation(BeginEndDaySituationType.OPEN.getCode());
		// Start Notification
		BusinessProcess businessProcessNotification = new BusinessProcess();					
		businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BEGIN_DAY_AUTOMATIC.getCode());
		Object[] parameters = new Object[]{CommonsUtilities.convertDatetoString(processDay)};
		notificationServiceFacade.sendNotification(loggerUser.getUserName(),
				businessProcessNotification,CommonsUtilities.convertDatetoString(processDay), parameters);
		// End Notification
		
		// Send Reports
		SettlementReportTO filter = new SettlementReportTO(); 
		filter.setDate(CommonsUtilities.convertDatetoString(processDay));
		List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.detailExpirationRepurchaseQuery(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
			settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.DETAIL_EXPIRATION_REPURCHASE.getCode());
		}		
		// End Reports
	}
	
	
	/**
	 * Gets the participants.
	 *
	 * @param blParameter the bl parameter
	 * @return the participants
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipants(boolean blParameter) throws ServiceException{
		return cashAccountService.getParticipants(blParameter);
	}
	
	/**
	 * Gets the issuers.
	 *
	 * @return the issuers
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> getIssuers() throws ServiceException{
		return cashAccountService.getActivateIssuer();
	}
	
	public boolean isSupervisorOrManagerProfile(String loginUser){
		return clientRestService.isSupervisorOrManagerProfile(loginUser);
	}
	
	public boolean validateUser(String loginUser, String pass) throws KeyManagementException, NoSuchAlgorithmException{
		return clientRestService.validateUser(loginUser, pass);
	}
}