package com.pradera.funds.fundoperations.util;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class PropertiesConstants {

	
	/**
	 * Instantiates a new properties constants.
	 */
	private PropertiesConstants() {
		super();
	}
	
	/** The Constant FUNDS_CASH_ACCOUNT_PREVIOUS_OPEN_DAY. */
	public static final String FUNDS_CASH_ACCOUNT_PREVIOUS_OPEN_DAY="lbl.cnf.start.day.msg";
	
	/** The Constant FUNDS_CASH_ACCOUNT_IS_NOT_CLOSED_TO_INIT. */
	public static final String FUNDS_CASH_ACCOUNT_IS_NOT_CLOSED_TO_INIT="cash.account.init.is.not.closed";
	
	/** The Constant CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_CLOSED. */
	public static final String CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_CLOSED="monitoring.cash.account.previous.day.not.closed";
	
	/** The Constant CASH_ACCOUNT_MONITORING_DAY_CLOSED_SUCCESS. */
	public static final String CASH_ACCOUNT_MONITORING_DAY_CLOSED_SUCCESS="lbl.day.close.success.msg";
	
	/** The Constant CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_OPEN. */
	public static final String CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_OPEN="cash.account.previous.day.not.opened";
	
	/** The Constant CASH_ACCOUNT_START_DAY_EXIST_AMOUNT_RELATEDCENTRALACCOUNT. */
	public static final String CASH_ACCOUNT_START_DAY_EXIST_AMOUNT_RELATEDCENTRALACCOUNT="cash.account.start.day.validate.exist.amount.relatedCentralAccount";
	
	/** The Constant CASH_ACCOUNT_START_DAY_EXIST_CENTRALACCOUNT_HASAMOUNT. */
	public static final String CASH_ACCOUNT_START_DAY_EXIST_CENTRALACCOUNT_HASAMOUNT="cash.account.start.day.validate.centralAccount.hasAmount";
	
	/** The Constant CASH_ACCOUNT_START_DAY_NOT_EXIST_CENTRALACCOUNT. */
	public static final String CASH_ACCOUNT_START_DAY_NOT_EXIST_CENTRALACCOUNT="cash.account.start.day.validate.centralAccount.hasNotCentralizedAccount";
	
	/** The Constant CASH_ACCOUNT_CANT_CLOSE_DAY. */
	public static final String CASH_ACCOUNT_CANT_CLOSE_DAY="cash.account.cant.close.day";
	
	public static final String EXISTS_PENDING_SETTLEMENT_OPERATIONS="exists.pending.settlement.operations";
	
	/** The Constant FUNDS_OPERATION_REGISTERED_ERROR. */
	public static final String FUNDS_OPERATION_REGISTERED_ERROR="funds.cash.account.funds.operations.registered";	
	
	/** The Constant FUNDS_OPERATION_OBSERVED_STATE. */
	public static final String FUNDS_OPERATION_OBSERVED_STATE="funds.cash.account.funds.operations.observed";
	
	/** The Constant FUNDS_CASH_ACCOUNT_IS_NOT_OPENED_TO_CLOSE. */
	public static final String FUNDS_CASH_ACCOUNT_IS_NOT_OPENED_TO_CLOSE="cash.account.init.is.not.opened";
	
	/** The Constant FUNDS_OPERATIONS_NOT_OPENED_TO_END. */
	public static final String FUNDS_OPERATIONS_NOT_OPENED_TO_END="lbl.cannot.end.day.msg";
	
	/** The Constant FUNDS_OPERATIONS_CONFIRM_CLOSE_DAY_MESSAGE. */
	public static final String FUNDS_OPERATIONS_CONFIRM_CLOSE_DAY_MESSAGE="lbl.cnf.close.day.msg";
	
	/** The Constant FUNDS_OPERATIONS_CONFIRM_EXISTAMOUNT_RELATEDCENTRAL. */
	public static final String FUNDS_OPERATIONS_CONFIRM_EXISTAMOUNT_RELATEDCENTRAL="cash.account.close.day.conf.existAmount.relatedCentral.msg";
	
	/** The Constant FUNDS_OPERATIONS_SETTLEMENT_SCHEMA_GROSS. */
	public static final String FUNDS_OPERATIONS_SETTLEMENT_SCHEMA_GROSS="funds.deposit.settlementSchema.gross";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATE_ERROR. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATE_ERROR="alt.funds.deposit.error.settlementDate";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEEXTEND_ERROR. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEEXTEND_ERROR="alt.funds.deposit.error.settlementDateExtend";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEPREPAID_ERROR. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEPREPAID_ERROR="alt.funds.deposit.error.settlementDatePrepaid";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INCHARGE_ERROR. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INCHARGE_ERROR="alt.funds.deposit.error.incharge";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_FUNDREFERENCE_ERROR. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_FUNDREFERENCE_ERROR="alt.funds.deposit.error.fundReference";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_OPERATION_NOTEXIST. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_OPERATION_NOTEXIST="alt.funds.deposit.operation.notExist";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_PARTICIPANT_EFFECTIVEACCOUNT_NOTEXIST. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_PARTICIPANT_EFFECTIVEACCOUNT_NOTEXIST="alt.funds.deposit.participant.effective.account.notExist";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_EMPTY_AMOUNT. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_EMPTY_AMOUNT="alt.funds.deposit.empty.amount";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SUCESS_REGISTER. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SUCESS_REGISTER="alt.funds.deposit.success.register";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTSCHEMA_NET. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTSCHEMA_NET="funds.deposit.settlementSchema.net";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED="alt.funds.deposit.effective.account.not.selected";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_ISSUER_EFFECTIVEACCOUNT_NOTEXIST. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_ISSUER_EFFECTIVEACCOUNT_NOTEXIST="alt.funds.deposit.issuer.effective.account.notExist";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_HOLDER_NOTREGISTER. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_HOLDER_NOTREGISTER="msg.funds.deposit.register.returns.holder.not.register";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_CUI_NOTFOUND. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_CUI_NOTFOUND="msg.funds.deposit.register.returns.cui.not.found";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_GREATERTHANZERO_AMOUNT. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_GREATERTHANZERO_AMOUNT="alt.funds.deposit.greaterthanzero.amount";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_EDV_EFFECTIVE_ACCOUNT_NOTEXIST. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_EDV_EFFECTIVE_ACCOUNT_NOTEXIST="alt.funds.deposit.edv.effective.account.notExist";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_ERROR_HOLDERCASHMOVEMENT. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_ERROR_HOLDERCASHMOVEMENT="alt.funds.deposit.error.holderCashMovement";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_RETURNS_CUITITULAR_MANDATORY. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_RETURNS_CUITITULAR_MANDATORY="msg.funds.deposit.register.returns.cuiTitular";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_HOLDERCASHMOVEMENT_EMPTY. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_HOLDERCASHMOVEMENT_EMPTY="alt.funds.deposit.error.holderCashMovement.empty";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_HOLDERCASHMOVEMENT_NOT_SELECTED. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_HOLDERCASHMOVEMENT_NOT_SELECTED="alt.funds.deposit.error.holderCashMovement.not.selected";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_REGISTRY_DATE_GREATERTHAN. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_REGISTRY_DATE_GREATERTHAN="alt.funds.deposit.registry.date.greaterThan";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_SETTLEMENT_DATE_GREATERTHAN. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_SETTLEMENT_DATE_GREATERTHAN="alt.funds.deposit.settlement.date.greaterThan";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_INVALIDSTATE. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_INVALIDSTATE="alt.funds.deposit.error.inter.invalidState";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_NOTEXIST. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_NOTEXIST="alt.funds.deposit.operations.notExist";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_EMPTY. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_EMPTY="alt.funds.deposit.empty.operation";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_NOTSELECTED. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_NOTSELECTED="alt.funds.deposit.operation.not.selected";	 
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SUCESS_CONFIRM. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SUCESS_CONFIRM="alt.funds.deposit.success.confirm";
	
	public static final String FUNDS_OPERATIONS_DEPOSIT_SUCESS_TRANSFER="alt.funds.deposit.success.transfer";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SUCESS_REJECT. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SUCESS_REJECT="alt.funds.deposit.success.reject";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INVALID_CONFIRM. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INVALID_CONFIRM="alt.funds.deposit.action.invalidConfirm";	
	
	public static final String FUNDS_OPERATIONS_DEPOSIT_INVALID_TRANSFER="alt.funds.deposit.action.invalidTransfer";	
	
	public static final String FUNDS_OPERATIONS_DEPOSIT_INVALID_NUMBER_TRANSFER="alt.funds.deposit.action.invalid.number.transfers";	
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_INVALID_REJECT. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_INVALID_REJECT="alt.funds.deposit.action.invalidReject";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED="alt.funds.deposit.record.not.selected";
	
	
	
	/** The Constant FUNDS_OPERATION_MECHANISM_OPERATION_NOT_OVER_PAID_AMOUNT. */
	public static final String FUNDS_OPERATION_MECHANISM_OPERATION_NOT_OVER_PAID_AMOUNT="funds.retirement.register.settlement.mechanisnOperation.not.overPaidAmount";
	
	/** The Constant FUNDS_OPERATION_WITH_FUNDS_COMPLAICE. */
	public static final String FUNDS_OPERATION_WITH_FUNDS_COMPLAICE = "funds.operation.participant.operation.funds.complaicen";
	
	/** The Constant FUNDS_OPERATION_PARTICIPANT_OPERATION_NULL. */
	public static final String FUNDS_OPERATION_PARTICIPANT_OPERATION_NULL="funds.operation.participant.operation.null";
	
	/** The Constant FUNDS_OPERATION_MECHANISM_OPERATION_NOT_FOUND. */
	public static final String FUNDS_OPERATION_MECHANISM_OPERATION_NOT_FOUND="funds.retirement.register.settlement.mechanisnOperation.not.found";
	
	/** The Constant FUNDS_OPERATION_REFERENCE_PAYMENT_NOT_FOUND. */
	public static final String FUNDS_OPERATION_REFERENCE_PAYMENT_NOT_FOUND="funds.retirement.register.settlement.referencePayment.not.found";
	
	/** The Constant FUNDS_OPERATIONS_PARTICIPANT_OPERATION_NOT_DEPOSIT_AMOUNT. */
	public static final String FUNDS_OPERATIONS_PARTICIPANT_OPERATION_NOT_DEPOSIT_AMOUNT = "funds.retirement.register.settlement.participantOperation.not.depositedAmount";
	
	/** The Constant FUNDS_OPERATION_OPERATION_PART_NOT_SELECTED. */
	public static final String FUNDS_OPERATION_OPERATION_PART_NOT_SELECTED="fund.retirement.operationPart.not.selected";
	
	/** The Constant FUNDS_OPERATION_RETIRE_AMOUNT_GREATER_THAN_AVAILABLE. */
	public static final String FUNDS_OPERATION_RETIRE_AMOUNT_GREATER_THAN_AVAILABLE="fund.retirement.nothing.to.retire.greater.availableBalance";
	
	/** The Constant FUNDS_RETIREMENT_OPERATION_BCB_DEVOLUTION_NOT_RELATED. */
	public static final String FUNDS_RETIREMENT_OPERATION_BCB_DEVOLUTION_NOT_RELATED="funds.retirement.devolution.bcrd.not.bcrd.related";
	
	/** The Constant FUNDS_OPERATION_BEFORE_SAVE_MESSAGE. */
	public static final String FUNDS_OPERATION_BEFORE_SAVE_MESSAGE="funds.retirement.register.before.save";
	
	/** The Constant FUNDS_RETIREMENT_SELECT_INSTITUTION_CASH_ACCOUNT. */
	public static final String FUNDS_RETIREMENT_SELECT_INSTITUTION_CASH_ACCOUNT="funds.retirement.select.institution.cash.account";
	
	/** The Constant FUNDS_OPERATION_DEPOSIT_ACCOUNT_NOT_SELECTED. */
	public static final String FUNDS_OPERATION_DEPOSIT_ACCOUNT_NOT_SELECTED="fund.retirement.nothing.to.deposit";
	
	/** The Constant FUNDS_OPERATION_NOTHING_TO_RETIRE. */
	public static final String FUNDS_OPERATION_NOTHING_TO_RETIRE = "fund.retirement.nothing.to.retire.error";
	
	/** The Constant FUNDS_OPERATION_INSTITUTION_CASH_ACCOUNT_NOT_SELECTED. */
	public static final String FUNDS_OPERATION_INSTITUTION_CASH_ACCOUNT_NOT_SELECTED="fund.retirement.institution.cash.account.not.selected";
	
	/** The Constant FUNDS_RETIREMENT_FOUND_IN_OTHER_REQUEST. */
	public static final String FUNDS_RETIREMENT_FOUND_IN_OTHER_REQUEST="fund.retirement.participant.operation.in.other.request";
	
	/** The Constant FUNDS_BENEFIT_RETIREMENT_NOT_ACCOUNT_WITH_CURRENCY. */
	public static final String FUNDS_BENEFIT_RETIREMENT_NOT_ACCOUNT_WITH_CURRENCY = "funds.retirement.register.benefitPayment.not.account.central";
	
	/** The Constant FUNDS_RETIREMENT_DEPOSIT_FOUND_IN_OTHER_REQUEST. */
	public static final String FUNDS_RETIREMENT_DEPOSIT_FOUND_IN_OTHER_REQUEST="fund.retirement.deposit.operation.in.other.request";
	
	/** The Constant FUNDS_OPERATION_SAVE_MESSAGE. */
	public static final String FUNDS_OPERATION_SAVE_MESSAGE="funds.retirement.register.save.message";
	
	public static final String FUNDS_OPERATION_NOT_BANK_MOVEMENT = "funds.operation.not.bank.movement";
	
	/** The Constant FUNDS_RETIREMENT_BEFORE_CONFIRM_MESSAGE. */
	public static final String FUNDS_RETIREMENT_BEFORE_CONFIRM_MESSAGE="funds.retirement.before.confirm";
	
	/** The Constant FUNDS_RETIREMENT_BEFORE_REJECT_MESSAGE. */
	public static final String FUNDS_RETIREMENT_BEFORE_REJECT_MESSAGE="funds.retirement.before.reject";
	
	/** The Constant FUNDS_RETIREMENT_REQUEST_NOT_IN_STATE. */
	public static final String FUNDS_RETIREMENT_REQUEST_NOT_IN_STATE="funds.retirement.error.consult.state";
	
	/** The Constant FUNDS_RETIREMENT_REGISTERED_LBL. */
	public static final String FUNDS_RETIREMENT_REGISTERED_LBL="funds.lbl.registered";
	
	/** The Constant ERROR_RECORD_NOT_SELECTED. */
	public static final String ERROR_RECORD_NOT_SELECTED = "error.record.not.selected";
	
	/** The Constant FUNDS_RETIREMENT_SUCCESS_CONFIRM_MESSAGE. */
	public static final String FUNDS_RETIREMENT_SUCCESS_CONFIRM_MESSAGE="funds.retirement.confirm.success";
	
	/** The Constant FUNDS_RETIREMENT_SUCCESS_REJECT_MESSAGE. */
	public static final String FUNDS_RETIREMENT_SUCCESS_REJECT_MESSAGE="funds.retirement.reject.success";
	
	/** The Constant ISSUER_VALIDATION_REGISTERED. */
	public static final String ISSUER_VALIDATION_REGISTERED="msg.banks.accounts.issuer.validation.registered";
	
	/** The Constant FUNDS_RETIREMENT_SELECT_OPERATION_SETTLEMENT_AUTOMATIC. */
	public static final String FUNDS_RETIREMENT_SELECT_OPERATION_SETTLEMENT_AUTOMATIC="funds.retirement.select.operation.settlement.automatic";
	
	/** The Constant CASH_ACCOUNT_REGISTER. */
	public static final String CASH_ACCOUNT_REGISTER="alt.cash.account.register";
	
	/** The Constant CASH_ACCOUNT_ASK_REGISTER. */
	public static final String CASH_ACCOUNT_ASK_REGISTER="alt.cash.account.askRegister";
	
	/** The Constant CASH_ACCOUNT_MODALITYGROUP_NOTSELECTED. */
	public static final String CASH_ACCOUNT_MODALITYGROUP_NOTSELECTED="alt.cash.account.modalityGroup.not.selected";
	
	/** The Constant CASH_ACCOUNT_BANK_NOTADDED. */
	public static final String CASH_ACCOUNT_BANK_NOTADDED="alt.cash.account.bank.not.added";
	
	public static final String ISSUER_NOT_EXIST="alt.issuer.not.exist";
	
	/** The Constant CASH_ACCOUNT_ASK_ACTIVE. */
	public static final String CASH_ACCOUNT_ASK_ACTIVE="alt.cash.account.askActive";
	
	/** The Constant CASH_ACCOUNT_ASK_DEACTIVE. */
	public static final String CASH_ACCOUNT_ASK_DEACTIVE="alt.cash.account.askDeactive";
	
	/** The Constant CASH_ACCOUNT_ASK_MODIFY. */
	public static final String CASH_ACCOUNT_ASK_MODIFY="alt.cash.account.askModify";
	
	/** The Constant CASH_ACCOUNT_CURRENCY_NOT_SELECTED. */
	public static final String CASH_ACCOUNT_CURRENCY_NOT_SELECTED="alt.cash.account.currency.not.selected";
	
	/** The Constant CASH_ACCOUNT_BANK_NOTFOUND. */
	public static final String CASH_ACCOUNT_BANK_NOTFOUND="alt.cash.account.data.bank.not.found";
	
	/** The Constant CASH_ACCOUNT_BANK_USERTYPE_RECEPTION. */
	public static final String CASH_ACCOUNT_BANK_USERTYPE_RECEPTION="alt.cash.account.bank.userType.reception";
	
	/** The Constant CASH_ACCOUNT_BANK_USERTYPE_SEND. */
	public static final String CASH_ACCOUNT_BANK_USERTYPE_SEND="alt.cash.account.bank.userType.send";
	
	/** The Constant CASH_ACCOUNT_OWN_CLASS_EXIST. */
	public static final String CASH_ACCOUNT_OWN_CLASS_EXIST="alt.cash.account.own.class.exist";
	
	/** The Constant CASH_ACCOUNT_BANK_ALREADY_ADDED. */
	public static final String CASH_ACCOUNT_BANK_ALREADY_ADDED="alt.cash.account.bank.already.added";
	
	/** The Constant CASH_ACCOUNT_NUMBER_ALREADY_ADDED. */
	public static final String CASH_ACCOUNT_NUMBER_ALREADY_ADDED="alt.cash.account.accountNumber.already.added";	
	
	/** The Constant CASH_ACCOUNT_BANK_MAX_QUANTITY. */
	public static final String CASH_ACCOUNT_BANK_MAX_QUANTITY="alt.cash.account.bank.max.quantity";
	
	/** The Constant CASH_ACCOUNT_DELETE_BANK. */
	public static final String CASH_ACCOUNT_DELETE_BANK="alt.cash.account.delete.bank";
	
	/** The Constant CASH_ACCOUNT_BANK_NOT_SELECTED. */
	public static final String CASH_ACCOUNT_BANK_NOT_SELECTED="alt.cash.account.bank.not.selected";
	
	/** The Constant FUNDS_REMOVE_CONFIRM. */
	public static final String FUNDS_REMOVE_CONFIRM="funds.remove.confirm";
	
	/** The Constant CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED. */
	public static final String CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED="alt.cash.account.bankaccount.not.selected";
	
	/** The Constant CASH_ACCOUNT_NOT_SELECTED. */
	public static final String CASH_ACCOUNT_NOT_SELECTED="alt.cash.account.not.selected";
	
	/** The Constant CASH_ACCOUNT_INVALID_ACTIVE. */
	public static final String CASH_ACCOUNT_INVALID_ACTIVE="alt.cash.account.invalidActive";
	
	/** The Constant CASH_ACCOUNT_INVALID_DEACTIVE. */
	public static final String CASH_ACCOUNT_INVALID_DEACTIVE="alt.cash.account.invalidDeactive";
	
	/** The Constant CASH_ACCOUNT_INVALID_MODIFY. */
	public static final String CASH_ACCOUNT_INVALID_MODIFY="alt.cash.account.invalidModify";
	
	/** The Constant CASH_ACCOUNT_INVALID_MODIFY. */
	public static final String CASH_ACCOUNT_INVALID_MODIFY_RETURN="alt.cash.account.invalidModifyReturn";
	
	/** The Constant CASH_ACCOUNT_BANK_NOT_FOUND. */
	public static final String CASH_ACCOUNT_BANK_NOT_FOUND="alt.cash.account.bank.not.found";
	
	/** The Constant CASH_ACCOUNT_DEPOSIT_ASK_CONFIRM. */
	public static final String CASH_ACCOUNT_DEPOSIT_ASK_CONFIRM="alt.funds.deposit.ask.confirm";
	
	public static final String CASH_ACCOUNT_DEPOSIT_ASK_TRANFER_CENTRAL="alt.funds.deposit.ask.transfer.central";
	
	/** The Constant CASH_ACCOUNT_DEPOSIT_ASK_REJECT. */
	public static final String CASH_ACCOUNT_DEPOSIT_ASK_REJECT="alt.funds.deposit.ask.reject";
	
	/** The Constant CASH_ACCOUNT_ACTIVE. */
	public static final String CASH_ACCOUNT_ACTIVE="alt.cash.account.active";
	
	/** The Constant CASH_ACCOUNT_DEACTIVE. */
	public static final String CASH_ACCOUNT_DEACTIVE="alt.cash.account.deactive";
	
	/** The Constant FUNDS_RETIREMENT_SELECT_INSTITUTION_BANK_ACCOUNT. */
	public static final String FUNDS_RETIREMENT_SELECT_INSTITUTION_BANK_ACCOUNT="funds.retirement.select.institution.bank.account";
	
	/** The Constant CASH_ACCOUNT_ALREADY_EXIST. */
	public static final String CASH_ACCOUNT_ALREADY_EXIST="alt.cash.account.already.exist";
	
	/** The Constant CASH_ACCOUNT_MODIFIED. */
	public static final String CASH_ACCOUNT_MODIFIED="alt.cash.account.modify";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SETTLEMENT_IN_PROCESS. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SETTLEMENT_IN_PROCESS="alt.funds.deposit.effective.account.settlement.in.process";
	
	/** The Constant CASH_ACCOUNT_CENTRALIZEDBANK_NOTFOUND. */
	public static final String CASH_ACCOUNT_CENTRALIZEDBANK_NOTFOUND="alt.cash.account.centralizedbank.not.found";	
	/** The Constant ERROR_CONFIRM. */
	public static final String ERROR_CONFIRM="funds.deposit.err.msg.confirm";
	/** The Constant MSG_CONFIRM_CERTIFIACTE_MULTI. */
	public static final String MSG_CONFIRM_CERTIFIACTE_MULTI="funds.deposit.msg.confirm.multi";
	/** The Constant report extended */
	public static final String MSG_CONFIRM_REPORT_EXTENDED="mcn.msg.operation.confirm.extended";
	/** The Constant ERROR_REJECT. */
	public static final String ERROR_REJECT="funds.deposit.err.msg.reeject";
	/** The Constant MSG_REJECT_CERTIFIACTE_MULTI. */
	public static final String MSG_REJECT_CERTIFIACTE_MULTI="funds.deposit.msg.reeject.multi";
	
	/** The Constant FUNDS_OPERATIONS_DEPOSIT_SUCESS_REJECT_MULTI. */
	public static final String FUNDS_OPERATIONS_DEPOSIT_SUCESS_REJECT_MULTI="alt.funds.deposit.success.reject.multiple";
	
	public static final String MESSAGES_SUCCESFUL="messages.succesful";
	public static final String EXTENDED_REPORTED_TERM="msg.alert.extended.reported.term";
	
	
	
	

	public static final String SEDIR_OPERATION_APPROVE_CONFIRM_MULTIPLE="sedir.operation.approve.confirm.multiple";
	public static final String SEDIR_OPERATION_REJECT_CONFIRM_MULTIPLE="sedir.operation.reject.confirm.multiple";
	public static final String SEDIR_OPERATION_ANNULATE_CONFIRM_MULTIPLE="sedir.operation.annulate.confirm.multiple";
	public static final String SEDIR_OPERATION_REVIEW_CONFIRM_MULTIPLE="sedir.operation.review.confirm.multiple";
	public static final String SEDIR_OPERATION_CONFIRM_CONFIRM_MULTIPLE="sedir.operation.confirm.confirm.multiple";
	public static final String SEDIR_OPERATION_AUTHORIZE_CONFIRM_MULTIPLE="sedir.operation.authorize.confirm.multiple";
	public static final String SEDIR_OPERATION_REGISTER_CONFIRM_MULTIPLE="sedir.operation.register.confirm.multiple";
	
	public static final String SEDIR_OPERATION_APPROVE_OK="sedir.operation.approve.confirm.ok";
	public static final String SEDIR_OPERATION_REJECT_OK="sedir.operation.reject.confirm.ok";
	public static final String SEDIR_OPERATION_ANNULATE_OK="sedir.operation.annulate.confirm.ok";
	public static final String SEDIR_OPERATION_REVIEW_OK="sedir.operation.review.confirm.ok";
	public static final String SEDIR_OPERATION_CONFIRM_OK="sedir.operation.confirm.confirm.ok";
	public static final String SEDIR_OPERATION_AUTHORIZE_OK="sedir.operation.authorize.confirm.ok";
	public static final String SEDIR_OPERATION_REGISTER_OK="sedir.operation.register.confirm.ok";
	
	public static final String SEDIR_OPERATION_QuANTITY_REG="sedir.operation.quantity.reg";
	public static final String SEDIR_OPERATION_QuANTITY_REG_ZERO="sedir.operation.quantity.reg.zero";
	public static final String SEDIR_OPERATION_QuANTITY_REG_NINE="sedir.operation.quantity.reg.nine";
	
	public static final String SEDIR_OPERATION_REGISTER="sedir.operation.register";
	public static final String SEDIR_OPERATION_REGISTER_CHAINED="sedir.operation.register.chained";
	
	public static final String UNFULFILLMENT_OPERATION_REGISTER="unfulfillment.operation.register";
	public static final String UNFULFILLMENT_OPERATION_REGISTER_CORRECT="msg.alert.unfulfillment.register.correct";
	
	public static final String UNFULFILLMENT_OPERATION_APPROVE_CONFIRM_MULTIPLE="unfulfillment.operation.approve.confirm.multiple";
	public static final String UNFULFILLMENT_OPERATION_REJECT_CONFIRM_MULTIPLE="unfulfillment.operation.reject.confirm.multiple";
	public static final String UNFULFILLMENT_OPERATION_ANNULATE_CONFIRM_MULTIPLE="unfulfillment.operation.annulate.confirm.multiple";
	public static final String UNFULFILLMENT_OPERATION_REVIEW_CONFIRM_MULTIPLE="unfulfillment.operation.review.confirm.multiple";
	public static final String UNFULFILLMENT_OPERATION_CONFIRM_CONFIRM_MULTIPLE="unfulfillment.operation.confirm.confirm.multiple";
	public static final String UNFULFILLMENT_OPERATION_AUTHORIZE_CONFIRM_MULTIPLE="unfulfillment.operation.authorize.confirm.multiple";
	public static final String UNFULFILLMENT_OPERATION_REGISTER_CONFIRM_MULTIPLE="unfulfillment.operation.register.confirm.multiple";
	
	public static final String UNFULFILLMENT_OPERATION_APPROVE_OK="unfulfillment.operation.approve.confirm.ok";
	public static final String UNFULFILLMENT_OPERATION_REJECT_OK="unfulfillment.operation.reject.confirm.ok";
	public static final String UNFULFILLMENT_OPERATION_ANNULATE_OK="unfulfillment.operation.annulate.confirm.ok";
	public static final String UNFULFILLMENT_OPERATION_REVIEW_OK="unfulfillment.operation.review.confirm.ok";
	public static final String UNFULFILLMENT_OPERATION_CONFIRM_OK="unfulfillment.operation.confirm.confirm.ok";
	public static final String UNFULFILLMENT_OPERATION_AUTHORIZE_OK="unfulfillment.operation.authorize.confirm.ok";
	public static final String UNFULFILLMENT_OPERATION_REGISTER_OK="unfulfillment.operation.register.confirm.ok";
	
	/** The Constant MESSAGE_DATA_REQUIRED. */
	public static final String MESSAGE_DATA_REQUIRED = "message.data.required";
	
	/** The Constant MESSAGES_ALERT. */
	public static final String MESSAGES_ALERT="messages.alert";
}
