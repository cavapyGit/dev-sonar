package com.pradera.funds.fundoperations.monitoring.view;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.effective.accounts.facade.ManageEffectiveAccountsFacade;
import com.pradera.funds.fundoperations.monitoring.facade.MonitoringFundOperationsServiceFacade;
import com.pradera.funds.fundoperations.monitoring.to.MonitoringFundOperationsTO;
import com.pradera.funds.fundoperations.util.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BeginEndDaySituationType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.settlements.core.facade.SettlementMonitoringFacade;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.core.view.ModalitySettlementTO;
import com.pradera.settlements.core.view.MonitorSettlementTO;
import com.pradera.settlements.reports.SettlementProcessReportsSender;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;

// TODO: Auto-generated Javadoc
/**
 * The Class InstitutionCashAccountBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MonitoringFundOperationsBean extends GenericBaseBean{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** PARAMETERS LISTS *. */
	private List<ParameterTable> accountsFundTypes;
	
	/** The currency types. */
	private List<ParameterTable> currencyTypes;
	
	/** The lst institution bank accounts type. */
	private List<ParameterTable> lstInstitutionBankAccountsTypeReturn;
	
	/** The obj institution bank accounts type. */
	private Integer objInstitutionBankAccountsType;
	
	/** The institution cash account service facade. */
	@EJB
	private MonitoringFundOperationsServiceFacade monitorCashAccountFacade;
	
	/** The holiday service. */
	@Inject
	private HolidayQueryServiceBean holidayService;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade; 

	/** The monitoring fund operations to search. */
	private MonitoringFundOperationsTO monitoringFundOperationsTOSearch; 
	
	/** The manage effective accounts facade. */
	@EJB
	ManageEffectiveAccountsFacade manageEffectiveAccountsFacade;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The report sender. */
	@Inject
	private SettlementProcessReportsSender reportSender; 
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	@EJB
	private SettlementMonitoringFacade monitorProcessFacade;
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	

	/** The previous date. */
	private Date lastProcessDay,processDay,currentDate,beforeOpenDay,beforeCloseDay,previousDate,lastOpenDay,lastCloseDay;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The bl no result. */
	private boolean blParticipant,blIssuer,blCentral,blNoResult,blReturns;
	
	/** The lst issuers. */
	private List<Issuer> lstIssuers;
	
	/** The institution cash account lst. */
	private List<InstitutionCashAccount> institutionCashAccountLst;
	
	/** The institution cash account dmodel. */
	private GenericDataModel<InstitutionCashAccount> institutionCashAccountDmodel;
	
	/** The institution cash account dmodel child. */
	private GenericDataModel<InstitutionCashAccount> institutionCashAccountDmodelChild;
	
	/** The institution cash account detail dmodel. */
	private GenericDataModel<InstitutionCashAccount> institutionCashAccountDetailDmodel;
	
	/** The institution cash account detail. */
	private InstitutionCashAccount institutionCashAccountDetail;
	
	/** The institution cash efective accounts without balances. */
	private List<InstitutionCashAccount> institutionCashEfectiveAccountsWithoutBalances;
	
	//CONSTANTS
	/** ACCOUNTS TYPES *. */
	private Integer ACCOUNT_CENTALIZING = AccountCashFundsType.CENTRALIZING.getCode();	
	
	/** The account retentions. */
	private Integer ACCOUNT_RETENTIONS = AccountCashFundsType.TAX.getCode();
	
	/** The return account. */
	private Integer RETURN_ACCOUNT = AccountCashFundsType.RETURN.getCode();	
	
	/** The account rates. */
	private Integer ACCOUNT_RATES = AccountCashFundsType.RATES.getCode();
	
	/** The account settlement. */
	private Integer ACCOUNT_SETTLEMENT = AccountCashFundsType.SETTLEMENT.getCode();
	
	
	/** The ind show right fees lst. */
	private Integer indShowRightFeesLst ;
	
	/** The Ind show settlement lst. */
	private Integer indShowSettlementLst;
	
	/** The ind show participant lst. */
	private Integer indShowParticipantLst;
	
	
	/** The Constant dialogBeginDay. */
	private static final  String dialogBeginDay="wvBeginDay";
	
	/** The Constant idBeginDay. */
	private static final String idBeginDay="idDayBegan";
	
	/** The Constant wvCloseDay. */
	private static final String wvCloseDay="wvCloseDay";
	
	/** The Constant wvDayClosed. */
	private static final String wvDayClosed="wvDayClosed";
	
	
	
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {
			currentDate = CommonsUtilities.currentDate();
//			previousDate = holidayService.getCalculateDate(currentDate, 1, 0,null);
			lastProcessDay = monitorCashAccountFacade.getLastProcessDay();
			lastOpenDay = monitorCashAccountFacade.getLastOpenDay();
			lastCloseDay = monitorCashAccountFacade.getLastCloseDay();
			if(Validations.validateIsNull(getMonitoringFundOperationsTOSearch())){
				monitoringFundOperationsTOSearch = new MonitoringFundOperationsTO();
			}
			if (Validations.validateListIsNullOrEmpty(accountsFundTypes)) {
				accountsFundTypes = monitorCashAccountFacade.getParameterList(MasterTableType.ACCOUNT_CASH_FUNDS_TYPE);
			}
			if (Validations.validateListIsNullOrEmpty(currencyTypes)) {
				currencyTypes = monitorCashAccountFacade.getParameterList(MasterTableType.CURRENCY);
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * SEARCH FUNDS OPERATIONS.
	 */
	@LoggerAuditWeb
	public void searchFundOperations()
	{
			JSFUtilities.hideGeneralDialogues();
			
			if(Validations.validateIsNullOrNotPositive(monitoringFundOperationsTOSearch.getAccountType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT), 
						PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));
				JSFUtilities.showValidationDialog();
				return;
			}
			
			cleanDetailData();
			try{
				processDay = CommonsUtilities.currentDate();
				//GET CASH ACCOUNT GROUPED BY CASH ACCOUNT TYPE
				institutionCashAccountLst = monitorCashAccountFacade.searchInstitutionCashAccounts(monitoringFundOperationsTOSearch);
				if(Validations.validateListIsNotNullAndNotEmpty(institutionCashAccountLst)){
					blNoResult = false;
					institutionCashAccountDmodel = new GenericDataModel<InstitutionCashAccount>(institutionCashAccountLst);
				}else{
					institutionCashAccountDmodel = null;
					blNoResult = true;
				}
				if(AccountCashFundsType.CENTRALIZING.getCode().equals(monitoringFundOperationsTOSearch.getAccountType().intValue())
						||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(monitoringFundOperationsTOSearch.getAccountType().intValue()))
					blCentral = true;
			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * Clean detail data.
	 */
	public void cleanDetailData()
	{
		indShowParticipantLst= GeneralConstants.ZERO_VALUE_INTEGER;
		indShowRightFeesLst= GeneralConstants.ZERO_VALUE_INTEGER;
		indShowSettlementLst= GeneralConstants.ZERO_VALUE_INTEGER;
		institutionCashAccountDmodelChild=null;
		institutionCashAccountDetailDmodel=null;
		institutionCashAccountDetail=null;
		blNoResult=false;
		blCentral=false;
	}
	
	/**
	 * Search detail.
	 *
	 * @param option the option
	 */
	@LoggerAuditWeb
	public void searchDetail(boolean option)
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			InstitutionCashAccount accountSelected = new InstitutionCashAccount();
			if(option){				
				accountSelected = institutionCashAccountDmodelChild.getRowData();
				institutionCashAccountDetailDmodel = null;
			} else {
				accountSelected = institutionCashAccountDmodel.getRowData();				
				institutionCashAccountDmodelChild = null;
				institutionCashAccountDetailDmodel = null;
			}
			Integer cashAccountType = accountSelected.getAccountType();
			Integer currency = accountSelected.getCurrency();
			if(ACCOUNT_CENTALIZING.equals(cashAccountType)){
				List<InstitutionCashAccount> lstDetail = monitorCashAccountFacade.getAccountTypeRelatedCentral(currency);
				if(Validations.validateListIsNotNullAndNotEmpty(lstDetail)){
					institutionCashAccountDmodelChild = new GenericDataModel<InstitutionCashAccount>(lstDetail);				
				}else{
					institutionCashAccountDmodelChild = null;
				}				
			}else{
				
				List<InstitutionCashAccount> lstDetail = monitorCashAccountFacade.getMonitoringCashAccountDetail(cashAccountType,currency,
						monitoringFundOperationsTOSearch.getParticipantSelected(),
						monitoringFundOperationsTOSearch.getIssuer());
				institutionCashAccountDetailDmodel = new GenericDataModel<InstitutionCashAccount>(lstDetail);
				if(AccountCashFundsType.BENEFIT.getCode().equals(cashAccountType)){
					indShowRightFeesLst = GeneralConstants.ONE_VALUE_INTEGER;
					indShowSettlementLst = GeneralConstants.ZERO_VALUE_INTEGER;
					indShowParticipantLst = GeneralConstants.ZERO_VALUE_INTEGER;
				}else if(AccountCashFundsType.SETTLEMENT.getCode().equals(cashAccountType)){
					indShowSettlementLst = GeneralConstants.ONE_VALUE_INTEGER;
					indShowRightFeesLst = GeneralConstants.ZERO_VALUE_INTEGER;
					indShowParticipantLst = GeneralConstants.ZERO_VALUE_INTEGER;
				}else{
					indShowParticipantLst = GeneralConstants.ONE_VALUE_INTEGER;
					indShowRightFeesLst = GeneralConstants.ZERO_VALUE_INTEGER;
					indShowSettlementLst = GeneralConstants.ZERO_VALUE_INTEGER;
				}
				institutionCashAccountDetail= new InstitutionCashAccount();
				institutionCashAccountDetail.setCurrency(accountSelected.getCurrency());
			}		
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * Clean.
	 */
	public void clean(){		
		JSFUtilities.resetViewRoot();
		monitoringFundOperationsTOSearch = new MonitoringFundOperationsTO();
		institutionCashAccountLst=null;
		institutionCashAccountDmodel=null;
		cleanDetailData();
	}

	/**
	 * Clean tables.
	 */
	public void cleanTables(){
		institutionCashAccountDmodelChild = null;
		institutionCashAccountDetailDmodel = null;
		institutionCashAccountDmodel = null;
	}
	
	/**
	 * On change account type.
	 */
	public void onChangeAccountType()
	{
		blParticipant=false;
		blReturns=false;
		blIssuer=false;
		blNoResult=false;
		blCentral=false;
		if(Validations.validateIsNotNull(monitoringFundOperationsTOSearch))
		{
			monitoringFundOperationsTOSearch.setParticipantSelected(null);
			monitoringFundOperationsTOSearch.setIssuer(null);
		}
		if (AccountCashFundsType.SETTLEMENT.getCode().equals(monitoringFundOperationsTOSearch.getAccountType().intValue())||
				AccountCashFundsType.GUARANTEES.getCode().equals(monitoringFundOperationsTOSearch.getAccountType().intValue()))
		{
			getParticipants(false);
			blParticipant=true;
		}
		else if(AccountCashFundsType.BENEFIT.getCode().equals(monitoringFundOperationsTOSearch. getAccountType().intValue()))
		{
			getIssuers();
			getParticipants(false);
			blReturns=true;
			monitoringFundOperationsTOSearch.setIssuer(new Issuer());
			fillInstitutionBankAccountsTypeReturn();
		}
		else if(AccountCashFundsType.RETURN.getCode().equals(monitoringFundOperationsTOSearch. getAccountType().intValue()))
		{
			getIssuers();
			getParticipants(false);
			blReturns=true;
			monitoringFundOperationsTOSearch.setIssuer(new Issuer());
			fillInstitutionBankAccountsTypeReturn();
		}
		cleanTables();
	}
	
	/**
	 * BEFOREE OPEN DAY
	 * 
	 * THIS METHOD IS EXECUTE TO VALIDATE IF THERES 
	 * ANOTHER DAY OPENED AND IT SHOWS THE CONFIRM MESSAGE IF EVERYTHING IS OK *.
	 */
	
	public void beforeOpenDay(){
		JSFUtilities.hideGeneralDialogues();
		try{
			Date previousDate = holidayService.getCalculateDate(processDay, 1, 0,null);
			BeginEndDay beginEndPreviousDay = monitorCashAccountFacade.searchBeginEnd(previousDate);
			if(Validations.validateIsNotNull(beginEndPreviousDay)){
				//VALIDATE IF THE PREVIOUS DAY HAS BEEN CLOSED
				if(BeginEndDaySituationType.CLOSE.getCode().equals(beginEndPreviousDay.getIndSituation())){
					BeginEndDay beginEndToday = monitorCashAccountFacade.searchBeginEnd(processDay);
					//WE VALIDATE IF EXIST THE BEGIN END DAY PROCESS FOR TODAY
					if(Validations.validateIsNull(beginEndToday)){
						
						//Validate if the accounts related to the central account has amount
						List<InstitutionCashAccount> lstAccounts=monitorCashAccountFacade.getAccountsValidateBeginDay();
						if(lstAccounts!=null && lstAccounts.size()>0)
						{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_START_DAY_EXIST_AMOUNT_RELATEDCENTRALACCOUNT));
							JSFUtilities.showValidationDialog();
							return;
						}
						//Validate if the central account has amount
						else 
						{ 	Integer iAccountAmounts=monitorCashAccountFacade.validateCentralizingAccountAmounts();
							if(iAccountAmounts.equals(GeneralConstants.ZERO_VALUE_INTEGER))
							{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_START_DAY_EXIST_CENTRALACCOUNT_HASAMOUNT));
								JSFUtilities.showValidationDialog();
								return;
							}
							else if(iAccountAmounts.equals(GeneralConstants.ONE_VALUE_INTEGER))
							{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_START_DAY_NOT_EXIST_CENTRALACCOUNT));
								JSFUtilities.showValidationDialog();
								return;
							}
							else
							{
								
								try {
									// issue 1205: ahora es necesario validar que haya tipo de cambio dado que tambien se realizara la liquidacion BRUTA para compra forzosa
									if(generalParametersFacade.getdailyExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode())==null){
										throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_EXCHANGE_RATE);
									}
								} catch (ServiceException e) {
									showExceptionMessage(com.pradera.settlements.utils.PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
									JSFUtilities.showSimpleValidationDialog();
									return;
								}
								
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
										PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_CASH_ACCOUNT_PREVIOUS_OPEN_DAY));
								showDialog(dialogBeginDay);
								return;
							}
						}
						
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_CASH_ACCOUNT_IS_NOT_CLOSED_TO_INIT));
						JSFUtilities.showValidationDialog();
						return;
					}
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_CLOSED));
					JSFUtilities.showValidationDialog();
					return;
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_MONITORING_PREVIOUS_DAY_NOT_OPEN));
				JSFUtilities.showValidationDialog();
				return;
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * OPEN DAY
	 * THIS METHOD OPEN THE DAY *.
	 */
	@LoggerAuditWeb
	public void openDay(){
		JSFUtilities.hideGeneralDialogues();
		try{
			
			try {
				// issue 1205: ahora es necesario validar que haya tipo de cambio dado que tambien se realizara la liquidacion BRUTA para compra forzosa
				if(generalParametersFacade.getdailyExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode())==null){
					throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_EXCHANGE_RATE);
				}
			} catch (ServiceException e) {
				showExceptionMessage(com.pradera.settlements.utils.PropertiesConstants.MESSAGES_ALERT, null,e.getMessage(),e.getParams());
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			
			//Setting the Begin Day
			BeginEndDay beginEndDayToSave = new BeginEndDay();
			beginEndDayToSave.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			beginEndDayToSave.setProcessDay(processDay);
			monitorCashAccountFacade.beginDay(beginEndDayToSave);
			lastProcessDay = monitorCashAccountFacade.getLastProcessDay();
			lastOpenDay = monitorCashAccountFacade.getLastOpenDay();
			lastCloseDay = monitorCashAccountFacade.getLastCloseDay();
			
			BusinessProcess businessProcess = new BusinessProcess();
			Map<String, Object> details = new HashMap<String, Object>();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.SETTLEMENT_OPERATIONS_UPDATE.getCode());
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			
			//identify the forced purchase to BBV
			BusinessProcess businessProcess2= new BusinessProcess();
			Map<String, Object> details2 = new HashMap<String, Object>();
			businessProcess2.setIdBusinessProcessPk(BusinessProcessType.IDENTIFY_FORCED_PURCHASE.getCode());
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess2,details2);
			
			//we calculate the issuer amount about corporative events Fixed income
			BusinessProcess businessProcess3= new BusinessProcess();
			businessProcess3.setIdBusinessProcessPk(BusinessProcessType.ISSUER_AMOUNT_BENEFIT_PAYMENT_CALCULATION.getCode());
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess3, null);
			
			// se verifica que si o si acabe el proceso de identificacion de compra forzosa par ainiciar la liquidacion bruta
			int n=0;
			do{
				System.out.println("::: verificando finaliacion del PROCESO DE IDENTIFICACION DE COMPRAS FORZOSAS intento nro: "+(++n));
			}while(!monitorProcessFacade.finishProcessForcedPurchaseIdentification());
			// issue 1205: Ejecutar Liquidacion Bruta en caso de existir compras forzozas
			// este fragmento de codigo esta basado en la liquidacion bruta, ver los siguientes metodos:
			// MonitoringSettlementProcessMgmtBean.searchSettlementProcesses() (hace el search)
			// MonitoringSettlementProcessMgmtBean.beginGrossSettlementProcess() (ejecuta la liquidacion bruta)
			List<ModalitySettlementTO> modalitySettlementListToSendNotification=new ArrayList<>();
			List<Integer> lstCurrency=new ArrayList<>();
			lstCurrency.add(CurrencyType.PYG.getCode());
			lstCurrency.add(CurrencyType.USD.getCode());
			for(Integer currency:lstCurrency){
				MonitorSettlementTO monitorSettlement = new MonitorSettlementTO();
				monitorSettlement.setCurrencyPk(currency);
				monitorSettlement.setNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
				monitorSettlement.setSettlementDate(CommonsUtilities.currentDate());
				monitorSettlement.setSettlementSchemePk(SettlementSchemaType.GROSS.getCode());
				List<ModalitySettlementTO> modalitySettlementList = monitorProcessFacade.getModalitySettlementFacade(monitorSettlement);
				for(ModalitySettlementTO modSettlement:modalitySettlementList){
					Date settlementDate = monitorSettlement.getSettlementDate();
					Integer currencyParam = monitorSettlement.getCurrencyPk();
					Long mechanismId = modSettlement.getMechanismId();
					Long modalityGroupId = modSettlement.getModalityGroupId();
					Integer settlementSchema = modSettlement.getSettlementSchemeId();
					
					if(settlementSchema.equals(SettlementSchemaType.GROSS.getCode())){//If settlement process is GROSS- Bruta
					
						//validate if exist some settlement process pendit of finish
						monitorProcessFacade.validateStartSettlementProcessFacade(mechanismId,settlementDate,modalityGroupId,settlementSchema,currencyParam);
						
						Map<String, Object> processParameters=new HashMap<String, Object>();
						processParameters.put(SettlementConstant.MECHANISM_PARAMETER, mechanismId);
						processParameters.put(SettlementConstant.CURRENCY_PARAMETER, currency);
						processParameters.put(SettlementConstant.MODALITY_GROUP_PARAMETER, modalityGroupId);
						//processParameters.put(SettlementConstant.SCHEDULE_TYPE, null);
						
						BusinessProcess objBusinessProcess=new BusinessProcess();
						objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GROSS_OPERATIONS_SETTLE_CASH_PART.getCode());	
						
						monitorProcessFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
					}
				}
				modalitySettlementListToSendNotification.addAll(modalitySettlementList);
			}
			
			// envio de notificacion de liq bruta a participantes vendedores y compradores
			List<Participant> lstPart = monitorProcessFacade.getLstParticipant();
			MechanismOperation mcn;
			StringBuilder numberOperatioAndSecuritiesToSell,numberOperatioAndSecuritiesToBuy;
			Participant psell,pbuy;
			for(Participant p : lstPart){
				numberOperatioAndSecuritiesToSell=new StringBuilder();
				numberOperatioAndSecuritiesToBuy=new StringBuilder();
				psell = null;
				pbuy = null;
				// enviar notificacion de liquidacion bruta a los participantes vendedores
				for (ModalitySettlementTO modSettlement : modalitySettlementListToSendNotification) {
					if (Validations.validateListIsNotNullAndNotEmpty(modSettlement.getLstSettledOperations())) {

						for (SettlementOperation so : modSettlement.getLstSettledOperations()) {
							if ( Validations.validateIsNotNull(so.getMechanismOperation())
								 && Validations.validateIsNotNullAndPositive(so.getMechanismOperation().getIdMechanismOperationPk()) ) {

								mcn = monitorProcessFacade.getMcnWithParticipantsAndSecurity(so.getMechanismOperation().getIdMechanismOperationPk());

								// vendedor
								if (mcn.getSellerParticipant().getIdParticipantPk().equals(p.getIdParticipantPk())) {
									psell = mcn.getSellerParticipant();
									numberOperatioAndSecuritiesToSell.append(mcn.getBallotNumber()).append("/").append(mcn.getSequential());
									numberOperatioAndSecuritiesToSell.append(" ").append(mcn.getSecurities().getIdSecurityCodePk()).append(",</br> ");
								}

								// comprador
								if (mcn.getBuyerParticipant().getIdParticipantPk().equals(p.getIdParticipantPk())) {
									pbuy = mcn.getBuyerParticipant();
									numberOperatioAndSecuritiesToBuy.append(mcn.getBallotNumber()).append("/").append(mcn.getSequential());
									numberOperatioAndSecuritiesToBuy.append(" ").append(mcn.getSecurities().getIdSecurityCodePk()).append(",</br> ");
								}
							}
						}
					}
				}
				
				//enviando al vendedor
				if(Validations.validateIsNotNullAndNotEmpty(numberOperatioAndSecuritiesToSell.toString()) && numberOperatioAndSecuritiesToSell.length() > 1
						&& Validations.validateIsNotNull(psell) && Validations.validateIsNotNullAndNotEmpty(psell.getDescription()) ){
					Object[] parameters = { psell.getDescription(), "</br>"+numberOperatioAndSecuritiesToSell.toString()};
					// envio de notificacion de liquidacion bruta al partipante vendedor
					BusinessProcess businessProcessNotification = new BusinessProcess();
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFY_GROSS_SETTLEMENT_TO_SELLER.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,
					psell.getIdParticipantPk(), parameters);
				}
				
				//enviando al comprador
				if(Validations.validateIsNotNullAndNotEmpty(numberOperatioAndSecuritiesToBuy.toString()) && numberOperatioAndSecuritiesToBuy.length() > 1
						&& Validations.validateIsNotNull(pbuy) && Validations.validateIsNotNullAndNotEmpty(pbuy.getDescription())	){
					Object[] parameters = { pbuy.getDescription(), "</br>"+numberOperatioAndSecuritiesToBuy.toString()};
					// envio de notificacion de liquidacion bruta al partipante vendedor
					BusinessProcess businessProcessNotification = new BusinessProcess();
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFY_GROSS_SETTLEMENT_TO_BUYER.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,
					pbuy.getIdParticipantPk(), parameters);
				}
			}
			
			// Start Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BEGIN_DAY_MANUAL.getCode());
			Object[] parameters = new Object[]{CommonsUtilities.convertDatetoString(beginEndDayToSave.getProcessDay())};
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,CommonsUtilities.convertDatetoString(beginEndDayToSave.getProcessDay()), parameters);
			// End Notification
			clean();
			hideDialog(dialogBeginDay);
			showDialog(idBeginDay);
			
			// Send Reports
			
			SettlementReportTO filter = new SettlementReportTO(); 
			filter.setDate(CommonsUtilities.convertDatetoString(processDay));
			List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.detailExpirationRepurchaseQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.DETAIL_EXPIRATION_REPURCHASE.getCode());
			}			
			
			// End Reports			
			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * THIS METHOD VALIDATE IF THE LAST DAY WAS OPENED TO THEN 
	 * CLOSE THE DAY AND SHOWS THE CONFIRM MESSAGE IF EVERYTHING IS OK *.
	 */
	public void beforeCloseDay(){
		JSFUtilities.hideGeneralDialogues();
		try{
			//WE GET THE BEGIN END DAY OBJECT TO VALIDATE IF EXIST OR ITS SITUATION
			BeginEndDay beginEndDay = monitorCashAccountFacade.searchBeginEnd(processDay);
			if(Validations.validateIsNotNull(beginEndDay)){
				if(BeginEndDaySituationType.OPEN.getCode().equals(beginEndDay.getIndSituation())){
					List<FundsOperation> lstFundsOperation = monitorCashAccountFacade.searchRegisteredFundsOperations(processDay);
					//WE VALIDATE IF EXIST OPERATIONS NOT CONFIRMED OR REJECTED
					if(Validations.validateListIsNotNullAndNotEmpty(lstFundsOperation)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATION_REGISTERED_ERROR));
						JSFUtilities.showValidationDialog();
						return;
					}else{
						//we verify if exists trading operations pending of settlement
						String strMessage= settlementProcessFacade.verifyMechanismPendingSettlementOperation(processDay);
						if (Validations.validateIsNotNullAndNotEmpty(strMessage)) {
							Object[] params= new Object[1];
							params[0]= strMessage;
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.EXISTS_PENDING_SETTLEMENT_OPERATIONS, params));
							JSFUtilities.showValidationDialog();
							return;
						} else {
							//VALIDATE IF EXIST FUND OPERATIONS IN OBSERVED STATE
							List<FundsOperation> lstObservedFundOperations = monitorCashAccountFacade.getObservedFundOperations(processDay);
							if(Validations.validateListIsNullOrEmpty(lstObservedFundOperations)){
								//Validating if efective accounts balances are greater than 0
								if(searchEfectiveAccountsBalance()){
									return;
								}else{
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
											, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_CONFIRM_CLOSE_DAY_MESSAGE));
									showDialog(wvCloseDay);
									return;
								}
							}else{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATION_OBSERVED_STATE));
								JSFUtilities.showValidationDialog();
							}
						}
					}
				}else{
					//IF THE DAY IS CLOSED, WE CANNOT DO IT AGAIN
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_CASH_ACCOUNT_IS_NOT_OPENED_TO_CLOSE));
					JSFUtilities.showValidationDialog();		
					return;
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_NOT_OPENED_TO_END));
				JSFUtilities.showValidationDialog();
				return;
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * THIS METHOD SEARCH ACCOUNTS WITH EFECTIVE BALANCES AND THEN SHOW ERROR *.
	 *
	 * @return the boolean
	 */
	public Boolean searchEfectiveAccountsBalance(){
		Boolean accountWithBalance = Boolean.FALSE;		
		try{
			//Searching efectives accounts
			institutionCashEfectiveAccountsWithoutBalances = this.monitorCashAccountFacade.searchEfectiveAccounts();			
			for (InstitutionCashAccount institutionCashAccountTmp : institutionCashEfectiveAccountsWithoutBalances) {
				if(institutionCashAccountTmp.getTotalAmount().compareTo(new BigDecimal(0)) > 0){
					
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_CONFIRM_EXISTAMOUNT_RELATEDCENTRAL));
					showDialog(wvCloseDay);
					/*
					 Object [] bodyData = {institutionCashAccountTmp.getAccountTypeDescription()};
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_CANT_CLOSE_DAY, bodyData));
					JSFUtilities.showValidationDialog();*/
					accountWithBalance = Boolean.TRUE;
					break;
				}
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return accountWithBalance;
	}
	
	/**
	 * METHOD WHICH WILL BE INCHARGE TO CLOSE THE DAY, DELIVER ALL THE AMOUNTS IN CASH ACCOUNTS TO SWIFT.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@LoggerAuditWeb
	public void endDay() throws IOException{
		//JSFUtilities.hideGeneralDialogues();
		try {
			hideDialog(wvCloseDay);
			monitorCashAccountFacade.endDay(processDay);
			
			//issue 1343 Automatizar batchero
			//monitorCashAccountFacade.registerUnblockBalanceSelidBatch();
			
			// Start Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CLOSE_DAY_MANUAL.getCode());
			Object[] parameters = new Object[]{CommonsUtilities.convertDatetoString(processDay)};
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,CommonsUtilities.convertDatetoString(processDay), parameters);
			// End Notification
			
			// Send Reports
			SettlementReportTO filter = new SettlementReportTO(); 
			filter.setInitialDate(CommonsUtilities.convertDatetoString(processDay));
			filter.setEndDate(CommonsUtilities.convertDatetoString(processDay));
			List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.settledOperationsQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				// Enviar solo para los participantes
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.SETTLED_OPERATIONS.getCode());
				
				// Enviar a los usuarios EDV
				List<String> lstSchema = new ArrayList<String>();
				for(SettlementReportTO objSettlementReportTO : lstSettlementReportTO) {
					if(!lstSchema.contains(objSettlementReportTO.getSchema())){
						lstSchema.add(objSettlementReportTO.getSchema());
					}					
				}							
				List<SettlementReportTO> lstSettlementReportTOEdv = new ArrayList<SettlementReportTO>();
				for(String strSchema : lstSchema) {
					SettlementReportTO filterEdv = new SettlementReportTO(); 
					filterEdv.setAllUser(Boolean.FALSE);
					filterEdv.setOnlyUser(Boolean.TRUE);
					filterEdv.setSchema(strSchema);
					filterEdv.setInitialDate(CommonsUtilities.convertDatetoString(processDay));
					filterEdv.setEndDate(CommonsUtilities.convertDatetoString(processDay));
					lstSettlementReportTOEdv.add(filterEdv);
				}				
				settlementReportFacade.sendReports(lstSettlementReportTOEdv, ReportIdType.SETTLED_OPERATIONS.getCode());				
			}			
			
			filter = new SettlementReportTO();
			filter.setInitialDate(CommonsUtilities.convertDatetoString(processDay));
			filter.setEndDate(CommonsUtilities.convertDatetoString(processDay));
			lstSettlementReportTO = settlementReportFacade.settledAdvanceOperationsQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){				
				SettlementReportTO objSettlementReportTO = new SettlementReportTO();
				objSettlementReportTO.setAllUser(lstSettlementReportTO.get(0).isAllUser());
				objSettlementReportTO.setOnlyUser(lstSettlementReportTO.get(0).isOnlyUser());
				objSettlementReportTO.setInitialDate(lstSettlementReportTO.get(0).getInitialDate());
				objSettlementReportTO.setEndDate(lstSettlementReportTO.get(0).getEndDate());				
				List<SettlementReportTO> lstSettlementReportTOEdv = new ArrayList<SettlementReportTO>();
				lstSettlementReportTOEdv.add(objSettlementReportTO);
				settlementReportFacade.sendReports(lstSettlementReportTOEdv, ReportIdType.SETTLED_ADVANCE_OPERATIONS.getCode());
			}
			// End Reports
			
			// Executed batch send interfaces BCB
			BusinessProcess businessProcess = new BusinessProcess();
			Map<String, Object> details = new HashMap<String, Object>();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.GENERATION_EXTERNAL_INTERFACES_BVD_BEM_TEM_TO_BCB.getCode());
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			// End executed batch send interfaces BCB
			
			clean();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_MONITORING_DAY_CLOSED_SUCCESS));
			showDialog(wvDayClosed);
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			showDialog(wvDayClosed);
		}
	}
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipant(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(objInstitutionBankAccountsType));
	}	
	
	/**
	 * Is Institution Type Issuer.
	 *
	 * @return validate Institution Type Issuer
	 */
	public boolean isIndInstitutionTypeIssuer(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.ISSUER.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Is Institution Type Bank.
	 *
	 * @return validate Institution Type Bank
	 */
	public boolean isIndInstitutionTypeBank(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.BANK.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Fill institution bank accounts type.
	 */
	public void fillInstitutionBankAccountsTypeReturn(){
		lstInstitutionBankAccountsTypeReturn = new ArrayList<ParameterTable>();
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setIndicator4(GeneralConstants.ONE_VALUE_INTEGER);
		try {
			lstInstitutionBankAccountsTypeReturn.addAll(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
	}
	
	public void onChangeInstitutionType(){
		JSFUtilities.hideGeneralDialogues();
		//registerDepositFundsTO.setLstCashAccountDetail(null);
		monitoringFundOperationsTOSearch.setParticipantSelected(null);
		monitoringFundOperationsTOSearch.setIssuer(new Issuer());
		//this.setBlCorporativeProcessReturns(false);
	}
	
	/**
	 * Gets the participants.
	 *
	 * @param blParameter the bl parameter
	 * @return the participants
	 */
	public void getParticipants(boolean blParameter)
	{
		try {
			lstParticipants = monitorCashAccountFacade.getParticipants(blParameter);
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * Gets the issuers.
	 *
	 * @return the issuers
	 */
	public void getIssuers()
	{
		try{
			if (lstIssuers == null) {
				lstIssuers = monitorCashAccountFacade.getIssuers();
			}
		}catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Hide dialog.
	 *
	 * @param dialog the dialog
	 */
	public void hideDialog(String dialog){
		JSFUtilities.executeJavascriptFunction("PF('" + dialog +"').hide();");	
	}
	
	/**
	 * Show dialog.
	 *
	 * @param dialog the dialog
	 */
	public void showDialog(String dialog){	
		JSFUtilities.executeJavascriptFunction("PF('"+dialog +"').show();");
	}
	
	/**
	 * Gets the monitoring fund operations to search.
	 *
	 * @return the monitoring fund operations to search
	 */
	public MonitoringFundOperationsTO getMonitoringFundOperationsTOSearch() {
		return monitoringFundOperationsTOSearch;
	}




	/**
	 * Sets the monitoring fund operations to search.
	 *
	 * @param monitoringFundOperationsTOSearch the new monitoring fund operations to search
	 */
	public void setMonitoringFundOperationsTOSearch(
			MonitoringFundOperationsTO monitoringFundOperationsTOSearch) {
		this.monitoringFundOperationsTOSearch = monitoringFundOperationsTOSearch;
	}




	/**
	 * Gets the accounts fund types.
	 *
	 * @return the accounts fund types
	 */
	public List<ParameterTable> getAccountsFundTypes() {
		return accountsFundTypes;
	}




	/**
	 * Sets the accounts fund types.
	 *
	 * @param accountsFundTypes the new accounts fund types
	 */
	public void setAccountsFundTypes(List<ParameterTable> accountsFundTypes) {
		this.accountsFundTypes = accountsFundTypes;
	}




	/**
	 * Gets the currency types.
	 *
	 * @return the currency types
	 */
	public List<ParameterTable> getCurrencyTypes() {
		return currencyTypes;
	}




	/**
	 * Sets the currency types.
	 *
	 * @param currencyTypes the new currency types
	 */
	public void setCurrencyTypes(List<ParameterTable> currencyTypes) {
		this.currencyTypes = currencyTypes;
	}




	/**
	 * Gets the last process day.
	 *
	 * @return the last process day
	 */
	public Date getLastProcessDay() {
		return lastProcessDay;
	}




	/**
	 * Sets the last process day.
	 *
	 * @param lastProcessDay the new last process day
	 */
	public void setLastProcessDay(Date lastProcessDay) {
		this.lastProcessDay = lastProcessDay;
	}
	
	public Date getLastOpenDay() {
		return lastOpenDay;
	}

	public void setLastOpenDay(Date lastOpenDay) {
		this.lastOpenDay = lastOpenDay;
	}

	public Date getLastCloseDay() {
		return lastCloseDay;
	}

	public void setLastCloseDay(Date lastCloseDay) {
		this.lastCloseDay = lastCloseDay;
	}

	/**
	 * Gets the monitor cash account facade.
	 *
	 * @return the monitor cash account facade
	 */
	public MonitoringFundOperationsServiceFacade getMonitorCashAccountFacade() {
		return monitorCashAccountFacade;
	}

	/**
	 * Sets the monitor cash account facade.
	 *
	 * @param monitorCashAccountFacade the new monitor cash account facade
	 */
	public void setMonitorCashAccountFacade(
			MonitoringFundOperationsServiceFacade monitorCashAccountFacade) {
		this.monitorCashAccountFacade = monitorCashAccountFacade;
	}

	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}

	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Checks if is bl returns.
	 *
	 * @return true, if is bl returns
	 */
	public boolean isBlReturns() {
		return blReturns;
	}
	
	/**
	 * Sets the bl returns.
	 *
	 * @param blReturns the new bl returns
	 */
	public void setBlReturns(boolean blReturns) {
		this.blReturns = blReturns;
	}


	/**
	 * Checks if is bl issuer.
	 *
	 * @return true, if is bl issuer
	 */
	public boolean isBlIssuer() {
		return blIssuer;
	}

	/**
	 * Sets the bl issuer.
	 *
	 * @param blIssuer the new bl issuer
	 */
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}

	/**
	 * Gets the process day.
	 *
	 * @return the process day
	 */
	public Date getProcessDay() {
		return processDay;
	}

	/**
	 * Sets the process day.
	 *
	 * @param processDay the new process day
	 */
	public void setProcessDay(Date processDay) {
		this.processDay = processDay;
	}

	/**
	 * Gets the institution cash account lst.
	 *
	 * @return the institution cash account lst
	 */
	public List<InstitutionCashAccount> getInstitutionCashAccountLst() {
		return institutionCashAccountLst;
	}

	/**
	 * Sets the institution cash account lst.
	 *
	 * @param institutionCashAccountLst the new institution cash account lst
	 */
	public void setInstitutionCashAccountLst(
			List<InstitutionCashAccount> institutionCashAccountLst) {
		this.institutionCashAccountLst = institutionCashAccountLst;
	}

	/**
	 * Gets the institution cash account dmodel.
	 *
	 * @return the institution cash account dmodel
	 */
	public GenericDataModel<InstitutionCashAccount> getInstitutionCashAccountDmodel() {
		return institutionCashAccountDmodel;
	}

	/**
	 * Sets the institution cash account dmodel.
	 *
	 * @param institutionCashAccountDmodel the new institution cash account dmodel
	 */
	public void setInstitutionCashAccountDmodel(
			GenericDataModel<InstitutionCashAccount> institutionCashAccountDmodel) {
		this.institutionCashAccountDmodel = institutionCashAccountDmodel;
	}

	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}

	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}

	/**
	 * Checks if is bl central.
	 *
	 * @return true, if is bl central
	 */
	public boolean isBlCentral() {
		return blCentral;
	}

	/**
	 * Sets the bl central.
	 *
	 * @param blCentral the new bl central
	 */
	public void setBlCentral(boolean blCentral) {
		this.blCentral = blCentral;
	}

	/**
	 * Gets the account centalizing.
	 *
	 * @return the account centalizing
	 */
	public Integer getACCOUNT_CENTALIZING() {
		return ACCOUNT_CENTALIZING;
	}

	/**
	 * Sets the account centalizing.
	 *
	 * @param aCCOUNT_CENTALIZING the new account centalizing
	 */
	public void setACCOUNT_CENTALIZING(Integer aCCOUNT_CENTALIZING) {
		ACCOUNT_CENTALIZING = aCCOUNT_CENTALIZING;
	}

	/**
	 * Gets the account retentions.
	 *
	 * @return the account retentions
	 */
	public Integer getACCOUNT_RETENTIONS() {
		return ACCOUNT_RETENTIONS;
	}

	/**
	 * Sets the account retentions.
	 *
	 * @param aCCOUNT_RETENTIONS the new account retentions
	 */
	public void setACCOUNT_RETENTIONS(Integer aCCOUNT_RETENTIONS) {
		ACCOUNT_RETENTIONS = aCCOUNT_RETENTIONS;
	}

	/**
	 * Gets the return account.
	 *
	 * @return the return account
	 */
	public Integer getRETURN_ACCOUNT() {
		return RETURN_ACCOUNT;
	}

	/**
	 * Sets the return account.
	 *
	 * @param rETURN_ACCOUNT the new return account
	 */
	public void setRETURN_ACCOUNT(Integer rETURN_ACCOUNT) {
		RETURN_ACCOUNT = rETURN_ACCOUNT;
	}

	/**
	 * Gets the account rates.
	 *
	 * @return the account rates
	 */
	public Integer getACCOUNT_RATES() {
		return ACCOUNT_RATES;
	}

	/**
	 * Sets the account rates.
	 *
	 * @param aCCOUNT_RATES the new account rates
	 */
	public void setACCOUNT_RATES(Integer aCCOUNT_RATES) {
		ACCOUNT_RATES = aCCOUNT_RATES;
	}

	/**
	 * Gets the institution cash account dmodel child.
	 *
	 * @return the institution cash account dmodel child
	 */
	public GenericDataModel<InstitutionCashAccount> getInstitutionCashAccountDmodelChild() {
		return institutionCashAccountDmodelChild;
	}

	/**
	 * Sets the institution cash account dmodel child.
	 *
	 * @param institutionCashAccountDmodelChild the new institution cash account dmodel child
	 */
	public void setInstitutionCashAccountDmodelChild(
			GenericDataModel<InstitutionCashAccount> institutionCashAccountDmodelChild) {
		this.institutionCashAccountDmodelChild = institutionCashAccountDmodelChild;
	}

	/**
	 * Gets the institution cash account detail dmodel.
	 *
	 * @return the institution cash account detail dmodel
	 */
	public GenericDataModel<InstitutionCashAccount> getInstitutionCashAccountDetailDmodel() {
		return institutionCashAccountDetailDmodel;
	}

	/**
	 * Sets the institution cash account detail dmodel.
	 *
	 * @param institutionCashAccountDetailDmodel the new institution cash account detail dmodel
	 */
	public void setInstitutionCashAccountDetailDmodel(
			GenericDataModel<InstitutionCashAccount> institutionCashAccountDetailDmodel) {
		this.institutionCashAccountDetailDmodel = institutionCashAccountDetailDmodel;
	}

	/**
	 * Gets the institution cash account detail.
	 *
	 * @return the institution cash account detail
	 */
	public InstitutionCashAccount getInstitutionCashAccountDetail() {
		return institutionCashAccountDetail;
	}

	/**
	 * Sets the institution cash account detail.
	 *
	 * @param institutionCashAccountDetail the new institution cash account detail
	 */
	public void setInstitutionCashAccountDetail(
			InstitutionCashAccount institutionCashAccountDetail) {
		this.institutionCashAccountDetail = institutionCashAccountDetail;
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * Gets the before open day.
	 *
	 * @return the before open day
	 */
	public Date getBeforeOpenDay() {
		return beforeOpenDay;
	}

	/**
	 * Sets the before open day.
	 *
	 * @param beforeOpenDay the new before open day
	 */
	public void setBeforeOpenDay(Date beforeOpenDay) {
		this.beforeOpenDay = beforeOpenDay;
	}

	/**
	 * Gets the before close day.
	 *
	 * @return the before close day
	 */
	public Date getBeforeCloseDay() {
		return beforeCloseDay;
	}

	/**
	 * Sets the before close day.
	 *
	 * @param beforeCloseDay the new before close day
	 */
	public void setBeforeCloseDay(Date beforeCloseDay) {
		this.beforeCloseDay = beforeCloseDay;
	}

	/**
	 * Gets the previous date.
	 *
	 * @return the previous date
	 */
	public Date getPreviousDate() {
		return previousDate;
	}

	/**
	 * Sets the previous date.
	 *
	 * @param previousDate the new previous date
	 */
	public void setPreviousDate(Date previousDate) {
		this.previousDate = previousDate;
	}

	/**
	 * Gets the account settlement.
	 *
	 * @return the account settlement
	 */
	public Integer getACCOUNT_SETTLEMENT() {
		return ACCOUNT_SETTLEMENT;
	}

	/**
	 * Sets the account settlement.
	 *
	 * @param aCCOUNT_SETTLEMENT the new account settlement
	 */
	public void setACCOUNT_SETTLEMENT(Integer aCCOUNT_SETTLEMENT) {
		ACCOUNT_SETTLEMENT = aCCOUNT_SETTLEMENT;
	}

	/**
	 * Gets the ind show right fees lst.
	 *
	 * @return the ind show right fees lst
	 */
	public Integer getIndShowRightFeesLst() {
		return indShowRightFeesLst;
	}

	/**
	 * Sets the ind show right fees lst.
	 *
	 * @param indShowRightFeesLst the new ind show right fees lst
	 */
	public void setIndShowRightFeesLst(Integer indShowRightFeesLst) {
		this.indShowRightFeesLst = indShowRightFeesLst;
	}



	/**
	 * Gets the ind show participant lst.
	 *
	 * @return the ind show participant lst
	 */
	public Integer getIndShowParticipantLst() {
		return indShowParticipantLst;
	}

	/**
	 * Sets the ind show participant lst.
	 *
	 * @param indShowParticipantLst the new ind show participant lst
	 */
	public void setIndShowParticipantLst(Integer indShowParticipantLst) {
		this.indShowParticipantLst = indShowParticipantLst;
	}

	/**
	 * Gets the ind show settlement lst.
	 *
	 * @return the ind show settlement lst
	 */
	public Integer getIndShowSettlementLst() {
		return indShowSettlementLst;
	}

	/**
	 * Sets the ind show settlement lst.
	 *
	 * @param indShowSettlementLst the new ind show settlement lst
	 */
	public void setIndShowSettlementLst(Integer indShowSettlementLst) {
		this.indShowSettlementLst = indShowSettlementLst;
	}

	/**
	 * Gets the institution cash efective accounts without balances.
	 *
	 * @return the institution cash efective accounts without balances
	 */
	public List<InstitutionCashAccount> getInstitutionCashEfectiveAccountsWithoutBalances() {
		return institutionCashEfectiveAccountsWithoutBalances;
	}

	/**
	 * Sets the institution cash efective accounts without balances.
	 *
	 * @param institutionCashEfectiveAccountsWithoutBalances the new institution cash efective accounts without balances
	 */
	public void setInstitutionCashEfectiveAccountsWithoutBalances(
			List<InstitutionCashAccount> institutionCashEfectiveAccountsWithoutBalances) {
		this.institutionCashEfectiveAccountsWithoutBalances = institutionCashEfectiveAccountsWithoutBalances;
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	/**
	 * Gets the lst issuers.
	 *
	 * @return the lst issuers
	 */
	public List<Issuer> getLstIssuers() {
		return lstIssuers;
	}

	/**
	 * Sets the lst issuers.
	 *
	 * @param lstIssuers the new lst issuers
	 */
	public void setLstIssuers(List<Issuer> lstIssuers) {
		this.lstIssuers = lstIssuers;
	}

	/**
	 * Search issuer handler register.
	 */
	public void searchIssuerHandlerRegister(){
		executeAction();	
		if(Validations.validateIsNotNullAndNotEmpty(monitoringFundOperationsTOSearch.getIssuer()) 
				&& Validations.validateIsNotNullAndNotEmpty(monitoringFundOperationsTOSearch.getIssuer().getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageEffectiveAccountsFacade.findIssuerFacade(monitoringFundOperationsTOSearch.getIssuer().getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){	
				monitoringFundOperationsTOSearch.setIssuer(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			monitoringFundOperationsTOSearch.setIssuer(issuerTemp);
		}
		cleanTables();
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	public List<ParameterTable> getLstInstitutionBankAccountsTypeReturn() {
		return lstInstitutionBankAccountsTypeReturn;
	}

	public void setLstInstitutionBankAccountsTypeReturn(List<ParameterTable> lstInstitutionBankAccountsTypeReturn) {
		this.lstInstitutionBankAccountsTypeReturn = lstInstitutionBankAccountsTypeReturn;
	}
	
	/**
	 * Gets the obj institution bank accounts type.
	 *
	 * @return the obj institution bank accounts type
	 */
	public Integer getObjInstitutionBankAccountsType() {
		return objInstitutionBankAccountsType;
	}

	/**
	 * Sets the obj institution bank accounts type.
	 *
	 * @param objInstitutionBankAccountsType the new obj institution bank accounts type
	 */
	public void setObjInstitutionBankAccountsType(
			Integer objInstitutionBankAccountsType) {
		this.objInstitutionBankAccountsType = objInstitutionBankAccountsType;
	}

	
}