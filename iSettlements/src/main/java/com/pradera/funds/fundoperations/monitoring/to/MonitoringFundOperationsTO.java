package com.pradera.funds.fundoperations.monitoring.to;

import java.io.Serializable;

import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitoringFundOperationsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class MonitoringFundOperationsTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The account type. */
	private Long accountType;
	
	/** The currency. */
	private Long currency;
	
	/** The participant selected. */
	private Long participantSelected;
	
	/** The issuer. */
	private Issuer issuer;
	
	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public Long getAccountType() {
		return accountType;
	}
	
	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Long getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Long currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	

	
	
}
