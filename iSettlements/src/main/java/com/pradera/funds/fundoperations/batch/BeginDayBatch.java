package com.pradera.funds.fundoperations.batch;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.fundoperations.monitoring.facade.MonitoringFundOperationsServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.core.facade.SettlementProcessFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BeginDayBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@RequestScoped
@BatchProcess(name="BeginDayBatch")
public class BeginDayBatch implements JobExecution{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(BeginDayBatch.class);
	
	/** The monitoring fund operations service facade. */
	@EJB
	private MonitoringFundOperationsServiceFacade monitoringFundOperationsServiceFacade; 
	
	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	
	private static int PROCESS_DAYS= 5; 
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {		
		logger.debug("BeginDayBatch: Starting the process ...");
		String strMessage= null;
		Date processDay= CommonsUtilities.currentDate();		
		try {
			strMessage= monitoringFundOperationsServiceFacade.validateBeginDay(processDay);
			if (Validations.validateIsNull(strMessage)) {
				//we start up the institutions cash accounts
				monitoringFundOperationsServiceFacade.startBeginDay(processDay);
				
				// we change the currency of UFV and MVD settlement operation
				settlementProcessFacade.updateSettlementExchangeRateOperations(processDay);
				
				//we identify the settlement forced purchases
				settlementProcessFacade.identifyForcedPurchase(processDay);
				
				//we calculate the issuer amount about corporative events Fixed income
				Date expirationDate = CommonsUtilities.addDate(CommonsUtilities.currentDate(), PROCESS_DAYS);
				settlementProcessFacade.calculateIssuerAmountAutomatic(expirationDate);
			}
		} catch (Exception e) {
			logger.error(strMessage);
			e.printStackTrace();
			String messageNotification = null;
			if (e instanceof ServiceException) {
				ServiceException se = (ServiceException) e;
				messageNotification = PropertiesUtilities.getExceptionMessage(se.getMessage(), se.getParams(),new Locale("es"));
			} else {
				messageNotification = "OCURRIO UN ERROR AL INICAR EL DIA, FAVOR REVISAR BITACORA DE PROCESOS";
			}
			
			UserFilterTO  userFilter = new UserFilterTO();
			userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());			
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);						
			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, messageNotification, NotificationType.EMAIL);
			
		} 
		
		logger.info("BeginDayBatch: Finishing the process ...");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

	
}
