package com.pradera.funds.effective.accounts.view;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InternationalOperationsAccount.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class InternationalOperationsAccount implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The participant selected. */
	private Integer participantSelected;
	
	/** The participant pk. */
	private Long participantPK;// REFERENTIAL OBJECT FOR PARTICIPANT PK
	
	/** The lst bank. */
	private List<Bank> lstBank;
	
	/** The bank selected. */
	private Integer bankSelected;
	
	/** The lst bank account type. */
	private List<ParameterTable> lstBankAccountType;
	
	/** The bank account type. */
	private Integer bankAccountType;
	
	/** The bank type. */
	private Integer bankType;
	
	/** The lst bank type. */
	private List<ParameterTable> lstBankType;
	
	/** The state. */
	private String state;
	
	/** The bank account pk. */
	private Long bankAccountPK;
	
	/** The lst bank accounts to data model. */
	private GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel;
	
	/** The institution bank account session. */
	private InstitutionBankAccount institutionBankAccountSession;

	// This variable is used to set the participant description in modificcation
	/** The str participant selected. */
	// screen.
	private Integer strParticipantSelected;

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Integer getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Integer participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the bank selected.
	 *
	 * @return the bank selected
	 */
	public Integer getBankSelected() {
		return bankSelected;
	}

	/**
	 * Sets the bank selected.
	 *
	 * @param bankSelected the new bank selected
	 */
	public void setBankSelected(Integer bankSelected) {
		this.bankSelected = bankSelected;
	}



	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return bankAccountType;
	}

	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}


	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the participant pk.
	 *
	 * @return the participant pk
	 */
	public Long getParticipantPK() {
		return participantPK;
	}

	/**
	 * Sets the participant pk.
	 *
	 * @param participantPK the new participant pk
	 */
	public void setParticipantPK(Long participantPK) {
		this.participantPK = participantPK;
	}

	/**
	 * Gets the str participant selected.
	 *
	 * @return the strParticipantSelected
	 */
	public Integer getStrParticipantSelected() {
		return strParticipantSelected;
	}

	/**
	 * Sets the str participant selected.
	 *
	 * @param strParticipantSelected            the strParticipantSelected to set
	 */
	public void setStrParticipantSelected(Integer strParticipantSelected) {
		this.strParticipantSelected = strParticipantSelected;
	}
	
	/**
	 * Gets the bank account pk.
	 *
	 * @return the bank account pk
	 */
	public Long getBankAccountPK() {
		return bankAccountPK;
	}
	
	/**
	 * Sets the bank account pk.
	 *
	 * @param bankAccountPK the new bank account pk
	 */
	public void setBankAccountPK(Long bankAccountPK) {
		this.bankAccountPK = bankAccountPK;
	}

	/**
	 * Gets the bank type.
	 *
	 * @return the bank type
	 */
	public Integer getBankType() {
		return bankType;
	}

	/**
	 * Sets the bank type.
	 *
	 * @param bankType the new bank type
	 */
	public void setBankType(Integer bankType) {
		this.bankType = bankType;
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 */
	public List<Bank> getLstBank() {
		return lstBank;
	}

	/**
	 * Sets the lst bank.
	 *
	 * @param lstBank the new lst bank
	 */
	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}

	/**
	 * Gets the lst bank account type.
	 *
	 * @return the lst bank account type
	 */
	public List<ParameterTable> getLstBankAccountType() {
		return lstBankAccountType;
	}

	/**
	 * Sets the lst bank account type.
	 *
	 * @param lstBankAccountType the new lst bank account type
	 */
	public void setLstBankAccountType(List<ParameterTable> lstBankAccountType) {
		this.lstBankAccountType = lstBankAccountType;
	}

	/**
	 * Gets the lst bank type.
	 *
	 * @return the lst bank type
	 */
	public List<ParameterTable> getLstBankType() {
		return lstBankType;
	}

	/**
	 * Sets the lst bank type.
	 *
	 * @param lstBankType the new lst bank type
	 */
	public void setLstBankType(List<ParameterTable> lstBankType) {
		this.lstBankType = lstBankType;
	}

	/**
	 * Gets the lst bank accounts to data model.
	 *
	 * @return the lst bank accounts to data model
	 */
	public GenericDataModel<InstitutionBankAccount> getLstBankAccountsTODataModel() {
		return lstBankAccountsTODataModel;
	}

	/**
	 * Sets the lst bank accounts to data model.
	 *
	 * @param lstBankAccountsTODataModel the new lst bank accounts to data model
	 */
	public void setLstBankAccountsTODataModel(
			GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel) {
		this.lstBankAccountsTODataModel = lstBankAccountsTODataModel;
	}

	/**
	 * Gets the institution bank account session.
	 *
	 * @return the institution bank account session
	 */
	public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}

	/**
	 * Sets the institution bank account session.
	 *
	 * @param institutionBankAccountSession the new institution bank account session
	 */
	public void setInstitutionBankAccountSession(
			InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}


}