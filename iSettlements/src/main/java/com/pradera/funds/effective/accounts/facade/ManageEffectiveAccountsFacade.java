package com.pradera.funds.effective.accounts.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.component.swift.to.CashAccountsSearchTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.funds.bank.accounts.service.ManageBankAccountsService;
import com.pradera.funds.bank.accounts.to.SearchBankAccountsTO;
import com.pradera.funds.banks.service.ManageBanksService;
import com.pradera.funds.effective.accounts.service.ManageEffectiveAccountsService;
import com.pradera.funds.effective.accounts.to.BankAccounts;
import com.pradera.funds.effective.accounts.to.BankDataTO;
import com.pradera.funds.effective.accounts.to.CashAccountsTO;
import com.pradera.funds.effective.accounts.to.SearchCashDetailTO;
import com.pradera.funds.effective.accounts.view.BenefitPaymentAccount;
import com.pradera.funds.effective.accounts.view.CentralizingAccount;
import com.pradera.funds.effective.accounts.view.InternationalOperationsAccount;
import com.pradera.funds.effective.accounts.view.MarginGuaranteeAccount;
import com.pradera.funds.effective.accounts.view.RatesAccount;
import com.pradera.funds.effective.accounts.view.RegisterCashAccount;
import com.pradera.funds.effective.accounts.view.ReturnAccount;
import com.pradera.funds.effective.accounts.view.SettlerAccount;
import com.pradera.funds.effective.accounts.view.TaxesAccount;
import com.pradera.funds.fundsoperations.deposit.service.ManageDepositServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.DetailCashAccountStateType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageEffectiveAccountsFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageEffectiveAccountsFacade {

	/** Transaction Registry. */
	@Resource
   TransactionSynchronizationRegistry transactionRegistry;
	
	/** 	Manage Effective Accounts Service. */
	@EJB
	ManageEffectiveAccountsService manageEffectiveAccountsService;
	
	/** 	Cash Account Service. */
	@EJB
	CashAccountManagementService cashAccountService;
	
	/** The manage bank accounts service. */
	@EJB
	ManageBankAccountsService manageBankAccountsService;
	
	/** The entity service bean. */
	@Inject
	LoaderEntityServiceBean entityServiceBean;
	
	/** The deposit service. */
	@EJB
	ManageDepositServiceBean depositService;
	
	/** The idepositary setup. */
	@Inject 
	@DepositarySetup 
	IdepositarySetup idepositarySetup;
	
	/** The manage banks service. */
	@EJB
	ManageBanksService manageBanksService;
	
	/**
	 * Get Participants.
	 *
	 * @param blDirect account type
	 * @return  Participants
	 * @throws ServiceException the Service Exception
	 */
	public List<Participant> getParticipants(boolean blDirect)throws ServiceException{
		return cashAccountService.getParticipants(blDirect);
	}
	
	/**
	 * Get Participant Bic Code.
	 *
	 * @param partSelected id participant
	 * @return bic code
	 */
	public String getParticipantBicCode(Integer partSelected){
		String bicCode = cashAccountService.getParticipantBicCode(partSelected);
		return bicCode;
	}
	
	/**
	 * Get List Bank.
	 *
	 * @param idBankAccountClass Bank Account Class
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	public List<Bank> getListBank(Integer idBankAccountClass)throws ServiceException{
		return manageEffectiveAccountsService.getListBank(idBankAccountClass);
	}
	
	/**
	 * Get List Bank Accounts Filter.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return List Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	
	public List<NegotiationMechanism> getLstMechanism(Long idParticipantPk) throws ServiceException {
		return cashAccountService.getLstMechanism(idParticipantPk);
	}
	
	/**
	 * Search cash account.
	 *
	 * @param parameters the parameters
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EFFECTIVE_ACCOUNT_QUERY)
	public List<CashAccountsTO> searchCashAccount(CashAccountsSearchTO parameters){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<CashAccountsTO> lstReturn = new ArrayList<CashAccountsTO>();
		try{
			Map<Long, String> currencyMap = getMappingDescriptions(FundsType.MAP_DESCRIPTION_CURRENCY.getCode());
			Map<Long, String> mechanismMap = getMappingDescriptions(FundsType.MAP_DESCRIPTION_MECHANISM.getCode());
			Map<Long, String> modalityMap = getMappingDescriptions(FundsType.MAP_DESCRIPTION_MODALITY_GROUP.getCode());
			Map<Long, String> stateMap = getMappingDescriptions(FundsType.MAP_DESCRIPTION_STATE.getCode());

			List<InstitutionCashAccount> lstCash = cashAccountService.getCashAccounts(parameters);
			if (Validations.validateListIsNotNullAndNotEmpty(lstCash)){
				for(InstitutionCashAccount cashAccount: lstCash){
					CashAccountsTO bean = new CashAccountsTO();
					Integer accountType = parameters.getCashAccountType();
					bean.setCashAccountType(parameters.getCashAccountType());
					bean.setId(cashAccount.getIdInstitutionCashAccountPk().intValue());
					bean.setIdPk(Integer.valueOf(cashAccount.getIdInstitutionCashAccountPk().toString()));
					bean.setIdCashAccount(cashAccount.getIdInstitutionCashAccountPk());
					bean.setIndRelatedBcrd(cashAccount.getIndRelatedBcrd());					
					bean.setCurrency(currencyMap.get(Long.valueOf(cashAccount.getCurrency().toString())));					
					bean.setIdState(cashAccount.getAccountState());
					bean.setState(stateMap.get(Long.valueOf(cashAccount.getAccountState().toString())));
					
					if (AccountCashFundsType.GUARANTEES.getCode().equals(accountType)&& Validations.validateIsNotNullAndNotEmpty(cashAccount.getParticipant())){
						bean.setInstitution(cashAccount.getParticipant().getDisplayCodeMnemonic());
						bean.setIdParticipantFk(Integer.valueOf(cashAccount.getParticipant().getIdParticipantPk().toString()));
					}else if (AccountCashFundsType.BENEFIT.getCode().equals(accountType) && 
							(Validations.validateIsNotNullAndNotEmpty(cashAccount.getIssuer())
							|| Validations.validateIsNotNullAndNotEmpty(cashAccount.getParticipant()))){
						if(Validations.validateIsNotNullAndNotEmpty(cashAccount.getIssuer())) {
							bean.setInstitution(cashAccount.getIssuer().getCodeMnemonic());
						}else if (Validations.validateIsNotNullAndNotEmpty(cashAccount.getParticipant())){
							bean.setInstitution(cashAccount.getParticipant().getDisplayCodeMnemonic());
						}
					}else if (AccountCashFundsType.CENTRALIZING.getCode().equals(accountType)) {
						String bicCode = cashAccountService.getBcbBankBicValue(parameters.getCurrency());
						bean.setBicCode(bicCode);
						bean.setInstitution(new Participant(idepositarySetup.getIdParticipantDepositary()).getDescription());
					}else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(accountType)) {
						bean.setIdParticipantFk(Integer.valueOf(cashAccount.getParticipant().getIdParticipantPk().toString()));
						bean.setInstitution(cashAccount.getParticipant().getDisplayCodeMnemonic());						
					}else if (AccountCashFundsType.RATES.getCode().equals(accountType)) {
						bean.setInstitution(cashAccount.getParticipant().getDisplayCodeMnemonic());	
					}else if (AccountCashFundsType.RETURN.getCode().equals(accountType)) {
						if(Validations.validateIsNotNullAndNotEmpty(cashAccount.getIssuer())) {
							bean.setInstitution(cashAccount.getIssuer().getCodeMnemonic());
						}else if (Validations.validateIsNotNullAndNotEmpty(cashAccount.getParticipant())){
							bean.setInstitution(cashAccount.getParticipant().getDisplayCodeMnemonic());
						}						
					}else if (AccountCashFundsType.SETTLEMENT.getCode().equals(accountType)) {
						bean.setMechanism(mechanismMap.get(cashAccount.getNegotiationMechanism().getIdNegotiationMechanismPk()));
						bean.setModalityGroup(modalityMap.get(cashAccount.getModalityGroup().getIdModalityGroupPk()));
						bean.setInstitution(cashAccount.getParticipant().getDisplayCodeMnemonic());
						bean.setBicCode(cashAccount.getParticipant().getBicCode());
						if (SettlementSchemaType.GROSS.getCode().equals(cashAccount.getSettlementSchema()))
							bean.setSettlementSchema(SettlementSchemaType.GROSS.getValue());
						else if(SettlementSchemaType.NET.getCode().equals(cashAccount.getSettlementSchema()))
							bean.setSettlementSchema(SettlementSchemaType.NET.getValue());						
					}else if (AccountCashFundsType.TAX.getCode().equals(accountType)) {
						bean.setInstitution(cashAccount.getParticipant().getDisplayCodeMnemonic());							
					}					
					lstReturn.add(bean);
				}
			}
		}catch(ServiceException e){
			e.printStackTrace();
		}
		return lstReturn;
	}
	
	/**
	 * Gets the mapping descriptions.
	 *
	 * @param parameter the parameter
	 * @return the mapping descriptions
	 * @throws ServiceException the service exception
	 */
	public Map<Long, String> getMappingDescriptions(Integer parameter) throws ServiceException {
		Map<Long, String> mpResult = new HashMap<Long, String>();
		List<ParameterTable> lst = null;
		List<NegotiationMechanism> lstMechanism = null;
		List<ModalityGroup> lstModality = null;

		if (parameter.equals(FundsType.MAP_DESCRIPTION_CURRENCY.getCode())) {
			lst = cashAccountService
					.getParameterTableElements(MasterTableType.CURRENCY
							.getCode());
			if (lst != null && lst.size() > 0) {
				for (Iterator<ParameterTable> itr = lst.iterator(); itr
						.hasNext();) {
					ParameterTable pt = (ParameterTable) itr.next();
					mpResult.put(Long.valueOf(pt.getParameterTablePk()),
							pt.getParameterName());
				}
			}
		} else if (parameter.equals(FundsType.MAP_DESCRIPTION_MECHANISM
				.getCode())) {
			lstMechanism = cashAccountService.getMechanismElements();
			if (lstMechanism != null && lstMechanism.size() > 0) {
				for (Iterator<NegotiationMechanism> itr = lstMechanism
						.iterator(); itr.hasNext();) {
					NegotiationMechanism pt = (NegotiationMechanism) itr.next();
					mpResult.put(pt.getIdNegotiationMechanismPk(),
							pt.getDescription());
				}
			}
		} else if (parameter.equals(FundsType.MAP_DESCRIPTION_MODALITY_GROUP
				.getCode())) {
			lstModality = cashAccountService.getModalityElements();
			if (lstModality != null && lstModality.size() > 0) {
				for (Iterator<ModalityGroup> itr = lstModality.iterator(); itr
						.hasNext();) {
					ModalityGroup pt = (ModalityGroup) itr.next();
					mpResult.put(pt.getIdModalityGroupPk(), pt.getGroupName());
				}
			}
		} else if (parameter.equals(FundsType.MAP_DESCRIPTION_STATE.getCode())) {
			lst = cashAccountService
					.getParameterTableElements(MasterTableType.MASTER_TABLE_CASH_ACCOUNT_STATE
							.getCode());
			if (lst != null && lst.size() > 0) {
				for (Iterator<ParameterTable> itr = lst.iterator(); itr
						.hasNext();) {
					ParameterTable pt = (ParameterTable) itr.next();
					mpResult.put(Long.valueOf(pt.getParameterTablePk()),
							pt.getParameterName());
				}
			}
		} else if (parameter.equals(FundsType.MAP_DESCRIPTION_PARTICIPANT
				.getCode())) {
			mpResult = cashAccountService.getParticipantDescriptions();
		}

		return mpResult;
	}
	
	/**
	 * Find issuer facade.
	 *
	 * @param idIssuer the id issuer
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerFacade (String idIssuer)throws ServiceException{
		return cashAccountService.findIssuer(idIssuer);
	}
	
	/**
	 * Gets the BCB bank bic code.
	 *
	 * @return the BCB bank bic code
	 */
	public List<String> getBCBBankBicCode() {
		List<String> lstValues = new ArrayList<String>();
		Bank centralizerBank = cashAccountService.getCentralizerBank();
		if(Validations.validateIsNotNull(centralizerBank)){
			lstValues.add(centralizerBank.getBicCode());
			lstValues.add(centralizerBank.getDescription());
			lstValues.add(centralizerBank.getIdBankPk().toString());
		}else{
			lstValues=null;
		}
		return lstValues;
	}
	
	/**
	 * Gets the lst moda group.
	 *
	 * @param idNegoMechanismPkSelected the id nego mechanism pk selected
	 * @return the lst moda group
	 * @throws ServiceException the service exception
	 */
	public List<ModalityGroup> getLstModaGroup(Long idNegoMechanismPkSelected) throws ServiceException{
		return cashAccountService.getLstModaGroup(idNegoMechanismPkSelected);
	}
	
	/**
	 * Gets the lst moda group.
	 *
	 * @param idNegoMechanismPkSelected the id nego mechanism pk selected
	 * @param idParticipantPk the id participant pk
	 * @return the lst moda group
	 * @throws ServiceException the service exception
	 */
	public List<ModalityGroup> getLstModaGroup(Long idNegoMechanismPkSelected, Long idParticipantPk) throws ServiceException{
		return cashAccountService.getLstModaGroup(idNegoMechanismPkSelected, idParticipantPk);
	}
	
	/**
	 * Gets the confirmed commercial bank.
	 *
	 * @return the confirmed commercial bank
	 * @throws ServiceException the service exception
	 */
	public List<Bank> getConfirmedCommercialBank() throws ServiceException {
		return cashAccountService.getConfirmedCommercialBank();
	}
	
	public List<InstitutionBankAccount> getParticipantBankAccounts(Long participantSelected, Integer currency, Long idBankPk) throws ServiceException{
		return cashAccountService.getParticipantBankAccounts(participantSelected, currency,idBankPk);
	}
	
	/**
	 * Gets the participant bank account.
	 *
	 * @param participantSelected the participant selected
	 * @param currency the currency
	 * @param idBankPk the id bank pk
	 * @return the participant bank account
	 * @throws ServiceException the service exception
	 */
	public InstitutionBankAccount getParticipantBankAccount(Long participantSelected, Integer currency, Long idBankPk) throws ServiceException{
		return cashAccountService.getParticipantBankAccount(participantSelected, currency,idBankPk);
	}
	
	/**
	 * Gets the bank data.
	 *
	 * @param bankSelectedPk the bank selected pk
	 * @param currency the currency
	 * @return the bank data
	 * @throws ServiceException the service exception
	 */
	public BankDataTO getBankData(Integer bankSelectedPk, Integer currency) throws ServiceException {
		BankDataTO returnObj = new BankDataTO();
		Bank bank = cashAccountService.find(new Long(bankSelectedPk), Bank.class);
		Bank objCentralizingBank=manageBanksService.getBankForBankType(BankType.CENTRALIZING.getCode());
		List<InstitutionBankAccount> institutionLst = (List<InstitutionBankAccount>) cashAccountService	.getInstitutionBankAccountData(bank.getIdBankPk(), 
				currency, objCentralizingBank.getIdBankPk());
		if (institutionLst != null && institutionLst.size() > 0) {
			for (Iterator<InstitutionBankAccount> institutionItr = institutionLst.iterator(); institutionItr.hasNext();) {
				InstitutionBankAccount institutionBank = (InstitutionBankAccount) institutionItr.next();
				returnObj.setIdInstitutionBankAccountPk(institutionBank.getIdInstitutionBankAccountPk());
				returnObj.setBankAccountNumber(institutionBank.getAccountNumber());
				returnObj.setBicCode(bank.getBicCode());
				ParameterTable pm = cashAccountService.find(ParameterTable.class, new Integer(institutionBank.getBankAcountType().toString()));
				returnObj.setPkBankAccountType(institutionBank.getBankAcountType());
				returnObj.setBankAccountType(pm.getDescription());
				return returnObj;
			}
		}
		return null;		
	}
	
	/**
	 * Gets the BCB bank.
	 *
	 * @return the BCB bank
	 * @throws ServiceException the service exception
	 */
	public Bank getBCBBank() throws ServiceException{
		return cashAccountService.getBCBBank();
	}
	
	/**
	 * Gets the list bank accounts filter.
	 *
	 * @param searchBankAccountsTO the search bank accounts to
	 * @return the list bank accounts filter
	 * @throws ServiceException the service exception
	 */
	public List <InstitutionBankAccount> getListBankAccountsFilter(SearchBankAccountsTO searchBankAccountsTO)throws ServiceException{
		return manageBankAccountsService.getListBankAccountsFilter(searchBankAccountsTO);		
	}
	
	/**
	 * Save cash account.
	 *
	 * @param registerObject the register object
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EFFECTIVE_ACCOUNT_REGISTRATION)
	public void saveCashAccount(RegisterCashAccount registerObject) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		InstitutionCashAccount cashAccount = null;
		Integer instiCashAccountType = registerObject.getCashAccountTypeSelected();
		// ACCORDING TO THE CASH ACCOUNT TYPE, WE FILL THE ENTITY_CASH_ACCOUNT
		// OBJECT
		if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(instiCashAccountType)) {
			// FOR CASH ACCOUNTS INTERNATIONAL OPERATIONS, WE WILL ITERATE ALL PARTICIPANTS SELECTED
			InternationalOperationsAccount intOpeAccount = registerObject.getInternationalOperationsAccount();
			Long participantPK = Long.valueOf(intOpeAccount.getParticipantSelected());
			registerObject.getInternationalOperationsAccount().setParticipantPK(participantPK);
			cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);			
			cashAccountService.create(cashAccount);
			CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
			objCashAccountDetail.setInstitutionBankAccount(intOpeAccount.getInstitutionBankAccountSession());
			objCashAccountDetail.setInstitutionCashAccount(cashAccount);
			cashAccountService.create(objCashAccountDetail);
		} else if (AccountCashFundsType.BENEFIT.getCode().equals(instiCashAccountType)) {
			Integer bankAccountType = registerObject.getBenefitPaymentAccount().getTypeAccountSelected();
			if (BankType.COMMERCIAL.getCode().equals(bankAccountType)) {				
				cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, true);
				cashAccountService.create(cashAccount);
				CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
				objCashAccountDetail.setInstitutionBankAccount(registerObject.getBenefitPaymentAccount().getInstitutionBankAccount());
				objCashAccountDetail.setInstitutionCashAccount(cashAccount);
				cashAccountService.create(objCashAccountDetail);
			} else if (BankType.CENTRALIZING.getCode().equals(bankAccountType)) {				
				cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);
				cashAccountService.create(cashAccount);
				CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
				objCashAccountDetail.setInstitutionBankAccount(registerObject.getBenefitPaymentAccount().getInstitutionBankAccount());
				objCashAccountDetail.setInstitutionCashAccount(cashAccount);
				cashAccountService.create(objCashAccountDetail);
			}			
		} else if (AccountCashFundsType.SETTLEMENT.getCode().equals(instiCashAccountType)) {
			ModalityGroup[] modaTemp = null;
			Long idNegotitationMechanismPk = null;
			List<BankAccounts> lstBanks = registerObject.getSettlerAccount().getLstBankAccounts();
			for(int i = 1; i < 5; i++){
				if(i == 1){
					if(Validations.validateIsNotNull(registerObject.getSettlerAccount().getModaGroupBolsa()) 
						&& registerObject.getSettlerAccount().getModaGroupBolsa().length > 0){
						idNegotitationMechanismPk = NegotiationMechanismType.BOLSA.getCode();
						modaTemp = registerObject.getSettlerAccount().getModaGroupBolsa();
					}else
						continue;
				 }else if(i == 2){
					 if(Validations.validateIsNotNull(registerObject.getSettlerAccount().getModaGroupHacienda()) 
						&& registerObject.getSettlerAccount().getModaGroupHacienda().length > 0){
						idNegotitationMechanismPk = NegotiationMechanismType.HAC.getCode();
						modaTemp = registerObject.getSettlerAccount().getModaGroupHacienda();
					 }else
						continue;
				 }else if(i == 3){					 
					 if(Validations.validateIsNotNull(registerObject.getSettlerAccount().getModaGroupOTC()) 
						&& registerObject.getSettlerAccount().getModaGroupOTC().length > 0){
						idNegotitationMechanismPk = NegotiationMechanismType.OTC.getCode();
						modaTemp = registerObject.getSettlerAccount().getModaGroupOTC();
					 }else
						continue;
				 }else if(i == 4){
					 if(Validations.validateIsNotNull(registerObject.getSettlerAccount().getModaGroupBCB()) 
						&& registerObject.getSettlerAccount().getModaGroupBCB().length > 0){
						idNegotitationMechanismPk = NegotiationMechanismType.BC.getCode();
						modaTemp = registerObject.getSettlerAccount().getModaGroupBCB();
					 }else
						continue;
				 }
				 for(ModalityGroup modaGroup:modaTemp){
					 boolean validateMechanismModality = cashAccountService.validateMechanismModality(idNegotitationMechanismPk.toString(), modaGroup.getIdModalityGroupPk().toString());
					 if(validateMechanismModality){
					 	cashAccount = new InstitutionCashAccount();
						registerObject.getSettlerAccount().setMechanism(idNegotitationMechanismPk);
						registerObject.getSettlerAccount().setModalityGroup(modaGroup.getIdModalityGroupPk());
						cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);
						cashAccountService.create(cashAccount);
						for (Iterator<BankAccounts> itBank = lstBanks.iterator(); itBank.hasNext();) {
							BankAccounts bank = (BankAccounts) itBank.next();
							registerObject.getSettlerAccount().setUseType(getUseType(bank));
							registerObject.getSettlerAccount().setBankIterate(bank);
							//InstitutionBankAccount bankAccount = fillInstitutionBankAccount(instiCashAccountType, registerObject, false);
							CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,bank);
							InstitutionBankAccount institutionBankAccount = cashAccountService.searchInstitutionBankAccount(bank.getIdInstitutionBankAccountPk());
							objCashAccountDetail.setInstitutionBankAccount(institutionBankAccount);
							objCashAccountDetail.setInstitutionCashAccount(cashAccount);
							cashAccountService.create(objCashAccountDetail);
						}
					}
				 }
			}
		} else {
			if (AccountCashFundsType.CENTRALIZING.getCode().equals(instiCashAccountType)
					||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(instiCashAccountType)
					||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(instiCashAccountType)
					||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(instiCashAccountType)
					||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(instiCashAccountType)
					||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(instiCashAccountType)
					||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(instiCashAccountType)
					||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(instiCashAccountType)){
				cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);		
				cashAccountService.create(cashAccount);
				CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
				objCashAccountDetail.setInstitutionBankAccount(registerObject.getCentralizingAccount().getInstitutionBankAccountSession());
				objCashAccountDetail.setInstitutionCashAccount(cashAccount);
				cashAccountService.create(objCashAccountDetail);
				if(AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(instiCashAccountType)) {
					InstitutionCashAccount cashAccountTax = null;
					instiCashAccountType=AccountCashFundsType.TAX.getCode();
					cashAccountTax = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);
					cashAccountTax.setInstitutionCashAccount(cashAccount);
					cashAccountService.create(cashAccountTax);
					CashAccountDetail objCashAccountDetailTax= fillCashAccountDetail(instiCashAccountType,null);
					objCashAccountDetailTax.setInstitutionBankAccount(registerObject.getCentralizingAccount().getInstitutionBankAccountSession());
					objCashAccountDetailTax.setInstitutionCashAccount(cashAccountTax);
					cashAccountService.create(objCashAccountDetailTax);
				}else if(AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(instiCashAccountType)) {
					InstitutionCashAccount cashAccountGuarantees = null;
					instiCashAccountType=AccountCashFundsType.GUARANTEES.getCode();
					cashAccountGuarantees = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);
					cashAccountGuarantees.setInstitutionCashAccount(cashAccount);
					cashAccountService.create(cashAccountGuarantees);
					CashAccountDetail objCashAccountDetailGuarantees= fillCashAccountDetail(instiCashAccountType,null);
					objCashAccountDetailGuarantees.setInstitutionBankAccount(registerObject.getCentralizingAccount().getInstitutionBankAccountSession());
					objCashAccountDetailGuarantees.setInstitutionCashAccount(cashAccountGuarantees);
					cashAccountService.create(objCashAccountDetailGuarantees);
				}else if(AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(instiCashAccountType)) {
					InstitutionCashAccount cashAccountRates = null;
					instiCashAccountType=AccountCashFundsType.RATES.getCode();
					cashAccountRates = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);
					cashAccountRates.setInstitutionCashAccount(cashAccount);
					cashAccountService.create(cashAccountRates);
					CashAccountDetail objCashAccountDetailRates= fillCashAccountDetail(instiCashAccountType,null);
					objCashAccountDetailRates.setInstitutionBankAccount(registerObject.getCentralizingAccount().getInstitutionBankAccountSession());
					objCashAccountDetailRates.setInstitutionCashAccount(cashAccountRates);
					cashAccountService.create(objCashAccountDetailRates);
				}
			}
			else if (AccountCashFundsType.GUARANTEES.getCode().equals(instiCashAccountType))
			{
				cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);		
				cashAccountService.create(cashAccount);
				CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
				objCashAccountDetail.setInstitutionBankAccount(registerObject.getMarginGuaranteeAccount().getInstitutionBankAccountSession());
				objCashAccountDetail.setInstitutionCashAccount(cashAccount);
				cashAccountService.create(objCashAccountDetail);
			}
			else if (AccountCashFundsType.RATES.getCode().equals(instiCashAccountType))
			{
				cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);		
				cashAccountService.create(cashAccount);
				CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
				objCashAccountDetail.setInstitutionBankAccount(registerObject.getRatesAccount().getInstitutionBankAccountSession());
				objCashAccountDetail.setInstitutionCashAccount(cashAccount);
				cashAccountService.create(objCashAccountDetail);
			}
			else if (AccountCashFundsType.RETURN.getCode().equals(instiCashAccountType))
			{
				cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);		
				cashAccountService.create(cashAccount);
				CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
				CashAccountDetail objCentralCashAccountDetail=getCentralCashAccountDetail(cashAccount.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
				objCashAccountDetail.setInstitutionBankAccount(objCentralCashAccountDetail.getInstitutionBankAccount());
				objCashAccountDetail.setInstitutionCashAccount(cashAccount);
				cashAccountService.create(objCashAccountDetail);
			}
			else if (AccountCashFundsType.TAX.getCode().equals(instiCashAccountType))
			{
				cashAccount = fillInstitutionCashAccount(instiCashAccountType, registerObject, false);		
				cashAccountService.create(cashAccount);
				CashAccountDetail objCashAccountDetail= fillCashAccountDetail(instiCashAccountType,null);
				objCashAccountDetail.setInstitutionBankAccount(registerObject.getTaxesAccount().getInstitutionBankAccountSession());
				objCashAccountDetail.setInstitutionCashAccount(cashAccount);
				cashAccountService.create(objCashAccountDetail);
			}
		}
	}
	
	/**
	 * Fill cash account detail.
	 *
	 * @param cashAccountType the cash account type
	 * @param bank the bank
	 * @return the cash account detail
	 */
	public CashAccountDetail fillCashAccountDetail(Integer cashAccountType,BankAccounts bank)
	{
		CashAccountDetail objCashAccountDetail=new CashAccountDetail();
		if (AccountCashFundsType.CENTRALIZING.getCode().equals(cashAccountType)
				||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(cashAccountType)
				||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(cashAccountType)
				||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(cashAccountType)
				||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(cashAccountType)
				||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(cashAccountType)
				||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(cashAccountType)
				||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(cashAccountType)){
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			objCashAccountDetail.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
			objCashAccountDetail.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		}
		else if (AccountCashFundsType.SETTLEMENT.getCode().equals(cashAccountType))
		{
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			int indReception=BooleanType.NO.getCode(),indSending=BooleanType.NO.getCode();
			if(bank.getIndReception().equals(BooleanType.YES.getValue()))
				indReception=BooleanType.YES.getCode();
			if(bank.getIndSent().equals(BooleanType.YES.getValue()))
				indSending=BooleanType.YES.getCode();
			objCashAccountDetail.setIndReception(indReception);
			objCashAccountDetail.setIndSending(indSending);
		}
		else if (AccountCashFundsType.BENEFIT.getCode().equals(cashAccountType))
		{
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			objCashAccountDetail.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
			objCashAccountDetail.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		}
		else if (AccountCashFundsType.GUARANTEES.getCode().equals(cashAccountType)){
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			objCashAccountDetail.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
			objCashAccountDetail.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		}
		else if (AccountCashFundsType.RATES.getCode().equals(cashAccountType)){
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			objCashAccountDetail.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
			objCashAccountDetail.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		}
		else if (AccountCashFundsType.RETURN.getCode().equals(cashAccountType)){
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			objCashAccountDetail.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
			objCashAccountDetail.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		}
		else if (AccountCashFundsType.TAX.getCode().equals(cashAccountType)){
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			objCashAccountDetail.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
			objCashAccountDetail.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		}
		else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(cashAccountType)){
			objCashAccountDetail.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
			objCashAccountDetail.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
			objCashAccountDetail.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		}
		objCashAccountDetail.setAccountState(DetailCashAccountStateType.ACTIVATE.getCode());
		
		return objCashAccountDetail;
	}
	
	/**
	 * Gets the use type.
	 *
	 * @param bank the bank
	 * @return the use type
	 */
	private Integer getUseType(BankAccounts bank) {
		String reception = bank.getIndReception();
		String sent = bank.getIndSent();
		if (BooleanType.YES.getValue().equals(reception) && BooleanType.YES.getValue().equals(sent)) {
			return new Integer(MasterTableType.PARAMETER_TABLE_USE_TYPE_BOTH.getLngCode().toString());
		} else if (BooleanType.YES.getValue().equals(reception)) {
			return new Integer(MasterTableType.PARAMETER_TABLE_USE_TYPE_RECEPTION.getLngCode().toString());
		} else if (BooleanType.YES.getValue().equals(sent)) {
			return new Integer(MasterTableType.PARAMETER_TABLE_USE_TYPE_SEND.getLngCode().toString());
		}
		return null;
	}
	
	/**
	 * Fill institution cash account.
	 *
	 * @param cashAccountType the cash account type
	 * @param objRegister the obj register
	 * @param blcom the blcom
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount fillInstitutionCashAccount(Integer cashAccountType, RegisterCashAccount objRegister, boolean blcom) throws ServiceException{
		InstitutionCashAccount institutionCashAcc = new InstitutionCashAccount();		
		institutionCashAcc.setSituation(BooleanType.YES.getCode());
		institutionCashAcc.setAccountState(CashAccountStateType.REGISTERED.getCode());
		institutionCashAcc.setIndAutomaticProcess(BooleanType.NO.getCode());
		institutionCashAcc.setDepositAmount(BigDecimal.ZERO);
		institutionCashAcc.setTotalAmount(BigDecimal.ZERO);
		institutionCashAcc.setAvailableAmount(BigDecimal.ZERO);
		institutionCashAcc.setRetirementAmount(BigDecimal.ZERO);
		institutionCashAcc.setMarginAmount(BigDecimal.ZERO);
		institutionCashAcc.setIndRelatedBcrd(BooleanType.NO.getCode());
		institutionCashAcc.setAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
		institutionCashAcc.setIndSettlementProcess(BooleanType.NO.getCode());
		if (AccountCashFundsType.CENTRALIZING.getCode().equals(cashAccountType)){
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if(AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(cashAccountType)) {
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if(AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(cashAccountType)) {
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING_BENEFIT.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if(AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(cashAccountType)) {
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if(AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(cashAccountType)) {
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING_RATES.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if(AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(cashAccountType)) {
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if(AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(cashAccountType)) {
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING_RETURN.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if(AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(cashAccountType)) {
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.CENTRALIZING_TAX.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}else if (AccountCashFundsType.SETTLEMENT.getCode().equals(cashAccountType)){
			SettlerAccount settlerAccount = objRegister.getSettlerAccount();
			//institutionCashAcc.setIndRelatedBcrd(BooleanType.YES.getCode());
			InstitutionCashAccount centralInstCashAccount = cashAccountService.getCentralInstitutionCashAccount(settlerAccount.getCurrencySelected(),AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode());
			if(Validations.validateIsNull(centralInstCashAccount))
				throw new ServiceException(ErrorServiceType.CENTRAL_CASH_ACCOUNT_NOT_ACTIVE);
			
			institutionCashAcc.setInstitutionCashAccount(centralInstCashAccount);
			institutionCashAcc.setAccountType(AccountCashFundsType.SETTLEMENT.getCode());
			institutionCashAcc.setCurrency(settlerAccount.getCurrencySelected());						
			institutionCashAcc.setParticipant(new Participant(Long.valueOf(settlerAccount.getParticipantSelected())));			
			institutionCashAcc.setNegotiationMechanism(new NegotiationMechanism(settlerAccount.getMechanism()));
			ModalityGroup modalityGroup = cashAccountService.find(ModalityGroup.class,settlerAccount.getModalityGroup());	
			institutionCashAcc.setModalityGroup(modalityGroup);					
			institutionCashAcc.setSettlementSchema(modalityGroup.getSettlementSchema());
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.YES.getCode());
		} else if (AccountCashFundsType.BENEFIT.getCode().equals(cashAccountType)){
			BenefitPaymentAccount benefitAccount = objRegister.getBenefitPaymentAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.BENEFIT.getCode());
			institutionCashAcc.setCurrency(benefitAccount.getCurrencySelected());
			//if (!blcom){
			InstitutionCashAccount centralInstCashAccount = cashAccountService.getCentralInstitutionCashAccount(benefitAccount.getCurrencySelected(),AccountCashFundsType.CENTRALIZING_BENEFIT.getCode());
			if(Validations.validateIsNull(centralInstCashAccount))
				throw new ServiceException(ErrorServiceType.CENTRAL_CASH_ACCOUNT_NOT_ACTIVE);				
			institutionCashAcc.setInstitutionCashAccount(centralInstCashAccount);
				//institutionCashAcc.setIndRelatedBcrd(BooleanType.YES.getCode());
			//}
			institutionCashAcc.setIssuer(new Issuer(objRegister.getBenefitPaymentAccount().getIssuer().getIdIssuerPk()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		} else if (AccountCashFundsType.GUARANTEES.getCode().equals(cashAccountType)){
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.GUARANTEES.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		} else if (AccountCashFundsType.RATES.getCode().equals(cashAccountType)){
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.RATES.getCode());
			institutionCashAcc.setIndAutomaticProcess(BooleanType.NO.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		} else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(cashAccountType)){
			InternationalOperationsAccount intOpeAccount = objRegister.getInternationalOperationsAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode());
			institutionCashAcc.setCurrency(intOpeAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(intOpeAccount.getParticipantPK()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		} else if (AccountCashFundsType.RETURN.getCode().equals(cashAccountType)){
			ReturnAccount returnAccount = objRegister.getReturnAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.RETURN.getCode());
			institutionCashAcc.setCurrency(returnAccount.getCurrencySelected());
 			Integer bankType = returnAccount.getBankType();
			//if (BankType.CENTRALIZING.getCode().equals(bankType)) {
			institutionCashAcc.setIndRelatedBcrd(BooleanType.NO.getCode());
			InstitutionCashAccount centralInstCashAccount = cashAccountService.getCentralInstitutionCashAccount(returnAccount.getCurrencySelected(),AccountCashFundsType.CENTRALIZING_RETURN.getCode());
			if(Validations.validateIsNull(centralInstCashAccount))
				throw new ServiceException(ErrorServiceType.CENTRAL_CASH_ACCOUNT_NOT_ACTIVE);				
			institutionCashAcc.setInstitutionCashAccount(centralInstCashAccount);
			if(Validations.validateIsNotNullAndNotEmpty(returnAccount.getParticipantSelected())) {
				institutionCashAcc.setParticipant(new Participant(returnAccount.getParticipantSelected().longValue()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(returnAccount.getIssuer().getIdIssuerPk())) {
				institutionCashAcc.setIssuer(new Issuer(returnAccount.getIssuer().getIdIssuerPk()));
			}
			//}else
			//	institutionCashAcc.setIndAutomaticProcess(returnAccount.getIndAutomatic());
			//institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		} else if (AccountCashFundsType.TAX.getCode().equals(cashAccountType)){
			CentralizingAccount centralizingAccount = objRegister.getCentralizingAccount();
			institutionCashAcc.setAccountType(AccountCashFundsType.TAX.getCode());
			institutionCashAcc.setIndAutomaticProcess(BooleanType.NO.getCode());
			institutionCashAcc.setCurrency(centralizingAccount.getInstitutionBankAccountSession().getCurrency());
			institutionCashAcc.setParticipant(new Participant(idepositarySetup.getIdParticipantDepositary()));
			institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		}
		return institutionCashAcc;
	}
	
	public CashAccountDetail getCentralCashAccountDetail(Long idCashAccount) throws ServiceException{
		CashAccountDetail instCashAccDet = cashAccountService.getCentralCashAccountDetail(idCashAccount);
		return instCashAccDet;
	}
	
	/**
	 * Gets the institution cash account data.
	 *
	 * @param idCashAccount the id cash account
	 * @return the institution cash account data
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount getInstitutionCashAccountData(Long idCashAccount) throws ServiceException{
		InstitutionCashAccount instCashAcc = cashAccountService.getInstitutionCashAccountData(idCashAccount);
		return instCashAcc;
	}
	
	/**
	 * Active cash account.
	 *
	 * @param cashAccountPK the cash account pk
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EFFECTIVE_ACCOUNT_ACTIVATE)
	public void activeCashAccount(Long cashAccountPK) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		
		InstitutionCashAccount cashAccount = getInstitutionCashAccountData(cashAccountPK);		
		if(!CashAccountStateType.REGISTERED.getCode().equals(cashAccount.getAccountState())){
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_REGISTER_STATE_ERROR);
		}
		entityServiceBean.detachmentEntity(cashAccount);
		
		for(CashAccountDetail cashAccountDetail:cashAccount.getCashAccountDetailResults()){
			if(MasterTableType.PARAMETER_TABLE_BANK_STATE_BLOCK.getCode().equals(cashAccountDetail.getInstitutionBankAccount().getProviderBank().getState()))
				throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);			
		}
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeActivate());
		if(BooleanType.YES.getCode().equals(cashAccount.getIndRelatedBcrd())
				|| AccountCashFundsType.CENTRALIZING.getCode().equals(cashAccount.getAccountType())
				|| AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(cashAccount.getAccountType())
				|| AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(cashAccount.getAccountType())
				|| AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(cashAccount.getAccountType())
				|| AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(cashAccount.getAccountType())
				|| AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(cashAccount.getAccountType())
				|| AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(cashAccount.getAccountType())
				|| AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(cashAccount.getAccountType())){
			InstitutionCashAccount activatedAccounts = cashAccountService.getActivatedCashAccount(cashAccount);
			if (Validations.validateIsNullOrEmpty(activatedAccounts)) {					
				cashAccount.setAccountState(CashAccountStateType.ACTIVATE.getCode());
				cashAccount.setSituation(BooleanType.YES.getCode());
				/*
				BeginEndDay beginEnd = cashAccountService.getBeginEndDay();
				if(Validations.validateIsNotNull(beginEnd)){
					throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
				}else{
					if(BooleanType.YES.getCode().equals(beginEnd.getIndSituation()))
						cashAccount.setSituation(BooleanType.YES.getCode());
					else
						cashAccount.setSituation(BooleanType.NO.getCode());
				}*/						
				cashAccountService.update(cashAccount);
			}else{
				throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_ACTIVE_ERROR);
			}
		}else{
			if(AccountCashFundsType.TAX.getCode().equals(cashAccount.getAccountType())      ||
				AccountCashFundsType.RATES.getCode().equals(cashAccount.getAccountType())	){
				if(cashAccountService.isOtherCashAccountWithAutomaticProcess(cashAccount))
					throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_ACTIVE_ERROR);
			}
			cashAccount.setAccountState(CashAccountStateType.ACTIVATE.getCode());
			cashAccountService.update(cashAccount);
		}
	
	}
	
	/**
	 * Validate active cash account.
	 *
	 * @param cashAccount the cash account
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount validateActiveCashAccount(InstitutionCashAccount cashAccount) throws ServiceException 
	{
		SearchCashDetailTO objSearchCashDetailTO = new SearchCashDetailTO();
		if (AccountCashFundsType.CENTRALIZING.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(cashAccount.getAccountType())) {
			objSearchCashDetailTO.setIdBankInstitutionPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}else if (AccountCashFundsType.SETTLEMENT.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdParticipantPk(cashAccount.getParticipant().getIdParticipantPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
			objSearchCashDetailTO.setMechanismPk(cashAccount.getNegotiationMechanism().getIdNegotiationMechanismPk());
			objSearchCashDetailTO.setModalityGroupPk(cashAccount.getModalityGroup().getIdModalityGroupPk());
		}
		else if (AccountCashFundsType.BENEFIT.getCode().equals(cashAccount.getAccountType())){
			objSearchCashDetailTO.setIdIssuerPk(cashAccount.getIssuer().getIdIssuerPk());
			objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
		}
		else if (AccountCashFundsType.RETURN.getCode().equals(cashAccount.getAccountType())
				||AccountCashFundsType.TAX.getCode().equals(cashAccount.getAccountType())
						||AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(cashAccount.getAccountType())
						|| AccountCashFundsType.RATES.getCode().equals(cashAccount.getAccountType())
						||AccountCashFundsType.GUARANTEES.getCode().equals(cashAccount.getAccountType())){
			if(AccountCashFundsType.RETURN.getCode().equals(cashAccount.getAccountType())){
				if(Validations.validateIsNotNullAndNotEmpty(cashAccount.getIssuer())) {
					objSearchCashDetailTO.setIdIssuerPk(cashAccount.getIssuer().getIdIssuerPk());
				}else if(Validations.validateIsNotNullAndNotEmpty(cashAccount.getParticipant())) {
					objSearchCashDetailTO.setIdParticipantPk(cashAccount.getParticipant().getIdParticipantPk());
				}
				objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
				objSearchCashDetailTO.setIdBankPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
			}else {
				objSearchCashDetailTO.setIdBankPk(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
				objSearchCashDetailTO.setCurrency(cashAccount.getCurrency());
				objSearchCashDetailTO.setIdParticipantPk(cashAccount.getParticipant().getIdParticipantPk());
			}
			
			//objSearchCashDetailTO.setAccountNumber(cashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
		}
		objSearchCashDetailTO.setCashAccountType(cashAccount.getAccountType());
		
		return manageEffectiveAccountsService.validateInstitutioCashAccount(objSearchCashDetailTO);
	}
	
	/**
	 * Disactivate cash account.
	 *
	 * @param cashAccountPK the cash account pk
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EFFECTIVE_ACCOUNT_DISACTIVATE)
	public void disactivateCashAccount(Long cashAccountPK) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		
		InstitutionCashAccount cashAccount = getInstitutionCashAccountData(cashAccountPK);
		if(!CashAccountStateType.ACTIVATE.getCode().equals(cashAccount.getAccountState())){
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_ACTIVE_STATE_ERROR);
		}
		entityServiceBean.detachmentEntity(cashAccount);
		//cashAccount.setInstitutionBankAccounts(cashAccountService.getInstitutionBanksAccount(cashAccountPK));
		List<Object[]> lstAmounts = cashAccountService.totalAndDepositAmount(cashAccount.getIdInstitutionCashAccountPk());
		if(((BigDecimal)lstAmounts.get(0)[0]).compareTo(BigDecimal.ZERO) == 1
				|| ((BigDecimal)lstAmounts.get(0)[1]).compareTo(BigDecimal.ZERO) == 1)
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_DEACTIVE_ERROR);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDeactivate());
		cashAccount.setSituation(BooleanType.NO.getCode());
		cashAccount.setAccountState(CashAccountStateType.REGISTERED.getCode());
		cashAccountService.update(cashAccount);
		
	}
	
	/**
	 * Modify cash account.
	 *
	 * @param idInstitutionCashAccountPk the id institution cash account pk
	 * @param registerCashAccount the register cash account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EFFECTIVE_ACCOUNT_MODIFICATION)
	public void modifyCashAccount(Long idInstitutionCashAccountPk, RegisterCashAccount registerCashAccount) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		InstitutionCashAccount instTemp = getInstitutionCashAccountData(idInstitutionCashAccountPk);
		if(AccountCashFundsType.CENTRALIZING.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			InstitutionBankAccount instBankAcc = registerCashAccount.getCentralizingAccount().getInstitutionBankAccountSession();
			instTemp.setCurrency(instBankAcc.getCurrency());
			cashAccountService.update(instTemp);
			CashAccountDetail cashAccountDetail = instTemp.getCashAccountDetailResults().get(0);
			cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
			CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), null);
			cashAccountDetailSave.setInstitutionBankAccount(instBankAcc);
			cashAccountDetailSave.setInstitutionCashAccount(instTemp);
			cashAccountService.create(cashAccountDetailSave);
			
		}else if (AccountCashFundsType.BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				
				Integer bankAccountType = registerCashAccount.getBenefitPaymentAccount().getTypeAccountSelected();
				instTemp.setCurrency( registerCashAccount.getBenefitPaymentAccount().getCurrencySelected());
				//if (BankType.COMMERCIAL.getCode().equals(bankAccountType)) {				
					instTemp.setIndRelatedBcrd(BooleanType.NO.getCode());
				//} else if (BankType.CENTRALIZING.getCode().equals(bankAccountType)) {
					InstitutionCashAccount centralInstCashAccount = cashAccountService.getCentralInstitutionCashAccount(registerCashAccount.getBenefitPaymentAccount().getCurrencySelected(),AccountCashFundsType.CENTRALIZING_BENEFIT.getCode());
					if(Validations.validateIsNull(centralInstCashAccount))
						throw new ServiceException(ErrorServiceType.CENTRAL_CASH_ACCOUNT_NOT_ACTIVE);				
					instTemp.setInstitutionCashAccount(centralInstCashAccount);
					//instTemp.setIndRelatedBcrd(BooleanType.YES.getCode());
				//}
				cashAccountService.update(instTemp);
			
				CashAccountDetail cashAccountDetail = instTemp.getCashAccountDetailResults().get(0);
				cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
				CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), null);
				cashAccountDetailSave.setInstitutionBankAccount(registerCashAccount.getBenefitPaymentAccount().getInstitutionBankAccount());
				cashAccountDetailSave.setInstitutionCashAccount(instTemp);
				cashAccountService.create(cashAccountDetailSave);
		}else if (AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			InstitutionBankAccount instBankAcc = registerCashAccount.getMarginGuaranteeAccount().getInstitutionBankAccountSession();
			instTemp.setCurrency(instBankAcc.getCurrency());
			cashAccountService.update(instTemp);
			CashAccountDetail cashAccountDetail = instTemp.getCashAccountDetailResults().get(0);
			cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
			CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), null);
			cashAccountDetailSave.setInstitutionBankAccount(instBankAcc);
			cashAccountDetailSave.setInstitutionCashAccount(instTemp);
			cashAccountService.create(cashAccountDetailSave);
		}else if (AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {			
			InstitutionBankAccount instBankAcc = registerCashAccount.getRatesAccount().getInstitutionBankAccountSession();
			instTemp.setCurrency(instBankAcc.getCurrency());
			cashAccountService.update(instTemp);
			CashAccountDetail cashAccountDetail = instTemp.getCashAccountDetailResults().get(0);
			cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
			CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), null);
			cashAccountDetailSave.setInstitutionBankAccount(instBankAcc);
			cashAccountDetailSave.setInstitutionCashAccount(instTemp);
			cashAccountService.create(cashAccountDetailSave);
			
		}else if (AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {			
			InstitutionBankAccount instBankAcc = registerCashAccount.getReturnAccount().getInstitutionBankAccountSession();
			instTemp.setCurrency(instBankAcc.getCurrency());
			cashAccountService.update(instTemp);
			CashAccountDetail cashAccountDetail = instTemp.getCashAccountDetailResults().get(0);
			cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
			CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), null);
			cashAccountDetailSave.setInstitutionBankAccount(instBankAcc);
			cashAccountDetailSave.setInstitutionCashAccount(instTemp);
			cashAccountService.create(cashAccountDetailSave);
		}else if (AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			InstitutionBankAccount instBankAcc = registerCashAccount.getTaxesAccount().getInstitutionBankAccountSession();
			instTemp.setCurrency(instBankAcc.getCurrency());
			cashAccountService.update(instTemp);
			CashAccountDetail cashAccountDetail = instTemp.getCashAccountDetailResults().get(0);
			cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
			CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), null);
			cashAccountDetailSave.setInstitutionBankAccount(instBankAcc);
			cashAccountDetailSave.setInstitutionCashAccount(instTemp);
			cashAccountService.create(cashAccountDetailSave);
		}else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			InstitutionBankAccount instBankAcc = registerCashAccount.getInternationalOperationsAccount().getInstitutionBankAccountSession();
			instTemp.setCurrency(instBankAcc.getCurrency());
			cashAccountService.update(instTemp);
			CashAccountDetail cashAccountDetail = instTemp.getCashAccountDetailResults().get(0);
			cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
			CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), null);
			cashAccountDetailSave.setInstitutionBankAccount(instBankAcc);
			cashAccountDetailSave.setInstitutionCashAccount(instTemp);
			cashAccountService.create(cashAccountDetailSave);
			
		}else if (AccountCashFundsType.SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			for(CashAccountDetail cashAccountDetail : instTemp.getCashAccountDetailResults())
				cashAccountService.delete(CashAccountDetail.class, cashAccountDetail.getIdCashAccountDetail());
			
			for (BankAccounts bank:registerCashAccount.getSettlerAccount().getLstBankAccounts()) {
				CashAccountDetail cashAccountDetailSave=fillCashAccountDetail(registerCashAccount.getCashAccountTypeSelected(), bank);
				InstitutionBankAccount institutionBankAccount = cashAccountService.searchInstitutionBankAccount(bank.getIdInstitutionBankAccountPk());
				cashAccountDetailSave.setInstitutionBankAccount(institutionBankAccount);
				//cashAccountDetailSave.setInstitutionBankAccount(new InstitutionBankAccount(bank.getIdInstitutionBankAccountPk(),BankAccountsStateType.REGISTERED.getCode()));
				cashAccountDetailSave.setInstitutionCashAccount(instTemp);
				cashAccountService.create(cashAccountDetailSave);
			}
		}
	}
	
		/**
		 * Fill bank by type.
		 *
		 * @param bankType the bank type
		 * @return the list
		 */
		public List<Bank> fillBankByType(Integer bankType) {
		return cashAccountService.fillBankByType(bankType);
		}
				
		/**
		 * Gets the issuer bank account.
		 *
		 * @param issuerSelected the issuer selected
		 * @param currency the currency
		 * @param idBank the id bank
		 * @return the issuer bank account
		 */
		public List<InstitutionBankAccount> getIssuerBankAccount(String issuerSelected, Integer currency, Long idBank)
		{
			return cashAccountService.getIssuerBankAccount(issuerSelected, currency, idBank);
		}

		
		public InstitutionBankAccount findBankAccount(Long idBankAccount)
		{
			return cashAccountService.findIssuerBankAccount(idBankAccount);
		}
		/**
		 * Gets the participants int depositary for deposit.
		 *
		 * @return the participants int depositary for deposit
		 * @throws ServiceException the service exception
		 */
		public List<Participant> getParticipantsIntDepositaryForDeposit() throws ServiceException{
			return depositService.getParticipantsIntDepositaryForDeposit();
		}
		
		/**
		 * Fill settlement bank.
		 *
		 * @return the list
		 * @throws ServiceException the service exception
		 */
		public List<Bank> fillSettlementBank() throws ServiceException {
			
			Bank objBank=manageBanksService.getBankForBankType(BankType.CENTRALIZING.getCode());
			return cashAccountService.fillSettlementBank(objBank.getIdBankPk());
		}
}
