package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.funds.InstitutionBankAccount;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CashAccountsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class CashAccountsTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	private int id;
	
	/** The id cash account. */
	private Long idCashAccount;
	
	/** The institution. */
	private String institution;
	
	/** The mechanism. */
	private String mechanism;
	
	/** The modality group. */
	private String modalityGroup;
	
	/** The currency. */
	private String currency;
	
	/** The settlement schema. */
	private String settlementSchema;
	
	/** The bic code. */
	private String bicCode;
	
	/** The state. */
	private String state;
	
	/** The id state. */
	private Integer idState;
	
	/** The id pk. */
	private Integer idPk;
	
	/** The id participant fk. */
	private Integer idParticipantFk;
	
	/** The bank. */
	private String bank;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The ind commercial. */
	private String indCommercial;
	
	/** The lst act type. */
	private List<InstitutionBankAccount> lstActType;
	
	/** The ind related bcrd. */
	private Integer indRelatedBcrd;
	
	/** The cash account type. */
	private Integer cashAccountType;

	/**
	 * Gets the lst act type.
	 *
	 * @return the lstActType
	 */
	public List<InstitutionBankAccount> getLstActType() {
		return lstActType;
	}

	/**
	 * Sets the lst act type.
	 *
	 * @param lstActType            the lstActType to set
	 */
	public void setLstActType(List<InstitutionBankAccount> lstActType) {
		this.lstActType = lstActType;
	}

	/**
	 * Gets the id participant fk.
	 *
	 * @return the idParticipantFk
	 */
	public Integer getIdParticipantFk() {
		return idParticipantFk;
	}

	/**
	 * Sets the id participant fk.
	 *
	 * @param idParticipantFk            the idParticipantFk to set
	 */
	public void setIdParticipantFk(Integer idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	/**
	 * Gets the id pk.
	 *
	 * @return the idPk
	 */
	public Integer getIdPk() {
		return idPk;
	}

	/**
	 * Sets the id pk.
	 *
	 * @param idPk            the idPk to set
	 */
	public void setIdPk(Integer idPk) {
		this.idPk = idPk;
	}

	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public String getInstitution() {
		return institution;
	}

	/**
	 * Sets the institution.
	 *
	 * @param institution the new institution
	 */
	public void setInstitution(String institution) {
		this.institution = institution;
	}

	/**
	 * Gets the mechanism.
	 *
	 * @return the mechanism
	 */
	public String getMechanism() {
		return mechanism;
	}

	/**
	 * Sets the mechanism.
	 *
	 * @param mechanism the new mechanism
	 */
	public void setMechanism(String mechanism) {
		this.mechanism = mechanism;
	}

	/**
	 * Gets the modality group.
	 *
	 * @return the modality group
	 */
	public String getModalityGroup() {
		return modalityGroup;
	}

	/**
	 * Sets the modality group.
	 *
	 * @param modalityGroup the new modality group
	 */
	public void setModalityGroup(String modalityGroup) {
		this.modalityGroup = modalityGroup;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public String getSettlementSchema() {
		return settlementSchema;
	}

	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the new settlement schema
	 */
	public void setSettlementSchema(String settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}

	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}

	/**
	 * Gets the id state.
	 *
	 * @return the id state
	 */
	public Integer getIdState() {
		return idState;
	}

	/**
	 * Sets the id state.
	 *
	 * @param idState the new id state
	 */
	public void setIdState(Integer idState) {
		this.idState = idState;
	}

	/**
	 * Gets the id cash account.
	 *
	 * @return the id cash account
	 */
	public Long getIdCashAccount() {
		return idCashAccount;
	}

	/**
	 * Sets the id cash account.
	 *
	 * @param idCashAccount the new id cash account
	 */
	public void setIdCashAccount(Long idCashAccount) {
		this.idCashAccount = idCashAccount;
	}

	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public String getBank() {
		return bank;
	}

	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}

	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	/**
	 * Gets the ind commercial.
	 *
	 * @return the ind commercial
	 */
	public String getIndCommercial() {
		return indCommercial;
	}

	/**
	 * Sets the ind commercial.
	 *
	 * @param indCommercial the new ind commercial
	 */
	public void setIndCommercial(String indCommercial) {
		this.indCommercial = indCommercial;
	}

	/**
	 * Gets the ind related bcrd.
	 *
	 * @return the ind related bcrd
	 */
	public Integer getIndRelatedBcrd() {
		return indRelatedBcrd;
	}

	/**
	 * Sets the ind related bcrd.
	 *
	 * @param indRelatedBcrd the new ind related bcrd
	 */
	public void setIndRelatedBcrd(Integer indRelatedBcrd) {
		this.indRelatedBcrd = indRelatedBcrd;
	}

	/**
	 * Gets the cash account type.
	 *
	 * @return the cash account type
	 */
	public Integer getCashAccountType() {
		return cashAccountType;
	}

	/**
	 * Sets the cash account type.
	 *
	 * @param cashAccountType the new cash account type
	 */
	public void setCashAccountType(Integer cashAccountType) {
		this.cashAccountType = cashAccountType;
	}
	
	
}