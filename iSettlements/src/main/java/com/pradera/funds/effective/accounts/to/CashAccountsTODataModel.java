package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CashAccountsTODataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class CashAccountsTODataModel extends ListDataModel<CashAccountsTO> implements Serializable, SelectableDataModel<CashAccountsTO>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new cash accounts to data model.
	 *
	 * @param data the data
	 */
	public CashAccountsTODataModel(List<CashAccountsTO> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public CashAccountsTO getRowData(String rowKey) {
		List<CashAccountsTO> parametros=(List<CashAccountsTO>)getWrappedData();
        for(CashAccountsTO parameter : parametros) {  
        	
            if(String.valueOf(parameter.getId()).equals(rowKey))
                return parameter;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	public Object getRowKey(CashAccountsTO parameter) {
		return parameter.getId();
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public Integer getSize(){
		List<CashAccountsTO> parametros = (List<CashAccountsTO>)getWrappedData();
		return parametros.size();
	}
}