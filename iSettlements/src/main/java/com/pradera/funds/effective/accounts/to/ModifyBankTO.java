package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.funds.InstitutionBankAccount;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ModifyBankTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class ModifyBankTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The modality group. */
	private Long modalityGroup;
	
	/** The mechanism. */
	private Long mechanism;
	
	/** The lst banks. */
	private List<InstitutionBankAccount> lstBanks;
	
	/** The participant. */
	private Long participant;
	
	/** The bic code. */
	private String bicCode;
	
	/** The currency. */
	private Integer currency;
	
	/** The participant description. */
	private String participantDescription;
	
	/** The state description. */
	private String stateDescription;
	
	/** The issuer pk. */
	private String issuerPK;
	
	/** The issuer description. */
	private String issuerDescription;

	/**
	 * Gets the modality group.
	 *
	 * @return the modality group
	 */
	public Long getModalityGroup() {
		return modalityGroup;
	}
	
	/**
	 * Sets the modality group.
	 *
	 * @param modalityGroup the new modality group
	 */
	public void setModalityGroup(Long modalityGroup) {
		this.modalityGroup = modalityGroup;
	}
	
	/**
	 * Gets the mechanism.
	 *
	 * @return the mechanism
	 */
	public Long getMechanism() {
		return mechanism;
	}
	
	/**
	 * Sets the mechanism.
	 *
	 * @param mechanism the new mechanism
	 */
	public void setMechanism(Long mechanism) {
		this.mechanism = mechanism;
	}
	
	/**
	 * Gets the lst banks.
	 *
	 * @return the lst banks
	 */
	public List<InstitutionBankAccount> getLstBanks() {
		return lstBanks;
	}
	
	/**
	 * Sets the lst banks.
	 *
	 * @param lstBanks the new lst banks
	 */
	public void setLstBanks(List<InstitutionBankAccount> lstBanks) {
		this.lstBanks = lstBanks;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Long getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Long participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}
	
	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return participantDescription;
	}
	
	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the issuer pk.
	 *
	 * @return the issuer pk
	 */
	public String getIssuerPK() {
		return issuerPK;
	}
	
	/**
	 * Sets the issuer pk.
	 *
	 * @param issuerPK the new issuer pk
	 */
	public void setIssuerPK(String issuerPK) {
		this.issuerPK = issuerPK;
	}
	
	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}
	
	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}	
}