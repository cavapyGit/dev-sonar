package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchEffectiveAccountsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchEffectiveAccountsTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Cash Account Type. */
	private Integer cashAccountType;
	
	/** The id babnk pk. */
	private Long idBankPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id mechanism pk. */
	private Long idMechanismPk;
	
	/** The id modality group pk. */
	private Long idModalityGroupPk;
	
	/** The currency. */
	private Integer currency;
	
	/** The id Use Type. */
	private Integer idUseType;
	
	/** The id Bank Account Class. */
	private Integer idBankAccountClass;
	
	/** The bic Code. */
	private String bicCode;
	
	/** The List Cash Account Type. */
	private List<ParameterTable> lstCashAccountType; 
		
	/** The List Use Type. */
	private List<ParameterTable> lstUseType; 
	
	/** The List bank. */
	private List<Bank> lstBank;
	
	/** The List participant. */
	private List<Participant> lstParticipant;
	
	/** The List Bank Account class. */
	private List<ParameterTable> lstBankAccountClass;
	
	/** The List currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The List Group Bolsa. */
	private ModalityGroupDataModel lstBolsa;
	
	/** The List Group Sirtex. */
	private ModalityGroupDataModel  lstSirtex;
	
	/** The List Group OTC. */
	private ModalityGroupDataModel lstOTC;
	
	/** The List Modality Group Bolsa. */
	private ModalityGroup[] modaGroupBolsa;	
	
	/** The List Modality Group Sirtex. */
	private ModalityGroup[] modaGroupSirtex;	
	
	/** The List Modality Group OTC. */
	private ModalityGroup[]  modaGroupOTC;	
	
	/** The List Mechanism. */
	private List<NegotiationMechanism> lstMechanism;
	
	/** The List Modality Group. */
	private List<ModalityGroup> lstModalityGroup;
	
	/**
	 * Instantiates a new search effective accounts to.
	 */
	public SearchEffectiveAccountsTO() {

	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst cash account type.
	 *
	 * @return the lst cash account type
	 */
	public List<ParameterTable> getLstCashAccountType() {
		return lstCashAccountType;
	}

	/**
	 * Sets the lst cash account type.
	 *
	 * @param lstCashAccountType the new lst cash account type
	 */
	public void setLstCashAccountType(List<ParameterTable> lstCashAccountType) {
		this.lstCashAccountType = lstCashAccountType;
	}

	/**
	 * Gets the cash account type.
	 *
	 * @return the cash account type
	 */
	public Integer getCashAccountType() {
		return cashAccountType;
	}

	/**
	 * Sets the cash account type.
	 *
	 * @param cashAccountType the new cash account type
	 */
	public void setCashAccountType(Integer cashAccountType) {
		this.cashAccountType = cashAccountType;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the lst use type.
	 *
	 * @return the lst use type
	 */
	public List<ParameterTable> getLstUseType() {
		return lstUseType;
	}

	/**
	 * Sets the lst use type.
	 *
	 * @param lstUseType the new lst use type
	 */
	public void setLstUseType(List<ParameterTable> lstUseType) {
		this.lstUseType = lstUseType;
	}

	/**
	 * Gets the lst bank account class.
	 *
	 * @return the lst bank account class
	 */
	public List<ParameterTable> getLstBankAccountClass() {
		return lstBankAccountClass;
	}

	/**
	 * Sets the lst bank account class.
	 *
	 * @param lstBankAccountClass the new lst bank account class
	 */
	public void setLstBankAccountClass(List<ParameterTable> lstBankAccountClass) {
		this.lstBankAccountClass = lstBankAccountClass;
	}

	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}

	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}

	/**
	 * Gets the lst bolsa.
	 *
	 * @return the lst bolsa
	 */
	public ModalityGroupDataModel getLstBolsa() {
		return lstBolsa;
	}

	/**
	 * Sets the lst bolsa.
	 *
	 * @param lstBolsa the new lst bolsa
	 */
	public void setLstBolsa(ModalityGroupDataModel lstBolsa) {
		this.lstBolsa = lstBolsa;
	}

	/**
	 * Gets the lst sirtex.
	 *
	 * @return the lst sirtex
	 */
	public ModalityGroupDataModel getLstSirtex() {
		return lstSirtex;
	}

	/**
	 * Sets the lst sirtex.
	 *
	 * @param lstSirtex the new lst sirtex
	 */
	public void setLstSirtex(ModalityGroupDataModel lstSirtex) {
		this.lstSirtex = lstSirtex;
	}

	/**
	 * Gets the lst otc.
	 *
	 * @return the lst otc
	 */
	public ModalityGroupDataModel getLstOTC() {
		return lstOTC;
	}

	/**
	 * Sets the lst otc.
	 *
	 * @param lstOTC the new lst otc
	 */
	public void setLstOTC(ModalityGroupDataModel lstOTC) {
		this.lstOTC = lstOTC;
	}

	/**
	 * Gets the moda group bolsa.
	 *
	 * @return the moda group bolsa
	 */
	public ModalityGroup[] getModaGroupBolsa() {
		return modaGroupBolsa;
	}

	/**
	 * Sets the moda group bolsa.
	 *
	 * @param modaGroupBolsa the new moda group bolsa
	 */
	public void setModaGroupBolsa(ModalityGroup[] modaGroupBolsa) {
		this.modaGroupBolsa = modaGroupBolsa;
	}

	/**
	 * Gets the moda group sirtex.
	 *
	 * @return the moda group sirtex
	 */
	public ModalityGroup[] getModaGroupSirtex() {
		return modaGroupSirtex;
	}

	/**
	 * Sets the moda group sirtex.
	 *
	 * @param modaGroupSirtex the new moda group sirtex
	 */
	public void setModaGroupSirtex(ModalityGroup[] modaGroupSirtex) {
		this.modaGroupSirtex = modaGroupSirtex;
	}

	/**
	 * Gets the moda group otc.
	 *
	 * @return the moda group otc
	 */
	public ModalityGroup[] getModaGroupOTC() {
		return modaGroupOTC;
	}

	/**
	 * Sets the moda group otc.
	 *
	 * @param modaGroupOTC the new moda group otc
	 */
	public void setModaGroupOTC(ModalityGroup[] modaGroupOTC) {
		this.modaGroupOTC = modaGroupOTC;
	}

	/**
	 * Gets the id mechanism pk.
	 *
	 * @return the id mechanism pk
	 */
	public Long getIdMechanismPk() {
		return idMechanismPk;
	}

	/**
	 * Sets the id mechanism pk.
	 *
	 * @param idMechanismPk the new id mechanism pk
	 */
	public void setIdMechanismPk(Long idMechanismPk) {
		this.idMechanismPk = idMechanismPk;
	}

	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}

	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}

	/**
	 * Gets the id modality group pk.
	 *
	 * @return the id modality group pk
	 */
	public Long getIdModalityGroupPk() {
		return idModalityGroupPk;
	}

	/**
	 * Sets the id modality group pk.
	 *
	 * @param idModalityGroupPk the new id modality group pk
	 */
	public void setIdModalityGroupPk(Long idModalityGroupPk) {
		this.idModalityGroupPk = idModalityGroupPk;
	}

	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}

	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}

	/**
	 * Gets the id use type.
	 *
	 * @return the id use type
	 */
	public Integer getIdUseType() {
		return idUseType;
	}

	/**
	 * Sets the id use type.
	 *
	 * @param idUseType the new id use type
	 */
	public void setIdUseType(Integer idUseType) {
		this.idUseType = idUseType;
	}

	/**
	 * Gets the id bank account class.
	 *
	 * @return the id bank account class
	 */
	public Integer getIdBankAccountClass() {
		return idBankAccountClass;
	}

	/**
	 * Sets the id bank account class.
	 *
	 * @param idBankAccountClass the new id bank account class
	 */
	public void setIdBankAccountClass(Integer idBankAccountClass) {
		this.idBankAccountClass = idBankAccountClass;
	}

	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 */
	public List<Bank> getLstBank() {
		return lstBank;
	}

	/**
	 * Sets the lst bank.
	 *
	 * @param lstBank the new lst bank
	 */
	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}

	/**
	 * Gets the id bank pk.
	 *
	 * @return the id bank pk
	 */
	public Long getIdBankPk() {
		return idBankPk;
	}

	/**
	 * Sets the id bank pk.
	 *
	 * @param idBankPk the new id bank pk
	 */
	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}

}
