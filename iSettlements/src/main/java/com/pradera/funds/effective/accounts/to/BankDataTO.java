package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BankDataTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class BankDataTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The pk bank account type. */
	private Long pkBankAccountType;
	
	/** The bic code. */
	private String bicCode;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The bank account type. */
	private String bankAccountType;
	
	/** The id institution bank account pk. */
	private Long idInstitutionBankAccountPk;

	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}
	
	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}
	
	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	
	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	
	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public String getBankAccountType() {
		return bankAccountType;
	}
	
	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(String bankAccountType) {
		this.bankAccountType = bankAccountType;
	}
	
	/**
	 * Gets the pk bank account type.
	 *
	 * @return the pk bank account type
	 */
	public Long getPkBankAccountType() {
		return pkBankAccountType;
	}
	
	/**
	 * Sets the pk bank account type.
	 *
	 * @param pkBankAccountType the new pk bank account type
	 */
	public void setPkBankAccountType(Long pkBankAccountType) {
		this.pkBankAccountType = pkBankAccountType;
	}
	
	/**
	 * Gets the id institution bank account pk.
	 *
	 * @return the id institution bank account pk
	 */
	public Long getIdInstitutionBankAccountPk() {
		return idInstitutionBankAccountPk;
	}
	
	/**
	 * Sets the id institution bank account pk.
	 *
	 * @param idInstitutionBankAccountPk the new id institution bank account pk
	 */
	public void setIdInstitutionBankAccountPk(Long idInstitutionBankAccountPk) {
		this.idInstitutionBankAccountPk = idInstitutionBankAccountPk;
	}
}