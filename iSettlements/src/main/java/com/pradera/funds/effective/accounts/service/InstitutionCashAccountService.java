package com.pradera.funds.effective.accounts.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.ParticipantSettlement;

// TODO: Auto-generated Javadoc
/**
 * The Class InstitutionCashAccountServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
public class InstitutionCashAccountService extends CrudDaoServiceBean{
	
	/**
	 * Search bank description.
	 *
	 * @param idInstitutionCashAccount the id institution cash account
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String searchBankDescription(Long idInstitutionCashAccount) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("   SELECT");
		sbQuery.append("   		  IBA.clientBank.description");
		sbQuery.append("     FROM InstitutionBankAccount IBA");
		sbQuery.append("    WHERE IBA.institutionCashAccount.idInstitutionCashAccountPk =:idInstitutionCashAccountPk");
		
		Query query = em.createQuery(sbQuery.toString());	
		query.setParameter("idInstitutionCashAccountPk",idInstitutionCashAccount);
		return query.getSingleResult().toString();
	}
	
	/**
	 * Search registered funds operations.
	 *
	 * @param processDay the process day
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperation> searchRegisteredFundsOperations(Date processDay) throws ServiceException{
		List<Integer> fundOperationsGroups = new ArrayList<Integer>();
		fundOperationsGroups.add(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundOperationsGroups.add(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode());
		fundOperationsGroups.add(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		fundOperationsGroups.add(FundsOperationGroupType.SPECIAL_OPERATION.getCode());

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("   	SELECT FO ");
		sbQuery.append("    FROM 	FundsOperation FO");
		sbQuery.append("    WHERE 	FO.operationState =:operationState ");
		sbQuery.append("    AND 	TRUNC(FO.registryDate) =:date ");
		sbQuery.append("    AND 	FO.fundsOperationGroup in (:operationGroups) ");
		sbQuery.append("    AND 	FO.indCentralized = :indCentralized ");
		Query query = em.createQuery(sbQuery.toString());	
		query.setParameter("operationState",MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
		query.setParameter("date",processDay);
		query.setParameter("operationGroups",fundOperationsGroups);
		query.setParameter("indCentralized",BooleanType.YES.getCode());
		@SuppressWarnings("unchecked")
		List<FundsOperation> lstFundsOperation = query.getResultList();		
		return lstFundsOperation;
	}

	/**
	 * Gets the observed fund operations.
	 *
	 * @param processDay the process day
	 * @return the observed fund operations
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<FundsOperation> getObservedFundOperations(Date processDay) throws ServiceException{
		List<Integer> fundOperationsGroups = new ArrayList<Integer>();
		fundOperationsGroups.add(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundOperationsGroups.add(FundsOperationGroupType.RATE_FUND_OPERATION.getCode());
		fundOperationsGroups.add(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("   	SELECT FO ");
		sbQuery.append("    FROM 	FundsOperation FO");
		sbQuery.append("    WHERE 	FO.operationState =:operationState ");
		sbQuery.append("    AND 	trunc(FO.registryDate) = trunc(:date) ");
		sbQuery.append("    AND 	FO.fundsOperationGroup in (:operationGroups) ");
		Query query = em.createQuery(sbQuery.toString());	
		query.setParameter("operationState",MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode());
		query.setParameter("date",processDay);
		query.setParameter("operationGroups",fundOperationsGroups);	
		return (List<FundsOperation>)query.getResultList();
	}
	
	/**
	 * WE GET THE BEGIN_END_DAY OBJECT TO VALIDATE IF IT EXISTS.
	 *
	 * @param date the date
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public BeginEndDay searchBeginEnd(Date date) throws ServiceException{
		BeginEndDay beginEndDay = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("   SELECT BED ");
			sbQuery.append("   FROM BeginEndDay BED ");
			sbQuery.append("   WHERE trunc(BED.processDay) = trunc(:date) ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("date", date);
			beginEndDay = (BeginEndDay) query.getSingleResult();
		}catch(NoResultException x){
			x.getStackTrace();
		}
		return beginEndDay;
		
	}
	
	/**
	 * Update institution cash account situation.
	 *
	 * @param situation the situation
	 * @throws ServiceException the service exception
	 */
		public void updateInstitutionCashAccountSituation(Integer situation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<Integer> lstAccountType = new ArrayList<Integer>();
		lstAccountType.add(AccountCashFundsType.SETTLEMENT.getCode());
		lstAccountType.add(AccountCashFundsType.BENEFIT.getCode());
		lstAccountType.add(AccountCashFundsType.RETURN.getCode());
		
		sbQuery.append("   	UPDATE");
		sbQuery.append("	InstitutionCashAccount ICA");
		sbQuery.append("    SET ICA.situation =:situation");//0
		sbQuery.append("    WHERE ICA.accountType in (:accountType)");
		sbQuery.append("    AND ICA.accountState = :state ");
		//sbQuery.append("    AND ICA.indRelatedBcrd = :indCentral ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("situation",situation);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("accountType",lstAccountType);
		//query.setParameter("indCentral",BooleanType.YES.getCode());
		query.executeUpdate();
	}

		/**
		 * METHOD TO UPDATE SITUATION OF CENTRALIZER CASH ACCOUNT .
		 *
		 * @param situation the situation
		 * @throws ServiceException the service exception
		 */
	public void updateCentralizerCashAccountSituation(Integer situation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<Integer> lstAccountType = new ArrayList<Integer>();
		lstAccountType.add(AccountCashFundsType.CENTRALIZING.getCode());
		lstAccountType.add(AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode());
		lstAccountType.add(AccountCashFundsType.CENTRALIZING_BENEFIT.getCode());
		lstAccountType.add(AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode());
		lstAccountType.add(AccountCashFundsType.CENTRALIZING_RATES.getCode());
		lstAccountType.add(AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode());
		lstAccountType.add(AccountCashFundsType.CENTRALIZING_RETURN.getCode());
		lstAccountType.add(AccountCashFundsType.CENTRALIZING_TAX.getCode());
		
		sbQuery.append("   	UPDATE");
		sbQuery.append("	InstitutionCashAccount ICA");
		sbQuery.append("    SET ICA.situation =:situation");//0
		sbQuery.append("    WHERE ICA.accountType in (:accountType)");
		sbQuery.append("    AND ICA.accountState = :state ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("situation",situation);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("accountType",lstAccountType);
		query.executeUpdate();
	}
	
	/**
	 * Search efective accounts.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionCashAccount> searchEfectiveAccounts() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("   SELECT");
		sbQuery.append("          ICA.totalAmount,");//0
		sbQuery.append("          ICA.accountType,");//1
		sbQuery.append("          ICA.idInstitutionCashAccountPk");//2
		sbQuery.append("     FROM InstitutionCashAccount ICA  ");
		sbQuery.append("    WHERE ICA.accountType in :accountType");
		//sbQuery.append("    AND ICA.indRelatedBcrd = :indCentral"); No utilizaran campo indRelatedBcrd
		Query query = em.createQuery(sbQuery.toString());
		List<Integer> accountsTypes = new ArrayList<Integer>();
		//accountsTypes.add(AccountCashFundsType.BENEFIT.getCode());
		//accountsTypes.add(AccountCashFundsType.RETURN.getCode());
		accountsTypes.add(AccountCashFundsType.SETTLEMENT.getCode());
		query.setParameter("accountType",accountsTypes);
		//query.setParameter("indCentral",BooleanType.YES.getCode());
		
		List<Object> objectsList = (List<Object>) query.getResultList();	
		List<InstitutionCashAccount> institutionCashAccounts = new ArrayList<InstitutionCashAccount>();
		for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
			institutionCashAccount.setTotalAmount((BigDecimal)objects[0]);
			institutionCashAccount.setAccountType((Integer)objects[1]);
			institutionCashAccount.setIdInstitutionCashAccountPk((Long)objects[2]);
			institutionCashAccounts.add(institutionCashAccount);
		}
		return institutionCashAccounts;
	}
	/**
	 * Search institution cash accounts.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchInstitutionCashAccounts(InstitutionCashAccount institutionCashAccountTO) throws ServiceException{
		List<Object[]>  lstResult = new ArrayList<Object[]>();
		String queryStr = 
					"select " +
					"p1.parameterName," +															//[0]
					"p2.parameterName," +															//[1]
					"sum(ic.totalAmount)," +														//[2]
					"sum(ic.availableAmount)," +													//[3]
					"sum(ic.depositAmount)," +														//[4]
					"sum(ic.retirementAmount)," +													//[5]
					"ic.situation, " +																//[6]
					"ic.accountType, " +															//[7]
					"ic.currency, " +																//[8]
					"ic.indRelatedBcrd " +															//[9]
					"from InstitutionCashAccount ic, ParameterTable p1, ParameterTable p2 " +
					"where " +
					"p1.parameterTablePk = ic.accountType " +
					"and p2.parameterTablePk = ic.currency " +
					"and ic.accountState = :state ";

		if(Validations.validateIsNotNull(institutionCashAccountTO.getAccountType())){
			queryStr += " and ic.accountType =:accountType ";
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getCurrency())){
			queryStr += " and ic.currency =:currency  ";
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getParticipant()) && Validations.validateIsNotNull(institutionCashAccountTO.getParticipant().getIdParticipantPk())){
			queryStr += " and ic.participant.idParticipantPk =:participant  ";
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getIssuer()) && Validations.validateIsNotNull(institutionCashAccountTO.getIssuer().getIdIssuerPk())){
			queryStr += " and ic.issuer.idIssuerPk =:issuer  ";
		}
		
		queryStr += "group by p1.parameterName,p2.parameterName,ic.situation,ic.accountType,ic.currency,ic.indRelatedBcrd";

		Query query = em.createQuery(queryStr);
		if(Validations.validateIsNotNull(institutionCashAccountTO.getAccountType())){
			query.setParameter("accountType",institutionCashAccountTO.getAccountType());
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getCurrency())){
			query.setParameter("currency",institutionCashAccountTO.getCurrency());
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getParticipant()) && Validations.validateIsNotNull(institutionCashAccountTO.getParticipant().getIdParticipantPk())){
			query.setParameter("participant",institutionCashAccountTO.getParticipant().getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getIssuer()) && Validations.validateIsNotNull(institutionCashAccountTO.getIssuer().getIdIssuerPk())){
			query.setParameter("issuer",institutionCashAccountTO.getIssuer().getIdIssuerPk());
		}
		
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());

		lstResult = (List<Object[]>) query.getResultList();
		return lstResult;
	}

	/**
	 * METHOD WHICH GET THE DETAIL OF THE AMOUNTS OF SETTLEMENT CASH ACCOUNTS.
	 *
	 * @param currency the currency
	 * @param participantPk the participant pk
	 * @param issuer the issuer
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> settlementCashAccountMonitoring(Integer currency,Long participantPk,Issuer issuer){
		List<Object[]> lstSettlementAccounts = new ArrayList<Object[]>();

		String queryStr = 
				"select " +
				"ic.participant.idParticipantPk," +						//[0]
				"ic.participant.description," +							//[1]
				"ic.settlementSchema," +								//[2]
				"ic.negotiationMechanism.description," +				//[3]
				"ic.modalityGroup.description," +						//[4]
				"ic.totalAmount " +										//[5]
				"from InstitutionCashAccount ic " +
				"where " +
				"ic.accountType = :accountType " +
				"and ic.currency = :currency " +
				"and ic.accountState = :state " +
				"and ic.totalAmount > 0";
		if(Validations.validateIsNotNullAndPositive(participantPk))
		{
			queryStr += " and ic.participant.idParticipantPk =:participant  ";
		}
		if(Validations.validateIsNotNull(issuer) && Validations.validateIsNotNull(issuer.getIdIssuerPk()))
		{
			queryStr += " and ic.issuer.idIssuerPk =:issuer  ";
		}
		Query query = em.createQuery(queryStr);
		query.setParameter("accountType",AccountCashFundsType.SETTLEMENT.getCode());
		query.setParameter("currency",currency);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		if(Validations.validateIsNotNullAndPositive(participantPk))
		{
			query.setParameter("participant",participantPk);
		}
		if(Validations.validateIsNotNull(issuer) && Validations.validateIsNotNull(issuer.getIdIssuerPk()))
		{
			query.setParameter("issuer",issuer.getIdIssuerPk());
		}
		lstSettlementAccounts = query.getResultList();

		return lstSettlementAccounts;
	}

	/**
	 * METHOD WHICH GET THE DETAIL OF THE AMOUNTS OF BENEFIT CASH ACCOUNTS.
	 *
	 * @param currency the currency
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> benefitCashAccountMonitoring(Integer currency, Issuer issuer){
		List<Object[]> lstSettlementAccounts = new ArrayList<Object[]>();		
		StringBuilder sb = new StringBuilder();
		sb.append( "Select isu.idIssuerPk, ");
		sb.append( " isu.businessName, ");
		sb.append( " pba.description, ");
		sb.append( " ica.totalAmount,");
		sb.append( " ica.idInstitutionCashAccountPk,");
		sb.append( " iba.accountNumber,");
		sb.append( " ica.availableAmount," );
		sb.append( " isu.mnemonic ");
		sb.append( " from InstitutionCashAccount ica ");
		sb.append( " left join ica.cashAccountDetailResults cad ");
		sb.append( " left join cad.institutionBankAccount iba");
		sb.append( " left join ica.issuer isu ");
		sb.append( " left join iba.providerBank pba");
		sb.append( " where ");
		sb.append( " ica.accountType = :accountType ");
		sb.append( " and ica.currency = :currency " );
		sb.append( " and ica.accountState = :state " );
		sb.append( " and ica.totalAmount > 0 ");
		if(Validations.validateIsNotNull(issuer) && Validations.validateIsNotNull(issuer.getIdIssuerPk())) {
			sb.append( " and isu.idIssuerPk = :idIssuerPk ");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("accountType",AccountCashFundsType.BENEFIT.getCode());
		query.setParameter("currency",currency);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		if(Validations.validateIsNotNull(issuer) && Validations.validateIsNotNull(issuer.getIdIssuerPk())) {
			query.setParameter("idIssuerPk",issuer.getIdIssuerPk());
		}		
		lstSettlementAccounts = query.getResultList();

		return lstSettlementAccounts;
	}

	/**
	 * METHOD WHICH GET THE DETAIL OF THE AMOUNTS OF BENEFIT CASH ACCOUNTS.
	 *
	 * @param accountType the account type
	 * @param currency the currency
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> otherCashAccountMonitoring(Integer accountType, Integer currency){
		List<Object[]> lstSettlementAccounts = new ArrayList<Object[]>();

		String queryStr = 
				" select " +
				" ic.participant.idParticipantPk," +						//[0]
				" ic.participant.description," +							//[1]
				" ib.providerBank.description," +						//[2]
				" ic.totalAmount, " +										//[3]	
				" ib.accountNumber, " +
				" ic.idInstitutionCashAccountPk " +
				" from InstitutionCashAccount ic " +
				" left join ic.cashAccountDetailResults cad " +
				" left join cad.institutionBankAccount ib " +				
				" where " +				
				" ic.accountType = :accountType " +
				" and ic.currency = :currency " +
				" and ic.accountState = :state " +
				" and ic.totalAmount > 0";
		Query query = em.createQuery(queryStr);
		query.setParameter("accountType",accountType);
		query.setParameter("currency",currency);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		lstSettlementAccounts = query.getResultList();

		return lstSettlementAccounts;
	}
	
	/**
	 * Gets the withdrawl unfulfill operations.
	 *
	 * @param processDay the process day
	 * @param operationPart the operation part
	 * @return the withdrawl unfulfill operations
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipantSettlement> getWithdrawlUnfulfillOperations(Date processDay, Integer operationPart){
		List<ParticipantSettlement> lstOperations = new ArrayList<ParticipantSettlement>();
		String queryStr = 	"select ps from ParticipantSettlement ps, SettlementOperation so, MechanismOperation m where " +
							"PS.settlementOperation.idSettlementOperationPk = so.idSettlementOperationPk " +
							"and m.idMechanismOperationPk = so.mechanismOperation.idMechanismOperationPk " +
							"and so.indUnfulfilled = :indYes AND trunc(so.settlementDate) = :settlementDate " +
							"and ps.role = :role " +
							//"and p.inchargeType = :inchargeType " +
							"and ps.settlementOperation.operationPart = :operationPart " +
							"and nvl(ps.depositedAmount,0) > 0 " +
							"and so.settlementType = :settlementType " +
							"and nvl(ps.indPayedBack,0) = :indNo ";

		Query query = em.createQuery(queryStr);
		query.setParameter("indNo",BooleanType.NO.getCode());
		query.setParameter("indYes",BooleanType.YES.getCode());
		//query.setParameter("inchargeType",InChargeNegotiationType.INCHARGE_FUNDS.getCode());
		query.setParameter("role",ParticipantRoleType.BUY.getCode());
		query.setParameter("settlementDate",processDay);
		query.setParameter("operationPart",operationPart);
		query.setParameter("settlementType",SettlementType.DVP.getCode());
		lstOperations = query.getResultList();
		return lstOperations;
	}

	/**
	 * Gets the withdrawl settled operations.
	 *
	 * @param processDay the process day
	 * @param operationPart the operation part
	 * @return the withdrawl settled operations
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipantSettlement> getWithdrawlSettledOperations(Date processDay, Integer operationPart){
		List<ParticipantSettlement> lstOperations = new ArrayList<ParticipantSettlement>();
		String queryStr = 	"select ps from ParticipantSettlement ps, SettlementOperation so, MechanismOperation m where " +
							"PS.settlementOperation.idSettlementOperationPk = so.idSettlementOperationPk " +
							"and m.idMechanismOperationPk = so.mechanismOperation.idMechanismOperationPk " +
							"and so.indUnfulfilled = :indNo AND trunc(so.settlementDate) = :settlementDate " +
							"and ps.role = :role " +
							"and ps.settlementOperation.operationPart = :operationPart " +
							//"and p.inchargeType = :inchargeType " +
							"and nvl(ps.depositedAmount,0) > nvl(ps.settlementAmount,0) " +
							"and m.settlementType = :settlementType " +
							"and nvl(ps.indPayedBack,0) = :indNo ";

		Query query = em.createQuery(queryStr);
		query.setParameter("settlementDate",processDay);
		query.setParameter("indNo",BooleanType.NO.getCode());
		query.setParameter("role",ParticipantRoleType.BUY.getCode());
		query.setParameter("settlementType",SettlementType.DVP.getCode());
		query.setParameter("operationPart",operationPart);

		lstOperations = query.getResultList();
		return lstOperations;
	}
	
	/**
	 * METHOD WHICH WILL GET THE CASH ACCOUNT RELATED FOR THE FUNDS WITHDRAWL.
	 *
	 * @param participantPK the participant pk
	 * @param mechanism the mechanism
	 * @param modalityGroup the modality group
	 * @param currency the currency
	 * @return the withdrawl cash account
	 */
	public InstitutionCashAccount getWithdrawlCashAccount(Long participantPK,Long mechanism, Long modalityGroup, Integer currency){
		InstitutionCashAccount cashAccount = new InstitutionCashAccount();
		try{
			String queryStr = 	"select i from InstitutionCashAccount i ";
					queryStr += " left join fetch i.cashAccountDetailResults cad";
					queryStr += " left join fetch cad.institutionBankAccount b";
					queryStr += "  where ";
					queryStr +=" i.participant.idParticipantPk = :participant " +
					"and i.accountType = :cashAccType " +
					"and i.accountState = :cashAccountState " +
					"and i.currency = :currency " +
					"and (cad.indReception = :useAccountType or cad.indSending = :useAccountType ) " +
					"and b.state = :bankAccountState " +
					"and i.negotiationMechanism.idNegotiationMechanismPk = :mechanism " +
					"and i.modalityGroup.idModalityGroupPk = :modalityGroup";
			Query query = em.createQuery(queryStr);
			query.setParameter("participant",participantPK);
			query.setParameter("cashAccType",AccountCashFundsType.SETTLEMENT.getCode());
			query.setParameter("cashAccountState",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("useAccountType",BooleanType.YES.getCode());
			query.setParameter("currency",currency);
			query.setParameter("mechanism",mechanism);
			query.setParameter("modalityGroup",modalityGroup);
			query.setParameter("bankAccountState",BankAccountsStateType.REGISTERED.getCode());
			cashAccount = (InstitutionCashAccount) query.getSingleResult();
		}catch(NoResultException x) {
			x.printStackTrace();
		}
		return cashAccount;
	}
	
	/**
	 * Gets the widthdrawl cash accounts.
	 *
	 * @param filter the filter
	 * @return the widthdrawl cash accounts
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionCashAccount> getWidthdrawlCashAccounts(InstitutionCashAccountTO filter) throws ServiceException{
		List<InstitutionCashAccount> lstAccounts;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT ica FROM InstitutionCashAccount ica left join fetch ica.cashAccountDetailResults cad");
		sbQuery.append(" where ica.accountType=:accountType");
		
		if(Validations.validateIsNotNull(filter.getSettlementSchema())){
	    sbQuery.append(" and ica.settlementSchema=:schema");
		}
		sbQuery.append(" and ica.accountState=:state");
		sbQuery.append(" and ica.availableAmount>0 ");
		Query query =em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNull(filter.getSettlementSchema())){
			query.setParameter("schema", filter.getSettlementSchema());
		} 		 
		query.setParameter("accountType",filter.getAccountType());
		query.setParameter("state", filter.getAccountState());
		lstAccounts =(List<InstitutionCashAccount>) query.getResultList();
		
		return lstAccounts;
	}

	/**
	 * METHOD TO GET THE AMOUNT IN CASH ACCOUNTS RELATED TO THE CENTRALIZER ACCOUNT.
	 *
	 * @return the cash account amount
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getCashAccountAmount() throws ServiceException{
		BigDecimal accountAmount = BigDecimal.ZERO;
		String strSql = 
				"SELECT " +
				"sum(i.totalAmount + i.availableAmount) " +
				"FROM InstitutionCashAccount i " +
				"WHERE " +
				"i.accountType = :type ";

		Query query =em.createQuery(strSql);
		query.setParameter("type",AccountCashFundsType.SETTLEMENT.getCode());
		accountAmount = (BigDecimal) query.getSingleResult();

		return accountAmount;
	}

	/**
	 * METHOD TO GET THE AMOUNT IN CENTRALIZER CASH ACCOUNT.
	 *
	 * @return the centralizer account amount
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getCentralizerAccountAmount() throws ServiceException{
		BigDecimal accountAmount = BigDecimal.ZERO;
		String strSql = 
				"SELECT " +
				"sum(i.totalAmount + i.availableAmount) " +
				"FROM InstitutionCashAccount i " +
				"WHERE " +
				"i.indRelatedBcrd = :indBcb " +
				"and i.accountType = :type " +
				"and i.accountState = :state ";

		Query query =em.createQuery(strSql);
		query.setParameter("indBcb",BooleanType.NO.getCode());
		query.setParameter("type",AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode());
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		accountAmount = (BigDecimal) query.getSingleResult();

		return accountAmount;
	}

	/**
	 * Gets the operations wo close settlement process.
	 *
	 * @param processDay the process day
	 * @param operationPart the operation part
	 * @return the operations wo close settlement process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipantSettlement> getOperationsWOCloseSettlementProcess(Date processDay, Integer operationPart) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ps");
		sbQuery.append("	FROM ParticipantSettlement ps");
		sbQuery.append("	WHERE ps.settlementOperation.indUnfulfilled = :indNo");
		sbQuery.append("	AND trunc(ps.settlementOperation.settlementDate) = :settlementDate ");
		sbQuery.append("	AND ps.role = :role");
		sbQuery.append("	AND ps.settlementOperation.operationState = :operationState");
		//sbQuery.append("	AND po.inchargeType = :inchargeType");
		sbQuery.append("	AND ps.settlementOperation.operationPart = :operationPart");
		sbQuery.append("	AND nvl(ps.depositedAmount,0) > 0");
		sbQuery.append("	AND ps.settlementOperation.settlementType = :settlementType");
		sbQuery.append("	AND nvl(ps.indPayedBack,0) = :indNo");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("indNo",BooleanType.NO.getCode());
		//query.setParameter("inchargeType",InChargeNegotiationType.INCHARGE_FUNDS.getCode());
		query.setParameter("operationState",MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("settlementDate",processDay);
		query.setParameter("role",ParticipantRoleType.BUY.getCode());		
		query.setParameter("operationPart",operationPart);
		query.setParameter("settlementType",SettlementType.DVP.getCode());	
		return query.getResultList();
	}

	/**
	 * Gets the last process day.
	 *
	 * @return the last process day
	 * @throws ServiceException the service exception
	 */
	public Date getLastProcessDay() throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("   SELECT processDay ");
			sbQuery.append("   FROM BeginEndDay BED ");
			sbQuery.append("	WHERE ROWNUM < 2");
			sbQuery.append("	ORDER BY idBeginEndDayPk DESC");
			Query query = em.createQuery(sbQuery.toString());
			return (Date) query.getSingleResult();
		}catch(NoResultException x){
			x.getStackTrace();
			return null;
		}
	}
	
	/**
	 * Gets the last open day.
	 *
	 * @return the last open day
	 * @throws ServiceException the service exception
	 */
	public Date getLastOpenDay() throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("   SELECT OPEN_DAY ");
			sbQuery.append("   FROM BEGIN_END_DAY BED ");
			sbQuery.append("	WHERE ROWNUM < 2");
			sbQuery.append("	AND OPEN_DAY IS NOT NULL");
			sbQuery.append("	ORDER BY ID_BEGIN_END_DAY_PK DESC");
			Query query = em.createNativeQuery(sbQuery.toString());
			return (Date) query.getSingleResult();
		}catch(NoResultException x){
			x.getStackTrace();
			return null;
		}
	}
	
	/**
	 * Gets the last process day.
	 *
	 * @return the last process day
	 * @throws ServiceException the service exception
	 */
	public Date getLastCloseDay() throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("   SELECT CLOSE_DAY ");
			sbQuery.append("   FROM BEGIN_END_DAY BED ");
			sbQuery.append("	WHERE ROWNUM < 2");
			sbQuery.append("	AND CLOSE_DAY IS NOT NULL");
			sbQuery.append("	ORDER BY ID_BEGIN_END_DAY_PK DESC");
			Query query = em.createNativeQuery(sbQuery.toString());
			return (Date) query.getSingleResult();
		}catch(NoResultException x){
			x.getStackTrace();
			return null;
		}
	}

	/**
	 * Gets the account type related central.
	 *
	 * @param currency the currency
	 * @return the account type related central
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getAccountTypeRelatedCentral(Integer currency) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT p1.parameterName,");				//0
		sbQuery.append("			p2.parameterName,");			//1
		sbQuery.append("			sum(ic.totalAmount),");			//2
		sbQuery.append("			sum(ic.availableAmount),");		//3
		sbQuery.append("			sum(ic.depositAmount),");		//4
		sbQuery.append("			sum(ic.retirementAmount),");	//5
		sbQuery.append("			ic.accountType,");				//6
		sbQuery.append("			ic.currency");					//7
		sbQuery.append("	from InstitutionCashAccount ic, ParameterTable p1, ParameterTable p2");
		sbQuery.append("	where p1.parameterTablePk = ic.accountType");
		sbQuery.append("	and p2.parameterTablePk = ic.currency");
		sbQuery.append("	and ic.accountState = :state");
		sbQuery.append("	and ic.currency = :currency");
		sbQuery.append("	and ic.indRelatedBcrd = :indYes");
		sbQuery.append("	group by p1.parameterName,p2.parameterName,ic.accountType,ic.currency");

		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("indYes",BooleanType.YES.getCode());
		query.setParameter("currency", currency);
		return query.getResultList();		
	}
	
	/**
	 * Gets the accounts validate begin day.
	 *
	 * @return the accounts validate begin day
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionCashAccount> getAccountsValidateBeginDay() {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ic ");					
		sbQuery.append("	from InstitutionCashAccount ic");
		sbQuery.append("	where ic.accountState = :state");
		sbQuery.append("	and ic.totalAmount > 0");
		sbQuery.append("	and ic.indRelatedBcrd = :indYes");

		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("indYes",BooleanType.YES.getCode());
		return query.getResultList();		
	}
	
	/**
	 * Gets the participant send bcb information.
	 *
	 * @param institutionCashAccountPk the institution cash account pk
	 * @return the participant send bcb information
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionCashAccount> getParticipantSendBCBInformation(Long institutionCashAccountPk){
		List<InstitutionCashAccount> lstSettlementAccounts = new ArrayList<InstitutionCashAccount>();		
		String queryStr = "Select ica"
				+ " from InstitutionCashAccount ica "
				+ " left join fetch ica.cashAccountDetailResults cad "
				+ " left join fetch cad.institutionBankAccount iba "
				+ " left join fetch ica.issuer isu "
				+ " left join fetch ica.participant part "
				+ " left join fetch iba.bank pba "
				+ " where "
				+ " ica.idInstitutionCashAccountPk = :idInstitutionCashAccount " 
				+ " and ica.accountState = :state "
				+ " and cad.indSending = :indSending";
		Query query = em.createQuery(queryStr);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("idInstitutionCashAccount",institutionCashAccountPk);
		query.setParameter("indSending",GeneralConstants.ONE_VALUE_INTEGER);
		lstSettlementAccounts = query.getResultList();

		return lstSettlementAccounts;
	}
	
	
	
	
}