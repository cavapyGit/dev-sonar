package com.pradera.funds.effective.accounts.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.swift.to.CashAccountsSearchTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.bank.accounts.to.SearchBankAccountsTO;
import com.pradera.funds.effective.accounts.facade.ManageEffectiveAccountsFacade;
import com.pradera.funds.effective.accounts.to.BankAccounts;
import com.pradera.funds.effective.accounts.to.BankAccountsDataModel;
import com.pradera.funds.effective.accounts.to.BankDataTO;
import com.pradera.funds.effective.accounts.to.CashAccountsTO;
import com.pradera.funds.effective.accounts.to.CashAccountsTODataModel;
import com.pradera.funds.effective.accounts.to.ModalityGroupDataModel;
import com.pradera.funds.effective.accounts.to.SearchEffectiveAccountsTO;
import com.pradera.funds.fundoperations.monitoring.facade.MonitoringFundOperationsServiceFacade;
import com.pradera.funds.fundoperations.util.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.settlements.core.service.SettlementProcessService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageEffectiveAccountsBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ManageEffectiveAccountsBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	 /** The manage Effective Accounts Facade. */
	@EJB
	ManageEffectiveAccountsFacade manageEffectiveAccountsFacade;
	
	 /** The institution bank account Session. */
	private InstitutionCashAccount institutionCashAccountSession;
	
	/** search Effective Accounts Register TO. */
	private SearchEffectiveAccountsTO searchEffectiveAccountsRegisterTO;
	
	/** search Effective Accounts TO. */
	private SearchEffectiveAccountsTO searchEffectiveAccountsTO;
	
	/** The general parameters facade. */
    @EJB
    GeneralParametersFacade generalParametersFacade;
    
    /** The monitor cash account facade. */
    @EJB
	private MonitoringFundOperationsServiceFacade monitorCashAccountFacade;
    
    /** The participant service bean. */
    @EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The issuer query service bean. */
	@EJB
	private IssuerQueryServiceBean issuerQueryServiceBean;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade;
	
	@EJB
	private SettlementProcessService settlementProcessServiceBean;
    
    /** The institution Bank Account. */
    private InstitutionBankAccount institutionBankAccount;	
    
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The lst cash account type. */
	private List<ParameterTable> lstCashAccountType;
	
	/** The lst account state. */
	private List<ParameterTable> lstAccountState;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The bl register. */
	private boolean blModify, blActive, blLock, blView, blRegister;
	
	/** The register cash account. */
	private RegisterCashAccount registerCashAccount;
	
	/** The bank accounts. */
	private BankAccounts bankAccounts;
	
	/** The cash account type selected. */
	private Integer cashAccountTypeSelected;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The participant selected. */
	private Integer participantSelected;
	
	/** The lst issuers. */
	private List<Issuer> lstIssuers;
	
	/** The currency selected. */
	private Integer currencySelected;
	
	/** The state account selected. */
	private Integer stateAccountSelected;
	
	/** The lst bank associated. */
	private List<Bank> lstBankAssociated;
	
	/** The bank associated selected. */
	private Integer bankAssociatedSelected;	
	
	/** The lst modality group. */
	private List<ModalityGroup> lstModalityGroup;
	
	/** The id moda group pk selected. */
	private Long idModaGroupPkSelected;
	
	/** The lst mechanism. */
	private List<NegotiationMechanism> lstMechanism;
	
	/** The id nego mechanism pk selected. */
	private Long idNegoMechanismPkSelected;
	
	/** The ind show mechanism. */
	private int indShowMechanism;
	
	/** The ind show modality group. */
	private int indShowModalityGroup;
	
	/** The ind show settlement schema. */
	private int indShowSettlementSchema;
	
	/** The ind show bic code. */
	private int indShowBicCode;
	
	/** The ind show bank. */
	private int indShowBank;
	
	/** The ind show bank account number. */
	private int indShowBankAccountNumber;
	
	/** The ind show commercial. */
	private int indShowCommercial;
	
	/** The ind show issuer. */
	private int indShowIssuer;
	
	/** The ind show participant. */
	private int indShowParticipant;
	
	/** The ind show cash accounts. */
	private int indShowCashAccounts;
	
	/** The cash account. */
	private CashAccountsTO cashAccount;
	
	/** The query cash account data model. */
	private CashAccountsTODataModel queryCashAccountDataModel;
	
	/** The Constant STR_VIEW_CASH_ACCOUNT. */
	private final static String STR_VIEW_CASH_ACCOUNT = "registerCashAccount";
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The lst institution bank accounts type. */
	private List<ParameterTable> lstInstitutionBankAccountsType;
	
	/** The lst institution bank accounts type. */
	private List<ParameterTable> lstInstitutionBankAccountsTypeReturn;
	
	/** The obj institution bank accounts type. */
	private Integer objInstitutionBankAccountsType;
	
	private List<InstitutionBankAccount> lstInstitutionBankAccount;
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		try{
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_FUND_ACCOUNT_TYPE.getCode());
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
			lstCashAccountType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			//lstCashAccountType = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_FUND_ACCOUNT_TYPE.getCode());
			lstAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_CASH_ACCOUNT_STATE.getCode());
			ParameterTableTO objParameterTableTO=new ParameterTableTO(); 
			objParameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			objParameterTableTO.setIndicator2(GeneralConstants.ONE_VALUE_STRING);
			lstCurrency = generalParametersFacade.getListParameterTableServiceBean(objParameterTableTO);
			showPrivilegesButtons();
			fillInstitutionBankAccountsType();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Show privileges buttons.
	 */
	private void showPrivilegesButtons(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnActivate(true);		
		privilegeComponent.setBtnDeactivate(true);
		privilegeComponent.setBtnModifyView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Registry Effective Account Handler.
	 *
	 * @return the string
	 */
	public String sendRegisterPage() {
		try {
			blRegister = true;
			blActive = false;
			blView = false;
			blModify = false;
			blLock = false;
			registerCashAccount = new RegisterCashAccount();
			if (lstCashAccountType == null) {
				lstCashAccountType = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_FUND_ACCOUNT_TYPE.getCode());
			}
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_FUND_ACCOUNT_TYPE.getCode());
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
			filterParameterTable.setShortInteger(GeneralConstants.ONE_VALUE_INTEGER);
			lstCashAccountType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			registerCashAccount.setLstCashAccountType(lstCashAccountType);
			return "registerCashAccount";
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Show entity.
	 *
	 * @throws Exception the exception
	 */
	public void showEntity() throws Exception {
		// SET THE CASH ACCOUNT DESCRIPTION
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetViewRoot();
		participantSelected = null;
		issuer=new Issuer();
		idNegoMechanismPkSelected = null;
		idModaGroupPkSelected = null;
		lstMechanism = null;
		lstModalityGroup = null;
		indShowMechanism = BooleanType.NO.getCode();
		indShowIssuer = BooleanType.NO.getCode();
		indShowParticipant = BooleanType.NO.getCode();
		if (AccountCashFundsType.SETTLEMENT.getCode().equals(cashAccountTypeSelected)) {
			registerCashAccount = new RegisterCashAccount();
			registerCashAccount.setSettlerAccount(new SettlerAccount());
			fillParticipant(false);
			registerCashAccount.getSettlerAccount().setBankAccountClassSelected(null);
			indShowMechanism = BooleanType.YES.getCode();			
			indShowParticipant = BooleanType.YES.getCode();			
		} else if (AccountCashFundsType.BENEFIT.getCode().equals(cashAccountTypeSelected)) {
			fillIssuer();
			indShowIssuer = BooleanType.YES.getCode();
		} else if (AccountCashFundsType.GUARANTEES.getCode().equals(cashAccountTypeSelected)
					|| AccountCashFundsType.SPECIAL_PAYMENT.getCode().equals(cashAccountTypeSelected)) {
			fillParticipant(false);
			indShowParticipant = BooleanType.YES.getCode();
		}
	}
	
	/**
	 * Fill participant.
	 *
	 * @param blDirect the bl direct
	 */
	private void fillParticipant(boolean blDirect){
		try {
			lstParticipants = monitorCashAccountFacade.getParticipants(blDirect);	
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Fill issuer.
	 */
	private void fillIssuer() {
		try{
			if (lstIssuers == null) {
				lstIssuers = monitorCashAccountFacade.getIssuers();
			}
		}catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load mechanism.
	 */
	public void loadMechanism(){
		JSFUtilities.hideGeneralDialogues();
		try {
			idModaGroupPkSelected = null;
			lstModalityGroup = null;
			idNegoMechanismPkSelected = null;
			if(Validations.validateIsNotNullAndNotEmpty(participantSelected)){
				lstMechanism = manageEffectiveAccountsFacade.getLstMechanism(new Long(participantSelected));
			}else{
				lstMechanism = null;			
			}	
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Search cash account.
	 */
	@LoggerAuditWeb
	public void searchCashAccount(){
		JSFUtilities.hideGeneralDialogues();
		try{
			cashAccount = new CashAccountsTO() ;
			indShowBicCode = BooleanType.NO.getCode();
			// GUARANTEE ACCOUNT
			if (AccountCashFundsType.GUARANTEES.getCode().equals(cashAccountTypeSelected)){
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 1;
			indShowBank = 1;
			indShowCommercial = 0;
		} else if (AccountCashFundsType.BENEFIT.getCode().equals(cashAccountTypeSelected)){
			// BENEFIT PAYMENT ACCOUNT
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 0;
			indShowBank = 1;
			indShowCommercial = 1;
		} else if (AccountCashFundsType.CENTRALIZING.getCode().equals(cashAccountTypeSelected)
				|| AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(cashAccountTypeSelected)
				|| AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(cashAccountTypeSelected)
				|| AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(cashAccountTypeSelected)
				|| AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(cashAccountTypeSelected)
				|| AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(cashAccountTypeSelected)
				|| AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(cashAccountTypeSelected)
				|| AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(cashAccountTypeSelected)){
			// CENTRALIZING ACCOUNT
			indShowBicCode = BooleanType.YES.getCode();
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 1;
			indShowCommercial = 0;
		} else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(cashAccountTypeSelected)){
			// INTERNATIONAL OPERATIONS ACCOUNT
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 0;
			indShowCommercial = 0;
		} else if (AccountCashFundsType.RATES.getCode().equals(cashAccountTypeSelected)){
			// RATE ACCOUNT
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 1;
			indShowBank = 1;
			indShowCommercial = 0;
		} else if (AccountCashFundsType.RETURN.getCode().equals(cashAccountTypeSelected)){
			// RETURN ACCOUNT
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 1;
			indShowBank = 1;
			indShowCommercial = 1;
		} else if (AccountCashFundsType.SETTLEMENT.getCode().equals(cashAccountTypeSelected)){
			// SETTLEMENT ACCOUNT
			indShowMechanism = 1;
			indShowModalityGroup = 1;
			indShowSettlementSchema = 1;
			indShowBankAccountNumber = 1;
			indShowBank = 1;
			indShowCommercial = 0;
			indShowBicCode = BooleanType.YES.getCode();
		} else if (AccountCashFundsType.TAX.getCode().equals(cashAccountTypeSelected)){
			// TAX ACCOUNT
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 1;
			indShowBank = 1;
			indShowCommercial = 0;
		} else if (AccountCashFundsType.SPECIAL_PAYMENT.getCode().equals(cashAccountTypeSelected)){
			indShowMechanism = 0;
			indShowModalityGroup = 0;
			indShowSettlementSchema = 0;
			indShowBankAccountNumber = 1;
			indShowBank = 1;
			indShowCommercial = 0;
		}

		CashAccountsSearchTO parameters = new CashAccountsSearchTO();
		parameters.setCashAccountType(cashAccountTypeSelected);
		parameters.setCurrency(currencySelected);
		parameters.setState(stateAccountSelected);
		if (Validations.validateIsNotNullAndNotEmpty(issuer)) 
			parameters.setIssuer(issuer.getIdIssuerPk());			
		if(Validations.validateIsNotNullAndNotEmpty(participantSelected))
			parameters.setParticipant(participantSelected);		
		if(Validations.validateIsNotNullAndNotEmpty(idModaGroupPkSelected))
			parameters.setIdModalityGroupPk(idModaGroupPkSelected);		
		if(Validations.validateIsNotNullAndNotEmpty(idNegoMechanismPkSelected))
			parameters.setIdNegoMechanismPk(idNegoMechanismPkSelected);

		List<CashAccountsTO> lstCashAccounts = manageEffectiveAccountsFacade.searchCashAccount(parameters);		
		if (Validations.validateListIsNotNullAndNotEmpty(lstCashAccounts)) {
			indShowCashAccounts = 1;
			queryCashAccountDataModel = new CashAccountsTODataModel(lstCashAccounts);
		} else {
			indShowCashAccounts = 2;
			queryCashAccountDataModel = null;
		}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean screen.
	 */
	public void cleanScreen() {
		JSFUtilities.resetViewRoot();
		cashAccountTypeSelected = null;
		participantSelected = null;
		currencySelected = null;
		stateAccountSelected = null;
		idNegoMechanismPkSelected = null;
		idModaGroupPkSelected = null;		
		cashAccount = new CashAccountsTO();
		queryCashAccountDataModel = null;
		indShowCashAccounts = BooleanType.NO.getCode();
		indShowIssuer = BooleanType.NO.getCode();
		indShowParticipant = BooleanType.NO.getCode();
		indShowCashAccounts = BooleanType.NO.getCode();
		indShowMechanism = BooleanType.NO.getCode();
		
	}
	
	/**
	 * Validate active.
	 *
	 * @return the string
	 */
	public String validateActive(){
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNotNullAndNotEmpty(cashAccount)){
			if(CashAccountStateType.REGISTERED.getCode().equals(cashAccount.getIdState())){
				blView = true;
				blActive = true;
				blLock = false;
				blModify = false;
				blRegister = false;
				fillView(cashAccount);
				return STR_VIEW_CASH_ACCOUNT;
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_INVALID_ACTIVE));
				JSFUtilities.showValidationDialog();
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}		
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Validate deactive.
	 *
	 * @return the string
	 */
	public String validateDeactive(){
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNotNullAndNotEmpty(cashAccount)){
			if(CashAccountStateType.ACTIVATE.getCode().equals(cashAccount.getIdState())){
				blView = true;
				blActive = false;
				blLock = true;
				blModify = false;
				blRegister = false;
				fillView(cashAccount);
				return STR_VIEW_CASH_ACCOUNT;
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_INVALID_DEACTIVE));
				JSFUtilities.showValidationDialog();
			}				
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}		
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Validate modify.
	 *
	 * @return the string
	 */
	public String validateModify(){
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNotNullAndNotEmpty(cashAccount)){
			if(CashAccountStateType.REGISTERED.getCode().equals(cashAccount.getIdState()))
			{
				InstitutionCashAccount instCashAcc;
				try {
					registerCashAccount = new RegisterCashAccount();
					registerCashAccount.setLstCashAccountType(lstCashAccountType);
					instCashAcc = manageEffectiveAccountsFacade.getInstitutionCashAccountData(cashAccount.getIdCashAccount());
					registerCashAccount.setCashAccountTypeSelected(instCashAcc.getAccountType());
					registerCashAccount.setAccountState(instCashAcc.getAccountState());
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
						||AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
						||AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
						||AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_INVALID_MODIFY_RETURN));
					JSFUtilities.showValidationDialog();
				}else {
					blView = false;
					blActive = false;
					blLock = false;
					blModify = true;
					blRegister = false;
					fillView(cashAccount);
					return STR_VIEW_CASH_ACCOUNT;
				}
			}
			else
			{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_INVALID_MODIFY));
				JSFUtilities.showValidationDialog();
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}		
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Fill view.
	 *
	 * @param cashAccTO the cash acc to
	 */
	private void fillView(CashAccountsTO cashAccTO){
		
		registerCashAccount = new RegisterCashAccount();
		registerCashAccount.setLstCashAccountType(lstCashAccountType);
		setOffIndicators();
		fillCurrency();		
		// ACCORDING TO CASH ACCOUNT TYPE, WE FILL DIFFERENT SCREEN
		try {
			InstitutionCashAccount instCashAcc = manageEffectiveAccountsFacade.getInstitutionCashAccountData(cashAccTO.getIdCashAccount());
			registerCashAccount.setCashAccountTypeSelected(instCashAcc.getAccountType());
			registerCashAccount.setAccountState(instCashAcc.getAccountState());
			if (AccountCashFundsType.CENTRALIZING.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setCentralizingAccount(new CentralizingAccount());			
				registerCashAccount.getCentralizingAccount().setLstBankAccountType(fillBankAccountType());
				registerCashAccount.getCentralizingAccount().setBankAccountType(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBankAcountType().intValue());				
				registerCashAccount.getCentralizingAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
				List<String> lstValues = manageEffectiveAccountsFacade.getBCBBankBicCode();
				registerCashAccount.getCentralizingAccount().setBicCode(lstValues.get(0));
				registerCashAccount.getCentralizingAccount().setBank(lstValues.get(1));				
				registerCashAccount.getCentralizingAccount().setBankAccount(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
				registerCashAccount.getCentralizingAccount().setLstCurrency(lstCurrency);
				registerCashAccount.getCentralizingAccount().setCurrencySelected(instCashAcc.getCurrency());
				registerCashAccount.setIndCentralizingAccount(BooleanType.YES.getCode());
				if(!blModify)
					fillViewBankAccountData(instCashAcc);
				else
					fillModifyBankAccountData(instCashAcc);
					
			} else if (AccountCashFundsType.SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setSettlerAccount(new SettlerAccount());
				fillParticipant(false);				

				if (registerCashAccount.getSettlerAccount().getUseTypeOptions() == null 
						|| registerCashAccount.getSettlerAccount().getUseTypeOptions().isEmpty()) {
					registerCashAccount.getSettlerAccount().setUseTypeOptions(
							generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_CASH_ACCOUNT_USE_TYPE.getCode()));
				}
				if (registerCashAccount.getSettlerAccount().getLstBankAccountClass() == null || registerCashAccount.getSettlerAccount().getLstBankAccountClass().size() == 0) {
					registerCashAccount.getSettlerAccount().setLstBankAccountClass(
							generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_CLASS.getCode()));
				}

				if (registerCashAccount.getSettlerAccount().getLstBankAccountType() == null || registerCashAccount.getSettlerAccount().getLstBankAccountType().size() == 0) {
					registerCashAccount.getSettlerAccount().setLstBankAccountType(
							generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode()));
				}
				registerCashAccount.getSettlerAccount().setLstParticipant(lstParticipants);				
				registerCashAccount.getSettlerAccount().setUseType(null);
				registerCashAccount.getSettlerAccount().setLstCurrency(lstCurrency);
				registerCashAccount.getSettlerAccount().setParticipantSelected(new Integer(instCashAcc.getParticipant().getIdParticipantPk().toString()));
				registerCashAccount.getSettlerAccount().setLstMechanism(manageEffectiveAccountsFacade.getLstMechanism(instCashAcc.getParticipant().getIdParticipantPk()));
				registerCashAccount.getSettlerAccount().setLstModalityGroup(manageEffectiveAccountsFacade.getLstModaGroup(instCashAcc.getNegotiationMechanism().getIdNegotiationMechanismPk()));
				registerCashAccount.getSettlerAccount().setMechanism(instCashAcc.getNegotiationMechanism().getIdNegotiationMechanismPk());
				registerCashAccount.getSettlerAccount().setModalityGroup(instCashAcc.getModalityGroup().getIdModalityGroupPk());
				registerCashAccount.getSettlerAccount().setCurrencySelected(instCashAcc.getCurrency());
				setCurrency();
				String participantBic = manageEffectiveAccountsFacade.getParticipantBicCode(registerCashAccount.getSettlerAccount().getParticipantSelected());
				if(Validations.validateIsNotNull(participantBic))
					registerCashAccount.getSettlerAccount().setBicCode(participantBic);
				List<BankAccounts> lstBanks = new ArrayList<BankAccounts>();
				for(CashAccountDetail cashAccountDetail:instCashAcc.getCashAccountDetailResults()){
					BankAccounts bankTO = new BankAccounts();
					bankTO.setIdInstitutionBankAccountPk(cashAccountDetail.getInstitutionBankAccount().getIdInstitutionBankAccountPk());
					bankTO.setBankAccountNumber(cashAccountDetail.getInstitutionBankAccount().getAccountNumber());
					bankTO.setCurrency(registerCashAccount.getSettlerAccount().getBankAccountCurrecy());
					for (ParameterTable bean:registerCashAccount.getSettlerAccount().getLstBankAccountClass()) {
						if ((bean.getParameterTablePk() + "").equals(cashAccountDetail.getBankAccountClass().toString())){
							bankTO.setIdBankAccountClass(bean.getParameterTablePk().longValue());
							bankTO.setBankAccountClass(bean.getDescription());
						}
					}
					bankTO.setIdBankAccountType(cashAccountDetail.getInstitutionBankAccount().getBankAcountType());
					bankTO.setBankAccountType(BankAccountType.get(new Integer(bankTO.getIdBankAccountType().toString())).getValue());
					if (Validations.validateIsNotNull(cashAccountDetail.getInstitutionBankAccount().getBank())) {
						bankTO.setOtherBank(cashAccountDetail.getInstitutionBankAccount().getBank().getDescription());
						bankTO.setBankAccountId(cashAccountDetail.getInstitutionBankAccount().getBank().getIdBankPk());
					} else {
						bankTO.setOtherBank(cashAccountDetail.getInstitutionBankAccount().getProviderBank().getDescription());
						bankTO.setBankAccountId(cashAccountDetail.getInstitutionBankAccount().getProviderBank().getIdBankPk());
					}
										
					if(cashAccountDetail.getIndReception().equals(BooleanType.YES.getCode()))
						bankTO.setIndReception(BooleanType.YES.getValue());
					else
						bankTO.setIndReception(BooleanType.NO.getValue());
					
					if(cashAccountDetail.getIndSending().equals(BooleanType.YES.getCode()))
						bankTO.setIndSent(BooleanType.YES.getValue());
					else
						bankTO.setIndSent(BooleanType.NO.getValue());					
					lstBanks.add(bankTO);
				}
				registerCashAccount.getSettlerAccount().setLstTypeAccount(fillBankType());
				registerCashAccount.getSettlerAccount().setLstBankAccounts(lstBanks);
				registerCashAccount.getSettlerAccount().setBankAccountDataModel(new BankAccountsDataModel(registerCashAccount.getSettlerAccount().getLstBankAccounts()));
				registerCashAccount.setIndSettlerAccount(BooleanType.YES.getCode());
			} else if (AccountCashFundsType.BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setBenefitPaymentAccount(new BenefitPaymentAccount());
				fillIssuer();
				registerCashAccount.getBenefitPaymentAccount().setLstCurrency(lstCurrency);
				registerCashAccount.getBenefitPaymentAccount().setLstIssuer(lstIssuers);
				registerCashAccount.getBenefitPaymentAccount().setLstTypeAccount(fillBankType());
				registerCashAccount.getBenefitPaymentAccount().setIssuer(instCashAcc.getIssuer());
				registerCashAccount.getBenefitPaymentAccount().setCurrencySelected(instCashAcc.getCurrency());
				
				if(BooleanType.YES.getCode().equals(instCashAcc.getIndRelatedBcrd())){
					registerCashAccount.getBenefitPaymentAccount().setTypeAccountSelected(BankType.CENTRALIZING.getCode());
				}else{
					registerCashAccount.getBenefitPaymentAccount().setTypeAccountSelected(BankType.COMMERCIAL.getCode());
				}
				
				registerCashAccount.getBenefitPaymentAccount().setLstBank(manageEffectiveAccountsFacade.fillBankByType(registerCashAccount.getBenefitPaymentAccount().getTypeAccountSelected()));
				registerCashAccount.getBenefitPaymentAccount().setBankSelected(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk().intValue());
				registerCashAccount.getBenefitPaymentAccount().setBicCode(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getBicCode());
				registerCashAccount.getBenefitPaymentAccount().setBankAccount(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
				registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount());
				registerCashAccount.setIndBenefitPaymentAccount(BooleanType.YES.getCode());		
				
			} 
			else if (AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setMarginGuaranteeAccount(new MarginGuaranteeAccount());
				fillParticipant(false);
				registerCashAccount.getMarginGuaranteeAccount().setLstParticipant(lstParticipants);
				registerCashAccount.getMarginGuaranteeAccount().setParticipantSelected(new Integer(instCashAcc.getParticipant().getIdParticipantPk().toString()));
				registerCashAccount.getMarginGuaranteeAccount().setLstBankAccountType(fillBankAccountType());
				registerCashAccount.getMarginGuaranteeAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
				registerCashAccount.getMarginGuaranteeAccount().setLstBankType(fillBankType());
				registerCashAccount.getMarginGuaranteeAccount().setBankType(BankType.COMMERCIAL.getCode());
				registerCashAccount.getMarginGuaranteeAccount().setLstBank(fillCommercialBank());				
				registerCashAccount.getMarginGuaranteeAccount().setBankSelected(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk().intValue());				
				registerCashAccount.setIndMaginGuaranteeAccount(BooleanType.YES.getCode());
				if(!blModify)
					fillViewBankAccountData(instCashAcc);
				else
					fillModifyBankAccountData(instCashAcc);
				
				
			}
			else if (AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setRatesAccount(new RatesAccount());		
				registerCashAccount.getRatesAccount().setLstAtomatic(fillAutomaticIndicators());
				registerCashAccount.getRatesAccount().setIndAutomatic(instCashAcc.getIndAutomaticProcess());
				registerCashAccount.getRatesAccount().setLstBankAccountType(fillBankAccountType());				
				registerCashAccount.getRatesAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
				registerCashAccount.getRatesAccount().setLstBankType(fillBankType());
				registerCashAccount.getRatesAccount().setBankType(BankType.COMMERCIAL.getCode());
				registerCashAccount.getRatesAccount().setLstBank(fillCommercialBank());
				registerCashAccount.getRatesAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
				registerCashAccount.getRatesAccount().setBankSelected(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk().intValue());
				registerCashAccount.setIndRatesAccount(BooleanType.YES.getCode());
				if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
					this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
				}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
					this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
				}
				if(!blModify)
					fillViewBankAccountData(instCashAcc);
				else
					fillModifyBankAccountData(instCashAcc);
				
			} else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setInternationalOperationsAccount(new InternationalOperationsAccount());
				fillInternationalParticipant();
				registerCashAccount.getInternationalOperationsAccount().setLstParticipants(lstParticipants);
				registerCashAccount.getInternationalOperationsAccount().setParticipantSelected(new Integer(instCashAcc.getParticipant().getIdParticipantPk().toString()));
				registerCashAccount.getInternationalOperationsAccount().setLstBankAccountType(fillBankAccountType());
				registerCashAccount.getInternationalOperationsAccount().setBankAccountType(BankAccountType.CURRENT.getCode());				
				registerCashAccount.getInternationalOperationsAccount().setLstBankType(fillBankType());
				registerCashAccount.getInternationalOperationsAccount().setBankType(BankType.COMMERCIAL.getCode());
				registerCashAccount.getInternationalOperationsAccount().setLstBank(fillCommercialBank());				
				registerCashAccount.getInternationalOperationsAccount().setBankSelected(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk().intValue());
				registerCashAccount.setIndInternationalOperationsAccount(BooleanType.YES.getCode());
				if(!blModify)
					fillViewBankAccountData(instCashAcc);
				else
					fillModifyBankAccountData(instCashAcc);				
				
			} else if (AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setReturnAccount(new ReturnAccount());
				fillParticipant(false);
				fillIssuer();
				registerCashAccount.getReturnAccount().setLstParticipant(lstParticipants);
				registerCashAccount.getReturnAccount().setLstCurrency(lstCurrency);
				registerCashAccount.getReturnAccount().setLstIssuer(lstIssuers);
				if(Validations.validateIsNotNullAndNotEmpty(instCashAcc.getIssuer())) {
					registerCashAccount.getReturnAccount().setIssuer(instCashAcc.getIssuer());
					objInstitutionBankAccountsType=InstitutionBankAccountsType.ISSUER.getCode();
				}	
				if(Validations.validateIsNotNullAndNotEmpty(instCashAcc.getParticipant())) {
					registerCashAccount.getReturnAccount().setParticipantSelected(new Integer(instCashAcc.getParticipant().getIdParticipantPk().toString()));
					objInstitutionBankAccountsType=InstitutionBankAccountsType.PARTICIPANT.getCode();
				}	
				registerCashAccount.getReturnAccount().setCurrencySelected(instCashAcc.getCurrency());
				registerCashAccount.getReturnAccount().setLstAutomatic(fillAutomaticIndicators());
				registerCashAccount.getReturnAccount().setIndAutomatic(instCashAcc.getIndAutomaticProcess());
				registerCashAccount.getReturnAccount().setLstBankAccountType(fillBankAccountType());				
				registerCashAccount.getReturnAccount().setBankAccountType(BankAccountType.CURRENT.getCode());				
				registerCashAccount.getReturnAccount().setLstBankType(fillBankType());
				registerCashAccount.getReturnAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
				if(BooleanType.YES.getCode().equals(instCashAcc.getIndRelatedBcrd())){
					registerCashAccount.getReturnAccount().setBankType(BankType.CENTRALIZING.getCode());
				}else{
					registerCashAccount.getReturnAccount().setBankType(BankType.COMMERCIAL.getCode());
					registerCashAccount.getReturnAccount().setBlOnlyCommercial(true);
				}
				fillInstitutionBankAccountsTypeReturn();
				fillBanksByType();
				registerCashAccount.getReturnAccount().setBankSelected(new Integer(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk().intValue()));				
				registerCashAccount.setIndReturnAccount(BooleanType.YES.getCode());
//				if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
//					this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
//				}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
//					this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				}
				if(!blModify)
					fillViewBankAccountData(instCashAcc);
				else
					fillModifyBankAccountData(instCashAcc);
				
				
			} else if (AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				registerCashAccount.setTaxesAccount(new TaxesAccount());
				registerCashAccount.getTaxesAccount().setLstAtomatic(fillAutomaticIndicators());
				registerCashAccount.getTaxesAccount().setIndAutomatic(instCashAcc.getIndAutomaticProcess());
				registerCashAccount.getTaxesAccount().setLstBankAccountType(fillBankAccountType());
				registerCashAccount.getTaxesAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
				registerCashAccount.getTaxesAccount().setLstBank(fillCommercialBank());
				registerCashAccount.getTaxesAccount().setLstBankType(fillBankType());
				registerCashAccount.getTaxesAccount().setBankType(BankType.COMMERCIAL.getCode());						
				registerCashAccount.getTaxesAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
				registerCashAccount.getTaxesAccount().setBankSelected(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk().intValue());
				registerCashAccount.setIndTaxesAccount(BooleanType.YES.getCode());
				if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
					this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
				}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
					this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
				}
				if(!blModify)
					fillViewBankAccountData(instCashAcc);
				else
					fillModifyBankAccountData(instCashAcc);
								
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fill banks by type.
	 */
	public void fillBanksByType() {
		Integer bankType = registerCashAccount.getReturnAccount().getBankType();
		registerCashAccount.getReturnAccount().setBlOnlyCommercial(false);
		registerCashAccount.getReturnAccount().setLstBank(new ArrayList<Bank>());
		if(Validations.validateIsNotNullAndNotEmpty(bankType)){
			if(BankType.COMMERCIAL.getCode().equals(bankType))
				registerCashAccount.getReturnAccount().setBlOnlyCommercial(true);
			List<Bank> lstBanks = manageEffectiveAccountsFacade.fillBankByType(bankType);
			registerCashAccount.getReturnAccount().setLstBank(lstBanks);			
		}		
	}
	
	/**
	 * Show cash account detail.
	 *
	 * @param event the event
	 */
	public void showCashAccountDetail(ActionEvent event){	
		try{
			CashAccountsTO cashAccTO = (CashAccountsTO)event.getComponent().getAttributes().get("varCashAcc");
			blView = true;
			blActive = false;
			blLock = false;
			blModify = false;
			blRegister = false;
			fillView(cashAccTO);
		}catch (Exception e) {
			e.printStackTrace();
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Search issuer handler.
	 */
	public void searchIssuerHandler(){
		executeAction();
		if(Validations.validateIsNotNullAndNotEmpty(issuer) 
				&& Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageEffectiveAccountsFacade.findIssuerFacade(issuer.getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){		
				issuer=null;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );		
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			issuer=issuerTemp;
		}
	}
	
	/**
	 * Load group modality.
	 */
	public void loadGroupModality(){
		JSFUtilities.hideGeneralDialogues();
		try {
			idModaGroupPkSelected = null;
			if(Validations.validateIsNotNullAndNotEmpty(idNegoMechanismPkSelected)){
				lstModalityGroup = manageEffectiveAccountsFacade.getLstModaGroup(idNegoMechanismPkSelected);
			}else{
				lstModalityGroup = null;			
			}	
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Fill institution bank accounts type.
	 */
	public void fillInstitutionBankAccountsType(){
		lstInstitutionBankAccountsType = new ArrayList<ParameterTable>();
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			lstInstitutionBankAccountsType.addAll(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
	}
	
	/**
	 * Fill institution bank accounts type.
	 */
	public void fillInstitutionBankAccountsTypeReturn(){
		lstInstitutionBankAccountsTypeReturn = new ArrayList<ParameterTable>();
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setIndicator4(GeneralConstants.ONE_VALUE_INTEGER);
		try {
			lstInstitutionBankAccountsTypeReturn.addAll(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
	}
	
	/**
	 * Show register screen.
	 *
	 * @throws Exception the exception
	 */
	public void showRegisterScreen() throws Exception {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetViewRoot();
		setOffIndicators();
		fillCurrency();
		if (AccountCashFundsType.CENTRALIZING.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setCentralizingAccount(new CentralizingAccount());			
			registerCashAccount.getCentralizingAccount().setLstBankAccountType(fillBankAccountType());
			registerCashAccount.getCentralizingAccount().setLstCurrency(lstCurrency);
			registerCashAccount.getCentralizingAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
			List<String> lstValues = manageEffectiveAccountsFacade.getBCBBankBicCode();
			registerCashAccount.getCentralizingAccount().setBicCode(lstValues.get(0));
			registerCashAccount.getCentralizingAccount().setBank(lstValues.get(1));
			registerCashAccount.getCentralizingAccount().setBankAccountPK(Long.valueOf(lstValues.get(2)));
			registerCashAccount.setIndCentralizingAccount(BooleanType.YES.getCode());
			
			setBankAccountToRegister();
			
		} else if (AccountCashFundsType.SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setSettlerAccount(new SettlerAccount());
			fillParticipant(false);
			if (registerCashAccount.getSettlerAccount().getUseTypeOptions() == null 
					|| registerCashAccount.getSettlerAccount().getUseTypeOptions().isEmpty())
				registerCashAccount.getSettlerAccount().setUseTypeOptions(generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_CASH_ACCOUNT_USE_TYPE.getCode()));
			if (registerCashAccount.getSettlerAccount().getLstBankAccountType() == null || registerCashAccount.getSettlerAccount().getLstBankAccountType().size() == 0) {
				registerCashAccount.getSettlerAccount().setLstBankAccountClass(
				generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_CLASS.getCode()));
			}
			if (registerCashAccount.getSettlerAccount().getLstBankAccountType() == null || registerCashAccount.getSettlerAccount().getLstBankAccountType().size() == 0) {
				registerCashAccount.getSettlerAccount().setLstBankAccountType(
						generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode()));
			}
			registerCashAccount.getSettlerAccount().setLstParticipant(lstParticipants);
			registerCashAccount.getSettlerAccount().setUseType(null);
			registerCashAccount.getSettlerAccount().setLstCurrency(lstCurrency);		
			registerCashAccount.setIndSettlerAccount(BooleanType.YES.getCode());
			registerCashAccount.getSettlerAccount().setLstTypeAccount(fillBankType());
			registerCashAccount.getSettlerAccount().setInstitutionBankAccount(new InstitutionBankAccount());
		} else if (AccountCashFundsType.BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setBenefitPaymentAccount(new BenefitPaymentAccount());
			fillIssuer();
			registerCashAccount.getBenefitPaymentAccount().setLstTypeAccount(fillBankType());
			registerCashAccount.getBenefitPaymentAccount().setLstCurrency(lstCurrency);
			registerCashAccount.getBenefitPaymentAccount().setLstIssuer(lstIssuers);
			registerCashAccount.setIndBenefitPaymentAccount(BooleanType.YES.getCode());
			registerCashAccount.getBenefitPaymentAccount().setIssuer(new Issuer());
			registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(new InstitutionBankAccount());
		} else if (AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setMarginGuaranteeAccount(new MarginGuaranteeAccount());
			fillParticipant(false);
			registerCashAccount.getMarginGuaranteeAccount().setLstBankAccountType(fillBankAccountType());
			registerCashAccount.getMarginGuaranteeAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
			registerCashAccount.getMarginGuaranteeAccount().setLstBankType(fillBankType());
			registerCashAccount.getMarginGuaranteeAccount().setBankType(BankType.COMMERCIAL.getCode());
			registerCashAccount.getMarginGuaranteeAccount().setLstBank(fillCommercialBank());
			registerCashAccount.getMarginGuaranteeAccount().setLstParticipant(lstParticipants);
			registerCashAccount.setIndMaginGuaranteeAccount(BooleanType.YES.getCode());
		} else if (AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setRatesAccount(new RatesAccount());
			registerCashAccount.getRatesAccount().setLstAtomatic(fillAutomaticIndicators());
			registerCashAccount.getRatesAccount().setIndAutomatic(BooleanType.NO.getCode());	
			registerCashAccount.getRatesAccount().setLstBank(fillCommercialBank());			
			registerCashAccount.getRatesAccount().setLstBankAccountType(fillBankAccountType());			
			registerCashAccount.getRatesAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
			registerCashAccount.getRatesAccount().setLstBankType(fillBankType());
			registerCashAccount.getRatesAccount().setBankType(BankType.COMMERCIAL.getCode());
//			List<String> lstValues = manageEffectiveAccountsFacade.getBCBBankBicCode();
//			registerCashAccount.getRatesAccount().setBankAccountPK(Long.valueOf(lstValues.get(2)));
			registerCashAccount.getRatesAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
			registerCashAccount.setIndRatesAccount(BooleanType.YES.getCode());
		} else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setInternationalOperationsAccount(new InternationalOperationsAccount());
			fillInternationalParticipant();
			registerCashAccount.getInternationalOperationsAccount().setLstBank(fillCommercialBank());			
			registerCashAccount.getInternationalOperationsAccount().setLstBankAccountType(fillBankAccountType());			
			registerCashAccount.getInternationalOperationsAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
			registerCashAccount.getInternationalOperationsAccount().setLstBankType(fillBankType());			
			registerCashAccount.getInternationalOperationsAccount().setBankType(BankType.COMMERCIAL.getCode());
			registerCashAccount.getInternationalOperationsAccount().setLstParticipants(lstParticipants);
			registerCashAccount.setIndInternationalOperationsAccount(BooleanType.YES.getCode());
		} else if (AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setReturnAccount(new ReturnAccount());
			fillParticipant(false);
			fillIssuer();
			registerCashAccount.getReturnAccount().setLstParticipant(lstParticipants);
			registerCashAccount.getReturnAccount().setLstCurrency(lstCurrency);
			registerCashAccount.getReturnAccount().setLstIssuer(lstIssuers);
			registerCashAccount.getReturnAccount().setLstAutomatic(fillAutomaticIndicators());
			registerCashAccount.getReturnAccount().setIndAutomatic(BooleanType.NO.getCode());			
			registerCashAccount.getReturnAccount().setLstBankAccountType(fillBankAccountType());
			registerCashAccount.getReturnAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
			registerCashAccount.getReturnAccount().setLstBankType(fillBankType());			
			registerCashAccount.getReturnAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
			registerCashAccount.setIndReturnAccount(BooleanType.YES.getCode());
			registerCashAccount.getReturnAccount().setIssuer(new Issuer());
			fillInstitutionBankAccountsTypeReturn();
			objInstitutionBankAccountsType=null;
		} else if (AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			registerCashAccount.setTaxesAccount(new TaxesAccount());
			registerCashAccount.getTaxesAccount().setLstAtomatic(fillAutomaticIndicators());
			registerCashAccount.getTaxesAccount().setIndAutomatic(BooleanType.NO.getCode());
			registerCashAccount.getTaxesAccount().setLstBank(fillCommercialBank());
			registerCashAccount.getTaxesAccount().setLstBankAccountType(fillBankAccountType());			
			registerCashAccount.getTaxesAccount().setBankAccountType(BankAccountType.CURRENT.getCode());
			registerCashAccount.getTaxesAccount().setLstBankType(fillBankType());			
			registerCashAccount.getTaxesAccount().setBankType(BankType.COMMERCIAL.getCode());
			registerCashAccount.getTaxesAccount().setInstitution(InstitutionType.DEPOSITARY.getValue());
			registerCashAccount.setIndTaxesAccount(BooleanType.YES.getCode());
		} else{
			registerCashAccount = new RegisterCashAccount();
			registerCashAccount.setLstCashAccountType(lstCashAccountType);
		}
	}
	
	/**
	 * Fill international participant.
	 */
	private void fillInternationalParticipant(){
		try {
			lstParticipants= manageEffectiveAccountsFacade.getParticipantsIntDepositaryForDeposit();
		} catch (ServiceException e) {
			e.printStackTrace();
		}			
	}
	
	/**
	 * Fill automatic indicators.
	 *
	 * @return the list
	 */
	private List<ParameterTable> fillAutomaticIndicators(){
		List<ParameterTable> lstAutomatic = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(BooleanType.YES.getCode());
		paramTab.setParameterName(BooleanType.YES.getValue());
		lstAutomatic.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(BooleanType.NO.getCode());
		paramTab.setParameterName(BooleanType.NO.getValue());
		lstAutomatic.add(paramTab);
		return lstAutomatic;
		
	}
	
	/**
	 * Show bank.
	 */
	public void showBank() {
		try {
			Integer accountType = registerCashAccount.getBenefitPaymentAccount().getTypeAccountSelected();
			registerCashAccount.getBenefitPaymentAccount().setBankSelected(null);
			registerCashAccount.getBenefitPaymentAccount().setBicCode(null);
			registerCashAccount.getBenefitPaymentAccount().setBankAccount(null);
			if (BankType.COMMERCIAL.getCode().equals(accountType)) {
				registerCashAccount.getBenefitPaymentAccount().setLstBank(fillCommercialBank());				
			}else{
				registerCashAccount.getBenefitPaymentAccount().setLstBank(manageEffectiveAccountsFacade.fillBankByType(accountType));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Show bank.
	 */
	public void showBankSettler() {
		try {
			Integer accountType = registerCashAccount.getSettlerAccount().getTypeAccountSelected();
			registerCashAccount.getSettlerAccount().setBankSelected(null);
			registerCashAccount.getSettlerAccount().setBicCode(null);
			registerCashAccount.getSettlerAccount().setBankAccount(null);
			if (BankType.COMMERCIAL.getCode().equals(accountType)) {
				registerCashAccount.getSettlerAccount().setLstBank(fillCommercialBank());				
			}else{
				registerCashAccount.getSettlerAccount().setLstBank(manageEffectiveAccountsFacade.fillBankByType(accountType));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Fill bank type.
	 *
	 * @return the list
	 */
	private List<ParameterTable> fillBankType(){
		try {
			return generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_BANK_TYPE.getCode());
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public void getListAccountBank() {
		JSFUtilities.hideGeneralDialogues();
		Integer currency = registerCashAccount.getBenefitPaymentAccount().getCurrencySelected();
		
		registerCashAccount.getBenefitPaymentAccount().setBicCode(null);
		registerCashAccount.getBenefitPaymentAccount().setBankAccount(null);
		registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(new InstitutionBankAccount());
		if(Validations.validateIsNotNullAndNotEmpty(currency)){
			if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getBenefitPaymentAccount().getBankSelected())){
				lstInstitutionBankAccount = manageEffectiveAccountsFacade.getIssuerBankAccount(registerCashAccount.getBenefitPaymentAccount().getIssuer().getIdIssuerPk(),
						currency, registerCashAccount.getBenefitPaymentAccount().getBankSelected().longValue());
				
//				
//				if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
//					registerCashAccount.getBenefitPaymentAccount().setBicCode(institutionBankAccount.getProviderBank().getBicCode());
//					registerCashAccount.getBenefitPaymentAccount().setBankAccount(institutionBankAccount.getAccountNumber());
//					registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(institutionBankAccount);
//				}else{
//					registerCashAccount.getBenefitPaymentAccount().setBicCode(null);
//					registerCashAccount.getBenefitPaymentAccount().setBankAccount(null);
//					registerCashAccount.getBenefitPaymentAccount().setBankSelected(null);
//					registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(null);
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_NOTFOUND));
//					JSFUtilities.showValidationDialog();
//				}
			}else{
				registerCashAccount.getBenefitPaymentAccount().setBicCode(null);
				registerCashAccount.getBenefitPaymentAccount().setBankAccount(null);
				registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(null);
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_CURRENCY_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}		
	}
	
	public void getListAccountBankSettler() throws ServiceException {
		JSFUtilities.hideGeneralDialogues();
		Integer currency = registerCashAccount.getSettlerAccount().getCurrencySelected();
		
		registerCashAccount.getSettlerAccount().setBicCode(null);
		registerCashAccount.getSettlerAccount().setBankAccount(null);
		registerCashAccount.getSettlerAccount().setInstitutionBankAccount(new InstitutionBankAccount());
		if(Validations.validateIsNotNullAndNotEmpty(currency)){
			if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getSettlerAccount().getBankSelected())){
				lstInstitutionBankAccount = manageEffectiveAccountsFacade.getParticipantBankAccounts(registerCashAccount.getSettlerAccount().getParticipantSelected().longValue(),
						currency, registerCashAccount.getSettlerAccount().getBankSelected().longValue());
			}else{
				registerCashAccount.getSettlerAccount().setBicCode(null);
				registerCashAccount.getSettlerAccount().setBankAccount(null);
				registerCashAccount.getSettlerAccount().setInstitutionBankAccount(null);
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_CURRENCY_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		}		
	}
	
	/**
	 * Sets the bic code.
	 */
	public void setBicCode() {
		JSFUtilities.hideGeneralDialogues();
			if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getBenefitPaymentAccount().getInstitutionBankAccount().getIdInstitutionBankAccountPk())){
				InstitutionBankAccount institutionBankAccount = manageEffectiveAccountsFacade.findBankAccount(
						registerCashAccount.getBenefitPaymentAccount().getInstitutionBankAccount().getIdInstitutionBankAccountPk());
				if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
					registerCashAccount.getBenefitPaymentAccount().setBicCode(institutionBankAccount.getProviderBank().getBicCode());
					registerCashAccount.getBenefitPaymentAccount().setBankAccount(institutionBankAccount.getAccountNumber());
					registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(institutionBankAccount);
				}else{
					registerCashAccount.getBenefitPaymentAccount().setBicCode(null);
					registerCashAccount.getBenefitPaymentAccount().setBankAccount(null);
					registerCashAccount.getBenefitPaymentAccount().setBankSelected(null);
					registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_NOTFOUND));
					JSFUtilities.showValidationDialog();
				}
			}else{
				registerCashAccount.getBenefitPaymentAccount().setBicCode(null);
				registerCashAccount.getBenefitPaymentAccount().setBankAccount(null);
				registerCashAccount.getBenefitPaymentAccount().setInstitutionBankAccount(null);
			}
	}
	
	/**
	 * Sets the bic code.
	 */
	public void setBicCodeSettler() {
		//JSFUtilities.hideGeneralDialogues();
			if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getSettlerAccount().getInstitutionBankAccount().getIdInstitutionBankAccountPk())){
				InstitutionBankAccount institutionBankAccount = manageEffectiveAccountsFacade.findBankAccount(
						registerCashAccount.getSettlerAccount().getInstitutionBankAccount().getIdInstitutionBankAccountPk());
				if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
					registerCashAccount.getSettlerAccount().setBicCode(institutionBankAccount.getProviderBank().getBicCode());
					registerCashAccount.getSettlerAccount().setBankAccountNumber(institutionBankAccount.getAccountNumber());
					registerCashAccount.getSettlerAccount().setInstitutionBankAccount(institutionBankAccount);
				}else{
					registerCashAccount.getSettlerAccount().setBicCode(null);
					registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
					registerCashAccount.getSettlerAccount().setBankSelected(null);
					registerCashAccount.getSettlerAccount().setInstitutionBankAccount(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_NOTFOUND));
					JSFUtilities.showValidationDialog();
				}
			}else{
				registerCashAccount.getSettlerAccount().setBicCode(null);
				registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
				registerCashAccount.getSettlerAccount().setInstitutionBankAccount(null);
			}
	}
	
	/**
	 * Sets the off indicators.
	 */
	private void setOffIndicators(){
		registerCashAccount.setIndCentralizingAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndBenefitPaymentAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndInternationalOperationsAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndMaginGuaranteeAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndRatesAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndReturnAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndSettlerAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndTaxesAccount(BooleanType.NO.getCode());
		registerCashAccount.setIndSpecialPaymentAccount(BooleanType.NO.getCode());
	}
	
	/**
	 * Fill currency.
	 */
	private void fillCurrency(){
		try{
			if (lstCurrency == null || lstCurrency.size() == 0) {
				ParameterTableTO objParameterTableTO=new ParameterTableTO(); 
				objParameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				objParameterTableTO.setIndicator2(GeneralConstants.ONE_VALUE_STRING);
				lstCurrency = generalParametersFacade.getListParameterTableServiceBean(objParameterTableTO);	
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Fill bank account type.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<ParameterTable> fillBankAccountType() throws ServiceException {
		return generalParametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());
	}
	
	/**
	 * Validate save cash account.
	 */
	public void validateSaveCashAccount() {
		try
		{
				JSFUtilities.hideGeneralDialogues();
				if(validateSelectedBankAccount())
				{
					Object[] parameters = new Object[2];
					parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
					parameters[1]=institutionDescription();
					if (AccountCashFundsType.SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
						String strError = validateSettlement();
						if(Validations.validateIsNullOrEmpty(strError)){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
										, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ASK_REGISTER,parameters));
							JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(strError));
							JSFUtilities.showValidationDialog();
						}
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
								, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ASK_REGISTER,parameters));
						JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
					}	
				}
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * Validate selected bank account.
	 *
	 * @return true, if successful
	 */
	public boolean validateSelectedBankAccount()
	{
		if (AccountCashFundsType.CENTRALIZING.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
		
			if(Validations.validateIsNull(registerCashAccount.getCentralizingAccount().getInstitutionBankAccountSession()))
			{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showValidationDialog();
				return false;
			}
		}
		else if (AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
		
			if(Validations.validateIsNull(registerCashAccount.getMarginGuaranteeAccount().getInstitutionBankAccountSession()))
			{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showValidationDialog();
				return false;
			}
		}
		else if (AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
			
			if(Validations.validateIsNull(registerCashAccount.getRatesAccount().getInstitutionBankAccountSession()))
			{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showValidationDialog();
				return false;
			}
		}
//		else if (AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
//			
//			if(Validations.validateIsNull(registerCashAccount.getReturnAccount().getInstitutionBankAccountSession()))
//			{
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
//						PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED));
//				JSFUtilities.showValidationDialog();
//				return false;
//			}
//		}
		else if (AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
			
			if(Validations.validateIsNull(registerCashAccount.getTaxesAccount().getInstitutionBankAccountSession()))
			{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showValidationDialog();
				return false;
			}
		}
		else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
			
			if(Validations.validateIsNull(registerCashAccount.getInternationalOperationsAccount().getInstitutionBankAccountSession()))
			{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showValidationDialog();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validate settlement.
	 *
	 * @return the string
	 */
	private String validateSettlement(){
		boolean blBolsa = false, blBCB = false, blOTC = false, blHacienda = false;
		if(Validations.validateIsNotNull(registerCashAccount.getSettlerAccount().getModaGroupBolsa())
				&& registerCashAccount.getSettlerAccount().getModaGroupBolsa().length > 0)
			blBolsa = true;
		if(Validations.validateIsNotNull(registerCashAccount.getSettlerAccount().getModaGroupBCB())
				&& registerCashAccount.getSettlerAccount().getModaGroupBCB().length > 0)
			blBCB = true;
		if(Validations.validateIsNotNull(registerCashAccount.getSettlerAccount().getModaGroupOTC())
				&& registerCashAccount.getSettlerAccount().getModaGroupOTC().length > 0)
			blOTC = true;
		if(Validations.validateIsNotNull(registerCashAccount.getSettlerAccount().getModaGroupHacienda())
				&& registerCashAccount.getSettlerAccount().getModaGroupHacienda().length > 0)
			blHacienda = true;
		
		
		if(!blBolsa && !blBCB && !blOTC && !blHacienda)
			return PropertiesConstants.CASH_ACCOUNT_MODALITYGROUP_NOTSELECTED;
		else if(Validations.validateIsNullOrEmpty(registerCashAccount.getSettlerAccount().getLstBankAccounts()))
			return PropertiesConstants.CASH_ACCOUNT_BANK_NOTADDED;
		else if(blBolsa || blBCB || blOTC || blHacienda)
			return null;
		else
			return null;
	}
	
	/**
	 * Ask for clean.
	 */
	public void askForClean(){
		JSFUtilities.resetViewRoot();
		if(validateRequiredFields()){
			cleanRegisterScreen();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.clean"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwCleanDialog').show()");
		}
	}
	
	/**
	 * Validate required fields.
	 *
	 * @return true, if successful
	 */
	private boolean validateRequiredFields(){
		if(Validations.validateIsNullOrEmpty(registerCashAccount.getCashAccountTypeSelected()))
			return true;
		return false;
	}
	
	/**
	 * Clean register screen.
	 */
	public void cleanRegisterScreen() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetViewRoot();
		registerCashAccount = new RegisterCashAccount();
		registerCashAccount.setCashAccountTypeSelected(null);
		registerCashAccount.setLstCashAccountType(lstCashAccountType);
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		try{
			JSFUtilities.hideGeneralDialogues();
			Object[] parameters = new Object[2];
			
			if(blActive){
				if(validateActivation())
				{	parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ACTIVE), 
							PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ALREADY_EXIST,parameters));
					JSFUtilities.showSimpleValidationDialog();
				}
				else
				{
					parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
					parameters[1]=institutionDescription();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ACTIVE)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ASK_ACTIVE,parameters));
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
				}
			}else if(blLock){
				if(!AccountCashFundsType.BENEFIT.getCode().equals(cashAccount.getCashAccountType())){
					InstitutionCashAccount instCash=manageEffectiveAccountsFacade.getInstitutionCashAccountData(cashAccount.getIdCashAccount());
					//we verify if not exists any settlement process at execution
					if(Validations.validateIsNotNullAndNotEmpty(instCash.getModalityGroup())) {
						List<SettlementProcess> lstSettlementProcess= settlementProcessServiceBean.getListSettlementProcess(
																									instCash.getModalityGroup().getIdModalityGroupPk(), 
																									CommonsUtilities.currentDate(), 
																									SettlementSchemaType.NET.getCode(), 
																									instCash.getCurrency());
						if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementProcess)) {
							for (SettlementProcess objSettlementProcess: lstSettlementProcess) {
								if (!SettlementProcessStateType.FINISHED.getCode().equals(objSettlementProcess.getProcessState())) {
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ACTIVE), 
																PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENT_IN_PROCESS));
									JSFUtilities.showSimpleValidationDialog();
									return;
								}
							}
						}
					}
				}
				
				parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
				parameters[1]=institutionDescription();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DEACTIVE)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ASK_DEACTIVE,parameters));
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
			}else if(blModify){
				if (AccountCashFundsType.SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())){
					if(Validations.validateIsNullOrEmpty(registerCashAccount.getSettlerAccount().getLstBankAccounts())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_NOTADDED));
						JSFUtilities.showSimpleValidationDialog();					
					}else{
						parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
						parameters[1]=institutionDescription();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
								, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ASK_MODIFY,parameters));
						JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
					}
				}else{
					parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
					parameters[1]=institutionDescription();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ASK_MODIFY,parameters));
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
				}	
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	

	
	/**
	 * Ask for back.
	 *
	 * @return the string
	 */
	public String askForBack(){
		try {
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_FUND_ACCOUNT_TYPE.getCode());
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
			lstCashAccountType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(blRegister){
			if(validateRequiredFields()){
				return "backMaintainCashAccount";
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getGenericMessage("conf.msg.back"));
				JSFUtilities.executeJavascriptFunction("PF('cnfwBackDialog').show()");
			}
		}else if(blModify)
		{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.back"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwBackDialog').show()");
		}
		else
			return "backMaintainCashAccount";
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		JSFUtilities.hideGeneralDialogues();
		try {
			if(blRegister)
				saveCashAccount();
			else if(blActive){
				manageEffectiveAccountsFacade.activeCashAccount(cashAccount.getIdCashAccount());
				Object[] parameters = new Object[2];
				parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
				parameters[1]=institutionDescription();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_ACTIVE,parameters));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				// Start Notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.EFFECTIVE_ACCOUNT_ACTIVATE.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,registerCashAccount.getIdInstituteCash(), parameters);
				// End Notification
				
				searchCashAccount();
			}else if(blLock){
				manageEffectiveAccountsFacade.disactivateCashAccount(cashAccount.getIdCashAccount());
				Object[] parameters = new Object[2];
				parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
				parameters[1]=institutionDescription();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_DEACTIVE,parameters));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				// Start Notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.EFFECTIVE_ACCOUNT_DISACTIVATE.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,registerCashAccount.getIdInstituteCash(), parameters);
				// End Notification
				searchCashAccount();
			}else if(blModify){
				Object[] parameters = new Object[2];
				parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
				parameters[1]=institutionDescription();
				manageEffectiveAccountsFacade.modifyCashAccount(cashAccount.getIdCashAccount(), registerCashAccount);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_MODIFIED,parameters));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				searchCashAccount();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}		
	}
	
	/**
	 * Save cash account.
	 *
	 * @throws ServiceException the service exception
	 */
	private void saveCashAccount() throws ServiceException {
		try {
			manageEffectiveAccountsFacade.saveCashAccount(registerCashAccount);
			Object[] parameters = new Object[2];
			parameters[0] = generalParametersFacade.getParameterTableById(registerCashAccount.getCashAccountTypeSelected()).getParameterName();
			parameters[1]=institutionDescription();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_REGISTER, parameters));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			// Start Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.EFFECTIVE_ACCOUNT_REGISTRATION.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,registerCashAccount.getIdInstituteCash(), parameters);
			// End Notification
			searchCashAccount();
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}
	
	/**
	 * Institution description.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String institutionDescription() throws ServiceException
	{
		String institutionDes=null;
		if(AccountCashFundsType.SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
		{
			Participant objParticipant=new Participant();
			if(AccountCashFundsType.SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
					objParticipant.setIdParticipantPk(registerCashAccount.getSettlerAccount().getParticipantSelected().longValue());
			else if(AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
				objParticipant.setIdParticipantPk(registerCashAccount.getMarginGuaranteeAccount().getParticipantSelected().longValue());
			else if(AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
				objParticipant.setIdParticipantPk(registerCashAccount.getInternationalOperationsAccount().getParticipantSelected().longValue());
			institutionDes=participantServiceBean.findParticipantByFiltersServiceBean(objParticipant).getDescription();
		}else if(AccountCashFundsType.BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
		{
			institutionDes=issuerQueryServiceBean.findIssuer(registerCashAccount.getBenefitPaymentAccount().getIssuer().getIdIssuerPk(),null).getBusinessName();
		}
		else if(AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				|| AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
		{
			if(AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
				institutionDes=registerCashAccount.getRatesAccount().getInstitution();
			else if(AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
				if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getReturnAccount().getParticipantSelected())) {
					Participant objParticipant=new Participant();
					objParticipant.setIdParticipantPk(registerCashAccount.getReturnAccount().getParticipantSelected().longValue());
					institutionDes=participantServiceBean.findParticipantByFiltersServiceBean(objParticipant).getDescription();
				}
				if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getReturnAccount().getIssuer())) {
					if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getReturnAccount().getIssuer().getIdIssuerPk())) {
						institutionDes=issuerQueryServiceBean.findIssuer(registerCashAccount.getReturnAccount().getIssuer().getIdIssuerPk(),null).getBusinessName();
					}
				}
			}
			else if(AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
				institutionDes=registerCashAccount.getTaxesAccount().getInstitution();
		}
		else
		{
			institutionDes=registerCashAccount.getCentralizingAccount().getInstitution();
		}
		return institutionDes;
	}
	
	/**
	 * Gets the bank account.
	 *
	 * @return the bank account
	 */
	public void getBankAccount()
	{
		try{				
			if (AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
			{
				if(Validations.validateIsNotNullAndPositive(registerCashAccount.getMarginGuaranteeAccount().getParticipantSelected())
						&& Validations.validateIsNotNullAndPositive(registerCashAccount.getMarginGuaranteeAccount().getBankSelected()))
				{
					SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
					objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
					objSearchBankAccountsTO.setIdParticipantPk(registerCashAccount.getMarginGuaranteeAccount().getParticipantSelected().longValue());
					objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getMarginGuaranteeAccount().getBankSelected().longValue());
					objSearchBankAccountsTO.setBankType(registerCashAccount.getMarginGuaranteeAccount().getBankType());
					objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
					List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
					registerCashAccount.getMarginGuaranteeAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsTmp));
				}
				else
					registerCashAccount.getMarginGuaranteeAccount().setLstBankAccountsTODataModel(null);
			}
			else if (AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
			{
				if(Validations.validateIsNotNullAndPositive(registerCashAccount.getRatesAccount().getBankSelected()))
				{
					SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
					objSearchBankAccountsTO.setInstitutionType(this.objInstitutionBankAccountsType);
					if(objInstitutionBankAccountsType.equals(InstitutionBankAccountsType.BANK.getCode())){
						objSearchBankAccountsTO.setIdBankInstitutionPk(registerCashAccount.getRatesAccount().getBankAccountPK());
					}if(objInstitutionBankAccountsType.equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
						objSearchBankAccountsTO.setIdParticipantPk(GeneralConstants.PARTICIPANT_EDB_CODE);
					}	
					objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getRatesAccount().getBankSelected().longValue());
					objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
					objSearchBankAccountsTO.setBankAccountType(registerCashAccount.getRatesAccount().getBankAccountType());
					List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
					registerCashAccount.getRatesAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsTmp));
				}
				else
					registerCashAccount.getRatesAccount().setLstBankAccountsTODataModel(null);
			}
			else if (AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
			{
				if(Validations.validateIsNotNullAndPositive(registerCashAccount.getReturnAccount().getBankSelected()))
				{
					SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
					objSearchBankAccountsTO.setInstitutionType(this.objInstitutionBankAccountsType);
					if(objInstitutionBankAccountsType.equals(InstitutionBankAccountsType.BANK.getCode())){
						objSearchBankAccountsTO.setIdBankInstitutionPk(registerCashAccount.getReturnAccount().getBankAccountPK());
					}if(objInstitutionBankAccountsType.equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
						objSearchBankAccountsTO.setIdParticipantPk(GeneralConstants.PARTICIPANT_EDB_CODE);
					}					
					objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getReturnAccount().getBankSelected().longValue());
					objSearchBankAccountsTO.setBankAccountType(registerCashAccount.getReturnAccount().getBankAccountType());
					objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
					List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
					registerCashAccount.getReturnAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsTmp));
				}
				else
					registerCashAccount.getReturnAccount().setLstBankAccountsTODataModel(null);
			}
			else if (AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
			{
				if(Validations.validateIsNotNullAndPositive(registerCashAccount.getTaxesAccount().getBankSelected()))
				{
					SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
					objSearchBankAccountsTO.setInstitutionType(this.objInstitutionBankAccountsType);
					if(objInstitutionBankAccountsType.equals(InstitutionBankAccountsType.BANK.getCode())){
						objSearchBankAccountsTO.setIdBankInstitutionPk(registerCashAccount.getTaxesAccount().getBankAccountPK());
					}if(objInstitutionBankAccountsType.equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
						objSearchBankAccountsTO.setIdParticipantPk(GeneralConstants.PARTICIPANT_EDB_CODE);
					}					
					objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getTaxesAccount().getBankSelected().longValue());
					objSearchBankAccountsTO.setBankAccountType(registerCashAccount.getTaxesAccount().getBankAccountType());
					objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
					List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
					registerCashAccount.getTaxesAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsTmp));
				}
				else
					registerCashAccount.getTaxesAccount().setLstBankAccountsTODataModel(null);
			}
			else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
			{
				if(Validations.validateIsNotNullAndPositive(registerCashAccount.getInternationalOperationsAccount().getParticipantSelected())
						&& Validations.validateIsNotNullAndPositive(registerCashAccount.getInternationalOperationsAccount().getBankSelected()))
				{
					SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
					objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
					objSearchBankAccountsTO.setIdParticipantPk(registerCashAccount.getInternationalOperationsAccount().getParticipantSelected().longValue());
					objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getInternationalOperationsAccount().getBankSelected().longValue());
					objSearchBankAccountsTO.setBankType(registerCashAccount.getInternationalOperationsAccount().getBankType());
					objSearchBankAccountsTO.setBankAccountType(registerCashAccount.getInternationalOperationsAccount().getBankAccountType());
					objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
					List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
					registerCashAccount.getInternationalOperationsAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsTmp));
				}
				else
					registerCashAccount.getInternationalOperationsAccount().setLstBankAccountsTODataModel(null);
			}
		}
		catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load moda group.
	 */
	public void loadModaGroup() {
		try{
			JSFUtilities.resetComponent("frmCashAccountRegister:idBicCode");
			JSFUtilities.resetComponent("frmCashAccountRegister:idBankAccountNumber");
			JSFUtilities.resetComponent("frmCashAccountRegister:idBnkAccType");
			JSFUtilities.resetComponent("frmCashAccountRegister:idThirdBank");
			registerCashAccount.getSettlerAccount().setIndOtherBank(BooleanType.NO.getCode());
			registerCashAccount.getSettlerAccount().setBankAccountClassSelected(null);
			registerCashAccount.getSettlerAccount().setLstBankAccounts(null);
			registerCashAccount.getSettlerAccount().setOtherBankSelected(null);
			registerCashAccount.getSettlerAccount().setBicCodeOtherBank(null);		
			registerCashAccount.getSettlerAccount().setBankAccountTypeOwner(null);
			registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
			registerCashAccount.getSettlerAccount().setBankAccountType(null);
			registerCashAccount.getSettlerAccount().setBankAccountTypeThird(null);
			registerCashAccount.getSettlerAccount().setModaGroupBolsa(null);
			registerCashAccount.getSettlerAccount().setModaGroupBCB(null);
			registerCashAccount.getSettlerAccount().setModaGroupHacienda(null);
			registerCashAccount.getSettlerAccount().setModaGroupOTC(null);
			registerCashAccount.getSettlerAccount().setLstBolsa(null);
			registerCashAccount.getSettlerAccount().setLstBCB(null);
			registerCashAccount.getSettlerAccount().setLstHacienda(null);
			registerCashAccount.getSettlerAccount().setLstOTC(null);
			registerCashAccount.getSettlerAccount().setBicCode(null);
			Integer partSelected = registerCashAccount.getSettlerAccount().getParticipantSelected();
			if(Validations.validateIsNotNullAndNotEmpty(partSelected)){
				List<ModalityGroup> lstTempModaGroup = manageEffectiveAccountsFacade.getLstModaGroup(NegotiationMechanismType.BOLSA.getCode(), new Long(partSelected));
				if(Validations.validateListIsNotNullAndNotEmpty(lstTempModaGroup))
					registerCashAccount.getSettlerAccount().setLstBolsa(new ModalityGroupDataModel(lstTempModaGroup));	
				
				lstTempModaGroup = manageEffectiveAccountsFacade.getLstModaGroup(NegotiationMechanismType.BC.getCode(), new Long(partSelected));
				if(Validations.validateListIsNotNullAndNotEmpty(lstTempModaGroup))
					registerCashAccount.getSettlerAccount().setLstBCB(new ModalityGroupDataModel(lstTempModaGroup));
				
				lstTempModaGroup = manageEffectiveAccountsFacade.getLstModaGroup(NegotiationMechanismType.OTC.getCode(), new Long(partSelected));
				if(Validations.validateListIsNotNullAndNotEmpty(lstTempModaGroup))
					registerCashAccount.getSettlerAccount().setLstOTC(new ModalityGroupDataModel(lstTempModaGroup));		
				
				lstTempModaGroup = manageEffectiveAccountsFacade.getLstModaGroup(NegotiationMechanismType.HAC.getCode(), new Long(partSelected));
				if(Validations.validateListIsNotNullAndNotEmpty(lstTempModaGroup))
					registerCashAccount.getSettlerAccount().setLstHacienda(new ModalityGroupDataModel(lstTempModaGroup));
				
				String participantBic = manageEffectiveAccountsFacade.getParticipantBicCode(partSelected);
				if(Validations.validateIsNotNull(participantBic))
					registerCashAccount.getSettlerAccount().setBicCode(participantBic);	
				
				//setBankAccountToRegister();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets the bank account to register.
	 */
	public void setBankAccountToRegister()
	{ try
		{
			if (AccountCashFundsType.CENTRALIZING.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
					||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected()))
			{
				SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
				objSearchBankAccountsTO.setIdBankInstitutionPk(registerCashAccount.getCentralizingAccount().getBankAccountPK());
				objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getCentralizingAccount().getBankAccountPK());
				objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
				List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
				registerCashAccount.getCentralizingAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsTmp));
			}
		}catch (ServiceException se)
		{
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Sets the currency.
	 */
	public void setCurrency() {
		if(blRegister){
			JSFUtilities.resetComponent("frmCashAccountRegister:idBankAccountNumber");
			JSFUtilities.resetComponent("frmCashAccountRegister:idBnkAccType");
			JSFUtilities.resetComponent("frmCashAccountRegister:idThirdBank");
			registerCashAccount.getSettlerAccount().setLstBankAccounts(null);
			registerCashAccount.getSettlerAccount().setOtherBankSelected(null);
			registerCashAccount.getSettlerAccount().setBicCodeOtherBank(null);		
			registerCashAccount.getSettlerAccount().setBankAccountTypeOwner(null);
			registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
			registerCashAccount.getSettlerAccount().setBankAccountType(null);
			registerCashAccount.getSettlerAccount().setBankAccountTypeThird(null);
		}
		Integer currencySelected = registerCashAccount.getSettlerAccount().getCurrencySelected();		
		if(CurrencyType.PYG.getCode().equals(currencySelected))
			registerCashAccount.getSettlerAccount().setBankAccountCurrecy(CurrencyType.PYG.getValue());
		else if (CurrencyType.USD.getCode().equals(currencySelected))
			registerCashAccount.getSettlerAccount().setBankAccountCurrecy(CurrencyType.USD.getValue());
		else
			registerCashAccount.getSettlerAccount().setBankAccountCurrecy(null);
		//setBankAccountToRegister();
	}
	
	/**
	 * Show other bank data.
	 *
	 * @throws Exception the exception
	 */
	public void showOtherBankData() throws Exception {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.resetComponent("frmCashAccountRegister:idBankAccountNumber");
		JSFUtilities.resetComponent("frmCashAccountRegister:idBnkAccType");
		JSFUtilities.resetComponent("frmCashAccountRegister:idThirdBank");
		registerCashAccount.getSettlerAccount().setOtherBankSelected(null);
		registerCashAccount.getSettlerAccount().setBankAccountTypeOwner(null);
		registerCashAccount.getSettlerAccount().setBankAccountType(null);
		registerCashAccount.getSettlerAccount().setBicCodeOtherBank(null);
		registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
		registerCashAccount.getSettlerAccount().setBankAccountTypeThird(null);
		registerCashAccount.getSettlerAccount().setIndOtherBank(BooleanType.NO.getCode());
		if (new Integer(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().toString()).equals(registerCashAccount.getSettlerAccount().getBankAccountClassSelected())) {
			registerCashAccount.getSettlerAccount().setIndOtherBank(GeneralConstants.ONE_VALUE_INTEGER);
			if (registerCashAccount.getSettlerAccount().getLstOtherBank() == null 
					|| registerCashAccount.getSettlerAccount().getLstOtherBank().size() == 0) {
				List<Bank> lstBanks = fillSettlementBank();
				registerCashAccount.getSettlerAccount().setLstOtherBank(lstBanks);
			}	
		} else if (new Integer(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().toString()).equals(registerCashAccount.getSettlerAccount().getBankAccountClassSelected())) {
			if(Validations.validateIsNullOrEmpty(registerCashAccount.getSettlerAccount().getCurrencySelected())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_CURRENCY_NOT_SELECTED));
				JSFUtilities.showValidationDialog();
				registerCashAccount.getSettlerAccount().setBankAccountClassSelected(null);
			}else{
				if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getSettlerAccount().getParticipantSelected())){
					List<String> lstValues = manageEffectiveAccountsFacade.getBCBBankBicCode();
					if(Validations.validateIsNull(lstValues)){
						
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_CENTRALIZEDBANK_NOTFOUND));
						JSFUtilities.showValidationDialog();
						registerCashAccount.getSettlerAccount().setBankAccountClassSelected(null);
						
					}else{
						InstitutionBankAccount institutionBankAccount = manageEffectiveAccountsFacade.getParticipantBankAccount(
								new Long(registerCashAccount.getSettlerAccount().getParticipantSelected()), 
								registerCashAccount.getSettlerAccount().getCurrencySelected(),
								Long.valueOf(lstValues.get(2)));
						if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
							registerCashAccount.getSettlerAccount().setBankAccountTypeOwner(institutionBankAccount.getBankAcountType().intValue());
							registerCashAccount.getSettlerAccount().setBankAccountNumber(institutionBankAccount.getAccountNumber());
							registerCashAccount.getSettlerAccount().setIdInsitutionBankAccountPk(institutionBankAccount.getIdInstitutionBankAccountPk());
						}
						else
						{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_NOT_FOUND));
							JSFUtilities.showValidationDialog();
							registerCashAccount.getSettlerAccount().setBankAccountClassSelected(null);
						}
					}
				}
				registerCashAccount.getSettlerAccount().setIndOtherBank(GeneralConstants.TWO_VALUE_INTEGER);
			}
		}
		
		
	}
	
	/**
	 * Fill commercial bank.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<Bank> fillCommercialBank() throws ServiceException {
	
		return manageEffectiveAccountsFacade.getConfirmedCommercialBank();
	}
	
	/**
	 * Fill settlement bank.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<Bank> fillSettlementBank() throws ServiceException {
		
		return manageEffectiveAccountsFacade.fillSettlementBank();
	}
	
	/**
	 * Fill bank account data.
	 */
	public void fillBankAccountData() {
		JSFUtilities.hideGeneralDialogues();
		  try{
			if (Validations.validateIsNullOrEmpty(registerCashAccount.getSettlerAccount().getOtherBankSelected())) {
				registerCashAccount.getSettlerAccount().setBicCodeOtherBank(null);
				registerCashAccount.getSettlerAccount().setBankAccountType(null);
				registerCashAccount.getSettlerAccount().setBankAccountTypeThird(null);
				registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
			} else {
				Integer currency = registerCashAccount.getSettlerAccount().getCurrencySelected();
				if (currency == null) {			
					registerCashAccount.getSettlerAccount().setOtherBankSelected(null);
					registerCashAccount.getSettlerAccount().setBicCodeOtherBank(null);
					registerCashAccount.getSettlerAccount().setBankAccountType(null);
					registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
					registerCashAccount.getSettlerAccount().setBankAccountTypeThird(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_CURRENCY_NOT_SELECTED));
					JSFUtilities.showValidationDialog();
				} else {
					BankDataTO objBankData = manageEffectiveAccountsFacade.getBankData(registerCashAccount.getSettlerAccount().getOtherBankSelected(), currency);
					if (objBankData == null) {
						registerCashAccount.getSettlerAccount().setOtherBankSelected(null);
						registerCashAccount.getSettlerAccount().setBicCodeOtherBank(null);
						registerCashAccount.getSettlerAccount().setBankAccountType(null);
						registerCashAccount.getSettlerAccount().setBankAccountNumber(null);
						registerCashAccount.getSettlerAccount().setBankAccountTypeThird(null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_NOTFOUND));
						JSFUtilities.showValidationDialog();
					} else {
						registerCashAccount.getSettlerAccount().setBicCodeOtherBank(objBankData.getBicCode());
						registerCashAccount.getSettlerAccount().setBankAccountNumber(objBankData.getBankAccountNumber());
						registerCashAccount.getSettlerAccount().setBankAccountType(objBankData.getBankAccountType());
						registerCashAccount.getSettlerAccount().setBankAccountTypeThird(objBankData.getPkBankAccountType());
						registerCashAccount.getSettlerAccount().setIdInsitutionBankAccountPk(objBankData.getIdInstitutionBankAccountPk());
					}
				}
			}
		  }catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Search issuer handler register.
	 */
	public void searchIssuerHandlerRegister(){
//		executeAction();	
		if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getBenefitPaymentAccount().getIssuer()) 
				&& Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getBenefitPaymentAccount().getIssuer().getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageEffectiveAccountsFacade.findIssuerFacade(registerCashAccount.getBenefitPaymentAccount().getIssuer().getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){	
				registerCashAccount.getBenefitPaymentAccount().setIssuer(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			registerCashAccount.getBenefitPaymentAccount().setIssuer(issuerTemp);
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_NOT_EXIST));				
			JSFUtilities.showSimpleValidationDialog();
		}
		
	}
	
	/**
	 * Search issuer handler register.
	 */
	public void searchIssuerHandlerReturn(){
//		executeAction();	
		if(Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getReturnAccount().getIssuer()) 
				&& Validations.validateIsNotNullAndNotEmpty(registerCashAccount.getReturnAccount().getIssuer().getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageEffectiveAccountsFacade.findIssuerFacade(registerCashAccount.getReturnAccount().getIssuer().getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){	
				registerCashAccount.getBenefitPaymentAccount().setIssuer(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			registerCashAccount.getReturnAccount().setIssuer(issuerTemp);
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_NOT_EXIST));				
			JSFUtilities.showSimpleValidationDialog();
		}
		
	}
	
	/**
	 * Validate add cash account.
	 */
	public void validateAddCashAccount() {
		JSFUtilities.hideGeneralDialogues();
		if(validateFieldsForAdd())
			return;
		
		List<BankAccounts> bankAccountsList = registerCashAccount.getSettlerAccount().getLstBankAccounts();
		Integer useeType = registerCashAccount.getSettlerAccount().getUseType();
		Integer bankAccountClass = registerCashAccount.getSettlerAccount().getBankAccountClassSelected();
		if(Validations.validateIsNullOrEmpty(registerCashAccount.getSettlerAccount().getBankAccountCurrecy())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_CURRENCY_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
			return;
		}
		if (bankAccountsList == null || bankAccountsList.size() == 0)
			addCashAccount();
		else if (bankAccountsList.size() >= 1) {
			for (Iterator<BankAccounts> it = bankAccountsList.iterator(); it.hasNext();) {
				BankAccounts obj = (BankAccounts) it.next();
				if (MasterTableType.PARAMETER_TABLE_USE_TYPE_BOTH.getLngCode().toString().equals(useeType.toString())) {
					if(obj.getIndReception().equals(BooleanType.YES.getValue())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_USERTYPE_RECEPTION));
						JSFUtilities.showValidationDialog();
						return;
					}else if(obj.getIndSent().equals(BooleanType.YES.getValue())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_USERTYPE_SEND));
						JSFUtilities.showValidationDialog();
						return;
					}					
				}
				/*
				if (obj.getIndReception().equals(BooleanType.YES.getValue())
						&& MasterTableType.PARAMETER_TABLE_USE_TYPE_RECEPTION.getLngCode().toString().equals(useeType.toString())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_USERTYPE_RECEPTION));
					JSFUtilities.showValidationDialog();
					return;
				}*/
				if (obj.getIndSent().equals(BooleanType.YES.getValue())
							&& MasterTableType.PARAMETER_TABLE_USE_TYPE_SEND.getLngCode().toString().equals(useeType.toString())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_USERTYPE_SEND));
					JSFUtilities.showValidationDialog();
					return;
				}
//				if(obj.getIdBankAccountClass().equals(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode())
//					&& MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().equals(Long.valueOf(bankAccountClass))) {
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_OWN_CLASS_EXIST));
//					JSFUtilities.showValidationDialog();
//					return;
//				}
			}
			Integer bank = registerCashAccount.getSettlerAccount().getOtherBankSelected();
			if (bank != null) {
				for (Iterator<BankAccounts> it = bankAccountsList.iterator(); it.hasNext();) {
					BankAccounts objBank = (BankAccounts) it.next();
					if (new Long(bank).equals(objBank.getBankAccountId())) {
						if (MasterTableType.PARAMETER_TABLE_USE_TYPE_RECEPTION.getLngCode().toString().equals(useeType.toString()) &&
							objBank.getIndReception().equals(BooleanType.YES.getValue()) ||
							MasterTableType.PARAMETER_TABLE_USE_TYPE_SEND.getLngCode().toString().equals(useeType.toString()) &&
							objBank.getIndSent().equals(BooleanType.YES.getValue())) 
						{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_ALREADY_ADDED));
							JSFUtilities.showValidationDialog();
							return;
						}
					}
				}
			}
			//VALIDATE IF ACCOUNT NUMBER EXIST IN LIST
			String bankAccountNumber = registerCashAccount.getSettlerAccount().getBankAccountNumber();
			for (Iterator<BankAccounts> it = bankAccountsList.iterator(); it.hasNext();) {
				BankAccounts objBank = (BankAccounts) it.next();
				if (bankAccountNumber.equals(objBank.getBankAccountNumber())) {
					if (MasterTableType.PARAMETER_TABLE_USE_TYPE_RECEPTION.getLngCode().toString().equals(useeType.toString()) &&
						objBank.getIndReception().equals(BooleanType.YES.getValue()) ||
						MasterTableType.PARAMETER_TABLE_USE_TYPE_SEND.getLngCode().toString().equals(useeType.toString()) &&
						objBank.getIndSent().equals(BooleanType.YES.getValue())) 
					{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_NUMBER_ALREADY_ADDED));
						JSFUtilities.showValidationDialog();
						return;
					}
				}
			}
			// IF VALIDATIONS ARE CORRECT, WE ADD THE NEW REGISTER
			addCashAccount();
		} /*else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_MAX_QUANTITY));
			JSFUtilities.showValidationDialog();
			return;
		}*/
		return;
	}
	
	/**
	 * Validate fields for add.
	 *
	 * @return true, if successful
	 */
	private boolean validateFieldsForAdd(){
		boolean blRequiredField = false;
		String empty = GeneralConstants.EMPTY_STRING;
//		if(Validations.validateIsNullOrNotPositive(registerCashAccount.getSettlerAccount().getBankAccountClassSelected())){
//			JSFUtilities.addContextMessage("frmCashAccountRegister:cboBankClass", FacesMessage.SEVERITY_ERROR, empty, empty);
//			blRequiredField = true;
//		}else{
//			if(new Integer(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().toString()).equals(registerCashAccount.getSettlerAccount().getBankAccountClassSelected())){
//				if(Validations.validateIsNullOrEmpty(registerCashAccount.getSettlerAccount().getBankAccountTypeOwner())){
//					JSFUtilities.addContextMessage("frmCashAccountRegister:idBnkAccType", FacesMessage.SEVERITY_ERROR, empty, empty);
//					blRequiredField = true;
//				}
//				if(Validations.validateIsNullOrEmpty(registerCashAccount.getSettlerAccount().getBankAccountNumber())){
//					JSFUtilities.addContextMessage("frmCashAccountRegister:idBankAccountNumber", FacesMessage.SEVERITY_ERROR, empty, empty);
//					blRequiredField = true;
//				}
//			}else if(new Integer(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().toString()).equals(registerCashAccount.getSettlerAccount().getBankAccountClassSelected())){
//				if(Validations.validateIsNullOrNotPositive(registerCashAccount.getSettlerAccount().getOtherBankSelected())){
//					JSFUtilities.addContextMessage("frmCashAccountRegister:idThirdBank", FacesMessage.SEVERITY_ERROR, empty, empty);
//					blRequiredField = true;
//				}
//			}
//		}
		if(Validations.validateIsNullOrNotPositive(registerCashAccount.getSettlerAccount().getUseType())){
			JSFUtilities.addContextMessage("frmCashAccountRegister:idUseType", FacesMessage.SEVERITY_ERROR, empty, empty);
			blRequiredField = true;
		}		
		return blRequiredField;
	}
	
	/**
	 * Adds the cash account.
	 *
	 * @return the string
	 */
	public String addCashAccount() {
		Integer useeType = registerCashAccount.getSettlerAccount().getUseType();
		
		List<BankAccounts> bankAccountsList = registerCashAccount.getSettlerAccount().getLstBankAccounts();
		if (bankAccountsList == null) {
			bankAccountsList = new ArrayList<BankAccounts>();
		}

		Integer bankAccountClass = registerCashAccount.getSettlerAccount().getBankAccountClassSelected();
		
		BankAccounts objBean = new BankAccounts();
		objBean.setIdInstitutionBankAccountPk(registerCashAccount.getSettlerAccount().getIdInsitutionBankAccountPk());
		objBean.setIndSent(BooleanType.NO.getValue());
		objBean.setIndReception(BooleanType.NO.getValue());
		
		if (useeType.toString().equals(MasterTableType.PARAMETER_TABLE_USE_TYPE_SEND.getLngCode().toString())){
			objBean.setIndSent(BooleanType.YES.getValue());
		} else if(useeType.toString().equals(MasterTableType.PARAMETER_TABLE_USE_TYPE_RECEPTION.getLngCode().toString())){
			objBean.setIndReception(BooleanType.YES.getValue());
		}else{
			objBean.setIndSent(BooleanType.YES.getValue());
			objBean.setIndReception(BooleanType.YES.getValue());
		}

		List<ParameterTable> lstBankClassAccount = registerCashAccount.getSettlerAccount().getLstBankAccountClass();
		if (lstBankClassAccount != null && lstBankClassAccount.size() > 0) {
			for (Iterator<ParameterTable> itr = lstBankClassAccount.iterator(); itr.hasNext();) {
				ParameterTable obj = (ParameterTable) itr.next();
				if (obj.getParameterTablePk().equals(bankAccountClass)){
					objBean.setBankAccountClass(obj.getDescription());
					objBean.setIdBankAccountClass(bankAccountClass.longValue());
				}
			}
		}
		Integer otherBank = registerCashAccount.getSettlerAccount().getBankSelected();
		List<Bank> lstBanks = registerCashAccount.getSettlerAccount().getLstBank();
		if (lstBanks != null && lstBanks.size() > 0) {
			for (Iterator<Bank> itr = lstBanks.iterator(); itr.hasNext();) {
				Bank obj = (Bank) itr.next();
				if (otherBank != null && (obj.getIdBankPk()).equals(otherBank.longValue())) {
					objBean.setOtherBank(obj.getDescription());
					objBean.setBankAccountId(obj.getIdBankPk());
				}
			}
		}
		
//			if (MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().equals(Long.valueOf(bankAccountClass))) {
//				Bank centralBank = manageEffectiveAccountsFacade.getBCBBank();
//				objBean.setOtherBank(centralBank.getDescription());
//				objBean.setBankAccountId(centralBank.getIdBankPk());
//			}

		Integer currency = registerCashAccount.getSettlerAccount().getCurrencySelected();
		List<ParameterTable> lstCurrency = registerCashAccount.getSettlerAccount().getLstCurrency();
		if (lstCurrency != null && lstCurrency.size() > 0) {
			for (Iterator<ParameterTable> itr = lstCurrency.iterator(); itr.hasNext();) {
				ParameterTable obj = (ParameterTable) itr.next();
				if ((obj.getParameterTablePk()).equals(currency)) {
					objBean.setCurrency(obj.getDescription());
				}
			}
		}

		objBean.setBankAccountNumber(registerCashAccount.getSettlerAccount().getBankAccountNumber());

//			if (MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().equals(new Long(bankAccountClass))) {
//				objBean.setIdBankAccountType(new Long(registerCashAccount.getSettlerAccount().getBankAccountTypeOwner().toString()));				
//			} else if (MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().equals(new Long(bankAccountClass))) {
//				objBean.setIdBankAccountType(registerCashAccount.getSettlerAccount().getBankAccountTypeThird());
//			}
		objBean.setIdBankAccountType(registerCashAccount.getSettlerAccount().getInstitutionBankAccount().getBankAcountType());
		objBean.setBankAccountType(BankAccountType.get(new Integer(objBean.getIdBankAccountType().toString())).getValue());
		objBean.setIdInstitutionBankAccountPk(registerCashAccount.getSettlerAccount().getInstitutionBankAccount().getIdInstitutionBankAccountPk());
		bankAccountsList.add(objBean);			
		registerCashAccount.getSettlerAccount().setLstBankAccounts(bankAccountsList);
		registerCashAccount.getSettlerAccount().setBankAccountDataModel(new BankAccountsDataModel(registerCashAccount.getSettlerAccount().getLstBankAccounts()));
		registerCashAccount.getSettlerAccount().setIndOtherBank(BooleanType.NO.getCode());
		registerCashAccount.getSettlerAccount().setBankAccountClassSelected(BooleanType.NO.getCode());
		registerCashAccount.getSettlerAccount().setOtherBankSelected(BooleanType.NO.getCode());
		registerCashAccount.getSettlerAccount().setUseType(null);
		registerCashAccount.getSettlerAccount().setBicCodeOtherBank(null);
		registerCashAccount.getSettlerAccount().setBankAccountNumber(null);	
		registerCashAccount.getSettlerAccount().setIdInsitutionBankAccountPk(null);
		registerCashAccount.getSettlerAccount().setBankAccountClassSelected(null);
		//registerCashAccount.getSettlerAccount().setTypeAccountSelected(null);
		//registerCashAccount.getSettlerAccount().setBankSelected(null);
		//registerCashAccount.getSettlerAccount().getInstitutionBankAccount().setIdInstitutionBankAccountPk(null);
		//registerCashAccount.getSettlerAccount().setBicCode(null);
		//lstInstitutionBankAccount=null;
		return null;
	}
	
	/**
	 * Delete cash account.
	 */
	public void deleteCashAccount(){
		JSFUtilities.hideGeneralDialogues();
		if (bankAccounts == null) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_BANK_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		} else {
			try {
				Object[] argObj = new Object[2];
				argObj[0] = bankAccounts.getCurrency();
				argObj[1] = bankAccounts.getBankAccountNumber();
				JSFUtilities.executeJavascriptFunction("PF('wvRemoveBank').show()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_REMOVE_CONFIRM, argObj));
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	
	/**
	 * Delete cash account confirmed.
	 */
	public void deleteCashAccountConfirmed() {
		JSFUtilities.hideGeneralDialogues();
		List<BankAccounts> lstBankData = new ArrayList<BankAccounts>(registerCashAccount.getSettlerAccount().getLstBankAccounts());
		List<BankAccounts> lstBankActData = new ArrayList<BankAccounts>(registerCashAccount.getSettlerAccount().getLstBankAccounts());
		for (int i = 0; i < lstBankData.size(); i++) {
			if (bankAccounts != null && lstBankData.get(i).getBankAccountNumber().equals(bankAccounts.getBankAccountNumber())) {
				lstBankActData.remove(lstBankData.get(i));
			}
		}
		if(Validations.validateListIsNullOrEmpty(lstBankActData)){
			registerCashAccount.getSettlerAccount().setLstBankAccounts(null);
			registerCashAccount.getSettlerAccount().setBankAccountDataModel(null);
		}else{
			registerCashAccount.getSettlerAccount().setLstBankAccounts(lstBankActData);
			registerCashAccount.getSettlerAccount().setBankAccountDataModel(new BankAccountsDataModel(registerCashAccount.getSettlerAccount().getLstBankAccounts()));
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_DELETE_BANK));
		JSFUtilities.showValidationDialog();
	}
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipant(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(objInstitutionBankAccountsType));
	}	
	
	/**
	 * Is Institution Type Issuer.
	 *
	 * @return validate Institution Type Issuer
	 */
	public boolean isIndInstitutionTypeIssuer(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.ISSUER.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Is Institution Type Bank.
	 *
	 * @return validate Institution Type Bank
	 */
	public boolean isIndInstitutionTypeBank(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.BANK.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Set User Information.
	 *
	 * @param userInfo User Information
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Get Institution Cash Account Session.
	 *
	 * @return Institution Cash Account Session
	 */
	public InstitutionCashAccount getInstitutionCashAccountSession() {
		return institutionCashAccountSession;
	}
	
	/**
	 * Set Institution Cash Account Session.
	 *
	 * @param institutionCashAccountSession Institution Cash Account Session
	 */
	public void setInstitutionCashAccountSession(
			InstitutionCashAccount institutionCashAccountSession) {
		this.institutionCashAccountSession = institutionCashAccountSession;
	}
	
	/**
	 * Get Search Effective Accounts Register TO.
	 *
	 * @return Search Effective Accounts Register TO
	 */
	public SearchEffectiveAccountsTO getSearchEffectiveAccountsRegisterTO() {
		return searchEffectiveAccountsRegisterTO;
	}
	
	/**
	 * Set Search Effective Accounts Register TO.
	 *
	 * @param searchEffectiveAccountsRegisterTO Search Effective Accounts Register TO
	 */
	public void setSearchEffectiveAccountsRegisterTO(
			SearchEffectiveAccountsTO searchEffectiveAccountsRegisterTO) {
		this.searchEffectiveAccountsRegisterTO = searchEffectiveAccountsRegisterTO;
	}
	
	/**
	 * Get Search Effective Accounts TO.
	 *
	 * @return Search Effective Accounts TO
	 */
	public SearchEffectiveAccountsTO getSearchEffectiveAccountsTO() {
		return searchEffectiveAccountsTO;
	}
	
	/**
	 * Set Search Effective Accounts TO.
	 *
	 * @param searchEffectiveAccountsTO Search Effective Accounts TO
	 */
	public void setSearchEffectiveAccountsTO(
			SearchEffectiveAccountsTO searchEffectiveAccountsTO) {
		this.searchEffectiveAccountsTO = searchEffectiveAccountsTO;
	}
	
	/**
	 * Get Institution Bank Account.
	 *
	 * @return Institution Bank Account
	 */
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}
	
	/**
	 * Set Institution Bank Account.
	 *
	 * @param institutionBankAccount Institution Bank Account
	 */
	public void setInstitutionBankAccount(
			InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}

	/**
	 * Gets the lst cash account type.
	 *
	 * @return the lst cash account type
	 */
	public List<ParameterTable> getLstCashAccountType() {
		return lstCashAccountType;
	}

	/**
	 * Sets the lst cash account type.
	 *
	 * @param lstCashAccountType the new lst cash account type
	 */
	public void setLstCashAccountType(List<ParameterTable> lstCashAccountType) {
		this.lstCashAccountType = lstCashAccountType;
	}

	/**
	 * Gets the lst account state.
	 *
	 * @return the lst account state
	 */
	public List<ParameterTable> getLstAccountState() {
		return lstAccountState;
	}

	/**
	 * Sets the lst account state.
	 *
	 * @param lstAccountState the new lst account state
	 */
	public void setLstAccountState(List<ParameterTable> lstAccountState) {
		this.lstAccountState = lstAccountState;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Checks if is bl modify.
	 *
	 * @return true, if is bl modify
	 */
	public boolean isBlModify() {
		return blModify;
	}

	/**
	 * Sets the bl modify.
	 *
	 * @param blModify the new bl modify
	 */
	public void setBlModify(boolean blModify) {
		this.blModify = blModify;
	}

	/**
	 * Checks if is bl active.
	 *
	 * @return true, if is bl active
	 */
	public boolean isBlActive() {
		return blActive;
	}

	/**
	 * Sets the bl active.
	 *
	 * @param blActive the new bl active
	 */
	public void setBlActive(boolean blActive) {
		this.blActive = blActive;
	}

	/**
	 * Checks if is bl lock.
	 *
	 * @return true, if is bl lock
	 */
	public boolean isBlLock() {
		return blLock;
	}

	/**
	 * Sets the bl lock.
	 *
	 * @param blLock the new bl lock
	 */
	public void setBlLock(boolean blLock) {
		this.blLock = blLock;
	}

	/**
	 * Checks if is bl view.
	 *
	 * @return true, if is bl view
	 */
	public boolean isBlView() {
		return blView;
	}

	/**
	 * Sets the bl view.
	 *
	 * @param blView the new bl view
	 */
	public void setBlView(boolean blView) {
		this.blView = blView;
	}

	/**
	 * Checks if is bl register.
	 *
	 * @return true, if is bl register
	 */
	public boolean isBlRegister() {
		return blRegister;
	}

	/**
	 * Sets the bl register.
	 *
	 * @param blRegister the new bl register
	 */
	public void setBlRegister(boolean blRegister) {
		this.blRegister = blRegister;
	}

	/**
	 * Gets the bank accounts.
	 *
	 * @return the bank accounts
	 */
	public BankAccounts getBankAccounts() {
		return bankAccounts;
	}

	/**
	 * Sets the bank accounts.
	 *
	 * @param bankAccounts the new bank accounts
	 */
	public void setBankAccounts(BankAccounts bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	/**
	 * Gets the cash account type selected.
	 *
	 * @return the cash account type selected
	 */
	public Integer getCashAccountTypeSelected() {
		return cashAccountTypeSelected;
	}

	/**
	 * Sets the cash account type selected.
	 *
	 * @param cashAccountTypeSelected the new cash account type selected
	 */
	public void setCashAccountTypeSelected(Integer cashAccountTypeSelected) {
		this.cashAccountTypeSelected = cashAccountTypeSelected;
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Integer getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Integer participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the lst issuers.
	 *
	 * @return the lst issuers
	 */
	public List<Issuer> getLstIssuers() {
		return lstIssuers;
	}

	/**
	 * Sets the lst issuers.
	 *
	 * @param lstIssuers the new lst issuers
	 */
	public void setLstIssuers(List<Issuer> lstIssuers) {
		this.lstIssuers = lstIssuers;
	}

	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}

	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}

	/**
	 * Gets the state account selected.
	 *
	 * @return the state account selected
	 */
	public Integer getStateAccountSelected() {
		return stateAccountSelected;
	}

	/**
	 * Sets the state account selected.
	 *
	 * @param stateAccountSelected the new state account selected
	 */
	public void setStateAccountSelected(Integer stateAccountSelected) {
		this.stateAccountSelected = stateAccountSelected;
	}

	/**
	 * Gets the lst bank associated.
	 *
	 * @return the lst bank associated
	 */
	public List<Bank> getLstBankAssociated() {
		return lstBankAssociated;
	}

	/**
	 * Sets the lst bank associated.
	 *
	 * @param lstBankAssociated the new lst bank associated
	 */
	public void setLstBankAssociated(List<Bank> lstBankAssociated) {
		this.lstBankAssociated = lstBankAssociated;
	}

	/**
	 * Gets the bank associated selected.
	 *
	 * @return the bank associated selected
	 */
	public Integer getBankAssociatedSelected() {
		return bankAssociatedSelected;
	}

	/**
	 * Sets the bank associated selected.
	 *
	 * @param bankAssociatedSelected the new bank associated selected
	 */
	public void setBankAssociatedSelected(Integer bankAssociatedSelected) {
		this.bankAssociatedSelected = bankAssociatedSelected;
	}

	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}

	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}

	/**
	 * Gets the id moda group pk selected.
	 *
	 * @return the id moda group pk selected
	 */
	public Long getIdModaGroupPkSelected() {
		return idModaGroupPkSelected;
	}

	/**
	 * Sets the id moda group pk selected.
	 *
	 * @param idModaGroupPkSelected the new id moda group pk selected
	 */
	public void setIdModaGroupPkSelected(Long idModaGroupPkSelected) {
		this.idModaGroupPkSelected = idModaGroupPkSelected;
	}

	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}

	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}

	/**
	 * Gets the id nego mechanism pk selected.
	 *
	 * @return the id nego mechanism pk selected
	 */
	public Long getIdNegoMechanismPkSelected() {
		return idNegoMechanismPkSelected;
	}

	/**
	 * Sets the id nego mechanism pk selected.
	 *
	 * @param idNegoMechanismPkSelected the new id nego mechanism pk selected
	 */
	public void setIdNegoMechanismPkSelected(Long idNegoMechanismPkSelected) {
		this.idNegoMechanismPkSelected = idNegoMechanismPkSelected;
	}

	/**
	 * Gets the ind show mechanism.
	 *
	 * @return the ind show mechanism
	 */
	public int getIndShowMechanism() {
		return indShowMechanism;
	}

	/**
	 * Sets the ind show mechanism.
	 *
	 * @param indShowMechanism the new ind show mechanism
	 */
	public void setIndShowMechanism(int indShowMechanism) {
		this.indShowMechanism = indShowMechanism;
	}

	/**
	 * Gets the ind show modality group.
	 *
	 * @return the ind show modality group
	 */
	public int getIndShowModalityGroup() {
		return indShowModalityGroup;
	}

	/**
	 * Sets the ind show modality group.
	 *
	 * @param indShowModalityGroup the new ind show modality group
	 */
	public void setIndShowModalityGroup(int indShowModalityGroup) {
		this.indShowModalityGroup = indShowModalityGroup;
	}

	/**
	 * Gets the ind show settlement schema.
	 *
	 * @return the ind show settlement schema
	 */
	public int getIndShowSettlementSchema() {
		return indShowSettlementSchema;
	}

	/**
	 * Sets the ind show settlement schema.
	 *
	 * @param indShowSettlementSchema the new ind show settlement schema
	 */
	public void setIndShowSettlementSchema(int indShowSettlementSchema) {
		this.indShowSettlementSchema = indShowSettlementSchema;
	}

	/**
	 * Gets the ind show bic code.
	 *
	 * @return the ind show bic code
	 */
	public int getIndShowBicCode() {
		return indShowBicCode;
	}

	/**
	 * Sets the ind show bic code.
	 *
	 * @param indShowBicCode the new ind show bic code
	 */
	public void setIndShowBicCode(int indShowBicCode) {
		this.indShowBicCode = indShowBicCode;
	}

	/**
	 * Gets the ind show bank.
	 *
	 * @return the ind show bank
	 */
	public int getIndShowBank() {
		return indShowBank;
	}

	/**
	 * Sets the ind show bank.
	 *
	 * @param indShowBank the new ind show bank
	 */
	public void setIndShowBank(int indShowBank) {
		this.indShowBank = indShowBank;
	}

	/**
	 * Gets the ind show bank account number.
	 *
	 * @return the ind show bank account number
	 */
	public int getIndShowBankAccountNumber() {
		return indShowBankAccountNumber;
	}

	/**
	 * Sets the ind show bank account number.
	 *
	 * @param indShowBankAccountNumber the new ind show bank account number
	 */
	public void setIndShowBankAccountNumber(int indShowBankAccountNumber) {
		this.indShowBankAccountNumber = indShowBankAccountNumber;
	}

	/**
	 * Gets the ind show commercial.
	 *
	 * @return the ind show commercial
	 */
	public int getIndShowCommercial() {
		return indShowCommercial;
	}

	/**
	 * Sets the ind show commercial.
	 *
	 * @param indShowCommercial the new ind show commercial
	 */
	public void setIndShowCommercial(int indShowCommercial) {
		this.indShowCommercial = indShowCommercial;
	}

	/**
	 * Gets the ind show issuer.
	 *
	 * @return the ind show issuer
	 */
	public int getIndShowIssuer() {
		return indShowIssuer;
	}

	/**
	 * Sets the ind show issuer.
	 *
	 * @param indShowIssuer the new ind show issuer
	 */
	public void setIndShowIssuer(int indShowIssuer) {
		this.indShowIssuer = indShowIssuer;
	}

	/**
	 * Gets the ind show participant.
	 *
	 * @return the ind show participant
	 */
	public int getIndShowParticipant() {
		return indShowParticipant;
	}

	/**
	 * Sets the ind show participant.
	 *
	 * @param indShowParticipant the new ind show participant
	 */
	public void setIndShowParticipant(int indShowParticipant) {
		this.indShowParticipant = indShowParticipant;
	}

	/**
	 * Gets the cash account.
	 *
	 * @return the cash account
	 */
	public CashAccountsTO getCashAccount() {
		return cashAccount;
	}

	/**
	 * Sets the cash account.
	 *
	 * @param cashAccount the new cash account
	 */
	public void setCashAccount(CashAccountsTO cashAccount) {
		this.cashAccount = cashAccount;
	}

	/**
	 * Gets the ind show cash accounts.
	 *
	 * @return the ind show cash accounts
	 */
	public int getIndShowCashAccounts() {
		return indShowCashAccounts;
	}

	/**
	 * Sets the ind show cash accounts.
	 *
	 * @param indShowCashAccounts the new ind show cash accounts
	 */
	public void setIndShowCashAccounts(int indShowCashAccounts) {
		this.indShowCashAccounts = indShowCashAccounts;
	}

	/**
	 * Gets the query cash account data model.
	 *
	 * @return the query cash account data model
	 */
	public CashAccountsTODataModel getQueryCashAccountDataModel() {
		return queryCashAccountDataModel;
	}

	/**
	 * Sets the query cash account data model.
	 *
	 * @param queryCashAccountDataModel the new query cash account data model
	 */
	public void setQueryCashAccountDataModel(
			CashAccountsTODataModel queryCashAccountDataModel) {
		this.queryCashAccountDataModel = queryCashAccountDataModel;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the register cash account.
	 *
	 * @return the register cash account
	 */
	public RegisterCashAccount getRegisterCashAccount() {
		return registerCashAccount;
	}

	/**
	 * Sets the register cash account.
	 *
	 * @param registerCashAccount the new register cash account
	 */
	public void setRegisterCashAccount(RegisterCashAccount registerCashAccount) {
		this.registerCashAccount = registerCashAccount;
	}
	
	/**
	 * Fill view bank account data.
	 *
	 * @param instCashAcc the inst cash acc
	 * @throws ServiceException the service exception
	 */
	public void fillViewBankAccountData( InstitutionCashAccount instCashAcc) throws ServiceException
	{
		if (AccountCashFundsType.CENTRALIZING.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
			objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
			objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
			
			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
			if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
			{
				lstBankAccountsFinal.add(institutionBankAccount);
				break;
			}
			registerCashAccount.getCentralizingAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		} else if (AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			
			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
			objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			objSearchBankAccountsTO.setIdParticipantPk(registerCashAccount.getMarginGuaranteeAccount().getParticipantSelected().longValue());
			objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getMarginGuaranteeAccount().getBankSelected().longValue());
			objSearchBankAccountsTO.setBankType(registerCashAccount.getMarginGuaranteeAccount().getBankType());
			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
					lstBankAccountsFinal.add(institutionBankAccount);
			registerCashAccount.getMarginGuaranteeAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		}
		else if (AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
//			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();			
//			if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
//			}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setIdParticipantPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant().getIdParticipantPk());
//			}			
//			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
//			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
//			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);				
//			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
//			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
//				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
//					lstBankAccountsFinal.add(institutionBankAccount);
//			registerCashAccount.getRatesAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal)); 
		}
		else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
			objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			objSearchBankAccountsTO.setIdParticipantPk(registerCashAccount.getInternationalOperationsAccount().getParticipantSelected().longValue());
			objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getInternationalOperationsAccount().getBankSelected().longValue());
			objSearchBankAccountsTO.setBankType(registerCashAccount.getInternationalOperationsAccount().getBankType());
			objSearchBankAccountsTO.setBankAccountType(registerCashAccount.getInternationalOperationsAccount().getBankAccountType());
			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
					lstBankAccountsFinal.add(institutionBankAccount);
			registerCashAccount.getInternationalOperationsAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal)); 
		
		} else if (AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {

//			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
//			if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
//			}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setIdParticipantPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant().getIdParticipantPk());
//			}	
//			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
//			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
//			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);				
//			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
//			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
//				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
//					lstBankAccountsFinal.add(institutionBankAccount);
//			registerCashAccount.getReturnAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		} else if (AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
//			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
//			if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
//			}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setIdParticipantPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant().getIdParticipantPk());
//			}	
//			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
//			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
//			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);				
//			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
//			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
//				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
//					lstBankAccountsFinal.add(institutionBankAccount);
//			registerCashAccount.getTaxesAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		}
	}
	
	/**
	 * Fill modify bank account data.
	 *
	 * @param instCashAcc the inst cash acc
	 * @throws ServiceException the service exception
	 */
	public void fillModifyBankAccountData( InstitutionCashAccount instCashAcc) throws ServiceException
	{
		if (AccountCashFundsType.CENTRALIZING.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_SETTLEMENT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_BENEFIT.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())
				||AccountCashFundsType.CENTRALIZING_TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
			objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
			objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
			
			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
			{
				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
				{
					registerCashAccount.getCentralizingAccount().setInstitutionBankAccountSession(institutionBankAccount);
				}
				lstBankAccountsFinal.add(institutionBankAccount);
			}
			registerCashAccount.getCentralizingAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		} else if (AccountCashFundsType.GUARANTEES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			
			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
			objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			objSearchBankAccountsTO.setIdParticipantPk(registerCashAccount.getMarginGuaranteeAccount().getParticipantSelected().longValue());
			objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getMarginGuaranteeAccount().getBankSelected().longValue());
			objSearchBankAccountsTO.setBankType(registerCashAccount.getMarginGuaranteeAccount().getBankType());
			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
			
			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
			{
				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
				{
					registerCashAccount.getMarginGuaranteeAccount().setInstitutionBankAccountSession(institutionBankAccount);
				}
				lstBankAccountsFinal.add(institutionBankAccount);
			}
			registerCashAccount.getMarginGuaranteeAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		}
		else if (AccountCashFundsType.RATES.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
//			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
//			if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
//			}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setIdParticipantPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant().getIdParticipantPk());
//			}	
//			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
//			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
//			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);	
//			
//			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
//			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
//			{
//				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
//				{
//					registerCashAccount.getRatesAccount().setInstitutionBankAccountSession(institutionBankAccount);
//				}
//				lstBankAccountsFinal.add(institutionBankAccount);
//			}
//			registerCashAccount.getRatesAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		}
		else if (AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
			objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			objSearchBankAccountsTO.setIdParticipantPk(registerCashAccount.getInternationalOperationsAccount().getParticipantSelected().longValue());
			objSearchBankAccountsTO.setIdBankProviderPk(registerCashAccount.getInternationalOperationsAccount().getBankSelected().longValue());
			objSearchBankAccountsTO.setBankType(registerCashAccount.getInternationalOperationsAccount().getBankType());
			objSearchBankAccountsTO.setBankAccountType(registerCashAccount.getInternationalOperationsAccount().getBankAccountType());
			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
			{
				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
				{
					registerCashAccount.getInternationalOperationsAccount().setInstitutionBankAccountSession(institutionBankAccount);
				}
				lstBankAccountsFinal.add(institutionBankAccount);
			}
			registerCashAccount.getInternationalOperationsAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		} else if (AccountCashFundsType.RETURN.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
	
//			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
//			if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
//			}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setIdParticipantPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant().getIdParticipantPk());
//			}	
//			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
//			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
//			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);				
//			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
//			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
//			{
//				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
//				{
//					registerCashAccount.getReturnAccount().setInstitutionBankAccountSession(institutionBankAccount);
//				}
//				lstBankAccountsFinal.add(institutionBankAccount);
//			}
//			registerCashAccount.getReturnAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		} else if (AccountCashFundsType.TAX.getCode().equals(registerCashAccount.getCashAccountTypeSelected())) {
//			SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
//			if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
//				objSearchBankAccountsTO.setIdBankInstitutionPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBankPk());
//			}else if(Validations.validateIsNotNull(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant())){
//				this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//				objSearchBankAccountsTO.setIdParticipantPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getParticipant().getIdParticipantPk());
//			}	
//			objSearchBankAccountsTO.setIdBankProviderPk(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getIdBankPk());
//			objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
//			List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);				
//			List<InstitutionBankAccount> lstBankAccountsFinal = new ArrayList<InstitutionBankAccount>() ;
//			for(InstitutionBankAccount institutionBankAccount : lstBankAccountsTmp)
//			{
//				if(institutionBankAccount.getIdInstitutionBankAccountPk().equals(instCashAcc.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getIdInstitutionBankAccountPk()))
//				{
//					registerCashAccount.getTaxesAccount().setInstitutionBankAccountSession(institutionBankAccount);
//				}
//				lstBankAccountsFinal.add(institutionBankAccount);
//			}
//			registerCashAccount.getTaxesAccount().setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsFinal));
		}
	}
	
	/**
	 * Validate activation.
	 *
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateActivation() throws ServiceException
	{ 	
		InstitutionCashAccount instCash=manageEffectiveAccountsFacade.getInstitutionCashAccountData(cashAccount.getIdCashAccount());
		InstitutionCashAccount instCashRegistered=manageEffectiveAccountsFacade.validateActiveCashAccount(instCash);
		if(Validations.validateIsNotNull(instCashRegistered))
			return true;
		else
			return false;
	}


	/**
	 * Gets the lst institution bank accounts type.
	 *
	 * @return the lst institution bank accounts type
	 */
	public List<ParameterTable> getLstInstitutionBankAccountsType() {
		return lstInstitutionBankAccountsType;
	}

	/**
	 * Sets the lst institution bank accounts type.
	 *
	 * @param lstInstitutionBankAccountsType the new lst institution bank accounts type
	 */
	public void setLstInstitutionBankAccountsType(
			List<ParameterTable> lstInstitutionBankAccountsType) {
		this.lstInstitutionBankAccountsType = lstInstitutionBankAccountsType;
	}
	
	public List<ParameterTable> getLstInstitutionBankAccountsTypeReturn() {
		return lstInstitutionBankAccountsTypeReturn;
	}

	public void setLstInstitutionBankAccountsTypeReturn(List<ParameterTable> lstInstitutionBankAccountsTypeReturn) {
		this.lstInstitutionBankAccountsTypeReturn = lstInstitutionBankAccountsTypeReturn;
	}

	/**
	 * Gets the obj institution bank accounts type.
	 *
	 * @return the obj institution bank accounts type
	 */
	public Integer getObjInstitutionBankAccountsType() {
		return objInstitutionBankAccountsType;
	}

	/**
	 * Sets the obj institution bank accounts type.
	 *
	 * @param objInstitutionBankAccountsType the new obj institution bank accounts type
	 */
	public void setObjInstitutionBankAccountsType(
			Integer objInstitutionBankAccountsType) {
		this.objInstitutionBankAccountsType = objInstitutionBankAccountsType;
	}

	public List<InstitutionBankAccount> getLstInstitutionBankAccount() {
		return lstInstitutionBankAccount;
	}

	public void setLstInstitutionBankAccount(List<InstitutionBankAccount> lstInstitutionBankAccount) {
		this.lstInstitutionBankAccount = lstInstitutionBankAccount;
	}
	
}