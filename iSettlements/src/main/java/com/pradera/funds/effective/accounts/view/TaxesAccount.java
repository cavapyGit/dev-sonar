package com.pradera.funds.effective.accounts.view;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class TaxesAccount.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class TaxesAccount implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The institution. */
	private String institution;
	
	/** The lst bank. */
	private List<Bank> lstBank;
	
	/** The bank selected. */
	private Integer bankSelected;
	
	/** The lst bank type. */
	private List<ParameterTable> lstBankType;
	
	/** The bank type. */
	private Integer bankType;
	
	/** The bank account type. */
	private Integer bankAccountType;
	
	/** The lst bank account type. */
	private List<ParameterTable> lstBankAccountType;	
	
	/** The state. */
	private String state;
	
	/** The ind bcrd account number. */
	private Integer indBcrdAccountNumber;	
	
	/** The lst atomatic. */
	private List<ParameterTable> lstAtomatic;
	
	/** The ind automatic. */
	private Integer indAutomatic;
	
	/** The bank account pk. */
	private Long bankAccountPK;
	
	/** The lst bank accounts to data model. */
	private GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel;
	
	/** The institution bank account session. */
	private InstitutionBankAccount institutionBankAccountSession;
	
	/**
	 * Gets the lst atomatic.
	 *
	 * @return the lst atomatic
	 */
	public List<ParameterTable> getLstAtomatic() {
		return lstAtomatic;
	}
	
	/**
	 * Sets the lst atomatic.
	 *
	 * @param lstAtomatic the new lst atomatic
	 */
	public void setLstAtomatic(List<ParameterTable> lstAtomatic) {
		this.lstAtomatic = lstAtomatic;
	}
	
	/**
	 * Gets the ind automatic.
	 *
	 * @return the ind automatic
	 */
	public Integer getIndAutomatic() {
		return indAutomatic;
	}
	
	/**
	 * Sets the ind automatic.
	 *
	 * @param indAutomatic the new ind automatic
	 */
	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	} 
	
	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public String getInstitution() {
		return institution;
	}
	
	/**
	 * Sets the institution.
	 *
	 * @param institution the new institution
	 */
	public void setInstitution(String institution) {
		this.institution = institution;
	}

	/**
	 * Gets the bank selected.
	 *
	 * @return the bank selected
	 */
	public Integer getBankSelected() {
		return bankSelected;
	}
	
	/**
	 * Sets the bank selected.
	 *
	 * @param bankSelected the new bank selected
	 */
	public void setBankSelected(Integer bankSelected) {
		this.bankSelected = bankSelected;
	}
	
	/**
	 * Gets the bank type.
	 *
	 * @return the bank type
	 */
	public Integer getBankType() {
		return bankType;
	}
	
	/**
	 * Sets the bank type.
	 *
	 * @param bankType the new bank type
	 */
	public void setBankType(Integer bankType) {
		this.bankType = bankType;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the ind bcrd account number.
	 *
	 * @return the ind bcrd account number
	 */
	public Integer getIndBcrdAccountNumber() {
		return indBcrdAccountNumber;
	}
	
	/**
	 * Sets the ind bcrd account number.
	 *
	 * @param indBcrdAccountNumber the new ind bcrd account number
	 */
	public void setIndBcrdAccountNumber(Integer indBcrdAccountNumber) {
		this.indBcrdAccountNumber = indBcrdAccountNumber;
	}
	
	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return bankAccountType;
	}
	
	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 */
	public List<Bank> getLstBank() {
		return lstBank;
	}
	
	/**
	 * Sets the lst bank.
	 *
	 * @param lstBank the new lst bank
	 */
	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}
	
	/**
	 * Gets the lst bank type.
	 *
	 * @return the lst bank type
	 */
	public List<ParameterTable> getLstBankType() {
		return lstBankType;
	}
	
	/**
	 * Sets the lst bank type.
	 *
	 * @param lstBankType the new lst bank type
	 */
	public void setLstBankType(List<ParameterTable> lstBankType) {
		this.lstBankType = lstBankType;
	}
	
	/**
	 * Gets the lst bank account type.
	 *
	 * @return the lst bank account type
	 */
	public List<ParameterTable> getLstBankAccountType() {
		return lstBankAccountType;
	}
	
	/**
	 * Sets the lst bank account type.
	 *
	 * @param lstBankAccountType the new lst bank account type
	 */
	public void setLstBankAccountType(List<ParameterTable> lstBankAccountType) {
		this.lstBankAccountType = lstBankAccountType;
	}
	
	/**
	 * Gets the bank account pk.
	 *
	 * @return the bank account pk
	 */
	public Long getBankAccountPK() {
		return bankAccountPK;
	}
	
	/**
	 * Sets the bank account pk.
	 *
	 * @param bankAccountPK the new bank account pk
	 */
	public void setBankAccountPK(Long bankAccountPK) {
		this.bankAccountPK = bankAccountPK;
	}
	
	/**
	 * Gets the lst bank accounts to data model.
	 *
	 * @return the lst bank accounts to data model
	 */
	public GenericDataModel<InstitutionBankAccount> getLstBankAccountsTODataModel() {
		return lstBankAccountsTODataModel;
	}
	
	/**
	 * Sets the lst bank accounts to data model.
	 *
	 * @param lstBankAccountsTODataModel the new lst bank accounts to data model
	 */
	public void setLstBankAccountsTODataModel(
			GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel) {
		this.lstBankAccountsTODataModel = lstBankAccountsTODataModel;
	}
	
	/**
	 * Gets the institution bank account session.
	 *
	 * @return the institution bank account session
	 */
	public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}
	
	/**
	 * Sets the institution bank account session.
	 *
	 * @param institutionBankAccountSession the new institution bank account session
	 */
	public void setInstitutionBankAccountSession(
			InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}
}