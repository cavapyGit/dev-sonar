package com.pradera.funds.effective.accounts.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.funds.bank.accounts.to.SearchBankAccountsTO;
import com.pradera.funds.effective.accounts.to.SearchCashDetailTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageEffectiveAccountsService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class ManageEffectiveAccountsService extends CrudDaoServiceBean{

	/**
	 * Get List Bank.
	 *
	 * @param idBankAccountClass Bank Account Class
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	public List<Bank> getListBank(Integer idBankAccountClass)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	");
			if(Validations.validateIsNotNull(idBankAccountClass))
				sbQuery.append(" WHERE cs.bankType = :bankType AND cs.state = :state");
			else
				sbQuery.append(" WHERE cs.state = :state");
			Query query = em.createQuery(sbQuery.toString()); 
			if(Validations.validateIsNotNull(idBankAccountClass)){
				Integer idTypeBank = null;
				if(idBankAccountClass.equals(new Integer(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().toString())))
					idTypeBank = BankType.CENTRALIZING.getCode();
				if(idBankAccountClass.equals(new Integer(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().toString())))
					idTypeBank = BankType.COMMERCIAL.getCode();
				query.setParameter("bankType",  idTypeBank);
				query.setParameter("state",  BankStateType.REGISTERED.getCode());
			}else query.setParameter("state",  BankStateType.REGISTERED.getCode());		
			List<Bank> bank = (List<Bank>)query.getResultList();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get List Bank Accounts Filter.
	 *
	 * @param searchBankAccountsTO search Bank Accounts
	 * @return List Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount getListBankAccountsFilter(SearchBankAccountsTO searchBankAccountsTO)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();	
			stringBuilder.append("	Select cs.idInstitutionBankAccountPk,cs.accountNumber,	");//0,1,
			stringBuilder.append("	cb.description,(Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.bankAcountType),	"); //2,3
			stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.currency),	"); //4	
			stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.state),	"); //5
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
				stringBuilder.append("	par.description,  	"); //6
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
				stringBuilder.append("	iss.description,   "); //6
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				stringBuilder.append("	ban.description,   "); //6
			stringBuilder.append("	cs.state,cb.bicCode	"); //7,8
			stringBuilder.append("	from InstitutionBankAccount cs");				
			stringBuilder.append("	join cs.providerBank cb	");
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
				stringBuilder.append("	join cs.participant par	");
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
				stringBuilder.append("	join cs.issuer iss	");
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				stringBuilder.append("	join cs.bank ban	");
			stringBuilder.append("	Where 1 = 1	");	
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode())){
				if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIssuer().getIdIssuerPk())){
					stringBuilder.append("	AND	cs.issuer.idIssuerPk = :idIssuerPk	");
					parameters.put("idIssuerPk", searchBankAccountsTO.getIssuer().getIdIssuerPk());							
				}
			}
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
				if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIdParticipantPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getIdParticipantPk())))){
					stringBuilder.append("	AND	cs.participant.idParticipantPk = :idParticipantPk	");
					parameters.put("idParticipantPk", searchBankAccountsTO.getIdParticipantPk());							
				}
			}	
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode())){
				if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIdBankInstitutionPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getIdBankInstitutionPk())))){
					stringBuilder.append("	AND	cs.bank.idBankPk = :idBankPk	");
					parameters.put("idBankPk", searchBankAccountsTO.getIdBankInstitutionPk());							
				}
			}	
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getAccountNumber())){
				stringBuilder.append("	AND	cs.accountNumber = :accountNumber	");
				parameters.put("accountNumber", searchBankAccountsTO.getAccountNumber());							
			}
			if(Validations.validateIsNotNull(searchBankAccountsTO.getState()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getState())))){
				stringBuilder.append("	AND	cs.state = :state	");
				parameters.put("state", searchBankAccountsTO.getState());							
			}
			if(Validations.validateIsNotNull(searchBankAccountsTO.getCurrency()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getCurrency())))){
				stringBuilder.append("	AND	cs.currency = :currency	");
				parameters.put("currency", searchBankAccountsTO.getCurrency());							
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getFinalDate())){
				stringBuilder.append(" and  TRUNC(cs.registryDate) between :dateIni and :dateEnd ");
				parameters.put("dateIni", searchBankAccountsTO.getInitialDate());
				parameters.put("dateEnd", searchBankAccountsTO.getFinalDate());			
			}
			stringBuilder.append("	order by cs.idInstitutionBankAccountPk desc	");	
			List<Object[]> lstInstitutionBankAccount =  findListByQueryString(stringBuilder.toString(), parameters);
			Map<Long,InstitutionBankAccount> institutionBankAccountPk = new HashMap<Long, InstitutionBankAccount>();
			for(Object[] object: lstInstitutionBankAccount){
				InstitutionBankAccount institutionBankAccount = new InstitutionBankAccount();
				institutionBankAccount.setIdInstitutionBankAccountPk(new Long(object[0].toString()));
				if(Validations.validateIsNotNull((object[1])))
					institutionBankAccount.setAccountNumber(object[1].toString());
				if(Validations.validateIsNotNull((object[2])))
					institutionBankAccount.setDescriptionBankClient(object[2].toString());
				if(Validations.validateIsNotNull((object[3])))
					institutionBankAccount.setDescriptionBankAcountType(object[3].toString());
				if(Validations.validateIsNotNull((object[4])))
					institutionBankAccount.setDescriptionCurrency((object[4].toString()));
				if(Validations.validateIsNotNull((object[5])))
					institutionBankAccount.setStateDescription(object[5].toString());
				if(Validations.validateIsNotNull((object[6]))){
					if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
						institutionBankAccount.setDescriptionInstitution(InstitutionBankAccountsType.PARTICIPANT.getValue()+" - "+object[6].toString());	
					if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
						institutionBankAccount.setDescriptionInstitution(InstitutionBankAccountsType.ISSUER.getValue()+" - "+object[6].toString());	
					if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
						institutionBankAccount.setDescriptionInstitution(InstitutionBankAccountsType.BANK.getValue()+" - "+object[6].toString());	
				}
				if(Validations.validateIsNotNull((object[7])))	
					institutionBankAccount.setState(new Integer(object[7].toString()));
				if(Validations.validateIsNotNull((object[8])))	
					institutionBankAccount.setBicCodeBank(object[8].toString());
				institutionBankAccountPk.put(institutionBankAccount.getIdInstitutionBankAccountPk(), institutionBankAccount);
			}
			return (InstitutionBankAccount)(institutionBankAccountPk.values());
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Validate institutio cash account.
	 *
	 * @param objSearchCashDetailTO the obj search cash detail to
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount validateInstitutioCashAccount(SearchCashDetailTO objSearchCashDetailTO) throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			Map<String, Object> parameters = new HashMap<String, Object>();
			strQuery.append("SELECT ica");
			strQuery.append("	FROM InstitutionCashAccount ica");
			strQuery.append("	LEFT JOIN FETCH ica.issuer isu");
			strQuery.append("	LEFT JOIN FETCH ica.participant pa");
			strQuery.append("	LEFT JOIN FETCH ica.cashAccountDetailResults cad");
			strQuery.append("	LEFT JOIN FETCH cad.institutionBankAccount iba");
			strQuery.append("	LEFT JOIN FETCH iba.providerBank pb");
			strQuery.append("	LEFT JOIN FETCH iba.bank b");
			strQuery.append("	LEFT JOIN FETCH ica.negotiationMechanism nm");
			strQuery.append("	LEFT JOIN FETCH ica.modalityGroup mg");
			strQuery.append("	WHERE 1=1 ");	
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getIdBankInstitutionPk()))
			{
				strQuery.append(" AND  pb.idBankPk = :idBankInstitutionPk");
				strQuery.append(" AND  b.idBankPk = :idBankInstitutionPk");
				parameters.put("idBankInstitutionPk", objSearchCashDetailTO.getIdBankInstitutionPk());
			}
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getIdParticipantPk()))
			{
				strQuery.append(" AND  pa.idParticipantPk = :participant");
				parameters.put("participant", objSearchCashDetailTO.getIdParticipantPk());
			}
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getIdIssuerPk()))
			{
				strQuery.append(" AND  isu.idIssuerPk = :issuer");
				parameters.put("issuer", objSearchCashDetailTO.getIdIssuerPk());
			}
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getIdBankPk()))
			{
				strQuery.append(" AND  pb.idBankPk = :bank");
				parameters.put("bank", objSearchCashDetailTO.getIdBankPk());
			}
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getCurrency()))
			{
				strQuery.append(" AND  iba.currency = :currency");
				parameters.put("currency", objSearchCashDetailTO.getCurrency());
			}
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getAccountNumber()))
			{
				strQuery.append(" AND  iba.accountNumber = :accountNumber");
				parameters.put("accountNumber", objSearchCashDetailTO.getAccountNumber());
			}	
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getMechanismPk()))
			{
				strQuery.append(" AND  nm.idNegotiationMechanismPk = :mechanism");
				parameters.put("mechanism", objSearchCashDetailTO.getMechanismPk());
			}
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getModalityGroupPk()))
			{
				strQuery.append(" AND  mg.idModalityGroupPk = :modality");
				parameters.put("modality", objSearchCashDetailTO.getModalityGroupPk());
			}
			if(Validations.validateIsNotNull(objSearchCashDetailTO.getCashAccountType()))
			{
				strQuery.append(" AND  ica.accountType = :accountType");
				parameters.put("accountType", objSearchCashDetailTO.getCashAccountType());
			}
			
			strQuery.append(" AND  ica.accountState = :state");
	//		strQuery.append(" AND  iba.state = :stateBank");
			parameters.put("state", CashAccountStateType.ACTIVATE.getCode());
		//	parameters.put("stateBank",  BankStateType.REGISTERED.getCode());
			
			List<InstitutionCashAccount> lstInstitutionCashAccount =  findListByQueryString(strQuery.toString(), parameters);
			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionCashAccount))
			{
				return (InstitutionCashAccount)lstInstitutionCashAccount.get(0);
			}
			else
				return null;
		} catch (NoResultException e) {
			return null;
		}
	}
}
