package com.pradera.funds.effective.accounts.view;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BenefitPaymentAccount.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class BenefitPaymentAccount implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst issuer. */
	private List<Issuer> lstIssuer;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The currency selected. */
	private Integer currencySelected;
	
	/** The lst type account. */
	private List<ParameterTable> lstTypeAccount;
	
	/** The type account selected. */
	private Integer typeAccountSelected;
	
	/** The ind list bank. */
	private int indListBank;
	
	/** The lst bank. */
	private List<Bank> lstBank;
	
	/** The bank selected. */
	private Integer bankSelected;
	
	/** The bic code. */
	private String bicCode;
	
	/** The bank account. */
	private String bankAccount;
	
	/** The state. */
	private String state;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The institution bank account. */
	private InstitutionBankAccount institutionBankAccount;


	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}
	
	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the type account selected.
	 *
	 * @return the type account selected
	 */
	public Integer getTypeAccountSelected() {
		return typeAccountSelected;
	}
	
	/**
	 * Sets the type account selected.
	 *
	 * @param typeAccountSelected the new type account selected
	 */
	public void setTypeAccountSelected(Integer typeAccountSelected) {
		this.typeAccountSelected = typeAccountSelected;
	}
	
	/**
	 * Gets the ind list bank.
	 *
	 * @return the ind list bank
	 */
	public int getIndListBank() {
		return indListBank;
	}
	
	/**
	 * Sets the ind list bank.
	 *
	 * @param indListBank the new ind list bank
	 */
	public void setIndListBank(int indListBank) {
		this.indListBank = indListBank;
	}

	/**
	 * Gets the bank selected.
	 *
	 * @return the bank selected
	 */
	public Integer getBankSelected() {
		return bankSelected;
	}
	
	/**
	 * Sets the bank selected.
	 *
	 * @param bankSelected the new bank selected
	 */
	public void setBankSelected(Integer bankSelected) {
		this.bankSelected = bankSelected;
	}
	
	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}
	
	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}
	
	/**
	 * Gets the bank account.
	 *
	 * @return the bank account
	 */
	public String getBankAccount() {
		return bankAccount;
	}
	
	/**
	 * Sets the bank account.
	 *
	 * @param bankAccount the new bank account
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	
	/**
	 * Gets the lst issuer.
	 *
	 * @return the lst issuer
	 */
	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}
	
	/**
	 * Sets the lst issuer.
	 *
	 * @param lstIssuer the new lst issuer
	 */
	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 */
	public List<Bank> getLstBank() {
		return lstBank;
	}
	
	/**
	 * Sets the lst bank.
	 *
	 * @param lstBank the new lst bank
	 */
	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}
	
	/**
	 * Gets the lst type account.
	 *
	 * @return the lst type account
	 */
	public List<ParameterTable> getLstTypeAccount() {
		return lstTypeAccount;
	}
	
	/**
	 * Sets the lst type account.
	 *
	 * @param lstTypeAccount the new lst type account
	 */
	public void setLstTypeAccount(List<ParameterTable> lstTypeAccount) {
		this.lstTypeAccount = lstTypeAccount;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the institution bank account.
	 *
	 * @return the institution bank account
	 */
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}
	
	/**
	 * Sets the institution bank account.
	 *
	 * @param institutionBankAccount the new institution bank account
	 */
	public void setInstitutionBankAccount(
			InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
}