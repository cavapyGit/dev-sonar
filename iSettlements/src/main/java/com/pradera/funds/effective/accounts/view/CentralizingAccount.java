package com.pradera.funds.effective.accounts.view;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CentralizingAccount.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class CentralizingAccount implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The institution. */
	private String institution;
	
	/** The bank. */
	private String bank;
	
	/** The lst bank account type. */
	private List<ParameterTable> lstBankAccountType;
	
	/** The bank account type. */
	private Integer bankAccountType;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The currency selected. */
	private Integer currencySelected;
	
	/** The bank account. */
	private String bankAccount;
	
	/** The bic code. */
	private String bicCode;
	
	/** The state. */
	private String state;
	
	/** The bank account pk. */
	private Long bankAccountPK;
	
	/** The lst bank accounts. */
	List<InstitutionBankAccount> lstBankAccounts;
	
	/** The lst bank accounts to data model. */
	private GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel;
	
	/** The institution bank account session. */
	private InstitutionBankAccount institutionBankAccountSession;

	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public String getInstitution() {
		return institution;
	}
	
	/**
	 * Sets the institution.
	 *
	 * @param institution the new institution
	 */
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	
	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public String getBank() {
		return bank;
	}
	
	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}
	
	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}
	
	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}
	
	/**
	 * Gets the bank account.
	 *
	 * @return the bank account
	 */
	public String getBankAccount() {
		return bankAccount;
	}
	
	/**
	 * Sets the bank account.
	 *
	 * @param bankAccount the new bank account
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	
	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}
	
	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}
	
	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return bankAccountType;
	}
	
	/**
	 * Gets the bank account pk.
	 *
	 * @return the bank account pk
	 */
	public Long getBankAccountPK() {
		return bankAccountPK;
	}
	
	/**
	 * Sets the bank account pk.
	 *
	 * @param bankAccountPK the new bank account pk
	 */
	public void setBankAccountPK(Long bankAccountPK) {
		this.bankAccountPK = bankAccountPK;
	}
	
	/**
	 * Gets the lst bank account type.
	 *
	 * @return the lst bank account type
	 */
	public List<ParameterTable> getLstBankAccountType() {
		return lstBankAccountType;
	}
	
	/**
	 * Sets the lst bank account type.
	 *
	 * @param lstBankAccountType the new lst bank account type
	 */
	public void setLstBankAccountType(List<ParameterTable> lstBankAccountType) {
		this.lstBankAccountType = lstBankAccountType;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the lst bank accounts.
	 *
	 * @return the lst bank accounts
	 */
	public List<InstitutionBankAccount> getLstBankAccounts() {
		return lstBankAccounts;
	}
	
	/**
	 * Sets the lst bank accounts.
	 *
	 * @param lstBankAccounts the new lst bank accounts
	 */
	public void setLstBankAccounts(List<InstitutionBankAccount> lstBankAccounts) {
		this.lstBankAccounts = lstBankAccounts;
	}
	
	/**
	 * Gets the lst bank accounts to data model.
	 *
	 * @return the lst bank accounts to data model
	 */
	public GenericDataModel<InstitutionBankAccount> getLstBankAccountsTODataModel() {
		return lstBankAccountsTODataModel;
	}
	
	/**
	 * Sets the lst bank accounts to data model.
	 *
	 * @param lstBankAccountsTODataModel the new lst bank accounts to data model
	 */
	public void setLstBankAccountsTODataModel(
			GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel) {
		this.lstBankAccountsTODataModel = lstBankAccountsTODataModel;
	}
	
	/**
	 * Gets the institution bank account session.
	 *
	 * @return the institution bank account session
	 */
	public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}
	
	/**
	 * Sets the institution bank account session.
	 *
	 * @param institutionBankAccountSession the new institution bank account session
	 */
	public void setInstitutionBankAccountSession(
			InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}
}