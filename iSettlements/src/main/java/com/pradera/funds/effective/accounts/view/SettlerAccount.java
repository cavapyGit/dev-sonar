package com.pradera.funds.effective.accounts.view;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.funds.effective.accounts.to.BankAccounts;
import com.pradera.funds.effective.accounts.to.BankAccountsDataModel;
import com.pradera.funds.effective.accounts.to.ModalityGroupDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlerAccount.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlerAccount implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The participant selected. */
	private Integer participantSelected;
	
	/** The bic code. */
	private String bicCode;
	
	/** The lst mechanism. */
	private List<NegotiationMechanism> lstMechanism;
	
	/** The lst modality group. */
	private List<ModalityGroup> lstModalityGroup;
	
	/** The mechanism. */
	private Long mechanism;		
	
	/** The modality group. */
	private Long modalityGroup;	
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The currency selected. */
	private Integer currencySelected;	
	
	/** The lst bcb. */
	private ModalityGroupDataModel lstBolsa, lstHacienda, lstOTC, lstBCB;
	
	/** The moda group bcb. */
	private ModalityGroup[] modaGroupBolsa, modaGroupHacienda, modaGroupOTC, modaGroupBCB;	
	
	/** The use type. */
	private Integer useType;
	
	/** The lst bank account class. */
	private List<ParameterTable> lstBankAccountClass;
	
	/** The lst type account. */
	private List<ParameterTable> lstTypeAccount;
	
	/** The bank account class selected. */
	private Integer bankAccountClassSelected;
	
	/** The type account selected. */
	private Integer typeAccountSelected;
	
	/** The use type options. */
	private List<ParameterTable> useTypeOptions;
	
	/** The ind other bank. */
	private int indOtherBank;
	
	/** The lst other bank. */
	private List<Bank> lstOtherBank;
	
	/** The lst bank. */
	private List<Bank> lstBank;

	/** The other bank selected. */
	private Integer otherBankSelected;
	
	/** The bank selected. */
	private Integer bankSelected;

	/** The bic code other bank. */
	private String bicCodeOtherBank;
	
	/** The lst bank account type. */
	private List<ParameterTable> lstBankAccountType;
	
	/** The bank account type owner. */
	private Integer bankAccountTypeOwner;
	
	/** The bank account type third. */
	private Long bankAccountTypeThird;
	
	/** The bank account type. */
	private String bankAccountType;
	
	/** The bank account currecy. */
	private String bankAccountCurrecy;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The id insitution bank account pk. */
	private Long idInsitutionBankAccountPk;

	/** The ind bank accounts. */
	private int indBankAccounts;
	
	/** The bank accounts. */
	private BankAccounts bankAccounts;
	
	/** The lst bank accounts. */
	private List<BankAccounts> lstBankAccounts;
	
	/** The bank iterate. */
	private BankAccounts bankIterate;
	
	/** The bank account. */
	private BankAccounts bankAccount;
	
	/** The bank account data model. */
	private BankAccountsDataModel bankAccountDataModel;
	
	/** The lst bank accounts to data model. */
	private GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel;
	
	/** The institution bank account session. */
	private InstitutionBankAccount institutionBankAccountSession;
	
	/** The institution bank account. */
	private InstitutionBankAccount institutionBankAccount;

	// The below variables intShowMechanism,intShowModality are used to show the
	/** The int show mechanism. */
	// disabed combobox values in the modification screens.
	private Integer intShowMechanism;
	
	/** The int show modality. */
	private Integer intShowModality;

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Integer getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Integer participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}
	
	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}


	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}

	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}


	/**
	 * Gets the bank account class selected.
	 *
	 * @return the bank account class selected
	 */
	public Integer getBankAccountClassSelected() {
		return bankAccountClassSelected;
	}

	/**
	 * Sets the bank account class selected.
	 *
	 * @param bankAccountClassSelected the new bank account class selected
	 */
	public void setBankAccountClassSelected(Integer bankAccountClassSelected) {
		this.bankAccountClassSelected = bankAccountClassSelected;
	}

	/**
	 * Gets the ind other bank.
	 *
	 * @return the ind other bank
	 */
	public int getIndOtherBank() {
		return indOtherBank;
	}

	/**
	 * Sets the ind other bank.
	 *
	 * @param indOtherBank the new ind other bank
	 */
	public void setIndOtherBank(int indOtherBank) {
		this.indOtherBank = indOtherBank;
	}

	/**
	 * Gets the other bank selected.
	 *
	 * @return the other bank selected
	 */
	public Integer getOtherBankSelected() {
		return otherBankSelected;
	}

	/**
	 * Sets the other bank selected.
	 *
	 * @param otherBankSelected the new other bank selected
	 */
	public void setOtherBankSelected(Integer otherBankSelected) {
		this.otherBankSelected = otherBankSelected;
	}

	/**
	 * Gets the bic code other bank.
	 *
	 * @return the bic code other bank
	 */
	public String getBicCodeOtherBank() {
		return bicCodeOtherBank;
	}

	/**
	 * Sets the bic code other bank.
	 *
	 * @param bicCodeOtherBank the new bic code other bank
	 */
	public void setBicCodeOtherBank(String bicCodeOtherBank) {
		this.bicCodeOtherBank = bicCodeOtherBank;
	}

	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public String getBankAccountType() {
		return bankAccountType;
	}

	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(String bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	/**
	 * Gets the bank account currecy.
	 *
	 * @return the bank account currecy
	 */
	public String getBankAccountCurrecy() {
		return bankAccountCurrecy;
	}

	/**
	 * Sets the bank account currecy.
	 *
	 * @param bankAccountCurrecy the new bank account currecy
	 */
	public void setBankAccountCurrecy(String bankAccountCurrecy) {
		this.bankAccountCurrecy = bankAccountCurrecy;
	}

	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	/**
	 * Gets the ind bank accounts.
	 *
	 * @return the ind bank accounts
	 */
	public int getIndBankAccounts() {
		return indBankAccounts;
	}

	/**
	 * Sets the ind bank accounts.
	 *
	 * @param indBankAccounts the new ind bank accounts
	 */
	public void setIndBankAccounts(int indBankAccounts) {
		this.indBankAccounts = indBankAccounts;
	}

	/**
	 * Gets the lst bank accounts.
	 *
	 * @return the lst bank accounts
	 */
	public List<BankAccounts> getLstBankAccounts() {
		return lstBankAccounts;
	}

	/**
	 * Sets the lst bank accounts.
	 *
	 * @param lstBankAccounts the new lst bank accounts
	 */
	public void setLstBankAccounts(List<BankAccounts> lstBankAccounts) {
		this.lstBankAccounts = lstBankAccounts;
	}

	/**
	 * Gets the bank accounts.
	 *
	 * @return the bank accounts
	 */
	public BankAccounts getBankAccounts() {
		return bankAccounts;
	}

	/**
	 * Sets the bank accounts.
	 *
	 * @param bankAccounts the new bank accounts
	 */
	public void setBankAccounts(BankAccounts bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	/**
	 * Gets the bank account.
	 *
	 * @return the bank account
	 */
	public BankAccounts getBankAccount() {
		return bankAccount;
	}

	/**
	 * Sets the bank account.
	 *
	 * @param bankAccount the new bank account
	 */
	public void setBankAccount(BankAccounts bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 * Gets the bank account data model.
	 *
	 * @return the bank account data model
	 */
	public BankAccountsDataModel getBankAccountDataModel() {
		return bankAccountDataModel;
	}

	/**
	 * Sets the bank account data model.
	 *
	 * @param bankAccountDataModel the new bank account data model
	 */
	public void setBankAccountDataModel(
			BankAccountsDataModel bankAccountDataModel) {
		this.bankAccountDataModel = bankAccountDataModel;
	}
	
	/**
	 * Gets the bank iterate.
	 *
	 * @return the bank iterate
	 */
	public BankAccounts getBankIterate() {
		return bankIterate;
	}

	/**
	 * Sets the bank iterate.
	 *
	 * @param bankIterate the new bank iterate
	 */
	public void setBankIterate(BankAccounts bankIterate) {
		this.bankIterate = bankIterate;
	}

	/**
	 * Gets the bank account type owner.
	 *
	 * @return the bank account type owner
	 */
	public Integer getBankAccountTypeOwner() {
		return bankAccountTypeOwner;
	}

	/**
	 * Sets the bank account type owner.
	 *
	 * @param bankAccountTypeOwner the new bank account type owner
	 */
	public void setBankAccountTypeOwner(Integer bankAccountTypeOwner) {
		this.bankAccountTypeOwner = bankAccountTypeOwner;
	}

	/**
	 * Gets the bank account type third.
	 *
	 * @return the bank account type third
	 */
	public Long getBankAccountTypeThird() {
		return bankAccountTypeThird;
	}

	/**
	 * Sets the bank account type third.
	 *
	 * @param bankAccountTypeThird the new bank account type third
	 */
	public void setBankAccountTypeThird(Long bankAccountTypeThird) {
		this.bankAccountTypeThird = bankAccountTypeThird;
	}

	/**
	 * Gets the use type.
	 *
	 * @return the use type
	 */
	public Integer getUseType() {
		return useType;
	}

	/**
	 * Sets the use type.
	 *
	 * @param useType the new use type
	 */
	public void setUseType(Integer useType) {
		this.useType = useType;
	}

	/**
	 * Gets the int show mechanism.
	 *
	 * @return the intShowMechanism
	 */
	public Integer getIntShowMechanism() {
		return intShowMechanism;
	}

	/**
	 * Sets the int show mechanism.
	 *
	 * @param intShowMechanism            the intShowMechanism to set
	 */
	public void setIntShowMechanism(Integer intShowMechanism) {
		this.intShowMechanism = intShowMechanism;
	}

	/**
	 * Gets the int show modality.
	 *
	 * @return the intShowModality
	 */
	public Integer getIntShowModality() {
		return intShowModality;
	}

	/**
	 * Sets the int show modality.
	 *
	 * @param intShowModality            the intShowModality to set
	 */
	public void setIntShowModality(Integer intShowModality) {
		this.intShowModality = intShowModality;
	}

	/**
	 * Gets the lst bolsa.
	 *
	 * @return the lst bolsa
	 */
	public ModalityGroupDataModel getLstBolsa() {
		return lstBolsa;
	}

	/**
	 * Sets the lst bolsa.
	 *
	 * @param lstBolsa the new lst bolsa
	 */
	public void setLstBolsa(ModalityGroupDataModel lstBolsa) {
		this.lstBolsa = lstBolsa;
	}

	/**
	 * Gets the lst hacienda.
	 *
	 * @return the lst hacienda
	 */
	public ModalityGroupDataModel getLstHacienda() {
		return lstHacienda;
	}

	/**
	 * Sets the lst hacienda.
	 *
	 * @param lstHacienda the new lst hacienda
	 */
	public void setLstHacienda(ModalityGroupDataModel lstHacienda) {
		this.lstHacienda = lstHacienda;
	}

	/**
	 * Gets the lst otc.
	 *
	 * @return the lst otc
	 */
	public ModalityGroupDataModel getLstOTC() {
		return lstOTC;
	}

	/**
	 * Sets the lst otc.
	 *
	 * @param lstOTC the new lst otc
	 */
	public void setLstOTC(ModalityGroupDataModel lstOTC) {
		this.lstOTC = lstOTC;
	}

	/**
	 * Gets the lst bcb.
	 *
	 * @return the lst bcb
	 */
	public ModalityGroupDataModel getLstBCB() {
		return lstBCB;
	}

	/**
	 * Sets the lst bcb.
	 *
	 * @param lstBCB the new lst bcb
	 */
	public void setLstBCB(ModalityGroupDataModel lstBCB) {
		this.lstBCB = lstBCB;
	}

	/**
	 * Gets the moda group bolsa.
	 *
	 * @return the moda group bolsa
	 */
	public ModalityGroup[] getModaGroupBolsa() {
		return modaGroupBolsa;
	}

	/**
	 * Sets the moda group bolsa.
	 *
	 * @param modaGroupBolsa the new moda group bolsa
	 */
	public void setModaGroupBolsa(ModalityGroup[] modaGroupBolsa) {
		this.modaGroupBolsa = modaGroupBolsa;
	}

	/**
	 * Gets the moda group hacienda.
	 *
	 * @return the moda group hacienda
	 */
	public ModalityGroup[] getModaGroupHacienda() {
		return modaGroupHacienda;
	}

	/**
	 * Sets the moda group hacienda.
	 *
	 * @param modaGroupHacienda the new moda group hacienda
	 */
	public void setModaGroupHacienda(ModalityGroup[] modaGroupHacienda) {
		this.modaGroupHacienda = modaGroupHacienda;
	}

	/**
	 * Gets the moda group otc.
	 *
	 * @return the moda group otc
	 */
	public ModalityGroup[] getModaGroupOTC() {
		return modaGroupOTC;
	}

	/**
	 * Sets the moda group otc.
	 *
	 * @param modaGroupOTC the new moda group otc
	 */
	public void setModaGroupOTC(ModalityGroup[] modaGroupOTC) {
		this.modaGroupOTC = modaGroupOTC;
	}


	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}

	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}

	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}

	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}

	/**
	 * Gets the mechanism.
	 *
	 * @return the mechanism
	 */
	public Long getMechanism() {
		return mechanism;
	}

	/**
	 * Sets the mechanism.
	 *
	 * @param mechanism the new mechanism
	 */
	public void setMechanism(Long mechanism) {
		this.mechanism = mechanism;
	}

	/**
	 * Gets the modality group.
	 *
	 * @return the modality group
	 */
	public Long getModalityGroup() {
		return modalityGroup;
	}

	/**
	 * Sets the modality group.
	 *
	 * @param modalityGroup the new modality group
	 */
	public void setModalityGroup(Long modalityGroup) {
		this.modalityGroup = modalityGroup;
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst bank account class.
	 *
	 * @return the lst bank account class
	 */
	public List<ParameterTable> getLstBankAccountClass() {
		return lstBankAccountClass;
	}

	/**
	 * Sets the lst bank account class.
	 *
	 * @param lstBankAccountClass the new lst bank account class
	 */
	public void setLstBankAccountClass(List<ParameterTable> lstBankAccountClass) {
		this.lstBankAccountClass = lstBankAccountClass;
	}

	/**
	 * Gets the use type options.
	 *
	 * @return the use type options
	 */
	public List<ParameterTable> getUseTypeOptions() {
		return useTypeOptions;
	}

	/**
	 * Sets the use type options.
	 *
	 * @param useTypeOptions the new use type options
	 */
	public void setUseTypeOptions(List<ParameterTable> useTypeOptions) {
		this.useTypeOptions = useTypeOptions;
	}

	/**
	 * Gets the lst other bank.
	 *
	 * @return the lst other bank
	 */
	public List<Bank> getLstOtherBank() {
		return lstOtherBank;
	}

	/**
	 * Sets the lst other bank.
	 *
	 * @param lstOtherBank the new lst other bank
	 */
	public void setLstOtherBank(List<Bank> lstOtherBank) {
		this.lstOtherBank = lstOtherBank;
	}

	/**
	 * Gets the lst bank account type.
	 *
	 * @return the lst bank account type
	 */
	public List<ParameterTable> getLstBankAccountType() {
		return lstBankAccountType;
	}

	/**
	 * Sets the lst bank account type.
	 *
	 * @param lstBankAccountType the new lst bank account type
	 */
	public void setLstBankAccountType(List<ParameterTable> lstBankAccountType) {
		this.lstBankAccountType = lstBankAccountType;
	}

	/**
	 * Gets the moda group bcb.
	 *
	 * @return the moda group bcb
	 */
	public ModalityGroup[] getModaGroupBCB() {
		return modaGroupBCB;
	}

	/**
	 * Sets the moda group bcb.
	 *
	 * @param modaGroupBCB the new moda group bcb
	 */
	public void setModaGroupBCB(ModalityGroup[] modaGroupBCB) {
		this.modaGroupBCB = modaGroupBCB;
	}

	/**
	 * Gets the lst bank accounts to data model.
	 *
	 * @return the lst bank accounts to data model
	 */
	public GenericDataModel<InstitutionBankAccount> getLstBankAccountsTODataModel() {
		return lstBankAccountsTODataModel;
	}

	/**
	 * Sets the lst bank accounts to data model.
	 *
	 * @param lstBankAccountsTODataModel the new lst bank accounts to data model
	 */
	public void setLstBankAccountsTODataModel(
			GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel) {
		this.lstBankAccountsTODataModel = lstBankAccountsTODataModel;
	}

	/**
	 * Gets the institution bank account session.
	 *
	 * @return the institution bank account session
	 */
	public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}

	/**
	 * Sets the institution bank account session.
	 *
	 * @param institutionBankAccountSession the new institution bank account session
	 */
	public void setInstitutionBankAccountSession(
			InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}

	/**
	 * Gets the id insitution bank account pk.
	 *
	 * @return the id insitution bank account pk
	 */
	public Long getIdInsitutionBankAccountPk() {
		return idInsitutionBankAccountPk;
	}

	/**
	 * Sets the id insitution bank account pk.
	 *
	 * @param idInsitutionBankAccountPk the new id insitution bank account pk
	 */
	public void setIdInsitutionBankAccountPk(Long idInsitutionBankAccountPk) {
		this.idInsitutionBankAccountPk = idInsitutionBankAccountPk;
	}
	
	public Integer getTypeAccountSelected() {
		return typeAccountSelected;
	}

	public void setTypeAccountSelected(Integer typeAccountSelected) {
		this.typeAccountSelected = typeAccountSelected;
	}
	
	public List<ParameterTable> getLstTypeAccount() {
		return lstTypeAccount;
	}

	public void setLstTypeAccount(List<ParameterTable> lstTypeAccount) {
		this.lstTypeAccount = lstTypeAccount;
	}
	
	public List<Bank> getLstBank() {
		return lstBank;
	}

	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}
	
	public Integer getBankSelected() {
		return bankSelected;
	}

	public void setBankSelected(Integer bankSelected) {
		this.bankSelected = bankSelected;
	}
	
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}

	public void setInstitutionBankAccount(InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
	
}