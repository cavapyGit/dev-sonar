package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchCashDetailTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchCashDetailTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id issuer pk. */
	private Long idBankInstitutionPk;
	
	/** The id participant pk. */
	private String idIssuerPk;
	
	/** The id Bank Institution. */
	private Long idParticipantPk;
	
	/** The currency. */
	private Integer currency;
	
	/** The account number. */
	private String accountNumber;
	
	/** The id bank pk. */
	private Long idBankPk;
	
	/** The bank acount type. */
	private Long bankAcountType;;
	
	/** The mechanism pk. */
	private Long mechanismPk;
	
	/** The modality group pk. */
	private Long modalityGroupPk;
	
	/** The cash account type. */
	private Integer cashAccountType;
	
	/**
	 * Instantiates a new search cash detail to.
	 */
	public SearchCashDetailTO(){
		
	}

	/**
	 * Gets the id bank institution pk.
	 *
	 * @return the id bank institution pk
	 */
	public Long getIdBankInstitutionPk() {
		return idBankInstitutionPk;
	}

	/**
	 * Sets the id bank institution pk.
	 *
	 * @param idBankInstitutionPk the new id bank institution pk
	 */
	public void setIdBankInstitutionPk(Long idBankInstitutionPk) {
		this.idBankInstitutionPk = idBankInstitutionPk;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the id bank pk.
	 *
	 * @return the id bank pk
	 */
	public Long getIdBankPk() {
		return idBankPk;
	}

	/**
	 * Sets the id bank pk.
	 *
	 * @param idBankPk the new id bank pk
	 */
	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}

	/**
	 * Gets the bank acount type.
	 *
	 * @return the bank acount type
	 */
	public Long getBankAcountType() {
		return bankAcountType;
	}

	/**
	 * Sets the bank acount type.
	 *
	 * @param bankAcountType the new bank acount type
	 */
	public void setBankAcountType(Long bankAcountType) {
		this.bankAcountType = bankAcountType;
	}

	/**
	 * Gets the mechanism pk.
	 *
	 * @return the mechanism pk
	 */
	public Long getMechanismPk() {
		return mechanismPk;
	}

	/**
	 * Sets the mechanism pk.
	 *
	 * @param mechanismPk the new mechanism pk
	 */
	public void setMechanismPk(Long mechanismPk) {
		this.mechanismPk = mechanismPk;
	}

	/**
	 * Gets the modality group pk.
	 *
	 * @return the modality group pk
	 */
	public Long getModalityGroupPk() {
		return modalityGroupPk;
	}

	/**
	 * Sets the modality group pk.
	 *
	 * @param modalityGroupPk the new modality group pk
	 */
	public void setModalityGroupPk(Long modalityGroupPk) {
		this.modalityGroupPk = modalityGroupPk;
	}

	/**
	 * Gets the cash account type.
	 *
	 * @return the cash account type
	 */
	public Integer getCashAccountType() {
		return cashAccountType;
	}

	/**
	 * Sets the cash account type.
	 *
	 * @param cashAccountType the new cash account type
	 */
	public void setCashAccountType(Integer cashAccountType) {
		this.cashAccountType = cashAccountType;
	}

	
	
	
}
