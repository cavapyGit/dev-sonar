package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.negotiation.ModalityGroup;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ModalityGroupDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class ModalityGroupDataModel extends ListDataModel<ModalityGroup> implements SelectableDataModel<ModalityGroup>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Instantiates a new modality group data model.
	 */
	public ModalityGroupDataModel(){
		
	}
	
	/**
	 * Instantiates a new modality group data model.
	 *
	 * @param data the data
	 */
	public ModalityGroupDataModel(List<ModalityGroup> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ModalityGroup getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<ModalityGroup> parametros=(List<ModalityGroup>)getWrappedData();
        for(ModalityGroup parameter : parametros) {        	
            if(parameter.getIdModalityGroupPk().toString().equals(rowKey))
                return parameter;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(ModalityGroup parameter) {
		return parameter.getIdModalityGroupPk();
	}	
}
