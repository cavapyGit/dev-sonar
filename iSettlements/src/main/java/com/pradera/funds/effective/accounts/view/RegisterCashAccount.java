package com.pradera.funds.effective.accounts.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterCashAccount.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterCashAccount implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst cash account type. */
	private List<ParameterTable> lstCashAccountType;
	
	/** The cash account type selected. */
	private Integer cashAccountTypeSelected;
	
	/** The id institute bank. */
	private Long idInstituteBank;
	
	/** The id institute cash. */
	private Long idInstituteCash;
	
	/** The account state. */
	private Integer accountState;
	
	/** The ind taxes account. */
	private int indTaxesAccount;
	
	/** The taxes account. */
	private TaxesAccount taxesAccount;
	
	/** The ind rates account. */
	private int indRatesAccount;
	
	/** The rates account. */
	private RatesAccount ratesAccount;
	
	/** The ind international operations account. */
	private int indInternationalOperationsAccount;
	
	/** The international operations account. */
	private InternationalOperationsAccount internationalOperationsAccount;
	
	/** The ind magin guarantee account. */
	private int indMaginGuaranteeAccount;
	
	/** The margin guarantee account. */
	private MarginGuaranteeAccount marginGuaranteeAccount;
	
	/** The ind return account. */
	private int indReturnAccount;
	
	/** The return account. */
	private ReturnAccount returnAccount;
	
	/** The ind benefit payment account. */
	private int indBenefitPaymentAccount;
	
	/** The benefit payment account. */
	private BenefitPaymentAccount benefitPaymentAccount;
	
	/** The ind settler account. */
	private int indSettlerAccount;
	
	/** The settler account. */
	private SettlerAccount settlerAccount;
	
	/** The ind centralizing account. */
	private int indCentralizingAccount;
	
	/** The centralizing account. */
	private CentralizingAccount centralizingAccount;
	
	/** The hidden entity. */
	private String hiddenEntity;
	
	/** The ind special payment account. */
	private int indSpecialPaymentAccount;
	
	/** The register user. */
	private String registerUser;
	
	/** The register date. */
	private Date registerDate;
	
	/** The last modified u ser. */
	private String lastModifiedUSer;
	
	/** The last modified ip. */
	private String lastModifiedIp;
	
	/** The last modified date. */
	private Date lastModifiedDate;
	
	/** The last modified app. */
	private Long lastModifiedApp;

	/**
	 * Instantiates a new register cash account.
	 */
	public RegisterCashAccount() {
		centralizingAccount = new CentralizingAccount();
		marginGuaranteeAccount = new MarginGuaranteeAccount();
		ratesAccount = new RatesAccount();
		internationalOperationsAccount = new InternationalOperationsAccount();
		returnAccount = new ReturnAccount();
		taxesAccount = new TaxesAccount();
		benefitPaymentAccount = new BenefitPaymentAccount();
		settlerAccount = new SettlerAccount();
	}

	/**
	 * Gets the id institute bank.
	 *
	 * @return the idInstituteBank
	 */
	public Long getIdInstituteBank() {
		return idInstituteBank;
	}

	/**
	 * Sets the id institute bank.
	 *
	 * @param idInstituteBank            the idInstituteBank to set
	 */
	public void setIdInstituteBank(Long idInstituteBank) {
		this.idInstituteBank = idInstituteBank;
	}

//	public List<BeanType> getLstCashAccountType() {
//		return lstCashAccountType;
//	}
//
//	public void setLstCashAccountType(List<BeanType> lstCashAccountType) {
//		this.lstCashAccountType = lstCashAccountType;
//	}

	/**
 * Gets the cash account type selected.
 *
 * @return the cash account type selected
 */
public Integer getCashAccountTypeSelected() {
		return cashAccountTypeSelected;
	}

	/**
	 * Sets the cash account type selected.
	 *
	 * @param cashAccountTypeSelected the new cash account type selected
	 */
	public void setCashAccountTypeSelected(Integer cashAccountTypeSelected) {
		this.cashAccountTypeSelected = cashAccountTypeSelected;
	}

	/**
	 * Gets the ind taxes account.
	 *
	 * @return the ind taxes account
	 */
	public int getIndTaxesAccount() {
		return indTaxesAccount;
	}

	/**
	 * Sets the ind taxes account.
	 *
	 * @param indTaxesAccount the new ind taxes account
	 */
	public void setIndTaxesAccount(int indTaxesAccount) {
		this.indTaxesAccount = indTaxesAccount;
	}

	/**
	 * Gets the taxes account.
	 *
	 * @return the taxes account
	 */
	public TaxesAccount getTaxesAccount() {
		return taxesAccount;
	}

	/**
	 * Sets the taxes account.
	 *
	 * @param taxesAccount the new taxes account
	 */
	public void setTaxesAccount(TaxesAccount taxesAccount) {
		this.taxesAccount = taxesAccount;
	}

	/**
	 * Gets the ind rates account.
	 *
	 * @return the ind rates account
	 */
	public int getIndRatesAccount() {
		return indRatesAccount;
	}

	/**
	 * Sets the ind rates account.
	 *
	 * @param indRatesAccount the new ind rates account
	 */
	public void setIndRatesAccount(int indRatesAccount) {
		this.indRatesAccount = indRatesAccount;
	}

	/**
	 * Gets the rates account.
	 *
	 * @return the rates account
	 */
	public RatesAccount getRatesAccount() {
		return ratesAccount;
	}

	/**
	 * Sets the rates account.
	 *
	 * @param ratesAccount the new rates account
	 */
	public void setRatesAccount(RatesAccount ratesAccount) {
		this.ratesAccount = ratesAccount;
	}

	/**
	 * Gets the ind international operations account.
	 *
	 * @return the ind international operations account
	 */
	public int getIndInternationalOperationsAccount() {
		return indInternationalOperationsAccount;
	}

	/**
	 * Sets the ind international operations account.
	 *
	 * @param indInternationalOperationsAccount the new ind international operations account
	 */
	public void setIndInternationalOperationsAccount(
			int indInternationalOperationsAccount) {
		this.indInternationalOperationsAccount = indInternationalOperationsAccount;
	}

	/**
	 * Gets the international operations account.
	 *
	 * @return the international operations account
	 */
	public InternationalOperationsAccount getInternationalOperationsAccount() {
		return internationalOperationsAccount;
	}

	/**
	 * Sets the international operations account.
	 *
	 * @param internationalOperationsAccount the new international operations account
	 */
	public void setInternationalOperationsAccount(
			InternationalOperationsAccount internationalOperationsAccount) {
		this.internationalOperationsAccount = internationalOperationsAccount;
	}

	/**
	 * Gets the ind magin guarantee account.
	 *
	 * @return the ind magin guarantee account
	 */
	public int getIndMaginGuaranteeAccount() {
		return indMaginGuaranteeAccount;
	}

	/**
	 * Sets the ind magin guarantee account.
	 *
	 * @param indMaginGuaranteeAccount the new ind magin guarantee account
	 */
	public void setIndMaginGuaranteeAccount(int indMaginGuaranteeAccount) {
		this.indMaginGuaranteeAccount = indMaginGuaranteeAccount;
	}

	/**
	 * Gets the margin guarantee account.
	 *
	 * @return the margin guarantee account
	 */
	public MarginGuaranteeAccount getMarginGuaranteeAccount() {
		return marginGuaranteeAccount;
	}

	/**
	 * Sets the margin guarantee account.
	 *
	 * @param marginGuaranteeAccount the new margin guarantee account
	 */
	public void setMarginGuaranteeAccount(
			MarginGuaranteeAccount marginGuaranteeAccount) {
		this.marginGuaranteeAccount = marginGuaranteeAccount;
	}

	/**
	 * Gets the ind return account.
	 *
	 * @return the ind return account
	 */
	public int getIndReturnAccount() {
		return indReturnAccount;
	}

	/**
	 * Sets the ind return account.
	 *
	 * @param indReturnAccount the new ind return account
	 */
	public void setIndReturnAccount(int indReturnAccount) {
		this.indReturnAccount = indReturnAccount;
	}

	/**
	 * Gets the return account.
	 *
	 * @return the return account
	 */
	public ReturnAccount getReturnAccount() {
		return returnAccount;
	}

	/**
	 * Sets the return account.
	 *
	 * @param returnAccount the new return account
	 */
	public void setReturnAccount(ReturnAccount returnAccount) {
		this.returnAccount = returnAccount;
	}

	/**
	 * Gets the ind benefit payment account.
	 *
	 * @return the ind benefit payment account
	 */
	public int getIndBenefitPaymentAccount() {
		return indBenefitPaymentAccount;
	}

	/**
	 * Sets the ind benefit payment account.
	 *
	 * @param indBenefitPaymentAccount the new ind benefit payment account
	 */
	public void setIndBenefitPaymentAccount(int indBenefitPaymentAccount) {
		this.indBenefitPaymentAccount = indBenefitPaymentAccount;
	}

	/**
	 * Gets the benefit payment account.
	 *
	 * @return the benefit payment account
	 */
	public BenefitPaymentAccount getBenefitPaymentAccount() {
		return benefitPaymentAccount;
	}

	/**
	 * Sets the benefit payment account.
	 *
	 * @param benefitPaymentAccount the new benefit payment account
	 */
	public void setBenefitPaymentAccount(
			BenefitPaymentAccount benefitPaymentAccount) {
		this.benefitPaymentAccount = benefitPaymentAccount;
	}

	/**
	 * Gets the ind settler account.
	 *
	 * @return the ind settler account
	 */
	public int getIndSettlerAccount() {
		return indSettlerAccount;
	}

	/**
	 * Sets the ind settler account.
	 *
	 * @param indSettlerAccount the new ind settler account
	 */
	public void setIndSettlerAccount(int indSettlerAccount) {
		this.indSettlerAccount = indSettlerAccount;
	}

	/**
	 * Gets the settler account.
	 *
	 * @return the settler account
	 */
	public SettlerAccount getSettlerAccount() {
		return settlerAccount;
	}

	/**
	 * Sets the settler account.
	 *
	 * @param settlerAccount the new settler account
	 */
	public void setSettlerAccount(SettlerAccount settlerAccount) {
		this.settlerAccount = settlerAccount;
	}

	/**
	 * Gets the ind centralizing account.
	 *
	 * @return the ind centralizing account
	 */
	public int getIndCentralizingAccount() {
		return indCentralizingAccount;
	}

	/**
	 * Sets the ind centralizing account.
	 *
	 * @param indCentralizingAccount the new ind centralizing account
	 */
	public void setIndCentralizingAccount(int indCentralizingAccount) {
		this.indCentralizingAccount = indCentralizingAccount;
	}

	/**
	 * Gets the centralizing account.
	 *
	 * @return the centralizing account
	 */
	public CentralizingAccount getCentralizingAccount() {
		return centralizingAccount;
	}

	/**
	 * Sets the centralizing account.
	 *
	 * @param centralizingAccount the new centralizing account
	 */
	public void setCentralizingAccount(CentralizingAccount centralizingAccount) {
		this.centralizingAccount = centralizingAccount;
	}

	/**
	 * Gets the hidden entity.
	 *
	 * @return the hidden entity
	 */
	public String getHiddenEntity() {
		return hiddenEntity;
	}

	/**
	 * Sets the hidden entity.
	 *
	 * @param hiddenEntity the new hidden entity
	 */
	public void setHiddenEntity(String hiddenEntity) {
		this.hiddenEntity = hiddenEntity;
	}

	/**
	 * Gets the id institute cash.
	 *
	 * @return the idInstituteCash
	 */
	public Long getIdInstituteCash() {
		return idInstituteCash;
	}

	/**
	 * Sets the id institute cash.
	 *
	 * @param idInstituteCash            the idInstituteCash to set
	 */
	public void setIdInstituteCash(Long idInstituteCash) {
		this.idInstituteCash = idInstituteCash;
	}

	/**
	 * Gets the register user.
	 *
	 * @return the registerUser
	 */
	public String getRegisterUser() {
		return registerUser;
	}

	/**
	 * Sets the register user.
	 *
	 * @param registerUser the registerUser to set
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the last modified u ser.
	 *
	 * @return the lastModifiedUSer
	 */
	public String getLastModifiedUSer() {
		return lastModifiedUSer;
	}

	/**
	 * Sets the last modified u ser.
	 *
	 * @param lastModifiedUSer the lastModifiedUSer to set
	 */
	public void setLastModifiedUSer(String lastModifiedUSer) {
		this.lastModifiedUSer = lastModifiedUSer;
	}

	/**
	 * Gets the last modified ip.
	 *
	 * @return the lastModifiedIp
	 */
	public String getLastModifiedIp() {
		return lastModifiedIp;
	}

	/**
	 * Sets the last modified ip.
	 *
	 * @param lastModifiedIp the lastModifiedIp to set
	 */
	public void setLastModifiedIp(String lastModifiedIp) {
		this.lastModifiedIp = lastModifiedIp;
	}

	/**
	 * Gets the last modified date.
	 *
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * Sets the last modified date.
	 *
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * Gets the last modified app.
	 *
	 * @return the lastModifiedApp
	 */
	public Long getLastModifiedApp() {
		return lastModifiedApp;
	}

	/**
	 * Sets the last modified app.
	 *
	 * @param lastModifiedApp the lastModifiedApp to set
	 */
	public void setLastModifiedApp(Long lastModifiedApp) {
		this.lastModifiedApp = lastModifiedApp;
	}

	/**
	 * Gets the account state.
	 *
	 * @return the account state
	 */
	public Integer getAccountState() {
		return accountState;
	}

	/**
	 * Sets the account state.
	 *
	 * @param accountState the new account state
	 */
	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}

	/**
	 * Gets the ind special payment account.
	 *
	 * @return the ind special payment account
	 */
	public int getIndSpecialPaymentAccount() {
		return indSpecialPaymentAccount;
	}

	/**
	 * Sets the ind special payment account.
	 *
	 * @param indSpecialPaymentAccount the new ind special payment account
	 */
	public void setIndSpecialPaymentAccount(int indSpecialPaymentAccount) {
		this.indSpecialPaymentAccount = indSpecialPaymentAccount;
	}

	/**
	 * Gets the lst cash account type.
	 *
	 * @return the lst cash account type
	 */
	public List<ParameterTable> getLstCashAccountType() {
		return lstCashAccountType;
	}

	/**
	 * Sets the lst cash account type.
	 *
	 * @param lstCashAccountType the new lst cash account type
	 */
	public void setLstCashAccountType(List<ParameterTable> lstCashAccountType) {
		this.lstCashAccountType = lstCashAccountType;
	}
	
	
}