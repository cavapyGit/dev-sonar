package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BankAccounts.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class BankAccounts implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	private int id;
	
	/** The bank account id. */
	private Long bankAccountId;
	
	/** The id bank account class. */
	private Long idBankAccountClass;
	
	/** The bank account class. */
	private String bankAccountClass;
	
	/** The id bank account type. */
	private Long idBankAccountType;
	
	/** The bank account type. */
	private String bankAccountType;
	
	/** The ind sent. */
	private String indSent;
	
	/** The ind reception. */
	private String indReception;
	
	/** The other bank. */
	private String otherBank;
	
	/** The currency. */
	private String currency;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The id institution bank account pk. */
	private Long idInstitutionBankAccountPk;


	/**
	 * Gets the bank account class.
	 *
	 * @return the bank account class
	 */
	public String getBankAccountClass() {
		return bankAccountClass;
	}
	
	/**
	 * Sets the bank account class.
	 *
	 * @param bankAccountClass the new bank account class
	 */
	public void setBankAccountClass(String bankAccountClass) {
		this.bankAccountClass = bankAccountClass;
	}
	
	/**
	 * Gets the other bank.
	 *
	 * @return the other bank
	 */
	public String getOtherBank() {
		return otherBank;
	}
	
	/**
	 * Sets the other bank.
	 *
	 * @param otherBank the new other bank
	 */
	public void setOtherBank(String otherBank) {
		this.otherBank = otherBank;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	
	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the ind sent.
	 *
	 * @return the ind sent
	 */
	public String getIndSent() {
		return indSent;
	}
	
	/**
	 * Sets the ind sent.
	 *
	 * @param indSent the new ind sent
	 */
	public void setIndSent(String indSent) {
		this.indSent = indSent;
	}
	
	/**
	 * Gets the ind reception.
	 *
	 * @return the ind reception
	 */
	public String getIndReception() {
		return indReception;
	}
	
	/**
	 * Sets the ind reception.
	 *
	 * @param indReception the new ind reception
	 */
	public void setIndReception(String indReception) {
		this.indReception = indReception;
	}
	
	/**
	 * Gets the bank account id.
	 *
	 * @return the bank account id
	 */
	public Long getBankAccountId() {
		return bankAccountId;
	}
	
	/**
	 * Sets the bank account id.
	 *
	 * @param bankAccountId the new bank account id
	 */
	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	
	/**
	 * Gets the id bank account type.
	 *
	 * @return the id bank account type
	 */
	public Long getIdBankAccountType() {
		return idBankAccountType;
	}
	
	/**
	 * Sets the id bank account type.
	 *
	 * @param idBankAccountType the new id bank account type
	 */
	public void setIdBankAccountType(Long idBankAccountType) {
		this.idBankAccountType = idBankAccountType;
	}
	
	/**
	 * Gets the id bank account class.
	 *
	 * @return the id bank account class
	 */
	public Long getIdBankAccountClass() {
		return idBankAccountClass;
	}
	
	/**
	 * Sets the id bank account class.
	 *
	 * @param idBankAccountClass the new id bank account class
	 */
	public void setIdBankAccountClass(Long idBankAccountClass) {
		this.idBankAccountClass = idBankAccountClass;
	}
	
	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public String getBankAccountType() {
		return bankAccountType;
	}
	
	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(String bankAccountType) {
		this.bankAccountType = bankAccountType;
	}
	
	/**
	 * Gets the id institution bank account pk.
	 *
	 * @return the id institution bank account pk
	 */
	public Long getIdInstitutionBankAccountPk() {
		return idInstitutionBankAccountPk;
	}
	
	/**
	 * Sets the id institution bank account pk.
	 *
	 * @param idInstitutionBankAccountPk the new id institution bank account pk
	 */
	public void setIdInstitutionBankAccountPk(Long idInstitutionBankAccountPk) {
		this.idInstitutionBankAccountPk = idInstitutionBankAccountPk;
	}
}