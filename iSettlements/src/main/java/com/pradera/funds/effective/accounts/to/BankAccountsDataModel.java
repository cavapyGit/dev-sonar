package com.pradera.funds.effective.accounts.to;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BankAccountsDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class BankAccountsDataModel extends ListDataModel<BankAccounts> implements SelectableDataModel<BankAccounts>,Serializable{

	/**
	 * Instantiates a new bank accounts data model.
	 *
	 * @param data the data
	 */
	public BankAccountsDataModel(List<BankAccounts> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public BankAccounts getRowData(String rowKey) {
		List<BankAccounts> parametros=(List<BankAccounts>)getWrappedData();
        for(BankAccounts parameter : parametros) {        	
            if(String.valueOf(parameter.getId()).equals(rowKey))
                return parameter;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	public Object getRowKey(BankAccounts parameter) {
		return parameter.getId();
	}
}