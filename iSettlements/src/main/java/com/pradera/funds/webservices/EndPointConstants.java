package com.pradera.funds.webservices;

public class EndPointConstants {

	public static final String GENERATE_UNFULLFILED_OPERATION_REPORT = "/resources/generate/negotiation/unfullfillment";
	
	public static final String GENERATE_AFFECTED_OPERATION_REPORT = "/resources/generate/negotiation/affected";
}
