package com.pradera.funds.webservices;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.remote.ReportRegisterTO;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ServiceAPIClients {

	@Inject
	ClientRestService clientRestService;
	
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
    
	Logger log = Logger.getLogger(ServiceAPIClients.class);
	
	private final String INSTITUTION_NAME = "CAJA DE VALORES DEL PARAGUAY S.A.";
	
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	public void sendCoreOpeUnfullFillmentReport(LoggerUser loggerUser, Date settlementDate, 
		Long idMechanism, Integer institutionType) throws ServiceException {
	
		ReportRegisterTO input = new ReportRegisterTO();
		input.setLoggerUser(loggerUser);
		input.setReportId(ReportIdType.COMMUNICATION_OPERATION_UNFULLFILLED.getCode());
		input.setStringInitDate(CommonsUtilities.convertDatetoString(settlementDate));
		input.setInstitutionName(INSTITUTION_NAME);
		input.setRelatedId(idMechanism);
		input.setInstitutionType(institutionType);
		
		this.clientRestService.callLocalWebService(ModuleWarType.REPORT.getValue(), EndPointConstants.GENERATE_UNFULLFILED_OPERATION_REPORT, input);
	}
	
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	public void sendUnfullfilledParticipantOpeUnfullFillmentReport(LoggerUser loggerUser, Date settlementDate, 
		Long idMechanism, String participantName, Long participantPk) throws ServiceException {
	
		ReportRegisterTO input = new ReportRegisterTO();
		input.setLoggerUser(loggerUser);
		input.setReportId(ReportIdType.COMMUNICATION_OPERATION_UNFULLFILLED.getCode());
		input.setStringInitDate(CommonsUtilities.convertDatetoString(settlementDate));
		input.setInstitutionName(participantName);
		input.setRelatedId(idMechanism);
		input.setParticipantCode(participantPk);
		
		this.clientRestService.callLocalWebService(ModuleWarType.REPORT.getValue(), EndPointConstants.GENERATE_UNFULLFILED_OPERATION_REPORT, input);
	}
	
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	public void sendAffectedParticipantOpeUnfullFillmentReport(LoggerUser loggerUser, Date settlementDate, 
		Long idMechanism, String participantName, Long participantPk) throws ServiceException {
	
		ReportRegisterTO input = new ReportRegisterTO();
		input.setLoggerUser(loggerUser);
		input.setReportId(ReportIdType.COMMUNICATION_OPERATION_UNFULLFILLED.getCode());
		input.setStringInitDate(CommonsUtilities.convertDatetoString(settlementDate));
		input.setInstitutionName(participantName);
		input.setRelatedId(idMechanism);
		input.setParticipantCode(participantPk);
		
		this.clientRestService.callLocalWebService(ModuleWarType.REPORT.getValue(), EndPointConstants.GENERATE_AFFECTED_OPERATION_REPORT, input);
	}
}
