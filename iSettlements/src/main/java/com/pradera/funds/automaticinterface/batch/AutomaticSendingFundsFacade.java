package com.pradera.funds.automaticinterface.batch;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.swift.service.AutomaticSendingFundsService;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.component.swift.to.RetirementProcess;
import com.pradera.core.component.swift.to.SwiftMessagesRetirement;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AutomaticSendingFundsFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
public class AutomaticSendingFundsFacade {
	
	/** The send service. */
	//SERVICES DECLARATION WHICH WILL BE USED IN CASH ACCOUNT MANAGEMENT FACADE
	@EJB
	AutomaticSendingFundsService sendService;
	
	/** The cash account service. */
	@EJB
	CashAccountManagementService cashAccountService;

	/**
	 * METHOD WHICH WILL SAVE THE AUTOMATIC FUND OPERATION DEVOLUTION.
	 *
	 * @param automaticDevolutionPK the automatic devolution pk
	 * @param devolutionError the devolution error
	 * @param fundOperationType the fund operation type
	 * @return the funds operation
	 */
	private FundsOperation saveAutomaticFundOperation(Long automaticDevolutionPK, Integer devolutionError, Long fundOperationType){
		FundsOperation returnOperation = new FundsOperation();
		returnOperation.setFundsOperation(returnOperation);
		FundsOperation originalOperation = sendService.find(FundsOperation.class, automaticDevolutionPK);
		returnOperation.setInstitutionCashAccount(originalOperation.getInstitutionCashAccount());
		returnOperation.setGuaranteeOperation(null);
		returnOperation.setMechanismOperation(originalOperation.getMechanismOperation());
		returnOperation.setBenefitPaymentAllocation(null);
		returnOperation.setSettlementProcess(originalOperation.getSettlementProcess());
		returnOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RETURN.getCode());
		returnOperation.setFundsOperationType(fundOperationType);
		returnOperation.setFundsOperationClass(FundsType.IND_RETIREMENT_PROCESS.getCode());
		returnOperation.setCurrency(originalOperation.getCurrency());
		returnOperation.setParticipant(originalOperation.getParticipant());
		returnOperation.setIssuer(null);
		returnOperation.setBank(originalOperation.getBank());
		returnOperation.setBankReference(originalOperation.getBankReference());
		returnOperation.setOperationAmount(originalOperation.getOperationAmount());
		returnOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		returnOperation.setIndAutomatic(FundsType.INDICATOR_FUND_PROCESS_TYPE_AUTOMATIC.getCode());
		returnOperation.setObservation(devolutionError);
		returnOperation.setRegistryUser(null);
		sendService.create(returnOperation);

		return returnOperation;
	}

	/**
	 * METHOD WHICH WILL SAVE THE BENEFIT PAYMENT FUND OPERATION.
	 *
	 * @param retirementObject the retirement object
	 * @return the funds operation
	 */
	private FundsOperation saveAutomaticRetirementBenefits(RetirementProcess retirementObject){
		FundsOperation fundRetirementOperation = new FundsOperation();
		FundsOperationType fundOperationType = sendService.find(FundsOperationType.class, retirementObject.getFundOperationType());
		Long cashAccountPk = sendService.getRelatedCashAccount(fundOperationType, retirementObject);

		fundRetirementOperation.setInstitutionCashAccount(new InstitutionCashAccount());
		fundRetirementOperation.getInstitutionCashAccount().setIdInstitutionCashAccountPk(cashAccountPk);
		fundRetirementOperation.getBenefitPaymentAllocation().setIdBenefitPaymentPK(retirementObject.getBenefitPaymentAllocationPK());
		fundRetirementOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_BENEFITS.getCode());
		fundRetirementOperation.setFundsOperationType(retirementObject.getFundOperationType());
		fundRetirementOperation.setFundsOperationClass(FundsType.IND_RETIREMENT_PROCESS.getCode());
		fundRetirementOperation.setCurrency(retirementObject.getCurrency());
		fundRetirementOperation.getIssuer().setIdIssuerPk(retirementObject.getIssuerPK());
		fundRetirementOperation.getBank().setIdBankPk(retirementObject.getBankPK());
		fundRetirementOperation.setOperationAmount(retirementObject.getAmount());
		fundRetirementOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundRetirementOperation.setIndAutomatic(FundsType.INDICATOR_FUND_PROCESS_TYPE_AUTOMATIC.getCode());
		fundRetirementOperation.setRegistryUser(null);
		sendService.create(fundRetirementOperation);

		return fundRetirementOperation;
	}

	/**
	 * METHOD WHICH WILL SAVE THE FUND_OPERATIONS FOR GROSS SETTLEMENT OPERATIONS.
	 *
	 * @param retirementObject the retirement object
	 * @return the funds operation
	 */
	private FundsOperation saveAutomaticRetirementGrossSettlement(RetirementProcess retirementObject){
		FundsOperation fundRetirementGrossOperation = new FundsOperation();
		FundsOperationType fundOperationType = sendService.find(FundsOperationType.class, retirementObject.getFundOperationType());
		Long cashAccountPk = sendService.getRelatedCashAccount(fundOperationType, retirementObject);

		fundRetirementGrossOperation.setInstitutionCashAccount(new InstitutionCashAccount());
		fundRetirementGrossOperation.getInstitutionCashAccount().setIdInstitutionCashAccountPk(cashAccountPk);
		fundRetirementGrossOperation.getMechanismOperation().setIdMechanismOperationPk(retirementObject.getMechanismOperationPK());
		fundRetirementGrossOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode());
		fundRetirementGrossOperation.setFundsOperationType(retirementObject.getFundOperationType());
		fundRetirementGrossOperation.setFundsOperationClass(FundsType.IND_RETIREMENT_PROCESS.getCode());
		fundRetirementGrossOperation.setCurrency(retirementObject.getCurrency());
		fundRetirementGrossOperation.getParticipant().setIdParticipantPk(retirementObject.getParticipantPK());
		fundRetirementGrossOperation.getBank().setIdBankPk(retirementObject.getBankPK());
		fundRetirementGrossOperation.setOperationAmount(retirementObject.getAmount());
		fundRetirementGrossOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundRetirementGrossOperation.setIndAutomatic(FundsType.INDICATOR_FUND_PROCESS_TYPE_AUTOMATIC.getCode());
		fundRetirementGrossOperation.setRegistryUser(null);
		sendService.create(fundRetirementGrossOperation);

		return fundRetirementGrossOperation;
	}


	/**
	 * METHOD WHICH WILL SAVE THE FUND_OPERATIONS FOR DEVOLUTIONS.
	 *
	 * @param retirementObject the retirement object
	 * @return the funds operation
	 */
	private FundsOperation saveRetirementReservation(RetirementProcess retirementObject){
		FundsOperation fundRetirementSupplyOperation = new FundsOperation();

		FundsOperationType fundOpeType = sendService.find(FundsOperationType.class,retirementObject.getFundOperationType());
		Long cashAccountPK = sendService.getRelatedCashAccount(fundOpeType,retirementObject);
		
		fundRetirementSupplyOperation.getInstitutionCashAccount().setIdInstitutionCashAccountPk(cashAccountPK);
		fundRetirementSupplyOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RETURN.getCode());
		fundRetirementSupplyOperation.setFundsOperationType(retirementObject.getFundOperationType());
		fundRetirementSupplyOperation.setFundsOperationClass(FundsType.IND_RETIREMENT_PROCESS.getCode());
		fundRetirementSupplyOperation.setCurrency(retirementObject.getCurrency());
		fundRetirementSupplyOperation.getParticipant().setIdParticipantPk(retirementObject.getParticipantPK());
		fundRetirementSupplyOperation.getBank().setIdBankPk(retirementObject.getCentralBankPk());
		fundRetirementSupplyOperation.setOperationAmount(retirementObject.getAmount());
		fundRetirementSupplyOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundRetirementSupplyOperation.setIndAutomatic(FundsType.INDICATOR_FUND_PROCESS_TYPE_AUTOMATIC.getCode());
		fundRetirementSupplyOperation.setRegistryUser(null);

		FundsOperation manualOperation = null;
		//WE MUST VALIDATE HERE IF THE OPERATION TYPE WILL GENERATE A MANUAL OPERATION FOR DEPOSIT IN COMMERCIAL BANKS
		manualOperation = createManualFundOperation(retirementObject);

		fundRetirementSupplyOperation.setFundsOperation(manualOperation);

		sendService.create(fundRetirementSupplyOperation);

		return fundRetirementSupplyOperation;
	}

	/**
	 * METHOD WHICH WILL CREATE THE MANUAL FUND OPERATION TO THE RELATED AUTOMATIC OPERACION OF DEVOLUTION.
	 *
	 * @param retirementObject the retirement object
	 * @return the funds operation
	 */
	private FundsOperation createManualFundOperation(RetirementProcess retirementObject){
		FundsOperation fundManualOperation = new FundsOperation();
		Long fundOperationType = retirementObject.getFundOperationType();

		FundsOperationType fundReturnOpeType = sendService.getDepositOperationFromOrigOpe(fundOperationType);
		Long returnOpePK = sendService.getRelatedCashAccount(fundReturnOpeType,retirementObject);

		fundManualOperation.setInstitutionCashAccount(new InstitutionCashAccount());
		fundManualOperation.getInstitutionCashAccount().setIdInstitutionCashAccountPk(returnOpePK);
		fundManualOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RETURN.getCode());
		fundManualOperation.setFundsOperationClass(FundsType.IND_RECEPTION_PROCESS.getCode());
		fundManualOperation.setCurrency(retirementObject.getCurrency());
		fundManualOperation.getBank().setIdBankPk(retirementObject.getBankPK());
		fundManualOperation.setOperationAmount(retirementObject.getAmount());
		fundManualOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
		fundManualOperation.setIndAutomatic(FundsType.INDICATOR_FUND_PROCESS_TYPE_MANUAL.getCode());
		fundManualOperation.setRegistryUser(null);
		sendService.create(fundManualOperation);

		return fundManualOperation;
	}

//	/**
//	 * METHOD WHICH WILL CONVERT THE RETIREMENT OBJECT INTO A STANDARD OBJECT FOR SWIFT JAR TO GENERATE THE MESSAGE
//	 * @param retirementObject
//	 * @return
//	 */
//	private SwiftMessagesRetirement getSwiftRetirementMessageBean(RetirementProcess retirementObject, Long fundOperationPK){
//		SwiftMessagesRetirement retirementSwiftMessageBean = new SwiftMessagesRetirement();
//		Long automaticDevolutionPK = retirementObject.getFundOperationReturnPK();
//		Long benefitPaymentPK = retirementObject.getBenefitPaymentAllocationPK();
//		Long mechanismOperationPK = retirementObject.getMechanismOperationPK();
//		Long settlementPK = retirementObject.getSettlementProcessPK();
//
//		String messageType = sendService.getMessageType(retirementObject.getFundOperationType(),retirementObject.getCurrency());
//		retirementSwiftMessageBean.setMessageType(messageType);
//
//		if(Validations.validateIsNotNull(automaticDevolutionPK)){
//			//AUTOMATIC DEVOLUTION
//			retirementSwiftMessageBean = fillSwiftAutomaticDevolutionObject(retirementObject, fundOperationPK);
//			retirementSwiftMessageBean.setGroupOperationFund(SwiftConstants.GROUP_OPERATION_DEPOSIT_DEVOLUTIONS);
//		}else if(Validations.validateIsNotNull(benefitPaymentPK)){
//			//BENEFIT PAYMENT
//			retirementSwiftMessageBean = fillSwiftBenefitsObject(retirementObject, fundOperationPK);
//			retirementSwiftMessageBean.setGroupOperationFund(SwiftConstants.GROUP_OPERATION_BENEFITS);
//		}else if(Validations.validateIsNotNull(mechanismOperationPK) || Validations.validateIsNotNull(settlementPK)){
//			//SETTLEMENT
//			retirementSwiftMessageBean = fillSwiftSettlementObject(retirementObject);
//			retirementSwiftMessageBean.setGroupOperationFund(SwiftConstants.GROUP_OPERATION_SETTLEMENT);
//		}else{
//			//RETIREMENT RESERVATION
//			retirementSwiftMessageBean = fillSwiftDevolutionObject(retirementObject,fundOperationPK);
//			retirementSwiftMessageBean.setGroupOperationFund(SwiftConstants.GROUP_OPERATION_RETURNS);
//		}
//		retirementSwiftMessageBean.setOperationType(Integer.valueOf(fundOperationPK.toString()));
//
//		return retirementSwiftMessageBean;
//	}

	/**
 * METHOD WHICH WILL GENERATE THE JAVA OBJECT FOR SWIFT PROCESS IN SETTLEMENT.
 *
 * @param retirementObject the retirement object
 * @return the swift messages retirement
 */
	private SwiftMessagesRetirement fillSwiftSettlementObject(RetirementProcess retirementObject){
		SwiftMessagesRetirement swiftSettlementBean = new SwiftMessagesRetirement();
		Long participantSettlerPK = retirementObject.getParticipantPK();

		//CEVALDOM BIC CODE
		String cevaldomBicCode = sendService.getCevaldomBicCode();
		swiftSettlementBean.setCevaldomBicCode(cevaldomBicCode);
		//PARTICIPANT BIC CODE
		String participantBicCode = sendService.getParticipantBicCode(participantSettlerPK);
		swiftSettlementBean.setReceptorBicCode(participantBicCode);
		//BCRD BIC CODE
		String bcrdBicCode = sendService.getBCRDBicCode();
		swiftSettlementBean.setBcrdBicCode(bcrdBicCode);
		//TRN OF THE PAID OPERATION
		String trn = sendService.getOperationTrn(retirementObject.getFundOperationType(),retirementObject.getCurrency());
		swiftSettlementBean.setTrn(trn);
		//VERIFY ACCORDING TO THE SETTLEMENT TYPE THE OTHER FIELDS FOR THE PAYMENT.REFERENCE
		Long operationPK = retirementObject.getSettlementProcessPK();
		if(Validations.validateIsNotNull(operationPK)){
			//LIQUIDACION BRUTA
			swiftSettlementBean.setOperationID(operationPK);
			swiftSettlementBean.setSettlementSchema(FundsType.SWIFT_INDICATOR_GROSS_SETTLEMENT.getStrCode());
		}else if(Validations.validateIsNotNull(retirementObject.getMechanismOperationPK())){
			//LIQUIDACION NETA
			swiftSettlementBean.setSettlementSchema(FundsType.SWIFT_INDICATOR_NET_SETTLEMENT.getStrCode());
			NegotiationMechanism mechanism = sendService.find(NegotiationMechanism.class,retirementObject.getMechanism());
			swiftSettlementBean.setMechanism(mechanism.getMechanismCode());
			NegotiationModality modality = sendService.find(NegotiationModality.class,retirementObject.getModalityGroup());
			swiftSettlementBean.setModalityGroup(modality.getModalityCode());
		}

		swiftSettlementBean.setSettlementDate(retirementObject.getSettlementDate());
		Integer currencyInt = retirementObject.getCurrency();
		String currency = sendService.getCurrencyCode(currencyInt);
		swiftSettlementBean.setCurrency(currency);
		swiftSettlementBean.setAmount(retirementObject.getAmount());

		Long participantBuyerPK = retirementObject.getParticipantBuyerPK();
		Participant participantBuyer = sendService.find(Participant.class,participantBuyerPK);
		String participantBankAccountNumber = sendService.getBankAccountNumber(participantBuyerPK, retirementObject.getCurrency(),retirementObject.getMechanism(),retirementObject.getModalityGroup());
		swiftSettlementBean.setParticipantBuyerCentralAccountNumber(participantBankAccountNumber);
		swiftSettlementBean.setParticipantBuyerDescription(participantBuyer.getCompleteDescription());

		Participant participantSettler = sendService.find(Participant.class,participantSettlerPK);
		String partSettlerBankAccountNumber = sendService.getBankAccountNumber(participantSettlerPK, retirementObject.getCurrency(),retirementObject.getMechanism(),retirementObject.getModalityGroup());
		swiftSettlementBean.setParticipantSettlerCentralAccountNumber(partSettlerBankAccountNumber);
		swiftSettlementBean.setParticipantSettlerDescription(participantSettler.getCompleteDescription());

		FundsOperationType operationType = sendService.find(FundsOperationType.class, retirementObject.getFundOperationType());
		swiftSettlementBean.setOperationTypeDescription(operationType.getName());

		return swiftSettlementBean;
	}

	/**
	 * METHOD WHICH WILL GENERATE THE JAVA OBJECT FOR SWIFT PROCESS IN BENEFITS.
	 *
	 * @param retirementObject the retirement object
	 * @param fundOperationPK the fund operation pk
	 * @return the swift messages retirement
	 */
	private SwiftMessagesRetirement fillSwiftBenefitsObject(RetirementProcess retirementObject, Long fundOperationPK){
		SwiftMessagesRetirement swiftSettlementBean = new SwiftMessagesRetirement();
		Long fundOperationType = retirementObject.getFundOperationType();
		//CEVALDOM BIC CODE
		String cevaldomBicCode = sendService.getCevaldomBicCode();
		swiftSettlementBean.setCevaldomBicCode(cevaldomBicCode);
		//TRN OF THE OPERATION
		String trn = sendService.getOperationTrn(retirementObject.getFundOperationType(),retirementObject.getCurrency());
		swiftSettlementBean.setTrn(trn);
		//FUND OPERATION
		swiftSettlementBean.setIdFundOperation(fundOperationPK);
		//SETTLEMENT DATE,CURRENCY,AMOUNT
		swiftSettlementBean.setSettlementDate(retirementObject.getSettlementDate());
		Integer currencyInt = retirementObject.getCurrency();
		String currency = sendService.getCurrencyCode(currencyInt);
		swiftSettlementBean.setCurrency(currency);
		swiftSettlementBean.setAmount(retirementObject.getAmount());

//		if(			com.pradera.model.component.type.FundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 	
//					com.pradera.model.component.type.FundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 	
//					com.pradera.model.component.type.FundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 	
//					com.pradera.model.component.type.FundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 	
//					com.pradera.model.component.type.FundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType)){
//					//BANK BIC CODE
//					Bank bank = sendService.find(Bank.class, retirementObject.getBankPK());
//					swiftSettlementBean.setBicCodeBeneficiaryBank(bank.getBicCode());
//					swiftSettlementBean.setCorporativeProcessDescription(retirementObject.getCorporativeProcessDescription());
//
//		}else if(	com.pradera.model.component.type.FundsOperationType.INSTITUTIONS_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
//					com.pradera.model.component.type.FundsOperationType.INSTITUTIONS_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) ||
//					com.pradera.model.component.type.FundsOperationType.INSTITUTIONS_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) ||
//					com.pradera.model.component.type.FundsOperationType.INSTITUTIONS_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) ||
//					com.pradera.model.component.type.FundsOperationType.INSTITUTIONS_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType)){
//
//					Long participantPK = retirementObject.getParticipantPK();
//					//PARTICIPANT BIC CODE
//					String participantBicCode = sendService.getParticipantBicCode(participantPK);
//					swiftSettlementBean.setReceptorBicCode(participantBicCode);
//					//BCRD BIC CODE
//					String bcrdBicCode = sendService.getBCRDBicCode();
//					swiftSettlementBean.setBcrdBicCode(bcrdBicCode);
//
//					//ACCOUNT NUMBER CEVALDOM IN CENTRAL BANK
//					Long bcrdBankPK = cashAccountService.getBCRDBank(retirementObject.getCurrency());
//					String cevaldomAccountNumberBCRD = sendService.getCevaldomBCRDAccountNumber(retirementObject.getCurrency(),bcrdBankPK);
//					swiftSettlementBean.setDepositaryCentralBankAccount(cevaldomAccountNumberBCRD);
//					//BANK ACCOUNT NUMBER,
//					String partBankAccountNumber = sendService.getBankAccountNumber(participantPK, retirementObject.getCurrency(),retirementObject.getMechanism(),retirementObject.getModalityGroup());
//					swiftSettlementBean.setParticipantSettlerCentralAccountNumber(partBankAccountNumber);
//					//DESCRIPTION DEPOSITARY
//					String depositaryDescription = sendService.getDepositaryDescription();
//					swiftSettlementBean.setDepositaryDescription(depositaryDescription);
//					//DESCRIPTION CORPORATIVE
//					swiftSettlementBean.setCorporativeProcessDescription(retirementObject.getCorporativeProcessDescription());
//
//		}else{
//			InstitutionBankAccount commercialBank = null;
//			if(com.pradera.model.component.type.FundsOperationType.CUSTODY_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode().equals(fundOperationType)){
//				commercialBank = sendService.getAccountNumberByTypeCurrency(retirementObject.getCurrency(), Long.valueOf(AccountCashFundsType.RATES.getCode().toString()));
//			}else if(com.pradera.model.component.type.FundsOperationType.ISSUER_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode().equals(fundOperationType)){
//				commercialBank = sendService.getAccountNumberByTypeCurrency(retirementObject.getCurrency(), Long.valueOf(AccountCashFundsType.RATES.getCode().toString()));
//			}else if(com.pradera.model.component.type.FundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode().equals(fundOperationType)){
//				commercialBank = sendService.getAccountNumberByTypeCurrency(retirementObject.getCurrency(), Long.valueOf(AccountCashFundsType.SETTLEMENT.getCode().toString()));
//			}else if(com.pradera.model.component.type.FundsOperationType.TAX_BENEFITS_WITHDRAWL_RETENTION.getCode().equals(fundOperationType)){
//				commercialBank = sendService.getAccountNumberByTypeCurrency(retirementObject.getCurrency(), Long.valueOf(AccountCashFundsType.TAX.getCode().toString()));
//			}else if(com.pradera.model.component.type.FundsOperationType.BENEFIT_SUPPLY_WITHDRAWL.getCode().equals(fundOperationType)){
//				commercialBank = sendService.getAccountNumberByTypeCurrency(retirementObject.getCurrency(), Long.valueOf(AccountCashFundsType.TAX.getCode().toString()));
//			}
//			//CEVALDOM ACCOUNT NUMBER OF BENEFITS IN CENTRAL BANK
//			Long bcrdBankPK = cashAccountService.getBCRDBank(retirementObject.getCurrency());
//			String cevaldomAccountNumberBCRD = sendService.getCevaldomBCRDAccountNumber(retirementObject.getCurrency(),bcrdBankPK);
//			swiftSettlementBean.setDepositaryCentralBankAccount(cevaldomAccountNumberBCRD);
//			//DEPOSITARY DESCRIPTION
//			String depositaryDescription = sendService.getDepositaryDescription();
//			swiftSettlementBean.setDepositaryDescription(depositaryDescription);
//
//			//COMMERCIAL BANK ACCOUNT NUMBER & COMMERCIAL BANK DESCRIPTION
//			swiftSettlementBean.setDepositaryCommercialAccountNumber(commercialBank.getAccountNumber());
//			swiftSettlementBean.setCommercialBankDescription(commercialBank.getProviderBank().getDescription());
//			//FUND OPERATION DESCRIPTION
//			FundsOperationType  fundOpeType = sendService.find(FundsOperationType.class, fundOperationPK);
//			swiftSettlementBean.setFundOperationTypeDescription(fundOpeType.getName());
//		}
//
//		return swiftSettlementBean;
		return null;
	}

	/**
	 * METHOD WHICH WILL GENERATE THE JAVA OBJECT FOR SWIFT PROCESS IN DEVOLUTIONS.
	 *
	 * @param retirementObject the retirement object
	 * @param fundOperationPK the fund operation pk
	 * @return the swift messages retirement
	 */
	private SwiftMessagesRetirement fillSwiftDevolutionObject(RetirementProcess retirementObject, Long fundOperationPK){
		SwiftMessagesRetirement swiftDevolutionBean = new SwiftMessagesRetirement();
		Long fundOperationType = retirementObject.getFundOperationType();
		//CEVALDOM BIC CODE
		String cevaldomBicCode = sendService.getCevaldomBicCode();
		swiftDevolutionBean.setCevaldomBicCode(cevaldomBicCode);
		//TRN OF THE OPERATION
		String trn = sendService.getOperationTrn(retirementObject.getFundOperationType(),retirementObject.getCurrency());
		swiftDevolutionBean.setTrn(trn);
		//FUND OPERATION
		swiftDevolutionBean.setIdFundOperation(fundOperationPK);
		//SETTLEMENT DATE,CURRENCY,AMOUNT
		swiftDevolutionBean.setSettlementDate(retirementObject.getSettlementDate());
		Integer currencyInt = retirementObject.getCurrency();
		String currency = sendService.getCurrencyCode(currencyInt);
		swiftDevolutionBean.setCurrency(currency);
		swiftDevolutionBean.setAmount(retirementObject.getAmount());

//		if(com.pradera.model.component.type.FundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode().equals(fundOperationType)){
			String issuer = retirementObject.getIssuerPK();
			//CASH ACCOUNT TYPE
			FundsOperationType fundsOperationType = sendService.find(FundsOperationType.class, fundOperationType);
			Integer cashAccountType = fundsOperationType.getCashAccountType();
			//COMMERCIAL BANK BIC CODE
			InstitutionBankAccount commercialBank = sendService.getAccountNumberCurrencyIssuer(retirementObject.getCurrency(),issuer,cashAccountType);
			swiftDevolutionBean.setBicCodeBeneficiaryBank(commercialBank.getProviderBank().getBicCode());
			Bank bank = sendService.find(Bank.class, commercialBank.getProviderBank().getIdBankPk());
			swiftDevolutionBean.setCommercialBankDescription(bank.getDescription());
			swiftDevolutionBean.setDepositaryCommercialAccountNumber(commercialBank.getAccountNumber());
			//FUND OPERATION DESCRIPTION
			FundsOperationType  fundOpeType = sendService.find(FundsOperationType.class, fundOperationPK);
			swiftDevolutionBean.setFundOperationTypeDescription(fundOpeType.getName());

//		}else if(com.pradera.model.component.type.FundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode().equals(fundOperationType) || 
//				com.pradera.model.component.type.FundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode().equals(fundOperationType)){

//			Bank bank = sendService.find(Bank.class, retirementObject.getBankPK());
//			Holder holder = sendService.find(Holder.class, retirementObject.getHolder());
//			HolderAccountBank holderAccount = sendService.getHolderAccountNumber(holder.getIdHolderPk(),retirementObject.getBankPK());
//			swiftDevolutionBean.setBicCodeBeneficiaryBank(bank.getBicCode());
//			swiftDevolutionBean.setCommercialBankDescription(bank.getDescription());
//			swiftDevolutionBean.setDepositaryCommercialAccountNumber(holderAccount.getBankAccountNumber());
//			//WE MUST INSERT THE HOLDER DESCRIPTION
//			swiftDevolutionBean.setHolderDescription(holder.getDescriptionHolder());
//
//			//FUND OPERATION DESCRIPTION
//			FundsOperationType  fundOpeType = sendService.find(FundsOperationType.class, fundOperationPK);
//			swiftDevolutionBean.setFundOperationTypeDescription(fundOpeType.getName());
//
//			//WE REGISTER THE CASH MOVEMENTS FOR HOLDER
//			HolderCashMovement cashMovement = new HolderCashMovement();
//			cashMovement.setFundsOperationType(new FundsOperationType());
//			cashMovement.getFundsOperationType().setIdFundsOperationTypePk(fundOperationPK);
//
//			cashMovement.setBank(bank);
//			cashMovement.setHolder(holder);
//			cashMovement.setHolderAmount(retirementObject.getAmount());
//			cashMovement.setCurrency(retirementObject.getCurrency());
//			cashMovement.setHolderAccountBank(holderAccount);
//			cashMovement.setBankAccountNumber(holderAccount.getBankAccountNumber());
//			cashMovement.setBankAccountType(Integer.valueOf(bank.getBankType().toString()));
//			cashMovement.setFullName(holder.getDescriptionHolder());
//			cashMovement.setDocumentHolder(holder.getDocumentNumber());
//			cashMovement.setMovementDate(new Date());
//			sendService.create(cashMovement);

//		}

		return swiftDevolutionBean;	
	}

//	/**
//	 * METHOD WICH WILL GENERATE THE SWIFT OBJECT FOR AUTOMATIC DEVOLUTIONS
//	 * @param retirementObject
//	 * @param fundOperationPK
//	 * @return
//	 */
//	private SwiftMessagesRetirement fillSwiftAutomaticDevolutionObject(RetirementProcess retirementObject, Long fundOperationPK){
//		SwiftMessagesRetirement swiftAutomaticDevolutionBean = new SwiftMessagesRetirement();
//
//		//CEVALDOM BIC CODE
//		String cevaldomBicCode = sendService.getCevaldomBicCode();
//		swiftAutomaticDevolutionBean.setCevaldomBicCode(cevaldomBicCode);
//		//BENEFICIARY INSTITUTION
//		Long participantPK = retirementObject.getParticipantPK();
//		String issuer = retirementObject.getIssuerPK();
//		if(Validations.validateIsNotNullAndNotEmpty(participantPK)){
//			Participant participant = sendService.find(Participant.class, participantPK);
//			swiftAutomaticDevolutionBean.setReceptorBicCode(participant.getBicCode());
//			swiftAutomaticDevolutionBean.setBicCodeBeneficiaryBank(participant.getCompleteDescription());
//
//		}else if(Validations.validateIsNotNullAndNotEmpty(issuer)){
//			Bank bank = sendService.find(Bank.class,retirementObject.getBankPK());
//			swiftAutomaticDevolutionBean.setReceptorBicCode(bank.getBicCode());
//			swiftAutomaticDevolutionBean.setBicCodeBeneficiaryBank(bank.getDescription());
//
//		}
//		//BCRD BIC CODE
//		String bcrdBicCode = sendService.getBCRDBicCode();
//		swiftAutomaticDevolutionBean.setBcrdBicCode(bcrdBicCode);
//		//TRN
//		String trn = sendService.getTrnAutomaticDevolution(retirementObject.getCurrency());
//		swiftAutomaticDevolutionBean.setTrn(trn);
//		//OPERATION PK
//		swiftAutomaticDevolutionBean.setIdFundOperation(retirementObject.getMechanismOperationPK());
//		//ORIGIN FUND OPERATION
//		swiftAutomaticDevolutionBean.setOriginalFundOperation(retirementObject.getOriginalTRNDevolutionOperation());
//		//SETTLEMENT DATE,CURRENCY,AMOUNT
//		swiftAutomaticDevolutionBean.setSettlementDate(retirementObject.getSettlementDate());
//		Integer currencyInt = retirementObject.getCurrency();
//		String currency = sendService.getCurrencyCode(currencyInt);
//		swiftAutomaticDevolutionBean.setCurrency(currency);
//		swiftAutomaticDevolutionBean.setAmount(retirementObject.getAmount());
//		//FUNDS OPERATION TYPE DESCRIPTION
//		FundsOperationType fundOpeType = sendService.find(FundsOperationType.class,retirementObject.getFundOperationType());
//		swiftAutomaticDevolutionBean.setFundOperationTypeDescription(fundOpeType.getName());
//
//		return swiftAutomaticDevolutionBean;
//	}
}