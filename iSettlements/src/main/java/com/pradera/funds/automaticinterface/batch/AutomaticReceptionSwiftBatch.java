//package com.pradera.funds.automaticinterface.batch;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.ejb.EJB;
//import javax.enterprise.context.RequestScoped;
//import javax.inject.Inject;
//
//import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
//import com.pradera.commons.contextholder.type.RegistryContextHolderType;
//import com.pradera.commons.type.BusinessProcessType;
//import com.pradera.core.component.business.to.SwiftDeposit;
//import com.pradera.core.component.business.to.SwiftMessageJavaObject;
//import com.pradera.core.component.business.to.SwiftMessagesDeposit;
//import com.pradera.core.framework.batchprocess.BatchProcess;
//import com.pradera.core.framework.batchprocess.service.JobExecution;
//import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
//import com.pradera.funds.automaticinterface.swift.facade.AutomaticReceptionSwiftFacade;
//import com.pradera.integration.common.validation.Validations;
//import com.pradera.integration.contextholder.LoggerUser;
//import com.pradera.integration.exception.ServiceException;
//import com.pradera.model.process.BusinessProcess;
//import com.pradera.model.process.ProcessLogger;
//
//
//@RequestScoped
//@BatchProcess(name="AutomaticReceptionSwiftBatch")
//public class AutomaticReceptionSwiftBatch implements JobExecution,Serializable{
//
//	private static final long serialVersionUID = 1L;
//	@EJB
//	private NotificationServiceFacade notificationServiceFacade;
//	@Inject
//	AutomaticReceptionSwiftFacade automaticReceptionFundsFacade;
//
//	@Override
//	public void startJob(ProcessLogger processLogger){
//		List<SwiftMessagesDeposit> swiftMessages = new ArrayList<SwiftMessagesDeposit>();
//		try {
//			LoggerUser loggerUser = (LoggerUser) ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
//			//WE VALIDATE IF THE BEGIN_END_DAY PROCESS HAS STARTED
//			boolean validateBeginEndDay = automaticReceptionFundsFacade.existBeginDayProcess();
//			if(validateBeginEndDay){
//				//BEFORE GETTING FILES FROM SWIFT REPOSITORY, WE MUST VERIFY CENTRALIZING ACCOUNT FOR CURRENCIES ARE ACTIVATE
//				if(automaticReceptionFundsFacade.validateCentralizerAccounts()){
//					//1.- GET THE SWIFT FILES
//					List<SwiftDeposit> swiftFiles = automaticReceptionFundsFacade.getSwiftFiles();
//
//					//2.- VALIDATE HOW MANY SWIFT MESSAGES WE HAVE GOT FROM AUTOMATIC FUND RECEPTION PROCESS
//					if(Validations.validateListIsNotNullAndNotEmpty(swiftFiles)){
//						for(SwiftDeposit swiftFile : swiftFiles){
//							//3.- CREATE THE SWIFT MESSAGE HISTORY BY EACH SWIFT MESSAGE WE GET
//							automaticReceptionFundsFacade.createMessageHistory(swiftFile);
//
//							//4.- GET IN MEMORY ALL SWIFT MESSAGE WE WILL PROCESS
//							List<SwiftMessagesDeposit> lstMessages = swiftFile.getDeposits();
//							if(Validations.validateListIsNotNullAndNotEmpty(lstMessages)){
//								for(SwiftMessagesDeposit swiftMessage : lstMessages){
//									swiftMessages.add(swiftMessage);
//								}
//							}
//						}
//					}
//				}else{
//					//WE MUST SEND A MESSAGE ALERT INFORMING USER, DOESNT EXIST CENTRALIZER ACCOUNT FOR CURRENCY					
//					BusinessProcess businessProc = new BusinessProcess();
//					businessProc.setIdBusinessProcessPk(BusinessProcessType.EFFECTIVE_ACCOUNT_REGISTRATION.getCode());
//					notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, null, null);
//				}
//			}else{
//				//WE MUST SEND A MESSAGE ALERT INFORMING USER, THE DAY HAS NOT BEEN BEGAN
//				BusinessProcess businessProc = new BusinessProcess();
//				businessProc.setIdBusinessProcessPk(BusinessProcessType.BEGIN_DAY_MANUAL.getCode());
//				notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, null, null);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		if(Validations.validateListIsNotNullAndNotEmpty(swiftMessages)){
//			try {
//				for(SwiftMessagesDeposit depositMessage : swiftMessages){
//					SwiftMessageJavaObject operationObject = new SwiftMessageJavaObject();
//					//5.- CREATE THE FUND OPERATION ACCORDING TO THE FUND RECEPTION OPERATION
//						operationObject = automaticReceptionFundsFacade.processFundReception(depositMessage);
//					//6.- EXECUTE THE COLLECT OF FUND OPERATION
//					if(Validations.validateIsNotNull(operationObject)){
//						automaticReceptionFundsFacade.fundOperationCollect(operationObject);
//					}
//				}
//			} catch (ServiceException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	@Override
//	public Object[] getParametersNotification() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Object> getDestinationInstitutions() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public boolean sendNotification() {
//		// TODO Auto-generated method stub
//		return true;
//	}
//}