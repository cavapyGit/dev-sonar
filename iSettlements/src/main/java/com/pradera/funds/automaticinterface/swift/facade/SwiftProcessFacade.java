package com.pradera.funds.automaticinterface.swift.facade;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.sql.rowset.serial.SerialException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.business.to.SwiftDeposit;
import com.pradera.core.component.business.to.SwiftMessagesDeposit;
import com.pradera.core.component.swift.facade.SwiftWithdrawlProcess;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.core.component.swift.to.SwiftConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
//import com.prowidesoftware.swift.io.ConversionService;
//import com.prowidesoftware.swift.model.SwiftBlock1;
//import com.prowidesoftware.swift.model.SwiftBlock2;
//import com.prowidesoftware.swift.model.SwiftBlock4;
//import com.prowidesoftware.swift.model.SwiftMessage;
//import com.prowidesoftware.swift.model.field.Field32A;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SwiftProcessFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SwiftProcessFacade {

//	private static final  Logger logger = LoggerFactory.getLogger(SwiftProcessFacade.class);
//	@Inject
//	SwiftWithdrawlProcess swiftProcess;
//	@Inject @Configurable
//	String ftpLocalPath, ftpPass, ftpRemotePath, ftpServer, ftpUser;
//	@Inject
//	@Configurable
//	static String ftpProcessInput;
//	@Inject
//	private PraderaLogger log;
//
//	/**
//	 * MAIN METHOD WILL PROCESS SWIFT REQUEST FOR DEPOSIT OR RETIREMENT
//	 * @param operationSwiftType
//	 * @param lstMessages
//	 * @param parameters
//	 */
//	public synchronized List<SwiftDeposit> executeSwiftDeposit(){
//		List<SwiftDeposit> lstProcess = new ArrayList<SwiftDeposit>();
//		try {
//			lstProcess = depositSwift();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return lstProcess;
//	}
//
//	/**
//	 * METHOD TO LOAD FTP FILE AND PROCESS DEPOSIT OPERATIONS
//	 * @param parameters 
//	 * @throws IOException 
//	 */
//	private synchronized List<SwiftDeposit> depositSwift() throws IOException{
//	 	FTPClient client = new FTPClient();
//	 	List<SwiftDeposit> lst = new ArrayList<SwiftDeposit>();
//		try{
//            FTPParameters parameters = swiftProcess.getParametersConnection();
//    		client.connect(parameters.getServer());
//			if(client.login(parameters.getUser(), parameters.getPass())){
//				int reply = client.getReplyCode();
//	            //FTPReply stores a set of constants for FTP reply codes. 
//	            if (FTPReply.isPositiveCompletion(reply)){
//	            	//enter passive mode
//	            	client.enterLocalPassiveMode();
//	            	boolean downloadFiles = downloadFiles(client);
//	            	client.disconnect();
//	            	if(downloadFiles){
//	            		//WILL GET ALL SWIFT MESSAGES DOWNLOADED AND CONVERT INTO JAVA OBJECTS
//	            		lst = processSwiftMessage();
//	            	}
//	            }
//	        }else{
//	        	//IF IT WAS NOT POSSIBLE TO CONNECT SWIFT BECAUSE OF WRONG PARAMETERS, WE MUST SEND A MESSAGE TO USER INFORMING THE PROBLEM
//	        }
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}finally{
//			try{
//				client.disconnect();
//			}catch (FTPConnectionClosedException e) {
//				e.printStackTrace();
//			}
//		}
//		return lst;
//	}
//
//	/**
//	 * METHOD WILL GET SWIFT MESSAGES DOWNLOADED FROM FTP SERVER AND WILL CONVERT TO JAVA OBJECTS
//	 * @param client
//	 * @return
//	 * @throws Exception 
//	 */
//	private synchronized List<SwiftDeposit> processSwiftMessage() throws Exception{
//		List<SwiftDeposit> lstJavaObject = new ArrayList<SwiftDeposit>();
//		List<SwiftMessagesDeposit> lstMessages=null;
//		String curDir = System.getProperty("user.dir");//ACTUAL ROUTE
//		StringBuilder fileContent = new StringBuilder();
//		BufferedReader reader = null;
//		String swiftFileName= StringUtils.EMPTY;
//		File file = null;
//
//		//1)	ITERATE ALL DOC IN DIR TO GET DATA OF SWIFT MESSAGES, KEEP IN MEMORY AND CONVERT TO JAVA OBJECTS
//		try {
//			String destinyURL = curDir+ftpProcessInput;
//			File directory = new File(destinyURL);
//			File[] files = directory.listFiles();
//
//			if(files!= null && files.length>0){
//				for (int i=0; i< files.length; i++) {
//					file = files[i];
//					lstMessages=new ArrayList<SwiftMessagesDeposit>();
//					if(file.isFile()){
//						swiftFileName = file.getName();
//						reader = new BufferedReader(new FileReader(file));
//						String text = StringUtils.EMPTY;
//						//WE READ ONE FILE AND GET MANY SWIFT MESSAGES
//						while((text = reader.readLine()) != null){
//							fileContent = fileContent.append(text).append(System.lineSeparator());
//						}
//						//FROM THE FILE WE GET MANY MESSAGES CONVERTED INTO JAVA OBJECTS
//						if(fileContent != null && !StringUtils.EMPTY.equals(fileContent.toString())){
//							lstMessages = processSwiftFiles(fileContent.toString(),file);
//						}
//					}
//					if(lstMessages != null && lstMessages.size() > 0){
//						SwiftDeposit object = new SwiftDeposit();
//						object.setFile(fileContent.toString());
//						object.setDeposits(lstMessages);
//						object.setFileName(file.getName());
//						lstJavaObject.add(object);
//
//						depositMessageDir(fileContent.toString(),swiftFileName,true);
//
//						fileContent = new StringBuilder();
//					}
//					reader.close();
//					file.delete();
//				}
//			}
//			directory.delete();
//		}catch(Exception e) {
//			depositMessageDir(fileContent.toString(),swiftFileName,false);
//			//deleteLocalFile(swiftFileName);
//			reader.close();
//			file.delete();
//			e.printStackTrace();
//			throw new Exception("Fallo la recepcion del archivo swift: "+ swiftFileName);
//		}
//
//		return lstJavaObject;
//	}
//
//	/**
//	 * METHOD WHICH WILL PROCESS SWIFT FILES AND RETURN ARRAY OF JAVA OBJECTS
//	 * @param swiftFiles
//	 * @return
//	 * @throws SQLException 
//	 * @throws SerialException 
//	 * @throws IOException 
//	 */
//	private synchronized static List<SwiftMessagesDeposit> processSwiftFiles(String swiftFiles, File file) throws Exception{
//		List<SwiftMessagesDeposit> messagesProcessed = new ArrayList<SwiftMessagesDeposit>();
//		SwiftMessagesDeposit parsedSwiftMessage = null;
//		swiftFiles = deleteTwoFirstGroups(swiftFiles);//WE DELETE TWO FIRST LINES OF THE FILE CAUSE ITS NOT REQUIRED
//		List<String> messages = formatSwiftMessage(swiftFiles);//CORRECT FORMAT TO SWIFT FILE FOR BEING WORKED CORRECTLY BY WIFE JAR
//
//		if(messages != null && messages.size() > 0){
//			for(Iterator<String> itrMessages = messages.iterator(); itrMessages.hasNext();){
//				String msg = itrMessages.next();
//
//				//WE INSTANCE THE OBJECT, AND INDICATE ITS A REGISTER AND SET THE SWIFT MESSAGE CORRESPONDING
//				parsedSwiftMessage = new SwiftMessagesDeposit();
//				parsedSwiftMessage.setSwiftMessage(msg);
//				parsedSwiftMessage.setFileName(file.getName());
//				SwiftMessage swift = (new ConversionService()).getMessageFromFIN(msg);
//				SwiftBlock1 block1 = swift.getBlock1();
//				SwiftBlock2 block2 = swift.getBlock2();
//				SwiftBlock4 block4 = swift.getBlock4();
//
//				//BLOCK 1
//				String cevaldomBicCode = block1.getLogicalTerminal();
//				cevaldomBicCode = cevaldomBicCode.substring(0, 8).concat(cevaldomBicCode.substring(9, 12));
//				parsedSwiftMessage.setCevaldomBicSwiftCode(cevaldomBicCode);
//
//				//BLOCK 2
//				//THIS IS A BIC CODE, IS THE ONE OF A FINANTIAL ENTITY, OR THE ONE OF BCRD
//				String bicCodeSwiftSender = null;
//				String block2Value= block2.getBlockValue();
//				bicCodeSwiftSender = block2Value.substring(14,26);
//				bicCodeSwiftSender = bicCodeSwiftSender.substring(0,8).concat(bicCodeSwiftSender.substring(9,12));
//
//				String messageType = block2.getMessageType();
//				parsedSwiftMessage.setMessageType(messageType);
//				
//				//BLOCK 4
//				//PAYMENT REFERENCE OF THE FUND OPERATION THAT MAKES REFERENCE
//				String paymentReference = block4.getTagValue(SwiftConstants.FIELD_NAME_20);
//				parsedSwiftMessage.setReferencePayment(paymentReference);
//
//				//PAYMENT REFERENCE OF THE REFERENCE OPERATION, JUST FOR RETURN FUND OPERATIONS
//				String originalPaymentReference = block4.getTagValue(SwiftConstants.FIELD_NAME_21);
//				parsedSwiftMessage.setOriginalReferencePayment(originalPaymentReference);
//
//				//FIELD 32A HAS 3 FIELDS: PAYMENT DATE(AAMMDD) - CURRENCY(3) - AMOUNT(14,2)
//				Field32A field32A = new Field32A(block4.getTagValue(SwiftConstants.FIELD_NAME_32A));
//				String paymentDate = field32A.getComponent(SwiftConstants.COMPONENT_NUMBER_PAYMENT_DATE);
//				String currency = field32A.getComponent(SwiftConstants.COMPONENT_NUMBER_CURRENCY);
//				String amount = field32A.getComponent(SwiftConstants.COMPONENT_NUMBER_AMOUNT);
//
//				parsedSwiftMessage.setSettlementDate(paymentDate);
//				parsedSwiftMessage.setCurrency(currency);
//				parsedSwiftMessage.setAmount(amount);
//
//				//FIELD 53A(THIS FIELD IS ONLY FOR DEPOSIT WHEN SENDER HAS NO SWIFT BIC CODE)
//				String field53 = block4.getTagValue(SwiftConstants.FIELD_NAME_53A);
//				if(field53 == null){
//					//IF THIS IS NULL ITS BECAUSE BIC SWIFT CODE OF BLOCK2 IF OF PARTICIPANT OR 
//					parsedSwiftMessage.setBeneficiaryBicSwiftCode(bicCodeSwiftSender);
//				}else{
//					//IF IT IS NOT NULL, BIC SWIFT CODE OF BLOCK2 IS OF BCRD AND 50K IS FROM PART. BUYER
//					parsedSwiftMessage.setBcrdBicSwiftCode(bicCodeSwiftSender);
//					int size = field53.length();
//					if(size == 12){
//						field53 = field53.substring(0,size-1);
//					}
//					parsedSwiftMessage.setBeneficiaryBicSwiftCode(field53);
//				}
//				//FIELD 70(THIS FIELD WILL HAVE THE ISSUER CODE, WHEN BCRD WANT TO DO A DEPOSIT FOR BENEFITS)
//				String field70 = block4.getTagValue(SwiftConstants.FIELD_NAME_70);
//				if(Validations.validateIsNotNull(field70)){
//					parsedSwiftMessage.setIssuer(field70.substring(0, 5));
//				}
//				messagesProcessed.add(parsedSwiftMessage);
//			}
//		}
//
//		return messagesProcessed;
//	}
//
//	/**
//	 * METHOD WILL DELETE TWO FIRST LINES OF EVERY FILE(THIS ARE NOT NECESARY FOR WIFE LIBRARY)
//	 * @param message
//	 * @return
//	 */
//	private synchronized static String deleteTwoFirstGroups(String message){
//		String finalMessage = StringUtils.EMPTY;
//		if(message.contains("{5")){
//			//IF MESSAGE CONTAINS '$', MEANS THAT THERE ARE MORE THAN ONE SWIFT MESSAGE TO PROCESS
//			while(message.contains("{5")){
//				int inMessageSize = message.length();
//				int inEndMessage = message.indexOf("{5");
//				String subMessage = message.substring(0,inEndMessage);
//				message = message.substring(inEndMessage,inMessageSize);
//
//				//WE PROCESS MESSAGE DELETING 2 FIRST GROUPS SEND IN SWIFT MESSAGE
//				int inEndGroup = subMessage.indexOf("}}");
//				int messageSize = subMessage.length();
//				finalMessage += subMessage.substring(inEndGroup+2, messageSize);
//				
//				//DELETE THE GROUP LINE 5
//				int totalSize = message.length();
//				inEndMessage = message.indexOf("{1");
//				if(inEndMessage != -1){
//					message = message.substring(inEndMessage,totalSize);
//				}else{
//					message = StringUtils.EMPTY;
//				}
//			}
//		}
//
//		return finalMessage;
//	}
//
//	/**
//	 * METHOD TO GET ALL RECEIPTED MESSAGE INTO SWIFT FORMAT
//	 * @param swiftMessages4
//	 * @return
//	 */
//	public static List<String> formatSwiftMessage(String message){
//		List<String> totalProcessedMessages = new ArrayList<String>();
//		String messageGroup4 = StringUtils.EMPTY;
//		String message4Processed = StringUtils.EMPTY;
//
//		if(message.length() > 0){
//			int totalSizeMessage = message.length();
//			int group4Location = message.indexOf("{4");//we get the position of the 3 first groups
//			int group4EndLocation = message.indexOf("-}");
//
//			while(totalSizeMessage > group4EndLocation && totalSizeMessage>0){
//				String messageGroup13 = message.substring(0,group4Location-2);//Swift Message 1-2-3 group
//				messageGroup4 = message.substring(group4Location-2,group4EndLocation+2);//Swift Message 4 group
//				message = message.substring(group4EndLocation+2, message.length());
//
//				message4Processed = convertGroup4SwiftFormat(messageGroup4.trim());//We give the correct format to Swift Message
//				totalSizeMessage = message.length();
//				group4Location = message.indexOf("{4");
//				group4EndLocation = message.indexOf("-}");
//
//				totalProcessedMessages.add(messageGroup13+message4Processed);
//			}
//		}
//
//		return totalProcessedMessages;
//	}
//
//	/**
//	 * METHOD TO CONVERT MESSAGE GROUP 4 IN SWIFT FORMAT
//	 * @param inputLine
//	 * @return
//	 */
//	public static String convertGroup4SwiftFormat(String iLine){
//		boolean firstSign=false;//verify the first symbol ':'
//		boolean secondSign=false;//verify the second symbol ':'
//		boolean firstAlterSign=true;//verify the first alter symbol ':', to set as '}'
//		boolean semiGroupAlterSign=true;//verify the symbol ':' not be changed in middle of group
//		boolean secondAlterSign=true;//verify the second alter symbol ':', to set as '{'
//
//		String outputLine = StringUtils.EMPTY;
//		String inputLine = StringUtils.EMPTY;
//
//		//FIRST OF ALL WE MUST DELETE ALL TAB SPACES FROM STRING
//		for(int i=0;i<=iLine.length()-1;i++){
//			Character c= iLine.charAt(i);
//			if(c.toString().equals("\t") || c.toString().equals("\r") || c.toString().equals("\n")){
//				continue;
//			}else{
//				inputLine = inputLine + c.toString();
//			}
//		}
//		//ONCE WE HAVE DELETE ALL TAB SPACES, WE CAN PROCESS FILE
//
//		inputLine= inputLine.replace("::", ":{");//We delete :: and get :{ to start giving correct Swift Message format
//		for(int i=0;i<=inputLine.length()-1;i++){
//			Character c= inputLine.charAt(i);
//			if(!secondAlterSign){
//				secondAlterSign=true;
//			}
//			if(c.toString().equals(":") && !firstSign){
//				firstSign=true;
//				outputLine=outputLine.concat(c.toString());
//			}else if(c.toString().equals(":") && !secondSign){
//				secondSign=true;
//				outputLine=outputLine.concat(":");
//			}else if(c.toString().equals(":") && firstAlterSign){
//				outputLine=outputLine.concat("}{");
//				firstAlterSign=false;
//			}else if(c.toString().equals(":") && secondAlterSign){
//				//verify not to change a symbol ':' in a semi-group
//				if(!semiGroupAlterSign){
//					outputLine=outputLine.concat("}{");
//					semiGroupAlterSign=true;
//				}else{
//					outputLine=outputLine.concat(":");
//					secondAlterSign=false;
//					semiGroupAlterSign=false;
//				}
//			}else{
//				outputLine=outputLine.concat(c.toString());
//			}
//		}
//
//		return outputLine+"}";
//	}
//
//	/**
//	 * THIS METHOD WILL DOWNLOAD TO A LOCAL TMP FILE ALL SWIFT MESSAGES
//	 * @param client
//	 * @return
//	 */
//	private synchronized static boolean downloadFiles(FTPClient client){
//		String filename = StringUtils.EMPTY;
//		File fileProcess = null;
//		FileOutputStream fos = null;
//		String curDir = System.getProperty("user.dir");//ACTUAL ROUTE
//		File file = new File(curDir+ftpProcessInput);//ROUTE OF THE DIR TO VERIFY IF EXISTS OR NOT
//		FTPFile[] ftpFiles;
//		try {
//			ftpFiles = client.listFiles();
//			if(ftpFiles != null && ftpFiles.length > 0){
//				for (int i=0; i< ftpFiles.length; i++) {//COPY ALL FILE TO LOCAL DIR EN SERVER APPLICATION
//					if (ftpFiles[i].getType() == FTPFile.FILE_TYPE && ftpFiles[i].isFile()) {
//						filename = ftpFiles[i].getName();//FILE NAME HOW IT WILL BE SAVED
//						if(file.exists()){//VERIFY IF DIR EXIST
//							fileProcess = new File(curDir+ftpProcessInput+"/"+filename);
//							if(!fileProcess.exists() && fileProcess.createNewFile()){
//								//IF IT EXISTS, WE MUST SAVE FILE IN THIS TEMPORAL DIR
//								fos = new FileOutputStream(curDir+ftpProcessInput+"/"+filename);
//								if(client.retrieveFile("/"+filename, fos)){
//									logger.info("Archivo descargado satisfactoriamente: "+ filename);
//									if(client.deleteFile("/"+filename)){
//										logger.info("Archivo eliminado satisfactoriamente: "+ filename);
//									}
//								}
//							}else{
//								continue;
//							}
//						}else{
//							//IF IT DOESNT EXIST, WE MUST CREATE IT
//							if(file.mkdir()){
//								fileProcess = new File(curDir+ftpProcessInput+"/"+filename);
//								if(!fileProcess.exists() && fileProcess.createNewFile()){
//									//WE SAVE FILE IN LOCAL TEMPORAL DIR
//									fos = new FileOutputStream(curDir+ftpProcessInput+"/"+filename);
//									if(client.retrieveFile("/"+filename, fos)){
//										logger.debug("Archivo descargado satisfactoriamente: "+ filename);
//										if(client.deleteFile("/"+filename)){
//											logger.debug("Archivo eliminado satisfactoriamente: "+ filename);
//										}
//									}
//								}else{
//									continue;
//								}
//							}
//						}
//						fos.close();
//					}
//				}
//				client.disconnect();
//			}else{
//				return false;
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
//		return true;
//	}
//
//	private void depositMessageDir(String fileContent, String fileName, boolean successfulProcessed) throws SocketException, IOException, ServiceException{
//		downloadFileLocally(fileContent,fileName,successfulProcessed);
//
//		uploadProcessedFile(fileName,successfulProcessed);
//	}
//
//	private synchronized void downloadFileLocally(String message, String fileName, boolean successfulProcessed) throws FileNotFoundException, IOException{
//
//		String curDir = System.getProperty("user.dir");
//		String pattern = null;
//		if(successfulProcessed){
//			pattern = CommonsUtilities.convertDatetoStringOptional(CommonsUtilities.currentDateOptional()) + SwiftConstants.ACCEPTED_PROCESS;
//		}else{
//			pattern = CommonsUtilities.convertDatetoStringOptional(CommonsUtilities.currentDateOptional()) + SwiftConstants.FILE_TO_PROCESS;
//		}
//		File file = new File(curDir + ftpProcessInput);
//		File fileDownload = new File(curDir + ftpProcessInput + "/" + fileName);
//
//		if(file.exists()){
//			//IF IT EXISTS WE CREATE THE FILE
//			if(!fileDownload.exists() && fileDownload.createNewFile()){
//				if(fileDownload.canWrite()){
//					OutputStream os = new FileOutputStream(fileDownload);
//					byte[] data = message.getBytes();
//					int bit = 256;
//					for (int i = 0; i < data.length; i++) {
//						bit = data[i];
//						os.write(bit);
//					}
//					os.flush();
//					os.close();
//				}
//			}
//		}else{
//			//IF IT DOESNT EXIST, WE MUST CREATE IT
//			if(file.mkdir()){
//				if(!fileDownload.exists() && fileDownload.createNewFile()){
//					if(fileDownload.canWrite()){
//						OutputStream os = new FileOutputStream(fileDownload);
//						byte[] data = message.getBytes();
//						int bit = 256;
//						for (int i = 0; i < data.length; i++) {
//							bit = data[i];
//							os.write(bit);
//						}
//						os.flush();
//						os.close();
//					}
//				}
//			}
//		}
//	}
//
//	private synchronized void uploadProcessedFile(String fileName,boolean successfulProcessed) throws SocketException, IOException, ServiceException{
//		FTPClient client = new FTPClient();
//		String curDir = System.getProperty("user.dir");//ACTUAL ROUTE
//		String pattern = null;
//		if(successfulProcessed){
//			pattern = CommonsUtilities.convertDatetoStringOptional(CommonsUtilities.currentDateOptional()) + SwiftConstants.ACCEPTED_PROCESS;
//		}else{
//			pattern = CommonsUtilities.convertDatetoStringOptional(CommonsUtilities.currentDateOptional()) + SwiftConstants.FILE_TO_PROCESS;
//		}
//		File directory = new File(curDir + "/" + pattern);
//		File[] files = directory.listFiles();
//
//		if(files != null && files.length > 0){
//			//WE GET PARAMETERS CONNECTION
//			FTPParameters parameters = getParametersConnection();
//			//ONCE WE HAVE VERIFIED EXIST FILES TO UPLOAD, WE OPEN CONNECTION
//			client.connect(parameters.getServer(),21);
//			if(client.login(parameters.getUser(), parameters.getPass())){
//				int reply = client.getReplyCode();
//				if (FTPReply.isPositiveCompletion(reply)){
//					for (int i=0; i< files.length; i++) {
//						File file = files[i];
//						if(file.isFile()){
//							BufferedInputStream buffIn = new BufferedInputStream(new FileInputStream(file));
//							if(client.changeWorkingDirectory("/" + pattern)){
//								client.enterLocalPassiveMode();
//								if(client.storeFile(fileName, buffIn)){
//									buffIn.close();
//								}else{
//									throw new ServiceException(	ErrorServiceType.SWIFT_PROCESS_FTP_UPLOAD_TEMPORAL_DIR_PROBLEM,
//											"No se pudo guardar el archivo: " + file.getName() + " a la carpeta temporal del FTP server");
//								}
//							}else{
//								if(client.makeDirectory("/" + pattern)){
//									if(client.changeWorkingDirectory("/" + pattern)){
//										if(client.storeFile(fileName, buffIn)){
//											buffIn.close();
//										}else{
//											throw new ServiceException(	ErrorServiceType.SWIFT_PROCESS_FTP_UPLOAD_TEMPORAL_DIR_PROBLEM,
//													"No se pudo guardar el archivo: " + file.getName() + " a la carpeta temporal del FTP server");
//										}
//									}
//								}
//							}
//							if(file.delete()){
//								log.info("Archivo: " + file.getName() + " fue eliminado de la carpeta temporal luego de ser subido al FTP");
//							}
//						}
//					}
//				}
//				client.logout();
//			}else{
//				throw new ServiceException(	ErrorServiceType.SWIFT_PROCESS_FTP_CONNECTION_PROBLEM,
//											"No se pudo conectar al servidor FTP para los parametros configurados" );
//			}
//			client.disconnect();
//		}
//	}
//
//	public FTPParameters getParametersConnection(){
//		FTPParameters parameters = new FTPParameters();
//		parameters.setLocalPath(ftpLocalPath);
//		parameters.setPass(ftpPass);
//		parameters.setRemotePath(ftpRemotePath);
//		parameters.setServer(ftpServer);
//		parameters.setUser(ftpUser);
//		return parameters;
//	}
//
//	private void deleteLocalFile(String fileName){
//		String curDir = System.getProperty("user.dir");//ACTUAL ROUTE
//		File directory = new File(curDir + ftpProcessInput);
//		File[] files = directory.listFiles();
//		if(Validations.validateIsNotNull(files)){
//			for(int i=0; i< files.length; i++){
//				File file = files[i];
//				if(file.isFile() && file.getName().equals(fileName)){
//					if(file.delete()){
//						log.info("Archivo: " + fileName + " fue eliminado de la carpeta temporal en el servidor por error en el mensaje");
//					}
//				}
//			}
//		}
//	}
}