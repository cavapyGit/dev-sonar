package com.pradera.funds.automaticinterface.batch;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExlackSecurOperFundReportBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="ExlackSecurOperFundReportBatch")
@RequestScoped
public class ExlackSecurOperFundReportBatch implements Serializable, JobExecution  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		try {			
			SettlementReportTO filter = new SettlementReportTO();
			filter.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			filter.setScheduleType(SettlementScheduleType.FIRST_SETTLEMENT.getCode().toString());
			filter.setCurrency(CurrencyType.PYG.getCode().toString());
			List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.exlackSecurOperationsFundQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.EXLACK_SECURITY_OPER_FUND_BAG.getCode());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
