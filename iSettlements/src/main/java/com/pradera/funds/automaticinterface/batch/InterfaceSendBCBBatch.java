package com.pradera.funds.automaticinterface.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.utils.ExtInterfaceConstanst;
import com.pradera.settlements.utils.InterfaceSendTO;

// TODO: Auto-generated Javadoc
/**
 * The Class InterfaceSendBCBBatch.
 */
@BatchProcess(name="InterfaceSendBCBBatch")
@RequestScoped
public class InterfaceSendBCBBatch implements JobExecution {

	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The Constant TRANSFER_BCB_BVD. */
	private static final Long TRANSFER_BCB_BVD = new Long(1030);
	
	/** The Constant TRANSFER_BCB_BEM. */
	private static final Long TRANSFER_BCB_BEM = new Long(1031);
	
	/** The Constant TRANSFER_BCB_TEM. */
	private static final Long TRANSFER_BCB_TEM = new Long(1032);			
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The batch process service facade. */
	@EJB 
	BatchProcessServiceFacade batchProcessServiceFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		logger.info("INICIO ::::::: InterfaceSendBCBBatch ");
		try {
			
			InterfaceSendTO objInterfaceSendTO = new InterfaceSendTO();		
			objInterfaceSendTO.setInterfaceId(TRANSFER_BCB_BVD);
			objInterfaceSendTO.setSendFtp(Boolean.TRUE);	
			BusinessProcess businessProcess = new BusinessProcess();
			Map<String, Object> parameters = new HashMap<String, Object>();
			
			parameters.put(ExtInterfaceConstanst.INTERFACE_PK, objInterfaceSendTO.getInterfaceId());							
			parameters.put(ExtInterfaceConstanst.INITIAL_DATE, CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), GeneralConstants.DATE_FORMAT_PATTERN));
			parameters.put(ExtInterfaceConstanst.FINAL_DATE, CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), GeneralConstants.DATE_FORMAT_PATTERN));	
			parameters.put(ExtInterfaceConstanst.SEND_FTP, objInterfaceSendTO.isSendFtp());			
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());
			batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), businessProcess, parameters);			
			
			objInterfaceSendTO = new InterfaceSendTO();
			objInterfaceSendTO.setInterfaceId(TRANSFER_BCB_BEM);
			objInterfaceSendTO.setSendFtp(Boolean.TRUE);			
			businessProcess = new BusinessProcess();
			parameters = new HashMap<String, Object>();
			parameters.put(ExtInterfaceConstanst.INTERFACE_PK, objInterfaceSendTO.getInterfaceId());							
			parameters.put(ExtInterfaceConstanst.INITIAL_DATE, CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), GeneralConstants.DATE_FORMAT_PATTERN));
			parameters.put(ExtInterfaceConstanst.FINAL_DATE, CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), GeneralConstants.DATE_FORMAT_PATTERN));
			parameters.put(ExtInterfaceConstanst.SEND_FTP, objInterfaceSendTO.isSendFtp());			
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());
			batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), businessProcess, parameters);						
			
			objInterfaceSendTO = new InterfaceSendTO();
			objInterfaceSendTO.setInterfaceId(TRANSFER_BCB_TEM);
			objInterfaceSendTO.setSendFtp(Boolean.TRUE);
			businessProcess = new BusinessProcess();
			parameters = new HashMap<String, Object>();			
			parameters.put(ExtInterfaceConstanst.INTERFACE_PK, objInterfaceSendTO.getInterfaceId());							
			parameters.put(ExtInterfaceConstanst.INITIAL_DATE, CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), GeneralConstants.DATE_FORMAT_PATTERN));
			parameters.put(ExtInterfaceConstanst.FINAL_DATE, CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), GeneralConstants.DATE_FORMAT_PATTERN));	
			parameters.put(ExtInterfaceConstanst.SEND_FTP, objInterfaceSendTO.isSendFtp());			
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXTERNAL_INTERFACE_BUSINESS_PROCESS.getCode());
			batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), businessProcess, parameters);
			
		} catch (Exception e) {	
			
			e.printStackTrace();
			logger.error("Error :::::::::::: InterfaceSendBCBBatch");
			
			String messageNotification = null;
			if (e instanceof ServiceException) {
				ServiceException se = (ServiceException) e;
				messageNotification = PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage(), se.getParams(), new Locale("es"));
			} else {
				messageNotification = "OCURRIO UN ERROR EN EL PROCESO DE GENERACION DE INTERFACES EXTERNAS BVD, BEM, TEM AL BIC";
			}						
			UserFilterTO  userFilter = new UserFilterTO();
			userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);						
			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, messageNotification, NotificationType.EMAIL);	
		}
		
		logger.info("FIN ::::::: InterfaceSendBCBBatch ");		
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
