package com.pradera.funds.automaticinterface.batch;

import java.io.Serializable;
import java.util.Currency;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.batchprocess.service.ManagerParameter;
import com.pradera.funds.fundsoperations.deposit.facade.ManageDepositServiceFacade;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AutomaticReceptionLipBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="AutomaticReceptionLipBatch")
@RequestScoped
public class AutomaticReceptionLipBatch implements Serializable, JobExecution  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The manage deposit service facade. */
	@EJB
	ManageDepositServiceFacade manageDepositServiceFacade;
	/** The manager parameter. */
	@Inject
	ManagerParameter managerParameter;	

	/* (non-Javadoc)	
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		try {
			
			String currencyType = null;
			//processLogger.getProcessLoggerDetails().get(0)
			for(ProcessLoggerDetail processLoggerDetail : processLogger.getProcessLoggerDetails()){
				if(processLoggerDetail != null && processLoggerDetail.getParameterName() != null 
						&& processLoggerDetail.getParameterName().length() > 0 
						&& "currencyType".equals(processLoggerDetail.getParameterName())){
					currencyType = processLoggerDetail.getParameterValue();
				}
			}
			
			Date currentDate = CommonsUtilities.currentDate();						
			manageDepositServiceFacade.sendDepositLip(currentDate, currencyType);							
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
