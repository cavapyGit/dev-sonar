package com.pradera.funds.automaticinterface.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.reports.SettlementProcessReportsSender;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class EdvComissionSettlementReportBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="EdvComissionSettlementReportBatch")
@RequestScoped
public class EdvComissionSettlementReportBatch implements Serializable, JobExecution  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		try {			
			SettlementReportTO filter = new SettlementReportTO();
			filter.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			filter.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.edvComissionSettlementOperQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.CSDCORE_COMISSION_SETTLEMENT_OPERATIONS.getCode());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
