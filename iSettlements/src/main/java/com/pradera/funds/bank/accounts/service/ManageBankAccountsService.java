package com.pradera.funds.bank.accounts.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.funds.bank.accounts.to.SearchBankAccountsTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.type.BankAccountType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageBankAccountsService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class ManageBankAccountsService extends CrudDaoServiceBean{
	
	/**
	 * Get List Bank.
	 *
	 * @param idTypeBank type bank
	 * @param idState the id state
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Bank> getListBank(Integer idTypeBank,Integer idState)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	where 1=1 ");
			if(Validations.validateIsNotNull(idTypeBank))
				sbQuery.append(" and cs.bankType = :bankType ");
			if(Validations.validateIsNotNull(idState))
				sbQuery.append(" AND cs.state = :state");
			sbQuery.append(" order by cs.description ");
			Query query = em.createQuery(sbQuery.toString()); 
			if(Validations.validateIsNotNull(idTypeBank))
				query.setParameter("bankType",  idTypeBank);
			if(Validations.validateIsNotNull(idState))
				query.setParameter("state",  BankStateType.REGISTERED.getCode());
			
			
			List<Bank> bank = (List<Bank>)query.getResultList();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get List Bank.
	 *
	 * @param idTypeBank type bank
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank getBankCentralizing(Integer idTypeBank)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	WHERE cs.bankType = :bankType AND cs.state = :state");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("bankType",  idTypeBank);
			query.setParameter("state",  BankStateType.REGISTERED.getCode());
			Bank bank = (Bank)query.getSingleResult();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Find Issuer Facade.
	 *
	 * @param idIssuer id issuer
	 * @return issuer
	 * @throws ServiceException the Service Exception
	 */
	public Issuer findIssuer(String idIssuer)throws ServiceException{
		try{
		return find(Issuer.class, idIssuer);
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Institution Bank Account For Currency.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idBank the id bank
	 * @param idCurrency id currency
	 * @return Institution Bank Account
	 * @throws ServiceException  the Service Exception
	 */
	public InstitutionBankAccount getInstitutionBankAccountForCurrency(Integer institution,String idInstitution,Long idBank,Integer idCurrency)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	WHERE cs.currency = :currency AND cs.providerBank.idBankPk = :idBankProviderPk AND cs.state = :state ");
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				sbQuery.append("  AND cs.issuer.idIssuerPk = :idIssuerPk ");
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				sbQuery.append("  AND cs.participant.idParticipantPk = :idParticipantPk ");
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				sbQuery.append("  AND cs.bank.idBankPk = :idBankPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("currency",  idCurrency);
			query.setParameter("idBankProviderPk",  idBank);
			query.setParameter("state",  BankAccountsStateType.REGISTERED.getCode());
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				query.setParameter("idIssuerPk",  idInstitution);
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				query.setParameter("idParticipantPk",  new Long(idInstitution));
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				query.setParameter("idBankPk",  new Long(idInstitution));
			InstitutionBankAccount institutionBankAccount = (InstitutionBankAccount)query.getSingleResult();			
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * getInstitutionBankAccountForAccounu Number And Currency.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idBank the id bank
	 * @param number number account
	 * @param idCurrency id currency
	 * @return Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount getInstitutionBankAccountForAccountNumberAndCurrency(Integer institution,String idInstitution,Long idBank,String number , Integer idCurrency)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	WHERE cs.currency = :currency AND cs.accountNumber = :accountNumber AND cs.providerBank.idBankPk = :idBankProviderPk AND cs.state = :state ");
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				sbQuery.append("  AND cs.issuer.idIssuerPk = :idIssuerPk ");
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				sbQuery.append("  AND cs.participant.idParticipantPk = :idParticipantPk ");
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				sbQuery.append("  AND cs.bank.idBankPk = :idBankPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("currency",  idCurrency);
			query.setParameter("accountNumber",  number);
			query.setParameter("idBankProviderPk",  idBank);
			query.setParameter("state",  BankAccountsStateType.REGISTERED.getCode());
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				query.setParameter("idIssuerPk",  idInstitution);
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				query.setParameter("idParticipantPk",  new Long(idInstitution));
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				query.setParameter("idBankPk",  new Long(idInstitution));
			InstitutionBankAccount institutionBankAccount = (InstitutionBankAccount)query.getSingleResult();			
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Registry Bank Accounts Facade.
	 *
	 * @param institutionBankAccount institution Bank Account
	 * @param loggerUser the logger user
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount registryBankAccountsFacade(InstitutionBankAccount institutionBankAccount,LoggerUser loggerUser)throws ServiceException{
		try{
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idBankPkParam", institutionBankAccount.getProviderBank().getIdBankPk());
			Bank bank = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramat);
			if(!bank.getState().equals(BankStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
			}
									
			if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode())){
				institutionBankAccount.setParticipant(null);
				institutionBankAccount.setBank(null);
				Map<String,Object> paramatAu= new HashMap<String,Object>();		
				paramatAu.put("idIssuerPkParam", institutionBankAccount.getIssuer().getIdIssuerPk());
				Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, paramatAu);
				if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
				}
			}
			else{
				if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
					institutionBankAccount.setIssuer(null);
					institutionBankAccount.setBank(null);
					Map<String,Object> paramatA= new HashMap<String,Object>();		
					paramatA.put("idParticipantPkParam", institutionBankAccount.getParticipant().getIdParticipantPk());
					Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramatA);
					if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
					}
				}
				else{
					if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					{
						institutionBankAccount.setParticipant(null);
						institutionBankAccount.setIssuer(null);
						Map<String,Object> paramatAux= new HashMap<String,Object>();		
						paramatAux.put("idBankPkParam", institutionBankAccount.getBank().getIdBankPk());
						Bank bankAux = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramatAux);
						if(!bankAux.getState().equals(BankStateType.REGISTERED.getCode())){
							throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
						}
					}
				}
			}
			 if(institutionBankAccount.getBankType().equals(BankType.CENTRALIZING.getCode())){
				 String idInstitution=null;
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
				 InstitutionBankAccount institutionBankAccountForCurrency = getInstitutionBankAccountForCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getCurrency());
				 //Varias cuentas bancarias banco centralizador
//				 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForCurrency))
//					 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				 
			 }
			 else{
				 
				 if(institutionBankAccount.getBankType().equals(BankType.COMMERCIAL.getCode())){
					 String idInstitution=null;
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
						 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
						 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
						 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
					 InstitutionBankAccount institutionBankAccountForAccountNumberAndCurrency = getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getAccountNumber(), institutionBankAccount.getCurrency());
					 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForAccountNumberAndCurrency))
						 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
					 
					 
				 }
			 }
			 
			 institutionBankAccount.setState(BankAccountsStateType.REGISTERED.getCode());
			 institutionBankAccount.setRegistryUser(loggerUser.getUserName());
			 institutionBankAccount.setRegistryDate(CommonsUtilities.currentDateTime());
			 create(institutionBankAccount);
			 return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Modify Bank Accounts Facade.
	 *
	 * @param institutionBankAccount  institution Bank Account
	 * @param loggerUser the logger user
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount modifyBankAccountsFacade(InstitutionBankAccount institutionBankAccount,LoggerUser loggerUser)throws ServiceException{
		try{
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idBankPkParam", institutionBankAccount.getProviderBank().getIdBankPk());
			Bank bank = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramat);
			if(!bank.getState().equals(BankStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
			}										
			if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode())){
				institutionBankAccount.setParticipant(null);
				institutionBankAccount.setBank(null);
				Map<String,Object> paramatAu= new HashMap<String,Object>();		
				paramatAu.put("idIssuerPkParam", institutionBankAccount.getIssuer().getIdIssuerPk());
				Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, paramatAu);
				if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
				}
			}
			else{
				if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
					institutionBankAccount.setIssuer(null);
					institutionBankAccount.setBank(null);
					Map<String,Object> paramatA= new HashMap<String,Object>();		
					paramatA.put("idParticipantPkParam", institutionBankAccount.getParticipant().getIdParticipantPk());
					Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramatA);
					if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
					}
				}
				else{
					if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					{
						institutionBankAccount.setParticipant(null);
						institutionBankAccount.setIssuer(null);
						Map<String,Object> paramatAux= new HashMap<String,Object>();		
						paramatAux.put("idBankPkParam", institutionBankAccount.getBank().getIdBankPk());
						Bank bankAux = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramatAux);
						if(!bankAux.getState().equals(BankStateType.REGISTERED.getCode())){
							throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
						}
					}
				}
			}
			 if(institutionBankAccount.getBankType().equals(BankType.CENTRALIZING.getCode())){
				 String idInstitution=null;
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
				 InstitutionBankAccount institutionBankAccountForCurrency = getInstitutionBankAccountForCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getCurrency());
				 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForCurrency)){
					 if(!(institutionBankAccountForCurrency.getIdInstitutionBankAccountPk().equals(institutionBankAccount.getIdInstitutionBankAccountPk())))
						 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				 }			 
			 }
			 else{
				 if(institutionBankAccount.getBankType().equals(BankType.COMMERCIAL.getCode())){
					 String idInstitution=null;
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
						 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
						 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
						 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
					 InstitutionBankAccount institutionBankAccountForAccountNumberAndCurrency = getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getAccountNumber(), institutionBankAccount.getCurrency());
					 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForAccountNumberAndCurrency)){
						 if(!(institutionBankAccountForAccountNumberAndCurrency.getIdInstitutionBankAccountPk().equals(institutionBankAccount.getIdInstitutionBankAccountPk())))
							 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
					 }					
				 }
			 }
			 InstitutionBankAccount institutionBankAccountID = find(InstitutionBankAccount.class, institutionBankAccount.getIdInstitutionBankAccountPk());
				if(institutionBankAccountID.getLastModifyDate().compareTo(institutionBankAccount.getLastModifyDate())==1)
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);		
			 update(institutionBankAccount);
			 return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get List Bank Accounts Filter.
	 *
	 * @param searchBankAccountsTO search Bank Accounts
	 * @return List Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	@SuppressWarnings("unchecked")
	public List <InstitutionBankAccount> getListBankAccountsFilter(SearchBankAccountsTO searchBankAccountsTO)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();	
			stringBuilder.append("	Select cs.idInstitutionBankAccountPk,cs.accountNumber,	");//0,1,
			stringBuilder.append("	cb.description,(Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.bankAcountType),	"); //2,3
			stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.currency),	"); //4	
			stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = cs.state),	"); //5
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
				stringBuilder.append("	par.description,  	"); //6
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
				stringBuilder.append("	iss.businessName,   "); //6
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				stringBuilder.append("	ban.description,   "); //6
			stringBuilder.append("	cs.state,	"); //7
			stringBuilder.append("	cs.currency,	"); //8
			stringBuilder.append("	cs.bankAcountType	"); //9
			stringBuilder.append("	from InstitutionBankAccount cs");				
			stringBuilder.append("	join cs.providerBank cb	");
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
				stringBuilder.append("	join cs.participant par	");
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
				stringBuilder.append("	join cs.issuer iss	");
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				stringBuilder.append("	join cs.bank ban	");
			stringBuilder.append("	Where 1 = 1	");	
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()) && Validations.validateIsNotNull(searchBankAccountsTO.getIssuer())){
				if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIssuer().getIdIssuerPk())){
					stringBuilder.append("	AND	cs.issuer.idIssuerPk = :idIssuerPk	");
					parameters.put("idIssuerPk", searchBankAccountsTO.getIssuer().getIdIssuerPk());							
				}
			}
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
				if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIdParticipantPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getIdParticipantPk())))){
					stringBuilder.append("	AND	cs.participant.idParticipantPk = :idParticipantPk	");
					parameters.put("idParticipantPk", searchBankAccountsTO.getIdParticipantPk());							
				}
			}	
//			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
//				if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIdBankInstitutionPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getIdBankInstitutionPk())))){
//					stringBuilder.append("	AND	cs.bank.idBankPk = :idBankPk	");
//					parameters.put("idBankPk", searchBankAccountsTO.getIdBankInstitutionPk());
//				}
//			}
			if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode())){
				if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIdBankInstitutionPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getIdBankInstitutionPk())))){
					stringBuilder.append("	AND	cs.bank.idBankPk = :idBankPk	");
					parameters.put("idBankPk", searchBankAccountsTO.getIdBankInstitutionPk());							
				}
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIdBankProviderPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getIdBankProviderPk())))){
				stringBuilder.append("	AND	cb.idBankPk = :idBankProviderPk	");
				parameters.put("idBankProviderPk", searchBankAccountsTO.getIdBankProviderPk());							
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getAccountNumber())){
				stringBuilder.append("	AND	cs.accountNumber = :accountNumber	");
				parameters.put("accountNumber", searchBankAccountsTO.getAccountNumber());							
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getBankType())){
				stringBuilder.append("	AND	cb.bankType = :bankType	");
				parameters.put("bankType", searchBankAccountsTO.getBankType());							
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getBankAccountType())){
				stringBuilder.append("	AND	cs.bankAcountType = :bankAcountType	");
				parameters.put("bankAcountType", searchBankAccountsTO.getBankAccountType().longValue());							
			}
			
			if(Validations.validateIsNotNull(searchBankAccountsTO.getState()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchBankAccountsTO.getState())))){
				stringBuilder.append("	AND	cs.state = :state	");
				parameters.put("state", searchBankAccountsTO.getState());							
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getInitialDate())){
				stringBuilder.append(" and  TRUNC(cs.registryDate) >= :dateIni ");
				parameters.put("dateIni", searchBankAccountsTO.getInitialDate());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getFinalDate())){
				stringBuilder.append(" and  TRUNC(cs.registryDate) <= :dateEnd ");
				parameters.put("dateEnd", searchBankAccountsTO.getFinalDate());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getCurrency())){
				stringBuilder.append("	AND	cs.currency = :currency	");
				parameters.put("currency", searchBankAccountsTO.getCurrency());							
			}
			stringBuilder.append("	order by cs.idInstitutionBankAccountPk desc	");	
			List<Object[]> lstInstitutionBankAccount =  findListByQueryString(stringBuilder.toString(), parameters);
			Map<Long,InstitutionBankAccount> institutionBankAccountPk = new HashMap<Long, InstitutionBankAccount>();
			for(Object[] object: lstInstitutionBankAccount){
				InstitutionBankAccount institutionBankAccount = new InstitutionBankAccount();
				institutionBankAccount.setIdInstitutionBankAccountPk(new Long(object[0].toString()));
				if(Validations.validateIsNotNull((object[1])))
					institutionBankAccount.setAccountNumber(object[1].toString());
				if(Validations.validateIsNotNull((object[2])))
					institutionBankAccount.setDescriptionBankClient(object[2].toString());
				if(Validations.validateIsNotNull((object[3])))
					institutionBankAccount.setDescriptionBankAcountType(object[3].toString());
				if(Validations.validateIsNotNull((object[4])))
					institutionBankAccount.setDescriptionCurrency((object[4].toString()));
				if(Validations.validateIsNotNull((object[5])))
					institutionBankAccount.setStateDescription(object[5].toString());
				if(Validations.validateIsNotNull((object[6]))){
					if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
						institutionBankAccount.setDescriptionInstitution(InstitutionBankAccountsType.PARTICIPANT.getValue()+" - "+object[6].toString());	
					if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
						institutionBankAccount.setDescriptionInstitution(InstitutionBankAccountsType.ISSUER.getValue()+" - "+object[6].toString());	
					if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
						institutionBankAccount.setDescriptionInstitution(InstitutionBankAccountsType.BANK.getValue()+" - "+object[6].toString());	
				}
				if(Validations.validateIsNotNull((object[7])))	
					institutionBankAccount.setState(new Integer(object[7].toString()));
				if(Validations.validateIsNotNull((object[8])))	
					institutionBankAccount.setCurrency(new Integer(object[8].toString()));
				if(Validations.validateIsNotNull((object[9])))	
					institutionBankAccount.setBankAcountType(new Long(object[9].toString()));
				institutionBankAccountPk.put(institutionBankAccount.getIdInstitutionBankAccountPk(), institutionBankAccount);
			}
			return new ArrayList<InstitutionBankAccount>(institutionBankAccountPk.values());
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Bank Accounts For Id.
	 *
	 * @param idInstitutionBankAccount id Institution Bank Account
	 * @return Institution Bank Account
	 * @throws ServiceException Institution Bank Account
	 */
	public InstitutionBankAccount getBankAccountsForId(Long idInstitutionBankAccount)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	"
					+ " INNER JOIN FETCH cs.providerBank "
					+ "WHERE cs.idInstitutionBankAccountPk = :idInstitutionBankAccountPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("idInstitutionBankAccountPk",  idInstitutionBankAccount);
			InstitutionBankAccount institutionBankAccount = (InstitutionBankAccount)query.getSingleResult();	
			if(Validations.validateIsNotNull(institutionBankAccount)){
				if(Validations.validateIsNotNull(institutionBankAccount.getProviderBank())){
					Bank bankCentralizing = getBankCentralizing(BankType.CENTRALIZING.getCode());
					if(bankCentralizing.getIdBankPk().equals(institutionBankAccount.getProviderBank().getIdBankPk()))
						institutionBankAccount.setBankType(BankType.CENTRALIZING.getCode());
					else institutionBankAccount.setBankType(BankType.COMMERCIAL.getCode());
					institutionBankAccount.getProviderBank().getDescription();
				}
				if(Validations.validateIsNotNull(institutionBankAccount.getParticipant())){
					institutionBankAccount.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
					institutionBankAccount.getParticipant().getDescription();
				}
				if(Validations.validateIsNotNull(institutionBankAccount.getIssuer())){
					institutionBankAccount.setInstitutionType(InstitutionBankAccountsType.ISSUER.getCode());
					institutionBankAccount.getIssuer().getDescription();
				}	
				if(Validations.validateIsNotNull(institutionBankAccount.getBank())){
					institutionBankAccount.setInstitutionType(InstitutionBankAccountsType.BANK.getCode());
					institutionBankAccount.getBank().getDescription();
				}	
			}			
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Block Bank Accounts Facade.
	 *
	 * @param institutionBankAccount institution Bank Account
	 * @param loggerUser the logger user
	 * @return  institution Bank Account
	 * @throws ServiceException the service exception
	 */
	public InstitutionBankAccount blockBankAccountsFacade(InstitutionBankAccount institutionBankAccount,LoggerUser loggerUser)throws ServiceException{
		try{
		Map<String,Object> paramat= new HashMap<String,Object>();		
		paramat.put("idBankPkParam", institutionBankAccount.getProviderBank().getIdBankPk());
		Bank bank = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramat);
		if(!bank.getState().equals(BankStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
		}	
						
		if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode())){
			institutionBankAccount.setParticipant(null);
			institutionBankAccount.setBank(null);
			Map<String,Object> paramatAu= new HashMap<String,Object>();		
			paramatAu.put("idIssuerPkParam", institutionBankAccount.getIssuer().getIdIssuerPk());
			Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, paramatAu);
			if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
			}
		}
		else{
			if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
				institutionBankAccount.setIssuer(null);
				institutionBankAccount.setBank(null);
				Map<String,Object> paramatA= new HashMap<String,Object>();		
				paramatA.put("idParticipantPkParam", institutionBankAccount.getParticipant().getIdParticipantPk());
				Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramatA);
				if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
				}
			}
			else{
				if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				{
					institutionBankAccount.setParticipant(null);
					institutionBankAccount.setIssuer(null);
					Map<String,Object> paramatAux= new HashMap<String,Object>();		
					paramatAux.put("idBankPkParam", institutionBankAccount.getBank().getIdBankPk());
					Bank bankAux = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramatAux);
					if(!bankAux.getState().equals(BankStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
					}
				}
			}
		}
		 if(institutionBankAccount.getBankType().equals(BankType.CENTRALIZING.getCode())){
			 String idInstitution=null;
			 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
				 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
			 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
				 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
			 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
			 InstitutionBankAccount institutionBankAccountForCurrency = getInstitutionBankAccountForCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getCurrency());
			 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForCurrency)){
				 if(!(institutionBankAccountForCurrency.getIdInstitutionBankAccountPk().equals(institutionBankAccount.getIdInstitutionBankAccountPk())))
					 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			 }			 
		 }
		 else{
			 if(institutionBankAccount.getBankType().equals(BankType.COMMERCIAL.getCode())){
				 String idInstitution=null;
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
				 InstitutionBankAccount institutionBankAccountForAccountNumberAndCurrency = getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getAccountNumber(), institutionBankAccount.getCurrency());
				 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForAccountNumberAndCurrency)){
					 if(!(institutionBankAccountForAccountNumberAndCurrency.getIdInstitutionBankAccountPk().equals(institutionBankAccount.getIdInstitutionBankAccountPk())))
						 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				 }					
			 }
		 }
		 Map<String,Object> mapParams = new HashMap<String, Object>();
		 mapParams.put("idBankAccountPkParam", institutionBankAccount.getIdInstitutionBankAccountPk());
		 InstitutionBankAccount institutionBankAccountAux = findObjectByNamedQuery(InstitutionBankAccount.BANK_ACCOUNT_STATE, mapParams);
		 if(!BankAccountsStateType.REGISTERED.getCode().equals(institutionBankAccountAux.getState())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		 }
		 InstitutionBankAccount InstitutionBankAccountUpdate = find(InstitutionBankAccount.class, institutionBankAccount.getIdInstitutionBankAccountPk());
		 InstitutionBankAccountUpdate.setState(BankAccountsStateType.BLOCKED.getCode());
		 InstitutionBankAccountUpdate.setBlockMotive(institutionBankAccount.getBlockMotive());
		 InstitutionBankAccountUpdate.setBlockOtherMotive(institutionBankAccount.getBlockOtherMotive());
		 InstitutionBankAccountUpdate.setBlockUser(loggerUser.getUserName());
		 InstitutionBankAccountUpdate.setBlockDate(CommonsUtilities.currentDateTime());
		
		update(InstitutionBankAccountUpdate);
		
		return InstitutionBankAccountUpdate;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Unblock  institution Bank Account.
	 *
	 * @param institutionBankAccount  institution Bank Account
	 * @param loggerUser the logger user
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount unblockBankAccountsFacade(InstitutionBankAccount institutionBankAccount,LoggerUser loggerUser)throws ServiceException{
		try{
		Map<String,Object> paramat= new HashMap<String,Object>();		
		paramat.put("idBankPkParam", institutionBankAccount.getProviderBank().getIdBankPk());
		Bank bank = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramat);
		if(!bank.getState().equals(BankStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
		}							
		if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode())){
			institutionBankAccount.setParticipant(null);
			institutionBankAccount.setBank(null);
			Map<String,Object> paramatAu= new HashMap<String,Object>();		
			paramatAu.put("idIssuerPkParam", institutionBankAccount.getIssuer().getIdIssuerPk());
			Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, paramatAu);
			if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
			}
		}
		else{
			if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
				institutionBankAccount.setIssuer(null);
				institutionBankAccount.setBank(null);
				Map<String,Object> paramatA= new HashMap<String,Object>();		
				paramatA.put("idParticipantPkParam", institutionBankAccount.getParticipant().getIdParticipantPk());
				Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramatA);
				if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
				}
			}else{
				if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				{
					institutionBankAccount.setParticipant(null);
					institutionBankAccount.setIssuer(null);
					Map<String,Object> paramatAux= new HashMap<String,Object>();		
					paramatAux.put("idBankPkParam", institutionBankAccount.getBank().getIdBankPk());
					Bank bankAux = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramatAux);
					if(!bankAux.getState().equals(BankStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
					}
				}
			}
		}
		 if(institutionBankAccount.getBankType().equals(BankType.CENTRALIZING.getCode())){
			 String idInstitution=null;
			 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
				 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
			 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
				 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
			 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
				 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
			 InstitutionBankAccount institutionBankAccountForCurrency = getInstitutionBankAccountForCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getCurrency());
			 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForCurrency)){
				 if(!(institutionBankAccountForCurrency.getIdInstitutionBankAccountPk().equals(institutionBankAccount.getIdInstitutionBankAccountPk())))
					 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			 }			 
		 }
		 else{
			 if(institutionBankAccount.getBankType().equals(BankType.COMMERCIAL.getCode())){
				 String idInstitution=null;
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
				 InstitutionBankAccount institutionBankAccountForAccountNumberAndCurrency = getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getAccountNumber(), institutionBankAccount.getCurrency());
				 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForAccountNumberAndCurrency)){
					 if(!(institutionBankAccountForAccountNumberAndCurrency.getIdInstitutionBankAccountPk().equals(institutionBankAccount.getIdInstitutionBankAccountPk())))
						 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				 }					
			 }
		 }
		 Map<String,Object> mapParams = new HashMap<String, Object>();
		 mapParams.put("idBankAccountPkParam", institutionBankAccount.getIdInstitutionBankAccountPk());
		 InstitutionBankAccount institutionBankAccountAux = findObjectByNamedQuery(InstitutionBankAccount.BANK_ACCOUNT_STATE, mapParams);
		 if(!BankAccountsStateType.BLOCKED.getCode().equals(institutionBankAccountAux.getState())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		 }
		 InstitutionBankAccount InstitutionBankAccountUpdate = find(InstitutionBankAccount.class, institutionBankAccount.getIdInstitutionBankAccountPk());
		 InstitutionBankAccountUpdate.setState(BankAccountsStateType.REGISTERED.getCode());
		 InstitutionBankAccountUpdate.setUnblockMotive(institutionBankAccount.getUnblockMotive());
		 InstitutionBankAccountUpdate.setUnblockOtherMotive(institutionBankAccount.getUnblockOtherMotive());
		 InstitutionBankAccountUpdate.setUnblockUser(loggerUser.getUserName());
		 InstitutionBankAccountUpdate.setUnblockDate(CommonsUtilities.currentDateTime());
		
		update(InstitutionBankAccountUpdate);
		
		return InstitutionBankAccountUpdate;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	
	/**
	 * Validate institution cash account.
	 *
	 * @param searchBankAccountsTO the search bank accounts to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionBankAccount> validateInstitutionCashAccount(SearchBankAccountsTO searchBankAccountsTO)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	"
					+ " LEFT JOIN FETCH cs.providerBank b"
					+ " LEFT JOIN FETCH cs.bank  ban"
					+ " WHERE 1=1 ");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getAccountNumber()))
				sbQuery.append(" and cs.accountNumber = :accountNumber");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getIdBankProviderPk()))
				sbQuery.append(" and b.idBankPk = :idBankPk");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getCurrency()))
				sbQuery.append(" and cs.currency = :currency");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getInstitutionType()))
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					sbQuery.append(" and cs.issuer.idIssuerPk = :idBankInstitutionPk");
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					sbQuery.append(" and cs.participant.idParticipantPk = :idBankInstitutionPk");
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					sbQuery.append(" and ban.idBankPk = :idBankInstitutionPk");
			Query query = em.createQuery(sbQuery.toString()); 
			if(Validations.validateIsNotNull(searchBankAccountsTO.getAccountNumber()))
				query.setParameter("accountNumber",  searchBankAccountsTO.getAccountNumber());
			if(Validations.validateIsNotNull(searchBankAccountsTO.getIdBankProviderPk()))
				query.setParameter("idBankPk",  searchBankAccountsTO.getIdBankProviderPk());
			if(Validations.validateIsNotNull(searchBankAccountsTO.getCurrency()))
				query.setParameter("currency",  searchBankAccountsTO.getCurrency());
			if(Validations.validateIsNotNull(searchBankAccountsTO.getInstitutionType()))
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					query.setParameter("idBankInstitutionPk",  searchBankAccountsTO.getInstitution());
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					query.setParameter("idBankInstitutionPk",  Long.valueOf(searchBankAccountsTO.getInstitution()));
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					query.setParameter("idBankInstitutionPk",  Long.valueOf(searchBankAccountsTO.getInstitution()));
			List<InstitutionBankAccount> lstInstitutionBankAccount = (ArrayList<InstitutionBankAccount>)query.getResultList();	
		
			return lstInstitutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
		
	/**
	 * Gets the institution bank account for lip.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idCurrency the id currency
	 * @param accountNumber the account number
	 * @return the institution bank account for lip
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public InstitutionBankAccount getInstitutionBankAccountForLip(Integer institution, 
			Long idInstitution, Integer idCurrency, String accountNumber) throws ServiceException{
		InstitutionBankAccount institutionBankAccount = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs WHERE cs.currency = :currency AND cs.state = :state ");
			sbQuery.append("  AND cs.participant.idParticipantPk = :idParticipantPk AND cs.bankAcountType = :idBankAcountType ");
			if(Validations.validateIsNotNullAndNotEmpty(accountNumber)){
				sbQuery.append("  AND cs.accountNumber = :accountNumber ");
			}			
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("currency",  idCurrency);
			query.setParameter("state",  BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("idParticipantPk", idInstitution);
			query.setParameter("idBankAcountType", new Long(BankAccountType.CURRENT.getCode()));
			if(Validations.validateIsNotNullAndNotEmpty(accountNumber)){
				query.setParameter("accountNumber", accountNumber);
			}			
			List<InstitutionBankAccount> lstInstitutionBankAccount = (ArrayList<InstitutionBankAccount>) query.getResultList();	
			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccount)){
				institutionBankAccount = new InstitutionBankAccount();
				institutionBankAccount = lstInstitutionBankAccount.get(0);
			}		
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
}
