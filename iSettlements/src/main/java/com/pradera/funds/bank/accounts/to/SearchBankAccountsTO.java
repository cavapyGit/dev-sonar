package com.pradera.funds.bank.accounts.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchBankAccountsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchBankAccountsTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id issuer pk. */
	private Issuer issuer;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id Bank Institution. */
	private Long idBankInstitutionPk;
	
	/** The account number. */
	private String accountNumber;
	
	/** The institution Type. */
	private Integer institutionType;
	
	/** The currency. */
	private Integer currency;
	
	/** The state. */
	private Integer state;
	
	/** The initial Date. */
	private Date initialDate;
	
	/** The final Date. */
	private Date finalDate;
	
	/** The current date. */
	private Date currentDate;
	
	/** The List Institution Type. */
	private List<ParameterTable> lstInstitutionType;
	
	/** The List participant. */
	private List<Participant> lstParticipant;
	
	/** The List Bank Type. */
	private List<ParameterTable> lstBankType;
	
	/** The List Bank Account Type. */
	private List<ParameterTable> lstBankAccountType;
	
	/** The List currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The List state. */
	private List<ParameterTable> lstState;

	/** The List bank provider. */
	private List<Bank> lstBankProvider;
	
	/** The List bank institution. */
	private List<Bank> lstBankInstitution;
	
	/** The id bank provider pk. */
	private Long idBankProviderPk;
	
	/** The bank type. */
	private Integer bankType;
	
	/** The bank account type. */
	private Integer bankAccountType;
	
	/** The institution. */
	private String institution;
	
	/**
	 * Instantiates a new search bank accounts to.
	 */
	public SearchBankAccountsTO(){
		
	}
	
	/**
	 * Instantiates a new search bank accounts to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 */
	public SearchBankAccountsTO(Date initialDate, Date finalDate) {
		this.initialDate = initialDate;
		this.finalDate = finalDate;
	}
	
	/**
	 * Instantiates a new search bank accounts to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 * @param currentDate the current date
	 */
	public SearchBankAccountsTO(Date initialDate, Date finalDate,Date currentDate) {
		this.initialDate = initialDate;
		this.finalDate = finalDate;
		this.currentDate=currentDate;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the lst institution type.
	 *
	 * @return the lst institution type
	 */
	public List<ParameterTable> getLstInstitutionType() {
		return lstInstitutionType;
	}

	/**
	 * Sets the lst institution type.
	 *
	 * @param lstInstitutionType the new lst institution type
	 */
	public void setLstInstitutionType(List<ParameterTable> lstInstitutionType) {
		this.lstInstitutionType = lstInstitutionType;
	}

	/**
	 * Gets the lst bank type.
	 *
	 * @return the lst bank type
	 */
	public List<ParameterTable> getLstBankType() {
		return lstBankType;
	}

	/**
	 * Sets the lst bank type.
	 *
	 * @param lstBankType the new lst bank type
	 */
	public void setLstBankType(List<ParameterTable> lstBankType) {
		this.lstBankType = lstBankType;
	}

	/**
	 * Gets the lst bank account type.
	 *
	 * @return the lst bank account type
	 */
	public List<ParameterTable> getLstBankAccountType() {
		return lstBankAccountType;
	}

	/**
	 * Sets the lst bank account type.
	 *
	 * @param lstBankAccountType the new lst bank account type
	 */
	public void setLstBankAccountType(List<ParameterTable> lstBankAccountType) {
		this.lstBankAccountType = lstBankAccountType;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}

	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the lst bank provider.
	 *
	 * @return the lst bank provider
	 */
	public List<Bank> getLstBankProvider() {
		return lstBankProvider;
	}

	/**
	 * Sets the lst bank provider.
	 *
	 * @param lstBankProvider the new lst bank provider
	 */
	public void setLstBankProvider(List<Bank> lstBankProvider) {
		this.lstBankProvider = lstBankProvider;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the lst bank institution.
	 *
	 * @return the lst bank institution
	 */
	public List<Bank> getLstBankInstitution() {
		return lstBankInstitution;
	}

	/**
	 * Sets the lst bank institution.
	 *
	 * @param lstBankInstitution the new lst bank institution
	 */
	public void setLstBankInstitution(List<Bank> lstBankInstitution) {
		this.lstBankInstitution = lstBankInstitution;
	}

	/**
	 * Gets the id bank institution pk.
	 *
	 * @return the id bank institution pk
	 */
	public Long getIdBankInstitutionPk() {
		return idBankInstitutionPk;
	}

	/**
	 * Sets the id bank institution pk.
	 *
	 * @param idBankInstitutionPk the new id bank institution pk
	 */
	public void setIdBankInstitutionPk(Long idBankInstitutionPk) {
		this.idBankInstitutionPk = idBankInstitutionPk;
	}

	/**
	 * Gets the id bank provider pk.
	 *
	 * @return the id bank provider pk
	 */
	public Long getIdBankProviderPk() {
		return idBankProviderPk;
	}

	/**
	 * Sets the id bank provider pk.
	 *
	 * @param idBankProviderPk the new id bank provider pk
	 */
	public void setIdBankProviderPk(Long idBankProviderPk) {
		this.idBankProviderPk = idBankProviderPk;
	}

	/**
	 * Gets the bank type.
	 *
	 * @return the bank type
	 */
	public Integer getBankType() {
		return bankType;
	}

	/**
	 * Sets the bank type.
	 *
	 * @param bankType the new bank type
	 */
	public void setBankType(Integer bankType) {
		this.bankType = bankType;
	}

	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return bankAccountType;
	}

	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * Gets the institution.
	 *
	 * @return the institution
	 */
	public String getInstitution() {
		return institution;
	}

	/**
	 * Sets the institution.
	 *
	 * @param institution the new institution
	 */
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	
}
