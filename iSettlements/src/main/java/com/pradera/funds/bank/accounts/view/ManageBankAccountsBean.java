package com.pradera.funds.bank.accounts.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.bank.accounts.facade.ManageBankAccountsFacade;
import com.pradera.funds.bank.accounts.to.SearchBankAccountsTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.type.BankAccountsMotiveBlockType;
import com.pradera.model.funds.type.BankAccountsMotiveUnBlockType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.settlements.utils.MotiveController;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageBankAccountsBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ManageBankAccountsBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	 /** The institution bank account Session. */
	private InstitutionBankAccount institutionBankAccountSession;
	
	/** Search Bank Accounts Register TO. */
	private SearchBankAccountsTO searchBankAccountsRegisterTO;
	
	/** Search Bank Accounts TO. */
	private SearchBankAccountsTO searchBankAccountsTO;
	
	/** The general parameters facade. */
    @EJB
    GeneralParametersFacade generalParametersFacade;
    
    /** The manage Bank Accounts Facade. */
    @EJB
    ManageBankAccountsFacade manageBankAccountsFacade;
    
    /** The notification service facade. */
    @EJB NotificationServiceFacade notificationServiceFacade;
    
    /** The Institution Bank Account Data Model. */
	private GenericDataModel<InstitutionBankAccount> searchBankAccountsTODataModel;
	
    /** The motive controller Block. */
	private MotiveController motiveControllerBlock; 
	
	/** The motive controller reject. */
	private MotiveController motiveControllerUnBlock;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	

	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		createObject();
		try {		
			loadInstitutionType();
			loadParticipant();
			loadBankInstitution();
			loadState();
			loadMotiveBlock();
			loadMotiveUnBlock();
			loadHolidays();
			loadUserValidation();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load Motive Block.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveBlock()throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.BANK_ACCOUNTS_BLOCK_MOTIVE.getCode());	
		motiveControllerBlock.setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * Load Motive UnBlock.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveUnBlock()throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.BANK_ACCOUNTS_UNBLOCK_MOTIVE.getCode());	
		motiveControllerUnBlock.setMotiveList(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	
	/**
	 * Load State.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadState()throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_STATE_TYPE.getCode());	
		searchBankAccountsTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));	
	}
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create Object.
	 */
	public void createObject(){
		searchBankAccountsTO = new SearchBankAccountsTO();	
		searchBankAccountsTO.setIssuer(new Issuer());
		searchBankAccountsTO.setCurrentDate(CommonsUtilities.currentDate());
		searchBankAccountsTO.setLstInstitutionType(new ArrayList<ParameterTable>());
		searchBankAccountsTO.setLstParticipant(new ArrayList<Participant>());
		searchBankAccountsTO.setLstState(new ArrayList<ParameterTable>());
		searchBankAccountsTO.setLstBankInstitution(new ArrayList<Bank>());
		motiveControllerBlock = new MotiveController();
		motiveControllerUnBlock = new MotiveController();
		createObjectBankAccountsSession();
	}
	
	/**
	 * Load User Validation.
	 */
	private void loadUserValidation() {

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);				
	}
	
	/**
	 * Registry Bank Account Handler.
	 */
	public void registryBankAccountHandler()
	{
		executeAction();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		createObjectBankAccountsSession();
		createObjectRegister();
		try {
			loadInstitutionType();
			loadBankType();
			loadParticipant();
			loadBankInstitution();
			loadBankAccountType();
			loadCurrency();
		} catch (ServiceException e) {
			e.printStackTrace();
		}			
	}
	
	/**
	 * Load Bank Institution.
	 *
	 * @throws ServiceException the Service Exception
	 */
	public void loadBankInstitution()throws ServiceException{
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))	
			searchBankAccountsTO.setLstBankInstitution(manageBankAccountsFacade.getListBank(null,BankStateType.REGISTERED.getCode()));
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))	
			searchBankAccountsRegisterTO.setLstBankInstitution(manageBankAccountsFacade.getListBank(null,BankStateType.REGISTERED.getCode()));
	}
	
	/**
	 * load Participant.
	 *
	 * @throws ServiceException the Service Exception
	 */
	public void loadParticipant() throws ServiceException{
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))		
			searchBankAccountsTO.setLstParticipant(manageBankAccountsFacade.getLisParticipantServiceBean(participantTO));		
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))		
			searchBankAccountsRegisterTO.setLstParticipant(manageBankAccountsFacade.getLisParticipantServiceBean(participantTO));		
	}
	
	/**
	 * load Currency.
	 *
	 * @throws ServiceException  the service exception
	 */
	private void loadCurrency() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		List<ParameterTable> lstParameterTable = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstParameterTable){
			if(parameterTable.getParameterTablePk().equals(CurrencyType.PYG.getCode()) 
					|| parameterTable.getParameterTablePk().equals(CurrencyType.USD.getCode()))
				lstParameterTableAux.add(parameterTable);
		}
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			searchBankAccountsTO.setLstCurrency(lstParameterTableAux);
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			searchBankAccountsRegisterTO.setLstCurrency(lstParameterTableAux);
	}
	
	/**
	 * Load Bank Type.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadBankType()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		searchBankAccountsRegisterTO.setLstBankType(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Load Bank Account Type.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadBankAccountType()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		searchBankAccountsRegisterTO.setLstBankAccountType(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Load Bank Type.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadInstitutionType()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			searchBankAccountsRegisterTO.setLstInstitutionType(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			searchBankAccountsTO.setLstInstitutionType(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Create Object Register.
	 */
	public void createObjectRegister(){
		searchBankAccountsRegisterTO = new SearchBankAccountsTO();
		searchBankAccountsRegisterTO.setLstInstitutionType(new ArrayList<ParameterTable>());
		searchBankAccountsRegisterTO.setLstBankType(new ArrayList<ParameterTable>());
		searchBankAccountsRegisterTO.setLstParticipant(new ArrayList<Participant>());
		searchBankAccountsRegisterTO.setLstBankAccountType(new ArrayList<ParameterTable>());
		searchBankAccountsRegisterTO.setLstCurrency(new ArrayList<ParameterTable>());
		searchBankAccountsRegisterTO.setLstBankInstitution(new ArrayList<Bank>());
		searchBankAccountsRegisterTO.setLstBankProvider(new ArrayList<Bank>());
	}
	
	/**
	 * Creates the object bank accounts session.
	 */
	public void createObjectBankAccountsSession(){
		institutionBankAccountSession = new InstitutionBankAccount();
		institutionBankAccountSession.setBank(new Bank());
		institutionBankAccountSession.setProviderBank(new Bank());
		institutionBankAccountSession.setParticipant(new Participant());
		institutionBankAccountSession.setIssuer(new Issuer());
	}
	
	/**
	 * Validate Bank Type.
	 */
	public void validateBankType(){
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())){
			try{
				if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
					JSFUtilities.resetComponent(":frmRegBank:bank");
				searchBankAccountsRegisterTO.setLstBankProvider(manageBankAccountsFacade.getListBank(null,BankStateType.REGISTERED.getCode())); 
				if(institutionBankAccountSession.getBankType().equals(BankType.CENTRALIZING.getCode())) {
					searchBankAccountsRegisterTO.setLstBankProvider(manageBankAccountsFacade.getListBank(institutionBankAccountSession.getBankType(),BankStateType.REGISTERED.getCode()));
					if(Validations.validateListIsNotNullAndNotEmpty(searchBankAccountsRegisterTO.getLstBankProvider())) {
						institutionBankAccountSession.setProviderBank(searchBankAccountsRegisterTO.getLstBankProvider().get(0));
					}else {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_BANK_CENTRALIZING));	
					   JSFUtilities.showSimpleValidationDialog();
					   return;
					}
				}	
				else {
					if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
						institutionBankAccountSession.setProviderBank(new Bank());
					if(!getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
						institutionBankAccountSession.setProviderBank(institutionBankAccountSession.getProviderBank());
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}	
		}else{
			searchBankAccountsRegisterTO.setLstBankProvider(new ArrayList<Bank>());
		}
			
	}
	
	/**
	 * Clean bank accounts handler.
	 */
	/*
	 * Clean Bank Accounts Handler
	 */
	public void cleanBankAccountsHandler(){
		executeAction();
		institutionBankAccountSession = new InstitutionBankAccount();
		institutionBankAccountSession.setBank(new Bank());
		institutionBankAccountSession.setProviderBank(new Bank());
		institutionBankAccountSession.setParticipant(new Participant());
		institutionBankAccountSession.setIssuer(new Issuer());
	}
	
	/**
	 * Before Bank Accounts Handler.
	 */
	public void beforeBankAccountsHandler(){
		 if (Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getInstitutionType()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankType())
					|| Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getProviderBank().getIdBankPk()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankAcountType())
					|| Validations.validateIsNullOrEmpty(institutionBankAccountSession.getAccountNumber()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getCurrency()))
			{
				  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
				   JSFUtilities.showSimpleValidationDialog();
				   return;
			}
		 else{
			 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode())){
				 if(Validations.validateIsNullOrEmpty(institutionBankAccountSession.getIssuer())){
					  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
					   JSFUtilities.showSimpleValidationDialog();
					   return;
				 }
			 }else{
				 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
					 if(Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getParticipant().getIdParticipantPk())){
						  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
						   JSFUtilities.showSimpleValidationDialog();
						   return;
					 }
				 } 
				 else{
					 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode())){
						 if(Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBank().getIdBankPk())){
							  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
							   JSFUtilities.showSimpleValidationDialog();
							   return;
						 }
					 }
				 }
			 }
			
			 if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
				 if(institutionBankAccountSession.getBankType().equals(BankType.CENTRALIZING.getCode())){
					 try{
						 String idInstitution=null;
						 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
							 idInstitution= institutionBankAccountSession.getIssuer().getIdIssuerPk();
						 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
							 idInstitution= institutionBankAccountSession.getParticipant().getIdParticipantPk().toString();
						 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
							 idInstitution= institutionBankAccountSession.getBank().getIdBankPk().toString();
					 InstitutionBankAccount institutionBankAccount=manageBankAccountsFacade.getInstitutionBankAccountForCurrency(institutionBankAccountSession.getInstitutionType(),idInstitution,institutionBankAccountSession.getProviderBank().getIdBankPk(),institutionBankAccountSession.getCurrency());					
//					 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
//						  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//									, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_CURRENCY));	
//						 JSFUtilities.showSimpleValidationDialog();
//						   return;
//					 }
					 } catch (ServiceException e) {
							e.printStackTrace();
						}
				 }else{
					 if(institutionBankAccountSession.getBankType().equals(BankType.COMMERCIAL.getCode())){
						 try{
							 String idInstitution=null;
							 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
								 idInstitution= institutionBankAccountSession.getIssuer().getIdIssuerPk();
							 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
								 idInstitution= institutionBankAccountSession.getParticipant().getIdParticipantPk().toString();
							 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
								 idInstitution= institutionBankAccountSession.getBank().getIdBankPk().toString();
						 InstitutionBankAccount institutionBankAccount=manageBankAccountsFacade.getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccountSession.getInstitutionType(),idInstitution,institutionBankAccountSession.getProviderBank().getIdBankPk() ,institutionBankAccountSession.getAccountNumber() ,institutionBankAccountSession.getCurrency());
						 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
							  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER_CURRENCY));	
							 JSFUtilities.showSimpleValidationDialog();
							   return;
						 }
						//validate if number account exist in the same institution
						 SearchBankAccountsTO objSearchBankAccountsTO= new SearchBankAccountsTO();
						 objSearchBankAccountsTO.setAccountNumber(institutionBankAccountSession.getAccountNumber());
						 objSearchBankAccountsTO.setInstitutionType(institutionBankAccountSession.getInstitutionType());
						 objSearchBankAccountsTO.setInstitution(idInstitution);
						 objSearchBankAccountsTO.setIdBankProviderPk(institutionBankAccountSession.getProviderBank().getIdBankPk());
						 List<InstitutionBankAccount> lstInstitutionBankAccount =manageBankAccountsFacade.validateInstitutionCashAccount(objSearchBankAccountsTO);
						 if(Validations.validateIsNotNull(lstInstitutionBankAccount) && lstInstitutionBankAccount.size()>0)
						 {
							 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER));	
							 JSFUtilities.showSimpleValidationDialog();
							   return;
						 }
						 
						 } catch (ServiceException e) {
								e.printStackTrace();
							}
					 }
				 }
				 try{
				 Bank bank = manageBankAccountsFacade.getBankCentralizing(BankType.CENTRALIZING.getCode());
				 if(Validations.validateIsNullOrEmpty(bank)){
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_BANK_CENTRALIZING));	
					 JSFUtilities.showSimpleValidationDialog();
					   return;
				 }				
				 } catch (ServiceException e) {
						e.printStackTrace();
					}				 
			 }	
			 else{
				 if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
					 if(institutionBankAccountSession.getBankType().equals(BankType.CENTRALIZING.getCode())){
						 try{
							 String idInstitution=null;
							 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
								 idInstitution= institutionBankAccountSession.getIssuer().getIdIssuerPk();
							 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
								 idInstitution= institutionBankAccountSession.getParticipant().getIdParticipantPk().toString();
							 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
								 idInstitution= institutionBankAccountSession.getBank().getIdBankPk().toString();
						 InstitutionBankAccount institutionBankAccount=manageBankAccountsFacade.getInstitutionBankAccountForCurrency(institutionBankAccountSession.getInstitutionType(),idInstitution,institutionBankAccountSession.getProviderBank().getIdBankPk(), institutionBankAccountSession.getCurrency());
						 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
							 if(!(institutionBankAccount.getIdInstitutionBankAccountPk().equals(institutionBankAccountSession.getIdInstitutionBankAccountPk()))){
								  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_CURRENCY));	
								 JSFUtilities.showSimpleValidationDialog();
								   return;
							 }
						 }						 
						 } catch (ServiceException e) {
								e.printStackTrace();
							}
					 }else{
						 if(institutionBankAccountSession.getBankType().equals(BankType.COMMERCIAL.getCode())){
							 try{
								 String idInstitution=null;
								 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
									 idInstitution= institutionBankAccountSession.getIssuer().getIdIssuerPk();
								 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
									 idInstitution= institutionBankAccountSession.getParticipant().getIdParticipantPk().toString();
								 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
									 idInstitution= institutionBankAccountSession.getBank().getIdBankPk().toString();
							 InstitutionBankAccount institutionBankAccount=manageBankAccountsFacade.getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccountSession.getInstitutionType(),idInstitution,institutionBankAccountSession.getProviderBank().getIdBankPk(), institutionBankAccountSession.getAccountNumber() ,institutionBankAccountSession.getCurrency());
							 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
								 if(!(institutionBankAccount.getIdInstitutionBankAccountPk().equals(institutionBankAccountSession.getIdInstitutionBankAccountPk()))){
									  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
												, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER_CURRENCY));	
									 JSFUtilities.showSimpleValidationDialog();
									   return;
								 }
							 }							 
							 } catch (ServiceException e) {
									e.printStackTrace();
								}
						 }
					 }
				 }				 
			 }
		 }
		  if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_REGISTER));	
		  
		  if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode()))
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_MODIFY,new Object[]{institutionBankAccountSession.getAccountNumber().toString()}));	
		  		  
		  JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
	}
	
	/**
	 * Save Bank Accounts Handler.
	 */
	@LoggerAuditWeb
	public void saveBankAccountsHandler(){
		InstitutionBankAccount institutionBankAccount=null;
		try{
			if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
				institutionBankAccount = manageBankAccountsFacade.registryBankAccountsFacade(institutionBankAccountSession);
			if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode()))			
				institutionBankAccount = manageBankAccountsFacade.modifyBankAccountsFacade(institutionBankAccountSession);	
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				cleanBankAccountsSearchHandler();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
			
			if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
				//Sending notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_ACCOUNT_REGISTRATION.getCode());
					Object[] parameters = new Object[]{institutionBankAccount.getAccountNumber().toString()};
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
							businessProcessNotification,institutionBankAccount.getIdInstitutionBankAccountPk(), parameters);
			}
			if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
				//Sending notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_ACCOUNT_MODIFICATION.getCode());
					Object[] parameters = new Object[]{institutionBankAccount.getAccountNumber().toString()};
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
							businessProcessNotification,institutionBankAccount.getIdInstitutionBankAccountPk(), parameters);
			}
				
			if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_REGISTER_OK, new Object[]{institutionBankAccount.getAccountNumber().toString()}));	
			if(getViewOperationType().equals(ViewOperationsType.MODIFY.getCode()))
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_MODIFY_OK, new Object[]{institutionBankAccount.getAccountNumber().toString()}));	
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}	
		cleanBankAccountsSearchHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());	
	}
	
	/**
	 * Clean Bank Accounts Search Handler.
	 */
	public void cleanBankAccountsSearchHandler(){
		executeAction();
		searchBankAccountsTO.setInstitutionType(null);
		searchBankAccountsTO.setIdParticipantPk(null);
		searchBankAccountsTO.setIssuer(new Issuer());
		searchBankAccountsTO.setAccountNumber(null);
		searchBankAccountsTO.setState(null);
		searchBankAccountsTO.setIdBankInstitutionPk(null);
		searchBankAccountsTO.setInitialDate(null);
		searchBankAccountsTO.setFinalDate(null);
		cleanDataModel();
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
		{
			JSFUtilities.resetComponent(":frmBank:state");
			JSFUtilities.resetComponent(":frmBank:institutionType");
		}
			
	}
	
	/**
	 * before Modify Action.
	 *
	 * @return the string
	 */
	public String beforeModifyAction(){
		InstitutionBankAccount institutionBankAccount = this.getInstitutionBankAccountSession(); 
		executeAction();
		if(institutionBankAccount==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(institutionBankAccount.getState().equals(BankAccountsStateType.REGISTERED.getCode())){			
			selectBankAccountsHandler(institutionBankAccount);
			this.setViewOperationType(ViewOperationsType.MODIFY.getCode());
			institutionBankAccountSession.setLastModifyDate(CommonsUtilities.currentDateTime());
			return "registerBankAccounts";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	
	/**
	 * Before Block Action.
	 *
	 * @return the string
	 */
	public String beforeBlockAction(){
		InstitutionBankAccount institutionBankAccount = this.getInstitutionBankAccountSession(); 
		executeAction();
		if(institutionBankAccount==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(institutionBankAccount.getState().equals(BankAccountsStateType.REGISTERED.getCode())){			
			selectBankAccountsHandler(institutionBankAccount);
			this.setViewOperationType(ViewOperationsType.BLOCK.getCode());
			return "viewBankAccounts";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	
	/**
	 * Before unblock action.
	 *
	 * @return the string
	 */
	/*
	 *  Before Unblock Action
	 */
	public String beforeUnblockAction(){
		InstitutionBankAccount institutionBankAccount = this.getInstitutionBankAccountSession(); 
		executeAction();
		if(institutionBankAccount==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(institutionBankAccount.getState().equals(BankAccountsStateType.BLOCKED.getCode())){			
			selectBankAccountsHandler(institutionBankAccount);
			this.setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
			return "viewBankAccounts";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATION_BLOCKED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	
	/**
	 * selectBankAccountsHandler.
	 *
	 * @param institutionBankAccount the institution bank account
	 */
	public void selectBankAccountsHandler(InstitutionBankAccount institutionBankAccount){
		executeAction();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		createObjectRegister();
		try {
			loadInstitutionType();
			loadBankType();
			loadParticipant();
			loadBankInstitution();
			loadBankAccountType();
			loadCurrency();
			institutionBankAccountSession = manageBankAccountsFacade.getBankAccountsForId(institutionBankAccount.getIdInstitutionBankAccountPk());
			searchBankAccountsRegisterTO.setLstBankProvider(manageBankAccountsFacade.getListBank(institutionBankAccountSession.getBankType(),BankStateType.REGISTERED.getCode())); 
			setViewOperationType(ViewOperationsType.CONSULT.getCode());	
			//validateBankType();
		} catch (ServiceException e) {
			e.printStackTrace();
		}			
	}
	
	/**
	 * Search Bank Accounts Handler.
	 */
	@LoggerAuditWeb
	public void searchBankAccountsHandler(){
		List <InstitutionBankAccount> lstInstitutionBankAccount=null;		
		try{			
			lstInstitutionBankAccount = manageBankAccountsFacade.getListBankAccountsFilter(searchBankAccountsTO);
			searchBankAccountsTODataModel= new GenericDataModel<InstitutionBankAccount>(lstInstitutionBankAccount);			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	/**
	 * Search issuer request handler.
	 */
	public void searchIssuerHandler(){
		executeAction();	
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getIssuer().getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageBankAccountsFacade.findIssuerFacade(institutionBankAccountSession.getIssuer().getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			institutionBankAccountSession.setIssuer(issuerTemp);
		}
	}
	/**
	 * Search issuer request handler search.
	 */
	public void searchIssuerHandlerSearch(){
		executeAction();	
		cleanDataModel();
		if(Validations.validateIsNotNullAndNotEmpty(searchBankAccountsTO.getIssuer().getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageBankAccountsFacade.findIssuerFacade(searchBankAccountsTO.getIssuer().getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			searchBankAccountsTO.setIssuer(issuerTemp);
		}
	}
	/**
	 * Before Block handler.
	 */
	public void beforeBlockHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(this.getInstitutionBankAccountSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getInstitutionBankAccountSession().getState().equals(BankAccountsStateType.REGISTERED.getCode())){
			setViewOperationType(ViewOperationsType.BLOCK.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveBlockHandler(){
		if(this.getMotiveControllerBlock().getIdMotivePK()==Long.parseLong(BankAccountsMotiveBlockType.OTHER.getCode().toString())){
			this.getMotiveControllerBlock().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerBlock().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveUnBlockHandler(){
		if(this.getMotiveControllerUnBlock().getIdMotivePK()==Long.parseLong(BankAccountsMotiveUnBlockType.OTHER.getCode().toString())){
			this.getMotiveControllerUnBlock().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerUnBlock().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Before Un Block handler.
	 */
	public void beforeUnBlockHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(this.getInstitutionBankAccountSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getInstitutionBankAccountSession().getState().equals(BankAccountsStateType.BLOCKED.getCode())){
			setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveUnBlock').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATION_BLOCKED));	
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Change action currency settlement request handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		switch(ViewOperationsType.lookup.get(getViewOperationType())){
			case BLOCK : blockBankAccounts();break;
			case UNBLOCK:unblockBankAccounts();break;			
		}
		cleanBankAccountsSearchHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());	
	}
	
	/**
	 * block Bank Accounts.
	 */
	@LoggerAuditWeb
	private void blockBankAccounts(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		institutionBankAccountSession.setBlockMotive(Integer.parseInt(""+motiveControllerBlock.getIdMotivePK()));
		institutionBankAccountSession.setBlockOtherMotive(motiveControllerBlock.getMotiveText());
		InstitutionBankAccount institutionBankAccount = null;
		try {
			institutionBankAccount = manageBankAccountsFacade.blockBankAccountsFacade(institutionBankAccountSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(institutionBankAccount!=null){
			
			//Sending notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_ACCOUNT_ACTIVATE.getCode());
				Object[] parameters = new Object[]{institutionBankAccount.getAccountNumber().toString()};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,institutionBankAccount.getIdInstitutionBankAccountPk(), parameters);
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_BLOCK_OK,
				    		  new Object[]{institutionBankAccount.getAccountNumber().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	
	/**
	 * unblock Bank Accounts.
	 */
	@LoggerAuditWeb
	private void unblockBankAccounts(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		institutionBankAccountSession.setUnblockMotive(Integer.parseInt(""+motiveControllerUnBlock.getIdMotivePK()));
		institutionBankAccountSession.setUnblockOtherMotive(motiveControllerUnBlock.getMotiveText());
		InstitutionBankAccount institutionBankAccount = null;
		try {
			institutionBankAccount = manageBankAccountsFacade.unblockBankAccountsFacade(institutionBankAccountSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(institutionBankAccount!=null){
			
			//Sending notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BANK_ACCOUNT_DISACTIVATE.getCode());
				Object[] parameters = new Object[]{institutionBankAccount.getAccountNumber().toString()};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,institutionBankAccount.getIdInstitutionBankAccountPk(), parameters);
				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_UNBLOCK_OK,
				    		  new Object[]{institutionBankAccount.getAccountNumber().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveBlockHandler(){
		
		if(Validations.validateIsNullOrEmpty(motiveControllerBlock.getIdMotivePK()))
		{
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		}
		if(Validations.validateIsNullOrEmpty(motiveControllerBlock.getMotiveText()) )
		{
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		}
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerBlock.getIdMotivePK()==Long.parseLong(BankAccountsMotiveBlockType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerBlock().getMotiveText())){	
				setViewOperationType(ViewOperationsType.BLOCK.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
			      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_BLOCK_CONFIRM,
			    		  new Object[]{this.getInstitutionBankAccountSession().getAccountNumber()}) );	
		setViewOperationType(ViewOperationsType.BLOCK.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveUnBlockHandler(){
		
		if(Validations.validateIsNullOrEmpty(motiveControllerUnBlock.getIdMotivePK()))
		{
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		}
		if(Validations.validateIsNullOrEmpty(motiveControllerUnBlock.getMotiveText()) )
		{
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		}
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerUnBlock.getIdMotivePK()==Long.parseLong(BankAccountsMotiveUnBlockType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerUnBlock().getMotiveText())){	
				setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
			      , PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_UNBLOCK_CONFIRM,
			    		  new Object[]{this.getInstitutionBankAccountSession().getAccountNumber()}) );	
		setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel(){
		searchBankAccountsTODataModel=null;
	}
	
	/**
	 * Get User Information.
	 *
	 * @return User Information
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Set User Information.
	 *
	 * @param userInfo User Information
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Get Institution Bank Account Session.
	 *
	 * @return Institution Bank Account Session
	 */
	public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}
	
	/**
	 * Set Institution Bank Account Session.
	 *
	 * @param institutionBankAccountSession Institution Bank Account Session
	 */
	public void setInstitutionBankAccountSession(
			InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}
	
	/**
	 * Get Search Bank Accounts Register TO.
	 *
	 * @return Search Bank Accounts Register TO
	 */
	public SearchBankAccountsTO getSearchBankAccountsRegisterTO() {
		return searchBankAccountsRegisterTO;
	}
	
	/**
	 * Set Search Bank Accounts Register TO.
	 *
	 * @param searchBankAccountsRegisterTO Search Bank Accounts Register TO
	 */
	public void setSearchBankAccountsRegisterTO(
			SearchBankAccountsTO searchBankAccountsRegisterTO) {
		this.searchBankAccountsRegisterTO = searchBankAccountsRegisterTO;
	}
	
	/**
	 * Get Search Bank Accounts TO.
	 *
	 * @return  Search Bank Accounts TO
	 */
	public SearchBankAccountsTO getSearchBankAccountsTO() {
		return searchBankAccountsTO;
	}
	
	/**
	 * Set  Search Bank Accounts TO.
	 *
	 * @param searchBankAccountsTO  Search Bank Accounts TO
	 */
	public void setSearchBankAccountsTO(SearchBankAccountsTO searchBankAccountsTO) {
		this.searchBankAccountsTO = searchBankAccountsTO;
	}	
	
	/**
	 * Is Bank Type Centralizing.
	 *
	 * @return validate Bank Type Centralizing
	 */
	public boolean isIndBankTypeCentralizing(){
		return Validations.validateIsNotNull(institutionBankAccountSession.getBankType()) &&
				institutionBankAccountSession.getBankType() > 0 &&
				(BankType.CENTRALIZING.getCode().equals(institutionBankAccountSession.getBankType()));
	}
	
	/**
	 * Is Bank Type Commercial.
	 *
	 * @return validate Bank Type Commercial
	 */
	public boolean isIndBankTypeCommercial(){
		return Validations.validateIsNotNull(institutionBankAccountSession.getBankType()) &&
				institutionBankAccountSession.getBankType() > 0 &&
				(BankType.COMMERCIAL.getCode().equals(institutionBankAccountSession.getBankType()));
	}
	
	/**
	 * Is Institution Type Issuer.
	 *
	 * @return validate Institution Type Issuer
	 */
	public boolean isIndInstitutionTypeIssuer(){
		return Validations.validateIsNotNull(institutionBankAccountSession.getInstitutionType()) &&
				institutionBankAccountSession.getInstitutionType() > 0 &&
				(InstitutionBankAccountsType.ISSUER.getCode().equals(institutionBankAccountSession.getInstitutionType()));
	}
	
	/**
	 * Is Institution Type Bank.
	 *
	 * @return validate Institution Type Bank
	 */
	public boolean isIndInstitutionTypeBank(){
		return Validations.validateIsNotNull(institutionBankAccountSession.getInstitutionType()) &&
				institutionBankAccountSession.getInstitutionType() > 0 &&
				(InstitutionBankAccountsType.BANK.getCode().equals(institutionBankAccountSession.getInstitutionType()));
	}
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipant(){
		return Validations.validateIsNotNull(institutionBankAccountSession.getInstitutionType()) &&
				institutionBankAccountSession.getInstitutionType() > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institutionBankAccountSession.getInstitutionType()));
	}	
	
	/**
	 * Is Institution Type Issuer.
	 *
	 * @return validate Institution Type Issuer
	 */
	public boolean isIndInstitutionTypeIssuerSearch(){
		return Validations.validateIsNotNull(searchBankAccountsTO.getInstitutionType()) &&
				searchBankAccountsTO.getInstitutionType() > 0 &&
				(InstitutionBankAccountsType.ISSUER.getCode().equals(searchBankAccountsTO.getInstitutionType()));
	}
	
	/**
	 * Is Institution Type Bank.
	 *
	 * @return validate Institution Type Bank
	 */
	public boolean isIndInstitutionTypeBankSearch(){
		return Validations.validateIsNotNull(searchBankAccountsTO.getInstitutionType()) &&
				searchBankAccountsTO.getInstitutionType() > 0 &&
				(InstitutionBankAccountsType.BANK.getCode().equals(searchBankAccountsTO.getInstitutionType()));
	}
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipantSearch(){
		return Validations.validateIsNotNull(searchBankAccountsTO.getInstitutionType()) &&
				searchBankAccountsTO.getInstitutionType() > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(searchBankAccountsTO.getInstitutionType()));
	}
	
	/**
	 * Get Motive Controller Block.
	 *
	 * @return Motive Controller Block
	 */
	public MotiveController getMotiveControllerBlock() {
		return motiveControllerBlock;
	}
	
	/**
	 * Set Motive Controller Block.
	 *
	 * @param motiveControllerBlock Motive Controller Block
	 */
	public void setMotiveControllerBlock(MotiveController motiveControllerBlock) {
		this.motiveControllerBlock = motiveControllerBlock;
	}
	
	/**
	 * Get Motive Controller UnBlock.
	 *
	 * @return Motive Controller UnBlock
	 */
	public MotiveController getMotiveControllerUnBlock() {
		return motiveControllerUnBlock;
	}
	
	/**
	 * Set Motive Controller UnBlock.
	 *
	 * @param motiveControllerUnBlock Motive Controller UnBlock
	 */
	public void setMotiveControllerUnBlock(MotiveController motiveControllerUnBlock) {
		this.motiveControllerUnBlock = motiveControllerUnBlock;
	}
	
	/**
	 * Get Search Bank Accounts TO Data Model.
	 *
	 * @return Search Bank Accounts TO Data Model
	 */
	public GenericDataModel<InstitutionBankAccount> getSearchBankAccountsTODataModel() {
		return searchBankAccountsTODataModel;
	}
	
	/**
	 * Set Search Bank Accounts TO Data Model.
	 *
	 * @param searchBankAccountsTODataModel Search Bank Accounts TO Data Model
	 */
	public void setSearchBankAccountsTODataModel(
			GenericDataModel<InstitutionBankAccount> searchBankAccountsTODataModel) {
		this.searchBankAccountsTODataModel = searchBankAccountsTODataModel;
	}


}
