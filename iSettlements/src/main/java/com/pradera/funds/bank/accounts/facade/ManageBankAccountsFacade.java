package com.pradera.funds.bank.accounts.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.funds.bank.accounts.service.ManageBankAccountsService;
import com.pradera.funds.bank.accounts.to.SearchBankAccountsTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageBankAccountsFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageBankAccountsFacade {

	/** Transaction Registry. */
	@Resource
   TransactionSynchronizationRegistry transactionRegistry;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** Manage Bank Accounts Service. */
	@EJB
	ManageBankAccountsService manageBankAccountsService;
	
	/**
	 * Gets the list participant service bean.
	 *
	 * @param filter the filter
	 * @return the list participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLisParticipantServiceBean(Participant filter) throws ServiceException{
		return participantServiceBean.getLisParticipantServiceBean(filter);
	}
	
	/**
	 * Get List Bank.
	 *
	 * @param idTypeBank type bank
	 * @param idState the id state
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	public List<Bank> getListBank(Integer idTypeBank,Integer idState)throws ServiceException{
		return manageBankAccountsService.getListBank(idTypeBank,idState);
	}
	
	/**
	 * Get List Bank.
	 *
	 * @param idTypeBank type bank
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	public Bank getBankCentralizing(Integer idTypeBank)throws ServiceException{
		return manageBankAccountsService.getBankCentralizing(idTypeBank);
	}
	
	/**
	 * Find Issuer Facade.
	 *
	 * @param idIssuer id issuer
	 * @return issuer
	 * @throws ServiceException the Service Exception
	 */
	public Issuer findIssuerFacade (String idIssuer)throws ServiceException{
		return manageBankAccountsService.findIssuer(idIssuer);
	}
	
	/**
	 * Get Institution Bank Account For Currency.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idBank the id bank
	 * @param idCurrency id currency
	 * @return Institution Bank Account
	 * @throws ServiceException  the Service Exception
	 */
	public InstitutionBankAccount getInstitutionBankAccountForCurrency(Integer institution,String idInstitution,Long idBank,Integer idCurrency)throws ServiceException{
		return manageBankAccountsService.getInstitutionBankAccountForCurrency(institution, idInstitution, idBank, idCurrency);
	}
	
	/**
	 * getInstitutionBankAccountForAccounu Number And Currency.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idBank the id bank
	 * @param number number account
	 * @param idCurrency id currency
	 * @return Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount getInstitutionBankAccountForAccountNumberAndCurrency(Integer institution,String idInstitution,Long idBank,String number , Integer idCurrency)throws ServiceException{
		return manageBankAccountsService.getInstitutionBankAccountForAccountNumberAndCurrency(institution, idInstitution, idBank, number, idCurrency);
	}
	
	/**
	 * Registry Bank Accounts Facade.
	 *
	 * @param institutionBankAccount institution Bank Account
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_ACCOUNT_REGISTRATION)
	public InstitutionBankAccount registryBankAccountsFacade(InstitutionBankAccount institutionBankAccount)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        return manageBankAccountsService.registryBankAccountsFacade(institutionBankAccount, loggerUser);
	}
	
	/**
	 * Modify Bank Accounts Facade.
	 *
	 * @param institutionBankAccount  institution Bank Account
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_ACCOUNT_MODIFICATION)
	public InstitutionBankAccount modifyBankAccountsFacade(InstitutionBankAccount institutionBankAccount)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());   
        return manageBankAccountsService.modifyBankAccountsFacade(institutionBankAccount, loggerUser);
	}
	
	/**
	 * Get List Bank Accounts Filter.
	 *
	 * @param searchBankAccountsTO search Bank Accounts
	 * @return List Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_ACCOUNT_QUERY)
	public List <InstitutionBankAccount> getListBankAccountsFilter(SearchBankAccountsTO searchBankAccountsTO)throws ServiceException{
		return manageBankAccountsService.getListBankAccountsFilter(searchBankAccountsTO);		
	}
	
	/**
	 * Get Bank Accounts For Id.
	 *
	 * @param idInstitutionBankAccount id Institution Bank Account
	 * @return Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount getBankAccountsForId(Long idInstitutionBankAccount)throws ServiceException{
		return manageBankAccountsService.getBankAccountsForId(idInstitutionBankAccount);
	}
	
	/**
	 * Block Bank Accounts Facade.
	 *
	 * @param institutionBankAccount institution Bank Account
	 * @return  institution Bank Account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_ACCOUNT_ACTIVATE)
	public InstitutionBankAccount blockBankAccountsFacade(InstitutionBankAccount institutionBankAccount)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
        return manageBankAccountsService.blockBankAccountsFacade(institutionBankAccount, loggerUser);
	}
	
	/**
	 * Unblock  institution Bank Account.
	 *
	 * @param institutionBankAccount  institution Bank Account
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_ACCOUNT_DISACTIVATE)
	public InstitutionBankAccount unblockBankAccountsFacade(InstitutionBankAccount institutionBankAccount)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock()); 
        return manageBankAccountsService.unblockBankAccountsFacade(institutionBankAccount, loggerUser);
	}	
	
	/**
	 * Validate institution cash account.
	 *
	 * @param objSearchBankAccountsTO the obj search bank accounts to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionBankAccount> validateInstitutionCashAccount(SearchBankAccountsTO objSearchBankAccountsTO)throws ServiceException{
		return manageBankAccountsService.validateInstitutionCashAccount(objSearchBankAccountsTO);
	}
	
}
