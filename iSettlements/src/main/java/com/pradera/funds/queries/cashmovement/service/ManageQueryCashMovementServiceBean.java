package com.pradera.funds.queries.cashmovement.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.funds.queries.cashmovement.to.SearchCashMovementTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageQueryCashMovementServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
public class ManageQueryCashMovementServiceBean extends CrudDaoServiceBean{
	
	
	/** The idepositary setup. */
	@Inject @DepositarySetup 
	IdepositarySetup idepositarySetup;

	/**
	 * Search cash accounts.
	 *
	 * @param searchCashMovementTO the search cash movement to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionCashAccount> searchCashAccounts(SearchCashMovementTO searchCashMovementTO) throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("SELECT distinct ica");
		strQuery.append("	FROM InstitutionCashAccount ica");
		strQuery.append("	LEFT JOIN FETCH ica.modalityGroup");
		strQuery.append(" 	LEFT JOIN FETCH ica.cashAccountDetailResults cad");
		strQuery.append(" 	LEFT JOIN FETCH cad.institutionBankAccount b");
		strQuery.append("	WHERE ica.accountType = :accountType");
		strQuery.append("	AND ica.currency = :currency");
		strQuery.append("	AND ica.accountState = 683");
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getParticipantSel()))
			strQuery.append("	AND ica.participant.idParticipantPk = :participant");
		else if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getIssuerSel()))
			strQuery.append("	AND ica.issuer.idIssuerPk = :issuer");
		else
			strQuery.append("	AND ica.participant.idParticipantPk = :depositary");
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getIdBank())){
			strQuery.append("	AND b.providerBank.idBankPk = :bank");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getNegotiationMechanismSel()))
			strQuery.append("	AND ica.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getModalityGroupSel()))
			strQuery.append("	AND ica.modalityGroup.idModalityGroupPk = :idModalityGroupPk");
		Query query = em.createQuery(strQuery.toString());
		//query.setParameter("state", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("accountType", searchCashMovementTO.getFundOperationGroupSel());
		query.setParameter("currency", searchCashMovementTO.getCurrencySel());
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getParticipantSel()))
			query.setParameter("participant", searchCashMovementTO.getParticipantSel());
		else if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getIssuerSel()))
			query.setParameter("issuer", searchCashMovementTO.getIssuerSel());
		else
			query.setParameter("depositary", idepositarySetup.getIdParticipantDepositary());
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getNegotiationMechanismSel()))
			query.setParameter("idNegotiationMechanismPk", searchCashMovementTO.getNegotiationMechanismSel());
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getModalityGroupSel()))
			query.setParameter("idModalityGroupPk", searchCashMovementTO.getModalityGroupSel());
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getIdBank())){
			query.setParameter("bank", searchCashMovementTO.getIdBank());
		}
		return (List<InstitutionCashAccount>)query.getResultList();
	}

	/**
	 * Search cash movements.
	 *
	 * @param searchCashMovementTO the search cash movement to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchCashMovements(SearchCashMovementTO searchCashMovementTO) throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("SELECT cam.movementType.movementName,");			//0
		strQuery.append("		cam.movementClass,");						//1
		strQuery.append("		cam.fundsOperation.idFundsOperationPk,");	//2
		strQuery.append("		cam.movementAmount,");						//3
		strQuery.append("		cam.idCashAccountMovementPk,");				//4
		strQuery.append("		cam.movementDate,");						//5
		strQuery.append("		cam.fundsOperation.paymentReference,");		//6
		strQuery.append("		(SELECT pt.parameterName FROM ParameterTable pt" +
								"	WHERE pt.parameterTablePk = cam.fundsOperation.observation)");	//7
		if(searchCashMovementTO.getInstitutionCashAccount().getAccountType().equals(AccountCashFundsType.SETTLEMENT.getCode())){
			strQuery.append("		,(SELECT distinct b.description FROM CashAccountDetail cad,InstitutionBankAccount iba,Bank b" +
									"	WHERE cad.institutionCashAccount.idInstitutionCashAccountPk = cam.institutionCashAccount.idInstitutionCashAccountPk"
									+ " AND cad.institutionBankAccount.idInstitutionBankAccountPk= iba.idInstitutionBankAccountPk"
									+ " AND iba.providerBank.idBankPk = b.idBankPk");
			if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getIdBank())){
				strQuery.append(" AND iba.providerBank.idBankPk=:bank");	
			}
			strQuery.append(")");
		}
		strQuery.append("	FROM CashAccountMovement cam");
		strQuery.append("	WHERE cam.institutionCashAccount.idInstitutionCashAccountPk = :idInstitutionCashAccountPk");
		strQuery.append("	AND cam.currency = :currency");
		strQuery.append("	AND TRUNC(cam.movementDate) between :initialDate and :endDate");
		strQuery.append("	ORDER BY cam.idCashAccountMovementPk DESC");
		Query query = em.createQuery(strQuery.toString());
		query.setParameter("idInstitutionCashAccountPk", searchCashMovementTO.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		query.setParameter("currency", searchCashMovementTO.getCurrencySel());
		query.setParameter("initialDate", searchCashMovementTO.getInitialDate());
		query.setParameter("endDate", searchCashMovementTO.getEndDate());
		if(Validations.validateIsNotNullAndNotEmpty(searchCashMovementTO.getIdBank())){
			query.setParameter("bank", searchCashMovementTO.getIdBank());
		}
		return (List<Object[]>)query.getResultList();
	}

	/**
	 * Gets the lst mechanism.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the lst mechanism
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getLstMechanism(Long idParticipantPk) throws ServiceException{
		String strQuery = "		SELECT nm" +
					"	FROM NegotiationMechanism nm" +
					"	WHERE nm.idNegotiationMechanismPk in (" +
					"		SELECT pm.mechanismModality.id.idNegotiationMechanismPk" +
					"			FROM ParticipantMechanism pm" +
					"			WHERE pm.participant.idParticipantPk = :idParticipantPk" +
					"			AND pm.stateParticipantMechanism = :stateParticipantMechanism)" +
					"	AND nm.stateMechanism = :stateMechanism";
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("stateMechanism", NegotiationMechanismStateType.ACTIVE.getCode());
		query.setParameter("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		return (List<NegotiationMechanism>)query.getResultList();
	}

	/**
	 * Gets the lst modality group.
	 *
	 * @param idNegoMechanismPkSelected the id nego mechanism pk selected
	 * @return the lst modality group
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ModalityGroup> getLstModalityGroup(Long idNegoMechanismPkSelected) throws ServiceException{
		String strQuery = "		SELECT mg" +
					"	FROM ModalityGroup mg" +
					"	WHERE mg.idModalityGroupPk in (" +
					"		SELECT mgd.modalityGroup.idModalityGroupPk" +
					"			FROM ModalityGroupDetail mgd" +
					"			WHERE mgd.negotiationMechanism.idNegotiationMechanismPk = :idNegoMechanismPk)" +
					"	ORDER BY mg.description ASC";
		Query query = em.createQuery(strQuery);
		query.setParameter("idNegoMechanismPk", idNegoMechanismPkSelected);
		return (List<ModalityGroup>)query.getResultList();
	}
}
