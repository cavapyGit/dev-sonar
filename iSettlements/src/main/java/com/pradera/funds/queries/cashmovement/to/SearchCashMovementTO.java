package com.pradera.funds.queries.cashmovement.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.funds.queries.cashmovement.view.InstitutionCashAccountDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchCashMovementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchCashMovementTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst currency. */
	private List<ParameterTable> lstFundOperationGroup, lstCurrency;
	
	/** The lst mechanism. */
	private List<NegotiationMechanism> lstMechanism;
	
	/** The lst modality group. */
	private List<ModalityGroup> lstModalityGroup;
	
	/** The currency sel. */
	private Integer fundOperationGroupSel, currencySel;
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The modality group sel. */
	private Long participantSel, negotiationMechanismSel, modalityGroupSel; 
	
	/** The lst issuer. */
	private List<Issuer> lstIssuer;
	
	/** The issuer sel. */
	private String issuerSel;
	
	/** The cevaldom. */
	private String cevaldom;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The lst institution cash account. */
	private InstitutionCashAccountDataModel lstInstitutionCashAccount;
	
	/** The institution cash account. */
	private InstitutionCashAccount institutionCashAccount;
	
	/** The lst cash account movement to. */
	private List<CashAccountMovementTO> lstCashAccountMovementTO;
	
	/** The bl central account. */
	private boolean blCevaldom, blParticipant, blIssuer, blSettlement, blCentralAccount, blBenefit, blReturns;
	
	/** The id bank. */
	private Long idBank;
	
	/** The lst bank. */
	private List<Bank> lstBank;
	
	/**
	 * Gets the lst fund operation group.
	 *
	 * @return the lst fund operation group
	 */
	public List<ParameterTable> getLstFundOperationGroup() {
		return lstFundOperationGroup;
	}
	
	/**
	 * Sets the lst fund operation group.
	 *
	 * @param lstFundOperationGroup the new lst fund operation group
	 */
	public void setLstFundOperationGroup(List<ParameterTable> lstFundOperationGroup) {
		this.lstFundOperationGroup = lstFundOperationGroup;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the fund operation group sel.
	 *
	 * @return the fund operation group sel
	 */
	public Integer getFundOperationGroupSel() {
		return fundOperationGroupSel;
	}
	
	/**
	 * Sets the fund operation group sel.
	 *
	 * @param fundOperationGroupSel the new fund operation group sel
	 */
	public void setFundOperationGroupSel(Integer fundOperationGroupSel) {
		this.fundOperationGroupSel = fundOperationGroupSel;
	}
	
	/**
	 * Gets the currency sel.
	 *
	 * @return the currency sel
	 */
	public Integer getCurrencySel() {
		return currencySel;
	}
	
	/**
	 * Sets the currency sel.
	 *
	 * @param currencySel the new currency sel
	 */
	public void setCurrencySel(Integer currencySel) {
		this.currencySel = currencySel;
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}
	
	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the participant sel.
	 *
	 * @return the participant sel
	 */
	public Long getParticipantSel() {
		return participantSel;
	}
	
	/**
	 * Sets the participant sel.
	 *
	 * @param participantSel the new participant sel
	 */
	public void setParticipantSel(Long participantSel) {
		this.participantSel = participantSel;
	}
	
	/**
	 * Gets the lst issuer.
	 *
	 * @return the lst issuer
	 */
	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}
	
	/**
	 * Sets the lst issuer.
	 *
	 * @param lstIssuer the new lst issuer
	 */
	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}
	
	/**
	 * Gets the issuer sel.
	 *
	 * @return the issuer sel
	 */
	public String getIssuerSel() {
		return issuerSel;
	}
	
	/**
	 * Sets the issuer sel.
	 *
	 * @param issuerSel the new issuer sel
	 */
	public void setIssuerSel(String issuerSel) {
		this.issuerSel = issuerSel;
	}
	
	/**
	 * Gets the cevaldom.
	 *
	 * @return the cevaldom
	 */
	public String getCevaldom() {
		return cevaldom;
	}
	
	/**
	 * Sets the cevaldom.
	 *
	 * @param cevaldom the new cevaldom
	 */
	public void setCevaldom(String cevaldom) {
		this.cevaldom = cevaldom;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the lst institution cash account.
	 *
	 * @return the lst institution cash account
	 */
	public InstitutionCashAccountDataModel getLstInstitutionCashAccount() {
		return lstInstitutionCashAccount;
	}
	
	/**
	 * Sets the lst institution cash account.
	 *
	 * @param lstInstitutionCashAccount the new lst institution cash account
	 */
	public void setLstInstitutionCashAccount(
			InstitutionCashAccountDataModel lstInstitutionCashAccount) {
		this.lstInstitutionCashAccount = lstInstitutionCashAccount;
	}
	
	/**
	 * Gets the institution cash account.
	 *
	 * @return the institution cash account
	 */
	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}
	
	/**
	 * Sets the institution cash account.
	 *
	 * @param institutionCashAccount the new institution cash account
	 */
	public void setInstitutionCashAccount(
			InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}
	
	/**
	 * Gets the lst cash account movement to.
	 *
	 * @return the lst cash account movement to
	 */
	public List<CashAccountMovementTO> getLstCashAccountMovementTO() {
		return lstCashAccountMovementTO;
	}
	
	/**
	 * Sets the lst cash account movement to.
	 *
	 * @param lstCashAccountMovementTO the new lst cash account movement to
	 */
	public void setLstCashAccountMovementTO(
			List<CashAccountMovementTO> lstCashAccountMovementTO) {
		this.lstCashAccountMovementTO = lstCashAccountMovementTO;
	}
	
	/**
	 * Checks if is bl cevaldom.
	 *
	 * @return true, if is bl cevaldom
	 */
	public boolean isBlCevaldom() {
		return blCevaldom;
	}
	
	/**
	 * Sets the bl cevaldom.
	 *
	 * @param blCevaldom the new bl cevaldom
	 */
	public void setBlCevaldom(boolean blCevaldom) {
		this.blCevaldom = blCevaldom;
	}
	
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Checks if is bl issuer.
	 *
	 * @return true, if is bl issuer
	 */
	public boolean isBlIssuer() {
		return blIssuer;
	}
	
	/**
	 * Sets the bl issuer.
	 *
	 * @param blIssuer the new bl issuer
	 */
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}
	
	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}
	
	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}
	
	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}
	
	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}
	
	/**
	 * Gets the negotiation mechanism sel.
	 *
	 * @return the negotiation mechanism sel
	 */
	public Long getNegotiationMechanismSel() {
		return negotiationMechanismSel;
	}
	
	/**
	 * Sets the negotiation mechanism sel.
	 *
	 * @param negotiationMechanismSel the new negotiation mechanism sel
	 */
	public void setNegotiationMechanismSel(Long negotiationMechanismSel) {
		this.negotiationMechanismSel = negotiationMechanismSel;
	}
	
	/**
	 * Gets the modality group sel.
	 *
	 * @return the modality group sel
	 */
	public Long getModalityGroupSel() {
		return modalityGroupSel;
	}
	
	/**
	 * Sets the modality group sel.
	 *
	 * @param modalityGroupSel the new modality group sel
	 */
	public void setModalityGroupSel(Long modalityGroupSel) {
		this.modalityGroupSel = modalityGroupSel;
	}
	
	/**
	 * Checks if is bl settlement.
	 *
	 * @return true, if is bl settlement
	 */
	public boolean isBlSettlement() {
		return blSettlement;
	}
	
	/**
	 * Sets the bl settlement.
	 *
	 * @param blSettlement the new bl settlement
	 */
	public void setBlSettlement(boolean blSettlement) {
		this.blSettlement = blSettlement;
	}
	
	public boolean isBlBenefit() {
		return blBenefit;
	}

	public void setBlBenefit(boolean blBenefit) {
		this.blBenefit = blBenefit;
	}
	
	/**
	 * Checks if is bl central account.
	 *
	 * @return true, if is bl central account
	 */
	public boolean isBlCentralAccount() {
		return blCentralAccount;
	}
	
	/**
	 * Sets the bl central account.
	 *
	 * @param blCentralAccount the new bl central account
	 */
	public void setBlCentralAccount(boolean blCentralAccount) {
		this.blCentralAccount = blCentralAccount;
	}
	
	/**
	 * Gets the id bank.
	 *
	 * @return the id bank
	 */
	public Long getIdBank() {
		return idBank;
	}
	
	/**
	 * Sets the id bank.
	 *
	 * @param idBank the new id bank
	 */
	public void setIdBank(Long idBank) {
		this.idBank = idBank;
	}
	
	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 */
	public List<Bank> getLstBank() {
		return lstBank;
	}
	
	/**
	 * Sets the lst bank.
	 *
	 * @param lstBank the new lst bank
	 */
	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}
	
	public boolean isBlReturns() {
		return blReturns;
	}

	public void setBlReturns(boolean blReturns) {
		this.blReturns = blReturns;
	}
	
}
