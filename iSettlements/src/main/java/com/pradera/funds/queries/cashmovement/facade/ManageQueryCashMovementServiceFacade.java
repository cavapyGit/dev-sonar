package com.pradera.funds.queries.cashmovement.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.funds.fundsoperations.deposit.service.ManageDepositServiceBean;
import com.pradera.funds.queries.cashmovement.service.ManageQueryCashMovementServiceBean;
import com.pradera.funds.queries.cashmovement.to.CashAccountMovementTO;
import com.pradera.funds.queries.cashmovement.to.SearchCashMovementTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageQueryCashMovementServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageQueryCashMovementServiceFacade {

	/** The manage deposit service bean. */
	@Inject
	ManageDepositServiceBean manageDepositServiceBean;
	
	/** The query cash movement service bean. */
	@EJB
	ManageQueryCashMovementServiceBean queryCashMovementServiceBean;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Gets the participants int depositary for deposit.
	 *
	 * @return the participants int depositary for deposit
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantsIntDepositaryForDeposit() throws ServiceException{
		return manageDepositServiceBean.getParticipantsIntDepositaryForDeposit();
	}

	/**
	 * Gets the participants for deposit.
	 *
	 * @param blDirect the bl direct
	 * @return the participants for deposit
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantsForDeposit(boolean blDirect) throws ServiceException{
		return manageDepositServiceBean.getParticipantsForDeposit(blDirect);
	}

	/**
	 * Gets the issuers for deposit.
	 *
	 * @return the issuers for deposit
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> getIssuersForDeposit() throws ServiceException{
		return manageDepositServiceBean.getIssuersForDeposit();
	}

	/**
	 * Search cash accounts.
	 *
	 * @param searchCashMovementTO the search cash movement to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EFFECTIVE_ACCOUNT_QUERY)
	public List<InstitutionCashAccount> searchCashAccounts(SearchCashMovementTO searchCashMovementTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return queryCashMovementServiceBean.searchCashAccounts(searchCashMovementTO);
	}
	
	/**
	 * Search cash movements.
	 *
	 * @param searchCashMovementTO the search cash movement to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.EFFECTIVE_ACCOUNT_QUERY_MOVEMENTS)
	public List<CashAccountMovementTO> searchCashMovements(SearchCashMovementTO searchCashMovementTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<Object[]> lstResult = queryCashMovementServiceBean.searchCashMovements(searchCashMovementTO);
		if(Validations.validateIsNotNullAndNotEmpty(lstResult)){
			List<CashAccountMovementTO> lstTemp = new ArrayList<CashAccountMovementTO>();
			for(Object[] objResult:lstResult){
				CashAccountMovementTO cashAccMovementTO = new CashAccountMovementTO();
				cashAccMovementTO.setMovementDesc(objResult[0].toString());
				cashAccMovementTO.setIdCashAccountMovementPk((Long)objResult[4]);
				cashAccMovementTO.setIdFundOperationPk((Long)objResult[2]);				
				//Integer operationClass = (Integer)objResult[1];
				/*if(OperationClassType.RECEPTION_FUND.getCode().equals(operationClass))
					cashAccMovementTO.setReceptionAmount((BigDecimal)objResult[3]);
				else if(OperationClassType.SEND_FUND.getCode().equals(operationClass))
					cashAccMovementTO.setSendAmount((BigDecimal)objResult[3]);
				else
					cashAccMovementTO.setReferentialAmount((BigDecimal)objResult[3]);*/
				cashAccMovementTO.setAmount((BigDecimal)objResult[3]);
				cashAccMovementTO.setMovementDate((Date)objResult[5]);
				cashAccMovementTO.setPaymentReference(Validations.validateIsNotNullAndNotEmpty(objResult[6])?objResult[6].toString():GeneralConstants.DASH);
				cashAccMovementTO.setObservations(Validations.validateIsNotNullAndNotEmpty(objResult[7])?objResult[7].toString():GeneralConstants.DASH);
				if(objResult.length>8)
					cashAccMovementTO.setBankDescription(Validations.validateIsNotNullAndNotEmpty(objResult[8])?objResult[8].toString():GeneralConstants.DASH);
				lstTemp.add(cashAccMovementTO);
			}
			return lstTemp;
		}else
			return null;
	}

	/**
	 * Gets the lst modality group.
	 *
	 * @param negotiationMechanismSel the negotiation mechanism sel
	 * @return the lst modality group
	 * @throws ServiceException the service exception
	 */
	public List<ModalityGroup> getLstModalityGroup(Long negotiationMechanismSel) throws ServiceException{
		return queryCashMovementServiceBean.getLstModalityGroup(negotiationMechanismSel);
	}

	/**
	 * Gets the lst mechanism.
	 *
	 * @param participantSel the participant sel
	 * @return the lst mechanism
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getLstMechanism(Long participantSel) throws ServiceException{
		return queryCashMovementServiceBean.getLstMechanism(participantSel);
	}

}
