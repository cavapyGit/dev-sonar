package com.pradera.funds.queries.cashmovement.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CashAccountMovementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class CashAccountMovementTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id cash account movement pk. */
	private Long idFundOperationPk, idCashAccountMovementPk;
	
	/** The payment reference. */
	private String movementDesc, observations, paymentReference;
	
	/** The movement date. */
	private Date movementDate;
	
	/** The amount. */
	private BigDecimal amount;
	
	/** The bank description. */
	private String bankDescription;
	
	/**
	 * Gets the id fund operation pk.
	 *
	 * @return the id fund operation pk
	 */
	public Long getIdFundOperationPk() {
		return idFundOperationPk;
	}
	
	/**
	 * Sets the id fund operation pk.
	 *
	 * @param idFundOperationPk the new id fund operation pk
	 */
	public void setIdFundOperationPk(Long idFundOperationPk) {
		this.idFundOperationPk = idFundOperationPk;
	}
	
	/**
	 * Gets the movement desc.
	 *
	 * @return the movement desc
	 */
	public String getMovementDesc() {
		return movementDesc;
	}
	
	/**
	 * Sets the movement desc.
	 *
	 * @param movementDesc the new movement desc
	 */
	public void setMovementDesc(String movementDesc) {
		this.movementDesc = movementDesc;
	}
	
	/**
	 * Gets the id cash account movement pk.
	 *
	 * @return the id cash account movement pk
	 */
	public Long getIdCashAccountMovementPk() {
		return idCashAccountMovementPk;
	}
	
	/**
	 * Sets the id cash account movement pk.
	 *
	 * @param idCashAccountMovementPk the new id cash account movement pk
	 */
	public void setIdCashAccountMovementPk(Long idCashAccountMovementPk) {
		this.idCashAccountMovementPk = idCashAccountMovementPk;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	
	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	
	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}
	
	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}
	
	/**
	 * Gets the payment reference.
	 *
	 * @return the payment reference
	 */
	public String getPaymentReference() {
		return paymentReference;
	}
	
	/**
	 * Sets the payment reference.
	 *
	 * @param paymentReference the new payment reference
	 */
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
	
	/**
	 * Gets the bank description.
	 *
	 * @return the bank description
	 */
	public String getBankDescription() {
		return bankDescription;
	}
	
	/**
	 * Sets the bank description.
	 *
	 * @param bankDescription the new bank description
	 */
	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}	
	
}
