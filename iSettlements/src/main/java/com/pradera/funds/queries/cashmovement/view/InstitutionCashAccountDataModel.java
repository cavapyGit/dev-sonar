package com.pradera.funds.queries.cashmovement.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.funds.InstitutionCashAccount;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InstitutionCashAccountDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class InstitutionCashAccountDataModel extends ListDataModel<InstitutionCashAccount> implements SelectableDataModel<InstitutionCashAccount>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new institution cash account data model.
	 */
	public InstitutionCashAccountDataModel() {}
	
	/**
	 * Instantiates a new institution cash account data model.
	 *
	 * @param lstInstitutionCashAccount the lst institution cash account
	 */
	public InstitutionCashAccountDataModel(List<InstitutionCashAccount> lstInstitutionCashAccount) {
		super(lstInstitutionCashAccount);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public InstitutionCashAccount getRowData(String rowKey) {
		List<InstitutionCashAccount> lstInstitutionCashAccount = (List<InstitutionCashAccount>)getWrappedData();
		for(InstitutionCashAccount objTemp : lstInstitutionCashAccount){
			if(objTemp.getIdInstitutionCashAccountPk().toString().equals(rowKey))
				return objTemp;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	public Object getRowKey(InstitutionCashAccount obj) {
		return obj.getIdInstitutionCashAccountPk();
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public Integer getSize(){
		List<InstitutionCashAccount> lstInstitutionCashAccount = (List<InstitutionCashAccount>)getWrappedData();
		return lstInstitutionCashAccount.size();
	}
	
}