package com.pradera.funds.queries.cashmovement.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.funds.bank.accounts.facade.ManageBankAccountsFacade;
import com.pradera.funds.queries.cashmovement.facade.ManageQueryCashMovementServiceFacade;
import com.pradera.funds.queries.cashmovement.to.CashAccountMovementTO;
import com.pradera.funds.queries.cashmovement.to.SearchCashMovementTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.FundOpeGroupEntityRelationType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageQueryCashMovementBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@LoggerCreateBean
@DepositaryWebBean
public class ManageQueryCashMovementBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search cash movement to. */
	private SearchCashMovementTO searchCashMovementTO;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The query cash movement service facade. */
	@EJB
	ManageQueryCashMovementServiceFacade queryCashMovementServiceFacade;
	
	/** The manage bank accounts facade. */
	@EJB
    ManageBankAccountsFacade manageBankAccountsFacade;
	
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The lst institution bank accounts type. */
	private List<ParameterTable> lstInstitutionBankAccountsTypeReturn;
	
	/** The obj institution bank accounts type. */
	private Integer objInstitutionBankAccountsType;
	
	/** The bl cam no result. */
	private boolean blICANoResult, blCAMNoResult;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The id participant. */
	private Long idParticipant;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){		
		searchCashMovementTO = new SearchCashMovementTO();
		searchCashMovementTO.setInitialDate(getCurrentSystemDate());
		searchCashMovementTO.setEndDate(getCurrentSystemDate());
		fillCombos();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			idParticipant=userInfo.getUserAccountSession().getParticipantCode();
		}
	}
	
	
	/**
	 * Clean.
	 */
	public void clean(){
		JSFUtilities.resetViewRoot();
		blICANoResult = false;
		blCAMNoResult = false;
		init();
	}
	
	
	/**
	 * Change fund operation group.
	 */
	public void changeFundOperationGroup(){
		try {			
			clearResult();
			ParameterTable fundOpeGroupSelected = null;
			//Get the entity relation for the funds operation group
			for(ParameterTable paramTab:searchCashMovementTO.getLstFundOperationGroup()){
				if(paramTab.getParameterTablePk().equals(searchCashMovementTO.getFundOperationGroupSel())){
					fundOpeGroupSelected = paramTab;
					break;
				}
			}
			searchCashMovementTO.setBlCevaldom(false);
			searchCashMovementTO.setBlParticipant(false);
			searchCashMovementTO.setBlIssuer(false);
			searchCashMovementTO.setBlBenefit(false);
			searchCashMovementTO.setBlReturns(false);
			searchCashMovementTO.setBlSettlement(false);
			searchCashMovementTO.setNegotiationMechanismSel(null);
			searchCashMovementTO.setModalityGroupSel(null);
			searchCashMovementTO.setParticipantSel(null);
			searchCashMovementTO.setIssuerSel(null);
			objInstitutionBankAccountsType = null;
			if(Validations.validateIsNotNullAndNotEmpty(fundOpeGroupSelected)){
				if(FundOpeGroupEntityRelationType.EDV.getCode().equals(new Integer(fundOpeGroupSelected.getIndicator2()))){
					searchCashMovementTO.setBlCevaldom(true);
					searchCashMovementTO.setCevaldom(InstitutionType.DEPOSITARY.getValue());
				}else if(FundOpeGroupEntityRelationType.PARTICIPANT.getCode().equals(new Integer(fundOpeGroupSelected.getIndicator2()))){
					if(AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode().equals(searchCashMovementTO.getFundOperationGroupSel()))
						searchCashMovementTO.setLstParticipant(queryCashMovementServiceFacade.getParticipantsIntDepositaryForDeposit());
					else{
						if(AccountCashFundsType.SETTLEMENT.getCode().equals(searchCashMovementTO.getFundOperationGroupSel())){
							searchCashMovementTO.setBlSettlement(true);
							if(userInfo.getUserAccountSession().isParticipantInstitucion()){
								changeParticipant();
							}
							searchCashMovementTO.setLstParticipant(queryCashMovementServiceFacade.getParticipantsForDeposit(true));
							searchCashMovementTO.setLstBank(manageBankAccountsFacade.getListBank(null,BankStateType.REGISTERED.getCode()));
						}else 
							searchCashMovementTO.setLstParticipant(queryCashMovementServiceFacade.getParticipantsForDeposit(false));
					}	
					searchCashMovementTO.setBlParticipant(true);
					searchCashMovementTO.setParticipantSel(idParticipant);
					if(userInfo.getUserAccountSession().isParticipantInstitucion()){
						searchCashMovementTO.setLstMechanism(queryCashMovementServiceFacade.getLstMechanism(searchCashMovementTO.getParticipantSel()));
					}					
				}else if(FundOpeGroupEntityRelationType.ISSUER.getCode().equals(new Integer(fundOpeGroupSelected.getIndicator2()))){
					searchCashMovementTO.setBlIssuer(true);
					searchCashMovementTO.setLstIssuer(queryCashMovementServiceFacade.getIssuersForDeposit());
					if(AccountCashFundsType.BENEFIT.getCode().equals(searchCashMovementTO.getFundOperationGroupSel())){
						searchCashMovementTO.setBlReturns(true);
						searchCashMovementTO.setBlIssuer(false);
						searchCashMovementTO.setLstParticipant(queryCashMovementServiceFacade.getParticipantsForDeposit(false));
						fillInstitutionBankAccountsTypeReturn();
						if(userInfo.getUserAccountSession().isParticipantInstitucion()){
							onChangeInstitutionType();
						}
					}
				}
				if(AccountCashFundsType.RETURN.getCode().equals(searchCashMovementTO.getFundOperationGroupSel())) {
					searchCashMovementTO.setBlCevaldom(false);
					searchCashMovementTO.setCevaldom(null);
					searchCashMovementTO.setBlReturns(true);
					searchCashMovementTO.setLstIssuer(queryCashMovementServiceFacade.getIssuersForDeposit());
					searchCashMovementTO.setLstParticipant(queryCashMovementServiceFacade.getParticipantsForDeposit(false));
					fillInstitutionBankAccountsTypeReturn();
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}

	/**
	 * Change participant.
	 */
	public void changeParticipant(){
		//JSFUtilities.hideGeneralDialogues();
		//clearResult();
		JSFUtilities.hideGeneralDialogues();
		blCAMNoResult = false;
		blICANoResult = false;
		searchCashMovementTO.setLstInstitutionCashAccount(null);
		searchCashMovementTO.setInstitutionCashAccount(null);
		searchCashMovementTO.setInitialDate(getCurrentSystemDate());
		searchCashMovementTO.setEndDate(getCurrentSystemDate());
		searchCashMovementTO.setLstCashAccountMovementTO(null);
		try {
			if(searchCashMovementTO.isBlSettlement()){
				searchCashMovementTO.setNegotiationMechanismSel(null);
				searchCashMovementTO.setLstModalityGroup(null);
				searchCashMovementTO.setModalityGroupSel(null);
				searchCashMovementTO.setLstMechanism(queryCashMovementServiceFacade.getLstMechanism(searchCashMovementTO.getParticipantSel()));
			}else if(searchCashMovementTO.isBlBenefit()){
				searchCashMovementTO.setIssuerSel(null);
			}else if(searchCashMovementTO.isBlReturns()){
				searchCashMovementTO.setNegotiationMechanismSel(null);
				searchCashMovementTO.setLstModalityGroup(null);
				searchCashMovementTO.setModalityGroupSel(null);
				searchCashMovementTO.setIssuerSel(null);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Change negotiation mechanism.
	 */
	public void changeNegotiationMechanism(){
		JSFUtilities.hideGeneralDialogues();
		clearResult();
		try {
			searchCashMovementTO.setModalityGroupSel(null);
			searchCashMovementTO.setLstModalityGroup(queryCashMovementServiceFacade.getLstModalityGroup(searchCashMovementTO.getNegotiationMechanismSel()));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	
	public void clearResult(){
		JSFUtilities.hideGeneralDialogues();
		blCAMNoResult = false;
		blICANoResult = false;
		searchCashMovementTO.setLstInstitutionCashAccount(null);
		searchCashMovementTO.setInstitutionCashAccount(null);
		searchCashMovementTO.setInitialDate(getCurrentSystemDate());
		searchCashMovementTO.setEndDate(getCurrentSystemDate());
		searchCashMovementTO.setLstCashAccountMovementTO(null);
		if(AccountCashFundsType.BENEFIT.getCode().equals(searchCashMovementTO.getFundOperationGroupSel())) {
			searchCashMovementTO.setParticipantSel(null);
		}
		if(searchCashMovementTO.isBlReturns()) {
			searchCashMovementTO.setParticipantSel(null);
		}
	}
	
	/**
	 * Search cash accounts.
	 */
	@LoggerAuditWeb
	public void searchCashAccounts(){
		JSFUtilities.hideGeneralDialogues();
		try {
			blCAMNoResult = false;
			searchCashMovementTO.setLstCashAccountMovementTO(null);
			searchCashMovementTO.setInstitutionCashAccount(null);
			List<InstitutionCashAccount> lstTemp = queryCashMovementServiceFacade.searchCashAccounts(searchCashMovementTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstTemp)){
				searchCashMovementTO.setLstInstitutionCashAccount(new InstitutionCashAccountDataModel(lstTemp));
				blICANoResult = false;
			}else{
				searchCashMovementTO.setLstInstitutionCashAccount(null);
				blICANoResult = true;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search cash movements.
	 */
	@LoggerAuditWeb
	public void searchCashMovements(){
		JSFUtilities.hideGeneralDialogues();
		try {
			if(Validations.validateIsNullOrEmpty(searchCashMovementTO.getInstitutionCashAccount())){
				JSFUtilities.showValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.query.cash.movement.not.selected"));
			}else{
				searchCashMovementTO.setBlCentralAccount(false);
				List<CashAccountMovementTO> lstTemp = queryCashMovementServiceFacade.searchCashMovements(searchCashMovementTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstTemp)){
					if(AccountCashFundsType.CENTRALIZING.getCode().equals(searchCashMovementTO.getInstitutionCashAccount().getAccountType()))
						searchCashMovementTO.setBlCentralAccount(true);
					blCAMNoResult = false;
					searchCashMovementTO.setLstCashAccountMovementTO(lstTemp);
				}else{
					blCAMNoResult = true;
					searchCashMovementTO.setLstCashAccountMovementTO(null);
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Fill combos.
	 */
	private void fillCombos(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();	
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				List<Integer> lstPks = new ArrayList<Integer>();
				lstPks.add(AccountCashFundsType.SETTLEMENT.getCode());
				lstPks.add(AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode());
				parameterTableTO.setLstParameterTablePk(lstPks);
			}else{
				parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_FUND_ACCOUNT_TYPE.getCode());
			}
			searchCashMovementTO.setLstFundOperationGroup(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Monedas
			parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
			for(ParameterTable paramTab:generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				if(CurrencyType.PYG.getCode().equals(paramTab.getParameterTablePk())
						|| CurrencyType.USD.getCode().equals(paramTab.getParameterTablePk()))
					lstTemp.add(paramTab);
			}
			searchCashMovementTO.setLstCurrency(lstTemp);
		} catch (ServiceException e){
			e.printStackTrace();
		}
	}
	
	public void onChangeInstitutionType() throws ServiceException{
		JSFUtilities.hideGeneralDialogues();
		searchCashMovementTO.setParticipantSel(null);
		searchCashMovementTO.setIssuerSel(null);
		if(AccountCashFundsType.BENEFIT.getCode().equals(searchCashMovementTO.getFundOperationGroupSel())) {
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				searchCashMovementTO.setLstParticipant(queryCashMovementServiceFacade.getParticipantsForDeposit(true));
				searchCashMovementTO.setParticipantSel(idParticipant);
				searchCashMovementTO.setLstMechanism(queryCashMovementServiceFacade.getLstMechanism(searchCashMovementTO.getParticipantSel()));
			}
		}
	}
	
	/**
	 * Fill institution bank accounts type.
	 */
	public void fillInstitutionBankAccountsTypeReturn(){
		lstInstitutionBankAccountsTypeReturn = new ArrayList<ParameterTable>();
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setIndicator4(GeneralConstants.ONE_VALUE_INTEGER);
		try {
			lstInstitutionBankAccountsTypeReturn.addAll(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
	}
	
	/**
	 * Gets the search cash movement to.
	 *
	 * @return the search cash movement to
	 */
	public SearchCashMovementTO getSearchCashMovementTO() {
		return searchCashMovementTO;
	}
	
	/**
	 * Sets the search cash movement to.
	 *
	 * @param searchCashMovementTO the new search cash movement to
	 */
	public void setSearchCashMovementTO(SearchCashMovementTO searchCashMovementTO) {
		this.searchCashMovementTO = searchCashMovementTO;
	}
	
	/**
	 * Checks if is bl ica no result.
	 *
	 * @return true, if is bl ica no result
	 */
	public boolean isBlICANoResult() {
		return blICANoResult;
	}
	
	/**
	 * Sets the bl ica no result.
	 *
	 * @param blICANoResult the new bl ica no result
	 */
	public void setBlICANoResult(boolean blICANoResult) {
		this.blICANoResult = blICANoResult;
	}
	
	/**
	 * Checks if is bl cam no result.
	 *
	 * @return true, if is bl cam no result
	 */
	public boolean isBlCAMNoResult() {
		return blCAMNoResult;
	}
	
	/**
	 * Sets the bl cam no result.
	 *
	 * @param blCAMNoResult the new bl cam no result
	 */
	public void setBlCAMNoResult(boolean blCAMNoResult) {
		this.blCAMNoResult = blCAMNoResult;
	}


	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}


	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipant(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(objInstitutionBankAccountsType));
	}	
	
	/**
	 * Is Institution Type Issuer.
	 *
	 * @return validate Institution Type Issuer
	 */
	public boolean isIndInstitutionTypeIssuer(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.ISSUER.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Is Institution Type Bank.
	 *
	 * @return validate Institution Type Bank
	 */
	public boolean isIndInstitutionTypeBank(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.BANK.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	public List<ParameterTable> getLstInstitutionBankAccountsTypeReturn() {
		return lstInstitutionBankAccountsTypeReturn;
	}

	public void setLstInstitutionBankAccountsTypeReturn(List<ParameterTable> lstInstitutionBankAccountsTypeReturn) {
		this.lstInstitutionBankAccountsTypeReturn = lstInstitutionBankAccountsTypeReturn;
	}
	
	/**
	 * Gets the obj institution bank accounts type.
	 *
	 * @return the obj institution bank accounts type
	 */
	public Integer getObjInstitutionBankAccountsType() {
		return objInstitutionBankAccountsType;
	}

	/**
	 * Sets the obj institution bank accounts type.
	 *
	 * @param objInstitutionBankAccountsType the new obj institution bank accounts type
	 */
	public void setObjInstitutionBankAccountsType(
			Integer objInstitutionBankAccountsType) {
		this.objInstitutionBankAccountsType = objInstitutionBankAccountsType;
	}
	
}
