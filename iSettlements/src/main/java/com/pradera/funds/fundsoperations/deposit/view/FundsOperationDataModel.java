package com.pradera.funds.fundsoperations.deposit.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.funds.FundsOperation;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FundsOperationDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class FundsOperationDataModel  extends ListDataModel<FundsOperation> implements SelectableDataModel<FundsOperation>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new funds operation data model.
	 */
	public FundsOperationDataModel() {}
	
	/**
	 * Instantiates a new funds operation data model.
	 *
	 * @param lstFundsOperationDataModel the lst funds operation data model
	 */
	public FundsOperationDataModel(List<FundsOperation> lstFundsOperationDataModel) {
		super(lstFundsOperationDataModel);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public FundsOperation getRowData(String rowKey) {
		List<FundsOperation> lstFundsOperationDataModel = (List<FundsOperation>)getWrappedData();
		for(FundsOperation fundOperation : lstFundsOperationDataModel){
			if(fundOperation.getIdFundsOperationPk().toString().equals(rowKey))
				return fundOperation;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	public Object getRowKey(FundsOperation fundOperation) {
		return fundOperation.getIdFundsOperationPk();
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public Integer getSize(){
		List<FundsOperation> lstFundsOperationDataModel = (List<FundsOperation>)getWrappedData();
		return lstFundsOperationDataModel.size();
	}
	
}