package com.pradera.funds.fundsoperations.retirement.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class InternationalOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The id international operation pk. */
	private Long idInternationalOperationPk;
	
	/** The currency. */
	private Integer currency;
	
	/** The cash amount. */
	private BigDecimal cashAmount;

	/** The holder account. */
	private Long holderAccount;

	/** The isin code. */
	private String isinCode;

	/** The local participant. */
	private Long localParticipant;
	
	/** The International participant. */
	private Long internationalParticipant;
	
	/** The settlement depository. */
	private Long settlementDepository;

	/** The trade depository. */
	private Long tradeDepository;

	/** The nominal value. */
	private BigDecimal nominalValue;

	/** The operation date. */
	private Date operationDate;

	/** The operation number. */
	private Long operationNumber;

	/** The operation price. */
	private BigDecimal operationPrice;

	/** The real settlement date. */
	private Date realSettlementDate;

	/** The register date. */
	private Date registerDate;

	/** The settlement date. */
	private Date settlementDate;

	/** The settlement type. */
	private Integer settlementType;

	/** The operation state. */
	private Long operationState;

	/** The stock quantity. */
	private BigDecimal stockQuantity;

	/** The transfer type. */
	private Long transferType;         

	/** The settlement amount. */
	private BigDecimal settlementAmount;

	/** The deposit amount. */
	private BigDecimal depositAmount;

	/** The funds operation. */
	private Long fundsOperation;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The initial settlement date. */
	private Date initialSettlementDate;
	
	/** The end settlement date. */
	private Date endSettlementDate;
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the id international operation pk.
	 *
	 * @return the id international operation pk
	 */
	public Long getIdInternationalOperationPk() {
		return idInternationalOperationPk;
	}

	/**
	 * Sets the id international operation pk.
	 *
	 * @param idInternationalOperationPk the new id international operation pk
	 */
	public void setIdInternationalOperationPk(Long idInternationalOperationPk) {
		this.idInternationalOperationPk = idInternationalOperationPk;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the isin code.
	 *
	 * @return the isin code
	 */
	public String getIsinCode() {
		return isinCode;
	}

	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the new isin code
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}

	/**
	 * Gets the local participant.
	 *
	 * @return the local participant
	 */
	public Long getLocalParticipant() {
		return localParticipant;
	}

	/**
	 * Sets the local participant.
	 *
	 * @param localParticipant the new local participant
	 */
	public void setLocalParticipant(Long localParticipant) {
		this.localParticipant = localParticipant;
	}

	/**
	 * Gets the settlement depository.
	 *
	 * @return the settlement depository
	 */
	public Long getSettlementDepository() {
		return settlementDepository;
	}

	/**
	 * Sets the settlement depository.
	 *
	 * @param settlementDepository the new settlement depository
	 */
	public void setSettlementDepository(Long settlementDepository) {
		this.settlementDepository = settlementDepository;
	}

	/**
	 * Gets the trade depository.
	 *
	 * @return the trade depository
	 */
	public Long getTradeDepository() {
		return tradeDepository;
	}

	/**
	 * Sets the trade depository.
	 *
	 * @param tradeDepository the new trade depository
	 */
	public void setTradeDepository(Long tradeDepository) {
		this.tradeDepository = tradeDepository;
	}

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominal value
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the new nominal value
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the operation price.
	 *
	 * @return the operation price
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}

	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the new operation price
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}

	/**
	 * Gets the real settlement date.
	 *
	 * @return the real settlement date
	 */
	public Date getRealSettlementDate() {
		return realSettlementDate;
	}

	/**
	 * Sets the real settlement date.
	 *
	 * @param realSettlementDate the new real settlement date
	 */
	public void setRealSettlementDate(Date realSettlementDate) {
		this.realSettlementDate = realSettlementDate;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}

	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	/**
	 * Gets the settlement type.
	 *
	 * @return the settlement type
	 */
	public Integer getSettlementType() {
		return settlementType;
	}

	/**
	 * Sets the settlement type.
	 *
	 * @param settlementType the new settlement type
	 */
	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Long getOperationState() {
		return operationState;
	}

	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Long operationState) {
		this.operationState = operationState;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the transfer type.
	 *
	 * @return the transfer type
	 */
	public Long getTransferType() {
		return transferType;
	}

	/**
	 * Sets the transfer type.
	 *
	 * @param transferType the new transfer type
	 */
	public void setTransferType(Long transferType) {
		this.transferType = transferType;
	}

	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	/**
	 * Gets the deposit amount.
	 *
	 * @return the deposit amount
	 */
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	/**
	 * Sets the deposit amount.
	 *
	 * @param depositAmount the new deposit amount
	 */
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	/**
	 * Gets the funds operation.
	 *
	 * @return the funds operation
	 */
	public Long getFundsOperation() {
		return fundsOperation;
	}

	/**
	 * Sets the funds operation.
	 *
	 * @param fundsOperation the new funds operation
	 */
	public void setFundsOperation(Long fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	/**
	 * Gets the initial settlement date.
	 *
	 * @return the initial settlement date
	 */
	public Date getInitialSettlementDate() {
		return initialSettlementDate;
	}

	/**
	 * Sets the initial settlement date.
	 *
	 * @param initialSettlementDate the new initial settlement date
	 */
	public void setInitialSettlementDate(Date initialSettlementDate) {
		this.initialSettlementDate = initialSettlementDate;
	}

	/**
	 * Gets the end settlement date.
	 *
	 * @return the end settlement date
	 */
	public Date getEndSettlementDate() {
		return endSettlementDate;
	}

	/**
	 * Sets the end settlement date.
	 *
	 * @param endSettlementDate the new end settlement date
	 */
	public void setEndSettlementDate(Date endSettlementDate) {
		this.endSettlementDate = endSettlementDate;
	}

	/**
	 * Gets the international participant.
	 *
	 * @return the international participant
	 */
	public Long getInternationalParticipant() {
		return internationalParticipant;
	}

	/**
	 * Sets the international participant.
	 *
	 * @param internationalParticipant the new international participant
	 */
	public void setInternationalParticipant(Long internationalParticipant) {
		this.internationalParticipant = internationalParticipant;
	}
}
