package com.pradera.funds.fundsoperations.retirement.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.funds.fundsoperations.retirement.to.HolderCashMovementTO;
import com.pradera.funds.fundsoperations.retirement.to.HolderFundsOperationTO;
import com.pradera.funds.fundsoperations.retirement.to.InternationalOperationTO;
import com.pradera.funds.fundsoperations.retirement.to.MechanismOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.corporatives.BenefitPaymentAllocation;
import com.pradera.model.corporatives.BenefitPaymentFile;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.SwiftMessage;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.InternationalParticipant;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementOperation;

// TODO: Auto-generated Javadoc
/**
 * The Class RetirementFundsOperationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
public class RetirementFundsOperationServiceBean extends CrudDaoServiceBean {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Search funds operations types.
	 *
	 * @param operationGroup the operation group
	 * @param view the view
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<FundsOperationType> searchFundsOperationsTypes(Integer operationGroup, String view) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("SELECT FOT.idFundsOperationTypePk,");
		sbQuery.append("       FOT.name,");
		sbQuery.append("       FOT.cashAccountType");
		sbQuery.append("  FROM FundsOperationType FOT");
		sbQuery.append(" WHERE FOT.operationGroup =:operationGroup");
		sbQuery.append("   AND FOT.operationClass = 2");
		sbQuery.append( "AND nvl(FOT.excecutionType,0) <> 2");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("operationGroup", operationGroup);
		
		List<Object> oList = query.getResultList();
		List<FundsOperationType> fundsOperationsTypes = new ArrayList<FundsOperationType>();
		
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			FundsOperationType fundsOperationType = new FundsOperationType();
			fundsOperationType.setIdFundsOperationTypePk((Long)objects[0]);
			fundsOperationType.setName((String)objects[1]);
			fundsOperationType.setCashAccountType((Integer)objects[2]);
			
			fundsOperationsTypes.add(fundsOperationType);
		}
		return fundsOperationsTypes;
	}
	
	/**
	 * Find international operation by funds operation.
	 *
	 * @param idFundsOperationPk the id funds operation pk
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public InternationalOperation findInternationalOperationByFundsOperation(Long idFundsOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("  SELECT ");
		sbQuery.append("          IO.idInternationalOperationPk,");//0
		sbQuery.append("          IO.currency,");//1
		sbQuery.append("          IO.cashAmount,");//2
		sbQuery.append("          IO.securities.idIsinCodePk,");//3		
		sbQuery.append("          IO.localParticipant.idParticipantPk,");//4		
		sbQuery.append("          IO.localParticipant.description,");		//5
		sbQuery.append("          IO.registerDate,");		//6
		sbQuery.append("          IO.settlementDate,");//7
		sbQuery.append("          IO.internationalParticipant.idInterParticipantPk,");//8
		sbQuery.append("          IO.internationalParticipant.description,");		//9
		sbQuery.append("          IO.operationNumber,");//10
		sbQuery.append("          IO.transferType,");//2
		sbQuery.append("          IO.settlementDepository.idInternationalDepositoryPk,");//3
		sbQuery.append("          IO.tradeDepository.idInternationalDepositoryPk");//4
		sbQuery.append("  FROM    FundsInternationalOperation FIO ");
		sbQuery.append("LEFT JOIN FIO.internationalOperation IO");
		sbQuery.append(" WHERE FIO.fundsOperation.idFundsOperationPk =:idFundsOperationPk ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idFundsOperationPk", idFundsOperationPk);
		List<Object> oList = query.getResultList();
		InternationalOperation internationalOperation = new InternationalOperation();
		//BUILDING DATA
		for(int i = 0; i < oList.size();++i){		
				
			Object[] objects =  (Object[]) oList.get(i);
			internationalOperation.setIdInternationalOperationPk((Long)objects[0]);
			//SETTING CURRENCY
			internationalOperation.setCurrency((Integer)objects[1]);
			//SETTING CASH AMOUNT
			internationalOperation.setCashAmount((BigDecimal)objects[2]);
	
			//CREATING SECURITY
			Security security = new Security();
			//SETTING ISIN CODE PK
			security.setIdSecurityCodePk((String)objects[3]);
			
			internationalOperation.setSecurities(security);
			
			Participant localParticipant = new Participant();
			localParticipant.setIdParticipantPk((Long)objects[4]);
			localParticipant.setDescription((String)objects[5]);
	
			InternationalParticipant internationalParticipant = new InternationalParticipant();
			internationalParticipant.setIdInterParticipantPk((Long)objects[8]);
			internationalParticipant.setDescription((String)objects[9]);
			
			internationalOperation.setLocalParticipant(localParticipant);
			internationalOperation.setInternationalParticipant(internationalParticipant);
			
			internationalOperation.setRegisterDate((Date)objects[6]);
			internationalOperation.setSettlementDate((Date)objects[7]);
			internationalOperation.setOperationNumber((Long)objects[10]);
			internationalOperation.setTransferType((Long)objects[11]);
			
			InternationalDepository settlementDepository = new InternationalDepository();
			settlementDepository.setIdInternationalDepositoryPk((Long)objects[12]);
			
			InternationalDepository tradeDepository = new InternationalDepository();
			tradeDepository.setIdInternationalDepositoryPk((Long)objects[13]);
			
			internationalOperation.setSettlementDepository(settlementDepository);
			internationalOperation.setTradeDepository(tradeDepository);		
			break;
		}
		return internationalOperation;
	}
	
	/**
	 * Search holder cash movement.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderCashMovement> searchHolderCashMovement(HolderCashMovementTO filter) throws ServiceException{		
		Map<String, Object> mapParam = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("SELECT HCM.idHolderCashMovementPk,");
		sbQuery.append("       HCM.movementDate,");
		sbQuery.append("       HCM.bankAccountType,");
		sbQuery.append("       HCM.bankAccountNumber,");
		sbQuery.append("       HCM.bank.description,");
		sbQuery.append("       HCM.holderAmount,");
		sbQuery.append("       HCM.bank.idBankPk,");
		sbQuery.append("       HCM.bank.mnemonic,");
		sbQuery.append("       HCM.holder.idHolderPk,");
		sbQuery.append("       HCM.holder.fullName,");
		sbQuery.append("       HCM.documentHolder");
		sbQuery.append("  FROM HolderCashMovement HCM");
		sbQuery.append(" WHERE 1 = 1");
		
		if(Validations.validateIsNotNull(filter.getCurrency())){
			sbQuery.append("   AND HCM.currency =:currency");
			mapParam.put("currency", filter.getCurrency());
		}
		
		if(Validations.validateIsNotNull(filter.getHolder())){
			sbQuery.append("   AND HCM.holder.idHolderPk =:holder");
			mapParam.put("holder", filter.getHolder());
		}
		if(Validations.validateIsNotNull(filter.getBank())){
			sbQuery.append("   AND HCM.bank.idBankPk =:idBank");
			mapParam.put("idBank", filter.getBank());
		}
		if(Validations.validateIsNotNull(filter.getMovementState())){
			sbQuery.append("   AND HCM.movementState =:movementState");
			mapParam.put("movementState", filter.getMovementState());
		}
		if(Validations.validateIsNotNull(filter.getFundsOperationType())){
			sbQuery.append("   AND HCM.fundsOperationType.idFundsOperationTypePk =:idFundsOperationTypePk");
			mapParam.put("idFundsOperationTypePk", filter.getFundsOperationType());
		}
		if(Validations.validateIsNotNull(filter.getIdBenefitPaymentPk())){
			sbQuery.append("   AND HCM.fundsOperation.benefitPaymentAllocation.idBenefitPaymentPK =:idBenefitPaymentPk");
			mapParam.put("idBenefitPaymentPk", filter.getIdBenefitPaymentPk());
		}
		Query query = em.createQuery(sbQuery.toString());
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		List<Object> oList = query.getResultList();
		List<HolderCashMovement> holderCashMovements = new ArrayList<HolderCashMovement>();
		
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			HolderCashMovement holderCashMovement = new HolderCashMovement();
			holderCashMovement.setIdHolderCashMovementPk((Long)objects[0]);
			holderCashMovement.setMovementDate((Date)objects[1]);
			holderCashMovement.setBankAccountType((Integer)objects[2]);
			holderCashMovement.setBankAccountNumber((String)objects[3]);
			
			Bank bank = new Bank();
			bank.setIdBankPk((Long)objects[6]);
			bank.setDescription((String)objects[4]);
			bank.setMnemonic((String)objects[7]);
			
			Holder holder = new Holder();
			holder.setIdHolderPk((Long)objects[8]);
			holder.setFullName((String)objects[9]);
			
			holderCashMovement.setHolder(holder);
			holderCashMovement.setDocumentHolder((String)objects[10]);
			holderCashMovement.setBank(bank);
			holderCashMovement.setHolderAmount((BigDecimal)objects[5]);
			
			holderCashMovements.add(holderCashMovement);
		}
		return holderCashMovements;
	}
	
	/**
	 * Gets the participant mnemonic.
	 *
	 * @param participantId the participant id
	 * @return the participant mnemonic
	 * @throws ServiceException the service exception
	 */
	public String getParticipantMnemonic(Long participantId) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("SELECT P.mnemonic");		
		sbQuery.append("  FROM Participant P");
		sbQuery.append(" WHERE P.idParticipantPk =:participantId");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("participantId", participantId);
		String mnemonic = (String) query.getSingleResult();
		return mnemonic;
	}
	
	/**
	 * Gets the issuer mnemonic.
	 *
	 * @param issuerId the issuer id
	 * @return the issuer mnemonic
	 * @throws ServiceException the service exception
	 */
	public String getIssuerMnemonic(String issuerId) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("SELECT I.mnemonic");		
		sbQuery.append("  FROM Issuer I");
		sbQuery.append(" WHERE I.idIssuerPk=:issuerId");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("issuerId", issuerId);
		String mnemonic = (String) query.getSingleResult();
		return mnemonic;
	}
	
	/**
	 * Find participant settlement.
	 *
	 * @param idMechanismOperationFk the id mechanism operation fk
	 * @param idParticipantPk the id participant pk
	 * @param operationPart the operation part
	 * @return the participant settlement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public ParticipantSettlement findParticipantSettlement(Long idMechanismOperationFk, Long idParticipantPk, Integer operationPart) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("  SELECT ");
		sbQuery.append("         ps.idParticipantSettlementPk,");
		sbQuery.append("         ps.settlementAmount,");
		sbQuery.append("         ps.depositedAmount,");
		sbQuery.append("         ps.fundsReference");
		sbQuery.append("    FROM ParticipantSettlement ps");
		sbQuery.append("   WHERE ps.settlementOperation.mechanismOperation.idMechanismOperationPk  =:idMechanismOperationFk");
		sbQuery.append("     AND ps.participant.idParticipantPk =:idParticipantPk");
		sbQuery.append("     AND ps.role = :role");
		sbQuery.append("     AND (ps.fundsReference =:depositReference ");
		sbQuery.append("     OR ps.fundsReference =:fcReference ");
		sbQuery.append("     OR ps.fundsReference =null) ");
		if(Validations.validateIsNotNull(operationPart)){
			sbQuery.append("     AND ps.operationPart =:operationPart");
		}
		
		
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationFk", idMechanismOperationFk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("depositReference", AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
		query.setParameter("fcReference", AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
		query.setParameter("role",  ParticipantRoleType.BUY.getCode());
		if(Validations.validateIsNotNull(operationPart)){
			query.setParameter("operationPart", operationPart);
		}		
		
		List<Object> oList = query.getResultList();
		ParticipantSettlement participantSettlement = new ParticipantSettlement();
		
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			participantSettlement.setIdParticipantSettlementPk((Long)objects[0]);
			
			participantSettlement.setSettlementAmount((BigDecimal)objects[1]);
			participantSettlement.setDepositedAmount((BigDecimal)objects[2]);
			participantSettlement.setFundsReference((Long)objects[3]);	
			break;
		}
		return participantSettlement;
	}
	
	/**
	 * Find participant operation.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @param operationPart the operation part
	 * @return the participant operation
	 * @throws ServiceException the service exception
	 */
	public ParticipantSettlement findParticipantSettlement(MechanismOperationTO mechanismOperationTO, Integer operationPart) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	

		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("  SELECT ");
		sbQuery.append("         ps.idParticipantSettlementPk,");// [0]
		sbQuery.append("         ps.settlementAmount,");				//[1]
		sbQuery.append("         ps.depositedAmount,");			//[2]
		sbQuery.append("         ps.fundsReference,");			//[3]
		sbQuery.append("         MO.idMechanismOperationPk,");	//[4]
		sbQuery.append("         MO.mechanisnModality.negotiationModality.idNegotiationModalityPk,"); //[5]
		sbQuery.append("         MO.operationNumber,"); //[6]
		sbQuery.append("         MO.operationDate,");	//[7]	
		sbQuery.append("         MO.currency,");		//[8]
		sbQuery.append("         MO.paymentReference,");//[9] 
		sbQuery.append("         MO.settlementSchema"); //[10]
		sbQuery.append("    FROM ParticipantSettlement ps, MechanismOperation MO, ModalityGroupDetail MGD");
		sbQuery.append("   WHERE ps.participant.idParticipantPk =:idParticipantPk");
		sbQuery.append("     AND ps.role = :role");
		sbQuery.append(" 	and ps.settlementOperation.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		sbQuery.append("	 and MGD.mechanismModality.id.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
		sbQuery.append(" 	 and MGD.mechanismModality.id.idNegotiationModalityPk = MO.mechanisnModality.id.idNegotiationModalityPk ");
		sbQuery.append("	 and MGD.modalityGroup.idModalityGroupPk = :idModalityGroupPk ");
		if(Validations.validateIsNotNull(operationPart)){
			sbQuery.append("     AND ps.settlementOperation.operationPart =:operationPart");
		}
		
		if (Validations.validateIsNotNull(mechanismOperationTO.getPaymentReference())) {
			sbQuery.append("     AND MO.paymentReference  =:paymentReference");
			mapParam.put("paymentReference",mechanismOperationTO.getPaymentReference());
		}
		if (Validations.validateIsNotNull(mechanismOperationTO.getIdNegotiationModalityPk())) {
			sbQuery.append("     AND MO.mechanisnModality.negotiationModality.idNegotiationModalityPk  =:idNegotiationModalityPk");
			mapParam.put("idNegotiationModalityPk",mechanismOperationTO.getIdNegotiationModalityPk());
		}
		if (Validations.validateIsNotNull(mechanismOperationTO.getIdNegotiationMechanismPk())) {
			sbQuery.append("     AND MO.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk =:idNegotiationMechanismPk");
			mapParam.put("idNegotiationMechanismPk",mechanismOperationTO.getIdNegotiationMechanismPk());
		}
		if (Validations.validateIsNotNull(mechanismOperationTO.getOperationDate())) {
			sbQuery.append("     AND trunc(MO.operationDate) =trunc(:operationDate)");
			mapParam.put("operationDate",mechanismOperationTO.getOperationDate());
		}
		if (Validations.validateIsNotNull(mechanismOperationTO.getOperationNumber())) {
			sbQuery.append("     AND MO.operationNumber =:operationNumber");
			mapParam.put("operationNumber",mechanismOperationTO.getOperationNumber());
		}
		
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idParticipantPk", mechanismOperationTO.getBuyerParticipant());
		query.setParameter("idModalityGroupPk", mechanismOperationTO.getIdModalityGroupPk());
		query.setParameter("role",  ParticipantRoleType.BUY.getCode());
		
		if(Validations.validateIsNotNull(operationPart)){
			query.setParameter("operationPart", operationPart);
		}		
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		List<Object> oList = query.getResultList();
		ParticipantSettlement participantSettlement = new ParticipantSettlement();
		
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			participantSettlement.setIdParticipantSettlementPk((Long)objects[0]);
			
			participantSettlement.setSettlementAmount((BigDecimal)objects[1]);
			participantSettlement.setDepositedAmount((BigDecimal)objects[2]);
			participantSettlement.setFundsReference((Long)objects[3]);
			
			MechanismOperation mechanismOperation = new MechanismOperation();
			mechanismOperation.setIdMechanismOperationPk((Long)objects[4]);
			
			MechanismModality mechanismModality = new MechanismModality();
			NegotiationModality negotiationModality = new NegotiationModality();
			negotiationModality.setIdNegotiationModalityPk((Long)objects[5]);
			
			mechanismModality.setNegotiationModality(negotiationModality);
			mechanismOperation.setMechanisnModality(mechanismModality);
			mechanismOperation.setOperationNumber((Long)objects[6]);
			mechanismOperation.setOperationDate((Date)objects[7]);
			mechanismOperation.setCurrency((Integer)objects[8]);
			mechanismOperation.setPaymentReference((String)objects[9]);
			mechanismOperation.setSettlementSchema((Integer)objects[10]);
			participantSettlement.setSettlementOperation(new SettlementOperation());
			participantSettlement.getSettlementOperation().setMechanismOperation(mechanismOperation);
				
		}
		return participantSettlement;
	}
	
	/**
	 * METHOD WHICH WILL BRING BCRD BANK BIC CODE.
	 *
	 * @return the centralizer bank bic code
	 * @throws ServiceException the service exception
	 */
	public String getCentralizerBankBicCode() throws ServiceException{
		String bicCode = "";
		
			Query query = em.createQuery("SELECT b.bicCode FROM Bank b WHERE b.bankType = :bankType and b.state = :state");
			query.setParameter("bankType",AccountCashFundsType.CENTRALIZING.getCode());
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getLngCode());
			bicCode =(String) query.getSingleResult();
		
		return bicCode;
	}

	/**
	 * METHOD WHICH WILL GET CEVALDOM'S BIC CODE.
	 *
	 * @return the cevaldom bic code
	 */
	public String getCevaldomBicCode(){
		String cevaldom = null;
		String sqlStr = "select p.bicCode from Participant p where p.indParticipantDepository = :indDepositary ";
		Query query =em.createQuery(sqlStr);
		query.setParameter("indDepositary", FundsType.IND_DEPOSITARY_PARTICIPANT.getCode());
		cevaldom = (String)query.getSingleResult();

		return cevaldom;
	}
	
	/**
	 * Gets the modality code and negotiation code.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the modality code and negotiation code
	 */
	public Object[] getModalityCodeAndNegotiationCode(Long idMechanismOperationPk){
		Object [] objects;
		String sqlStr = "select MO.negotiationMechanism.mechanismCode, MO.modalityCode from MechanismOperation MO where MO.idMechanismOperationPk = :idMechanismOperationPk ";
		Query query =em.createQuery(sqlStr);
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		objects = (Object [])query.getSingleResult();

		return objects;
	}
	
	/**
	 * Search institution cash account.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount searchInstitutionCashAccount(InstitutionCashAccountTO institutionCashAccountTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("  SELECT ");
		sbQuery.append("         ICA.idInstitutionCashAccountPk,");//0
		sbQuery.append("         ICA.totalAmount,");	//1
		sbQuery.append("         ICA.availableAmount,");	//2
		sbQuery.append("         ICA.currency,");		//3
		sbQuery.append("         ICA.modalityGroup.idModalityGroupPk,");//4
		sbQuery.append("         ICA.modalityGroup.groupName,");//5
		sbQuery.append("         ICA.negotiationMechanism.idNegotiationMechanismPk,");//6
		sbQuery.append("         ICA.negotiationMechanism.description,");//7
		sbQuery.append("         ICA.indRelatedBcrd");//8
		sbQuery.append("    FROM InstitutionCashAccount ICA");
		sbQuery.append("   WHERE 1=1");
		
		sbQuery.append(" AND ICA.accountState =:accountState");
		mapParam.put("accountState",CashAccountStateType.ACTIVATE.getCode());
		
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdParticipantFk())) {
			sbQuery.append("     AND ICA.participant.idParticipantPk =:idParticipantFk");
			mapParam.put("idParticipantFk",institutionCashAccountTO.getIdParticipantFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getSettlementSchema())) {
			sbQuery.append("     AND ICA.settlementSchema =:settlementSchema");
			mapParam.put("settlementSchema",institutionCashAccountTO.getSettlementSchema());
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getIdModalityGroupFk())){
			sbQuery.append("     AND ICA.modalityGroup.idModalityGroupPk =:idModalityGroupPk");
			mapParam.put("idModalityGroupPk",institutionCashAccountTO.getIdModalityGroupFk());
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getCurrency())){
			sbQuery.append("     AND ICA.currency =:currency");
			mapParam.put("currency",institutionCashAccountTO.getCurrency());
		}
		if(Validations.validateIsNotNull(institutionCashAccountTO.getIdNegotiationMechanismFk())){
			sbQuery.append("     AND ICA.negotiationMechanism.idNegotiationMechanismPk =:mechanism");
			mapParam.put("mechanism",institutionCashAccountTO.getIdNegotiationMechanismFk());
		}
		Query query = em.createQuery(sbQuery.toString());
		
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		
		List<Object> oList = query.getResultList();
		InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			institutionCashAccount.setIdInstitutionCashAccountPk((Long)objects[0]);
			institutionCashAccount.setTotalAmount((BigDecimal)objects[1]);
			institutionCashAccount.setAvailableAmount((BigDecimal)objects[2]);
			institutionCashAccount.setCurrency((Integer)objects[3]);
			
			ModalityGroup modalityGroup = new ModalityGroup();
			modalityGroup.setIdModalityGroupPk((Long)objects[4]);
			modalityGroup.setGroupName((String)objects[5]);
			
			NegotiationMechanism negotiationMechanism = new NegotiationMechanism();
			negotiationMechanism.setIdNegotiationMechanismPk((Long)objects[4]);
			negotiationMechanism.setDescription((String)objects[7]);
			institutionCashAccount.setIndRelatedBcrd((Integer)objects[8]);
			institutionCashAccount.setModalityGroup(modalityGroup);
			institutionCashAccount.setNegotiationMechanism(negotiationMechanism);			
			break;
		}		
		return institutionCashAccount;
	}
	
	/**
	 * Search institution cash accounts.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> searchInstitutionCashAccounts(InstitutionCashAccountTO institutionCashAccountTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("  SELECT ");
		sbQuery.append("          ICA.idInstitutionCashAccountPk,");//0
		sbQuery.append("          ICA.totalAmount,");	//1
		sbQuery.append("          ICA.availableAmount,");	//2
		sbQuery.append("          ICA.currency,");		//3
		sbQuery.append("          MG.idModalityGroupPk,");//4
		sbQuery.append("          MG.groupName,");//5
		sbQuery.append("          NM.idNegotiationMechanismPk,");//6
		sbQuery.append("          NM.description,");//7 
		sbQuery.append("          ICA.indRelatedBcrd,");//8
		sbQuery.append("          ICA.indSettlementProcess");//9
		sbQuery.append("    FROM  InstitutionCashAccount ICA ");
		sbQuery.append("LEFT JOIN ICA.modalityGroup MG ");
		sbQuery.append("LEFT JOIN ICA.negotiationMechanism NM");
		sbQuery.append("   WHERE 1=1");
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdParticipantFk())) {
			sbQuery.append("     AND ICA.participant.idParticipantPk =:idParticipantFk");
			mapParam.put("idParticipantFk",institutionCashAccountTO.getIdParticipantFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getCurrency())) {
			sbQuery.append("     AND ICA.currency =:currency");
			mapParam.put("currency",institutionCashAccountTO.getCurrency());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdModalityGroupFk())) {
			sbQuery.append("     AND ICA.modalityGroup.idModalityGroupPk =:idModalityGroupFk");
			mapParam.put("idModalityGroupFk",institutionCashAccountTO.getIdModalityGroupFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdNegotiationMechanismFk())) {
			sbQuery.append("     AND ICA.negotiationMechanism.idNegotiationMechanismPk =:idNegotiationMechanismFk");
			mapParam.put("idNegotiationMechanismFk",institutionCashAccountTO.getIdNegotiationMechanismFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getAccountType())) {
			sbQuery.append("     AND ICA.accountType =:accountType");
			mapParam.put("accountType",institutionCashAccountTO.getAccountType());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdIssuerFk())) {
			sbQuery.append("     AND ICA.issuer.idIssuerPk =:idIssuerFk");
			mapParam.put("idIssuerFk",institutionCashAccountTO.getIdIssuerFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getSettlementSchema())) {
			sbQuery.append("     AND ICA.settlementSchema =:settlementSchema");
			mapParam.put("settlementSchema",institutionCashAccountTO.getSettlementSchema());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getAccountState())) {
			sbQuery.append("     AND ICA.accountState =:accountState");
			mapParam.put("accountState",institutionCashAccountTO.getAccountState());
		}
		Query query = em.createQuery(sbQuery.toString());
			
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		List<Object> oList = query.getResultList();
		List<InstitutionCashAccount> institutionCashAccounts = new ArrayList<InstitutionCashAccount>();
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
			institutionCashAccount.setIdInstitutionCashAccountPk((Long)objects[0]);
			institutionCashAccount.setTotalAmount((BigDecimal)objects[1]);
			institutionCashAccount.setAvailableAmount((BigDecimal)objects[2]);
			institutionCashAccount.setCurrency((Integer)objects[3]);
			
			ModalityGroup modalityGroup = new ModalityGroup();
			modalityGroup.setIdModalityGroupPk((Long)objects[4]);
			modalityGroup.setGroupName((String)objects[5]);
			
			NegotiationMechanism negotiationMechanism = new NegotiationMechanism();
			negotiationMechanism.setIdNegotiationMechanismPk((Long)objects[4]);
			negotiationMechanism.setDescription((String)objects[7]);
			
			institutionCashAccount.setIndRelatedBcrd((Integer)objects[8]);
			institutionCashAccount.setIndSettlementProcess((Integer)objects[9]);
			institutionCashAccount.setModalityGroup(modalityGroup);
			institutionCashAccount.setNegotiationMechanism(negotiationMechanism);
			institutionCashAccounts.add(institutionCashAccount);
		}
		return institutionCashAccounts;
		
	}
	

	/**
	 * Search bank from bank.
	 *
	 * @param idBank the id bank
	 * @param currency the currency
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String searchBankFromBank(Long idBank, Integer currency) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("   SELECT");		
		sbQuery.append("   		  IBA.accountNumber");
		sbQuery.append("     FROM InstitutionBankAccount IBA");
		sbQuery.append("    WHERE IBA.providerBank.idBankPk =:idBank");
		sbQuery.append("      AND IBA.currency =:currency");
		sbQuery.append("      AND IBA.institutionCashAccount is null");
		Query query = em.createQuery(sbQuery.toString());	
		query.setParameter("idBank",idBank);		
		query.setParameter("currency",currency);		
		try{
			return (String) query.getSingleResult();
		}catch(Exception exception){
			return null;			
		}
	}
	
	/**
	 * Search institution cash account no modalities.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> searchInstitutionCashAccountNoModalities(InstitutionCashAccountTO institutionCashAccountTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("  SELECT ");
		sbQuery.append("         ICA.idInstitutionCashAccountPk,");//0
		sbQuery.append("         ICA.totalAmount,");	//1
		sbQuery.append("         ICA.availableAmount,");	//2
		sbQuery.append("         ICA.currency,");		//3 
		sbQuery.append("         ICA.indRelatedBcrd");//4
		sbQuery.append("    FROM InstitutionCashAccount ICA");
		sbQuery.append("   WHERE 1=1");
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdParticipantFk())) {
			sbQuery.append("     AND ICA.participant.idParticipantPk =:idParticipantFk");
			mapParam.put("idParticipantFk",institutionCashAccountTO.getIdParticipantFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getCurrency())) {
			sbQuery.append("     AND ICA.currency =:currency");
			mapParam.put("currency",institutionCashAccountTO.getCurrency());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdModalityGroupFk())) {
			sbQuery.append("     AND ICA.modalityGroup.idModalityGroupPk =:idModalityGroupFk");
			mapParam.put("idModalityGroupFk",institutionCashAccountTO.getIdModalityGroupFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdNegotiationMechanismFk())) {
			sbQuery.append("     AND ICA.negotiationMechanism.idNegotiationMechanismPk =:idNegotiationMechanismFk");
			mapParam.put("idNegotiationMechanismFk",institutionCashAccountTO.getIdNegotiationMechanismFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getAccountType())) {
			sbQuery.append("     AND ICA.accountType =:accountType");
			mapParam.put("accountType",institutionCashAccountTO.getAccountType());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getIdIssuerFk())) {
			sbQuery.append("     AND ICA.issuer.idIssuerPk =:idIssuerFk");
			mapParam.put("idIssuerFk",institutionCashAccountTO.getIdIssuerFk());
		}
		if (Validations.validateIsNotNull(institutionCashAccountTO.getSettlementSchema())) {
			sbQuery.append("     AND ICA.settlementSchema =:settlementSchema");
			mapParam.put("settlementSchema",institutionCashAccountTO.getSettlementSchema());
		}
		sbQuery.append(" AND ICA.accountState =:accountState");
		mapParam.put("accountState",CashAccountStateType.ACTIVATE.getCode());
		Query query = em.createQuery(sbQuery.toString());
			
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		List<Object> oList = query.getResultList();
		List<InstitutionCashAccount> institutionCashAccounts = new ArrayList<InstitutionCashAccount>();
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
			institutionCashAccount.setIdInstitutionCashAccountPk((Long)objects[0]);
			institutionCashAccount.setTotalAmount((BigDecimal)objects[1]);
			institutionCashAccount.setAvailableAmount((BigDecimal)objects[2]);
			institutionCashAccount.setCurrency((Integer)objects[3]);
			
			institutionCashAccount.setIndRelatedBcrd((Integer)objects[4]);
			institutionCashAccounts.add(institutionCashAccount);
		}
		return institutionCashAccounts;
		
	}
	
	/**
	 * Search bank description.
	 *
	 * @param idInstitutionCashAccount the id institution cash account
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccountTO searchBankDescription(Long idInstitutionCashAccount) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT ica FROM InstitutionCashAccount ica ");
		sbQuery.append("	LEFT JOIN FETCH ica.cashAccountDetailResults cad");
		sbQuery.append("	LEFT JOIN FETCH cad.institutionBankAccount iba");
		sbQuery.append("	LEFT JOIN FETCH iba.providerBank pb");
		sbQuery.append("    WHERE ica.idInstitutionCashAccountPk =:idInstitutionCashAccountPk");
		Query query = em.createQuery(sbQuery.toString());	
		query.setParameter("idInstitutionCashAccountPk",idInstitutionCashAccount);
		@SuppressWarnings("unchecked")
		List<InstitutionCashAccount> objectsList = ((List<InstitutionCashAccount>) query.getResultList());
		InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();
		for (int i = 0; i < objectsList.size(); ++i) {			
			InstitutionCashAccount objects =  (InstitutionCashAccount) objectsList.get(i);
			institutionCashAccountTO.setBankDescription(objects.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getProviderBank().getDescription());
			institutionCashAccountTO.setBankAccount(objects.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber());
			break;
		}
		return institutionCashAccountTO;
	}
	
	/**
	 * Find mechanism operation.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation findMechanismOperation(MechanismOperationTO mechanismOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("  SELECT ");
		sbQuery.append("         MO.idMechanismOperationPk,");
		sbQuery.append("         MO.mechanisnModality.negotiationModality.idNegotiationModalityPk,");
		sbQuery.append("         MO.operationNumber,");
		sbQuery.append("         MO.operationDate,");		
		sbQuery.append("         MO.currency,");		
		sbQuery.append("         MO.paymentReference,");
		sbQuery.append("         MO.settlementSchema");
		sbQuery.append("    FROM MechanismOperation MO");
		sbQuery.append("   WHERE MO.buyerParticipant.idParticipantPk =:idParticipantPk");
		if (Validations.validateIsNotNull(mechanismOperationTO.getPaymentReference())) {
			sbQuery.append("     AND MO.paymentReference  =:paymentReference");
			mapParam.put("paymentReference",mechanismOperationTO.getPaymentReference());
		}
		if (Validations.validateIsNotNull(mechanismOperationTO.getIdNegotiationModalityPk())) {
			sbQuery.append("     AND MO.mechanisnModality.negotiationModality.idNegotiationModalityPk  =:idNegotiationModalityPk");
			mapParam.put("idNegotiationModalityPk",mechanismOperationTO.getIdNegotiationModalityPk());
		}
		if (Validations.validateIsNotNull(mechanismOperationTO.getOperationDate())) {
			sbQuery.append("     AND trunc(MO.operationDate) =trunc(:operationDate)");
			mapParam.put("operationDate",mechanismOperationTO.getOperationDate());
		}
		if (Validations.validateIsNotNull(mechanismOperationTO.getOperationNumber())) {
			sbQuery.append("     AND MO.operationNumber =:operationNumber");
			mapParam.put("operationNumber",mechanismOperationTO.getOperationNumber());
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPk", mechanismOperationTO.getBuyerParticipant());				
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		List<Object> oList = query.getResultList();
		MechanismOperation mechanismOperation = new MechanismOperation();
		
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			mechanismOperation.setIdMechanismOperationPk((Long)objects[0]);
			MechanismModality mechanismModality = new MechanismModality();
			NegotiationModality negotiationModality = new NegotiationModality();
			negotiationModality.setIdNegotiationModalityPk((Long)objects[1]);
			
			mechanismModality.setNegotiationModality(negotiationModality);
			mechanismOperation.setMechanisnModality(mechanismModality);
			mechanismOperation.setOperationNumber((Long)objects[2]);
			mechanismOperation.setOperationDate((Date)objects[3]);
			mechanismOperation.setCurrency((Integer)objects[4]);
			mechanismOperation.setPaymentReference((String)objects[5]);
			mechanismOperation.setSettlementSchema((Integer)objects[6]);
		}
		return mechanismOperation;
	}
	
	/**
	 * Search lst negotiation modalities.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> searchLstNegotiationModalities() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("  SELECT ");
		sbQuery.append("         NM.idNegotiationModalityPk,");
		sbQuery.append("         NM.modalityName,");
		sbQuery.append("         NM.indTermSettlement");
		sbQuery.append("    FROM NegotiationModality NM");
		Query query = em.createQuery(sbQuery.toString());
		
		List<Object> oList = query.getResultList();
		List<NegotiationModality> negotiationModalities = new ArrayList<NegotiationModality>();
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			NegotiationModality negotiationModality = new NegotiationModality();
			negotiationModality.setIdNegotiationModalityPk((Long)objects[0]);
			negotiationModality.setModalityName((String)objects[1]);
			negotiationModality.setIndTermSettlement((Integer)objects[2]);
			
			negotiationModalities.add(negotiationModality);
		}
		return negotiationModalities;
	}
	
	/**
	 * Search participant mechanisms.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> searchParticipantMechanisms(Long idParticipantPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("  SELECT ");
		sbQuery.append("DISTINCT PM.mechanismModality.negotiationMechanism.idNegotiationMechanismPk,");
		sbQuery.append("         NM.description");
		sbQuery.append("    FROM ParticipantMechanism PM, NegotiationMechanism NM");
		sbQuery.append("   WHERE NM.idNegotiationMechanismPk  = PM.mechanismModality.negotiationMechanism.idNegotiationMechanismPk ");
		sbQuery.append("     AND PM.participant.idParticipantPk =:idParticipantPk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPk", idParticipantPk);
		
		List<Object> oList = query.getResultList();
		List<NegotiationMechanism> negotiationMechanisms = new ArrayList<NegotiationMechanism>();
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			NegotiationMechanism negotiationMechanism = new NegotiationMechanism();
			negotiationMechanism.setIdNegotiationMechanismPk((Long)objects[0]);
			negotiationMechanism.setDescription((String)objects[1]);
						
			negotiationMechanisms.add(negotiationMechanism);
		}
		return negotiationMechanisms;
	}
	
	/**
	 * Search settlement schema.
	 *
	 * @param idModalityGroupPk the id modality group pk
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer searchSettlementSchema(Long idModalityGroupPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("  SELECT ");
		sbQuery.append("		 MG.settlementSchema");		
		sbQuery.append("    FROM ModalityGroup MG");
		sbQuery.append("   WHERE MG.idModalityGroupPk =:idModalityGroupPk ");		
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idModalityGroupPk", idModalityGroupPk);		
		Integer settlementSchema = (Integer) query.getSingleResult();
		return settlementSchema;
	}
	
	/**
	 * Search modality group.
	 *
	 * @param negotiationMechanismId the negotiation mechanism id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ModalityGroup> searchModalityGroup(Long negotiationMechanismId) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("  SELECT ");
		sbQuery.append("DISTINCT MGD.modalityGroup.idModalityGroupPk,");
		sbQuery.append("         MGD.modalityGroup.groupName");
		sbQuery.append("    FROM ModalityGroupDetail MGD");
		sbQuery.append("   WHERE MGD.negotiationMechanism.idNegotiationMechanismPk=:negotiationMechanismId ");
		sbQuery.append("   and MGD.modalityGroup.idModalityGroupPk !=:idModalityGroupPk ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("negotiationMechanismId", negotiationMechanismId);
		query.setParameter("idModalityGroupPk", GeneralConstants.MODALITY_GROUP_FOP);
		List<Object> oList = query.getResultList();
		List<ModalityGroup> modalityGroups = new ArrayList<ModalityGroup>();
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			ModalityGroup modalityGroup = new ModalityGroup();
			modalityGroup.setIdModalityGroupPk((Long)objects[0]);
			modalityGroup.setGroupName((String)objects[1]);
						
			modalityGroups.add(modalityGroup);
		}
		return modalityGroups;
	}
	/**
	 * Find by query.
	 *
	 * 
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> findAllParticipants() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from Participant p ");
		sbQuery.append(" WHERE p.state =:state");
		sbQuery.append(" and p.indDepositary =:indDepositary");
		sbQuery.append(" order by p.description");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("indDepositary", BooleanType.YES.getCode());
		List<Participant> oList = (ArrayList<Participant> )query.getResultList();
		return oList;
	}
	
	/**
	 * Find banks.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Bank> findBanks() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT ");
		sbQuery.append("		 B.idBankPk,");
		sbQuery.append("		 B.description,");
		sbQuery.append("		 B.bicCode");
		sbQuery.append("    FROM Bank B");
		
		Query query = em.createQuery(sbQuery.toString());
		
		List<Object> oList = query.getResultList();
		List<Bank> bankList = new ArrayList<Bank>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating Participant
			Bank bank = new Bank();
			bank.setDescription((String) objects[1]);
			bank.setIdBankPk((Long) objects[0]);
			bank.setBicCode((String)objects[2]);
			bankList.add(bank);
		}
		return bankList;
	}
	
	/**
	 * Find a issuers.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> findAIssuers() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT ");
		sbQuery.append("		 I.idIssuerPk,");
		sbQuery.append("		 I.businessName");
		sbQuery.append("    FROM Issuer I");
		sbQuery.append("   WHERE I.stateIssuer =:stateIssuer order by I.businessName");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("stateIssuer", IssuerStateType.REGISTERED.getCode());
		List<Object> oList = query.getResultList();
		List<Issuer> issuerList = new ArrayList<Issuer>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating Participant
			Issuer issuer = new Issuer();
			issuer.setBusinessName((String) objects[1]);
			issuer.setIdIssuerPk((String) objects[0]);
			
			issuerList.add(issuer);
		}
		return issuerList;
	}
	
	/**
	 * Find international depositories.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public  List<InternationalDepository> findInternationalDepositories() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT ");
		sbQuery.append("		 ID.idInternationalDepositoryPk,");
		sbQuery.append("		 ID.description");
		sbQuery.append("    FROM InternationalDepository ID");
		sbQuery.append("   WHERE ID.stateIntDepositary =:state ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", InternationalDepositoryStateType.ACTIVE.getCode());
		List<Object> oList = query.getResultList();
		List<InternationalDepository> internationalDepositories = new ArrayList<InternationalDepository>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating Participant
			InternationalDepository internationalDepository = new InternationalDepository();
			internationalDepository.setDescription((String) objects[1]);
			internationalDepository.setIdInternationalDepositoryPk((Long) objects[0]);
			
			internationalDepositories.add(internationalDepository);
		}
		return internationalDepositories;		
	}
	
	/**
	 * Search international operations.
	 *
	 * @param internationalOperationTO the international operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InternationalOperation>searchInternationalOperations(InternationalOperationTO internationalOperationTO)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("  SELECT ");
		sbQuery.append("         IO.idInternationalOperationPk,");//0
		sbQuery.append("         IO.currency,");//1
		sbQuery.append("         IO.cashAmount,");//2
		sbQuery.append("         IO.securities.idSecurityCodePk,");//3		
		sbQuery.append("         IO.localParticipant.idParticipantPk,");//4		
		sbQuery.append("         IO.localParticipant.description,");		//5
		sbQuery.append("         IO.registerDate,");		//6
		sbQuery.append("         IO.settlementDate,");//7
		sbQuery.append("         IO.internationalParticipant.idInterParticipantPk,");//8
		sbQuery.append("         IO.internationalParticipant.description,");		//9
		sbQuery.append("         IO.operationNumber");//10
		sbQuery.append("    FROM InternationalOperation IO");
		sbQuery.append("   WHERE 1=1");
		//VALIDATING IF INTERNATIONAL OPERATIONS ID IS NOT NULL
		if (Validations.validateIsNotNull(internationalOperationTO.getIdInternationalOperationPk())) {
			sbQuery.append("     AND IO.idInternationalOperationPk =:idInternationalOperationPk");
			mapParam.put("idInternationalOperationPk",internationalOperationTO.getIdInternationalOperationPk());
		}
		//VALIDATING IF CURRENCY IS NOT NULL
		if (Validations.validateIsNotNull(internationalOperationTO.getCurrency())) {
			sbQuery.append("     AND IO.currency =:currency");
			mapParam.put("currency",internationalOperationTO.getCurrency());
		}
		//VALIDATING IF TRANSFER TYPE IS NOT NULL
		if (Validations.validateIsNotNull(internationalOperationTO.getTransferType())) {
			sbQuery.append("     AND IO.transferType =:transferType");
			mapParam.put("transferType",internationalOperationTO.getTransferType());
		}
		//VALIDATING IF LOCAL PARTICIPANT IS NOT NULL
		if (Validations.validateIsNotNull(internationalOperationTO.getLocalParticipant())) {
			sbQuery.append("     AND IO.localParticipant.idParticipantPk =:localParticipant");
			mapParam.put("localParticipant",internationalOperationTO.getLocalParticipant());
		}
		//VALIDATING IF INTERNATIONAL PARTICIPANT IS NOT NULL
		if (Validations.validateIsNotNull(internationalOperationTO.getInternationalParticipant())) {
			sbQuery.append("     AND IO.internationalParticipant.idParticipantPk =:internationalParticipant");
			mapParam.put("internationalParticipant",internationalOperationTO.getInternationalParticipant());
		}
		//VALIDATING IF SETTLEMENT DEPOSITORY IS NOT NULL kunai
		if (Validations.validateIsNotNull(internationalOperationTO.getSettlementDepository())) {
			sbQuery.append("     AND IO.settlementDepository.idInternationalDepositoryPk =:settlementDepository");
			mapParam.put("settlementDepository",internationalOperationTO.getSettlementDepository());
		}
		//VALIDATING IF TRADE DEPOSIROTY IS NOT NULL
		if (Validations.validateIsNotNull(internationalOperationTO.getTradeDepository())) {
			sbQuery.append("     AND IO.tradeDepository.idInternationalDepositoryPk =:tradeDepository");
			mapParam.put("tradeDepository",internationalOperationTO.getTradeDepository());
		}
		//VALIDATING IF INITIAL AND END DATE ARE NOT NULLS
		if (Validations.validateIsNotNull(internationalOperationTO.getInitialDate()) && Validations.validateIsNotNull(internationalOperationTO.getEndDate())) {
			sbQuery.append("     AND IO.registerDate >=:initialDate");
			sbQuery.append("     AND IO.registerDate <=:endDate");
			mapParam.put("initialDate",internationalOperationTO.getInitialDate());
			mapParam.put("endDate",internationalOperationTO.getEndDate());
		}
		//VALIDATING IF INITIAL SETTLEMENT DATE AND END SETTLEMENT DATE ARE NOT NULLS
		if (Validations.validateIsNotNull(internationalOperationTO.getInitialSettlementDate()) && Validations.validateIsNotNull(internationalOperationTO.getEndSettlementDate())) {
			sbQuery.append("     AND IO.settlementDate >=:initialSettlementDate");
			sbQuery.append("     AND IO.settlementDate <=:endSettlementDate");
			mapParam.put("initialSettlementDate",internationalOperationTO.getInitialSettlementDate());
			mapParam.put("endSettlementDate",internationalOperationTO.getEndSettlementDate());
		}
		//CREATING QUERY
		Query query = em.createQuery(sbQuery.toString());					
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		List<Object> oList = query.getResultList();
		List<InternationalOperation> internationalOperations= new ArrayList<InternationalOperation>();
		//BUILDING DATA
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//CREATING INTERNATIONAL OPERATION
			InternationalOperation internationalOperation = new InternationalOperation();
			//SETTING INTERNATIONAL OPERATION ID
			internationalOperation.setIdInternationalOperationPk((Long)objects[0]);
			//SETTING CURRENCY
			internationalOperation.setCurrency((Integer)objects[1]);
			//SETTING CASH AMOUNT
			internationalOperation.setCashAmount((BigDecimal)objects[2]);

			//CREATING SECURITY
			Security security = new Security();
			//SETTING ISIN CODE PK
			security.setIdSecurityCodePk((String)objects[3]);
			
			internationalOperation.setSecurities(security);
			
			Participant localParticipant = new Participant();
			localParticipant.setIdParticipantPk((Long)objects[4]);
			localParticipant.setDescription((String)objects[5]);

			InternationalParticipant internationalParticipant = new InternationalParticipant();
			internationalParticipant.setIdInterParticipantPk((Long)objects[8]);
			internationalParticipant.setDescription((String)objects[9]);
			
			internationalOperation.setLocalParticipant(localParticipant);
			internationalOperation.setInternationalParticipant(internationalParticipant);
			
			internationalOperation.setRegisterDate((Date)objects[6]);
			internationalOperation.setSettlementDate((Date)objects[7]);
			internationalOperation.setOperationNumber((Long)objects[10]);
			
			internationalOperations.add(internationalOperation);
		}
		return internationalOperations;
	}
	
	/**
	 * Funds operation detail.
	 *
	 * @param fundsOperationId the funds operation id
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	public FundsOperation fundsOperationDetail(Long fundsOperationId) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("     SELECT " );	
		sbQuery.append("         	FO.idFundsOperationPk," );//0
		sbQuery.append("         	FO.fundsOperationType," );//1
		sbQuery.append("         	FO.registryDate," );	   //2
		sbQuery.append("         	FO.institutionCashAccount.idInstitutionCashAccountPk," );//3
		sbQuery.append("         	FO.operationAmount," );//4
		sbQuery.append("         	FO.fundsOperationGroup," );//5
		sbQuery.append("         	FO.currency," );//6
		sbQuery.append("         	bank.idBankPk," );//7
		sbQuery.append("         	FO.issuer.idIssuerPk," );//8
		sbQuery.append("         	MO.operationDate," );//9
		sbQuery.append("         	MO.paymentReference," );//10
		sbQuery.append("         	MO.idMechanismOperationPk," );//11
		sbQuery.append("         	MN.idNegotiationModalityPk," );//12
		sbQuery.append("         	NMech.idNegotiationMechanismPk," );//13
		sbQuery.append("         	FO.participant.idParticipantPk," );//14
		sbQuery.append("         	MO.settlementType," );//15
		sbQuery.append("         	MO.settlementSchema," );//16
		sbQuery.append("         	MO.operationNumber," );//17
		sbQuery.append("         	MO.currency," );//18
		sbQuery.append("         	MG.idModalityGroupPk," );//19
		sbQuery.append("         	FO.institutionCashAccount.settlementSchema," );//20
		sbQuery.append("         	NM2.idNegotiationMechanismPk," );//21
		sbQuery.append("         	FO.institutionCashAccount.totalAmount," );//22
		sbQuery.append("         	FO.institutionCashAccount.availableAmount," );//23
		sbQuery.append("         	NM2.description," );//24
		sbQuery.append("         	MG.groupName," );//25
		sbQuery.append("         	bank.bicCode," );//26
		sbQuery.append("         	FO.bankAccountNumber," );//27		
		sbQuery.append("         	FO.institutionCashAccount.currency," );//28
		sbQuery.append("            FO.fundsOperation.idFundsOperationPk,");//29
		sbQuery.append("            0,");//30
		sbQuery.append("            0,");//31 
		sbQuery.append("            FO.operationPart,");//32 
		sbQuery.append("            bank.mnemonic,");//33 
		sbQuery.append("            BPA.idBenefitPaymentPK,");//34 
		sbQuery.append("            BPA.currency,");//35
		sbQuery.append("            FO.indBcrdPayback");//36
		sbQuery.append("            ,FO.indTransferCentralizing");//37
		sbQuery.append("			,FO.bankMovement");//38
		sbQuery.append("      FROM  FundsOperation FO");
		sbQuery.append(" LEFT JOIN  FO.mechanismOperation MO");
		sbQuery.append(" LEFT JOIN  MO.mechanisnModality MM");
		sbQuery.append(" LEFT JOIN  MM.negotiationModality MN");
		sbQuery.append(" LEFT JOIN  MM.negotiationMechanism NMech");
		sbQuery.append(" LEFT JOIN  FO.institutionCashAccount.modalityGroup MG");
		sbQuery.append(" LEFT JOIN  FO.institutionCashAccount.negotiationMechanism NM2");
		sbQuery.append(" LEFT JOIN  FO.bank bank");
		sbQuery.append(" LEFT JOIN  FO.benefitPaymentAllocation BPA");
		sbQuery.append("     WHERE  FO.idFundsOperationPk=:fundsOperationId");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("fundsOperationId", fundsOperationId);
		List<Object> objectList = query.getResultList(); 
		FundsOperation fundsOperation = new FundsOperation();
		
		for(int i=0;i<objectList.size();++i){
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			
			FundsOperation relatedFundsOperation = new FundsOperation();
			relatedFundsOperation.setIdFundsOperationPk((Long)sResults[29]);
			
			fundsOperation.setIdFundsOperationPk((Long)sResults[0]);
			fundsOperation.setFundsOperationType((Long)sResults[1]);
			fundsOperation.setRegistryDate((Date)sResults[2]);
			fundsOperation.setFundsOperation(relatedFundsOperation);			
			
			InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
			institutionCashAccount.setIdInstitutionCashAccountPk((Long)sResults[3]);
			
			ModalityGroup modalityGroup = new ModalityGroup();
			modalityGroup.setIdModalityGroupPk((Long)sResults[19]);
			modalityGroup.setGroupName((String)sResults[25]);
			
			NegotiationMechanism negotiationMechanismInst = new NegotiationMechanism();
			negotiationMechanismInst.setIdNegotiationMechanismPk((Long)sResults[21]);
			negotiationMechanismInst.setDescription((String)sResults[24]);
			
			
			institutionCashAccount.setNegotiationMechanism(negotiationMechanismInst);
			institutionCashAccount.setModalityGroup(modalityGroup);
			institutionCashAccount.setSettlementSchema((Integer)sResults[20]);
			institutionCashAccount.setTotalAmount((BigDecimal)sResults[22]);
			institutionCashAccount.setAvailableAmount((BigDecimal)sResults[23]);			
			institutionCashAccount.setCurrency((Integer)sResults[28]);
			
			fundsOperation.setInstitutionCashAccount(institutionCashAccount);
			fundsOperation.setOperationAmount((BigDecimal)sResults[4]);
			fundsOperation.setFundsOperationGroup((Integer)sResults[5]);
			fundsOperation.setCurrency((Integer)sResults[6]);
			fundsOperation.setBankAccountNumber((String)sResults[27]);
			fundsOperation.setOperationPart((Integer)sResults[32]);
			
			Bank bank = new Bank();
			bank.setIdBankPk((Long)sResults[7]);
			bank.setBicCode((String)sResults[26]);
			bank.setMnemonic((String)sResults[33]);
			fundsOperation.setBank(bank);
			
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk((String)sResults[8]);
			
			fundsOperation.setIssuer(issuer);
			
			MechanismOperation mechanismOperation = new MechanismOperation();
			mechanismOperation.setOperationDate((Date)sResults[9]);
			mechanismOperation.setPaymentReference((String)sResults[10]);
			mechanismOperation.setIdMechanismOperationPk((Long)sResults[11]);
			
			MechanismModality mechanisnModality = new MechanismModality();
			
			NegotiationModality negotiationModality = new NegotiationModality();
			negotiationModality.setIdNegotiationModalityPk((Long)sResults[12]);
			
			NegotiationMechanism negotiationMechanism = new NegotiationMechanism();
			negotiationMechanism.setIdNegotiationMechanismPk((Long)sResults[13]);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk((Long)sResults[14]);
			
			mechanisnModality.setNegotiationMechanism(negotiationMechanism);
			mechanisnModality.setNegotiationModality(negotiationModality);
			mechanismOperation.setMechanisnModality(mechanisnModality);
			mechanismOperation.setSettlementType((Integer)sResults[15]);
			mechanismOperation.setSettlementSchema((Integer)sResults[16]);
			mechanismOperation.setOperationNumber((Long)sResults[17]);
			mechanismOperation.setCurrency((Integer)sResults[18]);
			
			BenefitPaymentAllocation benefitPaymentAllocation = new BenefitPaymentAllocation();
			benefitPaymentAllocation.setIdBenefitPaymentPK((Long)sResults[34]);
			benefitPaymentAllocation.setCurrency((Integer)sResults[35]);
			fundsOperation.setIndBcrdPayback((Integer)sResults[36]);
			fundsOperation.setBlTransferCentralizing((Integer)sResults[37]==BooleanType.YES.getCode()?true:false);
			//fundsOperation.setHolder(holder);
			fundsOperation.setParticipant(participant);
			fundsOperation.setMechanismOperation(mechanismOperation);
			fundsOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
			fundsOperation.setBankMovement((String)sResults[38]);
		}
		return fundsOperation;
	}
	
	/**
	 * Search retirement fund operations.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperation>searchRetirementFundOperations(FundsOperation filter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("   SELECT " );	
		sbQuery.append("          FO.idFundsOperationPk," );
		sbQuery.append("          FO.fundsOperationType," );
		sbQuery.append("          FO.registryDate," );		
		sbQuery.append("          FO.institutionCashAccount.idInstitutionCashAccountPk," );
		sbQuery.append("          FO.operationAmount," );
		sbQuery.append("          FO.fundsOperationGroup," );
		sbQuery.append("          FO.issuer.idIssuerPk," );
		sbQuery.append("          FO.participant.idParticipantPk," );
		sbQuery.append("          MO.idMechanismOperationPk,");
		sbQuery.append("          FO.operationPart,");
		sbQuery.append("          FO.indAutomatic,");
		sbQuery.append("          part.mnemonic,	");
		sbQuery.append("          (select pt1.text1 from ParameterTable pt1 where pt1.parameterTablePk = FO.currency),");
		sbQuery.append("          (select pt2.parameterName from ParameterTable pt2 where pt2.parameterTablePk = FO.operationState),");
		sbQuery.append("          FO.operationState");
		sbQuery.append("          ,FO.bankMovement"); //15
		sbQuery.append("     FROM FundsOperation FO ");
		sbQuery.append("LEFT JOIN FO.mechanismOperation MO	");
		sbQuery.append("LEFT JOIN FO.participant part		");
		sbQuery.append("    WHERE FO.indCentralized=0");
		
		if(Validations.validateIsNotNull(filter.getMechanismOperation()) 
		&& Validations.validateIsNotNull(filter.getMechanismOperation().getIdMechanismOperationPk())){
			sbQuery.append(" AND MO.idMechanismOperationPk =:idMechanismOperationPk");
			mapParam.put("idMechanismOperationPk", filter.getMechanismOperation().getIdMechanismOperationPk());	
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdFundsOperationPk())){
			sbQuery.append(" AND FO.idFundsOperationPk =:idFundsOperationPk");
			mapParam.put("idFundsOperationPk", filter.getIdFundsOperationPk());	
		}
		if(Validations.validateIsNotNull(filter.getFundsOperationType())){
			sbQuery.append(" AND FO.fundsOperationType =:fundsOperationType");
			mapParam.put("fundsOperationType", filter.getFundsOperationType());	
		}
		if(Validations.validateIsNotNull(filter.getFundsOperationGroup())){
			sbQuery.append(" AND FO.fundsOperationGroup =:fundsOperationGroup");
			mapParam.put("fundsOperationGroup", filter.getFundsOperationGroup());	
		}		
		if(Validations.validateIsNotNull(filter.getFundsOperationClass())){
			sbQuery.append(" AND FO.fundsOperationClass =:fundsOperationClass");
			mapParam.put("fundsOperationClass", filter.getFundsOperationClass());	
		}
		if(Validations.validateIsNotNull(filter.getOperationState())){
			sbQuery.append(" AND FO.operationState =:operationState");
			mapParam.put("operationState", filter.getOperationState());	
		}
		if(Validations.validateIsNotNull(filter.getOperationPart())){
			sbQuery.append(" AND FO.operationPart =:operationPart");
			mapParam.put("operationPart", filter.getOperationPart());	
		}
		if(Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getEndDate())){
			sbQuery.append(" AND FO.registryDate >=:registerDatePrm ");
			sbQuery.append(" AND FO.registryDate <=:endDatePrm");
			mapParam.put("registerDatePrm", filter.getInitialDate());	
			mapParam.put("endDatePrm", CommonsUtilities.putTimeToDateToEndOfDay(filter.getEndDate()));
		}
		
		Query query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        List<Object> objectList = query.getResultList(); 
		List<FundsOperation> fundsOperations = new ArrayList<FundsOperation>();
		
		for(int i=0;i<objectList.size();++i){
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			FundsOperation fundsOperation = new FundsOperation();
			fundsOperation.setIdFundsOperationPk((Long)sResults[0]);
			fundsOperation.setFundsOperationType((Long)sResults[1]);
			fundsOperation.setRegistryDate((Date)sResults[2]);
			
			InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
			institutionCashAccount.setIdInstitutionCashAccountPk((Long)sResults[3]);
			
			fundsOperation.setInstitutionCashAccount(institutionCashAccount);
			fundsOperation.setOperationAmount((BigDecimal)sResults[4]);
			fundsOperation.setFundsOperationGroup((Integer)sResults[5]);
			
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk((String)sResults[6]);
			
			Participant participant = null;
			if((Long)sResults[7]!=null){
				participant = new Participant();
				participant.setIdParticipantPk((Long)sResults[7]);
				participant.setMnemonic(sResults[11].toString());
			}
			
			
			MechanismOperation mechanismOperation = new MechanismOperation();
			mechanismOperation.setIdMechanismOperationPk((Long)sResults[8]);
			
			fundsOperation.setOperationPart((Integer)sResults[9]);
			fundsOperation.setIndAutomatic((Integer)sResults[10]);
			fundsOperation.setCurrencyDescription(sResults[12].toString());
			fundsOperation.setOperationStateDescription(sResults[13].toString());
			fundsOperation.setIssuer(issuer);
			fundsOperation.setParticipant(participant);
			fundsOperation.setOperationState((Integer)sResults[14]);
			fundsOperation.setBankMovement((String)sResults[15]);
			fundsOperations.add(fundsOperation);
		}
		return fundsOperations;
	}
	
	
	/**
	 * Search funds operation related.
	 *
	 * @param idRelated the id related
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	public FundsOperation searchFundsOperationRelated(Long idRelated) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         FO.idFundsOperationPk," );
		sbQuery.append("         FO.fundsOperationType," );
		sbQuery.append("         FO.registryDate," );		
		sbQuery.append("         FO.institutionCashAccount.idInstitutionCashAccountPk," );
		sbQuery.append("         FO.operationAmount," );
		sbQuery.append("         FO.fundsOperationGroup," );
		sbQuery.append("         FO.issuer.idIssuerPk," );
		sbQuery.append("         FO.participant.idParticipantPk" );
		sbQuery.append("   FROM  FundsOperation FO");
		sbQuery.append("  WHERE  FO.indCentralized=0");
		
		if(Validations.validateIsNotNull(idRelated)){
			sbQuery.append(" AND FO.fundsOperation.idFundsOperationPk =:idFundsOperationPk");
			mapParam.put("idFundsOperationPk", idRelated);	
		}		
		
		Query query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        List<Object> objectList = query.getResultList(); 
        FundsOperation fundsOperation = new FundsOperation();
		
		for(int i=0;i<objectList.size();++i){
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			
			fundsOperation.setIdFundsOperationPk((Long)sResults[0]);
			fundsOperation.setFundsOperationType((Long)sResults[1]);
			fundsOperation.setRegistryDate((Date)sResults[2]);
			
			InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
			institutionCashAccount.setIdInstitutionCashAccountPk((Long)sResults[3]);
			
			fundsOperation.setInstitutionCashAccount(institutionCashAccount);
			fundsOperation.setOperationAmount((BigDecimal)sResults[4]);
			fundsOperation.setFundsOperationGroup((Integer)sResults[5]);
			
			Issuer issuer = new Issuer();			
			issuer.setIdIssuerPk((String)sResults[6]);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk((Long)sResults[7]);
			if(Validations.validateIsNotNull((String)sResults[6])){
				fundsOperation.setIssuer(issuer);
			}
			fundsOperation.setParticipant(participant);			
			break;
		}
		return fundsOperation;
	}
	
	/**
	 * Search holder funds operation.
	 *
	 * @param holderFundsOperationTO the holder funds operation to
	 * @return the holder funds operation
	 * @throws ServiceException the service exception
	 */
	public HolderFundsOperation searchHolderFundsOperation(HolderFundsOperationTO holderFundsOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		HolderFundsOperation holderFundsOperation = null;
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         HFO" );
		sbQuery.append("   FROM  HolderFundsOperation HFO");
		sbQuery.append("  WHERE  1=1");
		
		if(Validations.validateIsNotNull(holderFundsOperationTO.getIdHolderFundsOperationPk())){
			sbQuery.append(" AND HFO.fundsOperation.idFundsOperationPk =:idFundsOperationPk");
			mapParam.put("idFundsOperationPk", holderFundsOperationTO.getIdHolderFundsOperationPk());
		}
		if(Validations.validateIsNotNull(holderFundsOperationTO.getCurrency())){
			sbQuery.append(" AND HFO.currency =:currency");
			mapParam.put("currency", holderFundsOperationTO.getCurrency());
		}
		if(Validations.validateIsNotNull(holderFundsOperationTO.getHolder())){
			sbQuery.append(" AND HFO.holder.idHolderPk =:idHolderPk");
			mapParam.put("idHolderPk", holderFundsOperationTO.getHolder());
		}
		if(Validations.validateIsNotNull(holderFundsOperationTO.getHolderOperationState())){
			sbQuery.append(" AND HFO.holderOperationState =:holderOperationState");
			mapParam.put("holderOperationState", holderFundsOperationTO.getHolderOperationState());
		}
		
		TypedQuery<HolderFundsOperation> typedQuery =  em.createQuery(sbQuery.toString(),HolderFundsOperation.class);
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		 while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	typedQuery.setParameter(entry.getKey(), entry.getValue());
        }
		 try{
			 	holderFundsOperation = typedQuery.getSingleResult();
			}catch(NoResultException nex) {
				log.info("Account not found");
				holderFundsOperation = null;
			}
		return holderFundsOperation;
	}
	
	/**
	 * Search holder account bank lst.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBank> searchHolderAccountBankLst(Long idHolderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();				
		sbQuery.append("  SELECT " );	
		sbQuery.append("         HAB" );
		sbQuery.append("   FROM  HolderAccountBank HAB");
		sbQuery.append("  WHERE  HAB.holder.idHolderPk =:idHolderPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPk", idHolderPk);
		return (List<HolderAccountBank>) query.getResultList();
	}
	
	/**
	 * Search holder funds operations lst.
	 *
	 * @param holderFundsOperationTO the holder funds operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderFundsOperation> searchHolderFundsOperationsLst(HolderFundsOperationTO holderFundsOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         HFO" );
		sbQuery.append("   FROM  HolderFundsOperation HFO");
		sbQuery.append(" LEFT JOIN HFO.holder H");
		sbQuery.append(" LEFT JOIN HFO.holderAccountBank HAB");
		sbQuery.append("  WHERE  1=1");
		
		if(Validations.validateIsNotNull(holderFundsOperationTO.getIdHolderFundsOperationPk())){
			sbQuery.append(" AND HFO.fundsOperation.idFundsOperationPk =:idFundsOperationPk");
			mapParam.put("idFundsOperationPk", holderFundsOperationTO.getIdHolderFundsOperationPk());
		}
		if(Validations.validateIsNotNull(holderFundsOperationTO.getCurrency())){
			sbQuery.append(" AND HFO.currency =:currency");
			mapParam.put("currency", holderFundsOperationTO.getCurrency());
		}
		if(Validations.validateIsNotNull(holderFundsOperationTO.getHolder())){
			sbQuery.append(" AND HFO.holder.idHolderPk =:idHolderPk");
			mapParam.put("idHolderPk", holderFundsOperationTO.getHolder());
		}
		if(Validations.validateIsNotNull(holderFundsOperationTO.getHolderOperationState())){
			sbQuery.append(" AND HFO.holderOperationState =:holderOperationState");
			mapParam.put("holderOperationState", holderFundsOperationTO.getHolderOperationState());
		}
		
		Query query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		 while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		
		return (List<HolderFundsOperation>) query.getResultList();
	}
	
	/**
	 * Gets the issuer by benefit payment.
	 *
	 * @param idBenefitPaymentAllocationPk the id benefit payment allocation pk
	 * @return the issuer by benefit payment
	 * @throws ServiceException the service exception
	 */
	public String getIssuerByBenefitPayment(Long idBenefitPaymentAllocationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         BPA.issuer.idIssuerPk" );		
		sbQuery.append("   FROM  BenefitPaymentAllocation BPA");
		sbQuery.append("  WHERE  BPA.idBenefitPaymentPK=:idBenefitPaymentAllocationPk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idBenefitPaymentAllocationPk", idBenefitPaymentAllocationPk);		
		return (String)query.getSingleResult();
	}
	
	/**
	 * Gets the benefit payment file.
	 *
	 * @param idFundsOperation the id funds operation
	 * @return the benefit payment file
	 * @throws ServiceException the service exception
	 */
	public BenefitPaymentFile getBenefitPaymentFile(Long idFundsOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         BPF.fileName," );
		sbQuery.append("         BPF.fileType," );
		sbQuery.append("         BPF.documentFile" );
		sbQuery.append("   FROM  BenefitPaymentFile BPF");
		sbQuery.append("  WHERE  BPF.fundsOperation.idFundsOperationPk=:idFundsOperation");
		
		Query query = em.createQuery(sbQuery.toString());
		BenefitPaymentFile benefitPaymentFile = new BenefitPaymentFile();
		try{	
			query.setParameter("idFundsOperation", idFundsOperation);		
			Object[] sResult =(Object[])query.getSingleResult();
						
			benefitPaymentFile.setFileName((String)sResult[0]);
			benefitPaymentFile.setFileType((Integer)sResult[1]);
			benefitPaymentFile.setDocumentFile((byte[])sResult[2]);
		}catch(Exception e){
			e.printStackTrace();
		}
		return benefitPaymentFile;	
	}

	/**
	 * Gets the swift message.
	 *
	 * @param idFundsOperation the id funds operation
	 * @return the swift message
	 * @throws ServiceException the service exception
	 */
	public SwiftMessage getSwiftMessage(Long idFundsOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         SM" );		
		sbQuery.append("   FROM  SwiftMessage SM");
		sbQuery.append("  WHERE  SM.fundsOperation.idFundsOperationPk=:idFundsOperation");
		
		TypedQuery<SwiftMessage> query = em.createQuery(sbQuery.toString(), SwiftMessage.class);
		query.setParameter("idFundsOperation", idFundsOperation);	
		SwiftMessage swiftMessage = null;
		try{
		  swiftMessage = query.getSingleResult();		
		}catch(NoResultException e){
			log.info(e.getMessage());
		}
		return swiftMessage;
	}
	
	/**
	 * Search negotiation modality.
	 *
	 * @param negotiationModalityPk the negotiation modality pk
	 * @param modalityGroupPk the modality group pk
	 * @return the long
	 */
	public Long searchNegotiationModality(Long negotiationModalityPk, Long modalityGroupPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         MGD.negotiationModality.idNegotiationModalityPk" );		
		sbQuery.append("   FROM  ModalityGroupDetail MGD");
		sbQuery.append("  WHERE  MGD.modalityGroup.idModalityGroupPk =:modalityGroupPk");
		sbQuery.append("    AND MGD.negotiationMechanism.idNegotiationMechanismPk =:negotiationModalityPk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("negotiationModalityPk", negotiationModalityPk);
		query.setParameter("modalityGroupPk", modalityGroupPk);		
		return (Long)query.getSingleResult();
	}
	
	/**
	 * Search modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param modalityGroupPk the modality group pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> searchModality(Long mechanismPk, Long modalityGroupPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         MGD.negotiationModality" );		
		sbQuery.append("   FROM  ModalityGroupDetail MGD");
		sbQuery.append("  WHERE  MGD.modalityGroup.idModalityGroupPk =:modalityGroupPk");
		sbQuery.append("    AND MGD.negotiationMechanism.idNegotiationMechanismPk =:negotiationModalityPk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("negotiationModalityPk", mechanismPk);
		query.setParameter("modalityGroupPk", modalityGroupPk);		
		return (List<NegotiationModality>)query.getResultList();
	}		
	
	
	/**
	 * Update participant position.
	 *
	 * @param idSettlementProcces the id settlement procces
	 * @param idParticipant the id participant
	 * @param indAutomatic the ind automatic
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateParticipantPosition(Long idSettlementProcces, Long idParticipant, Integer indAutomatic, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update ParticipantPosition partpos set  "+				
				" partpos.indAutomaticSent = :parIndAutomaticSent , partpos.indSentFunds = :parIndSentFunds , " +				
				" partpos.lastModifyUser = :parLastModifyUser , partpos.lastModifyDate = :parLastModifyDate , " +
				" partpos.lastModifyIp = :parLastModifyIp , partpos.lastModifyApp = :parLastModifyApp " +
				" where partpos.settlementProcess.idSettlementProcessPk = :parIdSettlementProcess " +
				" and partpos.participant.idParticipantPk = :parIdParticipant " +
				" and partpos.indProcess = :indicatorOne " +
				" and partpos.indSentFunds = :indSentFunds ");
		parameters.put("parIndAutomaticSent", indAutomatic);
		parameters.put("parIndSentFunds", BooleanType.YES.getCode());
		parameters.put("parIdSettlementProcess", idSettlementProcces);	
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("indSentFunds", BooleanType.NO.getCode());
		parameters.put("parIdParticipant", idParticipant);	
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
}
