package com.pradera.funds.fundsoperations.retirement.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.CashAccountMovement;
import com.pradera.model.funds.FundsInternationalOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.MechanismOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RetirementFundsOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RetirementFundsOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The id funds operation pk. */
	private Long idFundsOperationPk;

	/** The funds operation. */
	private FundsOperation fundsOperation;

	/** The bank reference. */
	private String bankReference;

	/** The currency. */
	private Integer currency;

	/** The funds operation class. */
	private Integer fundsOperationClass;

	/** The funds operation group. */
	private Integer fundsOperationGroup;

	/** The funds operation type. */
	private Long fundsOperationType;

	/** The bank. */
	private Long bank;
	
	/** The bank bic code. */
	private String bankBicCode;
		
	/** The bank mnemonic. */
	private String bankMnemonic;
	
	/** The issuer. */
	private String issuer;

	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;

	/** The lst funds international operation. */
	private List<FundsInternationalOperation> lstFundsInternationalOperation;

	/** The participant. */
	private Participant participant;

	/** The institution cash account. */
	private InstitutionCashAccount institutionCashAccount; 

	/** The observation. */
	private Integer observation;

	/** The operation amount. */
	private BigDecimal operationAmount;

	/** The ind automatic. */
	private Integer indAutomatic;

	/** The registry user. */
	private String registryUser;
	
	/** The registry date. */
	private Date registryDate;

	/** The operation state. */
	private Integer operationState;

	/** The ind centralized. */
	private Integer indCentralized;

	/** The operation part. */
	private Integer operationPart = GeneralConstants.ZERO_VALUE_INTEGER;

	

	/** The cash account movements. */
	private List<CashAccountMovement> cashAccountMovements;

	/** The holder. */
	private Long holder;
	
	private Long idBankPk;
	
	private List<Bank> lstBanks;
	
	private String bcbCode,gloss;
	
	private Date operationDate, maxDate, minDate;
	
	private List<String> listGloss;
	
	private List<ParameterTable> lstCurrency;
	
	private Integer currencySelected;
	
	private BigDecimal amountDeposited;
	
	private Long idParticipantPk;
	
	
	
	/** The lst bank accounts to data model. */
	private GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel;
	
	/** The institution bank account session. */
	private InstitutionBankAccount institutionBankAccountSession;

	/**
	 * Gets the id funds operation pk.
	 *
	 * @return the id funds operation pk
	 */
	public Long getIdFundsOperationPk() {
		return idFundsOperationPk;
	}

	/**
	 * Sets the id funds operation pk.
	 *
	 * @param idFundsOperationPk the new id funds operation pk
	 */
	public void setIdFundsOperationPk(Long idFundsOperationPk) {
		this.idFundsOperationPk = idFundsOperationPk;
	}

	/**
	 * Gets the funds operation.
	 *
	 * @return the funds operation
	 */
	public FundsOperation getFundsOperation() {
		return fundsOperation;
	}

	/**
	 * Sets the funds operation.
	 *
	 * @param fundsOperation the new funds operation
	 */
	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	/**
	 * Gets the bank reference.
	 *
	 * @return the bank reference
	 */
	public String getBankReference() {
		return bankReference;
	}

	/**
	 * Sets the bank reference.
	 *
	 * @param bankReference the new bank reference
	 */
	public void setBankReference(String bankReference) {
		this.bankReference = bankReference;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the funds operation class.
	 *
	 * @return the funds operation class
	 */
	public Integer getFundsOperationClass() {
		return fundsOperationClass;
	}

	/**
	 * Sets the funds operation class.
	 *
	 * @param fundsOperationClass the new funds operation class
	 */
	public void setFundsOperationClass(Integer fundsOperationClass) {
		this.fundsOperationClass = fundsOperationClass;
	}

	/**
	 * Gets the funds operation group.
	 *
	 * @return the funds operation group
	 */
	public Integer getFundsOperationGroup() {
		return fundsOperationGroup;
	}

	/**
	 * Sets the funds operation group.
	 *
	 * @param fundsOperationGroup the new funds operation group
	 */
	public void setFundsOperationGroup(Integer fundsOperationGroup) {
		this.fundsOperationGroup = fundsOperationGroup;
	}

	/**
	 * Gets the funds operation type.
	 *
	 * @return the funds operation type
	 */
	public Long getFundsOperationType() {
		return fundsOperationType;
	}

	/**
	 * Sets the funds operation type.
	 *
	 * @param fundsOperationType the new funds operation type
	 */
	public void setFundsOperationType(Long fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}

	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public Long getBank() {
		return bank;
	}

	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(Long bank) {
		this.bank = bank;
	}

	/**
	 * Gets the bank bic code.
	 *
	 * @return the bank bic code
	 */
	public String getBankBicCode() {
		return bankBicCode;
	}

	/**
	 * Sets the bank bic code.
	 *
	 * @param bankBicCode the new bank bic code
	 */
	public void setBankBicCode(String bankBicCode) {
		this.bankBicCode = bankBicCode;
	}

	/**
	 * Gets the bank mnemonic.
	 *
	 * @return the bank mnemonic
	 */
	public String getBankMnemonic() {
		return bankMnemonic;
	}

	/**
	 * Sets the bank mnemonic.
	 *
	 * @param bankMnemonic the new bank mnemonic
	 */
	public void setBankMnemonic(String bankMnemonic) {
		this.bankMnemonic = bankMnemonic;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	/**
	 * Gets the lst funds international operation.
	 *
	 * @return the lst funds international operation
	 */
	public List<FundsInternationalOperation> getLstFundsInternationalOperation() {
		return lstFundsInternationalOperation;
	}

	/**
	 * Sets the lst funds international operation.
	 *
	 * @param lstFundsInternationalOperation the new lst funds international operation
	 */
	public void setLstFundsInternationalOperation(
			List<FundsInternationalOperation> lstFundsInternationalOperation) {
		this.lstFundsInternationalOperation = lstFundsInternationalOperation;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the institution cash account.
	 *
	 * @return the institution cash account
	 */
	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}

	/**
	 * Sets the institution cash account.
	 *
	 * @param institutionCashAccount the new institution cash account
	 */
	public void setInstitutionCashAccount(
			InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}

	/**
	 * Gets the observation.
	 *
	 * @return the observation
	 */
	public Integer getObservation() {
		return observation;
	}

	/**
	 * Sets the observation.
	 *
	 * @param observation the new observation
	 */
	public void setObservation(Integer observation) {
		this.observation = observation;
	}

	/**
	 * Gets the operation amount.
	 *
	 * @return the operation amount
	 */
	public BigDecimal getOperationAmount() {
		return operationAmount;
	}

	/**
	 * Sets the operation amount.
	 *
	 * @param operationAmount the new operation amount
	 */
	public void setOperationAmount(BigDecimal operationAmount) {
		this.operationAmount = operationAmount;
	}

	/**
	 * Gets the ind automatic.
	 *
	 * @return the ind automatic
	 */
	public Integer getIndAutomatic() {
		return indAutomatic;
	}

	/**
	 * Sets the ind automatic.
	 *
	 * @param indAutomatic the new ind automatic
	 */
	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}

	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	/**
	 * Gets the ind centralized.
	 *
	 * @return the ind centralized
	 */
	public Integer getIndCentralized() {
		return indCentralized;
	}

	/**
	 * Sets the ind centralized.
	 *
	 * @param indCentralized the new ind centralized
	 */
	public void setIndCentralized(Integer indCentralized) {
		this.indCentralized = indCentralized;
	}

	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}

	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}


	/**
	 * Gets the cash account movements.
	 *
	 * @return the cash account movements
	 */
	public List<CashAccountMovement> getCashAccountMovements() {
		return cashAccountMovements;
	}

	/**
	 * Sets the cash account movements.
	 *
	 * @param cashAccountMovements the new cash account movements
	 */
	public void setCashAccountMovements(
			List<CashAccountMovement> cashAccountMovements) {
		this.cashAccountMovements = cashAccountMovements;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Long getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Long holder) {
		this.holder = holder;
	}

	/**
	 * Gets the lst bank accounts to data model.
	 *
	 * @return the lst bank accounts to data model
	 */
	public GenericDataModel<InstitutionBankAccount> getLstBankAccountsTODataModel() {
		return lstBankAccountsTODataModel;
	}

	/**
	 * Sets the lst bank accounts to data model.
	 *
	 * @param lstBankAccountsTODataModel the new lst bank accounts to data model
	 */
	public void setLstBankAccountsTODataModel(
			GenericDataModel<InstitutionBankAccount> lstBankAccountsTODataModel) {
		this.lstBankAccountsTODataModel = lstBankAccountsTODataModel;
	}

	/**
	 * Gets the institution bank account session.
	 *
	 * @return the institution bank account session
	 */
	public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}

	/**
	 * Sets the institution bank account session.
	 *
	 * @param institutionBankAccountSession the new institution bank account session
	 */
	public void setInstitutionBankAccountSession(
			InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}

	public Long getIdBankPk() {
		return idBankPk;
	}

	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}

	public List<Bank> getLstBanks() {
		return lstBanks;
	}

	public void setLstBanks(List<Bank> lstBanks) {
		this.lstBanks = lstBanks;
	}

	public String getBcbCode() {
		return bcbCode;
	}

	public void setBcbCode(String bcbCode) {
		this.bcbCode = bcbCode;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public Date getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	public Date getMinDate() {
		return minDate;
	}

	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}

	public List<String> getListGloss() {
		return listGloss;
	}

	public void setListGloss(List<String> listGloss) {
		this.listGloss = listGloss;
	}

	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	public Integer getCurrencySelected() {
		return currencySelected;
	}

	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}

	public BigDecimal getAmountDeposited() {
		return amountDeposited;
	}

	public void setAmountDeposited(BigDecimal amountDeposited) {
		this.amountDeposited = amountDeposited;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
}
