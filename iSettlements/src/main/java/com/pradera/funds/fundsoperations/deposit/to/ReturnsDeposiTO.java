package com.pradera.funds.fundsoperations.deposit.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.funds.fundsoperations.deposit.view.CashAccountDetailDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReturnsDeposiTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class ReturnsDeposiTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id bank pk selected. */
	private Long idBankPkSelected;
	
	/** The lst bank. */
	private List<Bank> lstBank;
	
	/** The holder. */
	private Holder holder;
	
	/** The payed date. */
	private Date payedDate;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The currency selected. */
	private Integer currencySelected;
	
	/** The lst holder funds operation. */
	private List<HolderFundsOperation> lstHolderFundsOperation;
	
	/** The lst cash account detail data model. */
	private CashAccountDetailDataModel lstCashAccountDetailDataModel;
	
	/** The cash account detail. */
	private CashAccountDetail cashAccountDetail; 
	
	/** The lst cash account details. */
	private CashAccountDetailDataModel lstCashAccountDetails;
	
	/** The deposit amount. */
	private BigDecimal depositAmount;
	
	/** The reference payment. */
	private String referencePayment;
	
	/**
	 * Gets the id bank pk selected.
	 *
	 * @return the id bank pk selected
	 */
	public Long getIdBankPkSelected() {
		return idBankPkSelected;
	}
	
	/**
	 * Sets the id bank pk selected.
	 *
	 * @param idBankPkSelected the new id bank pk selected
	 */
	public void setIdBankPkSelected(Long idBankPkSelected) {
		this.idBankPkSelected = idBankPkSelected;
	}
	
	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 */
	public List<Bank> getLstBank() {
		return lstBank;
	}
	
	/**
	 * Sets the lst bank.
	 *
	 * @param lstBank the new lst bank
	 */
	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the payed date.
	 *
	 * @return the payed date
	 */
	public Date getPayedDate() {
		return payedDate;
	}
	
	/**
	 * Sets the payed date.
	 *
	 * @param payedDate the new payed date
	 */
	public void setPayedDate(Date payedDate) {
		this.payedDate = payedDate;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}
	
	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}
	
	/**
	 * Gets the lst holder funds operation.
	 *
	 * @return the lst holder funds operation
	 */
	public List<HolderFundsOperation> getLstHolderFundsOperation() {
		return lstHolderFundsOperation;
	}
	
	/**
	 * Sets the lst holder funds operation.
	 *
	 * @param lstHolderFundsOperation the new lst holder funds operation
	 */
	public void setLstHolderFundsOperation(
			List<HolderFundsOperation> lstHolderFundsOperation) {
		this.lstHolderFundsOperation = lstHolderFundsOperation;
	}
	
	/**
	 * Gets the deposit amount.
	 *
	 * @return the deposit amount
	 */
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}
	
	/**
	 * Sets the deposit amount.
	 *
	 * @param depositAmount the new deposit amount
	 */
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}


	/**
	 * Gets the reference payment.
	 *
	 * @return the reference payment
	 */
	public String getReferencePayment() {
		return referencePayment;
	}
	
	/**
	 * Sets the reference payment.
	 *
	 * @param referencePayment the new reference payment
	 */
	public void setReferencePayment(String referencePayment) {
		this.referencePayment = referencePayment;
	}
	
	/**
	 * Gets the lst cash account detail data model.
	 *
	 * @return the lst cash account detail data model
	 */
	public CashAccountDetailDataModel getLstCashAccountDetailDataModel() {
		return lstCashAccountDetailDataModel;
	}
	
	/**
	 * Sets the lst cash account detail data model.
	 *
	 * @param lstCashAccountDetailDataModel the new lst cash account detail data model
	 */
	public void setLstCashAccountDetailDataModel(
			CashAccountDetailDataModel lstCashAccountDetailDataModel) {
		this.lstCashAccountDetailDataModel = lstCashAccountDetailDataModel;
	}
	
	/**
	 * Gets the cash account detail.
	 *
	 * @return the cash account detail
	 */
	public CashAccountDetail getCashAccountDetail() {
		return cashAccountDetail;
	}
	
	/**
	 * Sets the cash account detail.
	 *
	 * @param cashAccountDetail the new cash account detail
	 */
	public void setCashAccountDetail(CashAccountDetail cashAccountDetail) {
		this.cashAccountDetail = cashAccountDetail;
	}
	
	/**
	 * Gets the lst cash account details.
	 *
	 * @return the lst cash account details
	 */
	public CashAccountDetailDataModel getLstCashAccountDetails() {
		return lstCashAccountDetails;
	}
	
	/**
	 * Sets the lst cash account details.
	 *
	 * @param lstCashAccountDetails the new lst cash account details
	 */
	public void setLstCashAccountDetails(
			CashAccountDetailDataModel lstCashAccountDetails) {
		this.lstCashAccountDetails = lstCashAccountDetails;
	}	
}
