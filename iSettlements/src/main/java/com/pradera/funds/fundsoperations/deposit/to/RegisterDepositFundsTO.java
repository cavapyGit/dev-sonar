package com.pradera.funds.fundsoperations.deposit.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.funds.fundsoperations.deposit.view.CashAccountDetailDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterDepositFundsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterDepositFundsTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst fund operation group. */
	private List<ParameterTable> lstFundOperationGroup;
	
	/** The lst operation type. */
	private List<FundsOperationType> lstOperationType;
	
	/**Lista de id de los bancos*/
	private List<Bank> lstBanks;
	
	/**Lista de tipos de moneda*/
	private List<ParameterTable> lstCurrency;
	
	/**Moneda de deposto registrada*/
	private Integer currencySelected;
	
	/**Monto de deposito para cobro de derechos economicos*/
	private BigDecimal amountDeposited;
	
	/**Glosa de deposito*/
	private String gloss;
	
	/**Lista de glosas que hayan sido generados*/
	private List<String> listGloss;
	/**Edicion de glosa*/
	private boolean disableEditGloss = true;
	
	/** The lst cash account type. */
	private List<ParameterTable> lstCashAccountType;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst issuers. */
	private List<Issuer> lstIssuers;
	
	/** The lst cash account detail modify. */
	private CashAccountDetailDataModel lstCashAccountDetail, lstCashAccountDetailModify;
	
	/** The fund operation group selected. */
	private Integer fundOperationGroupSelected;
	
	/** The operation type selected. */
	private Long operationTypeSelected;
	
	/** Deshabilitar tipo de operacion*/
	private boolean operationTypeSelectedDisabled;
	
	/** The account type selected. */
	private Long accountTypeSelected;
	
	/** The account type description. */
	private String accountTypeDescription;
	
	/** The institution id. */
	private Integer institutionId;
	
	/** The institution des. */
	private String institutionDes;
	
	/** The participant selected. */
	private Integer participantSelected;
	
	/** The bl issuer. */
	private boolean blEDV,blParticipant,blIssuer;
	
	/** The bl show bank reference. */
	private boolean blSettlement, blCorporativeProcess, blRates, blReturns, blSpecialPayments,blShowBankReference,blPaymentBenefits;
	
	/** The bl for central account. */
	private boolean blInternational, blGuarantees, blTaxation, blForCentralAccount, blCorporativeProcessReturns;
	
	/** The id funds operation mirror. */
	private Long idFundsOperationMirror;
	
	/** The settlement group to. */
	private SettlementDepositTO settlementGroupTO;
	
	/** The cash account detail. */
	private CashAccountDetail cashAccountDetail; 
	
	/** The international deposit to. */
	private InternationalDepositTO internationalDepositTO;
	
	/** The comments. */
	private String edvDescription,participantDes,issuerDes,comments;
	
	/** The bank reference. */
	private String bankReference;
	
	/**Id del Banco*/
	private Long idBankPk;
	
	/**Codigo BCB del banco o participante seleccionado*/
	private String bcbCode;
	
	/** The issuer. */
	private Issuer issuer;
	
	private Date operationDate;
	
	/** The returns deposit to. */
	private ReturnsDeposiTO returnsDepositTO;
	
	/** Fecha maxima */
	private Date maxDate;
	
	/**Fecha minima*/
	private Date minDate;
	
	private String bankMovement;
	
	/** The id funds operation. */
	Long idFundsOperation;
	
	/**
	 * Gets the lst fund operation group.
	 *
	 * @return the lst fund operation group
	 */
	public List<ParameterTable> getLstFundOperationGroup() {
		return lstFundOperationGroup;
	}
	
	/**
	 * Sets the lst fund operation group.
	 *
	 * @param lstFundOperationGroup the new lst fund operation group
	 */
	public void setLstFundOperationGroup(List<ParameterTable> lstFundOperationGroup) {
		this.lstFundOperationGroup = lstFundOperationGroup;
	}
	
	/**
	 * Gets the lst cash account type.
	 *
	 * @return the lst cash account type
	 */
	public List<ParameterTable> getLstCashAccountType() {
		return lstCashAccountType;
	}
	
	/**
	 * Sets the lst cash account type.
	 *
	 * @param lstCashAccountType the new lst cash account type
	 */
	public void setLstCashAccountType(List<ParameterTable> lstCashAccountType) {
		this.lstCashAccountType = lstCashAccountType;
	}
	
	/**
	 * Gets the fund operation group selected.
	 *
	 * @return the fund operation group selected
	 */
	public Integer getFundOperationGroupSelected() {
		return fundOperationGroupSelected;
	}
	
	/**
	 * Sets the fund operation group selected.
	 *
	 * @param fundOperationGroupSelected the new fund operation group selected
	 */
	public void setFundOperationGroupSelected(Integer fundOperationGroupSelected) {
		this.fundOperationGroupSelected = fundOperationGroupSelected;
	}
	
	/**
	 * Gets the operation type selected.
	 *
	 * @return the operation type selected
	 */
	public Long getOperationTypeSelected() {
		return operationTypeSelected;
	}
	
	/**
	 * Sets the operation type selected.
	 *
	 * @param operationTypeSelected the new operation type selected
	 */
	public void setOperationTypeSelected(Long operationTypeSelected) {
		this.operationTypeSelected = operationTypeSelected;
	}
	
	/**
	 * Gets the account type selected.
	 *
	 * @return the account type selected
	 */
	public Long getAccountTypeSelected() {
		return accountTypeSelected;
	}
	
	/**
	 * Sets the account type selected.
	 *
	 * @param accountTypeSelected the new account type selected
	 */
	public void setAccountTypeSelected(Long accountTypeSelected) {
		this.accountTypeSelected = accountTypeSelected;
	}
	
	/**
	 * Gets the institution id.
	 *
	 * @return the institution id
	 */
	public Integer getInstitutionId() {
		return institutionId;
	}
	
	/**
	 * Sets the institution id.
	 *
	 * @param institutionId the new institution id
	 */
	public void setInstitutionId(Integer institutionId) {
		this.institutionId = institutionId;
	}
	
	/**
	 * Gets the institution des.
	 *
	 * @return the institution des
	 */
	public String getInstitutionDes() {
		return institutionDes;
	}
	
	/**
	 * Sets the institution des.
	 *
	 * @param institutionDes the new institution des
	 */
	public void setInstitutionDes(String institutionDes) {
		this.institutionDes = institutionDes;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Integer getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Integer participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Checks if is bl edv.
	 *
	 * @return true, if is bl edv
	 */
	public boolean isBlEDV() {
		return blEDV;
	}
	
	/**
	 * Sets the bl edv.
	 *
	 * @param blEDV the new bl edv
	 */
	public void setBlEDV(boolean blEDV) {
		this.blEDV = blEDV;
	}
	
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Checks if is bl issuer.
	 *
	 * @return true, if is bl issuer
	 */
	public boolean isBlIssuer() {
		return blIssuer;
	}
	
	/**
	 * Sets the bl issuer.
	 *
	 * @param blIssuer the new bl issuer
	 */
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}
	
	/**
	 * Gets the lst operation type.
	 *
	 * @return the lst operation type
	 */
	public List<FundsOperationType> getLstOperationType() {
		return lstOperationType;
	}
	
	/**
	 * Sets the lst operation type.
	 *
	 * @param lstOperationType the new lst operation type
	 */
	public void setLstOperationType(List<FundsOperationType> lstOperationType) {
		this.lstOperationType = lstOperationType;
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the lst issuers.
	 *
	 * @return the lst issuers
	 */
	public List<Issuer> getLstIssuers() {
		return lstIssuers;
	}
	
	/**
	 * Sets the lst issuers.
	 *
	 * @param lstIssuers the new lst issuers
	 */
	public void setLstIssuers(List<Issuer> lstIssuers) {
		this.lstIssuers = lstIssuers;
	}
	
	/**
	 * Checks if is bl settlement.
	 *
	 * @return true, if is bl settlement
	 */
	public boolean isBlSettlement() {
		return blSettlement;
	}
	
	/**
	 * Sets the bl settlement.
	 *
	 * @param blSettlement the new bl settlement
	 */
	public void setBlSettlement(boolean blSettlement) {
		this.blSettlement = blSettlement;
	}
	
	/**
	 * Checks if is bl corporative process.
	 *
	 * @return true, if is bl corporative process
	 */
	public boolean isBlCorporativeProcess() {
		return blCorporativeProcess;
	}
	
	/**
	 * Sets the bl corporative process.
	 *
	 * @param blCorporativeProcess the new bl corporative process
	 */
	public void setBlCorporativeProcess(boolean blCorporativeProcess) {
		this.blCorporativeProcess = blCorporativeProcess;
	}
	
	public boolean isBlCorporativeProcessReturns() {
		return blCorporativeProcessReturns;
	}

	public void setBlCorporativeProcessReturns(boolean blCorporativeProcessReturns) {
		this.blCorporativeProcessReturns = blCorporativeProcessReturns;
	}
	
	/**
	 * Checks if is bl rates.
	 *
	 * @return true, if is bl rates
	 */
	public boolean isBlRates() {
		return blRates;
	}
	
	/**
	 * Sets the bl rates.
	 *
	 * @param blRates the new bl rates
	 */
	public void setBlRates(boolean blRates) {
		this.blRates = blRates;
	}
	
	/**
	 * Checks if is bl returns.
	 *
	 * @return true, if is bl returns
	 */
	public boolean isBlReturns() {
		return blReturns;
	}
	
	/**
	 * Sets the bl returns.
	 *
	 * @param blReturns the new bl returns
	 */
	public void setBlReturns(boolean blReturns) {
		this.blReturns = blReturns;
	}
	
	/**
	 * Checks if is bl special payments.
	 *
	 * @return true, if is bl special payments
	 */
	public boolean isBlSpecialPayments() {
		return blSpecialPayments;
	}
	
	/**
	 * Sets the bl special payments.
	 *
	 * @param blSpecialPayments the new bl special payments
	 */
	public void setBlSpecialPayments(boolean blSpecialPayments) {
		this.blSpecialPayments = blSpecialPayments;
	}
	
	/**
	 * Checks if is bl international.
	 *
	 * @return true, if is bl international
	 */
	public boolean isBlInternational() {
		return blInternational;
	}
	
	/**
	 * Sets the bl international.
	 *
	 * @param blInternational the new bl international
	 */
	public void setBlInternational(boolean blInternational) {
		this.blInternational = blInternational;
	}
	
	/**
	 * Checks if is bl guarantees.
	 *
	 * @return true, if is bl guarantees
	 */
	public boolean isBlGuarantees() {
		return blGuarantees;
	}
	
	/**
	 * Sets the bl guarantees.
	 *
	 * @param blGuarantees the new bl guarantees
	 */
	public void setBlGuarantees(boolean blGuarantees) {
		this.blGuarantees = blGuarantees;
	}
	
	/**
	 * Checks if is bl taxation.
	 *
	 * @return true, if is bl taxation
	 */
	public boolean isBlTaxation() {
		return blTaxation;
	}
	
	/**
	 * Sets the bl taxation.
	 *
	 * @param blTaxation the new bl taxation
	 */
	public void setBlTaxation(boolean blTaxation) {
		this.blTaxation = blTaxation;
	}
	
	/**
	 * Checks if is bl for central account.
	 *
	 * @return true, if is bl for central account
	 */
	public boolean isBlForCentralAccount() {
		return blForCentralAccount;
	}
	
	/**
	 * Sets the bl for central account.
	 *
	 * @param blForCentralAccount the new bl for central account
	 */
	public void setBlForCentralAccount(boolean blForCentralAccount) {
		this.blForCentralAccount = blForCentralAccount;
	}
	
	/**
	 * Gets the account type description.
	 *
	 * @return the account type description
	 */
	public String getAccountTypeDescription() {
		return accountTypeDescription;
	}
	
	/**
	 * Sets the account type description.
	 *
	 * @param accountTypeDescription the new account type description
	 */
	public void setAccountTypeDescription(String accountTypeDescription) {
		this.accountTypeDescription = accountTypeDescription;
	}
	
	/**
	 * Gets the settlement group to.
	 *
	 * @return the settlement group to
	 */
	public SettlementDepositTO getSettlementGroupTO() {
		return settlementGroupTO;
	}
	
	/**
	 * Sets the settlement group to.
	 *
	 * @param settlementGroupTO the new settlement group to
	 */
	public void setSettlementGroupTO(SettlementDepositTO settlementGroupTO) {
		this.settlementGroupTO = settlementGroupTO;
	}
	
	/**
	 * Gets the id funds operation mirror.
	 *
	 * @return the id funds operation mirror
	 */
	public Long getIdFundsOperationMirror() {
		return idFundsOperationMirror;
	}
	
	/**
	 * Sets the id funds operation mirror.
	 *
	 * @param idFundsOperationMirror the new id funds operation mirror
	 */
	public void setIdFundsOperationMirror(Long idFundsOperationMirror) {
		this.idFundsOperationMirror = idFundsOperationMirror;
	}
	
	/**
	 * Gets the cash account detail.
	 *
	 * @return the cash account detail
	 */
	public CashAccountDetail getCashAccountDetail() {
		return cashAccountDetail;
	}
	
	/**
	 * Sets the cash account detail.
	 *
	 * @param cashAccountDetail the new cash account detail
	 */
	public void setCashAccountDetail(CashAccountDetail cashAccountDetail) {
		this.cashAccountDetail = cashAccountDetail;
	}
	
	/**
	 * Gets the lst cash account detail.
	 *
	 * @return the lst cash account detail
	 */
	public CashAccountDetailDataModel getLstCashAccountDetail() {
		return lstCashAccountDetail;
	}
	
	/**
	 * Sets the lst cash account detail.
	 *
	 * @param lstCashAccountDetail the new lst cash account detail
	 */
	public void setLstCashAccountDetail(
			CashAccountDetailDataModel lstCashAccountDetail) {
		this.lstCashAccountDetail = lstCashAccountDetail;
	}
	
	/**
	 * Gets the lst cash account detail modify.
	 *
	 * @return the lst cash account detail modify
	 */
	public CashAccountDetailDataModel getLstCashAccountDetailModify() {
		return lstCashAccountDetailModify;
	}
	
	/**
	 * Sets the lst cash account detail modify.
	 *
	 * @param lstCashAccountDetailModify the new lst cash account detail modify
	 */
	public void setLstCashAccountDetailModify(
			CashAccountDetailDataModel lstCashAccountDetailModify) {
		this.lstCashAccountDetailModify = lstCashAccountDetailModify;
	}
	
	/**
	 * Gets the returns deposit to.
	 *
	 * @return the returns deposit to
	 */
	public ReturnsDeposiTO getReturnsDepositTO() {
		return returnsDepositTO;
	}
	
	/**
	 * Sets the returns deposit to.
	 *
	 * @param returnsDepositTO the new returns deposit to
	 */
	public void setReturnsDepositTO(ReturnsDeposiTO returnsDepositTO) {
		this.returnsDepositTO = returnsDepositTO;
	}
	
	/**
	 * Gets the international deposit to.
	 *
	 * @return the international deposit to
	 */
	public InternationalDepositTO getInternationalDepositTO() {
		return internationalDepositTO;
	}
	
	/**
	 * Sets the international deposit to.
	 *
	 * @param internationalDepositTO the new international deposit to
	 */
	public void setInternationalDepositTO(
			InternationalDepositTO internationalDepositTO) {
		this.internationalDepositTO = internationalDepositTO;
	}
	
	/**
	 * Gets the edv description.
	 *
	 * @return the edv description
	 */
	public String getEdvDescription() {
		return edvDescription;
	}
	
	/**
	 * Sets the edv description.
	 *
	 * @param edvDescription the new edv description
	 */
	public void setEdvDescription(String edvDescription) {
		this.edvDescription = edvDescription;
	}
	
	/**
	 * Gets the participant des.
	 *
	 * @return the participant des
	 */
	public String getParticipantDes() {
		return participantDes;
	}
	
	/**
	 * Sets the participant des.
	 *
	 * @param participantDes the new participant des
	 */
	public void setParticipantDes(String participantDes) {
		this.participantDes = participantDes;
	}
	
	/**
	 * Gets the issuer des.
	 *
	 * @return the issuer des
	 */
	public String getIssuerDes() {
		return issuerDes;
	}
	
	/**
	 * Sets the issuer des.
	 *
	 * @param issuerDes the new issuer des
	 */
	public void setIssuerDes(String issuerDes) {
		this.issuerDes = issuerDes;
	}
	
	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	
	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	/**
	 * Gets the bank reference.
	 *
	 * @return the bank reference
	 */
	public String getBankReference() {
		return bankReference;
	}
	
	/**
	 * Sets the bank reference.
	 *
	 * @param bankReference the new bank reference
	 */
	public void setBankReference(String bankReference) {
		this.bankReference = bankReference;
	}
	
	/**
	 * Checks if is bl show bank reference.
	 *
	 * @return true, if is bl show bank reference
	 */
	public boolean isBlShowBankReference() {
		return blShowBankReference;
	}
	
	/**
	 * Sets the bl show bank reference.
	 *
	 * @param blShowBankReference the new bl show bank reference
	 */
	public void setBlShowBankReference(boolean blShowBankReference) {
		this.blShowBankReference = blShowBankReference;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the id funds operation.
	 *
	 * @return the idFundsOperation
	 */
	public Long getIdFundsOperation() {
		return idFundsOperation;
	}
	
	/**
	 * Sets the id funds operation.
	 *
	 * @param idFundsOperation the idFundsOperation to set
	 */
	public void setIdFundsOperation(Long idFundsOperation) {
		this.idFundsOperation = idFundsOperation;
	}

	public boolean isBlPaymentBenefits() {
		return blPaymentBenefits;
	}

	public void setBlPaymentBenefits(boolean blPaymentBenefits) {
		this.blPaymentBenefits = blPaymentBenefits;
	}

	public Long getIdBankPk() {
		return idBankPk;
	}

	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}


	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	public List<Bank> getLstBanks() {
		return lstBanks;
	}

	public void setLstBanks(List<Bank> lstBanks) {
		this.lstBanks = lstBanks;
	}

	public boolean isOperationTypeSelectedDisabled() {
		return operationTypeSelectedDisabled;
	}

	public void setOperationTypeSelectedDisabled(
			boolean operationTypeSelectedDisabled) {
		this.operationTypeSelectedDisabled = operationTypeSelectedDisabled;
	}

	public String getBcbCode() {
		return bcbCode;
	}

	public void setBcbCode(String bcbCode) {
		this.bcbCode = bcbCode;
	}

	public Integer getCurrencySelected() {
		return currencySelected;
	}

	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}

	public BigDecimal getAmountDeposited() {
		return amountDeposited;
	}

	public void setAmountDeposited(BigDecimal amountDeposited) {
		this.amountDeposited = amountDeposited;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public boolean isDisableEditGloss() {
		return disableEditGloss;
	}

	public void setDisableEditGloss(boolean disableEditGloss) {
		this.disableEditGloss = disableEditGloss;
	}

	public List<String> getListGloss() {
		return listGloss;
	}

	public void setListGloss(List<String> listGloss) {
		this.listGloss = listGloss;
	}

	public Date getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	public Date getMinDate() {
		return minDate;
	}

	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}

	public String getBankMovement() {
		return bankMovement;
	}

	public void setBankMovement(String bankMovement) {
		this.bankMovement = bankMovement;
	}
	
}
