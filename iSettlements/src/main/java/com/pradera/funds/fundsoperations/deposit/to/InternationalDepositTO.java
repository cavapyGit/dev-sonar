package com.pradera.funds.fundsoperations.deposit.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.funds.fundsoperations.deposit.view.CashAccountDetailDataModel;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.InternationalOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InternationalDepositTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class InternationalDepositTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The bl send. */
	private boolean blReception, blSend;
	
	/** The settlement date. */
	private Date registryInitial, registryEnd, settlementInitial, settlementEnd, operationDate, settlementDate;
	
	/** The id international operation pk. */
	private Long operationNumber, idInternationalOperationPk;
	
	/** The real deposit amount. */
	private BigDecimal effectiveAmount, pendientAmount, excedentAmount, depositAmount, realDepositAmount;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The lst international operation. */
	private List<InternationalOperation> lstInternationalOperation;
	
	/** The lst international depository liquidator. */
	private List<InternationalDepository> lstInternationalDepositoryNegotiator, lstInternationalDepositoryLiquidator;
	
	/** The id int depo liqui pk selected. */
	private Long idIntDepoNegoPkSelected, idIntDepoLiquiPkSelected; 
	
	/** The id international participant pk. */
	private Long idInterDepoNegotitatiorPk, idInterDepoLiquidatorPk, idInternationalParticipantPk;
	
	/** The participant inter description. */
	private String interDepoNegotiatorDescription, interDepoLiquidatorDescription, participantInterDescription;
	
	/** The currency. */
	private Integer currencySelected, currency;
	
	/** The id isin code pk. */
	private String currencyDescription, idIsinCodePk;
	
	/** The error found. */
	private String errorFound;
	
	/** The lst cash account details. */
	private CashAccountDetailDataModel lstCashAccountDetails;
	
	/** The cash account detail. */
	private CashAccountDetail cashAccountDetail; 
	
	/**
	 * Checks if is bl reception.
	 *
	 * @return true, if is bl reception
	 */
	public boolean isBlReception() {
		return blReception;
	}
	
	/**
	 * Sets the bl reception.
	 *
	 * @param blReception the new bl reception
	 */
	public void setBlReception(boolean blReception) {
		this.blReception = blReception;
	}
	
	/**
	 * Checks if is bl send.
	 *
	 * @return true, if is bl send
	 */
	public boolean isBlSend() {
		return blSend;
	}
	
	/**
	 * Sets the bl send.
	 *
	 * @param blSend the new bl send
	 */
	public void setBlSend(boolean blSend) {
		this.blSend = blSend;
	}
	
	/**
	 * Gets the registry initial.
	 *
	 * @return the registry initial
	 */
	public Date getRegistryInitial() {
		return registryInitial;
	}
	
	/**
	 * Sets the registry initial.
	 *
	 * @param registryInitial the new registry initial
	 */
	public void setRegistryInitial(Date registryInitial) {
		this.registryInitial = registryInitial;
	}
	
	/**
	 * Gets the registry end.
	 *
	 * @return the registry end
	 */
	public Date getRegistryEnd() {
		return registryEnd;
	}
	
	/**
	 * Sets the registry end.
	 *
	 * @param registryEnd the new registry end
	 */
	public void setRegistryEnd(Date registryEnd) {
		this.registryEnd = registryEnd;
	}
	
	/**
	 * Gets the settlement initial.
	 *
	 * @return the settlement initial
	 */
	public Date getSettlementInitial() {
		return settlementInitial;
	}
	
	/**
	 * Sets the settlement initial.
	 *
	 * @param settlementInitial the new settlement initial
	 */
	public void setSettlementInitial(Date settlementInitial) {
		this.settlementInitial = settlementInitial;
	}
	
	/**
	 * Gets the settlement end.
	 *
	 * @return the settlement end
	 */
	public Date getSettlementEnd() {
		return settlementEnd;
	}
	
	/**
	 * Sets the settlement end.
	 *
	 * @param settlementEnd the new settlement end
	 */
	public void setSettlementEnd(Date settlementEnd) {
		this.settlementEnd = settlementEnd;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the effective amount.
	 *
	 * @return the effective amount
	 */
	public BigDecimal getEffectiveAmount() {
		return effectiveAmount;
	}
	
	/**
	 * Sets the effective amount.
	 *
	 * @param effectiveAmount the new effective amount
	 */
	public void setEffectiveAmount(BigDecimal effectiveAmount) {
		this.effectiveAmount = effectiveAmount;
	}
	
	/**
	 * Gets the pendient amount.
	 *
	 * @return the pendient amount
	 */
	public BigDecimal getPendientAmount() {
		return pendientAmount;
	}
	
	/**
	 * Sets the pendient amount.
	 *
	 * @param pendientAmount the new pendient amount
	 */
	public void setPendientAmount(BigDecimal pendientAmount) {
		this.pendientAmount = pendientAmount;
	}
	
	/**
	 * Gets the excedent amount.
	 *
	 * @return the excedent amount
	 */
	public BigDecimal getExcedentAmount() {
		return excedentAmount;
	}
	
	/**
	 * Sets the excedent amount.
	 *
	 * @param excedentAmount the new excedent amount
	 */
	public void setExcedentAmount(BigDecimal excedentAmount) {
		this.excedentAmount = excedentAmount;
	}
	
	/**
	 * Gets the deposit amount.
	 *
	 * @return the deposit amount
	 */
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}
	
	/**
	 * Sets the deposit amount.
	 *
	 * @param depositAmount the new deposit amount
	 */
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}
	
	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}
	
	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	
	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the lst international operation.
	 *
	 * @return the lst international operation
	 */
	public List<InternationalOperation> getLstInternationalOperation() {
		return lstInternationalOperation;
	}
	
	/**
	 * Sets the lst international operation.
	 *
	 * @param lstInternationalOperation the new lst international operation
	 */
	public void setLstInternationalOperation(
			List<InternationalOperation> lstInternationalOperation) {
		this.lstInternationalOperation = lstInternationalOperation;
	}
	
	/**
	 * Gets the lst international depository negotiator.
	 *
	 * @return the lst international depository negotiator
	 */
	public List<InternationalDepository> getLstInternationalDepositoryNegotiator() {
		return lstInternationalDepositoryNegotiator;
	}
	
	/**
	 * Sets the lst international depository negotiator.
	 *
	 * @param lstInternationalDepositoryNegotiator the new lst international depository negotiator
	 */
	public void setLstInternationalDepositoryNegotiator(
			List<InternationalDepository> lstInternationalDepositoryNegotiator) {
		this.lstInternationalDepositoryNegotiator = lstInternationalDepositoryNegotiator;
	}
	
	/**
	 * Gets the lst international depository liquidator.
	 *
	 * @return the lst international depository liquidator
	 */
	public List<InternationalDepository> getLstInternationalDepositoryLiquidator() {
		return lstInternationalDepositoryLiquidator;
	}
	
	/**
	 * Sets the lst international depository liquidator.
	 *
	 * @param lstInternationalDepositoryLiquidator the new lst international depository liquidator
	 */
	public void setLstInternationalDepositoryLiquidator(
			List<InternationalDepository> lstInternationalDepositoryLiquidator) {
		this.lstInternationalDepositoryLiquidator = lstInternationalDepositoryLiquidator;
	}
	
	/**
	 * Gets the id int depo nego pk selected.
	 *
	 * @return the id int depo nego pk selected
	 */
	public Long getIdIntDepoNegoPkSelected() {
		return idIntDepoNegoPkSelected;
	}
	
	/**
	 * Sets the id int depo nego pk selected.
	 *
	 * @param idIntDepoNegoPkSelected the new id int depo nego pk selected
	 */
	public void setIdIntDepoNegoPkSelected(Long idIntDepoNegoPkSelected) {
		this.idIntDepoNegoPkSelected = idIntDepoNegoPkSelected;
	}
	
	/**
	 * Gets the id int depo liqui pk selected.
	 *
	 * @return the id int depo liqui pk selected
	 */
	public Long getIdIntDepoLiquiPkSelected() {
		return idIntDepoLiquiPkSelected;
	}
	
	/**
	 * Sets the id int depo liqui pk selected.
	 *
	 * @param idIntDepoLiquiPkSelected the new id int depo liqui pk selected
	 */
	public void setIdIntDepoLiquiPkSelected(Long idIntDepoLiquiPkSelected) {
		this.idIntDepoLiquiPkSelected = idIntDepoLiquiPkSelected;
	}
	
	/**
	 * Gets the inter depo negotiator description.
	 *
	 * @return the inter depo negotiator description
	 */
	public String getInterDepoNegotiatorDescription() {
		return interDepoNegotiatorDescription;
	}
	
	/**
	 * Sets the inter depo negotiator description.
	 *
	 * @param interDepoNegotiatorDescription the new inter depo negotiator description
	 */
	public void setInterDepoNegotiatorDescription(
			String interDepoNegotiatorDescription) {
		this.interDepoNegotiatorDescription = interDepoNegotiatorDescription;
	}
	
	/**
	 * Gets the inter depo liquidator description.
	 *
	 * @return the inter depo liquidator description
	 */
	public String getInterDepoLiquidatorDescription() {
		return interDepoLiquidatorDescription;
	}
	
	/**
	 * Sets the inter depo liquidator description.
	 *
	 * @param interDepoLiquidatorDescription the new inter depo liquidator description
	 */
	public void setInterDepoLiquidatorDescription(
			String interDepoLiquidatorDescription) {
		this.interDepoLiquidatorDescription = interDepoLiquidatorDescription;
	}
	
	/**
	 * Gets the participant inter description.
	 *
	 * @return the participant inter description
	 */
	public String getParticipantInterDescription() {
		return participantInterDescription;
	}
	
	/**
	 * Sets the participant inter description.
	 *
	 * @param participantInterDescription the new participant inter description
	 */
	public void setParticipantInterDescription(String participantInterDescription) {
		this.participantInterDescription = participantInterDescription;
	}
	
	/**
	 * Gets the id international operation pk.
	 *
	 * @return the id international operation pk
	 */
	public Long getIdInternationalOperationPk() {
		return idInternationalOperationPk;
	}
	
	/**
	 * Sets the id international operation pk.
	 *
	 * @param idInternationalOperationPk the new id international operation pk
	 */
	public void setIdInternationalOperationPk(Long idInternationalOperationPk) {
		this.idInternationalOperationPk = idInternationalOperationPk;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the id inter depo negotitatior pk.
	 *
	 * @return the id inter depo negotitatior pk
	 */
	public Long getIdInterDepoNegotitatiorPk() {
		return idInterDepoNegotitatiorPk;
	}
	
	/**
	 * Sets the id inter depo negotitatior pk.
	 *
	 * @param idInterDepoNegotitatiorPk the new id inter depo negotitatior pk
	 */
	public void setIdInterDepoNegotitatiorPk(Long idInterDepoNegotitatiorPk) {
		this.idInterDepoNegotitatiorPk = idInterDepoNegotitatiorPk;
	}
	
	/**
	 * Gets the id inter depo liquidator pk.
	 *
	 * @return the id inter depo liquidator pk
	 */
	public Long getIdInterDepoLiquidatorPk() {
		return idInterDepoLiquidatorPk;
	}
	
	/**
	 * Sets the id inter depo liquidator pk.
	 *
	 * @param idInterDepoLiquidatorPk the new id inter depo liquidator pk
	 */
	public void setIdInterDepoLiquidatorPk(Long idInterDepoLiquidatorPk) {
		this.idInterDepoLiquidatorPk = idInterDepoLiquidatorPk;
	}
	
	/**
	 * Gets the id international participant pk.
	 *
	 * @return the id international participant pk
	 */
	public Long getIdInternationalParticipantPk() {
		return idInternationalParticipantPk;
	}
	
	/**
	 * Sets the id international participant pk.
	 *
	 * @param idInternationalParticipantPk the new id international participant pk
	 */
	public void setIdInternationalParticipantPk(Long idInternationalParticipantPk) {
		this.idInternationalParticipantPk = idInternationalParticipantPk;
	}
	
	/**
	 * Gets the error found.
	 *
	 * @return the error found
	 */
	public String getErrorFound() {
		return errorFound;
	}
	
	/**
	 * Sets the error found.
	 *
	 * @param errorFound the new error found
	 */
	public void setErrorFound(String errorFound) {
		this.errorFound = errorFound;
	}
	
	/**
	 * Gets the real deposit amount.
	 *
	 * @return the real deposit amount
	 */
	public BigDecimal getRealDepositAmount() {
		return realDepositAmount;
	}
	
	/**
	 * Sets the real deposit amount.
	 *
	 * @param realDepositAmount the new real deposit amount
	 */
	public void setRealDepositAmount(BigDecimal realDepositAmount) {
		this.realDepositAmount = realDepositAmount;
	}
	
	/**
	 * Gets the lst cash account details.
	 *
	 * @return the lst cash account details
	 */
	public CashAccountDetailDataModel getLstCashAccountDetails() {
		return lstCashAccountDetails;
	}
	
	/**
	 * Sets the lst cash account details.
	 *
	 * @param lstCashAccountDetails the new lst cash account details
	 */
	public void setLstCashAccountDetails(
			CashAccountDetailDataModel lstCashAccountDetails) {
		this.lstCashAccountDetails = lstCashAccountDetails;
	}
	
	/**
	 * Gets the cash account detail.
	 *
	 * @return the cash account detail
	 */
	public CashAccountDetail getCashAccountDetail() {
		return cashAccountDetail;
	}
	
	/**
	 * Sets the cash account detail.
	 *
	 * @param cashAccountDetail the new cash account detail
	 */
	public void setCashAccountDetail(CashAccountDetail cashAccountDetail) {
		this.cashAccountDetail = cashAccountDetail;
	}
	
}
