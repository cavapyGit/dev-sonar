package com.pradera.funds.fundsoperations.deposit.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchDepositFundsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchDepositFundsTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The fund operation group selected. */
	private Integer fundOperationGroupSelected;
	
	/** The operation type selected. */
	private Long operationTypeSelected;
	
	/** The account type selected. */
	private Long accountTypeSelected;
	
	/** The process type. */
	private Long processType;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The operation number. */
	private Integer operationNumber;
	
	/** The state. */
	private Integer state;
	
	/** The participant selected. */
	private Integer participantSelected;
	
	/** The currency. */
	private Long currency;
	
	/** The mechanism selected. */
	private Long mechanismSelected;
	
	/** The modality selected. */
	private Long modalitySelected;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The lst fund operation group. */
	//List search
	private List<ParameterTable> lstFundOperationGroup;
	
	/** The lst state. */
	private List<ParameterTable>  lstState;
	
	/** The lst operation type. */
	private List<FundsOperationType> lstOperationType;
	
	/** The lst process type. */
	private List<ParameterTable>  lstProcessType;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst issuers. */
	private List<Issuer> lstIssuers;
	
	/** The bl settlement. */
	private boolean blNoResult, blDefaultSearch, blSettlement;	
	
	/** The str account number. */
	private String strAccountNumber;
	
	/** The bl issuer. */
	private boolean blParticipant,blIssuer;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** id del banco seleccionado*/
	private Long idBankPk;
	
	/**Lista de bancos **/
	private List<Long> listBanks;
	
	/**
	 * Gets the fund operation group selected.
	 *
	 * @return the fund operation group selected
	 */
	public Integer getFundOperationGroupSelected() {
		return fundOperationGroupSelected;
	}
	
	/**
	 * Sets the fund operation group selected.
	 *
	 * @param fundOperationGroupSelected the new fund operation group selected
	 */
	public void setFundOperationGroupSelected(Integer fundOperationGroupSelected) {
		this.fundOperationGroupSelected = fundOperationGroupSelected;
	}
	
	/**
	 * Gets the operation type selected.
	 *
	 * @return the operation type selected
	 */
	public Long getOperationTypeSelected() {
		return operationTypeSelected;
	}
	
	/**
	 * Sets the operation type selected.
	 *
	 * @param operationTypeSelected the new operation type selected
	 */
	public void setOperationTypeSelected(Long operationTypeSelected) {
		this.operationTypeSelected = operationTypeSelected;
	}
	
	/**
	 * Gets the account type selected.
	 *
	 * @return the account type selected
	 */
	public Long getAccountTypeSelected() {
		return accountTypeSelected;
	}
	
	/**
	 * Sets the account type selected.
	 *
	 * @param accountTypeSelected the new account type selected
	 */
	public void setAccountTypeSelected(Long accountTypeSelected) {
		this.accountTypeSelected = accountTypeSelected;
	}
	
	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Long getProcessType() {
		return processType;
	}
	
	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Long processType) {
		this.processType = processType;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Integer getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Integer operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Integer getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Integer participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Long getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Long currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the mechanism selected.
	 *
	 * @return the mechanism selected
	 */
	public Long getMechanismSelected() {
		return mechanismSelected;
	}
	
	/**
	 * Sets the mechanism selected.
	 *
	 * @param mechanismSelected the new mechanism selected
	 */
	public void setMechanismSelected(Long mechanismSelected) {
		this.mechanismSelected = mechanismSelected;
	}
	
	/**
	 * Gets the modality selected.
	 *
	 * @return the modality selected
	 */
	public Long getModalitySelected() {
		return modalitySelected;
	}
	
	/**
	 * Sets the modality selected.
	 *
	 * @param modalitySelected the new modality selected
	 */
	public void setModalitySelected(Long modalitySelected) {
		this.modalitySelected = modalitySelected;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the lst fund operation group.
	 *
	 * @return the lst fund operation group
	 */
	public List<ParameterTable> getLstFundOperationGroup() {
		return lstFundOperationGroup;
	}
	
	/**
	 * Sets the lst fund operation group.
	 *
	 * @param lstFundOperationGroup the new lst fund operation group
	 */
	public void setLstFundOperationGroup(List<ParameterTable> lstFundOperationGroup) {
		this.lstFundOperationGroup = lstFundOperationGroup;
	}
	
	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	
	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	
	/**
	 * Gets the lst operation type.
	 *
	 * @return the lst operation type
	 */
	public List<FundsOperationType> getLstOperationType() {
		return lstOperationType;
	}
	
	/**
	 * Sets the lst operation type.
	 *
	 * @param lstOperationType the new lst operation type
	 */
	public void setLstOperationType(List<FundsOperationType> lstOperationType) {
		this.lstOperationType = lstOperationType;
	}
	
	/**
	 * Gets the lst process type.
	 *
	 * @return the lst process type
	 */
	public List<ParameterTable> getLstProcessType() {
		return lstProcessType;
	}
	
	/**
	 * Sets the lst process type.
	 *
	 * @param lstProcessType the new lst process type
	 */
	public void setLstProcessType(List<ParameterTable> lstProcessType) {
		this.lstProcessType = lstProcessType;
	}
	
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Checks if is bl issuer.
	 *
	 * @return true, if is bl issuer
	 */
	public boolean isBlIssuer() {
		return blIssuer;
	}
	
	/**
	 * Sets the bl issuer.
	 *
	 * @param blIssuer the new bl issuer
	 */
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Checks if is bl default search.
	 *
	 * @return true, if is bl default search
	 */
	public boolean isBlDefaultSearch() {
		return blDefaultSearch;
	}
	
	/**
	 * Sets the bl default search.
	 *
	 * @param blDefaultSearch the new bl default search
	 */
	public void setBlDefaultSearch(boolean blDefaultSearch) {
		this.blDefaultSearch = blDefaultSearch;
	}
	
	/**
	 * Checks if is bl settlement.
	 *
	 * @return true, if is bl settlement
	 */
	public boolean isBlSettlement() {
		return blSettlement;
	}
	
	/**
	 * Sets the bl settlement.
	 *
	 * @param blSettlement the new bl settlement
	 */
	public void setBlSettlement(boolean blSettlement) {
		this.blSettlement = blSettlement;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the lst issuers.
	 *
	 * @return the lst issuers
	 */
	public List<Issuer> getLstIssuers() {
		return lstIssuers;
	}
	
	/**
	 * Sets the lst issuers.
	 *
	 * @param lstIssuers the new lst issuers
	 */
	public void setLstIssuers(List<Issuer> lstIssuers) {
		this.lstIssuers = lstIssuers;
	}
	
	/**
	 * Gets the str account number.
	 *
	 * @return the strAccountNumber
	 */
	public String getStrAccountNumber() {
		return strAccountNumber;
	}
	
	/**
	 * Sets the str account number.
	 *
	 * @param strAccountNumber the strAccountNumber to set
	 */
	public void setStrAccountNumber(String strAccountNumber) {
		this.strAccountNumber = strAccountNumber;
	}

	public List<Long> getListBanks() {
		return listBanks;
	}

	public void setListBanks(List<Long> listBanks) {
		this.listBanks = listBanks;
	}

	public Long getIdBankPk() {
		return idBankPk;
	}

	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}
}
