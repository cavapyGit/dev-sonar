package com.pradera.funds.fundsoperations.retirement.to;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderFundsOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class HolderFundsOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The id holder funds operation pk. */
	private Long idHolderFundsOperationPk;
	
	/** The funds operation. */
	private Long fundsOperation;

	/** The related holder cash movement. */
	private Long relatedHolderCashMovement;
	
	/** The funds operation type. */
	private Long fundsOperationType;
	
	/** The holder account bank. */
	private Long holderAccountBank;	
		
	/** The holder. */
	private Long holder;
	
	/** The full name holder. */
	private String fullNameHolder;
	
	/** The holder document number. */
	private String holderDocumentNumber;
	
	/** The holder operation amount. */
	private BigDecimal holderOperationAmount;
	
	/** The holder operation state. */
	private Integer holderOperationState;

	/** The currency. */
	private Integer currency;

	/** The bank account type. */
	private Integer bankAccountType;

	/**
	 * Gets the id holder funds operation pk.
	 *
	 * @return the id holder funds operation pk
	 */
	public Long getIdHolderFundsOperationPk() {
		return idHolderFundsOperationPk;
	}

	/**
	 * Sets the id holder funds operation pk.
	 *
	 * @param idHolderFundsOperationPk the new id holder funds operation pk
	 */
	public void setIdHolderFundsOperationPk(Long idHolderFundsOperationPk) {
		this.idHolderFundsOperationPk = idHolderFundsOperationPk;
	}

	/**
	 * Gets the funds operation.
	 *
	 * @return the funds operation
	 */
	public Long getFundsOperation() {
		return fundsOperation;
	}

	/**
	 * Sets the funds operation.
	 *
	 * @param fundsOperation the new funds operation
	 */
	public void setFundsOperation(Long fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	/**
	 * Gets the related holder cash movement.
	 *
	 * @return the related holder cash movement
	 */
	public Long getRelatedHolderCashMovement() {
		return relatedHolderCashMovement;
	}

	/**
	 * Sets the related holder cash movement.
	 *
	 * @param relatedHolderCashMovement the new related holder cash movement
	 */
	public void setRelatedHolderCashMovement(Long relatedHolderCashMovement) {
		this.relatedHolderCashMovement = relatedHolderCashMovement;
	}

	/**
	 * Gets the funds operation type.
	 *
	 * @return the funds operation type
	 */
	public Long getFundsOperationType() {
		return fundsOperationType;
	}

	/**
	 * Sets the funds operation type.
	 *
	 * @param fundsOperationType the new funds operation type
	 */
	public void setFundsOperationType(Long fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}

	/**
	 * Gets the holder account bank.
	 *
	 * @return the holder account bank
	 */
	public Long getHolderAccountBank() {
		return holderAccountBank;
	}

	/**
	 * Sets the holder account bank.
	 *
	 * @param holderAccountBank the new holder account bank
	 */
	public void setHolderAccountBank(Long holderAccountBank) {
		this.holderAccountBank = holderAccountBank;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Long getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Long holder) {
		this.holder = holder;
	}

	/**
	 * Gets the full name holder.
	 *
	 * @return the full name holder
	 */
	public String getFullNameHolder() {
		return fullNameHolder;
	}

	/**
	 * Sets the full name holder.
	 *
	 * @param fullNameHolder the new full name holder
	 */
	public void setFullNameHolder(String fullNameHolder) {
		this.fullNameHolder = fullNameHolder;
	}

	/**
	 * Gets the holder document number.
	 *
	 * @return the holder document number
	 */
	public String getHolderDocumentNumber() {
		return holderDocumentNumber;
	}

	/**
	 * Sets the holder document number.
	 *
	 * @param holderDocumentNumber the new holder document number
	 */
	public void setHolderDocumentNumber(String holderDocumentNumber) {
		this.holderDocumentNumber = holderDocumentNumber;
	}

	/**
	 * Gets the holder operation amount.
	 *
	 * @return the holder operation amount
	 */
	public BigDecimal getHolderOperationAmount() {
		return holderOperationAmount;
	}

	/**
	 * Sets the holder operation amount.
	 *
	 * @param holderOperationAmount the new holder operation amount
	 */
	public void setHolderOperationAmount(BigDecimal holderOperationAmount) {
		this.holderOperationAmount = holderOperationAmount;
	}

	/**
	 * Gets the holder operation state.
	 *
	 * @return the holder operation state
	 */
	public Integer getHolderOperationState() {
		return holderOperationState;
	}

	/**
	 * Sets the holder operation state.
	 *
	 * @param holderOperationState the new holder operation state
	 */
	public void setHolderOperationState(Integer holderOperationState) {
		this.holderOperationState = holderOperationState;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return bankAccountType;
	}

	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}
}
