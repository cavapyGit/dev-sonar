package com.pradera.funds.fundsoperations.deposit.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.fundoperations.monitoring.facade.MonitoringFundOperationsServiceFacade;
import com.pradera.funds.fundoperations.util.PropertiesConstants;
import com.pradera.funds.fundsoperations.deposit.facade.ManageDepositServiceFacade;
import com.pradera.funds.fundsoperations.deposit.to.InternationalDepositTO;
import com.pradera.funds.fundsoperations.deposit.to.RegisterDepositFundsTO;
import com.pradera.funds.fundsoperations.deposit.to.ReturnsDeposiTO;
import com.pradera.funds.fundsoperations.deposit.to.SearchDepositFundsTO;
import com.pradera.funds.fundsoperations.deposit.to.SettlementDepositTO;
import com.pradera.funds.fundsoperations.retirement.facade.RetirementFundsOperationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.FundsInternationalOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.type.FundOpeGroupEntityRelationType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.funds.type.SearchOperationType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.type.OperationPartType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageDepositFundsBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@LoggerCreateBean
@DepositaryWebBean
public class ManageDepositFundsBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search deposit funds to. */
	private SearchDepositFundsTO searchDepositFundsTO;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The manage deposit service facade. */
	@EJB
	ManageDepositServiceFacade manageDepositServiceFacade;
	
	/** The general parameters facade. */
    @EJB
    GeneralParametersFacade generalParametersFacade;
	
	/** The monitor cash account facade. */
	@EJB
	private MonitoringFundOperationsServiceFacade monitorCashAccountFacade;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The issuer query service bean. */
	@EJB
	private IssuerQueryServiceBean issuerQueryServiceBean;
	/** The funds operation service facade. */
	@EJB
	private RetirementFundsOperationServiceFacade fundsOperationServiceFacade;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The register deposit funds to. */
	private RegisterDepositFundsTO registerDepositFundsTO;
	
	/** The max days of custody request. */
	@Inject @Configurable
	Integer maxDaysOfCustodyRequest;
	
	/** The bl reject. */
	private boolean blConfirm, blReject,blTransfersDep;
	
	/** The fund operation detail. */
	private FundsOperation fundOperation, fundOperationDetail;
	
	/** The array fund operation. */
	private FundsOperation[] arrayFundOperation;
	
	/** The lst funds operation. */
	private FundsOperationDataModel lstFundsOperation;
	
	/** The obj institution bank accounts type. */
	private Integer objInstitutionBankAccountsType;
	
	/** The lst institution bank accounts type. */
	private List<ParameterTable> lstInstitutionBankAccountsTypeReturn;
	
	/** The mp currency. */
	private Map<Integer, String> mpCurrency = new HashMap<Integer, String>();
	
	/** The mp fund ope group. */
	private Map<Integer, String> mpFundOpeGroup = new HashMap<Integer, String>();
	
	/** The mp state. */
	private Map<Integer, String> mpState = new HashMap<Integer, String>();
	
	/** The mp operation type. */
	private Map<Long, String> mpOperationType = new HashMap<Long, String>();
	
	/** The Constant STR_VIEW_DEPOSIT_MAPPING. */
	private final static String STR_VIEW_DEPOSIT_MAPPING = "viewDeposit";
	
	/** The Constant STR_NEW_DEPOSIT_MAPPING. */
	private final static String STR_NEW_DEPOSIT_MAPPING = "registerDepositFunds";
	
	/** The List Cash Account Type. */
	private List<ParameterTable> lstCashAccountType;
	/** The List currency. */
	private List<ParameterTable> lstCurrency;
	/** The List Negotiation Mechanism. */
	private List<NegotiationMechanism> lstNegMechanism;
	/** The List Modality Group. */
	private List<ModalityGroup> lstModalityGroup;
	//Tmp para el view
	/** The certificates nums. */
	private List<Long> operationsNums = new ArrayList<Long>();
	/**  */
	private String currencySelect;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		searchDepositFundsTO = new SearchDepositFundsTO();
		searchDepositFundsTO.setIssuer(new Issuer());
		searchDepositFundsTO.setInitialDate(getCurrentSystemDate());
		searchDepositFundsTO.setEndDate(getCurrentSystemDate());
		try{
			fillInitialList();
			fillMaps();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			loadUserValidation();
			fillInstitutionData();
			lstFundsOperation=null;
			lstCashAccountType = generalPametersFacade.getListParameterTableServiceBean(MasterTableType.MASTER_TABLE_FUND_ACCOUNT_TYPE.getCode());
			loadCurrency();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Load user validation.
	 */
	private void loadUserValidation() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();				
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmDepositary(Boolean.TRUE);
		privilegeComponent.setBtnAuthorize(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);				
	}
	
	/**
	 * Fill institution data.
	 */
	private void fillInstitutionData()
	{
		try{
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				searchDepositFundsTO.setFundOperationGroupSelected(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
				getParticipants(false);
				searchDepositFundsTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode().intValue());
				searchDepositFundsTO.setBlParticipant(true);
				searchDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(searchDepositFundsTO.getFundOperationGroupSelected(), false));
			}
			else if(userInfo.getUserAccountSession().isIssuerInstitucion()){
				searchDepositFundsTO.setFundOperationGroupSelected(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
				getIssuers();
				Issuer objIssuer=manageDepositServiceFacade.findIssuerFacade(userInfo.getUserAccountSession().getIssuerCode());
				searchDepositFundsTO.setIssuer(objIssuer);
				searchDepositFundsTO.setBlIssuer(true);
				searchDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(searchDepositFundsTO.getFundOperationGroupSelected(), false));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Fill maps.
	 */
	private void fillMaps() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();	
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			//Monedas
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable paramTab:generalPametersFacade.getListParameterTableServiceBean(parameterTableTO))
				mpCurrency.put(paramTab.getParameterTablePk(), paramTab.getDescription());
			//Estado
			for(ParameterTable paramTab:searchDepositFundsTO.getLstState())
				mpState.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			//Grupo operacion fondo
			for(ParameterTable paramTab:searchDepositFundsTO.getLstFundOperationGroup())
				mpFundOpeGroup.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			//Tipo Operacion
			for(FundsOperationType fundOpeType:manageDepositServiceFacade.getFundsOperationType())
				mpOperationType.put(fundOpeType.getIdFundsOperationTypePk(), fundOpeType.getName());
			
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Fill initial list.
	 */
	private void fillInitialList(){
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();	
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			//State List				
			parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_FUND_OPERATION_STATE.getCode());
			searchDepositFundsTO.setLstState(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de grupo de operacion de fondos	
			parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_FUND_OPERATION_GROUP_RETIREMENT.getCode());
			List<ParameterTable> lstFundGroupOpeTemp = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
			for(ParameterTable paramTab:lstFundGroupOpeTemp){
				if(FundsOperationGroupType.GROUP_OPERATION_CENTRALACCOUNT.getCode().equals(paramTab.getParameterTablePk())){
					lstFundGroupOpeTemp.remove(paramTab);
					break;
				}
			}
			searchDepositFundsTO.setLstFundOperationGroup(lstFundGroupOpeTemp);
			parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_FUND_OPERATION_PROCESS_TYPE.getCode());
			searchDepositFundsTO.setLstProcessType(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			
		}
		catch (ServiceException e) {
			e.printStackTrace();
		    excepcion.fire(new ExceptionToCatchEvent(e));
		 }
	}
	
	/**
	 * Change search fund operation type.
	 */
	public void changeSearchFundOperationType(){
		try {
			searchDepositFundsTO.setBlIssuer(false);
			searchDepositFundsTO.setBlParticipant(false);
			searchDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(searchDepositFundsTO.getFundOperationGroupSelected(), false));
			if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(searchDepositFundsTO.getFundOperationGroupSelected())
					||FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode().equals(searchDepositFundsTO.getFundOperationGroupSelected()))
			{
				searchDepositFundsTO.setParticipantSelected(null);
				getParticipants(false);
				searchDepositFundsTO.setBlParticipant(true);
			}
			else if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(searchDepositFundsTO.getFundOperationGroupSelected()))
			{
				getIssuers();
				searchDepositFundsTO.setBlIssuer(true);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		cleanMechanismAndGroupModaity();
	}
	
	/**
	 * On change fund operation type.
	 */
	public void onChangeFundOperationType(){
		try {
			searchDepositFundsTO.setBlIssuer(false);
			searchDepositFundsTO.setBlParticipant(false);
			searchDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(searchDepositFundsTO.getFundOperationGroupSelected(), false));
			if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(searchDepositFundsTO.getFundOperationGroupSelected())
					||FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode().equals(searchDepositFundsTO.getFundOperationGroupSelected()))
			{
				getParticipants(false);
				searchDepositFundsTO.setBlParticipant(true);
			}
			else if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(searchDepositFundsTO.getFundOperationGroupSelected()))
			{
				getIssuers();
				searchDepositFundsTO.setBlIssuer(true);
			}
			else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				JSFUtilities.resetViewRoot();
				registerDepositFundsTO.setReturnsDepositTO(new ReturnsDeposiTO());
				ParameterTableTO paramTO = new ParameterTableTO();
				paramTO.setState(ParameterTableStateType.REGISTERED.getCode());
				paramTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				registerDepositFundsTO.getReturnsDepositTO().setLstCurrency(generalPametersFacade.getListParameterTableServiceBean(paramTO));
				registerDepositFundsTO.getReturnsDepositTO().setLstBank(manageDepositServiceFacade.getLstBank());
				registerDepositFundsTO.getReturnsDepositTO().setPayedDate(getCurrentSystemDate());
				registerDepositFundsTO.getReturnsDepositTO().setHolder(new Holder());
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		init();
	}
	
	/**
	 * Gets the participants.
	 *
	 * @param blParameter the bl parameter
	 * @return the participants
	 */
	public void getParticipants(boolean blParameter)
	{
		try {
			searchDepositFundsTO.setLstParticipants(monitorCashAccountFacade.getParticipants(blParameter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * Gets the issuers.
	 *
	 * @return the issuers
	 */
	public void getIssuers()
	{
		try{
			if (searchDepositFundsTO.getLstIssuers() == null) {
				searchDepositFundsTO.setLstIssuers(monitorCashAccountFacade.getIssuers());
			}
		}catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search deposits.
	 */
	@LoggerAuditWeb
	public void searchDeposits(){
		JSFUtilities.hideGeneralDialogues();
		try {
			if(validateDates()){
				fundOperation = null;
				List<FundsOperation> lstFundsOperationResult = manageDepositServiceFacade.searchDepositFundsOperation(searchDepositFundsTO);
				/*for(FundsOperation fundsOperation : lstFundsOperationResult){
					System.out.println("ID: " + fundsOperation.getIdFundsOperationPk());
					System.out.println("BANK: " + fundsOperation.getInstitutionBankAccount());
				}*/
				if(Validations.validateListIsNotNullAndNotEmpty(lstFundsOperationResult)){
					showPrivilegeButtons();
					lstFundsOperation = new FundsOperationDataModel(setDescriptions(lstFundsOperationResult));
					searchDepositFundsTO.setBlNoResult(false);

				}else{
					lstFundsOperation = null;
					searchDepositFundsTO.setBlNoResult(true);
				}
				if(searchDepositFundsTO.getOperationNumber()!=null && searchDepositFundsTO.getOperationNumber().equals(GeneralConstants.ZERO_VALUE_INTEGER))searchDepositFundsTO.setOperationNumber(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		JSFUtilities.hideGeneralDialogues();
		try{			
			if(Validations.validateIsNullOrEmpty(arrayFundOperation)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED));
				JSFUtilities.showValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(arrayFundOperation != null && arrayFundOperation.length > 0){
					if(arrayFundOperation.length == 1){
						for(FundsOperation objFundOperation : arrayFundOperation){
							fundOperation = objFundOperation;
							break;
						}
						if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode().equals(fundOperation.getOperationState())){							
							blTransfersDep = false;
							blConfirm =true;
							blReject = false;							
							fillView();
							return STR_VIEW_DEPOSIT_MAPPING;							 
						}else{
							Object[] bodyData = new Object[1];
							bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INVALID_CONFIRM, bodyData));
							JSFUtilities.showValidationDialog();
							return GeneralConstants.EMPTY_STRING;
						}
					} else {	
						operationsNums = new ArrayList<Long>();
						String strCertInvalid = validListFundOperationForState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
						if(Validations.validateIsNullOrEmpty(strCertInvalid)){
							blConfirm = true;
							blReject = false;
							blTransfersDep = false;
							String oper = foundsOperationsMessage(operationsNums);	        				
	        				String headerMessage = confirmHeaderMessage(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
	        				String bodyMessage = confirmbodyMessage(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode(), oper);	        				
	        				question(headerMessage,bodyMessage);
						} else {
							String messageBody = errorStateMessageAlert(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode(), strCertInvalid);
		    				alert(messageBody);
						}
						
					}
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED));
					JSFUtilities.showValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateTransferCentral(){
		JSFUtilities.hideGeneralDialogues();
		try{			
			if(Validations.validateIsNullOrEmpty(arrayFundOperation)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED));
				JSFUtilities.showValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(arrayFundOperation != null && arrayFundOperation.length > 0){
					if(arrayFundOperation.length == 1){
						for(FundsOperation objFundOperation : arrayFundOperation){
							fundOperation = objFundOperation;
							break;
						}
						if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode().equals(fundOperation.getOperationState())){							
							blConfirm = false;
							blTransfersDep = true;
							blReject = false;							
							fillView();
							return STR_VIEW_DEPOSIT_MAPPING;							 
						}else{
							Object[] bodyData = new Object[1];
							bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INVALID_TRANSFER, bodyData));
							JSFUtilities.showValidationDialog();
							return GeneralConstants.EMPTY_STRING;
						}
					} else {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INVALID_NUMBER_TRANSFER));
						JSFUtilities.showValidationDialog();
						return GeneralConstants.EMPTY_STRING;
					}
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED));
					JSFUtilities.showValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}
	}
	
	/**
	 * Question.
	 *
	 * @param headerMessage the header message
	 * @param bodyMessage the body message
	 */
	public void question(String headerMessage,String bodyMessage){
    	RequestContext.getCurrentInstance().update(":formPopUp:cnfIdCAskDialog");	
    	showMessageOnDialog(headerMessage,bodyMessage); 
    	JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Confirmbody message.
	 *
	 * @param state the state
	 * @param certs the certs
	 * @return the string
	 */
	public String confirmbodyMessage(Integer state, String certs){
		if(state.equals(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode())){
			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_CERTIFIACTE_MULTI,certs);			
		}else if(state.equals(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode())){
			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_REJECT_CERTIFIACTE_MULTI,certs);
		}
		return "";
	}
	
	/**
	 * Confirm header message.
	 *
	 * @param state the state
	 * @return the string
	 */
	public String confirmHeaderMessage(Integer state){
		if(state.equals(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode())){
			return PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);			
		} else if(state.equals(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode())){
			return PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT);
		}
		return "";
	}
	
	/**
	 * Error state message alert.
	 *
	 * @param actualState the actual state
	 * @param strNum the str num
	 * @return the string
	 */
	public String errorStateMessageAlert(Integer actualState, String strNum){
		if(actualState.equals(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode())){
			return  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CONFIRM,new Object[]{strNum});
		} else if(actualState.equals(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode())){
			return  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_REJECT,new Object[]{strNum});
		}
		return "";
	}
	
	/**
	 * Certificate number message.
	 *
	 * @param operations the operations
	 * @return the string
	 */
    private String foundsOperationsMessage(List<Long> operations){
		String certs = StringUtils.EMPTY;
		if(operations != null && operations.size() > 0){
			for(Long crt : operations){
				if(StringUtils.equalsIgnoreCase(StringUtils.EMPTY, certs)){
					certs = crt.toString();
				}else{
					certs = certs.concat(GeneralConstants.STR_COMMA.concat(crt.toString()));
				}
			}
		}
		return certs;
	}
	
	/**
     * Alert.
     *
     * @param bodyMessage the body message
     */
    public void alert(String bodyMessage){
		String headerMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
				 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				 GeneralConstants.LBL_HEADER_ALERT);		
		showMessageOnDialog(headerMessage,bodyMessage); 
    	JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}
	
	/**
	 * Valid list fund operation for state.
	 *
	 * @param actualState the actual state
	 * @return the string
	 */
	public String validListFundOperationForState(Integer actualState){
		String certValid=GeneralConstants.EMPTY_STRING;
    	for(FundsOperation objFundOperation : arrayFundOperation){
    		if(!objFundOperation.getOperationState().equals(actualState) ){
    			certValid += objFundOperation.getIdFundsOperationPk() + GeneralConstants.STR_COMMA_WITHOUT_SPACE;
    		} else {
    			operationsNums.add(objFundOperation.getIdFundsOperationPk().longValue());
    		}
    	}
    	if(certValid.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
    		certValid=certValid.substring(0,certValid.length()-1);
    	}			
		return certValid;
	}
	
	/**
	 * Fill view.
	 */
	private void fillView() {
		JSFUtilities.hideGeneralDialogues();
		try {
			fundOperationDetail = manageDepositServiceFacade.getFundOperationDetail(fundOperation.getIdFundsOperationPk());
			fundOperationDetail.setCurrencyDescription(fundOperation.getCurrencyDescription());
			registerDepositFundsTO = new RegisterDepositFundsTO();
			registerDepositFundsTO.setLstFundOperationGroup(searchDepositFundsTO.getLstFundOperationGroup());
			registerDepositFundsTO.setFundOperationGroupSelected(fundOperationDetail.getFundsOperationGroup());
			registerDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(registerDepositFundsTO.getFundOperationGroupSelected(), false));
			registerDepositFundsTO.setOperationTypeSelected(fundOperationDetail.getFundsOperationType());
			registerDepositFundsTO.setBankMovement(fundOperation.getBankMovement());
			for(FundsOperationType fundOperationType:registerDepositFundsTO.getLstOperationType()){
				registerDepositFundsTO.setAccountTypeDescription(generalPametersFacade.getParameterTableById(fundOperationType.getCashAccountType()).getParameterName());
				registerDepositFundsTO.setAccountTypeSelected(fundOperationType.getCashAccountType().longValue());
				break;
			}
			if(BooleanType.YES.getCode().equals(fundOperationDetail.getIndCentralized())){
				registerDepositFundsTO.setBlForCentralAccount(true);
				registerDepositFundsTO.setEdvDescription(InstitutionType.DEPOSITARY.getValue());
			}else
				showEntityRelation();
			
			if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getParticipant())){
				registerDepositFundsTO.setParticipantSelected(fundOperationDetail.getParticipant().getIdParticipantPk().intValue());
				registerDepositFundsTO.setParticipantDes(fundOperationDetail.getParticipant().getDisplayDescriptionCode());
			}
			if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getIssuer())){
				registerDepositFundsTO.setIssuer(new Issuer(fundOperationDetail.getIssuer().getIdIssuerPk()));
				registerDepositFundsTO.setIssuerDes(fundOperationDetail.getIssuer().getBusinessNameCode());
			}
			
			if(BooleanType.YES.getCode().equals(fundOperationDetail.getIndCentralized())){
				if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getObservation())){
					ParameterTableTO paramTO = new ParameterTableTO();
					paramTO.setParameterTablePk(fundOperationDetail.getObservation());
					registerDepositFundsTO.setComments(generalPametersFacade.getListParameterTableServiceBean(paramTO).get(0).getParameterName());
				}
				fillBankAccount();
			}else{			
				if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					registerDepositFundsTO.setBlSettlement(true);
					fillSettlementDeposit();
				}else if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					registerDepositFundsTO.setBlCorporativeProcess(true);
					fillBankAccount();
				}else if(FundsOperationGroupType.RATE_FUND_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					registerDepositFundsTO.setBlRates(true);
					if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode().equals(fundOperationDetail.getOperationState()))
						registerDepositFundsTO.setBlShowBankReference(true);
					fillBankAccount();
				}else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getIssuer())){
						if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getIssuer().getIdIssuerPk())) {
							registerDepositFundsTO.setBlParticipant(false);
							registerDepositFundsTO.setBlIssuer(true);
						}
					}
					if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getParticipant())) {
						if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getParticipant().getIdParticipantPk())) {
							registerDepositFundsTO.setBlParticipant(true);
							registerDepositFundsTO.setBlIssuer(false);
						}
					}
					registerDepositFundsTO.setBlReturns(true);
					registerDepositFundsTO.setBlCorporativeProcessReturns(true);
					fillBankAccount();
					//fillReturns();
				}else if(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					registerDepositFundsTO.setBlInternational(true);
					fillInternationalGroup();
				}else if(FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					registerDepositFundsTO.setBlGuarantees(true);
					fillBankAccount();
				}else if(FundsOperationGroupType.SPECIAL_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					registerDepositFundsTO.setBlSpecialPayments(true);
					fillBankAccount();
				}else if(FundsOperationGroupType.TAX_FUND_OPERATION.getCode().equals(fundOperationDetail.getFundsOperationGroup())){
					registerDepositFundsTO.setBlTaxation(true);
					if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode().equals(fundOperationDetail.getOperationState()))
						registerDepositFundsTO.setBlShowBankReference(true);
					fillBankAccount();
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Fill bank account.
	 */
	private void fillBankAccount(){
		try {
			CashAccountDetail cashAccounDetail = manageDepositServiceFacade.getCashAccountDetail(fundOperationDetail.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
			List<CashAccountDetail> lstTemp = new ArrayList<CashAccountDetail>();
			cashAccounDetail.getInstitutionCashAccount().setCurrencyTypeDescription(fundOperationDetail.getCurrencyDescription());
			cashAccounDetail.getInstitutionCashAccount().setDepositAmount(fundOperationDetail.getOperationAmount());
			lstTemp.add(cashAccounDetail);
			registerDepositFundsTO.setLstCashAccountDetail(new CashAccountDetailDataModel(lstTemp));
			registerDepositFundsTO.setBankReference(fundOperationDetail.getBankReference());
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Fill settlement deposit.
	 */
	private void fillSettlementDeposit(){
		try {
			registerDepositFundsTO.setSettlementGroupTO(new SettlementDepositTO());
			registerDepositFundsTO.getSettlementGroupTO().setLstNegotiationMechanism(manageDepositServiceFacade.getNegotitationMechanismForParticipant(registerDepositFundsTO.getParticipantSelected().longValue()));			
			if(SettlementSchemaType.NET.getCode().equals(fundOperationDetail.getInstitutionCashAccount().getSettlementSchema())){
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementNet(true);
				registerDepositFundsTO.getSettlementGroupTO().setSettlementSchema(PropertiesUtilities.getMessage("funds.deposit.settlementSchema.net"));
				registerDepositFundsTO.getSettlementGroupTO().setMechanismSelected(fundOperationDetail.getInstitutionCashAccount().getNegotiationMechanism().getIdNegotiationMechanismPk());
				registerDepositFundsTO.getSettlementGroupTO().setLstModalityGroup(manageDepositServiceFacade.getModalityGroupForNegoMechanism(registerDepositFundsTO.getSettlementGroupTO().getMechanismSelected()));
				registerDepositFundsTO.getSettlementGroupTO().setModaGroupSelected(fundOperationDetail.getInstitutionCashAccount().getModalityGroup().getIdModalityGroupPk());
				CashAccountDetail cashAccountDetail = manageDepositServiceFacade.getCashAccountDetail(fundOperationDetail.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
				List<CashAccountDetail> lstTemp = new ArrayList<CashAccountDetail>();
				cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(fundOperationDetail.getCurrencyDescription());
				cashAccountDetail.getInstitutionCashAccount().setDepositAmount(fundOperationDetail.getOperationAmount());
				lstTemp.add(cashAccountDetail);
				registerDepositFundsTO.getSettlementGroupTO().setLstCashAccountDetails(new CashAccountDetailDataModel(lstTemp));
			}else{
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementGross(true);
				registerDepositFundsTO.getSettlementGroupTO().setSettlementSchema(PropertiesUtilities.getMessage("funds.deposit.settlementSchema.gross"));				
				registerDepositFundsTO.getSettlementGroupTO().setMechanismSelected(fundOperationDetail.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk());
				registerDepositFundsTO.getSettlementGroupTO().setLstModalityGroup(manageDepositServiceFacade.getModalityGroupForNegoMechanism(registerDepositFundsTO.getSettlementGroupTO().getMechanismSelected()));
				registerDepositFundsTO.getSettlementGroupTO().setModaGroupSelected(manageDepositServiceFacade.getModaGroupByNegoMechaAndNegoModa(fundOperationDetail));
				registerDepositFundsTO.getSettlementGroupTO().setReferencePayment(fundOperationDetail.getMechanismOperation().getPaymentReference());
				registerDepositFundsTO.getSettlementGroupTO().setOperationNumber(fundOperationDetail.getMechanismOperation().getOperationNumber());
				registerDepositFundsTO.getSettlementGroupTO().setOperationDate(fundOperationDetail.getMechanismOperation().getOperationDate());
				registerDepositFundsTO.getSettlementGroupTO().setCurrencyDescription(fundOperationDetail.getCurrencyDescription());
				registerDepositFundsTO.getSettlementGroupTO().setLstNegotiationModality(manageDepositServiceFacade.getNegotiationModalityForModaGroup(registerDepositFundsTO.getSettlementGroupTO().getModaGroupSelected()));
				registerDepositFundsTO.getSettlementGroupTO().setModaGroupSelected(fundOperationDetail.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk());
				registerDepositFundsTO.getSettlementGroupTO().setLstOperationPart(getLstOperationPart());
				registerDepositFundsTO.getSettlementGroupTO().setOperationPartSelected(fundOperationDetail.getOperationPart());
				@SuppressWarnings("unchecked")
				SettlementOperation objSettlementOperation = (SettlementOperation) manageDepositServiceFacade.getSettlementOperationFunds(
								fundOperationDetail.getMechanismOperation().getIdMechanismOperationPk(), fundOperationDetail.getOperationPart());
				@SuppressWarnings("unchecked")
				List<ParticipantSettlement> lstParticipantSettlement=
						(List<ParticipantSettlement>)manageDepositServiceFacade.getParticipantSettlements(objSettlementOperation.getIdSettlementOperationPk(), 
																				ParticipantRoleType.BUY.getCode(), null);
				for(ParticipantSettlement partSettlement:lstParticipantSettlement){
					if(partSettlement.getParticipant().getIdParticipantPk().equals(fundOperationDetail.getParticipant().getIdParticipantPk())
							&& ParticipantRoleType.BUY.getCode().equals(partSettlement.getRole())
							&& fundOperationDetail.getOperationPart().equals(fundOperationDetail.getOperationPart())){
						registerDepositFundsTO.getSettlementGroupTO().setEffectiveAmount(partSettlement.getSettlementAmount());
						registerDepositFundsTO.getSettlementGroupTO().setDepositAmount(fundOperationDetail.getOperationAmount());
						if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode().equals(fundOperationDetail.getOperationState())){
							BigDecimal totalDeposit = BigDecimal.ZERO;
							if(Validations.validateIsNotNullAndNotEmpty(partSettlement.getDepositedAmount()))
								totalDeposit = partSettlement.getDepositedAmount().add(fundOperationDetail.getOperationAmount());								
							else
								totalDeposit = fundOperationDetail.getOperationAmount();
														 
							if(totalDeposit.compareTo(registerDepositFundsTO.getSettlementGroupTO().getEffectiveAmount()) >= 0){
								registerDepositFundsTO.getSettlementGroupTO().setExcedentAmount(totalDeposit.subtract(registerDepositFundsTO.getSettlementGroupTO().getEffectiveAmount()));
								registerDepositFundsTO.getSettlementGroupTO().setPendientAmount(BigDecimal.ZERO);
							}else{
								registerDepositFundsTO.getSettlementGroupTO().setPendientAmount(registerDepositFundsTO.getSettlementGroupTO().getEffectiveAmount().subtract(totalDeposit));
								registerDepositFundsTO.getSettlementGroupTO().setExcedentAmount(BigDecimal.ZERO);
							}
						}else{
							if(Validations.validateIsNotNullAndNotEmpty(partSettlement.getDepositedAmount())){
								BigDecimal depositAmount = partSettlement.getDepositedAmount();
								if(registerDepositFundsTO.getSettlementGroupTO().getEffectiveAmount().compareTo(depositAmount) == 1){//MONTO EFECTIVO MAYOR A LO DEPOSITADO
									registerDepositFundsTO.getSettlementGroupTO().setPendientAmount(registerDepositFundsTO.getSettlementGroupTO().getEffectiveAmount().subtract(depositAmount));
									registerDepositFundsTO.getSettlementGroupTO().setExcedentAmount(BigDecimal.ZERO);
								}else{
									registerDepositFundsTO.getSettlementGroupTO().setPendientAmount(BigDecimal.ZERO);
									registerDepositFundsTO.getSettlementGroupTO().setExcedentAmount(depositAmount.subtract(registerDepositFundsTO.getSettlementGroupTO().getEffectiveAmount()));
								}
							}else{	
								registerDepositFundsTO.getSettlementGroupTO().setPendientAmount(BigDecimal.ZERO);
								registerDepositFundsTO.getSettlementGroupTO().setExcedentAmount(BigDecimal.ZERO);
							}
						}
						break;
					}
				}				
				registerDepositFundsTO.getSettlementGroupTO().setDepositAmount(fundOperationDetail.getOperationAmount());
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Fill returns.
	 */
	private void fillReturns(){
		try {
			List<HolderFundsOperation> lstResult = manageDepositServiceFacade.getLstHolderFundsOperationByFundsOperation(fundOperationDetail.getIdFundsOperationPk());
			for(HolderFundsOperation holFundsOpe:lstResult){
				holFundsOpe.setCurrencyDescription(mpCurrency.get(holFundsOpe.getCurrency()));
				holFundsOpe.setBankDescription(fundOperationDetail.getBank().getDescription());
			}
			registerDepositFundsTO.setReturnsDepositTO(new ReturnsDeposiTO());
			registerDepositFundsTO.getReturnsDepositTO().setLstHolderFundsOperation(lstResult);
			registerDepositFundsTO.getReturnsDepositTO().setDepositAmount(fundOperationDetail.getOperationAmount());
			CashAccountDetail cashAccountDetail = manageDepositServiceFacade.getCashAccountDetail(fundOperationDetail.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
			List<CashAccountDetail> lstTemp = new ArrayList<CashAccountDetail>();
			cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(fundOperationDetail.getCurrencyDescription());
			lstTemp.add(cashAccountDetail);
			registerDepositFundsTO.getReturnsDepositTO().setLstCashAccountDetails(new CashAccountDetailDataModel(lstTemp));
			registerDepositFundsTO.getReturnsDepositTO().setReferencePayment(fundOperationDetail.getPaymentReference());
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Fill international group.
	 */
	private void fillInternationalGroup(){
		try {
			registerDepositFundsTO.setInternationalDepositTO(new InternationalDepositTO());
			CashAccountDetail cashAccountDetail = manageDepositServiceFacade.getCashAccountDetail(fundOperationDetail.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
			List<CashAccountDetail> lstTemp = new ArrayList<CashAccountDetail>();
			cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(fundOperationDetail.getCurrencyDescription());
			lstTemp.add(cashAccountDetail);
			registerDepositFundsTO.getInternationalDepositTO().setLstCashAccountDetails(new CashAccountDetailDataModel(lstTemp));	
			if(com.pradera.model.component.type.ParameterFundsOperationType.INTERNATIONAL_SECURITIES_RECEPTION_DEPOSITS.getCode().equals(registerDepositFundsTO.getOperationTypeSelected())){
				//PARA RECEPCION SIEMPRE ES SOLO 1 OPERACION INTERNACIONAL
				FundsInternationalOperation objFundsInternationalOperation= manageDepositServiceFacade.getInternationalOperationbyFunds(fundOperationDetail.getLstFundsInternationalOperation().get(0).getIdFundsInterOperPk());
				registerDepositFundsTO.getInternationalDepositTO().setBlReception(true);
				registerDepositFundsTO.getInternationalDepositTO().setOperationDate(objFundsInternationalOperation.getInternationalOperation().getOperationDate());
				registerDepositFundsTO.getInternationalDepositTO().setOperationNumber(objFundsInternationalOperation.getInternationalOperation().getOperationNumber());
				registerDepositFundsTO.getInternationalDepositTO().setInterDepoNegotiatorDescription(objFundsInternationalOperation.getInternationalOperation().getSettlementDepository().getDescription());
				registerDepositFundsTO.getInternationalDepositTO().setInterDepoLiquidatorDescription(objFundsInternationalOperation.getInternationalOperation().getTradeDepository().getDescription());
				registerDepositFundsTO.getInternationalDepositTO().setParticipantInterDescription(objFundsInternationalOperation.getInternationalOperation().getInternationalParticipant().getDescription());
				registerDepositFundsTO.getInternationalDepositTO().setIdIsinCodePk(objFundsInternationalOperation.getInternationalOperation().getSecurities().getIdSecurityCodePk());
				registerDepositFundsTO.getInternationalDepositTO().setSettlementDate(objFundsInternationalOperation.getInternationalOperation().getSettlementDate());
				registerDepositFundsTO.getInternationalDepositTO().setCurrencyDescription(fundOperationDetail.getCurrencyDescription());
				registerDepositFundsTO.getInternationalDepositTO().setEffectiveAmount(objFundsInternationalOperation.getInternationalOperation().getSettlementAmount());
				registerDepositFundsTO.getInternationalDepositTO().setDepositAmount(fundOperationDetail.getOperationAmount());
				if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode().equals(fundOperationDetail.getOperationState())){
					BigDecimal totalDeposit = BigDecimal.ZERO;
					if(Validations.validateIsNotNullAndNotEmpty(objFundsInternationalOperation.getInternationalOperation().getDepositAmount()))
						totalDeposit = objFundsInternationalOperation.getInternationalOperation().getDepositAmount().add(fundOperationDetail.getOperationAmount());								
					else
						totalDeposit = fundOperationDetail.getOperationAmount();
												 
					if(totalDeposit.compareTo(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount()) >= 0){
						registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(totalDeposit.subtract(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount()));
						registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(BigDecimal.ZERO);
					}else{
						registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount().subtract(totalDeposit));
						registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(BigDecimal.ZERO);
					}
				}else{
					if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getLstFundsInternationalOperation().get(0).getInternationalOperation().getDepositAmount())){
						BigDecimal depositAmount = fundOperationDetail.getLstFundsInternationalOperation().get(0).getInternationalOperation().getDepositAmount();
						if(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount().compareTo(depositAmount) == 1){//MONTO EFECTIVO MAYOR A LO DEPOSITADO
							registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount().subtract(depositAmount));
							registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(BigDecimal.ZERO);
						}else{
							registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(BigDecimal.ZERO);
							registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(depositAmount.subtract(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount()));
						}
					}else{	
						registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(BigDecimal.ZERO);
						registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(BigDecimal.ZERO);
					}
				}				
			}else if(com.pradera.model.component.type.ParameterFundsOperationType.INTERNATIONAL_SECURITIES_SEND_DEPOSITS.getCode().equals(registerDepositFundsTO.getOperationTypeSelected())){
				registerDepositFundsTO.getInternationalDepositTO().setBlSend(true);
				List<InternationalOperation> lstInterOpe = new ArrayList<InternationalOperation>();
				for(FundsInternationalOperation fundsIntOpe:fundOperationDetail.getLstFundsInternationalOperation()){
					lstInterOpe.add(fundsIntOpe.getInternationalOperation());
				}
				registerDepositFundsTO.getInternationalDepositTO().setLstInternationalOperation(lstInterOpe);
				BigDecimal totalAmount = BigDecimal.ZERO;
				for(InternationalOperation interOpe:registerDepositFundsTO.getInternationalDepositTO().getLstInternationalOperation())
					totalAmount = totalAmount.add(interOpe.getSettlementAmount());
				registerDepositFundsTO.getInternationalDepositTO().setDepositAmount(totalAmount);
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		JSFUtilities.hideGeneralDialogues();
		try{			
			if(Validations.validateIsNullOrEmpty(arrayFundOperation)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED));
				JSFUtilities.showValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(arrayFundOperation != null && arrayFundOperation.length > 0){
					if(arrayFundOperation.length == 1){
						for(FundsOperation objFundOperation : arrayFundOperation){
							fundOperation = objFundOperation;
							break;
						}
						if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode().equals(fundOperation.getOperationState())
								|| MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode().equals(fundOperation.getOperationState())){
							blConfirm = false;
							blReject = true;
							blTransfersDep = false;
							fillView();
							return STR_VIEW_DEPOSIT_MAPPING; 
						}else{
							Object[] bodyData = new Object[1];
							bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INVALID_REJECT, bodyData));
							JSFUtilities.showValidationDialog();
							return GeneralConstants.EMPTY_STRING;
						}
					} else {
						operationsNums = new ArrayList<Long>();
						String strCertInvalid = validListFundOperationForState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());						
						if(Validations.validateIsNullOrEmpty(strCertInvalid)){
							blConfirm = false;
							blReject = true;
							blTransfersDep = false;
							String oper = foundsOperationsMessage(operationsNums);	        				
	        				String headerMessage = confirmHeaderMessage(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode());
	        				String bodyMessage = confirmbodyMessage(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode(), oper);	        				
	        				question(headerMessage,bodyMessage);
						} else {
							String messageBody = errorStateMessageAlert(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode(), strCertInvalid);
		    				alert(messageBody);
						}
						
					}
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_RECORD_NOTSELECTED));
					JSFUtilities.showValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Validate modify.
	 *
	 * @return the string
	 */
	public String validateModify(){
		JSFUtilities.hideGeneralDialogues();
//		try{			
//			if(Validations.validateIsNullOrEmpty(fundOperation)){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						, PropertiesUtilities.getMessage("alt.funds.deposit.record.not.selected"));
//				JSFUtilities.showValidationDialog();
//				return GeneralConstants.EMPTY_STRING;
//			}else{
//				if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode().equals(fundOperation.getOperationState())){
//					fillModify();
//					return STR_MODIFY_DEPOSIT_MAPPING; 
//				}else{
//					Object[] bodyData = new Object[1];
//					bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getMessage("alt.funds.deposit.action.invalidModify", bodyData));
//					JSFUtilities.showValidationDialog();
//					return GeneralConstants.EMPTY_STRING;
//				}				
//			}	
//		}catch (Exception e) {
//			e.printStackTrace();
//		    excepcion.fire(new ExceptionToCatchEvent(e));
//		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Sets the descriptions.
	 *
	 * @param lstFundsOperationResult the lst funds operation result
	 * @return the list
	 */
	private List<FundsOperation> setDescriptions(List<FundsOperation> lstFundsOperationResult) {
		List<FundsOperation> lstResult = new ArrayList<FundsOperation>();
		for(FundsOperation fundOperation:lstFundsOperationResult){
			fundOperation.setFundsOpeGroupDescription(mpFundOpeGroup.get(fundOperation.getFundsOperationGroup()));
			fundOperation.setCurrencyDescription(mpCurrency.get(fundOperation.getCurrency()));
			fundOperation.setFundsOpeTypeDescription(mpOperationType.get(fundOperation.getFundsOperationType()));
			fundOperation.setOperationStateDescription(mpState.get(fundOperation.getOperationState()));
			lstResult.add(fundOperation);
		}
		return lstResult;
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnConfirmView(true);	
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnConfirmDepositary(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Validate dates.
	 *
	 * @return true, if successful
	 */
	private boolean validateDates(){
		Integer diferenceDays = CommonsUtilities.getDaysBetween(searchDepositFundsTO.getInitialDate(), searchDepositFundsTO.getEndDate());
		if(searchDepositFundsTO.getInitialDate().after(searchDepositFundsTO.getEndDate())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("alt.funds.deposit.search.initial.greaterThan"));
			JSFUtilities.showValidationDialog();
			searchDepositFundsTO.setInitialDate(searchDepositFundsTO.getEndDate());
			return false;
		}else if(diferenceDays > maxDaysOfCustodyRequest){
			Object[] bodyData = new Object[1];
			bodyData[0] = maxDaysOfCustodyRequest.toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("alt.funds.deposit.search.maxDays", bodyData));
			JSFUtilities.showValidationDialog();
			searchDepositFundsTO.setInitialDate(CommonsUtilities.addDate(searchDepositFundsTO.getEndDate(),-maxDaysOfCustodyRequest));
			return false;
		}
		return true;
	}
	
	/**
	 * Inits the register deposit.
	 *
	 * @return the string
	 */
	public String initRegisterDeposit()
	{
		try{
			registerDepositFundsTO = new RegisterDepositFundsTO();
			registerDepositFundsTO.setIssuer(new Issuer());
			registerDepositFundsTO.setLstFundOperationGroup(searchDepositFundsTO.getLstFundOperationGroup());
			listHolidays();
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			validateDefaultUserData();
			objInstitutionBankAccountsType=null;
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return "registerDepositFunds";
	}
	
	/**
	 * Validate default user data.
	 */
	public void validateDefaultUserData()
	{
		try{
				if(userInfo.getUserAccountSession().isParticipantInstitucion()){
					registerDepositFundsTO.setFundOperationGroupSelected(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
					onChangeFundOperationGroup();
					getParticipants(false);
					registerDepositFundsTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode().intValue());
					registerDepositFundsTO.setBlParticipant(true);
					registerDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(searchDepositFundsTO.getFundOperationGroupSelected(), false));
					onChangeParticipant();
				}
				else if(userInfo.getUserAccountSession().isIssuerInstitucion()){
					registerDepositFundsTO.setFundOperationGroupSelected(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
					onChangeFundOperationGroup();
					getIssuers();
					Issuer objIssuer=manageDepositServiceFacade.findIssuerFacade(userInfo.getUserAccountSession().getIssuerCode());
					registerDepositFundsTO.setIssuer(objIssuer);
					registerDepositFundsTO.setBlIssuer(true);
					registerDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(searchDepositFundsTO.getFundOperationGroupSelected(), false));
					onChangeIssuer();
				}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);		    
		allHolidays = CommonsUtilities.convertListDateToString(generalPametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * On change fund operation group.
	 */
	public void onChangeFundOperationGroup()
	{
		JSFUtilities.hideGeneralDialogues();
		try {			
			showEntityRelation();
			registerDepositFundsTO.setOperationTypeSelected(null);
			objInstitutionBankAccountsType=null;
			showDepositType(registerDepositFundsTO.getFundOperationGroupSelected());
			registerDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(registerDepositFundsTO.getFundOperationGroupSelected(), true));
			for(FundsOperationType fundOperationType:registerDepositFundsTO.getLstOperationType()){
				registerDepositFundsTO.setAccountTypeDescription(generalPametersFacade.getParameterTableById(fundOperationType.getCashAccountType()).getParameterName());
				registerDepositFundsTO.setAccountTypeSelected(fundOperationType.getCashAccountType().longValue());
				break;
			}
			registerDepositFundsTO.setSettlementGroupTO(null);
			registerDepositFundsTO.setCashAccountDetail(null);
			registerDepositFundsTO.setInternationalDepositTO(null);
			registerDepositFundsTO.setReturnsDepositTO(null);
			registerDepositFundsTO.setIssuer(new Issuer());
			if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				JSFUtilities.resetViewRoot();
				registerDepositFundsTO.setSettlementGroupTO(new SettlementDepositTO());
			}else if(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				JSFUtilities.resetViewRoot();
				registerDepositFundsTO.setInternationalDepositTO(new InternationalDepositTO());
				ParameterTableTO paramTO = new ParameterTableTO();
				paramTO.setState(ParameterTableStateType.REGISTERED.getCode());
				paramTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				registerDepositFundsTO.getInternationalDepositTO().setLstCurrency(generalPametersFacade.getListParameterTableServiceBean(paramTO));
				registerDepositFundsTO.getInternationalDepositTO().setLstInternationalDepositoryLiquidator(manageDepositServiceFacade.getLstInternationalDepositary());
				registerDepositFundsTO.getInternationalDepositTO().setLstInternationalDepositoryNegotiator(manageDepositServiceFacade.getLstInternationalDepositary());
			}else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				JSFUtilities.resetViewRoot();
				registerDepositFundsTO.setReturnsDepositTO(new ReturnsDeposiTO());
				ParameterTableTO paramTO = new ParameterTableTO();
				paramTO.setState(ParameterTableStateType.REGISTERED.getCode());
				paramTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				registerDepositFundsTO.getReturnsDepositTO().setLstCurrency(generalPametersFacade.getListParameterTableServiceBean(paramTO));
				registerDepositFundsTO.getReturnsDepositTO().setLstBank(manageDepositServiceFacade.getLstBank());
				registerDepositFundsTO.getReturnsDepositTO().setPayedDate(getCurrentSystemDate());
				registerDepositFundsTO.getReturnsDepositTO().setHolder(new Holder());
				registerDepositFundsTO.setLstParticipants(manageDepositServiceFacade.getParticipantsForDeposit(false));
				fillInstitutionBankAccountsTypeReturn();
				objInstitutionBankAccountsType=null;
			}else if(FundsOperationGroupType.RATE_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
					|| FundsOperationGroupType.TAX_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				registerDepositFundsTO.setCashAccountDetail(null);
				List<CashAccountDetail> lstCashAccountDetail = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccountDetail)){
					List<CashAccountDetail> lstCashAccountDetailTemp = new ArrayList<CashAccountDetail>();
					for(CashAccountDetail cashAccountDetail:lstCashAccountDetail){
						cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(mpCurrency.get(cashAccountDetail.getInstitutionCashAccount().getCurrency()));
						cashAccountDetail.getInstitutionCashAccount().setDepositAmount(null);
						lstCashAccountDetailTemp.add(cashAccountDetail);
					}
					registerDepositFundsTO.setLstCashAccountDetail(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
				}else{
					registerDepositFundsTO.setLstCashAccountDetail(null);
					if(FundsOperationGroupType.RATE_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected()))
						registerDepositFundsTO.setBlRates(false);					
					else if(FundsOperationGroupType.TAX_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected()))
						registerDepositFundsTO.setBlTaxation(false);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EDV_EFFECTIVE_ACCOUNT_NOTEXIST));
					JSFUtilities.showValidationDialog();					
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Show deposit type.
	 *
	 * @param fundOpeGroupSelected the fund ope group selected
	 */
	private void showDepositType(Integer fundOpeGroupSelected){
		registerDepositFundsTO.setBlSettlement(false);
		registerDepositFundsTO.setBlCorporativeProcess(false);
		registerDepositFundsTO.setBlCorporativeProcessReturns(false);
		registerDepositFundsTO.setBlRates(false);
		registerDepositFundsTO.setBlReturns(false);
		registerDepositFundsTO.setBlInternational(false);
		registerDepositFundsTO.setBlGuarantees(false);
		registerDepositFundsTO.setBlTaxation(false);
		if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(fundOpeGroupSelected)){
			registerDepositFundsTO.setBlSettlement(true);
		}else if(FundsOperationGroupType.RATE_FUND_OPERATION.getCode().equals(fundOpeGroupSelected)){
			registerDepositFundsTO.setBlRates(true);
		}else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(fundOpeGroupSelected)){
			registerDepositFundsTO.setBlReturns(true);
		}else if(FundsOperationGroupType.TAX_FUND_OPERATION.getCode().equals(fundOpeGroupSelected)){
			registerDepositFundsTO.setBlTaxation(true);
		}
	}
	
	/**
	 * Show entity relation.
	 */
	private void showEntityRelation(){
		try {			
			ParameterTable fundOpeGroupSelected = new ParameterTable();
			//Get the entity relation for the funds operation group
			for(ParameterTable paramTab:registerDepositFundsTO.getLstFundOperationGroup()){
				if(paramTab.getParameterTablePk().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
					fundOpeGroupSelected = paramTab;
					break;
				}
			}
			registerDepositFundsTO.setBlEDV(false);
			registerDepositFundsTO.setBlParticipant(false);
			registerDepositFundsTO.setBlIssuer(false);
			if(FundOpeGroupEntityRelationType.EDV.getCode().equals(new Integer(fundOpeGroupSelected.getIndicator2()))){
				registerDepositFundsTO.setBlEDV(true);
				registerDepositFundsTO.setInstitutionDes(InstitutionType.DEPOSITARY.getValue());
			}else if(FundOpeGroupEntityRelationType.PARTICIPANT.getCode().equals(new Integer(fundOpeGroupSelected.getIndicator2()))){
				registerDepositFundsTO.setBlParticipant(true);
				if(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected()))
					registerDepositFundsTO.setLstParticipants(manageDepositServiceFacade.getParticipantsIntDepositaryForDeposit());
				else{
					if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected()))
						registerDepositFundsTO.setLstParticipants(manageDepositServiceFacade.getParticipantsForDeposit(false));
					else
						registerDepositFundsTO.setLstParticipants(manageDepositServiceFacade.getParticipantsForDeposit(false));
				}
				registerDepositFundsTO.setParticipantSelected(null);
			}else if(FundOpeGroupEntityRelationType.ISSUER.getCode().equals(new Integer(fundOpeGroupSelected.getIndicator2()))){
				registerDepositFundsTO.setBlIssuer(true);
				registerDepositFundsTO.setLstIssuers(manageDepositServiceFacade.getIssuersForDeposit());
				registerDepositFundsTO.setIssuer(null);
			}
			if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())) {
				registerDepositFundsTO.setBlEDV(false);
				registerDepositFundsTO.setInstitutionDes(null);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * On change negotation mechanism.
	 */
	public void onChangeNegotationMechanism(){
		
		JSFUtilities.hideGeneralDialogues();
		try {
			//JSFUtilities.resetComponent("cboModaGroup");
			registerDepositFundsTO.getSettlementGroupTO().setModaGroupSelected(null);
			registerDepositFundsTO.getSettlementGroupTO().setLstModalityGroup(manageDepositServiceFacade.getModalityGroupForNegoMechanism(registerDepositFundsTO.getSettlementGroupTO().getMechanismSelected()));
			registerDepositFundsTO.getSettlementGroupTO().setBlSettlementNet(false);
			registerDepositFundsTO.getSettlementGroupTO().setBlSettlementGross(false);
			registerDepositFundsTO.getSettlementGroupTO().setSettlementSchema(null);
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * On change modality group.
	 */
	public void onChangeModalityGroup(){
		JSFUtilities.hideGeneralDialogues();
		try {
			registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForNumber(false);
			registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForReference(false);		
			for(ModalityGroup modGroup:registerDepositFundsTO.getSettlementGroupTO().getLstModalityGroup()){
				if(modGroup.getIdModalityGroupPk().equals(registerDepositFundsTO.getSettlementGroupTO().getModaGroupSelected())){
					registerDepositFundsTO.getSettlementGroupTO().setSettlementSchemaType(modGroup.getSettlementSchema());
					break;
				}
			}
			registerDepositFundsTO.getSettlementGroupTO().setIdSearchOpeForPkSelected(null);
			cleanOperationInfo();
			if(SettlementSchemaType.GROSS.getCode().equals(registerDepositFundsTO.getSettlementGroupTO().getSettlementSchemaType())){
				JSFUtilities.resetViewRoot();
				registerDepositFundsTO.getSettlementGroupTO().setSettlementSchema(PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_SETTLEMENT_SCHEMA_GROSS));
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementGross(true);
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementNet(false);	
				registerDepositFundsTO.getSettlementGroupTO().setBlCashPart(false);
				ParameterTableTO parameterTableTO = new ParameterTableTO();	
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				parameterTableTO.setMasterTableFk(MasterTableType.FUNDS_SEARCH_TYPE_OPERATION.getCode());
				registerDepositFundsTO.getSettlementGroupTO().setLstSearchOpeFor(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
				registerDepositFundsTO.getSettlementGroupTO().setLstNegotiationModality(manageDepositServiceFacade.getNegotiationModalityForModaGroup(registerDepositFundsTO.getSettlementGroupTO().getModaGroupSelected()));
				registerDepositFundsTO.getSettlementGroupTO().setLstOperationPart(getLstOperationPart());
				registerDepositFundsTO.getSettlementGroupTO().setIdSearchOpeForPkSelected(null);
			}else if(SettlementSchemaType.NET.getCode().equals(registerDepositFundsTO.getSettlementGroupTO().getSettlementSchemaType())){
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementNet(true);
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementGross(false);
				registerDepositFundsTO.getSettlementGroupTO().setSettlementSchema(PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTSCHEMA_NET));
				registerDepositFundsTO.getSettlementGroupTO().setCashAccountDetail(null);
				List<CashAccountDetail> lstResult = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
					List<CashAccountDetail> lstCashAccountDetailTemp = new ArrayList<CashAccountDetail>();
					
					for(CashAccountDetail cashAccountDetail:lstResult){

						cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(mpCurrency.get(cashAccountDetail.getInstitutionCashAccount().getCurrency()));
						cashAccountDetail.getInstitutionCashAccount().setDepositAmount(null);
						
						lstCashAccountDetailTemp.add(cashAccountDetail);
					}
					registerDepositFundsTO.getSettlementGroupTO().setLstCashAccountDetails(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
				}else{
					Object[] bodyData = new Object[1];
					bodyData[0] = registerDepositFundsTO.getParticipantSelected().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_PARTICIPANT_EFFECTIVEACCOUNT_NOTEXIST, bodyData));
					JSFUtilities.showValidationDialog();
					registerDepositFundsTO.getSettlementGroupTO().setLstCashAccountDetail(null);
				}
			}else{
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementNet(false);
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementGross(false);
				registerDepositFundsTO.getSettlementGroupTO().setSettlementSchema(null);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean operation info.
	 */
	private void cleanOperationInfo(){
		registerDepositFundsTO.getSettlementGroupTO().setReferencePayment(null);
		if(SearchOperationType.REFERENCE_PAYMENT.getCode().equals(registerDepositFundsTO.getSettlementGroupTO().getIdSearchOpeForPkSelected()))
			registerDepositFundsTO.getSettlementGroupTO().setOperationDate(null);
		else if(SearchOperationType.OPERATION_INFO.getCode().equals(registerDepositFundsTO.getSettlementGroupTO().getIdSearchOpeForPkSelected()))
			registerDepositFundsTO.getSettlementGroupTO().setOperationDate(getCurrentSystemDate());
		else
			registerDepositFundsTO.getSettlementGroupTO().setOperationDate(null);
		registerDepositFundsTO.getSettlementGroupTO().setOperationNumber(null);
		registerDepositFundsTO.getSettlementGroupTO().setNegoModalitySelected(null);
		registerDepositFundsTO.getSettlementGroupTO().setMechanismOperationPk(null);
		registerDepositFundsTO.getSettlementGroupTO().setCurrency(null);
		registerDepositFundsTO.getSettlementGroupTO().setCurrencyDescription(null);
		registerDepositFundsTO.getSettlementGroupTO().setEffectiveAmount(null);
		registerDepositFundsTO.getSettlementGroupTO().setPendientAmount(null);
		registerDepositFundsTO.getSettlementGroupTO().setExcedentAmount(null);
		registerDepositFundsTO.getSettlementGroupTO().setDepositAmount(null);
		registerDepositFundsTO.getSettlementGroupTO().setOperationPartSelected(OperationPartType.CASH_PART.getCode());
	}
	
	/**
	 * Gets the lst operation part.
	 *
	 * @return the lst operation part
	 */
	private List<ParameterTable> getLstOperationPart(){
		List<ParameterTable> lstOperationPart = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(OperationPartType.CASH_PART.getCode());
		paramTab.setParameterName(OperationPartType.CASH_PART.getDescription());
		lstOperationPart.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(OperationPartType.TERM_PART.getCode());
		paramTab.setParameterName(OperationPartType.TERM_PART.getDescription());
		lstOperationPart.add(paramTab);
		return lstOperationPart;
	}
	
	/**
	 * On change search operation type.
	 */
	public void onChangeSearchOperationType()
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			JSFUtilities.resetViewRoot();
			cleanOperationInfo();
			registerDepositFundsTO.getSettlementGroupTO().setBlCashPart(false);
			if(SearchOperationType.REFERENCE_PAYMENT.getCode().equals(registerDepositFundsTO.getSettlementGroupTO().getIdSearchOpeForPkSelected())){
				registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForReference(true);
				registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForNumber(false);
			}else if(SearchOperationType.OPERATION_INFO.getCode().equals(registerDepositFundsTO.getSettlementGroupTO().getIdSearchOpeForPkSelected())){				
				registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForNumber(true);
				registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForReference(false);
			}else{
				registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForNumber(false);
				registerDepositFundsTO.getSettlementGroupTO().setBlFindOpeForReference(false);				
			}
		}catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Search operation.
	 */
	public void searchOperation()
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			boolean blFound = manageDepositServiceFacade.searchOperation(registerDepositFundsTO.getParticipantSelected().longValue(), registerDepositFundsTO.getSettlementGroupTO());
			if(!blFound){
				if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getSettlementGroupTO().getErrorFound()))
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_OPERATION_NOTEXIST));
				else
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(registerDepositFundsTO.getSettlementGroupTO().getErrorFound()));
				JSFUtilities.showValidationDialog();
				registerDepositFundsTO.getSettlementGroupTO().setMechanismOperationPk(null);
			}else{
				List<CashAccountDetail> lstInstiBankAcc = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstInstiBankAcc))
				{	registerDepositFundsTO.getSettlementGroupTO().setIdInstitutionCashAccountPk(
							lstInstiBankAcc.get(0).getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					registerDepositFundsTO.getSettlementGroupTO().setCashAccountDetail(lstInstiBankAcc.get(0));
				}
				else{
					Object[] bodyData = new Object[1];
					bodyData[0] = registerDepositFundsTO.getParticipantSelected().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_PARTICIPANT_EFFECTIVEACCOUNT_NOTEXIST, bodyData));
					JSFUtilities.showValidationDialog();
					registerDepositFundsTO.getSettlementGroupTO().setMechanismOperationPk(null);
					registerDepositFundsTO.getSettlementGroupTO().setSettlementOperationPk(null);
					registerDepositFundsTO.getSettlementGroupTO().setIdInstitutionCashAccountPk(null);
				}
			}			
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * New search.
	 */
	public void newSearch(){
		JSFUtilities.resetViewRoot();
		registerDepositFundsTO.getSettlementGroupTO().setBlCashPart(false);
		cleanOperationInfo();
		
	}
	
	/**
	 * On change negotiation modality.
	 */
	public void onChangeNegotiationModality(){
		
	}
	
	/**
	 * Calculate amounts.
	 */
	public void calculateAmounts(){
		
	}
	
	/**
	 * Reset amounts on grill.
	 */
	public void resetAmount(){
		
		CashAccountDetailDataModel test = registerDepositFundsTO.getSettlementGroupTO().getLstCashAccountDetails();
		for (CashAccountDetail test2 : test){
			test2.getInstitutionCashAccount().setDepositAmount(null);
		}
	}
	
	/**
	 * On change participant.
	 */
	public void onChangeParticipant()
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				JSFUtilities.resetViewRoot();
				registerDepositFundsTO.getSettlementGroupTO().setLstNegotiationMechanism(manageDepositServiceFacade.getNegotitationMechanismForParticipant(registerDepositFundsTO.getParticipantSelected().longValue()));
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementNet(false);
				registerDepositFundsTO.getSettlementGroupTO().setBlSettlementGross(false);
				registerDepositFundsTO.getSettlementGroupTO().setSettlementSchema(null);
				registerDepositFundsTO.getSettlementGroupTO().setLstModalityGroup(null);
				registerDepositFundsTO.getSettlementGroupTO().setMechanismSelected(null);
				registerDepositFundsTO.getSettlementGroupTO().setNegoModalitySelected(null);
				registerDepositFundsTO.getSettlementGroupTO().setModaGroupSelected(null);
				registerDepositFundsTO.getSettlementGroupTO().setInstBankAccountSelected(null);
			}else if(FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				registerDepositFundsTO.setCashAccountDetail(null);
				registerDepositFundsTO.setBlGuarantees(true);
				List<CashAccountDetail> lstCashAccountDetail = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccountDetail)){
					List<CashAccountDetail> lstCashAccountDetailTemp = new ArrayList<CashAccountDetail>();
					for(CashAccountDetail cashAccountDetail:lstCashAccountDetail){
						cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(mpCurrency.get(cashAccountDetail.getInstitutionCashAccount().getCurrency()));
						cashAccountDetail.getInstitutionCashAccount().setDepositAmount(null);
						lstCashAccountDetailTemp.add(cashAccountDetail);
					}
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
						registerDepositFundsTO.setLstCashAccountDetail(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
					else
						registerDepositFundsTO.setLstCashAccountDetailModify(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
				}else{
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
						registerDepositFundsTO.setLstCashAccountDetail(null);
					else
						registerDepositFundsTO.setLstCashAccountDetailModify(null);
					registerDepositFundsTO.setBlGuarantees(false);
					Object[] bodyData = new Object[1];
					bodyData[0] = registerDepositFundsTO.getParticipantSelected().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_PARTICIPANT_EFFECTIVEACCOUNT_NOTEXIST, bodyData));
					JSFUtilities.showValidationDialog();					
				}
			}
			else if(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				JSFUtilities.resetViewRoot();
				registerDepositFundsTO.getInternationalDepositTO().setLstCashAccountDetails(null);
				registerDepositFundsTO.getInternationalDepositTO().setCashAccountDetail(null);
				cleanInternationalOperationInfo();
				cleanInternationalOperationSearch();
				if(com.pradera.model.component.type.ParameterFundsOperationType.INTERNATIONAL_SECURITIES_RECEPTION_DEPOSITS.getCode().equals(registerDepositFundsTO.getOperationTypeSelected())){
					registerDepositFundsTO.setBlInternational(true);
					registerDepositFundsTO.getInternationalDepositTO().setBlReception(true);
					registerDepositFundsTO.getInternationalDepositTO().setBlSend(false);
				}else if(com.pradera.model.component.type.ParameterFundsOperationType.INTERNATIONAL_SECURITIES_SEND_DEPOSITS.getCode().equals(registerDepositFundsTO.getOperationTypeSelected())){
					registerDepositFundsTO.setBlInternational(true);
					registerDepositFundsTO.getInternationalDepositTO().setBlSend(true);
					registerDepositFundsTO.getInternationalDepositTO().setBlReception(false);					
				}else{
					registerDepositFundsTO.setBlInternational(false);
					registerDepositFundsTO.getInternationalDepositTO().setBlSend(false);
					registerDepositFundsTO.getInternationalDepositTO().setBlReception(false);	
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean international operation info.
	 */
	private void cleanInternationalOperationInfo(){		
		registerDepositFundsTO.getInternationalDepositTO().setOperationDate(getCurrentSystemDate());
		registerDepositFundsTO.getInternationalDepositTO().setOperationNumber(null);
		registerDepositFundsTO.getInternationalDepositTO().setDepositAmount(null);
		registerDepositFundsTO.getInternationalDepositTO().setEffectiveAmount(null);
		registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(null);
		registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(null);
		registerDepositFundsTO.getInternationalDepositTO().setParticipantInterDescription(null);
		registerDepositFundsTO.getInternationalDepositTO().setIdIsinCodePk(null);
		registerDepositFundsTO.getInternationalDepositTO().setSettlementDate(null);
		registerDepositFundsTO.getInternationalDepositTO().setInterDepoLiquidatorDescription(null);
		registerDepositFundsTO.getInternationalDepositTO().setInterDepoNegotiatorDescription(null);
		registerDepositFundsTO.getInternationalDepositTO().setIdInternationalOperationPk(null);
		registerDepositFundsTO.getInternationalDepositTO().setCurrencyDescription(null);
		registerDepositFundsTO.getInternationalDepositTO().setCurrency(null);
	}
	
	/**
	 * Clean international operation search.
	 */
	private void cleanInternationalOperationSearch(){
		registerDepositFundsTO.getInternationalDepositTO().setIdIntDepoLiquiPkSelected(null);
		registerDepositFundsTO.getInternationalDepositTO().setIdIntDepoNegoPkSelected(null);
		registerDepositFundsTO.getInternationalDepositTO().setCurrencySelected(null);
		registerDepositFundsTO.getInternationalDepositTO().setSettlementInitial(getCurrentSystemDate());
		registerDepositFundsTO.getInternationalDepositTO().setSettlementEnd(getCurrentSystemDate());
		registerDepositFundsTO.getInternationalDepositTO().setRegistryInitial(getCurrentSystemDate());
		registerDepositFundsTO.getInternationalDepositTO().setRegistryEnd(getCurrentSystemDate());
		registerDepositFundsTO.getInternationalDepositTO().setLstInternationalOperation(null);
	}
	
	/**
	 * Before save deposit.
	 */
	public void beforeSaveDeposit(){
		JSFUtilities.hideGeneralDialogues();
		try {
			if(!Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getBankMovement())) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATION_NOT_BANK_MOVEMENT));
				JSFUtilities.showValidationDialog();
				return;
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(validateSave())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(validateSave()));
				JSFUtilities.showValidationDialog();
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage("alt.funds.deposit.ask.register"));
				JSFUtilities.showComponent("frmRegisterDeposit:cnfIdCAskDialog");
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate save.
	 *
	 * @return the string
	 */
	private String validateSave(){
		if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
			if(registerDepositFundsTO.getSettlementGroupTO().isBlSettlementNet()){
//				if(registerDepositFundsTO.getSettlementGroupTO().getCashAccountDetail().getInstitutionCashAccount().getIndSettlementProcess().equals(GeneralConstants.ONE_VALUE_INTEGER))
//					return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENT_IN_PROCESS;
				if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getSettlementGroupTO().getLstCashAccountDetails()))
					return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED;
				else{
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getSettlementGroupTO().getCashAccountDetail()))
						return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED;
					else{
						if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getSettlementGroupTO().getCashAccountDetail().getInstitutionCashAccount().getDepositAmount()))
							return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EMPTY_AMOUNT;
						else
							return GeneralConstants.EMPTY_STRING;
					}
				}
			}else if(registerDepositFundsTO.getSettlementGroupTO().isBlSettlementGross()){
				if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getSettlementGroupTO().getMechanismOperationPk()))
					return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EMPTY_AMOUNT;
				else if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getSettlementGroupTO().getErrorFound()))
					return registerDepositFundsTO.getSettlementGroupTO().getErrorFound();
				else
					return GeneralConstants.EMPTY_STRING;
			}else
				return GeneralConstants.EMPTY_STRING;
		}
		else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
//			if(Validations.validateListIsNullOrEmpty(registerDepositFundsTO.getReturnsDepositTO().getLstHolderFundsOperation())){
//				return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_HOLDERCASHMOVEMENT_EMPTY;
//			}else if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getReturnsDepositTO().getCashAccountDetail())){
//				return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED;
//			}else if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getReturnsDepositTO().getDepositAmount())){
//				return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_HOLDERCASHMOVEMENT_NOT_SELECTED;
//			}else
//				return GeneralConstants.EMPTY_STRING;
		} else if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.RATE_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.TAX_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.SPECIAL_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
			if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getLstCashAccountDetail())
					&& Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
				return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED;
			else if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getLstCashAccountDetailModify())
					&& Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
				return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED;
			else{
				if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getCashAccountDetail()))
					return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED;
				else{
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().getDepositAmount()))
						return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EMPTY_AMOUNT;
					else if(registerDepositFundsTO.getCashAccountDetail().getInstitutionCashAccount().getDepositAmount().intValue()==GeneralConstants.ZERO_VALUE_BIGDECIMAL.intValue())
						return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_GREATERTHANZERO_AMOUNT;
					else
						return GeneralConstants.EMPTY_STRING;
				}
			}
		}
		else if(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
			if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getInternationalDepositTO().getCashAccountDetail()))
				return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EFFECTIVEACCOUNT_NOT_SELECTED;
			else if(registerDepositFundsTO.getInternationalDepositTO().isBlReception()){
				if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getInternationalDepositTO().getIdInternationalOperationPk()))
					return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_EMPTY;
				else
					return GeneralConstants.EMPTY_STRING;				
			}else if(registerDepositFundsTO.getInternationalDepositTO().isBlSend()){
				if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getInternationalDepositTO().getDepositAmount()))
					return PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_NOTSELECTED;
			}else 
				return GeneralConstants.EMPTY_STRING;
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Save deposit.
	 */
	@LoggerAuditWeb
	public void saveDeposit(){
		JSFUtilities.hideGeneralDialogues();
		hideOwnDialogs();
		try {
			Long idFundOperationPk = manageDepositServiceFacade.saveDeposit(registerDepositFundsTO);
			if(Validations.validateIsNotNullAndNotEmpty(idFundOperationPk)){
				Object[] bodyData = new Object[3];
				bodyData[0] = idFundOperationPk.toString();
				bodyData[1] = registerDepositFundsTO.getAccountTypeDescription();
				bodyData[2]=institutionDescription();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SUCESS_REGISTER, bodyData));
				JSFUtilities.showComponent("frmRegisterDeposit:cnfIdEndDialog");
				// Start Notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,idFundOperationPk.toString(), bodyData);
				// End Notification
				init();
			}
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));	
			JSFUtilities.showComponent("frmRegisterDeposit:cnfIdEndDialog");
		}
	}
	
	/**
	 * Institution description.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String institutionDescription() throws ServiceException
	{
		String institutionDes=null;
		if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected()))
		{
			Participant objParticipant=new Participant();
			objParticipant.setIdParticipantPk(registerDepositFundsTO.getParticipantSelected().longValue());
			institutionDes=participantServiceBean.findParticipantByFiltersServiceBean(objParticipant).getDescription();
		}else if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected()))
		{
			institutionDes=issuerQueryServiceBean.findIssuer(registerDepositFundsTO.getIssuer().getIdIssuerPk(),null).getBusinessName();
		}
		else if(FundsOperationGroupType.RATE_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
				|| FundsOperationGroupType.TAX_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected()))
			institutionDes=registerDepositFundsTO.getInstitutionDes();
		else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())) {
			if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getParticipantSelected())){
				Participant objParticipant=new Participant();
				objParticipant.setIdParticipantPk(registerDepositFundsTO.getParticipantSelected().longValue());
				institutionDes=participantServiceBean.findParticipantByFiltersServiceBean(objParticipant).getDescription();
			}
			if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getIssuer())){
				if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getIssuer().getIdIssuerPk())) {
					institutionDes=issuerQueryServiceBean.findIssuer(registerDepositFundsTO.getIssuer().getIdIssuerPk(),null).getBusinessName();
				}
			}
		}
		return institutionDes;
	}
	
	/**
	 * Search issuer handler.
	 */
	public void searchIssuerHandler(){
		executeAction();	
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getIssuer()) 
				&& Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getIssuer().getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageDepositServiceFacade.findIssuerFacade(searchDepositFundsTO.getIssuer().getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			searchDepositFundsTO.setIssuer(issuerTemp);
		}
		
	}
	
	/**
	 * Search issuer handler register.
	 */
	public void searchIssuerHandlerRegister(){
		executeAction();	
		if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getIssuer()) 
				&& Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getIssuer().getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = manageDepositServiceFacade.findIssuerFacade(registerDepositFundsTO.getIssuer().getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			registerDepositFundsTO.setIssuer(issuerTemp);
			onChangeIssuer();
		}
	}
	
	/**
	 * On change issuer.
	 */
	public void onChangeIssuer()
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())
					||FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())) {
					registerDepositFundsTO.setBlCorporativeProcess(false);
					registerDepositFundsTO.setBlCorporativeProcessReturns(true);
					registerDepositFundsTO.setBlIssuer(true);
					registerDepositFundsTO.setBlParticipant(false);
				}else
					registerDepositFundsTO.setBlCorporativeProcess(true);
				List<CashAccountDetail> lstCashAccountDetail = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
				registerDepositFundsTO.setCashAccountDetail(null);
				if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccountDetail)){
					List<CashAccountDetail> lstCashAccountDetailTemp = new ArrayList<CashAccountDetail>();
					for(CashAccountDetail cashAccountDetail:lstCashAccountDetail){
						cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(mpCurrency.get(cashAccountDetail.getInstitutionCashAccount().getCurrency()));
						cashAccountDetail.getInstitutionCashAccount().setDepositAmount(null);
						lstCashAccountDetailTemp.add(cashAccountDetail);
					}
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
						registerDepositFundsTO.setLstCashAccountDetail(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
					else
						registerDepositFundsTO.setLstCashAccountDetailModify(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
				}else{
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
						registerDepositFundsTO.setLstCashAccountDetail(null);
					else
						registerDepositFundsTO.setLstCashAccountDetailModify(null);
					registerDepositFundsTO.setBlCorporativeProcess(false);
					registerDepositFundsTO.setBlCorporativeProcessReturns(false);
					
					if(userInfo.getUserAccountSession().isIssuerInstitucion())
					{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_ISSUER_EFFECTIVEACCOUNT_NOTEXIST,
										new Object[]{registerDepositFundsTO.getIssuer().getIdIssuerPk()}) );
						JSFUtilities.showValidationDialog();
					}
					else
					{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_ISSUER_EFFECTIVEACCOUNT_NOTEXIST,
							    		  new Object[]{registerDepositFundsTO.getIssuer().getIdIssuerPk()}) );						
						registerDepositFundsTO.setIssuer(new Issuer());
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	
	}
	
	/**
	 * On change issuer.
	 */
	public void onChangeParticipantReturn()
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				registerDepositFundsTO.setBlCorporativeProcess(false);
				registerDepositFundsTO.setBlCorporativeProcessReturns(true);
				registerDepositFundsTO.setBlParticipant(true);
				registerDepositFundsTO.setBlIssuer(false);
				List<CashAccountDetail> lstCashAccountDetail = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
				registerDepositFundsTO.setCashAccountDetail(null);
				if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccountDetail)){
					List<CashAccountDetail> lstCashAccountDetailTemp = new ArrayList<CashAccountDetail>();
					for(CashAccountDetail cashAccountDetail:lstCashAccountDetail){
						cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(mpCurrency.get(cashAccountDetail.getInstitutionCashAccount().getCurrency()));
						cashAccountDetail.getInstitutionCashAccount().setDepositAmount(null);
						lstCashAccountDetailTemp.add(cashAccountDetail);
					}
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
						registerDepositFundsTO.setLstCashAccountDetail(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
					else
						registerDepositFundsTO.setLstCashAccountDetailModify(new CashAccountDetailDataModel(lstCashAccountDetailTemp));
				}else{
					if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getIdFundsOperationMirror()))
						registerDepositFundsTO.setLstCashAccountDetail(null);
					else
						registerDepositFundsTO.setLstCashAccountDetailModify(null);
					registerDepositFundsTO.setBlCorporativeProcess(false);
					registerDepositFundsTO.setBlCorporativeProcessReturns(false);
					
					if(userInfo.getUserAccountSession().isParticipantInstitucion())
					{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_ISSUER_EFFECTIVEACCOUNT_NOTEXIST,
										new Object[]{registerDepositFundsTO.getParticipantSelected()}) );
						JSFUtilities.showValidationDialog();
					}
					else
					{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_ISSUER_EFFECTIVEACCOUNT_NOTEXIST,
							    		  new Object[]{registerDepositFundsTO.getParticipantSelected()}) );						
						registerDepositFundsTO.setIssuer(new Issuer());
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	
	}
	
	public void onChangeInstitutionType(){
		JSFUtilities.hideGeneralDialogues();
		registerDepositFundsTO.setLstCashAccountDetail(null);
		registerDepositFundsTO.setParticipantSelected(null);
		registerDepositFundsTO.setIssuer(new Issuer());
		registerDepositFundsTO.setBlCorporativeProcessReturns(false);
	}
	
	/**
	 * On change operation type.
	 */
	public void onChangeOperationType(){
		
		JSFUtilities.hideGeneralDialogues();
		try {
			if(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode().equals(registerDepositFundsTO.getFundOperationGroupSelected())){
				//if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getParticipantSelected())){
					JSFUtilities.resetViewRoot();
					registerDepositFundsTO.getInternationalDepositTO().setLstCashAccountDetails(null);
					registerDepositFundsTO.getInternationalDepositTO().setCashAccountDetail(null);
					cleanInternationalOperationInfo();
					cleanInternationalOperationSearch();
					if(com.pradera.model.component.type.ParameterFundsOperationType.INTERNATIONAL_SECURITIES_RECEPTION_DEPOSITS.getCode().equals(registerDepositFundsTO.getOperationTypeSelected())){
						registerDepositFundsTO.setBlInternational(true);						
						registerDepositFundsTO.getInternationalDepositTO().setBlReception(true);
						registerDepositFundsTO.getInternationalDepositTO().setBlSend(false);						
					}else if(com.pradera.model.component.type.ParameterFundsOperationType.INTERNATIONAL_SECURITIES_SEND_DEPOSITS.getCode().equals(registerDepositFundsTO.getOperationTypeSelected())){
						registerDepositFundsTO.setBlInternational(true);
						registerDepositFundsTO.getInternationalDepositTO().setBlSend(true);
						registerDepositFundsTO.getInternationalDepositTO().setBlReception(false);
					}else{
						registerDepositFundsTO.setBlInternational(false);
						registerDepositFundsTO.getInternationalDepositTO().setBlSend(false);
						registerDepositFundsTO.getInternationalDepositTO().setBlReception(false);	
					}					
//				}else{
//					registerDepositFundsTO.setBlInternational(false);
//					registerDepositFundsTO.getInternationalDepositTO().setBlSend(false);
//					registerDepositFundsTO.getInternationalDepositTO().setBlReception(false);
//				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Ask for clean.
	 *
	 * @return the string
	 */
	public String askForClean(){
		JSFUtilities.resetViewRoot();
		if(validateRequiredFields()){
			return registerDeposit();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.clean"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwCleanDialog').show()");
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Register deposit.
	 *
	 * @return the string
	 */
	public String registerDeposit(){
		try{
			registerDepositFundsTO = new RegisterDepositFundsTO();
			registerDepositFundsTO.setLstFundOperationGroup(searchDepositFundsTO.getLstFundOperationGroup());
			listHolidays();
			return STR_NEW_DEPOSIT_MAPPING;
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Ask for back.
	 *
	 * @return the string
	 */
	public String askForBack(){
		if(validateRequiredFields()){
			return backFromRegister();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.back"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwBackDialog').show()");
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipant(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(objInstitutionBankAccountsType));
	}	
	
	/**
	 * Is Institution Type Issuer.
	 *
	 * @return validate Institution Type Issuer
	 */
	public boolean isIndInstitutionTypeIssuer(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.ISSUER.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Is Institution Type Bank.
	 *
	 * @return validate Institution Type Bank
	 */
	public boolean isIndInstitutionTypeBank(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.BANK.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Fill institution bank accounts type.
	 */
	public void fillInstitutionBankAccountsTypeReturn(){
		lstInstitutionBankAccountsTypeReturn = new ArrayList<ParameterTable>();
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setIndicator4(GeneralConstants.ONE_VALUE_INTEGER);
		try {
			lstInstitutionBankAccountsTypeReturn.addAll(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
	}

	/**
	 * Validate required fields.
	 *
	 * @return true, if successful
	 */
	private boolean validateRequiredFields(){
		if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getFundOperationGroupSelected()) &&
			Validations.validateIsNullOrEmpty(registerDepositFundsTO.getOperationTypeSelected()))
			return true;
		else
			return false;
	}
	
	/**
	 * Hide own dialogs.
	 */
	public void hideOwnDialogs(){
		JSFUtilities.hideComponent("frmRegisterDeposit:cnfIdCAskDialog");
		JSFUtilities.hideComponent("frmRegisterDeposit:cnfWEndDialog");
	}
	
	/**
	 * Hide own dialogs confirm.
	 */
	public void hideOwnDialogsConfirm(){
		//JSFUtilities.hideComponent(":formPopUp:alterValidation");
		//JSFUtilities.hideComponent(":formPopUp:cnfIdCAskDialog");
	}
	
	/**
	 * Search list holder account.
	 */
	public void searchListHolderAccount(){

		  if (Validations.validateIsNull(registerDepositFundsTO.getReturnsDepositTO().getHolder())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_CUI_NOTFOUND) );		   
			  registerDepositFundsTO.getReturnsDepositTO().setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if(Validations.validateIsNull(registerDepositFundsTO.getReturnsDepositTO().getHolder().getStateHolder())){
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_CUI_NOTFOUND) );		   
			  registerDepositFundsTO.getReturnsDepositTO().setHolder(new Holder());
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  if (!registerDepositFundsTO.getReturnsDepositTO().getHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_HOLDER_NOTREGISTER) );			  	   
			  registerDepositFundsTO.getReturnsDepositTO().setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  registerDepositFundsTO.getReturnsDepositTO().setHolder(registerDepositFundsTO.getReturnsDepositTO().getHolder());
	}
	
	/**
	 * Search holder cash movements.
	 */
	public void searchHolderCashMovements(){
		JSFUtilities.hideGeneralDialogues();
		try {
			if(Validations.validateIsNull(registerDepositFundsTO.getReturnsDepositTO().getHolder().getIdHolderPk())){
				  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_RETURNS_CUITITULAR_MANDATORY) );		   
				  registerDepositFundsTO.getReturnsDepositTO().setHolder(new Holder());
				   JSFUtilities.showSimpleValidationDialog();
				   return;
			  }
			registerDepositFundsTO.getReturnsDepositTO().setLstCashAccountDetails(null);
			registerDepositFundsTO.getReturnsDepositTO().setLstHolderFundsOperation(null);
			registerDepositFundsTO.getReturnsDepositTO().setCashAccountDetail(null);
			List<HolderFundsOperation> lstResult = manageDepositServiceFacade.getLstHolderFundsOperation(registerDepositFundsTO.getReturnsDepositTO());
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				List<CashAccountDetail> lstCashAccountDetail = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
				if(Validations.validateListIsNullOrEmpty(lstCashAccountDetail)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_EDV_EFFECTIVE_ACCOUNT_NOTEXIST));
					JSFUtilities.showValidationDialog();
				}else{
					for(CashAccountDetail cashAccountDetail:lstCashAccountDetail){
						cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(mpCurrency.get(cashAccountDetail.getInstitutionCashAccount().getCurrency()));
					}
					for(HolderFundsOperation holFundsOperation:lstResult){
						holFundsOperation.setCurrencyDescription(mpCurrency.get(holFundsOperation.getCurrency()));
					}
					registerDepositFundsTO.getReturnsDepositTO().setLstCashAccountDetails(new CashAccountDetailDataModel(lstCashAccountDetail));
					registerDepositFundsTO.getReturnsDepositTO().setLstHolderFundsOperation(lstResult);
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_ERROR_HOLDERCASHMOVEMENT));
				JSFUtilities.showValidationDialog();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Select holder funds ope.
	 *
	 * @param holdeFundsOperation the holde funds operation
	 */
	public void selectHolderFundsOpe(HolderFundsOperation holdeFundsOperation){
		if(holdeFundsOperation.isSelected()){
			BigDecimal depositAmount = registerDepositFundsTO.getReturnsDepositTO().getDepositAmount();
			if(Validations.validateIsNullOrEmpty(depositAmount))
				registerDepositFundsTO.getReturnsDepositTO().setDepositAmount(holdeFundsOperation.getHolderOperationAmount());
			else
				registerDepositFundsTO.getReturnsDepositTO().setDepositAmount(depositAmount.add(holdeFundsOperation.getHolderOperationAmount()));
		}else{
			BigDecimal depositAmount = registerDepositFundsTO.getReturnsDepositTO().getDepositAmount();
			registerDepositFundsTO.getReturnsDepositTO().setDepositAmount(depositAmount.subtract(holdeFundsOperation.getHolderOperationAmount()));
			if(BigDecimal.ZERO.compareTo(registerDepositFundsTO.getReturnsDepositTO().getDepositAmount()) == 0)
				registerDepositFundsTO.getReturnsDepositTO().setDepositAmount(null);
		}
	}
	
	/**
	 * Search operation international.
	 */
	public void searchOperationInternational(){
		JSFUtilities.hideGeneralDialogues();
		try {
			if(validateInternationalSearchDates()){
				registerDepositFundsTO.getInternationalDepositTO().setLstCashAccountDetails(null);
				registerDepositFundsTO.getInternationalDepositTO().setCashAccountDetail(null);
				boolean blFound = manageDepositServiceFacade.searchInternationalOperation(registerDepositFundsTO.getParticipantSelected().longValue(), registerDepositFundsTO.getInternationalDepositTO());
				if(!blFound){
					if(registerDepositFundsTO.getInternationalDepositTO().isBlReception()){
						if(Validations.validateIsNotNullAndNotEmpty(registerDepositFundsTO.getInternationalDepositTO().getErrorFound()))
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(registerDepositFundsTO.getInternationalDepositTO().getErrorFound()));
						else
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_OPERATION_NOTEXIST));
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_OPERATION_NOTEXIST));
					}
					JSFUtilities.showValidationDialog();
					registerDepositFundsTO.getInternationalDepositTO().setIdInternationalOperationPk(null);
				}else{
					List<CashAccountDetail> lstCashAccountDetail = manageDepositServiceFacade.getLstInstitutionBankAccount(registerDepositFundsTO);
					if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccountDetail)){
						for(CashAccountDetail cashAccountDetail:lstCashAccountDetail)
							cashAccountDetail.getInstitutionCashAccount().setCurrencyTypeDescription(mpCurrency.get(cashAccountDetail.getInstitutionCashAccount().getCurrency()));						
						registerDepositFundsTO.getInternationalDepositTO().setLstCashAccountDetails(new CashAccountDetailDataModel(lstCashAccountDetail));
					}else{
						Object[] bodyData = new Object[1];
						bodyData[0] = registerDepositFundsTO.getParticipantSelected().toString();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_PARTICIPANT_EFFECTIVEACCOUNT_NOTEXIST, bodyData));
						JSFUtilities.showValidationDialog();
						registerDepositFundsTO.getInternationalDepositTO().setIdInternationalOperationPk(null);
						registerDepositFundsTO.getInternationalDepositTO().setLstCashAccountDetails(null);
					}
				}	
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate international search dates.
	 *
	 * @return true, if successful
	 */
	private boolean validateInternationalSearchDates(){
		if(registerDepositFundsTO.getInternationalDepositTO().isBlSend()){
			if(registerDepositFundsTO.getInternationalDepositTO().getRegistryInitial().after(registerDepositFundsTO.getInternationalDepositTO().getRegistryEnd())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_REGISTRY_DATE_GREATERTHAN));
				registerDepositFundsTO.getInternationalDepositTO().setRegistryInitial(registerDepositFundsTO.getInternationalDepositTO().getRegistryEnd());
				JSFUtilities.showValidationDialog();
				return false;
			}else if(registerDepositFundsTO.getInternationalDepositTO().getSettlementInitial().after(registerDepositFundsTO.getInternationalDepositTO().getSettlementEnd())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_SETTLEMENT_DATE_GREATERTHAN));
				registerDepositFundsTO.getInternationalDepositTO().setSettlementInitial(registerDepositFundsTO.getInternationalDepositTO().getSettlementEnd());
				JSFUtilities.showValidationDialog();
				return false;
			}else{
				return true;
			}
		}else
			return true;
	}
	
	/**
	 * New international search.
	 */
	public void newInternationalSearch(){
		JSFUtilities.resetViewRoot();
		registerDepositFundsTO.getInternationalDepositTO().setLstCashAccountDetails(null);
		registerDepositFundsTO.getInternationalDepositTO().setCashAccountDetail(null);
		cleanInternationalOperationInfo();
	}
	
	/**
	 * Calculate inter amount.
	 */
	public void calculateInterAmount(){
		BigDecimal realDepositAmount = registerDepositFundsTO.getInternationalDepositTO().getRealDepositAmount();
		BigDecimal depositAmount = registerDepositFundsTO.getInternationalDepositTO().getDepositAmount();
		if(Validations.validateIsNotNullAndNotEmpty(depositAmount)){
			BigDecimal totalDeposit = realDepositAmount.add(depositAmount);
			if(totalDeposit.compareTo(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount()) >= 0){
				registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(totalDeposit.subtract(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount()));
				registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(BigDecimal.ZERO);
			}else{
				registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount().subtract(totalDeposit));
				registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(BigDecimal.ZERO);
			}
		}else{
			if(realDepositAmount.compareTo(BigDecimal.ZERO) == 0){
				registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(BigDecimal.ZERO);				
				registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(BigDecimal.ZERO);
			}else{
				if(realDepositAmount.compareTo(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount()) >= 0){
					registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(realDepositAmount.subtract(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount()));
					registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(BigDecimal.ZERO);
				}else{
					registerDepositFundsTO.getInternationalDepositTO().setPendientAmount(registerDepositFundsTO.getInternationalDepositTO().getEffectiveAmount().subtract(realDepositAmount));
					registerDepositFundsTO.getInternationalDepositTO().setExcedentAmount(BigDecimal.ZERO);
				}				
			}			
		}
	}
	
	/**
	 * Select international ope.
	 *
	 * @param internationalOpe the international ope
	 */
	public void selectInternationalOpe(InternationalOperation internationalOpe){
		if(internationalOpe.isBlCheck()){
			BigDecimal depositAmount = registerDepositFundsTO.getInternationalDepositTO().getDepositAmount();
			if(Validations.validateIsNullOrEmpty(depositAmount))
				registerDepositFundsTO.getInternationalDepositTO().setDepositAmount(internationalOpe.getSettlementAmount());
			else
				registerDepositFundsTO.getInternationalDepositTO().setDepositAmount(depositAmount.add(internationalOpe.getSettlementAmount()));
		}else{
			BigDecimal depositAmount = registerDepositFundsTO.getInternationalDepositTO().getDepositAmount();
			registerDepositFundsTO.getInternationalDepositTO().setDepositAmount(depositAmount.subtract(internationalOpe.getSettlementAmount()));
			if(BigDecimal.ZERO.compareTo(registerDepositFundsTO.getInternationalDepositTO().getDepositAmount()) == 0)
				registerDepositFundsTO.getInternationalDepositTO().setDepositAmount(null);
		}
	}
	
	/**
	 * Show fund operation detail.
	 *
	 * @param event the event
	 */
	public void showFundOperationDetail(ActionEvent event){	
		try{
			fundOperation = (FundsOperation)event.getComponent().getAttributes().get("fundOperationRow");
			blConfirm = false;
			blReject = false;
			blTransfersDep = false;
			fillView();
		}catch (Exception e) {
			e.printStackTrace();
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before action.
	 */
	@LoggerAuditWeb
	public void beforeAction(){
		JSFUtilities.hideGeneralDialogues();
		if(blConfirm){
			Object[] bodyData = new Object[1];				
			bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_DEPOSIT_ASK_CONFIRM, bodyData));
			JSFUtilities.showComponent("frmViewDeposit:cnfIdCAskDialog");
		}else if (blTransfersDep){
			Object[] bodyData = new Object[1];				
			bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_DEPOSIT_ASK_TRANFER_CENTRAL, bodyData));
			JSFUtilities.showComponent("frmViewDeposit:cnfIdCAskDialog");
		}
		else if(blReject){
			Object[] bodyData = new Object[1];				
			bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
					PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_DEPOSIT_ASK_REJECT, bodyData));
			JSFUtilities.showComponent("frmViewDeposit:cnfIdCAskDialog");
		}else if(blReject){
			Object[] bodyData = new Object[1];				
			bodyData[0] = fundOperation.getIdFundsOperationPk().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
					PropertiesUtilities.getMessage(PropertiesConstants.CASH_ACCOUNT_DEPOSIT_ASK_REJECT, bodyData));
			JSFUtilities.showComponent("frmViewDeposit:cnfIdCAskDialog");
		}
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		JSFUtilities.hideGeneralDialogues();
		if(blConfirm){
			confirmDeposit();
		}else if(blReject){
			rejectDeposit();
		}else if(blTransfersDep){
			transferCentral();
		}
	}
	
	/**
	 * Confirm deposit.
	 */
	private void confirmDeposit() {
		try{
			manageDepositServiceFacade.confirmDeposit(fundOperation, registerDepositFundsTO.getBankReference());
			Object[] bodyData = new Object[3];
			bodyData[0] = fundOperation.getIdFundsOperationPk().toString();			
			bodyData[1] = registerDepositFundsTO.getAccountTypeDescription();
			bodyData[2]=institutionDescription();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SUCESS_CONFIRM,bodyData));	
			JSFUtilities.showComponent("frmViewDeposit:cnfIdEndDialog");	
			// Start Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode());
			Object[] parameters = new Object[3];
			parameters[0] = fundOperation.getIdFundsOperationPk().toString();
			parameters[1] = registerDepositFundsTO.getAccountTypeDescription();
			parameters[2]=institutionDescription();
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,fundOperation.getIdFundsOperationPk().toString(), parameters);
			// End Notification
			searchDeposits();
		}catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));	
			JSFUtilities.showComponent("frmViewDeposit:cnfIdEndDialog");
		}
	}
	
	/**
	 * Reject deposit.
	 */
	private void rejectDeposit() {
		try{
			manageDepositServiceFacade.rejectDeposit(fundOperation);
			Object[] bodyData = new Object[3];
			bodyData[0] = fundOperation.getIdFundsOperationPk().toString();	
			bodyData[1] = registerDepositFundsTO.getAccountTypeDescription();
			bodyData[2]=institutionDescription();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SUCESS_REJECT, bodyData));	
			JSFUtilities.showComponent("frmViewDeposit:cnfIdEndDialog");	
			// Start Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_DEPOSIT_REJECT.getCode());
			Object[] parameters = new Object[3];
			parameters[0] = fundOperation.getIdFundsOperationPk().toString();
			parameters[1] = registerDepositFundsTO.getAccountTypeDescription();
			parameters[2]=institutionDescription();
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,fundOperation.getIdFundsOperationPk().toString(), parameters);
			// End Notification
			searchDeposits();
		}catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));	
			JSFUtilities.showComponent("frmViewDeposit:cnfIdEndDialog");
		}
	}
	
	/**
	 * Confirm deposit.
	 */
	private void transferCentral() {
		try{
			manageDepositServiceFacade.confirmTransferCentral(fundOperation, registerDepositFundsTO.getBankReference());
			Object[] bodyData = new Object[3];
			bodyData[0] = fundOperation.getIdFundsOperationPk().toString();			
			bodyData[1] = registerDepositFundsTO.getAccountTypeDescription();
			bodyData[2]=institutionDescription();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SUCESS_CONFIRM,bodyData));	
			JSFUtilities.showComponent("frmViewDeposit:cnfIdEndDialog");	
			// Start Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode());
			Object[] parameters = new Object[3];
			parameters[0] = fundOperation.getIdFundsOperationPk().toString();
			parameters[1] = registerDepositFundsTO.getAccountTypeDescription();
			parameters[2]=institutionDescription();
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,fundOperation.getIdFundsOperationPk().toString(), parameters);
			// End Notification
			searchDeposits();
		}catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));	
			JSFUtilities.showComponent("frmViewDeposit:cnfIdEndDialog");
		}
	}
	
	/**
	 * Do action multiple.
	 */
	@LoggerAuditWeb
	public void doActionMultiple(){
		JSFUtilities.hideGeneralDialogues();
		if(blConfirm){
			confirmDepositMultiple();
		} else if(blReject){
			rejectDepositMultiple();
		}
	}
	
	/**
	 * Reject deposit multiple.
	 */
	private void rejectDepositMultiple() {
		try{
			List<RegisterDepositFundsTO> lstRegisterDepositFunds = new ArrayList<RegisterDepositFundsTO>();
			if(operationsNums != null){
				operationsNums.clear();
			} else {
				operationsNums = new ArrayList<Long>();
			}
			if (arrayFundOperation != null && arrayFundOperation.length > 0) {
				List<FundsOperation> lstFundOperation = new ArrayList<FundsOperation>();
				for(FundsOperation objFundOperation : arrayFundOperation){					
					fundOperationDetail = manageDepositServiceFacade.getFundOperationDetail(objFundOperation.getIdFundsOperationPk());
					fundOperationDetail.setCurrencyDescription(objFundOperation.getCurrencyDescription());
					registerDepositFundsTO = new RegisterDepositFundsTO();
					registerDepositFundsTO.setIdFundsOperation(objFundOperation.getIdFundsOperationPk());
					registerDepositFundsTO.setLstFundOperationGroup(searchDepositFundsTO.getLstFundOperationGroup());
					registerDepositFundsTO.setFundOperationGroupSelected(fundOperationDetail.getFundsOperationGroup());
					registerDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(registerDepositFundsTO.getFundOperationGroupSelected(), false));
					registerDepositFundsTO.setOperationTypeSelected(fundOperationDetail.getFundsOperationType());
					for(FundsOperationType fundOperationType : registerDepositFundsTO.getLstOperationType()){
						registerDepositFundsTO.setAccountTypeDescription(generalPametersFacade.getParameterTableById(fundOperationType.getCashAccountType()).getParameterName());
						registerDepositFundsTO.setAccountTypeSelected(fundOperationType.getCashAccountType().longValue());
						break;
					}
					if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getParticipant())){
						registerDepositFundsTO.setParticipantSelected(fundOperationDetail.getParticipant().getIdParticipantPk().intValue());
						registerDepositFundsTO.setParticipantDes(fundOperationDetail.getParticipant().getDisplayDescriptionCode());
					}
					if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getIssuer())){
						registerDepositFundsTO.setIssuer(new Issuer(fundOperationDetail.getIssuer().getIdIssuerPk()));
						registerDepositFundsTO.setIssuerDes(fundOperationDetail.getIssuer().getBusinessNameCode());
					}
					lstRegisterDepositFunds.add(registerDepositFundsTO);
					objFundOperation.setBankReference(registerDepositFundsTO.getBankReference());
					lstFundOperation.add(objFundOperation);	
					operationsNums.add(objFundOperation.getIdFundsOperationPk());
				}
				
				manageDepositServiceFacade.rejectDepositMultiple(lstFundOperation);
				
				String oper = foundsOperationsMessage(operationsNums);
				
				Object[] bodyData = new Object[1];
				bodyData[0] = oper;					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
							, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SUCESS_REJECT_MULTI, bodyData));	
				JSFUtilities.showComponent("formPopUp:cnfIdEndDialog");				
				
				for(RegisterDepositFundsTO objRegisterDepositFundsTO : lstRegisterDepositFunds){
					// Start Notification
					BusinessProcess businessProcessNotification = new BusinessProcess();					
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_DEPOSIT_REJECT.getCode());
					Object[] parameters = new Object[3];
					parameters[0] = objRegisterDepositFundsTO.getIdFundsOperation().toString();
					parameters[1] = objRegisterDepositFundsTO.getAccountTypeDescription();
					parameters[2]=institutionDescription();
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
							businessProcessNotification, objRegisterDepositFundsTO.getIdFundsOperation().toString(), parameters);
					// End Notification					
				}								
				searchDeposits();
			}
		}catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));	
			JSFUtilities.showComponent("formPopUp:cnfIdEndDialog");
		}
	}
	
	/**
	 * Confirm deposit multiple.
	 */
	private void confirmDepositMultiple() {
		try{	
			List<RegisterDepositFundsTO> lstRegisterDepositFunds = new ArrayList<RegisterDepositFundsTO>();
			if(operationsNums != null){
				operationsNums.clear();
			} else {
				operationsNums = new ArrayList<Long>();
			}			
			if (arrayFundOperation != null && arrayFundOperation.length > 0) {
				List<FundsOperation> lstFundOperation = new ArrayList<FundsOperation>();			
				for(FundsOperation objFundOperation : arrayFundOperation){					
					fundOperationDetail = manageDepositServiceFacade.getFundOperationDetail(objFundOperation.getIdFundsOperationPk());
					fundOperationDetail.setCurrencyDescription(objFundOperation.getCurrencyDescription());
					registerDepositFundsTO = new RegisterDepositFundsTO();
					registerDepositFundsTO.setIdFundsOperation(objFundOperation.getIdFundsOperationPk());
					registerDepositFundsTO.setLstFundOperationGroup(searchDepositFundsTO.getLstFundOperationGroup());
					registerDepositFundsTO.setFundOperationGroupSelected(fundOperationDetail.getFundsOperationGroup());
					registerDepositFundsTO.setLstOperationType(manageDepositServiceFacade.getFundsOperationType(registerDepositFundsTO.getFundOperationGroupSelected(), false));
					registerDepositFundsTO.setOperationTypeSelected(fundOperationDetail.getFundsOperationType());
					for(FundsOperationType fundOperationType : registerDepositFundsTO.getLstOperationType()){
						registerDepositFundsTO.setAccountTypeDescription(generalPametersFacade.getParameterTableById(fundOperationType.getCashAccountType()).getParameterName());
						registerDepositFundsTO.setAccountTypeSelected(fundOperationType.getCashAccountType().longValue());
						break;
					}
					if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getParticipant())){
						registerDepositFundsTO.setParticipantSelected(fundOperationDetail.getParticipant().getIdParticipantPk().intValue());
						registerDepositFundsTO.setParticipantDes(fundOperationDetail.getParticipant().getDisplayDescriptionCode());
					}
					if(Validations.validateIsNotNullAndNotEmpty(fundOperationDetail.getIssuer())){
						registerDepositFundsTO.setIssuer(new Issuer(fundOperationDetail.getIssuer().getIdIssuerPk()));
						registerDepositFundsTO.setIssuerDes(fundOperationDetail.getIssuer().getBusinessNameCode());
					}
					lstRegisterDepositFunds.add(registerDepositFundsTO);
					objFundOperation.setBankReference(registerDepositFundsTO.getBankReference());
					lstFundOperation.add(objFundOperation);	
					operationsNums.add(objFundOperation.getIdFundsOperationPk());
				}					
				manageDepositServiceFacade.confirmDepositMultiple(lstFundOperation);					
				String oper = foundsOperationsMessage(operationsNums);				
				Object[] bodyData = new Object[1];
				bodyData[0] = oper;				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SUCESS_CONFIRM,bodyData));		    	
				JSFUtilities.showComponent("formPopUp:cnfIdEndDialog");
				
				for(RegisterDepositFundsTO objRegisterDepositFundsTO : lstRegisterDepositFunds){
					// Start Notification
					BusinessProcess businessProcessNotification = new BusinessProcess();					
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode());
					Object[] parameters = new Object[3];
					parameters[0] = objRegisterDepositFundsTO.getIdFundsOperation().toString();
					parameters[1] = objRegisterDepositFundsTO.getAccountTypeDescription();	
					parameters[2]=institutionDescription();
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
							businessProcessNotification,objRegisterDepositFundsTO.getIdFundsOperation().toString(), parameters);
					// End Notification				
				}							
				searchDeposits();				
			}			
		}catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));	
			JSFUtilities.showComponent("formPopUp:cnfIdEndDialog");
		}
	}
	
	/**
	 * Hide view dialogs.
	 */
	public void hideViewDialogs(){
		JSFUtilities.hideComponent("frmViewDeposit:cnfIdCAskDialog");
		JSFUtilities.hideComponent("frmViewDeposit:cnfWEndDialog");
	}
	
	/**
	 * Hide modify dialogs.
	 */
	public void hideModifyDialogs(){
		JSFUtilities.hideComponent("frmModifyDeposit:cnfIdCAskDialog");
		JSFUtilities.hideComponent("frmModifyDeposit:cnfWEndDialog");
	}
	
	/**
	 * Back from register.
	 *
	 * @return the string
	 */
	public String backFromRegister(){
		init();
		return "backManageDeposit";
	}
	

	/**
	 * load Currency.
	 *
	 * @throws ServiceException  the service exception
	 */
	private void loadCurrency() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		List<ParameterTable> lstParameterTable = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstParameterTable){
			if(parameterTable.getParameterTablePk().equals(CurrencyType.PYG.getCode()) 
					|| parameterTable.getParameterTablePk().equals(CurrencyType.USD.getCode()))
				lstParameterTableAux.add(parameterTable);
		}
		lstCurrency = lstParameterTableAux;	
	}
	
	/**
	 * Onchange participant.
	 */	
	@LoggerAuditWeb
	public void onchangeParticipant(){
		try {		
			cleanMechanismAndGroupModaity();
			if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getParticipantSelected())){
				lstNegMechanism = fundsOperationServiceFacade.searchParticipantMechanisms(Long.valueOf(searchDepositFundsTO.getParticipantSelected()));
			}
		}  catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean mechanism and group modaity.
	 */
	public void cleanMechanismAndGroupModaity(){
		searchDepositFundsTO.setMechanismSelected(null);
		searchDepositFundsTO.setModalitySelected(null);
		lstNegMechanism = null;
		lstModalityGroup = null;
	}
	
	/**
	 * Change mechanism.
	 */
	@LoggerAuditWeb
	public void changeMechanism(){
		try{		
			Long negotiationMechanismId = searchDepositFundsTO.getMechanismSelected();
			searchDepositFundsTO.setModalitySelected(null);			
			lstModalityGroup = fundsOperationServiceFacade.searchModalityGroup(negotiationMechanismId);			
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before process reception.
	 */
	public void beforeProcessReception(String opcion){
		
		currencySelect = opcion;
		
		if(currencySelect != null && CurrencyType.PYG.getCodeIso().equals(currencySelect)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.DEPOSIT_RECEPTION_LIP_BS_CONFIRM));
		} else if(currencySelect != null && CurrencyType.USD.getCodeIso().equals(currencySelect)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.DEPOSIT_RECEPTION_LIP_SUS_CONFIRM));
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.DEPOSIT_RECEPTION_LIP_CONFIRM));			
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfwProcessReception').show();");
		
	}
	
	/**
	 * Process reception lip.
	 */
	@LoggerAuditWeb
	public void processReceptionLip(){			
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_DEPOSIT_PROCESS.getCode());		
		try {
			Map<String, Object> details = new HashMap<String, Object>();				
			details.put("currencyType", currencySelect);
			
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, details);			
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.DEPOSIT_RECPTION_LIP_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();		
	}

	/**
	 * Gets the search deposit funds to.
	 *
	 * @return the search deposit funds to
	 */
	public SearchDepositFundsTO getSearchDepositFundsTO() {
		return searchDepositFundsTO;
	}

	/**
	 * Sets the search deposit funds to.
	 *
	 * @param searchDepositFundsTO the new search deposit funds to
	 */
	public void setSearchDepositFundsTO(SearchDepositFundsTO searchDepositFundsTO) {
		this.searchDepositFundsTO = searchDepositFundsTO;
	}

	/**
	 * Gets the register deposit funds to.
	 *
	 * @return the register deposit funds to
	 */
	public RegisterDepositFundsTO getRegisterDepositFundsTO() {
		return registerDepositFundsTO;
	}

	/**
	 * Sets the register deposit funds to.
	 *
	 * @param registerDepositFundsTO the new register deposit funds to
	 */
	public void setRegisterDepositFundsTO(
			RegisterDepositFundsTO registerDepositFundsTO) {
		this.registerDepositFundsTO = registerDepositFundsTO;
	}

	/**
	 * Gets the fund operation.
	 *
	 * @return the fund operation
	 */
	public FundsOperation getFundOperation() {
		return fundOperation;
	}

	/**
	 * Sets the fund operation.
	 *
	 * @param fundOperation the new fund operation
	 */
	public void setFundOperation(FundsOperation fundOperation) {
		this.fundOperation = fundOperation;
	}

	/**
	 * Gets the fund operation detail.
	 *
	 * @return the fund operation detail
	 */
	public FundsOperation getFundOperationDetail() {
		return fundOperationDetail;
	}

	/**
	 * Sets the fund operation detail.
	 *
	 * @param fundOperationDetail the new fund operation detail
	 */
	public void setFundOperationDetail(FundsOperation fundOperationDetail) {
		this.fundOperationDetail = fundOperationDetail;
	}

	/**
	 * Gets the lst funds operation.
	 *
	 * @return the lst funds operation
	 */
	public FundsOperationDataModel getLstFundsOperation() {
		return lstFundsOperation;
	}

	/**
	 * Sets the lst funds operation.
	 *
	 * @param lstFundsOperation the new lst funds operation
	 */
	public void setLstFundsOperation(FundsOperationDataModel lstFundsOperation) {
		this.lstFundsOperation = lstFundsOperation;
	}

	/**
	 * Gets the mp currency.
	 *
	 * @return the mp currency
	 */
	public Map<Integer, String> getMpCurrency() {
		return mpCurrency;
	}

	/**
	 * Sets the mp currency.
	 *
	 * @param mpCurrency the mp currency
	 */
	public void setMpCurrency(Map<Integer, String> mpCurrency) {
		this.mpCurrency = mpCurrency;
	}

	/**
	 * Gets the mp fund ope group.
	 *
	 * @return the mp fund ope group
	 */
	public Map<Integer, String> getMpFundOpeGroup() {
		return mpFundOpeGroup;
	}

	/**
	 * Sets the mp fund ope group.
	 *
	 * @param mpFundOpeGroup the mp fund ope group
	 */
	public void setMpFundOpeGroup(Map<Integer, String> mpFundOpeGroup) {
		this.mpFundOpeGroup = mpFundOpeGroup;
	}

	/**
	 * Gets the mp state.
	 *
	 * @return the mp state
	 */
	public Map<Integer, String> getMpState() {
		return mpState;
	}

	/**
	 * Sets the mp state.
	 *
	 * @param mpState the mp state
	 */
	public void setMpState(Map<Integer, String> mpState) {
		this.mpState = mpState;
	}

	/**
	 * Gets the mp operation type.
	 *
	 * @return the mp operation type
	 */
	public Map<Long, String> getMpOperationType() {
		return mpOperationType;
	}

	/**
	 * Sets the mp operation type.
	 *
	 * @param mpOperationType the mp operation type
	 */
	public void setMpOperationType(Map<Long, String> mpOperationType) {
		this.mpOperationType = mpOperationType;
	}

	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}

	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}

	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}

	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the lst cash account type.
	 *
	 * @return the lstCashAccountType
	 */
	public List<ParameterTable> getLstCashAccountType() {
		return lstCashAccountType;
	}

	/**
	 * Sets the lst cash account type.
	 *
	 * @param lstCashAccountType the lstCashAccountType to set
	 */
	public void setLstCashAccountType(List<ParameterTable> lstCashAccountType) {
		this.lstCashAccountType = lstCashAccountType;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lstCurrency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the lstCurrency to set
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst neg mechanism.
	 *
	 * @return the lstNegMechanism
	 */
	public List<NegotiationMechanism> getLstNegMechanism() {
		return lstNegMechanism;
	}

	/**
	 * Sets the lst neg mechanism.
	 *
	 * @param lstNegMechanism the lstNegMechanism to set
	 */
	public void setLstNegMechanism(List<NegotiationMechanism> lstNegMechanism) {
		this.lstNegMechanism = lstNegMechanism;
	}

	/**
	 * Gets the lst modality group.
	 *
	 * @return the lstModalityGroup
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}

	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the lstModalityGroup to set
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}

	/**
	 * Gets the array fund operation.
	 *
	 * @return the arrayFundOperation
	 */
	public FundsOperation[] getArrayFundOperation() {
		return arrayFundOperation;
	}

	/**
	 * Sets the array fund operation.
	 *
	 * @param arrayFundOperation the arrayFundOperation to set
	 */
	public void setArrayFundOperation(FundsOperation[] arrayFundOperation) {
		this.arrayFundOperation = arrayFundOperation;
	}

	/**
	 * Gets the operations nums.
	 *
	 * @return the operationsNums
	 */
	public List<Long> getOperationsNums() {
		return operationsNums;
	}

	/**
	 * Sets the operations nums.
	 *
	 * @param operationsNums the operationsNums to set
	 */
	public void setOperationsNums(List<Long> operationsNums) {
		this.operationsNums = operationsNums;
	}

	public String getCurrencySelect() {
		return currencySelect;
	}

	public void setCurrencySelect(String currencySelect) {
		this.currencySelect = currencySelect;
	}

	public boolean isBlTransfersDep() {
		return blTransfersDep;
	}

	public void setBlTransfersDep(boolean blTransfersDep) {
		this.blTransfersDep = blTransfersDep;
	}
	
	/**
	 * Gets the obj institution bank accounts type.
	 *
	 * @return the obj institution bank accounts type
	 */
	public Integer getObjInstitutionBankAccountsType() {
		return objInstitutionBankAccountsType;
	}

	/**
	 * Sets the obj institution bank accounts type.
	 *
	 * @param objInstitutionBankAccountsType the new obj institution bank accounts type
	 */
	public void setObjInstitutionBankAccountsType(
			Integer objInstitutionBankAccountsType) {
		this.objInstitutionBankAccountsType = objInstitutionBankAccountsType;
	}
	
	public List<ParameterTable> getLstInstitutionBankAccountsTypeReturn() {
		return lstInstitutionBankAccountsTypeReturn;
	}

	public void setLstInstitutionBankAccountsTypeReturn(List<ParameterTable> lstInstitutionBankAccountsTypeReturn) {
		this.lstInstitutionBankAccountsTypeReturn = lstInstitutionBankAccountsTypeReturn;
	}
	
	
}
