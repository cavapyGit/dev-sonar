package com.pradera.funds.fundsoperations.deposit.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
//import com.pradera.core.component.collectionoperations.CollectionProcessServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.funds.automaticinterface.lip.service.ManageLipServiceFacade;
import com.pradera.funds.fundoperations.util.PropertiesConstants;
import com.pradera.funds.fundsoperations.deposit.service.ManageDepositServiceBean;
import com.pradera.funds.fundsoperations.deposit.to.InternationalDepositTO;
import com.pradera.funds.fundsoperations.deposit.to.RegisterDepositFundsTO;
import com.pradera.funds.fundsoperations.deposit.to.ReturnsDeposiTO;
import com.pradera.funds.fundsoperations.deposit.to.SearchDepositFundsTO;
import com.pradera.funds.fundsoperations.deposit.to.SettlementDepositTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.FundsInternationalOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.settlements.core.service.FundsComponentSingleton;
import com.pradera.settlements.core.service.SettlementProcessService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageDepositServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageDepositServiceFacade {
	
	/** The manage deposit service bean. */
	@EJB
	ManageDepositServiceBean manageDepositServiceBean;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The reception service. */
	@EJB
	AutomaticReceptionFundsService receptionService;
	
	/** The cash account service. */
	@EJB
	CashAccountManagementService cashAccountService;
	
	/** The settlement process service bean. */
	@EJB
	SettlementProcessService settlementProcessServiceBean;
	
	/** The funds component singleton. */
	@EJB
	private FundsComponentSingleton fundsComponentSingleton;
	
	/** The manage lip service facade. */
	@Inject
	ManageLipServiceFacade manageLipServiceFacade;
	
	/**
	 * Gets the funds operation type.
	 *
	 * @return the funds operation type
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperationType> getFundsOperationType() throws ServiceException{
		return manageDepositServiceBean.getFundsOperationType();		
	} 
	
	/**
	 * Gets the funds operation type.
	 *
	 * @param fundsOperationGroup the funds operation group
	 * @param blOnlyRegister the bl only register
	 * @return the funds operation type
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperationType> getFundsOperationType(Integer fundsOperationGroup, boolean blOnlyRegister) throws ServiceException{
		return manageDepositServiceBean.getFundsOperationType(fundsOperationGroup, blOnlyRegister);		
	}
	
	/**
	 * Gets the participants int depositary for deposit.
	 *
	 * @return the participants int depositary for deposit
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantsIntDepositaryForDeposit() throws ServiceException{
		return manageDepositServiceBean.getParticipantsIntDepositaryForDeposit();
	}	
	
	/**
	 * Gets the participants for deposit.
	 *
	 * @param blDirect the bl direct
	 * @return the participants for deposit
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantsForDeposit(boolean blDirect) throws ServiceException{
		return manageDepositServiceBean.getParticipantsForDeposit(blDirect);
	}

	/**
	 * Gets the issuers for deposit.
	 *
	 * @return the issuers for deposit
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> getIssuersForDeposit() throws ServiceException{
		return manageDepositServiceBean.getIssuersForDeposit();
	}
	
	/**
	 * Gets the negotitation mechanism for participant.
	 *
	 * @param idParticipantPkSelected the id participant pk selected
	 * @return the negotitation mechanism for participant
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getNegotitationMechanismForParticipant(Long idParticipantPkSelected) throws ServiceException{
		return manageDepositServiceBean.getNegotitationMechanismForParticipant(idParticipantPkSelected);
	}

	/**
	 * Gets the modality group for nego mechanism.
	 *
	 * @param idNegoMechanismPkSelected the id nego mechanism pk selected
	 * @return the modality group for nego mechanism
	 * @throws ServiceException the service exception
	 */
	public List<ModalityGroup> getModalityGroupForNegoMechanism(Long idNegoMechanismPkSelected) throws ServiceException{
		return manageDepositServiceBean.getModalityGroupForNegoMechanism(idNegoMechanismPkSelected);
	}

	/**
	 * Gets the negotiation modality for moda group.
	 *
	 * @param idModaGroupPkSelected the id moda group pk selected
	 * @return the negotiation modality for moda group
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getNegotiationModalityForModaGroup(Long idModaGroupPkSelected) throws ServiceException{
		return manageDepositServiceBean.getNegotiationModalityForModaGroup(idModaGroupPkSelected);
	}
	
	/**
	 * Search deposit funds operation.
	 *
	 * @param searchDepositFundsTO the search deposit funds to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_QUERY)
	public List<FundsOperation> searchDepositFundsOperation( SearchDepositFundsTO searchDepositFundsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return manageDepositServiceBean.searchDepositFundsOperation(searchDepositFundsTO);
	}
	
	/**
	 * Gets the lst institution bank account.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the lst institution bank account
	 * @throws ServiceException the service exception
	 */
	public List<CashAccountDetail> getLstInstitutionBankAccount(RegisterDepositFundsTO registerDepositFundsTO) throws ServiceException{
		if(registerDepositFundsTO.isBlParticipant()){
			if(registerDepositFundsTO.isBlSettlement()){
				if(registerDepositFundsTO.getSettlementGroupTO().isBlSettlementGross())
					return manageDepositServiceBean.getCashAccountForParticipantSettlementGross(registerDepositFundsTO);
				else if(registerDepositFundsTO.getSettlementGroupTO().isBlSettlementNet())
					return manageDepositServiceBean.getLstCashAccountForParticipantSettlementNet(registerDepositFundsTO);
				else
					return null;
			}else if(registerDepositFundsTO.isBlGuarantees())
				return manageDepositServiceBean.getLstCashAccountForParticipant(registerDepositFundsTO);
			else if(registerDepositFundsTO.isBlInternational())
				return manageDepositServiceBean.getLstCashAccountForParticipantInternational(registerDepositFundsTO);
			else if(registerDepositFundsTO.isBlReturns())
				return manageDepositServiceBean.getLstCashAccountForParticipant(registerDepositFundsTO);
			else
				return null;
		} else if(registerDepositFundsTO.isBlIssuer() || registerDepositFundsTO.isBlReturns()){
			return manageDepositServiceBean.getLstCashAccountForIssuer(registerDepositFundsTO);
		}
		else if(registerDepositFundsTO.isBlEDV()){
//			if(registerDepositFundsTO.isBlReturns())
//				return manageDepositServiceBean.getLstInstitutionBankAccountForDepositaryWithCurrency(registerDepositFundsTO);
//			else
			return manageDepositServiceBean.getLstCashAccountAccountForDepositary(registerDepositFundsTO);
		}
		return null;
	}
	
	/**
	 * Search operation.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param settlementGroupTO the settlement group to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean searchOperation(Long idParticipantPk, SettlementDepositTO settlementGroupTO) throws ServiceException{
		List<Object[]> lstResult = manageDepositServiceBean.searchOperation(idParticipantPk, settlementGroupTO);
		settlementGroupTO.setErrorFound(null);
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			for(Object[] objResult:lstResult){
				if(OperationPartType.CASH_PART.getCode().equals(settlementGroupTO.getOperationPartSelected())){
					if(BooleanType.NO.getCode().equals(new Integer(objResult[13].toString()))
							&& BooleanType.NO.getCode().equals(new Integer(objResult[14].toString()))){
						if(!CommonsUtilities.currentDate().equals((Date)objResult[9])){
							settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATE_ERROR);
							return false;
						}
					}else if(BooleanType.YES.getCode().equals(new Integer(objResult[13].toString()))){//indicator settlement date extend
						if(!CommonsUtilities.currentDate().equals((Date)objResult[9])){
							settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEEXTEND_ERROR);
							return false;
						}
					}else if(BooleanType.YES.getCode().equals(new Integer(objResult[14].toString()))){//indicator settlement date prepaid
						if(!CommonsUtilities.currentDate().equals((Date)objResult[9])){
							settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEPREPAID_ERROR);
							return false;
						}
					}
				}else if(OperationPartType.TERM_PART.getCode().equals(settlementGroupTO.getOperationPartSelected())){
					if(BooleanType.NO.getCode().equals(new Integer(objResult[13].toString()))
							&& BooleanType.NO.getCode().equals(new Integer(objResult[14].toString()))){
						if(!CommonsUtilities.currentDate().equals((Date)objResult[9])){
							settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATE_ERROR);
							return false;
						}
					}else if(BooleanType.YES.getCode().equals(new Integer(objResult[13].toString()))){//indicator settlement date extend
						if(!CommonsUtilities.currentDate().equals((Date)objResult[9])){
							settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEEXTEND_ERROR);
							return false;
						}
					}else if(BooleanType.YES.getCode().equals(new Integer(objResult[14].toString()))){//indicator settlement date prepaid
						if(!CommonsUtilities.currentDate().equals((Date)objResult[9])){
							settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENTDATEPREPAID_ERROR);
							return false;
						}
					}
				}
//				if(BooleanType.YES.getCode().equals(new Integer(objResult[14].toString()))
//					&& HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode().equals(new Long(objResult[15].toString()))){
//					settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INCHARGE_ERROR);
//					return false;
//				}
				
				if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(new Long(objResult[12].toString()))
						|| AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode().equals(new Long(objResult[12].toString()))){
					settlementGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_FUNDREFERENCE_ERROR);
					return false;
				}
				settlementGroupTO.setReferencePayment(objResult[0] == null ? null : objResult[0].toString());
				settlementGroupTO.setNegoModalitySelected(new Long(objResult[1].toString()));
				settlementGroupTO.setOperationNumber(new Long(objResult[2].toString()));
				settlementGroupTO.setOperationDate((Date)objResult[3]);
				settlementGroupTO.setCurrency(new Integer(objResult[4].toString()));
				settlementGroupTO.setCurrencyDescription(objResult[5].toString());
				settlementGroupTO.setEffectiveAmount(new BigDecimal(objResult[6].toString()));				
				settlementGroupTO.setMechanismOperationPk(new Long(objResult[7].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(objResult[8])){
					BigDecimal depositAmount = new BigDecimal(objResult[8].toString());
					settlementGroupTO.setRealDepositAmount(depositAmount);
					if(settlementGroupTO.getEffectiveAmount().compareTo(depositAmount) == 1){
						settlementGroupTO.setPendientAmount(settlementGroupTO.getEffectiveAmount().subtract(depositAmount));
						settlementGroupTO.setExcedentAmount(BigDecimal.ZERO);
					}else{
						settlementGroupTO.setPendientAmount(BigDecimal.ZERO);
						settlementGroupTO.setExcedentAmount(depositAmount.subtract(settlementGroupTO.getEffectiveAmount()));
					}
				}else{
					settlementGroupTO.setRealDepositAmount(BigDecimal.ZERO);
					settlementGroupTO.setPendientAmount(settlementGroupTO.getEffectiveAmount());
					settlementGroupTO.setExcedentAmount(BigDecimal.ZERO);
				}
				settlementGroupTO.setSettlementOperationPk(new Long(objResult[15].toString()));
			}
			return true;
		}
		return false;
	}
			
	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 * @throws ServiceException the service exception
	 */
	public List<Bank> getLstBank() throws ServiceException{
		return manageDepositServiceBean.getLstBank();
	}
	
	/**
	 * Gets the holder service facade.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the holder service facade
	 * @throws ServiceException the service exception
	 */
	public Holder getHolderServiceFacade(Long idHolderPk) throws ServiceException{
	       return manageDepositServiceBean.getHolderServiceBean(idHolderPk);
	}
	
	/**
	 * Gets the lst holder funds operation.
	 *
	 * @param returnsDepositTO the returns deposit to
	 * @return the lst holder funds operation
	 * @throws ServiceException the service exception
	 */
	public List<HolderFundsOperation> getLstHolderFundsOperation(ReturnsDeposiTO returnsDepositTO) throws ServiceException{
		List<HolderCashMovement> lstTemp = manageDepositServiceBean.getLstHolderCashMovement(returnsDepositTO);
		List<HolderFundsOperation> lstResult = new ArrayList<HolderFundsOperation>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstTemp)){
			for(HolderCashMovement holCashMovement:lstTemp){
				HolderFundsOperation holFundsOperation = new HolderFundsOperation();
				holFundsOperation.setCurrency(holCashMovement.getCurrency());
				holFundsOperation.setHolder(holCashMovement.getHolder());
				holFundsOperation.setBankDescription(holCashMovement.getBank().getDescription());
				holFundsOperation.setHolderOperationAmount(holCashMovement.getHolderAmount());
				holFundsOperation.setRelatedHolderCashMovement(holCashMovement);
				holFundsOperation.setBankAccountType(holCashMovement.getBankAccountType());
				holFundsOperation.setFullNameHolder(holCashMovement.getHolder().getFullName());
				holFundsOperation.setHolderAccountBank(holCashMovement.getHolderAccountBank());
				holFundsOperation.setFundsOperationType(holCashMovement.getFundsOperationType());
				holFundsOperation.setHolderDocumentNumber(holCashMovement.getDocumentHolder());
				holFundsOperation.setHolderOperationAmount(holCashMovement.getHolderAmount());
  				
				lstResult.add(holFundsOperation);
			}
			return lstResult;
		}		
		return null;
	}
	
	/**
	 * Gets the lst international depositary.
	 *
	 * @return the lst international depositary
	 * @throws ServiceException the service exception
	 */
	public List<InternationalDepository> getLstInternationalDepositary() throws ServiceException{
		return manageDepositServiceBean.getLstInternationalDepositary();
	}
	
	/**
	 * Search international operation.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param internationalGroupTO the international group to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean searchInternationalOperation(Long idParticipantPk,
			InternationalDepositTO internationalGroupTO) throws ServiceException{
		internationalGroupTO.setErrorFound(null);
		if(internationalGroupTO.isBlReception()){
			List<Object[]> lstResult = manageDepositServiceBean.searchInternationalOperation(idParticipantPk, internationalGroupTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(Object[] objResult:lstResult){
					if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(new Long(objResult[13].toString()))
							|| AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode().equals(new Long(objResult[13].toString()))){
						internationalGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_FUNDREFERENCE_ERROR);
						return false;
					}
					if(!InternationalOperationStateType.CONFIRMADA.getCode().equals(new Integer(objResult[14].toString()))){
						internationalGroupTO.setErrorFound(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_INTERNATIONAL_INVALIDSTATE);
						return false;
					}
					
					internationalGroupTO.setCurrency(new Integer(objResult[0].toString()));
					internationalGroupTO.setCurrencyDescription(objResult[1].toString());
					internationalGroupTO.setEffectiveAmount(new BigDecimal(objResult[2].toString()));
					internationalGroupTO.setIdIsinCodePk(objResult[3].toString());
					internationalGroupTO.setSettlementDate((Date)objResult[4]);
					internationalGroupTO.setIdInterDepoNegotitatiorPk(new Long(objResult[5].toString()));
					internationalGroupTO.setInterDepoNegotiatorDescription(objResult[6].toString());
					internationalGroupTO.setIdInterDepoLiquidatorPk(new Long(objResult[7].toString()));
					internationalGroupTO.setInterDepoLiquidatorDescription(objResult[8].toString());
					internationalGroupTO.setIdInternationalOperationPk(new Long(objResult[9].toString()));
					internationalGroupTO.setIdInternationalParticipantPk(new Long(objResult[10].toString()));
					internationalGroupTO.setParticipantInterDescription(objResult[11].toString());
					if(Validations.validateIsNotNullAndNotEmpty(objResult[12])){
						BigDecimal depositAmount = new BigDecimal(objResult[12].toString());
						internationalGroupTO.setRealDepositAmount(depositAmount);
						if(internationalGroupTO.getEffectiveAmount().compareTo(depositAmount) == 1){
							internationalGroupTO.setPendientAmount(internationalGroupTO.getEffectiveAmount().subtract(depositAmount));
							internationalGroupTO.setExcedentAmount(BigDecimal.ZERO);
						}else{
							internationalGroupTO.setPendientAmount(BigDecimal.ZERO);
							internationalGroupTO.setExcedentAmount(depositAmount.subtract(internationalGroupTO.getEffectiveAmount()));
						}
					}else{
						internationalGroupTO.setRealDepositAmount(BigDecimal.ZERO);
						internationalGroupTO.setPendientAmount(internationalGroupTO.getEffectiveAmount());
						internationalGroupTO.setExcedentAmount(BigDecimal.ZERO);
					}	
				}
				return true;
			}
			return false;
		}else if(internationalGroupTO.isBlSend()){
			List<InternationalOperation> lstResult = manageDepositServiceBean.searchLstInternationalOperation(idParticipantPk, internationalGroupTO);
			if(Validations.validateListIsNullOrEmpty(lstResult))
				return false;
			else{
				internationalGroupTO.setLstInternationalOperation(lstResult);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the fund operation detail.
	 *
	 * @param idFundsOperationPk the id funds operation pk
	 * @return the fund operation detail
	 * @throws ServiceException the service exception
	 */
	public FundsOperation getFundOperationDetail(Long idFundsOperationPk) throws ServiceException{
		FundsOperation fundOpeResult = manageDepositServiceBean.getFundOperationDetail(idFundsOperationPk);		
		if(Validations.validateIsNotNullAndNotEmpty(fundOpeResult.getMechanismOperation()) &&
				Validations.validateIsNotNullAndNotEmpty(fundOpeResult.getMechanismOperation().getIdMechanismOperationPk()))
		fundOpeResult.getLstFundsInternationalOperation().size();
		if(Validations.validateListIsNotNullAndNotEmpty(fundOpeResult.getLstFundsInternationalOperation())){
			for(FundsInternationalOperation fundsIntOpe:fundOpeResult.getLstFundsInternationalOperation()){
				fundsIntOpe.getInternationalOperation().getIdInternationalOperationPk();
			}
		}
		return fundOpeResult;
	}
	
	/**
	 * Gets the cash account detail.
	 *
	 * @param idInstitutionCashAccountPk the id institution cash account pk
	 * @return the cash account detail
	 * @throws ServiceException the service exception
	 */
	public CashAccountDetail getCashAccountDetail(Long idInstitutionCashAccountPk) throws ServiceException{
		return manageDepositServiceBean.getCashAccountDetail(idInstitutionCashAccountPk);
	}
	
	/**
	 * Gets the moda group by nego mecha and nego moda.
	 *
	 * @param fundOperation the fund operation
	 * @return the moda group by nego mecha and nego moda
	 * @throws ServiceException the service exception
	 */
	public Long getModaGroupByNegoMechaAndNegoModa(FundsOperation fundOperation) throws ServiceException{
		return manageDepositServiceBean.getModaGroupByNegoMechaAndNegoModa(fundOperation);
	}
	
	/**
	 * Gets the lst holder funds operation by funds operation.
	 *
	 * @param idFundsOperationPk the id funds operation pk
	 * @return the lst holder funds operation by funds operation
	 * @throws ServiceException the service exception
	 */
	public List<HolderFundsOperation> getLstHolderFundsOperationByFundsOperation(Long idFundsOperationPk) throws ServiceException{
		return manageDepositServiceBean.getLstHolderFundsOperationByFundsOperation(idFundsOperationPk);
	}
	
	/**
	 * Validate day is open.
	 *
	 * @param fundOpeTemp the fund ope temp
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateDayIsOpen(FundsOperation fundOpeTemp)throws ServiceException
	{
		boolean isRelated = fundsComponentSingleton.isRelatedCentral(fundOpeTemp.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		if(isRelated){
			if(!fundsComponentSingleton.isDaysIsOpen())
				throw new ServiceException(ErrorServiceType.MANUAL_DEPOSIT_FUNDS_DAY_NOT_OPEN);
		}
		return isRelated;
	}
	
	/**
	 * Gets the international operationby funds.
	 *
	 * @param idFundsInterPk the id funds inter pk
	 * @return the international operationby funds
	 * @throws ServiceException the service exception
	 */
	public FundsInternationalOperation getInternationalOperationbyFunds(Long idFundsInterPk) throws ServiceException{
		return manageDepositServiceBean.getInternationalOperationbyFunds(idFundsInterPk);
	}
	
	/**
	 * Find issuer facade.
	 *
	 * @param idIssuer the id issuer
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerFacade (String idIssuer)throws ServiceException{
		return cashAccountService.findIssuer(idIssuer);
	}
	
	/**
	 * Save deposit.
	 *
	 * @param fundOperation the fund operation
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER)
	public Long saveDeposit (RegisterDepositFundsTO fundOperation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		
		return fundsComponentSingleton.saveDeposit(fundOperation,loggerUser);
	}
	
	/**
	 * Confirm deposit multiple.
	 *
	 * @param lstFundOperation the lst fund operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_CONFIRM)
	public void confirmDepositMultiple(List<FundsOperation> lstFundOperation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());		
		fundsComponentSingleton.confirmDepositMultiple(lstFundOperation, loggerUser);
	}
	
	/**
	 * Confirm deposit.
	 *
	 * @param fundOperation the fund operation
	 * @param bankReference the bank reference
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_CONFIRM)
	public void confirmDeposit (FundsOperation fundOperation, String bankReference)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		fundsComponentSingleton.confirmDeposit(fundOperation,bankReference,loggerUser);
	}
	
	/**
	 * Confirm Tranfers.
	 *
	 * @param fundOperation the fund operation
	 * @param bankReference the bank reference
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_CONFIRM)
	public void confirmTransferCentral (FundsOperation fundOperation, String bankReference)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		fundsComponentSingleton.confirmTransferCentral(fundOperation,loggerUser);
	}
	/**
	 * Reject deposit multiple.
	 *
	 * @param lstFundOperation the lst fund operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_REJECT)
	public void rejectDepositMultiple(List<FundsOperation> lstFundOperation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());		
		fundsComponentSingleton.rejectDepositMultiple(lstFundOperation,loggerUser);
	}
	
	/**
	 * Reject deposit.
	 *
	 * @param fundOperation the fund operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_REJECT)
	public void rejectDeposit (FundsOperation fundOperation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		fundsComponentSingleton.rejectDeposit(fundOperation,loggerUser);
	}
		
	/**
	 * Gets the settlement operation funds.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param operationPart the operation part
	 * @return the settlement operation funds
	 * @throws ServiceException the service exception
	 */
	public Object getSettlementOperationFunds(Long idMechanismOperation, Integer operationPart) throws ServiceException{
		return settlementProcessServiceBean.getSettlementOperationFunds(idMechanismOperation, operationPart);
	}
	
	/**
	 * Gets the participant settlements.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param role the role
	 * @param idParticipant the id participant
	 * @return the participant settlements
	 * @throws ServiceException the service exception
	 */
	public Object getParticipantSettlements(Long idSettlementOperation, Integer role,Long idParticipant) throws ServiceException{
		return settlementProcessServiceBean.getParticipantSettlements(idSettlementOperation,role, idParticipant);
	}
	
	/**
	 * Gets the institution cash account information.
	 *
	 * @param objInstitutionCashAccountTO the obj institution cash account to
	 * @return the institution cash account information
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> getInstitutionCashAccountInformation(InstitutionCashAccountTO objInstitutionCashAccountTO) throws ServiceException{				
		return fundsComponentSingleton.getInstitutionCashAccountInformation(objInstitutionCashAccountTO);
	}
	
	/**
	 * Send deposit lip.
	 *
	 * @param operationDate the operation date
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER)
	public void sendDepositLip(Date operationDate, String currencySelect) throws ServiceException {		
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());			
		List<FundsTransferRegisterTO> lstFundsTransferRegisterTO = new ArrayList<FundsTransferRegisterTO>();
		
		if(currencySelect == null){
			lstFundsTransferRegisterTO.add(manageLipServiceFacade.createDataExtract(CurrencyType.PYG.getCode().toString()));
			lstFundsTransferRegisterTO.add(manageLipServiceFacade.createDataExtract(CurrencyType.USD.getCode().toString()));
		} else if(CurrencyType.PYG.getCodeIso().equals(currencySelect)){
			lstFundsTransferRegisterTO.add(manageLipServiceFacade.createDataExtract(CurrencyType.PYG.getCode().toString()));
		} else if (CurrencyType.USD.getCodeIso().equals(currencySelect)){
			lstFundsTransferRegisterTO.add(manageLipServiceFacade.createDataExtract(CurrencyType.USD.getCode().toString()));
		}
			
		if(Validations.validateListIsNotNullAndNotEmpty(lstFundsTransferRegisterTO)){
			for(FundsTransferRegisterTO objFundsTransferRegisterTO : lstFundsTransferRegisterTO){				
				try {	
					objFundsTransferRegisterTO.getAutomaticSendTypeTO().setCodTypeOperation(ComponentConstant.INTERFACE_BCB_LIP_EXTRACT);
					objFundsTransferRegisterTO.getAutomaticSendTypeTO().setDate(operationDate);
					fundsComponentSingleton.sendDepositLipTx(objFundsTransferRegisterTO, loggerUser);					
				} catch (Exception ex) {					
				}				
			}
		}						
	}
	
}
