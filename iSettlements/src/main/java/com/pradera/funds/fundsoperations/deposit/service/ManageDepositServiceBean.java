package com.pradera.funds.fundsoperations.deposit.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
import com.pradera.core.component.business.to.LipMessageHistoryTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.funds.fundsoperations.deposit.to.InternationalDepositTO;
import com.pradera.funds.fundsoperations.deposit.to.RegisterDepositFundsTO;
import com.pradera.funds.fundsoperations.deposit.to.ReturnsDeposiTO;
import com.pradera.funds.fundsoperations.deposit.to.SearchDepositFundsTO;
import com.pradera.funds.fundsoperations.deposit.to.SettlementDepositTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.ParticipantIntDepositaryStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.FundsInternationalOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.LipMessageHistory;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.ExcecutionType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsProcessesType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.funds.type.SearchOperationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.ModalityGroupDetail;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.model.negotiation.type.InternationalOperationTransferType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageDepositServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
public class ManageDepositServiceBean extends CrudDaoServiceBean{

	
	/** The idepositary setup. */
	@Inject @DepositarySetup 
	IdepositarySetup idepositarySetup;
	
	/**
	 * Gets the funds operation type.
	 *
	 * @return the funds operation type
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<FundsOperationType> getFundsOperationType() throws ServiceException{
		String strQuery = "		SELECT fot" +
							"	FROM FundsOperationType fot" +
							"	WHERE fot.operationClass = :operationClass";
		Query query = em.createQuery(strQuery);
		query.setParameter("operationClass", OperationClassType.RECEPTION_FUND.getCode());
		return (List<FundsOperationType>)query.getResultList();
	}
	
	/**
	 * Search deposit funds operation.
	 *
	 * @param searchDepositFundsTO the search deposit funds to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<FundsOperation> searchDepositFundsOperation(SearchDepositFundsTO searchDepositFundsTO) throws ServiceException{
		String strQuery = "		SELECT fo" +
							"	FROM FundsOperation fo" +
							"	INNER JOIN FETCH fo.institutionCashAccount ica" +
							"	LEFT JOIN FETCH fo.mechanismOperation mechan" +
							//"   INNER JOIN FETCH ica.cashAccountDetailResults cad" +
							"   LEFT JOIN FETCH fo.institutionBankAccount iba" +
							"   LEFT JOIN FETCH fo.participant pa" +
							"	WHERE fo.fundsOperationClass = :deposit "
							//+ " AND cad.indReception = :parIndicatorOne " 
							;
		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getState()) 
				&& !searchDepositFundsTO.getState().equals(GeneralConstants.ZERO_VALUE_INTEGER)
				&& !MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode().equals(searchDepositFundsTO.getState()))
			strQuery += "	AND fo.operationState = :state AND fo.indCentralized <> 1";
		else if(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode().equals(searchDepositFundsTO.getState()))
			strQuery += "	AND fo.operationState = :state AND fo.indCentralized = 1";
		else
			//strQuery += "	AND (fo.indCentralized <> 1 OR (fo.indCentralized = 1 AND fo.operationState = :obs))";
			strQuery += "	AND fo.indCentralized <> 1 ";	
			strQuery += " AND TRUNC(fo.registryDate) >= :initialDate AND TRUNC(fo.registryDate) <= :endDate";	
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getFundOperationGroupSelected())
				&& !searchDepositFundsTO.getFundOperationGroupSelected().equals(GeneralConstants.ZERO_VALUE_INTEGER))
			strQuery += "	AND fo.fundsOperationGroup = :fundsOperationGroup";
		else
			strQuery += "	AND fo.fundsOperationGroup <> :fundsOperationGroup";
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getOperationNumber()) && !searchDepositFundsTO.getOperationNumber().equals(GeneralConstants.ZERO_VALUE_INTEGER) )
			strQuery += "	AND fo.idFundsOperationPk = :idFundsOperationPk";		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getOperationTypeSelected()) && !searchDepositFundsTO.getOperationTypeSelected().equals(GeneralConstants.ZERO_VALUE_LONG))
			strQuery += "	AND fo.fundsOperationType = :operationType";
		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getParticipantSelected()) 
				&& !searchDepositFundsTO.getParticipantSelected().equals(GeneralConstants.ZERO_VALUE_INTEGER))
			strQuery += "	AND pa.idParticipantPk = :participant";
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getIssuer()) 
				&& Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getIssuer().getIdIssuerPk()))
			strQuery += "	AND fo.issuer.idIssuerPk = :issuer";
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getProcessType()) 
				&& !searchDepositFundsTO.getProcessType().equals(GeneralConstants.ZERO_VALUE_LONG))
			if(searchDepositFundsTO.getProcessType().equals(FundsProcessesType.MANUAL.getCode().longValue())
					|| searchDepositFundsTO.getProcessType().equals(FundsProcessesType.AUTOMATIC.getCode().longValue()))
				strQuery += "	AND fo.indAutomatic = :processType";
		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getCurrency()) 
				&& !searchDepositFundsTO.getCurrency().equals(GeneralConstants.ZERO_VALUE_LONG)){
			strQuery += "	AND fo.currency = :parCurrency";
		}	
		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getStrAccountNumber()) 
				&& searchDepositFundsTO.getStrAccountNumber().length() > GeneralConstants.ZERO_VALUE_LONG){
			strQuery += "	AND iba.accountNumber = :parAccountNumber ";
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getMechanismSelected()) 
				&& !searchDepositFundsTO.getMechanismSelected().equals(GeneralConstants.ZERO_VALUE_LONG)){
			strQuery += "	AND ica.negotiationMechanism.idNegotiationMechanismPk = :parNegotiationMechanism";
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getModalitySelected()) 
				&& !searchDepositFundsTO.getModalitySelected().equals(GeneralConstants.ZERO_VALUE_LONG)){
			strQuery += "	AND ica.modalityGroup.idModalityGroupPk = :parModalityGroup";
		}
		
		strQuery += "	order by fo.currency, fo.idFundsOperationPk desc";
		Query query = em.createQuery(strQuery);
		query.setParameter("deposit", OperationClassType.RECEPTION_FUND.getCode());
		//query.setParameter("parIndicatorOne", BooleanType.YES.getCode());
		
			query.setParameter("initialDate", searchDepositFundsTO.getInitialDate());
			query.setParameter("endDate", searchDepositFundsTO.getEndDate());		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getFundOperationGroupSelected())
				&& !searchDepositFundsTO.getFundOperationGroupSelected().equals(GeneralConstants.ZERO_VALUE_INTEGER))
			query.setParameter("fundsOperationGroup", searchDepositFundsTO.getFundOperationGroupSelected());
		else
			query.setParameter("fundsOperationGroup", FundsOperationGroupType.GROUP_OPERATION_CENTRALACCOUNT.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getOperationNumber()) && !searchDepositFundsTO.getOperationNumber().equals(GeneralConstants.ZERO_VALUE_INTEGER))
			query.setParameter("idFundsOperationPk", searchDepositFundsTO.getOperationNumber().longValue());
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getState()) && !searchDepositFundsTO.getState().equals(GeneralConstants.ZERO_VALUE_INTEGER))
			query.setParameter("state", searchDepositFundsTO.getState());
//		else
//			query.setParameter("obs", MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode());		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getOperationTypeSelected())&& !searchDepositFundsTO.getOperationTypeSelected().equals(GeneralConstants.ZERO_VALUE_LONG))
			query.setParameter("operationType", searchDepositFundsTO.getOperationTypeSelected());
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getParticipantSelected()) 
				&& !searchDepositFundsTO.getParticipantSelected().equals(GeneralConstants.ZERO_VALUE_INTEGER))
			query.setParameter("participant", searchDepositFundsTO.getParticipantSelected().longValue());
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getIssuer()) 
				&& Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getIssuer().getIdIssuerPk()))
			query.setParameter("issuer", searchDepositFundsTO.getIssuer().getIdIssuerPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getProcessType()) 
				&& !searchDepositFundsTO.getProcessType().equals(GeneralConstants.ZERO_VALUE_LONG))
		{
			if(searchDepositFundsTO.getProcessType().equals(FundsProcessesType.MANUAL.getCode().longValue()))
				query.setParameter("processType", BooleanType.NO.getCode());
			else if (searchDepositFundsTO.getProcessType().equals(FundsProcessesType.AUTOMATIC.getCode().longValue()))
				query.setParameter("processType", BooleanType.YES.getCode());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getCurrency()) 
				&& !searchDepositFundsTO.getCurrency().equals(GeneralConstants.ZERO_VALUE_LONG)){
			query.setParameter("parCurrency",searchDepositFundsTO.getCurrency().intValue());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getStrAccountNumber()) 
				&& searchDepositFundsTO.getStrAccountNumber().length() > GeneralConstants.ZERO_VALUE_LONG){
			query.setParameter("parAccountNumber", searchDepositFundsTO.getStrAccountNumber());			
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getMechanismSelected()) 
				&& !searchDepositFundsTO.getMechanismSelected().equals(GeneralConstants.ZERO_VALUE_LONG)){
			query.setParameter("parNegotiationMechanism", searchDepositFundsTO.getMechanismSelected());			
		}		
		if(Validations.validateIsNotNullAndNotEmpty(searchDepositFundsTO.getModalitySelected()) 
				&& !searchDepositFundsTO.getModalitySelected().equals(GeneralConstants.ZERO_VALUE_LONG)){
			query.setParameter("parModalityGroup", searchDepositFundsTO.getModalitySelected());
		}
		
		List<FundsOperation> lista = (List<FundsOperation>) query.getResultList();
		
		return lista;
	}

	/**
	 * Gets the funds operation type.
	 *
	 * @param fundsOperationGroup the funds operation group
	 * @param blOnlyRegister the bl only register
	 * @return the funds operation type
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<FundsOperationType> getFundsOperationType(Integer fundsOperationGroup, boolean blOnlyRegister) throws ServiceException{
		StringBuilder strQuery = new StringBuilder(); 
		strQuery.append("		SELECT fot");
		strQuery.append("	FROM FundsOperationType fot");
		strQuery.append("	WHERE fot.operationGroup = :fundsOperationGroup");
		strQuery.append("	AND fot.operationClass = :operationClass");
		if(blOnlyRegister)
			strQuery.append("	AND (fot.excecutionType IS NULL OR fot.excecutionType = :executionType)");
		Query query = em.createQuery(strQuery.toString());
		query.setParameter("fundsOperationGroup", fundsOperationGroup);
		query.setParameter("operationClass", OperationClassType.RECEPTION_FUND.getCode());
		if(blOnlyRegister)
			query.setParameter("executionType", ExcecutionType.MANUAL_FUND.getCode());
		return (List<FundsOperationType>)query.getResultList();
	}
	
	/**
	 * Gets the participants int depositary for deposit.
	 *
	 * @return the participants int depositary for deposit
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipantsIntDepositaryForDeposit() throws ServiceException{
		String strQuery = "		SELECT pa" +
						"	FROM Participant pa" +
						"	WHERE pa.state = :state" +
						"	AND pa.idParticipantPk IN (SELECT pid.id.idParticipantPk " +
						"		FROM ParticipantIntDepositary pid" +
						"		WHERE pid.stateIntDepositary = :statePartiIntDepo" +
						"		GROUP BY pid.id.idParticipantPk)" +
						"	ORDER BY pa.description ASC";
						
		Query query = em.createQuery(strQuery);
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("statePartiIntDepo", ParticipantIntDepositaryStateType.REGISTERED.getCode());
		return (List<Participant>)query.getResultList();
	}
	
	/**
	 * Gets the participants for deposit.
	 *
	 * @param blDirect the bl direct
	 * @return the participants for deposit
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipantsForDeposit(boolean blDirect) throws ServiceException{
		String strQuery = "		SELECT distinct pa" +
							"	FROM Participant pa, ParticipantMechanism pm" +
							"	WHERE pa.state = :state" +							
							"	AND pm.participant.idParticipantPk = pa.idParticipantPk" +
							"	AND pm.stateParticipantMechanism = :stateParticipantMechanism"+
							"	AND pa.indDepositary = :indDepositary";
		if(blDirect)
			strQuery += "	AND pa.accountType = :accountType";
		strQuery += "	ORDER BY pa.mnemonic ASC";
		Query query = em.createQuery(strQuery);
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		query.setParameter("indDepositary", ComponentConstant.ONE);
		if(blDirect)
			query.setParameter("accountType", ParticipantType.DIRECT.getCode());
		return (List<Participant>)query.getResultList();
	}
	
	/**
	 * Gets the issuers for deposit.
	 *
	 * @return the issuers for deposit
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Issuer> getIssuersForDeposit() throws ServiceException{
		String strQuery = "		SELECT issuer" +
							"	FROM Issuer issuer" +
							"	WHERE issuer.stateIssuer = :stateIssuer" +
							"	ORDER BY issuer.businessName ASC";
		Query query = em.createQuery(strQuery);
		query.setParameter("stateIssuer", IssuerStateType.REGISTERED.getCode());
		return (List<Issuer>)query.getResultList();
	}
	
	/**
	 * Gets the negotitation mechanism for participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the negotitation mechanism for participant
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getNegotitationMechanismForParticipant(Long idParticipantPk) throws ServiceException{
		String strQuery = "		SELECT nm" +
							"	FROM NegotiationMechanism nm" +
							"	WHERE nm.idNegotiationMechanismPk in (" +
							"		SELECT pm.mechanismModality.id.idNegotiationMechanismPk" +
							"			FROM ParticipantMechanism pm" +
							"			WHERE pm.participant.idParticipantPk = :idParticipantPk" +
							"			AND pm.stateParticipantMechanism = :stateParticipantMechanism)" +
							"	AND nm.stateMechanism = :stateMechanism";
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("stateMechanism", NegotiationMechanismStateType.ACTIVE.getCode());
		query.setParameter("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		return (List<NegotiationMechanism>)query.getResultList();
	}

	/**
	 * Gets the modality group for nego mechanism.
	 *
	 * @param idNegoMechanismPk the id nego mechanism pk
	 * @return the modality group for nego mechanism
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ModalityGroup> getModalityGroupForNegoMechanism(Long idNegoMechanismPk) throws ServiceException{
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT mg ");
		sb.append(" FROM ModalityGroup mg");
		sb.append(" WHERE mg.idModalityGroupPk in (");
		sb.append("	 SELECT mgd.modalityGroup.idModalityGroupPk");
		sb.append("   FROM ModalityGroupDetail mgd");
		sb.append("   WHERE mgd.negotiationMechanism.idNegotiationMechanismPk = :idNegoMechanismPk)");
		sb.append(" and mg.idModalityGroupPk != :idModalityGroupPk");
		sb.append(" ORDER BY mg.description ASC");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("idNegoMechanismPk", idNegoMechanismPk);
		query.setParameter("idModalityGroupPk", GeneralConstants.MODALITY_GROUP_FOP);
		return (List<ModalityGroup>)query.getResultList();
	}

	/**
	 * Gets the negotiation modality for moda group.
	 *
	 * @param idModaGroupPk the id moda group pk
	 * @return the negotiation modality for moda group
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getNegotiationModalityForModaGroup(Long idModaGroupPk) {
		String strQuery = "		SELECT nm" +
						"	FROM NegotiationModality nm" +
						"	WHERE nm.idNegotiationModalityPk in (" +
						"		SELECT mgd.negotiationModality.idNegotiationModalityPk" +
						"			FROM ModalityGroupDetail mgd" +
						"			WHERE mgd.modalityGroup.idModalityGroupPk = :idModaGroupPk)";
		Query query = em.createQuery(strQuery);
		query.setParameter("idModaGroupPk", idModaGroupPk);
		return (List<NegotiationModality>)query.getResultList();
	}
	
	/**
	 * Gets the cash account for participant settlement gross.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the cash account for participant settlement gross
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<CashAccountDetail> getCashAccountForParticipantSettlementGross(RegisterDepositFundsTO registerDepositFundsTO) throws ServiceException{
		String strQuery = "		SELECT cad" +
				"	FROM CashAccountDetail cad" +
				"	INNER JOIN FETCH cad.institutionCashAccount ica" +
				"	INNER JOIN FETCH cad.institutionBankAccount ins" +
				"	LEFT JOIN FETCH ica.modalityGroup mg" +
				"	LEFT JOIN FETCH ica.negotiationMechanism nm" +
				"	WHERE ica.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk" +
				"	AND ica.modalityGroup.idModalityGroupPk = :idModalityGroupPk" +
				"	AND ica.accountType = :accountType" +
				"	AND ica.participant.idParticipantPk = :idParticipantPk" +
				"	AND ica.accountState = :accountState" +
				"	AND ins.state = :state" +
				"	AND ica.settlementSchema = :settlementSchema" +
				"	AND ica.currency = :currency" +
				"	AND ( cad.indReception=:reception" +
				"	or cad.indSending=:sending)" ;
				//"	AND ica.indRelatedBcrd = :yes";
		Query query = em.createQuery(strQuery);
		query.setParameter("idNegotiationMechanismPk", registerDepositFundsTO.getSettlementGroupTO().getMechanismSelected());
		query.setParameter("idModalityGroupPk", registerDepositFundsTO.getSettlementGroupTO().getModaGroupSelected());
		query.setParameter("accountType", registerDepositFundsTO.getAccountTypeSelected().intValue());
		query.setParameter("idParticipantPk", registerDepositFundsTO.getParticipantSelected().longValue());
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
		query.setParameter("settlementSchema", registerDepositFundsTO.getSettlementGroupTO().getSettlementSchemaType());
		query.setParameter("currency", registerDepositFundsTO.getSettlementGroupTO().getCurrency());
		query.setParameter("reception", BooleanType.YES.getCode());
		query.setParameter("sending", BooleanType.YES.getCode());
		//query.setParameter("yes", BooleanType.YES.getCode());
		return (List<CashAccountDetail>)query.getResultList();
	}

	/**
	 * Gets the lst cash account for participant settlement net.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the lst cash account for participant settlement net
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<CashAccountDetail> getLstCashAccountForParticipantSettlementNet(RegisterDepositFundsTO registerDepositFundsTO) throws ServiceException{
		String strQuery = "		SELECT cad" +
				"	FROM CashAccountDetail cad" +
				"	INNER JOIN FETCH cad.institutionCashAccount ica" +
				"	INNER JOIN FETCH cad.institutionBankAccount ins" +
				"	LEFT JOIN FETCH ica.modalityGroup mg" +
				"	LEFT JOIN FETCH ica.negotiationMechanism nm" +
				"	WHERE ica.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk" +
				"	AND ica.modalityGroup.idModalityGroupPk = :idModalityGroupPk" +
				"	AND ica.accountType = :accountType" +
				"	AND ica.participant.idParticipantPk = :idParticipantPk" +
				"	AND ica.accountState = :accountState" +
				"	AND ins.state = :state" +
				"	AND ica.settlementSchema = :settlementSchema" +
				"	AND ( cad.indReception=:reception" +
				"	or cad.indSending=:sending)" +
				//"	AND ica.indRelatedBcrd = :yes"+
		        "   order by cad.institutionCashAccount.currency asc";
		Query query = em.createQuery(strQuery);
		query.setParameter("idNegotiationMechanismPk", registerDepositFundsTO.getSettlementGroupTO().getMechanismSelected());
		query.setParameter("idModalityGroupPk", registerDepositFundsTO.getSettlementGroupTO().getModaGroupSelected());
		query.setParameter("accountType", registerDepositFundsTO.getAccountTypeSelected().intValue());
		query.setParameter("idParticipantPk", registerDepositFundsTO.getParticipantSelected().longValue());
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
		query.setParameter("settlementSchema", registerDepositFundsTO.getSettlementGroupTO().getSettlementSchemaType());
		query.setParameter("reception", BooleanType.YES.getCode());
		query.setParameter("sending", BooleanType.YES.getCode());
		//query.setParameter("yes", BooleanType.YES.getCode());
		
		List<CashAccountDetail> cashAccountDetails = new ArrayList<CashAccountDetail>();

		HashMap<Long, CashAccountDetail> hashCashAccountDetail = new HashMap();
		
		for (CashAccountDetail cashAcc : (List<CashAccountDetail>)query.getResultList()) {
			hashCashAccountDetail.put(cashAcc.getInstitutionCashAccount().getIdInstitutionCashAccountPk(), cashAcc);
		}
		
		for (Entry keyHash: hashCashAccountDetail.entrySet()) {
			cashAccountDetails.add((CashAccountDetail) keyHash.getValue());
		}

		return cashAccountDetails;
	}
	
	
	/**
	 * Gets the lst cash account for participant.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the lst cash account for participant
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<CashAccountDetail> getLstCashAccountForParticipant(RegisterDepositFundsTO registerDepositFundsTO) throws ServiceException{
		String strQuery = "		SELECT cad" +
						"	FROM CashAccountDetail cad" +
						"	INNER JOIN FETCH cad.institutionCashAccount ica" +
						"	INNER JOIN FETCH cad.institutionBankAccount ins" +
						"	INNER JOIN FETCH ins.providerBank" +
						"	WHERE ica.participant.idParticipantPk = :idParticipantPk" +
						"	AND ica.accountType = :accountType" +
						"	AND ins.state = :state" +
						"	AND ica.accountState = :accountState";		
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", registerDepositFundsTO.getParticipantSelected().longValue());
		query.setParameter("accountType", registerDepositFundsTO.getAccountTypeSelected().intValue());
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
		return (List<CashAccountDetail>)query.getResultList();
	}
	
	/**
	 * Gets the lst cash account for participant international.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the lst cash account for participant international
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<CashAccountDetail> getLstCashAccountForParticipantInternational(
			RegisterDepositFundsTO registerDepositFundsTO) throws ServiceException{
		String strQuery = "		SELECT cad" +
						"	FROM CashAccountDetail cad" +
						"	INNER JOIN FETCH cad.institutionCashAccount ica" +
						"	INNER JOIN FETCH cad.institutionBankAccount ins" +
						"	INNER JOIN FETCH ins.providerBank" +
						"	WHERE ica.participant.idParticipantPk = :idParticipantPk" +
						"	AND ica.accountType = :accountType" +
						"	AND ica.currency = :currency" +
						"	AND ica.accountState = :accountState";		
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", registerDepositFundsTO.getParticipantSelected().longValue());
		query.setParameter("accountType", registerDepositFundsTO.getAccountTypeSelected().intValue());
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		if(Validations.validateIsNullOrEmpty(registerDepositFundsTO.getInternationalDepositTO().getCurrency()))
			query.setParameter("currency", registerDepositFundsTO.getInternationalDepositTO().getCurrencySelected());
		else
			query.setParameter("currency", registerDepositFundsTO.getInternationalDepositTO().getCurrency());
		return (List<CashAccountDetail>)query.getResultList();
	}
	
	/**
	 * Gets the lst cash account for issuer.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the lst cash account for issuer
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<CashAccountDetail> getLstCashAccountForIssuer(RegisterDepositFundsTO registerDepositFundsTO) throws ServiceException{
		String strQuery = "		SELECT cad" +
				"	FROM CashAccountDetail cad" +
				"	INNER JOIN FETCH cad.institutionCashAccount ica" +
				"	INNER JOIN FETCH cad.institutionBankAccount ins" +
				"	INNER JOIN FETCH ins.providerBank" +
				"	WHERE ica.issuer.idIssuerPk = :idIssuerPk" +
				"	AND ica.accountType = :accountType" +
				"	AND ins.state = :state" +
				"	AND ica.accountState = :accountState";
		Query query = em.createQuery(strQuery);
		query.setParameter("idIssuerPk", registerDepositFundsTO.getIssuer().getIdIssuerPk());
		query.setParameter("accountType", registerDepositFundsTO.getAccountTypeSelected().intValue());
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
		return (List<CashAccountDetail>)query.getResultList();
	}
	
	
	
	/**
	 * Gets the lst cash account account for depositary.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the lst cash account account for depositary
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<CashAccountDetail> getLstCashAccountAccountForDepositary(RegisterDepositFundsTO registerDepositFundsTO) 
																								throws ServiceException{
		String strQuery = "		SELECT cad" +
						"	FROM CashAccountDetail cad" +
						"	INNER JOIN FETCH cad.institutionCashAccount ica" +
						"	INNER JOIN FETCH cad.institutionBankAccount ins" +
						"	INNER JOIN FETCH ins.providerBank" +
						"	WHERE ica.participant.idParticipantPk = :idParticipantPk" +
						"	AND ica.accountType = :accountType" +
						"	AND ins.state = :state" +
						"	AND ica.accountState = :accountState";
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idepositarySetup.getIdParticipantDepositary());
		query.setParameter("accountType", registerDepositFundsTO.getAccountTypeSelected().intValue());
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
		return (List<CashAccountDetail>)query.getResultList();
	}
	
	/**
	 * Gets the lst institution bank account for depositary with currency.
	 *
	 * @param registerDepositFundsTO the register deposit funds to
	 * @return the lst institution bank account for depositary with currency
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<CashAccountDetail> getLstInstitutionBankAccountForDepositaryWithCurrency(RegisterDepositFundsTO registerDepositFundsTO) 
																								throws ServiceException{
		String strQuery = "		SELECT cad" +
						"	FROM CashAccountDetail cad" +
						"	INNER JOIN FETCH cad.institutionCashAccount ica" +
						"	INNER JOIN FETCH cad.institutionBankAccount ins" +
						"	INNER JOIN FETCH ins.providerBank" +
						"	WHERE ica.participant.idParticipantPk = :idParticipantPk" +
						"	AND ica.accountType = :accountType" +
						"	AND ica.currency = :currency" +
						"	AND ins.state = :state" +
						"	AND ica.accountState = :accountState";
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idepositarySetup.getIdParticipantDepositary());
		query.setParameter("accountType", registerDepositFundsTO.getAccountTypeSelected().intValue());
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
		query.setParameter("currency", registerDepositFundsTO.getReturnsDepositTO().getCurrencySelected());
		return (List<CashAccountDetail>)query.getResultList();
	}
	
	
	
	
	
	/**
	 * Search operation.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param settlementGroupTO the settlement group to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchOperation(Long idParticipantPk, SettlementDepositTO settlementGroupTO) throws ServiceException{
		String strQuery = "		SELECT ps.settlementOperation.mechanismOperation.paymentReference," +								//0
							"		ps.settlementOperation.mechanismOperation.mechanisnModality.id.idNegotiationModalityPk," +		//1
							"		ps.settlementOperation.mechanismOperation.operationNumber," +									//2
							"		ps.settlementOperation.mechanismOperation.operationDate," +										//3
							"		ps.settlementOperation.settlementCurrency," +											//4					//4
							"		(SELECT pt.description FROM ParameterTable pt " +
							"			WHERE pt.parameterTablePk = ps.settlementOperation.settlementCurrency)," +			//5
							"		ps.settlementAmount," +																	//6
							"		ps.settlementOperation.mechanismOperation.idMechanismOperationPk," +					//7
							"		ps.depositedAmount," +																	//8
							"		TRUNC(ps.settlementOperation.settlementDate)," +										//9
							"		ps.indIncharge," +																		//10
							"		ps.settlementOperation.mechanismOperation.operationState," +							//11
							"		NVL(ps.fundsReference,0)," +															//12
							"		ps.settlementOperation.indExtended," +													//13
							"		ps.settlementOperation.indPrepaid," +													//14
							"		TRUNC(ps.settlementOperation.idSettlementOperationPk)" +								//15
							"	FROM ParticipantSettlement ps" +
							"	WHERE ps.participant.idParticipantPk = :idParticipantPk" +
							"	AND ps.role = :role" +
							"	AND ps.indIncharge = :indIncharge" +					
							"	AND ps.settlementOperation.operationPart = :operationPart" +
							"	AND ps.settlementOperation.mechanismOperation.mechanisnModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk" +
							"	AND ps.settlementOperation.mechanismOperation.settlementType = :settlementType";
		
		if(SearchOperationType.REFERENCE_PAYMENT.getCode().equals(settlementGroupTO.getIdSearchOpeForPkSelected())){
			strQuery += "	AND ps.settlementOperation.mechanismOperation.paymentReference = :paymentReference";
		}else if(SearchOperationType.OPERATION_INFO.getCode().equals(settlementGroupTO.getIdSearchOpeForPkSelected())){
			strQuery += "	AND ps.settlementOperation.mechanismOperation.operationNumber = :operationNumber" +
						"	AND TRUNC(ps.settlementOperation.mechanismOperation.operationDate) = :operationDate" +						
						"	AND ps.settlementOperation.mechanismOperation.mechanisnModality.id.idNegotiationModalityPk = :idNegotiationModalityPk";						
		}
		
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("operationPart", settlementGroupTO.getOperationPartSelected());
		query.setParameter("role", ParticipantRoleType.BUY.getCode());
		query.setParameter("indIncharge", GeneralConstants.ZERO_VALUE_INTEGER);	
		query.setParameter("settlementType", SettlementType.DVP.getCode());
		query.setParameter("idNegotiationMechanismPk", settlementGroupTO.getMechanismSelected());
				
		if(SearchOperationType.REFERENCE_PAYMENT.getCode().equals(settlementGroupTO.getIdSearchOpeForPkSelected())){
			query.setParameter("paymentReference", settlementGroupTO.getReferencePayment().trim());
		}else if(SearchOperationType.OPERATION_INFO.getCode().equals(settlementGroupTO.getIdSearchOpeForPkSelected())){
			query.setParameter("operationNumber", settlementGroupTO.getOperationNumber());
			query.setParameter("operationDate", settlementGroupTO.getOperationDate());
			query.setParameter("idNegotiationModalityPk", settlementGroupTO.getNegoModalitySelected());						
		}	
		return (List<Object[]>)query.getResultList();
	}
	
	/**
	 * Gets the ind related bcrd.
	 *
	 * @param idInstitutionCashAccountPk the id institution cash account pk
	 * @return the ind related bcrd
	 * @throws ServiceException the service exception
	 */
	public Integer getIndRelatedBCRD(Long idInstitutionCashAccountPk) throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("	SELECT ica.indRelatedBcrd");
		strQuery.append("	FROM InstitutionCashAccount ica");
		strQuery.append("	WHERE ica.idInstitutionCashAccountPk = :idInstitutionCashAccountPk");
		Query query = em.createQuery(strQuery.toString());
		query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
		return new Integer(query.getSingleResult().toString());
	}
	
	/**
	 * Gets the begin end day.
	 *
	 * @return the begin end day
	 * @throws ServiceException the service exception
	 */
	public BeginEndDay getBeginEndDay() throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT bed");
			strQuery.append("	FROM BeginEndDay bed");
			strQuery.append("	WHERE TRUNC(bed.processDay) = :day");
			strQuery.append("	AND bed.indSituation = :yes");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("day", CommonsUtilities.currentDate());
			query.setParameter("yes", BooleanType.YES.getCode());
			return (BeginEndDay) query.getSingleResult();	
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Gets the cash account for central account.
	 *
	 * @param currency the currency
	 * @return the cash account for central account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount getCashAccountForCentralAccount(Integer currency) throws ServiceException{
		try {
			String strQuery = "		SELECT ica" +
							"	FROM InstitutionCashAccount ica" +
							"	WHERE ica.currency = :currency" +
							"	AND ica.accountState = :accountState" +
							"	AND ica.accountType = :accountType";
			Query query = em.createQuery(strQuery);
			query.setParameter("currency", currency);
			query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("accountType", AccountCashFundsType.CENTRALIZING.getCode());
			InstitutionCashAccount objInstitutionCashAccount= (InstitutionCashAccount) query.getSingleResult();
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccount))
				return objInstitutionCashAccount;
		} catch (NoResultException e) {
			return null;
		}		
		return null;
	}
	
	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Bank> getLstBank() throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("	SELECT b");
		strQuery.append("		FROM Bank b");
		strQuery.append("		WHERE b.bankType = :bankType");
		strQuery.append("		AND b.state = :state");
		strQuery.append("		ORDER BY b.description ASC");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("bankType", BankType.COMMERCIAL.getCode());
		parameters.put("state", MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
		return (List<Bank>)findListByQueryString(strQuery.toString(), parameters);
	}
	
	/**
	 * Get Holder .
	 *
	 * @param idHolderPk id Holder
	 * @return object holder
	 * @throws ServiceException service exception
	 */
	public Holder getHolderServiceBean(Long idHolderPk) throws ServiceException{
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM Holder h WHERE h.idHolderPk = :idHolderPk");
	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	  
	
		 
	      return (Holder) query.getSingleResult();
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}
  }
	
	/**
	 * Gets the lst holder cash movement.
	 *
	 * @param returnsDeposiTO the returns deposi to
	 * @return the lst holder cash movement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderCashMovement> getLstHolderCashMovement(ReturnsDeposiTO returnsDeposiTO) throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("	SELECT hcm");
		strQuery.append("		FROM HolderCashMovement hcm");
		strQuery.append("		INNER JOIN FETCH hcm.holder");
		strQuery.append("		INNER JOIN FETCH hcm.bank");
		strQuery.append("		WHERE hcm.bank.idBankPk = :idBankPk");
		strQuery.append("		AND hcm.holder.idHolderPk = :idHolderPk");
		strQuery.append("		AND hcm.currency = :currency");
		strQuery.append("		AND TRUNC(hcm.movementDate) = :payedDate");
		strQuery.append("		AND hcm.fundsOperationType.idFundsOperationTypePk in (:lstFundsOperationType)");
		//strQuery.append("		order by hcm.currency, hcm.HolderCashMovement.idHolderCashMovementPk  desc");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idBankPk", returnsDeposiTO.getIdBankPkSelected());
		parameters.put("idHolderPk", returnsDeposiTO.getHolder().getIdHolderPk());
		parameters.put("currency", returnsDeposiTO.getCurrencySelected());
		parameters.put("payedDate", returnsDeposiTO.getPayedDate());
		List<Long> lstFundsOperationType = new ArrayList<Long>();
		lstFundsOperationType.add(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode());
		lstFundsOperationType.add(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode());
		lstFundsOperationType.add(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode());
		lstFundsOperationType.add(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode());
		lstFundsOperationType.add(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode());
		parameters.put("lstFundsOperationType", lstFundsOperationType);
		return (List<HolderCashMovement>)findListByQueryString(strQuery.toString(), parameters);
	}
	
	/**
	 * Gets the lst international depositary.
	 *
	 * @return the lst international depositary
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalDepository> getLstInternationalDepositary() throws ServiceException{
		String strQuery = "		SELECT ide" +
						"	FROM InternationalDepository ide" +
						"	WHERE ide.stateIntDepositary = :state";
		Query query = em.createQuery(strQuery);
		query.setParameter("state", InternationalDepositoryStateType.ACTIVE.getCode());
		return (List<InternationalDepository>)query.getResultList();
	}
	
	/**
	 * Search international operation.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param internationalGroupTO the international group to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchInternationalOperation(Long idParticipantPk,
			InternationalDepositTO internationalGroupTO) throws ServiceException{
		String strQuery = "		SELECT iop.currency," +											//0
						"		(SELECT pt.description FROM ParameterTable pt " +				
						"			WHERE pt.parameterTablePk=iop.currency)," +					//1
						"			iop.settlementAmount," +									//2
						"			iop.securities.idSecurityCodePk," +								//3
						"			iop.settlementDate," +										//4
						"			iop.tradeDepository.idInternationalDepositoryPk," +			//5
						"			iop.tradeDepository.description," +							//6
						"			iop.settlementDepository.idInternationalDepositoryPk," +	//7
						"			iop.settlementDepository.description," +					//8
						"			iop.idInternationalOperationPk," +							//9
						"			iop.internationalParticipant.idInterParticipantPk," +		//10
						"			iop.internationalParticipant.description," +				//11
						"			iop.depositAmount," +										//12
						"			NVL(iop.fundsReference,0)," +								//13
						"			iop.operationState" +										//14
						"	FROM InternationalOperation iop" +
						"	WHERE iop.localParticipant.idParticipantPk = :idParticipantPk" +
						"	AND iop.operationNumber = :operationNumber" +
						"	AND TRUNC(iop.operationDate) = :operationDate" +
						"	AND iop.transferType = :transferType";
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("operationDate", internationalGroupTO.getOperationDate());
		query.setParameter("operationNumber", internationalGroupTO.getOperationNumber());
		query.setParameter("transferType", InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode());
		return (List<Object[]>)query.getResultList();
	}
	
	/**
	 * Search lst international operation.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param internationalGroupTO the international group to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalOperation> searchLstInternationalOperation(Long idParticipantPk,
			InternationalDepositTO internationalGroupTO) throws ServiceException{
		String strQuery = "		SELECT iop" +
						"	FROM InternationalOperation iop" +
						"	WHERE iop.settlementDepository.idInternationalDepositoryPk = :settlementDepository" +
						"	AND iop.tradeDepository.idInternationalDepositoryPk = :tradeDepository" +
						"	AND iop.currency = :currency" +
						"	AND TRUNC(iop.settlementDate) >= :initialSettlement" +
						"	AND TRUNC(iop.settlementDate) <= :endSettlement" +
						"	AND TRUNC(iop.registerDate) >= :initialRegistry" +
						"	AND TRUNC(iop.registerDate) <= :endRegistry" +
						"	AND iop.localParticipant.idParticipantPk = :idParticipantPk" +
						"	AND NVL(iop.fundsReference,0) <> :fundsReference" +
						"	AND iop.transferType = :transferType" +
						"	AND iop.operationState = :operationState";
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("fundsReference", AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
		query.setParameter("settlementDepository", internationalGroupTO.getIdIntDepoLiquiPkSelected());
		query.setParameter("tradeDepository", internationalGroupTO.getIdIntDepoNegoPkSelected());
		query.setParameter("currency", internationalGroupTO.getCurrencySelected());
		query.setParameter("initialSettlement", internationalGroupTO.getSettlementInitial());
		query.setParameter("endSettlement", internationalGroupTO.getSettlementEnd());
		query.setParameter("initialRegistry", internationalGroupTO.getRegistryInitial());
		query.setParameter("endRegistry", internationalGroupTO.getRegistryEnd());
		query.setParameter("transferType", InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode());
		query.setParameter("operationState", InternationalOperationStateType.CONFIRMADA.getCode());
		return (List<InternationalOperation>)query.getResultList();
	}
	
	/**
	 * Gets the fund operation detail.
	 *
	 * @param idFundsOperationPk the id funds operation pk
	 * @return the fund operation detail
	 * @throws ServiceException the service exception
	 */
	public FundsOperation getFundOperationDetail(Long idFundsOperationPk) throws ServiceException{
		try {
			String strQuery = "		SELECT fo" +
							"	FROM FundsOperation fo" +
							"	INNER JOIN FETCH fo.institutionCashAccount ica" +
							"	LEFT JOIN FETCH ica.negotiationMechanism nm" +
							"	LEFT JOIN FETCH ica.modalityGroup mg" +
							"	LEFT JOIN FETCH fo.participant" +
							"	LEFT JOIN FETCH fo.issuer" +
							"	LEFT JOIN FETCH fo.bank" +
							"	LEFT JOIN FETCH fo.mechanismOperation mo" +
							"	LEFT JOIN FETCH mo.mechanisnModality" +
							"	WHERE fo.idFundsOperationPk = :idFundsOperationPk";
			Query query = em.createQuery(strQuery);
			query.setParameter("idFundsOperationPk", idFundsOperationPk);
			FundsOperation fundsOperation = (FundsOperation)query.getSingleResult();
			if(Validations.validateIsNotNullAndNotEmpty(fundsOperation)){				 
				return fundsOperation;
			}				
		} catch (NoResultException e) {
			return null;
		}		
		return null;
	}
	
	/**
	 * Gets the cash account detail.
	 *
	 * @param idInstitutionCashAccountPk the id institution cash account pk
	 * @return the cash account detail
	 * @throws ServiceException the service exception
	 */
	public CashAccountDetail getCashAccountDetail(Long idInstitutionCashAccountPk) throws ServiceException{
		try {
			String strQuery = "		SELECT cad" +
					"	FROM CashAccountDetail cad" +
					"	INNER JOIN FETCH cad.institutionCashAccount ica" +
					"	INNER JOIN FETCH cad.institutionBankAccount ins" +
					"	INNER JOIN FETCH ins.providerBank" +
					"	LEFT JOIN FETCH ica.modalityGroup mg" +
					"	LEFT JOIN FETCH ica.negotiationMechanism nm" +
					"	LEFT JOIN FETCH ins.providerBank" +
					"	WHERE ica.idInstitutionCashAccountPk = :idInstitutionCashAccountPk";
			Query query = em.createQuery(strQuery);
			query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
//			CashAccountDetail cashAccountDetail = (CashAccountDetail) query.getSingleResult();
			CashAccountDetail cashAccountDetail = null;
			
			List<CashAccountDetail> lista = query.getResultList();
			
			if(lista != null && lista.size() > 0){
				 cashAccountDetail = lista.get(0);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(cashAccountDetail))
				return cashAccountDetail;
		} catch (NoResultException e) {
			return null;
		}		
		return null;
	}
	
	/**
	 * Gets the moda group by nego mecha and nego moda.
	 *
	 * @param fundOperation the fund operation
	 * @return the moda group by nego mecha and nego moda
	 * @throws ServiceException the service exception
	 */
	public Long getModaGroupByNegoMechaAndNegoModa(FundsOperation fundOperation) throws ServiceException{
		try {
			String strQuery = "		SELECT mgd" +
						"	FROM ModalityGroupDetail mgd" +
						"	INNER JOIN FETCH mgd.modalityGroup mg" +
						"	WHERE mgd.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk" +
						"	AND mgd.negotiationModality.idNegotiationModalityPk = :idNegotiationModalityPk" + 	
						"	AND mg.settlementSchema= :settlementSchema";
			Query query = em.createQuery(strQuery);
			query.setParameter("idNegotiationMechanismPk", fundOperation.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk());
			query.setParameter("idNegotiationModalityPk", fundOperation.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk());
			query.setParameter("settlementSchema", fundOperation.getMechanismOperation().getMechanisnModality().getSettlementSchema());
			ModalityGroupDetail obj=(ModalityGroupDetail)query.getSingleResult();
			if(Validations.validateIsNotNullAndNotEmpty(obj))
				return obj.getModalityGroup().getIdModalityGroupPk();
		} catch (NoResultException e) {
			return null;
		}
		return null;
	}
	
	/**
	 * Gets the lst holder funds operation by funds operation.
	 *
	 * @param idFundsOperationPk the id funds operation pk
	 * @return the lst holder funds operation by funds operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderFundsOperation> getLstHolderFundsOperationByFundsOperation(Long idFundsOperationPk) throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("	SELECT hfo");
		strQuery.append("	FROM HolderFundsOperation hfo");
		strQuery.append("	INNER JOIN FETCH hfo.holder");
		strQuery.append("	WHERE hfo.fundsOperation.idFundsOperationPk = :idFundsOperationPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idFundsOperationPk", idFundsOperationPk);
		return (List<HolderFundsOperation>)findListByQueryString(strQuery.toString(), parameters);
	}
	
	/**
	 * Gets the total deposited.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param operationPart the operation part
	 * @return the total deposited
	 * @throws ServiceException the service exception
	 */
	public Integer getTotalDeposited(Long idMechanismOperationPk, Integer operationPart) throws ServiceException{
		try {
			String strQuery = "		SELECT COUNT(*)" +
					"	FROM ParticipantSettlement ps" +
					"	WHERE ps.settlementOperation.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk" +
					"	AND ps.fundsReference = :deposited" +
					"	AND ps.role = :role" +
					"	AND ps.settlementOperation.operationPart = :operationPart";
			Query query = em.createQuery(strQuery);
			query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
			query.setParameter("deposited", AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
			query.setParameter("operationPart", operationPart);
			query.setParameter("role", ParticipantRoleType.BUY.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return new Integer(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return null;
		}
		return null;
	}
	
	/**
	 * Gets the cash participant.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param operationPart the operation part
	 * @return the cash participant
	 */
	public Integer getCashParticipant(Long idMechanismOperationPk, Integer operationPart) {
		try {
			String strQuery = "		SELECT COUNT(*)" +
					"	FROM ParticipantSettlement ps" +
					"	WHERE ps.settlementOperation.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk" +
					"	AND ps.role = :role" +
					"	AND ps.settlementOperation.operationPart = :operationPart";
			Query query = em.createQuery(strQuery);
			query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
			query.setParameter("operationPart", operationPart);
			query.setParameter("role", ParticipantRoleType.BUY.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return new Integer(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return null;
		}
		return null;
	}
	
	/**
	 * Gets the fund operation mirror.
	 *
	 * @param idRefFundsOperationPk the id ref funds operation pk
	 * @return the fund operation mirror
	 * @throws ServiceException the service exception
	 */
	public FundsOperation getFundOperationMirror(Long idRefFundsOperationPk) throws ServiceException{
		try {
			String strQuery = "		SELECT fo" +
							"	FROM FundsOperation fo" +
							"	INNER JOIN fetch fo.institutionCashAccount" +
							"	LEFT JOIN fetch fo.fundsOperation for" +
							"	WHERE fo.idFundsOperationPk = :idRefFundsOperationPk";
			Query query = em.createQuery(strQuery);
			query.setParameter("idRefFundsOperationPk", idRefFundsOperationPk);
			FundsOperation objFundsOperation= (FundsOperation)query.getSingleResult();
			return objFundsOperation;
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	/**
	 * Gets the international operationby funds.
	 *
	 * @param idFundsInterPk the id funds inter pk
	 * @return the international operationby funds
	 * @throws ServiceException the service exception
	 */
	public FundsInternationalOperation getInternationalOperationbyFunds(Long idFundsInterPk) throws ServiceException{
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select fio from FundsInternationalOperation fio ");
		stringBuilder.append("inner join fetch fio.internationalOperation io ");
		stringBuilder.append("inner join fetch fio.fundsOperation ");
		stringBuilder.append("inner join fetch io.tradeOperation ");
		stringBuilder.append("inner join fetch io.securities ");
		stringBuilder.append("inner join fetch io.settlementDepository ");
		stringBuilder.append("inner join fetch io.tradeDepository ");
		stringBuilder.append("inner join fetch io.internationalParticipant ");
		stringBuilder.append("where ");
		stringBuilder.append("fio.idFundsInterOperPk = :idFundsInterPk");
		Query query = em.createQuery(stringBuilder.toString());
		query.setParameter("idFundsInterPk", idFundsInterPk);
		
		FundsInternationalOperation objFundsInternationalOperation = null;
		objFundsInternationalOperation = (FundsInternationalOperation) query.getSingleResult();
		return objFundsInternationalOperation;
	}
	
	/**
	 * Find issuer.
	 *
	 * @param idIssuer the id issuer
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuer(String idIssuer)throws ServiceException{
		try{
		return find(Issuer.class, idIssuer);
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Gets the institution cash account information.
	 *
	 * @param objInstitutionCashAccountTO the obj institution cash account to
	 * @return the institution cash account information
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionCashAccount> getInstitutionCashAccountInformation(InstitutionCashAccountTO objInstitutionCashAccountTO) throws ServiceException{
		List<InstitutionCashAccount> lstSettlementAccounts = new ArrayList<InstitutionCashAccount>();			
		try{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" Select ica ");
			stringBuilder.append(" from InstitutionCashAccount ica ");
			stringBuilder.append(" left join fetch ica.cashAccountDetailResults cad ");
			stringBuilder.append(" left join fetch cad.institutionBankAccount iba ");
			stringBuilder.append(" left join fetch ica.issuer isu ");
			stringBuilder.append(" left join fetch ica.participant part ");
			stringBuilder.append(" left join fetch iba.bank pba ");
			stringBuilder.append(" where 1=1 ");
			
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getIdInstitutionCashAccountPk())){
				stringBuilder.append(" and ica.idInstitutionCashAccountPk = :idInstitutionCashAccount " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getIdParticipantFk())){
				stringBuilder.append(" and part.idParticipantPk = :idParticipant " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getCurrency())){
				stringBuilder.append(" and iba.currency = :currency " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getAccountNumber())){
				stringBuilder.append(" and iba.accountNumber = :accountNumber " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getIdBcbCode())){
				stringBuilder.append(" and pba.IdBcbCode = :bcbCode " );
			}			
			stringBuilder.append(" and ica.accountState = :state ");
			stringBuilder.append(" and cad.indSending = :indSending");
			
			Query query = em.createQuery(stringBuilder.toString());		
			
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getIdInstitutionCashAccountPk())){
				query.setParameter("idInstitutionCashAccount", objInstitutionCashAccountTO.getIdInstitutionCashAccountPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getIdParticipantFk())){
				query.setParameter("idParticipant", objInstitutionCashAccountTO.getIdParticipantFk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getCurrency())){
				query.setParameter("currency", objInstitutionCashAccountTO.getCurrency());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getAccountNumber())){
				query.setParameter("accountNumber", objInstitutionCashAccountTO.getAccountNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objInstitutionCashAccountTO.getIdBcbCode())){
				query.setParameter("bcbCode", objInstitutionCashAccountTO.getIdBcbCode());
			}
			query.setParameter("state", CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("indSending", GeneralConstants.ONE_VALUE_INTEGER);
			lstSettlementAccounts = query.getResultList();
			return lstSettlementAccounts;
		} catch(NoResultException ex){
			   return null;
		}		
	}
	
	/**
	 * Gets the lip message history validate.
	 *
	 * @param objLipMessageHistoryTO the obj lip message history to
	 * @return the lip message history validate
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<LipMessageHistory> getLipMessageHistoryValidate(LipMessageHistoryTO objLipMessageHistoryTO) throws ServiceException{
		List<LipMessageHistory> lstLipMessageHistory = new ArrayList<LipMessageHistory>();		
		try {			
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" Select lmh ");
			stringBuilder.append(" from LipMessageHistory lmh ");			
			stringBuilder.append(" where 1=1 ");			
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getRequestNumber())){
				stringBuilder.append(" and lmh.requestNumber = :requestNumber " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getAccountNumber())){
				stringBuilder.append(" and lmh.accountNumber = :accountNumber " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationType())){
				stringBuilder.append(" and lmh.operationType = :opertionType " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getCurrency())){
				stringBuilder.append(" and lmh.currency = :currency " );
			}			
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getDescription())){
				stringBuilder.append(" and lmh.description = :description " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationDate())){
				stringBuilder.append(" and trunc(lmh.operationDate) = trunc(:operationDate) " );
			}			
			Query query = em.createQuery(stringBuilder.toString());	
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getRequestNumber())){
				query.setParameter("requestNumber", objLipMessageHistoryTO.getRequestNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getAccountNumber())){
				query.setParameter("accountNumber", objLipMessageHistoryTO.getAccountNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationType())){
				query.setParameter("opertionType", objLipMessageHistoryTO.getOperationType());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getCurrency())){
				query.setParameter("currency", objLipMessageHistoryTO.getCurrency());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getDescription())){
				query.setParameter("description", objLipMessageHistoryTO.getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationDate())){
				query.setParameter("operationDate", objLipMessageHistoryTO.getOperationDate());
			}			
			lstLipMessageHistory = query.getResultList();
			return lstLipMessageHistory;			
		} catch(NoResultException ex){
			   return null;
		}
	}
	
}
