package com.pradera.funds.fundsoperations.retirement.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderCashMovementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class HolderCashMovementTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The id holder cash movement pk. */
	private long idHolderCashMovementPk;

	/** The movement date. */
	private Date movementDate;

	/** The funds operation type. */
	private Long fundsOperationType;

	/** The funds operation. */
	private Long fundsOperation;

	/** The bank. */
	private Long bank;

	/** The holder. */
	private Long holder;

	/** The full name. */
	private String fullName;

	/** The currency. */
	private Integer currency;

	/** The holder account bank. */
	private Long holderAccountBank;

	/** The bank account number. */
	private String bankAccountNumber;

	/** The bank account type. */
	private Integer bankAccountType;
	
	/** The movement state. */
	private Integer movementState;
	
	/** The id benefit payment pk. */
	private Long idBenefitPaymentPk;
	
	/**
	 * Gets the id holder cash movement pk.
	 *
	 * @return the id holder cash movement pk
	 */
	public long getIdHolderCashMovementPk() {
		return idHolderCashMovementPk;
	}

	/**
	 * Sets the id holder cash movement pk.
	 *
	 * @param idHolderCashMovementPk the new id holder cash movement pk
	 */
	public void setIdHolderCashMovementPk(long idHolderCashMovementPk) {
		this.idHolderCashMovementPk = idHolderCashMovementPk;
	}

	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	/**
	 * Gets the funds operation type.
	 *
	 * @return the funds operation type
	 */
	public Long getFundsOperationType() {
		return fundsOperationType;
	}

	/**
	 * Sets the funds operation type.
	 *
	 * @param fundsOperationType the new funds operation type
	 */
	public void setFundsOperationType(Long fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}

	/**
	 * Gets the funds operation.
	 *
	 * @return the funds operation
	 */
	public Long getFundsOperation() {
		return fundsOperation;
	}

	/**
	 * Sets the funds operation.
	 *
	 * @param fundsOperation the new funds operation
	 */
	public void setFundsOperation(Long fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public Long getBank() {
		return bank;
	}

	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(Long bank) {
		this.bank = bank;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Long getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Long holder) {
		this.holder = holder;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the holder account bank.
	 *
	 * @return the holder account bank
	 */
	public Long getHolderAccountBank() {
		return holderAccountBank;
	}

	/**
	 * Sets the holder account bank.
	 *
	 * @param holderAccountBank the new holder account bank
	 */
	public void setHolderAccountBank(Long holderAccountBank) {
		this.holderAccountBank = holderAccountBank;
	}

	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return bankAccountType;
	}

	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	/**
	 * Gets the movement state.
	 *
	 * @return the movement state
	 */
	public Integer getMovementState() {
		return movementState;
	}

	/**
	 * Sets the movement state.
	 *
	 * @param movementState the new movement state
	 */
	public void setMovementState(Integer movementState) {
		this.movementState = movementState;
	}

	/**
	 * Gets the id benefit payment pk.
	 *
	 * @return the id benefit payment pk
	 */
	public Long getIdBenefitPaymentPk() {
		return idBenefitPaymentPk;
	}

	/**
	 * Sets the id benefit payment pk.
	 *
	 * @param idBenefitPaymentPk the new id benefit payment pk
	 */
	public void setIdBenefitPaymentPk(Long idBenefitPaymentPk) {
		this.idBenefitPaymentPk = idBenefitPaymentPk;
	}

	
	
}