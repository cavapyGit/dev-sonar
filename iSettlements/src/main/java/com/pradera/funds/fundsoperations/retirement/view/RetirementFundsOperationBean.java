package com.pradera.funds.fundsoperations.retirement.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.funds.bank.accounts.to.SearchBankAccountsTO;
import com.pradera.funds.effective.accounts.facade.ManageEffectiveAccountsFacade;
import com.pradera.funds.fundoperations.monitoring.facade.MonitoringFundOperationsServiceFacade;
import com.pradera.funds.fundoperations.util.PropertiesConstants;
import com.pradera.funds.fundsoperations.retirement.facade.RetirementFundsOperationServiceFacade;
import com.pradera.funds.fundsoperations.retirement.to.HolderCashMovementTO;
import com.pradera.funds.fundsoperations.retirement.to.HolderFundsOperationTO;
import com.pradera.funds.fundsoperations.retirement.to.InternationalOperationTO;
import com.pradera.funds.fundsoperations.retirement.to.MechanismOperationTO;
import com.pradera.funds.fundsoperations.retirement.to.RetirementFundsOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.BenefitPaymentFile;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.SwiftMessage;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsRetirementType;
import com.pradera.model.funds.type.HolderCashMovementStateType;
import com.pradera.model.funds.type.HolderFundsOperationStateType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.funds.type.SearchOperationType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.settlements.core.facade.SettlementProcessFacade;

// TODO: Auto-generated Javadoc
/**
 * The Class RetirementFundsOperationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class RetirementFundsOperationBean extends GenericBaseBean implements Serializable{
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst retirement funds operation group type. */
	
	private List<OperationPartType>       lstOperationPartType;
	
	/** The lst operation types. */
	private List<FundsOperationType>	  lstOperationTypes;
	
	/** The lst operation types to save. */
	private List<FundsOperationType>	  lstOperationTypesToSave;
	
	/** The lst participant. */
	private List<Participant> 			  lstParticipant;
	
	/** The lst issuer. */
	private List<Issuer> 				  lstIssuer;
	
	/** The lst bank. */
	private List<Bank> 					  lstBank;
	
	/** The lst international depositories. */
	private List<InternationalDepository> lstInternationalDepositories;	
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism>    lstNegotiationMechanism;
	
	/** The lst modality group. */
	private List<ModalityGroup>           lstModalityGroup;
	
	/** The lst negotiation modality. */
	private List<NegotiationModality>     lstNegotiationModality;
	
	/** The lst holder cash movement combo. */
	private List<HolderCashMovement>      lstHolderCashMovementCombo;
	
	/** The lst holder funds operation. */
	private List<HolderFundsOperation>    lstHolderFundsOperation;
	
	/** The lst holder account bank. */
	private List<HolderAccountBank>    	  lstHolderAccountBank;
	
	/** The lst holder cash movement for benefit. */
	private List<HolderCashMovement>	  lstHolderCashMovementForBenefit;
	
	/** The lst international operation. */
	private GenericDataModel<InternationalOperation>   lstInternationalOperation;
	
	/** The lst institution cash account. */
	private GenericDataModel<InstitutionCashAccount>   lstInstitutionCashAccount;
	
	/** The lst institution cash account bcrd. */
	private GenericDataModel<InstitutionCashAccount>   lstInstitutionCashAccountBcrd;
	
	/** The lst institution cash account target. */
	private GenericDataModel<InstitutionCashAccount>   lstInstitutionCashAccountTarget;
	
	/** The lst institution cash account to. */
	private GenericDataModel<InstitutionCashAccountTO> lstInstitutionCashAccountTO;
	
	/** The lst holder cash movement. */
	private GenericDataModel<HolderCashMovement>       lstHolderCashMovement;
	
	/** The lst retirement funds operation. */
	private GenericDataModel<FundsOperation>           lstRetirementFundsOperation;
	
	/** The lst retirement funds operation to. */
	private GenericDataModel<RetirementFundsOperationTO>lstRetirementFundsOperationTO;
	
	/** The dm holder cash movement for benefit. */
	private GenericDataModel<HolderCashMovement>	   dmHolderCashMovementForBenefit;
	
	/** The gm lst participant position. */
	private GenericDataModel<ParticipantPosition>      gmLstParticipantPosition;
	
	/** The array participant position. */
	private ParticipantPosition [] arrayParticipantPosition; 
	
	/**  OBJECTS *. */
	private FundsOperation 		 fundsOperationToSave;
	
	/** The funds operation to search. */
	private FundsOperation 		 fundsOperationToSearch;
	
	/** The funds operation selected. */
	private FundsOperation 		 fundsOperationSelected;
	
	/** The issuer. */
	private Issuer 				 issuer;
	
	/** The bank to save. */
	private Bank 				 bankToSave;
	
	/** The holder cash movement to save. */
	private HolderCashMovement 	 holderCashMovementToSave;
	
	/** The holder funds operation to save. */
	private HolderFundsOperation holderFundsOperationToSave;
	
	/** The holder account bank to save. */
	private HolderAccountBank    holderAccountBankToSave;
	
	/** The institution cash account to save. */
	private InstitutionCashAccount   institutionCashAccountToSave; 
	
	/** The participant settlement. */
	private ParticipantSettlement     participantSettlement;
	
	/** The international operation to save. */
	private InternationalOperation   internationalOperationToSave;
	
	/** The international depository tosave. */
	private InternationalDepository  internationalDepositoryTosave;
	
	/** The institution cash account to to save. */
	private InstitutionCashAccountTO institutionCashAccountTOToSave;
	
	/** The obj institution bank accounts type. */
	private Integer objInstitutionBankAccountsType;
	
	/** The lst institution bank accounts type. */
	private List<ParameterTable> lstInstitutionBankAccountsTypeReturn;
	
	/** The holder devolution filter. */
	private Holder                   holderDevolutionFilter;
	
	/** The participant to save. */
	private Participant              participantToSave;
	
	/** The cash account type desc. */
	private String 					 cashAccountTypeDesc;
	
	/** The benefit payment file. */
	private BenefitPaymentFile		 benefitPaymentFile;
	
	/** The retirement funds operation to. */
	private RetirementFundsOperationTO retirementFundsOperationTO;
	
	/** The swift message file. */
	private SwiftMessage             swiftMessageFile;
	
	/**  PARAMETERS LIST *. */
	private List<ParameterTable> accountsFundTypes;
	
	/** The lst search operation type. */
	private List<ParameterTable> lstSearchOperationType;
	
	/** The lst funds retirement type. */
	private List<ParameterTable> lstFundsRetirementType;
	
	/** The lst currency types. */
	private List<ParameterTable> lstCurrencyTypes;
	
	/** The lst funds operation states. */
	private List<ParameterTable> lstFundsOperationStates;
	
	/** The lst retirement funds operation group type. */
	private List<ParameterTable> lstRetirementFundsOperationGroupType;
	
	/**  INDICATORS OBJECTS *. */
	private Boolean indIssuer                           = Boolean.FALSE;
	
	/** The ind participant. */
	private Boolean indParticipant                      = Boolean.FALSE;
	
	/** The ind settlement retirement. */
	private Boolean indSettlementRetirement             = Boolean.FALSE;
	
	/** The ind settlement gross. */
	private Boolean indSettlementGross                  = Boolean.FALSE;
	
	/** The ind settlement net. */
	private Boolean indSettlementNet                    = Boolean.FALSE;
	
	/** The ind benefit retirement. */
	private Boolean indBenefitRetirement                = Boolean.FALSE;
	
	/** The ind taxes retirement. */
	private Boolean indTaxesRetirement                  = Boolean.FALSE;
	
	/** The ind institution. */
	private Boolean indInstitution                      = Boolean.FALSE;
	
	/** The ind int operations retirement. */
	private Boolean indIntOperationsRetirement          = Boolean.FALSE;
	
	/** The ind search int operations. */
	private Boolean indSearchIntOperations              = Boolean.FALSE;
	
	/** The ind return operation. */
	private Boolean indReturnOperation                  = Boolean.FALSE;
	
	/** The ind reserve retirement rights returned. */
	private Boolean indReserveRetirementRightsReturned  = Boolean.FALSE;
	
	/** The ind payment retirement rights returned. */
	private Boolean indPaymentRetirementRightsReturned  = Boolean.FALSE;
	
	/** The ind retirement rights returned searched. */
	private Boolean indRetirementRightsReturnedSearched = Boolean.FALSE;
	
	/** The ind retirement payment to blocked holder. */
	private Boolean indRetirementPaymentToBlockedHolder = Boolean.FALSE;
	
	/** The ind payment retirement rights ban. */
	private Boolean indPaymentRetirementRightsBan		= Boolean.FALSE;
	
	/** The ind detail view. */
	private Boolean indDetailView                       = Boolean.FALSE;
	
	/** The ind confirm clicked. */
	private Boolean indConfirmClicked                   = Boolean.FALSE; 
	
	/** The ind reject clicked. */
	private Boolean indRejectClicked                    = Boolean.FALSE; 
	
	/** The ind cash operation part. */
	private Boolean indCashOperationPart                = Boolean.FALSE; 
	
	/** The ind benefit automatic. */
	private Boolean indBenefitAutomatic		            = Boolean.FALSE; 
	
	/** The ind holder benefit detail. */
	private Boolean indHolderBenefitDetail 				= Boolean.FALSE;
	
	/** The ind show bcrd ins account. */
	private Boolean indShowBcrdInsAccount 				= Boolean.FALSE;
	
	/** The ind guarantees retirement. */
	private Boolean indGuaranteesRetirement 			= Boolean.FALSE;
	
	/** The ind return retirement. */
	private Boolean indReturnRetirement 				= Boolean.FALSE;
	
	/** The ind return retirement. */
	private Boolean indCorporativeProcessReturns 		= Boolean.FALSE;
	
	/** The ind retirement automatic. */
	private Boolean indRetirementAutomatic              = Boolean.FALSE;	
	
	/** The lst funds operation. */
	List<FundsOperation> lstFundsOperation;
	
	/** The lst send lipe. */
	List<BooleanType> lstSendLipe;
	
	/** The ind send lip. */
	private Integer indSendLip;
	
	/** The view cbo send lip. */
	private boolean viewCboSendLip;
	
	
	/** The payment reference type. */
	private Integer paymentReferenceType = SearchOperationType.REFERENCE_PAYMENT.getCode();
	
	/** The operation info type. */
	private Integer operationInfoType    = SearchOperationType.OPERATION_INFO.getCode();
	
	/** The over paid type. */
	private Integer overPaidType         = FundsRetirementType.BY_OVERPAID.getCode();
	
	/** The retirement type. */
	private Integer retirementType;
	
	/** The operation part selected. */
	private Integer operationPartSelected;
	
	/** The retirement amount. */
	private Double  retirementAmount = 0D;
	//private StreamedContent fileDownload; 
	
	/** The mechanism negotiation pk. */
	private Long mechanismNegotiationPk;
	
	/** The mechanism modality group pk. */
	private Long mechanismModalityGroupPk;
	
	/** The search operation type. */
	private Integer searchOperationType;
	
	/** The cash account type. */
	private Integer cashAccountType;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;
	
	/** The settlement schema description. */
	private String settlementSchemaDescription;
	
	/** The bic code desc. */
	private String bicCodeDesc;
	
	/** The mnemonic. */
	private String mnemonic = "";
	
	/** The institution name. */
	private String institutionName = "EDV";
	
	/** The now time. */
	private Date nowTime = CommonsUtilities.currentDate();	
	
	/** The international operation to. */
	private InternationalOperationTO internationalOperationTO;
	
	/** The funds operation service facade. */
	@EJB
	private RetirementFundsOperationServiceFacade fundsOperationServiceFacade;
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The manage effective accounts facade. */
	@EJB
	ManageEffectiveAccountsFacade manageEffectiveAccountsFacade;
	
	/** The monitor cash account facade. */
	@EJB
	private MonitoringFundOperationsServiceFacade monitorCashAccountFacade;
	
	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
		
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		if(Validations.validateIsNull(fundsOperationToSave)){
			fundsOperationToSave=new FundsOperation();
			fundsOperationToSave.setParticipant(null);
			fundsOperationToSave.setInstitutionCashAccount(new InstitutionCashAccount());							
		}
		if(Validations.validateIsNull(holderFundsOperationToSave)){
			holderFundsOperationToSave = new HolderFundsOperation();
		}
		if(Validations.validateIsNull(holderAccountBankToSave)){
			holderAccountBankToSave = new HolderAccountBank();
		}
		if(Validations.validateIsNull(fundsOperationToSearch)){
			fundsOperationToSearch=new FundsOperation();
			fundsOperationToSearch.setParticipant(new Participant());
			fundsOperationToSearch.setInstitutionCashAccount(new InstitutionCashAccount());	
			fundsOperationToSearch.setInitialDate(CommonsUtilities.currentDate());
			fundsOperationToSearch.setEndDate(CommonsUtilities.currentDate());
		}
		
		if(Validations.validateIsNull(internationalOperationTO)){
			internationalOperationTO=new InternationalOperationTO();
			internationalOperationTO.setInitialDate(CommonsUtilities.currentDate());
			internationalOperationTO.setEndDate(CommonsUtilities.currentDate());
			
			internationalOperationTO.setInitialSettlementDate(CommonsUtilities.currentDate());
			internationalOperationTO.setEndSettlementDate(CommonsUtilities.currentDate());
		}
		if(Validations.validateIsNull(internationalOperationToSave)){
			internationalOperationToSave=new InternationalOperation();
			internationalOperationToSave.setSettlementDepository(new InternationalDepository());
			internationalOperationToSave.setTradeDepository(new InternationalDepository());
		}
		
		if(Validations.validateIsNull(mechanismOperation)){
			mechanismOperation=new MechanismOperation();
			mechanismOperation.setMechanisnModality(new MechanismModality());
			mechanismOperation.getMechanisnModality().setNegotiationModality(new NegotiationModality());
		}
		if(Validations.validateIsNull(holderDevolutionFilter))
			holderDevolutionFilter=new Holder();
		
		if(Validations.validateIsNull(issuer))
			issuer=new Issuer();
		if(Validations.validateIsNull(bankToSave))
			bankToSave=new Bank();
		if(Validations.validateIsNull(participantToSave))
			participantToSave=new Participant();
				
		//Initializing groups types
		if(Validations.validateListIsNullOrEmpty(lstOperationPartType)){
			lstOperationPartType=OperationPartType.list;
		}
		try{
			/** VALIDATING PARTICIPANT LIST IS NULL TO INITIALIZE **/
			if(Validations.validateListIsNullOrEmpty(lstParticipant))
				lstParticipant=fundsOperationServiceFacade.findAllParticipants();
			
			//Initializing groups types
			if(Validations.validateListIsNullOrEmpty(lstRetirementFundsOperationGroupType)){
				lstRetirementFundsOperationGroupType=fundsOperationServiceFacade.getParameterList(MasterTableType.MASTER_TABLE_FUND_OPERATION_GROUP_RETIREMENT);
			}
			
			/** VALIDATION IF HOLDER CASH MOVEMENT IS NULL **/
			if(Validations.validateIsNull(holderCashMovementToSave)){
				holderCashMovementToSave=new HolderCashMovement();
				holderCashMovementToSave.setHolder(new Holder());
			}
			/** VALIDATION IF NEGOTIATION MODALITY LIST IS EMPTY OR NULL **/
			if(Validations.validateListIsNullOrEmpty(lstInternationalDepositories))
				lstInternationalDepositories=this.fundsOperationServiceFacade.findInternationalDepositories();
			
			/** VALIDATION IF NEGOTIATION MODALITY LIST IS EMPTY OR NULL **/
			if(Validations.validateListIsNullOrEmpty(lstNegotiationModality))
				lstNegotiationModality=fundsOperationServiceFacade.searchLstNegotiationModalities();			
			
			/** VALIDATING IF ACCOUNTS FUND TYPES PARAMETER LIST IS NULL TO INITIALIZE **/
			if (Validations.validateListIsNullOrEmpty(accountsFundTypes)) 
				accountsFundTypes=fundsOperationServiceFacade.getParameterList(MasterTableType.ACCOUNT_CASH_FUNDS_TYPE);
			
			/** VALIDATING SEARCH OPERATION TYPE LIST IS NULL TO INITIALIZE **/
			if (Validations.validateListIsNullOrEmpty(lstSearchOperationType)) 
				lstSearchOperationType=fundsOperationServiceFacade.getParameterList(MasterTableType.FUNDS_RETIREMENT_SEARCH_OPERATION);	
			
			if (Validations.validateListIsNullOrEmpty(lstFundsRetirementType)) 
				lstFundsRetirementType=fundsOperationServiceFacade.getParameterList(MasterTableType.FUNDS_RETIREMENT_TYPE);
			
			
			/** VALIDATING ISSUER LIST IS NULL TO INITIALIZE **/
			if(Validations.validateListIsNullOrEmpty(lstIssuer))
				lstIssuer=fundsOperationServiceFacade.findAIssuers();
			
			/** VALIDATING BANK LIST IS NULL TO INITIALIZE **/
			if(Validations.validateListIsNullOrEmpty(lstBank))
				lstBank=fundsOperationServiceFacade.findBanks();
			
			/** VALIDATING IF CURRENCY TYPES PARAMETER LIST IS NULL TO INITIALIZE **/
			if (Validations.validateListIsNullOrEmpty(lstCurrencyTypes)) {
				lstCurrencyTypes=monitorCashAccountFacade.getParameterList(MasterTableType.CURRENCY);
			}
			
			/** VALIDATING IF FUNDS OPERATION STATES PARAMETER LIST IS NULL TO INITIALIZE **/
			if (Validations.validateListIsNullOrEmpty(lstFundsOperationStates)) {
				lstFundsOperationStates=fundsOperationServiceFacade.getParameterList(MasterTableType.MASTER_TABLE_FUND_OPERATION_STATE);
			}
			//Calling service to get holidays list
			listHolidays();
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnConfirmView(true);	
		privilegeComponent.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Clear all objects.
	 */
	public void clearAllObjects(){
		//fundsOperationToSearch.setParticipant(new Participant());
		fundsOperationToSearch=new FundsOperation();	
		fundsOperationToSearch.setInstitutionCashAccount(new InstitutionCashAccount());	
		fundsOperationToSearch.setInitialDate(CommonsUtilities.currentDate());
		fundsOperationToSearch.setEndDate(CommonsUtilities.currentDate());
		lstOperationTypes=null;
		lstRetirementFundsOperation = null;
		clean();
	}
	//Clean method
	/**
	 * Clean.
	 */
	public void clean(){
		fundsOperationToSave=new FundsOperation();	
		participantToSave= new Participant();
		cleanSomeObjects();		
		indDetailView = Boolean.FALSE;
		lstHolderCashMovement=null;
		indBenefitAutomatic = Boolean.FALSE; 
		indShowBcrdInsAccount = Boolean.FALSE;
		indHolderBenefitDetail = Boolean.FALSE;
		indReturnRetirement = Boolean.FALSE;
		lstRetirementFundsOperationTO = null;
		dmHolderCashMovementForBenefit = null;
		//fileDownload = null;
		retirementFundsOperationTO = null;
		holderDevolutionFilter=new Holder();
		lstOperationTypesToSave=null;
		this.setLstInstitutionCashAccount(null);
		objInstitutionBankAccountsType=null;
		participantToSave.setIdParticipantPk(null);
		this.setIssuer(new Issuer());
	}
	
	/**
	 * Clean some objects.
	 */
	public void cleanSomeObjects(){
		fundsOperationToSave.setParticipant(null);
		fundsOperationToSave.setIssuer(null);
		fundsOperationToSave.setBank(null);
		issuer=new Issuer();
		cleanFromParticipant();
		indParticipant=Boolean.FALSE;
		indIssuer = Boolean.FALSE;
		indInstitution = Boolean.FALSE;
		indIntOperationsRetirement = Boolean.FALSE;
		cashAccountTypeDesc="";
		settlementSchemaDescription="";
		indTaxesRetirement=Boolean.FALSE;
		lstInstitutionCashAccountTarget = null;
		indReturnOperation = Boolean.FALSE;
		institutionCashAccountToSave = new InstitutionCashAccount();
		indPaymentRetirementRightsReturned = Boolean.FALSE;
		indRetirementRightsReturnedSearched = Boolean.FALSE;
		indRetirementPaymentToBlockedHolder=Boolean.FALSE;
		indPaymentRetirementRightsBan = Boolean.FALSE;
		indConfirmClicked=Boolean.FALSE;		
		indRejectClicked=Boolean.FALSE;
	}
	
	/**
	 * Clean from issuer.
	 */
	public void cleanFromIssuer(){
		fundsOperationToSave.setParticipant(null);
		participantToSave=new Participant();
		fundsOperationToSave.setInstitutionCashAccount(new InstitutionCashAccount());
		internationalOperationToSave=new InternationalOperation();
		cleanFromParticipant();
		indBenefitRetirement = Boolean.FALSE;
	}
	
	/**
	 * Clean from participant.
	 */
	public void cleanFromParticipant(){
		//Cleaning mechanism operation
		mechanismOperation=new MechanismOperation();
		mechanismOperation.setMechanisnModality(new MechanismModality());
		mechanismOperation.getMechanisnModality().setNegotiationModality(new NegotiationModality());
		participantSettlement=null;		
		lstInstitutionCashAccount=null;
		retirementType 					= null;
		indSettlementRetirement 		= Boolean.FALSE;
		indSettlementGross 				= Boolean.FALSE;
		indCashOperationPart 			= Boolean.FALSE;
		indSettlementNet 				= Boolean.FALSE;
		mechanismNegotiationPk 			= null;
		mechanismModalityGroupPk 		= null;
		institutionCashAccountToSave 	= null;	
		settlementSchemaDescription = "";
		indBenefitRetirement 			= Boolean.FALSE;	
		lstModalityGroup=null;
		bankToSave=new Bank();
		mechanismNegotiationPk = null;
		mechanismModalityGroupPk = null;
		institutionCashAccountTOToSave = new InstitutionCashAccountTO();	
		internationalOperationToSave.setSettlementDepository(new InternationalDepository());
		internationalOperationToSave.setTradeDepository(new InternationalDepository());
		indSearchIntOperations=Boolean.FALSE;
		/*** CLEANING INTERNATIONAL OPERATION ***/
		lstInstitutionCashAccountTO=null;
		lstInternationalOperation=null;
		internationalOperationTO.setCurrency(null);
		internationalOperationTO.setEndDate(nowTime);
		internationalOperationTO.setInitialDate(nowTime);
		internationalOperationTO.setInitialSettlementDate(nowTime);
		internationalOperationTO.setEndSettlementDate(nowTime);
		internationalOperationTO.setTradeDepository(null);
		internationalOperationTO.setSettlementDepository(null);
		
	}
	//Go to some view

	
	/**
	 * Change search operation type.
	 */
	public void changeSearchOperationType(){
		//Cleaning mechanism operation
		mechanismOperation = new MechanismOperation();
		mechanismOperation.setMechanisnModality(new MechanismModality());
		mechanismOperation.getMechanisnModality().setNegotiationModality(new NegotiationModality());
		participantSettlement=null;		
	}
	
	/**
	 * Onchange payment reference.
	 */
	@LoggerAuditWeb
	public void onchangePaymentReference(){
		try{
			//GETTING PAYMENT REFERENCE FROM MECHANISM OPERATION
			String paymentReference = mechanismOperation.getPaymentReference();
			// GETTING PARTICIPANT ID
			Long idParticipantPk = participantToSave.getIdParticipantPk();
			// VALIDATING PAYMENT REFERENCE NOT NULL OR EMPTY
			if(Validations.validateIsNotNullAndNotEmpty(paymentReference)){
				operationPartSelected = fundsOperationToSave.getOperationPart();
				
				if(Validations.validateIsNotNull(operationPartSelected) && operationPartSelected.equals(0)){
					String propertieConstant = PropertiesConstants.FUNDS_OPERATION_OPERATION_PART_NOT_SELECTED;												
					showDialogMessage(propertieConstant,"alertDialog");
					return;
				}
				
				// CREATING NEW MECHANISM OPERATION TO
				MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
				//SETTING FILTERS
				mechanismOperationTO.setPaymentReference(paymentReference);
				mechanismOperationTO.setBuyerParticipant(idParticipantPk);
				mechanismOperationTO.setIdNegotiationMechanismPk(mechanismNegotiationPk);
				/*Long negotiationModality = this.fundsOperationServiceFacade.searchNegotiationModality(mechanismNegotiationPk, mechanismModalityGroupPk);
				mechanismOperationTO.setIdNegotiationModalityPk(negotiationModality);*/
				mechanismOperationTO.setIdModalityGroupPk(mechanismModalityGroupPk);
				//GETTING MECHANISM OPERATION FROM REFERENCE.
//				MechanismOperation mechanismOperationTmp = this.fundsOperationServiceFacade.findMechanismOperation(mechanismOperationTO);
//				//GETTING MECHANISM OPERATION ID
//				Long idMechanismOperationFk = mechanismOperationTmp.getIdMechanismOperationPk();
//				// VALIDATING IF MECHANISM OPERATION ID IS NULL TO SHOW ERROR MESSAGE
//				if(Validations.validateIsNull(idMechanismOperationFk)){
//					String propertieConstant = PropertiesConstants.FUNDS_OPERATION_MECHANISM_OPERATION_NOT_FOUND;												
//					this.showDialogMessage(propertieConstant,"alertDialog");
//					return;
//				}
//				
//				
//				// SETTING MECHANISM OPERATION
//				this.setMechanismOperation(mechanismOperationTmp);
				
				
				//GETTING PARTICIPANT OPERATION FROM MECHANISM ID AND PARTICIPANT ID
				participantSettlement=fundsOperationServiceFacade.findParticipantSettlement(mechanismOperationTO,operationPartSelected);
				
				MechanismOperation mechanismOperationTmp = 	participantSettlement.getSettlementOperation().getMechanismOperation();		
				
				
				if(Validations.validateIsNotNull(participantSettlement) 
						&& Validations.validateIsNotNull(participantSettlement.getIdParticipantSettlementPk())){
					if(Validations.validateIsNotNull(participantSettlement.getFundsReference()) 
							&& participantSettlement.getFundsReference().equals(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode())){
						retirementType = overPaidType;						
						//SETTING OPERATION AMOUNT
						fundsOperationToSave.setOperationAmount(participantSettlement.getOverpaidAmount());
						fundsOperationToSave.setRetirementType(retirementType);
					}
					if(participantSettlement.getDepositedAmount().compareTo(BigDecimal.ZERO) <= 0){
						String propertieConstant = PropertiesConstants.FUNDS_OPERATIONS_PARTICIPANT_OPERATION_NOT_DEPOSIT_AMOUNT;												
						showDialogMessage(propertieConstant,"alertDialog");
						participantSettlement.setIdParticipantSettlementPk(null);
					}
					if(Validations.validateIsNotNull(participantSettlement.getSettlementOperation().getMechanismOperation())){
						//Setting Mechanism Operation Found
						mechanismOperation=mechanismOperationTmp;
					}
				}
				if(Validations.validateIsNotNull(participantSettlement) 
				&& Validations.validateIsNull(participantSettlement.getIdParticipantSettlementPk())){
					participantSettlement=new ParticipantSettlement();
					String propertieConstant = null;												
					if(searchOperationType.equals(SearchOperationType.REFERENCE_PAYMENT.getCode())){
						propertieConstant = PropertiesConstants.FUNDS_OPERATION_REFERENCE_PAYMENT_NOT_FOUND;
					}
					else{
						propertieConstant = PropertiesConstants.FUNDS_OPERATION_MECHANISM_OPERATION_NOT_FOUND;
					}
					showDialogMessage(propertieConstant,"alertDialog");
					participantSettlement.setIdParticipantSettlementPk(null);
					mechanismOperation.setPaymentReference(null);
				}
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	// METHOD RETIREMENT TYPE CHANGE
	/**
	 * Change retirement type.
	 */
	public void changeRetirementType(){
		//CLEANING OPERATION AMOUNT
		fundsOperationToSave.setOperationAmount(null);
		if(Validations.validateIsNull(participantSettlement)){
			String propertieConstant = PropertiesConstants.FUNDS_OPERATION_PARTICIPANT_OPERATION_NULL;												
			showDialogMessage(propertieConstant,"alertDialog");
			retirementType=null;
			return;
		}
		if(Validations.validateIsNotNull(participantSettlement.getFundsReference())
				&& participantSettlement.getFundsReference().equals(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode())){
			if(!retirementType.equals(overPaidType)){
				String propertieConstant = PropertiesConstants.FUNDS_OPERATION_WITH_FUNDS_COMPLAICE;												
				showDialogMessage(propertieConstant,"alertDialog");
				retirementType=overPaidType;
				return;
			}
		}
		fundsOperationToSave.setRetirementType(retirementType);
		// VALIDATING RETIREMENT TYPE
		if(retirementType.equals(overPaidType)){
			if(Validations.validateIsNull(participantSettlement.getOverpaidAmount()) || participantSettlement.getOverpaidAmount().compareTo(BigDecimal.ZERO) <= 0){
				String propertieConstant = PropertiesConstants.FUNDS_OPERATION_MECHANISM_OPERATION_NOT_OVER_PAID_AMOUNT;												
				showDialogMessage(propertieConstant,"alertDialog");
				retirementType=null;
				return;
			}
			//SETTING OPERATION AMOUNT
			fundsOperationToSave.setOperationAmount(participantSettlement.getOverpaidAmount().setScale(2, RoundingMode.HALF_UP));
		}else {
			//SETTING OPERATION AMOUNT
			fundsOperationToSave.setOperationAmount(participantSettlement.getSettlementAmount().setScale(2, RoundingMode.HALF_UP));
		}
		
	}
	
	/**
	 * Gets the document.
	 *
	 * @param fileName the file name
	 * @param file the file
	 * @return the document
	 */
	public StreamedContent getDocument(String fileName, byte[]file){
								
//		InputStream stream = new ByteArrayInputStream(file);
//		fileDownload = new DefaultStreamedContent(stream, "pdf", fileName); 
//
//		return fileDownload;
		return null;
	 }
	/**
	 * Change operation info.
	 */
	@LoggerAuditWeb
	public void changeOperationInfo(){
		participantSettlement=null;
		Long idParticipantPk = participantToSave.getIdParticipantPk();
		//SETTIG OPERATION PART
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk())){
			Long negotiationId = mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
			for(NegotiationModality modality : lstNegotiationModality){
				if(modality.getIdNegotiationModalityPk().equals(negotiationId)){
					if(modality.getIndTermSettlement().equals(BooleanType.NO.getCode())){
						fundsOperationToSave.setOperationPart(OperationPartType.CASH_PART.getCode());
						indCashOperationPart = Boolean.TRUE;
					}else{
						indCashOperationPart = Boolean.FALSE;
					}
					break;
				}
			}
		}
		
		try{
			// Validating fields to search mechanism operation
			if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk()) &&
			   Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getOperationDate()) &&
			   Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getOperationNumber()) ){
				//Creating Mechanism Operation TO
				MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();		
				//Setting Participant Buyer
				mechanismOperationTO.setBuyerParticipant(idParticipantPk);
				//Setting modalitypk
				mechanismOperationTO.setIdNegotiationModalityPk(mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk());
				//Setting Operation Date
				mechanismOperationTO.setOperationDate(mechanismOperation.getOperationDate());
				//Setting Operation Number
				mechanismOperationTO.setOperationNumber(mechanismOperation.getOperationNumber());
				mechanismOperationTO.setIdModalityGroupPk(mechanismModalityGroupPk);
				//GETTING MECHANISM OPERATION FROM OPERATION INFO.
				 
		
				operationPartSelected = fundsOperationToSave.getOperationPart();
				//GETTING PARTICIPANT OPERATION FROM MECHANISM ID AND PARTICIPANT ID
				participantSettlement=fundsOperationServiceFacade.findParticipantSettlement(mechanismOperationTO,operationPartSelected);
				
				MechanismOperation mechanismOperationTmp = 	participantSettlement.getSettlementOperation().getMechanismOperation();		
				
				
				
				if(Validations.validateIsNotNull(participantSettlement) 
						&& Validations.validateIsNotNull(participantSettlement.getIdParticipantSettlementPk())){
					if(Validations.validateIsNotNull(participantSettlement.getFundsReference()) 
							&& participantSettlement.getFundsReference().equals(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode())){
						retirementType = overPaidType;						
						//SETTING OPERATION AMOUNT
						fundsOperationToSave.setOperationAmount(participantSettlement.getOverpaidAmount());
						fundsOperationToSave.setRetirementType(retirementType);
						
					}
					if(participantSettlement.getDepositedAmount().compareTo(BigDecimal.ZERO) <= 0){
						String propertieConstant = PropertiesConstants.FUNDS_OPERATIONS_PARTICIPANT_OPERATION_NOT_DEPOSIT_AMOUNT;												
						showDialogMessage(propertieConstant,"alertDialog");
						participantSettlement.setIdParticipantSettlementPk(null);
					}
					if(Validations.validateIsNotNull(participantSettlement.getSettlementOperation().getMechanismOperation())){
						//Setting Mechanism Operation Found
						mechanismOperation=mechanismOperationTmp;
					}
				}
				if(Validations.validateIsNotNull(participantSettlement) 
				&& Validations.validateIsNull(participantSettlement.getIdParticipantSettlementPk())){
					participantSettlement=new ParticipantSettlement();
					String propertieConstant = null;
					if(searchOperationType.equals(SearchOperationType.REFERENCE_PAYMENT.getCode())){
						propertieConstant = PropertiesConstants.FUNDS_OPERATION_REFERENCE_PAYMENT_NOT_FOUND;
					}
					else{
						propertieConstant = PropertiesConstants.FUNDS_OPERATION_MECHANISM_OPERATION_NOT_FOUND;
					}												
					showDialogMessage(propertieConstant,"alertDialog");
					participantSettlement.setIdParticipantSettlementPk(null);
					fundsOperationToSave.setFundOperationNumber(null);
				}
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Change motive.
	 */
	public void changeMotive(){
		fundsOperationToSave.setBank(new Bank());
		indReturnOperation                  = Boolean.FALSE;
		indReserveRetirementRightsReturned  = Boolean.FALSE;
		indPaymentRetirementRightsReturned  = Boolean.FALSE;
		indRetirementRightsReturnedSearched = Boolean.FALSE;
		indPaymentRetirementRightsBan       = Boolean.FALSE;
		indRetirementPaymentToBlockedHolder=Boolean.FALSE;
		lstInstitutionCashAccountTarget     = null;
		institutionCashAccountToSave        = new InstitutionCashAccount();
		retirementAmount=0D;
		this.setLstInstitutionCashAccount(null);
		participantToSave.setIdParticipantPk(null);
		this.setIssuer(new Issuer());
		
		//Setting transfer type depending on motives
		if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.INTERNATIONAL_SECURITIES_RECEPTION_WITHDRAWL.getCode())){
			internationalOperationTO.setTransferType(ParameterOperationType.RECEPTION_INTERNATIONAL_SECURITIES.getCode());
			internationalOperationToSave.setTransferType(ParameterOperationType.RECEPTION_INTERNATIONAL_SECURITIES.getCode());
			indParticipant = Boolean.TRUE;
		}else if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.INTERNATIONAL_SECURITIES_SEND_WITHDRAWL.getCode())){
			internationalOperationTO.setTransferType(ParameterOperationType.SENDING_INTERNATIONAL_SECURITIES.getCode());
			internationalOperationToSave.setTransferType(ParameterOperationType.SENDING_INTERNATIONAL_SECURITIES.getCode());
			indParticipant = Boolean.TRUE;
		}else if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())){
			indReturnOperation = Boolean.TRUE;
//			InstitutionCashAccountTO filter = new InstitutionCashAccountTO();					
//			//Setting the cash account type
//			filter.setAccountType(cashAccountType);
//					
//			lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(this.findInstitutionCashAccountWithBankInfo(filter));
//			
//			filter.setAccountType(AccountCashFundsType.CENTRALIZING.getCode());
//			lstInstitutionCashAccountBcrd =new GenericDataModel<InstitutionCashAccount>(this.findInstitutionCashAccountWithBankInfo(filter));
//			indReserveRetirementRightsReturned = Boolean.TRUE;
			
		}else if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
			indReturnOperation = Boolean.TRUE;
			indPaymentRetirementRightsReturned = Boolean.TRUE;						
		}else if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())){
			indReturnOperation = Boolean.TRUE;
			indRetirementPaymentToBlockedHolder = Boolean.TRUE;
		}else if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
			indReturnOperation = Boolean.TRUE;
			indPaymentRetirementRightsBan = Boolean.TRUE;
		}	
		else if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT_MANUAL.getCode())){
			indRetirementAutomatic = Boolean.TRUE;
			gmLstParticipantPosition = null;
			indSettlementRetirement = Boolean.FALSE;			
		} else if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.FUNDS_SETTLEMENT_WITHDRAWL.getCode())){
			indRetirementAutomatic = Boolean.FALSE;
			gmLstParticipantPosition = null;
		}
	}
	
	/**
	 * Setting institution cash account.
	 */
	public void settingInstitutionCashAccount(){
		if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
			fundsOperationToSave.setInstitutionCashAccount(institutionCashAccountToSave);					
		}
	}
	
	/**
	 * Search rights devolutions info.
	 */
	public void searchRightsDevolutionsInfo(){
		try {
			fundsOperationToSave.setOperationAmount(null);
			retirementAmount=0D;
			Integer currency = fundsOperationToSave.getCurrency();
			Long idHolderPk = holderDevolutionFilter.getIdHolderPk();
			
			InstitutionCashAccountTO filter = new InstitutionCashAccountTO();					
			//Setting the cash account type
			filter.setAccountType(cashAccountType);
			filter.setCurrency(currency);
			//
			lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(this.findInstitutionCashAccountWithBankInfo(filter));
			
			HolderCashMovementTO cashFilter = new HolderCashMovementTO();
			cashFilter.setCurrency(currency);
			cashFilter.setHolder(idHolderPk);
			cashFilter.setMovementState(HolderCashMovementStateType.RESERVED.getCode());
			/** VALIDATING OPERATION TYPE **/
			if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){				
				cashFilter.setFundsOperationType(ParameterFundsOperationType.UNPAYED_BENEFITS_RETURN_DEPOSIT.getCode());				
			}else if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())){				
				cashFilter.setFundsOperationType(ParameterFundsOperationType.BLOCK_HOLDER_DEPOSIT_RETENTION.getCode());				
			}else if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){				
				cashFilter.setFundsOperationType(ParameterFundsOperationType.BAN_DEPOSIT_RETENTION.getCode());
				
			}
			lstHolderCashMovementCombo = fundsOperationServiceFacade.searchHolderCashMovement(cashFilter);
						
			lstHolderAccountBank = fundsOperationServiceFacade.searchHolderAccountBankLst(idHolderPk);
			//Filling holder cash movement list
			lstHolderCashMovement=new GenericDataModel<HolderCashMovement>(lstHolderCashMovementCombo);
			indRetirementRightsReturnedSearched = Boolean.TRUE;
			indPaymentRetirementRightsReturned = Boolean.TRUE;
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	//DEVULITIONS SUM TOTAL
	/**
	 * Sum totals.
	 */
	public void sumTotals(){
		retirementAmount = 0D;
		//Creating Holder Funds Detail
		lstHolderFundsOperation = new ArrayList<HolderFundsOperation>();
		for(HolderCashMovement holderCashMovement : this.getLstHolderCashMovement().getDataList()){
			if(holderCashMovement.getSelected()){
				retirementAmount+= holderCashMovement.getHolderAmount().doubleValue();
				HolderFundsOperation fundsOperationtmp = new HolderFundsOperation();

				
				//Setting Holder Cash Movement
				fundsOperationtmp.setRelatedHolderCashMovement(holderCashMovement);				
				//Adding to list
				lstHolderFundsOperation.add(fundsOperationtmp);
			}
		}
		fundsOperationToSave.setOperationAmount(new BigDecimal(retirementAmount));
	}
	
	/**
	 * Find institution cash account with bank info.
	 *
	 * @param filter the filter
	 * @return the list kunai
	 */
	public List<InstitutionCashAccount> findInstitutionCashAccountWithBankInfo(InstitutionCashAccountTO filter){
		List<InstitutionCashAccount> institutionCashAccountsTmp = null;
		try {
			//Searching institution cash accounts
			institutionCashAccountsTmp = fundsOperationServiceFacade.searchInstitutionCashAccountNoModalities(filter);
			//Searching banks
			for(InstitutionCashAccount institutionCashAccountTmp : institutionCashAccountsTmp){
				
				InstitutionCashAccount institutionCashAccount = manageEffectiveAccountsFacade.getInstitutionCashAccountData(institutionCashAccountTmp.getIdInstitutionCashAccountPk());
				InstitutionBankAccount bankAccount= institutionCashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount();
				if(Validations.validateIsNotNull(bankAccount) && Validations.validateIsNotNull(bankAccount.getAccountNumber())){					
					institutionCashAccountTmp.setBankDescription(bankAccount.getProviderBank().getDescription());
					institutionCashAccountTmp.setBankAccount(bankAccount.getAccountNumber());
				}
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return institutionCashAccountsTmp;
	}
	
	/**
	 * Onchange retirement funds operation group.
	 *
	 * @param view the view
	 */
	@LoggerAuditWeb
	public void onchangeRetirementFundsOperationGroup(String view){
		try{
			List<FundsOperationType> fundsOperationTypesTmp = null;
			lstInstitutionCashAccount=null;
			indParticipant=Boolean.FALSE;
			indInstitution = Boolean.FALSE;
			indTaxesRetirement= Boolean.FALSE;
			indGuaranteesRetirement=Boolean.FALSE;
			indReturnRetirement= Boolean.FALSE;
			this.setLstInstitutionCashAccount(null);
			objInstitutionBankAccountsType=null;
			participantToSave.setIdParticipantPk(null);
			this.setIssuer(new Issuer());
			if(view.equals("register")){
				cleanSomeObjects();
				fundsOperationTypesTmp = fundsOperationServiceFacade.searchFundsOperationsTypes(fundsOperationToSave.getFundsOperationGroup(),view);
			}else if(view.equals("search")){
				fundsOperationTypesTmp = fundsOperationServiceFacade.searchFundsOperationsTypes(fundsOperationToSearch.getFundsOperationGroup(),view);
			}else{
				fundsOperationTypesTmp = fundsOperationServiceFacade.searchFundsOperationsTypes(fundsOperationToSave.getFundsOperationGroup(),view);
			}
			for (FundsOperationType fundsOperationTypeTmp : fundsOperationTypesTmp) {		
				if(fundsOperationTypesTmp.size()==1)
					fundsOperationToSave.setFundsOperationType(fundsOperationTypeTmp.getIdFundsOperationTypePk());
				//SETTING ACCOUNT TYPE DESC.
				cashAccountTypeDesc=fundsOperationTypeTmp.getCashAccountTypeDesc();
				cashAccountType = fundsOperationTypeTmp.getCashAccountType();
				if(fundsOperationTypeTmp.getCashAccountType().equals(AccountCashFundsType.SETTLEMENT.getCode()) ||
						  fundsOperationTypeTmp.getCashAccountType().equals(AccountCashFundsType.GUARANTEES.getCode())){
					indParticipant=Boolean.TRUE;
				} else if(fundsOperationTypeTmp.getCashAccountType().equals(AccountCashFundsType.BENEFIT.getCode())){
						indIssuer=Boolean.TRUE;
				} else if(fundsOperationTypeTmp.getCashAccountType().equals(AccountCashFundsType.RATES.getCode()) || 
						  fundsOperationTypeTmp.getCashAccountType().equals(AccountCashFundsType.TAX.getCode()) ){
					indTaxesRetirement=Boolean.TRUE;
					indInstitution = Boolean.TRUE;
					//Calling and setting institution cash account only when the view is register
					if(view.equals("register")){
						InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();					
						institutionCashAccountTO.setAccountType(cashAccountType);
						institutionCashAccountTO.setAccountState(CashAccountStateType.ACTIVATE.getCode());
						lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(searchInstitutionCashAccounts(institutionCashAccountTO));
					}
				}else if(fundsOperationTypeTmp.getCashAccountType().equals(AccountCashFundsType.RETURN.getCode())){
					//indInstitution = Boolean.TRUE;					
					indReturnRetirement= Boolean.TRUE;
					fillInstitutionBankAccountsTypeReturn();
					objInstitutionBankAccountsType=null;
					changeMotive();
				}
				break;
			}				
			if(view.equals("register") || view.equals("view"))
				lstOperationTypesToSave=fundsOperationTypesTmp;
			else
				lstOperationTypes=fundsOperationTypesTmp;
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Search institution cash accounts.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> searchInstitutionCashAccounts(InstitutionCashAccountTO institutionCashAccountTO) throws ServiceException{		
		
		List<InstitutionCashAccount> lstInstitutionTmp = fundsOperationServiceFacade.searchInstitutionCashAccounts(institutionCashAccountTO);
		if(institutionCashAccountTO.getAccountType().equals(AccountCashFundsType.TAX.getCode()))
				indTaxesRetirement=Boolean.TRUE;
		else if(institutionCashAccountTO.getAccountType().equals(AccountCashFundsType.GUARANTEES.getCode()))
				indGuaranteesRetirement=Boolean.TRUE;
		//CREATING INSTITUTION CASH ACCOUN TO TO GRID
		for (InstitutionCashAccount institutionCashAccount : lstInstitutionTmp) {
			InstitutionCashAccountTO cashAccountTO = new InstitutionCashAccountTO();
			//CALLING SERVICE TO GET BANKS DESCRIPTIONS
			cashAccountTO = fundsOperationServiceFacade.searchBankDescription(institutionCashAccount.getIdInstitutionCashAccountPk());
			institutionCashAccount.setBankAccount(cashAccountTO.getBankAccount());
			institutionCashAccount.setBankDescription(cashAccountTO.getBankDescription());

		}
		return lstInstitutionTmp;
	}
	/**
	 * Onchange participant.
	 */
	@LoggerAuditWeb
	public void onchangeParticipant(){
		try{
			cleanFromParticipant();
			if(cashAccountType.equals(AccountCashFundsType.SETTLEMENT.getCode())){
				//Calling Participant's Negotiation Mechanism
				if(!indRetirementAutomatic){
					lstNegotiationMechanism=fundsOperationServiceFacade.searchParticipantMechanisms(participantToSave.getIdParticipantPk());
					indSettlementRetirement=Boolean.TRUE;
				} else {
					gmLstParticipantPosition = null;
					indSettlementRetirement=Boolean.FALSE;
				}
			}else if(cashAccountType.equals(AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode())){
				indIntOperationsRetirement=Boolean.TRUE;
			}else if(cashAccountType.equals(AccountCashFundsType.GUARANTEES.getCode())){
				InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();					
				institutionCashAccountTO.setAccountType(cashAccountType);
				institutionCashAccountTO.setIdParticipantFk(participantToSave.getIdParticipantPk());
				institutionCashAccountTO.setAccountState(CashAccountStateType.ACTIVATE.getCode());
				lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(searchInstitutionCashAccounts(institutionCashAccountTO));
			}
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change mechanism.
	 */
	@LoggerAuditWeb
	public void changeMechanism(){
		try{
			//Searching Modality Group			
			Long negotiationMechanismId = this.getMechanismNegotiationPk();
			mechanismModalityGroupPk=null;
			settlementSchemaDescription="";
			lstModalityGroup=fundsOperationServiceFacade.searchModalityGroup(negotiationMechanismId);
			indSettlementGross=Boolean.FALSE;
			indSettlementNet=Boolean.FALSE;
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	//SETTING BIC COCE
	/**
	 * Change bank.
	 */
	public void changeBank(){
		try{
			if(Validations.validateIsNotNullAndPositive(bankToSave.getIdBankPk()))
			{
				SearchBankAccountsTO objSearchBankAccountsTO = new SearchBankAccountsTO();
				objSearchBankAccountsTO.setInstitutionType(InstitutionBankAccountsType.ISSUER.getCode());
				objSearchBankAccountsTO.setIssuer(issuer);
				objSearchBankAccountsTO.setIdBankProviderPk(bankToSave.getIdBankPk());
				objSearchBankAccountsTO.setState(BankAccountsStateType.REGISTERED.getCode());
				List<InstitutionBankAccount> lstBankAccountsTmp=manageEffectiveAccountsFacade.getListBankAccountsFilter(objSearchBankAccountsTO);
				if(Validations.validateIsNull(retirementFundsOperationTO))
					retirementFundsOperationTO= new RetirementFundsOperationTO();
				retirementFundsOperationTO.setLstBankAccountsTODataModel(new GenericDataModel<InstitutionBankAccount>(lstBankAccountsTmp));
			}
		
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change modality group.
	 */
	public void changeModalityGroup(){
		try{
			//Settlement Schema
			Integer settlementSchema = fundsOperationServiceFacade.searchSettlementSchema(mechanismModalityGroupPk);
			fundsOperationToSave.setOperationAmount(null);
			indSettlementGross=Boolean.FALSE;
			indSettlementNet=Boolean.FALSE;
			//Cleaning mechanism operation
			mechanismOperation=new MechanismOperation();
			mechanismOperation.setMechanisnModality(new MechanismModality());
			mechanismOperation.getMechanisnModality().setNegotiationModality(new NegotiationModality());
			participantSettlement=null;
			searchOperationType=null;
			//Validating if settlement type is gross or net
			if(settlementSchema.equals(SettlementSchemaType.GROSS.getCode())){
				indSettlementGross=Boolean.TRUE;
			}else {
				indSettlementNet=Boolean.TRUE;
				
				Long idParticipantPk = participantToSave.getIdParticipantPk();
				
				InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();
				institutionCashAccountTO.setIdParticipantFk(idParticipantPk);
				institutionCashAccountTO.setIdModalityGroupFk(mechanismModalityGroupPk);
				institutionCashAccountTO.setIdNegotiationMechanismFk(mechanismNegotiationPk);
				institutionCashAccountTO.setAccountType(cashAccountType);
				institutionCashAccountTO.setAccountState(CashAccountStateType.ACTIVATE.getCode());
				lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(this.fundsOperationServiceFacade.searchInstitutionCashAccounts(institutionCashAccountTO));
				
			}
			lstNegotiationModality=fundsOperationServiceFacade.searchModality(mechanismNegotiationPk,mechanismModalityGroupPk);
			settlementSchemaDescription=SettlementSchemaType.get(settlementSchema).getValue();
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change issuer.
	 */
	public void changeIssuer(){
		cleanFromIssuer();
		if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())) {
			indCorporativeProcessReturns = Boolean.TRUE;
		}else {
			indBenefitRetirement=Boolean.TRUE;
		}
		InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();
		institutionCashAccountTO.setIdIssuerFk(issuer.getIdIssuerPk());
		institutionCashAccountTO.setAccountType(cashAccountType);
		institutionCashAccountTO.setAccountState(CashAccountStateType.ACTIVATE.getCode());
		try{
			
			List<InstitutionCashAccount> lstInstitutionTmp = fundsOperationServiceFacade.searchInstitutionCashAccounts(institutionCashAccountTO);
			//CREATING INSTITUTION CASH ACCOUN TO TO GRID
			for (InstitutionCashAccount institutionCashAccount : lstInstitutionTmp) {
				InstitutionCashAccountTO cashAccountTO = new InstitutionCashAccountTO();
				Integer currency = institutionCashAccount.getCurrency();
				institutionCashAccount.setCurrencyTypeDescription(CurrencyType.get(currency).getValue());
				//CALLING SERVICE TO GET BANKS DESCRIPTIONS
				cashAccountTO = this.fundsOperationServiceFacade.searchBankDescription(institutionCashAccount.getIdInstitutionCashAccountPk());
				institutionCashAccount.setBankAccount(cashAccountTO.getBankAccount());
				institutionCashAccount.setBankDescription(cashAccountTO.getBankDescription());

			}
			lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(lstInstitutionTmp);
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change issuer.
	 */
	public void onChangeParticipantReturn(){
		if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())) {
			indCorporativeProcessReturns = Boolean.TRUE;
		}else {
			indBenefitRetirement=Boolean.TRUE;
		}
		InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();
		institutionCashAccountTO.setIdParticipantFk(participantToSave.getIdParticipantPk());
		institutionCashAccountTO.setAccountType(cashAccountType);
		institutionCashAccountTO.setAccountState(CashAccountStateType.ACTIVATE.getCode());
		try{
			
			List<InstitutionCashAccount> lstInstitutionTmp = fundsOperationServiceFacade.searchInstitutionCashAccounts(institutionCashAccountTO);
			//CREATING INSTITUTION CASH ACCOUN TO TO GRID
			for (InstitutionCashAccount institutionCashAccount : lstInstitutionTmp) {
				InstitutionCashAccountTO cashAccountTO = new InstitutionCashAccountTO();
				Integer currency = institutionCashAccount.getCurrency();
				institutionCashAccount.setCurrencyTypeDescription(CurrencyType.get(currency).getValue());
				//CALLING SERVICE TO GET BANKS DESCRIPTIONS
				cashAccountTO = this.fundsOperationServiceFacade.searchBankDescription(institutionCashAccount.getIdInstitutionCashAccountPk());
				institutionCashAccount.setBankAccount(cashAccountTO.getBankAccount());
				institutionCashAccount.setBankDescription(cashAccountTO.getBankDescription());

			}
			lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(lstInstitutionTmp);
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Institution radio select.
	 */
	public void institutionRadioSelect(){
		fundsOperationToSave.setOperationAmount(null);
	}
	
	/**
	 * Institution to radio select.
	 */
	public void institutionToRadioSelect(){		
		fundsOperationToSave.setCurrency(institutionCashAccountTOToSave.getCurrency());
		fundsOperationToSave.getInstitutionCashAccount().setAvailableAmount(institutionCashAccountTOToSave.getAvailableCashAmount());
		fundsOperationToSave.getInstitutionCashAccount().setIdInstitutionCashAccountPk(institutionCashAccountTOToSave.getIdInstitutionCashAccountPk());
		fundsOperationToSave.getInstitutionCashAccount().setIndRelatedBcrd(Integer.valueOf(institutionCashAccountTOToSave.getIndRelatedBcrd().toString()));
	}
	
	/**
	 * Internal op to radio select.
	 */
	public void internalOpToRadioSelect(){
		fundsOperationToSave.setOperationAmount(internationalOperationToSave.getCashAmount());
	}
	
	/**
	 * Sets the funds operation amount.
	 */
	public void setFundsOperationAmount(){
		fundsOperationToSave.setOperationAmount(fundsOperationToSave.getInstitutionCashAccount().getFundsOperationAmount());
		if(fundsOperationToSave.getOperationAmount().doubleValue() > fundsOperationToSave.getInstitutionCashAccount().getAvailableAmount().doubleValue()){
			String propertieConstant = PropertiesConstants.FUNDS_OPERATION_RETIRE_AMOUNT_GREATER_THAN_AVAILABLE;
			fundsOperationToSave.setOperationAmount(null);
			fundsOperationToSave.getInstitutionCashAccount().setFundsOperationAmount(null);
			this.showDialogMessage(propertieConstant,"alertDialog");
			return;
		}
		//Setting operation amount
		
		
	}
	
	/**
	 * Search target institutions. kunai
	 */
	public void searchTargetInstitutions(){
		indShowBcrdInsAccount = Boolean.FALSE;
		fundsOperationToSave.setIndBcrdPayback(BooleanType.NO.getCode());
		fundsOperationToSave.setOperationAmount(null);
		if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())){
			List<InstitutionCashAccount> institutionCashAccountsTmpLst = new ArrayList<InstitutionCashAccount>();
			
			//Searching institutions cash account validating same currency and differento Related BCRD 
			for(InstitutionCashAccount institutionCashAccountTmp : lstInstitutionCashAccount){
				if(fundsOperationToSave.getInstitutionCashAccount().getCurrency().equals(institutionCashAccountTmp.getCurrency()) && 
				   !institutionCashAccountTmp.getIdInstitutionCashAccountPk().equals(fundsOperationToSave.getInstitutionCashAccount().getIdInstitutionCashAccountPk()) &&
				   !institutionCashAccountTmp.getIndRelatedBcrd().equals(fundsOperationToSave.getInstitutionCashAccount().getIndRelatedBcrd())){
					institutionCashAccountsTmpLst.add(institutionCashAccountTmp);
				}
			}
			
			lstInstitutionCashAccountTarget = new GenericDataModel<InstitutionCashAccount>(institutionCashAccountsTmpLst);
		}
	}
	
	/**
	 * Search bcrd instutions.
	 */
	public void searchBCRDInstutions(){
		List<InstitutionCashAccount> institutionCashAccountsTmpLst = new ArrayList<InstitutionCashAccount>();
		if(indShowBcrdInsAccount.equals(Boolean.FALSE)){
			fundsOperationToSave.setIndBcrdPayback(BooleanType.NO.getCode());
			//Searching institutions cash account validating same currency and differento Related BCRD 
			for(InstitutionCashAccount institutionCashAccountTmp : lstInstitutionCashAccount){
				if(fundsOperationToSave.getInstitutionCashAccount().getCurrency().equals(institutionCashAccountTmp.getCurrency()) && 
				   !institutionCashAccountTmp.getIdInstitutionCashAccountPk().equals(fundsOperationToSave.getInstitutionCashAccount().getIdInstitutionCashAccountPk()) &&
				   !institutionCashAccountTmp.getIndRelatedBcrd().equals(fundsOperationToSave.getInstitutionCashAccount().getIndRelatedBcrd())){
					institutionCashAccountsTmpLst.add(institutionCashAccountTmp);
				}
			}		
		}else if(indShowBcrdInsAccount.equals(Boolean.TRUE) && fundsOperationToSave.getInstitutionCashAccount().getIndRelatedBcrd().equals(BooleanType.YES.getCode())){
			fundsOperationToSave.setIndBcrdPayback(BooleanType.YES.getCode());
			for(InstitutionCashAccount institutionCashAccountTmp : lstInstitutionCashAccountBcrd){
				if(fundsOperationToSave.getInstitutionCashAccount().getCurrency().equals(institutionCashAccountTmp.getCurrency()) ){
					institutionCashAccountsTmpLst.add(institutionCashAccountTmp);
				}
			}
		}else{
			String propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_OPERATION_BCB_DEVOLUTION_NOT_RELATED;			
			showDialogMessage(propertieConstant,"alertDialog");
			return;
		}
		lstInstitutionCashAccountTarget = new GenericDataModel<InstitutionCashAccount>(institutionCashAccountsTmpLst);
	}
	/**
	 * Search retirement int operation.
	 */
	public void searchRetirementIntOperation(){
		try{
			//GETTING PARTICIPANT ID
			Long idParticipantPk = participantToSave.getIdParticipantPk();
			//CREATING INSTITUTION CASH ACCOUNT TO
			InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();
			//SETTING PARTICIPANT ID
			institutionCashAccountTO.setIdParticipantFk(idParticipantPk);
			//SETTING ACCOUNT TYPE
			institutionCashAccountTO.setAccountType(cashAccountType);
			institutionCashAccountTO.setCurrency(internationalOperationTO.getCurrency());
			//CALLING SERVICE TO SEARCH INSTITUTIONS CASH ACCOUNTS
			List<InstitutionCashAccount> institutionCashAccountsTmp = fundsOperationServiceFacade.searchInstitutionCashAccounts(institutionCashAccountTO);
			List<InstitutionCashAccountTO> institutionCashAccountsTmpTO = new ArrayList<InstitutionCashAccountTO>();
			
			//CREATING INSTITUTION CASH ACCOUN TO TO GRID
			for (InstitutionCashAccount institutionCashAccount : institutionCashAccountsTmp) {
				InstitutionCashAccountTO cashAccountTO = new InstitutionCashAccountTO();
				//CALLING SERVICE TO GET BANKS DESCRIPTIONS
				cashAccountTO = fundsOperationServiceFacade.searchBankDescription(institutionCashAccount.getIdInstitutionCashAccountPk());
				cashAccountTO.setIdInstitutionCashAccountPk(institutionCashAccount.getIdInstitutionCashAccountPk());
				cashAccountTO.setCashAmount(institutionCashAccount.getTotalAmount());
				cashAccountTO.setAvailableCashAmount(institutionCashAccount.getAvailableAmount());
				cashAccountTO.setCurrency(institutionCashAccount.getCurrency());
				cashAccountTO.setCurrencyDescription(institutionCashAccount.getCurrencyTypeDescription());
				cashAccountTO.setIndRelatedBcrd(Long.valueOf(institutionCashAccount.getIndRelatedBcrd().toString()));
				institutionCashAccountsTmpTO.add(cashAccountTO);
			}
			this.setLstInstitutionCashAccountTO(new GenericDataModel<InstitutionCashAccountTO>(institutionCashAccountsTmpTO));
			//CALLING SERVICE TO SEARCH INTERNATIONAL OPERATIONS
			List<InternationalOperation>lstInternationalOperationTmp = fundsOperationServiceFacade.searchInternationalOperations(this.getInternationalOperationTO());
			//SETTING RESULT TO DATA MODEL..
			lstInternationalOperation=new GenericDataModel<InternationalOperation>(lstInternationalOperationTmp);
			setIndSearchIntOperations(Boolean.TRUE);
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	
	
	/**
	 * Before register.
	 */
	@LoggerAuditWeb
	public void beforeRegister(){
		String propertieConstant = PropertiesConstants.FUNDS_OPERATION_BEFORE_SAVE_MESSAGE;
		
		if(indRetirementAutomatic && 
				fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT_MANUAL.getCode())){
			
			if(arrayParticipantPosition == null || arrayParticipantPosition.length <= 0){
				lstFundsOperation = null;
				propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_SELECT_OPERATION_SETTLEMENT_AUTOMATIC;												
				showDialogMessage(propertieConstant, "alertDialog");
				return;
			}
			
			try {
				lstFundsOperation = new ArrayList<FundsOperation>();
				FundsOperation objFundsOperation = new FundsOperation();
				if(arrayParticipantPosition != null && arrayParticipantPosition.length > 0){
					for(ParticipantPosition objParticipantPosition : arrayParticipantPosition ){
						objFundsOperation = new FundsOperation();
						objFundsOperation.setFundsOperationGroup(fundsOperationToSave.getFundsOperationGroup());
						objFundsOperation.setFundsOperationType(fundsOperationToSave.getFundsOperationType());
						
						objFundsOperation.setParticipant(objParticipantPosition.getParticipant());
						objFundsOperation.setCurrency(objParticipantPosition.getSettlementProcess().getCurrency());
						objFundsOperation.setBank(manageEffectiveAccountsFacade.getBCBBank());
						objFundsOperation.setSettlementProcess(objParticipantPosition.getSettlementProcess());
						// MONTO A RETIRAR
						objFundsOperation.setOperationAmount(objParticipantPosition.getNetPosition());							
						// CREATING INSTITUTION CASH ACCOUNT TO
			        	InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();
			        	institutionCashAccountTO.setIdParticipantFk(objParticipantPosition.getParticipant().getIdParticipantPk());
			        	institutionCashAccountTO.setSettlementSchema(objParticipantPosition.getSettlementProcess().getSettlementSchema());
			        	institutionCashAccountTO.setIdModalityGroupFk(objParticipantPosition.getSettlementProcess().getModalityGroup().getIdModalityGroupPk());
			        	institutionCashAccountTO.setIdNegotiationMechanismFk(objParticipantPosition.getSettlementProcess().getNegotiationMechanism().getIdNegotiationMechanismPk());		        	
			        	institutionCashAccountTO.setCurrency(objParticipantPosition.getSettlementProcess().getCurrency());
			        	InstitutionCashAccount institutionCashAccount = fundsOperationServiceFacade.searchInstitutionCashAccount(institutionCashAccountTO);
			        	institutionCashAccount = fundsOperationServiceFacade.find(InstitutionCashAccount.class, institutionCashAccount.getIdInstitutionCashAccountPk());				        	
			        	objFundsOperation.setInstitutionCashAccount(institutionCashAccount);
			        	objFundsOperation.getInstitutionCashAccount().setFundsOperationAmount(objFundsOperation.getOperationAmount());			        
						if(objFundsOperation.getOperationAmount().doubleValue() > objFundsOperation.getInstitutionCashAccount().getAvailableAmount().doubleValue()){
							String propertieConstantAux = PropertiesConstants.FUNDS_OPERATION_RETIRE_AMOUNT_GREATER_THAN_AVAILABLE;
							objFundsOperation.setOperationAmount(null);
							objFundsOperation.getInstitutionCashAccount().setFundsOperationAmount(null);
							lstFundsOperation = null;
							this.showDialogMessage(propertieConstantAux, "alertDialog");
							return;
						}			        	
			        	lstFundsOperation.add(objFundsOperation);				
					}											
				}																		
						
				if(Validations.validateIsNotNull(participantToSave) && Validations.validateIsNotNull(participantToSave.getIdParticipantPk())){
						//GETTING THE PARTICIPANT NEMONIC TO SHOW ON MESSAGE
						mnemonic = fundsOperationServiceFacade.getParticipantMnemonic(participantToSave.getIdParticipantPk());
				} else if(Validations.validateIsNotNull(issuer.getIdIssuerPk())){
						//GETTING THE ISSUER NEMONIC TO SHOW ON MESSAGE
						mnemonic = fundsOperationServiceFacade.getIssuerMnemonic(issuer.getIdIssuerPk());
				} else if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.RATE_FUND_OPERATION.getCode()) ||
							 fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.TAX_FUND_OPERATION.getCode()) ||
							 fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode())){
						mnemonic= institutionName;
				}				
			} catch(ServiceException e){
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			
		} else {
			if(Validations.validateIsNotNull(fundsOperationToSave.getBank()) && Validations.validateIsNull(fundsOperationToSave.getBank().getIdBankPk())){
				fundsOperationToSave.setBank(null);
			}
			if(Validations.validateIsNull(fundsOperationToSave.getInstitutionCashAccount())){
				propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_SELECT_INSTITUTION_CASH_ACCOUNT;												
				showDialogMessage(propertieConstant,"alertDialog");
				return;
			}
//			if(fundsOperationToSave.getInstitutionCashAccount().getIndSettlementProcess().equals(GeneralConstants.ONE_VALUE_INTEGER))
//			{
//				propertieConstant = PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_SETTLEMENT_IN_PROCESS;												
//				showDialogMessage(propertieConstant,"alertDialog");
//				return;
//			}		
			
			/** VALIDATING IF ORIGIN INSTITUTION CASH ACCOUNT IS SELECTED ON RETIREMENT DEVOLUTION TYPE **/
//			if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode())){
//				
//				if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())){
//					if(Validations.validateIsNull(institutionCashAccountToSave) || Validations.validateIsNull(institutionCashAccountToSave.getIdInstitutionCashAccountPk())){
//						propertieConstant = PropertiesConstants.FUNDS_OPERATION_DEPOSIT_ACCOUNT_NOT_SELECTED;												
//						showDialogMessage(propertieConstant,"alertDialog");
//						return;
//					}							
//				}	
//			}
			/** VALIDATING IF THERE IS AN AMOUNT TO RETIRE **/
			if(Validations.validateIsNull(fundsOperationToSave.getOperationAmount()) || fundsOperationToSave.getOperationAmount().compareTo(BigDecimal.ZERO) <= 0){
				propertieConstant = PropertiesConstants.FUNDS_OPERATION_NOTHING_TO_RETIRE;												
				showDialogMessage(propertieConstant,"alertDialog");
				return;
			}
			if(!this.getIndSettlementGross()){
				if(Validations.validateIsNull(fundsOperationToSave.getInstitutionCashAccount().getAvailableAmount())){
					propertieConstant = PropertiesConstants.FUNDS_OPERATION_INSTITUTION_CASH_ACCOUNT_NOT_SELECTED;												
					showDialogMessage(propertieConstant,"alertDialog");
					return;
				}
				if(fundsOperationToSave.getOperationAmount().doubleValue() > fundsOperationToSave.getInstitutionCashAccount().getAvailableAmount().doubleValue()){
					propertieConstant = PropertiesConstants.FUNDS_OPERATION_RETIRE_AMOUNT_GREATER_THAN_AVAILABLE;												
					showDialogMessage(propertieConstant,"alertDialog");
					return;
				}		
			}
			
			try{
				if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode())
				 && indSettlementGross){
					fundsOperationToSave.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
					List<FundsOperation>lstFundsOperationRegistered = fundsOperationServiceFacade.searchRetirementFundOperations(fundsOperationToSave);
					
					for (FundsOperation fundsOperationTmp : lstFundsOperationRegistered) {
						Long idMechanismOperationFk = mechanismOperation.getIdMechanismOperationPk();
						Long idParticipantPk = participantToSave.getIdParticipantPk();
						
						ParticipantSettlement participantSettlementTmp = fundsOperationServiceFacade.findParticipantSettlement(idMechanismOperationFk, idParticipantPk, operationPartSelected);
						if(participantSettlementTmp.getIdParticipantSettlementPk().equals(participantSettlementTmp.getIdParticipantSettlementPk())){
							propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_FOUND_IN_OTHER_REQUEST;												
							showDialogMessage(propertieConstant,"alertDialog");
							return;
						}
					}
				}						
				if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode())){
					 List<String> lstValues = manageEffectiveAccountsFacade.getBCBBankBicCode();
					if(Validations.validateIsNull(fundsOperationServiceFacade.getBCBBank(fundsOperationToSave.getInstitutionCashAccount().getCurrency(),lstValues.get(2)))){
						propertieConstant = PropertiesConstants.FUNDS_BENEFIT_RETIREMENT_NOT_ACCOUNT_WITH_CURRENCY;												
						showDialogMessage(propertieConstant,"alertDialog");
						return;
					}
//					if(Validations.validateIsNull(bankToSave) || Validations.validateIsNullOrNotPositive(bankToSave.getIdBankPk()))
//					{
//						propertieConstant = PropertiesConstants.CASH_ACCOUNT_BANK_NOT_SELECTED;	
//						this.showDialogMessage(propertieConstant,"alertDialog");
//						return;
//					}
//					if(Validations.validateIsNull(retirementFundsOperationTO.getInstitutionBankAccountSession())){
//						propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_SELECT_INSTITUTION_BANK_ACCOUNT;												
//						this.showDialogMessage(propertieConstant,"alertDialog");
//						return;
//					}
				}
				/** Validating if funds operation type is holder return withdrawl **/
				if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())
				 ||fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())
				 ||fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
					//Creating the filter
					HolderFundsOperationTO filter = new HolderFundsOperationTO();
					filter.setHolderOperationState(HolderFundsOperationStateType.REGISTERED.getCode());
					
					/** CALLING SERVICE TO SEARCH OPERATIONS LIST **/
					List<HolderFundsOperation> lstHolderFundsOperationTmp = fundsOperationServiceFacade.searchHolderFundsOperationsLst(filter);
					//Verifiying if there are requests with the holder cash account selecteds
					for (HolderFundsOperation holderFundsOperationTmp : lstHolderFundsOperationTmp) {
						//kunai
						for(HolderFundsOperation holderOperationTmp: lstHolderFundsOperation){
							if(Validations.validateIsNotNull(holderFundsOperationTmp.getRelatedHolderCashMovement()) 
							&& holderOperationTmp.getRelatedHolderCashMovement().getIdHolderCashMovementPk().equals(holderFundsOperationTmp.getRelatedHolderCashMovement().getIdHolderCashMovementPk())){
								propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_DEPOSIT_FOUND_IN_OTHER_REQUEST;												
								showDialogMessage(propertieConstant,"alertDialog");
								return;
							}
						}
						
					}
				}
				
				if(Validations.validateIsNotNull(participantToSave) && 
				   Validations.validateIsNotNull(participantToSave.getIdParticipantPk())){
					//GETTING THE PARTICIPANT NEMONIC TO SHOW ON MESSAGE
					mnemonic = fundsOperationServiceFacade.getParticipantMnemonic(participantToSave.getIdParticipantPk());
				}else if(Validations.validateIsNotNull(issuer.getIdIssuerPk())){
					//GETTING THE ISSUER NEMONIC TO SHOW ON MESSAGE
					mnemonic = fundsOperationServiceFacade.getIssuerMnemonic(issuer.getIdIssuerPk());
				}else if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.RATE_FUND_OPERATION.getCode()) ||
						 fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.TAX_FUND_OPERATION.getCode()) ||
						 fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode())){
					mnemonic= institutionName;
				}
			} catch(ServiceException e){
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
				
		//SETTING MESSAGE PARAMS
		Object [] body = {FundsOperationGroupType.get(fundsOperationToSave.getFundsOperationGroup()).getValue(),mnemonic };
		//SETTING MESSAGE
		this.showDialogMessageWithParam(propertieConstant, "beforeConfirmSaveDialog", body);		
	}
	
	/**
	 * Register funds operation.
	 */
	@LoggerAuditWeb
	public void registerFundsOperation(){						
		try{
			List<FundsOperation> lstFundsOperationResult = new ArrayList<FundsOperation>();
			//VALIDATING FUNDS OPERATION GROUP
			if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode())){
				
				if(indRetirementAutomatic && 
						fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT_MANUAL.getCode())){					
					if(lstFundsOperation!= null && lstFundsOperation.size() > 0){
						lstFundsOperationResult = fundsOperationServiceFacade.saveLstFundsOperation(lstFundsOperation);
					}
					
				} else {
					if(this.getIndSettlementGross()){
						//GETTING THE MECHANISM
						fundsOperationToSave.setMechanismOperation(this.getMechanismOperation());								
						fundsOperationToSave.setCurrency(this.getMechanismOperation().getCurrency());
						fundsOperationToSave.setPaymentReference(mechanismOperation.getPaymentReference());
						//SETTING PARTICIPANT
						fundsOperationToSave.setParticipant(participantToSave);
						// CREATING INSTITUTION CASH ACCOUNT TO
			        	InstitutionCashAccountTO institutionCashAccountTO = new InstitutionCashAccountTO();
			        	institutionCashAccountTO.setIdParticipantFk(fundsOperationToSave.getParticipant().getIdParticipantPk());
			        	institutionCashAccountTO.setSettlementSchema(fundsOperationToSave.getMechanismOperation().getSettlementSchema());
			        	institutionCashAccountTO.setIdModalityGroupFk(mechanismModalityGroupPk);
			        	institutionCashAccountTO.setIdNegotiationMechanismFk(mechanismNegotiationPk);		        	
			        	institutionCashAccountTO.setCurrency(getMechanismOperation().getCurrency());
			        	InstitutionCashAccount institutionCashAccount = fundsOperationServiceFacade.searchInstitutionCashAccount(institutionCashAccountTO);
			        	institutionCashAccount = fundsOperationServiceFacade.find(InstitutionCashAccount.class, institutionCashAccount.getIdInstitutionCashAccountPk());
			        	
						fundsOperationToSave =fundsOperationServiceFacade.saveFundsOperation(fundsOperationToSave,institutionCashAccount);						
						
					}else{
						fundsOperationToSave.setParticipant(participantToSave);
						//GETTING THE CURRENCY FROM INSTITUTION CASH ACCOUNT					
						fundsOperationToSave.setCurrency(fundsOperationToSave.getInstitutionCashAccount().getCurrency());
						setFundsOperationToSave((FundsOperation) fundsOperationServiceFacade.saveFundsOperation(fundsOperationToSave,null));
					}
				}				
																	
			}else if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode())){
				//GETTING THE CURRENCY FROM INSTITUTION CASH ACCOUNT
				InstitutionCashAccount institutionCashAccount = manageEffectiveAccountsFacade.getInstitutionCashAccountData(fundsOperationToSave.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
				fundsOperationToSave.setCurrency(fundsOperationToSave.getInstitutionCashAccount().getCurrency());
				//centralized bank
				Bank bank = manageEffectiveAccountsFacade.getBCBBank();
				fundsOperationToSave.setBank(bank);
				fundsOperationToSave.setIssuer(issuer);
				fundsOperationToSave.setIndTransferCentralizing(fundsOperationToSave.isBlTransferCentralizing()== true?BooleanType.YES.getCode():BooleanType.NO.getCode());
				fundsOperationToSave=((FundsOperation) fundsOperationServiceFacade.saveFundsOperation(fundsOperationToSave,institutionCashAccount));
				
			}else if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.RATE_FUND_OPERATION.getCode()) ||
					 fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.TAX_FUND_OPERATION.getCode()) || 
					 fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode())){
				
					if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode())){
						//SETTING PARTICIPANT
						fundsOperationToSave.setParticipant(participantToSave);
					}
					//SETTING THE CURRENCY
					fundsOperationToSave.setCurrency(fundsOperationToSave.getInstitutionCashAccount().getCurrency());
					//SAVING!
					fundsOperationToSave=(FundsOperation) fundsOperationServiceFacade.saveFundsOperation(fundsOperationToSave,null);
			}else if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.INTERNATIONAL_FUND_OPERATION.getCode())){
				fundsOperationToSave.setCurrency(this.getInternationalOperationToSave().getCurrency());
				fundsOperationToSave.setParticipant(participantToSave);
				//SAVING FUNDS OPERATION
				fundsOperationToSave=fundsOperationServiceFacade.saveFundsAndInternationalOperation(fundsOperationToSave,this.getInternationalOperationToSave());
				/** VALIDATING IF OPERATION GROUP TYPE IS DEVOLUTIONS **/
			}else if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode())){
					//Validating if the funds Operation Type is Benefit Supply Withdrawl
					if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())){
						//GETTING THE CURRENCY FROM INSTITUTION CASH ACCOUNT
						InstitutionCashAccount institutionCashAccount = manageEffectiveAccountsFacade.getInstitutionCashAccountData(fundsOperationToSave.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
						fundsOperationToSave.setCurrency(fundsOperationToSave.getInstitutionCashAccount().getCurrency());
						//centralized bank
						Bank bank = manageEffectiveAccountsFacade.getBCBBank();
						fundsOperationToSave.setBank(bank);
						if(Validations.validateIsNotNullAndNotEmpty(participantToSave.getIdParticipantPk())) {
							fundsOperationToSave.setParticipant(participantToSave);
						}
						if(Validations.validateIsNotNullAndNotEmpty(issuer)) {
							if(Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())) {
								fundsOperationToSave.setIssuer(issuer);
							}
						}
						fundsOperationToSave.setIndTransferCentralizing(fundsOperationToSave.isBlTransferCentralizing()== true?BooleanType.YES.getCode():BooleanType.NO.getCode());
						fundsOperationToSave=((FundsOperation) fundsOperationServiceFacade.saveFundsOperation(fundsOperationToSave,institutionCashAccount));
						
//						FundsOperation fundsOperationDepositToSave = new FundsOperation();
//						
//						fundsOperationDepositToSave.setCurrency(this.getInstitutionCashAccountToSave().getCurrency());
//						fundsOperationDepositToSave.setOperationAmount(fundsOperationToSave.getOperationAmount());
//						fundsOperationDepositToSave.setFundsOperationClass(1);						
//						fundsOperationDepositToSave.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
//						fundsOperationDepositToSave.setFundsOperationGroup(fundsOperationToSave.getFundsOperationGroup());						
//						fundsOperationDepositToSave.setInstitutionCashAccount(this.getInstitutionCashAccountToSave());
//						
//						fundsOperationToSave.setCurrency(this.getInstitutionCashAccountToSave().getCurrency());
//						this.setFundsOperationToSave(this.fundsOperationServiceFacade.saveFundsOperationRetirementAndDeposit(fundsOperationToSave, fundsOperationDepositToSave));
					}else if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())
							 ||fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())
							 ||fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
						//SETTING THE BANK
						fundsOperationToSave.setBank(holderCashMovementToSave.getBank());
						//SETTING THE HOLDER
						//fundsOperationToSave.setHolder(this.getHolderDevolutionFilter());

						//SETTING HOLDER AND CURRENCY TO HOLDER FUNDS OPERATIONS
						for(HolderFundsOperation holderFundsOperation : lstHolderFundsOperation){
							holderFundsOperation.setBankAccountType(holderCashMovementToSave.getBankAccountType());	
							holderFundsOperation.setHolderAccountBank(holderCashMovementToSave.getHolderAccountBank());
							holderFundsOperation.setHolder(this.getHolderDevolutionFilter());
							holderFundsOperation.setCurrency(fundsOperationToSave.getCurrency());
							holderFundsOperation.setFundsOperation(fundsOperationToSave);
						}
						fundsOperationToSave.setHolderFundsOperations(lstHolderFundsOperation);
						
						fundsOperationToSave=(FundsOperation) this.fundsOperationServiceFacade.saveFundsOperation(fundsOperationToSave,null);					
					}
			}
			//SHOWING MESSAGE..
			String propertieConstant = PropertiesConstants.FUNDS_OPERATION_SAVE_MESSAGE;
			String operationsSends = GeneralConstants.EMPTY_STRING;
			if(indRetirementAutomatic && 
					fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT_MANUAL.getCode())){
				for(FundsOperation objFundOperation : lstFundsOperationResult){
					operationsSends += objFundOperation.getIdFundsOperationPk() + GeneralConstants.STR_COMMA_WITHOUT_SPACE;
		    	}
				if(operationsSends.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					operationsSends = operationsSends.substring(0, operationsSends.length()-1);
		    	}
			} else {
				operationsSends = fundsOperationToSave.getIdFundsOperationPk().toString();
			}
			
			Object [] body = { operationsSends,
							  FundsOperationGroupType.get(fundsOperationToSave.getFundsOperationGroup()).getValue(),
							  mnemonic};
			this.hideDialog("beforeConfirmSaveDialog");
			this.showDialogMessageWithParam(propertieConstant, "successDialog", body);
			
			// Start Notification
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_RETIREMENT_MANUAL_REGISTER.getCode());
			Object[] parameters = new Object[]{fundsOperationToSave.getIdFundsOperationPk().toString()};
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,null, parameters);
			// End Notification
			
		} catch(ServiceException e){
			if(e.getErrorService()!=null){
				this.hideDialog("beforeConfirmSaveDialog");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('expDialog').show();");
			}
		}
	}
	
	/**
	 * Search retirement fund operations.
	 */
	@LoggerAuditWeb
	public void searchRetirementFundOperations(){
		try{
			fundsOperationToSearch.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
			fundsOperationToSearch.setOperationPart(null);
			List<FundsOperation> fundsOperationLstTmp = this.fundsOperationServiceFacade.searchRetirementFundOperations(fundsOperationToSearch);			
			if(Validations.validateListIsNotNullAndNotEmpty(fundsOperationLstTmp)){
				showPrivilegeButtons();
				if(Validations.validateIsNotNull(fundsOperationToSearch.getIdFundsOperationPk()))
					fundsOperationToSearch.setIdFundsOperationPk(null);
				lstRetirementFundsOperation = new GenericDataModel<FundsOperation>(fundsOperationLstTmp);
			} else {
				lstRetirementFundsOperation = null;
			}
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before action.
	 *
	 * @param fromView the from view
	 * @param action the action
	 * @return the string
	 */
	public String beforeAction(String fromView, String action){
		if(Validations.validateIsNotNull(this.getFundsOperationSelected())){
			//validating if the state is REGISTERED
			if(this.getFundsOperationSelected().getOperationState().equals(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode())){
				//Validating the view that is called the method
				if(fromView.equals("Search")){
					this.fundsOperationDetail(this.getFundsOperationSelected().getIdFundsOperationPk());
					if(action.equals("Confirm"))
					{
						if(fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_WITHDRAWL_PAYMENT_MANUAL.getCode())
								|| fundsOperationToSave.getFundsOperationType().equals(ParameterFundsOperationType.FUNDS_SETTLEMENT_WITHDRAWL.getCode())){							
							indSendLip = GeneralConstants.ZERO_VALUE_INTEGER;
							viewCboSendLip = Boolean.TRUE;
						}						
						this.setIndConfirmClicked(Boolean.TRUE);
						return "registerRetirementFunds";
					}
					else
					{
						indSendLip = null;
						viewCboSendLip = Boolean.FALSE;
						this.setIndRejectClicked(Boolean.TRUE);
						return "registerRetirementFunds";
					}
				}else{
					//SETTING MESSAGE PARAMS
					Object [] body = {FundsOperationGroupType.get(fundsOperationToSave.getFundsOperationGroup()).getValue(),this.getMnemonic() };
					String propertieConstant = "";
					if(action.equals("Confirm")){				
						if(fundsOperationToSave.getBankMovement() == null || fundsOperationToSave.getBankMovement().equals(GeneralConstants.EMPTY_STRING)) {
							propertieConstant = PropertiesConstants.FUNDS_OPERATION_NOT_BANK_MOVEMENT;												
							this.showDialogMessage(propertieConstant,"errorDialog");
							return GeneralConstants.EMPTY_STRING;
						}
						
						if(fundsOperationToSave.getFundsOperationGroup().equals(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode())){
							List<String> lstValues = manageEffectiveAccountsFacade.getBCBBankBicCode();
							Long existeBank = fundsOperationServiceFacade.getBCBBank(fundsOperationToSave.getInstitutionCashAccount().getCurrency(),lstValues.get(2));
							if(Validations.validateIsNull(existeBank)){
								propertieConstant = PropertiesConstants.FUNDS_BENEFIT_RETIREMENT_NOT_ACCOUNT_WITH_CURRENCY;												
								this.showDialogMessage(propertieConstant,"errorDialog");
								return GeneralConstants.EMPTY_STRING;
							}
						}
						propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_BEFORE_CONFIRM_MESSAGE;
						//SETTING MESSAGE					
						this.showDialogMessageWithParam(propertieConstant, "confirmRequest", body);
					}else{
						propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_BEFORE_REJECT_MESSAGE;
						//SETTING MESSAGE					
						this.showDialogMessageWithParam(propertieConstant, "rejectRequest", body);
					}
					return "registerRetirementFunds";
				}
			}else{
				//SHOWING NOT REGISTERED MESSAGE ERROR
				String propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_REQUEST_NOT_IN_STATE;	
				Object [] body = {PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_RETIREMENT_REGISTERED_LBL)};
				this.showDialogMessageWithParam(propertieConstant, "errorDialog", body);	
				return GeneralConstants.EMPTY_STRING;
			}
		}else{
			showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.executeJavascriptFunction("PF('errorDialog').show();");
			return GeneralConstants.EMPTY_STRING;
		}	
	}// end beforeAction
	
	/**
	 * Confirm request.
	 */
	@LoggerAuditWeb
	public void confirmRequest(){
		try{
			if(Validations.validateIsNotNull(fundsOperationToSave)){
				if(indSendLip != null){
					if(indSendLip.equals(GeneralConstants.ONE_VALUE_INTEGER)){
						fundsOperationToSave.setIndSendLip(Boolean.TRUE);
					} else {
						fundsOperationToSave.setIndSendLip(Boolean.FALSE);
					}
				}
				this.fundsOperationServiceFacade.confirmRetirement(fundsOperationToSave);				
				
				// Start Notification
				BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.FUNDS_RETIREMENT_CONFIRM.getCode());
				Object[] parameters = new Object[]{fundsOperationToSave.getIdFundsOperationPk().toString()};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcessNotification,null, parameters);
				// End Notification
				
				String propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_SUCCESS_CONFIRM_MESSAGE;	
				Object [] body = {fundsOperationToSave.getIdFundsOperationPk().toString()};
				this.showDialogMessageWithParam(propertieConstant, "successDialog", body);
				this.searchRetirementFundOperations();
			}
		}catch(ServiceException e){
			if(e.getErrorService()!=null){				
				JSFUtilities.executeJavascriptFunction("PF('expDialog').show();");
				if(GeneralConstants.ASTERISK_STRING.equals(e.getMessage().substring(0, 1))){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR) , e.getMessage());
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR) , 
							PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				}												
			}
		} 
	}
	
	/**
	 * Reject request.
	 */
	@LoggerAuditWeb
	public void rejectRequest(){
		try{
			if(Validations.validateIsNotNull(fundsOperationToSave)){
				this.fundsOperationServiceFacade.rejectRequest(fundsOperationToSave);
				
				String propertieConstant = PropertiesConstants.FUNDS_RETIREMENT_SUCCESS_REJECT_MESSAGE;	
				Object [] body = {fundsOperationToSave.getIdFundsOperationPk().toString()};
				this.showDialogMessageWithParam(propertieConstant, "successDialog", body);
				this.searchRetirementFundOperations();
			}
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('expDialog').show();");
			}
		}
	}
	/**
	 * Funds operation detail.
	 *
	 * @param idFundsOperationPk the id funds operation pk
	 */
	@LoggerAuditWeb
	public void fundsOperationDetail(Long idFundsOperationPk){
		this.setFundsOperationToSave(new FundsOperation());
		this.cleanSomeObjects();
		try{
			//Getting settlementSchema			
			indDetailView = Boolean.TRUE;
			fundsOperationToSave=this.fundsOperationServiceFacade.fundsOperationDetail(idFundsOperationPk);
			
			this.onchangeRetirementFundsOperationGroup("view");
			if(cashAccountType.equals(AccountCashFundsType.SETTLEMENT.getCode())){
				
				lstSendLipe = BooleanType.list;
				
				Long idParticipantPk = fundsOperationToSave.getParticipant().getIdParticipantPk();
				participantToSave.setIdParticipantPk(idParticipantPk);
				//Calling Participant's Negotiation Mechanism
				this.setLstNegotiationMechanism(this.fundsOperationServiceFacade.searchParticipantMechanisms(idParticipantPk));
				this.setMechanismOperation(fundsOperationToSave.getMechanismOperation());
				//SEARCHING NEGOTIATION MECHANISM PK
				if(Validations.validateIsNotNull(this.getMechanismOperation().getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk())){
					mechanismNegotiationPk=this.getMechanismOperation().getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
				}else{
					mechanismNegotiationPk=fundsOperationToSave.getInstitutionCashAccount().getNegotiationMechanism().getIdNegotiationMechanismPk();
				}
				
				changeMechanism();				 
				//Getting settlementSchema
				Integer settlementSchema = (mechanismOperation.getSettlementSchema()!=null) ? this.getMechanismOperation().getSettlementSchema() : fundsOperationToSave.getInstitutionCashAccount().getSettlementSchema();
				//GETTING PARTICIPANT OPERATION FROM MECHANISM ID AND PARTICIPANT ID
				participantSettlement=this.fundsOperationServiceFacade.findParticipantSettlement(mechanismOperation.getIdMechanismOperationPk(), idParticipantPk,operationPartSelected);
				mechanismModalityGroupPk = fundsOperationToSave.getInstitutionCashAccount().getModalityGroup().getIdModalityGroupPk();
				
				if(settlementSchema.equals(SettlementSchemaType.GROSS.getCode())){
					
					indSettlementGross=Boolean.TRUE;
					
				}else {
					indSettlementNet=Boolean.TRUE;										
					fundsOperationToSave.getInstitutionCashAccount().setFundsOperationAmount(fundsOperationToSave.getOperationAmount());
					fundsOperationToSave.getInstitutionCashAccount().setCurrency(fundsOperationToSave.getCurrency());
					
					List<InstitutionCashAccount> lstInstCashAccountTmp = new ArrayList<InstitutionCashAccount>();
					lstInstCashAccountTmp.add(fundsOperationToSave.getInstitutionCashAccount());									
					lstInstitutionCashAccount=new GenericDataModel<InstitutionCashAccount>(lstInstCashAccountTmp);
					
					indRetirementAutomatic = Boolean.FALSE;																
					
					indSendLip = null;
					viewCboSendLip = Boolean.FALSE;
					
				}
				
				//Searching settlementSchema description
				settlementSchemaDescription=SettlementSchemaType.get(settlementSchema).getValue();				
				indSettlementRetirement=Boolean.TRUE;
			}else if(cashAccountType.equals(AccountCashFundsType.INTERNATIONAL_OPERATIONS.getCode())){
				Long idParticipantPk = fundsOperationToSave.getParticipant().getIdParticipantPk();
				participantToSave.setIdParticipantPk(idParticipantPk);
				this.setIndIntOperationsRetirement(Boolean.TRUE);
				
				indParticipant = Boolean.TRUE;
				InternationalOperation internationalOperationTmp = this.fundsOperationServiceFacade.findInternationalOperationByFundsOperation(fundsOperationToSave.getIdFundsOperationPk());
				
				//SETTING FILTERS TO SEARCH PARAMS IN DETAIL
				this.getInternationalOperationTO().setTradeDepository(internationalOperationTmp.getTradeDepository().getIdInternationalDepositoryPk());
				this.getInternationalOperationTO().setSettlementDepository(internationalOperationTmp.getSettlementDepository().getIdInternationalDepositoryPk());
				this.getInternationalOperationTO().setCurrency(fundsOperationToSave.getCurrency());
				
				this.getInternationalOperationToSave().setTransferType(internationalOperationTmp.getTransferType());
				InstitutionCashAccount institutionCashAccountTmp = this.fundsOperationToSave.getInstitutionCashAccount();
				
				InstitutionCashAccountTO cashAccountTO = new InstitutionCashAccountTO();
				//CALLING SERVICE TO GET BANKS DESCRIPTIONS
				cashAccountTO = fundsOperationServiceFacade.searchBankDescription(institutionCashAccountTmp.getIdInstitutionCashAccountPk());
				cashAccountTO.setIdInstitutionCashAccountPk(institutionCashAccountTmp.getIdInstitutionCashAccountPk());
				cashAccountTO.setCashAmount(institutionCashAccountTmp.getTotalAmount());
				cashAccountTO.setAvailableCashAmount(institutionCashAccountTmp.getAvailableAmount());
				cashAccountTO.setCurrency(fundsOperationToSave.getCurrency());
				cashAccountTO.setCurrencyDescription(institutionCashAccountTmp.getCurrencyTypeDescription());
				
				List<InstitutionCashAccountTO> lstInstCashAccountTO = new ArrayList<InstitutionCashAccountTO>();
				lstInstCashAccountTO.add(cashAccountTO);
				
				//SETTING INSTITUTION CASH ACCOUNT SELECTED FROM LIST
				institutionCashAccountTOToSave.setIdInstitutionCashAccountPk(institutionCashAccountTmp.getIdInstitutionCashAccountPk());
				//SETTING INTERNATIONAL OPERATION SELECTED FROM LIST
				internationalOperationToSave.setIdInternationalOperationPk(internationalOperationTmp.getIdInternationalOperationPk());
				
				
				//SETTING DATA TO LIST
				setLstInstitutionCashAccountTO(new GenericDataModel<InstitutionCashAccountTO>(lstInstCashAccountTO));
				
				List<InternationalOperation>lstInternationalOperationTmp = new ArrayList<InternationalOperation>();
				lstInternationalOperationTmp.add(internationalOperationTmp);
				//SETTING RESULT TO DATA MODEL..
				lstInternationalOperation=new GenericDataModel<InternationalOperation>(lstInternationalOperationTmp);
				
				indSearchIntOperations = Boolean.TRUE;
				
			}else if(cashAccountType.equals(AccountCashFundsType.BENEFIT.getCode())){
				this.setIndIssuer(Boolean.TRUE);
				this.setIndBenefitRetirement(Boolean.TRUE);
				Long fundsOperationTypeTmp = fundsOperationToSave.getFundsOperationType();
				if(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)
				 ||com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundsOperationTypeTmp)){
					indBenefitAutomatic = Boolean.TRUE;
					//Setting the benefit payment PK
					Long idBenefitPayment = fundsOperationToSave.getBenefitPaymentAllocation().getIdBenefitPaymentPK();
					//Integer currencyNumber = fundsOperationToSave.getBenefitPaymentAllocation().getCurrency();
					Long idFundsOperationTmp = fundsOperationToSave.getIdFundsOperationPk();
					Long idBank = fundsOperationToSave.getBank().getIdBankPk();
					
					//String bankAccountNumber = this.fundsOperationServiceFacade.searchBankFromBank(idBank,currencyNumber);
					
					retirementFundsOperationTO = new RetirementFundsOperationTO();
					retirementFundsOperationTO.setBank(fundsOperationToSave.getBank().getIdBankPk());
					retirementFundsOperationTO.setBankMnemonic(fundsOperationToSave.getBank().getMnemonic());
					retirementFundsOperationTO.setBankBicCode(fundsOperationToSave.getBank().getBicCode());
					retirementFundsOperationTO.setOperationAmount(fundsOperationToSave.getOperationAmount());
					//retirementFundsOperationTO.setBankAccountNumber(bankAccountNumber);
					retirementFundsOperationTO.setIdFundsOperationPk(idFundsOperationTmp);
					
					HolderCashMovementTO hCashFilter = new HolderCashMovementTO();
					hCashFilter.setIdBenefitPaymentPk(idBenefitPayment);
					hCashFilter.setBank(idBank);
					//Searching all holder cash movement from benefit payment allocation table
					lstHolderCashMovementForBenefit = this.fundsOperationServiceFacade.searchHolderCashMovement(hCashFilter);
					dmHolderCashMovementForBenefit = new GenericDataModel<HolderCashMovement>(lstHolderCashMovementForBenefit);
										
					//Searching the issuer from benefit payment allocation
					issuer = fundsOperationServiceFacade.findIssuerFacade(fundsOperationServiceFacade.getIssuerByBenefitPayment(idBenefitPayment));
					//this.getIssuer().setIdIssuerPk(this.fundsOperationServiceFacade.getIssuerByBenefitPayment(idBenefitPayment));					
					//benefitPaymentFile = this.fundsOperationServiceFacade.getBenefitPaymentFile(idFundsOperationTmp);
					//swiftMessageFile = this.fundsOperationServiceFacade.getSwiftMessage(idFundsOperationTmp);
				}else{
					InstitutionCashAccount institutionCashFromFunds = fundsOperationToSave.getInstitutionCashAccount();
					
					institutionCashFromFunds.setFundsOperationAmount(fundsOperationToSave.getOperationAmount());
					institutionCashFromFunds.setCurrency(fundsOperationToSave.getCurrency());
					institutionCashFromFunds.setCurrencyTypeDescription(CurrencyType.get(fundsOperationToSave.getCurrency()).getValue());
										
					//Getting bank info
					InstitutionCashAccountTO cashAccountTO = this.fundsOperationServiceFacade.searchBankDescription(institutionCashFromFunds.getIdInstitutionCashAccountPk());					
					institutionCashFromFunds.setBankAccount(cashAccountTO.getBankAccount());
					institutionCashFromFunds.setBankDescription(cashAccountTO.getBankDescription());
					
					//kunai
					List<InstitutionCashAccount>lstInstCashAccountTmp = new ArrayList<InstitutionCashAccount>();
					lstInstCashAccountTmp.add(institutionCashFromFunds);
					//SETTING BANK PK
					bankToSave.setIdBankPk(fundsOperationToSave.getBank().getIdBankPk());
					//SETTING BANK BIC CODE
					bankToSave.setBicCode(fundsOperationToSave.getBank().getBicCode());
					
					//this.getIssuer().setIdIssuerPk(fundsOperationToSave.getIssuer().getIdIssuerPk());
					if( fundsOperationToSave.getIssuer() != null && fundsOperationToSave.getIssuer().getIdIssuerPk()!=null) {
						issuer = fundsOperationServiceFacade.findIssuerFacade(fundsOperationToSave.getIssuer().getIdIssuerPk());
					}
					this.setLstInstitutionCashAccount(new GenericDataModel<InstitutionCashAccount>(lstInstCashAccountTmp));
				}
			}else if(cashAccountType.equals(AccountCashFundsType.RETURN.getCode())){
				//indInstitution = Boolean.TRUE;	
				if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode())){
					indReturnOperation = Boolean.TRUE;
					indCorporativeProcessReturns = Boolean.TRUE;
					InstitutionCashAccount institutionCashFromFunds = fundsOperationToSave.getInstitutionCashAccount();
					
					institutionCashFromFunds.setFundsOperationAmount(fundsOperationToSave.getOperationAmount());
					institutionCashFromFunds.setCurrency(fundsOperationToSave.getCurrency());
					institutionCashFromFunds.setCurrencyTypeDescription(CurrencyType.get(fundsOperationToSave.getCurrency()).getValue());
										
					//Getting bank info
					InstitutionCashAccountTO cashAccountTO = this.fundsOperationServiceFacade.searchBankDescription(institutionCashFromFunds.getIdInstitutionCashAccountPk());					
					institutionCashFromFunds.setBankAccount(cashAccountTO.getBankAccount());
					institutionCashFromFunds.setBankDescription(cashAccountTO.getBankDescription());
					
					//kunai
					List<InstitutionCashAccount>lstInstCashAccountTmp = new ArrayList<InstitutionCashAccount>();
					lstInstCashAccountTmp.add(institutionCashFromFunds);
					//SETTING BANK PK
					bankToSave.setIdBankPk(fundsOperationToSave.getBank().getIdBankPk());
					//SETTING BANK BIC CODE
					bankToSave.setBicCode(fundsOperationToSave.getBank().getBicCode());
					
					//this.getIssuer().setIdIssuerPk(fundsOperationToSave.getIssuer().getIdIssuerPk());
					if( fundsOperationToSave.getIssuer() != null && fundsOperationToSave.getIssuer().getIdIssuerPk()!=null) {
						objInstitutionBankAccountsType=InstitutionBankAccountsType.ISSUER.getCode();
						issuer = fundsOperationServiceFacade.findIssuerFacade(fundsOperationToSave.getIssuer().getIdIssuerPk());
					}
					if( fundsOperationToSave.getParticipant() != null && fundsOperationToSave.getParticipant().getIdParticipantPk() != null) {
						objInstitutionBankAccountsType=InstitutionBankAccountsType.PARTICIPANT.getCode();
						Long idParticipantPk = fundsOperationToSave.getParticipant().getIdParticipantPk();
						participantToSave.setIdParticipantPk(idParticipantPk);
					}
					this.setLstInstitutionCashAccount(new GenericDataModel<InstitutionCashAccount>(lstInstCashAccountTmp));
					
//					List<InstitutionCashAccount>lstInstitutionCashAccountTmp = new ArrayList<InstitutionCashAccount>();
//					//Setting funds operation to the transient object in institution cash account
//					fundsOperationToSave.getInstitutionCashAccount().setFundsOperationAmount(fundsOperationToSave.getOperationAmount());
//					
//					/** SEARCHING THE RELATED FUNDS OPERATION **/				
//					FundsOperation relatedFundsOperation = this.fundsOperationServiceFacade.searchFundsOperationRelated(fundsOperationToSave.getIdFundsOperationPk());
//					if(Validations.validateIsNotNull(relatedFundsOperation)){
//						
//						InstitutionCashAccount targetInstitutionCashAccount = relatedFundsOperation.getInstitutionCashAccount();
//						//Searching BankInfo
//						targetInstitutionCashAccount = this.searchAndPutBankInfoToInstitutionCashAccount(targetInstitutionCashAccount);
//												
//						targetInstitutionCashAccount.setCurrency(fundsOperationToSave.getInstitutionCashAccount().getCurrency());
//						
//						List<InstitutionCashAccount>targetInstitutionCashAccountLst = new ArrayList<InstitutionCashAccount>();
//						targetInstitutionCashAccountLst.add(targetInstitutionCashAccount);
//						
//						lstInstitutionCashAccountTarget = new GenericDataModel<InstitutionCashAccount>(targetInstitutionCashAccountLst);
//						this.setInstitutionCashAccountToSave(targetInstitutionCashAccount);
//					}
//					
//					this.setInstitutionCashAccountToSave(relatedFundsOperation.getInstitutionCashAccount());
//					InstitutionCashAccount sourceInstitution = fundsOperationToSave.getInstitutionCashAccount();
//					sourceInstitution = this.searchAndPutBankInfoToInstitutionCashAccount(sourceInstitution);
//					//Adding to list
//					lstInstitutionCashAccountTmp.add(sourceInstitution);
//					
//					this.setLstInstitutionCashAccount(new GenericDataModel<InstitutionCashAccount>(lstInstitutionCashAccountTmp));
//					indReserveRetirementRightsReturned = Boolean.TRUE;
					//kunai
				}else if(fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())
						 ||fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())
						 ||fundsOperationToSave.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
					indReturnOperation = Boolean.TRUE;
					
					this.indPaymentRetirementRightsReturned = Boolean.TRUE;		
					
					List<InstitutionCashAccount>lstInstitutionCashAccountTmp = new ArrayList<InstitutionCashAccount>();

					InstitutionCashAccount sourceInstitution = fundsOperationToSave.getInstitutionCashAccount();
					sourceInstitution = this.searchAndPutBankInfoToInstitutionCashAccount(sourceInstitution);
					//Setting retirement amount to view
					this.setRetirementAmount(fundsOperationToSave.getOperationAmount().doubleValue());
					//Adding to list
					lstInstitutionCashAccountTmp.add(sourceInstitution);
					
					this.setLstInstitutionCashAccount(new GenericDataModel<InstitutionCashAccount>(lstInstitutionCashAccountTmp));
					
					lstHolderCashMovementCombo = new ArrayList<HolderCashMovement>();
					
					//Creating the filter
					HolderFundsOperationTO filter = new HolderFundsOperationTO();
					filter.setIdHolderFundsOperationPk(fundsOperationToSave.getIdFundsOperationPk());
					
					/** CALLING SERVICE TO SEARCH OPERATION **/
					List<HolderFundsOperation> holderFundsOperationTmp = this.fundsOperationServiceFacade.searchHolderFundsOperationsLst(filter);					
					if(Validations.validateListIsNotNullAndNotEmpty(holderFundsOperationTmp)){
						holderDevolutionFilter = holderFundsOperationTmp.get(0).getHolder();
					}
					for(HolderFundsOperation holderFundOpTmp: holderFundsOperationTmp){
						holderAccountBankToSave.setIdHolderAccountBankPk(holderFundOpTmp.getHolderAccountBank().getIdHolderAccountBankPk());
						//ADDING HOLDER CASH MOVEMENT TO LIST
						lstHolderCashMovementCombo.add(holderFundOpTmp.getRelatedHolderCashMovement());
					}					
										
					//Long idHolderPk = fundsOperationToSave.getHolder().getIdHolderPk();
					//this.getHolderDevolutionFilter().setIdHolderPk(idHolderPk);
					this.setBankToSave(fundsOperationToSave.getBank());
					//lstHolderAccountBank = this.fundsOperationServiceFacade.searchHolderAccountBankLst(idHolderPk);
					//Filling holder cash movement list
					this.setLstHolderCashMovement(new GenericDataModel<HolderCashMovement>(lstHolderCashMovementCombo));
					this.indRetirementRightsReturnedSearched = Boolean.TRUE;
					
				}
			}else if(cashAccountType.equals(AccountCashFundsType.TAX.getCode()) || cashAccountType.equals(AccountCashFundsType.RATES.getCode())
					|| cashAccountType.equals(AccountCashFundsType.GUARANTEES.getCode()) ){
				fundsOperationToSave.getInstitutionCashAccount().setFundsOperationAmount(fundsOperationToSave.getOperationAmount());
				List<InstitutionCashAccount> lstInstitutionTmp = new ArrayList<InstitutionCashAccount>();
				lstInstitutionTmp.add(fundsOperationToSave.getInstitutionCashAccount());
				participantToSave = fundsOperationToSave.getParticipant();
				//CREATING INSTITUTION CASH ACCOUN TO TO GRID
				for (InstitutionCashAccount institutionCashAccount : lstInstitutionTmp) {
					InstitutionCashAccountTO cashAccountTO = new InstitutionCashAccountTO();
					//CALLING SERVICE TO GET BANKS DESCRIPTIONS
					cashAccountTO = this.fundsOperationServiceFacade.searchBankDescription(institutionCashAccount.getIdInstitutionCashAccountPk());
					institutionCashAccount.setBankAccount(cashAccountTO.getBankAccount());
					institutionCashAccount.setBankDescription(cashAccountTO.getBankDescription());

				}
				if(cashAccountType.equals(AccountCashFundsType.TAX.getCode()))
					indTaxesRetirement=Boolean.TRUE;
				if(cashAccountType.equals(AccountCashFundsType.GUARANTEES.getCode()))
					indGuaranteesRetirement=Boolean.TRUE;
				this.setLstInstitutionCashAccount(new GenericDataModel<InstitutionCashAccount>(lstInstitutionTmp));
				
				if(cashAccountType.equals(AccountCashFundsType.GUARANTEES.getCode())){
					indParticipant = Boolean.TRUE;
				}
			}
		} catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('expDialog').show();");
			}
		}
	}
	
	/**
	 * Search and put bank info to institution cash account.
	 *
	 * @param institution the institution
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount searchAndPutBankInfoToInstitutionCashAccount(InstitutionCashAccount institution) throws ServiceException {
		InstitutionCashAccountTO cashAccountTO = this.fundsOperationServiceFacade.searchBankDescription(institution.getIdInstitutionCashAccountPk());
		if(Validations.validateIsNotNull(cashAccountTO) && Validations.validateIsNotNull(cashAccountTO.getBankAccount())){
			//SETTING PROVIDER BANK DESCRIPTION
			institution.setBankDescription(cashAccountTO.getBankDescription());
			//SETTING ACCOUNT NUMBER
			institution.setBankAccount(cashAccountTO.getBankAccount());
		}
		return institution;
	}
	
	/**
	 * Setting bank info.
	 */
	public void settingBankInfo(){
		for (HolderAccountBank objHolderAccountBank : lstHolderAccountBank) {
			//IF ID HOLDER CASH MOVEMENT PK SELECTED IS FOUND ON LIST
			if(objHolderAccountBank.getIdHolderAccountBankPk().equals(holderAccountBankToSave.getIdHolderAccountBankPk())){
				bankToSave.setIdBankPk(objHolderAccountBank.getBank().getIdBankPk());
				//SETTING BANK INFO
				fundsOperationToSave.setBankAccountNumber(objHolderAccountBank.getBankAccountNumber());
				holderFundsOperationToSave.setBankAccountType(objHolderAccountBank.getBankAccountType());	
				holderFundsOperationToSave.setHolderAccountBank(objHolderAccountBank);
				break;
			}
		}
	}
	
	/**
	 * Search issuer handler register.
	 */
	public void searchIssuerHandlerRegister(){
		executeAction();	
		if(Validations.validateIsNotNullAndNotEmpty(issuer) 
				&& Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk()))	
		{			
			Issuer issuerTemp = null;
			try {
				issuerTemp = fundsOperationServiceFacade.findIssuerFacade(issuer.getIdIssuerPk());
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_VALIDATION_REGISTERED,
					    		  new Object[]{issuerTemp.getIdIssuerPk().toString()}) );						
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			retirementFundsOperationTO = new RetirementFundsOperationTO();
			issuer=issuerTemp;
			changeIssuer();
		}
	}
	
	/**
	 * Search list holder account.
	 */
	public void searchListHolderAccount(){

		  if (Validations.validateIsNull(holderDevolutionFilter)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_CUI_NOTFOUND) );		   
			  holderDevolutionFilter=new Holder();
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if(Validations.validateIsNull(holderDevolutionFilter.getStateHolder())){
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_CUI_NOTFOUND) );		   
			  holderDevolutionFilter=new Holder();
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  if (!holderDevolutionFilter.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.FUNDS_OPERATIONS_DEPOSIT_REGISTER_RETURNS_HOLDER_NOTREGISTER) );			  	   
			  holderDevolutionFilter=new Holder();
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
	}
	
	/**
	 * View holder benefit detail.
	 */
	public void viewHolderBenefitDetail(){
		if(indHolderBenefitDetail)
			indHolderBenefitDetail = Boolean.FALSE;
		else
			indHolderBenefitDetail = Boolean.TRUE;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic(){

		try{
			if(Validations.validateIsNotNull(this.getFundsOperationSelected().getParticipant()) 
				&& Validations.validateIsNotNull(this.getFundsOperationSelected().getParticipant().getIdParticipantPk())){
				//GETTING THE PARTICIPANT NEMONIC TO SHOW ON MESSAGE
				mnemonic = this.fundsOperationServiceFacade.getParticipantMnemonic(this.getFundsOperationSelected().getParticipant().getIdParticipantPk());
			}else if(Validations.validateIsNotNull(this.getFundsOperationSelected().getIssuer())
					&& Validations.validateIsNotNull(this.getFundsOperationSelected().getIssuer().getIdIssuerPk())){
				//GETTING THE ISSUER NEMONIC TO SHOW ON MESSAGE
				mnemonic = this.fundsOperationServiceFacade.getIssuerMnemonic(this.getFundsOperationSelected().getIssuer().getIdIssuerPk());
			}else if(this.getFundsOperationSelected().getFundsOperationGroup().equals(FundsOperationGroupType.RATE_FUND_OPERATION.getCode()) ||
					 this.getFundsOperationSelected().getFundsOperationGroup().equals(FundsOperationGroupType.TAX_FUND_OPERATION.getCode()) ||
					 this.getFundsOperationSelected().getFundsOperationGroup().equals(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode())){
				mnemonic= this.getInstitutionName();
			}
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return mnemonic;
	}
	
	
	/**
	 * Ask for clean.
	 *
	 * @return the string
	 */
	public String askForClean(){
		if(validateRequiredFields()){
			clean();
			String pagina=goTo("Search");
			init();
			return pagina;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.back"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwCleanDialog').show()");
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Back from register.
	 *
	 * @return the string
	 */
	public String backFromRegister(){
		init();
		return goTo("Search");
	}
	
	
	/**
	 * Validate required fields.
	 *
	 * @return true, if successful
	 */
	private boolean validateRequiredFields(){
		if((Validations.validateIsNullOrNotPositive(fundsOperationToSave.getFundsOperationGroup()) &&
			Validations.validateIsNullOrEmpty(fundsOperationToSave.getFundsOperationType()))
			|| indDetailView)
			return true;
		else
			return false;
	}
	
	
	/**
	 * Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo){
		//Cleaning objects to go..
		this.clean();
		if(viewToGo.equals("Search")){
			return "backManageRetirement";
		}else{
			return "registerRetirementFunds";
		}
	}
	/**
	 * Show dialog message with param.
	 *
	 * @param message the message
	 * @param dialog the dialog
	 * @param bodyData the body data
	 */
	public void showDialogMessageWithParam(String message, String dialog, Object[] bodyData){
		  showMessageOnDialog(null, null,message,bodyData);
		  JSFUtilities.executeJavascriptFunction("PF('"+dialog +"').show();");	
	}
	/**
	 * Show dialog error.
	 *
	 * @param message the message
	 * @param dialog the dialog
	 */
	public void showDialogMessage(String message, String dialog){
		showMessageOnDialog(null, null, message, null);
		JSFUtilities.executeJavascriptFunction("PF('"+dialog +"').show();");	
	}
	
	
	
	
	/**
	 * Hide dialog.
	 *
	 * @param dialog the dialog
	 */
	public void hideDialog(String dialog){
		JSFUtilities.executeJavascriptFunction("PF('" +dialog +"').hide();");	
	}
	
	/**
	 * Show dialog.
	 *
	 * @param dialog the dialog
	 */
	public void showDialog(String dialog){	
		JSFUtilities.executeJavascriptFunction("PF('"+dialog +"').show();");
	}
	/**
	 * Show icon error.
	 *
	 * @param message the message
	 * @param component the component
	 */
	public void showIconError(String message, String component){		
		JSFUtilities.addContextMessage(component,FacesMessage.SEVERITY_ERROR,message, message);
	}
	
	// RETIRO AUTOMATICO POR SERVICIO WEB
	
	/**
	 * Search retirement fund operations automatic.
	 */
	public void searchRetirementFundOperationsAutomatic(){
		try{
			
			List<ParticipantPosition> lstParticipantPosition = settlementProcessFacade.getListParticipantPosition(
												null, participantToSave.getIdParticipantPk(), 
												SettlementConstant.POSITIVE_POSITION, BooleanType.NO.getCode(),
												CommonsUtilities.currentDate());			
			for(ParticipantPosition objParticipantPosition : lstParticipantPosition){
				objParticipantPosition.getSettlementProcess().setScheduleTypeDescription(
						generalParametersFacade.getParameterTableById(objParticipantPosition.getSettlementProcess().getScheduleType()).getDescription());
				objParticipantPosition.getSettlementProcess().setCurrencyDescription(
						generalParametersFacade.getParameterTableById(objParticipantPosition.getSettlementProcess().getCurrency()).getDescription());
			}			
			gmLstParticipantPosition = new GenericDataModel<ParticipantPosition>(lstParticipantPosition);
			
		} catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	

	/**
	 * Gets the lst operation types.
	 *
	 * @return the lst operation types
	 */
	public List<FundsOperationType> getLstOperationTypes() {
		return lstOperationTypes;
	}

	/**
	 * Sets the lst operation types.
	 *
	 * @param lstOperationTypes the new lst operation types
	 */
	public void setLstOperationTypes(List<FundsOperationType> lstOperationTypes) {
		this.lstOperationTypes = lstOperationTypes;
	}

	/**
	 * Gets the funds operation to save.
	 *
	 * @return the funds operation to save
	 */
	public FundsOperation getFundsOperationToSave() {
		return fundsOperationToSave;
	}

	/**
	 * Sets the funds operation to save.
	 *
	 * @param fundsOperationToSave the new funds operation to save
	 */
	public void setFundsOperationToSave(FundsOperation fundsOperationToSave) {
		this.fundsOperationToSave = fundsOperationToSave;
	}
	
	/**
	 * Gets the accounts fund types.
	 *
	 * @return the accounts fund types
	 */
	public List<ParameterTable> getAccountsFundTypes() {
		return accountsFundTypes;
	}
	
	/**
	 * Sets the accounts fund types.
	 *
	 * @param accountsFundTypes the new accounts fund types
	 */
	public void setAccountsFundTypes(List<ParameterTable> accountsFundTypes) {
		this.accountsFundTypes = accountsFundTypes;
	}

	/**
	 * Gets the cash account type desc.
	 *
	 * @return the cash account type desc
	 */
	public String getCashAccountTypeDesc() {
		return cashAccountTypeDesc;
	}
	
	/**
	 * Sets the cash account type desc.
	 *
	 * @param cashAccountTypeDesc the new cash account type desc
	 */
	public void setCashAccountTypeDesc(String cashAccountTypeDesc) {
		this.cashAccountTypeDesc = cashAccountTypeDesc;
	}
	
	/**
	 * Gets the ind participant.
	 *
	 * @return the ind participant
	 */
	public Boolean getIndParticipant() {
		return indParticipant;
	}
	
	/**
	 * Sets the ind participant.
	 *
	 * @param indParticipant the new ind participant
	 */
	public void setIndParticipant(Boolean indParticipant) {
		this.indParticipant = indParticipant;
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}
	
	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the ind settlement retirement.
	 *
	 * @return the ind settlement retirement
	 */
	public Boolean getIndSettlementRetirement() {
		return indSettlementRetirement;
	}
	
	/**
	 * Sets the ind settlement retirement.
	 *
	 * @param indSettlementRetirement the new ind settlement retirement
	 */
	public void setIndSettlementRetirement(Boolean indSettlementRetirement) {
		this.indSettlementRetirement = indSettlementRetirement;
	}
	
	/**
	 * Gets the mechanism negotiation pk.
	 *
	 * @return the mechanism negotiation pk
	 */
	public Long getMechanismNegotiationPk() {
		return mechanismNegotiationPk;
	}
	
	/**
	 * Sets the mechanism negotiation pk.
	 *
	 * @param mechanismNegotiationPk the new mechanism negotiation pk
	 */
	public void setMechanismNegotiationPk(Long mechanismNegotiationPk) {
		this.mechanismNegotiationPk = mechanismNegotiationPk;
	}

	/**
	 * Gets the mechanism modality group pk.
	 *
	 * @return the mechanism modality group pk
	 */
	public Long getMechanismModalityGroupPk() {
		return mechanismModalityGroupPk;
	}
	
	/**
	 * Sets the mechanism modality group pk.
	 *
	 * @param mechanismModalityGroupPk the new mechanism modality group pk
	 */
	public void setMechanismModalityGroupPk(Long mechanismModalityGroupPk) {
		this.mechanismModalityGroupPk = mechanismModalityGroupPk;
	}
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}
	
	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}
	
	/**
	 * Gets the settlement schema description.
	 *
	 * @return the settlement schema description
	 */
	public String getSettlementSchemaDescription() {
		return settlementSchemaDescription;
	}
	
	/**
	 * Sets the settlement schema description.
	 *
	 * @param settlementSchemaDescription the new settlement schema description
	 */
	public void setSettlementSchemaDescription(
			String settlementSchemaDescription) {
		this.settlementSchemaDescription = settlementSchemaDescription;
	}
	
	/**
	 * Gets the ind settlement gross.
	 *
	 * @return the ind settlement gross
	 */
	public Boolean getIndSettlementGross() {
		return indSettlementGross;
	}
	
	/**
	 * Sets the ind settlement gross.
	 *
	 * @param indSettlementGross the new ind settlement gross
	 */
	public void setIndSettlementGross(Boolean indSettlementGross) {
		this.indSettlementGross = indSettlementGross;
	}
	
	/**
	 * Gets the ind settlement net.
	 *
	 * @return the ind settlement net
	 */
	public Boolean getIndSettlementNet() {
		return indSettlementNet;
	}
	
	/**
	 * Sets the ind settlement net.
	 *
	 * @param indSettlementNet the new ind settlement net
	 */
	public void setIndSettlementNet(Boolean indSettlementNet) {
		this.indSettlementNet = indSettlementNet;
	}
	
	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}
	
	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}
	
	/**
	 * Gets the lst search operation type.
	 *
	 * @return the lst search operation type
	 */
	public List<ParameterTable> getLstSearchOperationType() {
		return lstSearchOperationType;
	}
	
	/**
	 * Sets the lst search operation type.
	 *
	 * @param lstSearchOperationType the new lst search operation type
	 */
	public void setLstSearchOperationType(List<ParameterTable> lstSearchOperationType) {
		this.lstSearchOperationType = lstSearchOperationType;
	}
	
	/**
	 * Gets the search operation type.
	 *
	 * @return the search operation type
	 */
	public Integer getSearchOperationType() {
		return searchOperationType;
	}
	
	/**
	 * Sets the search operation type.
	 *
	 * @param searchOperationType the new search operation type
	 */
	public void setSearchOperationType(Integer searchOperationType) {
		this.searchOperationType = searchOperationType;
	}
	
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}
	
	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
	
	/**
	 * Gets the payment reference type.
	 *
	 * @return the payment reference type
	 */
	public Integer getPaymentReferenceType() {
		return paymentReferenceType;
	}
	
	/**
	 * Sets the payment reference type.
	 *
	 * @param paymentReferenceType the new payment reference type
	 */
	public void setPaymentReferenceType(Integer paymentReferenceType) {
		this.paymentReferenceType = paymentReferenceType;
	}
	
	/**
	 * Gets the operation info type.
	 *
	 * @return the operation info type
	 */
	public Integer getOperationInfoType() {
		return operationInfoType;
	}
	
	/**
	 * Sets the operation info type.
	 *
	 * @param operationInfoType the new operation info type
	 */
	public void setOperationInfoType(Integer operationInfoType) {
		this.operationInfoType = operationInfoType;
	}
	
	/**
	 * Gets the lst funds retirement type.
	 *
	 * @return the lst funds retirement type
	 */
	public List<ParameterTable> getLstFundsRetirementType() {
		return lstFundsRetirementType;
	}
	
	/**
	 * Sets the lst funds retirement type.
	 *
	 * @param lstFundsRetirementType the new lst funds retirement type
	 */
	public void setLstFundsRetirementType(List<ParameterTable> lstFundsRetirementType) {
		this.lstFundsRetirementType = lstFundsRetirementType;
	}
	
	/**
	 * Gets the retirement amount.
	 *
	 * @return the retirement amount
	 */
	public Double getRetirementAmount() {
		return retirementAmount;
	}
	
	/**
	 * Sets the retirement amount.
	 *
	 * @param retirementAmount the new retirement amount
	 */
	public void setRetirementAmount(Double retirementAmount) {
		this.retirementAmount = retirementAmount;
	}
	
	/**
	 * Gets the retirement type.
	 *
	 * @return the retirement type
	 */
	public Integer getRetirementType() {
		return retirementType;
	}
	
	/**
	 * Sets the retirement type.
	 *
	 * @param retirementType the new retirement type
	 */
	public void setRetirementType(Integer retirementType) {
		this.retirementType = retirementType;
	}
	
	/**
	 * Gets the lst institution cash account.
	 *
	 * @return the lst institution cash account
	 */
	public GenericDataModel<InstitutionCashAccount> getLstInstitutionCashAccount() {
		return lstInstitutionCashAccount;
	}
	
	/**
	 * Sets the lst institution cash account.
	 *
	 * @param lstInstitutionCashAccount the new lst institution cash account
	 */
	public void setLstInstitutionCashAccount(
			GenericDataModel<InstitutionCashAccount> lstInstitutionCashAccount) {
		this.lstInstitutionCashAccount = lstInstitutionCashAccount;
	}
	
	/**
	 * Gets the ind issuer.
	 *
	 * @return the ind issuer
	 */
	public Boolean getIndIssuer() {
		return indIssuer;
	}
	
	/**
	 * Sets the ind issuer.
	 *
	 * @param indIssuer the new ind issuer
	 */
	public void setIndIssuer(Boolean indIssuer) {
		this.indIssuer = indIssuer;
	}
	
	/**
	 * Gets the lst issuer.
	 *
	 * @return the lst issuer
	 */
	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}
	
	/**
	 * Sets the lst issuer.
	 *
	 * @param lstIssuer the new lst issuer
	 */
	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the ind benefit retirement.
	 *
	 * @return the ind benefit retirement
	 */
	public Boolean getIndBenefitRetirement() {
		return indBenefitRetirement;
	}
	
	/**
	 * Sets the ind benefit retirement.
	 *
	 * @param indBenefitRetirement the new ind benefit retirement
	 */
	public void setIndBenefitRetirement(Boolean indBenefitRetirement) {
		this.indBenefitRetirement = indBenefitRetirement;
	}
	
	/**
	 * Gets the lst bank.
	 *
	 * @return the lst bank
	 */
	public List<Bank> getLstBank() {
		return lstBank;
	}
	
	/**
	 * Sets the lst bank.
	 *
	 * @param lstBank the new lst bank
	 */
	public void setLstBank(List<Bank> lstBank) {
		this.lstBank = lstBank;
	}
	
	/**
	 * Gets the bank to save.
	 *
	 * @return the bank to save
	 */
	public Bank getBankToSave() {
		return bankToSave;
	}
	
	/**
	 * Sets the bank to save.
	 *
	 * @param bankToSave the new bank to save
	 */
	public void setBankToSave(Bank bankToSave) {
		this.bankToSave = bankToSave;
	}
	
	/**
	 * Gets the bic code desc.
	 *
	 * @return the bic code desc
	 */
	public String getBicCodeDesc() {
		return bicCodeDesc;
	}
	
	/**
	 * Sets the bic code desc.
	 *
	 * @param bicCodeDesc the new bic code desc
	 */
	public void setBicCodeDesc(String bicCodeDesc) {
		this.bicCodeDesc = bicCodeDesc;
	}
	
	/**
	 * Gets the ind taxes retirement.
	 *
	 * @return the ind taxes retirement
	 */
	public Boolean getIndTaxesRetirement() {
		return indTaxesRetirement;
	}
	
	/**
	 * Sets the ind taxes retirement.
	 *
	 * @param indTaxesRetirement the new ind taxes retirement
	 */
	public void setIndTaxesRetirement(Boolean indTaxesRetirement) {
		this.indTaxesRetirement = indTaxesRetirement;
	}
	
	/**
	 * Gets the ind institution.
	 *
	 * @return the ind institution
	 */
	public Boolean getIndInstitution() {
		return indInstitution;
	}
	
	/**
	 * Sets the ind institution.
	 *
	 * @param indInstitution the new ind institution
	 */
	public void setIndInstitution(Boolean indInstitution) {
		this.indInstitution = indInstitution;
	}
	
	/**
	 * Gets the institution name.
	 *
	 * @return the institution name
	 */
	public String getInstitutionName() {
		return institutionName;
	}
	
	/**
	 * Gets the ind int operations retirement.
	 *
	 * @return the ind int operations retirement
	 */
	public Boolean getIndIntOperationsRetirement() {
		return indIntOperationsRetirement;
	}
	
	/**
	 * Sets the ind int operations retirement.
	 *
	 * @param indIntOperationsRetirement the new ind int operations retirement
	 */
	public void setIndIntOperationsRetirement(Boolean indIntOperationsRetirement) {
		this.indIntOperationsRetirement = indIntOperationsRetirement;
	}
	
	/**
	 * Gets the international operation to save.
	 *
	 * @return the international operation to save
	 */
	public InternationalOperation getInternationalOperationToSave() {
		return internationalOperationToSave;
	}
	
	/**
	 * Sets the international operation to save.
	 *
	 * @param internationalOperationToSave the new international operation to save
	 */
	public void setInternationalOperationToSave(
			InternationalOperation internationalOperationToSave) {
		this.internationalOperationToSave = internationalOperationToSave;
	}
	
	/**
	 * Gets the lst international depositories.
	 *
	 * @return the lst international depositories
	 */
	public List<InternationalDepository> getLstInternationalDepositories() {
		return lstInternationalDepositories;
	}
	
	/**
	 * Sets the lst international depositories.
	 *
	 * @param lstInternationalDepositories the new lst international depositories
	 */
	public void setLstInternationalDepositories(
			List<InternationalDepository> lstInternationalDepositories) {
		this.lstInternationalDepositories = lstInternationalDepositories;
	}
	
	/**
	 * Gets the international depository tosave.
	 *
	 * @return the international depository tosave
	 */
	public InternationalDepository getInternationalDepositoryTosave() {
		return internationalDepositoryTosave;
	}
	
	/**
	 * Sets the international depository tosave.
	 *
	 * @param internationalDepositoryTosave the new international depository tosave
	 */
	public void setInternationalDepositoryTosave(
			InternationalDepository internationalDepositoryTosave) {
		this.internationalDepositoryTosave = internationalDepositoryTosave;
	}
	
	/**
	 * Gets the lst currency types.
	 *
	 * @return the lst currency types
	 */
	public List<ParameterTable> getLstCurrencyTypes() {
		return lstCurrencyTypes;
	}
	
	/**
	 * Sets the lst currency types.
	 *
	 * @param lstCurrencyTypes the new lst currency types
	 */
	public void setLstCurrencyTypes(List<ParameterTable> lstCurrencyTypes) {
		this.lstCurrencyTypes = lstCurrencyTypes;
	}
	
	/**
	 * Gets the now time.
	 *
	 * @return the now time
	 */
	public Date getNowTime() {
		return nowTime;
	}
	
	/**
	 * Sets the now time.
	 *
	 * @param nowTime the new now time
	 */
	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}
	
	/**
	 * Gets the international operation to.
	 *
	 * @return the international operation to
	 */
	public InternationalOperationTO getInternationalOperationTO() {
		return internationalOperationTO;
	}
	
	/**
	 * Sets the international operation to.
	 *
	 * @param internationalOperationTO the new international operation to
	 */
	public void setInternationalOperationTO(InternationalOperationTO internationalOperationTO) {
		this.internationalOperationTO = internationalOperationTO;
	}
	
	/**
	 * Gets the lst international operation.
	 *
	 * @return the lst international operation
	 */
	public GenericDataModel<InternationalOperation> getLstInternationalOperation() {
		return lstInternationalOperation;
	}
	
	/**
	 * Sets the lst international operation.
	 *
	 * @param lstInternationalOperation the new lst international operation
	 */
	public void setLstInternationalOperation(GenericDataModel<InternationalOperation> lstInternationalOperation) {
		this.lstInternationalOperation = lstInternationalOperation;
	}
	
	/**
	 * Gets the ind search int operations.
	 *
	 * @return the ind search int operations
	 */
	public Boolean getIndSearchIntOperations() {
		return indSearchIntOperations;
	}
	
	/**
	 * Sets the ind search int operations.
	 *
	 * @param indSearchIntOperations the new ind search int operations
	 */
	public void setIndSearchIntOperations(Boolean indSearchIntOperations) {
		this.indSearchIntOperations = indSearchIntOperations;
	}
	
	/**
	 * Gets the institution cash account to to save.
	 *
	 * @return the institution cash account to to save
	 */
	public InstitutionCashAccountTO getInstitutionCashAccountTOToSave() {
		return institutionCashAccountTOToSave;
	}
	
	/**
	 * Sets the institution cash account to to save.
	 *
	 * @param institutionCashAccountTOToSave the new institution cash account to to save
	 */
	public void setInstitutionCashAccountTOToSave(InstitutionCashAccountTO institutionCashAccountTOToSave) {
		this.institutionCashAccountTOToSave = institutionCashAccountTOToSave;
	}
	
	/**
	 * Gets the lst institution cash account to.
	 *
	 * @return the lst institution cash account to
	 */
	public GenericDataModel<InstitutionCashAccountTO> getLstInstitutionCashAccountTO() {
		return lstInstitutionCashAccountTO;
	}
	
	/**
	 * Sets the lst institution cash account to.
	 *
	 * @param lstInstitutionCashAccountTO the new lst institution cash account to
	 */
	public void setLstInstitutionCashAccountTO(
			GenericDataModel<InstitutionCashAccountTO> lstInstitutionCashAccountTO) {
		this.lstInstitutionCashAccountTO = lstInstitutionCashAccountTO;
	}
	
	/**
	 * Gets the ind return operation.
	 *
	 * @return the ind return operation
	 */
	public Boolean getIndReturnOperation() {
		return indReturnOperation;
	}
	
	/**
	 * Sets the ind return operation.
	 *
	 * @param indReturnOperation the new ind return operation
	 */
	public void setIndReturnOperation(Boolean indReturnOperation) {
		this.indReturnOperation = indReturnOperation;
	}
	
	/**
	 * Gets the ind reserve retirement rights returned.
	 *
	 * @return the ind reserve retirement rights returned
	 */
	public Boolean getIndReserveRetirementRightsReturned() {
		return indReserveRetirementRightsReturned;
	}
	
	/**
	 * Sets the ind reserve retirement rights returned.
	 *
	 * @param indReserveRetirementRightsReturned the new ind reserve retirement rights returned
	 */
	public void setIndReserveRetirementRightsReturned(
			Boolean indReserveRetirementRightsReturned) {
		this.indReserveRetirementRightsReturned = indReserveRetirementRightsReturned;
	}
	
	/**
	 * Gets the institution cash account to save.
	 *
	 * @return the institution cash account to save
	 */
	public InstitutionCashAccount getInstitutionCashAccountToSave() {
		return institutionCashAccountToSave;
	}
	
	/**
	 * Sets the institution cash account to save.
	 *
	 * @param institutionCashAccountToSave the new institution cash account to save
	 */
	public void setInstitutionCashAccountToSave(
			InstitutionCashAccount institutionCashAccountToSave) {
		this.institutionCashAccountToSave = institutionCashAccountToSave;
	}
	
	/**
	 * Gets the lst institution cash account target.
	 *
	 * @return the lst institution cash account target
	 */
	public GenericDataModel<InstitutionCashAccount> getLstInstitutionCashAccountTarget() {
		return lstInstitutionCashAccountTarget;
	}
	
	/**
	 * Sets the lst institution cash account target.
	 *
	 * @param lstInstitutionCashAccountTarget the new lst institution cash account target
	 */
	public void setLstInstitutionCashAccountTarget(
			GenericDataModel<InstitutionCashAccount> lstInstitutionCashAccountTarget) {
		this.lstInstitutionCashAccountTarget = lstInstitutionCashAccountTarget;
	}
	
	/**
	 * Gets the ind payment retirement rights returned.
	 *
	 * @return the ind payment retirement rights returned
	 */
	public Boolean getIndPaymentRetirementRightsReturned() {
		return indPaymentRetirementRightsReturned;
	}
	
	/**
	 * Sets the ind payment retirement rights returned.
	 *
	 * @param indPaymentRetirementRightsReturned the new ind payment retirement rights returned
	 */
	public void setIndPaymentRetirementRightsReturned(
			Boolean indPaymentRetirementRightsReturned) {
		this.indPaymentRetirementRightsReturned = indPaymentRetirementRightsReturned;
	}
	
	/**
	 * Gets the holder cash movement to save.
	 *
	 * @return the holder cash movement to save
	 */
	public HolderCashMovement getHolderCashMovementToSave() {
		return holderCashMovementToSave;
	}
	
	/**
	 * Sets the holder cash movement to save.
	 *
	 * @param holderCashMovementToSave the new holder cash movement to save
	 */
	public void setHolderCashMovementToSave(HolderCashMovement holderCashMovementToSave) {
		this.holderCashMovementToSave = holderCashMovementToSave;
	}
	
	/**
	 * Gets the holder devolution filter.
	 *
	 * @return the holder devolution filter
	 */
	public Holder getHolderDevolutionFilter() {
		return holderDevolutionFilter;
	}
	
	/**
	 * Sets the holder devolution filter.
	 *
	 * @param holderDevolutionFilter the new holder devolution filter
	 */
	public void setHolderDevolutionFilter(Holder holderDevolutionFilter) {
		this.holderDevolutionFilter = holderDevolutionFilter;
	}
	
	/**
	 * Gets the ind retirement rights returned searched.
	 *
	 * @return the ind retirement rights returned searched
	 */
	public Boolean getIndRetirementRightsReturnedSearched() {
		return indRetirementRightsReturnedSearched;
	}
	
	/**
	 * Sets the ind retirement rights returned searched.
	 *
	 * @param indRetirementRightsReturnedSearched the new ind retirement rights returned searched
	 */
	public void setIndRetirementRightsReturnedSearched(
			Boolean indRetirementRightsReturnedSearched) {
		this.indRetirementRightsReturnedSearched = indRetirementRightsReturnedSearched;
	}
	
	/**
	 * Gets the lst holder cash movement.
	 *
	 * @return the lst holder cash movement
	 */
	public GenericDataModel<HolderCashMovement> getLstHolderCashMovement() {
		return lstHolderCashMovement;
	}
	
	/**
	 * Sets the lst holder cash movement.
	 *
	 * @param lstHolderCashMovement the new lst holder cash movement
	 */
	public void setLstHolderCashMovement(GenericDataModel<HolderCashMovement> lstHolderCashMovement) {
		this.lstHolderCashMovement = lstHolderCashMovement;
	}
	
	/**
	 * Gets the lst holder cash movement combo.
	 *
	 * @return the lst holder cash movement combo
	 */
	public List<HolderCashMovement> getLstHolderCashMovementCombo() {
		return lstHolderCashMovementCombo;
	}
	
	/**
	 * Sets the lst holder cash movement combo.
	 *
	 * @param lstHolderCashMovementCombo the new lst holder cash movement combo
	 */
	public void setLstHolderCashMovementCombo(
			List<HolderCashMovement> lstHolderCashMovementCombo) {
		this.lstHolderCashMovementCombo = lstHolderCashMovementCombo;
	}
	
	/**
	 * Gets the funds operation to search.
	 *
	 * @return the funds operation to search
	 */
	public FundsOperation getFundsOperationToSearch() {
		return fundsOperationToSearch;
	}
	
	/**
	 * Sets the funds operation to search.
	 *
	 * @param fundsOperationToSearch the new funds operation to search
	 */
	public void setFundsOperationToSearch(FundsOperation fundsOperationToSearch) {
		this.fundsOperationToSearch = fundsOperationToSearch;
	}
	
	/**
	 * Gets the lst funds operation states.
	 *
	 * @return the lst funds operation states
	 */
	public List<ParameterTable> getLstFundsOperationStates() {
		return lstFundsOperationStates;
	}
	
	/**
	 * Sets the lst funds operation states.
	 *
	 * @param lstFundsOperationStates the new lst funds operation states
	 */
	public void setLstFundsOperationStates(List<ParameterTable> lstFundsOperationStates) {
		this.lstFundsOperationStates = lstFundsOperationStates;
	}
	
	/**
	 * Gets the lst retirement funds operation.
	 *
	 * @return the lst retirement funds operation
	 */
	public GenericDataModel<FundsOperation> getLstRetirementFundsOperation() {
		return lstRetirementFundsOperation;
	}
	
	/**
	 * Sets the lst retirement funds operation.
	 *
	 * @param lstRetirementFundsOperation the new lst retirement funds operation
	 */
	public void setLstRetirementFundsOperation(
			GenericDataModel<FundsOperation> lstRetirementFundsOperation) {
		this.lstRetirementFundsOperation = lstRetirementFundsOperation;
	}
	
	/**
	 * Gets the ind detail view.
	 *
	 * @return the ind detail view
	 */
	public Boolean getIndDetailView() {
		return indDetailView;
	}
	
	/**
	 * Sets the ind detail view.
	 *
	 * @param indDetailView the new ind detail view
	 */
	public void setIndDetailView(Boolean indDetailView) {
		this.indDetailView = indDetailView;
	}
	
	/**
	 * Gets the ind confirm clicked.
	 *
	 * @return the ind confirm clicked
	 */
	public Boolean getIndConfirmClicked() {
		return indConfirmClicked;
	}
	
	/**
	 * Sets the ind confirm clicked.
	 *
	 * @param indConfirmClicked the new ind confirm clicked
	 */
	public void setIndConfirmClicked(Boolean indConfirmClicked) {
		this.indConfirmClicked = indConfirmClicked;
	}
	
	/**
	 * Gets the funds operation selected.
	 *
	 * @return the funds operation selected
	 */
	public FundsOperation getFundsOperationSelected() {
		return fundsOperationSelected;
	}
	
	/**
	 * Sets the funds operation selected.
	 *
	 * @param fundsOperationSelected the new funds operation selected
	 */
	public void setFundsOperationSelected(FundsOperation fundsOperationSelected) {
		this.fundsOperationSelected = fundsOperationSelected;
	}
	
	/**
	 * Gets the ind reject clicked.
	 *
	 * @return the ind reject clicked
	 */
	public Boolean getIndRejectClicked() {
		return indRejectClicked;
	}
	
	/**
	 * Sets the ind reject clicked.
	 *
	 * @param indRejectClicked the new ind reject clicked
	 */
	public void setIndRejectClicked(Boolean indRejectClicked) {
		this.indRejectClicked = indRejectClicked;
	}

	/**
	 * Gets the lst operation part type.
	 *
	 * @return the lst operation part type
	 */
	public List<OperationPartType> getLstOperationPartType() {
		return lstOperationPartType;
	}

	/**
	 * Sets the lst operation part type.
	 *
	 * @param lstOperationPartType the new lst operation part type
	 */
	public void setLstOperationPartType(List<OperationPartType> lstOperationPartType) {
		this.lstOperationPartType = lstOperationPartType;
	}

	/**
	 * Gets the operation part selected.
	 *
	 * @return the operation part selected
	 */
	public Integer getOperationPartSelected() {
		return operationPartSelected;
	}

	/**
	 * Sets the operation part selected.
	 *
	 * @param operationPartSelected the new operation part selected
	 */
	public void setOperationPartSelected(Integer operationPartSelected) {
		this.operationPartSelected = operationPartSelected;
	}

	/**
	 * Gets the ind cash operation part.
	 *
	 * @return the ind cash operation part
	 */
	public Boolean getIndCashOperationPart() {
		return indCashOperationPart;
	}

	/**
	 * Sets the ind cash operation part.
	 *
	 * @param indCashOperationPart the new ind cash operation part
	 */
	public void setIndCashOperationPart(Boolean indCashOperationPart) {
		this.indCashOperationPart = indCashOperationPart;
	}

	/**
	 * Gets the participant to save.
	 *
	 * @return the participant to save
	 */
	public Participant getParticipantToSave() {
		return participantToSave;
	}

	/**
	 * Sets the participant to save.
	 *
	 * @param participantToSave the new participant to save
	 */
	public void setParticipantToSave(Participant participantToSave) {
		this.participantToSave = participantToSave;
	}

	/**
	 * Gets the holder funds operation to save.
	 *
	 * @return the holder funds operation to save
	 */
	public HolderFundsOperation getHolderFundsOperationToSave() {
		return holderFundsOperationToSave;
	}

	/**
	 * Sets the holder funds operation to save.
	 *
	 * @param holderFundsOperationToSave the new holder funds operation to save
	 */
	public void setHolderFundsOperationToSave(HolderFundsOperation holderFundsOperationToSave) {
		this.holderFundsOperationToSave = holderFundsOperationToSave;
	}


	/**
	 * Gets the lst holder account bank.
	 *
	 * @return the lst holder account bank
	 */
	public List<HolderAccountBank> getLstHolderAccountBank() {
		return lstHolderAccountBank;
	}

	/**
	 * Sets the lst holder account bank.
	 *
	 * @param lstHolderAccountBank the new lst holder account bank
	 */
	public void setLstHolderAccountBank(List<HolderAccountBank> lstHolderAccountBank) {
		this.lstHolderAccountBank = lstHolderAccountBank;
	}

	/**
	 * Gets the holder account bank to save.
	 *
	 * @return the holder account bank to save
	 */
	public HolderAccountBank getHolderAccountBankToSave() {
		return holderAccountBankToSave;
	}

	/**
	 * Sets the holder account bank to save.
	 *
	 * @param holderAccountBankToSave the new holder account bank to save
	 */
	public void setHolderAccountBankToSave(HolderAccountBank holderAccountBankToSave) {
		this.holderAccountBankToSave = holderAccountBankToSave;
	}

	/**
	 * Gets the ind retirement payment to blocked holder.
	 *
	 * @return the ind retirement payment to blocked holder
	 */
	public Boolean getIndRetirementPaymentToBlockedHolder() {
		return indRetirementPaymentToBlockedHolder;
	}

	/**
	 * Sets the ind retirement payment to blocked holder.
	 *
	 * @param indRetirementPaymentToBlockedHolder the new ind retirement payment to blocked holder
	 */
	public void setIndRetirementPaymentToBlockedHolder(
			Boolean indRetirementPaymentToBlockedHolder) {
		this.indRetirementPaymentToBlockedHolder = indRetirementPaymentToBlockedHolder;
	}

	/**
	 * Gets the ind payment retirement rights ban.
	 *
	 * @return the ind payment retirement rights ban
	 */
	public Boolean getIndPaymentRetirementRightsBan() {
		return indPaymentRetirementRightsBan;
	}

	/**
	 * Sets the ind payment retirement rights ban.
	 *
	 * @param indPaymentRetirementRightsBan the new ind payment retirement rights ban
	 */
	public void setIndPaymentRetirementRightsBan(
			Boolean indPaymentRetirementRightsBan) {
		this.indPaymentRetirementRightsBan = indPaymentRetirementRightsBan;
	}

	/**
	 * Gets the lst retirement funds operation to.
	 *
	 * @return the lst retirement funds operation to
	 */
	public GenericDataModel<RetirementFundsOperationTO> getLstRetirementFundsOperationTO() {
		return lstRetirementFundsOperationTO;
	}

	/**
	 * Sets the lst retirement funds operation to.
	 *
	 * @param lstRetirementFundsOperationTO the new lst retirement funds operation to
	 */
	public void setLstRetirementFundsOperationTO(
			GenericDataModel<RetirementFundsOperationTO> lstRetirementFundsOperationTO) {
		this.lstRetirementFundsOperationTO = lstRetirementFundsOperationTO;
	}

	/**
	 * Gets the ind benefit automatic.
	 *
	 * @return the ind benefit automatic
	 */
	public Boolean getIndBenefitAutomatic() {
		return indBenefitAutomatic;
	}

	/**
	 * Sets the ind benefit automatic.
	 *
	 * @param indBenefitAutomatic the new ind benefit automatic
	 */
	public void setIndBenefitAutomatic(Boolean indBenefitAutomatic) {
		this.indBenefitAutomatic = indBenefitAutomatic;
	}

	/**
	 * Gets the lst holder cash movement for benefit.
	 *
	 * @return the lst holder cash movement for benefit
	 */
	public List<HolderCashMovement> getLstHolderCashMovementForBenefit() {
		return lstHolderCashMovementForBenefit;
	}

	/**
	 * Sets the lst holder cash movement for benefit.
	 *
	 * @param lstHolderCashMovementForBenefit the new lst holder cash movement for benefit
	 */
	public void setLstHolderCashMovementForBenefit(
			List<HolderCashMovement> lstHolderCashMovementForBenefit) {
		this.lstHolderCashMovementForBenefit = lstHolderCashMovementForBenefit;
	}

	/**
	 * Gets the dm holder cash movement for benefit.
	 *
	 * @return the dm holder cash movement for benefit
	 */
	public GenericDataModel<HolderCashMovement> getDmHolderCashMovementForBenefit() {
		return dmHolderCashMovementForBenefit;
	}

	/**
	 * Sets the dm holder cash movement for benefit.
	 *
	 * @param dmHolderCashMovementForBenefit the new dm holder cash movement for benefit
	 */
	public void setDmHolderCashMovementForBenefit(
			GenericDataModel<HolderCashMovement> dmHolderCashMovementForBenefit) {
		this.dmHolderCashMovementForBenefit = dmHolderCashMovementForBenefit;
	}

	/**
	 * Gets the benefit payment file.
	 *
	 * @return the benefit payment file
	 */
	public BenefitPaymentFile getBenefitPaymentFile() {
		return benefitPaymentFile;
	}

	/**
	 * Sets the benefit payment file.
	 *
	 * @param benefitPaymentFile the new benefit payment file
	 */
	public void setBenefitPaymentFile(BenefitPaymentFile benefitPaymentFile) {
		this.benefitPaymentFile = benefitPaymentFile;
	}

	/**
	 * Gets the ind holder benefit detail.
	 *
	 * @return the ind holder benefit detail
	 */
	public Boolean getIndHolderBenefitDetail() {
		return indHolderBenefitDetail;
	}

	/**
	 * Sets the ind holder benefit detail.
	 *
	 * @param indHolderBenefitDetail the new ind holder benefit detail
	 */
	public void setIndHolderBenefitDetail(Boolean indHolderBenefitDetail) {
		this.indHolderBenefitDetail = indHolderBenefitDetail;
	}

	/**
	 * Gets the retirement funds operation to.
	 *
	 * @return the retirement funds operation to
	 */
	public RetirementFundsOperationTO getRetirementFundsOperationTO() {
		return retirementFundsOperationTO;
	}

	/**
	 * Sets the retirement funds operation to.
	 *
	 * @param retirementFundsOperationTO the new retirement funds operation to
	 */
	public void setRetirementFundsOperationTO(
			RetirementFundsOperationTO retirementFundsOperationTO) {
		this.retirementFundsOperationTO = retirementFundsOperationTO;
	}

	/**
	 * Gets the swift message file.
	 *
	 * @return the swift message file
	 */
	public SwiftMessage getSwiftMessageFile() {
		return swiftMessageFile;
	}

	/**
	 * Sets the swift message file.
	 *
	 * @param swiftMessageFile the new swift message file
	 */
	public void setSwiftMessageFile(SwiftMessage swiftMessageFile) {
		this.swiftMessageFile = swiftMessageFile;
	}

	/**
	 * Gets the lst retirement funds operation group type.
	 *
	 * @return the lst retirement funds operation group type
	 */
	public List<ParameterTable> getLstRetirementFundsOperationGroupType() {
		return lstRetirementFundsOperationGroupType;
	}

	/**
	 * Sets the lst retirement funds operation group type.
	 *
	 * @param lstRetirementFundsOperationGroupType the new lst retirement funds operation group type
	 */
	public void setLstRetirementFundsOperationGroupType(
			List<ParameterTable> lstRetirementFundsOperationGroupType) {
		this.lstRetirementFundsOperationGroupType = lstRetirementFundsOperationGroupType;
	}

	/**
	 * Gets the lst holder funds operation.
	 *
	 * @return the lst holder funds operation
	 */
	public List<HolderFundsOperation> getLstHolderFundsOperation() {
		return lstHolderFundsOperation;
	}

	/**
	 * Gets the lst institution cash account bcrd.
	 *
	 * @return the lst institution cash account bcrd
	 */
	public GenericDataModel<InstitutionCashAccount> getLstInstitutionCashAccountBcrd() {
		return lstInstitutionCashAccountBcrd;
	}

	/**
	 * Sets the lst institution cash account bcrd.
	 *
	 * @param lstInstitutionCashAccountBcrd the new lst institution cash account bcrd
	 */
	public void setLstInstitutionCashAccountBcrd(
			GenericDataModel<InstitutionCashAccount> lstInstitutionCashAccountBcrd) {
		this.lstInstitutionCashAccountBcrd = lstInstitutionCashAccountBcrd;
	}

	/**
	 * Sets the lst holder funds operation.
	 *
	 * @param lstHolderFundsOperation the new lst holder funds operation
	 */
	public void setLstHolderFundsOperation(List<HolderFundsOperation> lstHolderFundsOperation) {
		this.lstHolderFundsOperation = lstHolderFundsOperation;
	}

	/**
	 * Gets the ind show bcrd ins account.
	 *
	 * @return the ind show bcrd ins account
	 */
	public Boolean getIndShowBcrdInsAccount() {
		return indShowBcrdInsAccount;
	}

	/**
	 * Sets the ind show bcrd ins account.
	 *
	 * @param indShowBcrdInsAccount the new ind show bcrd ins account
	 */
	public void setIndShowBcrdInsAccount(Boolean indShowBcrdInsAccount) {
		this.indShowBcrdInsAccount = indShowBcrdInsAccount;
	}

	/**
	 * Gets the ind guarantees retirement.
	 *
	 * @return the ind guarantees retirement
	 */
	public Boolean getIndGuaranteesRetirement() {
		return indGuaranteesRetirement;
	}

	/**
	 * Sets the ind guarantees retirement.
	 *
	 * @param indGuaranteesRetirement the new ind guarantees retirement
	 */
	public void setIndGuaranteesRetirement(Boolean indGuaranteesRetirement) {
		this.indGuaranteesRetirement = indGuaranteesRetirement;
	}

	/**
	 * Gets the ind return retirement.
	 *
	 * @return the ind return retirement
	 */
	public Boolean getIndReturnRetirement() {
		return indReturnRetirement;
	}

	/**
	 * Sets the ind return retirement.
	 *
	 * @param indReturnRetirement the new ind return retirement
	 */
	public void setIndReturnRetirement(Boolean indReturnRetirement) {
		this.indReturnRetirement = indReturnRetirement;
	}
	
	public Boolean getIndCorporativeProcessReturns() {
		return indCorporativeProcessReturns;
	}

	public void setIndCorporativeProcessReturns(Boolean indCorporativeProcessReturns) {
		this.indCorporativeProcessReturns = indCorporativeProcessReturns;
	}

	/**
	 * Gets the lst operation types to save.
	 *
	 * @return the lst operation types to save
	 */
	public List<FundsOperationType> getLstOperationTypesToSave() {
		return lstOperationTypesToSave;
	}

	/**
	 * Sets the lst operation types to save.
	 *
	 * @param lstOperationTypesToSave the new lst operation types to save
	 */
	public void setLstOperationTypesToSave(
			List<FundsOperationType> lstOperationTypesToSave) {
		this.lstOperationTypesToSave = lstOperationTypesToSave;
	}

	/**
	 * Gets the participant settlement.
	 *
	 * @return the participant settlement
	 */
	public ParticipantSettlement getParticipantSettlement() {
		return participantSettlement;
	}

	/**
	 * Sets the participant settlement.
	 *
	 * @param participantSettlement the new participant settlement
	 */
	public void setParticipantSettlement(ParticipantSettlement participantSettlement) {
		this.participantSettlement = participantSettlement;
	}

	/**
	 * Checks if is ind retirement automatic.
	 *
	 * @return the indRetirementAutomatic
	 */
	public Boolean getIndRetirementAutomatic() {
		return indRetirementAutomatic;
	}

	/**
	 * Sets the ind retirement automatic.
	 *
	 * @param indRetirementAutomatic the indRetirementAutomatic to set
	 */
	public void setIndRetirementAutomatic(Boolean indRetirementAutomatic) {
		this.indRetirementAutomatic = indRetirementAutomatic;
	}

	/**
	 * Gets the gm lst participant position.
	 *
	 * @return the gmLstParticipantPosition
	 */
	public GenericDataModel<ParticipantPosition> getGmLstParticipantPosition() {
		return gmLstParticipantPosition;
	}

	/**
	 * Sets the gm lst participant position.
	 *
	 * @param gmLstParticipantPosition the gmLstParticipantPosition to set
	 */
	public void setGmLstParticipantPosition(
			GenericDataModel<ParticipantPosition> gmLstParticipantPosition) {
		this.gmLstParticipantPosition = gmLstParticipantPosition;
	}

	/**
	 * Gets the array participant position.
	 *
	 * @return the arrayParticipantPosition
	 */
	public ParticipantPosition[] getArrayParticipantPosition() {
		return arrayParticipantPosition;
	}

	/**
	 * Sets the array participant position.
	 *
	 * @param arrayParticipantPosition the arrayParticipantPosition to set
	 */
	public void setArrayParticipantPosition(
			ParticipantPosition[] arrayParticipantPosition) {
		this.arrayParticipantPosition = arrayParticipantPosition;
	}

	/**
	 * Gets the lst send lipe.
	 *
	 * @return the lstSendLipe
	 */
	public List<BooleanType> getLstSendLipe() {
		return lstSendLipe;
	}

	/**
	 * Sets the lst send lipe.
	 *
	 * @param lstSendLipe the lstSendLipe to set
	 */
	public void setLstSendLipe(List<BooleanType> lstSendLipe) {
		this.lstSendLipe = lstSendLipe;
	}

	/**
	 * Gets the ind send lip.
	 *
	 * @return the indSendLip
	 */
	public Integer getIndSendLip() {
		return indSendLip;
	}

	/**
	 * Sets the ind send lip.
	 *
	 * @param indSendLip the indSendLip to set
	 */
	public void setIndSendLip(Integer indSendLip) {
		this.indSendLip = indSendLip;
	}

	/**
	 * Checks if is view cbo send lip.
	 *
	 * @return the viewCboSendLip
	 */
	public boolean isViewCboSendLip() {
		return viewCboSendLip;
	}

	/**
	 * Sets the view cbo send lip.
	 *
	 * @param viewCboSendLip the viewCboSendLip to set
	 */
	public void setViewCboSendLip(boolean viewCboSendLip) {
		this.viewCboSendLip = viewCboSendLip;
	}
	
	public Integer getObjInstitutionBankAccountsType() {
		return objInstitutionBankAccountsType;
	}

	public void setObjInstitutionBankAccountsType(Integer objInstitutionBankAccountsType) {
		this.objInstitutionBankAccountsType = objInstitutionBankAccountsType;
	}
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipant(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(objInstitutionBankAccountsType));
	}	
	
	/**
	 * Is Institution Type Issuer.
	 *
	 * @return validate Institution Type Issuer
	 */
	public boolean isIndInstitutionTypeIssuer(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.ISSUER.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Is Institution Type Bank.
	 *
	 * @return validate Institution Type Bank
	 */
	public boolean isIndInstitutionTypeBank(){
		return Validations.validateIsNotNull(objInstitutionBankAccountsType) &&
				objInstitutionBankAccountsType > 0 &&
				(InstitutionBankAccountsType.BANK.getCode().equals(objInstitutionBankAccountsType));
	}
	
	/**
	 * Fill institution bank accounts type.
	 */
	public void fillInstitutionBankAccountsTypeReturn(){
		lstInstitutionBankAccountsTypeReturn = new ArrayList<ParameterTable>();
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setIndicator4(GeneralConstants.ONE_VALUE_INTEGER);
		try {
			lstInstitutionBankAccountsTypeReturn.addAll(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.setObjInstitutionBankAccountsType(InstitutionBankAccountsType.PARTICIPANT.getCode());
	}
	
	public void onChangeInstitutionType(){
		JSFUtilities.hideGeneralDialogues();
		this.setLstInstitutionCashAccount(null);
		participantToSave.setIdParticipantPk(null);
		this.setIssuer(new Issuer());
		indCorporativeProcessReturns = Boolean.FALSE;
	}
	
	public List<ParameterTable> getLstInstitutionBankAccountsTypeReturn() {
		return lstInstitutionBankAccountsTypeReturn;
	}

	public void setLstInstitutionBankAccountsTypeReturn(List<ParameterTable> lstInstitutionBankAccountsTypeReturn) {
		this.lstInstitutionBankAccountsTypeReturn = lstInstitutionBankAccountsTypeReturn;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
}
