package com.pradera.funds.fundsoperations.retirement.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.business.to.InstitutionCashAccountTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.swift.facade.SwiftWithdrawlProcess;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.funds.fundsoperations.deposit.service.ManageDepositServiceBean;
import com.pradera.funds.fundsoperations.retirement.service.RetirementFundsOperationServiceBean;
import com.pradera.funds.fundsoperations.retirement.to.HolderCashMovementTO;
import com.pradera.funds.fundsoperations.retirement.to.HolderFundsOperationTO;
import com.pradera.funds.fundsoperations.retirement.to.InternationalOperationTO;
import com.pradera.funds.fundsoperations.retirement.to.MechanismOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.corporatives.BenefitPaymentFile;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.funds.FundsInternationalOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.SwiftMessage;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.funds.type.HolderFundsOperationStateType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.settlements.core.service.CollectionProcessService;
import com.pradera.settlements.core.service.FundsComponentSingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class RetirementFundsOperationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class RetirementFundsOperationServiceFacade {
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The funds operation service bean. */
	@Inject
	private RetirementFundsOperationServiceBean fundsOperationServiceBean;
	
	/** The param service bean. */
	@EJB
	private ParameterServiceBean paramServiceBean;
	/** The executor component service bean. */
	@Inject
    Instance<FundsComponentService> executorComponentServiceBean;
	
	/** The deposit service bean. */
	@Inject
	private ManageDepositServiceBean depositServiceBean;
	
	/** The swift withdrawl process. */
	@EJB
	SwiftWithdrawlProcess swiftWithdrawlProcess;
	
	/** The cash account service. */
	@EJB
	CashAccountManagementService cashAccountService;	
	
	/** The collection process service bean. */
	@Inject
	CollectionProcessService collectionProcessServiceBean;
	
	/** The reception service. */
	@EJB
	AutomaticReceptionFundsService receptionService;
	
	/** The funds component singleton. */
	@EJB
	private FundsComponentSingleton fundsComponentSingleton;
	
	/**
	 * Search funds operations types.
	 *
	 * @param operationGroup the operation group
	 * @param view the view
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperationType> searchFundsOperationsTypes(Integer operationGroup, String view) throws ServiceException{
		return this.fundsOperationServiceBean.searchFundsOperationsTypes(operationGroup, view);
	}
	
	/**
	 * Gets the participant mnemonic.
	 *
	 * @param participantId the participant id
	 * @return the participant mnemonic
	 * @throws ServiceException the service exception
	 */
	public String getParticipantMnemonic(Long participantId) throws ServiceException{
		return this.fundsOperationServiceBean.getParticipantMnemonic(participantId);
	}
	/**
	 * Find.
	 *
	 * @param <T> the generic type
	 * @param type the type
	 * @param id the id
	 * @return the t
	 */
	public <T> T find(Class<T> type, Object id) {
		return (T) this.fundsOperationServiceBean.find(type, id);
	}
	/**
	 * Gets the issuer mnemonic.
	 *
	 * @param issuerId the issuer id
	 * @return the issuer mnemonic
	 * @throws ServiceException the service exception
	 */
	public String getIssuerMnemonic(String issuerId) throws ServiceException{
		return this.fundsOperationServiceBean.getIssuerMnemonic(issuerId);
	}
	
	/**
	 * Save funds operation retirement and deposit.
	 *
	 * @param sourceFundsOperation the source funds operation
	 * @param targetFundsOperation the target funds operation
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger(process=BusinessProcessType.FOUNDS_RETIREMENT_MANUAL_REGISTER)
	public FundsOperation saveFundsOperationRetirementAndDeposit(FundsOperation sourceFundsOperation, FundsOperation targetFundsOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

        Integer nextOperationNumber = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), 
        		targetFundsOperation.getFundsOperationGroup());
        targetFundsOperation.setFundOperationNumber(nextOperationNumber);
        targetFundsOperation.setIndAutomatic(0);
		targetFundsOperation.setIndCentralized(0);
		targetFundsOperation.setRegistryUser(loggerUser.getUserName());
		targetFundsOperation.setRegistryDate(CommonsUtilities.currentDate());
		targetFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
		targetFundsOperation.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_SUPPLY_DEPOSIT.getCode());
       
		sourceFundsOperation.setIndAutomatic(0);
		sourceFundsOperation.setIndCentralized(0);
		sourceFundsOperation.setFundsOperationClass(2);
		sourceFundsOperation.setFundOperationNumber(nextOperationNumber);
		sourceFundsOperation.setRegistryUser(loggerUser.getUserName());
		sourceFundsOperation.setRegistryDate(CommonsUtilities.currentDate());		
		sourceFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());				
		
		
		if(sourceFundsOperation.getInstitutionCashAccount().getIndRelatedBcrd().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			//Creating Funds Operation for centralizing cash account
	        FundsOperation centralizingFundsOperation = this.getFundsOperationForCentralAccount(sourceFundsOperation);
	        centralizingFundsOperation.setRegistryUser(loggerUser.getUserName());
	        centralizingFundsOperation.setFundOperationNumber(nextOperationNumber);
			
			 //Saving Funds Operation for centralizing cash account
			centralizingFundsOperation  = fundsOperationServiceBean.create(centralizingFundsOperation);	
			
			sourceFundsOperation.setFundsOperation(centralizingFundsOperation);	//setting centralizing funds operation
			
			//Executing component for centralizing accounts funds operation
	        this.executeComponenteService(centralizingFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_MANUAL_REGISTER.getCode());
		}
		
		 //Saving the FundsOperation
		sourceFundsOperation 		= fundsOperationServiceBean.create(sourceFundsOperation);
		
		targetFundsOperation.setFundsOperation(sourceFundsOperation);//Setting retirement funds operation
		//Saving the target FundsOperation
		fundsOperationServiceBean.create(targetFundsOperation);
		

        //now we have to execute the funds component
        this.executeComponenteService(sourceFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_MANUAL_REGISTER.getCode());                        
        		
		return sourceFundsOperation;
		
	}
	
	/**
	 * Save Massive.
	 *
	 * @param lstFundsOperation the lst funds operation
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public List<FundsOperation> saveLstFundsOperation(List<FundsOperation> lstFundsOperation) throws ServiceException{
		List<FundsOperation> lstFundsOperationAux = new ArrayList<FundsOperation>();
		for(FundsOperation objFundsOperation : lstFundsOperation){
			FundsOperation objFundsOperationAux = saveFundsOperation(objFundsOperation, objFundsOperation.getInstitutionCashAccount());
			lstFundsOperationAux.add(objFundsOperationAux);
		}
		return lstFundsOperationAux;
	}
	
	/**
	 * Save.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @param institutionCashAccount the institution cash account
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public FundsOperation saveFundsOperation(FundsOperation objFundsOperation, InstitutionCashAccount institutionCashAccount) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

        //SETTING COMMOM DATA
        Integer nextOperationNumber = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), 
        									objFundsOperation.getFundsOperationGroup());

        objFundsOperation.setFundOperationNumber(nextOperationNumber);
        objFundsOperation.setRegistryUser(loggerUser.getUserName());
        objFundsOperation.setRegistryDate(CommonsUtilities.currentDate());
        objFundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
        objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
      	objFundsOperation.setIndCentralized(BooleanType.NO.getCode());
        objFundsOperation.setIndAutomatic(BooleanType.NO.getCode());
        
        if(Validations.validateIsNotNull(institutionCashAccount)){
        	//SETTING INSTITUTION CASH ACCOUNT
        	institutionCashAccount.setRetirementAmount(objFundsOperation.getOperationAmount());
        	objFundsOperation.setInstitutionCashAccount(institutionCashAccount);
        	//fundsOperationServiceBean.update(institutionCashAccount);
        }
        //Validating institution cash accounts
        if(objFundsOperation.getInstitutionCashAccount().getIndRelatedBcrd().equals(BooleanType.YES.getCode())){
	        //Creating Funds Operation for centralizing cash account
	        FundsOperation centralizingFundsOperation = this.getFundsOperationForCentralAccount(objFundsOperation);
	        centralizingFundsOperation.setRegistryUser(loggerUser.getUserName());
	        centralizingFundsOperation.setFundOperationNumber(objFundsOperation.getFundOperationNumber());
	        //Saving..
	        centralizingFundsOperation = fundsOperationServiceBean.create(centralizingFundsOperation);
	        
	        objFundsOperation.setFundsOperation(centralizingFundsOperation);
	      //now we have to execute the funds component for centralizing funds Operagtion account
	        this.executeComponenteService(centralizingFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_MANUAL_REGISTER.getCode());
        }
         
        if(objFundsOperation.getFundsOperationGroup().equals(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode())){
        	if(objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())
   			  ||objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())
   			  ||objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
        		
        		FundsOperationType fundsOperationType = new FundsOperationType();
        		fundsOperationType.setIdFundsOperationTypePk(objFundsOperation.getFundsOperationType());
        		
        		//Holder holder = this.fundsOperationServiceBean.find(Holder.class,objFundsOperation.getHolder().getIdHolderPk());
        		        		
        		for(HolderFundsOperation holderFundsOperation : objFundsOperation.getHolderFundsOperations()){
	        		//holderFundsOperation.setHolder(holder);
	        		//holderFundsOperation.setHolderDocumentNumber(holder.getDocumentNumber());
        			//holderFundsOperation.setFullNameHolder(holder.getFullName());
	        		holderFundsOperation.setHolderOperationAmount(objFundsOperation.getOperationAmount());  
	        		holderFundsOperation.setFundsOperation(objFundsOperation);
	        		holderFundsOperation.setFundsOperationType(fundsOperationType);
	        		holderFundsOperation.setHolderOperationState(HolderFundsOperationStateType.REGISTERED.getCode());
        		}
        	}
        }
        
      //Saving the FundsOperation
        if(objFundsOperation.getInstitutionCashAccount()!=null){
        	Long institutionCashAccountId = objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk();
        	objFundsOperation.setInstitutionCashAccount(fundsOperationServiceBean.find(InstitutionCashAccount.class, institutionCashAccountId)); 
        }
        objFundsOperation = fundsOperationServiceBean.create(objFundsOperation);
        //now we have to execute the funds component
        //if(objFundsOperation.getFundsOperationGroup().equals(Integer.parseInt("709")))
        	//return objFundsOperation;
        executeComponenteService(objFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_MANUAL_REGISTER.getCode());

		return objFundsOperation;
	}
	
	/**
	 * Save funds and international operation.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @param internationalOperation the international operation
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger(process=BusinessProcessType.FOUNDS_RETIREMENT_MANUAL_REGISTER)
	public FundsOperation saveFundsAndInternationalOperation(FundsOperation objFundsOperation, InternationalOperation internationalOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        //SETTING COMMOM DATA
        objFundsOperation.setRegistryUser(loggerUser.getUserName());
        objFundsOperation.setRegistryDate(CommonsUtilities.currentDate());
        objFundsOperation.setFundsOperationClass(2);
        objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
        objFundsOperation.setIndCentralized(0);
        objFundsOperation.setIndAutomatic(0);
        
        if(objFundsOperation.getInstitutionCashAccount().getIndRelatedBcrd().equals(BooleanType.YES.getCode())){
	        //Creating Funds Operation for centralizing cash account
	        FundsOperation centralizingFundsOperation = this.getFundsOperationForCentralAccount(objFundsOperation);
	        centralizingFundsOperation.setRegistryUser(loggerUser.getUserName());
	        //Saving..
	        centralizingFundsOperation = fundsOperationServiceBean.create(centralizingFundsOperation);
	        
	        objFundsOperation.setFundsOperation(centralizingFundsOperation);
	        
	        //Executing component for centralizing accounts funds operation
//	        this.executeComponenteService(centralizingFundsOperation,BusinessProcessType.FOUNDS_RETIREMENT_MANUAL_REGISTER.getCode());
        }
        //Saving the FundsOperation
        objFundsOperation = fundsOperationServiceBean.create(objFundsOperation);
        //FINDING COMPLETE INTERNATIONAL OPERATION OBJECT..
        internationalOperation = this.fundsOperationServiceBean.find(InternationalOperation.class, internationalOperation.getIdInternationalOperationPk());


        FundsInternationalOperation fundsInternationalOperation = new FundsInternationalOperation();
        
        fundsInternationalOperation.setFundsOperation(objFundsOperation);
        fundsInternationalOperation.setInternationalOperation(internationalOperation);
                       
        fundsOperationServiceBean.create(fundsInternationalOperation);
        
        //now we have to execute the funds component
//        this.executeComponenteService(objFundsOperation,BusinessProcessType.FOUNDS_RETIREMENT_MANUAL_REGISTER.getCode());
        
		return objFundsOperation;
	}	
	
	/**
	 * Confirm request.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @throws ServiceException the service exception
	 */
	

	
	/**
	 * Reject request.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_RETIREMENT_REJECT)
	public void rejectRequest(FundsOperation objFundsOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
        
        objFundsOperation = this.fundsOperationServiceBean.find(FundsOperation.class, objFundsOperation.getIdFundsOperationPk());        
        objFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode());

        if(Validations.validateIsNotNull(objFundsOperation.getFundsOperation())
        	&& Validations.validateIsNotNull(objFundsOperation.getFundsOperation().getIdFundsOperationPk())	){        	
        	Long idRelatedFundsOperation= objFundsOperation.getFundsOperation().getIdFundsOperationPk();
	        FundsOperation objRelatedFundsOperation = this.fundsOperationServiceBean.find(FundsOperation.class, idRelatedFundsOperation);
	        objRelatedFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED.getCode());	        
	        /** EXECUTING COMPONENT FOR RELATED FUNDS OPERATION **/
	        this.executeComponenteService(objRelatedFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_REJECT.getCode());
	        this.fundsOperationServiceBean.update(objRelatedFundsOperation);
        }
        
        if(objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode())
		  ||objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode())
		  ||objFundsOperation.getFundsOperationType().equals(com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode())){
        	
        	HolderFundsOperationTO holderFundsOperationTO = new HolderFundsOperationTO();
        	holderFundsOperationTO.setIdHolderFundsOperationPk(objFundsOperation.getIdFundsOperationPk());
        	
        	//Calling service to get holder funds operation related
        	HolderFundsOperation objHolderFundsOperation = this.fundsOperationServiceBean.searchHolderFundsOperation(holderFundsOperationTO);
        	objHolderFundsOperation.setHolderOperationState(HolderFundsOperationStateType.REJECTED.getCode());
			this.fundsOperationServiceBean.update(objHolderFundsOperation);
        }
        
        /** EXECUTING COMPONENT FUNDS OPERATION **/
        this.executeComponenteService(objFundsOperation,BusinessProcessType.FUNDS_RETIREMENT_REJECT.getCode());        
        this.fundsOperationServiceBean.update(objFundsOperation);                
	}
	
	/**
	 * Execute swift component.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @throws ServiceException the service exception
	 */
	public void executeSwiftComponent(FundsOperation objFundsOperation) throws ServiceException{
		swiftWithdrawlProcess.executeSwiftWithdrawl(objFundsOperation, null, null);
	}
	
	/**
	 * Execute componente service.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @param businessProcess the business process
	 * @throws ServiceException the service exception
	 */
	public void executeComponenteService(FundsOperation objFundsOperation, Long businessProcess)throws ServiceException{
		FundsComponentTO objFundsComponentTO = new FundsComponentTO();
        objFundsComponentTO.setIdFundsOperation(objFundsOperation.getIdFundsOperationPk());
        objFundsComponentTO.setIdBusinessProcess(businessProcess);
        objFundsComponentTO.setIdFundsOperationType(objFundsOperation.getFundsOperationType());
        objFundsComponentTO.setCashAmount(objFundsOperation.getOperationAmount());
        objFundsComponentTO.setIdInstitutionCashAccount(objFundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
        objFundsComponentTO.setOperationState(objFundsOperation.getOperationState());
        //Executing component
        executorComponentServiceBean.get().executeFundsComponent(objFundsComponentTO);
	}
	
	/**
	 * Search institution cash accounts.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> searchInstitutionCashAccounts(InstitutionCashAccountTO institutionCashAccountTO) throws ServiceException{
		return this.fundsOperationServiceBean.searchInstitutionCashAccounts(institutionCashAccountTO);
	}
	
	/**
	 * Search institution cash account.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the institution cash account
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccount searchInstitutionCashAccount(InstitutionCashAccountTO institutionCashAccountTO) throws ServiceException{
		return this.fundsOperationServiceBean.searchInstitutionCashAccount(institutionCashAccountTO);
	}
	
	/**
	 * Search institution cash account no modalities.
	 *
	 * @param institutionCashAccountTO the institution cash account to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionCashAccount> searchInstitutionCashAccountNoModalities(InstitutionCashAccountTO institutionCashAccountTO) throws ServiceException{
		return this.fundsOperationServiceBean.searchInstitutionCashAccountNoModalities(institutionCashAccountTO);
	}
	

	
	/**
	 * Gets the currency description.
	 *
	 * @param currencyInt the currency int
	 * @return the currency description
	 */
	private String getCurrencyDescription(Integer currencyInt){
	   String currency = null;
	   if(MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_DOLARES.getCode().equals(currencyInt)){
	    currency = FundsType.DESCRIPTION_SHORT_CURRENCY_DOLLAR.getStrCode();
	   }else if(MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_PESOS.getCode().equals(currencyInt)){
	    currency = FundsType.DESCRIPTION_SHORT_CURRENCY_LOCAL.getStrCode();
	   }
	   return currency;
	}
	
	/**
	 * Gets the parameter list.
	 *
	 * @param type the type
	 * @return the parameter list
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterList(MasterTableType type) throws ServiceException {

		ParameterTableTO parameterFilter = new ParameterTableTO();
		
		parameterFilter.setMasterTableFk(type.getCode());
		parameterFilter.setState(1);
		
		if(MasterTableType.MASTER_TABLE_FUND_OPERATION_GROUP_RETIREMENT.getCode().equals(type.getCode())){
			parameterFilter.setIndicator4(1);
		}
		
		List<ParameterTable> parameterList = new ArrayList<ParameterTable>();
	
		parameterList = this.paramServiceBean.getListParameterTableServiceBean(parameterFilter);	
		return parameterList;

	}
	
	/**
	 * Find a issuers.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> findAIssuers() throws ServiceException{
		return this.fundsOperationServiceBean.findAIssuers();
	}
	
	/**
	 * Search participant mechanisms.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> searchParticipantMechanisms(Long idParticipantPk) throws ServiceException{
		return this.fundsOperationServiceBean.searchParticipantMechanisms(idParticipantPk);
	}
	
	/**
	 * Find all participants.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> findAllParticipants() throws ServiceException{
		return this.fundsOperationServiceBean.findAllParticipants();
	}
	
	/**
	 * Search modality group.
	 *
	 * @param negotiationMechanismId the negotiation mechanism id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ModalityGroup> searchModalityGroup(Long negotiationMechanismId) throws ServiceException{
		return this.fundsOperationServiceBean.searchModalityGroup(negotiationMechanismId);
	}
	
	/**
	 * Search settlement schema.
	 *
	 * @param idModalityGroupPk the id modality group pk
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer searchSettlementSchema(Long idModalityGroupPk) throws ServiceException{
		return this.fundsOperationServiceBean.searchSettlementSchema(idModalityGroupPk);
	}
	
	/**
	 * Find mechanism operation.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation findMechanismOperation(MechanismOperationTO mechanismOperationTO) throws ServiceException{
		return this.fundsOperationServiceBean.findMechanismOperation(mechanismOperationTO);
	}
	
	/**
	 * Find participant operation.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @param operationPart the operation part
	 * @return the participant operation
	 * @throws ServiceException the service exception
	 */
	public ParticipantSettlement findParticipantSettlement(MechanismOperationTO mechanismOperationTO,Integer operationPart) throws ServiceException{
		ParticipantSettlement participantSettlement = this.fundsOperationServiceBean.findParticipantSettlement(mechanismOperationTO,operationPart);
		
		if(Validations.validateIsNotNull(participantSettlement.getSettlementAmount())){
			 if(Validations.validateIsNull(participantSettlement.getDepositedAmount())){
				 participantSettlement.setDepositedAmount(new BigDecimal(0));
			 }
			Double unpaidAmount = (participantSettlement.getSettlementAmount().doubleValue() - participantSettlement.getDepositedAmount().doubleValue());
			if(unpaidAmount >= 0){
				participantSettlement.setUnpaidAmount(new BigDecimal(unpaidAmount));
			}else{
				Double overpaidAmount = (participantSettlement.getDepositedAmount().doubleValue() - participantSettlement.getSettlementAmount().doubleValue());
				participantSettlement.setOverpaidAmount(new BigDecimal(overpaidAmount));
			}
		}
		return participantSettlement;
	}
	/**
	 * Find participant operation.
	 *
	 * @param idMechanismOperationFk the id mechanism operation fk
	 * @param idParticipantPk the id participant pk
	 * @param operationPart the operation part
	 * @return the participant operation
	 * @throws ServiceException the service exception
	 */
	public ParticipantSettlement findParticipantSettlement(Long idMechanismOperationFk, Long idParticipantPk,Integer operationPart) throws ServiceException{
		ParticipantSettlement participantSettlement = fundsOperationServiceBean.findParticipantSettlement(idMechanismOperationFk, idParticipantPk,operationPart);
		
		if(Validations.validateIsNotNull(participantSettlement.getSettlementAmount())){
			 if(Validations.validateIsNull(participantSettlement.getDepositedAmount())){
				 participantSettlement.setDepositedAmount(new BigDecimal(0));
			 }
			Double unpaidAmount = (participantSettlement.getSettlementAmount().doubleValue() - participantSettlement.getDepositedAmount().doubleValue());
			if(unpaidAmount >= 0){
				participantSettlement.setUnpaidAmount(new BigDecimal(unpaidAmount));
			}else{
				Double overpaidAmount = (participantSettlement.getDepositedAmount().doubleValue() - participantSettlement.getSettlementAmount().doubleValue());
				participantSettlement.setOverpaidAmount(new BigDecimal(overpaidAmount));
			}
		}
		return participantSettlement;
	}
	/**
	 * Search lst negotiation modalities.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> searchLstNegotiationModalities() throws ServiceException{
		return this.fundsOperationServiceBean.searchLstNegotiationModalities();
	}
	
	/**
	 * Find banks.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Bank> findBanks() throws ServiceException{
		return this.fundsOperationServiceBean.findBanks();
	}
	
	/**
	 * Find international depositories.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public  List<InternationalDepository> findInternationalDepositories() throws ServiceException{
		return this.fundsOperationServiceBean.findInternationalDepositories();
	}
	
	/**
	 * Search international operations.
	 *
	 * @param internationalOperationTO the international operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InternationalOperation>searchInternationalOperations(InternationalOperationTO internationalOperationTO)throws ServiceException{
		return this.fundsOperationServiceBean.searchInternationalOperations(internationalOperationTO);
	}
	
	/**
	 * Search bank description.
	 *
	 * @param idInstitutionCashAccount the id institution cash account
	 * @return the institution cash account to
	 * @throws ServiceException the service exception
	 */
	public InstitutionCashAccountTO searchBankDescription(Long idInstitutionCashAccount) throws ServiceException{
		return this.fundsOperationServiceBean.searchBankDescription(idInstitutionCashAccount);
	}
	
	/**
	 * Search holder cash movement.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderCashMovement> searchHolderCashMovement(HolderCashMovementTO filter) throws ServiceException{	
		return this.fundsOperationServiceBean.searchHolderCashMovement(filter);
	}
	
	/**
	 * Search retirement fund operations.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger(process=BusinessProcessType.FOUNDS_RETIREMENT_QUERY)
	public List<FundsOperation>searchRetirementFundOperations(FundsOperation filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);

        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return this.fundsOperationServiceBean.searchRetirementFundOperations(filter);
	}
	
	/**
	 * Funds operation detail.
	 *
	 * @param fundsOperationId the funds operation id
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	public FundsOperation fundsOperationDetail(Long fundsOperationId) throws ServiceException{
		return this.fundsOperationServiceBean.fundsOperationDetail(fundsOperationId);
	}
	
	/**
	 * Find international operation by funds operation.
	 *
	 * @param idFundsOperationPk the id funds operation pk
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation findInternationalOperationByFundsOperation(Long idFundsOperationPk) throws ServiceException{
		return this.fundsOperationServiceBean.findInternationalOperationByFundsOperation(idFundsOperationPk);
	}
	
	/**
	 * Gets the funds operation for central account.
	 *
	 * @param objFundsOperation the obj funds operation
	 * @return the funds operation for central account
	 * @throws ServiceException the service exception
	 */
	public FundsOperation getFundsOperationForCentralAccount(FundsOperation objFundsOperation) throws ServiceException{
		FundsOperation centralizingFundsOperation = new FundsOperation();
        
        centralizingFundsOperation.setBank(objFundsOperation.getBank());
        centralizingFundsOperation.setCurrency(objFundsOperation.getCurrency());
        centralizingFundsOperation.setFundsOperationClass(objFundsOperation.getFundsOperationClass());
        centralizingFundsOperation.setFundsOperationGroup(objFundsOperation.getFundsOperationGroup());
        centralizingFundsOperation.setFundsOperationType(objFundsOperation.getFundsOperationType());
        //centralizingFundsOperation.setHolder(objFundsOperation.getHolder());
        centralizingFundsOperation.setIndCentralized(1);
        /** SEARCHING CENTRALIZING INSTITUTION CASH ACCOUNT **/
        centralizingFundsOperation.setInstitutionCashAccount(depositServiceBean.getCashAccountForCentralAccount(objFundsOperation.getCurrency()));
        centralizingFundsOperation.setIssuer(objFundsOperation.getIssuer());
        centralizingFundsOperation.setMechanismOperation(objFundsOperation.getMechanismOperation());
        centralizingFundsOperation.setOperationAmount(objFundsOperation.getOperationAmount());
        centralizingFundsOperation.setOperationPart(objFundsOperation.getOperationPart());
        centralizingFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
        centralizingFundsOperation.setIndAutomatic(BooleanType.NO.getCode());
        centralizingFundsOperation.setRegistryUser(objFundsOperation.getRegistryUser());
        centralizingFundsOperation.setRegistryDate(CommonsUtilities.currentDate());
        centralizingFundsOperation.setParticipant(objFundsOperation.getParticipant());
        return centralizingFundsOperation;
	}
	
	/**
	 * Search funds operation related.
	 *
	 * @param idRelated the id related
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	public FundsOperation searchFundsOperationRelated(Long idRelated) throws ServiceException{
		return this.fundsOperationServiceBean.searchFundsOperationRelated(idRelated);
	}
	
	/**
	 * Search holder account bank lst.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBank> searchHolderAccountBankLst(Long idHolderPk) throws ServiceException{
		List<HolderAccountBank> lstReturned = this.fundsOperationServiceBean.searchHolderAccountBankLst(idHolderPk);
		for (HolderAccountBank holderAccountBank : lstReturned) {
			holderAccountBank.getBank().getIdBankPk();
			holderAccountBank.getBank().getMnemonic();
			holderAccountBank.getBank().getDescription();
		}
		return lstReturned;
	}
	
	/**
	 * Search holder funds operation.
	 *
	 * @param holderFundsOperationTO the holder funds operation to
	 * @return the holder funds operation
	 * @throws ServiceException the service exception
	 */
	public HolderFundsOperation searchHolderFundsOperation(HolderFundsOperationTO holderFundsOperationTO) throws ServiceException{
		HolderFundsOperation holderFundsOperation = this.fundsOperationServiceBean.searchHolderFundsOperation(holderFundsOperationTO);
	
		return holderFundsOperation;
	}
	
	/**
	 * Search holder funds operations lst.
	 *
	 * @param holderFundsOperationTO the holder funds operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderFundsOperation> searchHolderFundsOperationsLst(HolderFundsOperationTO holderFundsOperationTO) throws ServiceException{
		List<HolderFundsOperation> holderFundsOperations = this.fundsOperationServiceBean.searchHolderFundsOperationsLst(holderFundsOperationTO);
		
		for(HolderFundsOperation holderFundsOperation : holderFundsOperations){
			holderFundsOperation.getHolderAccountBank().getIdHolderAccountBankPk();	
			holderFundsOperation.getHolder().getIdHolderPk();
			holderFundsOperation.getHolder().getFullName();
			if(Validations.validateIsNotNull(holderFundsOperation.getRelatedHolderCashMovement())){
				holderFundsOperation.getRelatedHolderCashMovement().getIdHolderCashMovementPk();
				holderFundsOperation.getRelatedHolderCashMovement().getBank().getIdBankPk();
				holderFundsOperation.getRelatedHolderCashMovement().getBank().getDescription();
			}
		}
		return holderFundsOperations;
	}
	
	/**
	 * Creates the retirement holder cash movement.
	 *
	 * @param idBenefitPaymentAllocationPk the id benefit payment allocation pk
	 * @return the holder cash movement
	 * @throws ServiceException the service exception
	 */
	
	public String getIssuerByBenefitPayment(Long idBenefitPaymentAllocationPk) throws ServiceException{
		return this.fundsOperationServiceBean.getIssuerByBenefitPayment(idBenefitPaymentAllocationPk);
	}
	
	/**
	 * Gets the benefit payment file.
	 *
	 * @param idFundsOperation the id funds operation
	 * @return the benefit payment file
	 * @throws ServiceException the service exception
	 */
	public BenefitPaymentFile getBenefitPaymentFile(Long idFundsOperation) throws ServiceException{
		return this.fundsOperationServiceBean.getBenefitPaymentFile(idFundsOperation);
	}
	
	/**
	 * Search bank from bank.
	 *
	 * @param idBank the id bank
	 * @param currency the currency
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String searchBankFromBank(Long idBank, Integer currency) throws ServiceException{
		return this.fundsOperationServiceBean.searchBankFromBank(idBank,currency);
	}
	
	/**
	 * Gets the swift message.
	 *
	 * @param idFundsOperation the id funds operation
	 * @return the swift message
	 * @throws ServiceException the service exception
	 */
	public SwiftMessage getSwiftMessage(Long idFundsOperation) throws ServiceException{
		return this.fundsOperationServiceBean.getSwiftMessage(idFundsOperation);
	}
	
	/**
	 * Gets the BCB bank.
	 *
	 * @param currency the currency
	 * @param bankId the bank id
	 * @return the BCB bank
	 */
	public Long getBCBBank(Integer currency,String bankId) {
		return cashAccountService.getBCBBank(currency,bankId);
	}
	
	/**
	 * Search negotiation modality.
	 *
	 * @param negotiationModalityPk the negotiation modality pk
	 * @param modalityGroupPk the modality group pk
	 * @return the long
	 */
	public Long searchNegotiationModality(Long negotiationModalityPk, Long modalityGroupPk){
		return this.fundsOperationServiceBean.searchNegotiationModality(negotiationModalityPk, modalityGroupPk);
	}
	
	/**
	 * Find issuer facade.
	 *
	 * @param idIssuer the id issuer
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerFacade (String idIssuer)throws ServiceException{
		return cashAccountService.findIssuer(idIssuer);
	}
	
	/**
	 * Confirm retirement.
	 *
	 * @param fundOperation the fund operation
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.FUNDS_RETIREMENT_CONFIRM)
	public void confirmRetirement (FundsOperation fundOperation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		fundsComponentSingleton.confirmRetirement(fundOperation,loggerUser);
	}
	
	/**
	 * Search modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param modalityGroupPk the modality group pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> searchModality(Long mechanismPk, Long modalityGroupPk)throws ServiceException{
		return fundsOperationServiceBean.searchModality(mechanismPk,modalityGroupPk);
	}
	
}
