package com.pradera.funds.fundsoperations.retirement.to;

import java.io.Serializable;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MechanismOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class MechanismOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The id mechanism operation pk. */
	private Long idMechanismOperationPk;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The payment reference. */
	private String paymentReference;
	
	/** The id modality group pk. */
	private Long idModalityGroupPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The id negotiation mechanism pk. */
	private Long idNegotiationMechanismPk;
	
	/** The buyer participant. */
	private Long buyerParticipant;
	
	/** The seller participant. */
	private Long sellerParticipant;
	
	/** The currency. */
	private Integer currency;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the id mechanism operation pk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	
	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the new id mechanism operation pk
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the payment reference.
	 *
	 * @return the payment reference
	 */
	public String getPaymentReference() {
		return paymentReference;
	}
	
	/**
	 * Sets the payment reference.
	 *
	 * @param paymentReference the new payment reference
	 */
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
	
	/**
	 * Gets the buyer participant.
	 *
	 * @return the buyer participant
	 */
	public Long getBuyerParticipant() {
		return buyerParticipant;
	}
	
	/**
	 * Sets the buyer participant.
	 *
	 * @param buyerParticipant the new buyer participant
	 */
	public void setBuyerParticipant(Long buyerParticipant) {
		this.buyerParticipant = buyerParticipant;
	}
	
	/**
	 * Gets the seller participant.
	 *
	 * @return the seller participant
	 */
	public Long getSellerParticipant() {
		return sellerParticipant;
	}
	
	/**
	 * Sets the seller participant.
	 *
	 * @param sellerParticipant the new seller participant
	 */
	public void setSellerParticipant(Long sellerParticipant) {
		this.sellerParticipant = sellerParticipant;
	}
	
	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}
	
	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}
	
	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	
	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the id modality group pk.
	 *
	 * @return the id modality group pk
	 */
	public Long getIdModalityGroupPk() {
		return idModalityGroupPk;
	}
	
	/**
	 * Sets the id modality group pk.
	 *
	 * @param idModalityGroupPk the new id modality group pk
	 */
	public void setIdModalityGroupPk(Long idModalityGroupPk) {
		this.idModalityGroupPk = idModalityGroupPk;
	}
}
