package com.pradera.funds.fundsoperations.deposit.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.funds.fundsoperations.deposit.view.CashAccountDetailDataModel;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementDepositTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementDepositTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst search ope for. */
	private List<ParameterTable> lstSearchOpeFor;
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism> lstNegotiationMechanism;
	
	/** The lst modality group. */
	private List<ModalityGroup> lstModalityGroup;
	
	/** The lst negotiation modality. */
	private List<NegotiationModality> lstNegotiationModality;
	
	/** The inst bank account selected. */
	private InstitutionBankAccount instBankAccountSelected;
	
	/** The settlement operation pk. */
	private Long mechanismSelected, modaGroupSelected, negoModalitySelected, mechanismOperationPk,settlementOperationPk;
	
	/** The real deposit amount. */
	private BigDecimal effectiveAmount, pendientAmount, excedentAmount, depositAmount, realDepositAmount;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The id institution cash account pk. */
	private Long operationNumber, idInstitutionCashAccountPk;
	
	/** The settlement schema type. */
	private Integer currency, idSearchOpeForPkSelected, settlementSchemaType;
	
	/** The reference payment. */
	private String settlementSchema, currencyDescription, referencePayment;
	
	/** The bl cash part. */
	private boolean blSettlementGross, blSettlementNet, blFindOpeForReference, blFindOpeForNumber, blCashPart;
	
	/** The lst operation part. */
	private List<ParameterTable> lstOperationPart;
	
	/** The operation part selected. */
	private Integer operationPartSelected;
	
	/** The error found. */
	private String errorFound;
	
	/** The lst cash account detail. */
	private List<CashAccountDetail> lstCashAccountDetail;
	
	/** The cash account detail. */
	private CashAccountDetail cashAccountDetail; 
	
	/** The lst cash account details. */
	private CashAccountDetailDataModel lstCashAccountDetails;
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}
	
	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}
	
	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public String getSettlementSchema() {
		return settlementSchema;
	}
	
	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the new settlement schema
	 */
	public void setSettlementSchema(String settlementSchema) {
		this.settlementSchema = settlementSchema;
	}
	
	/**
	 * Gets the deposit amount.
	 *
	 * @return the deposit amount
	 */
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}
	
	/**
	 * Sets the deposit amount.
	 *
	 * @param depositAmount the new deposit amount
	 */
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}
	
	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(
			List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
	
	/**
	 * Gets the effective amount.
	 *
	 * @return the effective amount
	 */
	public BigDecimal getEffectiveAmount() {
		return effectiveAmount;
	}
	
	/**
	 * Sets the effective amount.
	 *
	 * @param effectiveAmount the new effective amount
	 */
	public void setEffectiveAmount(BigDecimal effectiveAmount) {
		this.effectiveAmount = effectiveAmount;
	}
	
	/**
	 * Gets the pendient amount.
	 *
	 * @return the pendient amount
	 */
	public BigDecimal getPendientAmount() {
		return pendientAmount;
	}
	
	/**
	 * Sets the pendient amount.
	 *
	 * @param pendientAmount the new pendient amount
	 */
	public void setPendientAmount(BigDecimal pendientAmount) {
		this.pendientAmount = pendientAmount;
	}
	
	/**
	 * Gets the excedent amount.
	 *
	 * @return the excedent amount
	 */
	public BigDecimal getExcedentAmount() {
		return excedentAmount;
	}
	
	/**
	 * Sets the excedent amount.
	 *
	 * @param excedentAmount the new excedent amount
	 */
	public void setExcedentAmount(BigDecimal excedentAmount) {
		this.excedentAmount = excedentAmount;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the reference payment.
	 *
	 * @return the reference payment
	 */
	public String getReferencePayment() {
		return referencePayment;
	}
	
	/**
	 * Sets the reference payment.
	 *
	 * @param referencePayment the new reference payment
	 */
	public void setReferencePayment(String referencePayment) {
		this.referencePayment = referencePayment;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	
	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	
	/**
	 * Checks if is bl settlement gross.
	 *
	 * @return true, if is bl settlement gross
	 */
	public boolean isBlSettlementGross() {
		return blSettlementGross;
	}
	
	/**
	 * Sets the bl settlement gross.
	 *
	 * @param blSettlementGross the new bl settlement gross
	 */
	public void setBlSettlementGross(boolean blSettlementGross) {
		this.blSettlementGross = blSettlementGross;
	}
	
	/**
	 * Checks if is bl settlement net.
	 *
	 * @return true, if is bl settlement net
	 */
	public boolean isBlSettlementNet() {
		return blSettlementNet;
	}
	
	/**
	 * Sets the bl settlement net.
	 *
	 * @param blSettlementNet the new bl settlement net
	 */
	public void setBlSettlementNet(boolean blSettlementNet) {
		this.blSettlementNet = blSettlementNet;
	}
	
	/**
	 * Checks if is bl find ope for reference.
	 *
	 * @return true, if is bl find ope for reference
	 */
	public boolean isBlFindOpeForReference() {
		return blFindOpeForReference;
	}
	
	/**
	 * Sets the bl find ope for reference.
	 *
	 * @param blFindOpeForReference the new bl find ope for reference
	 */
	public void setBlFindOpeForReference(boolean blFindOpeForReference) {
		this.blFindOpeForReference = blFindOpeForReference;
	}
	
	/**
	 * Checks if is bl find ope for number.
	 *
	 * @return true, if is bl find ope for number
	 */
	public boolean isBlFindOpeForNumber() {
		return blFindOpeForNumber;
	}
	
	/**
	 * Sets the bl find ope for number.
	 *
	 * @param blFindOpeForNumber the new bl find ope for number
	 */
	public void setBlFindOpeForNumber(boolean blFindOpeForNumber) {
		this.blFindOpeForNumber = blFindOpeForNumber;
	}
	
	/**
	 * Gets the lst search ope for.
	 *
	 * @return the lst search ope for
	 */
	public List<ParameterTable> getLstSearchOpeFor() {
		return lstSearchOpeFor;
	}
	
	/**
	 * Sets the lst search ope for.
	 *
	 * @param lstSearchOpeFor the new lst search ope for
	 */
	public void setLstSearchOpeFor(List<ParameterTable> lstSearchOpeFor) {
		this.lstSearchOpeFor = lstSearchOpeFor;
	}
	
	/**
	 * Gets the id search ope for pk selected.
	 *
	 * @return the id search ope for pk selected
	 */
	public Integer getIdSearchOpeForPkSelected() {
		return idSearchOpeForPkSelected;
	}
	
	/**
	 * Sets the id search ope for pk selected.
	 *
	 * @param idSearchOpeForPkSelected the new id search ope for pk selected
	 */
	public void setIdSearchOpeForPkSelected(Integer idSearchOpeForPkSelected) {
		this.idSearchOpeForPkSelected = idSearchOpeForPkSelected;
	}
	
	/**
	 * Gets the inst bank account selected.
	 *
	 * @return the inst bank account selected
	 */
	public InstitutionBankAccount getInstBankAccountSelected() {
		return instBankAccountSelected;
	}
	
	/**
	 * Sets the inst bank account selected.
	 *
	 * @param instBankAccountSelected the new inst bank account selected
	 */
	public void setInstBankAccountSelected(
			InstitutionBankAccount instBankAccountSelected) {
		this.instBankAccountSelected = instBankAccountSelected;
	}
	
	/**
	 * Gets the id institution cash account pk.
	 *
	 * @return the id institution cash account pk
	 */
	public Long getIdInstitutionCashAccountPk() {
		return idInstitutionCashAccountPk;
	}
	
	/**
	 * Sets the id institution cash account pk.
	 *
	 * @param idInstitutionCashAccountPk the new id institution cash account pk
	 */
	public void setIdInstitutionCashAccountPk(Long idInstitutionCashAccountPk) {
		this.idInstitutionCashAccountPk = idInstitutionCashAccountPk;
	}
	
	/**
	 * Gets the settlement schema type.
	 *
	 * @return the settlement schema type
	 */
	public Integer getSettlementSchemaType() {
		return settlementSchemaType;
	}
	
	/**
	 * Sets the settlement schema type.
	 *
	 * @param settlementSchemaType the new settlement schema type
	 */
	public void setSettlementSchemaType(Integer settlementSchemaType) {
		this.settlementSchemaType = settlementSchemaType;
	}
	
	/**
	 * Gets the lst operation part.
	 *
	 * @return the lst operation part
	 */
	public List<ParameterTable> getLstOperationPart() {
		return lstOperationPart;
	}
	
	/**
	 * Sets the lst operation part.
	 *
	 * @param lstOperationPart the new lst operation part
	 */
	public void setLstOperationPart(List<ParameterTable> lstOperationPart) {
		this.lstOperationPart = lstOperationPart;
	}
	
	/**
	 * Gets the operation part selected.
	 *
	 * @return the operation part selected
	 */
	public Integer getOperationPartSelected() {
		return operationPartSelected;
	}
	
	/**
	 * Sets the operation part selected.
	 *
	 * @param operationPartSelected the new operation part selected
	 */
	public void setOperationPartSelected(Integer operationPartSelected) {
		this.operationPartSelected = operationPartSelected;
	}
	
	/**
	 * Checks if is bl cash part.
	 *
	 * @return true, if is bl cash part
	 */
	public boolean isBlCashPart() {
		return blCashPart;
	}
	
	/**
	 * Sets the bl cash part.
	 *
	 * @param blCashPart the new bl cash part
	 */
	public void setBlCashPart(boolean blCashPart) {
		this.blCashPart = blCashPart;
	}
	
	/**
	 * Gets the error found.
	 *
	 * @return the error found
	 */
	public String getErrorFound() {
		return errorFound;
	}
	
	/**
	 * Sets the error found.
	 *
	 * @param errorFound the new error found
	 */
	public void setErrorFound(String errorFound) {
		this.errorFound = errorFound;
	}
	
	/**
	 * Gets the real deposit amount.
	 *
	 * @return the real deposit amount
	 */
	public BigDecimal getRealDepositAmount() {
		return realDepositAmount;
	}
	
	/**
	 * Sets the real deposit amount.
	 *
	 * @param realDepositAmount the new real deposit amount
	 */
	public void setRealDepositAmount(BigDecimal realDepositAmount) {
		this.realDepositAmount = realDepositAmount;
	}
	
	/**
	 * Gets the mechanism selected.
	 *
	 * @return the mechanism selected
	 */
	public Long getMechanismSelected() {
		return mechanismSelected;
	}
	
	/**
	 * Sets the mechanism selected.
	 *
	 * @param mechanismSelected the new mechanism selected
	 */
	public void setMechanismSelected(Long mechanismSelected) {
		this.mechanismSelected = mechanismSelected;
	}
	
	/**
	 * Gets the moda group selected.
	 *
	 * @return the moda group selected
	 */
	public Long getModaGroupSelected() {
		return modaGroupSelected;
	}
	
	/**
	 * Sets the moda group selected.
	 *
	 * @param modaGroupSelected the new moda group selected
	 */
	public void setModaGroupSelected(Long modaGroupSelected) {
		this.modaGroupSelected = modaGroupSelected;
	}
	
	/**
	 * Gets the mechanism operation pk.
	 *
	 * @return the mechanism operation pk
	 */
	public Long getMechanismOperationPk() {
		return mechanismOperationPk;
	}
	
	/**
	 * Sets the mechanism operation pk.
	 *
	 * @param mechanismOperationPk the new mechanism operation pk
	 */
	public void setMechanismOperationPk(Long mechanismOperationPk) {
		this.mechanismOperationPk = mechanismOperationPk;
	}
	
	/**
	 * Gets the nego modality selected.
	 *
	 * @return the nego modality selected
	 */
	public Long getNegoModalitySelected() {
		return negoModalitySelected;
	}
	
	/**
	 * Sets the nego modality selected.
	 *
	 * @param negoModalitySelected the new nego modality selected
	 */
	public void setNegoModalitySelected(Long negoModalitySelected) {
		this.negoModalitySelected = negoModalitySelected;
	}
	
	/**
	 * Gets the lst cash account detail.
	 *
	 * @return the lst cash account detail
	 */
	public List<CashAccountDetail> getLstCashAccountDetail() {
		return lstCashAccountDetail;
	}
	
	/**
	 * Sets the lst cash account detail.
	 *
	 * @param lstCashAccountDetail the new lst cash account detail
	 */
	public void setLstCashAccountDetail(List<CashAccountDetail> lstCashAccountDetail) {
		this.lstCashAccountDetail = lstCashAccountDetail;
	}
	
	/**
	 * Gets the cash account detail.
	 *
	 * @return the cash account detail
	 */
	public CashAccountDetail getCashAccountDetail() {
		return cashAccountDetail;
	}
	
	/**
	 * Sets the cash account detail.
	 *
	 * @param cashAccountDetail the new cash account detail
	 */
	public void setCashAccountDetail(CashAccountDetail cashAccountDetail) {
		this.cashAccountDetail = cashAccountDetail;
	}
	
	/**
	 * Gets the lst cash account details.
	 *
	 * @return the lst cash account details
	 */
	public CashAccountDetailDataModel getLstCashAccountDetails() {
		return lstCashAccountDetails;
	}
	
	/**
	 * Sets the lst cash account details.
	 *
	 * @param lstCashAccountDetails the new lst cash account details
	 */
	public void setLstCashAccountDetails(
			CashAccountDetailDataModel lstCashAccountDetails) {
		this.lstCashAccountDetails = lstCashAccountDetails;
	}
	
	/**
	 * Gets the settlement operation pk.
	 *
	 * @return the settlement operation pk
	 */
	public Long getSettlementOperationPk() {
		return settlementOperationPk;
	}
	
	/**
	 * Sets the settlement operation pk.
	 *
	 * @param settlementOperationPk the new settlement operation pk
	 */
	public void setSettlementOperationPk(Long settlementOperationPk) {
		this.settlementOperationPk = settlementOperationPk;
	}
}
