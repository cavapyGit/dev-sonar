package com.pradera.funds.fundsoperations.deposit.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.funds.CashAccountDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CashAccountDetailDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class CashAccountDetailDataModel  extends ListDataModel<CashAccountDetail> implements SelectableDataModel<CashAccountDetail>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new cash account detail data model.
	 */
	public CashAccountDetailDataModel() {}
	
	/**
	 * Instantiates a new cash account detail data model.
	 *
	 * @param lstCashAccountDetailDataModel the lst cash account detail data model
	 */
	public CashAccountDetailDataModel(List<CashAccountDetail> lstCashAccountDetailDataModel) {
		super(lstCashAccountDetailDataModel);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public CashAccountDetail getRowData(String rowKey) {
		List<CashAccountDetail> lstCashAccountDetailDataModel = (List<CashAccountDetail>)getWrappedData();
		for(CashAccountDetail cashAccountDetail : lstCashAccountDetailDataModel){
			if(cashAccountDetail.getIdCashAccountDetail().toString().equals(rowKey))
				return cashAccountDetail;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	public Object getRowKey(CashAccountDetail cashAccountDetail) {
		return cashAccountDetail.getIdCashAccountDetail();
	}
	
}