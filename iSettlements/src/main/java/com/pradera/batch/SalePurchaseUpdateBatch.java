package com.pradera.batch;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.AccountOperationTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SalePurchaseUpdateBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="SalePurchaseUpdateBatch")
public class SalePurchaseUpdateBatch implements JobExecution{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(SalePurchaseUpdateBatch.class);
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The account assignment facade. */
	@EJB
	private AccountAssignmentFacade accountAssignmentFacade;
	
	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The Constant CONCURRNECY_BATCH_ERROR. */
	private static final String CONCURRNECY_BATCH_ERROR= "EXISTE UN PROCESO EJECUTANDOSE CONCURRENTEMENTE";
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		logger.debug("SalePurchaseUpdateBatch: Starting the process ...");
		
		boolean isExecutingProcess = batchProcessServiceFacade.isExecutingBatchProcess(processLogger.getBusinessProcess().getIdBusinessProcessPk(), 
																						processLogger.getIdProcessLoggerPk());
		
		if (!isExecutingProcess) {
			Long idMechanism= null;
			Long idModality= null;
			Integer idRole= null;
			Integer settlementSchema= SettlementSchemaType.GROSS.getCode();
			Date blockDate= CommonsUtilities.currentDate();
			List<AccountOperationTO> lstAccountOperations= null;
			List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
			for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
			{
				if ((Validations.validateIsNotNull(objDetail.getParameterName())))
				{
					if (StringUtils.equalsIgnoreCase(ComponentConstant.MECHANISM, objDetail.getParameterName())) {
						idMechanism= new Long(objDetail.getParameterValue());
					} else if (StringUtils.equalsIgnoreCase(ComponentConstant.MODALITY, objDetail.getParameterName())) {
						idModality= new Long(objDetail.getParameterValue());
					} else if (StringUtils.equalsIgnoreCase(ComponentConstant.SETTLEMENT_SCHEMA, objDetail.getParameterName())) {
						settlementSchema= new Integer(objDetail.getParameterValue());
					}
				}
			}
			
			//To update the balances to PURCHASE position
			idRole= ComponentConstant.PURCHARSE_ROLE;
			updateSalePurchaseBalances(lstAccountOperations, processLogger.getBusinessProcess().getIdBusinessProcessPk(), idMechanism,  idModality, 
										idRole, blockDate, settlementSchema);
			
			//To update the balances to SALE position
			idRole= ComponentConstant.SALE_ROLE;
			updateSalePurchaseBalances(lstAccountOperations, processLogger.getBusinessProcess().getIdBusinessProcessPk(), idMechanism, idModality, 
										idRole, blockDate, settlementSchema);
			
			if (SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
				//we send the chaining process automatic
				BusinessProcess chainedBusinessProcess= new BusinessProcess();
				chainedBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GENERATE_CHAIN_OPERATION.getCode());
				try {
					batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), chainedBusinessProcess, null);
				} catch (ServiceException e) {
					e.printStackTrace();
				}
												
				//we send the reports	
				try {
					// Enviar reporte siempre y cuando el usuario sea DEPOSITARY
					UserFilterTO userFilter = new UserFilterTO();
					userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());
					userFilter.setUserName(processLogger.getIdUserAccount());
					List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);	
					
					if(Validations.validateIsNotNullAndNotEmpty(lstUserAccount)){
						
						Thread.sleep(20000); // 20 seconds waiting for send reports
						sendReportOperDetailWithSecPendingDeliv();
					}										
				} catch (InterruptedException e) {			
					e.printStackTrace();
				}
			}
		} else {
			processLogger.setErrorDetail(CONCURRNECY_BATCH_ERROR);
		}
		
		logger.debug("SalePurchaseUpdateBatch: Finishing the process ...");
	}
	
	
	/**
	 * Update sale purchase balances.
	 *
	 * @param lstAccountOperations the lst account operations
	 * @param idBusinessProcess the id business process
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idRole the id role
	 * @param blockDate the block date
	 * @param settlementSchema the settlement schema
	 */
	public void updateSalePurchaseBalances(List lstAccountOperations, Long idBusinessProcess, Long idMechanism, Long idModality, 
			Integer idRole, Date blockDate, Integer settlementSchema)
	{	
		try {
			if (ComponentConstant.SALE_ROLE.equals(idRole) && SettlementSchemaType.NET.getCode().equals(settlementSchema)) {
				//synchronize the assigned market facts about DPF operations
				accountAssignmentFacade.synchronizeSettlementAccountMarketfact(null);
			}
			/*
			 * STEP 01:
			 * To get the list of holder accounts to block the for each role
			 * The list of account operation could come as parameter, in that case is not required to get the accounts operations for all modalities 
			 */
			if (Validations.validateIsNull(lstAccountOperations))
			{
				//holder accounts for cash part and term part
				lstAccountOperations= settlementProcessService.getListSettlementAccountOperationsToBlockBalances(idMechanism, idModality, idRole, 
																						blockDate, settlementSchema);
					
				//we have to get the holder accounts from swap operations
//				List lstAccountSwapOperations = settlementProcessService.getListAccountSwapOperationsToBlockBalances(idMechanism, idModality, idRole, blockDate);
//				if (Validations.validateListIsNotNullAndNotEmpty(lstAccountSwapOperations)){
//					lstAccountOperations.addAll(lstAccountSwapOperations);
//				}
					
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * STEP 02:
		 * To call the component to generates the movements and updates the balances for each holder account operation 
		 */
		if (Validations.validateListIsNotNullAndNotEmpty(lstAccountOperations))
		{
			for (int i=0; i<lstAccountOperations.size(); ++i)
			{
				AccountOperationTO objAccountOperationTO = null;
				/* when the list of account operations come as parameter we have the AccountOperatioTO entity inside the list
				 * and when we get the list from DB we get Object[] inside the list. It's comfortable works with AccountOperatioTO entity */
				if (lstAccountOperations.get(i) instanceof Object[]) {
					objAccountOperationTO = populateAccountOperationTO((Object[]) lstAccountOperations.get(i));
				} else {
					objAccountOperationTO = (AccountOperationTO) lstAccountOperations.get(i);
				}
				//we have to update the balance to this holder account operation
				settlementProcessFacade.blockSalePurchaseBalancesTx(objAccountOperationTO, idBusinessProcess,idepositarySetup.getIndMarketFact());
			}
		}
	}

	
	/**
	 * Populate account operation to.
	 *
	 * @param arrObject the arr object
	 * @return the account operation to
	 */
	private AccountOperationTO populateAccountOperationTO(Object[] arrObject)
	{
		AccountOperationTO objAccountOperationTO= new AccountOperationTO();
		objAccountOperationTO.setIdMechanismOperation((Long)arrObject[0]);
		objAccountOperationTO.setIdHolderAccountOperation((Long)arrObject[1]);
		objAccountOperationTO.setIdMechanism((Long)arrObject[2]);
		objAccountOperationTO.setIdModality((Long)arrObject[3]);
		objAccountOperationTO.setIdParticipant((Long)arrObject[4]);
		objAccountOperationTO.setIdHolderAccount((Long)arrObject[5]);
		objAccountOperationTO.setIdRole((Integer)arrObject[6]);
		objAccountOperationTO.setOperationPart((Integer)arrObject[7]);
		objAccountOperationTO.setTotalStockQuantity((BigDecimal)arrObject[8]);
		objAccountOperationTO.setIdSecurityCode((String)arrObject[9]);
		if (Validations.validateIsNotNullAndPositive((Integer)arrObject[10])) {
			objAccountOperationTO.setIdSwapOperation((Long)arrObject[10]);
		}
		if (((BigDecimal)arrObject[12]).compareTo(BigDecimal.ZERO) > 0) {
			objAccountOperationTO.setIndInchain(BooleanType.YES.getCode());
		} else {
			objAccountOperationTO.setIndInchain(BooleanType.NO.getCode());
		}
		objAccountOperationTO.setChainedQuantity((BigDecimal)arrObject[12]);
		objAccountOperationTO.setStockQuantity((BigDecimal)arrObject[13]);
		if (ComponentConstant.SALE_ROLE.equals(objAccountOperationTO.getIdRole())) {
			//the normal quantity only
			objAccountOperationTO.setBlockQuantity(objAccountOperationTO.getStockQuantity().subtract(objAccountOperationTO.getChainedQuantity()));
		} else if (ComponentConstant.PURCHARSE_ROLE.equals(objAccountOperationTO.getIdRole())) {
			objAccountOperationTO.setBlockQuantity(objAccountOperationTO.getStockQuantity());
		}
		objAccountOperationTO.setIdSettlementAccountOperation((Long)arrObject[14]);
		objAccountOperationTO.setIdSettlementOperation((Long)arrObject[15]);
		objAccountOperationTO.setCashPrice((BigDecimal)arrObject[17]);
		objAccountOperationTO.setInstrumentType((Integer)arrObject[18]);
		objAccountOperationTO.setIndReportingBalance((Integer) arrObject[19]);
		
		return objAccountOperationTO;
	}
	
	/**
	 * Send report operation detail with securities pending delivery.
	 */
	public void sendReportOperDetailWithSecPendingDeliv(){
		try {
			logger.info("ENVIAMOS REPORTE : OPERACIONES PENDIENTES DE ENTREGA DE VALORES .........");	
			List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			SettlementReportTO filter = new SettlementReportTO(); 						
			filter.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			lstSettlementReportTO = settlementReportFacade.operDetailWithSecPendingDelivQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				logger.info("SEND :::: REPORTE DETALLE DE OPERACIONES PENDIENTES DE ENTREGA DE VALORES CONINCONSISTENCIA ");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.OPERATIONS_DETAIL_WITH_PENDING_DELIV.getCode());
			} else{
				logger.info("SEND :::: REPORTE DETALLE DE OPERACIONES PENDIENTES DE ENTREGA DE VALORES SININCONSISTENCIA ");
				lstSettlementReportTO = new ArrayList<SettlementReportTO>();				
				SettlementReportTO settlementReportTO = new SettlementReportTO();
				settlementReportTO.setOnlyUser(Boolean.TRUE);
				settlementReportTO.setSchema(SettlementSchemaType.NET.getCode().toString());
				settlementReportTO.setIndExtended(GeneralConstants.ZERO_VALUE_STRING);
				settlementReportTO.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));				
				lstSettlementReportTO.add(settlementReportTO);
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.OPERATIONS_DETAIL_WITH_PENDING_DELIV.getCode());
			}
			
		} catch(ServiceException ex){
			logger.error(":::::::::::::::::::::: ERROR AL ENVIAR EL REPORTE: PERACIONES PENDIENTES DE ENTREGA DE VALORES ::::::::::::::::::::::::");
			ex.printStackTrace();
		}				
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
