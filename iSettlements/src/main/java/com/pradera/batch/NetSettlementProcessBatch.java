package com.pradera.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;

// TODO: Auto-generated Javadoc
/**
 * Descripcion: Proceso batch que ejecuta la liquidacion neta en cualquier etapa de liquidacion.
 *
 * @author PRADERA TECHNOLOGIES
 */
@BatchProcess(name="NetSettlementProcessBatch")
public class NetSettlementProcessBatch implements JobExecution {

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(GrossSettlementProcessBatch.class);
	
	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
    @Inject 
    ClientRestService clientRestService;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		
		Long idMechanism= null;
		Long idModalityGroup= null;
		Integer idCurrency= null;
		Long idSettlementProcess= null;
		Integer scheduleType = null;
		Date settlementDate= CommonsUtilities.currentDate();
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
		{
			if ((Validations.validateIsNotNull(objDetail.getParameterName())))
			{
				if (StringUtils.equalsIgnoreCase(SettlementConstant.MECHANISM_PARAMETER, objDetail.getParameterName())) {
					idMechanism= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.MODALITY_GROUP_PARAMETER, objDetail.getParameterName())) {
					idModalityGroup= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.CURRENCY_PARAMETER, objDetail.getParameterName())) {
					idCurrency= new Integer(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, objDetail.getParameterName())) {
					idSettlementProcess= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.SCHEDULE_TYPE, objDetail.getParameterName())) {
					scheduleType= new Integer(objDetail.getParameterValue());
				}
			}
		}
		
		try {
			boolean blError = settlementProcessFacade.executeNetSettlementProcess(idMechanism, idModalityGroup, idCurrency, settlementDate, idSettlementProcess, scheduleType);
			
			// Send Notification
			if(!blError) {
				sendNotificationProcess(processLogger, idCurrency, CommonsUtilities.convertDatetoString(settlementDate), scheduleType);
			}						
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//we send the reports	
		try {
			Thread.sleep(20000); // 20 seconds waiting for send reports
			sendReports(idCurrency.toString(), CommonsUtilities.convertDatetoString(settlementDate), scheduleType.toString());
			
			if(scheduleType.equals(SettlementScheduleType.MELOR.getCode())) {
				registerUnblockBalanceSelidBatch(processLogger);
			}else {
				// Issue 1343 Calculo de precio curva
				if (!settlementProcessFacade.verifyOperationExist(CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()))) {
					registerUnblockBalanceSelidBatch(processLogger);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
	}

	/**
	 * Metodo para registrar el batch de reversion de bloqueo de saldos en operaciones SELID. issue 1040
	 */
	@LoggerAuditWeb
	private void registerUnblockBalanceSelidBatch(ProcessLogger processLogger){
		
		Schedulebatch scheduleBatch = new Schedulebatch();
        scheduleBatch.setIdBusinessProcess(Long.valueOf(40866));
        scheduleBatch.setUserName(processLogger.getIdUserAccount());
        scheduleBatch.setPrivilegeCode(processLogger.getLastModifyApp());
        scheduleBatch.setParameters(new HashMap<String, String>());
    	clientRestService.excuteRemoteBatchProcess(scheduleBatch,"iSecurities");

	}
	
	/**
	 * Send reports.
	 *
	 * @param currency the currency
	 * @param settlementDate the settlement date
	 * @param scheduleType the schedule type
	 */
	public void sendReports(String currency, String settlementDate, String scheduleType){
		try {
			logger.info("ENVIAMOS LOS REPORTES NetSettlementProcessBatch .........");
								
			SettlementReportTO filter = new SettlementReportTO();						
			List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();															
			String parScheduleType = null;
			if(Validations.validateIsNotNullAndNotEmpty(scheduleType)){				
				if(SettlementScheduleType.FIRST_SETTLEMENT.getCode().equals(new Integer(scheduleType)) || 
						SettlementScheduleType.SECOND_SETTLEMENT.getCode().equals(new Integer(scheduleType)) ){					
					if(SettlementScheduleType.FIRST_SETTLEMENT.getCode().equals(new Integer(scheduleType))){
						parScheduleType = SettlementScheduleType.SECOND_SETTLEMENT.getCode().toString();
					} else if(SettlementScheduleType.SECOND_SETTLEMENT.getCode().equals(new Integer(scheduleType))) {
						parScheduleType = SettlementScheduleType.MELOR.getCode().toString();
					}
					if(Validations.validateIsNotNullAndNotEmpty(parScheduleType)){
						filter.setCurrency(currency);
						filter.setDate(settlementDate);
						filter.setScheduleType(parScheduleType);					
						lstSettlementReportTO = settlementReportFacade.netSettlementPositionsPreQuery(filter);
						if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
							logger.info("SEND :::: REPORTE DE RESUMEN DE POSICIONES NETAS (PRELIMINAR) ");
							settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.NET_SETTLEMENT_POSITIONS_PRE.getCode());
						}
					}										
				}								 
			}
			
			filter = new SettlementReportTO();
			filter.setCurrency(currency);
			filter.setDate(settlementDate);
			filter.setScheduleType(scheduleType);
			
			lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			lstSettlementReportTO = settlementReportFacade.netSettlementPositionsSettlementQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){	
				logger.info("SEND :::: POSICIONES NETAS - LIQUIDADAS ");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.NET_POSITIONS_SETTLED.getCode());
			}
			
			lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			lstSettlementReportTO = settlementReportFacade.netSettlementPositionsRetirementQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				logger.info("SEND :::: POSICIONES NETAS - RETIRADAS ");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.NET_POSITIONS_WITHDRAWALS.getCode());
			}			
		} catch(ServiceException ex){
			logger.error("ERROR AL ENVIAR LOS REPORTES........");
			ex.printStackTrace();
		}
	}
	
	/**
	 * Send notification process.
	 *
	 * @param processLogger the process logger
	 * @param currency the currency
	 * @param settlementDate the settlement date
	 * @param scheduleType the schedule type
	 */
	public void sendNotificationProcess(ProcessLogger processLogger, Integer currency, String settlementDate, Integer scheduleType) {
		if(Validations.validateIsNotNullAndNotEmpty(currency) && Validations.validateIsNotNullAndNotEmpty(scheduleType)) {
			String currencyDescription = CurrencyType.get(new Integer(currency)).getValue();
			String scheludeDescription = SettlementScheduleType.lookup.get(scheduleType).getDescription();
			notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), 
					processLogger.getBusinessProcess(), null, new Object[] {settlementDate, currencyDescription, scheludeDescription});
		}		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
}