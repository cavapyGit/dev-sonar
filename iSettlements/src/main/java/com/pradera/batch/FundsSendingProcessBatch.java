package com.pradera.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * Descripcion: Proceso batch que envia los fondos via servicio web al LIP para el pago a los acreedores del proceso de liquidacion.
 *
 * @author PRADERA TECHNOLOGIES
 */
@BatchProcess(name="FundsSendingProcessBatch")
public class FundsSendingProcessBatch implements JobExecution{

	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info(":::::::: INICIO PROCESO BATCH FundsSendingProcessBatch: ENVIO DE FONDOS AL LIP ::::::::");
		Long idMechanism= null;
		Long idModalityGroup= null;
		Integer currency= null;
		Long idSettlementProcess= null;
		Long idParticipant= null;
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
		{
			if ((Validations.validateIsNotNull(objDetail.getParameterName())))
			{
				if (StringUtils.equalsIgnoreCase(SettlementConstant.MECHANISM_PARAMETER, objDetail.getParameterName())) {
					idMechanism= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.MODALITY_GROUP_PARAMETER, objDetail.getParameterName())) {
					idModalityGroup= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.CURRENCY_PARAMETER, objDetail.getParameterName())) {
					currency= new Integer(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, objDetail.getParameterName())) {
					idSettlementProcess= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.ID_PARTICIPANT, objDetail.getParameterName())) {
					idParticipant= new Long(objDetail.getParameterValue());
				}
			}
		}
		
		log.info(":::::::: VARIABLES OBTENIDAS FundsSendingProcessBatch ::::::::");
		log.info(":::::::: idMechanism ::::::::" + idMechanism);
		log.info(":::::::: idModalityGroup ::::::::" + idModalityGroup);
		log.info(":::::::: currency ::::::::" + currency);
		log.info(":::::::: idSettlementProcess ::::::::" + idSettlementProcess);
		log.info(":::::::: idParticipant ::::::::" + idParticipant);
		
		try {
			settlementProcessFacade.sendFundsParticipantPositionsTx(idSettlementProcess, idParticipant, idMechanism, idModalityGroup, currency);
		} catch (ServiceException e) {
			log.error("ERROR AL ENVIAR FONDOS AL LIP........");
			e.printStackTrace();
		}
		
		//we send the settlement information by web service
		Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, idSettlementProcess);
		BusinessProcess objBusinessProcess=new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.SEND_SETTLEMENT_OPERATION_WEBSERVICE.getCode());
		try {
			batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), objBusinessProcess, processParameters);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
