package com.pradera.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.core.facade.SettlementProcessFacade;

@BatchProcess(name="CalculateIssuerAmountBatch")
public class CalculateIssuerAmountBatch implements Serializable, JobExecution{

	private static final long serialVersionUID = 1L;

	
	/** The corporate process facade. */
	@EJB 
	private SettlementProcessFacade settlementProcessFacade;
	
	private static int PROCESS_DAYS= 5; 
	
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		Date expirationDate = CommonsUtilities.addDate(CommonsUtilities.currentDate(), PROCESS_DAYS);
		try {
			settlementProcessFacade.calculateIssuerAmountAutomatic(expirationDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
