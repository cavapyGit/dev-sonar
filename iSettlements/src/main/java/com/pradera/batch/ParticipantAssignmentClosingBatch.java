package com.pradera.batch;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.process.ProcessLogger;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;

// TODO: Auto-generated Javadoc
/**
 * Descripcion: .
 *
 * @author PRADERA TECHNOLOGIES
 */
@BatchProcess(name="ParticipantAssignmentClosingBatch")
public class ParticipantAssignmentClosingBatch implements JobExecution {

	
	/** The account assignment facade. */
	@EJB
	private AccountAssignmentFacade accountAssignmentFacade;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		Date processDate= CommonsUtilities.currentDate();
		accountAssignmentFacade.closeParticipantAssignmentProcess(processDate);		
		
		// Send notifications
		sendNotificationProcess(processLogger, processDate);		
	}
	
	/**
	 * Send notification process.
	 *
	 * @param processLogger the process logger
	 * @param processDate the process date
	 */
	public void sendNotificationProcess(ProcessLogger processLogger, Date processDate) {
		String strLstMnemonic = GeneralConstants.EMPTY_STRING;
		List<String> lstMnemonicParticipant = accountAssignmentFacade.getListMnemonicParticipantAssignment(processDate);
		if(Validations.validateListIsNotNullAndNotEmpty(lstMnemonicParticipant)) {
			for(String strMnemonic : lstMnemonicParticipant) {
				if(GeneralConstants.EMPTY_STRING.compareTo(strLstMnemonic) == 0){
					strLstMnemonic = strMnemonic;
				} else {
					strLstMnemonic = strLstMnemonic + GeneralConstants.STR_COMMA_WITHOUT_SPACE + strMnemonic;
				}
			}
			notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), null, new Object[] {strLstMnemonic});
		}					
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
