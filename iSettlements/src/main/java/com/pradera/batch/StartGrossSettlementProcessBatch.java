package com.pradera.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.core.service.SettlementProcessService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class StartGrossSettlementProcessBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="StartGrossSettlementProcessBatch")
public class StartGrossSettlementProcessBatch implements JobExecution{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(StartGrossSettlementProcessBatch.class);
	
	/** The settlement process facade. */
	@EJB
 	private SettlementProcessFacade settlementProcessFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		logger.debug("StartGrossSettlementProcessBatch: Starting the process ...");
		
		Date settlementDate= CommonsUtilities.currentDate();
		
		//we remove all operations which have blocked entities (Issuer, Issuance, Securities, Participant, Holder, Holder Account)  
		try {	
			settlementProcessFacade.startGrosSettlementProcess(settlementDate, processLogger.getIdUserAccount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.debug("StartGrossSettlementProcessBatch: Finishing the process ...");
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
