package com.pradera.batch;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.settlements.chainedoperation.facade.ChainedOperationsFacade;

// TODO: Auto-generated Javadoc
/**
 * Descripcion: Proceso batch que realiza el encadenamiento automatico de las operaciones a liquidar .
 *
 * @author PRADERA TECHNOLOGIES
 */
@BatchProcess(name="GeneratorChainOperationBatch")
public class GeneratorChainOperationBatch implements JobExecution{

	/** The chained operations facade. */
	@EJB
	private ChainedOperationsFacade chainedOperationsFacade;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) 
	{
		try {
			Date settlementDate= CommonsUtilities.currentDate();
			Integer instrumentType= InstrumentType.FIXED_INCOME.getCode();
			Long idParticipant= null;
			List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
			for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
			{
				if ((Validations.validateIsNotNull(objDetail.getParameterName())))
				{
					if (StringUtils.equalsIgnoreCase(SettlementConstant.ID_PARTICIPANT, objDetail.getParameterName())) {
						idParticipant= new Long(objDetail.getParameterValue());
					}
				}
			}
			Integer indExtended= BooleanType.YES.getCode();
			chainedOperationsFacade.createChainedHolderOperationsAutomatic(settlementDate, idParticipant, indExtended, instrumentType);
			indExtended= BooleanType.NO.getCode();
			chainedOperationsFacade.createChainedHolderOperationsAutomatic(settlementDate, idParticipant, indExtended, instrumentType);
			
			// Send notifications
			if(Validations.validateIsNotNullAndNotEmpty(idParticipant)) {
				sendNotificationProcess(processLogger, idParticipant);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Send notification process.
	 *
	 * @param processLogger the process logger
	 * @param idParticipant the id participant
	 */
	public void sendNotificationProcess(ProcessLogger processLogger, Long idParticipant) {
		Participant objParticipant = chainedOperationsFacade.getParticipant(idParticipant);
		if(objParticipant != null) {
			notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), 
					processLogger.getBusinessProcess(), null, new Object[] {objParticipant.getMnemonic()});
		}		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
