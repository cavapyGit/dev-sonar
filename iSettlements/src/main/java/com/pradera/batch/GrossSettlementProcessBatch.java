package com.pradera.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;

// TODO: Auto-generated Javadoc
/**
 * Descripcion: Proceso batch para la ejecucion del proceso de liquidacion bruta.
 *
 * @author PRADERA TECHNOLOGIES
 */
@BatchProcess(name="GrossSettlementProcessBatch")
public class GrossSettlementProcessBatch implements JobExecution {

	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The send notification. */
	private Boolean sendNotification = Boolean.FALSE;
	
	/** The operation cash part. */
	private List<Long> operationCashPart = new ArrayList<Long>();
	
	/** The operation term part. */
	private List<Long> operationTermPart = new ArrayList<Long>();
	
	/** The currency. */
	private CurrencyType currency;
	
	/** The mechanism type. */
	private NegotiationMechanismType mechanismType;
	
	/** The modality group. */
	private ModalityGroup modalityGroup;
	
	/** The parameter to send. */
	private Object[] parameterToSend;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		Long idMechanism= null;
		Long idModalityGroup= null;
		Integer idCurrency= null;
		Long idSettlementProcess= null;
		Integer scheduleType = null;
		boolean closeSettlementProcess= false; //flag to know if the closing process was executed
		
		Date settlementDate= CommonsUtilities.currentDate();
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
		{
			if ((Validations.validateIsNotNull(objDetail.getParameterName())))
			{
				if (StringUtils.equalsIgnoreCase(SettlementConstant.MECHANISM_PARAMETER, objDetail.getParameterName())) {
					idMechanism= new Long(objDetail.getParameterValue());
					mechanismType = NegotiationMechanismType.get(idMechanism);
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.MODALITY_GROUP_PARAMETER, objDetail.getParameterName())) {
					idModalityGroup= new Long(objDetail.getParameterValue());
					modalityGroup = new ModalityGroup(idModalityGroup);
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.CURRENCY_PARAMETER, objDetail.getParameterName())) {
					idCurrency= new Integer(objDetail.getParameterValue());
					currency = CurrencyType.get(idCurrency);
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.SCHEDULE_TYPE, objDetail.getParameterName())) {
					scheduleType= new Integer(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, objDetail.getParameterName())) {
					idSettlementProcess= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.CLOSSING_SETTLEMENT_PROCESS, objDetail.getParameterName())) {
					closeSettlementProcess= true;
				}
			}
		}
		
		try {
			settlementProcessFacade.executeGrossSettlementProcess(idMechanism, idModalityGroup, idCurrency, settlementDate, idSettlementProcess, closeSettlementProcess, scheduleType, null);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//we send the reports	
		try {
			Thread.sleep(20000); // 20 seconds waiting for send reports
			sendReportsGrossSettlement(settlementDate);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Send reports gross settlement.
	 *
	 * @param settlementDate the settlement date
	 */
	public void sendReportsGrossSettlement(Date settlementDate){
		try {
			SettlementReportTO filter = new SettlementReportTO();
			filter.setInitialDate(CommonsUtilities.convertDatetoString(settlementDate));
			filter.setEndDate(CommonsUtilities.convertDatetoString(settlementDate));
			List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.purchaseForceExpirationSettledQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.PURCHASE_FORCE_EXPIRATION_SETTLED.getCode());
				
				lstSettlementReportTO = new ArrayList<SettlementReportTO>();
				SettlementReportTO objSettlementReportTO = new SettlementReportTO();
				objSettlementReportTO.setOnlyUser(Boolean.TRUE);
				objSettlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(settlementDate));
				objSettlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(settlementDate));
				lstSettlementReportTO.add(objSettlementReportTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
					settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.PURCHASE_FORCE_EXPIRATION_SETTLED.getCode());
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}

	/**
	 * Send notifications.
	 *
	 * @throws ServiceException the service exception
	 */
	private void sendNotifications() throws ServiceException{
		if(!operationCashPart.isEmpty() || !operationTermPart.isEmpty()){
			sendNotification = Boolean.TRUE;
		}
		

		StringBuilder operations = new StringBuilder();
		if(sendNotification){
			modalityGroup = settlementProcessFacade.getModalityGroupFacade(modalityGroup.getIdModalityGroupPk());
			
			if(!operationCashPart.isEmpty()){
				for(MechanismOperation operation: settlementProcessFacade.getMechanismOperationFacade(operationCashPart)){
					operations.append(operation.getOperationNumber()).append("-");
				}
			}
			
			if(!operationTermPart.isEmpty()){
				for(MechanismOperation operation: settlementProcessFacade.getMechanismOperationFacade(operationTermPart)){
					operations.append(operation.getOperationNumber()).append("-");
				}
			}
			
			parameterToSend = new Object[]{operations.toString(),mechanismType.name(),modalityGroup.getGroupName(),currency.name()};
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return parameterToSend;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return sendNotification;
	}
}
