package com.pradera.batch;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.core.to.SettlementOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementProcessWebServiceBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="SettlementProcessWebServiceBatch")
public class SettlementProcessWebServiceBatch implements JobExecution {

	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		Date settlementDate= CommonsUtilities.currentDate();
		Long idSettlementProcess= null;
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
		{
			if ((Validations.validateIsNotNull(objDetail.getParameterName())))
			{
				if (StringUtils.equalsIgnoreCase(SettlementConstant.ID_SETTLEMENT_PROCESS_PARAMETER, objDetail.getParameterName())) {
					idSettlementProcess= new Long(objDetail.getParameterValue());
				}
			}
		}
		List<SettlementOperationTO> lstSettlementOperationTOs= settlementProcessFacade.getListSettlementOperationByProcess(settlementDate, idSettlementProcess);
		if (Validations.validateListIsNotNullAndNotEmpty(lstSettlementOperationTOs)) {
			settlementProcessFacade.sendSettlementOperationWebClient(lstSettlementOperationTOs);
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
