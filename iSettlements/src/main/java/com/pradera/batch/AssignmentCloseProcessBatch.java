package com.pradera.batch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.accountassignment.service.AccountAssignmentService;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;

// TODO: Auto-generated Javadoc
/**
 * Descripcion: Ejecuta proceso batch para el cierre de asignacion general.
 *
 * @author PRADERA TECHNOLOGIES
 */
@BatchProcess(name="AssignmentCloseProcessBatch")
public class AssignmentCloseProcessBatch implements JobExecution {

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(AssignmentCloseProcessBatch.class);
	
	/** The manage holder account assignment facade. */
	@EJB
	private AccountAssignmentFacade manageHolderAccountAssignmentFacade;
	
	@EJB 
	private AccountAssignmentService accountAssignmentService; 
	
	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) 
	{	
		logger.info("AssignmentCloseProcessBatch: Starting the process ...");
		String strIdAssignmentProcess= null;
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails) {
			if ((Validations.validateIsNotNull(objDetail.getParameterName()))) {
				if (StringUtils.equalsIgnoreCase(SettlementConstant.CLOSSING_ASSIGNMENT_PROCESS, objDetail.getParameterName())) {
					strIdAssignmentProcess= objDetail.getParameterValue();
				} 
			}
		}		
		if(Validations.validateIsNotNullAndNotEmpty(strIdAssignmentProcess)) {
			
			try {				
				
				manageHolderAccountAssignmentFacade.closeGeneralAssignmentProcess(strIdAssignmentProcess);								
				
				//we send the reports	
				Thread.sleep(20000); // 20 seconds waiting for send reports
				sendReports();
				
			} catch (Exception e) {
				logger.error(":::::::::::::::::::::: ERROR CIERRE DE ASIGNACION GENERAL ::::::::::::::::::::::::");
				e.printStackTrace();
				String message= null;
				if (e instanceof ServiceException) {
					ServiceException se= (ServiceException) e;
					message = PropertiesUtilities.getExceptionMessage(se.getMessage(), se.getParams(),new Locale("es"));
				} else {
					message = "ERROR PROCESO DE CIERRE DE ASIGNACION GENERAL";
				}
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.GENERAL_ASSIGNMENT_CLOSE.getCode());
				UserAccountSession userSession = new UserAccountSession();
				userSession.setUserName(processLogger.getIdUserAccount());			
				notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), businessProcess, Arrays.asList(userSession), 
											BusinessProcessType.GENERAL_ASSIGNMENT_CLOSE.getDescription(), message, NotificationType.PROCESS);
			}
		}
		logger.info("AssignmentCloseProcessBatch: Finishing the process ...");
	}
	
	/**
	 * Send reports.
	 */
	public void sendReports(){
		try{
			logger.info("ENVIAMOS LOS REPORTES.........");			
			SettlementReportTO filter = new SettlementReportTO(); 
			List<SettlementReportTO> lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			
			SettlementReportTO objSettlementReportTO = new SettlementReportTO();
			objSettlementReportTO.setOnlyUser(Boolean.TRUE);
			objSettlementReportTO.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));			
			lstSettlementReportTO.add(objSettlementReportTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				logger.info("SEND :::: REPORTE DE REGISTRO DE OPERACIONES BBV ");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.TRADING_RECORD_BBV.getCode());
			}						
						
			filter = new SettlementReportTO(); 
			lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			filter.setDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));	
			filter.setScheduleType(SettlementScheduleType.FIRST_SETTLEMENT.getCode().toString());
			lstSettlementReportTO = settlementReportFacade.netSettlementPositionsPreQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				logger.info("SEND :::: REPORTE DE RESUMEN DE POSICIONES NETAS (PRELIMINAR) ");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.NET_SETTLEMENT_POSITIONS_PRE.getCode());
			}		
						
			filter = new SettlementReportTO(); 
			lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			filter.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			filter.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			lstSettlementReportTO = settlementReportFacade.prepaidSettlementQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				logger.info("SEND :::: REPORTE DE SOLICITUDES DE LIQUIDACION ANTICIPADA - PARTICIPANTES ");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.PREPAID_SETTLEMENT.getCode());
				
				lstSettlementReportTO = new ArrayList<SettlementReportTO>();
				objSettlementReportTO = new SettlementReportTO();
				objSettlementReportTO.setOnlyUser(Boolean.TRUE);			
				objSettlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//initialDate
				objSettlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));//endDate
				lstSettlementReportTO.add(objSettlementReportTO);
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
					logger.info("SEND :::: REPORTE DE SOLICITUDES DE LIQUIDACION ANTICIPADA - USUARIO EDV");
					settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.PREPAID_SETTLEMENT.getCode());
				}
				
			}											
			
			filter = new SettlementReportTO(); 
			lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			filter.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			filter.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			lstSettlementReportTO = settlementReportFacade.settlementTypeExchangeQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				logger.info("SEND :::: REPORTE DE SOLICITUDES DE CAMBIO DE TIPO DE LIQUIDACION (SELVE) - PARTICIPANTES");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.SETTLEMENT_TYPE_EXCHANGE.getCode());
				
				lstSettlementReportTO = new ArrayList<SettlementReportTO>();
				objSettlementReportTO = new SettlementReportTO(); 
				objSettlementReportTO.setOnlyUser(Boolean.TRUE);
				objSettlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
				objSettlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
				lstSettlementReportTO.add(objSettlementReportTO);			
				if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
					logger.info("SEND :::: REPORTE DE SOLICITUDES DE CAMBIO DE TIPO DE LIQUIDACION (SELVE) USUARIO EDV");
					settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.SETTLEMENT_TYPE_EXCHANGE.getCode());
				}
				
			}										
			
			lstSettlementReportTO = new ArrayList<SettlementReportTO>();
			objSettlementReportTO = new SettlementReportTO(); 
			objSettlementReportTO.setOnlyUser(Boolean.TRUE);
			objSettlementReportTO.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			objSettlementReportTO.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			lstSettlementReportTO.add(objSettlementReportTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				logger.info("SEND :::: REGISTRO DE GENERACION DE ENCADENAMIENTOS :::::::::");
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.GENERATOR_CHAINED_OPERATION.getCode());
			}
			
		} catch(ServiceException ex){
			logger.error(":::::::::::::::::::::: ERROR AL ENVIAR LOS REPORTES ::::::::::::::::::::::::");
			ex.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
