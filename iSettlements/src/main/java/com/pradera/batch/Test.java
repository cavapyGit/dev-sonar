package com.pradera.batch;

import java.io.IOException;
import java.math.BigDecimal;

import javax.xml.parsers.ParserConfigurationException;

import org.beanio.BeanIOConfigurationException;

import com.pradera.integration.exception.ServiceException;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class Test.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class Test {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public static void main(String args[]) throws BeanIOConfigurationException, IllegalArgumentException, ServiceException, IOException, ParserConfigurationException {
		
//		ProcessFileTO objProcessFileTO= null;
//		objProcessFileTO = new ProcessFileTO();
//		objProcessFileTO.setStreamFileDir("schema/edv/issuances.xml");
//		objProcessFileTO.setStreamResponseFileDir("schema/edvresponse/issuances_response.xml");					
//		objProcessFileTO.setTempProcessFile(new File("M:\\desarrollo\\workspace.edv\\INTERFACES_EXT\\BCB_TGN\\registroEmisionValorTGN.xml"));						
//		objProcessFileTO.setLocale(new Locale("es"));
//		objProcessFileTO.setFileName("lolwat.xml");
//		objProcessFileTO.setIndAutomaticProcess(GeneralConstants.TWO_VALUE_INTEGER);
//		objProcessFileTO.setIdExternalInterface(26l);	
//		objProcessFileTO.setInterfaceName("RVA");
//		objProcessFileTO.setRootTag("REG_VALOR");					
//		CommonsUtilities.validateFileOperationStruct(objProcessFileTO);
//		int b = 0;
		
		BigDecimal realPrice = new BigDecimal(990);
		BigDecimal rate = new BigDecimal(1.5);
		BigDecimal newPrice = NegotiationUtils.getTermPrice(realPrice , rate, 44l);
		System.out.println(newPrice);
		
	}
	
}
