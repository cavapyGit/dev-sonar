package com.pradera.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.batchprocess.service.ManagerParameter;
import com.pradera.funds.fundsoperations.deposit.facade.ManageDepositServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.process.ProcessLogger;
import com.pradera.negotiations.jaxb.dto.OperationTutXmlDTO;
import com.pradera.settlements.core.facade.SettlementProcessFacade;

@BatchProcess(name="SendLastHolderOfDPFandDPABatch")
@RequestScoped
public class SendLastHolderOfDPFandDPABatch implements Serializable, JobExecution  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private static final String ISSUER_BEC = "BO011";
	
	@Inject
	private PraderaLogger log;
	
	/** The manage deposit service facade. */
	@EJB
	ManageDepositServiceFacade manageDepositServiceFacade;
	
	@EJB
	SettlementProcessFacade settlementProcessFacade;
	
	/** The manager parameter. */
	@Inject
	ManagerParameter managerParameter;	

	/* (non-Javadoc)	
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info("::: Iniciando batchero: SendLastHolderOfDPFandDPABatch");
		try {
			
			// paso 1: obtener todos los emisores en estado registrado
			Date date = CommonsUtilities.currentDate();
			List<String> lstIssuers=settlementProcessFacade.lstIdAllIssuer();
			OperationTutXmlDTO operationTut;
			if(Validations.validateListIsNotNullAndNotEmpty(lstIssuers)){
				// paso 2: obtener los valores
				for(String issuer: lstIssuers){
					if(issuer.equals(ISSUER_BEC)){
						// cosntruccion del XML a nivel objeto
						operationTut=settlementProcessFacade.buildXmlToSendIssuer(issuer, date);
						if(Validations.validateIsNotNull(operationTut)){
							// envio del xml al BUS
							settlementProcessFacade.sendInformationLastHolderDPFWebClient(operationTut);
						}
					}
				}
			}else{
				log.info("::: No se encontraron Emisores");
			}
			
//			String currencyType = null;
//			//processLogger.getProcessLoggerDetails().get(0)
//			for(ProcessLoggerDetail processLoggerDetail : processLogger.getProcessLoggerDetails()){
//				if(processLoggerDetail != null && processLoggerDetail.getParameterName() != null 
//						&& processLoggerDetail.getParameterName().length() > 0 
//						&& "currencyType".equals(processLoggerDetail.getParameterName())){
//					currencyType = processLoggerDetail.getParameterValue();
//				}
//			}
//			
//			Date currentDate = CommonsUtilities.currentDate();						
//			manageDepositServiceFacade.sendDepositLip(currentDate, currencyType);	
			
			System.out.println("::: Fin batchero: SendLastHolderOfDPFandDPABatch ..... ");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("::: Fin batchero: SendLastHolderOfDPFandDPABatch");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}