package com.pradera.batch;

import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.core.facade.SettlementProcessFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementExchangeRateProcessBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="SettlementExchangeRateProcessBatch")
public class SettlementExchangeRateProcessBatch implements JobExecution{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(SettlementExchangeRateProcessBatch.class);
	
	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		logger.debug("SettlementExchangeRateProcessBatch: Starting the process  SettlementExchangeRateProcessBatch ...");
		
		try {
			settlementProcessFacade.updateSettlementExchangeRateOperations(CommonsUtilities.currentDate());
		} catch (Exception e) {
			logger.error("Error executing the process SettlementExchangeRateProcessBatch");
			e.printStackTrace();
			
			String messageNotification = null;
			if (e instanceof ServiceException) {
				ServiceException se = (ServiceException) e;
				messageNotification = PropertiesUtilities.getExceptionMessage(se.getMessage(), se.getParams(),new Locale("es"));
			} else {
				messageNotification = "OCURRIO UN ERROR EN EL PROCESO ACTUALIZAR MONTOS DE OPERACIONES DE LIQUIDACION, FAVOR REVISAR BITACORA DE PROCESOS";
			}
			
			UserFilterTO  userFilter = new UserFilterTO();
			userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());
			userFilter.setUserName(processLogger.getIdUserAccount());
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);						
			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, messageNotification, NotificationType.EMAIL);
		}		
		logger.debug("SettlementExchangeRateProcessBatch: Finishing the process  SettlementExchangeRateProcessBatch ...");
	}
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
