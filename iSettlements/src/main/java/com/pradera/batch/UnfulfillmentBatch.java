package com.pradera.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.settlements.chainedoperation.facade.ChainedOperationsFacade;
import com.pradera.settlements.core.facade.SettlementProcessFacade;
import com.pradera.settlements.reports.SettlementReportTO;
import com.pradera.settlements.reports.facade.SettlementReportFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UnfulfillmentBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="UnfulfillmentBatch")
public class UnfulfillmentBatch implements JobExecution{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(UnfulfillmentBatch.class);
	
	/** The settlement process facade. */
	@EJB
	private SettlementProcessFacade settlementProcessFacade;
	
	/** The chained operations facade. */
	@EJB
	private ChainedOperationsFacade chainedOperationsFacade;
	
	/** The settlement report facade. */
	@EJB
	private SettlementReportFacade settlementReportFacade;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		logger.debug("UnfulfillmentBatch: Starting the process ...");

		Long idAssignmentProcess= null;
		Long idMechanism= null;
		Long idModality= null;
		Integer currency = null;
		Date settlementDate= CommonsUtilities.currentDate();
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
		{
			if ((Validations.validateIsNotNull(objDetail.getParameterName())))
			{
				if (StringUtils.equalsIgnoreCase(SettlementConstant.ASSIGNMENT_UNFULFILLMENT, objDetail.getParameterName())) {
					idAssignmentProcess= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.MECHANISM_PARAMETER, objDetail.getParameterName())) {
					idMechanism= new Long(objDetail.getParameterValue());
				} else if (StringUtils.equalsIgnoreCase(SettlementConstant.MODALITY_PARAMETER, objDetail.getParameterName())) {
					idModality= new Long(objDetail.getParameterValue());
				}else if (StringUtils.equalsIgnoreCase(SettlementConstant.CURRENCY_PARAMETER, objDetail.getParameterName())) {
					currency= new Integer(objDetail.getParameterValue());
				}
				
			}
		}
		try {
			//first we must cancel the chained operation still alive
			//chainedOperationsFacade.cancelAllChainedHolderOperations(settlementDate, BooleanType.NO.getCode());
			//to identify all the unfulfillment about mechanism operation 
			int quantityUnfulfillment = settlementProcessFacade.identifySettlementUnfulfillment(settlementDate, idAssignmentProcess, idMechanism, idModality, currency);			
			settlementProcessFacade.sendOpeUnfulReports(settlementDate, idMechanism, currency);
			
			// Send Notification
			sendNotificationProcess(processLogger, quantityUnfulfillment);
			
			Thread.sleep(20000); 
			sendReports();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.debug("UnfulfillmentBatch: Finishing the process ...");
	}
	
	/**
	 * Send reports.
	 */
	public void sendReports(){
		try{
			SettlementReportTO filter = new SettlementReportTO(); 
			filter.setInitialDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			filter.setEndDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			List<SettlementReportTO> lstSettlementReportTO = settlementReportFacade.unfulfilledOperationsQuery(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementReportTO)){
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.UNFULFILLED_OPERATIONS.getCode());				
				lstSettlementReportTO = new ArrayList<SettlementReportTO>();
				filter.setOnlyUser(Boolean.TRUE);
				lstSettlementReportTO.add(filter);
				settlementReportFacade.sendReports(lstSettlementReportTO, ReportIdType.UNFULFILLED_OPERATIONS.getCode());
			}
		}catch(ServiceException ex){
			logger.error(":::::::::::::::::::::: ERROR AL ENVIAR LOS REPORTES ::::::::::::::::::::::::");
			ex.printStackTrace();
		}
	}
	
	/**
	 * Send notification process.
	 *
	 * @param processLogger the process logger
	 * @param quantityUnfulfillment the quantity unfulfillment
	 */
	public void sendNotificationProcess(ProcessLogger processLogger, int quantityUnfulfillment) {
		if(quantityUnfulfillment > 0) {
			notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), 
					processLogger.getBusinessProcess(), null, new Object[] {quantityUnfulfillment});
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
}
