package com.pradera.batch;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.core.service.SettlementProcessService;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SalePurchaseUpdateBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="ValuatorSPBalanceUnblockSelidBatch")
public class ValuatorSPBalanceUnblockSelidBatch implements JobExecution{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(ValuatorSPBalanceUnblockSelidBatch.class);
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
		
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		logger.debug("ValuatorSPBalanceUnblockSelidBatch: Starting the process ...");
		
		//issue 1040 
		try {
			settlementProcessService.updateHolderMarketfactBalanceSelid(processLogger.getLastModifyUser(),
																		processLogger.getLastModifyApp(),
																		processLogger.getLastModifyDate(),
																		processLogger.getLastModifyIp());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
		logger.debug("ValuatorSPBalanceUnblockSelidBatch: Finishing the process ...");
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
