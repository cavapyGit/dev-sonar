package com.pradera.guarantees.gvc.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.guarantees.gvc.facade.GvcListServiceFacade;
import com.pradera.guarantees.gvc.to.GvcGroupTO;
import com.pradera.guarantees.gvc.to.GvcListSearchFilter;
import com.pradera.guarantees.management.facade.GuaranteeOperationsServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.generalparameter.type.SecurityGroupType;
import com.pradera.model.guarantees.GvcList;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class TableValueReferenceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2013
 */
/**
 * @author allen
 *
 */
@DepositaryWebBean
@LoggerCreateBean
public class GvcManagementBean extends GenericBaseBean implements Serializable {

	/** Added Default Serial UID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
    UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
    UserPrivilege userPrivilege;
	
	/** The modality mechanism facade. */
	@EJB
	private GvcListServiceFacade modalityMechanismFacade;

	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The guarantees service bean. */
	@EJB
	private GuaranteeOperationsServiceFacade guaranteesServiceBean;

	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;

	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;

	/** The securities helper bean. */
	@Inject
	private SecuritiesHelperBean securitiesHelperBean;

	/** The lst mechanisms. */
	private List<NegotiationMechanism> lstMechanisms;

	/** The lst modalities. */
	private List<NegotiationModality> lstModalities;

	/** The lst security class. */
	private List<ParameterTable> lstSecurityClass;

	/** The lst modalities reg. */
	private List<NegotiationModality> lstModalitiesReg;

	/** The lst security class reg. */
	private List<ParameterTable> lstSecurityClassReg;
	
	/** The lst issuance currency. */
	private List<ParameterTable> lstIssuanceCurrency;

	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The security to add. */
	private Security securityToAdd;

	/** The gvc group data model. */
	private GenericDataModel<GvcGroupTO> gvcGroupDataModel;

	/** The selected group. */
	private GvcGroupTO selectedGroup;

	/** The gvc list. */
	private GvcList gvcList;

	/** The tmp gvc list. */
	private GvcList tmpGvcList;

	/** The security data model. */
	private GenericDataModel<Security> securityDataModel; // only for view purposes, current securities in group

	/** The option type. */
	private Integer optionType;

	/** The disable security. */
	private boolean disableSecurity;

	/** The disable other security. */
	private boolean disableOtherSecurity;

	/** The search filter data. */
	private GvcListSearchFilter searchFilterData;

	/** The punishment factor flag. */
	private boolean punishmentFactorFlag;

	/** The remove securities. */
	private List<Security> removeSecurities; // total securities marked for removal

	/** The added securities. */
	private List<Security> addedSecurities; // total securities for new insert

	/** The remove security. */
	private Security removeSecurity; // marked for removal

	/** The valid security flag. */
	private boolean validSecurityFlag;

	/** The modify punishment factor. */
	private BigDecimal modifyPunishmentFactor;

	/** The lst security group. */
	private List<ParameterTable> lstSecurityGroup;

	/** The is update. */
	private boolean isUpdate; // if an existing group is loaded or is an update action si un grupo existente es cargado o es una accion de actualizacion
	
	/** The is group complete. */
	private boolean isGroupComplete; // know if mechanism - modality - class - group list combination is filled saber si el mecanismo - modalidad - clase - Lista de grupo de combinacion se llena
	
	/** The is detail view. */
	private boolean isDetailView;
	
	/** The search by mechanism. */
	private Integer searchByMechanism = Integer.valueOf(1);
	
	/** The search by security. */
	private Integer searchBySecurity = Integer.valueOf(2);

	/**
	 * Instantiates a new gvc management bean.
	 */
	public GvcManagementBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Removes the security group.
	 */
	public void removeSecurityGroup() {
		hideDialogs();
		if (Validations.validateIsNull(removeSecurities)) {
			removeSecurities = new ArrayList<Security>();
		}
		removeSecurities.add(removeSecurity);
		if (Validations.validateIsNotNull(addedSecurities)) {
			addedSecurities.remove(removeSecurity);
		}
		securityDataModel.getDataList().remove(removeSecurity);
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {
			searchFilterData = new GvcListSearchFilter();
			searchFilterData.setSecurityFilter(new Security());
			loadMechanisms();
			loadData();
			loadCurrency();
			
			PrivilegeComponent privilegeComponent = new PrivilegeComponent();
			privilegeComponent.setBtnModifyView(true);
			userPrivilege.setPrivilegeComponent(privilegeComponent);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load cbo filter curency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCurrency() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstIssuanceCurrency=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstIssuanceCurrency, false);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	/**
	 * Metodo que busca y llena el combo box(id="tvrMechanism"). Los tipos de
	 * mecanismos cargadons son:BOLSA DE VALORES, OTC y SIRTEX.
	 */
	public void loadMechanisms() {
		try {
			lstMechanisms = guaranteesServiceBean.searchMechanismsServiceFacade();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Load data.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadData() throws ServiceException {
		optionType = searchByMechanism;
		disableSecurity = true;
		disableOtherSecurity = false;
		
		ParameterTableTO param = new ParameterTableTO();
		param.setMasterTableFk(MasterTableType.SECURITIES_GROUP.getCode());
		param.setOrderbyParameterTableCd(BooleanType.YES.getCode());
		lstSecurityGroup = generalParametersFacade.getListParameterTableServiceBean(param);
	}
	
	/**
	 * Metodo que carga y llena el combo box de modalidades segun el tipo de
	 * mecanismo seleccionado.
	 */
	public void fillModalidity() {
		try {

			lstSecurityClass = null;
			searchFilterData.setSecurityClass(null);
			searchFilterData.setIdNegotiationModFk(null);
			Long negotiationMechanism = searchFilterData.getIdNegotiationMechFk();
			
			if (Validations.validateIsNotNullAndPositive(negotiationMechanism)) {
				lstModalities = modalityMechanismFacade.searchModalityMechanism(searchFilterData.getIdNegotiationMechFk());
			} else {
				lstModalities = null;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * On change mechanism.
	 */
	public void onChangeMechanism() {
		try {
			isGroupComplete = false;
			lstModalitiesReg = null;
			lstSecurityClassReg = null;
			gvcList.getMechanismModality().getId().setIdNegotiationModalityPk(null);
			gvcList.setSecurityClass(null);
			Long negotiationMechanism = gvcList.getMechanismModality().getId().getIdNegotiationMechanismPk();
			if (Validations.validateIsNotNullAndPositive(negotiationMechanism)) {
				lstModalitiesReg = modalityMechanismFacade.searchModalityMechanism(negotiationMechanism);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Gets the Search results.
	 */
	public void searchResultGvcList() {
		try {
			if(searchFilterData != null ){
				List<GvcGroupTO> referenceValues = modalityMechanismFacade.searchResultTvrFacade(searchFilterData);
				gvcGroupDataModel = new GenericDataModel<GvcGroupTO>(referenceValues);
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Add the Security to Group.
	 */
	public void addSecurityGvc() {
		hideDialogs();
		try {

			if (Validations.validateIsNotNullAndNotEmpty(securityToAdd.getIdSecurityCodePk())) {
				// Verifying duplicate security added or not
				if (Validations.validateIsNull(securityDataModel)) {
					securityDataModel = new GenericDataModel<Security>();
				}

				if (Validations.validateIsNull(addedSecurities)) {
					addedSecurities = new ArrayList<Security>();
				}

				for (Security security : securityDataModel.getDataList()) {
					if (securityToAdd.getIdSecurityCodePk().equals(
							security.getIdSecurityCodePk())) {
						securityToAdd = new Security();
						JSFUtilities.showComponent("cnfEndTransactionSamePage");
						showMessageOnDialog(
								PropertiesUtilities
										.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities
										.getMessage(PropertiesConstants.MSG_GVC_DUPLICATE_SECURITY_INSERTED));
						return;
					}
				}

				securityDataModel.getDataList().add(securityToAdd);
				addedSecurities.add(securityToAdd);

				securityToAdd = new Security();
				validSecurityFlag = false;
				securitiesHelperBean
						.setSecurityDescription(GeneralConstants.EMPTY_STRING);

			}// Check if security is not selected to add the group
			else {
				securityToAdd = new Security();
				JSFUtilities.showComponent("cnfEndTransactionSamePage");
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Gets the option type.
	 *
	 * @return the optionType
	 */
	public Integer getOptionType() {
		return optionType;
	}

	/**
	 * Sets the option type.
	 *
	 * @param optionType            the optionType to set
	 */
	public void setOptionType(Integer optionType) {
		if (GeneralConstants.ONE_VALUE_INTEGER.equals(optionType)) {
			disableSecurity = true;
			disableOtherSecurity = false;
			searchFilterData = new GvcListSearchFilter();
			searchFilterData.setSecurityFilter(new Security());
			securitiesHelperBean.setSecurityDescription(GeneralConstants.EMPTY_STRING);
		}
		if (GeneralConstants.TWO_VALUE_INTEGER.equals(optionType)) {
			disableOtherSecurity = true;
			disableSecurity = false;
			searchFilterData = new GvcListSearchFilter();
			searchFilterData.setSecurityFilter(new Security());
		}
		this.optionType = optionType;
	}

	/**
	 * Setting values before load the update page Metodo llamado desde boton de
	 * registro(search.xhtml) y carga de la pantalla de registro(gvcMgmt.xhtml)
	 * 
	 * @return the String type
	 */

	public String register() {
		clearGvcRegisterDetails();
		return "update";
	}

	/**
	 * Details gvc.
	 *
	 * @param seletedGroup the seleted group
	 * @return the string
	 */
	public String detailsGvc(GvcGroupTO seletedGroup) {

		isDetailView = true;

		loadGvcDetails(seletedGroup);

		return "update";
	}

	/**
	 * Load gvc details.
	 *
	 * @param seletedGroup the seleted group
	 */
	public void loadGvcDetails(GvcGroupTO seletedGroup) {
		gvcList = modalityMechanismFacade.getGvcGroupById(seletedGroup
				.getIdGroupPk());
		List<Security> securities = modalityMechanismFacade
				.getGvcDetails(gvcList);
		securityDataModel = new GenericDataModel<Security>(securities);
		modifyPunishmentFactor = gvcList.getGroupPercent();

		lstModalitiesReg = new ArrayList<NegotiationModality>(0);
		lstModalitiesReg.add(gvcList.getMechanismModality()
				.getNegotiationModality());

		lstSecurityClassReg = new ArrayList<ParameterTable>(0);
		lstSecurityClassReg.add(new ParameterTable(gvcList.getSecurityClass(),
				seletedGroup.getSecurityClassDescription()));

	}

	/**
	 * Update.
	 *
	 * @return the string
	 */
	public String update() {
		hideDialogs();
		clearGvcRegisterDetails();

		// Check the Any record selected or not
		if (Validations.validateIsNull(selectedGroup)) {
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
		} else {

			loadGvcDetails(selectedGroup);
			securitiesHelperBean.setSecurityDescription(GeneralConstants.EMPTY_STRING);
			securitiesHelperBean.getSecuritiesSearcher().setInstrumentType(gvcList.getMechanismModality().getNegotiationModality().getInstrumentType());
			securitiesHelperBean.loadCboSecuritieTypeByInstrumentHandler();

			isGroupComplete = true;
			punishmentFactorFlag = false;
			isUpdate = true;

			return "update";
		}
		return null;
	}

	/**
	 * Gets the security data model.
	 *
	 * @return the securityDataModel
	 */
	public GenericDataModel<Security> getSecurityDataModel() {
		return securityDataModel;
	}

	/**
	 * Sets the security data model.
	 *
	 * @param securityDataModel            the securityDataModel to set
	 */
	public void setSecurityDataModel(
			GenericDataModel<Security> securityDataModel) {
		this.securityDataModel = securityDataModel;
	}

	/**
	 * Verify the Incorporate Security details.
	 */
	public void verifyInCorporateSecurity() {
		hideDialogs();
		securitiesHelperBean.getSecuritiesSearcher().setIsinCode(null);
		try {
			if (Validations.validateIsNotNullAndNotEmpty(securityToAdd
					.getIdSecurityCodePk())) {

				String message = GeneralConstants.EMPTY_STRING;
				boolean verifySuccFlag;
				// Get the Corresponding Security
				SecurityTO securityTO = new SecurityTO();
				securityTO.setIdSecurityCodePk(securityToAdd
						.getIdSecurityCodePk());

				securityToAdd = helperComponentFacade
						.findSecurityComponentServiceFacade(securityTO);

				if (!securityToAdd.getStateSecurity().equals(
						SecurityStateType.REGISTERED.getCode())) {
					message = PropertiesConstants.MSG_GVC_SECURITY_NOT_REGISTERED;
				}

				if (!securityToAdd.getIssuance().getStateIssuance()
						.equals(IssuanceStateType.AUTHORIZED.getCode())) {
					message = PropertiesConstants.MSG_GVC_ISSUANCE_NOT_REGISTERED;
				}
				verifySuccFlag = modalityMechanismFacade
						.verifySecurityAlreadyGroup(gvcList,
								securityToAdd.getIdSecurityCodePk());
				if (verifySuccFlag) {
					message = PropertiesConstants.MSG_GVC_SECURITY_ALREADY_GROUP;
				}
				verifySuccFlag = modalityMechanismFacade
						.verifyInstrumentTypeSecurityModality(gvcList
								.getMechanismModality().getId()
								.getIdNegotiationModalityPk(),
								securityToAdd.getInstrumentType());
				if (!verifySuccFlag) {
					message = PropertiesConstants.MSG_GVC_INSTRUMENT_TYPE_NOT_MODALITY;
				}
				if (!securityToAdd.getSecurityClass().equals(
						gvcList.getSecurityClass())) {
					message = PropertiesConstants.MSG_GVC_SECURITY_CLASS_NOT_GROUP;
				}

				if (!GeneralConstants.EMPTY_STRING.equals(message)) {
					JSFUtilities
							.showComponent("frmTvrUpdate:idcnfClearISINDlg");
					showMessageOnDialog(
							PropertiesUtilities
									.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities.getMessage(message));
					securitiesHelperBean
							.setSecurityDescription(GeneralConstants.EMPTY_STRING);
					securityToAdd = new Security();
				} else {

					validSecurityFlag = true;
				}
			} else{
				validSecurityFlag = false;
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Checks if is punishment factor flag.
	 *
	 * @return the punishmentFactorFlag
	 */
	public boolean isPunishmentFactorFlag() {
		return punishmentFactorFlag;
	}

	/**
	 * Sets the punishment factor flag.
	 *
	 * @param punishmentFactorFlag            the punishmentFactorFlag to set
	 */
	public void setPunishmentFactorFlag(boolean punishmentFactorFlag) {
		this.punishmentFactorFlag = punishmentFactorFlag;
	}

	/**
	 * Verify the entered Punishment factor value.
	 */
	public void verifyPunishmentFactor() {
		hideDialogs();
		if (modifyPunishmentFactor != null) {
			boolean verifyFactorFlag;
			try {
				verifyFactorFlag = modalityMechanismFacade
						.verifyPunishmentFactorFacade(gvcList,
								modifyPunishmentFactor);
				if (verifyFactorFlag) {
					modifyPunishmentFactor = null;
					JSFUtilities.showComponent("cnfEndTransactionSamePage");
					showMessageOnDialog(
							PropertiesUtilities
									.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities
									.getMessage(PropertiesConstants.MSG_GVC_PUNISHMENT_FACTOR_ERROR));
				}
			} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
			}
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityToAdd.getIdSecurityCodePk()) && 
				Validations.validateIsNotNullAndNotEmpty(modifyPunishmentFactor)){
			validSecurityFlag = true;
		}else{
			validSecurityFlag = false;
		}
	}

	/**
	 * Hides all the show dialogs.
	 */
	private void hideDialogs() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("frmTvrUpdate:idcnfPunishmentDlg");
		JSFUtilities.hideComponent("frmTvrUpdate:idcnfUpdatePunishmentDlg");
		JSFUtilities.hideComponent("frmTvrUpdate:idcnfDeleteSecurityDlg");
		JSFUtilities.hideComponent("frmTvrUpdate:idcnfSaveGroupDlg");
		JSFUtilities.hideComponent("frmTvrUpdate:idcnfClearISINDlg");
		JSFUtilities.hideComponent("frmTvrUpdate:idcnfLoadExistingGroup");
		JSFUtilities.hideComponent("frmTvrUpdate:idcnfSaveGroupConfirmDlg");
		JSFUtilities.hideComponent("frmTvrManage:cnfClear");
	}

	/**
	 * Before save gvc list.
	 */
	public void beforeSaveGvcList() {
		hideDialogs();

		if (Validations.validateListIsNullOrEmpty(addedSecurities)) {
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities
							.getMessage(PropertiesConstants.MSG_GVC_SECURITY_LIST_EMPTY));
			return;
		}

		JSFUtilities.showComponent("frmTvrUpdate:idcnfSaveGroupConfirmDlg");
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities
						.getMessage(PropertiesConstants.MSG_GVC_SAVE_CONFIRM));
	}

	/**
	 * Save gvc list.
	 */
	@LoggerAuditWeb
	public void saveGvcList() {
		updateGvcList();
	}

	/**
	 * Confirmation before select the Punishment factor.
	 */
	public void beforeUpdateGvcList() {
		hideDialogs();

		JSFUtilities.showComponent("frmTvrUpdate:idcnfUpdatePunishmentDlg");
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities
						.getMessage(PropertiesConstants.MSG_GVC_PUNISHMENT_FACTOR_UPDATE_CONF));
	}

	/**
	 * Update Punishment factor value,Delete and Incorporate securities details.
	 */
	@LoggerAuditWeb
	public void updateGvcList() {
		hideDialogs();
		Object[] args = new Object[5];
		gvcList.setGroupPercent(modifyPunishmentFactor);
		try {

			String strTemp = modalityMechanismFacade.saveTvrListSecurityGroup(gvcList, removeSecurities, addedSecurities, isUpdate);
			
			// Preparing parameters data
			args[0] = SecurityGroupType.lookup.get(gvcList.getGroupList()).getNumeric();
			args[1] = SecurityClassType.lookup.get(gvcList.getSecurityClass()).getValue().toString();
			args[2] = gvcList.getGroupPercent();

			List<String> addList = new ArrayList<String>();
			if (Validations.validateIsNotNull(addedSecurities)) {
				for (Security sec : addedSecurities) {
					addList.add(sec.getIdSecurityCodePk() + "-"
							+ sec.getDescription());
				}
			}
			args[3] = addList;
			List<String> deleteList = new ArrayList<String>();
			if (Validations.validateIsNotNull(removeSecurities)) {
				for (Security sec : removeSecurities) {
					deleteList.add(sec.getIdSecurityCodePk() + "-"
							+ sec.getDescription());
				}
			}
			args[4] = deleteList;

			JSFUtilities.showComponent("frmTvrUpdate:idcnfSaveGroupDlg");
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(strTemp, args));

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Confirmation before delete the security.
	 *
	 * @param removeDetail            the Security
	 */
	public void beforeRemoveSecurityConfirm(Security removeDetail) {
		hideDialogs();
		if (Validations.validateIsNotNull(removeDetail)) {
			removeSecurity = removeDetail;
			JSFUtilities.showComponent("frmTvrUpdate:idcnfDeleteSecurityDlg");
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities
							.getMessage(PropertiesConstants.MSG_GVC_DELETE_SECURITY_GROUP_CONF));
		}
	}

	/**
	 * Clear the Search details Inicio y/o limpia de valores al momento de
	 * cargar la pantalla de registro(gvcMgmt.xhtml)
	 */
	public void clearGvcRegisterDetails() {
		JSFUtilities.setValidViewComponentAndChildrens(":frmTvrUpdate");
		hideDialogs();
		
		securityToAdd = new Security();

		securitiesHelperBean.getSecuritiesSearcher().setStateSecuritie(SecurityStateType.REGISTERED.getCode());
		securitiesHelperBean.setSecurityDescription(null);
		
		gvcList = new GvcList();
		gvcList.setMechanismModality(new MechanismModality(new MechanismModalityPK()));
		
		lstModalitiesReg = null;
		lstSecurityClassReg = null;

		isUpdate = false;
		isDetailView = false;
		isGroupComplete = false;
		modifyPunishmentFactor = null;
		removeSecurities = null;
		removeSecurity = null;
		addedSecurities = null;
		securityDataModel = null;
	}
	

	/**
	 * Clear gvc update details.
	 */
	public void clearGvcUpdateDetails() {
		JSFUtilities.setValidViewComponentAndChildrens(":frmTvrUpdate");
		hideDialogs();
		
		if(isUpdate == true){
			setModifyPunishmentFactor(null);
			securityToAdd = new Security();
		} else {
			clearGvcRegisterDetails();
		}
			validSecurityFlag = false;
	}
	
	/**
	 * Fill the Security class based the Modality Busqueda de clases segun
	 * mecanismo y modalidades registradas.
	 */
	public void fillSecurityClass() {
		Integer instrumentType = 0;
		Long modality = searchFilterData.getIdNegotiationModFk();
		try {
			lstSecurityClass = null;
			searchFilterData.setSecurityClass(null);
			// Get the instrument type of the selected modality
			if (Validations.validateIsNotNullAndPositive(modality)
					&& Validations.validateIsNotNull(lstModalities)) {
				for (NegotiationModality modalities : lstModalities) {
					if (modalities.getIdNegotiationModalityPk()
							.equals(modality)) {
						instrumentType = modalities.getInstrumentType();
						break;
					}
				}
				// Get the security class
				if (Validations.validateIsPositiveNumber(instrumentType)) {
					lstSecurityClass = modalityMechanismFacade
							.getParameterSecurityClas(
									MasterTableType.SECURITIES_CLASS.getCode(),
									instrumentType);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * On change modality.
	 *
	 * @author Pradera software
	 * @author Henry Coarite Fill the Security class based the Modality Evento
	 *         que se carga al momento de seleccionar una modalidad, en este
	 *         caso debemos actulizar el combo box de clase valor. Se cargan
	 *         todas las clases valor segun la modalidad
	 */
	public void onChangeModality() {
		try {
			Integer instrumentType = 0;
			isGroupComplete = false;
			lstSecurityClassReg = null;
			gvcList.setSecurityClass(null);
			Long negotiationModalityPk = gvcList.getMechanismModality().getId()
					.getIdNegotiationModalityPk();// obtenemos el PK del
													// mecanismo de negociacion
			// Get the instrument type of the selected modality
			if (Validations.validateIsNotNullAndPositive(negotiationModalityPk)
					&& Validations.validateIsNotNull(lstModalitiesReg)) {
				for (NegotiationModality modality : lstModalitiesReg) {
					if (modality.getIdNegotiationModalityPk().equals(
							negotiationModalityPk)) {
						instrumentType = modality.getInstrumentType();
						break;
					}
				}// Get the security class
				if (Validations.validateIsPositiveNumber(instrumentType)) {
					securitiesHelperBean.getSecuritiesSearcher()
							.setInstrumentType(instrumentType);
					securitiesHelperBean
							.loadCboSecuritieTypeByInstrumentHandler();
					// cargamos la lista para el combo box de clases.
					lstSecurityClassReg = modalityMechanismFacade
							.getParameterSecurityClas(
									MasterTableType.SECURITIES_CLASS.getCode(),
									instrumentType);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * On change security class.
	 */
	public void onChangeSecurityClass() {
		isGroupComplete = true;
		gvcList.setGroupList(null);
	}

	/**
	 * On change security group.
	 */
	public void onChangeSecurityGroup() {
		hideDialogs();
		tmpGvcList = modalityMechanismFacade.getGvcGroupByParameters(gvcList);
		if (tmpGvcList != null) {
			tmpGvcList.setMechanismModality(gvcList.getMechanismModality());
			JSFUtilities.showComponent("frmTvrUpdate:idcnfLoadExistingGroup");
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities
							.getMessage(PropertiesConstants.MSG_GVC_LOAD_EXISTING_GROUP_CONFIRM));
			// JSFUtilities.showMessageOnDialog(
			// PropertiesConstants.DIALOG_HEADER_CONFIRM, null,
			// PropertiesConstants.MSG_GVC_LOAD_EXISTING_GROUP_CONFIRM, null);
		}
	}

	// estaba comentado
	/**
	 * Load existing gvc list.
	 */
	public void loadExistingGvcList() {
		gvcList = tmpGvcList;
		tmpGvcList = null;

		List<Security> securities = modalityMechanismFacade.getGvcDetails(gvcList);
		securityDataModel = new GenericDataModel<Security>(securities);
		modifyPunishmentFactor = gvcList.getGroupPercent();
		isUpdate = true;

	}

	/**
	 * Clean security class.
	 */
	public void cleanSecurityClass() {
		isGroupComplete = false;
		gvcList.setSecurityClass(null);
		gvcList.setGroupList(null);
	}

	/**
	 * Refresh the search page.
	 *
	 * @return the string
	 */
	public String saveSecurityGroupSucc() {
		clearGvcRegisterDetails();
		if (Validations.validateIsNotNull(removeSecurities)) {
			removeSecurities.clear();
		}
		//searchResultGvcList();
		return "searchGvc";
	}

	/**
	 * Clear the Entered ISIN Code.
	 */
	public void clearIsinCode() {
		hideDialogs();
		securityToAdd = new Security();
		securitiesHelperBean
				.setSecurityDescription(GeneralConstants.EMPTY_STRING);
	}

	/**
	 * Back to search page handler.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String backToSearchPageHandler() throws ServiceException{
		setViewOperationType(ViewOperationsType.CONSULT.getCode());		
		return "searchGvc";
	}
	
	/**
	 * Method for logout the user session on idle time of the application.
	 */
	public void sessionLogout() {
		try {
			JSFUtilities.setHttpSessionAttribute(
					GeneralConstants.LOGOUT_MOTIVE,
					LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession();
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Confirmation before clear the search page data.
	 */
	public void beforeClearSearchPage() {
		searchFilterData = new GvcListSearchFilter();
		searchFilterData.setSecurityFilter(new Security());
		gvcGroupDataModel = null;
		optionType = searchByMechanism;
		disableSecurity = true;
		disableOtherSecurity = false;
		hideDialogs();
		
		lstSecurityClass = null;
		searchFilterData.setSecurityClass(null);
		searchFilterData.setIdNegotiationModFk(null);
		lstModalities = null;
		
		JSFUtilities.resetComponent("frmTvrManage:rbtTipoOpcion");
		JSFUtilities.resetComponent("frmTvrManage:tvrMechanism");
		JSFUtilities.resetComponent("frmTvrManage:gvcModality");
		JSFUtilities.resetComponent("frmTvrManage:idSecClass");
		JSFUtilities.resetComponent("frmTvrManage:cboGroupList");
		JSFUtilities.resetComponent("frmTvrManage:idsecurity");
	}

	/**
	 * Gets the removes the security.
	 *
	 * @return the removeSecurity
	 */
	public Security getRemoveSecurity() {
		return removeSecurity;
	}

	/**
	 * Sets the removes the security.
	 *
	 * @param removeSecurity            the removeSecurity to set
	 */
	public void setRemoveSecurity(Security removeSecurity) {
		this.removeSecurity = removeSecurity;
	}

	/**
	 * Gets the lst mechanisms.
	 *
	 * @return the lstMechanisms
	 */
	public List<NegotiationMechanism> getLstMechanisms() {
		return lstMechanisms;
	}

	/**
	 * Sets the lst mechanisms.
	 *
	 * @param lstMechanisms            the lstMechanisms to set
	 */
	public void setLstMechanisms(List<NegotiationMechanism> lstMechanisms) {
		this.lstMechanisms = lstMechanisms;
	}

	/**
	 * Gets the lst modalities.
	 *
	 * @return the lstModalities
	 */
	public List<NegotiationModality> getLstModalities() {
		return lstModalities;
	}

	/**
	 * Sets the lst modalities.
	 *
	 * @param lstModalities            the lstModalities to set
	 */
	public void setLstModalities(List<NegotiationModality> lstModalities) {
		this.lstModalities = lstModalities;
	}

	/**
	 * Gets the modify punishment factor.
	 *
	 * @return the modifyPunishmentFactor
	 */
	public BigDecimal getModifyPunishmentFactor() {
		return modifyPunishmentFactor;
	}

	/**
	 * Sets the modify punishment factor.
	 *
	 * @param modifyPunishmentFactor the new modify punishment factor
	 */
	public void setModifyPunishmentFactor(BigDecimal modifyPunishmentFactor) {
		this.modifyPunishmentFactor = modifyPunishmentFactor;
	}

	/**
	 * Gets the lst security class.
	 *
	 * @return the lstSecurityClass
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass            the lstSecurityClass to set
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	/**
	 * Checks if is disable security.
	 *
	 * @return the disableSecurity
	 */
	public boolean isDisableSecurity() {
		return disableSecurity;
	}

	/**
	 * Sets the disable security.
	 *
	 * @param disableSecurity the new disable security
	 */
	public void setDisableSecurity(boolean disableSecurity) {
		this.disableSecurity = disableSecurity;
	}

	/**
	 * Checks if is disable other security.
	 *
	 * @return the disableOtherSecurity
	 */
	public boolean isDisableOtherSecurity() {
		return disableOtherSecurity;
	}
	
	/**
	 * Sets the disable other security.
	 *
	 * @param disableOtherSecurity the new disable other security
	 */
	public void setDisableOtherSecurity(boolean disableOtherSecurity) {
		this.disableOtherSecurity = disableOtherSecurity;
	}

	/**
	 * Gets the search filter data.
	 *
	 * @return the searchFilterData
	 */
	public GvcListSearchFilter getSearchFilterData() {
		return searchFilterData;
	}

	/**
	 * Sets the search filter data.
	 *
	 * @param searchFilterData the new search filter data
	 */
	public void setSearchFilterData(GvcListSearchFilter searchFilterData) {
		this.searchFilterData = searchFilterData;
	}

	/**
	 * Gets the removes the securities.
	 *
	 * @return the removeSecurities
	 */
	public List<Security> getRemoveSecurities() {
		return removeSecurities;
	}

	/**
	 * Sets the removes the securities.
	 *
	 * @param removeSecurities the new removes the securities
	 */
	public void setRemoveSecurities(List<Security> removeSecurities) {
		this.removeSecurities = removeSecurities;
	}

	/**
	 * Gets the gvc group data model.
	 *
	 * @return the gvcGroupDataModel
	 */
	public GenericDataModel<GvcGroupTO> getGvcGroupDataModel() {
		return gvcGroupDataModel;
	}

	/**
	 * Sets the gvc group data model.
	 *
	 * @param gvcGroupDataModel the new gvc group data model
	 */
	public void setGvcGroupDataModel(
			GenericDataModel<GvcGroupTO> gvcGroupDataModel) {
		this.gvcGroupDataModel = gvcGroupDataModel;
	}

	/**
	 * Gets the selected group.
	 *
	 * @return the selectedGroup
	 */
	public GvcGroupTO getSelectedGroup() {
		return selectedGroup;
	}

	/**
	 * Sets the selected group.
	 *
	 * @param selectedGroup            the selectedGroup to set
	 */
	public void setSelectedGroup(GvcGroupTO selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	/**
	 * Gets the gvc list.
	 *
	 * @return the gvcList
	 */
	public GvcList getGvcList() {
		return gvcList;
	}

	/**
	 * Sets the gvc list.
	 *
	 * @param gvcList the new gvc list
	 */
	public void setGvcList(GvcList gvcList) {
		this.gvcList = gvcList;
	}

	/**
	 * Gets the security to add.
	 *
	 * @return the securityToAdd
	 */
	public Security getSecurityToAdd() {
		return securityToAdd;
	}

	/**
	 * Sets the security to add.
	 *
	 * @param securityToAdd the new security to add
	 */
	public void setSecurityToAdd(Security securityToAdd) {
		this.securityToAdd = securityToAdd;
	}

	/**
	 * Gets the added securities.
	 *
	 * @return the addedSecurities
	 */
	public List<Security> getAddedSecurities() {
		return addedSecurities;
	}

	/**
	 * Sets the added securities.
	 *
	 * @param addedSecurities the new added securities
	 */
	public void setAddedSecurities(List<Security> addedSecurities) {
		this.addedSecurities = addedSecurities;
	}

	/**
	 * Gets the lst modalities reg.
	 *
	 * @return the lstModalitiesReg
	 */
	public List<NegotiationModality> getLstModalitiesReg() {
		return lstModalitiesReg;
	}

	/**
	 * Sets the lst modalities reg.
	 *
	 * @param lstModalitiesReg the new lst modalities reg
	 */
	public void setLstModalitiesReg(List<NegotiationModality> lstModalitiesReg) {
		this.lstModalitiesReg = lstModalitiesReg;
	}

	/**
	 * Gets the lst security class reg.
	 *
	 * @return the lstSecurityClassReg
	 */
	public List<ParameterTable> getLstSecurityClassReg() {
		return lstSecurityClassReg;
	}

	/**
	 * Sets the lst security class reg.
	 *
	 * @param lstSecurityClassReg the new lst security class reg
	 */
	public void setLstSecurityClassReg(List<ParameterTable> lstSecurityClassReg) {
		this.lstSecurityClassReg = lstSecurityClassReg;
	}

	/**
	 * Checks if is valid security flag.
	 *
	 * @return the validSecurityFlag
	 */
	public boolean isValidSecurityFlag() {
		return validSecurityFlag;
	}

	/**
	 * Sets the valid security flag.
	 *
	 * @param validSecurityFlag the new valid security flag
	 */
	public void setValidSecurityFlag(boolean validSecurityFlag) {
		this.validSecurityFlag = validSecurityFlag;
	}

	/**
	 * Gets the lst security group.
	 *
	 * @return the lstSecurityGroup
	 */
	public List<ParameterTable> getLstSecurityGroup() {
		return lstSecurityGroup;
	}

	/**
	 * Sets the lst security group.
	 *
	 * @param lstSecurityGroup the new lst security group
	 */
	public void setLstSecurityGroup(List<ParameterTable> lstSecurityGroup) {
		this.lstSecurityGroup = lstSecurityGroup;
	}

	/**
	 * Checks if is update.
	 *
	 * @return the isUpdate
	 */
	public boolean isUpdate() {
		return isUpdate;
	}

	/**
	 * Sets the update.
	 *
	 * @param isUpdate the new update
	 */
	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	/**
	 * Checks if is group complete.
	 *
	 * @return the isGroupComplete
	 */
	public boolean isGroupComplete() {
		return isGroupComplete;
	}

	/**
	 * Sets the group complete.
	 *
	 * @param isGroupComplete the new group complete
	 */
	public void setGroupComplete(boolean isGroupComplete) {
		this.isGroupComplete = isGroupComplete;
	}

	/**
	 * Checks if is detail view.
	 *
	 * @return the isDetailView
	 */
	public boolean isDetailView() {
		return isDetailView;
	}

	/**
	 * Sets the detail view.
	 *
	 * @param isDetailView the new detail view
	 */
	public void setDetailView(boolean isDetailView) {
		this.isDetailView = isDetailView;
	}

	/**
	 * Gets the search by mechanism.
	 *
	 * @return the search by mechanism
	 */
	public Integer getSearchByMechanism() {
		return searchByMechanism;
	}

	/**
	 * Gets the search by security.
	 *
	 * @return the search by security
	 */
	public Integer getSearchBySecurity() {
		return searchBySecurity;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}	
	
	
	
}
