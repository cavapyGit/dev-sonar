package com.pradera.guarantees.gvc.to;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GvcGroupTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class GvcGroupTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The modality name. */
	private String modalityName;
	
	/** The id group pk. */
	private Long idGroupPk;
	
	/** The group description. */
	private String groupDescription;
	
	/** The group percent. */
	private BigDecimal groupPercent;
	
	/** The security class description. */
	private	String securityClassDescription;
	

	/**
	 * Instantiates a new gvc group to.
	 *
	 * @param mechanismName the mechanism name
	 * @param modalityName the modality name
	 * @param groupDescription the group description
	 * @param groupPercent the group percent
	 * @param securityClassDescription the security class description
	 * @param idGroupPk the id group pk
	 */
	public GvcGroupTO(String mechanismName, String modalityName,
			String groupDescription, BigDecimal groupPercent,
			String securityClassDescription,Long idGroupPk) {
		super();
		this.mechanismName = mechanismName;
		this.modalityName = modalityName;
		this.groupDescription = groupDescription;
		this.groupPercent = groupPercent;
		this.securityClassDescription = securityClassDescription;
		this.idGroupPk = idGroupPk;
	}
	
	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanismName
	 */
	public String getMechanismName() {
		return mechanismName;
	}
	
	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the mechanismName to set
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}
	
	/**
	 * Gets the modality name.
	 *
	 * @return the modalityName
	 */
	public String getModalityName() {
		return modalityName;
	}
	
	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the modalityName to set
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}
	
	/**
	 * Gets the id group pk.
	 *
	 * @return the idGroupPk
	 */
	public Long getIdGroupPk() {
		return idGroupPk;
	}
	
	/**
	 * Sets the id group pk.
	 *
	 * @param idGroupPk the idGroupPk to set
	 */
	public void setIdGroupPk(Long idGroupPk) {
		this.idGroupPk = idGroupPk;
	}
	
	/**
	 * Gets the group description.
	 *
	 * @return the groupDescription
	 */
	public String getGroupDescription() {
		return groupDescription;
	}
	
	/**
	 * Sets the group description.
	 *
	 * @param groupDescription the groupDescription to set
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	
	/**
	 * Gets the group percent.
	 *
	 * @return the groupPercent
	 */
	public BigDecimal getGroupPercent() {
		return groupPercent;
	}
	
	/**
	 * Sets the group percent.
	 *
	 * @param groupPercent the groupPercent to set
	 */
	public void setGroupPercent(BigDecimal groupPercent) {
		this.groupPercent = groupPercent;
	}
	
	/**
	 * Gets the security class description.
	 *
	 * @return the securityClassDescription
	 */
	public String getSecurityClassDescription() {
		return securityClassDescription;
	}
	
	/**
	 * Sets the security class description.
	 *
	 * @param securityClassDescription the securityClassDescription to set
	 */
	public void setSecurityClassDescription(String securityClassDescription) {
		this.securityClassDescription = securityClassDescription;
	}
	
}
