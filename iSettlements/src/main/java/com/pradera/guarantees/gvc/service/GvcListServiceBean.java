package com.pradera.guarantees.gvc.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.guarantees.gvc.to.GvcGroupTO;
import com.pradera.guarantees.gvc.to.GvcListSearchFilter;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.guarantees.GvcList;
import com.pradera.model.guarantees.type.SecurityGvcStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class ModalityMechanismServiceBean
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2013
 */
@Stateless
@TransactionAttribute
public class GvcListServiceBean extends CrudDaoServiceBean implements Serializable {

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(GvcListServiceBean.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	/**
	 * This method is used to get the values from the database to filled the
	 * list values into Modality combo box based on user selected value from
	 * the Mechanism combo box.
	 *
	 * @param idNegMechanism the id neg mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> searchModalityMechanismValues (Long idNegMechanism) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> paramters = new HashMap<String, Object>();
		sbQuery.append("select negoMod from NegotiationModality negoMod,MechanismModality mech ");
		sbQuery.append("where negoMod.idNegotiationModalityPk = mech.id.idNegotiationModalityPk ");
		// sbQuery.append("negomech.idNegotiationMechanismPk = mech.id.idNegotiationMechanismPk ");
		if (Validations.validateIsNotNullAndNotEmpty(idNegMechanism) && idNegMechanism > 0) {
			sbQuery.append("and mech.id.idNegotiationMechanismPk =:mechanismPk ");
			paramters.put("mechanismPk",(idNegMechanism));
		}
		sbQuery.append(" and negoMod.indMarginGuarantee = 1 ");
		sbQuery.append("order by negoMod.modalityName ");
		return (List<NegotiationModality>) findListByQueryString(sbQuery.toString(),paramters);
	}

	/**
	 * Update Group value in Security table and get the data from data base and displayed in
	 * the data table.
	 *
	 * @param idgrouppk the idgrouppk
	 * @param strVal the str val
	 * @param loggerUsr the logger usr
	 * @return list of objects
	 * @throws ServiceException the service exception
	 */
	
	public void updateSecurityGroupValue(Long idgrouppk, String strVal, LoggerUser loggerUsr)throws ServiceException {	
			
		String strSql = "update SecurityGvcList gvl set gvl.securityGvcState  = :secGvcState , " 
				  + "lastModifyDate =  :lastMDate , "
				  + "lastModifyUser= :lastMuser , " 
				 + "lastModifyIp= :lastMIp ,"				
				+ "lastModifyApp= :lstMApp " 
			//	+ "  where gvl.security.idIsinCodePk = :idisincode " 
			+ "  where gvl.security.idSecurityCodePk = :idisincode "
				+ "  and gvl.gvcList.idGroupPk = :idgroupfk " ;
		Query query =em.createQuery(strSql);	
		query.setParameter("idgroupfk", idgrouppk);
		query.setParameter("secGvcState",SecurityGvcStateType.DISABLED.getCode());
		
		query.setParameter("lastMDate", loggerUsr.getAuditTime());
		query.setParameter("lastMuser", loggerUsr.getUserName());
		query.setParameter("lastMIp", loggerUsr.getIpAddress());
		query.setParameter("lstMApp", loggerUsr.getIdPrivilegeOfSystem());
		
		query.setParameter("idisincode", strVal);		
		query.executeUpdate();		

	}

	/**
	 *  Get the Tvr List data based on the Mechanism and Modality
	 *  
	 *
	 * @param searchFilterData the TvrListSearchFilter
	 * @return the List<TvrList>
	 * @throws ServiceException the service exception
	 */
	public List<GvcGroupTO> searchResultGvc(GvcListSearchFilter searchFilterData) throws ServiceException{
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbSql.append("select distinct ");
		sbSql.append(" new com.pradera.guarantees.gvc.to.GvcGroupTO( ");
		sbSql.append(" gvc.mechanismModality.negotiationMechanism.mechanismName, ");
		sbSql.append(" gvc.mechanismModality.negotiationModality.modalityName, ");
		sbSql.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = gvc.groupList) as groupListDes , ");
		sbSql.append(" gvc.groupPercent, ");
		sbSql.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = gvc.securityClass), gvc.idGroupPk ) ");
		sbSql.append("from SecurityGvcList sgl inner join sgl.gvcList gvc where 1=1 ");		
		
		if(Validations.validateIsNotNullAndNotEmpty(searchFilterData.getIdNegotiationMechFk())){
			sbSql.append("and gvc.mechanismModality.id.idNegotiationMechanismPk = :idNegMechanism ");
			parameters.put("idNegMechanism", searchFilterData.getIdNegotiationMechFk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchFilterData.getIdNegotiationModFk())){
			sbSql.append("and gvc.mechanismModality.id.idNegotiationModalityPk = :idNegModality ");
			parameters.put("idNegModality", searchFilterData.getIdNegotiationModFk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchFilterData.getSecurityClass())){
			sbSql.append("and gvc.securityClass = :secClass ");
			parameters.put("secClass", searchFilterData.getSecurityClass());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchFilterData.getGroupDescription())){
			sbSql.append("  and gvc.groupDescription like :description ");
			if(searchFilterData.getGroupDescription().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				parameters.put("description", searchFilterData.getGroupDescription().replace(GeneralConstants.ASTERISK_CHAR, 
						GeneralConstants.PERCENTAGE_CHAR));
			} else {
				parameters.put("description", searchFilterData.getGroupDescription());
			}
		}

		if(Validations.validateIsNotNullAndNotEmpty(searchFilterData.getGroupList())){
			sbSql.append("and gvc.groupList = :gpList ");
			parameters.put("gpList", searchFilterData.getGroupList());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchFilterData.getSecurityFilter().getIdSecurityCodePk())){
			sbSql.append("and sgl.security.idSecurityCodePk = :idSecurityCodePk ");
			parameters.put("idSecurityCodePk", searchFilterData.getSecurityFilter().getIdSecurityCodePk());
		}
	
		sbSql.append(" order by groupListDes asc ");
		
		@SuppressWarnings("unchecked")
		List<GvcGroupTO> lstFilter = findListByQueryString(sbSql.toString(),parameters);
		return lstFilter;
	}

	/**
	 * Verify the Instrument type of the security related selected modality.
	 *
	 * @param idmodalitypk the Long type
	 * @param instrumentType the Integer type
	 * @return the boolean type
	 * @throws ServiceException the service exception
	 */
	public boolean verifyInstrumentTypeSecurityModality(Long idmodalitypk,Integer instrumentType)throws ServiceException{
		boolean satisfyFlag;
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("select modality.idNegotiationModalityPk ");
		sbSql.append("from NegotiationModality modality ");
		sbSql.append("where modality.idNegotiationModalityPk =:idmodalitypk ");
		sbSql.append("and modality.instrumentType = :instType ");		
		parameters.put("idmodalitypk", idmodalitypk);
		parameters.put("instType", instrumentType);
		
		Object result =	findObjectByQueryString(sbSql.toString(), parameters);
		if(Validations.validateIsNotNull(result)){
			satisfyFlag = true;
		}else{
			satisfyFlag = false;
		}
		
		return satisfyFlag;
		
	}

	
	/**
	 * Verify the Punishment factor belongs to any other group.
	 *
	 * @param gvcList the gvc list
	 * @param modifyPunishmentFactor the TvrList type
	 * @return the List<Object> type
	 * @throws ServiceException the service exception
	 */
	public boolean verifyPunishmentFactorService(GvcList gvcList, BigDecimal modifyPunishmentFactor)throws ServiceException{		
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("select nvl(count(gvc.idGroupPk),0) ");
		sbSql.append("from GvcList gvc ");		
		sbSql.append("where gvc.mechanismModality.id.idNegotiationMechanismPk = :NegMechFk  ");
		sbSql.append("and gvc.mechanismModality.id.idNegotiationModalityPk = :NegModFk  ");
		sbSql.append("and gvc.securityClass = :secClass  ");	
		//sbSql.append("and gvc.groupList = :groupList  ");
		sbSql.append("and gvc.groupPercent = :factor  ");
			
		parameters.put("NegMechFk", gvcList.getMechanismModality().getId().getIdNegotiationMechanismPk());
		parameters.put("NegModFk", gvcList.getMechanismModality().getId().getIdNegotiationModalityPk());
		parameters.put("secClass", gvcList.getSecurityClass());
		//parameters.put("groupList", gvcList.getGroupList());
		parameters.put("factor", modifyPunishmentFactor);
		
		Long count = (Long)findObjectByQueryString(sbSql.toString(), parameters);
		if(count > 0){
			return true;
		}else{
			return false;
		}	
	}
	

	/**
	 * Verify the security belongs to the any other group.
	 *
	 * @param gvcList the gvc list
	 * @param idSecurityCodePk the id security code pk
	 * @return the boolean type
	 * @throws ServiceException the service exception
	 */
	public boolean verifySecurityAlreadyGroup(GvcList gvcList, String idSecurityCodePk)throws ServiceException{
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("select nvl(count(gvl.idSecurityGvcPk),0) ");
		sbSql.append("from SecurityGvcList gvl inner join gvl.gvcList gvc ");		
		sbSql.append("where gvc.mechanismModality.id.idNegotiationMechanismPk = :NegMechFk  ");
		sbSql.append("and gvc.mechanismModality.id.idNegotiationModalityPk = :NegModFk  ");
		sbSql.append("and gvc.securityClass = :secClass  ");
		//agregando grupo valor
		sbSql.append("and gvc.groupList = :groupList  ");
		sbSql.append("and gvl.security.idSecurityCodePk = :idSecurityCodePk  ");
		sbSql.append(" and gvl.securityGvcState = :securityGvcState  ");
			
		parameters.put("NegMechFk", gvcList.getMechanismModality().getId().getIdNegotiationMechanismPk());
		parameters.put("NegModFk", gvcList.getMechanismModality().getId().getIdNegotiationModalityPk());
		parameters.put("secClass", gvcList.getSecurityClass());
		//agregando grupo valor
		parameters.put("groupList", gvcList.getGroupList());
		parameters.put("idSecurityCodePk", idSecurityCodePk);
		parameters.put("securityGvcState", SecurityGvcStateType.ENABLED.getCode());
		
		Long count = (Long)findObjectByQueryString(sbSql.toString(), parameters);
		if(count > 0){
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * Gets the parameter details based on the master key and short integer.
	 *
	 * @param masterpk the Integer type
	 * @param shortinteger the Integer type
	 * @return the List<ParameterTable>
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterSecurityClassService(Integer masterpk,Integer shortinteger) throws ServiceException{
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("Select P from  ");
		sbSql.append("ParameterTable P ");		
		sbSql.append("Where 1 = 1 ");
		sbSql.append("And P.masterTable.masterTablePk = :masterTableFk ");
		sbSql.append("And P.shortInteger = :shortInt ");
		sbSql.append("Order by P.parameterName ");
		parameters.put("masterTableFk", masterpk);
		parameters.put("shortInt", shortinteger);	
		return	(List<ParameterTable>)findListByQueryString(sbSql.toString(), parameters);		
	}

	/**
	 * Gets the gvc group by parameters.
	 *
	 * @param gvcList the gvc list
	 * @return the gvc group by parameters
	 */
	public GvcList getGvcGroupByParameters(GvcList gvcList) {
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("select tl ");
		sbSql.append("from GvcList tl ");		
		sbSql.append("where tl.mechanismModality.id.idNegotiationMechanismPk = :NegMechFk  ");
		sbSql.append("and tl.mechanismModality.id.idNegotiationModalityPk = :NegModFk  ");
		sbSql.append("and tl.securityClass = :secClass  ");	
		sbSql.append("and tl.groupList = :groupList  ");	
			
		parameters.put("NegMechFk", gvcList.getMechanismModality().getId().getIdNegotiationMechanismPk());
		parameters.put("NegModFk", gvcList.getMechanismModality().getId().getIdNegotiationModalityPk());
		parameters.put("secClass", gvcList.getSecurityClass());
		parameters.put("groupList", gvcList.getGroupList());
		
		return	(GvcList)findObjectByQueryString(sbSql.toString(), parameters);
	}
	
	/**
	 * Gets the gvc group by id.
	 *
	 * @param idGroupPk the id group pk
	 * @return the gvc group by id
	 */
	public GvcList getGvcGroupById(Long idGroupPk) {
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("select tl ");
		sbSql.append("from GvcList tl inner join fetch tl.mechanismModality mm ");
		sbSql.append(" inner join fetch mm.negotiationMechanism nme  ");
		sbSql.append(" inner join fetch mm.negotiationModality nmo ");
		sbSql.append("where  ");
		sbSql.append(" tl.idGroupPk = :idGroupPk  ");	
		parameters.put("idGroupPk",idGroupPk);
		
		return	(GvcList)findObjectByQueryString(sbSql.toString(), parameters);
	}

	/**
	 * Gets the gvc details.
	 *
	 * @param gvcList the gvc list
	 * @return the gvc details
	 */
	@SuppressWarnings("unchecked")
	public List<Security> getGvcDetails(GvcList gvcList) {
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("select new Security(sec.idSecurityCodePk,sec.alternativeCode,sec.cfiCode,sec.description, ");
		sbSql.append(" sec.mnemonic, sec.securityType, sec.stateSecurity, sec.currency , sec.initialNominalValue, sec.currentNominalValue) ");
		sbSql.append("from SecurityGvcList sgl inner join sgl.security sec ");		
		sbSql.append("where sgl.gvcList.idGroupPk = :idGroupPk  ");
		sbSql.append("and sgl.securityGvcState = :securityGvcState  ");
		
		parameters.put("idGroupPk", gvcList.getIdGroupPk());
		parameters.put("securityGvcState", SecurityGvcStateType.ENABLED.getCode());
		
		return findListByQueryString(sbSql.toString(), parameters);
	}
	
	/**
	 * Gets the gvc data by params.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idSecurityCodePk the id security code pk
	 * @return the gvc data by params
	 */
	//must return unique result
	public GvcList getGvcDataByParams(Long idMechanism, Long idModality, String idSecurityCodePk){
		StringBuffer sbSql = new StringBuffer();
		Map<String,Object> parameters = new HashMap<String, Object>();	
		sbSql.append("select gvc ");
		sbSql.append("from SecurityGvcList gvl inner join gvl.gvcList gvc ");		
		sbSql.append("where  ");
		sbSql.append(" gvc.mechanismModality.id.idNegotiationMechanismPk = :NegMechFk  ");
		sbSql.append("and gvc.mechanismModality.id.idNegotiationModalityPk = :NegModFk  ");
//		sbSql.append("and gvc.securityClass = :secClass  ");	
//		sbSql.append("and gvc.groupList = :groupList  ");
		sbSql.append("and gvl.security.idSecurityCodePk = :idSecurityCodePk  ");
		sbSql.append(" and gvl.securityGvcState = :securityGvcState  ");
			
		parameters.put("NegMechFk",idMechanism);
		parameters.put("NegModFk", idModality);
//		parameters.put("secClass", gvcList.getSecurityClass());
//		parameters.put("groupList", gvcList.getGroupList());
		parameters.put("idSecurityCodePk", idSecurityCodePk);
		parameters.put("securityGvcState", SecurityGvcStateType.ENABLED.getCode());
		
		try {
			return (GvcList)findObjectByQueryString(sbSql.toString(), parameters);
		} catch (NoResultException e) {
			logger.error("el valor no se encuentra en un grupo de castigo "+idSecurityCodePk);
		} catch (NonUniqueResultException e) {
			logger.error("el valor se encuentra en mas de un grupo de castigo"+idSecurityCodePk);
		}
		return null;
	}
	
}


