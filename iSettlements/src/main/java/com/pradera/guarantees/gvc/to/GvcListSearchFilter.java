package com.pradera.guarantees.gvc.to;

import java.io.Serializable;

import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class TvrListSearchFilter.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2013
 */
public class GvcListSearchFilter implements Serializable{

	/** Added Default serial UID. */
	private static final long serialVersionUID = 1L;
	
	/** Group Name. */
	private String groupDescription;	
	
	/** Security Class. */
	private Integer securityClass;	
	
	/** Negotiation Mechanism. */
	private Long idNegotiationMechFk;	
	
	/** Negotiation Modality. */
	private Long idNegotiationModFk;
	
	/** Security Code. */
	//private String idIsinCodePk;
	
	private String idSecurityCodePk;
	
	/** Securities List. */
	private Integer securityList;	
	
	/** Group Pk. */
	private Integer idGroupPk;	
	
	/** The security filter. */
	private Security securityFilter;
	
	/** The group list. */
	private Integer groupList;
	/**
	 * Gets the security class.
	 *
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the id negotiation mech fk.
	 *
	 * @return the idNegotiationMechFk
	 */
	public Long getIdNegotiationMechFk() {
		return idNegotiationMechFk;
	}
	
	/**
	 * Sets the id negotiation mech fk.
	 *
	 * @param idNegotiationMechFk the idNegotiationMechFk to set
	 */
	public void setIdNegotiationMechFk(Long idNegotiationMechFk) {
		this.idNegotiationMechFk = idNegotiationMechFk;
	}
	
	/**
	 * Gets the id negotiation mod fk.
	 *
	 * @return the idNegotiationModFk
	 */
	public Long getIdNegotiationModFk() {
		return idNegotiationModFk;
	}
	
	/**
	 * Sets the id negotiation mod fk.
	 *
	 * @param idNegotiationModFk the idNegotiationModFk to set
	 */
	public void setIdNegotiationModFk(Long idNegotiationModFk) {
		this.idNegotiationModFk = idNegotiationModFk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the idIsinCodePk
	 */
//	public String getIdIsinCodePk() {
//		return idIsinCodePk;
//	}
	/**
	 * @param idIsinCodePk the idIsinCodePk to set
	 */
//	public void setIdIsinCodePk(String idIsinCodePk) {
//		this.idIsinCodePk = idIsinCodePk;
//	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the security list.
	 *
	 * @return the securityList
	 */
	public Integer getSecurityList() {
		return securityList;
	}
	
	/**
	 * Sets the security list.
	 *
	 * @param securityList the securityList to set
	 */
	public void setSecurityList(Integer securityList) {
		this.securityList = securityList;
	}
	
	/**
	 * Gets the id group pk.
	 *
	 * @return the idGroupPk
	 */
	public Integer getIdGroupPk() {
		return idGroupPk;
	}
	
	/**
	 * Sets the id group pk.
	 *
	 * @param idGroupPk the idGroupPk to set
	 */
	public void setIdGroupPk(Integer idGroupPk) {
		this.idGroupPk = idGroupPk;
	}
	
	/**
	 * Gets the group description.
	 *
	 * @return the groupDescription
	 */
	public String getGroupDescription() {
		return groupDescription;
	}
	
	/**
	 * Sets the group description.
	 *
	 * @param groupDescription the groupDescription to set
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	
	/**
	 * Gets the security filter.
	 *
	 * @return the security filter
	 */
	public Security getSecurityFilter() {
		return securityFilter;
	}
	
	/**
	 * Sets the security filter.
	 *
	 * @param securityFilter the new security filter
	 */
	public void setSecurityFilter(Security securityFilter) {
		this.securityFilter = securityFilter;
	}

	/**
	 * Gets the group list.
	 *
	 * @return the group list
	 */
	public Integer getGroupList() {
		return groupList;
	}

	/**
	 * Sets the group list.
	 *
	 * @param groupList the new group list
	 */
	public void setGroupList(Integer groupList) {
		this.groupList = groupList;
	}

}
