package com.pradera.guarantees.gvc.facade;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.guarantees.gvc.service.GvcListServiceBean;
import com.pradera.guarantees.gvc.to.GvcGroupTO;
import com.pradera.guarantees.gvc.to.GvcListSearchFilter;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.guarantees.GvcList;
import com.pradera.model.guarantees.SecurityGvcList;
import com.pradera.model.guarantees.type.SecurityGvcStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.settlements.utils.PropertiesConstants;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class ModalityMechanismFacade
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 27/03/2013
 */

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class GvcListServiceFacade {
	

	/** The Transaction synchronization Registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	

	/** The mechanism service bean. */
	@EJB
	private GvcListServiceBean mechanismServiceBean;

	/**
	 * This method is used to get the values from the database to filled the
	 * list values into Modality combo box based on user selected value from
	 * the Mechanism combo box.
	 *
	 * @param idNegMechanism the id neg mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> searchModalityMechanism(Long idNegMechanism) throws ServiceException {
		return mechanismServiceBean.searchModalityMechanismValues(idNegMechanism);

	}

	/**
	 * This method is used to insert the data into TvrList table,update the
	 * data in Security table and get the data from data base and displayed in
	 * the data table.
	 *
	 * @param gvcList the gvc list
	 * @param removeSecurities the remove securities
	 * @param addSecurities the add securities
	 * @param isUpdate the is update
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String saveTvrListSecurityGroup(GvcList gvcList,List<Security> removeSecurities,List<Security> addSecurities, boolean isUpdate) throws ServiceException {
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		 if(loggerUser.getUserAction().isRegister()){
			 loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}else{
	   		//SETTING THE DEFAULT VALUE AS 1 FOR idPrivilegeOfSystem IF IT IS NULL
    		 loggerUser.setIdPrivilegeOfSystem(GeneralConstants.ONE_VALUE_INTEGER);
     	}
		
        boolean addMsg = false;
        boolean deleteMsg = false;
		
		String strTemp = PropertiesConstants.MSG_GVC_PUNISHMENT_FACTOR_UPDATE_SUCCESS;
		
		if(gvcList.getIdGroupPk() == null){
			mechanismServiceBean.create(gvcList);
			
		}else if (isUpdate){
			mechanismServiceBean.update(gvcList);
			
			if(Validations.validateIsNotNull(removeSecurities)){
				 for (Security security : removeSecurities) {
				 	mechanismServiceBean.updateSecurityGroupValue(gvcList.getIdGroupPk() , security.getIdSecurityCodePk(), loggerUser);
				 	deleteMsg = true;
				}
			 }
		}
		
		if(Validations.validateIsNotNull(addSecurities)){
			 for (Security security : addSecurities) {
			 	 SecurityGvcList secList = new SecurityGvcList();
			 	 secList.setGvcList(gvcList);
			 	 secList.setSecurity(security);
			 	 secList.setSecurityGvcState(SecurityGvcStateType.ENABLED.getCode());
				 mechanismServiceBean.create(secList);
				 addMsg = true;
			}
		}
		
		//Exclude the security from group related message
		 if(deleteMsg){
			 strTemp = PropertiesConstants.MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_REMOVE;
			//Incorporate the security to group related message
		 }else  if(addMsg){
			 strTemp = PropertiesConstants.MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_INSERT;
		 }
		//Exclude  and Incorporate the security related message
		 if(deleteMsg && addMsg){
			 strTemp = PropertiesConstants.MSG_GVC_PUNISHMENT_FACTOR_SUCCESS_ALL;
		 }
		 return strTemp;
		
		
	}	
	
	/**
	 * Get search result based the search criteria.
	 *
	 * @param searchFilterData the TvrListSearchFilter
	 * @return the List<ReferenceValue>
	 * @throws ServiceException the service exception
	 */
	public List<GvcGroupTO> searchResultTvrFacade(GvcListSearchFilter searchFilterData) throws ServiceException{
		List<GvcGroupTO> searchResults= mechanismServiceBean.searchResultGvc(searchFilterData);
		return searchResults;
		}

	/**
	 * Verify the Punishment factor already register to some other group.
	 *
	 * @param gvcList the gvc list
	 * @param modifyPunishmentFactor the TvrList type
	 * @return the boolean type
	 * @throws ServiceException the service exception
	 */
	public boolean verifyPunishmentFactorFacade(GvcList gvcList, BigDecimal modifyPunishmentFactor)throws ServiceException{
		return mechanismServiceBean.verifyPunishmentFactorService(gvcList,modifyPunishmentFactor);	
	}
	
	
	
	/**
	 * Get the selected id related Security details.
	 *
	 * @param isindCodePk the String type
	 * @return the Security type
	 * @throws ServiceException the service exception
	 */
	public Security getSecurity(String isindCodePk) throws ServiceException{
		return  mechanismServiceBean.find(Security.class, isindCodePk);
	}
	
	/**
	 * Gets the parameter details based on the master key and short integer.
	 *
	 * @param masterpk the Integer type
	 * @param shortinteger the Integer type
	 * @return the List<ParameterTable>
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterSecurityClas(Integer masterpk,Integer shortinteger) throws ServiceException{
		return  mechanismServiceBean.getParameterSecurityClassService(masterpk, shortinteger);
	}

	/**
	 * Verify instrument type security modality.
	 *
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 * @param instrumentType the instrument type
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean verifyInstrumentTypeSecurityModality(
			Long idNegotiationModalityPk, Integer instrumentType) throws ServiceException {
		return mechanismServiceBean.verifyInstrumentTypeSecurityModality(idNegotiationModalityPk, instrumentType);
	}

	/**
	 * Gets the gvc group by parameters.
	 *
	 * @param gvcList the gvc list
	 * @return the gvc group by parameters
	 */
	public GvcList getGvcGroupByParameters(GvcList gvcList) {
		return mechanismServiceBean.getGvcGroupByParameters(gvcList);
	}

	/**
	 * Gets the gvc details.
	 *
	 * @param gvcList the gvc list
	 * @return the gvc details
	 */
	public List<Security> getGvcDetails(GvcList gvcList) {
		return mechanismServiceBean.getGvcDetails(gvcList);
	}

	/**
	 * Gets the gvc group by id.
	 *
	 * @param idGroupPk the id group pk
	 * @return the gvc group by id
	 */
	public GvcList getGvcGroupById(Long idGroupPk) {
	    return mechanismServiceBean.getGvcGroupById(idGroupPk);
	}

	/**
	 * Verify security already group.
	 *
	 * @param gvcList the gvc list
	 * @param idSecurityCodePk the id security code pk
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean verifySecurityAlreadyGroup(GvcList gvcList,String idSecurityCodePk) throws ServiceException {
		return mechanismServiceBean.verifySecurityAlreadyGroup(gvcList,idSecurityCodePk);
	}
}
