package com.pradera.guarantees.management.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.guarantees.management.facade.GuaranteeOperationsServiceFacade;
import com.pradera.guarantees.management.to.GuaranteesFilter;
import com.pradera.guarantees.management.to.GuaranteesFilterByMech;
import com.pradera.guarantees.management.to.GuaranteesFilterByOper;
import com.pradera.guarantees.management.to.GuaranteesRegisterTO;
import com.pradera.guarantees.management.to.GuaranteesResultTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.guarantees.GuaranteeMarketFactOperation;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeDateType;
import com.pradera.model.guarantees.type.GuaranteeRegistrationType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.McnCancelReasonType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class GuaranteesManagementBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/06/2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class GuaranteesManagementBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The guarantees service facade. */
	@EJB
	GuaranteeOperationsServiceFacade guaranteesServiceFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst guarantee registry type. */
	private List<ParameterTable> lstGuaranteeRegistryType;
	
	/** The guarantees filter. */
	private GuaranteesFilter guaranteesFilter;
	
	/** The selected by participant. */
	private GuaranteesFilter selectedByParticipant;
	
	/** The selected by mechanism. */
	private GuaranteesFilterByMech selectedByMechanism;
	
	/** The is initial guarantees. */
	private boolean isInitialGuarantees;
	
	/** The is reposition guarantees. */
	private boolean isRepositionGuarantees;
	
	/** The is aditional guarantees. */
	private boolean isAditionalGuarantees;
	
	/** The is withdrawal guarantees. */
	private boolean isWithdrawalGuarantees;
	
	/** The selected account guarantees. */
	private GuaranteesResultTO selectedAccountGuarantees;
	
	/** The guarantees per part data model. */
	private GenericDataModel<GuaranteesFilter> guaranteesPerPartDataModel;
	
	/** The guarantees per mech data model. */
	private GenericDataModel<GuaranteesFilterByMech> guaranteesPerMechDataModel;
	
	/** The guarantees per op data model. */
	private GenericDataModel<GuaranteesFilterByOper> guaranteesPerOpDataModel;
	
	/** The guarantees per acc data model. */
	private GenericDataModel<GuaranteesResultTO> guaranteesPerAccDataModel;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;

	/** The operation curreny. */
	private ParameterTable operationCurreny;
	
	/** The settlement type. */
	private ParameterTable settlementType;
	
	/** The settlement schema. */
	private ParameterTable settlementSchema;
	
	/** The list guarantee date type. */
	private List<ParameterTable> listGuaranteeDateType;
	
	/** The is participant user. */
	private boolean isParticipantUser;
	
	/** The participant service bean. */
	@Inject
	private ParticipantServiceBean participantServiceBean;
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		guaranteesFilter = new GuaranteesFilter();
		guaranteesFilter.setSettlementDate(getCurrentSystemDate());
		
		lstGuaranteeRegistryType =  new ArrayList<ParameterTable>();
		for(GuaranteeRegistrationType type : GuaranteeRegistrationType.list){
			ParameterTable parameterTable = new ParameterTable(type.getCode(), type.getDescription());
			lstGuaranteeRegistryType.add(parameterTable);
		}
		
		listGuaranteeDateType = new ArrayList<ParameterTable>();
		for(GuaranteeDateType type : GuaranteeDateType.list){
			ParameterTable parameterTable = new ParameterTable(type.getCode(), type.getDescription());
			listGuaranteeDateType.add(parameterTable);
		}		
		
		 if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			 Participant participantFilter = new Participant();
			 participantFilter.setState(ParticipantStateType.REGISTERED.getCode());
			 
		 	try {
				lstParticipants = participantServiceBean.getLisParticipantServiceBean(participantFilter);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			 
		 }else if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			 Participant participant = guaranteesServiceFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
			 if(participant != null){
				 lstParticipants.add(participant);
			 }			 
		 }
		
		
	}

	/**
	 * Clean search page.
	 */
	public void cleanSearchPage(){
		hideDialogs();
		isInitialGuarantees = false;
		isAditionalGuarantees = false;
		isRepositionGuarantees = false;
		isWithdrawalGuarantees = false;
		guaranteesFilter = new GuaranteesFilter();
		guaranteesFilter.setSettlementDate(getCurrentSystemDate());
		guaranteesPerPartDataModel = null;
		guaranteesPerMechDataModel = null;
		guaranteesPerOpDataModel = null;
		
		if(getViewOperationType() != null && isViewOperationConsult()){
			JSFUtilities.resetComponent("frmGuaranteesMonitoring:idRegistryType");
			JSFUtilities.resetComponent("frmGuaranteesMonitoring:idParticipant");
			JSFUtilities.resetComponent("frmGuaranteesMonitoring:calTermSettlementDate");
		}
	}
	
	/**
	 * Search guarantees status.
	 */
	public void searchGuaranteesStatus(){
		isInitialGuarantees = false;
		isRepositionGuarantees = false;
		isAditionalGuarantees = false;
		isWithdrawalGuarantees = false;
		List<GuaranteesFilter> parts = new ArrayList<GuaranteesFilter>(0);
		if(guaranteesFilter.getGuaranteesRegType().equals(GuaranteeRegistrationType.INITIAL_GUARANTEES.getCode())){
			parts = guaranteesServiceFacade.getPendingOperationsDetails(guaranteesFilter);
			isInitialGuarantees = true;
		}else {
			if(guaranteesFilter.getGuaranteesRegType().equals(GuaranteeRegistrationType.GUARANTEES_REPOSITION.getCode())){
				isRepositionGuarantees = true;
			}else if(guaranteesFilter.getGuaranteesRegType().equals(GuaranteeRegistrationType.ADITIONAL_GUARANTEES.getCode())){
				isAditionalGuarantees = true;
			}else if(guaranteesFilter.getGuaranteesRegType().equals(GuaranteeRegistrationType.GUARANTEES_LIBERATION.getCode())){
				isWithdrawalGuarantees = true;
			}
			parts = guaranteesServiceFacade.getPendingOperationsAfterValorization(guaranteesFilter);
		}
		
		guaranteesPerPartDataModel = new GenericDataModel<GuaranteesFilter>(parts);
		guaranteesPerMechDataModel = null;
		guaranteesPerOpDataModel = null;
	}
	
	/**
	 * Search guarantees per mechanism.
	 *
	 * @param resultByParticipant the result by participant
	 */
	public void searchGuaranteesPerMechanism(GuaranteesFilter resultByParticipant){
		//List<GuaranteesFilterByMech> mechMod = guaranteesServiceFacade.getPendingOperationsDetailsByMech(resultByParticipant);
		selectedByParticipant = resultByParticipant;
		guaranteesPerMechDataModel = new GenericDataModel<GuaranteesFilterByMech>(selectedByParticipant.getListByMech());
		guaranteesPerOpDataModel = null;
	}
	
	/**
	 * Search guarantees per operation.
	 *
	 * @param resultByMechanism the result by mechanism
	 */
	public void searchGuaranteesPerOperation(GuaranteesFilterByMech resultByMechanism){
		//List<GuaranteesResultTO> parts = guaranteesServiceFacade.getPendingOperationsByPk(resultByMechanism.getIdMechanismOperationsPk());
		selectedByMechanism = resultByMechanism;
		if(isInitialGuarantees || isRepositionGuarantees){
			guaranteesPerOpDataModel = new GenericDataModel<GuaranteesFilterByOper>(selectedByMechanism.getListByOperations());
		}
		if(isAditionalGuarantees || isWithdrawalGuarantees){
			guaranteesPerOpDataModel = new GenericDataModel<GuaranteesFilterByOper>(selectedByMechanism.getCoveredListByOperations());
		}
		
	}
	
	/**
	 * Go to guarantees mgmt.
	 *
	 * @param resultByOperation the result by operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String goToGuaranteesMgmt(GuaranteesFilterByOper resultByOperation) throws ServiceException{
		//selectedMechanismOperation = resultByOperation;
		List<GuaranteesResultTO> parts = guaranteesServiceFacade.getPendingAccountsByOp(resultByOperation,isInitialGuarantees,isRepositionGuarantees,isAditionalGuarantees);
		guaranteesPerAccDataModel = new GenericDataModel<GuaranteesResultTO>(parts);
		
		mechanismOperation = guaranteesServiceFacade.getMechanismOperationDetails(resultByOperation.getIdMechanismOperationPk());
		
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getCurrency())){
			mechanismOperation.setCurrencyDescription(CurrencyType.lookup.get(mechanismOperation.getCurrency()).getValue());
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperation.getSettlementType())){
			settlementType = new ParameterTable(mechanismOperation.getSettlementType(),SettlementType.lookup.get(mechanismOperation.getSettlementType()).getValue());
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperation.getSettlementSchema())){
			settlementSchema = new ParameterTable(mechanismOperation.getSettlementSchema(),SettlementSchemaType.lookup.get(mechanismOperation.getSettlementSchema()).getValue());
		}
		
		if(Validations.validateIsNotNullAndPositive(mechanismOperation.getCurrency())){
			operationCurreny = new ParameterTable(mechanismOperation.getCurrency(),mechanismOperation.getCurrencyDescription());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getOperationState())){
			mechanismOperation.setStateDescription(MechanismOperationStateType.lookup.get(mechanismOperation.getOperationState()).getValue());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getCancelMotive())){
			mechanismOperation.setCancelDescription(McnCancelReasonType.lookup.get(mechanismOperation.getCancelMotive()).getDescripcion());
		}
		
		
		return "operationView";
	}
	
	/**
	 * Go to guarantees deposit.
	 *
	 * @param guaranteesResultTO the guarantees result to
	 * @return the string
	 */
	public String goToGuaranteesDeposit(GuaranteesResultTO guaranteesResultTO){
		
		guaranteesResultTO.setLstGuaranteesInFunds(null);
		guaranteesResultTO.setLstGuaranteesInSecurities(null);
		
		selectedAccountGuarantees = guaranteesServiceFacade.getGuaranteesRegistrationLists(
				mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk(),
				mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk(),
				guaranteesResultTO);
		selectedAccountGuarantees.setAccMarginAmountSum(BigDecimal.ZERO);
		
		if(guaranteesResultTO.getLstGuaranteesInFunds() == null && guaranteesResultTO.getLstGuaranteesInSecurities() ==null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_NO_FUNDS_NOR_SECURITIES_BALANCES));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		return "registerView";
	}
	
	/**
	 * Go to guarantees withdrawal.
	 *
	 * @param guaranteesResultTO the guarantees result to
	 * @return the string
	 */
	public String goToGuaranteesWithdrawal(GuaranteesResultTO guaranteesResultTO){
		
		guaranteesResultTO.setLstGuaranteesInFunds(null);
		guaranteesResultTO.setLstGuaranteesInSecurities(null);
		
		selectedAccountGuarantees = guaranteesServiceFacade.getGuaranteeBalances(
				mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk(),
				mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk(),
				guaranteesResultTO);
		selectedAccountGuarantees.setAccMarginAmountSum(BigDecimal.ZERO);
		
		return "withdrawalView";
	}
	
	/**
	 * On change amount to use.
	 *
	 * @param selDetail the sel detail
	 */
	public void onChangeAmountToUse(GuaranteesRegisterTO selDetail){
		hideDialogs();
		
		//restar ultimo monto usado
		if(Validations.validateIsNotNull(selDetail.getLastAmountToUse())){
			selectedAccountGuarantees.setAccMarginAmountSum(selectedAccountGuarantees.getAccMarginAmountSum().subtract(selDetail.getLastAmountToUse()));
		}
		
		//si el monto ingresado es distinto a blanco
		if(Validations.validateIsNotNull(selDetail.getAmountToUse())){
			if(selDetail.getAmountToUse().compareTo(selDetail.getCashAccountAvailableAmount()) > 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_CUMULATIVE_GREATER_AVAILABLE_AMO));
				selDetail.setAmountToUse(null);
				selDetail.setLastAmountToUse(null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			selectedAccountGuarantees.setAccMarginAmountSum(selectedAccountGuarantees.getAccMarginAmountSum().add(selDetail.getAmountToUse()));
		}
		
		selDetail.setLastAmountToUse(selDetail.getAmountToUse());
	}	
	
	/**
	 * On change select funds row.
	 *
	 * @param selDetail the sel detail
	 */
	public void onChangeSelectFundsRow(GuaranteesRegisterTO selDetail){
		for(GuaranteesRegisterTO temp : selectedAccountGuarantees.getLstGuaranteesInFunds()){
			//if(!selDetail.getIdInstitutionCashAccountPk().equals(selDetail.getIdInstitutionCashAccountPk())){
			//if(!temp.isSelected()){
				if(temp.getLastAmountToUse() != null){
					selectedAccountGuarantees.setAccMarginAmountSum(selectedAccountGuarantees.getAccMarginAmountSum().subtract(temp.getLastAmountToUse()));
				}
				temp.setLastAmountToUse(null);
				temp.setAmountToUse(null);
				if(!selDetail.getIdInstitutionCashAccountPk().equals(temp.getIdInstitutionCashAccountPk())){
					temp.setSelected(false);
				}
			//}
			//}
//			if(!temp.isSelected()){
//				
//			}
		}
		
	}
	
	/**
	 * On change balance to use.
	 *
	 * @param selDetail the sel detail
	 */
	public void onChangeBalanceToUse(GuaranteesRegisterTO selDetail){
		hideDialogs();
		
		//restar ultimo monto usado
		if(Validations.validateIsNotNull(selDetail.getLastValuedAmount())){
			selectedAccountGuarantees.setAccMarginAmountSum(selectedAccountGuarantees.getAccMarginAmountSum().subtract(selDetail.getLastValuedAmount()));
		}
		selDetail.setLastValuedAmount(null);
		
		if(Validations.validateIsNotNull(selDetail.getBalanceToUse())){

			if(selDetail.getBalanceToUse().compareTo(selDetail.getAvailableBalance()) > 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_CUMULATIVE_GREATER_AVAILABLE_BAL));
				selDetail.setBalanceToUse(null);
				selDetail.setValuedAmount(null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			BigDecimal newValuedAmount = NegotiationUtils.calculateValuedAmount(selDetail.getBalanceToUse(),selDetail.getPunishmentFactor(),selDetail.getQuotationPrice()); 

			selDetail.setValuedAmount(newValuedAmount);
			selDetail.setLastValuedAmount(newValuedAmount);
			selectedAccountGuarantees.setAccMarginAmountSum(selectedAccountGuarantees.getAccMarginAmountSum().add(newValuedAmount));
		}
		
	}	
	
	/**
	 * On change select stock row.
	 *
	 * @param selDetail the sel detail
	 */
	public void onChangeSelectStockRow(GuaranteesRegisterTO selDetail){
		if(selDetail.isSelected()){
			
		}
		if(!selDetail.isSelected()){
			if(selDetail.getLastValuedAmount() != null){
				selectedAccountGuarantees.setAccMarginAmountSum(
						selectedAccountGuarantees.getAccMarginAmountSum().subtract(selDetail.getLastValuedAmount()));
			}
			selDetail.setLastValuedAmount(null);
			selDetail.setValuedAmount(null);
			selDetail.setBalanceToUse(null);
		}
	}
	
	/**
	 * Before save guarantee operations.
	 */
	public void beforeSaveGuaranteeOperations(){
		hideDialogs();
		if(selectedAccountGuarantees.getAccMarginAmountSum() == null ||
				selectedAccountGuarantees.getAccMarginAmountSum().compareTo(selectedAccountGuarantees.getAccMissingAmount()) < 0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_AMOUNT_SUM_ZERO));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		String confirmMessage = null;
		if(isInitialGuarantees){
			confirmMessage = PropertiesConstants.MSG_GUARANTEES_REGISTER_INITIAL_CONFIRM;
		}else if (isRepositionGuarantees){
			confirmMessage = PropertiesConstants.MSG_GUARANTEES_REGISTER_RECOVER_CONFIRM;
		}else if (isAditionalGuarantees){
			confirmMessage = PropertiesConstants.MSG_GUARANTEES_REGISTER_ADITIONAL_CONFIRM;
		}
		
		String accountNumber = selectedAccountGuarantees.getHolderAccount().getAlternateCode();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
				PropertiesUtilities.getMessage(confirmMessage,new Object[]{accountNumber}));
		JSFUtilities.showComponent("cnfDlgSave"); 
	}
	
	/**
	 * Save guarantee operations.
	 */
	@LoggerAuditWeb
	public void saveGuaranteeOperations(){
		hideDialogs();
		Long fundsOperationType = null; 
		Long guaranteeOperationType = null;
		String successMessage = null;
		if(isInitialGuarantees){
			successMessage = PropertiesConstants.MSG_GUARANTEES_REGISTER_INITIAL_SUCESS;
			selectedAccountGuarantees.setRegistrationType(GuaranteeRegistrationType.INITIAL_GUARANTEES.getCode());
			fundsOperationType = ParameterFundsOperationType.SHOW_INITIAL_GUARANTEE.getCode();
			guaranteeOperationType = ParameterOperationType.INITIAL_GUARANTEES_MARGIN_BLOCK.getCode();
		}else if (isRepositionGuarantees){
			successMessage = PropertiesConstants.MSG_GUARANTEES_REGISTER_RECOVER_SUCCESS;
			selectedAccountGuarantees.setRegistrationType(GuaranteeRegistrationType.GUARANTEES_REPOSITION.getCode());
			fundsOperationType = ParameterFundsOperationType.GUARANTEE_MARGIN_REPLENISHMENT.getCode();
			guaranteeOperationType = ParameterOperationType.REPLACEMENT_GUARANTEES_MARGIN_BLOCK.getCode();
		}else if (isAditionalGuarantees){
			successMessage = PropertiesConstants.MSG_GUARANTEES_REGISTER_ADITIONAL_SUCCESS;
			selectedAccountGuarantees.setRegistrationType(GuaranteeRegistrationType.ADITIONAL_GUARANTEES.getCode());
			fundsOperationType = ParameterFundsOperationType.SHOW_ADITIONAL_GUARANTEE.getCode();
			guaranteeOperationType = ParameterOperationType.ADITIONAL_GUARANTEES_MARGIN_BLOCK.getCode();
		}
		
		List<GuaranteeOperation> guaranteeOperations = generateGuaranteeOperations(fundsOperationType,guaranteeOperationType);
		
		try {
			
			List<GuaranteesResultTO> objGuarAccounts = guaranteesServiceFacade.saveGuaranteeOperations(selectedAccountGuarantees,guaranteeOperations);
			if(objGuarAccounts.size() > 0){
				guaranteesPerAccDataModel = new GenericDataModel<GuaranteesResultTO>(objGuarAccounts);
			}else{
				guaranteesPerAccDataModel = null;
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(successMessage,
							new Object[]{selectedAccountGuarantees.getHolderAccount().getAlternateCode()}));
			JSFUtilities.showComponent("cnfDlgBackToMgmt");
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
 
	/**
	 * Before save guarantees withdrawal.
	 */
	public void beforeSaveGuaranteesWithdrawal(){
		hideDialogs();
		
		if(selectedAccountGuarantees.getAccMarginAmountSum() == null ||
				selectedAccountGuarantees.getAccMarginAmountSum().compareTo(selectedAccountGuarantees.getAccSurplusAmount()) > 0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_AMOUNT_WITHDRAW_INVALID));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		String accountNumber = selectedAccountGuarantees.getHolderAccount().getAlternateCode();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_REGISTER_WITHDRAWAL_CONFIRM,new Object[]{accountNumber}));
		JSFUtilities.showComponent("cnfDlgSave"); 
	}
	
	/**
	 * Save guarantees withdrawal.
	 */
	@LoggerAuditWeb
	public void saveGuaranteesWithdrawal(){
		hideDialogs();
		
		selectedAccountGuarantees.setRegistrationType(GuaranteeRegistrationType.GUARANTEES_LIBERATION.getCode());
		Long fundsOperationType = ParameterFundsOperationType.GUARANTEE_LIBERATION.getCode();
		Long guaranteeOperationType = ParameterOperationType.GUARANTEES_MARGIN_UNBLOCK.getCode();
		
		List<GuaranteeOperation> guaranteeOperations = generateGuaranteeOperations(fundsOperationType,guaranteeOperationType);
		
		try {
			
			List<GuaranteesResultTO> objGuarAccounts = guaranteesServiceFacade.saveGuaranteeOperations(selectedAccountGuarantees,guaranteeOperations);
			
			if(objGuarAccounts.size() > 0){
				guaranteesPerAccDataModel = new GenericDataModel<GuaranteesResultTO>(objGuarAccounts);
			}else{
				guaranteesPerAccDataModel = null;
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_REGISTER_WITHDRAWAL_SUCCESS,
							new Object[]{selectedAccountGuarantees.getHolderAccount().getAlternateCode()}));
			JSFUtilities.showComponent("cnfDlgBackToMgmt");
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
		
	}

	/**
	 * Generate guarantee operations.
	 *
	 * @param fundsOperationType the funds operation type
	 * @param guaranteeOperationType the guarantee operation type
	 * @return the list
	 */
	private List<GuaranteeOperation> generateGuaranteeOperations(Long fundsOperationType, Long guaranteeOperationType) {
		
		List<GuaranteeOperation> guaranteeOperations = new ArrayList<GuaranteeOperation>(0);
		
		if(selectedAccountGuarantees.getLstGuaranteesInFunds()!=null){
			
			BigDecimal totalAmount = new BigDecimal(0);
			GuaranteeOperation guaranteeOperation = new GuaranteeOperation();
			guaranteeOperation.setFundsOperations(new ArrayList<FundsOperation>(0));
			for(GuaranteesRegisterTO detail : selectedAccountGuarantees.getLstGuaranteesInFunds()){
				if(detail.isSelected() && Validations.validateIsNotNull(detail.getAmountToUse())){
					FundsOperation fundsOperation = new FundsOperation();
					fundsOperation.setFundsOperationGroup(FundsOperationGroupType.GUARANTEE_FUND_OPERATION.getCode());
					fundsOperation.setFundsOperationType(fundsOperationType);
					fundsOperation.setGuaranteeOperation(guaranteeOperation);
					fundsOperation.setIndAutomatic(GeneralConstants.ZERO_VALUE_INTEGER);
					InstitutionCashAccount ins = guaranteesServiceFacade.getFindInstitutionCashAccount(detail.getIdInstitutionCashAccountPk());					
					fundsOperation.setInstitutionCashAccount(ins);					
					//InstitutionBankAccount bankAccount =  ins.getInstitutionBankAccounts().get(0); // unique bank account
					Bank providerBank = new Bank();
					providerBank.setIdBankPk(detail.getIdBankPk());
					fundsOperation.setBank(providerBank);
					fundsOperation.setBankAccountNumber(detail.getBankAccountNumber());
					fundsOperation.setCurrency(selectedAccountGuarantees.getCurrency());
					if(new Integer(fundsOperationType.toString()).equals(GuaranteeRegistrationType.GUARANTEES_LIBERATION.getCode())){
						fundsOperation.setFundsOperationClass(GeneralConstants.TWO_VALUE_INTEGER); // 1 deposito //2 retiro
					}else{
						fundsOperation.setFundsOperationClass(GeneralConstants.ONE_VALUE_INTEGER); // 1 deposito //2 retiro
					}
					
					fundsOperation.setIndCentralized(GeneralConstants.ZERO_VALUE_INTEGER);
					
					MechanismOperation mechanismOperation = new MechanismOperation(selectedAccountGuarantees.getIdMechanismOperationPk());
					fundsOperation.setMechanismOperation(mechanismOperation);
					fundsOperation.setOperationAmount(detail.getAmountToUse());
					fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
					fundsOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					fundsOperation.setRegistryDate(CommonsUtilities.currentDate());
					
					guaranteeOperation.getFundsOperations().add(fundsOperation);
					totalAmount = totalAmount.add(detail.getAmountToUse());
				}
			}
			if(totalAmount.compareTo(BigDecimal.ZERO) >0){
				guaranteeOperation.setCurrency(selectedAccountGuarantees.getCurrency());
				guaranteeOperation.setGuaranteeBalance(totalAmount);
				guaranteeOperation.setGuaranteeClass(GuaranteeClassType.FUNDS.getCode());
				guaranteeOperation.setGuaranteeType(GuaranteeType.MARGIN.getCode());
				guaranteeOperation.setHolderAccount(selectedAccountGuarantees.getHolderAccount());
				HolderAccountOperation holderAccountOperation = new HolderAccountOperation();
				holderAccountOperation.setIdHolderAccountOperationPk(selectedAccountGuarantees.getIdAccountOperationPk()); // parte plazo
				guaranteeOperation.setHolderAccountOperation(holderAccountOperation);
				guaranteeOperation.setOperationDate(CommonsUtilities.currentDateTime());
				guaranteeOperation.setOperationType(guaranteeOperationType);
				guaranteeOperation.setPendingAmount(BigDecimal.ZERO);
				guaranteeOperations.add(guaranteeOperation);
			}
		}
		
		if(selectedAccountGuarantees.getLstGuaranteesInSecurities()!=null){
			for(GuaranteesRegisterTO detail : selectedAccountGuarantees.getLstGuaranteesInSecurities()){
				if(detail.isSelected() && Validations.validateIsNotNull(detail.getBalanceToUse())){
					GuaranteeOperation guaranteeOperation = new GuaranteeOperation();
					//guaranteeOperation.setCurrency(selectedAccountGuarantees.getCurrency());
					guaranteeOperation.setGuaranteeBalance(detail.getBalanceToUse());
					guaranteeOperation.setGuaranteeClass(GuaranteeClassType.SECURITIES.getCode());
					guaranteeOperation.setGuaranteeType(GuaranteeType.MARGIN.getCode());
					guaranteeOperation.setHolderAccount(selectedAccountGuarantees.getHolderAccount());
					HolderAccountOperation holderAccountOperation = new HolderAccountOperation();
					holderAccountOperation.setMechanismOperation(new MechanismOperation(selectedAccountGuarantees.getIdMechanismOperationPk()));
					holderAccountOperation.setIdHolderAccountOperationPk(selectedAccountGuarantees.getIdAccountOperationPk()); // parte plazo
					guaranteeOperation.setHolderAccountOperation(holderAccountOperation);
					guaranteeOperation.setOperationDate(CommonsUtilities.currentDateTime());
					guaranteeOperation.setOperationType(guaranteeOperationType);
					guaranteeOperation.setParticipant(new Participant(selectedAccountGuarantees.getIdStockParticipantPk()));
					guaranteeOperation.setPendingAmount(BigDecimal.ZERO);
					Security security = new Security(detail.getIdSecurityCodePk());
					guaranteeOperation.setSecurities(security);
					//guaranteeOperation.setStockBalance(detail.getBalanceToUse());
					
					//MarketFacts
					if(detail.getGuaranteeMarketFactOperations() != null && detail.getGuaranteeMarketFactOperations().size() > 0){
						for(GuaranteeMarketFactOperation guaranteeMarketFactOperation : detail.getGuaranteeMarketFactOperations()){
							guaranteeMarketFactOperation.setGuaranteeOperation(guaranteeOperation);
						}
					}					
					guaranteeOperation.setGuaranteeMarketFactOperations(detail.getGuaranteeMarketFactOperations());
					
					guaranteeOperations.add(guaranteeOperation);
				}
			}
		}
		
		return guaranteeOperations;
	}
	
	/**
	 * Go back decider.
	 *
	 * @return the string
	 */
	public String goBackDecider(){
		JSFUtilities.hideComponent("cnfDlgBackToMgmt");
		if(guaranteesPerAccDataModel!=null){
			return "operationView";
		}else{
			cleanSearchPage();
			return "searchView";
		}
	}
	
	/**
	 * Do valorization process.
	 */
	@LoggerAuditWeb
	public void doValorizationProcess(){
		BusinessProcess process = new BusinessProcess(BusinessProcessType.VALORIZATION_PROCESS_EXECUTION.getCode(),"");
		Map<String, Object> parameters= new HashMap<String, Object>();
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), process, parameters);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Do coverage am calc.
	 */
	@LoggerAuditWeb
	public void doCoverageAmCalc(){
		BusinessProcess process = new BusinessProcess(BusinessProcessType.COVERAGE_AMOUNT_CALCULATION.getCode(),"");
		Map<String, Object> parameters= new HashMap<String, Object>();
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), process, parameters);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Change guarantee registry type.
	 */
	public void changeGuaranteeRegistryType(){
		if(Validations.validateIsNotNull(guaranteesFilter) &&
			Validations.validateIsNotNullAndPositive(guaranteesFilter.getGuaranteesRegType())){
			if(GuaranteeRegistrationType.INITIAL_GUARANTEES.getCode().equals(guaranteesFilter.getGuaranteesRegType())){
				guaranteesFilter.setDateType(GuaranteeDateType.CONTADO.getCode());
			}else{
				guaranteesFilter.setDateType(GuaranteeDateType.PLAZO.getCode());
			}
		}
	}
	
	/**
	 * Hide dialogs.
	 */
	private void hideDialogs() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("cnfDlgSave");
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param garantee the garantee
	 */
	public void showMarketFactUI(GuaranteesRegisterTO garantee){
		selectedAccountGuarantees.setSelectedGuarantee(garantee);
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		if(garantee.getIdSecurityCodePk() != null && garantee.getIdHolderAccountPk() != null && garantee.getIdParticipantPk() != null){
			
			marketFactBalance.setSecurityCodePk(garantee.getIdSecurityCodePk());
			marketFactBalance.setHolderAccountPk(garantee.getIdHolderAccountPk());
			marketFactBalance.setParticipantPk(garantee.getIdParticipantPk());
			marketFactBalance.setUiComponentName("balancemarket");
			
			marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());			
			
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
			
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_GUARANTEES_VALID_MARKETFACT));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		List<GuaranteeMarketFactOperation> guaranteeMarketFactOperations =  new ArrayList<GuaranteeMarketFactOperation>();
		
		BigDecimal totalStockQuantity = BigDecimal.ZERO;
		
		for(MarketFactDetailHelpTO marketFactDetailHelpTO : holderMarketFactBalance.getMarketFacBalances()){
			GuaranteeMarketFactOperation marketFact = new GuaranteeMarketFactOperation();
			
			marketFact.setMarketDate(marketFactDetailHelpTO.getMarketDate());
			marketFact.setMarketRate(marketFactDetailHelpTO.getMarketRate());
			marketFact.setMarketPrice(marketFactDetailHelpTO.getMarketPrice());
			marketFact.setQuantity(marketFactDetailHelpTO.getEnteredBalance());
			
			totalStockQuantity = totalStockQuantity.add(marketFactDetailHelpTO.getEnteredBalance());
			guaranteeMarketFactOperations.add(marketFact);
			
		}
		
		selectedAccountGuarantees.getSelectedGuarantee().setGuaranteeMarketFactOperations(guaranteeMarketFactOperations);
		selectedAccountGuarantees.getSelectedGuarantee().setBalanceToUse(totalStockQuantity);		
		onChangeBalanceToUse(selectedAccountGuarantees.getSelectedGuarantee());
		selectedAccountGuarantees.setSelectedGuarantee(null);
		
		JSFUtilities.updateComponent("frmGuaranteesRegister:fldsDataStock");
		JSFUtilities.updateComponent("frmGuaranteesRegister:fldsSummary");
		JSFUtilities.updateComponent("opnlGenericDialogs");
	}

	/**
	 * Gets the guarantees filter.
	 *
	 * @return the guaranteesFilter
	 */
	public GuaranteesFilter getGuaranteesFilter() {
		return guaranteesFilter;
	}

	/**
	 * Sets the guarantees filter.
	 *
	 * @param guaranteesFilter the guaranteesFilter to set
	 */
	public void setGuaranteesFilter(GuaranteesFilter guaranteesFilter) {
		this.guaranteesFilter = guaranteesFilter;
	}
	
	/**
	 * Gets the guarantees per part data model.
	 *
	 * @return the guaranteesPerPartDataModel
	 */
	public GenericDataModel<GuaranteesFilter> getGuaranteesPerPartDataModel() {
		return guaranteesPerPartDataModel;
	}

	/**
	 * Sets the guarantees per part data model.
	 *
	 * @param guaranteesPerPartDataModel the guaranteesPerPartDataModel to set
	 */
	public void setGuaranteesPerPartDataModel(
			GenericDataModel<GuaranteesFilter> guaranteesPerPartDataModel) {
		this.guaranteesPerPartDataModel = guaranteesPerPartDataModel;
	}


	/**
	 * Gets the guarantees per op data model.
	 *
	 * @return the guaranteesPerOpDataModel
	 */
	public GenericDataModel<GuaranteesFilterByOper> getGuaranteesPerOpDataModel() {
		return guaranteesPerOpDataModel;
	}

	/**
	 * Sets the guarantees per op data model.
	 *
	 * @param guaranteesPerOpDataModel the guaranteesPerOpDataModel to set
	 */
	public void setGuaranteesPerOpDataModel(
			GenericDataModel<GuaranteesFilterByOper> guaranteesPerOpDataModel) {
		this.guaranteesPerOpDataModel = guaranteesPerOpDataModel;
	}

	/**
	 * Gets the guarantees per acc data model.
	 *
	 * @return the guaranteesPerAccDataModel
	 */
	public GenericDataModel<GuaranteesResultTO> getGuaranteesPerAccDataModel() {
		return guaranteesPerAccDataModel;
	}

	/**
	 * Sets the guarantees per acc data model.
	 *
	 * @param guaranteesPerAccDataModel the guaranteesPerAccDataModel to set
	 */
	public void setGuaranteesPerAccDataModel(
			GenericDataModel<GuaranteesResultTO> guaranteesPerAccDataModel) {
		this.guaranteesPerAccDataModel = guaranteesPerAccDataModel;
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanismOperation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the mechanismOperation to set
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	/**
	 * Gets the selected account guarantees.
	 *
	 * @return the selectedAccountGuarantees
	 */
	public GuaranteesResultTO getSelectedAccountGuarantees() {
		return selectedAccountGuarantees;
	}

	/**
	 * Sets the selected account guarantees.
	 *
	 * @param selectedAccountGuarantees the selectedAccountGuarantees to set
	 */
	public void setSelectedAccountGuarantees(GuaranteesResultTO selectedAccountGuarantees) {
		this.selectedAccountGuarantees = selectedAccountGuarantees;
	}

	/**
	 * Gets the guarantees per mech data model.
	 *
	 * @return the guaranteesPerMechDataModel
	 */
	public GenericDataModel<GuaranteesFilterByMech> getGuaranteesPerMechDataModel() {
		return guaranteesPerMechDataModel;
	}

	/**
	 * Sets the guarantees per mech data model.
	 *
	 * @param guaranteesPerMechDataModel the guaranteesPerMechDataModel to set
	 */
	public void setGuaranteesPerMechDataModel(
			GenericDataModel<GuaranteesFilterByMech> guaranteesPerMechDataModel) {
		this.guaranteesPerMechDataModel = guaranteesPerMechDataModel;
	}

	/**
	 * Checks if is initial guarantees.
	 *
	 * @return the isInitialGuarantees
	 */
	public boolean isInitialGuarantees() {
		return isInitialGuarantees;
	}

	/**
	 * Sets the initial guarantees.
	 *
	 * @param isInitialGuarantees the isInitialGuarantees to set
	 */
	public void setInitialGuarantees(boolean isInitialGuarantees) {
		this.isInitialGuarantees = isInitialGuarantees;
	}

	/**
	 * Checks if is reposition guarantees.
	 *
	 * @return the isRepositionGuarantees
	 */
	public boolean isRepositionGuarantees() {
		return isRepositionGuarantees;
	}

	/**
	 * Sets the reposition guarantees.
	 *
	 * @param isRepositionGuarantees the isRepositionGuarantees to set
	 */
	public void setRepositionGuarantees(boolean isRepositionGuarantees) {
		this.isRepositionGuarantees = isRepositionGuarantees;
	}

	/**
	 * Checks if is aditional guarantees.
	 *
	 * @return the isAditionalGuarantees
	 */
	public boolean isAditionalGuarantees() {
		return isAditionalGuarantees;
	}

	/**
	 * Sets the aditional guarantees.
	 *
	 * @param isAditionalGuarantees the isAditionalGuarantees to set
	 */
	public void setAditionalGuarantees(boolean isAditionalGuarantees) {
		this.isAditionalGuarantees = isAditionalGuarantees;
	}

	/**
	 * Gets the selected by participant.
	 *
	 * @return the selectedByParticipant
	 */
	public GuaranteesFilter getSelectedByParticipant() {
		return selectedByParticipant;
	}

	/**
	 * Sets the selected by participant.
	 *
	 * @param selectedByParticipant the selectedByParticipant to set
	 */
	public void setSelectedByParticipant(GuaranteesFilter selectedByParticipant) {
		this.selectedByParticipant = selectedByParticipant;
	}

	/**
	 * Gets the selected by mechanism.
	 *
	 * @return the selectedByMechanism
	 */
	public GuaranteesFilterByMech getSelectedByMechanism() {
		return selectedByMechanism;
	}

	/**
	 * Sets the selected by mechanism.
	 *
	 * @param selectedByMechanism the selectedByMechanism to set
	 */
	public void setSelectedByMechanism(GuaranteesFilterByMech selectedByMechanism) {
		this.selectedByMechanism = selectedByMechanism;
	}

	/**
	 * Checks if is withdrawal guarantees.
	 *
	 * @return the isWithdrawalGuarantees
	 */
	public boolean isWithdrawalGuarantees() {
		return isWithdrawalGuarantees;
	}

	/**
	 * Sets the withdrawal guarantees.
	 *
	 * @param isWithdrawalGuarantees the isWithdrawalGuarantees to set
	 */
	public void setWithdrawalGuarantees(boolean isWithdrawalGuarantees) {
		this.isWithdrawalGuarantees = isWithdrawalGuarantees;
	}

	/**
	 * Gets the lst guarantee registry type.
	 *
	 * @return the lst guarantee registry type
	 */
	public List<ParameterTable> getLstGuaranteeRegistryType() {
		return lstGuaranteeRegistryType;
	}

	/**
	 * Sets the lst guarantee registry type.
	 *
	 * @param lstGuaranteeRegistryType the new lst guarantee registry type
	 */
	public void setLstGuaranteeRegistryType(
			List<ParameterTable> lstGuaranteeRegistryType) {
		this.lstGuaranteeRegistryType = lstGuaranteeRegistryType;
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	/**
	 * Gets the operation curreny.
	 *
	 * @return the operation curreny
	 */
	public ParameterTable getOperationCurreny() {
		return operationCurreny;
	}

	/**
	 * Sets the operation curreny.
	 *
	 * @param operationCurreny the new operation curreny
	 */
	public void setOperationCurreny(ParameterTable operationCurreny) {
		this.operationCurreny = operationCurreny;
	}

	/**
	 * Gets the settlement type.
	 *
	 * @return the settlement type
	 */
	public ParameterTable getSettlementType() {
		return settlementType;
	}

	/**
	 * Sets the settlement type.
	 *
	 * @param settlementType the new settlement type
	 */
	public void setSettlementType(ParameterTable settlementType) {
		this.settlementType = settlementType;
	}

	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public ParameterTable getSettlementSchema() {
		return settlementSchema;
	}

	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the new settlement schema
	 */
	public void setSettlementSchema(ParameterTable settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	/**
	 * Gets the list guarantee date type.
	 *
	 * @return the list guarantee date type
	 */
	public List<ParameterTable> getListGuaranteeDateType() {
		return listGuaranteeDateType;
	}

	/**
	 * Sets the list guarantee date type.
	 *
	 * @param listGuaranteeDateType the new list guarantee date type
	 */
	public void setListGuaranteeDateType(List<ParameterTable> listGuaranteeDateType) {
		this.listGuaranteeDateType = listGuaranteeDateType;
	}

	/**
	 * Checks if is participant user.
	 *
	 * @return true, if is participant user
	 */
	public boolean isParticipantUser() {
		return isParticipantUser;
	}

	/**
	 * Sets the participant user.
	 *
	 * @param isParticipantUser the new participant user
	 */
	public void setParticipantUser(boolean isParticipantUser) {
		this.isParticipantUser = isParticipantUser;
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}
	
	
}
