package com.pradera.guarantees.management.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.guarantees.management.facade.GuaranteeOperationsServiceFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValorizationProcessBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="ValorizationProcessBatch")
@RequestScoped
public class ValorizationProcessBatch implements JobExecution,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7654786370027894595L;

	/** The guarantees operations service facade. */
	@EJB
	private GuaranteeOperationsServiceFacade guaranteesOperationsServiceFacade;
	
//	@EJB
//	private ParameterServiceBean parameterServiceBean;
	
	/* (non-Javadoc)
 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
 */
@Override
	public void startJob(ProcessLogger processLogger) {
		
		try {
			
			guaranteesOperationsServiceFacade.calculateValorizations(null);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}