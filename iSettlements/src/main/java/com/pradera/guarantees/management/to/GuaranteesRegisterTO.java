package com.pradera.guarantees.management.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.GuaranteeMarketFactOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class GuaranteesRegisterTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/06/2014
 */
public class GuaranteesRegisterTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	//funds
	/** The id institution cash account pk. */
	private Long idInstitutionCashAccountPk;
	
	/** The id institution bank account pk. */
	private Long idInstitutionBankAccountPk;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The id bank pk. */
	private Long idBankPk;
	
	/** The cash account type. */
	private Integer cashAccountType;
	
	/** The cash account type description. */
	private String cashAccountTypeDescription;
	
	/** The cash account total amount. */
	private BigDecimal cashAccountTotalAmount;
	
	/** The cash account available amount. */
	private BigDecimal cashAccountAvailableAmount;
	
	/** The margin amount. */
	private BigDecimal marginAmount;
	
	/** The amount to use. */
	private BigDecimal amountToUse;
	
	/** The last amount to use. */
	private BigDecimal lastAmountToUse;
	
	/** The last valued amount. */
	private BigDecimal lastValuedAmount;
	
	// securities stock 
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The id isin code pk. */
	//private String idIsinCodePk;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The quotation price. */
	private BigDecimal quotationPrice;
	
	/** The balance to use. */
	private BigDecimal balanceToUse;
	
	/** The valued amount. */
	private BigDecimal valuedAmount;
	
	/** The punishment factor. */
	private BigDecimal punishmentFactor;
	
	
	
	// withdrawal
	/** The guarantees balance. */
	private AccountGuaranteeBalance guaranteesBalance;
	
//	private BigDecimal marginDividendsBalance;
//	private BigDecimal principalDividendsBalance;
//	private BigDecimal marginBalance;
	
	/** The selected. */
	private boolean selected;
	
	/** The guarantee market fact operations. */
	private List<GuaranteeMarketFactOperation> guaranteeMarketFactOperations;
	
	/**
	 * Gets the id institution bank account pk.
	 *
	 * @return the idInstitutionBankAccountPk
	 */
	public Long getIdInstitutionBankAccountPk() {
		return idInstitutionBankAccountPk;
	}
	
	/**
	 * Sets the id institution bank account pk.
	 *
	 * @param idInstitutionBankAccountPk the idInstitutionBankAccountPk to set
	 */
	public void setIdInstitutionBankAccountPk(Long idInstitutionBankAccountPk) {
		this.idInstitutionBankAccountPk = idInstitutionBankAccountPk;
	}
	
	/**
	 * Gets the bank account number.
	 *
	 * @return the bankAccountNumber
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	
	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the bankAccountNumber to set
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	
	/**
	 * Gets the id bank pk.
	 *
	 * @return the idBankPk
	 */
	public Long getIdBankPk() {
		return idBankPk;
	}
	
	/**
	 * Sets the id bank pk.
	 *
	 * @param idBankPk the idBankPk to set
	 */
	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}
	
	/**
	 * Gets the last valued amount.
	 *
	 * @return the lastValuedAmount
	 */
	public BigDecimal getLastValuedAmount() {
		return lastValuedAmount;
	}
	
	/**
	 * Sets the last valued amount.
	 *
	 * @param lastValuedAmount the lastValuedAmount to set
	 */
	public void setLastValuedAmount(BigDecimal lastValuedAmount) {
		this.lastValuedAmount = lastValuedAmount;
	}
	
	/**
	 * Gets the punishment factor.
	 *
	 * @return the punishmentFactor
	 */
	public BigDecimal getPunishmentFactor() {
		return punishmentFactor;
	}
	
	/**
	 * Sets the punishment factor.
	 *
	 * @param punishmentFactor the punishmentFactor to set
	 */
	public void setPunishmentFactor(BigDecimal punishmentFactor) {
		this.punishmentFactor = punishmentFactor;
	}
	
	/**
	 * Gets the cash account type.
	 *
	 * @return the cashAccountType
	 */
	public Integer getCashAccountType() {
		return cashAccountType;
	}
	
	/**
	 * Sets the cash account type.
	 *
	 * @param cashAccountType the cashAccountType to set
	 */
	public void setCashAccountType(Integer cashAccountType) {
		this.cashAccountType = cashAccountType;
	}
	
	/**
	 * Gets the cash account type description.
	 *
	 * @return the cashAccountTypeDescription
	 */
	public String getCashAccountTypeDescription() {
		return cashAccountTypeDescription;
	}
	
	/**
	 * Sets the cash account type description.
	 *
	 * @param cashAccountTypeDescription the cashAccountTypeDescription to set
	 */
	public void setCashAccountTypeDescription(String cashAccountTypeDescription) {
		this.cashAccountTypeDescription = cashAccountTypeDescription;
	}
	
	/**
	 * Gets the cash account total amount.
	 *
	 * @return the cashAccountTotalAmount
	 */
	public BigDecimal getCashAccountTotalAmount() {
		return cashAccountTotalAmount;
	}
	
	/**
	 * Sets the cash account total amount.
	 *
	 * @param cashAccountTotalAmount the cashAccountTotalAmount to set
	 */
	public void setCashAccountTotalAmount(BigDecimal cashAccountTotalAmount) {
		this.cashAccountTotalAmount = cashAccountTotalAmount;
	}
	
	/**
	 * Gets the cash account available amount.
	 *
	 * @return the cashAccountAvailableAmount
	 */
	public BigDecimal getCashAccountAvailableAmount() {
		return cashAccountAvailableAmount;
	}
	
	/**
	 * Sets the cash account available amount.
	 *
	 * @param cashAccountAvailableAmount the cashAccountAvailableAmount to set
	 */
	public void setCashAccountAvailableAmount(BigDecimal cashAccountAvailableAmount) {
		this.cashAccountAvailableAmount = cashAccountAvailableAmount;
	}
	
	/**
	 * Gets the margin amount.
	 *
	 * @return the marginAmount
	 */
	public BigDecimal getMarginAmount() {
		return marginAmount;
	}
	
	/**
	 * Sets the margin amount.
	 *
	 * @param marginAmount the marginAmount to set
	 */
	public void setMarginAmount(BigDecimal marginAmount) {
		this.marginAmount = marginAmount;
	}
	
	/**
	 * Gets the amount to use.
	 *
	 * @return the amountToUse
	 */
	public BigDecimal getAmountToUse() {
		return amountToUse;
	}
	
	/**
	 * Sets the amount to use.
	 *
	 * @param amountToUse the amountToUse to set
	 */
	public void setAmountToUse(BigDecimal amountToUse) {
		this.amountToUse = amountToUse;
	}
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the idHolderAccountPk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the idHolderAccountPk to set
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the accountNumber
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the id idSecurity code pk.
	 *
	 * @return the idSecurityCodePk
	 */

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the total balance.
	 *
	 * @return the totalBalance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	
	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the totalBalance to set
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	
	/**
	 * Gets the quotation price.
	 *
	 * @return the quotationPrice
	 */
	public BigDecimal getQuotationPrice() {
		return quotationPrice;
	}
	
	/**
	 * Sets the quotation price.
	 *
	 * @param quotationPrice the quotationPrice to set
	 */
	public void setQuotationPrice(BigDecimal quotationPrice) {
		this.quotationPrice = quotationPrice;
	}
	
	/**
	 * Gets the balance to use.
	 *
	 * @return the balanceToUse
	 */
	public BigDecimal getBalanceToUse() {
		return balanceToUse;
	}
	
	/**
	 * Sets the balance to use.
	 *
	 * @param balanceToUse the balanceToUse to set
	 */
	public void setBalanceToUse(BigDecimal balanceToUse) {
		this.balanceToUse = balanceToUse;
	}
	
	/**
	 * Gets the valued amount.
	 *
	 * @return the valuedAmount
	 */
	public BigDecimal getValuedAmount() {
		return valuedAmount;
	}
	
	/**
	 * Sets the valued amount.
	 *
	 * @param valuedAmount the valuedAmount to set
	 */
	public void setValuedAmount(BigDecimal valuedAmount) {
		this.valuedAmount = valuedAmount;
	}
	
	/**
	 * Gets the available balance.
	 *
	 * @return the availableBalance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	
	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the availableBalance to set
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	/**
	 * Gets the last amount to use.
	 *
	 * @return the lastAmountToUse
	 */
	public BigDecimal getLastAmountToUse() {
		return lastAmountToUse;
	}
	
	/**
	 * Sets the last amount to use.
	 *
	 * @param lastAmountToUse the lastAmountToUse to set
	 */
	public void setLastAmountToUse(BigDecimal lastAmountToUse) {
		this.lastAmountToUse = lastAmountToUse;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	/**
	 * Gets the id institution cash account pk.
	 *
	 * @return the idInstitutionCashAccountPk
	 */
	public Long getIdInstitutionCashAccountPk() {
		return idInstitutionCashAccountPk;
	}
	
	/**
	 * Sets the id institution cash account pk.
	 *
	 * @param idInstitutionCashAccountPk the idInstitutionCashAccountPk to set
	 */
	public void setIdInstitutionCashAccountPk(Long idInstitutionCashAccountPk) {
		this.idInstitutionCashAccountPk = idInstitutionCashAccountPk;
	}
	
	/**
	 * Gets the alternate code.
	 *
	 * @return the alternateCode
	 */
	public String getAlternateCode() {
		return alternateCode;
	}
	
	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the alternateCode to set
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	
	/**
	 * Gets the guarantees balance.
	 *
	 * @return the guaranteesBalance
	 */
	public AccountGuaranteeBalance getGuaranteesBalance() {
		return guaranteesBalance;
	}
	
	/**
	 * Sets the guarantees balance.
	 *
	 * @param guaranteesBalance the guaranteesBalance to set
	 */
	public void setGuaranteesBalance(AccountGuaranteeBalance guaranteesBalance) {
		this.guaranteesBalance = guaranteesBalance;
	}
	
	/**
	 * Gets the guarantee market fact operations.
	 *
	 * @return the guarantee market fact operations
	 */
	public List<GuaranteeMarketFactOperation> getGuaranteeMarketFactOperations() {
		return guaranteeMarketFactOperations;
	}
	
	/**
	 * Sets the guarantee market fact operations.
	 *
	 * @param guaranteeMarketFactOperations the new guarantee market fact operations
	 */
	public void setGuaranteeMarketFactOperations(
			List<GuaranteeMarketFactOperation> guaranteeMarketFactOperations) {
		this.guaranteeMarketFactOperations = guaranteeMarketFactOperations;
	}
	
	
}
