package com.pradera.guarantees.management.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.guarantees.gvc.service.GvcListServiceBean;
import com.pradera.guarantees.management.service.GuaranteeOperationsServiceBean;
import com.pradera.guarantees.management.to.GuaranteesFilter;
import com.pradera.guarantees.management.to.GuaranteesFilterByMech;
import com.pradera.guarantees.management.to.GuaranteesFilterByOper;
import com.pradera.guarantees.management.to.GuaranteesRegisterTO;
import com.pradera.guarantees.management.to.GuaranteesResultTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.AccountOperationValorization;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.GvcList;
import com.pradera.model.guarantees.type.AccountValorizationStateType;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.GuaranteeRegistrationType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.negotiations.mcnoperations.service.McnOperationServiceBean;
import com.pradera.settlements.core.service.SettlementProcessService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class GuaranteeOperationsServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/06/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class GuaranteeOperationsServiceFacade {
	
	/** The guarantees service bean. */
	@EJB
	GuaranteeOperationsServiceBean guaranteesServiceBean;
	
	/** The gvc list service bean. */
	@EJB
	private GvcListServiceBean gvcListServiceBean;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The mcn operation service bean. */
	@EJB
	private McnOperationServiceBean mcnOperationServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The account component service bean. */
	@EJB
	HolderAccountComponentServiceBean accountComponentServiceBean;
	
	/**
	 * Gets the lst participant for state.
	 *
	 * @param state the state
	 * @return the lst participant for state
	 */
	public List<Participant> getLstParticipantForState(Integer state){
		return guaranteesServiceBean.getListParticipantForState(state);		
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param idParticipant the id participant
	 * @return the participant
	 */
	public Participant getParticipant(Long idParticipant){
		return guaranteesServiceBean.getParticipant(idParticipant);		
	}
	
	/**
	 * Search mechanisms service facade.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> searchMechanismsServiceFacade() throws ServiceException{
		return mcnOperationServiceBean.getLstNegotiationMechanism(null);
	}

	/**
	 * Gets the pending operations details.
	 *
	 * @param guaranteesFilter the guarantees filter
	 * @return the pending operations details
	 */
	public List<GuaranteesFilter> getPendingOperationsDetails(GuaranteesFilter guaranteesFilter) {
		List<GuaranteesFilter> guarantessResult = new ArrayList<GuaranteesFilter>(0);
		List<Object[]> result = guaranteesServiceBean.getPendingOperationsDetails(guaranteesFilter);
		
		for (Object[] objects : result) {
			
			GuaranteesFilter guar = new GuaranteesFilter(guaranteesFilter);
			guar.setIdParticipantPk((Long)objects[0]);
			guar.setPartMnemonic((String)objects[1]);
			guar.setPartDescription((String)objects[15]);
			if(guarantessResult.contains(guar)){
				int idx = guarantessResult.indexOf(guar);
				guar = guarantessResult.get(idx);
			}else{
				guar.setListByMech(new ArrayList<GuaranteesFilterByMech>());
				guarantessResult.add(guar);	
			}
			
			if(objects[14]==null){
				guar.setUncoveredOps(guar.getUncoveredOps()+1);
				guar.setMissingMargin(guar.getMissingMargin().add((BigDecimal)objects[13]));
			}else{
				guar.setCoveredOps(guar.getCoveredOps()+1);
			}
			guar.setTotalOps(guar.getTotalOps()+1);
			
			 /// mechanism level
			GuaranteesFilterByMech guarMech = new GuaranteesFilterByMech();
			guarMech.setIdNegotiationMechanismPk(((Long)objects[2]));
			guarMech.setMechanismName(((String)objects[3]));
			guarMech.setIdNegotiationModalityPk(((Long)objects[4]));
			guarMech.setModalityName(((String)objects[5]));
			
			if(guar.getListByMech().contains(guarMech)){
				int idx = guar.getListByMech().indexOf(guarMech);
				guarMech = guar.getListByMech().get(idx);
			}else{
				guarMech.setListByOperations(new ArrayList<GuaranteesFilterByOper>(0));
				guar.getListByMech().add(guarMech);
			}
			
			if(objects[14]==null){
				guarMech.setUncoveredOps(guarMech.getUncoveredOps()+1);
				guarMech.setMissingMargin(guarMech.getMissingMargin().add((BigDecimal)objects[13]));
			}else{
				guarMech.setCoveredOps(guarMech.getCoveredOps()+1);
			}
			guarMech.setTotalOps(guarMech.getTotalOps()+1);
			
			/// operation level
			if(objects[14]==null){
				GuaranteesFilterByOper obj = new GuaranteesFilterByOper();
				obj.setIdMechanismOperationPk((Long)objects[6]);
				obj.setOperationNumber((Long)objects[7]);
				obj.setOperationDate((Date)objects[8]);
				obj.setIdIsinCodePk((String)objects[9]);
				obj.setStockQuantity((BigDecimal)objects[10]);
				obj.setCurrency((Integer)objects[11]);
				obj.setCurrencyDescription((String)objects[12]);
				obj.setMissingMargin((BigDecimal)objects[13]);
				obj.setOperationMargin((BigDecimal)objects[16]);
				guarMech.getListByOperations().add(obj);
			}
			
		}
		return guarantessResult;
	}
	
	/**
	 * Gets the pending operations after valorization.
	 *
	 * @param guaranteesFilter the guarantees filter
	 * @return the pending operations after valorization
	 */
	public List<GuaranteesFilter> getPendingOperationsAfterValorization(
			GuaranteesFilter guaranteesFilter) {
		
		List<GuaranteesFilter> guarantessResult = new ArrayList<GuaranteesFilter>(0);
		List<Object[]> result = guaranteesServiceBean.getPendingOperationsAfterValorization(guaranteesFilter);
		
		for (Object[] objects : result) {
			GuaranteesFilter guar = new GuaranteesFilter(guaranteesFilter);
			guar.setIdParticipantPk((Long)objects[0]);
			guar.setPartMnemonic((String)objects[1]);
			guar.setPartDescription((String)objects[16]);
			if(guarantessResult.contains(guar)){
				int idx = guarantessResult.indexOf(guar);
				guar = guarantessResult.get(idx);
			}else{
				guar.setListByMech(new ArrayList<GuaranteesFilterByMech>());
				guarantessResult.add(guar);	
			}
			
			if(objects[13]!=null && ((BigDecimal)objects[13]).compareTo(BigDecimal.ZERO) > 0){
				guar.setUncoveredOps(guar.getUncoveredOps()+1);
				guar.setMissingMargin(guar.getMissingMargin().add((BigDecimal)objects[13]));
			}else{
				guar.setCoveredOps(guar.getCoveredOps()+1);
			}
			guar.setTotalOps(guar.getTotalOps()+1);
			
			 /// mechanism level
			GuaranteesFilterByMech guarMech = new GuaranteesFilterByMech();
			guarMech.setIdNegotiationMechanismPk(((Long)objects[2]));
			guarMech.setMechanismName(((String)objects[3]));
			guarMech.setIdNegotiationModalityPk(((Long)objects[4]));
			guarMech.setModalityName(((String)objects[5]));
			
			if(guar.getListByMech().contains(guarMech)){
				int idx = guar.getListByMech().indexOf(guarMech);
				guarMech = guar.getListByMech().get(idx);
			}else{
				guarMech.setListByOperations(new ArrayList<GuaranteesFilterByOper>(0));
				guarMech.setCoveredListByOperations(new ArrayList<GuaranteesFilterByOper>(0));
				guar.getListByMech().add(guarMech);
			}
			
			if(objects[13]!=null && ((BigDecimal)objects[13]).compareTo(BigDecimal.ZERO) > 0){
				guarMech.setUncoveredOps(guarMech.getUncoveredOps()+1);
				guarMech.setMissingMargin(guarMech.getMissingMargin().add((BigDecimal)objects[13]));
			}else{
				guarMech.setCoveredOps(guarMech.getCoveredOps()+1);
			}
			guarMech.setTotalOps(guarMech.getTotalOps()+1);
			
			/// operation level
			
			GuaranteesFilterByOper obj = new GuaranteesFilterByOper();
			obj.setIdMechanismOperationPk((Long)objects[6]);
			obj.setOperationNumber((Long)objects[7]);
			obj.setOperationDate((Date)objects[8]);
			obj.setIdIsinCodePk((String)objects[9]);
			obj.setStockQuantity((BigDecimal)objects[10]);
			obj.setCurrency((Integer)objects[11]);
			obj.setCurrencyDescription((String)objects[12]);
			obj.setCoverageAmount((BigDecimal)objects[15]);
			obj.setOperationValuedAmount(objects[14]==null?(BigDecimal)objects[15]:(BigDecimal)objects[14]);
			obj.setMissingMargin((BigDecimal)objects[13]==null?BigDecimal.ZERO:(BigDecimal)objects[13]);
			
			if(objects[13]!=null && ((BigDecimal)objects[13]).compareTo(BigDecimal.ZERO) > 0){
				guarMech.getListByOperations().add(obj);
			}else{
				guarMech.getCoveredListByOperations().add(obj);
			}
			
		}
		return guarantessResult;
	}

	/**
	 * Gets the pending accounts by op.
	 *
	 * @param resultByOperation the result by operation
	 * @param isInitialGuarantees the is initial guarantees
	 * @param isRepositionGuarantees the is reposition guarantees
	 * @param isAditionalGuarantees the is aditional guarantees
	 * @return the pending accounts by op
	 * @throws ServiceException the service exception
	 */
	public List<GuaranteesResultTO> getPendingAccountsByOp(GuaranteesFilterByOper resultByOperation, boolean isInitialGuarantees, 
			boolean isRepositionGuarantees, boolean isAditionalGuarantees) throws ServiceException {
		List<GuaranteesResultTO> guarantessResult = new ArrayList<GuaranteesResultTO>(0);
		List<Object[]> result = guaranteesServiceBean.getPendingAccountsByOp(resultByOperation.getIdMechanismOperationPk(),isInitialGuarantees);
		for (Object[] objects : result) {
			GuaranteesResultTO obj = new GuaranteesResultTO(resultByOperation);
			
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setNeedHolder(true);
			holderAccountTO.setNeedParticipant(false);
			holderAccountTO.setNeedBanks(false);
			holderAccountTO.setIdHolderAccountPk((Long)objects[0]);
			HolderAccount holderAccount = accountComponentServiceBean.getHolderAccountComponentServiceBean(holderAccountTO );
			obj.setHolderAccount(holderAccount);
			obj.setAccStockQuantity((BigDecimal)objects[1]);
			obj.setAccCoverageAmount(objects[2]==null? BigDecimal.ZERO : (BigDecimal)objects[2]); // monto de cobertura
			obj.setAccMarginAmount(objects[3]==null? BigDecimal.ZERO : (BigDecimal)objects[3]); // monto margen inicial
			obj.setAccMarginAmountSum(BigDecimal.ZERO); // monto acumulado en el registro (cero en inicial)
			obj.setAccMissingAmount(BigDecimal.ZERO);  // monto a reponer o depositar inicial
			obj.setAccTermAmount((BigDecimal)objects[4]);
			obj.setAccCashAmount(objects[5]==null? BigDecimal.ZERO : (BigDecimal)objects[5]);
			obj.setIdAccountOperationPk((Long)objects[6]);  /// term
			obj.setIdAccountOperationRefPk((Long)objects[7]); /// cash
			obj.setIdStockParticipantPk((Long)objects[8]);
			if(objects[9] == null){
				obj.setAccValuedAmount(obj.getAccCoverageAmount());
			}else{
				obj.setAccValuedAmount((BigDecimal) objects[9]);
			}
			obj.setAccSurplusAmount(obj.getAccValuedAmount().subtract(obj.getAccCoverageAmount()));
			
			if(isInitialGuarantees){
				if(obj.getAccMarginAmount()!=null){
					obj.setAccMissingAmount(obj.getAccMarginAmount());
				}
				
			}
			if(isRepositionGuarantees){
				List<AccountOperationValorization> valorizations = guaranteesServiceBean.findPreviousAccountValorizations(
						(Long)objects[6],CommonsUtilities.currentDate(),AccountValorizationStateType.PENDING.getCode());
				if(valorizations.size() >0){
					obj.setAccountValorizations(valorizations);
					for (AccountOperationValorization accountValorization : valorizations) {
						obj.setAccMissingAmount(obj.getAccMissingAmount().add(accountValorization.getMissingAmount()));
					}
				}
				//guarantessResult.add(obj);
			}
			
			guarantessResult.add(obj);
			
//			if(isAditionalGuarantees){
//				List<AccountOperationValorization> valorizations = guaranteesServiceBean.
//						findPreviousAccountValorizations((Long)objects[7],CommonsUtilities.currentDate(),
//								AccountValorizationStateType.FULFILLED.getCode());
//				obj.setAccValuedAmount(BigDecimal.ZERO);
//				if(valorizations.size() >0){
//					obj.setAccountValorizations(valorizations);
//					for (AccountOperationValorization accountValorization : valorizations) {
//						obj.setAccValuedAmount(obj.getAccValuedAmount().add(accountValorization.getTotalValorization()));
//					}
//				} else {
//				obj.setAccValuedAmount(obj.getAccCoverageAmount());
//				}
//				guarantessResult.add(obj);
//			}
		}
		return guarantessResult;
	}

	/**
	 * Gets the mechanism operation details.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the mechanism operation details
	 */
	public MechanismOperation getMechanismOperationDetails(Long idMechanismOperationPk) {
		MechanismOperation mechanismOperation = guaranteesServiceBean.getMechanismOperationDetails(idMechanismOperationPk);
		return mechanismOperation;
	}
	
	/**
	 * Gets the find institution cash account.
	 *
	 * @param idInstitutionCash the id institution cash
	 * @return the find institution cash account
	 */
	public InstitutionCashAccount getFindInstitutionCashAccount(Long idInstitutionCash) {
		return guaranteesServiceBean.getFindInstitutionCashAccount(idInstitutionCash);
	}

	/**
	 * Gets the guarantees registration lists.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param guaranteesResultTO the guarantees result to
	 * @return the guarantees registration lists
	 */
	public GuaranteesResultTO getGuaranteesRegistrationLists(
			Long mechanismId, Long modalityId, GuaranteesResultTO guaranteesResultTO) {
		
		List<Object[]> accs =  guaranteesServiceBean.getInstitutionCashAccountsDetails(guaranteesResultTO.getIdStockParticipantPk(),guaranteesResultTO.getCurrency());
		
		if(accs!=null && accs.size() >0){
			guaranteesResultTO.setLstGuaranteesInFunds(new GenericDataModel<GuaranteesRegisterTO>());
			for (Object[] icaDetail : accs) {
				GuaranteesRegisterTO register = new GuaranteesRegisterTO();
				register.setIdHolderAccountPk(guaranteesResultTO.getHolderAccount().getIdHolderAccountPk());
				register.setIdInstitutionCashAccountPk((Long)icaDetail[0]);			
				register.setCashAccountTotalAmount((BigDecimal)icaDetail[1]);
				register.setCashAccountAvailableAmount((BigDecimal)icaDetail[2]);
				register.setCashAccountType((Integer)icaDetail[3]);
				register.setCashAccountTypeDescription(icaDetail[4] == null ? GeneralConstants.EMPTY_STRING : (String)icaDetail[4]);
				register.setIdInstitutionBankAccountPk((Long)icaDetail[5]);
				register.setBankAccountNumber((String)icaDetail[6]);
				register.setIdBankPk((Long)icaDetail[7]);
				register.setMarginAmount(guaranteesResultTO.getAccMarginAmount());
				guaranteesResultTO.getLstGuaranteesInFunds().getDataList().add(register);
			}
		}
		
		List<Object[]> habs =  guaranteesServiceBean.getSupportedAccountBalances(
				mechanismId,
				modalityId,
				guaranteesResultTO.getIdStockParticipantPk(), 
				guaranteesResultTO.getHolderAccount().getIdHolderAccountPk());
		
		if(habs.size() >0){
			guaranteesResultTO.setLstGuaranteesInSecurities(new GenericDataModel<GuaranteesRegisterTO>());
			for (Object[] objects : habs) {
				
				GuaranteesRegisterTO register = new GuaranteesRegisterTO();
				BigDecimal price = guaranteesServiceBean.getLatestQuotationPrice(mechanismId,(String)objects[2],CommonsUtilities.currentDate());
				if(price == null){
					register.setQuotationPrice((BigDecimal)objects[4]); // price is nominal value
				}else{
					register.setQuotationPrice(price); // price is quotation close price
				}
				
				register.setIdHolderAccountPk(guaranteesResultTO.getHolderAccount().getIdHolderAccountPk());
				register.setAccountNumber(guaranteesResultTO.getHolderAccount().getAccountNumber());
				register.setAlternateCode(guaranteesResultTO.getHolderAccount().getAlternateCode());
				//register.setBalanceToUse(BigDecimal.ZERO);
				register.setIdSecurityCodePk((String)objects[2]);
				register.setTotalBalance((BigDecimal)objects[0]);
				register.setAvailableBalance((BigDecimal)objects[1]);
				//register.setValuedAmount(BigDecimal.ZERO);
				register.setIdParticipantPk(guaranteesResultTO.getIdStockParticipantPk());
				
				register.setPunishmentFactor((BigDecimal)objects[3]);
				guaranteesResultTO.getLstGuaranteesInSecurities().getDataList().add(register);
			}
		}
		
		return guaranteesResultTO;
	}

	/**
	 * Gets the guarantee balances.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param guaranteesResultTO the guarantees result to
	 * @return the guarantee balances
	 */
	public GuaranteesResultTO getGuaranteeBalances(Long idMechanism, Long idModality, GuaranteesResultTO guaranteesResultTO) {
		List<AccountGuaranteeBalance> guaranteeBalances = guaranteesServiceBean.getGuaranteeBalancesByParams(guaranteesResultTO.getIdAccountOperationPk());
		guaranteesResultTO.setLstGuaranteesInFunds(new GenericDataModel<GuaranteesRegisterTO>());
		guaranteesResultTO.setLstGuaranteesInSecurities(new GenericDataModel<GuaranteesRegisterTO>());
		
		for (AccountGuaranteeBalance accountGuaranteeBalance : guaranteeBalances) {
			GuaranteesRegisterTO objGuaranteesBalance = new GuaranteesRegisterTO();
			objGuaranteesBalance.setGuaranteesBalance(accountGuaranteeBalance);
			objGuaranteesBalance.setAccountNumber(guaranteesResultTO.getHolderAccount().getAccountNumber());
			objGuaranteesBalance.setAlternateCode(guaranteesResultTO.getHolderAccount().getAlternateCode());
			objGuaranteesBalance.setTotalBalance(accountGuaranteeBalance.getMarginGuarantee().add(accountGuaranteeBalance.getMarginDividendsGuarantee()));
			
			if(accountGuaranteeBalance.getGuaranteeClass().equals(GuaranteeClassType.FUNDS.getCode())){
				
				Long cashAccountPk = accountGuaranteeBalance.getInstitutionCashAccount().getIdInstitutionCashAccountPk();
				Object[] cashAccDetails =  guaranteesServiceBean.getInstitutionCashAccountsDetailsByPk(cashAccountPk);
				objGuaranteesBalance.setIdInstitutionCashAccountPk(accountGuaranteeBalance.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
				objGuaranteesBalance.setIdBankPk((Long)cashAccDetails[6]);
				objGuaranteesBalance.setIdInstitutionBankAccountPk((Long)cashAccDetails[4]);
				objGuaranteesBalance.setBankAccountNumber((String)cashAccDetails[5]);
				objGuaranteesBalance.setCashAccountAvailableAmount(objGuaranteesBalance.getTotalBalance());
				guaranteesResultTO.getLstGuaranteesInFunds().getDataList().add(objGuaranteesBalance);	
			
			}else if(accountGuaranteeBalance.getGuaranteeClass().equals(GuaranteeClassType.SECURITIES.getCode())){
				Object[] securityBalance =  guaranteesServiceBean.getGuaranteeBalancesInSecurities(idMechanism, idModality, accountGuaranteeBalance.getIdAccountGuarBalancePk());
				
				objGuaranteesBalance.setAvailableBalance(objGuaranteesBalance.getTotalBalance());
				objGuaranteesBalance.setIdSecurityCodePk((String)securityBalance[0]);
				//modificado
				BigDecimal price = guaranteesServiceBean.getLatestQuotationPrice(idMechanism,objGuaranteesBalance.getIdSecurityCodePk(),CommonsUtilities.currentDate());
				if(price == null){
					objGuaranteesBalance.setQuotationPrice((BigDecimal)securityBalance[2]); // price is nominal value
				}else{
					objGuaranteesBalance.setQuotationPrice(price);// price is quotation close price
				}
				
				objGuaranteesBalance.setPunishmentFactor((BigDecimal)securityBalance[1]);

				guaranteesResultTO.getLstGuaranteesInSecurities().getDataList().add(objGuaranteesBalance);
			}
		}
		
		return guaranteesResultTO;
	}
	
	/**
	 * Save guarantee operations.
	 *
	 * @param objGuaranteesAccount the obj guarantees account
	 * @param lstGuaranteeOperations the lst guarantee operations
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<GuaranteesResultTO> saveGuaranteeOperations(GuaranteesResultTO objGuaranteesAccount, List<GuaranteeOperation> lstGuaranteeOperations) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        List<GuaranteesResultTO> remainingAccountOperations = new ArrayList<GuaranteesResultTO>(0);
        
        if(objGuaranteesAccount.getRegistrationType().equals(GuaranteeRegistrationType.INITIAL_GUARANTEES.getCode())){
        	guaranteesServiceBean.saveInitialGuaranteeOperations(objGuaranteesAccount.getIdAccountOperationRefPk(),objGuaranteesAccount.getIdAccountOperationPk(), lstGuaranteeOperations);
    		
    		remainingAccountOperations = getPendingAccountsByOp(objGuaranteesAccount,true,false,false);
    		
    		if(remainingAccountOperations.size() <= 0 ){
    			guaranteesServiceBean.updateOperationReference(objGuaranteesAccount.getIdMechanismOperationPk());
    			// Add update settlement operation
    			guaranteesServiceBean.updateSettlementOperationMargin(objGuaranteesAccount.getIdMechanismOperationPk(), loggerUser);
    		}
    		
        }else{
        	if (objGuaranteesAccount.getRegistrationType().equals(GuaranteeRegistrationType.GUARANTEES_REPOSITION.getCode())){
        		// se regitran las operaciones de garantias de la cuenta 
	        	guaranteesServiceBean.saveGuaranteeOperations(lstGuaranteeOperations);
	        	
	        	//se cancelan las desvalorizaciones, por revalorizacion
	    		if(Validations.validateListIsNotNullAndNotEmpty(objGuaranteesAccount.getAccountValorizations())){
	    			for (AccountOperationValorization valorization : objGuaranteesAccount.getAccountValorizations()) {
	    				valorization.setPaymentDate(CommonsUtilities.currentDateTime());
	    				valorization.setState(AccountValorizationStateType.CANCELLED_BY_REVALORIZATION.getCode());
	    				guaranteesServiceBean.update(valorization);
	    			}
	    		}
        	}else if (objGuaranteesAccount.getRegistrationType().equals(GuaranteeRegistrationType.ADITIONAL_GUARANTEES.getCode())){
        		guaranteesServiceBean.saveGuaranteeOperations(lstGuaranteeOperations);
        	}else if (objGuaranteesAccount.getRegistrationType().equals(GuaranteeRegistrationType.GUARANTEES_LIBERATION.getCode())){
        		guaranteesServiceBean.saveWithdrawalGuaranteeOperations(lstGuaranteeOperations);
        	}
        	
        	//se calculan las nuevas valorizaciones 
        	calculateValorizations(objGuaranteesAccount.getIdAccountOperationRefPk()); // cash part
        	
        	remainingAccountOperations = getPendingAccountsByOp(objGuaranteesAccount,false,true,false);
        }
        return remainingAccountOperations;
	}


	
	/**
	 * Calculate valorizations.
	 *
	 * @param idHolderAccountOperationPk the id holder account operation pk
	 * @throws ServiceException the service exception
	 */
	public void calculateValorizations(Long idHolderAccountOperationPk) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		Date currentDate = CommonsUtilities.currentDate();
		
		List<Object[]> coveredAccountOperations = guaranteesServiceBean.getCoveredAccountOperationDetails(currentDate,idHolderAccountOperationPk);
		 
		for (Object[] accountOperation : coveredAccountOperations) {
		
			// guarantee balances per account operation
			Long idAccountOperationPk = (Long)accountOperation[3];  // term part
			List<AccountGuaranteeBalance> accountGuaranteeBalances = guaranteesServiceBean.getGuaranteeBalancesByParams(idAccountOperationPk);
			
			// calculate valorization per balances
			BigDecimal valorizationInSecurities = BigDecimal.ZERO;
			BigDecimal valorizationInFunds = BigDecimal.ZERO;
			BigDecimal operationCashAmount = (BigDecimal)accountOperation[6];
			BigDecimal coverageAmount = (BigDecimal)accountOperation[7];
			
			for(AccountGuaranteeBalance accountBalance: accountGuaranteeBalances){
				// reported guarantees = margin + dividends margin
				BigDecimal reportedMargin = accountBalance.getMarginGuarantee().add(accountBalance.getMarginDividendsGuarantee());
				//totalReportedMargin = totalReportedMargin.add(reportedMargin);
				
				if(accountBalance.getGuaranteeClass().equals(GuaranteeClassType.FUNDS.getCode())){
					valorizationInFunds = valorizationInFunds.add(reportedMargin);
				}else if(accountBalance.getGuaranteeClass().equals(GuaranteeClassType.SECURITIES.getCode())){
					
					// group punishment factor
					Long idMechanism = (Long)accountOperation[1];
					Long idModality = (Long)accountOperation[2];
					String idSecurityCodePk = accountBalance.getSecurities().getIdSecurityCodePk();
					GvcList gvclist = gvcListServiceBean.getGvcDataByParams(idMechanism , idModality, idSecurityCodePk);
					if(gvclist!=null){
						
						BigDecimal punishFactor = gvclist.getGroupPercent();
						//quotation price
						BigDecimal quotationPrice = guaranteesServiceBean.getLatestQuotationPrice(idMechanism,idSecurityCodePk,currentDate);
						
						if(quotationPrice == null){
							quotationPrice = (BigDecimal)accountBalance.getSecurities().getCurrentNominalValue();
						}

						BigDecimal securityValuedAmount = quotationPrice.multiply(punishFactor).multiply(reportedMargin);
						valorizationInSecurities = valorizationInSecurities.add(securityValuedAmount);
					}
				}
			}
			
			//calculate total valorization per ac	count operation
			BigDecimal marginValorization = valorizationInSecurities.add(valorizationInFunds);
			BigDecimal totalValorization = operationCashAmount.add(marginValorization);
			BigDecimal valDifference = totalValorization.subtract(coverageAmount);
			
			List<AccountOperationValorization> accountValorizations = 
					guaranteesServiceBean.findPreviousAccountValorizations(
							idAccountOperationPk,currentDate,AccountValorizationStateType.PENDING.getCode());
			
			if(valDifference.compareTo(BigDecimal.ZERO) < 0){
				// en desvalorizacion
				// cancelamos desvalorizaciones pendientes no cubiertas 
				if(accountValorizations.size() > 0){
					for (AccountOperationValorization accountOperationValorization : accountValorizations) {
						accountOperationValorization.setState(AccountValorizationStateType.CANCELLED_BY_UNFULFILLMENT.getCode());
						guaranteesServiceBean.update(accountOperationValorization);
					}
				}
				
				// registrar nota de valorizacion
				AccountOperationValorization accountOperationValorization = new AccountOperationValorization();
				accountOperationValorization.setValorizationDate(currentDate);
				accountOperationValorization.setTotalValorization(totalValorization);
				accountOperationValorization.setMarginValorization(marginValorization);
				accountOperationValorization.setMissingAmount(valDifference.negate()); // for positive
				accountOperationValorization.setState(AccountValorizationStateType.PENDING.getCode());
				//accountOperationValorization.setSituation(AccountValorizationSituationType.PENDING.getCode());
				HolderAccountOperation holderAccountOperation = new HolderAccountOperation();
				holderAccountOperation.setIdHolderAccountOperationPk(idAccountOperationPk);
				accountOperationValorization.setHolderAccountOperation(holderAccountOperation);
				accountOperationValorization.setCurrency((Integer)accountOperation[8]);
				guaranteesServiceBean.create(accountOperationValorization);
				
			}else{
				//cubierto o revalorizado 
				// se registra nueva nota de valorizacion si hay valorizaciones no cubiertas pendientes 
				if(accountValorizations.size() > 0){
					for (AccountOperationValorization accountOperationValorization : accountValorizations) {
						accountOperationValorization.setState(AccountValorizationStateType.CANCELLED_BY_REVALORIZATION.getCode());
						guaranteesServiceBean.update(accountOperationValorization);
					}
				}	

				// registrar nota de valorizacion
				AccountOperationValorization accountOperationValorization = new AccountOperationValorization();
				accountOperationValorization.setValorizationDate(currentDate);
				accountOperationValorization.setTotalValorization(totalValorization);
				accountOperationValorization.setMarginValorization(marginValorization);
				accountOperationValorization.setMissingAmount(BigDecimal.ZERO);
				//accountOperationValorization.setState(AccountValorizationStateType.COVERED.getCode());
				accountOperationValorization.setState(AccountValorizationStateType.FULFILLED.getCode());
				HolderAccountOperation holderAccountOperation = new HolderAccountOperation();
				holderAccountOperation.setIdHolderAccountOperationPk(idAccountOperationPk);
				accountOperationValorization.setHolderAccountOperation(holderAccountOperation);
				accountOperationValorization.setCurrency((Integer)accountOperation[8]);
				guaranteesServiceBean.create(accountOperationValorization);
				
			}
			
			guaranteesServiceBean.updateAccountOperationValorization(idAccountOperationPk,totalValorization,loggerUser);
		}

	}
	
}
