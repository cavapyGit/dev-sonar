package com.pradera.guarantees.management.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class GuaranteesFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/06/2014
 */
public class GuaranteesFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The date type. */
	private Integer dateType;
	
	/** The guarantees reg type. */
	private Integer guaranteesRegType;

	/** The total ops. */
	private long totalOps = 0;
	
	/** The covered ops. */
	private long coveredOps = 0;
	
	/** The uncovered ops. */
	private long uncoveredOps = 0;

	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The part mnemonic. */
	private String partMnemonic;
	
	/** The part description. */
	private String partDescription;
	
	/** The missing margin. */
	private BigDecimal missingMargin = BigDecimal.ZERO;
	
	/** The list by mech. */
	private List<GuaranteesFilterByMech> listByMech;
	
	/** The covered list by mech. */
	private List<GuaranteesFilterByMech> coveredListByMech;
	
	/**
	 * Instantiates a new guarantees filter.
	 */
	public GuaranteesFilter() {
		super();
	}
	
	/**
	 * Instantiates a new guarantees filter.
	 *
	 * @param guaranteesFilter the guarantees filter
	 */
	public GuaranteesFilter(GuaranteesFilter guaranteesFilter) {
		this.settlementDate = guaranteesFilter.getSettlementDate();
		this.dateType = guaranteesFilter.getDateType();
		this.guaranteesRegType = guaranteesFilter.getGuaranteesRegType();
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlementDate
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the settlementDate to set
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the part mnemonic.
	 *
	 * @return the partMnemonic
	 */
	public String getPartMnemonic() {
		return partMnemonic;
	}
	
	/**
	 * Sets the part mnemonic.
	 *
	 * @param partMnemonic the partMnemonic to set
	 */
	public void setPartMnemonic(String partMnemonic) {
		this.partMnemonic = partMnemonic;
	}
	
	/**
	 * Gets the total ops.
	 *
	 * @return the totalOps
	 */
	public long getTotalOps() {
		return totalOps;
	}
	
	/**
	 * Sets the total ops.
	 *
	 * @param totalOps the totalOps to set
	 */
	public void setTotalOps(long totalOps) {
		this.totalOps = totalOps;
	}
	
	/**
	 * Gets the covered ops.
	 *
	 * @return the coveredOps
	 */
	public long getCoveredOps() {
		return coveredOps;
	}
	
	/**
	 * Sets the covered ops.
	 *
	 * @param coveredOps the coveredOps to set
	 */
	public void setCoveredOps(long coveredOps) {
		this.coveredOps = coveredOps;
	}
	
	/**
	 * Gets the uncovered ops.
	 *
	 * @return the uncoveredOps
	 */
	public long getUncoveredOps() {
		return uncoveredOps;
	}
	
	/**
	 * Sets the uncovered ops.
	 *
	 * @param uncoveredOps the uncoveredOps to set
	 */
	public void setUncoveredOps(long uncoveredOps) {
		this.uncoveredOps = uncoveredOps;
	}

	/**
	 * Gets the guarantees reg type.
	 *
	 * @return the guaranteesRegType
	 */
	public Integer getGuaranteesRegType() {
		return guaranteesRegType;
	}
	
	/**
	 * Sets the guarantees reg type.
	 *
	 * @param guaranteesRegType the guaranteesRegType to set
	 */
	public void setGuaranteesRegType(Integer guaranteesRegType) {
		this.guaranteesRegType = guaranteesRegType;
	}
	
	/**
	 * Gets the date type.
	 *
	 * @return the dateType
	 */
	public Integer getDateType() {
		return dateType;
	}
	
	/**
	 * Sets the date type.
	 *
	 * @param dateType the dateType to set
	 */
	public void setDateType(Integer dateType) {
		this.dateType = dateType;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idParticipantPk == null) ? 0 : idParticipantPk.hashCode());
		result = prime * result
				+ ((partMnemonic == null) ? 0 : partMnemonic.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GuaranteesFilter other = (GuaranteesFilter) obj;
		if (idParticipantPk == null) {
			if (other.idParticipantPk != null)
				return false;
		} else if (!idParticipantPk.equals(other.idParticipantPk))
			return false;
		if (partMnemonic == null) {
			if (other.partMnemonic != null)
				return false;
		} else if (!partMnemonic.equals(other.partMnemonic))
			return false;
		return true;
	}
	
	/**
	 * Gets the list by mech.
	 *
	 * @return the listByMech
	 */
	public List<GuaranteesFilterByMech> getListByMech() {
		return listByMech;
	}
	
	/**
	 * Sets the list by mech.
	 *
	 * @param listByMech the listByMech to set
	 */
	public void setListByMech(List<GuaranteesFilterByMech> listByMech) {
		this.listByMech = listByMech;
	}
	
	/**
	 * Gets the missing margin.
	 *
	 * @return the missingMargin
	 */
	public BigDecimal getMissingMargin() {
		return missingMargin;
	}
	
	/**
	 * Sets the missing margin.
	 *
	 * @param missingMargin the missingMargin to set
	 */
	public void setMissingMargin(BigDecimal missingMargin) {
		this.missingMargin = missingMargin;
	}
	
	/**
	 * Gets the part description.
	 *
	 * @return the partDescription
	 */
	public String getPartDescription() {
		return partDescription;
	}
	
	/**
	 * Sets the part description.
	 *
	 * @param partDescription the partDescription to set
	 */
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	
	/**
	 * Gets the covered list by mech.
	 *
	 * @return the coveredListByMech
	 */
	public List<GuaranteesFilterByMech> getCoveredListByMech() {
		return coveredListByMech;
	}
	
	/**
	 * Sets the covered list by mech.
	 *
	 * @param coveredListByMech the coveredListByMech to set
	 */
	public void setCoveredListByMech(List<GuaranteesFilterByMech> coveredListByMech) {
		this.coveredListByMech = coveredListByMech;
	}	
}
