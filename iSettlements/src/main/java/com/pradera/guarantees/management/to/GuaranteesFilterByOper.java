package com.pradera.guarantees.management.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GuaranteesFilterByOper.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class GuaranteesFilterByOper implements Serializable {

	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation pk. */
	private Long idMechanismOperationPk;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The currency. */
	private Integer currency;
	
	/** The currency description. */
	private String currencyDescription;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The coverage amount. */
	private BigDecimal coverageAmount;
	
	/** The operation margin. */
	private BigDecimal operationMargin;
	
	/** The operation valued amount. */
	private BigDecimal operationValuedAmount;
	
	/** The missing margin. */
	private BigDecimal missingMargin = BigDecimal.ZERO;
	
	
	/**
	 * Instantiates a new guarantees filter by oper.
	 */
	public GuaranteesFilterByOper() {
		super();
	}
	
	/**
	 * Instantiates a new guarantees filter by oper.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param operationNumber the operation number
	 * @param operationDate the operation date
	 * @param idIsinCodePk the id isin code pk
	 * @param currency the currency
	 * @param currencyDescription the currency description
	 * @param stockQuantity the stock quantity
	 * @param coverageAmount the coverage amount
	 */
	public GuaranteesFilterByOper(Long idMechanismOperationPk,
			Long operationNumber, Date operationDate, String idIsinCodePk,
			Integer currency, String currencyDescription,
			BigDecimal stockQuantity, BigDecimal coverageAmount) {
		super();
		this.idMechanismOperationPk = idMechanismOperationPk;
		this.operationNumber = operationNumber;
		this.operationDate = operationDate;
		this.idIsinCodePk = idIsinCodePk;
		this.currency = currency;
		this.currencyDescription = currencyDescription;
		this.stockQuantity = stockQuantity;
		this.coverageAmount = coverageAmount;
	}
	
	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the idMechanismOperationPk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	
	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the idMechanismOperationPk to set
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the idIsinCodePk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the idIsinCodePk to set
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the currency description.
	 *
	 * @return the currencyDescription
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	
	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the currencyDescription to set
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the coverage amount.
	 *
	 * @return the coverageAmount
	 */
	public BigDecimal getCoverageAmount() {
		return coverageAmount;
	}
	
	/**
	 * Sets the coverage amount.
	 *
	 * @param coverageAmount the coverageAmount to set
	 */
	public void setCoverageAmount(BigDecimal coverageAmount) {
		this.coverageAmount = coverageAmount;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idMechanismOperationPk == null) ? 0
						: idMechanismOperationPk.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GuaranteesFilterByOper other = (GuaranteesFilterByOper) obj;
		if (idMechanismOperationPk == null) {
			if (other.idMechanismOperationPk != null)
				return false;
		} else if (!idMechanismOperationPk.equals(other.idMechanismOperationPk))
			return false;
		return true;
	}
	
	/**
	 * Gets the operation margin.
	 *
	 * @return the operationMargin
	 */
	public BigDecimal getOperationMargin() {
		return operationMargin;
	}
	
	/**
	 * Sets the operation margin.
	 *
	 * @param operationMargin the operationMargin to set
	 */
	public void setOperationMargin(BigDecimal operationMargin) {
		this.operationMargin = operationMargin;
	}
	
	/**
	 * Gets the operation valued amount.
	 *
	 * @return the operationValuedAmount
	 */
	public BigDecimal getOperationValuedAmount() {
		return operationValuedAmount;
	}
	
	/**
	 * Sets the operation valued amount.
	 *
	 * @param operationValuedAmount the operationValuedAmount to set
	 */
	public void setOperationValuedAmount(BigDecimal operationValuedAmount) {
		this.operationValuedAmount = operationValuedAmount;
	}
	
	/**
	 * Gets the missing margin.
	 *
	 * @return the missingMargin
	 */
	public BigDecimal getMissingMargin() {
		return missingMargin;
	}
	
	/**
	 * Sets the missing margin.
	 *
	 * @param missingMargin the missingMargin to set
	 */
	public void setMissingMargin(BigDecimal missingMargin) {
		this.missingMargin = missingMargin;
	}
		
}
