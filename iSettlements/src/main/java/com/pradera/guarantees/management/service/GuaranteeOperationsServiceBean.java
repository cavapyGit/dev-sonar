package com.pradera.guarantees.management.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.guarantees.management.to.GuaranteesFilter;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.DetailCashAccountStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.AccountOperationValorization;
import com.pradera.model.guarantees.GuaranteeMarketFactOperation;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.guarantees.type.AccountValorizationStateType;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.guarantees.type.SecurityGvcStateType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GuaranteeOperationsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class GuaranteeOperationsServiceBean extends CrudDaoServiceBean {

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(GuaranteeOperationsServiceBean.class);
	
	/** The funds component service. */
	@Inject
    Instance<FundsComponentService> fundsComponentService;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/**
	 * Gets the margin factor param.
	 *
	 * @return the margin factor param
	 */
	public BigDecimal getMarginFactorParam(){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select pt.decimalAmount1 from  ");
		sbQuery.append(" ParameterTable pt ");
		sbQuery.append(" where pt.masterTable.masterTablePk = :idMasterTablePk ");
		
		parameters.put("idMasterTablePk", MasterTableType.MASTER_TABLE_MARGIN_FACTOR.getCode());
		return (BigDecimal)findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the covered account operation details.
	 *
	 * @param date the date
	 * @param idHolderAccountOperationPk the id holder account operation pk
	 * @return the covered account operation details
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getCoveredAccountOperationDetails(Date date,Long idHolderAccountOperationPk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select ");
		sbQuery.append("	mo.securities.idSecurityCodePk, "); //0
		sbQuery.append("	mo.mechanisnModality.id.idNegotiationMechanismPk, "); //1
		sbQuery.append("	mo.mechanisnModality.id.idNegotiationModalityPk, "); //2
		sbQuery.append("	hao.idHolderAccountOperationPk, "); //3 take the term account
		sbQuery.append("	hao.holderAccount.idHolderAccountPk, "); //4
		sbQuery.append("	cashHao.stockQuantity, "); //5
		sbQuery.append("	cashHao.settlementAmount, "); //6
		sbQuery.append("	cashHao.coverageAmount, "); //7
		sbQuery.append("	mo.termSettlementCurrency, "); //8
		sbQuery.append("	mo.securities.currentNominalValue, "); //9
		sbQuery.append("	mo.idMechanismOperationPk "); //10 pk
		sbQuery.append(" from HolderAccountOperation hao ");
		sbQuery.append(" inner join hao.refAccountOperation cashHao ");
		sbQuery.append(" inner join hao.mechanismOperation mo ");
		sbQuery.append(" where mo.operationState  = :operationState ");
		sbQuery.append(" and mo.mechanisnModality.negotiationModality.indMarginGuarantee = :indGuarantees ");
		sbQuery.append(" and hao.role = :buyRole ");
		sbQuery.append(" and hao.operationPart = :termPart ");
		sbQuery.append(" and trunc(mo.cashSettlementDate) <= :operationDate ");
		sbQuery.append(" and trunc(mo.termSettlementDate) >= :operationDate ");
		sbQuery.append(" and hao.marginReference = :marginReference ");
		if(idHolderAccountOperationPk!=null){
			sbQuery.append(" and hao.idHolderAccountOperationPk = :idHolderAccountOperationPk "); // always term part
			parameters.put("idHolderAccountOperationPk", idHolderAccountOperationPk);
		}
		
		parameters.put("operationState", MechanismOperationStateType.CASH_SETTLED.getCode());
		parameters.put("indGuarantees", GeneralConstants.ONE_VALUE_INTEGER);
		parameters.put("buyRole", ComponentConstant.PURCHARSE_ROLE);
		//parameters.put("sellRole", ComponentConstant.SALE_ROLE);
		//parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("termPart", ComponentConstant.TERM_PART);
		parameters.put("operationDate",date);
		parameters.put("marginReference", AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
		return findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getParticipantList() {
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct part_mech.participant.idParticipantPk,part_mech.participant.description from ParticipantMechanism part_mech");
		//sbQuery.append(" where part_mech.mechanismModality.negotiationMechanism.idNegotiationMechanismPk = :mechanism");
		sbQuery.append(" where part_mech.mechanismModality.negotiationModality.idNegotiationModalityPk = :modality and part_mech.participant.state = :partState");
		sbQuery.append(" and part_mech.stateParticipantMechanism = :idStateParticipantMechanism ");
		sbQuery.append(" order by part_mech.participant.description ");
		parameters.put("modality", NegotiationModalityType.REPORT_EQUITIES.getCode());
		parameters.put("partState", ParticipantStateType.REGISTERED.getCode());
		parameters.put("idStateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		objects = 	findListByQueryString(sbQuery.toString(),parameters);
		return objects;
	}

	/**
	 * Gets the pending operations details.
	 *
	 * @param guaranteesFilter the guarantees filter
	 * @return the pending operations details
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getPendingOperationsDetails(GuaranteesFilter guaranteesFilter) {
			
		List<Object[]> result = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select  ");
		sbQuery.append(" selpa.idParticipantPk, selpa.mnemonic, "); // 0 1 
		sbQuery.append(" nme.idNegotiationMechanismPk, nme.mechanismName, "); // 2 3 
		sbQuery.append(" nmo.idNegotiationModalityPk, nmo.modalityName, "); // 4 5 
		sbQuery.append(" mo.idMechanismOperationPk,mo.operationNumber,mo.operationDate, "); // 6 7 8
		sbQuery.append(" mo.securities.idSecurityCodePk, mo.stockQuantity, "); // 9 10 
		sbQuery.append(" mo.cashSettlementCurrency, "); // 11
		sbQuery.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = mo.currency), "); //12
		sbQuery.append(" (select nvl(sum(hao.initialMarginAmount),0)   "); 
		sbQuery.append("   from HolderAccountOperation hao "); 
		sbQuery.append("   where hao.operationPart = :termPart  "); 
		sbQuery.append("   and hao.role = :purchaseRole  ");
		sbQuery.append("   and hao.marginReference is null  ");
		sbQuery.append("   and hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk  "); 
		sbQuery.append(" ) as missing_margin , "); // 13
		sbQuery.append(" mo.marginReference, "); // 14
		sbQuery.append(" selpa.description, "); // 15
		sbQuery.append(" (select nvl(sum(hao.initialMarginAmount),0)   "); 
		sbQuery.append("   from HolderAccountOperation hao "); 
		sbQuery.append("   where hao.operationPart = :termPart "); 
		sbQuery.append("   and hao.role = :purchaseRole ");
		sbQuery.append("   and hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk  "); 
		sbQuery.append(" ) as total_margin "); // 16
		sbQuery.append(" from MechanismOperation mo inner join mo.sellerParticipant selpa ");
		sbQuery.append(" inner join mo.mechanisnModality nm ");
		sbQuery.append(" inner join nm.negotiationMechanism nme ");
		sbQuery.append(" inner join nm.negotiationModality nmo ");
		sbQuery.append(" where nmo.indMarginGuarantee  = :indGuarantees ");
		if(guaranteesFilter.getDateType().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
			sbQuery.append(" and trunc(mo.cashSettlementDate) = :cashSettlementDate ");
			parameters.put("cashSettlementDate", guaranteesFilter.getSettlementDate());
		}
		if(guaranteesFilter.getDateType().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			sbQuery.append(" and trunc(mo.termSettlementDate) = :termSettlementDate ");
			parameters.put("termSettlementDate", guaranteesFilter.getSettlementDate());
		}
		if(Validations.validateIsNotNullAndPositive(guaranteesFilter.getIdParticipantPk())){
			sbQuery.append(" and selpa.idParticipantPk = :idParticipantPk ");
			parameters.put("idParticipantPk", guaranteesFilter.getIdParticipantPk());
		}
		
		sbQuery.append(" and mo.operationState in (:states) ");
//		sbQuery.append(" and mo.cashStockReference = :complianceSecurities  ");
		sbQuery.append(" and mo.coverageAmount is not null ");
		sbQuery.append(" order by selpa.idParticipantPk,nme.idNegotiationMechanismPk, nmo.idNegotiationModalityPk , mo.idMechanismOperationPk ");
		
		List<Integer> states = new ArrayList<Integer>(2);
		states.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		parameters.put("states",states);
		parameters.put("indGuarantees",ComponentConstant.ONE);
		parameters.put("termPart",ComponentConstant.TERM_PART);
		parameters.put("purchaseRole",ComponentConstant.PURCHARSE_ROLE);
//		parameters.put("complianceSecurities", AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
		
		result = findListByQueryString(sbQuery.toString(), parameters);
		
		return result;
	}
	
	/**
	 * Gets the pending operations after valorization.
	 *
	 * @param guaranteesFilter the guarantees filter
	 * @return the pending operations after valorization
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getPendingOperationsAfterValorization(
			GuaranteesFilter guaranteesFilter) {
			
		List<Object[]> result = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select  ");
		sbQuery.append(" selpa.idParticipantPk, selpa.mnemonic, "); // 0 1 
		sbQuery.append(" nme.idNegotiationMechanismPk, nme.mechanismName, "); // 2 3 
		sbQuery.append(" nmo.idNegotiationModalityPk, nmo.modalityName, "); // 4 5 
		sbQuery.append(" mo.idMechanismOperationPk,mo.operationNumber,mo.operationDate, "); // 6 7 8
		sbQuery.append(" mo.securities.idSecurityCodePk, mo.stockQuantity,mo.termSettlementCurrency, "); // 9 10  11
		sbQuery.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = mo.currency), "); //12
		sbQuery.append(" ( select sum(agv.missingAmount)   "); 
		sbQuery.append("   from AccountOperationValorization agv inner join agv.holderAccountOperation hao "); 
		sbQuery.append("   where agv.state = :pendingState "); 
		sbQuery.append("   and hao.operationPart = 2  "); 
		sbQuery.append("   and hao.role = 1  ");
		sbQuery.append("   and hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk  "); 
		sbQuery.append(" ) as deval, "); // 13
		sbQuery.append(" ( select sum(hao.valuedAmount) "); 
		sbQuery.append("   from HolderAccountOperation hao "); 
		sbQuery.append("   where hao.operationPart = 2  "); 
		sbQuery.append("   and hao.role = 1  ");
		sbQuery.append("   and hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk  "); 
		sbQuery.append(" ) as deval, "); // 14
		sbQuery.append(" mo.coverageAmount, "); // 15
		sbQuery.append(" selpa.description "); // 16
		sbQuery.append(" from MechanismOperation mo inner join mo.sellerParticipant selpa ");
		sbQuery.append(" inner join mo.mechanisnModality nm ");
		sbQuery.append(" inner join nm.negotiationMechanism nme ");
		sbQuery.append(" inner join nm.negotiationModality nmo ");
		sbQuery.append(" where nmo.indMarginGuarantee  = :indGuarantees ");
		if(guaranteesFilter.getDateType().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
			sbQuery.append(" and trunc(mo.cashSettlementDate) = :cashSettlementDate ");
			parameters.put("cashSettlementDate", guaranteesFilter.getSettlementDate());
		}
		if(guaranteesFilter.getDateType().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			sbQuery.append(" and trunc(mo.termSettlementDate) = :termSettlementDate ");
			parameters.put("termSettlementDate", guaranteesFilter.getSettlementDate());
		}
		if(Validations.validateIsNotNullAndPositive(guaranteesFilter.getIdParticipantPk())){
			sbQuery.append(" and selpa.idParticipantPk = :idParticipantPk ");
			parameters.put("idParticipantPk", guaranteesFilter.getIdParticipantPk());
		}
		
		sbQuery.append(" and mo.operationState in (:states) ");
		sbQuery.append(" order by selpa.idParticipantPk,nme.idNegotiationMechanismPk, nmo.idNegotiationModalityPk , mo.idMechanismOperationPk ");
		
		List<Integer> states = new ArrayList<Integer>(1);
		states.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		parameters.put("states",states);
		parameters.put("indGuarantees",1);
		//parameters.put("complianceMargin", AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
		//parameters.put("uncoveredState", AccountValorizationStateType.UNCOVERED.getCode());
		parameters.put("pendingState", AccountValorizationStateType.PENDING.getCode());
		//parameters.put("fulfilledState", AccountValorizationStateType.FULFILLED.getCode());
		
		result = findListByQueryString(sbQuery.toString(), parameters);
		
		return result;
	}


	/**
	 * Gets the pending accounts by op.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param isInitialGuarantees the is initial guarantees
	 * @return the pending accounts by op
	 */
	public List<Object[]> getPendingAccountsByOp(Long idMechanismOperationPk, boolean isInitialGuarantees) {
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct ha.idHolderAccountPk, hao.stockQuantity,cashHao.coverageAmount,cashHao.initialMarginAmount, "); // 0 1 2 3
		sbQuery.append(" hao.settlementAmount, "); // 4 term amount
		sbQuery.append(" cashHao.settlementAmount, "); // 5 cash amount
		sbQuery.append(" hao.idHolderAccountOperationPk, "); //6
		sbQuery.append(" cashHao.idHolderAccountOperationPk, ");//7
		sbQuery.append(" hao.inchargeStockParticipant.idParticipantPk, ");//8
//		sbQuery.append(" ( select nvl(bal.totalGuarantee,0) ");
//		sbQuery.append("  from AccountGuaranteeBalance bal where ");
//		sbQuery.append("  bal.holderAccountOperation.idHolderAccountOperationPk = hao.idHolderAccountOperationPk ");
//		sbQuery.append(" ) as totalGuarBal,  "); 
		sbQuery.append(" cashHao.valuedAmount "); //9
		sbQuery.append(" from HolderAccountOperation hao ");
		sbQuery.append(" inner join hao.refAccountOperation cashHao  ");
		sbQuery.append(" inner join hao.holderAccount ha ");
		sbQuery.append(" where hao.role = :buyRole ");
		sbQuery.append(" and hao.operationPart = :termPart ");
		if(isInitialGuarantees){
			sbQuery.append(" and cashHao.marginReference is null ");
		}
		sbQuery.append(" and hao.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk ");
		sbQuery.append(" order by hao.operationPart, hao.role desc ");
		
		parameters.put("idMechanismOperationPk",idMechanismOperationPk);
		parameters.put("buyRole", ComponentConstant.PURCHARSE_ROLE);
		//parameters.put("sellRole", ComponentConstant.SALE_ROLE);
		//parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("termPart", ComponentConstant.TERM_PART);
		
		objects = 	findListByQueryString(sbQuery.toString(),parameters);
		return objects;
	}

	/**
	 * Gets the mechanism operation details.
	 *
	 * @param idMechanismOprPk the id mechanism opr pk
	 * @return the mechanism operation details
	 */
	public MechanismOperation getMechanismOperationDetails(Long idMechanismOprPk) {
		MechanismOperation obj = null;
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select mech ");
		sbQuery.append(" from MechanismOperation mech ");
		sbQuery.append(" inner join fetch mech.mechanisnModality mecmod ");
		
		sbQuery.append(" inner join fetch mecmod.negotiationModality negmod ");
		
		sbQuery.append(" inner join fetch mecmod.negotiationMechanism negmech ");
		sbQuery.append(" inner join fetch mech.buyerParticipant buypart ");
		sbQuery.append(" inner join fetch mech.sellerParticipant sellpart ");
		sbQuery.append(" inner join fetch mech.sellerParticipant sellpart ");
		sbQuery.append(" inner join fetch mech.securities sec ");
		sbQuery.append(" where mech.idMechanismOperationPk = :idTradeOprPk");
		parameters.put("idTradeOprPk", idMechanismOprPk);
		try {
			obj = (MechanismOperation) findObjectByQueryString(sbQuery.toString(), parameters);
		} catch (NoResultException e) {
			logger.error(" No se encontro MechanismOperation con param " + parameters);
		} catch (NonUniqueResultException e) {
			logger.error(" Se encontro mas de un MechanismOperation " + parameters);
		}
		return obj;
	}

	/**
	 * Gets the institution cash accounts details.
	 *
	 * @param idStockParticipantPk the id stock participant pk
	 * @param currency the currency
	 * @return the institution cash accounts details
	 */
	public List<Object[]> getInstitutionCashAccountsDetails(
			Long idStockParticipantPk, Integer currency) {
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct ica.idInstitutionCashAccountPk, ica.totalAmount, ica.availableAmount, ica.accountType, "); // 0 1 2 3
		sbQuery.append(" (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ica.accountType), ");  // 4
		sbQuery.append(" iba.idInstitutionBankAccountPk, iba.accountNumber, "); // 5 6
		sbQuery.append(" iba.providerBank.idBankPk "); // 7
		sbQuery.append(" from "); 
		sbQuery.append(" InstitutionCashAccount ica, CashAccountDetail cad, InstitutionBankAccount iba ");
		sbQuery.append(" where ica.idInstitutionCashAccountPk = cad.institutionCashAccount.idInstitutionCashAccountPk ");
		sbQuery.append(" and iba.idInstitutionBankAccountPk = cad.institutionBankAccount.idInstitutionBankAccountPk ");
		sbQuery.append(" and ica.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append(" and ica.accountType = :accountType ");
		sbQuery.append(" and ica.accountState = :accountState ");
		sbQuery.append(" and cad.accountState = :detailState ");
		sbQuery.append(" and ica.currency = :currency ");
		
		parameters.put("idParticipantPk", idStockParticipantPk);
		parameters.put("accountType", AccountCashFundsType.GUARANTEES.getCode());
		parameters.put("accountState", CashAccountStateType.ACTIVATE.getCode());
		parameters.put("detailState", DetailCashAccountStateType.ACTIVATE.getCode());
		parameters.put("currency",currency);
		
		objects = findListByQueryString(sbQuery.toString(),parameters);
		return objects;
	}

	/**
	 * Gets the supported account balances.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param idParticipantPk the id participant pk
	 * @param idHolderAccountPk the id holder account pk
	 * @return the supported account balances
	 */
	public List<Object[]> getSupportedAccountBalances(
			Long mechanismId, Long modalityId,Long idParticipantPk, Long idHolderAccountPk) {
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct ");
		sbQuery.append(" hab.totalBalance, "); //0
		sbQuery.append(" hab.availableBalance, "); //1
		sbQuery.append(" sec.idSecurityCodePk, "); // 2
		sbQuery.append(" sgvc.gvcList.groupPercent, "); // 3;
		sbQuery.append(" sec.currentNominalValue "); // 4
		sbQuery.append(" from "); 
		sbQuery.append(" HolderAccountBalance hab inner join hab.security sec ");
		sbQuery.append(" inner join sec.securityGvcLists sgvc ");
		sbQuery.append(" where hab.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append(" and hab.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append(" and sgvc.securityGvcState = :securityGvcState ");
		sbQuery.append(" and sgvc.gvcList.mechanismModality.id.idNegotiationMechanismPk = :mechanismId ");
		//sbQuery.append(" and sgvc.gvcList.mechanismModality.id.idNegotiationModalityPk = :modalityId "); // allow RF and RV
		
		parameters.put("idParticipantPk", idParticipantPk);
		parameters.put("idHolderAccountPk", idHolderAccountPk);
		parameters.put("securityGvcState", SecurityGvcStateType.ENABLED.getCode());
		parameters.put("mechanismId", mechanismId);
		//parameters.put("modalityId", modalityId);
		
		objects = findListByQueryString(sbQuery.toString(),parameters);
		return objects;
	}

	/**
	 * Save initial guarantee operations.
	 *
	 * @param accountOperationCashPartPk the account operation cash part pk
	 * @param accountOperationTermPartPk the account operation term part pk
	 * @param lstGuaranteeOperations the lst guarantee operations
	 * @throws ServiceException the service exception
	 */
	public void saveInitialGuaranteeOperations(
			Long accountOperationCashPartPk, Long accountOperationTermPartPk, List<GuaranteeOperation> lstGuaranteeOperations) throws ServiceException {
		
		saveAll(lstGuaranteeOperations);
		
		executeGuaranteeComponents(lstGuaranteeOperations, BusinessProcessType.GUARANTEE_DEPOSIT_REGISTER.getCode());
		
		HolderAccountOperation cashToUpdate  = find(HolderAccountOperation.class, accountOperationCashPartPk);
		cashToUpdate.setInitialMarginDate(CommonsUtilities.currentDate());
		cashToUpdate.setMarginReference(AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
		
		HolderAccountOperation termToUpdate  = find(HolderAccountOperation.class, accountOperationTermPartPk);
		termToUpdate.setInitialMarginDate(CommonsUtilities.currentDate());
		termToUpdate.setMarginReference(AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
	}
	
	/**
	 * Save guarantee operations.
	 *
	 * @param lstGuaranteeOperations the lst guarantee operations
	 * @throws ServiceException the service exception
	 */
	public void saveGuaranteeOperations(List<GuaranteeOperation> lstGuaranteeOperations) throws ServiceException {
		saveAll(lstGuaranteeOperations);
		executeGuaranteeComponents(lstGuaranteeOperations, BusinessProcessType.GUARANTEE_DEPOSIT_REGISTER.getCode());
	}
	
	/**
	 * Save withdrawal guarantee operations.
	 *
	 * @param lstGuaranteeOperations the lst guarantee operations
	 * @throws ServiceException the service exception
	 */
	public void saveWithdrawalGuaranteeOperations(List<GuaranteeOperation> lstGuaranteeOperations) throws ServiceException {
		saveAll(lstGuaranteeOperations);
		executeGuaranteeComponents(lstGuaranteeOperations, BusinessProcessType.GUARANTEE_RETIREMENT_REGISTER.getCode());
	}
		
	/**
	 * Execute guarantee components.
	 *
	 * @param lstGuaranteeOperations the lst guarantee operations
	 * @param businessType the business type
	 * @throws ServiceException the service exception
	 */
	public void executeGuaranteeComponents(List<GuaranteeOperation> lstGuaranteeOperations , Long businessType) throws ServiceException{
		for(GuaranteeOperation guaranteeOperation : lstGuaranteeOperations){
			
			if(guaranteeOperation.getGuaranteeClass().equals(GuaranteeClassType.FUNDS.getCode())){
				
				for(FundsOperation fundsOperation : guaranteeOperation.getFundsOperations()){
					
					FundsComponentTO objFundsComponentTO = new FundsComponentTO();
					objFundsComponentTO.setCashAmount(fundsOperation.getOperationAmount());
					objFundsComponentTO.setIdBusinessProcess(businessType );
					objFundsComponentTO.setIdFundsOperation(fundsOperation.getIdFundsOperationPk());
					objFundsComponentTO.setIdFundsOperationType(fundsOperation.getFundsOperationType());
					objFundsComponentTO.setIdInstitutionCashAccount(fundsOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
					objFundsComponentTO.setIdGuaranteeOperation(guaranteeOperation.getIdGuaranteeOperationPk());
					fundsComponentService.get().executeFundsComponent(objFundsComponentTO);
				}
				
			}else if(guaranteeOperation.getGuaranteeClass().equals(GuaranteeClassType.SECURITIES.getCode())){
				
				AccountsComponentTO objAccountComponent = new AccountsComponentTO();
				objAccountComponent.setIdBusinessProcess(businessType);
				objAccountComponent.setIdGuaranteeOperation(guaranteeOperation.getIdGuaranteeOperationPk());
				objAccountComponent.setIdOperationType(guaranteeOperation.getOperationType());
				objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
//				objAccountComponent.setIdTradeOperation(guaranteeOperation.getHolderAccountOperation().
//						getMechanismOperation().getIdMechanismOperationPk());
				
				List<HolderAccountBalanceTO> lstSourceAccounts = new ArrayList<HolderAccountBalanceTO>(0);
				HolderAccountBalanceTO holderAccountBalanceTo = new HolderAccountBalanceTO();
				holderAccountBalanceTo.setIdHolderAccount(guaranteeOperation.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTo.setIdParticipant(guaranteeOperation.getParticipant().getIdParticipantPk());
				holderAccountBalanceTo.setIdSecurityCode(guaranteeOperation.getSecurities().getIdSecurityCodePk());
				holderAccountBalanceTo.setStockQuantity(guaranteeOperation.getGuaranteeBalance());
				
				if(ComponentConstant.ONE.equals(objAccountComponent.getIndMarketFact())){
					if(guaranteeOperation.getGuaranteeMarketFactOperations()!=null){
						holderAccountBalanceTo.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
						for(GuaranteeMarketFactOperation guaranteeMktFact : guaranteeOperation.getGuaranteeMarketFactOperations()){
							MarketFactAccountTO mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(guaranteeMktFact.getMarketDate());
							mktFact.setMarketPrice(guaranteeMktFact.getMarketPrice());
							mktFact.setMarketRate(guaranteeMktFact.getMarketRate());
							mktFact.setQuantity(guaranteeMktFact.getQuantity());
							holderAccountBalanceTo.getLstMarketFactAccounts().add(mktFact);
						}
					}
				}
				
				lstSourceAccounts.add(holderAccountBalanceTo);
				objAccountComponent.setLstSourceAccounts(lstSourceAccounts);
				
				accountsComponentService.get().executeAccountsComponent(objAccountComponent);
			}
			
		}
	}

	/**
	 * Update operation reference.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @throws ServiceException the service exception
	 */
	public void updateOperationReference(Long idMechanismOperationPk) throws ServiceException {
		MechanismOperation mechanismOperation = find(MechanismOperation.class,idMechanismOperationPk);
		mechanismOperation.setMarginReference( AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
		update(mechanismOperation);
	}

	/**
	 * Gets the latest quotation price.
	 *
	 * @param idMechanism the id mechanism
	 * @param idSecurityCodePk the id security code pk
	 * @param maxQuoteDate the max quote date
	 * @return the latest quotation price
	 */
	public BigDecimal getLatestQuotationPrice(Long idMechanism, String idSecurityCodePk, Date maxQuoteDate) {
		StringBuilder sbQuery = new StringBuilder();
		//sbQuery.append(" Select max(view.closePrice) from ( ");
		sbQuery.append(" select  q.closePrice from Quotation q  ");
		sbQuery.append(" Where q.security.idSecurityCodePk  = :idSecurityCodePkPrm ");
		sbQuery.append(" And trunc(q.quotationDate) <= :quotationDatePrm ");
		sbQuery.append(" And q.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPrm ");
		sbQuery.append(" order by q.quotationDate desc ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("idSecurityCodePkPrm", idSecurityCodePk);
		query.setParameter("quotationDatePrm", maxQuoteDate);
		query.setParameter("idNegotiationMechanismPrm", idMechanism);
		
		if(query.getResultList().size() > 0){
			return (BigDecimal)query.getResultList().get(0);
		} else {
			logger.error("el valor "+idSecurityCodePk+" no posee cotizacion. mecanismo:"+idMechanism );
		} 		
		
		return null;
	}
	
	/**
	 * Gets the guarantee balances in securities.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param idAccountGuarBalancePk the id account guar balance pk
	 * @return the guarantee balances in securities
	 */
	public Object[] getGuaranteeBalancesInSecurities(Long mechanismId, Long modalityId, Long idAccountGuarBalancePk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct ");
		sbQuery.append(" sec.idSecurityCodePk, "); // 0 
		sbQuery.append(" sgvc.gvcList.groupPercent, "); // 1;
		sbQuery.append(" sec.currentNominalValue, "); // 2
		sbQuery.append(" agb.idAccountGuarBalancePk, "); // 3
		sbQuery.append(" agb.holderAccount.accountNumber, "); // 4
		sbQuery.append(" agb.totalGuarantee, "); // 5
		sbQuery.append(" agb.marginGuarantee, "); // 6
		sbQuery.append(" agb.marginDividendsGuarantee "); // 7
		sbQuery.append(" from "); 
		sbQuery.append(" AccountGuaranteeBalance agb inner join agb.securities sec ");
		sbQuery.append(" inner join sec.securityGvcLists sgvc ");
		sbQuery.append(" where agb.idAccountGuarBalancePk= :idAccountGuarBalancePk ");
		sbQuery.append(" and sgvc.securityGvcState = :securityGvcState ");
		sbQuery.append(" and sgvc.gvcList.mechanismModality.id.idNegotiationMechanismPk = :mechanismId ");
		sbQuery.append(" and sgvc.gvcList.mechanismModality.id.idNegotiationModalityPk = :modalityId ");
		
		parameters.put("idAccountGuarBalancePk", idAccountGuarBalancePk);
		parameters.put("securityGvcState", SecurityGvcStateType.ENABLED.getCode());
		parameters.put("mechanismId", mechanismId);
		parameters.put("modalityId", modalityId);
		
		return (Object[]) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the guarantee balances by params.
	 *
	 * @param idAccountOperationPk the id account operation pk
	 * @return the guarantee balances by params
	 */
	public List<AccountGuaranteeBalance> getGuaranteeBalancesByParams(Long idAccountOperationPk) {
		
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select agb from AccountGuaranteeBalance agb left join fetch agb.institutionCashAccount ");
		sbQuery.append(" where agb.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperationPk ");
		
		parameters.put("idHolderAccountOperationPk", idAccountOperationPk);
		
		return findListByQueryString(sbQuery.toString(),parameters);
	}
	
	
	/**
	 * Gets the institution cash accounts details by pk.
	 *
	 * @param idInstitutionCashAccountPk the id institution cash account pk
	 * @return the institution cash accounts details by pk
	 */
	public Object[] getInstitutionCashAccountsDetailsByPk(Long idInstitutionCashAccountPk) {
		Object[] objects = null;
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct ica.idInstitutionCashAccountPk, ica.totalAmount, ica.availableAmount, ica.accountType, "); // 0 1 2 3
		sbQuery.append(" iba.idInstitutionBankAccountPk, iba.accountNumber, "); // 4 5 
		sbQuery.append(" iba.providerBank.idBankPk "); // 6
		sbQuery.append(" from "); 
		sbQuery.append(" InstitutionCashAccount ica, CashAccountDetail cad, InstitutionBankAccount iba ");
		sbQuery.append(" where ica.idInstitutionCashAccountPk = cad.institutionCashAccount.idInstitutionCashAccountPk ");
		sbQuery.append(" and iba.idInstitutionBankAccountPk = cad.institutionCashAccount.idInstitutionBankAccountPk ");
		sbQuery.append(" where ica.idInstitutionCashAccountPk = :idInstitutionCashAccountPk ");
		
		parameters.put("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
		
		objects = (Object[]) findObjectByQueryString(sbQuery.toString(),parameters);
		return objects;
	}

	/**
	 * Find previous account valorizations.
	 *
	 * @param idAccountOperationPk the id account operation pk
	 * @param refDate the ref date
	 * @param state the state
	 * @return the list
	 */
	public List<AccountOperationValorization> findPreviousAccountValorizations(
			Long idAccountOperationPk, Date refDate, Integer state) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select aov ");
		sbQuery.append(" from "); 
		sbQuery.append(" AccountOperationValorization aov  ");
		sbQuery.append(" where aov.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperationPk ");
		sbQuery.append(" and aov.state = :aovState ");
		//sbQuery.append(" and aov.situation = :aovSituation ");
		sbQuery.append(" and trunc(aov.valorizationDate) <= :refDate ");
		parameters.put("idHolderAccountOperationPk", idAccountOperationPk);
		parameters.put("aovState", state );
		parameters.put("refDate", refDate);
		
		return findListByQueryString(sbQuery.toString(),parameters);
	}

	/**
	 * Update account operation valorization.
	 *
	 * @param idAccountOperationPk the id account operation pk
	 * @param totalValorization the total valorization
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateAccountOperationValorization(Long idAccountOperationPk,
			BigDecimal totalValorization, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE HolderAccountOperation set valuedAmount = :valuedAmount ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idHolderAccountOperationPk = :idHolderAccountOperationPk ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idHolderAccountOperationPk", idAccountOperationPk);
		query.setParameter("valuedAmount", totalValorization);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}

	/**
	 * Gets the list participant for state.
	 *
	 * @param state the state
	 * @return the list participant for state
	 */
	public List<Participant> getListParticipantForState(Integer state) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select p from Participant p where p.state = :state ");
				
		TypedQuery<Participant> query = em.createQuery(sbQuery.toString(),Participant.class);
		
		if (Validations.validateIsNotNullAndPositive(state)){
			query.setParameter("state", state);
			return query.getResultList();	
		}else{
			return new ArrayList<Participant>();
		}
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param idParticipant the id participant
	 * @return the participant
	 */
	public Participant getParticipant(Long idParticipant){
		return find(Participant.class, idParticipant);
	}
	
	/**
	 * Gets the find institution cash account.
	 *
	 * @param idInstitutionCash the id institution cash
	 * @return the find institution cash account
	 */
	public InstitutionCashAccount getFindInstitutionCashAccount(Long idInstitutionCash) {
		return find(InstitutionCashAccount.class, idInstitutionCash);
	}
	
	/**
	 * Update settlement operation margin.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateSettlementOperationMargin(Long idMechanismOperationPk, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE SettlementOperation seoper set seoper.marginReference = :marginReference ");
		stringBuffer.append(" , seoper.lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , seoper.lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , seoper.lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , seoper.lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE seoper.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk ");
		stringBuffer.append(" and seoper.operationPart = :operationPart ");	
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("operationPart", ComponentConstant.CASH_PART);
		query.setParameter("marginReference", AccountOperationReferenceType.COMPLIANCE_MARGIN.getCode());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());		
		return query.executeUpdate();
	}

}
