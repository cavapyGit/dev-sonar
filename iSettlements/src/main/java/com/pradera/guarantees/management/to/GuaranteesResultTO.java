package com.pradera.guarantees.management.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.guarantees.AccountOperationValorization;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GuaranteesResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class GuaranteesResultTO extends GuaranteesFilterByOper implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder account. */
	// per account
	private HolderAccount holderAccount;
	
	/** The acc coverage amount. */
	private BigDecimal accCoverageAmount;
	
	/** The acc margin amount. */
	private BigDecimal accMarginAmount;
	
	/** The acc missing amount. */
	private BigDecimal accMissingAmount;
	
	/** The acc surplus amount. */
	private BigDecimal accSurplusAmount;
	
	/** The acc margin amount sum. */
	private BigDecimal accMarginAmountSum;
	
	/** The acc stock quantity. */
	private BigDecimal accStockQuantity;
	
	/** The acc cash amount. */
	private BigDecimal accCashAmount;
	
	/** The acc term amount. */
	private BigDecimal accTermAmount;
	
	/** The acc deposited margin amount. */
	private BigDecimal accDepositedMarginAmount;
	
	/** The acc stock reference. */
	private BigDecimal accStockReference;
	
	/** The acc margin reference. */
	private BigDecimal accMarginReference;
	
	/** The acc valued amount. */
	private BigDecimal accValuedAmount;
	
	/** The id account operation pk. */
	private Long idAccountOperationPk;   // parte plazo
	
	/** The id account operation ref pk. */
	private Long idAccountOperationRefPk; // parte contado
	
	/** The id stock participant pk. */
	private Long idStockParticipantPk;
	
	/** The registration type. */
	private Integer registrationType;
	
	/** The lst guarantees in funds. */
	GenericDataModel<GuaranteesRegisterTO> lstGuaranteesInFunds;
	
	/** The lst guarantees in securities. */
	GenericDataModel<GuaranteesRegisterTO> lstGuaranteesInSecurities;
	
	/** The selected guarantee. */
	private GuaranteesRegisterTO selectedGuarantee;
	
	/** The account valorizations. */
	List<AccountOperationValorization> accountValorizations;
	
	/**
	 * Instantiates a new guarantees result to.
	 */
	public GuaranteesResultTO() {
		super();
	}

	/**
	 * Instantiates a new guarantees result to.
	 *
	 * @param resultByOperation the result by operation
	 */
	public GuaranteesResultTO(GuaranteesFilterByOper resultByOperation) {
		super(resultByOperation.getIdMechanismOperationPk(), resultByOperation.getOperationNumber(), 
				resultByOperation.getOperationDate(), resultByOperation.getIdIsinCodePk(), 
				resultByOperation.getCurrency(), resultByOperation.getCurrencyDescription(), 
				resultByOperation.getStockQuantity(),resultByOperation.getCoverageAmount());
	}

	/**
	 * Gets the acc term amount.
	 *
	 * @return the accTermAmount
	 */
	public BigDecimal getAccTermAmount() {
		return accTermAmount;
	}
	
	/**
	 * Sets the acc term amount.
	 *
	 * @param accTermAmount the accTermAmount to set
	 */
	public void setAccTermAmount(BigDecimal accTermAmount) {
		this.accTermAmount = accTermAmount;
	}
	
	/**
	 * Gets the acc deposited margin amount.
	 *
	 * @return the accDepositedMarginAmount
	 */
	public BigDecimal getAccDepositedMarginAmount() {
		return accDepositedMarginAmount;
	}
	
	/**
	 * Sets the acc deposited margin amount.
	 *
	 * @param accDepositedMarginAmount the accDepositedMarginAmount to set
	 */
	public void setAccDepositedMarginAmount(BigDecimal accDepositedMarginAmount) {
		this.accDepositedMarginAmount = accDepositedMarginAmount;
	}
	
	/**
	 * Gets the acc stock reference.
	 *
	 * @return the accStockReference
	 */
	public BigDecimal getAccStockReference() {
		return accStockReference;
	}
	
	/**
	 * Sets the acc stock reference.
	 *
	 * @param accStockReference the accStockReference to set
	 */
	public void setAccStockReference(BigDecimal accStockReference) {
		this.accStockReference = accStockReference;
	}
	
	/**
	 * Gets the acc margin reference.
	 *
	 * @return the accMarginReference
	 */
	public BigDecimal getAccMarginReference() {
		return accMarginReference;
	}
	
	/**
	 * Sets the acc margin reference.
	 *
	 * @param accMarginReference the accMarginReference to set
	 */
	public void setAccMarginReference(BigDecimal accMarginReference) {
		this.accMarginReference = accMarginReference;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the acc coverage amount.
	 *
	 * @return the accCoverageAmount
	 */
	public BigDecimal getAccCoverageAmount() {
		return accCoverageAmount;
	}
	
	/**
	 * Sets the acc coverage amount.
	 *
	 * @param accCoverageAmount the accCoverageAmount to set
	 */
	public void setAccCoverageAmount(BigDecimal accCoverageAmount) {
		this.accCoverageAmount = accCoverageAmount;
	}
	
	/**
	 * Gets the acc margin amount.
	 *
	 * @return the accMarginAmount
	 */
	public BigDecimal getAccMarginAmount() {
		return accMarginAmount;
	}
	
	/**
	 * Sets the acc margin amount.
	 *
	 * @param accMarginAmount the accMarginAmount to set
	 */
	public void setAccMarginAmount(BigDecimal accMarginAmount) {
		this.accMarginAmount = accMarginAmount;
	}
	
	/**
	 * Gets the acc stock quantity.
	 *
	 * @return the accStockQuantity
	 */
	public BigDecimal getAccStockQuantity() {
		return accStockQuantity;
	}
	
	/**
	 * Sets the acc stock quantity.
	 *
	 * @param accStockQuantity the accStockQuantity to set
	 */
	public void setAccStockQuantity(BigDecimal accStockQuantity) {
		this.accStockQuantity = accStockQuantity;
	}

	/**
	 * Gets the acc cash amount.
	 *
	 * @return the accCashAmount
	 */
	public BigDecimal getAccCashAmount() {
		return accCashAmount;
	}
	
	/**
	 * Sets the acc cash amount.
	 *
	 * @param accCashAmount the accCashAmount to set
	 */
	public void setAccCashAmount(BigDecimal accCashAmount) {
		this.accCashAmount = accCashAmount;
	}
	
	/**
	 * Gets the id account operation pk.
	 *
	 * @return the idAccountOperationPk
	 */
	public Long getIdAccountOperationPk() {
		return idAccountOperationPk;
	}
	
	/**
	 * Sets the id account operation pk.
	 *
	 * @param idAccountOperationPk the idAccountOperationPk to set
	 */
	public void setIdAccountOperationPk(Long idAccountOperationPk) {
		this.idAccountOperationPk = idAccountOperationPk;
	}
	
	/**
	 * Gets the id account operation ref pk.
	 *
	 * @return the idAccountOperationRefPk
	 */
	public Long getIdAccountOperationRefPk() {
		return idAccountOperationRefPk;
	}
	
	/**
	 * Sets the id account operation ref pk.
	 *
	 * @param idAccountOperationRefPk the idAccountOperationRefPk to set
	 */
	public void setIdAccountOperationRefPk(Long idAccountOperationRefPk) {
		this.idAccountOperationRefPk = idAccountOperationRefPk;
	}
	
	/**
	 * Gets the id stock participant pk.
	 *
	 * @return the idStockParticipantPk
	 */
	public Long getIdStockParticipantPk() {
		return idStockParticipantPk;
	}
	
	/**
	 * Sets the id stock participant pk.
	 *
	 * @param idStockParticipantPk the idStockParticipantPk to set
	 */
	public void setIdStockParticipantPk(Long idStockParticipantPk) {
		this.idStockParticipantPk = idStockParticipantPk;
	}
	
	/**
	 * Gets the lst guarantees in funds.
	 *
	 * @return the lstGuaranteesInFunds
	 */
	public GenericDataModel<GuaranteesRegisterTO> getLstGuaranteesInFunds() {
		return lstGuaranteesInFunds;
	}
	
	/**
	 * Sets the lst guarantees in funds.
	 *
	 * @param lstGuaranteesInFunds the lstGuaranteesInFunds to set
	 */
	public void setLstGuaranteesInFunds(
			GenericDataModel<GuaranteesRegisterTO> lstGuaranteesInFunds) {
		this.lstGuaranteesInFunds = lstGuaranteesInFunds;
	}
	
	/**
	 * Gets the lst guarantees in securities.
	 *
	 * @return the lstGuaranteesInSecurities
	 */
	public GenericDataModel<GuaranteesRegisterTO> getLstGuaranteesInSecurities() {
		return lstGuaranteesInSecurities;
	}
	
	/**
	 * Sets the lst guarantees in securities.
	 *
	 * @param lstGuaranteesInSecurities the lstGuaranteesInSecurities to set
	 */
	public void setLstGuaranteesInSecurities(
			GenericDataModel<GuaranteesRegisterTO> lstGuaranteesInSecurities) {
		this.lstGuaranteesInSecurities = lstGuaranteesInSecurities;
	}
	
	/**
	 * Gets the acc margin amount sum.
	 *
	 * @return the accMarginAmountSum
	 */
	public BigDecimal getAccMarginAmountSum() {
		return accMarginAmountSum;
	}
	
	/**
	 * Sets the acc margin amount sum.
	 *
	 * @param accMarginAmountSum the accMarginAmountSum to set
	 */
	public void setAccMarginAmountSum(BigDecimal accMarginAmountSum) {
		this.accMarginAmountSum = accMarginAmountSum;
	}

	/**
	 * Gets the acc missing amount.
	 *
	 * @return the accMissingAmount
	 */
	public BigDecimal getAccMissingAmount() {
		return accMissingAmount;
	}

	/**
	 * Sets the acc missing amount.
	 *
	 * @param accMissingAmount the accMissingAmount to set
	 */
	public void setAccMissingAmount(BigDecimal accMissingAmount) {
		this.accMissingAmount = accMissingAmount;
	}

	/**
	 * Gets the registration type.
	 *
	 * @return the registrationType
	 */
	public Integer getRegistrationType() {
		return registrationType;
	}

	/**
	 * Sets the registration type.
	 *
	 * @param registrationType the registrationType to set
	 */
	public void setRegistrationType(Integer registrationType) {
		this.registrationType = registrationType;
	}

	/**
	 * Gets the account valorizations.
	 *
	 * @return the accountValorizations
	 */
	public List<AccountOperationValorization> getAccountValorizations() {
		return accountValorizations;
	}

	/**
	 * Sets the account valorizations.
	 *
	 * @param accountValorizations the accountValorizations to set
	 */
	public void setAccountValorizations(
			List<AccountOperationValorization> accountValorizations) {
		this.accountValorizations = accountValorizations;
	}

	/**
	 * Gets the acc valued amount.
	 *
	 * @return the accValuedAmount
	 */
	public BigDecimal getAccValuedAmount() {
		return accValuedAmount;
	}

	/**
	 * Sets the acc valued amount.
	 *
	 * @param accValuedAmount the accValuedAmount to set
	 */
	public void setAccValuedAmount(BigDecimal accValuedAmount) {
		this.accValuedAmount = accValuedAmount;
	}

	/**
	 * Gets the acc surplus amount.
	 *
	 * @return the accSurplusAmount
	 */
	public BigDecimal getAccSurplusAmount() {
		return accSurplusAmount;
	}

	/**
	 * Sets the acc surplus amount.
	 *
	 * @param accSurplusAmount the accSurplusAmount to set
	 */
	public void setAccSurplusAmount(BigDecimal accSurplusAmount) {
		this.accSurplusAmount = accSurplusAmount;
	}

	/**
	 * Gets the selected guarantee.
	 *
	 * @return the selected guarantee
	 */
	public GuaranteesRegisterTO getSelectedGuarantee() {
		return selectedGuarantee;
	}

	/**
	 * Sets the selected guarantee.
	 *
	 * @param selectedGuarantee the new selected guarantee
	 */
	public void setSelectedGuarantee(GuaranteesRegisterTO selectedGuarantee) {
		this.selectedGuarantee = selectedGuarantee;
	}

}
