package com.pradera.guarantees.management.to;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GuaranteesFilterByMech.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class GuaranteesFilterByMech extends GuaranteesFilter {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id negotiation mechanism pk. */
	// per mechanism
	private Long idNegotiationMechanismPk;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The modality name. */
	private String modalityName;
	
	/** The missing margin. */
	private BigDecimal missingMargin = BigDecimal.ZERO;
	
	/** The list by operations. */
	List<GuaranteesFilterByOper> listByOperations;

	/** The covered list by operations. */
	List<GuaranteesFilterByOper> coveredListByOperations;
	
	/**
	 * Gets the list by operations.
	 *
	 * @return the listByOperations
	 */
	public List<GuaranteesFilterByOper> getListByOperations() {
		return listByOperations;
	}

	/**
	 * Sets the list by operations.
	 *
	 * @param listByOperations the listByOperations to set
	 */
	public void setListByOperations(List<GuaranteesFilterByOper> listByOperations) {
		this.listByOperations = listByOperations;
	}

	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the idNegotiationMechanismPk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}

	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the idNegotiationMechanismPk to set
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}

	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanismName
	 */
	public String getMechanismName() {
		return mechanismName;
	}

	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the mechanismName to set
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the idNegotiationModalityPk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the idNegotiationModalityPk to set
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
	 * Gets the modality name.
	 *
	 * @return the modalityName
	 */
	public String getModalityName() {
		return modalityName;
	}

	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the modalityName to set
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}

	/**
	 * Gets the missing margin.
	 *
	 * @return the missingMargin
	 */
	public BigDecimal getMissingMargin() {
		return missingMargin;
	}

	/**
	 * Sets the missing margin.
	 *
	 * @param missingMargin the missingMargin to set
	 */
	public void setMissingMargin(BigDecimal missingMargin) {
		this.missingMargin = missingMargin;
	}
	
	/**
	 * Gets the covered list by operations.
	 *
	 * @return the coveredListByOperations
	 */
	public List<GuaranteesFilterByOper> getCoveredListByOperations() {
		return coveredListByOperations;
	}

	/**
	 * Sets the covered list by operations.
	 *
	 * @param coveredListByOperations the coveredListByOperations to set
	 */
	public void setCoveredListByOperations(
			List<GuaranteesFilterByOper> coveredListByOperations) {
		this.coveredListByOperations = coveredListByOperations;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((idNegotiationMechanismPk == null) ? 0
						: idNegotiationMechanismPk.hashCode());
		result = prime
				* result
				+ ((idNegotiationModalityPk == null) ? 0
						: idNegotiationModalityPk.hashCode());
		result = prime * result
				+ ((mechanismName == null) ? 0 : mechanismName.hashCode());
		result = prime * result
				+ ((modalityName == null) ? 0 : modalityName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GuaranteesFilterByMech other = (GuaranteesFilterByMech) obj;
		if (idNegotiationMechanismPk == null) {
			if (other.idNegotiationMechanismPk != null)
				return false;
		} else if (!idNegotiationMechanismPk
				.equals(other.idNegotiationMechanismPk))
			return false;
		if (idNegotiationModalityPk == null) {
			if (other.idNegotiationModalityPk != null)
				return false;
		} else if (!idNegotiationModalityPk
				.equals(other.idNegotiationModalityPk))
			return false;
		if (mechanismName == null) {
			if (other.mechanismName != null)
				return false;
		} else if (!mechanismName.equals(other.mechanismName))
			return false;
		if (modalityName == null) {
			if (other.modalityName != null)
				return false;
		} else if (!modalityName.equals(other.modalityName))
			return false;
		return true;
	}	
}
