package com.pradera.negotiations.otcoperations.view;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.massive.type.MechanismFileProcessType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.negotiations.otcoperations.facade.OtcOperationServiceFacade;
import com.pradera.settlements.utils.PropertiesConstants;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OtcOperationMassiveMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class OtcOperationMassiveMgmt extends GenericBaseBean {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The otc operation service facade. */
	@EJB
	OtcOperationServiceFacade otcOperationServiceFacade;

	/** The mcn operation service facade. */
	@EJB
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The upload file types. */
	private String fUploadFileTypes = "xml";
	
	/** The upload file types pattern. */
	private Pattern fUploadFileTypesPattern=Pattern.compile("([^\\s]+(\\.(?i)("+fUploadFileTypes+"))$)");
	
	/** The upload file types display. */
	private String fUploadFileTypesDisplay="*.xml";
	
	/** The participant in session. */
	private Participant participantInSession;

	/** The errors data model. */
	private GenericDataModel<RecordValidationType> errorsDataModel;

	/** The session state is active. */
	private boolean sessionStateIsActive;
	
	/** The otc mechanism. */
	private NegotiationMechanism otcMechanism;
	
	/** The otc process file filter. */
	private ProcessFileTO otcProcessFileFilter;
	
	/** The otc process file. */
	private ProcessFileTO otcProcessFile;
	
	/** The tmp otc process file. */
	private ProcessFileTO tmpOtcProcessFile;
	
	/** The upload type. */
	private Integer uploadType;
	
	/** The otc process files data model. */
	private GenericDataModel<ProcessFileTO> otcProcessFilesDataModel;

	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		setOtcProcessFileFilter(new ProcessFileTO(CommonsUtilities.currentDate()));
		setOtcProcessFile(null);
		setTmpOtcProcessFile(new ProcessFileTO());
		setOtcMechanism(otcOperationServiceFacade.getMechanismServiceFacade(NegotiationMechanismType.OTC.getCode()));
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			participantInSession = accountsFacade.getParticipantStateServiceFacade(
					userInfo.getUserAccountSession().getParticipantCode());
			if(!participantInSession.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				setSessionStateIsActive(Boolean.FALSE);
			}else{
				setSessionStateIsActive(Boolean.TRUE);
			}
		}else if (userInfo.getUserAccountSession().isDepositaryInstitution()){
			setSessionStateIsActive(Boolean.TRUE);
		}
	}
	
	/**
	 * On change process date.
	 */
	public void onChangeProcessDate() {
		setOtcProcessFilesDataModel(null);
		//setOtcProcessFileFilter(new MechanismProcessFileTO(CommonsUtilities.currentDate()));
		tmpOtcProcessFile = new ProcessFileTO();
		otcProcessFile = null;
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
			if (fDisplayName != null) {
				tmpOtcProcessFile.setFileName(fDisplayName);
				tmpOtcProcessFile.setProcessFile(event.getFile().getContents());
				tmpOtcProcessFile.setTempProcessFile(File.createTempFile("tempOtcfile", ".tmp"));
				FileUtils.writeByteArrayToFile(tmpOtcProcessFile.getTempProcessFile(), tmpOtcProcessFile.getProcessFile());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removefile handler.
	 *
	 * @param actionEvent the action event
	 */
	public void removefileHandler(ActionEvent actionEvent) {
		tmpOtcProcessFile.setProcessFile(null);
		tmpOtcProcessFile.setFileName(null);
		if(tmpOtcProcessFile.getTempProcessFile()!=null){
			tmpOtcProcessFile.getTempProcessFile().delete();
			tmpOtcProcessFile.setTempProcessFile(null);
		}
	}
	
	/**
	 * Gets the otc stream content.
	 *
	 * @return the otc stream content
	 */
	public StreamedContent getOtcStreamContent() {
		try {
			return getStreamedContentFromFile(tmpOtcProcessFile.getProcessFile(), null,
					tmpOtcProcessFile.getFileName());
		} catch (IOException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Search processed files.
	 */
	public void searchProcessedFiles() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idNegotiationMechanismPk", otcMechanism.getIdNegotiationMechanismPk());
		parameters.put("processDate", otcProcessFileFilter.getProcessDate());
		parameters.put("processType",MechanismFileProcessType.OTC_REGISTRATION_UPLOAD.getCode());
		if(participantInSession!=null){
			parameters.put("idParticipantPk", participantInSession.getIdParticipantPk());
		}
		List<ProcessFileTO> mcnProcessFiles = mcnOperationServiceFacade
				.getMcnProcessedFilesInformation(parameters);
		otcProcessFilesDataModel = new GenericDataModel<ProcessFileTO>(
				mcnProcessFiles);
	}
	
	/**
	 * Gets the accepted file content.
	 *
	 * @param otcfile the otcfile
	 * @return the accepted file content
	 */
	public StreamedContent getAcceptedFileContent(ProcessFileTO otcfile) {
		try {
			
			byte[] file = mcnOperationServiceFacade.getAcceptedMcnFileContent(otcfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,
					"accepted_files"+otcfile.getFileName()+".txt");
		} catch (Exception e) {
			showMessageOnDialog(null,null,PropertiesConstants.MCN_MASSIVE_ERROR_DOWNLOADING_FILE,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	

	/**
	 * Gets the rejected file content.
	 *
	 * @param otcfile the otcfile
	 * @return the rejected file content
	 */
	public StreamedContent getRejectedFileContent(ProcessFileTO otcfile) {
		try {
			
			byte[] file = mcnOperationServiceFacade.getRejectedMcnFileContent(otcfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,
					"rejected_files"+otcfile.getFileName()+".txt");
		} catch (Exception e) {
			e.printStackTrace();
			showMessageOnDialog(null,null,PropertiesConstants.MCN_MASSIVE_ERROR_DOWNLOADING_FILE,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	
	
	/**
	 * Validate otc file.
	 */
	public void validateOtcFile() {
		
		int countValidationErrors = 0;
		
		if(Validations.validateIsNull(tmpOtcProcessFile.getTempProcessFile())){
    		JSFUtilities.addContextMessage(":frmMassiveOTC:fuplOtcFile",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MCNFILE_NULL), 
    				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MCNFILE_NULL));
			countValidationErrors++;
    	}
		
		if(countValidationErrors > 0) {
    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
    		return;
    	}
		
		try {
			tmpOtcProcessFile.setRootTag(NegotiationConstant.MASSIVE_XML_OTC_ROOT_TAG);
			tmpOtcProcessFile.setIdNegotiationMechanismPk(otcMechanism.getIdNegotiationMechanismPk());
			if(participantInSession!=null){
				tmpOtcProcessFile.setIdParticipantPk(participantInSession.getIdParticipantPk());
			}
			tmpOtcProcessFile.setStreamFileDir(NegotiationConstant.OTC_OPERATION_STREAM);
			CommonsUtilities.validateFileOperation(tmpOtcProcessFile);
			
			ValidationProcessTO otcValidationTO = tmpOtcProcessFile.getValidationProcessTO();
			
			errorsDataModel = new GenericDataModel<RecordValidationType>(otcValidationTO==null?new ArrayList<RecordValidationType>():otcValidationTO.getLstValidations());
			otcProcessFile = tmpOtcProcessFile;
			tmpOtcProcessFile = new ProcessFileTO();			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Before process otc file.
	 */
	public void beforeProcessOtcFile(){	
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_REGISTER_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('cnfwProcessMcnFile').show();");
	}

	/**
	 * Process otc file.
	 */
	@LoggerAuditWeb
	public void processOtcFile(){	
		BusinessProcess businessProcess = new BusinessProcess();
		if(uploadType.equals(GeneralConstants.ONE_VALUE_INTEGER)){
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.OTC_OPERATION_MASSIVE_REGISTER.getCode());
		}else{
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.OTC_OPERATION_MASSIVE_REVIEW.getCode());
		}
		try {
			
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PARAMETER_FILE, otcProcessFile.getTempProcessFile().getAbsolutePath());
			details.put(GeneralConstants.PARAMETER_STREAM, otcProcessFile.getStreamFileDir());
			details.put(GeneralConstants.PARAMETER_REPO, true); // to know if is repo structure.
			//details.put(GeneralConstants.PARAMETER_INCHARGE, otcProcessFile.isInCharge());
			details.put(GeneralConstants.PARAMETER_MECHANISM, otcProcessFile.getIdNegotiationMechanismPk());
			details.put(GeneralConstants.PARAMETER_LOCALE, "es");
			details.put(GeneralConstants.PARAMETER_FILE_NAME, otcProcessFile.getFileName());
			if(otcProcessFile.getIdParticipantPk()!=null){
				details.put(GeneralConstants.PARAMETER_OTC_PART_ID,otcProcessFile.getIdParticipantPk());
			}
			
			batchProcessServiceFacade.registerBatch(
					userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_REGISTER_SUCCESS));
		JSFUtilities.showComponent("cnfEndProcess");
		cleanAll();
	}
	
	/**
	 * Clean all.
	 */
	public void cleanAll() {
		setOtcProcessFileFilter(new ProcessFileTO(CommonsUtilities.currentDate()));
		setTmpOtcProcessFile(new ProcessFileTO());
		setOtcProcessFile(null);
		setOtcProcessFilesDataModel(null);
	}

	/**
	 * Gets the otc process file filter.
	 *
	 * @return the otcProcessFileFilter
	 */
	public ProcessFileTO getOtcProcessFileFilter() {
		return otcProcessFileFilter;
	}

	/**
	 * Sets the otc process file filter.
	 *
	 * @param otcProcessFileFilter the otcProcessFileFilter to set
	 */
	public void setOtcProcessFileFilter(ProcessFileTO otcProcessFileFilter) {
		this.otcProcessFileFilter = otcProcessFileFilter;
	}

	/**
	 * Gets the otc mechanism.
	 *
	 * @return the otcMechanism
	 */
	public NegotiationMechanism getOtcMechanism() {
		return otcMechanism;
	}

	/**
	 * Sets the otc mechanism.
	 *
	 * @param otcMechanism the otcMechanism to set
	 */
	public void setOtcMechanism(NegotiationMechanism otcMechanism) {
		this.otcMechanism = otcMechanism;
	}

	/**
	 * Gets the otc process file.
	 *
	 * @return the otcProcessFile
	 */
	public ProcessFileTO getOtcProcessFile() {
		return otcProcessFile;
	}

	/**
	 * Sets the otc process file.
	 *
	 * @param otcProcessFile the otcProcessFile to set
	 */
	public void setOtcProcessFile(ProcessFileTO otcProcessFile) {
		this.otcProcessFile = otcProcessFile;
	}

	/**
	 * Gets the tmp otc process file.
	 *
	 * @return the tmpOtcProcessFile
	 */
	public ProcessFileTO getTmpOtcProcessFile() {
		return tmpOtcProcessFile;
	}

	/**
	 * Sets the tmp otc process file.
	 *
	 * @param tmpOtcProcessFile the tmpOtcProcessFile to set
	 */
	public void setTmpOtcProcessFile(ProcessFileTO tmpOtcProcessFile) {
		this.tmpOtcProcessFile = tmpOtcProcessFile;
	}

	/**
	 * Gets the otc process files data model.
	 *
	 * @return the otcProcessFilesDataModel
	 */
	public GenericDataModel<ProcessFileTO> getOtcProcessFilesDataModel() {
		return otcProcessFilesDataModel;
	}

	/**
	 * Sets the otc process files data model.
	 *
	 * @param otcProcessFilesDataModel the otcProcessFilesDataModel to set
	 */
	public void setOtcProcessFilesDataModel(GenericDataModel<ProcessFileTO> otcProcessFilesDataModel) {
		this.otcProcessFilesDataModel = otcProcessFilesDataModel;
	}

	/**
	 * Gets the errors data model.
	 *
	 * @return the errorsDataModel
	 */
	public GenericDataModel<RecordValidationType> getErrorsDataModel() {
		return errorsDataModel;
	}

	/**
	 * Sets the errors data model.
	 *
	 * @param errorsDataModel the errorsDataModel to set
	 */
	public void setErrorsDataModel(GenericDataModel<RecordValidationType> errorsDataModel) {
		this.errorsDataModel = errorsDataModel;
	}
	
	/**
	 * Gets the f upload file types display.
	 *
	 * @return the fUploadFileTypesDisplay
	 */
	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	/**
	 * Sets the f upload file types display.
	 *
	 * @param fUploadFileTypesDisplay the fUploadFileTypesDisplay to set
	 */
	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}

	/**
	 * Checks if is session state is active.
	 *
	 * @return the sessionStateIsActive
	 */
	public boolean isSessionStateIsActive() {
		return sessionStateIsActive;
	}

	/**
	 * Sets the session state is active.
	 *
	 * @param sessionStateIsActive the sessionStateIsActive to set
	 */
	public void setSessionStateIsActive(boolean sessionStateIsActive) {
		this.sessionStateIsActive = sessionStateIsActive;
	}

	/**
	 * Gets the upload type.
	 *
	 * @return the uploadType
	 */
	public Integer getUploadType() {
		return uploadType;
	}

	/**
	 * Sets the upload type.
	 *
	 * @param uploadType the uploadType to set
	 */
	public void setUploadType(Integer uploadType) {
		this.uploadType = uploadType;
	}
}
