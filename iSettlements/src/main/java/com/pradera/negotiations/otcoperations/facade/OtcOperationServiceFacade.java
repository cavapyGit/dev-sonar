package com.pradera.negotiations.otcoperations.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.operation.to.MechanismModalityTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationAdj;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.CashCalendarType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.negotiations.operations.to.NegotiationOperationTO;
import com.pradera.negotiations.otcoperations.service.OtcOperationServiceBean;
import com.pradera.negotiations.otcoperations.to.OtcOperationResultTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationTO;
import com.pradera.negotiations.otcoperations.view.OtcOperationParameters;
import com.pradera.negotiations.otcoperations.view.OtcOperationUtils;
import com.pradera.negotiations.sirtex.facade.SirtexOperationFacade;
import com.pradera.negotiations.sirtex.service.OtcAndSirtexSettlementService;
import com.pradera.settlements.core.service.CancellationOperationService;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class OtcOperationServiceFacade {
	
	/** The otc operation service bean.*/
	@Inject private UserInfo userInfo;
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The otc operation service bean. */
	@EJB private OtcOperationServiceBean otcOperationServiceBean;
	
	/** The negotiation operation service bean. */
	@EJB private NegotiationOperationServiceBean negotiationOperationServiceBean;
	
	/** The cancelation service bean. */
	@EJB private CancellationOperationService cancelationServiceBean;
	
	/** The accounts facade. */
	@EJB private AccountsFacade accountsFacade;
	
	/** The helper component facade. */
	@EJB private HelperComponentFacade helperComponentFacade;
	
	/** The holiday query service bean. */
	@EJB private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The account service bean. */
	@EJB private HolderAccountComponentServiceBean accountServiceBean;
	
	/** The parameter service bean. */
	@EJB private ParameterServiceBean parameterServiceBean;
	
	/** The sirtex facade. */
	@EJB private SirtexOperationFacade sirtexFacade;
	
	/** The otc settlement service. */
	@EJB private OtcAndSirtexSettlementService otcSettlementService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/**
	 * Gets the mechanism modality list service facade.
	 *
	 * @param mechanismModalityTO the mechanism modality to
	 * @param parametersDesc the parameters desc
	 * @return the mechanism modality list service facade
	 * @throws ServiceException the service exception
	 */
	public List<MechanismModality> findMechanismModalityList(MechanismModalityTO mechanismModalityTO, Map<Integer,String> parametersDesc) throws ServiceException{
		List<MechanismModality> mecModalities = otcOperationServiceBean.getMechanismModalityListServiceBean(mechanismModalityTO);
		for(MechanismModality mecModality: mecModalities){
			mecModality.setSettlementTypeDescription(SettlementType.get(mecModality.getSettlementType()).getValue());
			mecModality.setSettlementSchemaDescription(SettlementSchemaType.get(mecModality.getSettlementSchema()).getValue());
		}
		return mecModalities;
	}
	
	/**
	 * Find mechanism modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param modalityPk the modality pk
	 * @return the mechanism modality
	 * @throws ServiceException the service exception
	 */
	public MechanismModality findMechanismModality(Long mechanismPk, Long modalityPk) throws ServiceException{
		return otcOperationServiceBean.getMechanismModality(mechanismPk,modalityPk);
	}
	
	/**
	 * Validate reference number.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public void validateReferenceNumber(MechanismOperation mechanismOperation) throws ServiceException{
		boolean exists = negotiationOperationServiceBean.chkOtcOperationReferenceNumber(mechanismOperation);
		if(exists) throw new ServiceException(ErrorServiceType.MECHANISM_OPERATION_REF_NUMB);
	}
	
	/**
	 * Validate holder account investor.
	 *
	 * @param holderAccount the holder account
	 * @param isinCode the isin code
	 * @throws ServiceException the service exception
	 */
	public void validateHolderAccountInvestor(HolderAccount holderAccount, String isinCode) throws ServiceException{
		negotiationOperationServiceBean.validateHolderAssignment(false, holderAccount, isinCode, null);
	}
	
	/**
	 * Registry otc operation service facade.
	 *
	 * @param newOperation the otc mechanism operation
	 * @param swapOperation the swap operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */

	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REGISTER)
	public MechanismOperation registryNewMechanismOperation(MechanismOperation newOperation, SwapOperation swapOperation, MechanismOperationAdj mechanismOperationAdj, LoggerUser loggerUser) throws ServiceException{
		return registryNewMechanismOperationTx(newOperation, swapOperation, mechanismOperationAdj, loggerUser);
	}


	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REGISTER)
	public MechanismOperation registryNewMechanismOperation(MechanismOperation newOperation, SwapOperation swapOperation, MechanismOperationAdj mechanismOperationAdj) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return registryNewMechanismOperationTx(newOperation, swapOperation, mechanismOperationAdj, loggerUser);
	}	
	
	public MechanismOperation registryNewMechanismOperationTx(MechanismOperation newOperation, SwapOperation swapOperation, MechanismOperationAdj mechanismOperationAdj, LoggerUser loggerUser) throws ServiceException{
		
		NegotiationModalityType negModalityType = NegotiationModalityType.get(newOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		newOperation.setRegisterUser(loggerUser.getUserName());
		newOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		newOperation.setOperationDate(CommonsUtilities.currentDateTime());
		newOperation.setOperationState(OtcOperationStateType.REGISTERED.getCode());
		newOperation.setIndCashSettlementExchange(BooleanType.NO.getCode());
		newOperation.setIndTermSettlementExchange(BooleanType.NO.getCode());
		newOperation.setIndDpf(ComponentConstant.ZERO);
		
		/*CREAMOS EL TRADE OPERATION*/
		TradeOperation tradeOperation = new TradeOperation();
		tradeOperation.setRegisterUser(loggerUser.getUserName());
		tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		tradeOperation.setOperationState(OtcOperationStateType.REGISTERED.getCode());
		tradeOperation.setOperationType(negModalityType.getParameterOperationType());
		newOperation.setTradeOperation(tradeOperation);
		
		/*ACTUALIZAMOS INFORMACION EN LOS HOLDER_ACCOUNT_OPERATIONS*/
		for(HolderAccountOperation holderAccountOperation: newOperation.getHolderAccountOperations()){//Recorremos los HolderAccountOperations
			holderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());//Seteamos la fecha de registro
			holderAccountOperation.setRegisterUser(loggerUser.getUserName());//Seteamos el usuario que realiza la transaccion
		}

		/*VALIDAMOS SI ES PERMUTA*/
		if(NegotiationUtils.mechanismOperationIsSwap(newOperation)){
			swapOperation.setMechanismOperation(newOperation);
			if(swapOperation.getRealCashAmount()==null){
				swapOperation.setRealCashAmount(BigDecimal.ZERO);
			}
			/*ACTUALIZAMOS INFORMACION EN LOS HOLDER_ACCOUNT_OPERATIONS*/
			for(HolderAccountOperation accountOperation: swapOperation.getHolderAccountOperations()){
				accountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
				accountOperation.setRegisterUser(loggerUser.getUserName());
			}
			newOperation.getSwapOperations().add(swapOperation);
		}
		
		/*VALIDAMOS LAS ENTIDADES*/
		negotiationOperationServiceBean.validateMechanismOperation(newOperation);
		if(newOperation.getReferenceNumber()!=null && !newOperation.getReferenceNumber().isEmpty()){
			validateReferenceNumber(newOperation);//validate reference number
		}
		
		/*GUARDAMOS OPREACION EN BD*///TODO: crear otro metodo
		return otcOperationServiceBean.createNewMechanismOperation(newOperation, mechanismOperationAdj);		
	}
	
	
	/**
	 * Gets the otc operations service facade.
	 *
	 * @param otcOperationTO the otc operation to
	 * @param maps the maps
	 * @return the otc operations service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_QUERY)
	public List<OtcOperationResultTO> getOtcOperationsServiceFacade(OtcOperationTO otcOperationTO, Map<Integer,String> maps) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<OtcOperationResultTO> otcResultList = otcOperationServiceBean.getOtcOperationsServiceBean(otcOperationTO);
		for(OtcOperationResultTO otcResult: otcResultList){
			otcResult.setStateDescription(maps.get(otcResult.getOperationState()));
			if(! (otcResult.getOperationState().equals(OtcOperationStateType.REGISTERED.getCode()) || 
					otcResult.getOperationState().equals(OtcOperationStateType.APROVED.getCode()) ||
					otcResult.getOperationState().equals(OtcOperationStateType.ANULATE.getCode()) ||
					otcResult.getOperationState().equals(OtcOperationStateType.CANCELED.getCode()) ||
					otcResult.getOperationState().equals(OtcOperationStateType.CASH_CANCELED.getCode()) ||
					otcResult.getOperationState().equals(OtcOperationStateType.REJECTED.getCode())||
					otcResult.getOperationState().equals(OtcOperationStateType.REVIEWED.getCode())||
					otcResult.getOperationState().equals(OtcOperationStateType.AUTHORIZED.getCode())||
					otcResult.getOperationState().equals(OtcOperationStateType.TERM_CANCELED.getCode())) ){
				Integer settlementState = otcSettlementService.getUnSettlementOperationState(otcResult.getIdMechanismOperationRequestPk());
				
				if(settlementState.equals(MechanismOperationStateType.CASH_SETTLED.getCode()) 
						|| settlementState.equals(MechanismOperationStateType.TERM_SETTLED.getCode())) {
					otcResult.setSettlementState(GeneralConstants.ONE_VALUE_INTEGER);
				} else {
					otcResult.setSettlementState(GeneralConstants.ZERO_VALUE_INTEGER);
				}
			}
			otcResult.setSettlementTypeDescription( 
					SettlementType.get(otcResult.getSettlementType()).getMnemonic() );
		}
		return otcResultList;
	}
	
	/**
	 * Gets the otc operations for review service facade.
	 *
	 * @param otcOperationTO the otc operation to
	 * @return the otc operations for review service facade
	 * @throws ServiceException the service exception
	 */
	public List<OtcOperationResultTO> getOtcOperationsForReviewServiceFacade(OtcOperationTO otcOperationTO) throws ServiceException{
		// build mechanismOperationType
		return otcOperationServiceBean.getOtcOperations4ReviewServiceBean(otcOperationTO);
	}
	
	/**
	 * Gets the otc operation service facade.
	 *
	 * @param otcOperationTO the otc operation to
	 * @return the otc operation service facade
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation getOtcOperationServiceFacade(OtcOperationTO otcOperationTO) throws ServiceException{
		return otcOperationServiceBean.getOtcOperationServiceBean(otcOperationTO);
	}
	
	public MechanismOperationAdj getMechanismOperationAdj(OtcOperationTO otcOperationTO) throws ServiceException{
		return otcOperationServiceBean.getMechanismOperationAdj(otcOperationTO.getIdMechanismOperation());
	}
	/**
	 * Confirm otc operation service bean.
	 *
	 * @param operation the operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_CONFIRM)
	public MechanismOperation confirmOtcOperationServiceFacade(MechanismOperation operation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		 
//		negotiationOperationServiceBean.validateMechanismOperation(operation);//Validamos el objeto OTC
//		negotiationOperationServiceBean.validateOtcOperationState(operation, Arrays.asList(OtcOperationStateType.REVIEWED.getCode()));
		
		if (SettlementType.DVP.getCode().equals(operation.getSettlementType()) || SettlementType.FOP.getCode().equals(operation.getSettlementType())){
			negotiationOperationServiceBean.generatePaymentReference(operation);
		}
		
		/**If operation is DPF settlement operation*/
		if(operation.getIndDpf().equals(BooleanType.YES.getCode())){
			operation = otcOperationServiceBean.find(MechanismOperation.class, operation.getIdMechanismOperationPk());
			/**Variable for account operation*/
			HolderAccountOperation accountOperationSale = null;
			HolderAccountOperation accountOperationBuyer = null;
			for(HolderAccountOperation accountOperation: operation.getHolderAccountOperations()){
				if(accountOperation.getRole().equals(ComponentConstant.SALE_ROLE)){
					accountOperationSale = accountOperation;
				}else if(accountOperation.getRole().equals(ComponentConstant.PURCHARSE_ROLE)){
					accountOperationBuyer = accountOperation;
				}
			}
			/**Getting account market fact*/
			Long participantCode = operation.getSellerParticipant().getIdParticipantPk();
			String securityCode  = operation.getSecurities().getIdSecurityCodePk();
			Long accountPk       = accountOperationSale.getHolderAccount().getIdHolderAccountPk();
			/**getting list of account market fact*/
			List<HolderMarketFactBalance> accountMarketFact = getMarketFactBalance(participantCode, securityCode, accountPk);
			/**VERIFICAMOS SI EXISTEN HECHOS DE MERCADO*/
			for(HolderMarketFactBalance accTradeMarketFact: accountMarketFact){
				AccountOperationMarketFact accountMarkFact = new AccountOperationMarketFact();
				accountMarkFact.setMarketDate(accTradeMarketFact.getMarketDate());
				accountMarkFact.setMarketRate(accTradeMarketFact.getMarketRate());
				accountMarkFact.setMarketPrice(accTradeMarketFact.getMarketPrice());
				accountMarkFact.setOperationQuantity(operation.getStockQuantity());
				accountMarkFact.setHolderAccountOperation(accountOperationBuyer);
				
				/**save account operation market fact*/
				otcOperationServiceBean.create(accountMarkFact);
			}
		}
		
		operation = otcOperationServiceBean.confirmOtcMechanismOperation(operation);
		
		/**If operation is DPF settlement operation*/
		if(operation.getMechanisnModality().getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_EQUITIES.getCode()) ||
		   operation.getMechanisnModality().getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_FIXED_INCOME.getCode())){
			/**loock balance of mechanismOperation*/ 
			Integer operationPart = ComponentConstant.CASH_PART;
			Long operationPk 	  = operation.getIdMechanismOperationPk(); 
			otcSettlementService.lockBalancesOfOperation(operationPk, operationPart);
			/**settlement balance of mechanismOperation*/
			otcSettlementService.settlementBalancesOfOperation(operationPk, operationPart, operation.getCashSettlementDate());
			/**Validate if all balances have been settled*/
//			sirtexFacade.validateOperationSettlements(Arrays.asList(operationPk),operationPart);
		}
		return operation;
	}
	
	public void markSettlementOperationAsSettled(Integer operationPart, MechanismOperation mechanismOperation) {
		
		SettlementOperation settlementOperation = otcSettlementService.getSettlementOperation(mechanismOperation.getIdMechanismOperationPk(), operationPart);
		
		List<SettlementOperationTO> lstSettlementOperationTOs=new ArrayList<>();
		SettlementOperationTO operationTO=new SettlementOperationTO();
		operationTO.setIdSettlementOperation(settlementOperation.getIdSettlementOperationPk()); 
		operationTO.setSecurityClass(mechanismOperation.getSecurities().getSecurityClass());                                                   
		lstSettlementOperationTOs.add(operationTO);
		
		sendSettlementOperationWebClient(lstSettlementOperationTOs);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_SETTLEMENT)
	public MechanismOperation settleOtcOperationServiceFacade(MechanismOperation operation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());

		/**If operation is DPF settlement operation*/
		if(operation.getMechanisnModality().getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_EQUITIES.getCode()) ||
		   operation.getMechanisnModality().getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_FIXED_INCOME.getCode())){
			/**loock balance of mechanismOperation*/ 
			Integer operationPart = ComponentConstant.CASH_PART;
			Long operationPk 	  = operation.getIdMechanismOperationPk(); 
			/**settlement balance of mechanismOperation*/
			otcSettlementService.settlementBalancesOfOperation(operationPk, operationPart, operation.getCashSettlementDate(), Boolean.FALSE);
			/**Validate if all balances have been settled*/
		}
		return operation;
	}
	
	@EJB
	private SettlementProcessService settlementProcessServiceBean;
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendSettlementOperationWebClient(List<SettlementOperationTO> lstSettlementOperationTOs) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        settlementProcessServiceBean.sendSettlementOperationWebClient(lstSettlementOperationTOs, loggerUser);
	}
	
	/**
	 * Approve otc opereation service bean.
	 *
	 * @param otcMechanismOperation the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_APPROVE)
	public MechanismOperation approveOtcOpereationServiceFacade(MechanismOperation otcMechanismOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		negotiationOperationServiceBean.validateMechanismOperation(otcMechanismOperation);//Validamos el objeto OTC
		negotiationOperationServiceBean.validateOtcOperationState(otcMechanismOperation, Arrays.asList(OtcOperationStateType.REGISTERED.getCode()));//Validate state again
		
		return otcOperationServiceBean.approveOtcOpereationServiceBean(otcMechanismOperation);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_AUTHORIZE)
	public MechanismOperation authorizeOtcOperationServiceFacade(MechanismOperation otcMechanismOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		negotiationOperationServiceBean.validateMechanismOperation(otcMechanismOperation);//Validamos el objeto OTC
		negotiationOperationServiceBean.validateOtcOperationState(otcMechanismOperation, Arrays.asList(OtcOperationStateType.REVIEWED.getCode()));//Validate state again
		
		return otcOperationServiceBean.authorizeOtcOperationServiceBean(otcMechanismOperation);
	}
	
	/**
	 * Review otc opereation service facade.
	 *
	 * @param otcMechanismOperation the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REVIEW)
	public MechanismOperation reviewOtcOpereationServiceFacade(MechanismOperation otcMechanismOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());

		negotiationOperationServiceBean.validateOtcOperationState(otcMechanismOperation, Arrays.asList(OtcOperationStateType.APROVED.getCode()));//Validate state again
		MechanismOperation operation = otcOperationServiceBean.reviewOtcOpereationServiceBean(otcMechanismOperation);
		return operation;
	}
	
	/**
	 * Gets the placement segment on issuance service facade.
	 *
	 * @param otcOperationTO the otc operation to
	 * @return the placement segment on issuance service facade
	 */
	public PlacementSegment findPlacementSegmentOnIssuance(OtcOperationTO otcOperationTO){
		return otcOperationServiceBean.getPlacementSegmentOnIssuanceServiceBean(otcOperationTO);
	}
	
	/**
	 * Gets the placement segment completed service facade.
	 *
	 * @param negotiationOperationTO the negotiation operation to
	 * @return the placement segment completed service facade
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment getPlacementSegmentCompletedServiceFacade(NegotiationOperationTO negotiationOperationTO) throws ServiceException{
		return negotiationOperationServiceBean.getPlacementSegmentServiceBean(negotiationOperationTO);
	}
	
	/**
	 * Find mechanism operation by number service facade.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation findMechanismOperationByNumberServiceFacade(MechanismOperation mechanismOperationTO) throws ServiceException{
		return otcOperationServiceBean.findMechanismOperationByNumberServiceBean(mechanismOperationTO);
	}
	
	public MechanismOperation findMechanismOperationAvailableBal(MechanismOperation mechanismOperationTO) throws ServiceException{
		return otcOperationServiceBean.findMechanismOperationAvailableBal(mechanismOperationTO);
	}
	
	/**
	 * Gets the secundary report from reporto.
	 *
	 * @param mechanismOperationParam the mechanism operation param
	 * @return the secundary report from reporto
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation getSecundaryReportFromReporto(MechanismOperation mechanismOperationParam) throws ServiceException{
		List<MechanismOperation> mechanismModalityList = otcOperationServiceBean.getMechanismOperationReferenceServiceBean(mechanismOperationParam);//Obtenemos todos los mechanismModalityList
		MechanismOperation secundaryReport = null;
		for(MechanismOperation mechanismOperation: mechanismModalityList){
			if(mechanismOperation.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode())){
				continue;
			}else{
				if(secundaryReport==null){
					secundaryReport = mechanismOperation;
				}else{
					throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
				}
			}
		}
		return secundaryReport;
	}
	
	/**
	 * Gets the security by modality service facade.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @param securityTemp the security temp
	 * @return the security by modality service facade
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityIntoModality(MechanismOperation mechanismOperationTO, Security securityTemp) throws ServiceException{
		NegotiationOperationTO negotiationOperationTO = new NegotiationOperationTO();
		negotiationOperationTO.setIdSecurityCode(securityTemp.getIdSecurityCodePk());
		negotiationOperationTO.setIdNegotiationMechanismPk(NegotiationMechanismType.OTC.getCode());
		negotiationOperationTO.setIdNegotiationModalityPk(mechanismOperationTO.getMechanisnModality().getId().getIdNegotiationModalityPk());
		return negotiationOperationServiceBean.getSecurityByModalities(negotiationOperationTO);
	}
	
	public HolderAccountBalance checkQuantityBalance(Long idHolderAccountPk, String idSecurityCodePk,BigDecimal quantity, Long idParticipantPk){
		return negotiationOperationServiceBean.getHolderAccountBalanceForChekQuantity(idHolderAccountPk, idSecurityCodePk, idParticipantPk);
	}
	/**
	 * Gets the program interest coupon list service facade.
	 *
	 * @param otcOperationTO the otc operation to
	 * @return the program interest coupon list service facade
	 * @throws ServiceException the service exception
	 */
	public List<ProgramInterestCoupon> getProgramInterestCouponListServiceFacade(OtcOperationTO otcOperationTO) throws ServiceException{
		return otcOperationServiceBean.getProgramInterestCouponListServiceBean(otcOperationTO);
	}
	
	/**
	 * Anulate otc operation service facade.
	 *
	 * @param otcMechanismOperation the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REJECT)
	public MechanismOperation anulateOtcOperationServiceFacade(MechanismOperation otcMechanismOperation) throws ServiceException{
		//Validate state again
		negotiationOperationServiceBean.validateOtcOperationState(otcMechanismOperation, Arrays.asList(OtcOperationStateType.REGISTERED.getCode()));
		return otcOperationServiceBean.anulateOtcOperationServiceBean(otcMechanismOperation);
	}
	
	/**
	 * Reject otc operation service facade.
	 *
	 * @param otcMechanismOperation the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REJECT)
	public MechanismOperation rejectOtcOperationServiceFacade(MechanismOperation otcMechanismOperation) throws ServiceException{
		//Validate state again
		negotiationOperationServiceBean.validateOtcOperationState(otcMechanismOperation, Arrays.asList(OtcOperationStateType.APROVED.getCode(),OtcOperationStateType.REVIEWED.getCode(), OtcOperationStateType.AUTHORIZED.getCode()));
		return otcOperationServiceBean.rejectOtcOperationServiceBean(otcMechanismOperation);
	}
	
	/**
	 * Cancel otc operation service facade.
	 *
	 * @param otcMechanismOperation the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.OTC_CANCEL_OPERATION)
	public MechanismOperation cancelOtcOperationServiceFacade(MechanismOperation otcMechanismOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
		
		//Validate state again
		Integer operationPart = null;
		if(otcMechanismOperation.getOperationState().equals(MechanismOperationStateType.ASSIGNED_STATE.getCode())){
			operationPart = OperationPartType.CASH_PART.getCode();
		}else if(otcMechanismOperation.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode())){
			operationPart = OperationPartType.TERM_PART.getCode();
		}
		
		//getting data to send cancel service
//		Long idMechanismOperation = otcMechanismOperation.getIdMechanismOperationPk();
//		Long idModality = otcMechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		//cancelationServiceBean.unblockStockOperation(idMechanismOperation, idModality, operationPart, loggerUser);
		
		//cancel state of operation
		otcOperationServiceBean.cancelOtcOperationServiceBean(otcMechanismOperation,operationPart);
		
		return otcMechanismOperation;
	}
		
	
	/**
	 * Issuance is placement segment.
	 * metodo para validar si emision esta en colocacion primaria
	 * @param security the security
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean issuanceIsPlacementSegment(Security security) throws ServiceException{
		return negotiationOperationServiceBean.issuanceIsPlacementSegment(security);
	}
	
	/**
	 * Gets the Negotiation Mechanism Object.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the NegotiationMechanism Object
	 */
	public NegotiationMechanism getMechanismServiceFacade(Long idNegotiationMechanismPk){
		return negotiationOperationServiceBean.getMechanismServiceBean(idNegotiationMechanismPk);
	}

	/*REFACTORING METHODS FOR EDV PROJECT*/
	
	/**
	 * Have day exchanges rate service facade.
	 *
	 * @param currencyId the currency id
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public DailyExchangeRates findExchangesRateDay(Integer currencyId) throws ServiceException{
		DailyExchangeRates exchangeRateDay = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),currencyId);
		
		if(exchangeRateDay==null){
			throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST);
		}
		
		return exchangeRateDay;
	}
	
	/**
	 * Validations before new operation.
	 *
	 * @param mechanismModalities the mechanism modalities
	 * @param participantState the participant state
	 * @throws ServiceException the service exception
	 */
	public void validationsBeforeNewOperation(List<MechanismModality> mechanismModalities,Participant participantState) throws ServiceException{
//		Long idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
		
		/*VALIDAMOS EL ESTADO DEL PARTICIPANTE*/
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion() ){
//			Participant participant = accountsFacade.getParticipantStateServiceFacade(idParticipantPk);
//			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			if(!participantState.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK_PARAM, new Object[]{participantState.getIdParticipantPk().toString()});
			}
		}else if(!userInfo.getUserAccountSession().isDepositaryInstitution()){/*SI EL USUARIO LOGUEADO NO ES NI PARTICIPANTE NI IDEPOSITARY*/
			throw new ServiceException(ErrorServiceType.USERSESSION_INCORRECT_ACTION);
		}
		
		/*VALIDAMOS QUE EXISTA AL MENOS 1 MODALIDAD DISPONIBLE PARA NEGOCIAR*/
		List<MechanismModality> modalitiesList = new ArrayList<MechanismModality>();
		if(mechanismModalities!=null && !mechanismModalities.isEmpty()){
			if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				for(MechanismModality mecMod: mechanismModalities){
					for(ParticipantMechanism participantMechanism: mecMod.getParticipantMechanisms()){
						if(participantMechanism.getParticipant().getIdParticipantPk().equals(participantState.getIdParticipantPk())){
							modalitiesList.add(mecMod);
							break;
						}
					}
				}
			}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				modalitiesList.addAll(mechanismModalities);
			}
		}
		if(modalitiesList.isEmpty()){
			throw new ServiceException(ErrorServiceType.NEG_MODALITY_DONT_EXIST);
		}
	}
	

	/**
	 * Search negotiation modality on list.
	 *
	 * @param operation the operation
	 * @param modalityList the modality list
	 * @return the negotiation modality
	 */
	public NegotiationModality findNegModality(MechanismOperation operation, List<MechanismModality> modalityList){
		return findMechanismModality(operation,modalityList).getNegotiationModality();
	}
	
	/**
	 * Gets the modality by instrument type.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param instrumentType the instrument type
	 * @param participantPk the participant pk
	 * @return the modality by instrument type
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getModalityByInstrumentType(Long mechanismPk, Integer instrumentType, Long participantPk) throws ServiceException{
		return otcOperationServiceBean.getListNegotiationModality(mechanismPk, instrumentType, participantPk);
	}
	
	/**
	 * Find mechanism modality.
	 *
	 * @param operation the operation
	 * @param modalityList the modality list
	 * @return the mechanism modality
	 */
	public MechanismModality findMechanismModality(MechanismOperation operation, List<MechanismModality> modalityList){
		if( operation != null &&
			operation.getMechanisnModality() != null &&
			operation.getMechanisnModality().getId() != null &&
			operation.getMechanisnModality().getId().getIdNegotiationModalityPk() != null) {
		Long negotiationModalityID = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
			for(MechanismModality mechanismModality: modalityList){//Recorremos las modalidades
				Long negModality = mechanismModality.getNegotiationModality().getIdNegotiationModalityPk(); 
				Long mecModality = mechanismModality.getId().getIdNegotiationMechanismPk();
				if(mecModality.equals(NegotiationMechanismType.OTC.getCode()) && negModality.equals(negotiationModalityID)){
					return mechanismModality;
				}
			}
		}
		return null;
	}
	
	/**
	 * Config otc operation with modality.
	 *
	 * @param otcOperationParameter the otc operation parameter
	 * @param otcOperation the otc operation
	 * @param mecModality the mec modality
	 * @return the otc operation parameters
	 * @throws ServiceException the service exception
	 */
	public OtcOperationParameters configModalityOnOtcOperation(OtcOperationParameters otcOperationParameter, MechanismOperation otcOperation, MechanismModality mecModality) throws ServiceException{
		/*OBTENEMOS MECHANISM_MODALITY DE ACUERDO A LA MODALIDAD SELECCIONADA*/
//		MechanismModality mecModality = findMechanismModality(otcOperation, otcOperationParameter.getMechanismModalityList());
		otcOperation.setIndMarginGuarantee(mecModality.getNegotiationModality().getIndMarginGuarantee());
		otcOperation.setIndPrincipalGuarantee(mecModality.getNegotiationModality().getIndPrincipalGuarantee());
		otcOperation.setIndReportingBalance(mecModality.getNegotiationModality().getIndReportingBalance());
		otcOperation.setIndTermSettlement(mecModality.getNegotiationModality().getIndTermSettlement());
		otcOperation.setIndCashStockBlock(mecModality.getNegotiationModality().getIndCashStockBlock());
		otcOperation.setIndTermStockBlock(mecModality.getNegotiationModality().getIndTermStockBlock());
		otcOperation.setIndPrimaryPlacement(mecModality.getNegotiationModality().getIndPrimaryPlacement());
		
		if(mecModality.getIndIncharge().equals(BooleanType.YES.getCode())){/*VERIFICAMOS SI MODALIDAD MANEJA ENCARGO*/
			otcOperationParameter.setModalityHaveInCharge(Boolean.TRUE);
		}else{
			otcOperationParameter.setModalityHaveInCharge(Boolean.FALSE);
		}
		if(mecModality.getIndSecuritiesCurrency().equals(BooleanType.YES.getCode())){/*VERIFICAMOS SI MOD PUEDE NEGOCIAR EN MONEDA != AL VALOR*/
			otcOperationParameter.setOtcDiferentMoney(Boolean.TRUE);
		}else{
			otcOperationParameter.setOtcDiferentMoney(Boolean.FALSE);
		}
		
		if(mecModality.getNegotiationModality().getIndTermSettlement().equals(BooleanType.YES.getCode())){/*VALIDAMOS SI TIENE LIQ PLAZO*/
			if(NegotiationUtils.mechanismOperationIsViewReport(otcOperation)){/*SI LA OPE ES REPO A LA VISTA*/
				otcOperationParameter.setIsSettlementPlazeDisabled(Boolean.TRUE);
			}else{
				otcOperationParameter.setIsSettlementPlazeDisabled(Boolean.FALSE);
			}
			otcOperationParameter.setIsSettlementPlaze(Boolean.TRUE);//Seteamos los attributos manejadores en la vista
			otcOperationParameter.setIsShowTe(Boolean.TRUE);
		}else{
			otcOperationParameter.setIsSettlementPlaze(Boolean.FALSE);
			otcOperationParameter.setIsShowTe(Boolean.FALSE);
			otcOperationParameter.setIsSwap(Boolean.FALSE);
		}
		
		
		/*VALIDAMOS LA MODALIDAD DE LA OPERACION*/
		if(NegotiationUtils.mechanismOperationIsSecundaryReport(otcOperation)){
			/*SI EL ORIGEN ES != NULL y LA SOLICITUD ES DIFERENTE A CRUZADA*/
			if(otcOperation.getOriginRequest()!=null && !OtcOperationUtils.otcOperationIsCrusade(otcOperation)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECREPORT_SELL);
			}
			otcOperation.setReferenceOperation(getNewMechanismOperation());
			otcOperationParameter.setIsOriginRequestDisabled(Boolean.TRUE);
			otcOperationParameter.setIsSecundaryReport(Boolean.TRUE);
			otcOperationParameter.setIsQuantityPendientSecReport(Boolean.TRUE);
			otcOperationParameter.setParticipationSwap(null);
			otcOperationParameter.setMechanismOperationAuxiliar(getNewMechanismOperation());
		}else if(NegotiationUtils.mechanismOperationIsSwap(otcOperation)){
			otcOperationParameter.setIsSwap(Boolean.TRUE);
			otcOperationParameter.setParticipationSwap(ParticipantRoleType.BUY.getCode());//Seteamos la participacion del isin dentro de swap
			otcOperationParameter.setIsIsinQuantityEquals(Boolean.FALSE);
		}else{//Para las demas modalidades el Origen de la solicitud debe ser elegido(VENTA,CRUZADA)
			otcOperationParameter.setIsOriginRequestDisabled(Boolean.FALSE);
			otcOperationParameter.setIsSecundaryReport(Boolean.FALSE);
			otcOperationParameter.setIsQuantityPendientSecReport(Boolean.FALSE);
			otcOperationParameter.setIsSwap(Boolean.FALSE);
			otcOperationParameter.setParticipationSwap(null);
		}
		return otcOperationParameter;
	}
	
	/**
	 * Find participant from id.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param participantList the participant list
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantFromId(Long idParticipantPk, List<Participant> participantList) throws ServiceException{
		for(Participant participant: participantList){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}
	
	/**
	 * Config modalities on participant map.
	 *
	 * @param mecModList the mec mod list
	 * @param partList the part list
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public Map<Long,List<Participant>> configModalitiesOnParticipantMap(List<MechanismModality> mecModList, List<Participant> partList) throws ServiceException{
		Map<Long,List<Participant>> participantByModalities = new HashMap<Long,List<Participant>>();
		
		List<NegotiationModalityType> negModalityAvailables = new ArrayList<NegotiationModalityType>();
		negModalityAvailables.add(NegotiationModalityType.CASH_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.CASH_FIXED_INCOME);
		negModalityAvailables.add(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME);
		negModalityAvailables.add(NegotiationModalityType.FOWARD_SALE_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.FOWARD_SALE_FIXED_INCOME);
		negModalityAvailables.add(NegotiationModalityType.REPORT_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.REPORT_FIXED_INCOME);
		negModalityAvailables.add(NegotiationModalityType.VIEW_REPO_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.VIEW_REPO_FIXED_INCOME);
		negModalityAvailables.add(NegotiationModalityType.SECUNDARY_REPO_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME);
		negModalityAvailables.add(NegotiationModalityType.SWAP_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.SWAP_FIXED_INCOME);
		negModalityAvailables.add(NegotiationModalityType.REPURCHASE_EQUITIES);
		negModalityAvailables.add(NegotiationModalityType.REPURCHASE_FIXED_INCOME);
//		negModalityAvailables.add(NegotiationModalityType.SECURITIES_LOAN_EQUITIES);
//		negModalityAvailables.add(NegotiationModalityType.SECURITIES_LOAN_FIXED_INCOME);
//		negModalityAvailables.add(NegotiationModalityType.COUPON_SPLIT_FIXED_INCOME);
		_jump:
		for(MechanismModality mechanismModality: mecModList){
			Long idNegModality = mechanismModality.getNegotiationModality().getIdNegotiationModalityPk();
			participantByModalities.put(idNegModality, new ArrayList<Participant>());
			for(ParticipantMechanism partMechanism: mechanismModality.getParticipantMechanisms()){
				Participant participantSearch = findParticipantFromId(partMechanism.getParticipant().getIdParticipantPk(),partList);//Validamos si el participante existe como registrado
				if(participantSearch!=null){//Si esta registrado.
					participantByModalities.get(idNegModality).add(partMechanism.getParticipant());
				}
			}
			for(NegotiationModalityType negModalityType: negModalityAvailables){
				if(negModalityType.getCode().equals(idNegModality)){
					mechanismModality.setStateMechanismModality(BooleanType.YES.getCode());
					continue _jump;
				}
			}
		}
		return participantByModalities;
	}
	
	/**
	 * Validate security object.
	 *
	 * @param otcOperation the otc operation
	 * @param mecModalitList the mec modalit list
	 * @throws ServiceException the service exception
	 */
	public void validateSecurityObject(MechanismOperation otcOperation, List<MechanismModality> mecModalitList) throws ServiceException {
		
		if(otcOperation.getSecurities().getIdSecurityCodePk()==null) throw new ServiceException();
		
		Security securityTemp = otcOperationServiceBean.findSecurityFromCode(otcOperation.getSecurities());
		
		/*SI EL VALOR ES NULL, O NO TIENE EMISION NI EMISOR NI MONEDA NI MONTO EN CIRCULACION*/
		/*SI LOS INDICADORES DE NEGOCIACION COMO NATURAL O JURIDICO SON NULL*/
		if(securityTemp==null || securityTemp.getCurrency()==null || CurrencyType.get(securityTemp.getCurrency())==null ||
		   securityTemp.getIndTraderSecondary()==null || securityTemp.getCirculationAmount()==null ||
		   securityTemp.getInstrumentType()==null || securityTemp.getSecurityInvestor()==null ||
		   securityTemp.getSecurityInvestor().getIndJuridical()==null || securityTemp.getSecurityInvestor().getIndNatural()==null ){
			
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}
		
		/*SI ES RTA FIJA Y NO TIENE FECHA DE EXPIRACION*/
		if(securityTemp.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()) && securityTemp.getExpirationDate()==null){
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}
		
		if(!securityTemp.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
		}
		
		/*VALIDAMOS EL EMISOR SEA DIFERENTE A NULL*/
		if(securityTemp.getIssuer()==null){//Validamos que el valor tenga al menos un emisor
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}else{
			/*VERIFICAMOS QUE EL ESTADO DEL EMISOR SEA REGISTRADO*/
			if(!securityTemp.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
			}
		}
		
		/*VALIDAMOS QUE LA EMISION SEA DIFERENTE A NULL*/
		if(securityTemp.getIssuance()==null){
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}else{
			/*VERIFICAMOS QUE EL ESTADO DE LA EMISION SEA DIFERENTE A NULL*/
			if(!securityTemp.getIssuance().getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
				throw new ServiceException(ErrorServiceType.ISSUANCE_BLOCK);
			}
		}
		
		/*VERIFICAMOS QUE EL INSTRUMENTO DE LA MODALIDAD COINCIDA CON EL DEL VALOR*/
		NegotiationModality negModality = findNegModality(otcOperation,mecModalitList);
		if(!securityTemp.getInstrumentType().equals(negModality.getInstrumentType())){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SEC_INSTRUMENT);
		}
		
		/*VERIFICAMOS QUE EL VALOR PUEDA NEGOCIAR EN LA MODALIDAD SELECCIONADA*/
		Security securityIntoModality = findSecurityIntoModality(otcOperation,securityTemp);
		if(securityIntoModality==null){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_MECHANISM);
		}
		
		/*
		if(!NegotiationUtils.mechanismOperationIsPrimaryPlacement(otcOperation)){
			//VALIDAMOS SI LA EMISION NO ESTA EN COLOCACION PRIMARIA
			if(issuanceIsPlacementSegment(securityTemp)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_PRIMARY_PLACEMENT);
			}
		}
		*/
		
		if( Validations.validateIsNotNull(securityTemp.getExpirationDate()) && 
				CommonsUtilities.getDaysBetween(securityTemp.getExpirationDate(), new Date()) == 0 ) {
			throw new ServiceException(ErrorServiceType.ERROR_SECURITY_EXPIRATION_DATE_IS_TODAY);
		}
		
		/**Config OtcOperation to handle exchangeRate*/
		Integer currency = securityTemp.getCurrency();
		ParameterTable parameterTable = parameterServiceBean.getParameterDetail(currency);
		Integer termPart = otcOperation.getIndTermSettlement();
		/**Config exchangeRate to mechanismOperation*/
		negotiationOperationServiceBean.setCurrency(parameterTable, otcOperation, termPart);
		otcOperation.setSecurities(securityTemp);
	}
	
	/**
	 * Validate security swap operation.
	 *
	 * @param otcOperation the otc operation
	 * @param swapOperation the swap operation
	 * @param otcParameters the otc parameters
	 * @return the otc operation parameters
	 * @throws ServiceException the service exception
	 */
	public OtcOperationParameters validateSecuritySwapOperation(MechanismOperation otcOperation, SwapOperation swapOperation, OtcOperationParameters otcParameters) 
								throws ServiceException{
		
		if(!NegotiationUtils.mechanismOperationIsSwap(otcOperation)) return otcParameters;
		
		if(swapOperation.getSecurities().getIdSecurityCodePk()!=null){
			if(swapOperation.getSecurities().getIdSecurityCodePk().equals(otcOperation.getSecurities().getIdIsinCode())){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITY_SWAP_EXIST);
			}
			
			if(swapOperation.getSecurities().getCurrency().equals(otcOperation.getSecurities().getCurrency())){
				otcParameters.setIsSwapDifCurr(Boolean.TRUE);
			}else{
				otcParameters.setIsSwapDifCurr(Boolean.FALSE);
			}
		}
		return otcParameters;
	}
	
	/**
	 * Validate security primary placement.
	 *
	 * @param otcOperation the otc operation
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void validateSecurityPrimaryPlacement(MechanismOperation otcOperation, OtcOperationParameters parameters) throws ServiceException{
		
		if(!NegotiationUtils.mechanismOperationIsPrimaryPlacement(otcOperation)) return;
		
		Security securityTemp = otcOperation.getSecurities();
		
		OtcOperationTO otcOperationTemp = new OtcOperationTO();
//		NegotiationOperationTO otcOperationTemp = new NegotiationOperationTO();
		otcOperationTemp.setIdIssuancePk(securityTemp.getIssuance().getIdIssuanceCodePk());
		otcOperationTemp.setIdSecurityCodePk(securityTemp.getIdSecurityCodePk());
		otcOperationTemp.setIdParticipantPlacement(otcOperation.getSellerParticipant().getIdParticipantPk());
		
		PlacementSegment placSegmentTemp = findPlacementSegmentOnIssuance(otcOperationTemp);
//		
//		if(placSegmentTemp == null){
//			throw new ServiceException(ErrorServiceType.NEG_OPERATION_PRIMARYPLAC_BLOCK);
//		}
		
//		placSegmentTemp = findPlacementSegmentOnIssuance(otcOperationTemp);
		if(placSegmentTemp == null){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_PRIMARYPLAC_PARTICIPANT);
		}
		parameters.setPlacementSegment(placSegmentTemp);
		
		HolderAccountTO holderAccountTO = new HolderAccountTO();
		holderAccountTO.setParticipantTO(otcOperation.getSellerParticipant().getIdParticipantPk());
		holderAccountTO.setHolderaccountGroupType(HolderAccountGroupType.ISSUER.getCode());
		holderAccountTO.setIdIssuerPk(securityTemp.getIssuer().getIdIssuerPk());
		holderAccountTO.setNeedBanks(false);
		holderAccountTO.setNeedParticipant(false);
		holderAccountTO.setNeedHolder(true);
		HolderAccount holderAccount = accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
		
		if(holderAccount==null){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_PRIMARYPLAC_ACCOUNT_EMPTY);
		}
		
		if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_PRIMARYPLAC_ACCOUNT_BLOCK);
		}
	}
	
	/**
	 * Fill parameters from security.
	 *
	 * @param security the security
	 * @param otcParameters the otc parameters
	 * @return the otc operation parameters
	 * @throws ServiceException the service exception
	 */
	public OtcOperationParameters fillParametersFromSecurity(Security security, OtcOperationParameters otcParameters) throws ServiceException{
		otcParameters.setIsVariableIncome(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()));
		if(security.getCurrency().equals(CurrencyType.PYG.getCode())){//IF the money is DOP security currency must be equals than OtcOperation currency
			otcParameters.setOtcDiferentMoney(Boolean.TRUE);
		}
		return otcParameters;
	}
	
	/**
	 * Gets the new mechanism operation.
	 *
	 * @return the new mechanism operation
	 */
	public MechanismOperation getNewMechanismOperation(){
		MechanismOperation mechanismOperation = new MechanismOperation();
		mechanismOperation.setMechanisnModality(new MechanismModality());
		mechanismOperation.getMechanisnModality().getId().setIdNegotiationMechanismPk(NegotiationMechanismType.OTC.getCode());
		mechanismOperation.setOperationDate(CommonsUtilities.currentDate());
		mechanismOperation.setReferenceDate(CommonsUtilities.currentDateTime());
		mechanismOperation.setBuyerParticipant(new Participant());
		mechanismOperation.setSellerParticipant(new Participant());
		mechanismOperation.setSecurities(new Security());
//		mechanismOperationOtcSession.setSwapOperations(new ArrayList<SwapOperation>());
		mechanismOperation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		mechanismOperation.setOperationState(OtcOperationStateType.REGISTERED.getCode());
		mechanismOperation.setIndTermSettlement(BooleanType.NO.getCode());
		

		/*CONFIGURAMOS EL OBJETO PARA MANEJAR HECHOS DE MERCADO*/
		if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			MechanismOperationMarketFact operationMarketFact = new MechanismOperationMarketFact();
			operationMarketFact.setMarketDate(CommonsUtilities.currentDate());
			
			mechanismOperation.setMechanismOperationMarketFacts(new ArrayList<MechanismOperationMarketFact>());
			mechanismOperation.getMechanismOperationMarketFacts().add(operationMarketFact);
		}
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			Long idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
			mechanismOperation.getSellerParticipant().setIdParticipantPk(idParticipantPk);
			mechanismOperation.getBuyerParticipant().setIdParticipantPk(null);
		}
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			try{
			Participant participant = accountsFacade.getParticipantForIssuerDpfDpa(userInfo.getUserAccountSession().getIssuerCode());
			mechanismOperation.getSellerParticipant().setIdParticipantPk(participant.getIdParticipantPk());
			mechanismOperation.getBuyerParticipant().setIdParticipantPk(null);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		}
		return mechanismOperation;
	}
	
	/**
	 * Gets the new holder account operation.
	 *
	 * @param otcOperation the otc operation
	 * @param blReview the bl review
	 * @return the new holder account operation
	 * @throws ServiceException the service exception
	 */
	public HolderAccountOperation getNewHolderAccountOperation(MechanismOperation otcOperation, Boolean blReview) throws ServiceException{
		HolderAccountOperation holdAccountOperationRequest = new HolderAccountOperation();
		holdAccountOperationRequest.setInchargeFundsParticipant(new Participant());
		holdAccountOperationRequest.setInchargeStockParticipant(new Participant());
		holdAccountOperationRequest.setHolderAccount(new HolderAccount());
		holdAccountOperationRequest.getHolderAccount().setHolder(new Holder());
		holdAccountOperationRequest.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
		holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());
		holdAccountOperationRequest.setRole(ParticipantRoleType.SELL.getCode());
		holdAccountOperationRequest.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion() ||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			Participant participantLogout = new Participant(userInfo.getUserAccountSession().getParticipantCode());
			//Si el participante es el encargado y es el y esta asignando sus cuentas 
			if(OtcOperationUtils.participantIsInCharge(otcOperation, participantLogout)){
				//Verificamos que el participante no sea el comprador y que no este en esstado de aprobacion
				if(OtcOperationUtils.participantIsSeller(otcOperation, participantLogout) && blReview){
					holdAccountOperationRequest.setInchargeFundsParticipant(participantLogout);
					holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());
				}
			}
		}
		return holdAccountOperationRequest;
	}
	
	/**
	 * Gets the acc operation for primary plac.
	 *
	 * @param operation the operation
	 * @param issuerAccount the issuer account
	 * @return the acc operation for primary plac
	 * @throws ServiceException the service exception
	 */
	public HolderAccountOperation getAccOperationForPrimaryPlac(MechanismOperation operation, HolderAccount issuerAccount) throws ServiceException{
		HolderAccountOperation accountOperation = getNewHolderAccountOperation(operation, false);
		accountOperation.setHolderAccount(issuerAccount);//Seteamos la cuenta
		accountOperation.setRole(ParticipantRoleType.SELL.getCode());//Seteamos el Rol
		accountOperation.setIndIncharge(BooleanType.NO.getCode());//Seteamos si tiene en cargo, porfault no tiene
		accountOperation.setCashAmount(operation.getStockQuantity().multiply(operation.getCashPrice()));//Seteamos la el monto a utilizar
		accountOperation.setInchargeFundsParticipant(operation.getSellerParticipant());//Seteamos el participante vendedor
		accountOperation.setInchargeStockParticipant(operation.getSellerParticipant());//Seteamos el participante vendedor
		accountOperation.setStockQuantity(operation.getStockQuantity());//Seteamos la cantidad de valores
		accountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
		accountOperation.setOperationPart(OperationPartType.CASH_PART.getCode());//Setteamos el operation part, como contado
		accountOperation.setMechanismOperation(operation);
		
		return accountOperation;
	}
	
	/**
	 * Validate stock quantity.
	 *
	 * @param mechanismOperationOtcSession the mechanism operation otc session
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void validateStockQuantity(MechanismOperation mechanismOperationOtcSession, OtcOperationParameters parameters) throws ServiceException{
		BigDecimal quantitySecurities = mechanismOperationOtcSession.getStockQuantity();//Obtenemos la cantidad de valores
		BigDecimal circulationBalance = mechanismOperationOtcSession.getSecurities().getCirculationBalance();
		
		if(quantitySecurities==null || quantitySecurities.equals(BigDecimal.ZERO)){
			mechanismOperationOtcSession.setStockQuantity(null);
			return;
		}
		
		if(NegotiationUtils.mechanismOperationIsPrimaryPlacement(mechanismOperationOtcSession)){
			Long participantCode = mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk();
			String securityCode = mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk();
			Long placementCode = parameters.getPlacementSegment().getIdPlacementSegmentPk();
			
			PlacementSegment placementSegment = otcOperationServiceBean.placementSecurityDetail(placementCode,participantCode, securityCode);
			if(!OtcOperationUtils.isinHadBalanceAvailableOnPlacementSegment(placementSegment, mechanismOperationOtcSession)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_PRIMARYPLAC_STOCKQUANTITY);
			}
		}else{//this validation is only for secundary market
			if(quantitySecurities.compareTo(circulationBalance)>0){//Validamos si la cantidad ingresada es menor a los valores en circulacion
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_CIRCULATION);
			}
		}
		
		if(NegotiationUtils.mechanismOperationIsSecundaryReport(mechanismOperationOtcSession)){//Si la Operacion es Repo secundario
			MechanismOperation mechanismOperationAuxiliar = mechanismOperationOtcSession.getReferenceOperation();
			BigDecimal quantitySecuritiesCurrentRepo = mechanismOperationAuxiliar.getStockQuantity();//Obtenemos la cantidad de valores de la repo original
			if(quantitySecurities.compareTo(quantitySecuritiesCurrentRepo) > BigDecimal.ZERO.intValue()){//validamos si la cantidad de valores es menor O igual que cantidad de repo original
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_REPOSECUNDARY);
			}
		}
	}
	
	/**
	 * Validate clean cash amount.
	 *
	 * @param otcOperation the otc operation
	 * @throws ServiceException the service exception
	 */
	public void validateCleanCashAmount(MechanismOperation otcOperation) throws ServiceException{
		BigDecimal cleanCashAmount = otcOperation.getRealCashAmount();
		BigDecimal dirtyCashAmount = otcOperation.getCashAmount();
		if(cleanCashAmount==null || cleanCashAmount.intValue()==BigDecimal.ZERO.intValue()){//Si es 0 o Null
			throw new ServiceException();
		}
		if(dirtyCashAmount!=null){//Si el monto sucio ya fue ingresado
			if(dirtyCashAmount.compareTo(cleanCashAmount)<0){//Si el monto limpio es mayor O igual al monto sucio
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_CLEANAMOUNT_GATHER);
			}
		}
	}
	
	/**
	 * Validate settlement date.
	 *
	 * @param settlementDays the settlement days
	 * @param operationPart the operation part
	 * @param operation the operation
	 * @param mecModalities the mec modalities
	 * @throws ServiceException the service exception
	 */
	public void validateSettlementDate(Long settlementDays, Integer operationPart, MechanismOperation operation, List<MechanismModality> mecModalities) throws ServiceException{
		NegotiationModality negModality = findNegModality(operation, mecModalities);
		Date settlementDate = null ;//Dato para almacenar la fecha de liquidacion contado
		
		/*Indicador de dias maximo para liquidacion contado*/
		Integer minSettlemenDay = GeneralConstants.ZERO_VALUE_INTEGER, maxSettlemenDay = GeneralConstants.ZERO_VALUE_INTEGER;
		/*tipo de calendario en liquidacion */
		Integer settCalendarType = negModality.getIndCashCallendar();
		
		/*variables para manejar los dias maximo Y minimo para liquidacion*/
		if(operationPart.equals(OperationPartType.CASH_PART.getCode())){
			minSettlemenDay = negModality.getCashMinSettlementDays();
			maxSettlemenDay = negModality.getCashMaxSettlementDays();
		}else if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
			minSettlemenDay = negModality.getTermMinSettlementDays();
			maxSettlemenDay = negModality.getTermMaxSettlementDays();
		}
		
		/*Si la cantidad de dias, NO esta en el rango de dias de la modalidad*/
		if(settlementDays!=null && (settlementDays<minSettlemenDay || settlementDays>maxSettlemenDay)){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTLEMENT_DAYS);
		}else if(settCalendarType==null || CashCalendarType.get(settCalendarType)==null){//Si la modalidad no tiene un calendario fijado en la base datos
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_INCONSISTENT);
		}else  if(CashCalendarType.get(settCalendarType).equals(CashCalendarType.CALENDAR_DAYS)){//Si los dias son calendario
			settlementDate = CommonsUtilities.addDate(operation.getOperationDate(), settlementDays.intValue());
			Calendar date = Calendar.getInstance();
			date.setTime(settlementDate);
			
			if(CommonsUtilities.isWeekend(date) || holidayQueryServiceBean.isNonWorkingDayServiceBean(settlementDate, Boolean.FALSE)){//Si el dia de liquidacion es fin de semana O si es un feriado
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTDATE_NOTWORKING,new Object[]{OperationPartType.get(operationPart).getDescription()});
			}
		}else if(CashCalendarType.get(settCalendarType).equals(CashCalendarType.UTIL_DAYS)){//Si los dias son utiles
			if(settlementDays!=null && settlementDays.intValue()!=0){
				settlementDate = holidayQueryServiceBean.getCalculateDate(operation.getOperationDate(), settlementDays.intValue(), 1,NegotiationConstant.WORKING_DAYS_PASS_WEEKENDS);//Calculamos el siguiente dia habil
			}else{
				settlementDate = operation.getOperationDate();
			}
			
			Calendar date = Calendar.getInstance();
			if(settlementDate!=null){
				date.setTime(settlementDate);
			}
			if(settlementDate==null || CommonsUtilities.isWeekend(date)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTDATE_NOTWORKING,new Object[]{OperationPartType.get(operationPart).getDescription()});
			}
		}
		
		if(operationPart.equals(OperationPartType.CASH_PART.getCode())){
			operation.setCashSettlementDate(settlementDate);
		}else if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
			Date cashSettlementDate = operation.getCashSettlementDate();
			if(cashSettlementDate!=null && cashSettlementDate.after(settlementDate)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTLEMENT_DAYS);
			}
			/*SET TERM SETTLEMENT DATE TO OPERATION*/
			operation.setTermSettlementDate(settlementDate);
		}
	}
	
	/**
	 * Validate sett date with security.
	 *
	 * @param swapOperation the swap operation
	 * @param operationPart the operation part
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	public void validateSettDateWithSecurity(SwapOperation swapOperation, Integer operationPart, MechanismOperation operation) throws ServiceException{
		Date settlementDate = null;
		if(operationPart.equals(OperationPartType.CASH_PART.getCode())){
			settlementDate = operation.getCashSettlementDate();
		}else if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
			settlementDate = operation.getTermSettlementDate();
		}
		
		Security securityTemp = operation.getSecurities();
		Date expirationDate = securityTemp.getExpirationDate(); //obtenemos la fecha de expiracion del valor
		if(securityTemp.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
			Boolean isSwapWithExpirationDays = Boolean.FALSE;
			if(NegotiationUtils.mechanismOperationIsSwap(operation)){
				Security securitySwapTemp = swapOperation.getSecurities();
				Date expirationSwapDate = securitySwapTemp.getExpirationDate(); //obtenemos la fecha de expiracion del valor
				if(CommonsUtilities.addDate(settlementDate,1).after(expirationSwapDate)){
					isSwapWithExpirationDays = Boolean.TRUE;
				}
			}
			/*Si es rta fija y la fecha calculada es mayor o igual a la fecha que vence el valor (Validas los 2 valores))*/
			if((CommonsUtilities.addDate(settlementDate,1).after(expirationDate) || isSwapWithExpirationDays)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_SETTLEMENTDATE_SECURITY_VENC);
			}
		}
	}
	
	/**
	 * Validate sett date with modality.
	 *
	 * @param operation the operation
	 * @param cashSettlementDate the cash settlement date
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void validateSettDateWithModality(MechanismOperation operation, Date cashSettlementDate, OtcOperationParameters parameters) throws ServiceException{
		/**Si es Colocacion primaria, se necesita agregar cuenta vendedora*/	
		if(NegotiationUtils.mechanismOperationIsPrimaryPlacement(operation)){
			Long placementCode = parameters.getPlacementSegment().getIdPlacementSegmentPk();
			Long participantCode = operation.getSellerParticipant().getIdParticipantPk();
			HolderAccount issuerAccount = otcOperationServiceBean.getPlacementSecurityAccount(placementCode,participantCode);
			HolderAccountOperation accountOperation = getAccOperationForPrimaryPlac(operation, issuerAccount);
			/*Si la cuenta vendedora aun no ha sido agregada*/
			if(OtcOperationUtils.getAccountOperationFromOperation(operation, ParticipantRoleType.SELL).isEmpty()){
				operation.getHolderAccountOperations().add(accountOperation);
			}
		}
	}
	
	/**
	 * Find accounts onparticipant.
	 *
	 * @param holder the holder
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findAccountsOnparticipant(Holder holder, Participant participant) throws ServiceException{
		if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED, new Object[]{holder.getIdHolderPk().toString()});
		}
		
		if(participant==null){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_PARTICIPANT_ASSIG);
		}
		
		List<HolderAccount> accountList = new ArrayList<HolderAccount>();
		for(HolderAccount account: accountServiceBean.getAccountsHolderAndParticipant(holder.getIdHolderPk(),participant.getIdParticipantPk())){
			if(account.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()) &&
			   account.getAccountGroup().equals(HolderAccountGroupType.INVERSTOR.getCode())){
				account.setSelected(Boolean.FALSE);
				accountList.add(account);
			}
		}
		if(accountList.isEmpty()){
			throw new ServiceException(ErrorServiceType.HOLDER_WITHOUT_HOLDERACCOUNTS);
		}
		return accountList;
	}
	
	/**
	 * Validate holder account.
	 *
	 * @param account the account
	 * @param operation the operation
	 * @param haveInCharge the have in charge
	 * @throws ServiceException the service exception
	 */
	public void validateHolderAccount(HolderAccount account, MechanismOperation operation, Integer haveInCharge) throws ServiceException{
		if(haveInCharge.equals(BooleanType.NO.getCode()) && account.getIdHolderAccountPk()==null){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_NOT_REGISTERED);
		}
		
		for(HolderAccountDetail detail: account.getHolderAccountDetails()){
			if(!detail.getHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED, new Object[]{detail.getHolder().getIdHolderPk().toString()});
			}
		}
		/*Validamos que el titular de la cuenta no haya sido ingresado anteriormente*/
//		if(OtcOperationUtils.holderExistOperation(operation, account)){
//			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_HOLDER_DUPLICATED);
//		}
		/**************************************************************
         * issue 167 Modificacion de 
         * validacion en Proceso OTC de 
         * Transferencia Extrabursatil
         * Validamos que la cuenta no haya sido ingresado anteriormente
		 **************************************************************/
		if(OtcOperationUtils.holderAccountExistOperation(operation, account)){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_HOLDERACCOUNT_DUPLICATED);
		}
		
		negotiationOperationServiceBean.validateHolderAssignment(false, account, operation.getSecurities().getIdSecurityCodePk(), null);
	}
	
	/**
	 * Validate social kapital.
	 *
	 * @param accoutOperation the accout operation
	 * @param operation the operation
	 * @param socialKapital the social kapital
	 * @return the boolean
	 */
	public Boolean validateSocialKapital(HolderAccountOperation accoutOperation, MechanismOperation operation, Double socialKapital){
		if(operation.getSecurities().getIdSecurityCodePk()!=null){
			InstrumentType securityInstrument = InstrumentType.lookup.get(operation.getSecurities().getInstrumentType());
			BigDecimal stockQuantAssigment = accoutOperation.getStockQuantity();
			if(securityInstrument.equals(InstrumentType.VARIABLE_INCOME)){//Validamos cuando es Renta Variable
				if(!NegotiationUtils.mechanismOperationIsPrimaryPlacement(operation)){
					BigDecimal issuanceAmount = operation.getSecurities().getIssuance().getIssuanceAmount();//Obtenemos el Valor de la Emision
					BigDecimal amountEntered = stockQuantAssigment.multiply(operation.getSecurities().getCurrentNominalValue());//Calculamos la cantidad
					BigDecimal porcentIssuanceAmount = CommonsUtilities.getPercentageOf(issuanceAmount, socialKapital); 
					if(amountEntered.compareTo(porcentIssuanceAmount) >= 0){//Si el monto es mayor al % de Factor de Negociacion de la emision
						return Boolean.TRUE;
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Validate stock quant assigment.
	 *
	 * @param operation the operation
	 * @param swapOperation the swap operation
	 * @param accountOperation the account operation
	 * @throws ServiceException the service exception
	 */
	public void validateStockQuantAssigment(MechanismOperation operation, SwapOperation swapOperation, HolderAccountOperation accountOperation) throws ServiceException{
		BigDecimal stockQuantOperation = OtcOperationUtils.getStockQuantityFromOperation(operation, swapOperation, accountOperation.getRole());//Obtenemos el stock quantity de acuerdo al tipo de participacion
		BigDecimal stockQuantAssigment = accountOperation.getStockQuantity();
		BigDecimal cashPriceAssigment;
		
		if(stockQuantAssigment.compareTo(stockQuantOperation) > 0){//Si el monto ingresado es mayor a la cantidad de valores de la Operacion
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_QUANTITY_OPERATION);
		}else{
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){//Si es participante, no puede agregar cuentas de sus encargados
				if(accountOperation.getIndIncharge().equals(BooleanType.YES.getCode())){//Si tiene encargo
					if(operation.getOperationState().equals(OtcOperationStateType.REGISTERED.getCode()) && operation.getHolderAccountOperations().isEmpty()){
						if(!stockQuantOperation.equals(stockQuantAssigment)){
							throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_QUANTITY_LESS);
						}
					}else if(operation.getOperationState().equals(OtcOperationStateType.APROVED.getCode())){
						if(!stockQuantOperation.equals(stockQuantAssigment)){
							throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_QUANTITY_LESS);
						}
					}
				}
			}
		}
		
		if(NegotiationUtils.mechanismOperationIsSwap(operation) && accountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
			cashPriceAssigment = swapOperation.getCashPrice();
			accountOperation.setCashAmount(cashPriceAssigment.multiply(stockQuantAssigment));
		}else{
			accountOperation.setCashAmount(operation.getRealCashAmount());
		}
	}
	
	/**
	 * Validate stock quantit range.
	 *
	 * @param accountOperation the account operation
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	public void validateStockQuantitRange(HolderAccountOperation accountOperation, MechanismOperation operation) throws ServiceException{
		if(NegotiationUtils.mechanismOperationIsPrimaryPlacement(operation)){//Si la operacion es colocacion primaria
			BigDecimal minimValueInvestor = operation.getSecurities().getMinimumInvesment();
			BigDecimal maximValueInvestor = operation.getSecurities().getMaximumInvesment();

			BigDecimal nominalValue = operation.getSecurities().getCurrentNominalValue();
			BigDecimal amountEntered = accountOperation.getStockQuantity().multiply(nominalValue);
			
			if(minimValueInvestor!=null && minimValueInvestor.compareTo(amountEntered)>0){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_RANGE_AMOUNTS);
			}
//			else if(maximValueInvestor!=null && maximValueInvestor.compareTo(amountEntered)<0){
//				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_RANGE_MAX_AMOUNTS);
//			}
		}
	}
	
	/**
	 * Validate new account operation.
	 *
	 * @param operation the operation
	 * @param swapOperation the swap operation
	 * @param accountOperation the account operation
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void validateNewAccountOperation(MechanismOperation operation, SwapOperation swapOperation, HolderAccountOperation accountOperation, OtcOperationParameters parameters) throws ServiceException{		
		/**SI ES REPO SECUNDARIO Y AUN TIENE CANTIDAD PENDIENTE DE INGRESAR, DEBE SER VALIDADO POR LA SIGUIENTE VALIDACION "validateAccontOperationForSecundReport"*/
		if(NegotiationUtils.mechanismOperationIsSecundaryReport(operation) && parameters.getIsQuantityPendientSecReport()) return;
		
		/**OBTENEMOS LA CUENTA SELECCIONADA DE LA GRILLA*/
		HolderAccount account = parameters.getAccountSelectedAssigment();
		
		/**ASIGNAMOS LOS PARTICIPANTES CON TODA SU INFORMACION, DENTRO DE LA OPERACION*/
		Participant sellerParticipant = findParticipantFromId(operation.getSellerParticipant().getIdParticipantPk(), parameters.getAllParticipantList());
		Participant buyerParticipant = findParticipantFromId(operation.getBuyerParticipant().getIdParticipantPk(), parameters.getAllParticipantList());
		Participant inchargeFundsParticipant = null, inchargeSockParticipant = null;
		
		/**VERIFICAMOS SI LA SOLICITUD TIENE CANTIDAD DISPONIBLE A SER AGREGADA*/
		if(!OtcOperationUtils.haveStockPendWithNegByRole(operation, swapOperation, accountOperation, accountOperation.getRole())){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_QUANTITY_SUM);
		}
		
		/**CREAMOS EL NUEVO HOLDER_ACCOUNT_OPERATION*/
		HolderAccountOperation newAccountOperation = new HolderAccountOperation();
		newAccountOperation.setIndIncharge(accountOperation.getIndIncharge());
		newAccountOperation.setRole(accountOperation.getRole());
		newAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
		newAccountOperation.setStockQuantity(accountOperation.getStockQuantity());
		newAccountOperation.setCashAmount(accountOperation.getCashAmount());
		newAccountOperation.setOperationPart(OperationPartType.CASH_PART.getCode());
		newAccountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
		newAccountOperation.setSettlementAmount(accountOperation.getCashAmount());
		
		if(newAccountOperation.getIndIncharge().equals(BooleanType.YES.getCode())){//Si tiene encargo, Por ahora se manejara Encargado de fondos y valores en 1 solo
			inchargeFundsParticipant = findParticipantFromId(accountOperation.getInchargeFundsParticipant().getIdParticipantPk(), parameters.getAllParticipantList());
			inchargeSockParticipant = findParticipantFromId(accountOperation.getInchargeStockParticipant().getIdParticipantPk(), parameters.getAllParticipantList());
			if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){//Si es Participante
				Participant participantLogout ;
				if (userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
				 participantLogout = new Participant(userInfo.getUserAccountSession().getParticipantCode());//Obtenemo el participante de la session
				else{
					participantLogout = accountsFacade.getParticipantForIssuerDpfDpa(userInfo.getUserAccountSession().getIssuerCode());
//					idParticipantPkForIssuer=participantState.getIdParticipantPk();
				}
				
				//Verificamos si el participante es el que esta agregando sus cuentas
				if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, accountOperation.getRole())){
					if(!OtcOperationUtils.haveStockPendLikeInChargeByRole(operation, accountOperation, accountOperation.getRole())){
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_QUANTITY_SUM);
					}
				}else if(OtcOperationUtils.haveInChargeByParticipantRole(operation, newAccountOperation.getRole())){//Si ya existe 1 encargo
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_QUANTITY_INCHARGE);
				}
			}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				if(OtcOperationUtils.haveInChargeByParticipantRole(operation, newAccountOperation.getRole())){
					if(!OtcOperationUtils.haveStockPendLikeInChargeByRole(operation, accountOperation, accountOperation.getRole())){
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_QUANTITY_SUM);
					}
				}
			}
		}else{
			if(account==null || account.getIdHolderAccountPk()==null){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
			}
			if(OtcOperationUtils.otcOperationIsSell(operation)){//Por ahora siempre sera Sale,Crusade(Venta,Cruzada)
				if(newAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
					inchargeFundsParticipant = sellerParticipant;
					inchargeSockParticipant = sellerParticipant;
				}else if(newAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
					inchargeFundsParticipant = buyerParticipant;
					inchargeSockParticipant = buyerParticipant;
				}
			}else if(OtcOperationUtils.otcOperationIsCrusade(operation)){
				inchargeFundsParticipant = sellerParticipant;
				inchargeSockParticipant = sellerParticipant;
			}
		}

		/*VALIDACIONES COMO PARTICIPANTE NEGOCIADOR Y ENCARGADO DE SU POSICION CONTRARIA*/
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){//Verificamos si el usuario es participante
//			Participant participantLogout = new Participant(userInfo.getUserAccountSession().getParticipantCode());//Traemos el participante de la session
			Participant participantLogout ;
			if (userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			 participantLogout = new Participant(userInfo.getUserAccountSession().getParticipantCode());//Obtenemo el participante de la session
			else{
				participantLogout = accountsFacade.getParticipantForIssuerDpfDpa(userInfo.getUserAccountSession().getIssuerCode());
//				idParticipantPkForIssuer=participantState.getIdParticipantPk();
			}
			
			if(OtcOperationUtils.participantIsSeller(operation, participantLogout)){//Verificamos si el participante es el vendedor
				if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.SELL.getCode())){//Si no tiene encargo, se pueden agregar cuentas
					//Para el caso que el vendedo ya registro su encargo y desea registrar el encargo de la compra
					if(operation.getOperationState().equals(OtcOperationStateType.REGISTERED.getCode()) && OtcOperationUtils.otcOperationIsSell(operation)){
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_PARTICIPANT_ASSIG);
					}
				}else{
					//Si la operacion es Venta y Si ya no tiene saldo pendiente
					if(OtcOperationUtils.otcOperationIsSell(operation) && !OtcOperationUtils.haveStockPendientByRole(operation,swapOperation, ParticipantRoleType.SELL.getCode())){
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_PARTICIPANT_ASSIG);
					}
				}
			}else if(OtcOperationUtils.participantIsBuyer(operation, participantLogout)){//Verificamos si el participante es el comprador
				if(!OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){//Si no tiene encargo, se pueden agregar cuentas
					//Si la operacion es Venta y Si ya no tiene saldo pendiente
					if(OtcOperationUtils.otcOperationIsSell(operation) && !OtcOperationUtils.haveStockPendientByRole(operation,swapOperation, ParticipantRoleType.BUY.getCode())){
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_PARTICIPANT_ASSIG);
					}
				}
			}
		}

		/**ASIGNAMOS LA CUENTA EN EL NUEVO ACCOUNT OPERATION Y PARTICIPANTES ENCARGADOS*/
		newAccountOperation.setHolderAccount(account);
		newAccountOperation.setInchargeFundsParticipant(inchargeFundsParticipant);
		newAccountOperation.setInchargeStockParticipant(inchargeSockParticipant);

		Boolean swappAddAccount = Boolean.FALSE;
		/**Si la operacion es permuta, la asignacion de cuentas es diferente pueda que vaya a Swap O mechanismOperation*/
		if(NegotiationUtils.mechanismOperationIsSwap(operation)){
			if(newAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
				newAccountOperation.setRole(ParticipantRoleType.SELL.getCode());
				newAccountOperation.setSwapOperation(swapOperation);
				swapOperation.getHolderAccountOperations().add(newAccountOperation);
				swappAddAccount = Boolean.TRUE;
			}
		}
		if(!swappAddAccount){
			newAccountOperation.setMechanismOperation(operation);
			operation.getHolderAccountOperations().add(newAccountOperation);//Agregamos Cuenta de negociacion a la Solicitud OTC
		}
	}
	
	/**
	 * Validate accont operation for secund report.
	 *
	 * @param operation the operation
	 * @param accountOperation the account operation
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void validateAccontOperationForSecundReport(MechanismOperation operation, HolderAccountOperation accountOperation, OtcOperationParameters parameters)  throws ServiceException{
		if(NegotiationUtils.mechanismOperationIsSecundaryReport(operation) && parameters.getIsQuantityPendientSecReport()){
			BigDecimal stockQuantRepoSec = BigDecimal.ZERO;
			BigDecimal stockQuantOperat = operation.getStockQuantity();
			for(HolderAccountOperation holdAccOperation: parameters.getHoldAccNegotiationSellers()){//Recorremos las cuentas vendedoras plazo dentro de la grilla
				if(holdAccOperation.getIsSelected()){//Si fue seleccionada mediante el checbBox
					BigDecimal amountSelectedSecundaryReport = holdAccOperation.getStockQuantity();//Traemos el monto de las cantidades asignadas a las cuentas seleccionadas
					if(amountSelectedSecundaryReport!=null){//Si el valor existe. NOT NULL
						stockQuantRepoSec = stockQuantRepoSec.add(holdAccOperation.getStockQuantity());//Agregamos la cantidad para luego validarla con la cantidad de la operacion
					}else{
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_SECUNDARY_REPORT_EMPTY);
					}
				}
			}
			if(!stockQuantOperat.equals(stockQuantRepoSec)){//Si no coincide con el monto de la operacion
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_SECUNDARY_REPORT_QUANTITY);
			}

			/*Traemos las cuentas Compradoras, siempre y cuando existan*/
			List<HolderAccountOperation> accountOperationsSells = OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.BUY.getCode());
			if(accountOperationsSells!=null){//Si no existen cuentas vendedoras O si es diferente de null
				operation.setHolderAccountOperations(accountOperationsSells);
			}else{
				operation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());//Boorro las cuentas compradoras, las agregare despues
			}
			for(HolderAccountOperation holdAccOperation: parameters.getHoldAccNegotiationSellers()){//Recorremos las cuentas vendedoras plazo dentro de la grilla
				if(holdAccOperation.getIsSelected()){
					HolderAccountOperation holdAccountOperation = new HolderAccountOperation();
					holdAccountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
					holdAccountOperation.setInchargeFundsParticipant(operation.getSellerParticipant());//Asignamos los participantes encargados
					holdAccountOperation.setInchargeStockParticipant(operation.getSellerParticipant());//Asignamos los participantes encargados
					holdAccountOperation.setIndIncharge(accountOperation.getIndIncharge());//Verificamos si tiene encargo
					holdAccountOperation.setRole(accountOperation.getRole());//Para repo secundario, asignamos la venta
					holdAccountOperation.setStockQuantity(holdAccOperation.getStockQuantity());//Asignamos la cantidad de valores por cuenta de titular
					holdAccountOperation.setCashAmount(holdAccOperation.getStockQuantity().multiply(operation.getCashPrice()));//Asignamos la cantidad Multiplicado con el precio sucio del la operacion 
					holdAccountOperation.setRegisterUser(userInfo.getUserAccountSession().getUserName());//Seteamos el usuario que registra
					holdAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());//Seteamos la fecha actual de la transaccion
					holdAccountOperation.setHolderAccount(holdAccOperation.getHolderAccount());//Asignamos la cuenta del titular
					holdAccountOperation.setMechanismOperation(operation);//Asignamos el objeto padre al hijo HolderAccountOperation
					holdAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());//Asignamos el estado de la cuenta para repo secundario
					holdAccountOperation.setOperationPart(OperationPartType.CASH_PART.getCode());//Seteamos el contado en la cuenta de operacion, El plazo sera seteado en la confirmacion
					operation.getHolderAccountOperations().add(holdAccountOperation);//Agregamos holderAccountOperation en la operacion
				}
			}
			parameters.setIsQuantityPendientSecReport(Boolean.FALSE);//la asignacion de cuentas vendedoras para repo secundario ya fueron agregadas
		}
	}
	
	/**
	 * Round mechanism operation.
	 *
	 * @param operation the operation
	 * @return the mechanism operation
	 */
	public MechanismOperation roundMechanismOperation(MechanismOperation operation){
		List<ParticipantRoleType> partRoleTypes = ParticipantRoleType.listSomeElements(ParticipantRoleType.SELL,ParticipantRoleType.BUY);
		for(ParticipantRoleType partType: partRoleTypes){
			BigDecimal cashAmount = CommonsUtilities.round(operation.getCashAmount(),OtcOperationParameters.OPERATION_AMOUNT_SCALE);
			BigDecimal cashPrice = operation.getCashPrice();
			BigDecimal stockQuantity = operation.getStockQuantity();
			BigDecimal cashAmountByAccount = BigDecimal.ZERO;
			BigDecimal stockQuantityByAccount = BigDecimal.ZERO;
			HolderAccountOperation lastAccountOperation = null;
			for(HolderAccountOperation holderAccountOperation: operation.getHolderAccountOperations()){
				if(partType.getCode().equals(holderAccountOperation.getRole())){
					holderAccountOperation.setCashAmount(cashPrice.multiply(holderAccountOperation.getStockQuantity()));
					cashAmountByAccount = (cashAmountByAccount.add(CommonsUtilities.round(holderAccountOperation.getCashAmount(),OtcOperationParameters.OPERATION_AMOUNT_SCALE)));
					stockQuantityByAccount = stockQuantityByAccount.add(holderAccountOperation.getStockQuantity());
					lastAccountOperation = holderAccountOperation;
				}
			}
			if(stockQuantityByAccount.equals(stockQuantity)){
				if(cashAmountByAccount.compareTo(cashAmount)!=0){
					BigDecimal lastCashAmount = lastAccountOperation.getCashAmount();
					BigDecimal roundOperation = cashAmountByAccount.subtract(cashAmount);
					BigDecimal cashAmountRound = lastCashAmount.subtract(roundOperation.abs());
					lastAccountOperation.setCashAmount(cashAmountRound);
				}
			}
		}
		return operation;
	}
	
	/**
	 * Validate new market fact operation.
	 *
	 * @param accountMarket the account market
	 * @param accountOperation the account operation
	 * @throws ServiceException the service exception
	 */
	public void validateNewMarketFactOperation(AccountOperationMarketFact accountMarket, HolderAccountOperation accountOperation) throws ServiceException{
		BigDecimal stockQuantitMarkFactAdded = BigDecimal.ZERO;
		BigDecimal stockQuantitEntered = accountMarket.getOperationQuantity();
		BigDecimal stockQuantitAccOpe = accountOperation.getStockQuantity();
		if(accountMarket.getMarketRate()!=null){
			BigDecimal marketRateEntered = CommonsUtilities.round(accountMarket.getMarketRate(),4);
			accountMarket.setMarketRate(marketRateEntered);
		}
		
		for(AccountOperationMarketFact marketFact: accountOperation.getAccountOperationMarketFacts()){
			stockQuantitMarkFactAdded = stockQuantitMarkFactAdded.add(marketFact.getOperationQuantity());
			if(accountMarket.getMarketDate().equals(marketFact.getMarketDate())){
				Date currentFormat = CommonsUtilities.truncateDateTime(accountMarket.getMarketDate());
				if(accountMarket.getMarketPrice()!=null && accountMarket.getMarketPrice().equals(marketFact.getMarketPrice())){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_MARKETFACT, new Object[]{currentFormat});
				}
				if(accountMarket.getMarketRate()!=null && accountMarket.getMarketRate().equals(marketFact.getMarketRate())){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_MARKETFACT, new Object[]{currentFormat});
				}
			}
		}
		
		/*VALIDAMOS SI EL MONTO EXCEDE AL INGRESADO*/
		if((stockQuantitMarkFactAdded.add(stockQuantitEntered)).compareTo(stockQuantitAccOpe)>0){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_MARKETFACT_QUANT_SUM);
		}
		
		accountMarket.setHolderAccountOperation(accountOperation);
		accountOperation.getAccountOperationMarketFacts().add(accountMarket);
	}
	
	/**
	 * Validate before save new operation.
	 *
	 * @param operation the operation
	 * @param swapOperation the swap operation
	 * @throws ServiceException the service exception
	 */
	public void validateBeforeSaveNewOperation(MechanismOperation operation, SwapOperation swapOperation) throws ServiceException{
		MechanismModality mechanismModality = operation.getMechanisnModality();
		NegotiationModality negotiationModality = mechanismModality.getNegotiationModality();
		Integer indPrimaryPlacement = negotiationModality.getIndPrimaryPlacement();
		/**VALIDAMOS HECHOS DE MERCADO PARA LAS CUENTAS*/
		/*for(HolderAccountOperation accountOperation: operation.getHolderAccountOperations()){
			HolderAccount account = accountOperation.getHolderAccount();
			BigDecimal stockQuantitMarkFact = BigDecimal.ZERO;
			if(accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode()) && account!=null && account.getIdHolderAccountPk()!=null && 
			   !indPrimaryPlacement.equals(BooleanType.YES.getCode())){
				if(accountOperation.getAccountOperationMarketFacts()==null || accountOperation.getAccountOperationMarketFacts().isEmpty()){
					Long accountPk = account.getIdHolderAccountPk();
					Long partCode  = operation.getSellerParticipant().getIdParticipantPk();
					String secCode = operation.getSecurities().getIdSecurityCodePk();
					List<HolderMarketFactBalance> accountMarketFact = getMarketFactBalance(partCode, secCode, accountPk);
					if(accountMarketFact!=null && accountMarketFact.size() == ComponentConstant.ONE){
						HolderMarketFactBalance ham = accountMarketFact.get(0);
						AccountOperationMarketFact aom = new AccountOperationMarketFact();
						aom.setMarketDate(ham.getMarketDate());
						aom.setMarketRate(ham.getMarketRate());
						aom.setMarketPrice(ham.getMarketPrice());
						aom.setHolderAccountOperation(accountOperation);
						aom.setOperationQuantity(operation.getStockQuantity());
						aom.setOriginalQuantity(operation.getStockQuantity());
						accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
						accountOperation.getAccountOperationMarketFacts().add(aom);
					}else if(accountMarketFact!=null && accountMarketFact.isEmpty()){
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_PARTICIPANT_WITHOUT_BALANCE);
					}else{
						accountOperation.setIndMultipleMarketFact(ComponentConstant.ONE);
					}
				}
				
				
				for(AccountOperationMarketFact accountMark: accountOperation.getAccountOperationMarketFacts()){
					stockQuantitMarkFact = stockQuantitMarkFact.add(accountMark.getOperationQuantity());
				}
				if(!stockQuantitMarkFact.equals(accountOperation.getStockQuantity())){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_MARKETFACT_INCOMPLETE, new Object[]{account.getAccountNumber().toString()});
				}
			}
		}*/
		
		/**SI AUN TIENE CUENTAS POR ASIGNAR*/
		if(OtcOperationUtils.haveStockPendientByRole(operation,swapOperation, ParticipantRoleType.SELL.getCode())){
			/*SI NO SE HAN AGREGADO CUENTAS EN LA VENTA*/
			if(OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.SELL.getCode()).isEmpty()){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_SELL_EMPTY);
			}else{
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_SELL);
			}
		}
		
		if(OtcOperationUtils.otcOperationIsCrusade(operation)){//Si la operacion es cruzada
			if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.BUY.getCode())){//Si aun tiene cuentas compradoras por asignar
				if(OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.BUY.getCode()).isEmpty()){//Si aun no se han agregado cuentas en la compra
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY);
				}else{
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY);
				}
			}
		}
		else{
			if(OtcOperationUtils.haveStockPendientByRole(operation,swapOperation, ParticipantRoleType.BUY.getCode())){//Si aun tiene cuentas por asignar en la compra
				if(userInfo.getUserAccountSession().isDepositaryInstitution()){
					if(OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.BUY.getCode()).isEmpty()){//Si no se han agregado cuentas en la venta
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY);
					}else{
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY);
					}
				}
			}
		}
		
		if(operation.getIndTermSettlement().equals(BooleanType.YES.getCode())) {
			if(Validations.validateIsNull(operation.getWarrantyCause())) {
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_WARRANTY_CAUSE_EMPTY);
			}
			if(Validations.validateIsNull(operation.getTermInterestRate())) {
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_TERM_INTEREST_RATE);
			}
			if(Validations.validateIsNull(operation.getTermSettlementDays())) {
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_SETTLEMENT_TERM_DAY);
			}
			if(Validations.validateIsNull(operation.getTermPrice())) {
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_TERM_PRICE);
			}
			if(Validations.validateIsNull(operation.getTermAmount())) {
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_TERM_AMOUNT);
			}
		}
	}
	
	/**
	 * Validate before review operation.
	 *
	 * @param operation the operation
	 * @param swapOperation the swap operation
	 * @throws ServiceException the service exception
	 */
	public void validateBeforeReviewOperation(MechanismOperation operation, SwapOperation swapOperation) throws ServiceException{
		if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.BUY.getCode())){
			if(OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.BUY.getCode()).isEmpty()){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY);
			}else{//caso contrario aun no se completa la cantidad de la operacion
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY);
			}
		}
	}
	
	/**
	 * Validate before approve operation.
	 *
	 * @param operation the operation
	 * @param swapOperation the swap operation
	 * @throws ServiceException the service exception
	 */
	public void validateBeforeApproveOperation(MechanismOperation operation, SwapOperation swapOperation) throws ServiceException{
		/*VALIDAMOS HECHOS DE MERCADO PARA LAS CUENTAS*/
		for(HolderAccountOperation accountOperation: operation.getHolderAccountOperations()){
			HolderAccount account = accountOperation.getHolderAccount();
			/*BigDecimal stockQuantitMarkFact = BigDecimal.ZERO;
			if(accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode()) && account!=null && account.getIdHolderAccountPk()!=null){
				for(AccountOperationMarketFact accountMark: accountOperation.getAccountOperationMarketFacts()){
					stockQuantitMarkFact = stockQuantitMarkFact.add(accountMark.getOperationQuantity());
				}
				if(!stockQuantitMarkFact.equals(accountOperation.getStockQuantity())){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_MARKETFACT_INCOMPLETE, new Object[]{account.getAccountNumber().toString()});
				}
			}*/
		}
		
		
		if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.SELL.getCode())){
			if(OtcOperationUtils.haveStockPendientInChargedByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
				Participant participantInCharged = OtcOperationUtils.getParticipantInChargedByRole(operation, ParticipantRoleType.SELL.getCode());
				if(OtcOperationUtils.getHoldAccOperationsByPartSellInCharge(operation, participantInCharged).isEmpty()){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_SELL_EMPTY);
				}else{
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY);
				}
			}
		}
	}
	
	/**
	 * Validate before confirm operation.
	 *
	 * @param operation the operation
	 * @param swapOperation the swap operation
	 * @throws ServiceException the service exception
	 */
	public void validateBeforeConfirmOperation(MechanismOperation operation, SwapOperation swapOperation) throws ServiceException{
		if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){
			if(OtcOperationUtils.haveStockPendientInChargedByRole(operation, swapOperation, ParticipantRoleType.BUY.getCode())){
				Participant participantInCharged = OtcOperationUtils.getParticipantInChargedByRole(operation, ParticipantRoleType.BUY.getCode());
				if(OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantInCharged).isEmpty()){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY);
				}else{
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY);
				}
			}
		}
	}
	
	public void validateBeforeAuthorizeOperation(MechanismOperation operation, SwapOperation swapOperation) throws ServiceException{
		if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){
			if(OtcOperationUtils.haveStockPendientInChargedByRole(operation, swapOperation, ParticipantRoleType.BUY.getCode())){
				Participant participantInCharged = OtcOperationUtils.getParticipantInChargedByRole(operation, ParticipantRoleType.BUY.getCode());
				if(OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantInCharged).isEmpty()){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY);
				}else{
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY);
				}
			}
		}
	}
	
	public void validateBeforeSettlementOperation(MechanismOperation operation, SwapOperation swapOperation) throws ServiceException{
		if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){
			if(OtcOperationUtils.haveStockPendientInChargedByRole(operation, swapOperation, ParticipantRoleType.BUY.getCode())){
				Participant participantInCharged = OtcOperationUtils.getParticipantInChargedByRole(operation, ParticipantRoleType.BUY.getCode());
				if(OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantInCharged).isEmpty()){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY_EMPTY);
				}else{
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY);
				}
			}
		}
	}
	
	/**
	 * *
	 * Get securityObject from key.
	 *
	 * @param securityCode the security code
	 * @return the security from key
	 * @throws ServiceException the service exception
	 */
	public Security getSecurityFromKey(String securityCode) throws ServiceException{
		Security security = new Security(securityCode);
		return otcOperationServiceBean.findSecurityFromCode(security);
	}
	
	/**
	 * *
	 * Method to get HolderAccount from role .
	 *
	 * @param participantCode the participant code
	 * @param holdeCode the holde code
	 * @param securityCode the security code
	 * @param role the role
	 * @return the holder account
	 * @throws ServiceException the service exception
	 * @param. holdeCode
	 */
	public HolderAccountBalance getHolderAccount(Long participantCode, Long holdeCode, String securityCode, Integer role) throws ServiceException{
		HolderAccountBalance hab = new HolderAccountBalance();
		/**Inizializated participant entity*/
		Security security = new Security();
		security.setIdSecurityCodePk(securityCode);
		/**Inizializated participant entity*/
		Participant participant = new Participant();
		participant.setIdParticipantPk(participantCode);
		/**Get All holderAccounts in list*/
		//List<HolderAccount> lstAccount = accountServiceBean.getAccountsHolderAndParticipant(holdeCode,participantCode);
		HolderAccount objHolderAccount = accountServiceBean.getHolderAccountByParticipant(holdeCode, participantCode);
		if(objHolderAccount != null){
			/**If holder is seller*/
			if(role.equals(ComponentConstant.SALE_ROLE)){
				/**Verified if seller have balances to settlement*/
				hab = otcOperationServiceBean.getBalanceFromSecurity(participant, objHolderAccount, security, Boolean.FALSE);
				if(hab != null){
					hab.setHolderAccount(objHolderAccount);
					return hab;
				}
			}else{
				return hab;  
			}
		}
		return null;
	}
	
	public HolderAccountBalance getHolderAccountBalance (Participant p, HolderAccount h, Security s) throws ServiceException{
		HolderAccountBalance hab = new HolderAccountBalance();
		hab = otcOperationServiceBean.getBalanceFromSecurity(p, h, s, Boolean.FALSE);			
		if(hab != null){			
			return hab;
		}
		return null;
	}
	
	
	public HolderAccountBalance getHolderAccountBalance (Participant p, HolderAccount h, Security s, Boolean searchOnlyAvailable) throws ServiceException{
		HolderAccountBalance hab = new HolderAccountBalance();
		hab = otcOperationServiceBean.getBalanceFromSecurity(p, h, s, searchOnlyAvailable);			
		if(hab != null){			
			return hab;
		}
		return null;
	}
	
	/**
	 * *
	 * Method to get list of HolderMarketFactBalance.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param accountPk the account pk
	 * @return the market fact balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderMarketFactBalance> getMarketFactBalance(Long participantCode, String securityCode, Long accountPk) throws ServiceException{
		/**Inizializated participant entity*/
		Security security = new Security();
		security.setIdSecurityCodePk(securityCode);
		/**Inizializated participant entity*/
		Participant participant = new Participant();
		participant.setIdParticipantPk(participantCode);
		/**Inizitalizated holderAccount*/
		HolderAccount account = new HolderAccount();
		account.setIdHolderAccountPk(accountPk);
		return otcOperationServiceBean.getMarketFactBalance(participant, account, security);
	}
	

	/**
	 * Fill operation with market fact.
	 *
	 * @param sec the sec
	 * @param p_sell the p_sell
	 * @param p_buy the p_buy
	 * @param a_sell the a_sell
	 * @param a_buy the a_buy
	 * @param stockQuantity the stock quantity
	 * @param marketFactList the market fact list
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation fillOperationWithMarketFact(Security sec, Participant p_sell, Participant p_buy, HolderAccount a_sell, HolderAccount a_buy, 
														  BigDecimal stockQuantity, List<HolderMarketFactBalance> marketFactList) throws ServiceException{
		
		
		List<HolderMarketFactBalance> marketBalanceList = getMarketFactBalance(p_sell.getIdParticipantPk(), sec.getIdSecurityCodePk(), a_sell.getIdHolderAccountPk());
		
		/**Verificar si tu lista del parametro existe en los saldos de la BD en todos los HM y cantidad de valores*/
		//si existe: usar fillMechanismOperation(Security sec, Participant p_sell, Participant p_buy, HolderAccount a_sell, HolderAccount a_buy,stockQuantty, List HM);
		
		//caso contrario: lanzar un nuevo throw new ServiceException(ErrorServiceType.ERROR_TUYO);
		//		parecido a este throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_ACCOUNT_QUANT_BUY);
		
		return null;
	}
	
	/**
	 * *
	 * Method to fill MechanismOperation.
	 *
	 * @param sec the sec
	 * @param p_sell the p_sell
	 * @param p_buy the p_buy
	 * @param a_sell the a_sell
	 * @param a_buy the a_buy
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation fillMechanismOperation(Security sec, Participant p_sell, Participant p_buy, HolderAccount a_sell, HolderAccount a_buy, BigDecimal stockQuantity)
																																throws ServiceException{
		
		/**Logger User*/
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		/**Get mechanismOperation empty*/
		MechanismOperation operation = sirtexFacade.configNewMechanismOperation(NegotiationMechanismType.OTC.getCode(),
																				NegotiationModalityType.CASH_FIXED_INCOME.getCode(),null);
		
		/**Get object to handle MechanisModality*/
		MechanismModality mechModality = otcOperationServiceBean.getMechanismModality(operation.getMechanisnModality().getId().getIdNegotiationMechanismPk(), 
																					  operation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		
		/**Trade Operation*/
		NegotiationModalityType negModalityType = NegotiationModalityType.get(mechModality.getId().getIdNegotiationModalityPk());
		TradeOperation tradeOperation = new TradeOperation();
		tradeOperation.setRegisterUser(loggerUser.getUserName());
		tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		tradeOperation.setOperationState(OtcOperationStateType.REGISTERED.getCode());
		tradeOperation.setOperationType(negModalityType.getParameterOperationType());
		tradeOperation.setAudit(loggerUser);
		
		/**Assigment tradeOperation into MechanismOperation*/
		operation.setTradeOperation(tradeOperation);
		
		/**Assigment mechanismModality into MechanismOperation*/
		operation.setMechanisnModality(mechModality);
		
		/**Get settlementType*/
		Integer settlementType 	 = mechModality.getSettlementType();
		Integer settlementScheme = mechModality.getSettlementSchema();
		
		/**Get price of operation with nominalValue*/
		BigDecimal cashPrice = sec.getCurrentNominalValue();
		Integer securityCurr = sec.getCurrency();
		
		/**Get exchangeRate from bd*/
		DailyExchangeRates rate = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());
		/**Variable to get ExchangeRate*/
		BigDecimal exchangeRate = null;
		/**If exchange Rate exist, assgiment value*/
		if(rate!=null){
			exchangeRate = rate.getAveragePrice();
		}else{/**however if doesn't exist, put one like value*/
			exchangeRate = new BigDecimal(ComponentConstant.ONE);
		}

		/**Get marketFact*/
		Long partCode = p_sell.getIdParticipantPk();
		String secCode = sec.getIdSecurityCodePk();
		Long accountCod= a_sell.getIdHolderAccountPk();
		List<HolderMarketFactBalance> marketBalanceList = getMarketFactBalance(partCode, secCode, accountCod);
		
		/**Fill operation with data*/
		operation.setSecurities(sec);
		operation.setCashPrice(cashPrice);
		operation.setCurrency(securityCurr);
		operation.setBuyerParticipant(p_buy);
		operation.setSellerParticipant(p_sell);
		operation.setExchangeRate(exchangeRate);
		operation.setTermPrice(BigDecimal.ZERO);
		operation.setTermAmount(BigDecimal.ZERO);
		operation.setSettlementType(settlementType);
		operation.setSettlementSchema(settlementScheme);
		operation.setRegisterUser(loggerUser.getUserName());
		operation.setRealCashPrice(operation.getCashPrice());
		operation.setRegisterDate(CommonsUtilities.currentDateTime());
		operation.setCashSettlementDate(operation.getOperationDate());
		//if rte or tra
		if (!(stockQuantity==null)){
			operation.setStockQuantity(stockQuantity);
		}else{
			operation.setStockQuantity(new BigDecimal(ComponentConstant.ONE));
		}
		operation.setCashSettlementDays(new Long(ComponentConstant.ONE));
		operation.setCashAmount(operation.getStockQuantity().multiply(operation.getCashPrice()));
		operation.setRealCashAmount(operation.getStockQuantity().multiply(operation.getRealCashPrice()));
		operation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		

		/**Set indicator by default in mechanismOperation*/
		operation.setIndTermSettlement(mechModality.getNegotiationModality().getIndTermSettlement());
		operation.setIndPrimaryPlacement(mechModality.getNegotiationModality().getIndPrimaryPlacement());
		operation.setIndPrincipalGuarantee(mechModality.getNegotiationModality().getIndPrincipalGuarantee());
		operation.setIndCashSettlementExchange(BooleanType.NO.getCode());
		operation.setIndMarginGuarantee(mechModality.getNegotiationModality().getIndMarginGuarantee());
		operation.setIndReportingBalance(mechModality.getNegotiationModality().getIndReportingBalance());
		operation.setIndCashStockBlock(mechModality.getNegotiationModality().getIndCashStockBlock());
		operation.setIndTermStockBlock(mechModality.getNegotiationModality().getIndTermStockBlock());
		operation.setIndTermSettlementExchange(mechModality.getIndTermCurrencyExchange());
		
		/**CONFIGURAMOS LOS ACCOUNT OPERATION*/
		operation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		/**Iterate accounts*/
		for(HolderAccount account: new HolderAccount[]{a_buy,a_sell}){
			/**If account is different than null*/
			if(account!=null){

				/**Variable to handle role and participant*/
				Integer role = null;
				Participant p = null;
				if(account.equals(a_buy)){
					role = ComponentConstant.PURCHARSE_ROLE;
					p	 = p_buy;
				}else if(account.equals(a_sell)){
					role = ComponentConstant.SALE_ROLE;
					p	 = p_sell;
				}

				/**Iterate holder account operation*/
				HolderAccountOperation accountOperation = new HolderAccountOperation();
				accountOperation.setRegisterDate(operation.getRegisterDate());
				accountOperation.setRegisterUser(operation.getRegisterUser());
				accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());			
				accountOperation.setHolderAccount(account);
				accountOperation.setIndIncharge(BooleanType.NO.getCode());
				accountOperation.setInchargeFundsParticipant(p);
				accountOperation.setInchargeStockParticipant(p);
				accountOperation.setStockQuantity(operation.getStockQuantity());
				accountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
				accountOperation.setMechanismOperation(operation);
				accountOperation.setRole(role);
				accountOperation.setOperationPart(OperationPartType.CASH_PART.getCode());
				accountOperation.setCashAmount(operation.getCashAmount());
				accountOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
				accountOperation.setSettlementAmount(operation.getCashPrice().multiply(accountOperation.getStockQuantity()));
				
				/**VERIFICAMOS SI EXISTEN HECHOS DE MERCADO*/
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					/**CONFIGURAMOS LOS HECHOS DE MERCADO*/
					for(HolderMarketFactBalance accTradeMarketFact: marketBalanceList){
						AccountOperationMarketFact accountMarkFact = new AccountOperationMarketFact();
						accountMarkFact.setMarketDate(accTradeMarketFact.getMarketDate());
						accountMarkFact.setMarketRate(accTradeMarketFact.getMarketRate());
						accountMarkFact.setMarketPrice(accTradeMarketFact.getMarketPrice());
						accountMarkFact.setOperationQuantity(operation.getStockQuantity());
						accountMarkFact.setHolderAccountOperation(accountOperation);
						accountOperation.getAccountOperationMarketFacts().add(accountMarkFact);
					}
				}
				operation.getHolderAccountOperations().add(accountOperation);
			}
		}
		return operation;
	}
	
	/**
	 * *
	 * Method to fill, create, confirm, block balance and settlement one mechanismOperation.
	 *
	 * @param sec the sec
	 * @param p_sell the p_sell
	 * @param p_buy the p_buy
	 * @param a_sell the a_sell
	 * @param a_buy the a_buy
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation createAndSettlementOperation(Security sec, Participant p_sell, Participant p_buy, HolderAccount a_sell, HolderAccount a_buy, BigDecimal stockQuantity)
																																	throws ServiceException{

		/**Fill mechanismOperation With data*/
		MechanismOperation operation = fillMechanismOperation(sec, p_sell, p_buy, a_sell, a_buy, stockQuantity);
		/**Create MechanismOperation*/
		MechanismOperationAdj mechanismOperationAdj = new MechanismOperationAdj();//empty obj for no errors
		otcOperationServiceBean.createNewMechanismOperation(operation, mechanismOperationAdj);
		/**Confirm MechanismOperation*/
		otcOperationServiceBean.confirmOtcMechanismOperation(operation);
		/**loock balance of mechanismOperation*/
		Integer operationPart = ComponentConstant.CASH_PART;
		Long operationPk 	  = operation.getIdMechanismOperationPk(); 
		otcSettlementService.lockBalancesOfOperation(operationPk, operationPart);
		/**settlement balance of mechanismOperation*/
		otcSettlementService.settlementBalancesOfOperation(operationPk, operationPart, operation.getCashSettlementDate());
		/**Validate if all balances have been settled*/
		sirtexFacade.validateOperationSettlements(Arrays.asList(operationPk),operationPart);
		
		return operation;
	}
	
	/**
	 * Creates the and settlement operation with hm.
	 *
	 * @param sec the sec
	 * @param p_sell the p_sell
	 * @param p_buy the p_buy
	 * @param a_sell the a_sell
	 * @param a_buy the a_buy
	 * @param stockQuantity the stock quantity
	 * @param marketFactList the market fact list
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation createAndSettlementOperationWithHM(Security sec, Participant p_sell, Participant p_buy, HolderAccount a_sell, HolderAccount a_buy,
																BigDecimal stockQuantity, List<HolderMarketFactBalance> marketFactList)
			throws ServiceException{
		/**Fill mechanismOperation With data*/
		MechanismOperation operation = fillOperationWithMarketFact(sec, p_sell, p_buy, a_sell, a_buy,stockQuantity,marketFactList);
		/**Create MechanismOperation*/
		MechanismOperationAdj mechanismOperationAdj = new MechanismOperationAdj();//empty obj for no errors
		otcOperationServiceBean.createNewMechanismOperation(operation, mechanismOperationAdj);
		/**Confirm MechanismOperation*/
		otcOperationServiceBean.confirmOtcMechanismOperation(operation);
		/**loock balance of mechanismOperation*/
		Integer operationPart = ComponentConstant.CASH_PART;
		Long operationPk 	  = operation.getIdMechanismOperationPk(); 
		otcSettlementService.lockBalancesOfOperation(operationPk, operationPart);
		/**settlement balance of mechanismOperation*/
		otcSettlementService.settlementBalancesOfOperation(operationPk, operationPart, operation.getCashSettlementDate());
		/**Validate if all balances have been settled*/
		sirtexFacade.validateOperationSettlements(Arrays.asList(operationPk),operationPart);		
		return operation;
	}
	
	/**
	 * Find security from serie.
	 *
	 * @param securitySerial the security serial
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityFromSerie(String securitySerial) throws ServiceException{
		return otcOperationServiceBean.findSecurityFromSerie(securitySerial);
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param idHolderAccount the id holder account
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(Long idHolderAccount) throws ServiceException{		
		return otcOperationServiceBean.getHolderAccount(idHolderAccount);
	}
	
	public ParameterTable findParameterTableLongTxt() throws ServiceException{
		return otcOperationServiceBean.findParameterTableLongTxt();
	}
	
	
	/**
	 * Get participante for id
	 * @param idParticipantPk
	 * @return
	 * @throws ServiceException
	 */
	public Participant findParticipantFromId(Long idParticipantPk) throws ServiceException{
		return  otcOperationServiceBean.getParticipantFromId(idParticipantPk);
	}
	

	public List<AccountAnnotationOperation> findAccountAnnotationOTCMotive(List<Long> lstAccountAnnotationPk) throws ServiceException{
		return  otcOperationServiceBean.findAccountAnnotationOTCMotive(lstAccountAnnotationPk);
	}
	
	public HolderAccountBalance getOwnerOfSecurity(String idSecurityCodePk, Long idParticipantPk) {
		List<HolderAccountBalance> lstRes = this.negotiationOperationServiceBean.getHolderAccountBySecurity(idSecurityCodePk, idParticipantPk);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstRes)) {
			if(lstRes.size() > 1) {
				return null;
			} else {
				return lstRes.get(0);
			}
		} else {
			return null;
		}
	}
	
	public Boolean checkIfSecurityHasAnExpiringCouponToday(String idSecurityCodePk) {
		Long count = this.negotiationOperationServiceBean.countCouponExpiringByDate(new Date(), idSecurityCodePk);
		
		return Validations.validateIsPositiveNumber(count);
	}
	
}