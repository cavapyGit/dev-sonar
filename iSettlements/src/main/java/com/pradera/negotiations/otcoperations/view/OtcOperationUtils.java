package com.pradera.negotiations.otcoperations.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.negotiations.otcoperations.to.OtcOperationResultTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/07/2013
 */
public class OtcOperationUtils  implements Serializable{
	
	/**
	 * Gets the operation result.
	 *
	 * @param objResult the obj result
	 * @return the operation result
	 */
	public static OtcOperationResultTO getOperationResult(Object[] objResult){
		OtcOperationResultTO octOperationResult = new OtcOperationResultTO();
		octOperationResult.setIdMechanismOperationRequestPk ((Long)objResult[0]);
		octOperationResult.setIdNegotiationModalityPk ((Long)objResult[1]);
		octOperationResult.setModalityName ((String)objResult[2]);
		octOperationResult.setOperationDate ((Date)objResult[3]);
		octOperationResult.setOperationNumber ((Long)objResult[4]);
//		octOperationResult.setReferenceNumber ((String)objResult[5]);
		octOperationResult.setIdPartSeller ((Long)objResult[5]);
		octOperationResult.setMnemoPartSeller ((String)objResult[6]);
		octOperationResult.setIdPartBuyer ((Long)objResult[7]);
		octOperationResult.setMnemoPartBuyer ((String)objResult[8]);
		octOperationResult.setSettlementCashDate ((Date)objResult[9]);
		octOperationResult.setSettlementTermDate ((Date)objResult[10]);
		octOperationResult.setSecurityCode((String)objResult[11]);
		octOperationResult.setOperationState ((Integer)objResult[12]);
		octOperationResult.setRegisterUser((String)objResult[13]);
		octOperationResult.setReviewUser((String)(objResult[14]!=null?objResult[14]:""));
		octOperationResult.setSettlementType((objResult[15]!=null?(Integer)objResult[15]:null));
		
		return octOperationResult;
	}
	/**
	 * Have in charge.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean haveInCharge(MechanismOperation otcOperation){
		List<HolderAccountOperation> holdAccOperationList = null;
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return Boolean.FALSE;
		}else if(otcOperation.getHolderAccountOperations()!=null){
			holdAccOperationList = otcOperation.getHolderAccountOperations();
		}		
		if(holdAccOperationList!=null){//Verificamos si existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList) && !holdAccOperationList.isEmpty()){
				for(HolderAccountOperation holderAccountOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					if(holderAccountOperation.getIndIncharge().equals(BooleanType.YES.getCode())){//Verificamos Si tiene Encargo
						return Boolean.TRUE;
					}
				}
			}			
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Otc operation have recursive.
	 * metodo que me permite saber, si la operacion OTC tiene O no, sus HolderAccountOperation Recursivos
	 * para el caso de Liquidacion Contado y Plazo
	 * @param otcOperationTO the otc operation to
	 * @return the boolean
	 */
	public static Boolean otcOperationHaveRecursive(OtcOperationTO otcOperationTO){
		List<NegotiationModalityType> negModTypeList = 
				NegotiationModalityType.listSomeElements(NegotiationModalityType.REPORT_EQUITIES,
														 NegotiationModalityType.REPORT_FIXED_INCOME,
														 NegotiationModalityType.VIEW_REPO_EQUITIES,
														 NegotiationModalityType.SECURITIES_LOAN_EQUITIES,
														 NegotiationModalityType.SECURITIES_LOAN_FIXED_INCOME,
														 NegotiationModalityType.VIEW_REPO_EQUITIES,
														 NegotiationModalityType.VIEW_REPO_FIXED_INCOME,
														 NegotiationModalityType.REPURCHASE_EQUITIES,
														 NegotiationModalityType.REPURCHASE_FIXED_INCOME);
		if(negModTypeList.contains(NegotiationModalityType.get(otcOperationTO.getIdModalityNegotiation()))){//La modalidad es Reporto
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	/**
	 * Holder account negotiation is sell.
	 *
	 * @param holdAccountOperation the hold account operation
	 * @return the boolean
	 */
	public static Boolean holderAccountNegotiationIsSell(HolderAccountOperation holdAccountOperation){
		if(holdAccountOperation==null || holdAccountOperation.getRole()==null){
			return Boolean.FALSE;
		}
		return holdAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode());
	}
	/**
	 * Holder account negotiation is buy.
	 *
	 * @param holdAccountOperation the hold account operation
	 * @return the boolean
	 */
	public static Boolean holderAccountNegotiationIsBuy(HolderAccountOperation holdAccountOperation){
		if(holdAccountOperation==null){
			return Boolean.FALSE;
		}
		return holdAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode());
	}
	/**
	 * Have in charge by participant role.
	 *
	 * @param otcOperation the otc operation
	 * @param role the participant role type
	 * @return the boolean
	 */
	public static Boolean haveInChargeByParticipantRole(MechanismOperation otcOperation, Integer role){
		List<HolderAccountOperation> holdAccOperationList  = null;
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return Boolean.FALSE;
		}else if(otcOperation.getHolderAccountOperations()!=null){
			holdAccOperationList = otcOperation.getHolderAccountOperations();
		}		
		if(holdAccOperationList!=null){//Verificamos si existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList)){
				for(HolderAccountOperation holdAccOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					//Verificamos Si tiene Encargo y si Coincide con el Rol de la Participacion
					if(holdAccOperation.getIndIncharge().equals(BooleanType.YES.getCode()) && holdAccOperation.getRole().equals(role)){
						return Boolean.TRUE;
					}
				}
			}			
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Have amount pendient by participation role.
	 * metodo me permite saber si existen cuentas negociadoras con monto pendiente de asignacion sobre la cantidad total
	 * de la operacion OTC.
	 *
	 * @param otcOperation the otc operation
	 * @param swapOperation the swap operation
	 * @param role the participant role
	 * @return the boolean
	 */
	public static Boolean haveStockPendientByRole(MechanismOperation otcOperation, SwapOperation swapOperation, Integer role){
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return Boolean.FALSE;
		}
		List<HolderAccountOperation> holdAccOperationList = otcOperation.getHolderAccountOperations();
		if(holdAccOperationList!=null || Hibernate.isInitialized(holdAccOperationList)){//Si es null no existen cuentas negociadoras
			BigDecimal stockByParticipationRole = BigDecimal.ZERO;//Variable para almacenar el monto asignado de cuentas, por participacion (COMPRADOR,VENDEDOR)
			
			stockByParticipationRole = getStockQuantityAddedFromOperation(otcOperation, swapOperation, role);
			BigDecimal stockQuantity = getStockQuantityFromOperation(otcOperation, swapOperation, role);//Obtenemos el stockQuantity de la operacion(Soporta tambien permuta)
			if(stockQuantity!=null){
				if(stockByParticipationRole.compareTo(stockQuantity)<BigDecimal.ZERO.intValue()){//Si es mayor que la cantidad de la operacion
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the stock quantity added from operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param swapOperation the swap operation
	 * @param role the participation role
	 * @return the stock quantity added from operation
	 */
	public static BigDecimal getStockQuantityAddedFromOperation(MechanismOperation mechanismOperation, SwapOperation swapOperation, Integer role){
		if(mechanismOperation==null || mechanismOperation.getStockQuantity()==null || role==null){
			return null;
		}
		BigDecimal stockByParticipationRole = BigDecimal.ZERO;//Variable para almacenar el monto asignado de cuentas, por participacion (COMPRADOR,VENDEDOR) 
		
		if(NegotiationUtils.mechanismOperationIsSwap(mechanismOperation) && ParticipantRoleType.BUY.getCode().equals(role)){
			if(swapOperation!=null){
				for(HolderAccountOperation holdAccOperation: swapOperation.getHolderAccountOperations()){//Recorremos cuentas negociadoras
					//Verificamos Si Coincide con el Rol de la Participacion
					if(holdAccOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
						stockByParticipationRole = stockByParticipationRole.add(holdAccOperation.getStockQuantity());
					}
				}
			}
		}else{
			for(HolderAccountOperation holdAccOperation: mechanismOperation.getHolderAccountOperations()){//Recorremos cuentas negociadoras
				//Verificamos Si Coincide con el Rol de la Participacion
				if(holdAccOperation.getRole().equals(role)){
					stockByParticipationRole = stockByParticipationRole.add(holdAccOperation.getStockQuantity());
				}
			}
		}
		return stockByParticipationRole;
	} 
	
	/**
	 * Gets the stock quantity from operation.
	 * metodo para retornar el StockQuantity de alguna operacion
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param swapOperation the swap operation
	 * @param role the participation role
	 * @return the stock quantity from operation
	 */
	public static BigDecimal getStockQuantityFromOperation(MechanismOperation mechanismOperation, SwapOperation swapOperation, Integer role){
		if(mechanismOperation==null || mechanismOperation.getStockQuantity()==null || role==null){
			return null;
		}
		if(NegotiationUtils.mechanismOperationIsSwap(mechanismOperation)){
			if(ParticipantRoleType.BUY.getCode().equals(role)){
				if(mechanismOperation.getSwapOperations()!=null){
					return swapOperation.getStockQuantity();
				}
				return null;
			}
		}
		return mechanismOperation.getStockQuantity();
	}
	
	/**
	 * Have stock pendient with negotiation by role.
	 * metodo para validar si la cantidad en cuenta negociadora es suficiente con la cantidad actual sobre la cantidad de la operacion con respecto a el Rol
	 *
	 * @param otcOperation the otc operation
	 * @param swapOperation the swap operation
	 * @param holdAccOperation the hold acc operation
	 * @param role the part role
	 * @return the boolean
	 */
	public static Boolean haveStockPendWithNegByRole(MechanismOperation otcOperation, SwapOperation swapOperation, HolderAccountOperation holdAccOperation, Integer role){
		List<HolderAccountOperation> holdAccOperationList = null;
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return Boolean.FALSE;
		}else if(otcOperation.getHolderAccountOperations()!=null){
			holdAccOperationList = otcOperation.getHolderAccountOperations();
		}		
		if(holdAccOperationList!=null){//Si es null no existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList)){
				BigDecimal stockByParticipationRole = BigDecimal.ZERO;//Variable para almacenar el monto asignado de cuentas, por participacion (COMPRADOR,VENDEDOR) 
				for(HolderAccountOperation holderAccountOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					//Verificamos Si Coincide con el Rol de la Participacion
					if(holderAccountOperation.getRole().equals(role)){
						if(holderAccountOperation.getIndIncharge().equals(BooleanType.NO.getCode())){
							if(NegotiationUtils.mechanismOperationIsSwap(otcOperation)){
								if(holderAccountOperation.getMechanismOperation()!=null){
									stockByParticipationRole = stockByParticipationRole.add(holderAccountOperation.getStockQuantity());
								}
							}else{
								stockByParticipationRole = stockByParticipationRole.add(holderAccountOperation.getStockQuantity());
							}
						}else{
							//Para el caso que el encargado este agregando sus cuentas, no deberia entrar este metodo
							if(holderAccountOperation.getHolderAccount()==null && 
									!holderAccountOperation.getInchargeFundsParticipant().equals(holdAccOperation.getInchargeFundsParticipant())){
								stockByParticipationRole = stockByParticipationRole.add(holderAccountOperation.getStockQuantity());
							}
						}
					}
				}
				BigDecimal stockQuantity = getStockQuantityFromOperation(otcOperation, swapOperation, role);//Obtenemos el stocQuantity de la operacion, Valida el caso de la permuta
				if (stockQuantity != null && holdAccOperation.getStockQuantity() != null) {
					if(stockQuantity.compareTo(stockByParticipationRole.add(holdAccOperation.getStockQuantity()))!=BigDecimal.ONE.negate().intValue()){
						return Boolean.TRUE;
					}
				}				
			}			
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Have stock pend like in charge by role.
	 * metodo para saber si existe stock pendiente para los participantes encargados
	 * @param otcOperation the otc operation
	 * @param holdAccOperation the hold acc operation
	 * @param role the part role
	 * @return the boolean
	 */
	public static Boolean haveStockPendLikeInChargeByRole(MechanismOperation otcOperation, HolderAccountOperation holdAccOperation, Integer role){
		List<HolderAccountOperation> holdAccOperationList = null;
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return Boolean.FALSE;
		}else if(otcOperation.getHolderAccountOperations()!=null){
			holdAccOperationList = otcOperation.getHolderAccountOperations();
		}		
		if(holdAccOperationList!=null){//Si es null no existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList)){
				BigDecimal stockByParticipationRole = BigDecimal.ZERO;//Variable para almacenar el monto asignado de cuentas, por participacion (COMPRADOR,VENDEDOR) 
				for(HolderAccountOperation holderAccountOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					//Verificamos Si Coincide con el Rol de la Participacion
					if(holderAccountOperation.getRole().equals(role)){
						//Para el caso que el encargado este agregando sus cuentas, no deberia entrar este metodo
						if(holderAccountOperation.getHolderAccount()!=null && 
								holderAccountOperation.getInchargeFundsParticipant().equals(holdAccOperation.getInchargeFundsParticipant())){
							stockByParticipationRole = stockByParticipationRole.add(holderAccountOperation.getStockQuantity());
						}
					}
				}
				if(otcOperation.getStockQuantity().compareTo(stockByParticipationRole.add(holdAccOperation.getStockQuantity()))!=BigDecimal.ONE.negate().intValue()){
					return Boolean.TRUE;
				}
			}			
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the hold acc operations by negotiatior.
	 *
	 * @param otcOperation the otc operation
	 * @param role the participation role
	 * @return the hold acc operations by negotiatior
	 */
	public static List<HolderAccountOperation> getHoldAccOperationsByNegotiatior(MechanismOperation otcOperation, Integer role){
		List<HolderAccountOperation> holdAccOperationsListOnPart = new ArrayList<HolderAccountOperation>();
		List<HolderAccountOperation> holdAccOperationList = null;
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return null;
		}else if(otcOperation.getHolderAccountOperations()!=null){
			holdAccOperationList = otcOperation.getHolderAccountOperations();
		}	
		if(holdAccOperationList!=null){//Si es null no existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList)){
				for(HolderAccountOperation holdAccOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					if(holdAccOperation.getRefAccountOperation()==null){
						//Verificamos Si Coincide con el Rol de la Participacion
						if(holdAccOperation.getRole().equals(role)){
							if(NegotiationUtils.mechanismOperationIsSwap(otcOperation) && holdAccOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
								continue;
							}
							holdAccOperationsListOnPart.add(holdAccOperation);
						}
					}
				}
				return holdAccOperationsListOnPart;
			}			
		}
		return null;
	}
	
	/**
	 * Gets the account operations.
	 *
	 * @param otcOperation the otc operation
	 * @param role the role
	 * @param operationPart the operation part
	 * @return the account operations
	 */
	public static List<HolderAccountOperation> getAccountOperations(MechanismOperation otcOperation, Integer role, Integer operationPart){
		List<HolderAccountOperation> holdAccOperationsListOnPart = new ArrayList<HolderAccountOperation>();
		List<HolderAccountOperation> holdAccOperationList = null;
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return null;
		}else if(otcOperation.getHolderAccountOperations()!=null){
			holdAccOperationList = otcOperation.getHolderAccountOperations();
		}		
		if(holdAccOperationList!=null){
			if(Hibernate.isInitialized(holdAccOperationList)){
				for(HolderAccountOperation accountOperation: holdAccOperationList){
					/**VERIFICAMOS EL ROL Y EL OPERATION_PART*/
					if(accountOperation.getRole().equals(role) && operationPart.equals(accountOperation.getOperationPart())){
						if(NegotiationUtils.mechanismOperationIsSwap(otcOperation) && accountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
							continue;
						}
						holdAccOperationsListOnPart.add(accountOperation);
					}
				}
				return holdAccOperationsListOnPart;
			}			
		}
		return null;
	}
	
	/**
	 * Gets the accounts by role.
	 *
	 * @param operation the operation
	 * @param operationPart the operation part
	 * @return the accounts by role
	 */
	public static List<HolderAccountOperation> getAccountsByRole(MechanismOperation operation, Integer operationPart){
		List<HolderAccountOperation> accountOperationList = new ArrayList<HolderAccountOperation>(); 
		for(HolderAccountOperation accountOperation: operation.getHolderAccountOperations()){
			if(accountOperation.getOperationPart().equals(operationPart)){
				accountOperationList.add(accountOperation);
			}
		}
		return accountOperationList;
	}
	
	/**
	 * Gets the account operations by part.
	 *
	 * @param accountOperations the account operations
	 * @return the account operations by part
	 */
	public static List<HolderAccountOperation> getAccountOperationsByPart(List<HolderAccountOperation> accountOperations){
		List<HolderAccountOperation> accountOperationsByPart = new ArrayList<HolderAccountOperation>();
		for(HolderAccountOperation accOperation: accountOperations){
			if(accOperation.getRefAccountOperation()==null){
				accountOperationsByPart.add(accOperation);
			}
		}
		return accountOperationsByPart;
	}
	
	/**
	 * Gets the hold acc operations by part buy in charge.
	 * metodo para obtener las cuentas encargadas por participante
	 * @param otcOperation the otc operation
	 * @param participant the participant
	 * @return the hold acc operations by part buy in charge
	 */
	public static List<HolderAccountOperation> getHoldAccOperationsByPartBuyInCharge(MechanismOperation otcOperation, Participant participant){
		List<HolderAccountOperation> holdAccOperationsListOnPart = new ArrayList<HolderAccountOperation>();
		if(otcOperation==null || participant==null){//Si es Null, OTC operation no existe
			return null;
		}
		List<HolderAccountOperation> holdAccOperationList = otcOperation.getHolderAccountOperations();
		if(holdAccOperationList!=null){//Si es null no existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList)){
				for(HolderAccountOperation holdAccOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					if(holdAccOperation.getRefAccountOperation()==null){
						if(holdAccOperation.getIndIncharge().equals(BooleanType.YES.getCode())){//Verificamos si la cuenta tiene en cargo
							//Verificamos Si Coincide con el participante recibido
							Participant participantInCharge = holdAccOperation.getInchargeFundsParticipant();
							if(holdAccOperation.getRole().equals(ParticipantRoleType.BUY.getCode()) && participantInCharge.getIdParticipantPk().equals(participant.getIdParticipantPk())){
								if(holdAccOperation.getHolderAccount()!=null){//Filtramos las cuentas que ya fueron asignadas
									holdAccOperationsListOnPart.add(holdAccOperation);
								}
							}
						}
					}
				}
				return holdAccOperationsListOnPart;
			}			
		}
		return null;
	}
	
	/**
	 * Gets the hold acc operations by part sell in charge.
	 * metodo para retornar las cuentas encargadas por participante
	 * @param otcOperation the otc operation
	 * @param participant the participant
	 * @return the hold acc operations by part sell in charge
	 */
	public static List<HolderAccountOperation> getHoldAccOperationsByPartSellInCharge(MechanismOperation otcOperation, Participant participant){
		List<HolderAccountOperation> holdAccOperationsListOnPart = new ArrayList<HolderAccountOperation>();
		if(otcOperation==null || participant==null){//Si es Null, OTC operation no existe
			return null;
		}
		List<HolderAccountOperation> holdAccOperationList = otcOperation.getHolderAccountOperations();
		if(holdAccOperationList!=null){//Si es null no existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList)){
				for(HolderAccountOperation holdAccOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					if(holdAccOperation.getRefAccountOperation()==null){
						if(holdAccOperation.getIndIncharge().equals(BooleanType.YES.getCode())){//Verificamos si la cuenta tiene en cargo
							//Verificamos Si Coincide con el participante recibido
							Participant participantInCharge = holdAccOperation.getInchargeFundsParticipant();
							if(holdAccOperation.getRole().equals(ParticipantRoleType.SELL.getCode()) && participantInCharge.getIdParticipantPk().equals(participant.getIdParticipantPk())){
								if(holdAccOperation.getHolderAccount()!=null){//Filtramos las cuentas que ya fueron asignadas
									holdAccOperationsListOnPart.add(holdAccOperation);
								}
							}
						}
					}
				}
				return holdAccOperationsListOnPart;
			}			
		}
		return null;
	}
	/**
	 * Otc operation is crusade.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean otcOperationIsCrusade(MechanismOperation otcOperation){
		if(otcOperation==null || otcOperation.getOriginRequest()==null){
			return Boolean.FALSE;
		}
		OtcOperationOriginRequestType otcOperationOrigin = OtcOperationOriginRequestType.lookup.get(otcOperation.getOriginRequest());
		if(otcOperationOrigin==null){
			return Boolean.FALSE;
		}
		return otcOperationOrigin.equals(OtcOperationOriginRequestType.CRUSADE);
	}
	/**
	 * Otc operation is crusade.
	 *
	 * @param otcOperation the otc operation
	 * @return the boolean
	 */
	public static Boolean otcOperationIsSell(MechanismOperation otcOperation){
		return !otcOperationIsCrusade(otcOperation);
	}
	/**
	 * Participant is seller.
	 * metodo para validar si participante es el que vende
	 * @param mechanismOperation the mechanism operation
	 * @param participant the participant
	 * @return the boolean
	 */
	public static Boolean participantIsSeller(MechanismOperation mechanismOperation, Participant participant){
		if(mechanismOperation==null || mechanismOperation.getSellerParticipant()==null || participant==null){
			return Boolean.FALSE;
		}
		if(mechanismOperation.getSellerParticipant().getIdParticipantPk().equals(participant.getIdParticipantPk())){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	/**
	 * Participant is buyer.
	 * metodo para validar si participante es el que compra
	 * @param mechanismOperation the mechanism operation
	 * @param participant the participant
	 * @return the boolean
	 */
	public static Boolean participantIsBuyer(MechanismOperation mechanismOperation, Participant participant){
		if(mechanismOperation==null || mechanismOperation.getBuyerParticipant()==null || participant==null){
			return Boolean.FALSE;
		}
		if(mechanismOperation.getBuyerParticipant().getIdParticipantPk().equals(participant.getIdParticipantPk())){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	/**
	 * Participant is in charge by role.
	 * metodo para validar si es el que esta en cargo por Participacion(VENTA,COMPRA)
	 * @param mechanismOperation the mechanism operation
	 * @param participant the participant
	 * @param role the participantion role
	 * @return the boolean
	 */
	public static Boolean participantIsInChargeByRole(MechanismOperation mechanismOperation, Participant participant, Integer role){
		if(mechanismOperation==null || mechanismOperation.getHolderAccountOperations()==null || participant==null || role==null){
			return Boolean.FALSE;
		}
		for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
			if(holderAccountOperation.getIndIncharge().equals(BooleanType.YES.getCode())){//Preguntamos si es que tiene Encargo
				if(holderAccountOperation.getRole().equals(role)){//Validamos que coincide con el tipo de participacion ingresada
					Participant participantInCharge = holderAccountOperation.getInchargeFundsParticipant();//Puede ser el de fondos o valores, en cevaldom sera el mismo 
					if(participantInCharge!=null){//Validamos si es !=null y si Esta inizializado
						if(Hibernate.isInitialized(participantInCharge)){
							return participantInCharge.equals(participant);
						}						
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	/**
	 * Participant is in charge.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param participant the participant
	 * @return the boolean
	 */
	public static Boolean participantIsInCharge(MechanismOperation mechanismOperation, Participant participant){
		if(mechanismOperation==null || mechanismOperation.getHolderAccountOperations()==null || participant==null ){
			return Boolean.FALSE;
		}
		for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
			if(holderAccountOperation.getIndIncharge().equals(BooleanType.YES.getCode())){//Preguntamos si es que tiene Encargo
				Participant participantInCharge = holderAccountOperation.getInchargeFundsParticipant();//Puede ser el de fondos o valores, en cevaldom sera el mismo 
				if(participantInCharge!=null){//Validamos si es !=null y si Esta inizializado
					if(Hibernate.isInitialized(participantInCharge)){
						if(participantInCharge.equals(participant)){
							return Boolean.TRUE;
						}
					}					
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Holder account exist operation.
	 * metodo para saber si la cuenta ya fue ingresada en la operacion de negociacion
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param holderAccount the holder account
	 * @return the boolean
	 */
	public static Boolean holderAccountExistOperation(MechanismOperation mechanismOperation, HolderAccount holderAccount){
		if(mechanismOperation==null || mechanismOperation.getHolderAccountOperations()==null){
			return Boolean.FALSE;
		}
		if(Hibernate.isInitialized(mechanismOperation.getHolderAccountOperations())){
			for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
				if(holderAccountOperation.getHolderAccount()!=null && holderAccountOperation.getHolderAccount().getIdHolderAccountPk()!=null){
					if(holderAccountOperation.getHolderAccount().getIdHolderAccountPk().equals(holderAccount.getIdHolderAccountPk())){
						return Boolean.TRUE;
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	/**
	 * Holder exist operation.
	 * metodo para validar si titular ya fue ingresado en la operacion
	 * @param mechanismOperation the mechanism operation
	 * @param holderAccount the holder account
	 * @return the boolean
	 */
	public static Boolean holderExistOperation(MechanismOperation mechanismOperation, HolderAccount holderAccount){
		if(mechanismOperation==null || mechanismOperation.getHolderAccountOperations()==null){
			return Boolean.FALSE;
		}
		if(Hibernate.isInitialized(mechanismOperation.getHolderAccountOperations())){
			for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
				if(holderAccountOperation.getHolderAccount()!=null){
					for(HolderAccountDetail holderDetail: holderAccountOperation.getHolderAccount().getHolderAccountDetails()){
						Holder holderAdded = holderDetail.getHolder();
						for(HolderAccountDetail holderDetailParam: holderAccount.getHolderAccountDetails()){
							Holder holder = holderDetailParam.getHolder();
							if(holderAdded.getIdHolderPk().equals(holder.getIdHolderPk())){
								return Boolean.TRUE;
							}
						}
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	/**
	 * Gets the participants to buy.
	 *
	 * @param otcOperation the otc operation
	 * @param participantListParam the participant list param
	 * @param viewOperationsType the view operations type
	 * @return the participants to buy
	 */
	public static List<Participant> getParticipantsToBuy(MechanismOperation otcOperation, List<Participant> participantListParam, ViewOperationsType viewOperationsType){
		List<Participant> participantLists;
		if(otcOperation==null || participantListParam==null){
			return null;
		}
		if(viewOperationsType.equals(ViewOperationsType.REGISTER)){//Si nos encontramo en el registro, se necesita hacer esta validacion
			if(otcOperationIsSell(otcOperation)){//Si la operacion es venta
				Long participantSeller = otcOperation.getSellerParticipant().getIdParticipantPk();//Obtenemos el id del participante vendedor
				if(participantSeller!=null){//Si existe participante vendedor
					participantLists = new ArrayList<Participant>();
					participantLists.addAll(participantListParam);
					participantLists.remove(new Participant(participantSeller));//El participante vendedor no puede ser un comprador, En una VENTA
					return getParticipantOrderByMnemonic(participantLists);
				}
			}else{
				
			}
		}else{
			List<Participant> participantList = new ArrayList<Participant>();
			participantList.add(otcOperation.getBuyerParticipant());
			return participantList;
		}
		return getParticipantOrderByMnemonic(participantListParam);
	}
	
	/**
	 * Gets the efective tasa.
	 * metodo para calcular la tasa efectiva,
	 * Fórmula TE = ((MP-MC)/MC) x 100
	 * TE: Tasa Efectiva (efectiveTasa)
	 * MP: Monto Plazo (termAmount)
	 * MC: Monto Contado (cashAmount)
	 * @param termAmount the term amount
	 * @param cashAmount the cash amount
	 * @return the efective tasa
	 */
	public static BigDecimal getEfectiveTasa(BigDecimal termAmount, BigDecimal cashAmount){
		if(termAmount==null || cashAmount==null){
			return BigDecimal.ZERO;
		}
		BigDecimal efectiveTasa = ((termAmount.subtract(cashAmount)).divide(cashAmount,20,RoundingMode.CEILING)).multiply(new BigDecimal(100));
		return efectiveTasa;
	}
	
	/**
	 * Evaluate participant role by origin.
	 * Metodo me permite saber el tipo de participacion en cuenta negociadora
	 *
	 * @param operation the mechanism operation otc session
	 * @param swapOperation the swap operation
	 * @param userInfo the user info
	 * @param viewOperationType the view operation type
	 * @return the integer
	 */
	public Integer getParticipantRoleByOrigin(MechanismOperation operation, SwapOperation swapOperation, UserInfo userInfo, ViewOperationsType viewOperationType){
		Integer participationRole = null;
		if(operation.getMechanisnModality().getId().getIdNegotiationModalityPk()==null || operation.getOriginRequest()==null ||
		   operation.getCashAmount()==null){
			return null;
		}
		
		
		Long partCode = getParticipantLogout(userInfo).getIdParticipantPk();
		
		Long partBuy  = operation.getBuyerParticipant().getIdParticipantPk();
		Long partSell = operation.getSellerParticipant().getIdParticipantPk();
		if(partCode!=null && !operation.getOriginRequest().equals(OtcOperationOriginRequestType.CRUSADE.getCode())){ 
			if(partBuy!=null && partBuy.equals(partCode)){
				return ParticipantRoleType.BUY.getCode();
			}else if(partSell!=null && partSell.equals(partCode)){
				return ParticipantRoleType.SELL.getCode();
			}
		}
		
		//Por ahora se esta manejando LA VAlidacion como CASH y FOWARD que son las que tienen encargo, La validacion deberia ser SI tiene o NO encargo
		if(NegotiationUtils.mechanismOperationIsCash(operation) || NegotiationUtils.mechanismOperationIsFoward(operation)){//Si la Solicitud OTC es Contado o Foward
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.SALE.getCode())){
					if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
						participationRole = ParticipantRoleType.SELL.getCode();
					}else{
						participationRole = ParticipantRoleType.BUY.getCode();
					}
				}else if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.CRUSADE.getCode())){
					if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
						participationRole = ParticipantRoleType.SELL.getCode();
					}else{
						participationRole = ParticipantRoleType.BUY.getCode();
					}
				}
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				Participant participantLogout = getParticipantLogout(userInfo);//Obtenemos el participante de la session
				
				if(OtcOperationUtils.participantIsSeller(operation, participantLogout)){//Si el participante es el vendedor
					if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.BUY.getCode()) && viewOperationType.equals(ViewOperationsType.CONFIRM)){
						//Pueda que sea encargado de la compra
						participationRole = ParticipantRoleType.BUY.getCode();
					}else{
						if(OtcOperationUtils.otcOperationIsSell(operation)){//Si la operacion es venta
							participationRole = ParticipantRoleType.SELL.getCode();
						}else if(OtcOperationUtils.otcOperationIsCrusade(operation)){//Si la operacion es por la compra
							if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
								participationRole = ParticipantRoleType.SELL.getCode();
							}else{
								participationRole = ParticipantRoleType.BUY.getCode();
							}
						}
					}
				}else if(OtcOperationUtils.participantIsBuyer(operation, participantLogout)){//Si el participante es el Comprador
					if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.SELL.getCode())&& viewOperationType.equals(ViewOperationsType.APPROVE)){
						//Si esta en la Aprobacion y es el encargado de la venta
						participationRole = ParticipantRoleType.SELL.getCode();
					}else{
						if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.SALE.getCode())){
							if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
								participationRole = ParticipantRoleType.SELL.getCode();
							}else{
								participationRole = ParticipantRoleType.BUY.getCode();
							}
						}else if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.CRUSADE.getCode())){
							if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
								participationRole = ParticipantRoleType.SELL.getCode();
							}else{
								participationRole = ParticipantRoleType.BUY.getCode();
							}
						}
					}
				}else if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.BUY.getCode()) && viewOperationType.equals(ViewOperationsType.REVIEW)){
					//Si el participante es el emcargado de compra y si esta en Revision
					participationRole = ParticipantRoleType.BUY.getCode();
				}else if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.SELL.getCode()) && viewOperationType.equals(ViewOperationsType.APPROVE)){
					//Si el participante es el emcargado de venta y si esta en Aprobacion
					participationRole = ParticipantRoleType.SELL.getCode();
				}else{
					if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.SALE.getCode())){
						if(OtcOperationUtils.haveStockPendientByRole(operation,swapOperation, ParticipantRoleType.SELL.getCode())){
							participationRole = ParticipantRoleType.SELL.getCode();
						}else{
							participationRole = ParticipantRoleType.BUY.getCode();
						}
					}else if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.CRUSADE.getCode())){
						if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
							participationRole = ParticipantRoleType.SELL.getCode();
						}else{
							participationRole = ParticipantRoleType.BUY.getCode();
						}
					}
				}
			}
		}else if(NegotiationUtils.mechanismOperationIsPrimaryPlacement(operation) || NegotiationUtils.mechanismOperationIsReport(operation)//Si es Colocacion primaria O si es Reporto 
				 || NegotiationUtils.mechanismOperationIsViewReport(operation) || NegotiationUtils.mechanismOperationIsSecundaryReport(operation) //O si Es Reporto a la Vista O si es Repo secundario  
				 || NegotiationUtils.mechanismOperationIsSwap(operation) || NegotiationUtils.mechanismOperationIsSecuritiesLoan(operation)//O si es Permuta O si es Prestamo de valores  
				 || NegotiationUtils.mechanismOperationIsRepurchase(operation) || NegotiationUtils.mechanismOperationIsSplit(operation)){//O si es Recompra O si es Split
			if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){//Si aun queda stock pendiente en venta por agregar
				participationRole = ParticipantRoleType.SELL.getCode();
			}else{//Caso contrario, se necesita validar los demas escenario. P.D.(en estas casuisticas NO HAY ENCARGO)
				if(userInfo.getUserAccountSession().isDepositaryInstitution()){//Si es Cevaldom como usuario
					if(OtcOperationUtils.haveStockPendientByRole(operation, swapOperation, ParticipantRoleType.SELL.getCode())){
						participationRole = ParticipantRoleType.SELL.getCode();
					}else{
						participationRole = ParticipantRoleType.BUY.getCode();
					}
				}else if(userInfo.getUserAccountSession().isParticipantInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()
						|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){//Si el usuario es un participante
					Participant participantLogout = getParticipantLogout(userInfo);
					if(OtcOperationUtils.participantIsSeller(operation, participantLogout)){//Si el participante es vendedor
						if(OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.SELL.getCode()).isEmpty()){//Si la cuenta vendedora no es ingresada
							participationRole = ParticipantRoleType.SELL.getCode();
						}else{
							if(OtcOperationUtils.otcOperationIsCrusade(operation)){//Si la Operacion es Crusada, el mismo es el que compra
								participationRole = ParticipantRoleType.BUY.getCode();
							}else{
								participationRole = ParticipantRoleType.SELL.getCode();
							}
						}
					}else if(OtcOperationUtils.participantIsBuyer(operation, participantLogout)){//Si el participante es Comprador
						if(!OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.SELL.getCode()).isEmpty()){//Si existe la cuenta vendedora
							participationRole = ParticipantRoleType.BUY.getCode();
						}
					}
				}
			}
		}
		return participationRole;
	}
	
	/**
	 * Gets the participant for hold account negotiation.
	 * METODO QUE ME PERMITE OBTENER EL PARTICIPANTE, PARA LA CUENTA NEGOCIADORA, DEPENDIENDO DEL COMPORTAMIENTO DE LA OPERACION OTC
	 *
	 * @param mechanismOperationOtcSession the mechanism operation otc session
	 * @param swapOperation the swap operation
	 * @param holdAccountOperationRequest the hold account operation request
	 * @param userInfo the user info
	 * @return the participant for hold account negotiation
	 */
	public  Participant getPartToHoldAccNegotiation(MechanismOperation mechanismOperationOtcSession, SwapOperation swapOperation, HolderAccountOperation holdAccountOperationRequest, UserInfo userInfo){
		Participant participantSelected = null;
		if(NegotiationUtils.mechanismOperationIsCash(mechanismOperationOtcSession) || NegotiationUtils.mechanismOperationIsFoward(mechanismOperationOtcSession)){//Si es COntado O Foward
			//Si existe Encargo, obtenemos el participante seleccionado al encargo
			if(holdAccountOperationRequest.getIndIncharge().equals(BooleanType.YES.getCode())){//Si tiene encargo
				if(userInfo.getUserAccountSession().isDepositaryInstitution()){
					if(holdAccountOperationRequest.getInchargeFundsParticipant()!=null && holdAccountOperationRequest.getInchargeFundsParticipant().getIdParticipantPk()!=null){
						participantSelected = holdAccountOperationRequest.getInchargeFundsParticipant();
					}
				}else{
					Participant participantLogout = getParticipantLogout(userInfo); 
					//Si el participante logueado es el encargado por el rol de la cuenta negociadora
					if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantLogout,holdAccountOperationRequest.getRole())){
						participantSelected = participantLogout;
					}
				}
			}else{
				//verificamos el tipo de solicitud si es Venta o Compra
				OtcOperationOriginRequestType otcRequestType = OtcOperationOriginRequestType.lookup.get(mechanismOperationOtcSession.getOriginRequest());
				if(otcRequestType==null){
					return null;
				}else if(otcRequestType.equals(OtcOperationOriginRequestType.SALE)){
					if(!OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession, swapOperation, ParticipantRoleType.SELL.getCode())){
						participantSelected = new Participant(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk());//Para venta, Y se esta asignando la parte compradora
					}else{
						participantSelected = new Participant(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());//Para venta, obtenemos el participante que esta vendiendo.
					}
				}else if(otcRequestType.equals(OtcOperationOriginRequestType.PURCHASE)){
					participantSelected = new Participant(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk());//Para compra, obtenemos el participante que esta comprando.
				}else if(otcRequestType.equals(OtcOperationOriginRequestType.CRUSADE)){
					participantSelected = new Participant(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk());//Para crusada, Siempre sera el mismo participante.
				}
			}
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				if(!OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.SELL.getCode()) &&
				   !OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.BUY.getCode())){//Si la compra aun tiene stock pendiente por agregar
					return null;
				}
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){//Si es participante se necesita el manejo de registro de cuentas
				Participant participantLogout = getParticipantLogout(userInfo);//Traemos el participante de la session
				if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantLogout)){//Verificamos si el participante es el vendedor
					if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode())){//Si no tiene encargo
						if(OtcOperationUtils.otcOperationIsSell(mechanismOperationOtcSession) && !OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession, swapOperation, ParticipantRoleType.SELL.getCode())){//Si ya no tiene cantidad para asignar
							if(!OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantLogout, ParticipantRoleType.BUY.getCode())){
								participantSelected = null;
							}
						}
					}else if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.BUY.getCode())){
						participantSelected = participantLogout;
					}else{
						//y si la Operacion es Venta Si ya no tiene cantidad para asignar
						if(OtcOperationUtils.otcOperationIsSell(mechanismOperationOtcSession) && !OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.SELL.getCode())){
							participantSelected = null;
						}
					}
				}else if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantLogout)){//Verificamos si el participante es el vendedor
					if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.BUY.getCode())){//Si no tiene encargo
						//Verificamos si el comprador fue encargado en la venta
						if(!OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantLogout, ParticipantRoleType.BUY.getCode())){
							participantSelected = null;
						}
						//Si ya no tiene cantidad para asignar
						if(OtcOperationUtils.otcOperationIsSell(mechanismOperationOtcSession) && !OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.BUY.getCode())){
							participantSelected = null;
						}
					}else{
						//y si la Operacion es Venta Si ya no tiene cantidad para asignar
						if(OtcOperationUtils.otcOperationIsSell(mechanismOperationOtcSession) && !OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.BUY.getCode())){
							participantSelected = null;
						}
					}
				}
			}
		}else if(NegotiationUtils.mechanismOperationIsPrimaryPlacement(mechanismOperationOtcSession)){//Si es colocacion Primaria
			if(OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode()).isEmpty()){
				participantSelected = new Participant(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());
			}else{
				if(userInfo.getUserAccountSession().isDepositaryInstitution()){
					if(OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.BUY.getCode())){//Si la compra aun tiene stock pendiente por agregar
						if(OtcOperationUtils.otcOperationIsCrusade(mechanismOperationOtcSession)){//Si la operacion es cruzada, el vendedor y comprador es el mismo
							participantSelected = new Participant(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());
						}else{//Caso contrario, retornamos el participante comprador
							participantSelected = new Participant(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk());
						}
					}else{//Ya no hay suficiente stock por agregar
						participantSelected = null;
					}
				}else if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
						||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					Participant participantLogout =getParticipantLogout(userInfo);
					if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantLogout)){//Si el participante logueado es el vendedor
						if(OtcOperationUtils.holderAccountNegotiationIsSell(holdAccountOperationRequest)){//Validamos si el rol esta en Venta
							participantSelected = null;
						}else{
							if(OtcOperationUtils.otcOperationIsCrusade(mechanismOperationOtcSession)){//Si la operacion es cruzada, el vendedor y comprador es el mismo
								participantSelected = participantLogout;
							}else{//Caso contrario, el vendedor no puede usar el help para compra
								participantSelected = null;
							}
						}
					}else if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantLogout)){//Si el participante lgueado es el comprador
						if(OtcOperationUtils.holderAccountNegotiationIsBuy(holdAccountOperationRequest)){//Validamos si el rol esta en Venta
							participantSelected = participantLogout;
						}
					}
				}
			}
		}else if(NegotiationUtils.mechanismOperationIsReport(mechanismOperationOtcSession) || NegotiationUtils.mechanismOperationIsViewReport(mechanismOperationOtcSession)//Si es Repo, Repo a la vista 
					|| NegotiationUtils.mechanismOperationIsSecundaryReport(mechanismOperationOtcSession) || NegotiationUtils.mechanismOperationIsSwap(mechanismOperationOtcSession)//O Repo secundario O si es permuta
					|| NegotiationUtils.mechanismOperationIsSecuritiesLoan(mechanismOperationOtcSession) || NegotiationUtils.mechanismOperationIsRepurchase(mechanismOperationOtcSession)
					|| NegotiationUtils.mechanismOperationIsSplit(mechanismOperationOtcSession)){//O si es Split
			if(OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.SELL.getCode())){
				participantSelected = new Participant(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());
			}else if(OtcOperationUtils.otcOperationIsCrusade(mechanismOperationOtcSession)){
				participantSelected = new Participant(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());
			}else if(OtcOperationUtils.otcOperationIsSell(mechanismOperationOtcSession)){
				Participant participantLogout = getParticipantLogout(userInfo);
				if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
						||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantLogout)){
						participantSelected = null;
					}else if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantLogout)){
						if(OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession,swapOperation, ParticipantRoleType.BUY.getCode())){
							participantSelected = participantLogout;
						}else{
							participantSelected = null;
						}
					}
				}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){//Si es participate cevaldom
					participantSelected = new Participant(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk());//Se asigna como participante al participanbte comprador
				}
			}
		}
		return participantSelected;
	}
	
	/**
	 * Gets the participant logout.
	 *
	 * @param userInfo the user info
	 * @return the participant logout
	 */
	public Participant getParticipantLogout ( UserInfo userInfo){
		
		Participant participantLogout= new Participant();
			if (userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
				participantLogout = new Participant(userInfo.getUserAccountSession().getParticipantCode());//Obtenemos el participante logueado
			else if(userInfo.getUserAccountSession().isIssuerDpfInstitucion())
				participantLogout = new Participant(userInfo.getUserAccountSession().getPartIssuerCode());//Obtenemos el participante logueado
		return participantLogout ;
	}
	
	/**
	 * issue 747: metodo para retornar el vendedor y comprador al momento de confirmar la operacion OTC (esto es para los usuarios NO EDV)
	 * <br/>
	 * cuando es usuario EDV no da error
	 * <br/>
	 * cuando es usuario NO edv da error, por eso se ha implementado este metodo
	 * <br/>
	 * este medodo esta basado en en el metodo  getAccountOperationValidated de esta misma clase
	 * <br/>
	 * @param operation
	 * @param swapOperation
	 * @param viewOperation
	 * @return
	 */
	public List<HolderAccountOperation> getAccountOperationValidatedToConfirmedOTC(MechanismOperation operation, SwapOperation swapOperation, ViewOperationsType viewOperation){
		List<HolderAccountOperation> holdAccOperationList = new ArrayList<HolderAccountOperation>();
		if(operation.getHolderAccountOperations()==null || operation.getHolderAccountOperations().isEmpty()){
			return holdAccOperationList;
		}
		
		List<HolderAccountOperation> accountOperations = operation.getHolderAccountOperations();
		holdAccOperationList.addAll(getAccountOperationsByPart(accountOperations));//Agregamos todas las cuentas de la operacion
		holdAccOperationList.addAll(OtcSwapOperationUtils.getAllHoldAccOperation(operation, swapOperation));//Para el caso del swap
		
		if(NegotiationUtils.mechanismOperationIsSwap(operation)){//Si es swap, necesitamos el codigo isin como un campo mas
			holdAccOperationList = getHoldAccOperValidToSwap(holdAccOperationList, operation, swapOperation);
		}
		return holdAccOperationList;
	}
	
	/**
	 * Gets the holder account negotiation validated.
	 * metodo que retorna las cuentas negociadoras validadadas por el tipo de usuario para ser visualizadas en la grilla
	 *
	 * @param operation the mechanism operation otc session
	 * @param swapOperation the swap operation
	 * @param userInfo the user info
	 * @param viewOperation the view operations type
	 * @return the holder account negotiation validated
	 */
	
	public  List<HolderAccountOperation> getAccountOperationValidated(MechanismOperation operation, SwapOperation swapOperation, UserInfo userInfo, ViewOperationsType viewOperation){
		List<HolderAccountOperation> holdAccOperationList = new ArrayList<HolderAccountOperation>();
		if(operation == null || operation.getHolderAccountOperations()==null || operation.getHolderAccountOperations().isEmpty()){
			return holdAccOperationList;
		}
		
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			List<HolderAccountOperation> accountOperations = operation.getHolderAccountOperations();
			holdAccOperationList.addAll(getAccountOperationsByPart(accountOperations));//Agregamos todas las cuentas de la operacion
			holdAccOperationList.addAll(OtcSwapOperationUtils.getAllHoldAccOperation(operation, swapOperation));//Para el caso del swap
		}else if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			Participant participantLogout = getParticipantLogout(userInfo);
			if(NegotiationUtils.mechanismOperationIsCash(operation) || NegotiationUtils.mechanismOperationIsFoward(operation)){//Si la operacion es contado o Foward
				if(OtcOperationUtils.otcOperationIsCrusade(operation)){//Verificamos si la operacion es cruzada
					if(OtcOperationUtils.participantIsSeller(operation, participantLogout)){//Verificamos si es vendedor o comprador, es cruzada
						if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.SELL.getCode()) && OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){//Si hay encargado en venta O compra
							holdAccOperationList = OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(operation, ParticipantRoleType.SELL.getCode());//Obtenemos las cuentas encargadas de la venta
							holdAccOperationList.addAll(OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(operation, ParticipantRoleType.BUY.getCode()));//Obtenemos las cuentas encargadas de la venta
						}else if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.SELL.getCode())){//Si tiene encargo en solo venta
							holdAccOperationList = OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(operation, ParticipantRoleType.SELL.getCode());//Traemos las cuentas vendedoras del encargado
							holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.BUY.getCode()));//Obtenemos las propias cuentas del vendedor como venta
						}else if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){//Si tiene encargo en la compra
							holdAccOperationList = OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(operation, ParticipantRoleType.BUY.getCode());//Traemos las cuentas compradoras del encargado
							holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.SELL.getCode()));//Obtenemos las propias cuentas del vendedor como compra
						}else{//Si es cruzada y no tiene ningun encargo
							holdAccOperationList = operation.getHolderAccountOperations();
						}
					}else if(OtcOperationUtils.participantIsInCharge(operation, participantLogout)){//Si es algun participante encargado
						if(OtcOperationUtils.participantIsInChargeByRole(operation,participantLogout, ParticipantRoleType.SELL.getCode()) && OtcOperationUtils.participantIsInChargeByRole(operation,participantLogout, ParticipantRoleType.BUY.getCode())){
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByPartSellInCharge(operation, participantLogout);
							holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantLogout));
						}else if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.SELL.getCode())){//Si es encargado de la venta
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByPartSellInCharge(operation, participantLogout);
						}else if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.BUY.getCode())){//Si es encargado de la compra
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantLogout);
						}
					}
				}else{
					if(OtcOperationUtils.participantIsSeller(operation, participantLogout)){//Verificamos si es vendedor
						if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.SELL.getCode())){//Si tiene encargo en la venta
							holdAccOperationList = OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(operation, ParticipantRoleType.SELL.getCode());//Obtenemos las cuentas encargadas de la venta
							if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.BUY.getCode())){
								holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantLogout));
							}
						}else if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){//Si tiene encargo en la compra
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.SELL.getCode());
							if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.BUY.getCode())){
								holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantLogout));
							}
						}else{
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.SELL.getCode());
						}
					}else if(OtcOperationUtils.participantIsBuyer(operation, participantLogout)){//Verificamos si es Comprador
						if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){//Si tiene encargo en la compra
							holdAccOperationList = OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(operation, ParticipantRoleType.BUY.getCode());//Obtenemos las cuentas encargadas de la venta
							if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.SELL.getCode())){
								holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByPartSellInCharge(operation, participantLogout));
							}
						}else if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.SELL.getCode())){//Si tiene encargo en la venta
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.BUY.getCode());
							if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.SELL.getCode())){
								holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByPartSellInCharge(operation, participantLogout));
							}
						}else{
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation, ParticipantRoleType.BUY.getCode());
						}
					}else if(OtcOperationUtils.participantIsInCharge(operation, participantLogout)){//Si es algun participante encargado
						if(OtcOperationUtils.participantIsInChargeByRole(operation,participantLogout, ParticipantRoleType.SELL.getCode()) && OtcOperationUtils.participantIsInChargeByRole(operation,participantLogout, ParticipantRoleType.BUY.getCode())){
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByPartSellInCharge(operation, participantLogout);
							holdAccOperationList.addAll(OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantLogout));
						}else if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.SELL.getCode())){//Si es encargado de la venta
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByPartSellInCharge(operation, participantLogout);
						}else if(OtcOperationUtils.participantIsInChargeByRole(operation, participantLogout, ParticipantRoleType.BUY.getCode())){//Si es encargado de la compra
							holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByPartBuyInCharge(operation, participantLogout);
						}
					}
				}
			}else{//Si la operacion es Contado O Foward O si es Report a la Vista O Repo secundario
				if(OtcOperationUtils.otcOperationIsCrusade(operation)){//Si la operacion es cruzada
					holdAccOperationList.addAll(operation.getHolderAccountOperations());
					holdAccOperationList.addAll(OtcSwapOperationUtils.getAllHoldAccOperation(operation,swapOperation));//Para el caso del swap, traemos todas las cuentas
				}else{
					if(OtcOperationUtils.participantIsBuyer(operation, participantLogout)){//Traemos las cuentas del participante Comprador
						holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation,ParticipantRoleType.BUY.getCode());
						holdAccOperationList.addAll(OtcSwapOperationUtils.getHoldAccOperationByNegotiation(operation, swapOperation, ParticipantRoleType.SELL.getCode()));//Para el caso de swap, esta lista traera cuentas
					}else if(OtcOperationUtils.participantIsSeller(operation, participantLogout)){//Traemos las cuentas del participante Vendedor
						holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(operation,ParticipantRoleType.SELL.getCode());
						holdAccOperationList.addAll(OtcSwapOperationUtils.getHoldAccOperationByNegotiation(operation, swapOperation, ParticipantRoleType.BUY.getCode()));//Para el caso de swap, esta lista traera cuentas
					}
				}
			}
		}
		if(NegotiationUtils.mechanismOperationIsSwap(operation)){//Si es swap, necesitamos el codigo isin como un campo mas
			holdAccOperationList = getHoldAccOperValidToSwap(holdAccOperationList, operation, swapOperation);
		}
		return holdAccOperationList;
	}
	
	/**
	 * Gets the hold acc oper valid to swap.
	 *
	 * @param holdAccOpList the hold acc op list
	 * @param mecOperation the mec operation
	 * @param swapOperation the swap operation
	 * @return the hold acc oper valid to swap
	 */
	public static List<HolderAccountOperation> getHoldAccOperValidToSwap(List<HolderAccountOperation> holdAccOpList, MechanismOperation mecOperation, SwapOperation swapOperation){
		Security security = mecOperation.getSecurities();
		Security securitySwap = swapOperation.getSecurities();
		for(HolderAccountOperation holdAccountOperation: holdAccOpList){
			if(holdAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
				if(holdAccountOperation.getSwapOperation()!=null){
					holdAccountOperation.setIdIsinCode(securitySwap.getIdSecurityCodePk());
				}else{
					holdAccountOperation.setIdIsinCode(security.getIdSecurityCodePk());
				}
			}
		}
		return holdAccOpList;
	}
	/**
	 * Gets the mechanisn modality ind plaze handler.
	 * metodo para saber si la operacion tiene O no Plazo
	 *
	 * @param mechanismModality the mechanism modality
	 * @return the mechanisn modality ind plaze handler
	 */
	public static Boolean isMechanisnModalityTermPlaze(MechanismModality mechanismModality){
		//Obtenemos el Mecanismo con la modalidad
		NegotiationModality negotiationModality = mechanismModality.getNegotiationModality();//Obtenemos la modalidad seleccionada en la pantalla
		if(negotiationModality==null){//Si es null, aun no ha sido seleccionada
			return Boolean.FALSE;
		}else if(negotiationModality.getIndTermSettlement().equals(BooleanType.NO.getCode())){//Modalidad no tiene Liquidacion Plazo
			return Boolean.FALSE;
		}
		return Boolean.TRUE;//Modalidad si tiene liquidacion plazo
	}
	
	/**
	 * Checks if is mechanisn modality with in charge.
	 * metodo para validar si el mecanismo con modalidad tiene O no encargo
	 * @param mechanismModality the mechanism modality
	 * @return the boolean
	 */
	public static Boolean isMechanisnModalityWithInCharge(MechanismModality mechanismModality){
		//Obtenemos el Mecanismo con la modalidad
		if(mechanismModality==null || mechanismModality.getIndIncharge()==null){
			return Boolean.FALSE;
		}
		return mechanismModality.getIndIncharge().equals(BooleanType.YES.getCode());//Modalidad si tiene ENcargo
	}
	
	/**
	 * Gets the indicator hold acc operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param holdAccOperationParam the hold acc operation param
	 * @return the indicator hold acc operation
	 */
	public static Integer getIndicatorHoldAccOperation(MechanismOperation mechanismOperation, HolderAccountOperation holdAccOperationParam){
		HolderAccount holdAccSelected = holdAccOperationParam.getHolderAccount();
		for(int i=0; i<mechanismOperation.getHolderAccountOperations().size(); i++){
			HolderAccountOperation holdAccOperation = mechanismOperation.getHolderAccountOperations().get(i);
			if(holdAccOperationParam.getIndIncharge().equals(BooleanType.YES.getCode()) && holdAccOperation.getHolderAccount()==null){//Si tiene encargo y aun no hay cuentas
				Participant participantInCharge = holdAccOperation.getInchargeFundsParticipant();
				Participant participantSelected = holdAccOperationParam.getInchargeFundsParticipant();
				if(holdAccOperationParam.getRole().equals(holdAccOperation.getRole()) && participantSelected.equals(participantInCharge)){//Verificamos el ROL(compra-venta) y el participante encargado
					return i;
				}
			}else{
				if(holdAccOperation.getHolderAccount().getIdHolderAccountPk().equals(holdAccSelected.getIdHolderAccountPk())){//Verificamos por el Id de la cuenta
					return i;
				}
			}
		}
		return null;
	}
	
	/**
	 * Gets the mechanism modality by instrument.
	 * metodo para retornar las modalidades a partir de un determinado instrumento
	 * @param mecModList the mec mod list
	 * @param instrumentType the instrument type
	 * @return the mechanism modality by instrument
	 */
	public static List<MechanismModality> getMechanismModalityByInstrument(List<MechanismModality> mecModList, InstrumentType instrumentType){
		if(mecModList==null || instrumentType==null){
			return null;
		}
		List<MechanismModality> mecModListByInstrument = new ArrayList<MechanismModality>();
		for(MechanismModality mecMod: mecModList){
			InstrumentType _instrumentType = InstrumentType.get(mecMod.getNegotiationModality().getInstrumentType());//Obtenemos el instrumento de la modalidad
			if(instrumentType.equals(_instrumentType)){
				mecModListByInstrument.add(mecMod);
			}
		}
		return mecModListByInstrument;
	}
	
	/**
	 * Gets the mechanism modality by instrument.
	 * metodo para retornar las modalidades de acuerdo al instrumento seleccionado
	 *
	 * @param mecModList the mec mod list
	 * @param instrumentType the instrument type
	 * @param userInfo the user info
	 * @return the mechanism modality by instrument
	 */
	public  List<MechanismModality> getMechanismModalityByInstrument(List<MechanismModality> mecModList, InstrumentType instrumentType,UserInfo userInfo){
		if(mecModList==null || instrumentType==null){
			return null;
		}
		List<MechanismModality> mecModListByInstrument = new ArrayList<MechanismModality>();
		Boolean userIsParticipant = userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||userInfo.getUserAccountSession().isParticipantInvestorInstitucion();
		Participant participantLogout = getParticipantLogout(userInfo);
		Long idParticipantPk = participantLogout.getIdParticipantPk();
		_jump:
		for(MechanismModality mecMod: mecModList){
			InstrumentType _instrumentType = InstrumentType.get(mecMod.getNegotiationModality().getInstrumentType());//Obtenemos el instrumento de la modalidad
			if(userIsParticipant && idParticipantPk!=null){
				for(ParticipantMechanism participantMechanism: mecMod.getParticipantMechanisms()){
					if(participantMechanism.getParticipant().getIdParticipantPk().equals(idParticipantPk) && instrumentType.equals(_instrumentType)){
						mecModListByInstrument.add(mecMod);
						continue _jump;
					}
				}
			}else{
				if(instrumentType.equals(_instrumentType)){
					mecModListByInstrument.add(mecMod);
					continue _jump;
				}
			}
		}
		return mecModListByInstrument;
	}
	
	/**
	 * Gets the hold acc operation from operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param participationRole the participation role
	 * @return the hold acc operation from operation
	 */
	public static List<HolderAccountOperation> getAccountOperationFromOperation(MechanismOperation mechanismOperation, ParticipantRoleType participationRole){
		if(mechanismOperation==null || mechanismOperation.getHolderAccountOperations()==null || !Hibernate.isInitialized(mechanismOperation.getHolderAccountOperations())){
			return null;
		}
		List<HolderAccountOperation> holdAccOperationList = new ArrayList<HolderAccountOperation>();
		for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
			if(holderAccountOperation.getRole().equals(participationRole.getCode())){
				HolderAccountOperation _holderAccountOperation = null;//Objeto que sera puesto en la lista, teniendo como referencia el objeto verdadero
				try {
					_holderAccountOperation = holderAccountOperation.clone();//Clonamos el objeto del repo original
					_holderAccountOperation.setRefAccountOperation(holderAccountOperation.clone());//Asignamos el verdadero objeto como referencia
					_holderAccountOperation.setStockQuantity(null);//Seteamos la cantidad, sera ingresada desde el datatable
					_holderAccountOperation.setIsSelected(Boolean.FALSE);
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
				holdAccOperationList.add(_holderAccountOperation);
			}
		}
		return holdAccOperationList;
	}
	
	/**
	 * Gets the hol acc operation by part in charged.
	 * metodo para obtener la cuenta de un participante encargado con sus respectivos montos, Sin poder ver las cuentas inversionistas de este
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param role the participant role
	 * @return the hol acc operation by part in charged
	 */
	public static List<HolderAccountOperation> getHolAccOperationInChargedForNegotiatior(MechanismOperation mechanismOperation, Integer role){
		if(mechanismOperation==null || mechanismOperation.getHolderAccountOperations()==null || mechanismOperation.getHolderAccountOperations().isEmpty() || role==null){
			return null;
		}
		List<HolderAccountOperation> holdAccOperationList = new ArrayList<HolderAccountOperation>();
		for(HolderAccountOperation holdAccInCharged: OtcOperationUtils.getParticipantsInCharged(mechanismOperation)){
			if(holdAccInCharged.getIndIncharge().equals(BooleanType.YES.getCode())){// && holdAccInCharged.getRole().equals(participantRole.getCode())){
				Participant participantInCharged = holdAccInCharged.getInchargeFundsParticipant();
				HolderAccountOperation holderAccountOperationReturn = new HolderAccountOperation();
				holderAccountOperationReturn.setMechanismOperation(mechanismOperation);
				holderAccountOperationReturn.setRole(role);
				holderAccountOperationReturn.setInchargeFundsParticipant(participantInCharged);
				holderAccountOperationReturn.setIndIncharge(BooleanType.YES.getCode());
				BigDecimal quantityOfParticipant = BigDecimal.ZERO;
				BigDecimal quantityCashOfParticipant = BigDecimal.ZERO;
				for(HolderAccountOperation holdAccountOperation: mechanismOperation.getHolderAccountOperations()){
					if(participantInCharged.getIdParticipantPk().equals(holdAccountOperation.getInchargeStockParticipant().getIdParticipantPk()) && 
					   role.equals(holdAccountOperation.getRole())){
						quantityOfParticipant = quantityOfParticipant.add(holdAccountOperation.getStockQuantity());
						quantityCashOfParticipant = quantityCashOfParticipant.add(holdAccountOperation.getCashAmount());
					}
				}
				if(!quantityOfParticipant.equals(BigDecimal.ZERO) && !quantityCashOfParticipant.equals(BigDecimal.ZERO)){
					holderAccountOperationReturn.setStockQuantity(quantityOfParticipant);
					holderAccountOperationReturn.setCashAmount(quantityCashOfParticipant);
					holdAccOperationList.add(holderAccountOperationReturn);
				}
			}
		}
		return holdAccOperationList;
	}
	
	/**
	 * Gets the participants in charged.
	 * metodo para obtener las cuentas, pero agrupados por participantes encargados diferentes Y Rol
	 * @param mechanismOperation the mechanism operation
	 * @return the participants in charged
	 */
	public static List<HolderAccountOperation> getParticipantsInCharged(MechanismOperation mechanismOperation){
		if(mechanismOperation==null || mechanismOperation.getHolderAccountOperations()==null || mechanismOperation.getHolderAccountOperations().isEmpty()){
			return null;
		}
		List<HolderAccountOperation> holdAccOperationList = new ArrayList<HolderAccountOperation>();
		List<Participant> participantList = new ArrayList<Participant>();
		_jump:
		for(HolderAccountOperation holdAccInCharged: mechanismOperation.getHolderAccountOperations()){
			if(holdAccInCharged.getIndIncharge().equals(BooleanType.YES.getCode())){
				Participant participant = holdAccInCharged.getInchargeFundsParticipant();
				if(participantList.isEmpty()){
					holdAccOperationList.add(holdAccInCharged);
					participantList.add(participant);
				}else{
					Boolean existOnParticipant = Boolean.FALSE;
					for(Participant participantOnList: participantList){
						if(participantOnList.getIdParticipantPk().equals(participant.getIdParticipantPk())){
							existOnParticipant = Boolean.TRUE;
						}
					}
					if(!existOnParticipant){
						holdAccOperationList.add(holdAccInCharged);
						participantList.add(participant);
						continue _jump;
					}
				}
			}
		}
		return holdAccOperationList;
	}
	/**
	 * Have coupon pendient of pay.
	 * metodo que me retorna si existe algun cupon pendiente de pago.
	 * @param programInterestCouponList the program interest coupon list
	 * @return the boolean
	 */
	public static Boolean haveCouponPendientOfPay(List<ProgramInterestCoupon> programInterestCouponList){
		if(programInterestCouponList==null || programInterestCouponList.isEmpty()){
			return Boolean.FALSE;
		}
		for(ProgramInterestCoupon programInterestCoupon: programInterestCouponList){
			if(programInterestCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	/**
	 * Gets the participant in charged by role.
	 * metodo para obtener el participante encargado de una determinada operacion
	 * @param mechanismOperation the mechanism operation
	 * @param role the participant role
	 * @return the participant in charged by role
	 */
	public static Participant getParticipantInChargedByRole(MechanismOperation mechanismOperation, Integer role){
		if(mechanismOperation==null || role==null){
			return null;
		}
		for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
			if(holderAccountOperation.getRole().equals(role)){
				if(holderAccountOperation.getIndIncharge().equals(BooleanType.YES.getCode())){
					return holderAccountOperation.getInchargeFundsParticipant();
				}
			}
		}
		return null;
	}
	
	/**
	 * Have stock pendient in charged by role.
	 * metodo para obtener el stock pendiente del encargo por role
	 *
	 * @param otcOperation the otc operation
	 * @param swapOperation the swap operation
	 * @param role the participant role
	 * @return the boolean
	 */
	public static Boolean haveStockPendientInChargedByRole(MechanismOperation otcOperation, SwapOperation swapOperation, Integer role){
		List<HolderAccountOperation> holdAccOperationList = null;
		if(otcOperation==null){//Si es Null, OTC operation no existe
			return Boolean.FALSE;
		}else if(otcOperation.getHolderAccountOperations()!=null){
			holdAccOperationList = otcOperation.getHolderAccountOperations();//Pueda que tenga encargo, y aun no se hayan ingresado cantidades. Puede ser 0
		}
		if(!OtcOperationUtils.haveInChargeByParticipantRole(otcOperation, role)){//Si la operacion no tiene encargo por el rol recibido
			return Boolean.FALSE;
		}		
		if(holdAccOperationList!=null){//Si es null no existen cuentas negociadoras
			if(Hibernate.isInitialized(holdAccOperationList)){
				BigDecimal stockByParticipationRole = BigDecimal.ZERO;//Variable para almacenar el monto asignado de cuentas, por participacion (COMPRADOR,VENDEDOR) 
				for(HolderAccountOperation holdAccOperation: holdAccOperationList){//Recorremos cuentas negociadoras
					//Verificamos Si Coincide con el Rol de la Participacion Si tiene Encargo Y si la cuenta ya fue ingresada
					if(holdAccOperation.getRole().equals(role) && holdAccOperation.getIndIncharge().equals(BooleanType.YES.getCode()) && holdAccOperation.getHolderAccount()!=null){
						stockByParticipationRole = stockByParticipationRole.add(holdAccOperation.getStockQuantity());
					}
				}
				BigDecimal stockQuantity = getStockQuantityFromOperation(otcOperation, swapOperation, role);//Obtenemos el stockQuantity de la operacion(Soporta tambien permuta)
				if(stockQuantity!=null){//Si es diferente de null
					if(stockByParticipationRole.compareTo(stockQuantity)!=BigDecimal.ZERO.intValue()){
						return Boolean.TRUE;
					}
				}
			}			
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the participant for charge.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param participantSellers the participant sellers
	 * @param role the participat type
	 * @return the participant for charge
	 */
	public static List<Participant> getParticipantForCharge(MechanismOperation mechanismOperation, List<Participant> participantSellers, Integer role){
		if(mechanismOperation==null || participantSellers==null || participantSellers.isEmpty() || role==null){
			return null;
		}
		//Para el caso de Encargo, obtenemos una lista de los participantes 
		List<Participant> participantList = new ArrayList<Participant>();
		//Agregamos los participantes a la lista a retornar
		participantList.addAll(participantSellers);
		if(OtcOperationUtils.otcOperationIsSell(mechanismOperation)){
			Participant participantSelected = null;
			if(role.equals(ParticipantRoleType.SELL.getCode())){
				participantSelected = mechanismOperation.getSellerParticipant();
			}else if(role.equals(ParticipantRoleType.BUY.getCode())){
				participantSelected = mechanismOperation.getBuyerParticipant();
			}
			participantList.remove(participantSelected);
		}else if(OtcOperationUtils.otcOperationIsCrusade(mechanismOperation)){
			Participant participantBuyer = mechanismOperation.getSellerParticipant();//El participante comprador o vendedor siempre sera el mismo
			participantList.remove(participantBuyer);
		}
		return getParticipantOrderByDescrip(participantList);
	}
	
	/**
	 * Gets the incharged disabled.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param swapOperation the swap operation
	 * @param userInfo the user info
	 * @param viewOperationType the view operation type
	 * @return the incharged disabled
	 */
	public static Boolean getInchargedDisabled(MechanismOperation mechanismOperation, SwapOperation swapOperation, UserInfo userInfo, ViewOperationsType viewOperationType){
		if(mechanismOperation.getCashAmount()==null || mechanismOperation.getCashSettlementDays()==null){
			return Boolean.TRUE;
		}
		if(userInfo.getUserAccountSession().isParticipantInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			if(viewOperationType.equals(ViewOperationsType.APPROVE)){
				return Boolean.TRUE;
			}else if(viewOperationType.equals(ViewOperationsType.CONFIRM)){
				return Boolean.TRUE;
			}else if(viewOperationType.equals(ViewOperationsType.CONSULT)){
				return Boolean.TRUE;
			}
		}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			if(!viewOperationType.equals(ViewOperationsType.REGISTER)){
				return Boolean.TRUE;
			}
			if(OtcOperationUtils.haveInCharge(mechanismOperation)){
				if((OtcOperationUtils.haveStockPendientInChargedByRole(mechanismOperation, swapOperation, ParticipantRoleType.SELL.getCode()) && !OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(mechanismOperation, ParticipantRoleType.SELL.getCode()).isEmpty())
				 ||(OtcOperationUtils.haveStockPendientInChargedByRole(mechanismOperation, swapOperation, ParticipantRoleType.BUY.getCode()) && !OtcOperationUtils.getHolAccOperationInChargedForNegotiatior(mechanismOperation, ParticipantRoleType.BUY.getCode()).isEmpty())){
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Isin had balance available on placement segment.
	 *
	 * @param placementSegment the placement segment
	 * @param mechanismOperation the mechanism operation
	 * @return the boolean
	 */
	public static Boolean isinHadBalanceAvailableOnPlacementSegment(PlacementSegment placementSegment, MechanismOperation mechanismOperation){
		if(placementSegment==null || placementSegment.getPlacementSegmentDetails()==null || placementSegment.getPlacementSegmentDetails().isEmpty() || mechanismOperation == null){
			return Boolean.FALSE;
		}
		for(PlacementSegmentDetail placementSegmentDetail: placementSegment.getPlacementSegmentDetails()){
			if(placementSegmentDetail.getSecurity().getIdSecurityCodePk().equals(mechanismOperation.getSecurities().getIdSecurityCodePk())){
				BigDecimal nominalValue = mechanismOperation.getSecurities().getCurrentNominalValue();
				BigDecimal quantitySecurities = mechanismOperation.getStockQuantity();
				BigDecimal amountUsedOnOperation = nominalValue.multiply(quantitySecurities);
				if(placementSegmentDetail.getPlacementSegmentDetType().equals(SecurityPlacementType.FIXED.getCode())){
					BigDecimal amountPlacementToIsin = placementSegmentDetail.getPlacedAmount();
					BigDecimal amountOnRequestToPlacement = placementSegmentDetail.getRequestAmount();
					if((amountUsedOnOperation.add(amountOnRequestToPlacement)).compareTo(amountPlacementToIsin)<=0){
						return Boolean.TRUE;
					}
				}else if(placementSegmentDetail.getPlacementSegmentDetType().equals(SecurityPlacementType.FLOATING.getCode())){
					BigDecimal amountFloatingByPlac = placementSegment.getFloatingAmount();
					BigDecimal amountOnRequestByPlac = placementSegment.getRequestFloatingAmount();
					if((amountUsedOnOperation.add(amountOnRequestByPlac)).compareTo(amountFloatingByPlac)<=0){
						return Boolean.TRUE;
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Issuance is primary placement.
	 *
	 * @param issuance the issuance
	 * @return the boolean
	 */
	public static Boolean issuanceIsLastPlacement(Issuance issuance){
		if(issuance==null || issuance.getNumberTotalTranches()==null){
			return Boolean.FALSE;
		}
		Integer numberPlacements = issuance.getNumberTotalTranches();
		Integer numberCurrentPlacement = issuance.getCurrentTranche()==null?0:issuance.getCurrentTranche();
		return numberCurrentPlacement>=numberPlacements;
	}
	
	/**
	 * Gets the participant order by descrip.
	 *
	 * @param participantList the participant list
	 * @return the participant order by descrip
	 */
	public static List<Participant> getParticipantOrderByDescrip(List<Participant> participantList){
		Collections.sort(participantList, new Comparator<Participant>(){
			  
  			/**
  			 * Compare.
  			 *
  			 * @param s1 the s1
  			 * @param s2 the s2
  			 * @return the int
  			 */
  			public int compare(Participant s1, Participant s2) {
			    return s1.getDescription().compareToIgnoreCase(s2.getDescription());
			  }
			});
		return participantList;
	}
	
	/**
	 * Gets the participant order by mnemonic.
	 *
	 * @param participantList the participant list
	 * @return the participant order by descrip
	 */
	public static List<Participant> getParticipantOrderByMnemonic(List<Participant> participantList){
		Collections.sort(participantList, new Comparator<Participant>(){
			  
  			/**
  			 * Compare.
  			 *
  			 * @param s1 the s1
  			 * @param s2 the s2
  			 * @return the int
  			 */
  			public int compare(Participant s1, Participant s2) {
			    return s1.getMnemonic().compareToIgnoreCase(s2.getMnemonic());
			  }
			});
		return participantList;
	}
	/**
	 * Gets the assigment operation list.
	 *
	 * @param operation the operation
	 * @return the assigment operation list
	 */
	public static List<AssigmentOperationTO> getAssigmentOperationList(MechanismOperation operation){
		if(operation==null || operation.getHolderAccountOperations()==null || operation.getHolderAccountOperations().isEmpty()){
			return null;
		}
		List<AssigmentOperationTO> assigmentOperationList = new ArrayList<AssigmentOperationTO>();
		AssigmentOperationTO assigmentOperationTO = new AssigmentOperationTO();
		BigDecimal quantityAsignBuy = BigDecimal.ZERO;
		BigDecimal amountAsignBuy = BigDecimal.ZERO;
		BigDecimal quantityAsignSell = BigDecimal.ZERO;
		BigDecimal amountAsignSell = BigDecimal.ZERO;
		
		for(OperationPartType partType: OperationPartType.list){
			if(partType.equals(OperationPartType.CASH_PART)){
				for(HolderAccountOperation _accountOperation: operation.getHolderAccountOperations()){
					if(_accountOperation.getHolderAccount()!=null && partType.getCode().equals(_accountOperation.getOperationPart())){
						if(_accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
							quantityAsignSell = quantityAsignSell.add(_accountOperation.getStockQuantity());
							amountAsignSell = amountAsignSell.add(_accountOperation.getCashAmount());
						}else if(_accountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
							quantityAsignBuy = quantityAsignBuy.add(_accountOperation.getStockQuantity());
							amountAsignBuy = amountAsignBuy.add(_accountOperation.getCashAmount());
						}
					}
				}
			}
		}
		assigmentOperationTO.setQuantityAsignBuy(CommonsUtilities.round(quantityAsignBuy, 2));
		assigmentOperationTO.setAmountAsignBuy(amountAsignBuy);
		assigmentOperationTO.setQuantityAsignSell(CommonsUtilities.round(quantityAsignSell,2));
		assigmentOperationTO.setAmountAsignSell(amountAsignSell);
		assigmentOperationList.add(assigmentOperationTO);
		return assigmentOperationList;
	}
}