package com.pradera.negotiations.otcoperations.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.TradeOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class OtcDpfManagementTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The otc dpf trade operation. */
	private TradeOperation otcDpfTradeOperation;
	
	/** The otc dpf operation. */
	private MechanismOperation otcDpfOperation;
	
	/** The all participant list. */
	private List<Participant> allParticipantList;
	
	/** The participant buy list. */
	private List<Participant> participantBuyList;
	
	/** The participant sale list. */
	private List<Participant> participantSaleList;
	
	/** The all participant map. */
	private Map<Long,Participant> allParticipantMap;
	
	/** The account sale. */
	private HolderAccount accountSale;
	
	/** The account buy. */
	private HolderAccount accountBuy;
	
	/** The lst security class bcb. */
	private List<ParameterTable> lstCboDateSearchType,lstCboOtcOperationStateType,lstCboOtcOriginRequestType,motiveCancelOperationList, 
	 							 lstSettlementType, lstSchemeType, lstSecurityClassBcb;
	
	
	/** The security class allowed. */
	private List<Integer> securityClassAllowed;
	
	/** For consults. */
	private OtcOperationTO otcDpfFiltersTO;
	
	/** The otc result data model. */
	private GenericDataModel<OtcOperationResultTO> otcResultDataModel;
	
	/** The otc dpf selected. */
	private OtcOperationResultTO otcDpfSelected;
	
	
	/** The source holder. */
	private Holder sellerHolder;
	
	/** The target holder. */
	private Holder buyerHolder;
	
	/** The source holder accounts. */
	private List<HolderAccount> sellerHolderAccounts;
	
	/** The target holder accounts. */
	private List<HolderAccount> buyerHolderAccounts;
	
	
	/**
	 * Gets the otc dpf trade operation.
	 *
	 * @return the otc dpf trade operation
	 */
	public TradeOperation getOtcDpfTradeOperation() {
		return otcDpfTradeOperation;
	}
	
	/**
	 * Sets the otc dpf trade operation.
	 *
	 * @param otcDpfTradeOperation the new otc dpf trade operation
	 */
	public void setOtcDpfTradeOperation(TradeOperation otcDpfTradeOperation) {
		this.otcDpfTradeOperation = otcDpfTradeOperation;
	}
	
	/**
	 * Gets the otc dpf operation.
	 *
	 * @return the otc dpf operation
	 */
	public MechanismOperation getOtcDpfOperation() {
		return otcDpfOperation;
	}
	
	/**
	 * Sets the otc dpf operation.
	 *
	 * @param otcDpfOperation the new otc dpf operation
	 */
	public void setOtcDpfOperation(MechanismOperation otcDpfOperation) {
		this.otcDpfOperation = otcDpfOperation;
	}
	
	/**
	 * Gets the all participant list.
	 *
	 * @return the all participant list
	 */
	public List<Participant> getAllParticipantList() {
		return allParticipantList;
	}
	
	/**
	 * Sets the all participant list.
	 *
	 * @param allParticipantList the new all participant list
	 */
	public void setAllParticipantList(List<Participant> allParticipantList) {
		this.allParticipantList = allParticipantList;
	}
	
	/**
	 * Gets the all participant map.
	 *
	 * @return the all participant map
	 */
	public Map<Long, Participant> getAllParticipantMap() {
		return allParticipantMap;
	}
	
	/**
	 * Sets the all participant map.
	 *
	 * @param allParticipantMap the all participant map
	 */
	public void setAllParticipantMap(Map<Long, Participant> allParticipantMap) {
		this.allParticipantMap = allParticipantMap;
	}
	
	/**
	 * Gets the lst cbo date search type.
	 *
	 * @return the lst cbo date search type
	 */
	public List<ParameterTable> getLstCboDateSearchType() {
		return lstCboDateSearchType;
	}
	
	/**
	 * Sets the lst cbo date search type.
	 *
	 * @param lstCboDateSearchType the new lst cbo date search type
	 */
	public void setLstCboDateSearchType(List<ParameterTable> lstCboDateSearchType) {
		this.lstCboDateSearchType = lstCboDateSearchType;
	}
	
	/**
	 * Gets the lst cbo otc operation state type.
	 *
	 * @return the lst cbo otc operation state type
	 */
	public List<ParameterTable> getLstCboOtcOperationStateType() {
		return lstCboOtcOperationStateType;
	}
	
	/**
	 * Sets the lst cbo otc operation state type.
	 *
	 * @param lstCboOtcOperationStateType the new lst cbo otc operation state type
	 */
	public void setLstCboOtcOperationStateType(List<ParameterTable> lstCboOtcOperationStateType) {
		this.lstCboOtcOperationStateType = lstCboOtcOperationStateType;
	}
	
	/**
	 * Gets the lst cbo otc origin request type.
	 *
	 * @return the lst cbo otc origin request type
	 */
	public List<ParameterTable> getLstCboOtcOriginRequestType() {
		return lstCboOtcOriginRequestType;
	}
	
	/**
	 * Sets the lst cbo otc origin request type.
	 *
	 * @param lstCboOtcOriginRequestType the new lst cbo otc origin request type
	 */
	public void setLstCboOtcOriginRequestType(List<ParameterTable> lstCboOtcOriginRequestType) {
		this.lstCboOtcOriginRequestType = lstCboOtcOriginRequestType;
	}
	
	/**
	 * Gets the motive cancel operation list.
	 *
	 * @return the motive cancel operation list
	 */
	public List<ParameterTable> getMotiveCancelOperationList() {
		return motiveCancelOperationList;
	}
	
	/**
	 * Sets the motive cancel operation list.
	 *
	 * @param motiveCancelOperationList the new motive cancel operation list
	 */
	public void setMotiveCancelOperationList(List<ParameterTable> motiveCancelOperationList) {
		this.motiveCancelOperationList = motiveCancelOperationList;
	}
	
	/**
	 * Gets the lst settlement type.
	 *
	 * @return the lst settlement type
	 */
	public List<ParameterTable> getLstSettlementType() {
		return lstSettlementType;
	}
	
	/**
	 * Sets the lst settlement type.
	 *
	 * @param lstSettlementType the new lst settlement type
	 */
	public void setLstSettlementType(List<ParameterTable> lstSettlementType) {
		this.lstSettlementType = lstSettlementType;
	}
	
	/**
	 * Gets the lst scheme type.
	 *
	 * @return the lst scheme type
	 */
	public List<ParameterTable> getLstSchemeType() {
		return lstSchemeType;
	}
	
	/**
	 * Sets the lst scheme type.
	 *
	 * @param lstSchemeType the new lst scheme type
	 */
	public void setLstSchemeType(List<ParameterTable> lstSchemeType) {
		this.lstSchemeType = lstSchemeType;
	}
	
	/**
	 * Gets the lst security class bcb.
	 *
	 * @return the lst security class bcb
	 */
	public List<ParameterTable> getLstSecurityClassBcb() {
		return lstSecurityClassBcb;
	}
	
	/**
	 * Sets the lst security class bcb.
	 *
	 * @param lstSecurityClassBcb the new lst security class bcb
	 */
	public void setLstSecurityClassBcb(List<ParameterTable> lstSecurityClassBcb) {
		this.lstSecurityClassBcb = lstSecurityClassBcb;
	}
	
	/**
	 * Gets the participant buy list.
	 *
	 * @return the participant buy list
	 */
	public List<Participant> getParticipantBuyList() {
		return participantBuyList;
	}
	
	/**
	 * Sets the participant buy list.
	 *
	 * @param participantBuyList the new participant buy list
	 */
	public void setParticipantBuyList(List<Participant> participantBuyList) {
		this.participantBuyList = participantBuyList;
	}
	
	/**
	 * Gets the participant sale list.
	 *
	 * @return the participant sale list
	 */
	public List<Participant> getParticipantSaleList() {
		return participantSaleList;
	}
	
	/**
	 * Sets the participant sale list.
	 *
	 * @param participantSaleList the new participant sale list
	 */
	public void setParticipantSaleList(List<Participant> participantSaleList) {
		this.participantSaleList = participantSaleList;
	}
	
	/**
	 * Gets the account sale.
	 *
	 * @return the account sale
	 */
	public HolderAccount getAccountSale() {
		return accountSale;
	}
	
	/**
	 * Sets the account sale.
	 *
	 * @param accountSale the new account sale
	 */
	public void setAccountSale(HolderAccount accountSale) {
		this.accountSale = accountSale;
	}
	
	/**
	 * Gets the account buy.
	 *
	 * @return the account buy
	 */
	public HolderAccount getAccountBuy() {
		return accountBuy;
	}
	
	/**
	 * Sets the account buy.
	 *
	 * @param accountBuy the new account buy
	 */
	public void setAccountBuy(HolderAccount accountBuy) {
		this.accountBuy = accountBuy;
	}
	
	/**
	 * Gets the otc dpf filters to.
	 *
	 * @return the otc dpf filters to
	 */
	public OtcOperationTO getOtcDpfFiltersTO() {
		return otcDpfFiltersTO;
	}
	
	/**
	 * Sets the otc dpf filters to.
	 *
	 * @param otcDpfFiltersTO the new otc dpf filters to
	 */
	public void setOtcDpfFiltersTO(OtcOperationTO otcDpfFiltersTO) {
		this.otcDpfFiltersTO = otcDpfFiltersTO;
	}
	
	/**
	 * Gets the otc result data model.
	 *
	 * @return the otc result data model
	 */
	public GenericDataModel<OtcOperationResultTO> getOtcResultDataModel() {
		return otcResultDataModel;
	}
	
	/**
	 * Sets the otc result data model.
	 *
	 * @param otcResultDataModel the new otc result data model
	 */
	public void setOtcResultDataModel(GenericDataModel<OtcOperationResultTO> otcResultDataModel) {
		this.otcResultDataModel = otcResultDataModel;
	}
	
	/**
	 * Gets the otc dpf selected.
	 *
	 * @return the otc dpf selected
	 */
	public OtcOperationResultTO getOtcDpfSelected() {
		return otcDpfSelected;
	}
	
	/**
	 * Sets the otc dpf selected.
	 *
	 * @param otcDpfSelected the new otc dpf selected
	 */
	public void setOtcDpfSelected(OtcOperationResultTO otcDpfSelected) {
		this.otcDpfSelected = otcDpfSelected;
	}
	
	/**
	 * Gets the security class allowed.
	 *
	 * @return the security class allowed
	 */
	public List<Integer> getSecurityClassAllowed() {
		return securityClassAllowed;
	}
	
	/**
	 * Sets the security class allowed.
	 *
	 * @param securityClassAllowed the new security class allowed
	 */
	public void setSecurityClassAllowed(List<Integer> securityClassAllowed) {
		this.securityClassAllowed = securityClassAllowed;
	}

	/**
	 * @return the sellerHolder
	 */
	public Holder getSellerHolder() {
		return sellerHolder;
	}

	/**
	 * @param sellerHolder the sellerHolder to set
	 */
	public void setSellerHolder(Holder sellerHolder) {
		this.sellerHolder = sellerHolder;
	}

	/**
	 * @return the buyerHolder
	 */
	public Holder getBuyerHolder() {
		return buyerHolder;
	}

	/**
	 * @param buyerHolder the buyerHolder to set
	 */
	public void setBuyerHolder(Holder buyerHolder) {
		this.buyerHolder = buyerHolder;
	}

	/**
	 * @return the sellerHolderAccounts
	 */
	public List<HolderAccount> getSellerHolderAccounts() {
		return sellerHolderAccounts;
	}

	/**
	 * @param sellerHolderAccounts the sellerHolderAccounts to set
	 */
	public void setSellerHolderAccounts(List<HolderAccount> sellerHolderAccounts) {
		this.sellerHolderAccounts = sellerHolderAccounts;
	}

	/**
	 * @return the buyerHolderAccounts
	 */
	public List<HolderAccount> getBuyerHolderAccounts() {
		return buyerHolderAccounts;
	}

	/**
	 * @param buyerHolderAccounts the buyerHolderAccounts to set
	 */
	public void setBuyerHolderAccounts(List<HolderAccount> buyerHolderAccounts) {
		this.buyerHolderAccounts = buyerHolderAccounts;
	}
	
}