package com.pradera.negotiations.otcoperations.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class OtcOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The id modality negotiation. */
	private Long idModalityNegotiation;
	
	/** The lst id modality negotiation. */
	private List<Long> lstIdModalityNegotiation;
	
	/** The id participant buyer. */
	private Long idParticipantBuyer;
	
	/** The id participant seller. */
	private Long idParticipantSeller;
	
	/** The id participant placement. */
	private Long idParticipantPlacement;//Para participante colocador
	
	/** The id isin code. */
	private String idIsinCode;
	
	/** The id search date type. */
	private Integer idSearchDateType;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The otc operation state. */
	private Integer otcOperationState;
	
	/** The ind in charge. */
	private Integer indInCharge;
	
	/** The id issuance pk. */
	private String idIssuancePk;
	
	/** The placement object. */
	private Boolean placementObject;
	
	/** The id placement segment state. */
	private Long idPlacementSegmentState;
	
	/** The id part seller mnemonic. */
	private String idPartSellerMnemonic;
	
	/** The id part buyer mnemonic. */
	private String idPartBuyerMnemonic;
	
	/** The reference number. */
	private String referenceNumber;
	
	/** The reference date. */
	private Date referenceDate;
	
	/** The need accounts. */
	private boolean needAccounts;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The ind dpf. */
	private Integer indDpf;
	
	/** The instrumentType */
	private Integer instrumentType;

	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}

	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}

	/**
	 * Gets the id modality negotiation.
	 *
	 * @return the id modality negotiation
	 */
	public Long getIdModalityNegotiation() {
		return idModalityNegotiation;
	}

	/**
	 * Sets the id modality negotiation.
	 *
	 * @param idModalityNegotiation the new id modality negotiation
	 */
	public void setIdModalityNegotiation(Long idModalityNegotiation) {
		this.idModalityNegotiation = idModalityNegotiation;
	}

	/**
	 * Gets the id participant buyer.
	 *
	 * @return the id participant buyer
	 */
	public Long getIdParticipantBuyer() {
		return idParticipantBuyer;
	}

	/**
	 * Sets the id participant buyer.
	 *
	 * @param idParticipantBuyer the new id participant buyer
	 */
	public void setIdParticipantBuyer(Long idParticipantBuyer) {
		this.idParticipantBuyer = idParticipantBuyer;
	}

	/**
	 * Gets the id participant seller.
	 *
	 * @return the id participant seller
	 */
	public Long getIdParticipantSeller() {
		return idParticipantSeller;
	}

	/**
	 * Sets the id participant seller.
	 *
	 * @param idParticipantSeller the new id participant seller
	 */
	public void setIdParticipantSeller(Long idParticipantSeller) {
		this.idParticipantSeller = idParticipantSeller;
	}

	/**
	 * Gets the id isin code.
	 *
	 * @return the id isin code
	 */
	public String getIdIsinCode() {
		return idIsinCode;
	}

	/**
	 * Sets the id isin code.
	 *
	 * @param idIsinCode the new id isin code
	 */
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}

	/**
	 * Gets the id search date type.
	 *
	 * @return the id search date type
	 */
	public Integer getIdSearchDateType() {
		return idSearchDateType;
	}

	/**
	 * Sets the id search date type.
	 *
	 * @param idSearchDateType the new id search date type
	 */
	public void setIdSearchDateType(Integer idSearchDateType) {
		this.idSearchDateType = idSearchDateType;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the otc operation state.
	 *
	 * @return the otc operation state
	 */
	public Integer getOtcOperationState() {
		return otcOperationState;
	}

	/**
	 * Sets the otc operation state.
	 *
	 * @param otcOperationState the new otc operation state
	 */
	public void setOtcOperationState(Integer otcOperationState) {
		this.otcOperationState = otcOperationState;
	}

	/**
	 * Gets the ind in charge.
	 *
	 * @return the ind in charge
	 */
	public Integer getIndInCharge() {
		return indInCharge;
	}

	/**
	 * Sets the ind in charge.
	 *
	 * @param indInCharge the new ind in charge
	 */
	public void setIndInCharge(Integer indInCharge) {
		this.indInCharge = indInCharge;
	}

	/**
	 * Gets the id issuance pk.
	 *
	 * @return the id issuance pk
	 */
	public String getIdIssuancePk() {
		return idIssuancePk;
	}

	/**
	 * Sets the id issuance pk.
	 *
	 * @param idIssuancePk the new id issuance pk
	 */
	public void setIdIssuancePk(String idIssuancePk) {
		this.idIssuancePk = idIssuancePk;
	}

	/**
	 * Gets the id participant placement.
	 *
	 * @return the id participant placement
	 */
	public Long getIdParticipantPlacement() {
		return idParticipantPlacement;
	}

	/**
	 * Sets the id participant placement.
	 *
	 * @param idParticipantPlacement the new id participant placement
	 */
	public void setIdParticipantPlacement(Long idParticipantPlacement) {
		this.idParticipantPlacement = idParticipantPlacement;
	}

	/**
	 * Gets the placement object.
	 *
	 * @return the placement object
	 */
	public Boolean getPlacementObject() {
		return placementObject;
	}

	/**
	 * Sets the placement object.
	 *
	 * @param placementObject the new placement object
	 */
	public void setPlacementObject(Boolean placementObject) {
		this.placementObject = placementObject;
	}

	/**
	 * Gets the id placement segment state.
	 *
	 * @return the id placement segment state
	 */
	public Long getIdPlacementSegmentState() {
		return idPlacementSegmentState;
	}

	/**
	 * Sets the id placement segment state.
	 *
	 * @param idPlacementSegmentState the new id placement segment state
	 */
	public void setIdPlacementSegmentState(Long idPlacementSegmentState) {
		this.idPlacementSegmentState = idPlacementSegmentState;
	}

	/**
	 * Gets the lst id modality negotiation.
	 *
	 * @return the lstIdModalityNegotiation
	 */
	public List<Long> getLstIdModalityNegotiation() {
		return lstIdModalityNegotiation;
	}

	/**
	 * Sets the lst id modality negotiation.
	 *
	 * @param lstIdModalityNegotiation the lstIdModalityNegotiation to set
	 */
	public void setLstIdModalityNegotiation(List<Long> lstIdModalityNegotiation) {
		this.lstIdModalityNegotiation = lstIdModalityNegotiation;
	}

	/**
	 * Gets the id part seller mnemonic.
	 *
	 * @return the idPartSellerMnemonic
	 */
	public String getIdPartSellerMnemonic() {
		return idPartSellerMnemonic;
	}

	/**
	 * Sets the id part seller mnemonic.
	 *
	 * @param idPartSellerMnemonic the idPartSellerMnemonic to set
	 */
	public void setIdPartSellerMnemonic(String idPartSellerMnemonic) {
		this.idPartSellerMnemonic = idPartSellerMnemonic;
	}

	/**
	 * Gets the id part buyer mnemonic.
	 *
	 * @return the idPartBuyerMnemonic
	 */
	public String getIdPartBuyerMnemonic() {
		return idPartBuyerMnemonic;
	}

	/**
	 * Sets the id part buyer mnemonic.
	 *
	 * @param idPartBuyerMnemonic the idPartBuyerMnemonic to set
	 */
	public void setIdPartBuyerMnemonic(String idPartBuyerMnemonic) {
		this.idPartBuyerMnemonic = idPartBuyerMnemonic;
	}

	/**
	 * Gets the reference number.
	 *
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * Sets the reference number.
	 *
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * Gets the reference date.
	 *
	 * @return the referenceDate
	 */
	public Date getReferenceDate() {
		return referenceDate;
	}

	/**
	 * Sets the reference date.
	 *
	 * @param referenceDate the referenceDate to set
	 */
	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	/**
	 * Checks if is need accounts.
	 *
	 * @return the needAccounts
	 */
	public boolean isNeedAccounts() {
		return needAccounts;
	}

	/**
	 * Sets the need accounts.
	 *
	 * @param needAccounts the needAccounts to set
	 */
	public void setNeedAccounts(boolean needAccounts) {
		this.needAccounts = needAccounts;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the ind dpf.
	 *
	 * @return the ind dpf
	 */
	public Integer getIndDpf() {
		return indDpf;
	}

	/**
	 * Sets the ind dpf.
	 *
	 * @param indDpf the new ind dpf
	 */
	public void setIndDpf(Integer indDpf) {
		this.indDpf = indDpf;
	}

	/**
	 * Gets the instrument type
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type
	 *
	 * @return the instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	
	
}