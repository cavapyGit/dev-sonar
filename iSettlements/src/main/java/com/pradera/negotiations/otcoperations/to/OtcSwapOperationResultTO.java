package com.pradera.negotiations.otcoperations.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.ParticipantRoleType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class OtcSwapOperationResultTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation request pk. */
	private Long idMechanismOperationRequestPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id part seller. */
	private Participant participantSeller;
	
	/** The id part buyer. */
	private Participant participantBuyer;
	
	/** The isin operation. */
	private Security isinOperation;
	
	/** The participation operation. */
	private Integer participationOperation;//Participacion del isin, como Venta O compra. Type "ParticipantRoleType".
	
	/** The cash amount. */
	private BigDecimal cashAmount;//Monto sucio de la operacion
	
	/** The cash price. */
	private BigDecimal cashPrice;//Precio sucio de la operacion
	
	/** The real cash amount. */
	private BigDecimal realCashAmount;//Monto limpio de la operacion
	
	/** The real priceamount. */
	private BigDecimal realPriceamount;//Precio sucio de la operacion
	
	/** The Stock quantity. */
	private BigDecimal StockQuantity;//Cantidad de valores
	
	/** The currency. */
	private Integer currency;//Moneda del valor
	
	/** The nominal value. */
	private BigDecimal nominalValue;

	/**
	 * Gets the id mechanism operation request pk.
	 *
	 * @return the id mechanism operation request pk
	 */
	public Long getIdMechanismOperationRequestPk() {
		return idMechanismOperationRequestPk;
	}

	/**
	 * Sets the id mechanism operation request pk.
	 *
	 * @param idMechanismOperationRequestPk the new id mechanism operation request pk
	 */
	public void setIdMechanismOperationRequestPk(Long idMechanismOperationRequestPk) {
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
	}

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	/**
	 * Gets the isin operation.
	 *
	 * @return the isin operation
	 */
	public Security getIsinOperation() {
		return isinOperation;
	}

	/**
	 * Sets the isin operation.
	 *
	 * @param isinOperation the new isin operation
	 */
	public void setIsinOperation(Security isinOperation) {
		this.isinOperation = isinOperation;
	}
	/**
	 * Gets the participation operation.
	 *
	 * @return the participation operation
	 */
	public Integer getParticipationOperation() {
		return participationOperation;
	}
	/**
	 * Sets the participation operation.
	 *
	 * @param participationOperation the new participation operation
	 */
	public void setParticipationOperation(Integer participationOperation) {
		this.participationOperation = participationOperation;
	}
	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}
	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}
	/**
	 * Gets the cash price.
	 *
	 * @return the cash price
	 */
	public BigDecimal getCashPrice() {
		return cashPrice;
	}
	/**
	 * Sets the cash price.
	 *
	 * @param cashPrice the new cash price
	 */
	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}
	/**
	 * Gets the real cash amount.
	 *
	 * @return the real cash amount
	 */
	public BigDecimal getRealCashAmount() {
		return realCashAmount;
	}
	/**
	 * Sets the real cash amount.
	 *
	 * @param realCashAmount the new real cash amount
	 */
	public void setRealCashAmount(BigDecimal realCashAmount) {
		this.realCashAmount = realCashAmount;
	}
	/**
	 * Gets the real priceamount.
	 *
	 * @return the real priceamount
	 */
	public BigDecimal getRealPriceamount() {
		return realPriceamount;
	}
	/**
	 * Sets the real priceamount.
	 *
	 * @param realPriceamount the new real priceamount
	 */
	public void setRealPriceamount(BigDecimal realPriceamount) {
		this.realPriceamount = realPriceamount;
	}
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return StockQuantity;
	}
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		StockQuantity = stockQuantity;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	/**
	 * Gets the participant seller.
	 *
	 * @return the participant seller
	 */
	public Participant getParticipantSeller() {
		return participantSeller;
	}
	/**
	 * Sets the participant seller.
	 *
	 * @param participantSeller the new participant seller
	 */
	public void setParticipantSeller(Participant participantSeller) {
		this.participantSeller = participantSeller;
	}
	/**
	 * Gets the participant buyer.
	 *
	 * @return the participant buyer
	 */
	public Participant getParticipantBuyer() {
		return participantBuyer;
	}
	/**
	 * Sets the participant buyer.
	 *
	 * @param participantBuyer the new participant buyer
	 */
	public void setParticipantBuyer(Participant participantBuyer) {
		this.participantBuyer = participantBuyer;
	}
	/**
	 * Gets the participation role description.
	 *
	 * @return the participation role description
	 */
	public String getParticipationRoleDescription(){
		return ParticipantRoleType.get(this.participationOperation).getValue();
	}
	/**
	 * Gets the nominal value.
	 *
	 * @return the nominal value
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}
	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the new nominal value
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}
}