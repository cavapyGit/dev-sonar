package com.pradera.negotiations.otcoperations.view;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.model.accounts.Participant;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/07/2013
 */
public class AssigmentOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The participant. */
	private Participant participant;
	
	/** The operation part. */
	private String operationPart;
	
	/** The quantity asign sell. */
	private BigDecimal quantityAsignSell;
	
	/** The amount asign sell. */
	private BigDecimal amountAsignSell;
	
	/** The is incharge sell. */
	private Boolean isInchargeSell;
	
	/** The ind incharge sell. */
	private String indInchargeSell;
	
	/** The quantity asign buy. */
	private BigDecimal quantityAsignBuy;
	
	/** The amount asign buy. */
	private BigDecimal amountAsignBuy;
	
	/** The is incharge buy. */
	private Boolean isInchargeBuy;
	
	/** The ind incharge buy. */
	private String indInchargeBuy;

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the quantity asign sell.
	 *
	 * @return the quantity asign sell
	 */
	public BigDecimal getQuantityAsignSell() {
		return quantityAsignSell;
	}

	/**
	 * Sets the quantity asign sell.
	 *
	 * @param quantityAsignSell the new quantity asign sell
	 */
	public void setQuantityAsignSell(BigDecimal quantityAsignSell) {
		this.quantityAsignSell = quantityAsignSell;
	}

	/**
	 * Gets the amount asign sell.
	 *
	 * @return the amount asign sell
	 */
	public BigDecimal getAmountAsignSell() {
		return amountAsignSell;
	}

	/**
	 * Sets the amount asign sell.
	 *
	 * @param amountAsignSell the new amount asign sell
	 */
	public void setAmountAsignSell(BigDecimal amountAsignSell) {
		this.amountAsignSell = amountAsignSell;
	}

	/**
	 * Gets the checks if is incharge sell.
	 *
	 * @return the checks if is incharge sell
	 */
	public Boolean getIsInchargeSell() {
		return isInchargeSell;
	}

	/**
	 * Sets the checks if is incharge sell.
	 *
	 * @param isInchargeSell the new checks if is incharge sell
	 */
	public void setIsInchargeSell(Boolean isInchargeSell) {
		this.isInchargeSell = isInchargeSell;
	}

	/**
	 * Gets the ind incharge sell.
	 *
	 * @return the ind incharge sell
	 */
	public String getIndInchargeSell() {
		return indInchargeSell;
	}

	/**
	 * Sets the ind incharge sell.
	 *
	 * @param indInchargeSell the new ind incharge sell
	 */
	public void setIndInchargeSell(String indInchargeSell) {
		this.indInchargeSell = indInchargeSell;
	}

	/**
	 * Gets the quantity asign buy.
	 *
	 * @return the quantity asign buy
	 */
	public BigDecimal getQuantityAsignBuy() {
		return quantityAsignBuy;
	}

	/**
	 * Sets the quantity asign buy.
	 *
	 * @param quantityAsignBuy the new quantity asign buy
	 */
	public void setQuantityAsignBuy(BigDecimal quantityAsignBuy) {
		this.quantityAsignBuy = quantityAsignBuy;
	}

	/**
	 * Gets the amount asign buy.
	 *
	 * @return the amount asign buy
	 */
	public BigDecimal getAmountAsignBuy() {
		return amountAsignBuy;
	}

	/**
	 * Sets the amount asign buy.
	 *
	 * @param amountAsignBuy the new amount asign buy
	 */
	public void setAmountAsignBuy(BigDecimal amountAsignBuy) {
		this.amountAsignBuy = amountAsignBuy;
	}

	/**
	 * Gets the checks if is incharge buy.
	 *
	 * @return the checks if is incharge buy
	 */
	public Boolean getIsInchargeBuy() {
		return isInchargeBuy;
	}

	/**
	 * Sets the checks if is incharge buy.
	 *
	 * @param isInchargeBuy the new checks if is incharge buy
	 */
	public void setIsInchargeBuy(Boolean isInchargeBuy) {
		this.isInchargeBuy = isInchargeBuy;
	}

	/**
	 * Gets the ind incharge buy.
	 *
	 * @return the ind incharge buy
	 */
	public String getIndInchargeBuy() {
		return indInchargeBuy;
	}

	/**
	 * Sets the ind incharge buy.
	 *
	 * @param indInchargeBuy the new ind incharge buy
	 */
	public void setIndInchargeBuy(String indInchargeBuy) {
		this.indInchargeBuy = indInchargeBuy;
	}

	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public String getOperationPart() {
		return operationPart;
	}

	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(String operationPart) {
		this.operationPart = operationPart;
	}
}