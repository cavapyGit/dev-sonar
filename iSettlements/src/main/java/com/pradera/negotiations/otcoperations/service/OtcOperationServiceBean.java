package com.pradera.negotiations.otcoperations.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.operation.to.MechanismModalityTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationAdj;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.ParticipantOperation;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.InChargeNegotiationType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityStateType;
import com.pradera.model.negotiation.type.OtcOperationDateSearchType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.negotiations.otcoperations.to.OtcOperationResultTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationTO;
import com.pradera.negotiations.otcoperations.view.OtcOperationUtils;
import com.pradera.negotiations.otcoperations.view.OtcSwapOperationUtils;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class OtcOperationServiceBean extends CrudDaoServiceBean {

	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/** The negotiation operation service bean. */
	@EJB
	NegotiationOperationServiceBean negotiationOperationServiceBean;

	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
	
	/** The settlement service. */
	@EJB
	SettlementProcessService settlementService;
	
	/** The securities remote service. */
	@Inject Instance<SecuritiesRemoteService> securitiesRemoteService;

	/**
	 * Gets the mechanism modality list.
	 * 
	 * @param mechanismModalityTO
	 *            the mechanism modality to
	 * @return the mechanism modality list
	 * @throws ServiceException
	 *             the service exception
	 */
	@SuppressWarnings({ "unchecked" })
	public List<MechanismModality> getMechanismModalityListServiceBean(MechanismModalityTO mechanismModalityTO) throws ServiceException {
		StringBuilder selctSql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		StringBuilder orderBySql = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		selctSql.append("Select Distinct mo From MechanismModality mo						");
		selctSql.append("Inner Join Fetch mo.negotiationModality negmod						");
		selctSql.append("Inner Join Fetch mo.participantMechanisms part						");
		selctSql.append("Inner Join Fetch part.participant participante						");
		whereSql.append("Where 1 = 1														");
		if (mechanismModalityTO.getIdNegotiationMechanismPK() != null) {
			whereSql.append("And mo.id.idNegotiationMechanismPk = :negMechanismId			");
			parameters.put("negMechanismId",mechanismModalityTO.getIdNegotiationMechanismPK());
		}

		if (mechanismModalityTO.getNegotiationModalityState() != null) {
			whereSql.append("And mo.stateMechanismModality = :stateMechanismMod				");
			parameters.put("stateMechanismMod",mechanismModalityTO.getNegotiationModalityState());
		}

		if (mechanismModalityTO.getIdParticipantPk() != null) {
			whereSql.append("And participante.idParticipantPk = :participantCode			");
			parameters.put("participantCode",mechanismModalityTO.getIdParticipantPk());
		}

		whereSql.append("And part.stateParticipantMechanism = :statePartMechanism			");
		parameters.put("statePartMechanism",ParticipantMechanismStateType.REGISTERED.getCode());

		orderBySql.append("Order By negmod.modalityName Asc									");

		List<MechanismModality> mechanismModalityList = findListByQueryString(selctSql.toString().concat(whereSql.toString()).concat(orderBySql.toString()), parameters);
		return mechanismModalityList;
	}

	/**
	 * Gets the otc operations service bean.
	 * 
	 * @param otcOperationTO
	 *            the otc operation to
	 * @return the otc operations service bean
	 * @throws ServiceException
	 *             the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<OtcOperationResultTO> getOtcOperationsServiceBean(OtcOperationTO otcOperationTO) throws ServiceException {
		List<OtcOperationResultTO> otcOperationResult = new ArrayList<OtcOperationResultTO>();
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();

		//selectSQL.append("Select new com.pradera.negotiations.otcoperations.to.OtcOperationResultTO(							");
		selectSQL.append("select otc.idMechanismOperationPk, otc.mechanisnModality.negotiationModality.idNegotiationModalityPk,	");
		selectSQL.append("otc.mechanisnModality.negotiationModality.modalityName, otc.operationDate, otc.operationNumber, 		");
		selectSQL.append("otc.sellerParticipant.idParticipantPk, otc.sellerParticipant.mnemonic,								");
		selectSQL.append("otc.buyerParticipant.idParticipantPk, otc.buyerParticipant.mnemonic,									");
		selectSQL.append("otc.cashSettlementDate, otc.termSettlementDate, otc.securities.idSecurityCodePk,						");
		selectSQL.append("otc.tradeOperation.operationState,otc.registerUser, otc.tradeOperation.reviewUser,					");
		selectSQL.append("otc.settlementType																					");
		selectSQL.append("From MechanismOperation otc																			");
		whereSQL.append("Where otc.mechanisnModality.id.idNegotiationMechanismPk = :negMechanismId								");
		parameters.put("negMechanismId", NegotiationMechanismType.OTC.getCode());

		if (otcOperationTO.getIdSearchDateType() != null) {
			String operationDateType = null;
			switch (OtcOperationDateSearchType.get(otcOperationTO.getIdSearchDateType())) {
				case OPERATION_DATE:	operationDateType = "registerDate";break;
				case CASH_SETTLEM_DATE:	operationDateType = "cashSettlementDate";break;
				case TERM_SETTLEM_DATE:	operationDateType = "termSettlementDate";break;
			}
			whereSQL.append("And (otc.").append(operationDateType).append("	BETWEEN :initialDate And :finalDate)			");
			parameters.put("initialDate", otcOperationTO.getInitialDate());
			parameters.put("finalDate", CommonsUtilities.addDaysToDate(otcOperationTO.getFinalDate(),1));
		}else{
			if(otcOperationTO.getInitialDate()!=null && otcOperationTO.getFinalDate()!=null ){
				whereSQL.append("And ( (otc.cashSettlementDate BETWEEN :initialDate And :finalDate) ").
				append(" Or (otc.termSettlementDate BETWEEN :initialDate And :finalDate) ) ");
				
				parameters.put("initialDate", otcOperationTO.getInitialDate());
				parameters.put("finalDate", CommonsUtilities.addDaysToDate(otcOperationTO.getFinalDate(),1));
			}
			
		}

		if(otcOperationTO.getIndDpf()!=null){
			whereSQL.append("And ( otc.indDpf = :indOneDpf or otc.indDpf is null )																	");
			parameters.put("indOneDpf",otcOperationTO.getIndDpf());
		}
		
		if (otcOperationTO.getOtcOperationState() != null) {
			whereSQL.append("And otc.tradeOperation.operationState = :otcOperationState										");
			parameters.put("otcOperationState",otcOperationTO.getOtcOperationState());
		}
		if (otcOperationTO.getIdModalityNegotiation() != null) {
			whereSQL.append("And otc.mechanisnModality.negotiationModality.idNegotiationModalityPk = :negModalityid			");
			parameters.put("negModalityid",otcOperationTO.getIdModalityNegotiation());
		}
		if (otcOperationTO.getInstrumentType() != null) {
			whereSQL.append("And otc.mechanisnModality.negotiationModality.instrumentType = :instrumentType					");
			parameters.put("instrumentType",otcOperationTO.getInstrumentType());
		}
		if (otcOperationTO.getIndInCharge()!=null && otcOperationTO.getIndInCharge().equals(BooleanType.YES.getCode())) {
			selectSQL.append("Inner Join otc.holderAccountOperations hao													");
			whereSQL.append("And 	hao.indIncharge = :inchargeOne															");
			parameters.put("inchargeOne", otcOperationTO.getIndInCharge());

			if (otcOperationTO.getIdParticipantBuyer() != null) {
				whereSQL.append("And	hao.inchargeFundsParticipant.idParticipantPk = :participantChargeFunds				");
				parameters.put("participantChargeFunds",otcOperationTO.getIdParticipantBuyer());
			}
		} else {
			if (otcOperationTO.getIdParticipantBuyer() != null) {
				whereSQL.append("And	((otc.buyerParticipant.idParticipantPk = :participCode 		And						");
				whereSQL.append("		 otc.tradeOperation.operationState NOT IN (:tradeRegisterState) )					");
				whereSQL.append("		 OR																					");
				whereSQL.append("		 otc.sellerParticipant.idParticipantPk = :participCode )							");
				parameters.put("participCode",otcOperationTO.getIdParticipantBuyer());

				List<Integer> arrayStates = new ArrayList<Integer>();
				arrayStates.add(OtcOperationStateType.REGISTERED.getCode());
				arrayStates.add(OtcOperationStateType.ANULATE.getCode());
				parameters.put("tradeRegisterState", arrayStates);
			}
		}
		whereSQL.append("ORDER BY 	otc.idMechanismOperationPk desc															");
		List<Object[]> result =  findListByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
		for(Object[] objResult  : result){
			otcOperationResult.add(OtcOperationUtils.getOperationResult(objResult));
		}
		
		return otcOperationResult;
	}

	/**
	 * Gets the otc operations service bean.
	 * 
	 * @param otcOperationTO
	 *            the otc operation to
	 * @return the otc operations service bean
	 * @throws ServiceException
	 *             the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<OtcOperationResultTO> getOtcOperations4ReviewServiceBean(OtcOperationTO otcOperationTO) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		StringBuilder orderBySQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();

		selectSQL.append("Select new com.pradera.negotiations.otcoperations.to.OtcOperationResultTO(");
		selectSQL.append("otc.idMechanismOperationPk, otc.mechanisnModality.negotiationModality.idNegotiationModalityPk,	");
		selectSQL.append("otc.mechanisnModality.negotiationModality.modalityName, otc.operationDate, otc.operationNumber,	");
		selectSQL.append("otc.referenceNumber, otc.sellerParticipant.idParticipantPk, otc.sellerParticipant.mnemonic,		");
		selectSQL.append("otc.buyerParticipant.idParticipantPk, otc.buyerParticipant.mnemonic, otc.cashSettlementDate,		");
		selectSQL.append("otc.termSettlementDate, otc.securities.idSecurityCodePk, otc.tradeOperation.operationState,		");
		selectSQL.append("otc.termSettlementDate, otc.securities.idSecurityCodePk, otc.tradeOperation.operationState,		");
		selectSQL.append("otc.mechanisnModality.negotiationModality.modalityCode, otc.referenceDate, otc.stockQuantity,		");
		selectSQL.append("otc.realCashPrice, otc.cashAmount, otc.termSettlementDays, otc.termPrice, otc.termAmount)			");

		List<Integer> otcStates = Arrays.asList(OtcOperationStateType.ANULATE.getCode(),OtcOperationStateType.REJECTED.getCode(),
												OtcOperationStateType.CANCELED.getCode(),MechanismOperationStateType.CANCELED_STATE.getCode());

		selectSQL.append("From MechanismOperation otc																			");
		whereSQL.append("Where otc.operationState NOT IN :otcOperationState And 												");
		whereSQL.append("	   otc.mechanisnModality.id.idNegotiationMechanismPk = :negMechanismId								");

		parameters.put("otcOperationState", otcStates);
		parameters.put("negMechanismId", NegotiationMechanismType.OTC.getCode());

		if (otcOperationTO.getLstIdModalityNegotiation() != null) {
			whereSQL.append("And   otc.mechanisnModality.negotiationModality.idNegotiationModalityPk IN :lstNegMechanismId		");
			parameters.put("lstNegMechanismId",otcOperationTO.getLstIdModalityNegotiation());
		}
		if (otcOperationTO.getOtcOperationState() != null) {
			whereSQL.append("And   otc.tradeOperation.operationState = :otcOperationState										");
			parameters.put("otcOperationState",otcOperationTO.getOtcOperationState());
		}
		if (otcOperationTO.getReferenceDate() != null) {
			whereSQL.append("And	otc.referenceDate = :referencDate															");
			parameters.put("referencDate", otcOperationTO.getReferenceDate());
		}
		if (otcOperationTO.getReferenceNumber() != null) {
			whereSQL.append("And	otc.referenceNumber = :referencNumber														");
			parameters.put("referencNumber",otcOperationTO.getReferenceNumber());
		}
		if (otcOperationTO.getIdParticipantSeller() != null) {
			whereSQL.append("And	otc.sellerParticipant.idParticipantPk = :sellParticipantCode								");
			parameters.put("sellParticipantCode",otcOperationTO.getIdParticipantSeller());
		}

		if (otcOperationTO.getIdParticipantBuyer() != null) {
			whereSQL.append("And	otc.buyerParticipant.idParticipantPk = :buyParticipantCode									");
			parameters.put("buyParticipantCode",otcOperationTO.getIdParticipantBuyer());
		}

		orderBySQL.append("otc.idMechanismOperationPk DESC");

		try {
			return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()).concat(orderBySQL.toString()), parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * Find mechanism operation by number. metodo para buscar una operacion.
	 *
	 * @param mechanismOperationTO            the mechanism operation to
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation findMechanismOperationByNumberServiceBean(MechanismOperation mechanismOperationTO) throws ServiceException {
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT DISTINCT(mo) FROM MechanismOperation mo ");
		querySql.append("INNER JOIN FETCH mo.holderAccountOperations hao ");
		querySql.append("WHERE trunc(mo.operationDate) = trunc(:idMechanismOperationDateParam) ");
		querySql.append("AND mo.operationNumber = :idOperationNumberParam ");
		querySql.append("AND mo.mechanisnModality.id.idNegotiationMechanismPk = :idNegotiationMechanismParam ");
		querySql.append("AND mo.mechanisnModality.id.idNegotiationModalityPk = :idNegotiationModalityParam ");
		querySql.append("AND hao.refAccountOperation IS null ");
		Query query = em.createQuery(querySql.toString());
		query.setParameter("idNegotiationMechanismParam",NegotiationMechanismType.OTC.getCode());
		query.setParameter("idMechanismOperationDateParam",mechanismOperationTO.getOperationDate());
		query.setParameter("idOperationNumberParam",mechanismOperationTO.getOperationNumber());
		query.setParameter("idNegotiationModalityParam", mechanismOperationTO.getMechanisnModality().getId().getIdNegotiationModalityPk());

		MechanismOperation mechanismOperation = null;
		try {
			mechanismOperation = (MechanismOperation) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
		negotiationOperationServiceBean.validateSecundaryOnReporto(mechanismOperation.getIdMechanismOperationPk());

		for (HolderAccountOperation holderAccountOperation : mechanismOperation.getHolderAccountOperations()) {// Read operation's holderAccounts
			if (holderAccountOperation.getHolderAccount() != null) {
				holderAccountOperation.getHolderAccount().getHolderAccountDetails().get(0).getHolder();
			}
			holderAccountOperation.getInchargeFundsParticipant().getIdParticipantPk();
			holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk();
		}
		if (mechanismOperation.getReferenceOperation() != null) {// Si tiene referencia
			mechanismOperation.getReferenceOperation().getIdMechanismOperationPk();// Leemos la referencia, para  el caso que el repo tenga secundario).
		}

		mechanismOperation.getBuyerParticipant().getIdParticipantPk();
		mechanismOperation.getSecurities().getIdSecurityCodePk();
		mechanismOperation.getSecurities().getIssuance().getIdIssuanceCodePk();
		mechanismOperation.getSecurities().getIssuer().getIdIssuerPk();
		mechanismOperation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
		mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		return mechanismOperation;
	}
	
	public MechanismOperation findMechanismOperationAvailableBal(MechanismOperation mechanismOperationTO) throws ServiceException {
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT DISTINCT(mo) FROM MechanismOperation mo ");
		querySql.append("INNER JOIN FETCH mo.holderAccountOperations hao ");
		querySql.append("WHERE trunc(mo.operationDate) = trunc(:idMechanismOperationDateParam) ");
		if(mechanismOperationTO.getOperationNumber() != null) {
			querySql.append("AND mo.operationNumber = " + mechanismOperationTO.getOperationNumber());
		} else {
			querySql.append("AND mo.operationNumber IS null ");
		}
		querySql.append("AND mo.mechanisnModality.id.idNegotiationMechanismPk = :idNegotiationMechanismParam ");
		querySql.append("AND mo.mechanisnModality.id.idNegotiationModalityPk = :idNegotiationModalityParam ");
		querySql.append("AND mo.idMechanismOperationPk = :idMechanismOperationPk ");
		querySql.append("AND hao.refAccountOperation IS null ");
		Query query = em.createQuery(querySql.toString());
		query.setParameter("idNegotiationMechanismParam",NegotiationMechanismType.OTC.getCode());
		query.setParameter("idMechanismOperationDateParam",mechanismOperationTO.getOperationDate());
		query.setParameter("idNegotiationModalityParam", mechanismOperationTO.getMechanisnModality().getId().getIdNegotiationModalityPk());
		query.setParameter("idMechanismOperationPk",mechanismOperationTO.getIdMechanismOperationPk()); 

		MechanismOperation mechanismOperation = null;
		try {
			mechanismOperation = (MechanismOperation) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
		for (HolderAccountOperation holderAccountOperation : mechanismOperation.getHolderAccountOperations()) {// Read operation's holderAccounts
			if (holderAccountOperation.getHolderAccount() != null) {
				holderAccountOperation.getHolderAccount().getHolderAccountDetails().get(0).getHolder();
			}
			holderAccountOperation.getInchargeFundsParticipant().getIdParticipantPk();
			holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk();
		}
		if (mechanismOperation.getReferenceOperation() != null) {// Si tiene referencia
			mechanismOperation.getReferenceOperation().getIdMechanismOperationPk();// Leemos la referencia, para  el caso que el repo tenga secundario).
		}

		mechanismOperation.getBuyerParticipant().getIdParticipantPk();
		mechanismOperation.getSecurities().getIdSecurityCodePk();
		mechanismOperation.getSecurities().getIssuance().getIdIssuanceCodePk();
		mechanismOperation.getSecurities().getIssuer().getIdIssuerPk();
		mechanismOperation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
		mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		return mechanismOperation;
	}
	/**
	 * Gets the otc operation.
	 * 
	 * @param otcOperationTO
	 *            the otc operation to
	 * @return the otc operation
	 * @throws ServiceException
	 *             the service exception
	 */
	public MechanismOperation getOtcOperationServiceBean(OtcOperationTO otcOperationTO) throws ServiceException {
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT DISTINCT(mo) FROM MechanismOperation mo ");
		querySql.append(" inner join fetch mo.mechanisnModality mm ");
		querySql.append(" inner join fetch mm.negotiationModality ");
		querySql.append(" inner join fetch mm.negotiationMechanism ");
		querySql.append(" left join mo.mechanismOperationMarketFacts ");
//		if (OtcOperationUtils.otcOperationHaveRecursive(otcOperationTO) && otcOperationTO.isNeedAccounts()){// Si es recursivo solo se necesita traer los hijos
			querySql.append("INNER JOIN mo.holderAccountOperations hao ");
			querySql.append("left JOIN hao.accountOperationMarketFacts mark ");
//		}
		
		querySql.append("WHERE mo.idMechanismOperationPk = :idMechanismOperationPkParam ");
		
		if (OtcOperationUtils.otcOperationHaveRecursive(otcOperationTO))
			querySql.append("AND hao.refAccountOperation IS null ");
		
		Query queryJpql = em.createQuery(querySql.toString());
		queryJpql.setParameter("idMechanismOperationPkParam",otcOperationTO.getIdMechanismOperation());

		MechanismOperation mechanismOperation = null;
		try {
			mechanismOperation = (MechanismOperation) queryJpql.getSingleResult();
			mechanismOperation.getBuyerParticipant().getIdParticipantPk();
			mechanismOperation.getSellerParticipant().getIdParticipantPk();
			mechanismOperation.getTradeOperation().getIdTradeOperationPk();
			mechanismOperation.getSecurities().getIdSecurityCodePk();
			mechanismOperation.getSecurities().getIssuer().getIdIssuerPk();
			mechanismOperation.getSecurities().getIssuance().getIdIssuanceCodePk();
			mechanismOperation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
			mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
			if (otcOperationTO.isNeedAccounts()) {
				for (HolderAccountOperation holderAccountOperation : mechanismOperation.getHolderAccountOperations()) {
					if (holderAccountOperation.getHolderAccount() != null) {
						for (HolderAccountDetail accountDetail : holderAccountOperation.getHolderAccount().getHolderAccountDetails()) {
							accountDetail.getHolder().getIdHolderPk();
						}
						for(AccountOperationMarketFact accountMarketFact: holderAccountOperation.getAccountOperationMarketFacts()){
							accountMarketFact.getMarketDate();
						}
					}
					holderAccountOperation.getInchargeFundsParticipant().getIdParticipantPk();
					holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk();
				}
			}
			
			if(mechanismOperation.getSwapOperations()!=null){
				for (SwapOperation swapOperation : mechanismOperation.getSwapOperations()) {
					swapOperation.getSecurities().getIdSecurityCodePk();
					for (HolderAccountOperation holderAccountOperation : swapOperation.getHolderAccountOperations()) {
						if (holderAccountOperation.getHolderAccount() != null) {
							for (HolderAccountDetail accountDetail : holderAccountOperation.getHolderAccount().getHolderAccountDetails()) {
								accountDetail.getHolder().getIdHolderPk();
							}
						}
						holderAccountOperation.getInchargeFundsParticipant().getIdParticipantPk();
						holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk();
					}
				}				
			}
			
			// Cuando los participantes son iguales, la Operacion es Cruzada
			if (mechanismOperation.getBuyerParticipant().getIdParticipantPk().equals(mechanismOperation.getSellerParticipant().getIdParticipantPk())) {
				mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.CRUSADE.getCode());
			} else {// Cuando los participantes son Diferentes, Siempre sera
					// Venta
				mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.SALE.getCode());
			}
		} catch (NoResultException ex) {
			return null;
		}
		return mechanismOperation;
	}

	/**
	 * Mechanism operation is completed. metodo para saber si el objeto enviado
	 * tiene la informacion completa
	 * 
	 * @param mechanismOperation
	 *            the mechanism operation
	 * @return the boolean
	 * @throws ServiceException
	 *             the service exception
	 */
	private Boolean MechanismOperationIsCompleted(MechanismOperation mechanismOperation) throws ServiceException {
		if (mechanismOperation == null) {
			throw new ServiceException();
		}
		Boolean isSwap = NegotiationUtils.mechanismOperationIsSwap(mechanismOperation);
		List<ParticipantRoleType> participantRoleList = ParticipantRoleType.listSomeElements(ParticipantRoleType.SELL);

		if (isSwap) {
			for (SwapOperation swapOperation : mechanismOperation.getSwapOperations()) {
				BigDecimal stockQuantitySwap = swapOperation.getStockQuantity();
				BigDecimal stockQuantityByRole = BigDecimal.ZERO;
				for (HolderAccountOperation accountOperation : swapOperation.getHolderAccountOperations()) {
					if (accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())) {
						stockQuantityByRole = stockQuantityByRole.add(accountOperation.getStockQuantity());
					}
				}
				if (!stockQuantitySwap.equals(stockQuantityByRole)) {
					return Boolean.FALSE;
				}
			}
		} else {
			participantRoleList.add(ParticipantRoleType.BUY);
		}

		for (ParticipantRoleType participantRoleType : participantRoleList) {
			BigDecimal stockQuantity = BigDecimal.ZERO;
			BigDecimal stockQuantityByRole = BigDecimal.ZERO;
			for (HolderAccountOperation holderAccountOperation : mechanismOperation.getHolderAccountOperations()) {
				if(holderAccountOperation.getHolderAccount()!=null){
					if (participantRoleType.getCode().equals(holderAccountOperation.getRole())) {
						if (holderAccountOperation.getIndIncharge().equals(BooleanType.YES.getCode())) {
							if (holderAccountOperation.getHolderAccount() != null) {
								stockQuantity = stockQuantity.add(holderAccountOperation.getStockQuantity());
							}
						} else {
							stockQuantity = stockQuantity.add(holderAccountOperation.getStockQuantity());
						}
					}
				}
			}
			stockQuantityByRole = mechanismOperation.getStockQuantity();
			if (stockQuantity.equals(stockQuantityByRole)) {
				continue;
			} else {
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}

	/**
	 * Registry otc operation.
	 * 
	 * @param otcOperation
	 *            the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException
	 *             the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public MechanismOperation createNewMechanismOperation(MechanismOperation otcOperation, MechanismOperationAdj mechanismOperationAdj) throws ServiceException {
		if (otcOperation.getIndPrimaryPlacement().equals(BooleanType.YES.getCode())) {
			/**UPDATE PLACEMENTE SEGMENT INFORMATION*/
			/*for(HolderAccountOperation accountOperation: otcOperation.getHolderAccountOperations()){
				accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());;
				if(accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
					AccountOperationMarketFact accountMarketFact = new AccountOperationMarketFact();
					accountMarketFact.setHolderAccountOperation(accountOperation);
					accountMarketFact.setOperationQuantity(accountOperation.getStockQuantity());
					accountMarketFact.setOriginalQuantity(accountOperation.getStockQuantity());
					accountMarketFact.setChainedQuantity(BigDecimal.ZERO);
					accountMarketFact.setIndInchain(BooleanType.NO.getCode());
					accountMarketFact.setChainedQuantity(BigDecimal.ZERO);
					accountMarketFact.setMarketDate(CommonsUtilities.currentDate());
					accountMarketFact.setMarketRate(otcOperation.getSecurities().getRateValue());
					accountOperation.getAccountOperationMarketFacts().add(accountMarketFact);
				}
			}*/
		}
		/**Update parameter when currency's security it's different than operation*/
		Integer currency = otcOperation.getCurrency();
		ParameterTable ptCurrency = find(ParameterTable.class, currency);
		negotiationOperationServiceBean.setCurrency(ptCurrency, otcOperation, ComponentConstant.CASH_PART);
		if(otcOperation.getIndTermSettlement().equals(BooleanType.YES.getCode())){
			negotiationOperationServiceBean.setCurrency(ptCurrency, otcOperation, ComponentConstant.TERM_PART);
		}

		/* INDICADORES NOT NULL, DEBEN RECIBIR AL MENOS 1 VALOR */
		otcOperation.setIndCashSettlementExchange(BooleanType.NO.getCode());
		if(otcOperation.getCurrency().equals(otcOperation.getCashSettlementCurrency())){
			otcOperation.setCashSettlementAmount(otcOperation.getRealCashAmount());
			otcOperation.setCashSettlementPrice(otcOperation.getRealCashPrice());
		}else{
			otcOperation.setCashSettlementAmount(otcOperation.getRealCashAmount().multiply(otcOperation.getExchangeRate()));
			otcOperation.setCashSettlementPrice(otcOperation.getRealCashPrice().multiply(otcOperation.getExchangeRate()));
		}

		if(otcOperation.getIndTermSettlement().equals(BooleanType.YES.getCode())){
			otcOperation.setIndTermSettlementExchange(BooleanType.NO.getCode());
			otcOperation.setTermSettlementAmount(otcOperation.getTermAmount());
			otcOperation.setTermSettlementPrice(otcOperation.getTermPrice());
			
			if(!otcOperation.getCurrency().equals(otcOperation.getTermSettlementCurrency())){
				otcOperation.setTermSettlementAmount(otcOperation.getTermSettlementAmount().multiply(otcOperation.getExchangeRate()));
				otcOperation.setTermSettlementPrice(otcOperation.getTermSettlementPrice().multiply(otcOperation.getExchangeRate()));
			}

			otcOperation.setNominalValue(otcOperation.getSecurities().getCurrentNominalValue());
			otcOperation.getTradeOperation().setOperationState(OtcOperationStateType.REGISTERED.getCode());
		}

		/*
		 * VALIDAMOS SI LA OPERACION TIENE TODA LA INFORMACION NECESARIA PARA
		 * SER DIRECTAMENTE REVISADA
		 */
		if (MechanismOperationIsCompleted(otcOperation)) {
			otcOperation.getTradeOperation().setAprovalDate(CommonsUtilities.currentDateTime());
			otcOperation.getTradeOperation().setAprovalUser(otcOperation.getTradeOperation().getRegisterUser());
			otcOperation.getTradeOperation().setReviewDate(CommonsUtilities.currentDateTime());
			otcOperation.getTradeOperation().setReviewUser(otcOperation.getTradeOperation().getRegisterUser());
			otcOperation.getTradeOperation().setOperationState(OtcOperationStateType.REVIEWED.getCode());
			otcOperation.setOperationState(OtcOperationStateType.REVIEWED.getCode());// La solicitud pasara a ser revisada
		}else{
			otcOperation.setOperationState(OtcOperationStateType.REGISTERED.getCode());
		}
		// --------------------- VALIDAMOS SI ES REPO SECUNDARIO
		// -------------------------------------------------
		if (NegotiationUtils.mechanismOperationIsSecundaryReport(otcOperation)) {
			MechanismOperation originRepo = new MechanismOperation(otcOperation.getReferenceOperation().getIdMechanismOperationPk());
			otcOperation.setReferenceOperation(originRepo);
		}
		/** CREAMOS NUEVA OPERACION */
		create(otcOperation);
		//mechanismOperationAdj.setMechanismOperation(otcOperation);
		/*if(Validations.validateIsNotNullAndNotEmpty(mechanismOperationAdj)) {
			create(mechanismOperationAdj);
		}*/
		/** SI LA REFERENCIA ES NULL O SI ESTA VACIA */
		if (otcOperation.getReferenceNumber() == null || otcOperation.getReferenceNumber().isEmpty()) {
			otcOperation.setReferenceNumber(otcOperation.getIdMechanismOperationPk().toString());
		}

		/** INSERTAMOS LOS HECHOS DE MERCADO */
		/*AccountOperationMarketFact accountMarketFact = null;
		for (HolderAccountOperation accountOperation : otcOperation.getHolderAccountOperations()) {
			accountOperation.setSettlementAmount(accountOperation.getStockQuantity().multiply(otcOperation.getCashSettlementPrice()));
			for (AccountOperationMarketFact marketFact : accountOperation.getAccountOperationMarketFacts()) {
				if(accountOperation.getRole().equals(ComponentConstant.SALE_ROLE)){
					accountMarketFact =marketFact;
				}
				create(marketFact);
			}
		}*/

		/**By default assignment market fact in operation from account*/
		/*if(otcOperation.getMechanismOperationMarketFacts()!=null){
			for (MechanismOperationMarketFact operationMarkFact : otcOperation.getMechanismOperationMarketFacts()) {
				if (operationMarkFact.getMarketDate() != null) {
					operationMarkFact.setMarketDate(accountMarketFact.getMarketDate());
					operationMarkFact.setMarketRate(accountMarketFact.getMarketRate());
					operationMarkFact.setMarketPrice(accountMarketFact.getMarketPrice());
					operationMarkFact.setMechanismOperation(otcOperation);
					create(operationMarkFact);
				}
			}
		}*/
		return otcOperation;
	}

	/**
	 * Confirm otc operation service bean. a
	 * 
	 * @param mechanismOperationParam
	 *            the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException
	 *             the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public MechanismOperation confirmOtcMechanismOperation(MechanismOperation mechanismOperationParam) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());

		OtcOperationTO otcOperationTO = new OtcOperationTO();
		otcOperationTO.setIdMechanismOperation(mechanismOperationParam.getIdMechanismOperationPk());
		otcOperationTO.setNeedAccounts(true);

		/**GET OPERATION FROM BD*/
		MechanismOperation mechanismOperation = getOtcOperationServiceBean(otcOperationTO);
		/**Generate reference Number if this not exist*/
		if(mechanismOperationParam.getPaymentReference()==null){
			negotiationOperationServiceBean.generatePaymentReference(mechanismOperationParam);
		}
		/**If operation is primary placement*/
		if(mechanismOperation.getIndPrimaryPlacement() != null && mechanismOperation.getIndPrimaryPlacement().equals(BooleanType.YES.getCode())){
			Long participantCode = mechanismOperation.getSellerParticipant().getIdParticipantPk();
			String securityCode = mechanismOperation.getSecurities().getIdSecurityCodePk();
			Long operationCode= mechanismOperation.getIdMechanismOperationPk();
			BigDecimal quantit= mechanismOperation.getStockQuantity();
			securitiesRemoteService.get().addOperationPlacement(participantCode, securityCode, operationCode, quantit);
		}
		/**SET PAYMENT REFERENCE*/
		mechanismOperation.setPaymentReference(mechanismOperationParam.getPaymentReference());

		/**WHEN WE CONFIRM ON THE REVIEW*/
		if (mechanismOperation.getTradeOperation().getReviewUser() == null) {
			mechanismOperationParam.getTradeOperation().setReviewUser(mechanismOperationParam.getTradeOperation().getConfirmUser());
			mechanismOperationParam.getTradeOperation().setReviewDate(mechanismOperationParam.getTradeOperation().getConfirmDate());
		}

		/**IF OPERATION IS COMPLETE*/
		if (!MechanismOperationIsCompleted(mechanismOperation)) {
			if (OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperation, ParticipantRoleType.BUY.getCode())) {
				/**SI HAY ENCARGO  EL ENCARGADO EN COMPRA CONFIRMA LA SOLICITUD*/
				List<HolderAccountOperation> currentHoldAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperation,ParticipantRoleType.BUY.getCode());
				List<HolderAccountOperation> newHoldAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperationParam,ParticipantRoleType.BUY.getCode());
				/**SETEAMOS LA AUDITORIA*/
				for (HolderAccountOperation newHoldAccOperation : newHoldAccOperationList) {
					newHoldAccOperation.setAudit(loggerUser);
				}
				jump: 
				for (HolderAccountOperation currentHoldAccounOperation : currentHoldAccOperationList) {
					for (HolderAccountOperation newHoldAccOperation : newHoldAccOperationList) {
						if (newHoldAccOperation.equals(currentHoldAccounOperation)&& currentHoldAccounOperation.getHolderAccount() == null) {
							newHoldAccOperationList.remove(newHoldAccOperation);
							continue jump;
						}
					}
				}
				mechanismOperation.getHolderAccountOperations().removeAll(currentHoldAccOperationList);
				mechanismOperation.getHolderAccountOperations().addAll(newHoldAccOperationList);
			}
		}
		
		/**SI MANEJA HECHOS DE MERCADO*/
		/*if(depositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			Integer buyRole  = ParticipantRoleType.BUY.getCode();
			Integer cashPart = OperationPartType.CASH_PART.getCode();
			List<HolderAccountOperation> accountCashOperationsBuy  = OtcOperationUtils.getAccountOperations(mechanismOperation, buyRole, cashPart);
			
			if (mechanismOperation.getIndTermSettlement().equals(BooleanType.YES.getCode())) {
				//GENERAMOS LOS HECHOS DE MERCADO EN EL COMPRADOR
				for(HolderAccountOperation accountCashOperationBuy: accountCashOperationsBuy){
					accountCashOperationBuy.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
						AccountOperationMarketFact accountCashMarketFact = new AccountOperationMarketFact();
						accountCashMarketFact.setMarketDate(mechanismOperation.getOperationDate());
						accountCashMarketFact.setMarketRate(CommonsUtilities.getPercentOnDecimals(mechanismOperation.getAmountRate()));
						accountCashMarketFact.setMarketPrice(mechanismOperation.getCashPrice());
						accountCashMarketFact.setOperationQuantity(accountCashOperationBuy.getStockQuantity());
						accountCashMarketFact.setHolderAccountOperation(accountCashOperationBuy);
						accountCashOperationBuy.getAccountOperationMarketFacts().add(accountCashMarketFact);
				}
			}else{
				if(mechanismOperation.getMechanismOperationMarketFacts()!=null){
					for(MechanismOperationMarketFact operationMarketFact: mechanismOperation.getMechanismOperationMarketFacts()){
						for(HolderAccountOperation accountCashOperationBuy: accountCashOperationsBuy){
							AccountOperationMarketFact accountCashMarketFact = new AccountOperationMarketFact();
							accountCashMarketFact.setMarketDate(operationMarketFact.getMarketDate());
							accountCashMarketFact.setMarketRate(operationMarketFact.getMarketRate());
							accountCashMarketFact.setMarketPrice(operationMarketFact.getMarketPrice());
							accountCashMarketFact.setOperationQuantity(accountCashOperationBuy.getStockQuantity());
							accountCashMarketFact.setHolderAccountOperation(accountCashOperationBuy);
							accountCashOperationBuy.getAccountOperationMarketFacts().add(accountCashMarketFact);
						}
					}
				}
			}
		}*/

		/**SI LA MODALIDAD MANEJA PLAZO*/
		if (mechanismOperation.getMechanisnModality().getNegotiationModality().getIndTermSettlement().equals(BooleanType.YES.getCode())) {
			/**LIST TO HANDLE TERM ACCOUNT_OPERATION*/
			List<HolderAccountOperation> holdAccOperationListToTerm = new ArrayList<HolderAccountOperation>();
			/**GETTING ACCOUNT_OPERATION TO USE*/
			for (HolderAccountOperation holdAccNegotiations : mechanismOperation.getHolderAccountOperations()) {
				/**CREATE TERM ACCOUNT_OPERATION FOR MECHANISM_OPERATION*/
				HolderAccountOperation accountOperationTerm = new HolderAccountOperation();
				accountOperationTerm.setMechanismOperation(holdAccNegotiations.getMechanismOperation());
				/**NEW ACCOUNT_OPERATION ARE TO ADD  TERM ON OPERATION*/
				accountOperationTerm.setOperationPart(OperationPartType.TERM_PART.getCode());
				if (holdAccNegotiations.getRole().equals(ParticipantRoleType.BUY.getCode())) {
					accountOperationTerm.setRole(ParticipantRoleType.SELL.getCode());
				} else if (holdAccNegotiations.getRole().equals(ParticipantRoleType.SELL.getCode())) {
					accountOperationTerm.setRole(ParticipantRoleType.BUY.getCode());
				}
				accountOperationTerm.setHolderAccount(holdAccNegotiations.getHolderAccount());
				accountOperationTerm.setStockQuantity(holdAccNegotiations.getStockQuantity());
				accountOperationTerm.setHolderAccountState(holdAccNegotiations.getHolderAccountState());
				accountOperationTerm.setInchargeFundsParticipant(holdAccNegotiations.getInchargeFundsParticipant());
				accountOperationTerm.setInchargeStockParticipant(holdAccNegotiations.getInchargeStockParticipant());
				accountOperationTerm.setIndIncharge(holdAccNegotiations.getIndIncharge());
				accountOperationTerm.setCashAmount(holdAccNegotiations.getRealTermAmount());
				accountOperationTerm.setSettlementAmount(holdAccNegotiations.getStockQuantity().multiply(mechanismOperation.getTermSettlementPrice()));
				accountOperationTerm.setRegisterDate(CommonsUtilities.currentDateTime());
				accountOperationTerm.setRegisterUser(loggerUser.getUserName());
				accountOperationTerm.setAudit(loggerUser);
				/**ADD OBJECT REFERENCE TO OBJECT FROM BD*/
				accountOperationTerm.setRefAccountOperation(holdAccNegotiations);
				accountOperationTerm.setConfirmDate(CommonsUtilities.currentDateTime());
				
				/**SI MANEJA HECHOS DE MERCADO*/
				/*if(depositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					accountOperationTerm.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
					for(AccountOperationMarketFact accountCashMarkFact: holdAccNegotiations.getAccountOperationMarketFacts()){
						AccountOperationMarketFact accountTermMarkFact = new AccountOperationMarketFact();
						accountTermMarkFact.setMarketDate(accountCashMarkFact.getMarketDate());
						accountTermMarkFact.setMarketRate(accountCashMarkFact.getMarketRate());
						accountTermMarkFact.setMarketPrice(accountCashMarkFact.getMarketPrice());
						accountTermMarkFact.setOperationQuantity(accountCashMarkFact.getOperationQuantity());
						accountTermMarkFact.setHolderAccountOperation(accountOperationTerm);
						accountOperationTerm.getAccountOperationMarketFacts().add(accountTermMarkFact);
					}
				}*/
				/**ADD ACCOUNT TERM ON ACCOUNT_OPERATION*/
				holdAccOperationListToTerm.add(accountOperationTerm);
			}
			/**ADD TERM ACCOUNT_OPERATION HAVING CASH ACCOUNT_OPERATION ON MECHANISM_OPERATION*/
			mechanismOperation.getHolderAccountOperations().addAll(holdAccOperationListToTerm);
		}

		/**VALIDAMOS SI ES PERMUTA*/
		SwapOperation swapOperation = null;
		if (NegotiationUtils.mechanismOperationIsSwap(mechanismOperation)) {
			swapOperation = mechanismOperation.getSwapOperations().get(0);

			List<HolderAccountOperation> accountOperationList = new ArrayList<HolderAccountOperation>();
			accountOperationList.addAll(mechanismOperation.getHolderAccountOperations());
			accountOperationList.addAll(swapOperation.getHolderAccountOperations());

			for (HolderAccountOperation accountOperation : accountOperationList) {
				HolderAccountOperation holderAccOperation = OtcSwapOperationUtils.getAccountOperation(accountOperation,mechanismOperation);
				holderAccOperation.setHolderAccount(accountOperation.getHolderAccount());
				holderAccOperation.setIndAutomaticAsignment(accountOperation.getIndAutomaticAsignment());
				holderAccOperation.setHolderAccountState(accountOperation.getHolderAccountState());
				holderAccOperation.setOperationPart(accountOperation.getOperationPart());
				//holderAccOperation.setIndUnfulfilled(accountOperation.getIndUnfulfilled());
				holderAccOperation.setInchargeFundsParticipant(accountOperation.getInchargeFundsParticipant());
				holderAccOperation.setInchargeStockParticipant(accountOperation.getInchargeStockParticipant());
				holderAccOperation.setInchargeState(accountOperation.getInchargeState());
				holderAccOperation.setAssignmentRequest(accountOperation.getAssignmentRequest());
				holderAccOperation.setIndIncharge(accountOperation.getIndIncharge());
				holderAccOperation.setAudit(loggerUser);

				if (accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())) {
					holderAccOperation.setRole(ParticipantRoleType.BUY.getCode());
				} else {
					holderAccOperation.setRole(ParticipantRoleType.SELL.getCode());
				}

				if (accountOperation.getSwapOperation() != null) {
					holderAccOperation.setMechanismOperation(mechanismOperation);
					mechanismOperation.getHolderAccountOperations().add(holderAccOperation);
				} else if (accountOperation.getMechanismOperation() != null) {
					holderAccOperation.setSwapOperation(swapOperation);
					swapOperation.getHolderAccountOperations().add(holderAccOperation);
				}
			}
		}

		/**CREAMOS LA LISTA DE PARTICIPANT_OPERATIONS*/
		List<ParticipantOperation> participantOperationList = new ArrayList<ParticipantOperation>();
		/**CREAMOS LA LISTA DE PARTICIPANT_OPERATIONS PARA PERMUTA*/
		List<ParticipantOperation> participantOperationSwapList = new ArrayList<ParticipantOperation>();
		List<HolderAccountOperation> holderAccountOperationFinded = new ArrayList<HolderAccountOperation>();
		holderAccountOperationFinded.addAll(mechanismOperation.getHolderAccountOperations());

		/**SI ES PERMUTA NECESITAMOS ASIGNAR LAS CUENTAS EN LA OPERACION*/
		if (NegotiationUtils.mechanismOperationIsSwap(mechanismOperation)) {
			/**AGREGAMOS LAS CUENTAS DE LA PERMUTA DENTRO DE LA OPERACION*/
			holderAccountOperationFinded.addAll(swapOperation.getHolderAccountOperations());
		}

		for (HolderAccountOperation holdAccNegotiations : holderAccountOperationFinded) {
			/**OBTENEMOS CUENTAS COMPRADORAS Y VENDEDORAS EN otcOperationOriginRequestTypesList"*/
			List<InChargeNegotiationType> chargeNegotiationTypeList = InChargeNegotiationType.listSomeElements(InChargeNegotiationType.INCHARGE_STOCK,InChargeNegotiationType.INCHARGE_FUNDS);
			for (InChargeNegotiationType inChargeNegotiationType : chargeNegotiationTypeList) {
				/**CREAMOS EL PARTICIPANT_OPERATION*/
				ParticipantOperation participantOperation = new ParticipantOperation();

				participantOperation.setInchargeType(inChargeNegotiationType.getCode());
				/**SI ES ENCARGADO DE FONDOS*/
				if (inChargeNegotiationType.equals(InChargeNegotiationType.INCHARGE_FUNDS)) {
					/**SE ASIGNA EL ENCARGADO DE FONDOS*/
					participantOperation.setParticipant(holdAccNegotiations.getInchargeFundsParticipant());
					participantOperation.setInchargeType(InChargeNegotiationType.INCHARGE_FUNDS.getCode());
				} else if (inChargeNegotiationType.equals(InChargeNegotiationType.INCHARGE_STOCK)) {/**SI ES ENCARGADO DE VALORES*/
					/**ASIGNAMOS EL ENCARGADO*/
					participantOperation.setParticipant(holdAccNegotiations.getInchargeStockParticipant());
					participantOperation.setInchargeType(InChargeNegotiationType.INCHARGE_STOCK.getCode());

					/**SI ES COLOCACION PRIMARIA Y VENTA*/
					if (mechanismOperation.getIndPrimaryPlacement().equals(BooleanType.YES.getCode()) && 
						ParticipantRoleType.SELL.getCode().equals(participantOperation.getRole())) {
						/**INDICAMOS EL INDICADOR DE VALORES CONFORMES*/
						participantOperation.setStockReference(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
					}
				}
				participantOperation.setIndIncharge(holdAccNegotiations.getIndIncharge());
				if (holdAccNegotiations.getIndIncharge() != null && holdAccNegotiations.getIndIncharge().equals(BooleanType.YES.getCode())) {
					participantOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
				}
				participantOperation.setOperationPart(holdAccNegotiations.getOperationPart());
				participantOperation.setRole(holdAccNegotiations.getRole());
				participantOperation.setStockQuantity(holdAccNegotiations.getStockQuantity());
				participantOperation.setCashAmount(holdAccNegotiations.getCashAmount());
				participantOperation.setAudit(loggerUser);
				// TODO
				participantOperation.setBlockedStock(BigDecimal.ZERO);
				participantOperation.setCurrency(mechanismOperation.getCurrency());
				participantOperation.setDepositedAmount(BigDecimal.ZERO);

				if (holdAccNegotiations.getMechanismOperation() != null) {
					participantOperation.setMechanismOperation(mechanismOperation);
					participantOperationList.add(participantOperation);
				} else if (holdAccNegotiations.getSwapOperation() != null) {
					participantOperation.setSwapOperation(holdAccNegotiations.getSwapOperation());
					participantOperationSwapList.add(participantOperation);
				}
			}
		}
		if (NegotiationUtils.mechanismOperationIsSwap(mechanismOperation)) {
			List<ParticipantOperation> partGroupByAmounts = getParticipantOperationGroupByAmountsList(participantOperationSwapList);
			swapOperation.setParticipantOperations(partGroupByAmounts);
		}

		mechanismOperation.setOperationDate(CommonsUtilities.currentDateTime());
		mechanismOperation.setOperationNumber(getCurrentOperationNumber(mechanismOperation));
		mechanismOperation.setOperationState(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		mechanismOperation.getTradeOperation().setOperationState(OtcOperationStateType.CONFIRMED.getCode());
		mechanismOperation.getTradeOperation().setConfirmDate(CommonsUtilities.currentDateTime());
		mechanismOperation.getTradeOperation().setConfirmUser(loggerUser.getUserName());

		for (HolderAccountOperation accountOperation : mechanismOperation.getHolderAccountOperations()) {
			accountOperation.setConfirmDate(CommonsUtilities.currentDateTime());
			if (accountOperation.getIndIncharge().equals(BooleanType.YES.getCode())) {
				accountOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
			}
		}

		if (mechanismOperation.getIndMarginGuarantee().equals(GeneralConstants.ONE_VALUE_INTEGER)) {
			negotiationOperationServiceBean.calculateCoverageAmount(mechanismOperation.getIdMechanismOperationPk(), loggerUser);
		}
		
		/**SI MANEJA HECHOS DE MERCADO*/
		/*if(depositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			Integer sellRole = ParticipantRoleType.SELL.getCode();
			Integer buyRole  = ParticipantRoleType.BUY.getCode();
			Integer cashPart = OperationPartType.CASH_PART.getCode();
			Integer termPart = OperationPartType.TERM_PART.getCode();
			//GETTING ACCOUNT_MARKETFACT_OPERATION PENDIENT OF REGISTER
			List<HolderAccountOperation> accountCashOperationsBuy  = OtcOperationUtils.getAccountOperations(mechanismOperation, buyRole, cashPart);
			List<HolderAccountOperation> accountTermOperationsSell  = OtcOperationUtils.getAccountOperations(mechanismOperation, sellRole, termPart);
			List<HolderAccountOperation> accountTermOperationsBuy  = OtcOperationUtils.getAccountOperations(mechanismOperation, buyRole, termPart);
			List<HolderAccountOperation> accountOperationWithOutMarkFact = new ArrayList<HolderAccountOperation>();
			//PUT ALL ACCOUNT_OPERATION WITHOUT MARKETFACT IN ONE LIST
			accountOperationWithOutMarkFact.addAll(accountCashOperationsBuy);
			accountOperationWithOutMarkFact.addAll(accountTermOperationsSell);
			accountOperationWithOutMarkFact.addAll(accountTermOperationsBuy);
			//ITERATE ALL ACCOUNT_OPERATION WITHOUT MARKET_FACT AND CREATE THEM
			for(HolderAccountOperation accountOperation: accountOperationWithOutMarkFact){
				for(AccountOperationMarketFact accountMarkFact: accountOperation.getAccountOperationMarketFacts()){
					create(accountMarkFact);
				}
			}
		}*/
		
		Long operationID = mechanismOperation.getIdMechanismOperationPk();
		Map<String,SettlementAccountOperation> settAccountOperationMap = new HashMap<String,SettlementAccountOperation>();
		/**Config Settlement Operation*/
		SettlementOperation cashSettlementOperation = settlementService.populateSettlementOperation(mechanismOperation,ComponentConstant.CASH_PART);
		/**If operation it's primary placement*/
		if(mechanismOperation.getIndPrimaryPlacement().equals(BooleanType.YES.getCode())){
			//only for PRIMARY PLACEMENTS modalities with set the stock reference by default
			cashSettlementOperation.setStockReference(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
			
			// validate if bank shares are in primary placement
			Object[] issuerDetail= settlementService.getIssuerDetails(mechanismOperation.getSecurities().getIdSecurityCodePk());
			Integer objEconomicActivity = (Integer)(issuerDetail[3] == null ? ComponentConstant.ZERO : issuerDetail[3]);
			Integer objSecurityClass = (Integer)(issuerDetail[4] == null ? ComponentConstant.ZERO : issuerDetail[4]);
			
			if(ComponentConstant.ONE.equals(objEconomicActivity) && ComponentConstant.ONE.equals(objSecurityClass)){
				cashSettlementOperation.setIndDematerialization(ComponentConstant.ONE);
			}
		}

		if(mechanismOperation.getIndPrimaryPlacement().equals(BooleanType.YES.getCode())){
			cashSettlementOperation.setStockReference(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
			cashSettlementOperation.setStockBlockDate(CommonsUtilities.currentDateTime());
		}
		create(cashSettlementOperation);
		saveAll(cashSettlementOperation.getParticipantSettlements());
		/**Iterate Cash account*/
		List<HolderAccountOperation> holderAccountCash = OtcOperationUtils.getAccountsByRole(mechanismOperation, ComponentConstant.CASH_PART);
		for(HolderAccountOperation accountOperation: holderAccountCash){
			SettlementAccountOperation settAccount = settlementService.populateSettlementAccountOperation(operationID, accountOperation, null);
			if(mechanismOperation.getIndPrimaryPlacement().equals(BooleanType.YES.getCode()) && accountOperation.getRole().equals(ComponentConstant.SALE_ROLE)){
				settAccount.setStockReference(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode());
				settAccount.setStockBlockDate(CommonsUtilities.currentDateTime());
			}
			create(settAccount);
			for(AccountOperationMarketFact accountMarketFact: accountOperation.getAccountOperationMarketFacts()){
				SettlementAccountMarketfact settMarkFact = settlementService.populateSettlementAccountMarketFact(accountMarketFact, settAccount);
				create(settMarkFact);
			}
			/**Config Map to use on term Part*/
			Long participantId = accountOperation.getInchargeStockParticipant().getIdParticipantPk();
			Long accountId     = accountOperation.getHolderAccount().getIdHolderAccountPk();
			settAccountOperationMap.put(participantId.toString().concat(accountId.toString()), settAccount);
		}
		
		/**Verified if exist term part*/
		if(mechanismOperation.getIndTermSettlement().equals(BooleanType.YES.getCode())){
			SettlementOperation termSettlementOperation = settlementService.populateSettlementOperation(mechanismOperation,ComponentConstant.TERM_PART);
			create(termSettlementOperation);
			saveAll(termSettlementOperation.getParticipantSettlements());
			/**Iterate Term Acount*/
			List<HolderAccountOperation> holderAccountTerm = OtcOperationUtils.getAccountsByRole(mechanismOperation, ComponentConstant.TERM_PART);
			for(HolderAccountOperation accountOperation: holderAccountTerm){
				/**Config variable to read Map*/
				Long participantId = accountOperation.getInchargeStockParticipant().getIdParticipantPk();
				Long accountId     = accountOperation.getHolderAccount().getIdHolderAccountPk();
				SettlementAccountOperation settCashAccount = settAccountOperationMap.get(participantId.toString().concat(accountId.toString()));
				/**If it's null exists some error*/
				if(settCashAccount==null){
					throw new ServiceException();
				}
				/**Create SettlementAccountOperation on term Part*/
				SettlementAccountOperation settTermAccount = settlementService.populateSettlementAccountOperation(operationID, accountOperation, settCashAccount);
				create(settTermAccount);
				/**Create MarketFact*/
				if(Validations.validateListIsNotNullAndNotEmpty(accountOperation.getAccountOperationMarketFacts())) {
					for(AccountOperationMarketFact accountMarketFact: accountOperation.getAccountOperationMarketFacts()){
						SettlementAccountMarketfact settMarkFact = settlementService.populateSettlementAccountMarketFact(accountMarketFact, settTermAccount);
						create(settMarkFact);
					}
				}
			}
		}		

		update(mechanismOperation);
		return mechanismOperation;
	}

	/**
	 * Gets the participant operation list group by participant.
	 * 
	 * @param participantList
	 *            the participant list
	 * @return the participant operation list group by participant
	 */
	public List<ParticipantOperation> getParticipantOperationGroupByAmountsList(List<ParticipantOperation> participantList) {
		List<ParticipantOperation> partOperationReturn = new ArrayList<ParticipantOperation>();
		for (ParticipantOperation participantOperation : participantList) {
			Boolean existOnReturnList = Boolean.FALSE;
			BigDecimal stockQuantity = BigDecimal.ZERO;
			BigDecimal cashAmount = BigDecimal.ZERO;
			ParticipantOperation participantOperationToSave = participantOperation;
			for (ParticipantOperation partiOperation : partOperationReturn) {
				if (participantOperation.getParticipant().getIdParticipantPk().equals(partiOperation.getParticipant().getIdParticipantPk())) {
					if (participantOperation.getRole().equals(partiOperation.getRole())) {
						if (participantOperation.getOperationPart().equals(partiOperation.getOperationPart())) {
							if (participantOperation.getInchargeType().equals(partiOperation.getInchargeType())) {
								if (participantOperation.getIndIncharge().equals(partiOperation.getIndIncharge())) {
									existOnReturnList = Boolean.TRUE;
								}
							}
						}
					}
				}
			}
			if (!existOnReturnList) {
				stockQuantity = BigDecimal.ZERO;
				for (ParticipantOperation partOperationGroup : participantList) {
					if (participantOperation.getParticipant().getIdParticipantPk().equals(partOperationGroup.getParticipant().getIdParticipantPk())) {
						if (participantOperation.getRole().equals(partOperationGroup.getRole())) {
							if (participantOperation.getOperationPart().equals(partOperationGroup.getOperationPart())) {
								if (participantOperation.getInchargeType().equals(partOperationGroup.getInchargeType())) {
									if (participantOperation.getIndIncharge().equals(partOperationGroup.getIndIncharge())) {
										stockQuantity = stockQuantity.add(partOperationGroup.getStockQuantity());
										cashAmount = cashAmount.add(partOperationGroup.getCashAmount());
									}
								}
							}
						}
					}
				}
				participantOperationToSave.setStockQuantity(stockQuantity);
				participantOperationToSave.setCashAmount(cashAmount);
				partOperationReturn.add(participantOperationToSave);
			}
		}
		return partOperationReturn;
	}

	/**
	 * Approve otc opereation service bean.
	 * 
	 * @param otcMechanismOperation
	 *            the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException
	 *             the service exception
	 */
	public MechanismOperation approveOtcOpereationServiceBean(MechanismOperation otcMechanismOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);// Otenemos
																	// el Logg
																	// de la
																	// transaccion
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		OtcOperationTO otcOperationTO = new OtcOperationTO();
		otcOperationTO.setIdMechanismOperation(otcMechanismOperation.getIdMechanismOperationPk());
		otcOperationTO.setNeedAccounts(true);

		MechanismOperation mechanismOperation = getOtcOperationServiceBean(otcOperationTO);// Obtenemos
																							// La
																							// Operacion
																							// desde
																							// La
																							// BD
		mechanismOperation.getTradeOperation().setAprovalDate(CommonsUtilities.currentDateTime());
		mechanismOperation.getTradeOperation().setAprovalUser(otcMechanismOperation.getTradeOperation().getAprovalUser());
		mechanismOperation.setOperationState(OtcOperationStateType.APROVED.getCode());
		mechanismOperation.getTradeOperation().setOperationState(OtcOperationStateType.APROVED.getCode());

		if (OtcOperationUtils.otcOperationIsCrusade(mechanismOperation)) {// Si
																			// la
																			// operacion
																			// es
																			// cruzada
			mechanismOperation.getTradeOperation().setReviewDate(CommonsUtilities.currentDateTime());
			mechanismOperation.getTradeOperation().setReviewUser(otcMechanismOperation.getTradeOperation().getAprovalUser());
			mechanismOperation.setOperationState(OtcOperationStateType.REVIEWED.getCode());
			mechanismOperation.getTradeOperation().setOperationState(OtcOperationStateType.REVIEWED.getCode());
		}
		if (OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperation,ParticipantRoleType.SELL.getCode())) {// Si la Aprobacion se da
														// en el encargo de la
														// venta
			// Si tiene encargo, siempre el participante encargado de venta sera
			// el que aprueba la solcitud
			List<HolderAccountOperation> currentHoldAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperation,ParticipantRoleType.SELL.getCode());
			List<HolderAccountOperation> newHoldAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(otcMechanismOperation,ParticipantRoleType.SELL.getCode());
			// Seteamos el track de auditoria a los nuevos registros agregados
			for (HolderAccountOperation newHoldAccOperation : newHoldAccOperationList) {
				newHoldAccOperation.setAudit(loggerUser);
			}
			jump: for (HolderAccountOperation currentHoldAccounOperation : currentHoldAccOperationList) {
				for (HolderAccountOperation newHoldAccOperation : newHoldAccOperationList) {
					// Eliminamos la cuenta encargada sin Cuenta titular
					if (newHoldAccOperation.equals(currentHoldAccounOperation)
							&& currentHoldAccounOperation.getHolderAccount() == null) {
						newHoldAccOperationList.remove(newHoldAccOperation);
						continue jump;// hacemos un salto al primer For
					}
				}
			}
			mechanismOperation.getHolderAccountOperations().removeAll(
					currentHoldAccOperationList);
			mechanismOperation.getHolderAccountOperations().addAll(
					newHoldAccOperationList);
		}

		update(mechanismOperation);
		return mechanismOperation;
	}
	
	public MechanismOperation authorizeOtcOperationServiceBean(MechanismOperation otcMechanismOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);// Otenemos
																	// el Logg
																	// de la
																	// transaccion
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		OtcOperationTO otcOperationTO = new OtcOperationTO();
		otcOperationTO.setIdMechanismOperation(otcMechanismOperation.getIdMechanismOperationPk());
		otcOperationTO.setNeedAccounts(true);

		MechanismOperation mechanismOperation = getOtcOperationServiceBean(otcOperationTO);// Obtenemos
																							// La
																							// Operacion
																							// desde
																							// La
																							// BD
		mechanismOperation.getTradeOperation().setAuthorizeDate(CommonsUtilities.currentDateTime());
		mechanismOperation.getTradeOperation().setAuthorizeUser(otcMechanismOperation.getTradeOperation().getAprovalUser());
		mechanismOperation.setOperationState(OtcOperationStateType.AUTHORIZED.getCode());
		mechanismOperation.getTradeOperation().setOperationState(OtcOperationStateType.AUTHORIZED.getCode());

		if (OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperation,ParticipantRoleType.SELL.getCode())) {// Si la Aprobacion se da
														// en el encargo de la
														// venta
			// Si tiene encargo, siempre el participante encargado de compra sera
			// el que autorize la solcitud
			List<HolderAccountOperation> currentHoldAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperation,ParticipantRoleType.BUY.getCode());
			List<HolderAccountOperation> newHoldAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(otcMechanismOperation,ParticipantRoleType.BUY.getCode());
			// Seteamos el track de auditoria a los nuevos registros agregados
			for (HolderAccountOperation newHoldAccOperation : newHoldAccOperationList) {
				newHoldAccOperation.setAudit(loggerUser);
			}
			jump: for (HolderAccountOperation currentHoldAccounOperation : currentHoldAccOperationList) {
				for (HolderAccountOperation newHoldAccOperation : newHoldAccOperationList) {
					// Eliminamos la cuenta encargada sin Cuenta titular
					if (newHoldAccOperation.equals(currentHoldAccounOperation)
							&& currentHoldAccounOperation.getHolderAccount() == null) {
						newHoldAccOperationList.remove(newHoldAccOperation);
						continue jump;// hacemos un salto al primer For
					}
				}
			}
			mechanismOperation.getHolderAccountOperations().removeAll(
					currentHoldAccOperationList);
			mechanismOperation.getHolderAccountOperations().addAll(
					newHoldAccOperationList);
		}

		update(mechanismOperation);
		return mechanismOperation;
	}

	/**
	 * Review otc opereation service bean.
	 * 
	 * @param otcMechanismOperation
	 *            the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException
	 *             the service exception
	 */
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public MechanismOperation reviewOtcOpereationServiceBean(MechanismOperation otcMechanismOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);// Otenemos el Logg de la transaccion
		negotiationOperationServiceBean.validateMechanismOperation(otcMechanismOperation);// Validamos el objeto otc

		OtcOperationTO otcOperationTO = new OtcOperationTO();
		otcOperationTO.setIdMechanismOperation(otcMechanismOperation.getIdMechanismOperationPk());
		otcOperationTO.setNeedAccounts(true);

		MechanismOperation mechanismOperation = getOtcOperationServiceBean(otcOperationTO);// Obtenemos La Operacion desde La BD
		mechanismOperation.getTradeOperation().setReviewDate(CommonsUtilities.currentDateTime());
		mechanismOperation.getTradeOperation().setReviewUser(otcMechanismOperation.getTradeOperation().getReviewUser());
		mechanismOperation.setOperationState(OtcOperationStateType.REVIEWED.getCode());
		mechanismOperation.getTradeOperation().setOperationState(OtcOperationStateType.REVIEWED.getCode());

		List<HolderAccountOperation> holdAccOperationList = OtcOperationUtils.getHoldAccOperationsByNegotiatior(otcMechanismOperation,ParticipantRoleType.BUY.getCode());// Traemos las cuentas ingresadas
		for (HolderAccountOperation holderAccountOperation : holdAccOperationList) {
			mechanismOperation.getHolderAccountOperations().add(holderAccountOperation);// Seteamos las cuentas de encargo ingresadas
			if(holderAccountOperation.getRegisterUser() == null){
				holderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
				holderAccountOperation.setRegisterUser(loggerUser.getUserName());
			}
			holderAccountOperation.setAudit(loggerUser);
		}

		if (NegotiationUtils.mechanismOperationIsSwap(otcMechanismOperation)) {// Si la revision es de una permuta
			SwapOperation swapOperation = mechanismOperation.getSwapOperations().get(0);// obtenemos el swap de la operacion
			SwapOperation swapOperationNew = otcMechanismOperation.getSwapOperations().get(0);// Obtenemos el swap la operacion revisada
			swapOperation.setHolderAccountOperations(swapOperationNew.getHolderAccountOperations());// agregamos las cuentas de la operacion revisada,hacia la operacion otc
			for (HolderAccountOperation holdAccountOperation : swapOperation.getHolderAccountOperations()) {
				holdAccountOperation.setAudit(loggerUser);
			}
			update(swapOperation);// Actualizamos el objeto swap operation
		}

		update(mechanismOperation);
		return mechanismOperation;
	}

	/**
	 * Anulate otc operation service bean.
	 * 
	 * @param otcMechanismOperation
	 *            the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException
	 *             the service exception
	 */
	public MechanismOperation anulateOtcOperationServiceBean(
			MechanismOperation otcMechanismOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry
				.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction()
				.getIdPrivilegeAnnular());
		OtcOperationTO otcOperationTO = new OtcOperationTO();
		otcOperationTO.setIdMechanismOperation(otcMechanismOperation
				.getIdMechanismOperationPk());
		otcOperationTO.setNeedAccounts(true);

		MechanismOperation mechanismOperation = getOtcOperationServiceBean(otcOperationTO);// Obtenemos
																							// La
																							// Operacion
																							// desde
																							// La
																							// BD
		mechanismOperation.getTradeOperation().setRejectUser(
				otcMechanismOperation.getTradeOperation().getRejectUser());
		mechanismOperation.getTradeOperation().setRejectDate(new Date());
		mechanismOperation.setOperationState(OtcOperationStateType.ANULATE
				.getCode());
		mechanismOperation.getTradeOperation().setOperationState(
				OtcOperationStateType.ANULATE.getCode());

		if (NegotiationUtils
				.mechanismOperationIsPrimaryPlacement(otcMechanismOperation)) {// Si
																				// es
																				// colocacion
																				// primaria
//			negotiationOperationServiceBean
//					.cancelOperationPrimaryPlacement(otcMechanismOperation);// Anulamos
//																			// los
//																			// montos
//																			// en
//																			// solicitudes
//																			// de
//																			// colocacion
//																			// primaria
		}
		update(mechanismOperation);
		return mechanismOperation;
	}

	/**
	 * Anulate otc operation service bean.
	 * 
	 * @param otcMechanismOperation
	 *            the otc mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException
	 *             the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public MechanismOperation rejectOtcOperationServiceBean(
			MechanismOperation otcMechanismOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry
				.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction()
				.getIdPrivilegeReject());
		OtcOperationTO otcOperationTO = new OtcOperationTO();
		otcOperationTO.setIdMechanismOperation(otcMechanismOperation
				.getIdMechanismOperationPk());
		otcOperationTO.setNeedAccounts(true);

		MechanismOperation mechanismOperation = getOtcOperationServiceBean(otcOperationTO);// Obtenemos
																							// La
																							// Operacion
																							// desde
																							// La
																							// BD
		mechanismOperation.getTradeOperation().setRejectUser(
				otcMechanismOperation.getTradeOperation().getRejectUser());
		mechanismOperation.getTradeOperation().setRejectDate(new Date());
		mechanismOperation.setOperationState(OtcOperationStateType.REJECTED
				.getCode());
		mechanismOperation.getTradeOperation().setOperationState(
				OtcOperationStateType.REJECTED.getCode());

		if (NegotiationUtils
				.mechanismOperationIsPrimaryPlacement(otcMechanismOperation)) {// Si
																				// es
																				// colocacion
																				// primaria
//			negotiationOperationServiceBean
//					.cancelOperationPrimaryPlacement(otcMechanismOperation);// Anulamos
//																			// los
//																			// montos
//																			// en
//																			// solicitudes
//																			// de
//																			// colocacion
//																			// primaria
		}
		update(mechanismOperation);
		return mechanismOperation;
	}

	/**
	 * Cancel otc operation service bean.
	 * 
	 * @param otcMechanismOperation
	 *            the otc mechanism operation
	 * @param operationPart
	 *            the operation part
	 * @return the mechanism operation
	 * @throws ServiceException
	 *             the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public MechanismOperation cancelOtcOperationServiceBean(MechanismOperation otcMechanismOperation, Integer operationPart)throws ServiceException {

		MechanismOperation mechanismOperation = find(MechanismOperation.class,otcMechanismOperation.getIdMechanismOperationPk());

		switch (OperationPartType.get(operationPart)) {
			case CASH_PART:
				if (!mechanismOperation.getOperationState().equals(MechanismOperationStateType.ASSIGNED_STATE.getCode())) {
					throw new ServiceException(ErrorServiceType.OTC_OPERATION_STATE);
				}
				mechanismOperation.setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
				break;
			case TERM_PART:
				if (!mechanismOperation.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode())) {
					throw new ServiceException(ErrorServiceType.OTC_OPERATION_STATE);
				}
				mechanismOperation.setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
				break;
		}

		mechanismOperation.getTradeOperation().setOperationState(mechanismOperation.getOperationState());
		mechanismOperation.getTradeOperation().setRejectUser(otcMechanismOperation.getTradeOperation().getRejectUser());
		mechanismOperation.getTradeOperation().setRejectDate(new Date());
		mechanismOperation.setCancelMotive(otcMechanismOperation.getCancelMotive());
		mechanismOperation.setCancelMotiveOther(otcMechanismOperation.getCancelMotiveOther());

		if (NegotiationUtils.mechanismOperationIsPrimaryPlacement(otcMechanismOperation)) {
//			negotiationOperationServiceBean
//					.cancelOperationPrimaryPlacement(otcMechanismOperation);// Anulamos
//																			// los
//																			// montos
//																			// en
//																			// solicitudes
//																			// de
//																			// colocacion
//																			// primaria
		}
		update(mechanismOperation);
		return otcMechanismOperation;
	}

	/**
	 * Gets the current operation number.
	 * 
	 * @param otcMechanismOperation
	 *            the otc mechanism operation
	 * @return the current operation number
	 */
	public Long getCurrentOperationNumber(MechanismOperation otcMechanismOperation) {
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT MAX(OPERATION_NUMBER) FROM mechanism_operation "
				+ "WHERE "
				+ "ID_NEGOTIATION_MECHANISM_FK = :idNegMechanParam AND "
				+ "ID_NEGOTIATION_MODALITY_FK = :idNegModalParam AND "
				+ "TO_CHAR(OPERATION_DATE,'DD/MM/YYYY') = TO_CHAR(:currentDateParam,'DD/MM/YYYY')");

		Query queryJpa = em.createNativeQuery(querySql.toString());
		queryJpa.setParameter("idNegMechanParam", otcMechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk());
		queryJpa.setParameter("idNegModalParam", otcMechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		queryJpa.setParameter("currentDateParam",CommonsUtilities.currentDate());
		Long operationNumber = null;
		try {
			Object result = queryJpa.getSingleResult();
			if (result != null) {
				operationNumber = ((BigDecimal) queryJpa.getSingleResult()).longValue();
				operationNumber = operationNumber + 1;
			} else {
				operationNumber = 1L;
			}
		} catch (NoResultException ex) {
			return 1L;
		}
		return operationNumber;
	}
	
	/**
	 * Placement security detail.
	 *
	 * @param placementCode the placement code
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment placementSecurityDetail(Long placementCode, Long participantCode, String securityCode) throws ServiceException{
		StringBuilder stringSql = new StringBuilder();
		stringSql.append("Select plac From PlacementSegment plac										");
		stringSql.append("Inner Join Fetch plac.placementSegmentDetails placDet							");
		stringSql.append("Inner Join plac.placementSegParticipaStructs struct							");
		stringSql.append("Where placDet.security.idSecurityCodePk = :securityCode	   AND				");
		stringSql.append("		struct.participant.idParticipantPk = :participantCode  AND				");
		stringSql.append("		plac.idPlacementSegmentPk = :placementCode								");
		Query query = em.createQuery(stringSql.toString());
		query.setParameter("securityCode", securityCode);
		query.setParameter("participantCode", participantCode);
		query.setParameter("placementCode", placementCode);
		return (PlacementSegment) query.getSingleResult();
	}
	
	/**
	 * Gets the placement security account.
	 *
	 * @param placementCode the placement code
	 * @param participantCode the participant code
	 * @return the placement security account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getPlacementSecurityAccount(Long placementCode, Long participantCode) throws ServiceException{
		StringBuilder stringSql = new StringBuilder();
		stringSql.append("Select account From PlacementSegment plac										");
		stringSql.append("Inner Join plac.placementSegParticipaStructs struct							");
		stringSql.append("Inner Join struct.holderAccount account										");
		stringSql.append("Inner Join Fetch account.holderAccountDetails details							");
		stringSql.append("Inner Join Fetch details.holder hold											");
		stringSql.append("Where struct.participant.idParticipantPk = :participantCode  AND				");
		stringSql.append("		plac.idPlacementSegmentPk = :placementCode								");
		
		Query query = em.createQuery(stringSql.toString());
		query.setParameter("participantCode", participantCode);
		query.setParameter("placementCode", placementCode);
		try{
			return  (HolderAccount) query.getSingleResult();
		}catch(NoResultException ex){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_PRIMARYPLAC_ACCOUNT_NULL);
		}
		
	}

	/**
	 * Gets the placement segment on issuance service bean.
	 * 
	 * @param otcOperationTO
	 *            the otc operation to
	 * @return the placement segment on issuance service bean
	 */
	public PlacementSegment getPlacementSegmentOnIssuanceServiceBean(OtcOperationTO otcOperationTO) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringSql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		stringSql.append("Select new com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment(									");
		stringSql.append("plac.idPlacementSegmentPk, plac.amountToPlace, plac.placedAmount, plac.placementTerm, plac.placementSegmentState,	");
		stringSql.append("plac.trancheNumber)																								");
		stringSql.append("From PlacementSegment plac																						");
		whereSql.append("Where plac.issuance.idIssuanceCodePk = :issuanceCode	And plac.placementSegmentState = :placState					");

		parameters.put("issuanceCode", otcOperationTO.getIdIssuancePk());
		parameters.put("placState", PlacementSegmentStateType.OPENED.getCode());
		if (otcOperationTO.getIdSecurityCodePk() != null) {
			stringSql.append("Inner Join plac.placementSegmentDetails det			");
			whereSql.append("And 	det.security.idSecurityCodePk = :securityCode	");
			parameters.put("securityCode", otcOperationTO.getIdSecurityCodePk());
		}
		if (otcOperationTO.getIdParticipantPlacement() != null) {
			stringSql.append("Inner Join plac.placementSegParticipaStructs part	");
			whereSql.append("And 	part.participant.idParticipantPk = :participantCode	");
			parameters.put("participantCode",otcOperationTO.getIdParticipantPlacement());
		}

		PlacementSegment placSegmentTemp = null;
		try {
			placSegmentTemp = (PlacementSegment) findObjectByQueryString(stringSql.toString().concat(whereSql.toString()), parameters);
			return placSegmentTemp;
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * Gets the mechanism operation reference service bean. metodo para obtener
	 * las modalidades que estan referenciadas a una operacion P.E.(el reporto
	 * que puede tener mas de 1 repo secundario) este metodo le envias el PK del
	 * reporto y te devuelve todos los repos secundarios P.D.: en esta capa no
	 * se considera el estado.
	 * 
	 * @param reportoOperation
	 *            the reporto operation
	 * @return the mechanism operation reference service bean
	 * @throws ServiceException
	 *             the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> getMechanismOperationReferenceServiceBean(MechanismOperation reportoOperation) throws ServiceException {
		StringBuilder jpqlQuery = new StringBuilder();
		jpqlQuery
				.append("SELECT mo FROM MechanismOperation mo WHERE mo.referenceOperation.idMechanismOperationPk != null");
		jpqlQuery
				.append("										 AND mo.referenceOperation.idMechanismOperationPk = :idMechanismOpeartionParam ORDER BY mo.idMechanismOperationPk DESC");
		Query emQuery = em.createQuery(jpqlQuery.toString());
		emQuery.setParameter("idMechanismOpeartionParam",
				reportoOperation.getIdMechanismOperationPk());
		List<MechanismOperation> mechanismModalityList = null;
		try {
			mechanismModalityList = emQuery.getResultList();
		} catch (NoResultException ex) {
			return null;
		}
		return mechanismModalityList;
	}

	/**
	 * Gets the program interest coupon list service bean.
	 *
	 * @param otcOperationTO            the otc operation to
	 * @return the program interest coupon list service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ProgramInterestCoupon> getProgramInterestCouponListServiceBean(
			OtcOperationTO otcOperationTO) throws ServiceException {

		StringBuilder stringSql = new StringBuilder();
		stringSql.append("Select new com.pradera.model.issuancesecuritie.ProgramInterestCoupon(	");
		stringSql.append("prog.idProgramInterestPk, prog.couponNumber, prog.interestRate, prog.paymentDate, prog.stateProgramInterest, prog.experitationDate)	");
		stringSql.append("From ProgramInterestCoupon prog	");
		stringSql.append("Inner Join prog.interestPaymentSchedule paym	");
		stringSql.append("Where paym.security.idSecurityCodePk = :securityCode	");

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("securityCode", otcOperationTO.getIdSecurityCodePk());

		try {
			return findListByQueryString(stringSql.toString(), parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * Gets the otc operation to reject.
	 * 
	 * @param currentDate
	 *            the current date
	 * @return the otc operation to reject
	 * @throws ServiceException
	 *             the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> getOtcOperationToReject(Date currentDate) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		List<Integer> statesEvaluate = Arrays.asList(OtcOperationStateType.REGISTERED.getCode(),OtcOperationStateType.APROVED.getCode(),
													 OtcOperationStateType.REVIEWED.getCode(),  OtcOperationStateType.AUTHORIZED.getCode());
		String querySql = "Select new com.pradera.model.negotiation.MechanismOperation(me.idMechanismOperationPk,me.tradeOperation.operationState) "
				+ "		   From MechanismOperation me "
				+ "		   Where trunc(me.registerDate) <= :dateParam AND me.tradeOperation.operationState IN (:stateParam)";
		Query query = em.createQuery(querySql);
		query.setParameter("dateParam", currentDate);
		query.setParameter("stateParam", statesEvaluate);
		List<MechanismOperation> operationsToReject = null;

		try {
			operationsToReject = query.getResultList();
			for (MechanismOperation operation : operationsToReject) {
				OtcOperationStateType otcState = null;
				MechanismOperation operationToUpdate = em.find(MechanismOperation.class,operation.getIdMechanismOperationPk());
				if (operationToUpdate.getTradeOperation().getOperationState().equals(OtcOperationStateType.REGISTERED.getCode())) {
					otcState = OtcOperationStateType.ANULATE;
				} else {
					otcState = OtcOperationStateType.REJECTED;
				}
				operationToUpdate.getTradeOperation().setOperationState(otcState.getCode());
				operationToUpdate.getTradeOperation().setRejectDate(CommonsUtilities.currentDateTime());
				operationToUpdate.setOperationState(otcState.getCode());
				operationToUpdate.setAudit(loggerUser);
				update(operationToUpdate);
			}
		} catch (NoResultException ex) {
			return null;
		}
		return operationsToReject;
	}
	
	/**
	 * Gets the sirtex operation to reject.
	 *
	 * @param currentDate the current date
	 * @return the sirtex operation to reject
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<TradeRequest> getSirtexOperationToReject(Date currentDate) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		//issue 600
		List<TradeRequest> lstTradeRequest = null;
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("   SELECT TRE.ID_TRADE_REQUEST_PK, TRE.OPERATION_STATE                                               ");
		querySql.append("   FROM TRADE_REQUEST TRE                                                                            ");
		querySql.append("   INNER JOIN TRADE_OPERATION TOP ON TRE.ID_TRADE_REQUEST_PK = TOP.ID_TRADE_REQUEST_FK               ");
		querySql.append("   INNER JOIN MECHANISM_OPERATION MO ON TOP.ID_TRADE_OPERATION_PK = MO.ID_MECHANISM_OPERATION_PK     ");
		querySql.append("   WHERE 1 = 1                                                                                       ");
		querySql.append("   AND TRE.ID_NEGOTIATION_MECHANISM_FK = :negMechanism 				                              "); //5 SIRTEX, 3 OTC
		querySql.append("   AND TRUNC(TRE.OPERATION_DATE) = :currentDate                                                      ");
		querySql.append("   AND TRE.OPERATION_STATE IN (1862,1863,1865)                                                       "); 

		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("currentDate", CommonsUtilities.currentDate());
		query.setParameter("negMechanism", NegotiationMechanismType.SIRTEX.getCode());
		List<Object[]> lstRestul = query.getResultList();
		
		if (lstRestul.size()>0){
			
			lstTradeRequest = new ArrayList<TradeRequest>();
			
			for (Object[] lstFiltr : lstRestul){
				TradeRequest tradeRequest = new TradeRequest();
				tradeRequest.setIdTradeRequestPk(Long.valueOf(lstFiltr[0].toString()));
				tradeRequest.setOperationState(Integer.valueOf(lstFiltr[1].toString()));
				
				StringBuilder sbQuery = new StringBuilder();
				
				sbQuery.append(" select tre.tradeOperationList From TradeRequest tre ");
				sbQuery.append(" where tre.idTradeRequestPk = :idTrade ");
				Query queryAdd = em.createQuery(sbQuery.toString());
				queryAdd.setParameter("idTrade", tradeRequest.getIdTradeRequestPk());
				List<TradeOperation> resultListAdd = queryAdd.getResultList();
				
				tradeRequest.setTradeOperationList(resultListAdd);
				lstTradeRequest.add(tradeRequest);
			}
		}
		
		return lstTradeRequest;
		
		
		/*
		 List<Integer> statesEvaluate = Arrays.asList(SirtexOperationStateType.REGISTERED.getCode(),
													 SirtexOperationStateType.APROVED.getCode(),
													 SirtexOperationStateType.REVIEWED.getCode());
		 
		 
		 String querySql = "Select new com.pradera.model.negotiation.MechanismOperation(me.idMechanismOperationPk,me.tradeOperation.operationState) "
				+ "		   From MechanismOperation me "
				+ "		   Where trunc(me.registerDate) <= :dateParam AND me.operationState IN (:stateParam)";
		Query query = em.createQuery(querySql);
		query.setParameter("dateParam", currentDate);
		query.setParameter("stateParam", statesEvaluate);
		List<MechanismOperation> operationsToReject = null;

		try {
			operationsToReject = query.getResultList();
			for (MechanismOperation operation : operationsToReject) {
				SirtexOperationStateType sirtexState = null;
				MechanismOperation operationToUpdate = em.find(MechanismOperation.class,operation.getIdMechanismOperationPk());
				if (operationToUpdate.getTradeOperation().getOperationState().equals(SirtexOperationStateType.REGISTERED.getCode())) {
					sirtexState = SirtexOperationStateType.ANULATE;
				} else {
					sirtexState = SirtexOperationStateType.REJECTED;
				}
				operationToUpdate.getTradeOperation().setOperationState(sirtexState.getCode());
				operationToUpdate.getTradeOperation().setRejectDate(CommonsUtilities.currentDateTime());
				operationToUpdate.getTradeOperation().getTradeRequest().setOperationState(sirtexState.getCode());
				operationToUpdate.setOperationState(sirtexState.getCode());
				operationToUpdate.setAudit(loggerUser);
				update(operationToUpdate);
			}
		} catch (NoResultException ex) {
			return null;
		}
		
		return operationsToReject;
		*/
	}

	/**
	 * Find security from code.
	 *
	 * @param security the security
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityFromCode(Security security)throws ServiceException {
		return find(Security.class, security.getIdSecurityCodePk());
	}
	

	
	/**
	 * Gets the list negotiation modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param instrumentTyp the instrument typ
	 * @param participantPk the participant pk
	 * @return the list negotiation modality
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getListNegotiationModality(Long mechanismPk, Integer instrumentTyp, Long participantPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mod FROM MechanismModality 	mm							");
		sbQuery.append("	INNER JOIN mm.negotiationModality	mod							");
		
		if(participantPk!=null){
			sbQuery.append("	INNER JOIN mm.participantMechanisms	partMech	");
		}
		
		sbQuery.append("	WHERE mm.stateMechanismModality = :state	");
		sbQuery.append("	AND mm.negotiationModality.modalityState = :modalityState	");
		sbQuery.append("	AND mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk	");
		if(instrumentTyp!=null) {
			sbQuery.append("	AND mod.instrumentType = :instrumentType						");
		}
		if(participantPk!=null){
			sbQuery.append("	 AND 	partMech.participant.idParticipantPk = :participantPk	");
			sbQuery.append("	 AND 	partMech.stateParticipantMechanism	 = :statePartMech	");
		}
		
		sbQuery.append("	ORDER BY mm.negotiationModality.modalityName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", MechanismModalityStateType.ACTIVE.getCode());
		query.setParameter("modalityState", NegotiationModalityStateType.ACTIVE.getCode());
		query.setParameter("idNegotiationMechanismPk", mechanismPk);
		if(instrumentTyp!=null) {
			query.setParameter("instrumentType", instrumentTyp);
		}
		if(participantPk!=null){
			query.setParameter("participantPk", participantPk);
			query.setParameter("statePartMech", ParticipantMechanismStateType.REGISTERED.getCode());
		}
		
		return (List<NegotiationModality>)query.getResultList();
	}
	
	/**
	 * Gets the mechanism modality.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @return the mechanism modality
	 * @throws ServiceException the service exception
	 */
	public MechanismModality getMechanismModality(Long mechanismId,Long modalityId) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
	    sbQuery.append("Select mm from MechanismModality mm   ");
	    sbQuery.append(" inner join fetch mm.negotiationMechanism nme  ");
	    sbQuery.append(" inner join fetch mm.negotiationModality nmo  ");
	    sbQuery.append(" where mm.id.idNegotiationMechanismPk = :MechanismPk ");
	    sbQuery.append(" and   nmo.id.idNegotiationModalityPk = :modalityId ");
		parameters.put("MechanismPk", mechanismId);
		parameters.put("modalityId", modalityId);
		return (MechanismModality)findObjectByQueryString(sbQuery.toString(), parameters);
	}

	
	/**
	 * Begin of method for DPD.
	 *
	 * @param p the p
	 * @param h the h
	 * @param s the s
	 * @param isValidateAvailable the is validate available
	 * @return the balance from security
	 * @throws ServiceException the service exception
	 */
	/***
	 * Method to get HolderAccountBalance and know if balances exists
	 * @param p
	 * @param h
	 * @param s
	 * @return
	 * @throws ServiceException
	 */
	public HolderAccountBalance getBalanceFromSecurity(Participant p, HolderAccount h, Security s, boolean isValidateAvailable) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
	    sbQuery.append("Select hab from HolderAccountBalance hab   						");
	    sbQuery.append(" where hab.holderAccount.idHolderAccountPk = :accountPk		 	");
	    sbQuery.append(" and   hab.participant.idParticipantPk = :participantPk			");
	    sbQuery.append(" and   hab.security.idSecurityCodePk = :securityPk				");
	    if(isValidateAvailable){
	    	sbQuery.append(" and   hab.availableBalance > :zeroParam						");
	    }	    
		parameters.put("accountPk", h.getIdHolderAccountPk());
		parameters.put("participantPk", p.getIdParticipantPk());
		parameters.put("securityPk", s.getIdSecurityCodePk());
		if(isValidateAvailable){
			parameters.put("zeroParam", new BigDecimal(ComponentConstant.ZERO));
		}		
		try{
			return (HolderAccountBalance)findObjectByQueryString(sbQuery.toString(), parameters);
		}catch(NoResultException ex){
			return null; 
		}
	}
	
	/**
	 * **
	 * Method to find if exists marketFact from main balance.
	 *
	 * @param p the p
	 * @param h the h
	 * @param s the s
	 * @return the market fact balance
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderMarketFactBalance> getMarketFactBalance(Participant p, HolderAccount h, Security s) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
	    sbQuery.append("Select hab from HolderMarketFactBalance hab   					");
	    sbQuery.append(" where hab.holderAccount.idHolderAccountPk = :accountPk		 	");
	    sbQuery.append(" and   hab.participant.idParticipantPk = :participantPk			");
	    sbQuery.append(" and   hab.security.idSecurityCodePk = :securityPk				");
	    sbQuery.append(" and   hab.availableBalance >= :oneParam						");
	    sbQuery.append(" and   hab.indActive = :oneParam								");
	    
		parameters.put("accountPk", h.getIdHolderAccountPk());
		parameters.put("participantPk", p.getIdParticipantPk());
		parameters.put("securityPk", s.getIdSecurityCodePk());
		parameters.put("oneParam", new BigDecimal(ComponentConstant.ONE));
		return (List<HolderMarketFactBalance>)findListByQueryString(sbQuery.toString(), parameters);
	}		
	
	/**
	 * Find security from serie.
	 *
	 * @param securitySerial the security serial
	 * @return the security
	 */
	public Security findSecurityFromSerie(String securitySerial){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select sec from Security sec where sec.idSecurityCodeOnly = :idSecurityCode ");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCode", securitySerial);		
		try {
			return (Security) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param idHolderAccount the id holder account
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(Long idHolderAccount) throws ServiceException{
		return find(HolderAccount.class, idHolderAccount);
	}
	
	/**
	 * Gets the participant from mechanism modality.
	 *
	 * @param mechanism the mechanism
	 * @param modality the modality
	 * @return the participant from mechanism modality
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipantFromMechanismModality(Long mechanism, Long modality) throws ServiceException{
		StringBuilder selctSql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		StringBuilder orderBySql = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		selctSql.append("Select Distinct participante From Participant participante		");
		selctSql.append("Inner Join  participante.participantMechanisms part			");
		selctSql.append("Inner Join  part.mechanismModality mo							");
		selctSql.append("Inner Join  mo.negotiationModality negmod						");
		
//		selctSql.append("Select Distinct participante From MechanismModality mo			");
//		selctSql.append("Inner Join  mo.negotiationModality negmod						");
//		selctSql.append("Inner Join  mo.participantMechanisms part						");
//		selctSql.append("Inner Join Fetch part.participant participante					");
		whereSql.append("Where 1 = 1													");
		if (mechanism != null) {
			whereSql.append("And mo.id.idNegotiationMechanismPk = :negMechanismId		");
			parameters.put("negMechanismId",mechanism);
		}

//		whereSql.append("And mo.stateMechanismModality = :stateMechanismMod				");
//		parameters.put("stateMechanismMod",NegotiationModalityStateType.ACTIVE.getCode());

		if (modality != null) {
			whereSql.append("And mo.id.idNegotiationModalityPk = :negModalityId			");
			parameters.put("negModalityId",modality);
		}

		whereSql.append("And part.stateParticipantMechanism = :statePartMechanism		");
		parameters.put("statePartMechanism",ParticipantMechanismStateType.REGISTERED.getCode());

		orderBySql.append("Order By negmod.modalityName Asc								");
		
		return findListByQueryString(selctSql.toString().concat(whereSql.toString()), parameters);
	}
	
	public ParameterTable findParameterTableLongTxt () throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		ParameterTable parameterTable = null;
		sbQuery.append(" SELECT pt FROM ParameterTable pt WHERE pt.parameterTablePk = 2379 ");	
		Query query = em.createQuery(sbQuery.toString());
		
		parameterTable = (ParameterTable) query.getSingleResult();
		return parameterTable;
				
	}
	
	public MechanismOperationAdj getMechanismOperationAdj (Long idMechanismOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT a FROM MechanismOperationAdj a WHERE a.mechanismOperation.idMechanismOperationPk = :idMechanismOperationFk ");	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationFk", idMechanismOperation);
		
		try {
			return (MechanismOperationAdj) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
				
	}
	
	public Participant getParticipantFromId (Long idParticipantPk) throws ServiceException {
		return find(Participant.class,idParticipantPk);
	}
	
	public List<AccountAnnotationOperation> findAccountAnnotationOTCMotive(List<Long> lstAccountAnnotationPk) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
	    sbQuery.append(" SELECT aao FROM AccountAnnotationOperation aao ");
	    sbQuery.append(" JOIN FETCH aao.holderAccountSeller has ");
	    sbQuery.append(" JOIN FETCH aao.holderAccount ha ");
	    sbQuery.append(" JOIN FETCH aao.security s ");
	    sbQuery.append(" JOIN FETCH has.participant ps ");
	    sbQuery.append(" JOIN FETCH ha.participant p ");
	    sbQuery.append(" WHERE aao.motive = :motive ");
	    sbQuery.append(" and aao.idAnnotationOperationPk in (:lstAccountAnnotationPk) ");
	    
		parameters.put("motive", DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode() );
		parameters.put("lstAccountAnnotationPk", lstAccountAnnotationPk );
		
		List<AccountAnnotationOperation> lstAccountAnnotation = (List<AccountAnnotationOperation>)findListByQueryString(sbQuery.toString(), parameters);
		
		/*for (AccountAnnotationOperation accountAnnotationOperation : lstAccountAnnotation) {
			accountAnnotationOperation.getHolderAccountSeller().getParticipant();
			accountAnnotationOperation.getHolderAccount().getParticipant();
		}
		*/
		return lstAccountAnnotation;
	}
	
	public HolderAccount findHolderAccountAndDepositary(Long idHolderAccountPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT ha FROM HolderAccount ha JOIN FETCH ha.participant p WHERE ha.idHolderAccountPk = :idHolderAccountPk ");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);		
		try {
			return (HolderAccount) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	
	
}