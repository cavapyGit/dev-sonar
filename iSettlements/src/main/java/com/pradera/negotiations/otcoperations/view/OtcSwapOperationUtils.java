package com.pradera.negotiations.otcoperations.view;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.negotiations.otcoperations.to.OtcSwapOperationResultTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/07/2013
 */
public class OtcSwapOperationUtils {
	/**
	 * Checks if is in code exist operations.
	 * metodo para validar si ISIN ya existe en las operaciones SWAP
	 * @param isinCode the isin code
	 * @param otcSwapOperationList the otc swap operation list
	 * @return true, if is in code exist operations
	 */
	public static boolean isinCodeExistOperations(String isinCode, List<OtcSwapOperationResultTO> otcSwapOperationList){
		if(isinCode==null || otcSwapOperationList==null || otcSwapOperationList.isEmpty()){
			return Boolean.FALSE;
		}
		for(OtcSwapOperationResultTO otcSwapOperation: otcSwapOperationList){
			if(isinCode.equals(otcSwapOperation.getIsinOperation().getIdSecurityCodePk())){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the all hold acc operation.
	 *
	 * @param mecOperation the mec operation
	 * @param swapOperation the swap operation
	 * @return the all hold acc operation
	 */
	public static List<HolderAccountOperation> getAllHoldAccOperation(MechanismOperation mecOperation, SwapOperation swapOperation){
		List<HolderAccountOperation> holdAccOperationList = new ArrayList<HolderAccountOperation>();
		holdAccOperationList.addAll(OtcSwapOperationUtils.getHoldAccOperationByNegotiation(mecOperation, swapOperation, ParticipantRoleType.BUY.getCode()));
		holdAccOperationList.addAll(OtcSwapOperationUtils.getHoldAccOperationByNegotiation(mecOperation, swapOperation, ParticipantRoleType.SELL.getCode()));
		return holdAccOperationList;
	}
	
	/**
	 * Gets the hold acc operation by negotiation.
	 * metodo para obtener las cuentas de acuerdo al rol VENTA - COMPRA
	 *
	 * @param mecOperation the mec operation
	 * @param swapOperation the swap operation
	 * @param role the part role type
	 * @return the hold acc operation by negotiation
	 */
	public static List<HolderAccountOperation> getHoldAccOperationByNegotiation(MechanismOperation mecOperation, SwapOperation swapOperation, Integer role){
		List<HolderAccountOperation> holdAccountOperations = new ArrayList<HolderAccountOperation>();
		if(swapOperation!=null){
			for(HolderAccountOperation holdAccountOperation: swapOperation.getHolderAccountOperations()){
				if(holdAccountOperation.getRefAccountOperation()==null){
					if(holdAccountOperation.getRole().equals(role)){
						if(holdAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
							continue;
						}
						holdAccountOperations.add(holdAccountOperation);
					}
				}
			}
		}
		return holdAccountOperations;
	}
	
	/**
	 * Gets the stock quantity oposition.
	 *
	 * @param holdAccOperation the hold acc operation
	 * @param mechanismOperation the mechanism operation
	 * @return the stock quantity oposition
	 */
	public static HolderAccountOperation getAccountOperation(HolderAccountOperation holdAccOperation, MechanismOperation mechanismOperation){
		HolderAccountOperation holderAccountOperation = new HolderAccountOperation();
		SwapOperation swapOperation = mechanismOperation.getSwapOperations().get(0);
		BigDecimal stockQuantity = holdAccOperation.getStockQuantity();
		BigDecimal stockQuantitySwap = swapOperation.getStockQuantity();
		BigDecimal stockQuantityMech = mechanismOperation.getStockQuantity();
		BigDecimal percent = null;
		BigDecimal cashPrice = null;
		if(holdAccOperation.getSwapOperation()!=null){
			percent = stockQuantity.divide(stockQuantitySwap,8,RoundingMode.HALF_UP);
			cashPrice = mechanismOperation.getCashPrice();
			stockQuantity = percent.multiply(stockQuantityMech);
		}else if(holdAccOperation.getMechanismOperation()!=null){
			percent = stockQuantity.divide(stockQuantityMech,8,RoundingMode.HALF_UP);
			cashPrice = swapOperation.getCashPrice();
			stockQuantity = percent.multiply(stockQuantitySwap);
		}
		holderAccountOperation.setStockQuantity(stockQuantity);
		if(stockQuantity != null && cashPrice!= null){
			holderAccountOperation.setCashAmount(stockQuantity.multiply(cashPrice));
		}		
		return holderAccountOperation;
	}
	/**
	 * Gets the otc swap from list.
	 *
	 * @param otcSwatList the otc swat list
	 * @param participationRoleType the participation role type
	 * @return the otc swap from list
	 */
	public static OtcSwapOperationResultTO getOtcSwapFromList(List<OtcSwapOperationResultTO> otcSwatList, ParticipantRoleType participationRoleType){
		if(participationRoleType==null || otcSwatList==null || otcSwatList.isEmpty()){
			return null;
		}
		for(OtcSwapOperationResultTO otcSwapOperation: otcSwatList){
			if(otcSwapOperation.getParticipationOperation().equals(participationRoleType.getCode())){
				return otcSwapOperation;
			}
		}
		return null;
	}
	/**
	 * Gets the otc swap by currency.
	 * metodo para retornar el objeto OtcSwapOperation a partir de una determinada moneda
	 * @param otcSwatList the otc swat list
	 * @param currencyType the currency type
	 * @return the otc swap by currency
	 */
	public static OtcSwapOperationResultTO getOtcSwapByCurrency(List<OtcSwapOperationResultTO> otcSwatList, CurrencyType currencyType){
		if(currencyType==null || otcSwatList==null || otcSwatList.isEmpty()){
			return null;
		}
		for(OtcSwapOperationResultTO otcSwapOperation: otcSwatList){
			if(CurrencyType.get(otcSwapOperation.getCurrency()).equals(currencyType)){
				return otcSwapOperation;
			}
		}
		return null;
	}
	/**
	 * Gets the last otc swap from list.
	 * metodo para obtener el ultimo OtcSwapOperation de la lista
	 * @param otcSwatList the otc swat list
	 * @return the last otc swap from list
	 */
	public static OtcSwapOperationResultTO getLastOtcSwapFromList(List<OtcSwapOperationResultTO> otcSwatList){
		if(otcSwatList==null || otcSwatList.isEmpty()){
			return null;
		}
		Integer tamanio = otcSwatList.size();
		return otcSwatList.get(tamanio-1);
	}
	/**
	 * Gets the swap operation from to.
	 * metodo para obtener el objeto SWAP_OPERATION desde el TO
	 * @param mechanismOperation the mechanism operation
	 * @param otcSwapOperationTO the otc swap operation to
	 * @return the swap operation from to
	 */
	public static SwapOperation getSwapOperationFromTO(MechanismOperation mechanismOperation, OtcSwapOperationResultTO otcSwapOperationTO){
		if(otcSwapOperationTO==null){
			return null;
		}
		SwapOperation swapOperation = new SwapOperation();
		swapOperation.setMechanismOperation(mechanismOperation);
		swapOperation.setSellerParticipant(otcSwapOperationTO.getParticipantSeller());
		swapOperation.setBuyerParticipant(otcSwapOperationTO.getParticipantBuyer());
		swapOperation.setSecurities(otcSwapOperationTO.getIsinOperation());
		swapOperation.setCurrency(otcSwapOperationTO.getCurrency());
		swapOperation.setCashPrice(otcSwapOperationTO.getCashPrice());
		swapOperation.setCashAmount(otcSwapOperationTO.getCashAmount());
		swapOperation.setStockQuantity(otcSwapOperationTO.getStockQuantity());
		swapOperation.setRealCashAmount(otcSwapOperationTO.getRealCashAmount()==null?BigDecimal.ZERO:otcSwapOperationTO.getRealCashAmount());
		swapOperation.setRealCashPrice(otcSwapOperationTO.getRealPriceamount()==null?BigDecimal.ZERO:otcSwapOperationTO.getRealPriceamount());
		return swapOperation;
	}
	/**
	 * Gets the exchange rate from otc swap list.
	 * metodo para obtener el tipo de cambio cuando la moneda de la operacion
	 * y la moneda de SwapOperation siempre y cuando las monedas sean DISTINTAS
	 * @param otcSwapOperationList the otc swap operation list
	 * @return the exchange rate from otc swap list
	 */
	public static BigDecimal getExchangeRateFromOtcSwapList(List<OtcSwapOperationResultTO> otcSwapOperationList){
		OtcSwapOperationResultTO otcSwapOperation_DOP = OtcSwapOperationUtils.getOtcSwapByCurrency(otcSwapOperationList, CurrencyType.PYG);
		OtcSwapOperationResultTO otcSwapOperation_USD = OtcSwapOperationUtils.getOtcSwapByCurrency(otcSwapOperationList, CurrencyType.USD);
		BigDecimal exchangeRate = BigDecimal.ZERO;
		if(otcSwapOperation_DOP==null || otcSwapOperation_USD==null){
			return null;
		}
		if(otcSwapOperation_DOP.getCashAmount()!=null && otcSwapOperation_USD.getCashAmount()!=null){
			exchangeRate = otcSwapOperation_DOP.getCashAmount().divide(otcSwapOperation_USD.getCashAmount(),6,RoundingMode.CEILING);
		}
		return exchangeRate;
	}
	
	/**
	 * Have same moneys.
	 * metodo para validar si otcSwapOperation tiene la misma moneda de Security
	 *
	 * @param otcSwapOperationResultTO the otc swap operation result to
	 * @param currencyType the currency type
	 * @return the boolean
	 */
	public static Boolean haveSameMoneys(OtcSwapOperationResultTO otcSwapOperationResultTO, CurrencyType currencyType){
		if(otcSwapOperationResultTO==null || otcSwapOperationResultTO.getCurrency()==null || currencyType==null){
			return Boolean.FALSE;
		}
		return CurrencyType.get(otcSwapOperationResultTO.getCurrency()).equals(currencyType);
	}
	/**
	 * Have same money.
	 * metodo para saber si List<OtcSwapTO> tienen la misma moneda
	 * @param otcSwapOperationResultTOs the otc swap operation result t os
	 * @return the boolean
	 */
	public static Boolean haveSameMoney(List<OtcSwapOperationResultTO> otcSwapOperationResultTOs){
		if(otcSwapOperationResultTOs==null || otcSwapOperationResultTOs.isEmpty()){
			return Boolean.FALSE;
		}
		List<CurrencyType> currencyTypes = new ArrayList<CurrencyType>();
		for(OtcSwapOperationResultTO otcSwapOperationResultTO: otcSwapOperationResultTOs){
			CurrencyType currency = CurrencyType.get(otcSwapOperationResultTO.getCurrency());
			if(currencyTypes.contains(currency)){
				return Boolean.TRUE;
			}
			currencyTypes.add(currency);
		}
		return Boolean.FALSE;
	}
	/**
	 * Gets the stock quantity by participation.
	 * metodo para obtener el precio sucio de la operacion
	 * @param mechanismOperation the mechanism operation
	 * @param participantRoleType the participant role type
	 * @return the stock quantity by participation
	 */
	public static BigDecimal getCashPriceByParticipation(MechanismOperation mechanismOperation, ParticipantRoleType participantRoleType){
		if(mechanismOperation==null || mechanismOperation.getSwapOperations()==null || mechanismOperation.getSwapOperations().isEmpty() || participantRoleType==null){
			return null;
		}
		if(ParticipantRoleType.SELL.equals(participantRoleType)){//Si es venta
			return mechanismOperation.getCashPrice();//Obtenemos el precio sucio de la operacion
		}else if(ParticipantRoleType.BUY.equals(participantRoleType)){//Si es compra
			SwapOperation swapOperation = mechanismOperation.getSwapOperations().get(0);//Obtenemos el objeto Swap
			return swapOperation.getCashPrice();//retornamos el precio sucio de Swap 
		}
		return null;
	}
	/**
	 * Gets the otc swap operation result to from object.
	 * metodo para obtener swapOperationsTO desde mechanismOperations
	 * @param mechanismOperation the mechanism operation
	 * @return the otc swap operation result to from object
	 */
	public static List<OtcSwapOperationResultTO> getOtcSwapOperationResultTOFromObject(MechanismOperation mechanismOperation){
		if(mechanismOperation==null || mechanismOperation.getSwapOperations()==null || mechanismOperation.getSwapOperations().isEmpty()){
			return null;
		}
		List<OtcSwapOperationResultTO> otcSwaList = new ArrayList<OtcSwapOperationResultTO>();
		OtcSwapOperationResultTO swapOperationSell = new OtcSwapOperationResultTO();
		swapOperationSell.setParticipationOperation(ParticipantRoleType.SELL.getCode());
		swapOperationSell.setParticipantSeller(mechanismOperation.getSellerParticipant());
		swapOperationSell.setParticipantBuyer(mechanismOperation.getBuyerParticipant());
		swapOperationSell.setCurrency(mechanismOperation.getCurrency());
		swapOperationSell.setCashAmount(mechanismOperation.getCashAmount());
		swapOperationSell.setCashPrice(mechanismOperation.getCashPrice());
		swapOperationSell.setRealCashAmount(mechanismOperation.getRealCashAmount());
		swapOperationSell.setRealPriceamount(mechanismOperation.getRealCashPrice());
		swapOperationSell.setIsinOperation(mechanismOperation.getSecurities());
		swapOperationSell.setStockQuantity(mechanismOperation.getStockQuantity());
		
		SwapOperation swapOperation = mechanismOperation.getSwapOperations().get(0);
		OtcSwapOperationResultTO swapOperationBuy = new OtcSwapOperationResultTO();
		swapOperationBuy.setParticipationOperation(ParticipantRoleType.BUY.getCode());
		swapOperationBuy.setParticipantSeller(swapOperation.getSellerParticipant());
		swapOperationBuy.setParticipantBuyer(swapOperation.getBuyerParticipant());
		swapOperationBuy.setCurrency(swapOperation.getCurrency());
		swapOperationBuy.setCashAmount(swapOperation.getCashAmount());
		swapOperationBuy.setCashPrice(swapOperation.getCashPrice());
		swapOperationBuy.setRealCashAmount(swapOperation.getCashAmount());
		swapOperationBuy.setRealPriceamount(swapOperation.getCashPrice());
		swapOperationBuy.setIsinOperation(swapOperation.getSecurities());
		swapOperationBuy.setStockQuantity(swapOperation.getStockQuantity());
		
		otcSwaList.add(swapOperationSell);
		otcSwaList.add(swapOperationBuy);
		
		return otcSwaList;
	}
	
	/**
	 * Gets the indicator hold acc operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param holdAccOperationParam the hold acc operation param
	 * @return the indicator hold acc operation
	 */
	public static Integer getIndicatorHoldAccOperation(SwapOperation mechanismOperation, HolderAccountOperation holdAccOperationParam){
		HolderAccount holdAccSelected = holdAccOperationParam.getHolderAccount();
		for(int i=0; i<mechanismOperation.getHolderAccountOperations().size(); i++){
			HolderAccountOperation holdAccOperation = mechanismOperation.getHolderAccountOperations().get(i);
			if(holdAccOperationParam.getIndIncharge().equals(BooleanType.YES.getCode()) && holdAccOperation.getHolderAccount()==null){//Si tiene encargo y aun no hay cuentas
				Participant participantInCharge = holdAccOperation.getInchargeFundsParticipant();
				Participant participantSelected = holdAccOperationParam.getInchargeFundsParticipant();
				if(holdAccOperationParam.getRole().equals(holdAccOperation.getRole()) && participantSelected.equals(participantInCharge)){//Verificamos el ROL(compra-venta) y el participante encargado
					return i;
				}
			}else{
				if(holdAccOperation.getHolderAccount().getIdHolderAccountPk().equals(holdAccSelected.getIdHolderAccountPk())){//Verificamos por el Id de la cuenta
					return i;
				}
			}
		}
		return null;
	}
}