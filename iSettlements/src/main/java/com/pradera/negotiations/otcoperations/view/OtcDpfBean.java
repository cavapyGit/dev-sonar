package com.pradera.negotiations.otcoperations.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationAdj;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.negotiations.otcoperations.facade.OtcDpfOperationFacade;
import com.pradera.negotiations.otcoperations.to.OtcDpfManagementTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationResultTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2014.</li>
 * </ul>
 * 
 * The Class OtcOperationMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/01/2014
 */
@DepositaryWebBean
public class OtcDpfBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7262971568117324014L;
	
	/** Navigation rule to management the page. */
	private static final String OTC_RULE = "otcDpfOperationRule";
	
	/** The otc dpf register to. */
	private OtcDpfManagementTO otcDpfRegisterTO;
	
	/** The otc dpf consult to. */
	private OtcDpfManagementTO otcDpfConsultTO;
	
	/** The id participant pk for issuer. */
	private Long idParticipantPkForIssuer;
	
	/** The accounts facade. */
	@EJB private AccountsFacade accountsFacade;
	
	/** The general parameter facade. */
	@EJB private GeneralParametersFacade generalParameterFacade;

	/** The otc service facade. */
	@EJB private OtcDpfOperationFacade otcServiceFacade;		

	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;

	/** The parameter description. */
	private Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	
	/** The map parameters. */
	private HashMap<Integer, ParameterTable> mapParameters = new HashMap<Integer,ParameterTable>();
	
	/** issue 716 campos adicionales */
	private MechanismOperationAdj attachFileSelected;
	private String fileNameDisplay;
	private String observaciones;
	private String textDescription;
	private transient StreamedContent fileDownload;
	
	public StreamedContent getFileDownload() {
		return fileDownload;
	}

	public void setFileDownload(StreamedContent fileDownload) {
		this.fileDownload = fileDownload;
	}
	
	public String getTextDescription() {
		return textDescription;
	}

	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}

	public MechanismOperationAdj getAttachFileSelected() {
		return attachFileSelected;
	}

	public void setAttachFileSelected(MechanismOperationAdj attachFileSelected) {
		this.attachFileSelected = attachFileSelected;
	}

	public String getFileNameDisplay() {
		return fileNameDisplay;
	}

	public void setFileNameDisplay(String fileNameDisplay) {
		this.fileNameDisplay = fileNameDisplay;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		otcDpfConsultTO = new OtcDpfManagementTO();
		try{
			loadPriviligies();
			loadOtcDpfConsult();
			loadCboParticipants();
			loadOtcOperationStateType();
			loadCboOtcOriginRequestType();
			loadCboParemetersAccount();
		}catch(ServiceException ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	/** issue 716 **/
	public void documentAttachFile(FileUploadEvent event){
		String fDisplayName=fUploadValidateFile(event.getFile(),null, getfUploadFileDocumentsSize(),getfUploadFileDocumentsTypes());
		if(fDisplayName!=null){
			 fileNameDisplay = fDisplayName;
		}
		MechanismOperationAdj mechanismOperationAdj = new MechanismOperationAdj();
		mechanismOperationAdj.setDocumentFile(event.getFile().getContents());
		attachFileSelected = mechanismOperationAdj;
	}
	/**
	 * @throws ServiceException */
	public StreamedContent getDocumentoAdjunto() throws ServiceException{
		MechanismOperationAdj mechanismOperationAdj = otcServiceFacade.getMechanismOperationAdj(attachFileSelected.getMechanismOperation().getIdMechanismOperationPk());
		byte[] bs = mechanismOperationAdj.getDocumentFile();
		 if(Validations.validateIsNotNull(bs)){						
			 InputStream stream = new ByteArrayInputStream(bs);
			 fileDownload = new DefaultStreamedContent(stream,null,mechanismOperationAdj.getFileName()); 
		 } 
		return fileDownload;
	}
	
	/**
	 * Method to load New operation.
	 *
	 * @return the string
	 */
	public String newOtcDpfOperationAction(){
		try{
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
					&& Validations.validateIsNullOrNotPositive(idParticipantPkForIssuer)){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}
			/**Set by default register view*/
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			otcDpfRegisterTO = new OtcDpfManagementTO();
						
			if(otcDpfRegisterTO.getOtcDpfOperation() != null){
				if(otcDpfRegisterTO.getOtcDpfOperation().getSecurities() != null){
					otcDpfRegisterTO.getOtcDpfOperation().setSecurities(new Security());
				}
				if(otcDpfRegisterTO.getOtcDpfOperation().getSellerParticipant() != null){
					otcDpfRegisterTO.getOtcDpfOperation().setSellerParticipant(new Participant());
				}
				if(otcDpfRegisterTO.getOtcDpfOperation().getBuyerParticipant() != null){
					otcDpfRegisterTO.getOtcDpfOperation().setBuyerParticipant(new Participant());
				}				
			}
			if(Validations.validateIsNotNull(attachFileSelected)) {
				attachFileSelected = new MechanismOperationAdj();
				observaciones = "";
				textDescription = "";
				fileNameDisplay = "";
				fileDownload = null;
			}
			
			/**Load some parameters, loaded before*/
			otcDpfRegisterTO.setLstCboOtcOriginRequestType(otcDpfConsultTO.getLstCboOtcOriginRequestType());
			
			ParameterTable objParameterTable = otcServiceFacade.findParameterTableLongTxt();
			textDescription = objParameterTable.getText3();
			
			/**begin of variables*/
			MechanismOperation otcDpfOperation = otcServiceFacade.setupNewMechanismOperation(idParticipantPkForIssuer);
			
			/**Setting mechanism operation into TO*/
			otcDpfRegisterTO.setOtcDpfOperation(otcDpfOperation);
			otcDpfRegisterTO.setAccountSale(new HolderAccount());
			otcDpfRegisterTO.setAccountBuy(new HolderAccount());
			otcDpfRegisterTO.setSellerHolder(new Holder());
			otcDpfRegisterTO.setBuyerHolder(new Holder());
			/**Method to load participants**/
			changeOriginRequest();
			
			/**List to handle security class allowed at the operation*/
			List<Integer> securityClassList = Arrays.asList(SecurityClassType.DPF.getCode(),SecurityClassType.DPA.getCode());
			otcDpfRegisterTO.setSecurityClassAllowed(securityClassList);
			
			return OTC_RULE;
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			return "";
		}
	}
	
	/**
	 * Methods to load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboParemetersAccount() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
		masterPks.add(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());
		parameterTableTO.setLstMasterTableFk(masterPks);
		List<ParameterTable> lst = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt);
		}
	}
	
	/**
	 * Methods to load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboParticipants() throws ServiceException{
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
//		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getPartIssuerCode())) {
//			otcDpfConsultTO.getOtcDpfFiltersTO().setIdParticipantBuyer(userInfo.getUserAccountSession().getPartIssuerCode());
//			idParticipantPkForIssuer=userInfo.getUserAccountSession().getPartIssuerCode();
//		}
		
		if (userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			idParticipantPkForIssuer = getIssuerDpfInstitution(userInfo);
				if(Validations.validateIsNotNullAndNotEmpty(idParticipantPkForIssuer)){
//					otcDpfConsultTO.getOtcDpfFiltersTO.idParticipantBuyer
					otcDpfConsultTO.getOtcDpfFiltersTO().setIdParticipantBuyer(idParticipantPkForIssuer);
				}else {
					otcDpfConsultTO.getOtcDpfFiltersTO().setIdParticipantBuyer(null);
//					flagExistParticipantDPF = true;
//					holderAccountRequestHySession.setParticipant(new Participant(participantDpf));
//					holderAccountTO.setParticipantTO(participantDpf);
//				}else{
//					flagExistParticipantDPF = false;
//					holderAccountTO.setParticipantTO(null);
				}
		} else if (userInfo.getUserAccountSession().isBcbInstitution()){
			// solo para el BCB
			idParticipantPkForIssuer = userInfo.getUserAccountSession().getParticipantCode();
			otcDpfConsultTO.getOtcDpfFiltersTO().setIdParticipantBuyer(idParticipantPkForIssuer);
		}

		otcDpfConsultTO.setAllParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
		for(Participant p: otcDpfConsultTO.getAllParticipantList()){
			if(otcDpfConsultTO.getAllParticipantMap()==null){
				otcDpfConsultTO.setAllParticipantMap(new HashMap<Long,Participant>());
			}
			otcDpfConsultTO.getAllParticipantMap().put(p.getIdParticipantPk(), p);
		}
	}

	/**
	 * Method to load origin request type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboOtcOriginRequestType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.OTC_REQUEST_ORIGING.getCode());
		parameterTableTO.setParameterTablePk(OtcOperationOriginRequestType.CRUSADE.getCode().intValue());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<ParameterTable> lstCboOtcOriginRequest = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstCboOtcOriginRequest){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
		}
		otcDpfConsultTO.setLstCboOtcOriginRequestType(lstCboOtcOriginRequest);
	}
	
	/**
	 * Method to load all states.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadOtcOperationStateType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.OTC_OPERATION_STATE.getCode());
		List<ParameterTable> operationStates = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: operationStates)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
		otcDpfConsultTO.setLstCboOtcOperationStateType(operationStates);
		
		parameterTableTO.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		operationStates = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: operationStates)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
	}
	
	/**
	 * Method to load dpf consult to show in view.
	 */
	private void loadOtcDpfConsult() {
		// TODO Auto-generated method stub
		OtcOperationTO otcDpfOperation = new OtcOperationTO();
		otcDpfOperation.setInitialDate(new Date());
		otcDpfOperation.setFinalDate(new Date());
		otcDpfConsultTO.setOtcDpfFiltersTO(otcDpfOperation);
		otcDpfConsultTO.setOtcResultDataModel(null);
		otcDpfConsultTO.setSellerHolder(new Holder());
		otcDpfConsultTO.setBuyerHolder(new Holder());
	}
	
	/**
	 * Method to verify origin request.
	 */
	public void changeOriginRequest(){
		try {
			otcDpfRegisterTO.getOtcDpfOperation().getBuyerParticipant().setIdParticipantPk(null);
			otcDpfRegisterTO.getOtcDpfOperation().getSellerParticipant().setIdParticipantPk(null);
			otcDpfRegisterTO.getOtcDpfOperation().setSecurities(new Security());
			otcDpfRegisterTO.setAccountBuy(new HolderAccount());
			otcDpfRegisterTO.setAccountSale(new HolderAccount());
			if(Validations.validateIsNotNullAndPositive(idParticipantPkForIssuer)){
				if(otcDpfRegisterTO.getOtcDpfOperation().getOriginRequest().equals(OtcOperationOriginRequestType.CRUSADE.getCode())){
						otcDpfRegisterTO.getOtcDpfOperation().getSellerParticipant().setIdParticipantPk(idParticipantPkForIssuer);
						otcDpfRegisterTO.getOtcDpfOperation().getBuyerParticipant().setIdParticipantPk(idParticipantPkForIssuer);
						List<Participant> participantBuyerList = otcServiceFacade.getParticipantForMechanism();
						/**Assignment participant buyers in TO*/
						otcDpfRegisterTO.setParticipantBuyList(participantBuyerList);
				}else {
					otcDpfRegisterTO.getOtcDpfOperation().getSellerParticipant().setIdParticipantPk(idParticipantPkForIssuer);
					otcDpfRegisterTO.getOtcDpfOperation().getBuyerParticipant().setIdParticipantPk(null);
					List<Participant> participantBuyerList = otcServiceFacade.getParticipantForMechanism();
					participantBuyerList.remove(new Participant(idParticipantPkForIssuer));
					/**Assignment participant buyers in TO*/
					otcDpfRegisterTO.setParticipantBuyList(participantBuyerList);
				}
			}
			
			List<Participant> participantSeller = otcServiceFacade.getParticipantForMechanism();
			/**Assignment participant seller at TO*/
			otcDpfRegisterTO.setParticipantSaleList(participantSeller);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Method to verify Participant seller.
	 */
	public void changeParticipantSeller(){
		MechanismOperation otcDPfOperation = otcDpfRegisterTO.getOtcDpfOperation();
		Long originRequest = otcDPfOperation.getOriginRequest();
		Long participantSale = otcDPfOperation.getSellerParticipant().getIdParticipantPk();		
		otcDpfRegisterTO.getOtcDpfOperation().setSecurities(new Security());		
		otcDpfRegisterTO.setAccountSale(new HolderAccount());		
		otcDpfRegisterTO.setSellerHolder(new Holder());
		otcDpfRegisterTO.setSellerHolderAccounts(null);
		
		try {
			/**Getting participant buyers*/
			List<Participant> participantBuyerList = otcServiceFacade.getParticipantForMechanism();
			/**If, request is crusade the participant could be the same*/
			if(originRequest.equals(OtcOperationOriginRequestType.CRUSADE.getCode())){
				Participant participantBuyer = new Participant(participantSale);
				otcDpfRegisterTO.setBuyerHolder(new Holder());
				otcDPfOperation.setBuyerParticipant(participantBuyer);
			}else{
				/**Remove participant Seller from list*/
				participantBuyerList.remove(new Participant(participantSale));
			}
			/**Assignment participant buyers in TO*/
			otcDpfRegisterTO.setParticipantBuyList(participantBuyerList);
			otcDpfRegisterTO.setAccountSale(new HolderAccount());
			otcDpfRegisterTO.getOtcDpfOperation().setSecurities(new Security());
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change participant buyer.
	 */
	public void changeParticipantBuyer(){
		otcDpfRegisterTO.setBuyerHolder(new Holder());
		otcDpfRegisterTO.setBuyerHolderAccounts(null);		
		otcDpfRegisterTO.setAccountBuy(new HolderAccount());		
		otcDpfRegisterTO.getOtcDpfOperation().setSecurities(new Security());
	}
	
	/**
	 * Method to validate seller account on operation.
	 */
	public void validateAccountSeller(){
		/**Getting account from TO*/
		HolderAccount accountSale = otcDpfRegisterTO.getAccountSale();
		/**If id oh holderAccount is different than null*/
		if(accountSale.getIdHolderAccountPk()!=null){
			try {
				/**Validate holderAccount*/
				otcServiceFacade.validateHolderAccountSeller(accountSale);
			} catch (ServiceException e) {
				otcDpfRegisterTO.setAccountSale(new HolderAccount());
				if(e.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
			/**Clean HolderAccount buyer*/
			otcDpfRegisterTO.setAccountBuy(new HolderAccount());
		}
	}
	
	/**
	 * Method to validate buyer account on operation.
	 */
	public void validateAccountBuyer(){
		/**Getting account from TO*/
		HolderAccount accountBuy = otcDpfRegisterTO.getAccountBuy();
		HolderAccount accountSale = otcDpfRegisterTO.getAccountSale();
		
		if(accountSale == null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_HOLDER_SALE);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		/**If id oh holderAccount is different than null*/
		if(accountBuy.getIdHolderAccountPk() != null && accountSale.getIdHolderAccountPk() != null){
			try {
				/**Validate holderAccount*/
				otcServiceFacade.validateHolderAccountBuyer(accountBuy,accountSale);
			} catch (ServiceException e) {
				otcDpfRegisterTO.setAccountBuy(new HolderAccount());
				if(e.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
	}
	
	/**
	 * Method to validate all security's features.
	 */
	public void validateSecurityCode(){
		/**Getting Operation from TO*/
		MechanismOperation otcDpfOperation = otcDpfRegisterTO.getOtcDpfOperation();
		/**Validate mechanism operation*/
		try {
			/**Validate holderAccount*/
			otcServiceFacade.validateSecurityCode(otcDpfOperation,otcDpfRegisterTO.getAccountSale());
		} catch (ServiceException e) {
			otcDpfOperation.setSecurities(new Security());
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Method before to save any mechanism operation.
	 */
	public void beforeSaveOtcOperation(){
		MechanismOperation operation = otcDpfRegisterTO.getOtcDpfOperation();
		if(Validations.validateIsNullOrEmpty(otcDpfRegisterTO.getAccountSale().getIdHolderAccountPk())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_HOLDER_SALE);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
//		if(Validations.validateIsNullOrEmpty(otcDpfRegisterTO.getAccountBuy().getIdHolderAccountPk())){
//			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
//			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_HOLDER_BUYER);
//			showMessageOnDialog(headerMessage, bodyMessage);
//			JSFUtilities.showSimpleValidationDialog();
//			return;
//		}
		
		if(Validations.validateIsNullOrEmpty(operation.getSecurities().getIdSecurityCodePk())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_SECURITY_PK);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}		
		
		String messageProperties = "";
		Boolean haveValidation = Boolean.FALSE;
		
		MechanismOperationAdj aditionalInf = attachFileSelected;		
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER: 	
				if (Validations.validateIsNotNullAndNotEmpty(aditionalInf)) {
					if (observaciones != null && observaciones.trim().length()>0 
							&& aditionalInf.getDocumentFile() != null) {
							messageProperties = PropertiesConstants.OTC_DPF_OPERATION_REGISTER_CONFIRM;
							haveValidation 	  = validateBeforeRegister();break;
					}
				}
			case APPROVE:	messageProperties = PropertiesConstants.OTC_DPF_OPERATION_APPROVE_CONFIRM;break;
			case REVIEW:	messageProperties = PropertiesConstants.OTC_DPF_OPERATION_REVIEW_CONFIRM;
							haveValidation    = validateBeforeReview(operation);break;
			case CONFIRM:	messageProperties = PropertiesConstants.OTC_DPF_OPERATION_CONFIRM_CONFIRM;break;
			case ANULATE:	messageProperties = PropertiesConstants.OTC_DPF_OPERATION_ANNULATE_CONFIRM;break;
			case REJECT:	messageProperties = PropertiesConstants.OTC_DPF_OPERATION_REJECT_CONFIRM;break;
			default:break;
		}
		
		if(!haveValidation){
			Long operationPk = operation.getIdMechanismOperationPk();
			String operationPkToString = null;
			if(operationPk!=null){
				operationPkToString = operationPk.toString();
			}
			if(ViewOperationsType.get(getViewOperationType()).name() == "REGISTER") {
				if (Validations.validateIsNotNullAndNotEmpty(aditionalInf)) {
					if (observaciones != null && observaciones.trim().length()>0 
							&& aditionalInf.getDocumentFile() != null) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
											PropertiesUtilities.getMessage(messageProperties,operationPkToString));
						JSFUtilities.executeJavascriptFunction("PF('cnfwOtcDpfOperations').show()");
					} else {
			        	showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED_ADJUNTOS,null);
						JSFUtilities.showSimpleValidationDialog();
			        }
				} else {
		        	showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED_ADJUNTOS,null);
					JSFUtilities.showSimpleValidationDialog();
		        }
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(messageProperties,operationPkToString));
				JSFUtilities.executeJavascriptFunction("PF('cnfwOtcDpfOperations').show()");
			}
		}
	}
	
	/**
	 * Method to validate before begin transaction.
	 *
	 * @return the boolean
	 */
	public Boolean validateBeforeRegister(){
		/**Variable to handle properties*/
		String propertieKey = null;
		
//		if(otcDpfRegisterTO.getOtcDpfOperation().getOriginRequest().equals(OtcOperationOriginRequestType.SALE.getCode())){
//			HolderAccount accountBuyer = otcDpfRegisterTO.getAccountBuy();
//			if(accountBuyer==null || accountBuyer.getIdHolderAccountPk()==null){
//				propertieKey = "otc.operation.account.buyer.empty";
//			}
//		}
		
		/**If properties key have some validation, it's time to show. GO*/
		if(propertieKey!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),PropertiesUtilities.getMessage(propertieKey));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}
		return false;
	}
	
	/**
	 * Method to validate before begin transaction.
	 *
	 * @param operation the operation
	 * @return the boolean
	 */
	public Boolean validateBeforeReview(MechanismOperation operation){
		/**Variable to handle properties*/
		String propertieKey = null;
		
		HolderAccount accountBuyer = otcDpfRegisterTO.getAccountBuy();
		if(accountBuyer==null || accountBuyer.getIdHolderAccountPk()==null){
			propertieKey = "otc.operation.account.buyer.empty";
		}

		/**If properties key have some validation, it's time to show. GO*/
		if(propertieKey!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),PropertiesUtilities.getMessage(propertieKey));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}
		
		return false;
	}
	
	/**
	 * ***************TRANSACTIONAL METHODS TO GO AT DATABASSE*****************.
	 */
	
	/**Method to save otc operation in bd*/
	@LoggerAuditWeb
	public void saveNewMechanismOperation(){
		try{
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	registerOtcDpfOperation();break;
				case CONFIRM:  	confirmOtcDpfOperation();break;
				case APPROVE: 	approveOtcDpfOperation();break;
				case REVIEW: 	reviewOtcDpfOperation();break;
				case ANULATE: 	anulateOtcDpfOperation();break;
				case REJECT: 	rejectOtcDpfOperation();break;
				default:		break;
			}
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
		}
		JSFUtilities.showSimpleEndTransactionDialog("searchOtcDpf");
		
		if(otcDpfConsultTO.getOtcResultDataModel()!=null){
			searchOtcDpfOperations();
		}
	}
	
	
	/**
	 * Register otc dpf operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void registerOtcDpfOperation() throws ServiceException{
		OtcDpfManagementTO otcDpfOperationTO = otcDpfRegisterTO;
		attachFileSelected.setDescription(observaciones);
		attachFileSelected.setFileName(fileNameDisplay);
		MechanismOperation otcDpfOperation = otcServiceFacade.registerOtcDpfOperation(otcDpfOperationTO,attachFileSelected);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_DPF_OPERATION_REGISTER_CONFIRM_OK,
															new Object[]{otcDpfOperation.getIdMechanismOperationPk().toString()}));
	}
	
	/**
	 * Approve otc dpf operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void approveOtcDpfOperation() throws ServiceException{
		OtcDpfManagementTO otcDpfOperationTO = otcDpfRegisterTO;
		MechanismOperation otcDpfOperation = otcServiceFacade.approveOtcDpfOperation(otcDpfOperationTO);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_DPF_OPERATION_APPROVE_CONFIRM_OK,
															new Object[]{otcDpfOperation.getIdMechanismOperationPk().toString()}));
	}
	
	/**
	 * Anulate otc dpf operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void anulateOtcDpfOperation() throws ServiceException{
		OtcDpfManagementTO otcDpfOperationTO = otcDpfRegisterTO;
		MechanismOperation otcDpfOperation = otcServiceFacade.annulateOtcDpfOperation(otcDpfOperationTO);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_DPF_OPERATION_ANNULATE_CONFIRM_OK,
															new Object[]{otcDpfOperation.getIdMechanismOperationPk().toString()}));
	}
	
	/**
	 * Review otc dpf operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void reviewOtcDpfOperation() throws ServiceException{
		OtcDpfManagementTO otcDpfOperationTO = otcDpfRegisterTO;
		MechanismOperation otcDpfOperation = otcServiceFacade.reviewOtcDpfOperation(otcDpfOperationTO,true);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_DPF_OPERATION_REVIEW_CONFIRM_OK,
															new Object[]{otcDpfOperation.getIdMechanismOperationPk().toString()}));
	}
	
	/**
	 * Reject otc dpf operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void rejectOtcDpfOperation() throws ServiceException{
		OtcDpfManagementTO otcDpfOperationTO = otcDpfRegisterTO;
		MechanismOperation otcDpfOperation = otcServiceFacade.rejectOtcDpfOperation(otcDpfOperationTO);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_DPF_OPERATION_REJECT_CONFIRM_OK,
															new Object[]{otcDpfOperation.getIdMechanismOperationPk().toString()}));
	}
	
	/**
	 * Confirm otc dpf operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void confirmOtcDpfOperation() throws ServiceException{
		OtcDpfManagementTO otcDpfOperationTO = otcDpfRegisterTO;
		MechanismOperation otcDpfOperation = otcServiceFacade.confirmOtcDpfOperation(otcDpfOperationTO);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_DPF_OPERATION_CONFIRM_CONFIRM_OK,
															new Object[]{otcDpfOperation.getIdMechanismOperationPk().toString()}));
	}
	
	/**
	 * ***************END - TRANSACTIONAL METHODS TO GO AT DATABASSE*****************.
	 */
	
	/**Method to find all request*/
	@LoggerAuditWeb
	public void searchOtcDpfOperations(){
		OtcOperationTO otcOperationSearch = otcDpfConsultTO.getOtcDpfFiltersTO();
		OtcOperationTO otcDpfOperationTO = new OtcOperationTO();
		otcDpfOperationTO.setInitialDate(otcOperationSearch.getInitialDate());
		otcDpfOperationTO.setFinalDate(otcOperationSearch.getFinalDate());
		otcDpfOperationTO.setIndInCharge(otcOperationSearch.getIndInCharge());
		otcDpfOperationTO.setIdSearchDateType(otcOperationSearch.getIdSearchDateType());
		otcDpfOperationTO.setIdModalityNegotiation(otcOperationSearch.getIdModalityNegotiation());
		otcDpfOperationTO.setOtcOperationState(otcOperationSearch.getOtcOperationState());
		otcDpfOperationTO.setIndDpf(ComponentConstant.ONE);
		otcDpfOperationTO.setIdParticipantBuyer(otcOperationSearch.getIdParticipantBuyer());
		List<OtcOperationResultTO> otcOperationsList = null;
		try {
			if(!(userInfo.getUserAccountSession().isDepositaryInstitution() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()|| userInfo.getUserAccountSession().isBcbInstitution())){
				throw new ServiceException(ErrorServiceType.USERSESSION_INCORRECT_ACTION);
			}
			
			/***Get OTC DPF operations*/
			otcOperationsList = otcServiceFacade.getOtcOperationsServiceFacade(otcDpfOperationTO,parameterDescription);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(ex.getMessage()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
		if(otcOperationsList==null){
			otcDpfConsultTO.setOtcResultDataModel(new GenericDataModel<OtcOperationResultTO>());
		}else{
			otcDpfConsultTO.setOtcResultDataModel(new GenericDataModel<OtcOperationResultTO>(otcOperationsList));
		}
	}
	
	/**
	 * ***************METHODS TO HANDLE REDIRECT FROM SEARCH SCREEN TO DIFERENTS OPTIONS LIKE CONFIRM,APPROVE,ANULATE,REVIEE***********.
	 *
	 * @return the string
	 */
	public String approveSirtexAction(){
		OtcOperationResultTO sirtexOperation = otcDpfConsultTO.getOtcDpfSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateOtcDpfOperation(ViewOperationsType.APPROVE.getCode(),sirtexOperation);
	}
	
	/**
	 * Method to annulate.
	 *
	 * @return the string
	 */
	public String annulateOtcDpfAction(){
		OtcOperationResultTO sirtexOperation = otcDpfConsultTO.getOtcDpfSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateOtcDpfOperation(ViewOperationsType.ANULATE.getCode(),sirtexOperation);
	}
	
	/**
	 * Method to review operation.
	 *
	 * @return the string
	 */
	public String reviewOtcDpfAction(){
		OtcOperationResultTO sirtexOperation = otcDpfConsultTO.getOtcDpfSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateOtcDpfOperation(ViewOperationsType.REVIEW.getCode(),sirtexOperation);
	}
	
	/**
	 * Method to reject operation.
	 *
	 * @return the string
	 */
	public String rejectOtcDpfAction(){
		OtcOperationResultTO sirtexOperation = otcDpfConsultTO.getOtcDpfSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		
		return validateOtcDpfOperation(ViewOperationsType.REJECT.getCode(),sirtexOperation);
	}
	
	/**
	 * Method to confirm action.
	 *
	 * @return the string
	 */
	public String confirmOtcDpfAction(){
		OtcOperationResultTO sirtexOperation = otcDpfConsultTO.getOtcDpfSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		
		return validateOtcDpfOperation(ViewOperationsType.CONFIRM.getCode(),sirtexOperation);
	}
	
	/**
	 * Methods to validate and go at database.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the string
	 */
	public String viewOtcDpfOperationDetail(OtcOperationResultTO sirtexOperation){
		return validateOtcDpfOperation(ViewOperationsType.CONSULT.getCode(),sirtexOperation);
	}
	
	/**
	 * Validate otc dpf operation.
	 *
	 * @param viewOperationType the view operation type
	 * @param otcOperation the otc operation
	 * @return the string
	 */
	public String validateOtcDpfOperation(Integer viewOperationType, OtcOperationResultTO otcOperation){
		Long mechanismID = otcOperation.getIdMechanismOperationRequestPk();
		MechanismOperation mechanismOperation = null;
		MechanismOperationAdj mechanismOperationAdj = null;
		Boolean userIsParticipant = userInfo.getUserAccountSession().isParticipantInstitucion();
		Long participantCode = userInfo.getUserAccountSession().getParticipantCode();
		
		List<Integer> statesValidate = new ArrayList<Integer>();
		switch(ViewOperationsType.get(viewOperationType)){
			case APPROVE: case ANULATE:
					statesValidate.add(OtcOperationStateType.REGISTERED.getCode());break;
			case REVIEW:
					statesValidate.add(OtcOperationStateType.APROVED.getCode());break;
			case CONFIRM: 
					statesValidate.add(OtcOperationStateType.REVIEWED.getCode());break;
			case REJECT:
				statesValidate.add(OtcOperationStateType.REVIEWED.getCode());
				statesValidate.add(OtcOperationStateType.APROVED.getCode());break;
			default:
				break;
		}
		
		try {
			/**GET THE MECHANISM OPERATION*/
			mechanismOperation = otcServiceFacade.getMechanismOperation(mechanismID);
			mechanismOperationAdj = otcServiceFacade.getMechanismOperationAdj(mechanismID);
			if(mechanismOperation.getBuyerParticipant().getIdParticipantPk().equals(mechanismOperation.getSellerParticipant().getIdParticipantPk())){
				mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.CRUSADE.getCode());
			}else{
				mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.SALE.getCode());
			}
			
			Integer operationState = mechanismOperation.getOperationState();
			
			if(!statesValidate.isEmpty() && !statesValidate.contains(operationState)){
				throw new ServiceException(ErrorServiceType.TEMPLATE_INCORRECT_STATE);
			}
			if(mechanismOperationAdj!=null){
				attachFileSelected = mechanismOperationAdj;
				attachFileSelected.setMechanismOperation(mechanismOperation);
				observaciones = attachFileSelected.getDescription();
				fileNameDisplay = attachFileSelected.getFileName();
			}
			
			setViewOperationType(viewOperationType);

			/**SI USUARIO ES PARTICIPANTE*/
			if(userIsParticipant){
				Long participantPermited = null;
				switch(OtcOperationStateType.get(operationState)){
					case REGISTERED:
						participantPermited = mechanismOperation.getSellerParticipant().getIdParticipantPk();break;
					case APROVED: case REVIEWED:
						participantPermited = mechanismOperation.getBuyerParticipant().getIdParticipantPk();break;
					default:
						break;
				}
				
				if(participantPermited!=null && !participantPermited.equals(participantCode)){
					throw new ServiceException(ErrorServiceType.TRADE_OPERATION_PARTICIPANT_INCORRECT_ACTION);
				}
			}

			/**Setup instance to view object*/
			otcDpfRegisterTO = new OtcDpfManagementTO();			
			otcDpfRegisterTO.setOtcDpfOperation(mechanismOperation);
			otcDpfRegisterTO.setAccountBuy(new HolderAccount()); 
			
			otcDpfRegisterTO.setLstCboOtcOriginRequestType(otcDpfConsultTO.getLstCboOtcOriginRequestType());
			
			/**CARGAMOS PARAMETROS PARA EL REGISTRO DE OTC*/
			Map<Long,Participant> allParticipantMap = otcDpfConsultTO.getAllParticipantMap();
			otcDpfRegisterTO.setParticipantBuyList(Arrays.asList(allParticipantMap.get(mechanismOperation.getBuyerParticipant().getIdParticipantPk())));
			otcDpfRegisterTO.setParticipantSaleList(Arrays.asList(allParticipantMap.get(mechanismOperation.getSellerParticipant().getIdParticipantPk())));
			
			/**Load account operations*/
			for(HolderAccountOperation accOperation: mechanismOperation.getHolderAccountOperations()){
				if(accOperation.getRole().equals(ComponentConstant.PURCHARSE_ROLE)){
					if(accOperation.getHolderAccount()!=null){						
						otcDpfRegisterTO.setAccountBuy(accOperation.getHolderAccount());						
						otcDpfRegisterTO.setBuyerHolderAccounts(new ArrayList<HolderAccount>());
						otcDpfRegisterTO.getBuyerHolderAccounts().add(accOperation.getHolderAccount());
						otcDpfRegisterTO.getBuyerHolderAccounts().get(0).setSelected(true);
					}
				}else if(accOperation.getRole().equals(ComponentConstant.SALE_ROLE)){
					otcDpfRegisterTO.setAccountSale(accOperation.getHolderAccount());
					otcDpfRegisterTO.setSellerHolderAccounts(new ArrayList<HolderAccount>());
					otcDpfRegisterTO.getSellerHolderAccounts().add(accOperation.getHolderAccount());
					otcDpfRegisterTO.getSellerHolderAccounts().get(0).setSelected(true);
				}				
			}			
			return OTC_RULE;
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
		return "";
	}
	
	/**
	 * Method to show security with all finances data.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityFinancialData(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		/***Call and show the component UI*/
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * Method to load privileges.
	 */
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnReview(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		if(userPrivilege.getUserAcctions().isSettlement() && userPrivilege.getUserAcctions().isConfirm()){
			/**WHEN THE USER HAS CONFIRM PRIVILEGE, THEN IT'S COULD SETTLEMENT OPERATIONS*/
			privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Validate is valid user.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the list
	 */
	private List<String> validateIsValidUser(OtcOperationResultTO sirtexOperation) { 
		  List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		  List<String> lstOperationUserResult; 
		  OperationUserTO operationUserTO = new OperationUserTO();  
		  operationUserTO.setOperNumber(sirtexOperation.getIdMechanismOperationRequestPk().toString());
		  operationUserTO.setUserName(sirtexOperation.getRegisterUser());
		  lstOperationUserParam.add(operationUserTO);
		  lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		  return lstOperationUserResult;
	}
	
	/**
	 * Validate source holder account.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSellerHolderAccount() throws ServiceException {		
		if(otcDpfRegisterTO.getAccountSale() != null && otcDpfRegisterTO.getAccountSale().getIdHolderAccountPk() != null){
			if(HolderAccountStatusType.CLOSED.getCode().equals(otcDpfRegisterTO.getAccountSale().getStateAccount())){				
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_ACCOUNT_SELLER_CLOSED);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();				
				otcDpfRegisterTO.setAccountSale(null);
				return;
			}
		}
	}
	
	/**
	 * Validate source holder account.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateBuyerHolderAccount() throws ServiceException {		
		if(otcDpfRegisterTO.getAccountBuy() != null && otcDpfRegisterTO.getAccountBuy().getIdHolderAccountPk() != null){
			if(HolderAccountStatusType.CLOSED.getCode().equals(otcDpfRegisterTO.getAccountBuy().getStateAccount())){				
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_ACCOUNT_SELLER_CLOSED);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();				
				otcDpfRegisterTO.setAccountBuy(null);
				return;
			}
		}
	}
	/**
	 * Validate source holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSellerHolder() throws ServiceException{
		otcDpfRegisterTO.setSellerHolderAccounts(null);
		otcDpfRegisterTO.setAccountSale(new HolderAccount());
		if(otcDpfRegisterTO.getSellerHolder() != null && otcDpfRegisterTO.getSellerHolder().getIdHolderPk() != null){
			// verify if is registered or blocked
			if(!otcDpfRegisterTO.getSellerHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode()) && 
					!otcDpfRegisterTO.getSellerHolder().getStateHolder().equals(HolderStateType.BLOCKED.getCode()) ){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();				
				otcDpfRegisterTO.setSellerHolder(new Holder());
				return;
			}
			
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(otcDpfRegisterTO.getOtcDpfOperation().getSellerParticipant().getIdParticipantPk());
			holderAccountTO.setIdHolderPk(otcDpfRegisterTO.getSellerHolder().getIdHolderPk());
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.TRUE);
			holderAccountTO.setNeedBanks(Boolean.FALSE);			
			List<HolderAccount> accountList = otcServiceFacade.getHolderAccounts(holderAccountTO);
			if(accountList.size() > 0 ){				
				otcDpfRegisterTO.setSellerHolderAccounts(accountList);
				//check true when accountList return only one account
				if(otcDpfRegisterTO.getSellerHolderAccounts().size() == ComponentConstant.ONE){
					otcDpfRegisterTO.getSellerHolderAccounts().get(0).setSelected(true);
					HolderAccount ha = otcDpfRegisterTO.getSellerHolderAccounts().get(0);
					ha.setSelected(true);
					otcDpfRegisterTO.setAccountSale(ha);
					//call validation of holder accounts
					validateSellerHolderAccount();
				}
			}else{
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_NO_ACCOUNTS_FOR_PARTICIPANT);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();				
				otcDpfRegisterTO.setSellerHolder(new Holder());
				return;
			}
			
		}
	}
	
	
	
	/**
	 * Validate source holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateBuyerHolder() throws ServiceException{
		otcDpfRegisterTO.setBuyerHolderAccounts(null);
		otcDpfRegisterTO.setAccountBuy(new HolderAccount());
		if(otcDpfRegisterTO.getBuyerHolder() != null && otcDpfRegisterTO.getBuyerHolder().getIdHolderPk() != null){
			// verify if is registered or blocked
			if(!otcDpfRegisterTO.getBuyerHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode()) && 
					!otcDpfRegisterTO.getBuyerHolder().getStateHolder().equals(HolderStateType.BLOCKED.getCode()) ){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();				
				otcDpfRegisterTO.setBuyerHolder(new Holder());
				return;
			}
			
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(otcDpfRegisterTO.getOtcDpfOperation().getBuyerParticipant().getIdParticipantPk());
			holderAccountTO.setIdHolderPk(otcDpfRegisterTO.getBuyerHolder().getIdHolderPk());
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.TRUE);
			holderAccountTO.setNeedBanks(Boolean.FALSE);			
			List<HolderAccount> accountList = otcServiceFacade.getHolderAccounts(holderAccountTO);
			if(accountList.size() > 0 ){				
				otcDpfRegisterTO.setBuyerHolderAccounts(accountList);
				//check true when accountList return only one account
				if(otcDpfRegisterTO.getBuyerHolderAccounts().size() == ComponentConstant.ONE){
					otcDpfRegisterTO.getBuyerHolderAccounts().get(0).setSelected(true);
					HolderAccount ha = otcDpfRegisterTO.getBuyerHolderAccounts().get(0);
					ha.setSelected(true);
					otcDpfRegisterTO.setAccountBuy(ha);
					//call validation of holder accounts
					validateBuyerHolderAccount();
				}
			}else{
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_NO_ACCOUNTS_FOR_PARTICIPANT);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();				
				otcDpfRegisterTO.setBuyerHolder(new Holder());
				return;
			}
			
		}
	}
	
	/**
	 * Gets the otc dpf register to.
	 *
	 * @return the otc dpf register to
	 */
	public OtcDpfManagementTO getOtcDpfRegisterTO() {
		return otcDpfRegisterTO;
	}
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Sets the otc dpf register to.
	 *
	 * @param otcDpfRegisterTO the new otc dpf register to
	 */
	public void setOtcDpfRegisterTO(OtcDpfManagementTO otcDpfRegisterTO) {
		this.otcDpfRegisterTO = otcDpfRegisterTO;
	}

	/**
	 * Gets the otc dpf consult to.
	 *
	 * @return the otc dpf consult to
	 */
	public OtcDpfManagementTO getOtcDpfConsultTO() {
		return otcDpfConsultTO;
	}

	/**
	 * Sets the otc dpf consult to.
	 *
	 * @param otcDpfConsultTO the new otc dpf consult to
	 */
	public void setOtcDpfConsultTO(OtcDpfManagementTO otcDpfConsultTO) {
		this.otcDpfConsultTO = otcDpfConsultTO;
	}

	/**
	 * Gets the parameter description.
	 *
	 * @return the parameter description
	 */
	public Map<Integer, String> getParameterDescription() {
		return parameterDescription;
	}

	/**
	 * Sets the parameter description.
	 *
	 * @param parameterDescription the parameter description
	 */
	public void setParameterDescription(Map<Integer, String> parameterDescription) {
		this.parameterDescription = parameterDescription;
	}

	/**
	 * @return the mapParameters
	 */
	public HashMap<Integer, ParameterTable> getMapParameters() {
		return mapParameters;
	}

	/**
	 * @param mapParameters the mapParameters to set
	 */
	public void setMapParameters(HashMap<Integer, ParameterTable> mapParameters) {
		this.mapParameters = mapParameters;
	}
	
}