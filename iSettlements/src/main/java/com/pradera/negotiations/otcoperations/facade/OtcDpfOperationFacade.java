package com.pradera.negotiations.otcoperations.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationAdj;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.negotiations.operations.to.NegotiationOperationTO;
import com.pradera.negotiations.otcoperations.service.OtcOperationServiceBean;
import com.pradera.negotiations.otcoperations.to.OtcDpfManagementTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationResultTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationTO;
import com.pradera.negotiations.sirtex.facade.SirtexOperationFacade;
import com.pradera.negotiations.sirtex.service.OtcAndSirtexSettlementService;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class OtcDpfOperationFacade {
	

	/** The otc operation service. */
	@EJB private OtcOperationServiceBean otcOperationService;
	
	/** The otc operation facade. */
	@EJB private OtcOperationServiceFacade otcOperationFacade;
	
	/** The operation validation service. */
	@EJB private NegotiationOperationServiceBean operationValidationService;
	
	/** The otc settlement service. */
	@EJB private OtcAndSirtexSettlementService otcSettlementService;
	
	/** The sirtex facade. */
	@EJB private SirtexOperationFacade sirtexFacade;
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The holder account component service bean. */
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	/**
	 * *
	 * Method to setup new Mechanism Operation.
	 *
	 * @param idParticipantPkForIssuer the id participant pk for issuer
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation setupNewMechanismOperation(Long idParticipantPkForIssuer) throws ServiceException{
		MechanismOperation operation = new MechanismOperation();
		operation.setOriginRequest(OtcOperationOriginRequestType.CRUSADE.getCode());
		operation.setOperationDate(CommonsUtilities.currentDate());
		
		Participant pSeller = new Participant();
		Participant pBuyer = new Participant();

		if(Validations.validateIsNotNullAndPositive(idParticipantPkForIssuer)){
			pSeller.setIdParticipantPk(idParticipantPkForIssuer);
			pBuyer.setIdParticipantPk(idParticipantPkForIssuer);
		}
		Security security = new Security();
		
		operation.setSellerParticipant(pSeller);
		operation.setBuyerParticipant(pBuyer);
		operation.setSecurities(security);
		
		/**Assignment mechanism and modality by default*/
		Long mechanismCode = NegotiationMechanismType.OTC.getCode();
		Long modalityCode =  NegotiationModalityType.CASH_FIXED_INCOME.getCode();
		/**Assignment Negotiation mechanism at real operation**/
		MechanismModality mechanismModality = new MechanismModality();
		mechanismModality.getId().setIdNegotiationMechanismPk(mechanismCode);
		mechanismModality.getId().setIdNegotiationModalityPk(modalityCode);
		/**Put mechanism modality in main operation*/
		operation.setMechanisnModality(mechanismModality);
		
		return operation;
	}

	/**
	 * Method to get participant from mechanism modality.
	 *
	 * @return the participant for mechanism
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantForMechanism() throws ServiceException{
		Long mechanism = NegotiationMechanismType.OTC.getCode();
		Long modality = NegotiationModalityType.CASH_FIXED_INCOME.getCode();
		return otcOperationService.getParticipantFromMechanismModality(mechanism, modality);
	}
	
	/**
	 * Method to validate account seller*.
	 *
	 * @param holderAccount the holder account
	 * @throws ServiceException the service exception
	 */
	public void validateHolderAccountSeller(HolderAccount holderAccount) throws ServiceException{
		Long accountPk = holderAccount.getIdHolderAccountPk();
		HolderAccount account = otcOperationService.find(HolderAccount.class, accountPk);
		
		/**Validate account state*/
		if(!account.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_NOT_REGISTERED,new Object[]{account.getAccountNumber().toString()});
		}
		
		/**Validate state of holders*/
		for(HolderAccountDetail holderDetail : account.getHolderAccountDetails()){
			Holder holder = holderDetail.getHolder();
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED,new Object[]{holder.getIdHolderPk().toString()});
			}
		}
	}
	
	/**
	 * Method to validate account Buyer*.
	 *
	 * @param holderAccount the holder account
	 * @param holderAccountSale the holder account sale
	 * @throws ServiceException the service exception
	 */
	public void validateHolderAccountBuyer(HolderAccount holderAccount, HolderAccount holderAccountSale) throws ServiceException{
		Long accountPk = holderAccount.getIdHolderAccountPk();
		Long accountSalePk = holderAccountSale.getIdHolderAccountPk();
		HolderAccount account = otcOperationService.find(HolderAccount.class, accountPk);
		HolderAccount accountSale = otcOperationService.find(HolderAccount.class, accountSalePk);
		
		/**Validate account state*/
		if(!account.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_NOT_REGISTERED,new Object[]{account.getAccountNumber().toString()});
		}
		
		/**Validate state of holders*/
		for(HolderAccountDetail holderDetail : account.getHolderAccountDetails()){
			Holder holder = holderDetail.getHolder();
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED,new Object[]{holder.getIdHolderPk().toString()});
			}
		}
		
		/**Validate if holder exists on other account*/
//		for(HolderAccountDetail accountDetail: account.getHolderAccountDetails()){
//			/**Getting cui code buyer*/
//			Long cuiBuyer = accountDetail.getHolder().getIdHolderPk();
//			/***Iterate  sales account */
//			for(HolderAccountDetail accountSaleDetail: accountSale.getHolderAccountDetails()){
//				/**Getting cui code sale*/
//				Long cuiSale = accountSaleDetail.getHolder().getIdHolderPk();
//				/**If holder exist either in buyer or sale*/
//				if(cuiBuyer.equals(cuiSale)){
//					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_HOLDER_DUPLICATED);
//				}				
//			}
//		}
		if(holderAccount.getIdHolderAccountPk().equals(holderAccountSale.getIdHolderAccountPk())){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_HOLDERACCOUNT_DUPLICATED);
		}		
	}
	
	/**
	 * Method to validate Security code.
	 *
	 * @param otcDpfOperation the otc dpf operation
	 * @param accountSeller the account seller
	 * @throws ServiceException the service exception
	 */
	public void validateSecurityCode(MechanismOperation otcDpfOperation, HolderAccount accountSeller) throws ServiceException{
		/**Getting security from operation*/
		Security security = otcDpfOperation.getSecurities();
		/**If is different than null, is necessary validate security information*/
		if(security.getIdSecurityCodePk()!=null){
			/**Getting security with all data*/
			Security securityTmp = otcOperationService.findSecurityFromCode(security);
			/**SI LOS INDICADORES DE NEGOCIACION COMO NATURAL O JURIDICO SON NULL*/
			if(securityTmp==null || securityTmp.getCurrency()==null || CurrencyType.get(securityTmp.getCurrency())==null ||
			   securityTmp.getIndTraderSecondary()==null || securityTmp.getCirculationAmount()==null ||
			   securityTmp.getInstrumentType()==null || securityTmp.getSecurityInvestor()==null ||
			   securityTmp.getSecurityInvestor().getIndJuridical()==null || securityTmp.getSecurityInvestor().getIndNatural()==null ){			
				throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
			}
			/**SI ES RTA FIJA Y NO TIENE FECHA DE EXPIRACION*/
			if(securityTmp.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()) && securityTmp.getExpirationDate()==null){
				throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
			}
			
			/**Verified state of security*/
			if(!securityTmp.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
			}
			
			/**Verify state of issuer*/
			if(securityTmp.getIssuer()==null){
				throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
			}else{
				/**Verify issuer's state*/
				if(!securityTmp.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
				}
			}
			
			/**Verify state of issuance*/
			if(securityTmp.getIssuance()==null){
				throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
			}else{
				/** Verify issuance's state*/
				if(!securityTmp.getIssuance().getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
					throw new ServiceException(ErrorServiceType.ISSUANCE_BLOCK);
				}
			}
			
			/**setup to know if security can be settled by OTC*/
			NegotiationOperationTO negotiationOperationTO = new NegotiationOperationTO();
			negotiationOperationTO.setIdSecurityCode(securityTmp.getIdSecurityCodePk());
			negotiationOperationTO.setIdNegotiationMechanismPk(otcDpfOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk());
			negotiationOperationTO.setIdNegotiationModalityPk(otcDpfOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
			
			/**If security can't settled by his own mechanism modality*/
			if(operationValidationService.getSecurityByModalities(negotiationOperationTO)==null){
				/**Throw exception of privileges*/
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_MECHANISM);
			}
			
			/**Validate if seller have balances*/
			Participant p = otcDpfOperation.getSellerParticipant();
			Security s = securityTmp;
			HolderAccount account = accountSeller;
			/**Getting holder account balance*/
			HolderAccountBalance hab = otcOperationService.getBalanceFromSecurity(p, account, s, true);
			/**verify if balance is greater than 1*/
			if(hab==null || !hab.getAvailableBalance().equals(BigDecimal.ONE)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_PARTICIPANT_WITHOUT_BALANCE);
			}
			/**Assignment security temporal at main operation*/
			otcDpfOperation.setSecurities(securityTmp);
		}
	}
	
	/**
	 * *
	 * Method to register OTC DPF Operation.
	 *
	 * @param otcDpfOperation the otc dpf operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REGISTER)
	public MechanismOperation registerOtcDpfOperation(OtcDpfManagementTO otcDpfOperation, MechanismOperationAdj mechanismOperationAdj) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		Security security = otcDpfOperation.getOtcDpfOperation().getSecurities();
		Participant p_sell = otcDpfOperation.getOtcDpfOperation().getSellerParticipant();
		Participant p_buy = otcDpfOperation.getOtcDpfOperation().getBuyerParticipant();
		HolderAccount a_sell = otcDpfOperation.getAccountSale();
		HolderAccount a_buy = otcDpfOperation.getAccountBuy();
		Boolean accountBuyEmpty = a_buy.getIdHolderAccountPk()==null;
		
		/**Fill mechanismOperation With data*/
		MechanismOperation operation = otcOperationFacade.fillMechanismOperation(security, p_sell, p_buy, a_sell, accountBuyEmpty?null:a_buy,null);
		operation.setIndDpf(ComponentConstant.ONE);
		/**Create MechanismOperation*/
		otcOperationService.createNewMechanismOperation(operation, mechanismOperationAdj);
		return operation;
	}
	
	public ParameterTable findParameterTableLongTxt() throws ServiceException{
		return otcOperationService.findParameterTableLongTxt();
	}
	
	/**
	 * *
	 * Method to approve OTC DPF Operation.
	 *
	 * @param otcDpfOperation the otc dpf operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_APPROVE)
	public MechanismOperation approveOtcDpfOperation(OtcDpfManagementTO otcDpfOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		/**Getting object from TO*/
		MechanismOperation otcDpf = otcOperationService.find(MechanismOperation.class, otcDpfOperation.getOtcDpfOperation().getIdMechanismOperationPk());
		/**Validate otc Dpf*/
		operationValidationService.validateMechanismOperation(otcDpf);
		operationValidationService.validateOtcOperationState(otcDpf, Arrays.asList(OtcOperationStateType.REGISTERED.getCode()));
		/**Approve otc Dpf*/
		return otcOperationService.approveOtcOpereationServiceBean(otcDpf);
	}
	
	/**
	 * *
	 * Method to cancel OTC DPF Operation.
	 *
	 * @param otcDpfOperation the otc dpf operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_ANNULATE)
	public MechanismOperation annulateOtcDpfOperation(OtcDpfManagementTO otcDpfOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		
		/**Getting object from TO*/
		MechanismOperation otcDpf = otcOperationService.find(MechanismOperation.class, otcDpfOperation.getOtcDpfOperation().getIdMechanismOperationPk());
		/**Validate otc Dpf*/
		operationValidationService.validateOtcOperationState(otcDpf, Arrays.asList(OtcOperationStateType.REGISTERED.getCode()));
		/**cancel OTC DPF operation*/
		return otcOperationService.anulateOtcOperationServiceBean(otcDpf);
	}
	
	/**
	 * *
	 * Method to review OTC DPF operation.
	 *
	 * @param otcDpfOperation the otc dpf operation
	 * @param assignHolderAccount the assign holder account
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REVIEW)
	public MechanismOperation reviewOtcDpfOperation(OtcDpfManagementTO otcDpfOperation, boolean assignHolderAccount) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());

		/**Getting operation from BD*/
		MechanismOperation otcDpf = otcOperationService.find(MechanismOperation.class, otcDpfOperation.getOtcDpfOperation().getIdMechanismOperationPk());
		/**Applying validation of state*/
		operationValidationService.validateOtcOperationState(otcDpf, Arrays.asList(OtcOperationStateType.APROVED.getCode()));
		/**Variable to know account seller*/
		HolderAccount accountSeller = null;
		/**If review has new holder account*/
		if(assignHolderAccount){
			/**Getting holder account from to*/
			HolderAccount accountBuy = otcDpfOperation.getAccountBuy();
			/**Iterate account operation detail*/
			for(HolderAccountOperation accountOperation: otcDpf.getHolderAccountOperations()){
				/**Only if is buyer*/
//				if(accountOperation.getRole().equals(ComponentConstant.PURCHARSE_ROLE)){
					/**Update holder Account*/
//					accountOperation.setHolderAccount(accountBuy);
					/**Update on BD*/
//					otcOperationService.update(accountOperation);
				/*}else*/ 
				if(accountOperation.getRole().equals(ComponentConstant.SALE_ROLE)){
					/**Assignment account seller*/
					accountSeller = accountOperation.getHolderAccount();
				}
			}
			
			/**Assignment holder account operation at OTC operation*/
			HolderAccountOperation accountOperation = new HolderAccountOperation();
			accountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
			accountOperation.setRegisterUser(loggerUser.getUserName());
			accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());			
			accountOperation.setHolderAccount(accountBuy);
			accountOperation.setIndIncharge(BooleanType.NO.getCode());
			accountOperation.setInchargeFundsParticipant(otcDpf.getBuyerParticipant());
			accountOperation.setInchargeStockParticipant(otcDpf.getBuyerParticipant());
			accountOperation.setStockQuantity(otcDpf.getStockQuantity());
			accountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
			accountOperation.setMechanismOperation(otcDpf);
			accountOperation.setRole(ComponentConstant.PURCHARSE_ROLE);
			accountOperation.setOperationPart(OperationPartType.CASH_PART.getCode());
			accountOperation.setCashAmount(otcDpf.getCashAmount());
			accountOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
			accountOperation.setSettlementAmount(otcDpf.getCashPrice().multiply(accountOperation.getStockQuantity()));
			
			/**Insert account operation at BD*/
			otcOperationService.create(accountOperation);
			
			/**Getting account market fact*/
			Long participantCode = otcDpf.getSellerParticipant().getIdParticipantPk();
			String securityCode  = otcDpf.getSecurities().getIdSecurityCodePk();
			Long accountPk       = accountSeller.getIdHolderAccountPk();
			/**getting list of account market fact*/
			List<HolderMarketFactBalance> accountMarketFact = otcOperationFacade.getMarketFactBalance(participantCode, securityCode, accountPk);
			/**VERIFICAMOS SI EXISTEN HECHOS DE MERCADO*/
			for(HolderMarketFactBalance accTradeMarketFact: accountMarketFact){
				AccountOperationMarketFact accountMarkFact = new AccountOperationMarketFact();
				accountMarkFact.setMarketDate(accTradeMarketFact.getMarketDate());
				accountMarkFact.setMarketRate(accTradeMarketFact.getMarketRate());
				accountMarkFact.setMarketPrice(accTradeMarketFact.getMarketPrice());
				accountMarkFact.setOperationQuantity(accountOperation.getStockQuantity());
				accountMarkFact.setHolderAccountOperation(accountOperation);
				
				/**save account operation market fact*/
				otcOperationService.create(accountMarkFact);
//				accountOperation.getAccountOperationMarketFacts().add(accountMarkFact);
			}
//			otcDpf.getHolderAccountOperations().add(accountOperation);
		}
		
		otcDpf.getTradeOperation().setReviewDate(CommonsUtilities.currentDateTime());
		otcDpf.getTradeOperation().setReviewUser(loggerUser.getUserName());
		otcDpf.setOperationState(OtcOperationStateType.REVIEWED.getCode());
		otcDpf.getTradeOperation().setOperationState(OtcOperationStateType.REVIEWED.getCode());
		
		/**Update main operation on BD*/
		otcOperationService.update(otcDpf);
		return otcDpf;
	}
	
	/**
	 * *
	 * Method to reject OTC DPF operation.
	 *
	 * @param otcDpfOperation the otc dpf operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_REJECT)
	public MechanismOperation rejectOtcDpfOperation(OtcDpfManagementTO otcDpfOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		/**Getting object from TO*/
		MechanismOperation otcDpf = otcOperationService.find(MechanismOperation.class, otcDpfOperation.getOtcDpfOperation().getIdMechanismOperationPk());
		/**Validate otc Dpf*/
		operationValidationService.validateOtcOperationState(otcDpf, Arrays.asList(OtcOperationStateType.APROVED.getCode(),OtcOperationStateType.REVIEWED.getCode()));
		/**Reject OTC DPF operation*/
		return otcOperationService.rejectOtcOperationServiceBean(otcDpf);
	}
	
	/**
	 * *
	 * Method to confirm OTC DPF operation.
	 *
	 * @param otcDpfOperation the otc dpf operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_CONFIRM)
	public MechanismOperation confirmOtcDpfOperation(OtcDpfManagementTO otcDpfOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		/**Getting object from TO*/
		MechanismOperation otcDpf = otcOperationService.find(MechanismOperation.class, otcDpfOperation.getOtcDpfOperation().getIdMechanismOperationPk());
		/**apply validation of state*/
		operationValidationService.validateOtcOperationState(otcDpf, Arrays.asList(OtcOperationStateType.REVIEWED.getCode()));
		/**Confirm MechanismOperation*/
		otcOperationService.confirmOtcMechanismOperation(otcDpf);
		/**loock balance of mechanismOperation*/ 
		Integer operationPart = ComponentConstant.CASH_PART;
		Long operationPk 	  = otcDpf.getIdMechanismOperationPk(); 
		otcSettlementService.lockBalancesOfOperation(operationPk, operationPart);
		/**settlement balance of mechanismOperation*/
		otcSettlementService.settlementBalancesOfOperation(operationPk, operationPart, otcDpf.getCashSettlementDate());
		/**Validate if all balances have been settled*/
		sirtexFacade.validateOperationSettlements(Arrays.asList(operationPk),operationPart);
		sendSettlementTransferOperationWebClientAsync(otcDpf,otcDpfOperation);
		return otcDpf;
	}
	
	@Asynchronous
	public void sendSettlementTransferOperationWebClientAsync(MechanismOperation mechanismOperation,OtcDpfManagementTO otcDpfRegisterTO){
		sendSettlementTransferOperationWebClient(mechanismOperation, otcDpfRegisterTO);
	}
		
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_CONFIRM)
	public void sendSettlementTransferOperationWebClient(MechanismOperation mechanismOperation,OtcDpfManagementTO otcDpfRegisterTO){
		List<SettlementOperationTO> lstSettlementOperationTOs=new ArrayList<>();
		SettlementOperationTO operationTO=new SettlementOperationTO();
		operationTO.setIdSettlementOperation(BusinessProcessType.OTC_OPERATION_CONFIRM.getCode());
		operationTO.setIdMechanismOperation(mechanismOperation.getIdMechanismOperationPk());                        /////
		operationTO.setIdReferencedOperation(BusinessProcessType.OTC_OPERATION_CONFIRM.getCode());                                                                   /////
		operationTO.setIdMechanism(mechanismOperation.getIdMechanismOperationPk());                                                                             /////
		//operationTO.setIdModalityGroup(otcDpfRegisterTO.getOtcDpfTradeOperation());                                                                         /////
		operationTO.setIdModality(mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());                                                                              /////
		operationTO.setInstrumentType(mechanismOperation.getSecurities().getInstrumentType());
		operationTO.setIdSecurityCode(mechanismOperation.getSecurities().getIdSecurityCodePk());
		operationTO.setStockQuantity(mechanismOperation.getStockQuantity());
		operationTO.setSettlementType(mechanismOperation.getSettlementType());
		operationTO.setSettlementSchema(mechanismOperation.getSettlementSchema());
		operationTO.setCurrency(mechanismOperation.getCurrency());
		operationTO.setIndReportingBalance(mechanismOperation.getIndReportingBalance());
		operationTO.setIndPrincipalGuarantee(mechanismOperation.getIndPrimaryPlacement());
		operationTO.setIndMarginGuarantee(mechanismOperation.getIndMarginGuarantee());
		operationTO.setIndTermSettlement(mechanismOperation.getIndTermSettlement());
		operationTO.setIndCashStockBlock(mechanismOperation.getIndCashSettlementExchange());
		operationTO.setIndTermStockBlock(mechanismOperation.getIndTermStockBlock());
		operationTO.setIndPrimaryPlacement(mechanismOperation.getIndPrimaryPlacement());
		//operationTO.setIndDematerialization(1);                                                                     /////                        
		operationTO.setCurrentNominalValue(mechanismOperation.getNominalValue());
		//operationTO.setOperationPart(1);                                                                            /////
		//operationTO.setIndPartial(1);                                                                               /////
		//operationTO.setIndForcedPurchase(1);    																	/////
		//TODO: Seteando el tipo de transferencia segun Issue 1104
		if (operationTO.getIdModality().equals(NegotiationModalityType.CASH_FIXED_INCOME.getCode()) || 
			operationTO.getIdModality().equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode())){
			operationTO.setInterfaceTransferCode("COX");           
		}
		operationTO.setSecurityClass(mechanismOperation.getSecurities().getSecurityClass());
		operationTO.setOperationNumber(mechanismOperation.getIdMechanismOperationPk());
		operationTO.setLstBuyerHolderAccounts(getLstSellerHolderAccounts(otcDpfRegisterTO));                       //comprador                                                 /////
		operationTO.setLstSellerHolderAccounts(setLstBuyerHolderAccounts(otcDpfRegisterTO));                       //vendedor
		//operationTO.setIndCompleteSettlement(1);                                                                    /////
		lstSettlementOperationTOs.add(operationTO);
		this.sendSettlementOperationWebClient(lstSettlementOperationTOs);
	}
	public List<SettlementHolderAccountTO>  getLstSellerHolderAccounts(OtcDpfManagementTO otcDpfRegisterTO){
		HolderAccount holderAccount = otcDpfRegisterTO.getAccountBuy();
		SettlementHolderAccountTO accountTO=new SettlementHolderAccountTO();
		accountTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		List<SettlementHolderAccountTO> lstBuyerHolderAccounts=new ArrayList<>();
		lstBuyerHolderAccounts.add(accountTO);
		return lstBuyerHolderAccounts;
	}
	public List<SettlementHolderAccountTO>  setLstBuyerHolderAccounts(OtcDpfManagementTO otcDpfRegisterTO){
		HolderAccount holderAccount = otcDpfRegisterTO.getAccountBuy();
		SettlementHolderAccountTO accountTO=new SettlementHolderAccountTO();
		accountTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		List<SettlementHolderAccountTO> lstBuyerHolderAccounts=new ArrayList<>();
		lstBuyerHolderAccounts.add(accountTO);
		return lstBuyerHolderAccounts;
	}

	/**
	 * *
	 * Method to get OTC DPF operation .
	 *
	 * @param otcOperationTO the otc operation to
	 * @param maps the maps
	 * @return the otc operations service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OTC_OPERATION_QUERY)
	public List<OtcOperationResultTO> getOtcOperationsServiceFacade(OtcOperationTO otcOperationTO, Map<Integer,String> maps) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<OtcOperationResultTO> otcResultList = otcOperationService.getOtcOperationsServiceBean(otcOperationTO);
		for(OtcOperationResultTO otcResult: otcResultList){
			otcResult.setStateDescription(maps.get(otcResult.getOperationState()));
		}
		return otcResultList;
	}
	
	/**
	 * *
	 * Method to get mechanism operation from BD.
	 *
	 * @param mechanismOperationPk the mechanism operation pk
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation getMechanismOperation(Long mechanismOperationPk) throws ServiceException{
		MechanismOperation operation = otcOperationService.find(MechanismOperation.class, mechanismOperationPk);
		operation.getSecurities().toString();
		operation.getBuyerParticipant().toString();
		operation.getSellerParticipant().toString();
		for(HolderAccountOperation accOperation: operation.getHolderAccountOperations()){
			if(accOperation.getHolderAccount()!=null){
				accOperation.getHolderAccount().toString();
				accOperation.getHolderAccount().getHolderAccountDetails().toString();
			}
		}
		return operation;
	}
	public MechanismOperationAdj getMechanismOperationAdj(Long mechanismOperationPk) throws ServiceException{
		MechanismOperationAdj operation = otcOperationService.getMechanismOperationAdj(mechanismOperationPk);
		
		return operation;
	}
	
	/**
	 * Gets the holder accounts.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder accounts
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccounts(HolderAccountTO holderAccountTO) throws ServiceException {
		return holderAccountComponentServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO );
	}
	@EJB
	private SettlementProcessService settlementProcessServiceBean;
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendSettlementOperationWebClient(List<SettlementOperationTO> lstSettlementOperationTOs) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		} catch (Exception e) {
			System.out.println("ERROR EN PRIVILEGIOS");
		}
		settlementProcessServiceBean.sendSettlementOperationWebClient(lstSettlementOperationTOs, loggerUser);
	}
}