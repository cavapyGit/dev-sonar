/*
 * 
 */
package com.pradera.negotiations.otcoperations.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.negotiations.otcoperations.to.OtcSwapOperationResultTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationParameters.
 * clase que me permitira manejar los distintos attributos que cuenta 
 * la opcion de "OPERACIONES OTC, disponible tanto para Registro Manual y Massivo"
 * @author PraderaTechnologies.
 * @version 1.0 , 14/07/2013
 */
public class OtcOperationParameters implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The is part buyer disabled. */
	private Boolean isPartBuyerDisabled;//Atributo que me permite manejar si el participante comprador deberia aparecer como desabilitado
	
	/** The is settlement plaze. */
	private Boolean isSettlementPlaze;//Attributo que me permite manejar si la operacion tiene liquidacion plazo
	
	/** The is settlement plaze disabled. */
	private Boolean isSettlementPlazeDisabled;//Attributo que me permite manejar si la operacion tiene liquidacion plazo, pero estara desabilitado
	
	/** The is show te. */
	private Boolean isShowTe;//Attributo que me permite manejar si tiene O no tasa efectiva
	
	/** The is hold acc neg del butt disabled. */
	private Boolean isHoldAccNegDelButtEnabled;//Attributo que me permite manejar si el boton de eliminar cuentas de negociacion puede estar Habilitado
	
	/** The participant for hold account negotiation. */
	private Participant partToHoldAccNegotiation;//Participante que me permite manejar, para la cuenta de negociacion
	
	/** The hold acc negotiation validated. */
	private List<HolderAccountOperation> holdAccNegotiationValidated;//Lista que me permite manejar la lista de Cuentas negociadoras de acuerdo al usuario Logueado
	
	/** The hold acc operation list. */
	private List<HolderAccountOperation> holdAccNegotiationSellers;//Para repo secundario, Me permite almacenar los vendedores plazo.
	
	/** The mec mod by instrument list. */
	private List<MechanismModality> mecModByInstrumentList;//Lista que me permite mostrar las modalidades de acuerdo al instrumento seleccionado

	/** The mec mod by instrument search list. */
	private List<MechanismModality> mecModByInstrumentSearchList;//Lista que me permite mostrar las modalidades de acuerdo al instrumento seleccionado, para busquedad Search
	
	/** The instrument type selected. */
	private Integer instrumentTypeSelected;//Parametro que me permite manejar, el tipo de instrumento(FIJO o VARIABLE) seleccionado en la pantalla OTC
	
	/** The instrument type selected search. */
	private Integer instrumentTypeSelectedSearch;//Parametro que me permite manejar, el tipo de instrumento(FIJO o VARIABLE) seleccionado en la pantalla OTC para la pantalla de busquedad
	
	/** The is origin request disabled. */
	private Boolean isOriginRequestDisabled;//Attributo que me permite manejar si el combo "ORIGEN DE SOLICITUD" esta O NO Deshabilitado
	
	/** The is secundary report. */
	private Boolean isSecundaryReport;//Atributo que me permite manejar si la operacion es Reporto para FieldSet con ID "flsRepoOperation", Por rendimiento es mejor manejarlo en una variable
	
	/** The is quantity pendient sec report. */
	private Boolean isQuantityPendientSecReport;//Atributo que me permite manejar si la reporto secundario tiene cantidad pendiente de asignar en la venta.
	
	/** The is swap. */
	private Boolean isSwap;//Atributo que me permite manejar si la operacion es permuta
	
	/** The participation swap. */
	private Integer participationSwap;//Attributo que me permite manejar el estado del combo para la participacion del ISIN, "VENTA o COMPRA"
	
	/** The efective tase size. */
	public static Integer EFECTIVE_TASE_SIZE = 5;//Attributo que me permite manejar el tamaño de la tasa efectiva
	
	/** The operation amount scale. */
	public static Integer OPERATION_AMOUNT_SCALE = 2;//Attributo que me permite manejar la scala de los montos en las operationes
	
	/** The is isin quantity equals. */
	private Boolean isIsinQuantityEquals;//Atributo que me permite manejar si la cantidad de isin ya fuero ingresadas
	
	/** The is swap same money. */
	private Boolean isSwapDifCurr;//Attributo que me permite manejar si la operacion Swap tiene la misma moneda
	
	/** The is variable income. */
	private Boolean isVariableIncome; //attributo que me permite manejar si la operacion renta variable;
	
	/** The otc swap operation result to. */
	private List<OtcSwapOperationResultTO> otcSwapOperationResultTO;
	
	/** The swap exchange rate. */
	private BigDecimal swapExchangeRate = BigDecimal.ZERO;//Atributo para manejar el tipo de cambio en la permuta, cuando los valores sean de moneda distinta
	
	/** The is split coupon. */
	private Boolean isSplitCoupon;//Atributo que me permite determinar si la operacion en la pantalla es Split O no
	
	/** The modality have in charge. */
	private Boolean modalityHaveInCharge;//Attributo que me permite determinar si la tiene O no encargados;
	
	/** The quantity swap isin. */
	public static Integer QUANTITY_SWAP_ISIN = 2;
	
	/** The current date. */
	private Date currentDate;//Attributo que me permite manejar la hora y fecha actual del sistema
	
	/** The participant for change. */
	private List<Participant> participantForChange;//Lista de participantes que me permite manejar los participantes disponibles para el encargo
	
	/** The all participant list. */
	private List<Participant> allParticipantList;
	
	/** The is incharged disabled. */
	private Boolean isInchargedDisabled;
	
	/** The placement segment. */
	private PlacementSegment placementSegment;//Attribuyto que me permite manejar el tramo para colocacion primaria
	
	/** The mechanism modality list. */
	private List<MechanismModality> mechanismModalityList;
	
	/** The lst negotiation modality. */
	private List<NegotiationModality> lstNegotiationModality;
	
	/** The last working date for secundary report. */
	private Date lastWorkingDateForViewOperation;//attributo que me permite almacenar el ultimo dia laborable para el plazo de repo a la vista
	
	/** The otc diferent money. */
	private Boolean otcDiferentMoney;//Atributo que me permite manejar si la operacion puede negociar con moneda diferente al valor
	
	/** The holder account issuer. */
	private HolderAccount holderAccountIssuer;//Cuenta del emisor que me permitira manejar la cuenta vendedora en la colocacion primaria
	
	/** The asigment operation list. */
	private List<AssigmentOperationTO> asigmentOperationList;//lista que me permite manbejar el total de asignacion de los participantes
	
	/** The motive cancel. */
	private Integer motiveCancel;//Parametro para manejar el motivo de cancelacion
	
	/** The other motive cancel. */
	private String otherMotiveCancel;//parametro para manejar el otro motivo de cancelacion
	
	/** The settlement panel. */
	private String settlementPanel = "frmOtcOperation:settlementAccPanel";
	
	/** The account operation panel. */
	private String accountOperationPanel = "frmOtcOperation:pnlAccount";
	
	/** The holder accounts assigment. */
	private GenericDataModel<HolderAccount> holderAccountsAssigment;
	
	/** The account selected assigment. */
	private HolderAccount accountSelectedAssigment;
	
	/** The account operation market fact. */
	private HolderAccountOperation accountOperationMarketFact;/*OBJETO PARA MANEJAR HECHOS DE MERCADO A NIVEL DE ACCOUNT OPERATION*/
	
	/** The account market fact detail. */
	private AccountOperationMarketFact accountMarketFactDetail;/**/
	
	/** The mechanism operation auxiliar. */
	private MechanismOperation mechanismOperationAuxiliar;
	/**
	 * Gets the checks if is part buyer disabled.
	 *
	 * @return the checks if is part buyer disabled
	 */
	public Boolean getIsPartBuyerDisabled() {
		return isPartBuyerDisabled;
	}
	/**
	 * Sets the checks if is part buyer disabled.
	 *
	 * @param isPartBuyerDisabled the new checks if is part buyer disabled
	 */
	public void setIsPartBuyerDisabled(Boolean isPartBuyerDisabled) {
		this.isPartBuyerDisabled = isPartBuyerDisabled;
	}
	/**
	 * Gets the checks if is settlement plaze.
	 *
	 * @return the checks if is settlement plaze
	 */
	public Boolean getIsSettlementPlaze() {
		return isSettlementPlaze;
	}
	/**
	 * Sets the checks if is settlement plaze.
	 *
	 * @param isSettlementPlaze the new checks if is settlement plaze
	 */
	public void setIsSettlementPlaze(Boolean isSettlementPlaze) {
		this.isSettlementPlaze = isSettlementPlaze;
	}
	/**
	 * Gets the checks if is show te.
	 *
	 * @return the checks if is show te
	 */
	public Boolean getIsShowTe() {
		return isShowTe;
	}
	/**
	 * Sets the checks if is show te.
	 *
	 * @param isShowTe the new checks if is show te
	 */
	public void setIsShowTe(Boolean isShowTe) {
		this.isShowTe = isShowTe;
	}
	
	/**
	 * Gets the checks if is hold acc neg del butt disabled.
	 *
	 * @return the checks if is hold acc neg del butt disabled
	 */
	public Boolean getIsHoldAccNegDelButtEnabled() {
		return isHoldAccNegDelButtEnabled;
	}
	
	/**
	 * Sets the checks if is hold acc neg del butt disabled.
	 *
	 * @param isHoldAccNegDelButtDisabled the new checks if is hold acc neg del butt disabled
	 */
	public void setIsHoldAccNegDelButtEnabled(Boolean isHoldAccNegDelButtDisabled) {
		this.isHoldAccNegDelButtEnabled = isHoldAccNegDelButtDisabled;
	}
	
	/**
	 * Gets the part for hold acc negotiation.
	 *
	 * @return the part for hold acc negotiation
	 */
	public Participant getPartToHoldAccNegotiation() {
		return partToHoldAccNegotiation;
	}
	
	/**
	 * Sets the part for hold acc negotiation.
	 *
	 * @param partForHoldAccNegotiation the new part for hold acc negotiation
	 */
	public void setPartToHoldAccNegotiation(Participant partForHoldAccNegotiation) {
		this.partToHoldAccNegotiation = partForHoldAccNegotiation;
	}
	
	/**
	 * Gets the hold acc negotiation validated.
	 *
	 * @return the hold acc negotiation validated
	 */
	public List<HolderAccountOperation> getHoldAccNegotiationValidated() {
		return holdAccNegotiationValidated;
	}
	
	/**
	 * Sets the hold acc negotiation validated.
	 *
	 * @param holdAccNegotiationValidated the new hold acc negotiation validated
	 */
	public void setHoldAccNegotiationValidated(List<HolderAccountOperation> holdAccNegotiationValidated) {
		this.holdAccNegotiationValidated = holdAccNegotiationValidated;
	}
	
	/**
	 * Gets the mec mod by instrument list.
	 *
	 * @return the mec mod by instrument list
	 */
	public List<MechanismModality> getMecModByInstrumentList() {
		return mecModByInstrumentList;
	}
	/**
	 * Sets the mec mod by instrument list.
	 *
	 * @param mecModByInstrumentList the new mec mod by instrument list
	 */
	public void setMecModByInstrumentList(
			List<MechanismModality> mecModByInstrumentList) {
		this.mecModByInstrumentList = mecModByInstrumentList;
	}
	/**
	 * Gets the instrument type list.
	 *
	 * @return the instrument type list
	 */
	public List<InstrumentType> getInstrumentTypeList(){
		return InstrumentType.list;
	}
	/**
	 * Gets the instrument type selected.
	 *
	 * @return the instrument type selected
	 */
	public Integer getInstrumentTypeSelected() {
		return instrumentTypeSelected;
	}
	/**
	 * Sets the instrument type selected.
	 *
	 * @param instrumentTypeSelected the new instrument type selected
	 */
	public void setInstrumentTypeSelected(Integer instrumentTypeSelected) {
		this.instrumentTypeSelected = instrumentTypeSelected;
	}
	/**
	 * Gets the checks if is settlement plaze disabled.
	 *
	 * @return the checks if is settlement plaze disabled
	 */
	public Boolean getIsSettlementPlazeDisabled() {
		return isSettlementPlazeDisabled;
	}
	/**
	 * Sets the checks if is settlement plaze disabled.
	 *
	 * @param isSettlementPlazeDisabled the new checks if is settlement plaze disabled
	 */
	public void setIsSettlementPlazeDisabled(Boolean isSettlementPlazeDisabled) {
		this.isSettlementPlazeDisabled = isSettlementPlazeDisabled;
	}
	/**
	 * Gets the checks if is origin request disabled.
	 *
	 * @return the checks if is origin request disabled
	 */
	public Boolean getIsOriginRequestDisabled() {
		return isOriginRequestDisabled;
	}
	/**
	 * Sets the checks if is origin request disabled.
	 *
	 * @param isOriginRequestDisabled the new checks if is origin request disabled
	 */
	public void setIsOriginRequestDisabled(Boolean isOriginRequestDisabled) {
		this.isOriginRequestDisabled = isOriginRequestDisabled;
	}
	/**
	 * Gets the checks if is secundary report.
	 *
	 * @return the checks if is secundary report
	 */
	public Boolean getIsSecundaryReport() {
		return isSecundaryReport;
	}
	/**
	 * Sets the checks if is secundary report.
	 *
	 * @param isSecundaryReport the new checks if is secundary report
	 */
	public void setIsSecundaryReport(Boolean isSecundaryReport) {
		this.isSecundaryReport = isSecundaryReport;
	}
	
	/**
	 * Gets the checks if is quantity pendient sec report.
	 *
	 * @return the checks if is quantity pendient sec report
	 */
	public Boolean getIsQuantityPendientSecReport() {
		return isQuantityPendientSecReport;
	}
	/**
	 * Sets the checks if is quantity pendient sec report.
	 *
	 * @param isQuantityPendientSecReport the new checks if is quantity pendient sec report
	 */
	public void setIsQuantityPendientSecReport(Boolean isQuantityPendientSecReport) {
		this.isQuantityPendientSecReport = isQuantityPendientSecReport;
	}
	/**
	 * Gets the hold acc negotiation sellers.
	 *
	 * @return the hold acc negotiation sellers
	 */
	public List<HolderAccountOperation> getHoldAccNegotiationSellers() {
		return holdAccNegotiationSellers;
	}
	/**
	 * Sets the hold acc negotiation sellers.
	 *
	 * @param holdAccNegotiationSellers the new hold acc negotiation sellers
	 */
	public void setHoldAccNegotiationSellers(List<HolderAccountOperation> holdAccNegotiationSellers) {
		this.holdAccNegotiationSellers = holdAccNegotiationSellers;
	}
	/**
	 * Gets the checks if is swap.
	 *
	 * @return the checks if is swap
	 */
	public Boolean getIsSwap() {
		return isSwap;
	}
	/**
	 * Sets the checks if is swap.
	 *
	 * @param isSwap the new checks if is swap
	 */
	public void setIsSwap(Boolean isSwap) {
		this.isSwap = isSwap;
	}
	/**
	 * Gets the otc swap operation result to.
	 *
	 * @return the otc swap operation result to
	 */
	public List<OtcSwapOperationResultTO> getOtcSwapOperationResultTO() {
		return otcSwapOperationResultTO;
	}
	/**
	 * Sets the otc swap operation result to.
	 *
	 * @param otcSwapOperationResultTO the new otc swap operation result to
	 */
	public void setOtcSwapOperationResultTO(List<OtcSwapOperationResultTO> otcSwapOperationResultTO) {
		this.otcSwapOperationResultTO = otcSwapOperationResultTO;
	}
	/**
	 * Gets the participation swap.
	 *
	 * @return the participation swap
	 */
	public Integer getParticipationSwap() {
		return participationSwap;
	}
	/**
	 * Sets the participation swap.
	 *
	 * @param participationSwap the new participation swap
	 */
	public void setParticipationSwap(Integer participationSwap) {
		this.participationSwap = participationSwap;
	}
	/**
	 * Gets the checks if is isin quantity equals.
	 *
	 * @return the checks if is isin quantity equals
	 */
	public Boolean getIsIsinQuantityEquals() {
		return isIsinQuantityEquals;
	}
	/**
	 * Sets the checks if is isin quantity equals.
	 *
	 * @param isIsinQuantityEquals the new checks if is isin quantity equals
	 */
	public void setIsIsinQuantityEquals(Boolean isIsinQuantityEquals) {
		this.isIsinQuantityEquals = isIsinQuantityEquals;
	}
	/**
	 * Gets the checks if is swap same money.
	 *
	 * @return the checks if is swap same money
	 */
	public Boolean getIsSwapDifCurr() {
		return isSwapDifCurr;
	}
	/**
	 * Sets the checks if is swap same money.
	 *
	 * @param isSwapSameMoney the new checks if is swap same money
	 */
	public void setIsSwapDifCurr(Boolean isSwapSameMoney) {
		this.isSwapDifCurr = isSwapSameMoney;
	}
	/**
	 * Gets the swap exchange rate.
	 *
	 * @return the swap exchange rate
	 */
	public BigDecimal getSwapExchangeRate() {
		return swapExchangeRate;
	}
	/**
	 * Sets the swap exchange rate.
	 *
	 * @param swapExchangeRate the new swap exchange rate
	 */
	public void setSwapExchangeRate(BigDecimal swapExchangeRate) {
		this.swapExchangeRate = swapExchangeRate;
	}
	/**
	 * Gets the checks if is split coupon.
	 *
	 * @return the checks if is split coupon
	 */
	public Boolean getIsSplitCoupon() {
		return isSplitCoupon;
	}
	/**
	 * Sets the checks if is split coupon.
	 *
	 * @param isSplitCoupon the new checks if is split coupon
	 */
	public void setIsSplitCoupon(Boolean isSplitCoupon) {
		this.isSplitCoupon = isSplitCoupon;
	}
	/**
	 * Gets the modality have in charge.
	 *
	 * @return the modality have in charge
	 */
	public Boolean getModalityHaveInCharge() {
		return modalityHaveInCharge;
	}
	/**
	 * Sets the modality have in charge.
	 *
	 * @param modalityHaveInCharge the new modality have in charge
	 */
	public void setModalityHaveInCharge(Boolean modalityHaveInCharge) {
		this.modalityHaveInCharge = modalityHaveInCharge;
	}
	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}
	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	/**
	 * Gets the participant for change.
	 *
	 * @return the participant for change
	 */
	public List<Participant> getParticipantForChange() {
		return participantForChange;
	}
	/**
	 * Sets the participant for change.
	 *
	 * @param participantForChange the new participant for change
	 */
	public void setParticipantForChange(List<Participant> participantForChange) {
		this.participantForChange = participantForChange;
	}
	/**
	 * Gets the checks if is incharged disabled.
	 *
	 * @return the checks if is incharged disabled
	 */
	public Boolean getIsInchargedDisabled() {
		return isInchargedDisabled;
	}
	/**
	 * Sets the checks if is incharged disabled.
	 *
	 * @param isInchargedDisabled the new checks if is incharged disabled
	 */
	public void setIsInchargedDisabled(Boolean isInchargedDisabled) {
		this.isInchargedDisabled = isInchargedDisabled;
	}
	/**
	 * Gets the placement segment.
	 *
	 * @return the placement segment
	 */
	public PlacementSegment getPlacementSegment() {
		return placementSegment;
	}
	/**
	 * Sets the placement segment.
	 *
	 * @param placementSegment the new placement segment
	 */
	public void setPlacementSegment(PlacementSegment placementSegment) {
		this.placementSegment = placementSegment;
	}
	/**
	 * Gets the all participant list.
	 *
	 * @return the all participant list
	 */
	public List<Participant> getAllParticipantList() {
		return allParticipantList;
	}
	/**
	 * Sets the all participant list.
	 *
	 * @param allParticipantList the new all participant list
	 */
	public void setAllParticipantList(List<Participant> allParticipantList) {
		this.allParticipantList = allParticipantList;
	}
	/**
	 * Gets the instrument type selected search.
	 *
	 * @return the instrument type selected search
	 */
	public Integer getInstrumentTypeSelectedSearch() {
		return instrumentTypeSelectedSearch;
	}
	/**
	 * Sets the instrument type selected search.
	 *
	 * @param instrumentTypeSelectedSearch the new instrument type selected search
	 */
	public void setInstrumentTypeSelectedSearch(Integer instrumentTypeSelectedSearch) {
		this.instrumentTypeSelectedSearch = instrumentTypeSelectedSearch;
	}
	/**
	 * Gets the mechanism modality list.
	 *
	 * @return the mechanism modality list
	 */
	public List<MechanismModality> getMechanismModalityList() {
		return mechanismModalityList;
	}
	/**
	 * Sets the mechanism modality list.
	 *
	 * @param mechanismModalityList the new mechanism modality list
	 */
	public void setMechanismModalityList(
			List<MechanismModality> mechanismModalityList) {
		this.mechanismModalityList = mechanismModalityList;
	}
	
	/**
	 * Gets the mec mod by instrument search list.
	 *
	 * @return the mec mod by instrument search list
	 */
	public List<MechanismModality> getMecModByInstrumentSearchList() {
		return mecModByInstrumentSearchList;
	}
	
	/**
	 * Sets the mec mod by instrument search list.
	 *
	 * @param mecModByInstrumentSearchList the new mec mod by instrument search list
	 */
	public void setMecModByInstrumentSearchList(
			List<MechanismModality> mecModByInstrumentSearchList) {
		this.mecModByInstrumentSearchList = mecModByInstrumentSearchList;
	}
	
	/**
	 * Gets the last working date for secundary report.
	 *
	 * @return the last working date for secundary report
	 */
	public Date getLastWorkingDateForViewOperation() {
		return lastWorkingDateForViewOperation;
	}
	
	/**
	 * Sets the last working date for secundary report.
	 *
	 * @param lastWorkingDateForSecundaryReport the new last working date for secundary report
	 */
	public void setLastWorkingDateForViewOperation(Date lastWorkingDateForSecundaryReport) {
		this.lastWorkingDateForViewOperation = lastWorkingDateForSecundaryReport;
	}
	
	/**
	 * Gets the otc diferent money.
	 *
	 * @return the otc diferent money
	 */
	public Boolean getOtcDiferentMoney() {
		return otcDiferentMoney;
	}
	
	/**
	 * Sets the otc diferent money.
	 *
	 * @param otcDiferentMoney the new otc diferent money
	 */
	public void setOtcDiferentMoney(Boolean otcDiferentMoney) {
		this.otcDiferentMoney = otcDiferentMoney;
	}
	
	/**
	 * Gets the holder account issuer.
	 *
	 * @return the holder account issuer
	 */
	public HolderAccount getHolderAccountIssuer() {
		return holderAccountIssuer;
	}
	
	/**
	 * Sets the holder account issuer.
	 *
	 * @param holderAccountIssuer the new holder account issuer
	 */
	public void setHolderAccountIssuer(HolderAccount holderAccountIssuer) {
		this.holderAccountIssuer = holderAccountIssuer;
	}
	
	/**
	 * Gets the asigment operation list.
	 *
	 * @return the asigment operation list
	 */
	public List<AssigmentOperationTO> getAsigmentOperationList() {
		return asigmentOperationList;
	}
	
	/**
	 * Sets the asigment operation list.
	 *
	 * @param asigmentOperationList the new asigment operation list
	 */
	public void setAsigmentOperationList(
			List<AssigmentOperationTO> asigmentOperationList) {
		this.asigmentOperationList = asigmentOperationList;
	}
	
	/**
	 * Gets the checks if is variable income.
	 *
	 * @return the checks if is variable income
	 */
	public Boolean getIsVariableIncome() {
		return isVariableIncome;
	}
	
	/**
	 * Sets the checks if is variable income.
	 *
	 * @param isVariableIncome the new checks if is variable income
	 */
	public void setIsVariableIncome(Boolean isVariableIncome) {
		this.isVariableIncome = isVariableIncome;
	}
	
	/**
	 * Gets the motive cancel.
	 *
	 * @return the motive cancel
	 */
	public Integer getMotiveCancel() {
		return motiveCancel;
	}
	
	/**
	 * Sets the motive cancel.
	 *
	 * @param motiveCancel the new motive cancel
	 */
	public void setMotiveCancel(Integer motiveCancel) {
		this.motiveCancel = motiveCancel;
	}
	
	/**
	 * Gets the other motive cancel.
	 *
	 * @return the other motive cancel
	 */
	public String getOtherMotiveCancel() {
		return otherMotiveCancel;
	}
	
	/**
	 * Sets the other motive cancel.
	 *
	 * @param otherMotiveCancel the new other motive cancel
	 */
	public void setOtherMotiveCancel(String otherMotiveCancel) {
		this.otherMotiveCancel = otherMotiveCancel;
	}
	
	/**
	 * Gets the settlement panel.
	 *
	 * @return the settlement panel
	 */
	public String getSettlementPanel() {
		return settlementPanel;
	}
	
	/**
	 * Sets the settlement panel.
	 *
	 * @param settlementPanel the new settlement panel
	 */
	public void setSettlementPanel(String settlementPanel) {
		this.settlementPanel = settlementPanel;
	}
	
	/**
	 * Gets the account operation panel.
	 *
	 * @return the account operation panel
	 */
	public String getAccountOperationPanel() {
		return accountOperationPanel;
	}
	
	/**
	 * Sets the account operation panel.
	 *
	 * @param accountOperationPanel the new account operation panel
	 */
	public void setAccountOperationPanel(String accountOperationPanel) {
		this.accountOperationPanel = accountOperationPanel;
	}
	
	/**
	 * Gets the account selected assigment.
	 *
	 * @return the account selected assigment
	 */
	public HolderAccount getAccountSelectedAssigment() {
		return accountSelectedAssigment;
	}
	
	/**
	 * Sets the account selected assigment.
	 *
	 * @param accountSelectedAssigment the new account selected assigment
	 */
	public void setAccountSelectedAssigment(HolderAccount accountSelectedAssigment) {
		this.accountSelectedAssigment = accountSelectedAssigment;
	}
	
	/**
	 * Gets the holder accounts assigment.
	 *
	 * @return the holder accounts assigment
	 */
	public GenericDataModel<HolderAccount> getHolderAccountsAssigment() {
		return holderAccountsAssigment;
	}
	
	/**
	 * Sets the holder accounts assigment.
	 *
	 * @param holderAccountsAssigment the new holder accounts assigment
	 */
	public void setHolderAccountsAssigment(GenericDataModel<HolderAccount> holderAccountsAssigment) {
		this.holderAccountsAssigment = holderAccountsAssigment;
	}
	
	/**
	 * Gets the account operation market fact.
	 *
	 * @return the account operation market fact
	 */
	public HolderAccountOperation getAccountOperationMarketFact() {
		return accountOperationMarketFact;
	}
	
	/**
	 * Sets the account operation market fact.
	 *
	 * @param accountOperationMarketFact the new account operation market fact
	 */
	public void setAccountOperationMarketFact(HolderAccountOperation accountOperationMarketFact) {
		this.accountOperationMarketFact = accountOperationMarketFact;
	}
	
	/**
	 * Gets the account market fact detail.
	 *
	 * @return the account market fact detail
	 */
	public AccountOperationMarketFact getAccountMarketFactDetail() {
		return accountMarketFactDetail;
	}
	
	/**
	 * Sets the account market fact detail.
	 *
	 * @param accountMarketFactDetail the new account market fact detail
	 */
	public void setAccountMarketFactDetail(
			AccountOperationMarketFact accountMarketFactDetail) {
		this.accountMarketFactDetail = accountMarketFactDetail;
	}
	
	/**
	 * Gets the mechanism operation auxiliar.
	 *
	 * @return the mechanism operation auxiliar
	 */
	public MechanismOperation getMechanismOperationAuxiliar() {
		return mechanismOperationAuxiliar;
	}
	
	/**
	 * Sets the mechanism operation auxiliar.
	 *
	 * @param mechanismOperationAuxiliar the new mechanism operation auxiliar
	 */
	public void setMechanismOperationAuxiliar(
			MechanismOperation mechanismOperationAuxiliar) {
		this.mechanismOperationAuxiliar = mechanismOperationAuxiliar;
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}
	
	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(
			List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
}