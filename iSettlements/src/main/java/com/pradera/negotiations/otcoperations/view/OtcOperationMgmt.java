package com.pradera.negotiations.otcoperations.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.core.component.operation.to.MechanismModalityTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationAdj;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationFactorsType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationDateSearchType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.negotiations.operations.calculator.OperationPriceCalculatorServiceFacade;
import com.pradera.negotiations.otcoperations.facade.OtcOperationServiceFacade;
import com.pradera.negotiations.otcoperations.to.OtcOperationResultTO;
import com.pradera.negotiations.otcoperations.to.OtcOperationTO;
import com.pradera.negotiations.otcoperations.to.OtcSwapOperationResultTO;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2014.</li>
 * </ul>
 * 
 * The Class OtcOperationMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/01/2014
 */
@DepositaryWebBean
public class OtcOperationMgmt extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7262971568117324014L;
	
	/** The kapital social. */
	public static Integer KAPITAL_SOCIAL = 0;//Valor seteado desde la bd. method "loadNegotiationFactors(OtcOperationMgmt.java)"

	/** The mechanism operation otc session. */
	private MechanismOperation mechanismOperationOtcSession;//Objeto que me permite almacenar, todos los datos de la pantalla de una operacion OTC. 
	
	/** The mechanis operation auxiliar. */
//	private MechanismOperation mechanismOperationAuxiliar;//Objeto que me permitira almacenar, un objeto OTC temporal, P.E.(en Repo secundario cuando se consulta la REPO plazo)
	
	/** The otc operations list. */
	private GenericDataModel<OtcOperationResultTO> otcOperationsList;//Me permite almacenar, los resultados de una determinada busquedad
	
	/** The otc operation session. */
	private OtcOperationResultTO otcOperationSession;
	
	/** The hold acc sell operation request. */
	private HolderAccountOperation holdAccountOperationRequest;//Me permite almacenar el objeto en el pequenio formulario antes de insertar a la grilla, para registro de OTC
	
	/** The hold account operation selected. */
	private HolderAccountOperation holdAccountOperationSelected;//Me permite almacenar el objeto que selecciona en la grilla, para el registro de OTC
	
	/** The otc operation to. */
	private OtcOperationTO otcOperationTO;
	
	/** The otc operation param session. */
	private OtcOperationParameters otcOperationParamSession;//Me permite manejar los distintos atributos de "OTC Operations"
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The participant to buy list. */
	private List<Participant> participantToBuyList;//Me permite manejar los participantes que venden, en caso de ser venta El part vendedor y comprador no son iguales
	
	/** The participant by modalities. */
	private Map<Long,List<Participant>> participantByModalities;//Me permite almacenar los participants disponibles por modalidad
	
	/** The program interest coupon list. */
	private List<ProgramInterestCoupon> programInterestCouponList;//Me permite almacenar los cupones de un determinado valor(PARA SPLIT_CUPONES)
	
	/** The lst cbo date search type. */
	private List<ParameterTable> lstCboDateSearchType,lstCboOtcOperationStateType,lstCboOtcOriginRequestType,motiveCancelOperationList, lstSettlementType, lstSchemeType, lstCurrency;
	
	/** The parameter description. */
	Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	
	/** The swap operation. */
	private SwapOperation swapOperationSession;//Atributto to handle swap Operation, on Mechanism operation
	
	/** The id participant pk for issuer. */
	private Long idParticipantPkForIssuer;
	
	/** The participant state. */
	Participant participantState = null;
	
	/** The param account type desc. */
	Map<Integer,String> paramAccountTypeDesc = new HashMap<Integer,String>();
	
	private Integer optionSelectedOneRadio;
	
	private Boolean disableStockQuantity = Boolean.FALSE;
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The accounts facade. */
	@EJB private AccountsFacade accountsFacade;
	
	/** The otc operation facade. */
	@EJB private OtcOperationServiceFacade otcOperationFacade;
	
	/** The general parameter facade. */
	@EJB private GeneralParametersFacade generalParameterFacade;
	
	/** The holiday query service bean. */
	@EJB private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The helper component facade. */
	@EJB HelperComponentFacade helperComponentFacade;
	
	/** The otc operation utils. */
	private OtcOperationUtils otcOperationUtils = new OtcOperationUtils();
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
	
	/** The market fact component ui. */
	private MarketFactBalanceHelpTO marketFactComponentUI;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	@EJB
	OperationPriceCalculatorServiceFacade operationPriceCalculatorServiceFacade;
	
	/** issue 716 campos adicionales */
	private MechanismOperationAdj attachFileSelected;
	private String fileNameDisplay;
	private String observaciones;
	private BigDecimal PporQ;
	private String textDescription;
	private transient StreamedContent fileDownload;
	
	public StreamedContent getFileDownload() {
		return fileDownload;
	}

	public void setFileDownload(StreamedContent fileDownload) {
		this.fileDownload = fileDownload;
	}

	public String getTextDescription() {
		return textDescription;
	}

	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}

	public MechanismOperationAdj getAttachFileSelected() {
		return attachFileSelected;
	}

	public void setAttachFileSelected(MechanismOperationAdj attachFileSelected) {
		this.attachFileSelected = attachFileSelected;
	}

	public String getFileNameDisplay() {
		return fileNameDisplay;
	}

	public void setFileNameDisplay(String fileNameDisplay) {
		this.fileNameDisplay = fileNameDisplay;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public BigDecimal getPporQ() {
		return PporQ;
	}

	public void setPporQ(BigDecimal pporQ) {
		PporQ = pporQ;
	}

	/**
	 * Registry otc operation handler.
	 *
	 * @return the string
	 */
	public String loadNewOtcOperationAction(){	
		try {
			
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
					&& Validations.validateIsNullOrNotPositive(idParticipantPkForIssuer)){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}
			/*VALIDAMOS TIPO DE CAMIO Y VALIDACIONES GENERICAS*/
			DailyExchangeRates dailyExchangeRates = otcOperationFacade.findExchangesRateDay(CurrencyType.USD.getCode());
			otcOperationFacade.validationsBeforeNewOperation(otcOperationParamSession.getMechanismModalityList(),participantState);

			/*INICIALIZAMOS EL OBJETO SWAP OPERATION*/
			swapOperationSession = new SwapOperation(new Security());
			swapOperationSession.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
			
			/*INICIALIZAMOS VARIABLES*/
			mechanismOperationOtcSession = otcOperationFacade.getNewMechanismOperation();//Tramos el Objeto MechanismOperation Limpio de data
			loadNewHolderAccountOperation();
			reLoadOtcOperationParameters();//Volvemos a cargar los parametros OTC
			loadMechanismOperationOtcSession();
			attachFileSelected = new MechanismOperationAdj();
			fileNameDisplay = "";
			observaciones = "";
			textDescription = "";
			fileDownload = null;
			
			/*CARGAMOS TIPO DE CAMBIO A ENTIDAD QUE MANEJARA LA OPERACION*/
			mechanismOperationOtcSession.setExchangeRate(dailyExchangeRates.getReferencePrice());//Cargamos el tipo de cambio
			
			/*REINICIAMOS LISTA DE PARTICIPANTES*/
			participantList = new ArrayList<Participant>();
			UserAccountSession userAccount = userInfo.getUserAccountSession();
			if(userAccount.isParticipantInstitucion()|| userAccount.isParticipantInvestorInstitucion()){
				 participantList.add(otcOperationFacade.findParticipantFromId(idParticipantPkForIssuer, otcOperationParamSession.getAllParticipantList()));
			}else if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
//				Participant participant = accountsFacade.getParticipantForIssuerDpfDpa(userInfo.getUserAccountSession().getIssuerCode());
				if(participantState!=null && participantState.getIdParticipantPk()!=null){
//					idParticipantPkForIssuer = participant.getIdParticipantPk();
					participantList.add(otcOperationFacade.findParticipantFromId(idParticipantPkForIssuer, otcOperationParamSession.getAllParticipantList()));
				}
			}
			
			ParameterTable objParameterTable = otcOperationFacade.findParameterTableLongTxt();
			textDescription = objParameterTable.getText3();

			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			
			return "otcOperationMgmtRule";
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
	}
	
	/**
	 * Select market fact.
	 */
	public void selectMarketFact(){
		MarketFactBalanceHelpTO marketFactBalance = getMarketFactComponentUI();
		List<HolderAccountOperation> accountAdded = otcOperationParamSession.getHoldAccNegotiationValidated();
		for(HolderAccountOperation accountOperation: accountAdded){
			if(accountOperation.getHolderAccount().getIdHolderAccountPk().equals(marketFactBalance.getHolderAccountPk())){
				accountOperation.setStockQuantity(marketFactBalance.getBalanceResult());
				accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
				
				for(MarketFactDetailHelpTO marketFactDetail: marketFactBalance.getMarketFacBalances()){
					AccountOperationMarketFact accountMarketFact = new AccountOperationMarketFact();
					accountMarketFact.setMarketDate(marketFactDetail.getMarketDate());
					accountMarketFact.setMarketRate(marketFactDetail.getMarketRate());
					accountMarketFact.setMarketPrice(marketFactDetail.getMarketPrice());
					accountMarketFact.setHolderAccountOperation(accountOperation);
					accountMarketFact.setOperationQuantity(marketFactDetail.getEnteredBalance());
					accountMarketFact.setOriginalQuantity(marketFactDetail.getEnteredBalance());
					accountOperation.getAccountOperationMarketFacts().add(accountMarketFact);
				}
			}
		}
	}
	
	/**
	 * Massive otc registry handler.
	 *
	 * @return the string
	 */
	public String massiveOtcRegistryHandler(){
		return "otcOperationMassiveMgmtRule";
	}
	
	/**
	 * Gets the participant for charge.
	 *
	 * @return the participant for charge
	 */
	public List<Participant> getParticipantForCharge(){
		//Para el caso de Encargo, obtenemos una lista de los participantes 
		List<Participant> participantList = new ArrayList<Participant>();
		//Agregamos los participantes a la lista a retornar
		participantList.addAll(this.participantList);
		OtcOperationOriginRequestType otcRequestType = OtcOperationOriginRequestType.lookup.get(mechanismOperationOtcSession.getOriginRequest());
		if(otcRequestType.equals(OtcOperationOriginRequestType.SALE)){
			if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(!OtcOperationUtils.participantIsInCharge(mechanismOperationOtcSession, participantState)){//Si el participante es encargado, solo debe asignar las cuentas
					participantList.remove(participantState);
				}
				//Si el participante es Comprador
				if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantState)){
					participantList.remove(participantState);
				}
			}else{
				//Cuando es venta, se debe mostrar todos los participantes excepcto el participante que vende
				Participant participantSeller = this.getParticipantSeller();
				participantList.remove(participantSeller);
			}
		}else if(otcRequestType.equals(OtcOperationOriginRequestType.PURCHASE)){
			//Cuando es compra, se debe mostrar todos los participantes excepcto el participante que compra
			Participant participantBuyer = this.getParticipanteBuyer();
			participantList.remove(participantBuyer);
		}else if(otcRequestType.equals(OtcOperationOriginRequestType.CRUSADE)){
			Participant participantBuyer = this.getParticipanteBuyer();//El participante comprador o vendedor siempre sera el mismo
			participantList.remove(participantBuyer);
		}
		return participantList;
	}
	/**
	 * Gets the participant seller.
	 *
	 * @return the participant seller
	 */
	public Participant getParticipantSeller(){
		//Cuando sera usuario participante es necesario obtener de Seguridad
		Long idParticipantPk = this.mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk();
		if(idParticipantPk!=null)
			for(Participant participant: otcOperationParamSession.getAllParticipantList()){
				if(participant.getIdParticipantPk().equals(idParticipantPk)){
					return participant;
				}
			}
		return null;
	}
	
	/**
	 * Clean holder account negotiation.
	 */
	public void cleanHolderAccountNegotiation(){
		if(holdAccountOperationRequest!=null){
			holdAccountOperationRequest.setHolderAccount(new HolderAccount());
			holdAccountOperationRequest.setStockQuantity(null);
			holdAccountOperationRequest.setCashAmount(null);
		}
	}
	
	public void dirtyPriceChange() {
		if(validateStockQuantity() && isPriceCalculation()) {				
			this.calculateCashSettlementDate();		
			this.operationPriceCalculatorServiceFacade.dirtyPriceChangeCalculation(mechanismOperationOtcSession);

			if(Validations.validateIsNotNull(holdAccountOperationRequest.getHolderAccount())
					&& Validations.validateIsNotNull(holdAccountOperationRequest.getHolderAccount().getHolder())
					&& Validations.validateIsNotNull(holdAccountOperationRequest.getHolderAccount().getHolder().getIdHolderPk())){
				this.changeStockQuantityAssigment();		
			}
		}
	}
	
	public void cleanCalculationPrices() {
		this.operationPriceCalculatorServiceFacade.cleanCalculationPrices(mechanismOperationOtcSession);
	}
	
	public void dirtyAmountChange() {
		if(validateStockQuantity() && isAmountCalculation()) {
			this.calculateCashSettlementDate();
			this.operationPriceCalculatorServiceFacade.dirtyAmountChangeCalculation(mechanismOperationOtcSession);			
			
			if(Validations.validateIsNotNull(holdAccountOperationRequest.getHolderAccount())
					&& Validations.validateIsNotNull(holdAccountOperationRequest.getHolderAccount().getHolder())
					&& Validations.validateIsNotNull(holdAccountOperationRequest.getHolderAccount().getHolder().getIdHolderPk())){
				this.changeStockQuantityAssigment();		
			}
		} 
	}
	
	public Boolean validateStockQuantity() {
		if(!Validations.validateIsNotNullAndPositive(mechanismOperationOtcSession.getStockQuantity())) {
			mechanismOperationOtcSession.setStockQuantity(null);
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta agregar Cantidad de Valores. Verifique."
					);
			JSFUtilities.showSimpleValidationDialog();
			this.cleanPrices();
			return Boolean.FALSE;
		} else {
			return Boolean.TRUE;
		}
	}
	
	public void cleanPriceChange() {
		this.operationPriceCalculatorServiceFacade.cleanPriceChange(mechanismOperationOtcSession);
	}
	
	/**
	 * Dummy validation to ensure update
	 */
	public void changeSettlementType() {
		mechanismOperationOtcSession.getSettlementType();
	}
	
	/**
	 * Load otc parameters.
	 * metodo para cargar, parametros OTC en estado diferente a registrado
	 */
	private void loadOtcParameters(){
		MechanismOperation operation = mechanismOperationOtcSession;
		if(NegotiationUtils.mechanismOperationIsSwap(operation)){//Verificamos si la operacion es Swap
			swapOperationSession = mechanismOperationOtcSession.getSwapOperations().get(0);
			otcOperationParamSession.setIsSwap(Boolean.TRUE);
			otcOperationParamSession.setIsIsinQuantityEquals(Boolean.TRUE);
			otcOperationParamSession.setOtcSwapOperationResultTO(OtcSwapOperationUtils.getOtcSwapOperationResultTOFromObject(mechanismOperationOtcSession));//obtenemos el to swap desde mechanismOperation
			otcOperationParamSession.setIsSwapDifCurr(OtcSwapOperationUtils.haveSameMoney(otcOperationParamSession.getOtcSwapOperationResultTO()));//Obtenemos si operacion permuta es de la misma moneda
			if(!otcOperationParamSession.getIsSwapDifCurr()){
				otcOperationParamSession.setSwapExchangeRate(mechanismOperationOtcSession.getSwapOperations().get(0).getExchangeRate());//asignamos el tipo de cambio
			}
		}else{
			otcOperationParamSession.setIsSwap(Boolean.FALSE);
			otcOperationParamSession.setIsIsinQuantityEquals(Boolean.FALSE);
			otcOperationParamSession.setIsSwapDifCurr(Boolean.TRUE);
			otcOperationParamSession.setSwapExchangeRate(null);
		}
		
		otcOperationParamSession.setIsHoldAccNegDelButtEnabled(Boolean.FALSE);//Seteamos el indicador, para NO poder eliminar cuentas desde la grilla
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())){//Si es consulta, no esta permitido agregar cuentas
			otcOperationParamSession.setPartToHoldAccNegotiation(null);
		}
		
		MechanismModality mecModality = otcOperationFacade.findMechanismModality(operation, otcOperationParamSession.getMechanismModalityList());
		
		if( mecModality != null &&
			mecModality.getNegotiationModality( )!= null &&
			mecModality.getNegotiationModality().getIndTermSettlement() != null && 
			mecModality.getNegotiationModality().getIndTermSettlement().equals(BooleanType.YES.getCode())){//Verificamos si el mecanismo con la modalidad tiene Plazo
			if(NegotiationUtils.mechanismOperationIsViewReport(operation) && (operation.getTermPrice()==null || operation.getTermPrice().equals(BigDecimal.ZERO))){
				otcOperationParamSession.setIsSettlementPlazeDisabled(Boolean.TRUE);
			}else{
				otcOperationParamSession.setIsSettlementPlazeDisabled(Boolean.FALSE);
			}
			otcOperationParamSession.setIsSettlementPlaze(Boolean.TRUE);
		}
		
		if(OtcOperationUtils.isMechanisnModalityWithInCharge(mecModality)){//Si el mecanismo y modalidad de la operacion tiene encargo
			List<Participant> particList = participantByModalities.get(mechanismOperationOtcSession.getMechanisnModality().getId().getIdNegotiationModalityPk());
			otcOperationParamSession.setParticipantForChange(OtcOperationUtils.getParticipantForCharge(mechanismOperationOtcSession, particList, holdAccountOperationRequest.getRole()));//Cargams los participantes encargados
			otcOperationParamSession.setIsInchargedDisabled(OtcOperationUtils.getInchargedDisabled(mechanismOperationOtcSession, swapOperationSession, userInfo, ViewOperationsType.get(getViewOperationType())));//Cargamos si el indicador de encargo esta en Disabled
			otcOperationParamSession.setModalityHaveInCharge(Boolean.TRUE);
		}else{
			otcOperationParamSession.setModalityHaveInCharge(Boolean.FALSE);
		}
		otcOperationParamSession.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidated(mechanismOperationOtcSession, swapOperationSession, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		
		/*ASIGNAMOS PARTICIPANTE A LISTA*/
		participantList = new ArrayList<Participant>();
		if(operation.getSellerParticipant()!=null) {
			participantList.add(operation.getSellerParticipant());
		}
		
		/*ASIGNANOS PARTICIPANTE A LISTA DE COMPRADORES*/
		participantToBuyList = new ArrayList<Participant>();
		if(operation.getBuyerParticipant()!=null) {
			participantToBuyList.add(operation.getBuyerParticipant());
		}
		
		otcOperationParamSession.setAsigmentOperationList(OtcOperationUtils.getAssigmentOperationList(mechanismOperationOtcSession));
	}
	
	/**
	 * Gets the participante buyer.
	 *
	 * @return the participante buyer
	 */
	public Participant getParticipanteBuyer(){
		//Cuando sera usuario participante es necesario obtener de Seguridad
		Long idParticipantPk = this.mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk();
		if(idParticipantPk!=null)
			for(Participant participant: participantList){
				if(participant.getIdParticipantPk().equals(idParticipantPk)){
					return participant;
				}
			}
		return null;
	}
	
	/**
	 * Search holder account handler.
	 */
	/*Metodo para validar la cuenta ingresada*/
	public void changeHolderAccount(){
		MechanismOperation operation = mechanismOperationOtcSession;
		HolderAccountOperation accountOperation = holdAccountOperationRequest;
		OtcOperationParameters parameters = otcOperationParamSession;
		
		try {
			HolderAccount account = parameters.getAccountSelectedAssigment();
			/*VALIDAMOS ALGUNOS CRITERIOS PARA QUE LA CUENTA SEA CONSIDERADA EN LA ASIGNACION*/
			otcOperationFacade.validateHolderAccount(account, operation,accountOperation.getIndIncharge());
			
			//ISSUE 800, correccion issue 716
			//Al seleccionar la cuenta titular, Si el rol es VENTA validamos que exista saldo en cartera
			if(holdAccountOperationRequest.getRole().equals(GeneralConstants.TWO_VALUE_INTEGER)){
				SwapOperation swapOperation = swapOperationSession;
				Participant participant = otcOperationUtils.getPartToHoldAccNegotiation(operation, swapOperation, accountOperation, userInfo);
				
				//Extraemos HolderAccountBalance
				HolderAccountBalance hab =  otcOperationFacade.checkQuantityBalance(account.getIdHolderAccountPk(),mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk(),
						mechanismOperationOtcSession.getStockQuantity(),participant.getIdParticipantPk());
			
				//Si es nulo es q no existe el valor en la cuenta titular o la cantidad es insuficiente
				if(!Validations.validateIsNotNullAndNotEmpty(hab) || (mechanismOperationOtcSession.getStockQuantity().compareTo(hab.getAvailableBalance())>0)){
					//reiniciamos la seleccion de la grilla
					parameters.setAccountSelectedAssigment(null);
					//mandamos excepcion
					throw new ServiceException(ErrorServiceType.PARTICIPANT_BALANCE_NOT_VALID, new Object[]{});
				}
			}
			
			if(NegotiationUtils.mechanismOperationIsSwap(operation)){
				if(accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
					accountOperation.setStockQuantity(mechanismOperationOtcSession.getStockQuantity());
				}else if(accountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
					accountOperation.setStockQuantity(swapOperationSession.getStockQuantity());
				}
				//validamos el stockQuantity
				changeStockQuantityAssigment();
				//asignamos la cuenta a la grilla
				addNewAccountOperation();
			}
		} catch (ServiceException e) {
			if(e.getErrorService().equals(ErrorServiceType.PARTICIPANT_BALANCE_NOT_VALID)){
				//Mensaje de la linea 540
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.DOES_NOT_HAVE_SECURITITES));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				if(e.getErrorService()!=null){
					parameters.setAccountSelectedAssigment(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
	}
	/**
	 * Gets the participant role type list.
	 *
	 * @return the participant role type list
	 */
	public List<ParticipantRoleType> getParticipantRoleTypeList(){
		return ParticipantRoleType.list;
	}
	
	/**
	 * Gets the boolean type list.
	 * metodo necesario para listar "SI - NO" en componente JSF
	 * @return the boolean type list
	 */
	public List<BooleanType> getBooleanTypeList(){
		return BooleanType.list;
	}
	
	/**
	 * Search security handler.
	 */
	public void changeSecurity(){//Metodo para validar el valor consultado
		try{
			disableStockQuantity = Boolean.FALSE;
			otcOperationFacade.validateSecurityObject(mechanismOperationOtcSession, otcOperationParamSession.getMechanismModalityList());
			otcOperationFacade.validateSecuritySwapOperation(mechanismOperationOtcSession, swapOperationSession, otcOperationParamSession);
			otcOperationFacade.validateSecurityPrimaryPlacement(mechanismOperationOtcSession,otcOperationParamSession);

			Security securityObject = mechanismOperationOtcSession.getSecurities();
			mechanismOperationOtcSession.setCurrency(securityObject.getCurrency());
			mechanismOperationOtcSession.setNominalValue(securityObject.getCurrentNominalValue());
			mechanismOperationOtcSession.setSecurities(securityObject);
			
			otcOperationParamSession.setIsVariableIncome(securityObject.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()));
//			if(securityObject.getCurrency().equals(CurrencyType.PYG.getCode())){//IF the money is DOP security currency must be equals than OtcOperation currency
//				otcOperationParamSession.setOtcDiferentMoney(Boolean.TRUE);
//			}
			
			//Validando restricciones de operaciones OTC
			/*List<ParameterTable> lstRestriccionesOpeOtc = generalParameterFacade.getListParameterTableServiceBean(MasterTableType.RESTRICCIONES_OPERACIONES_OTC.getCode());
			if(Validations.validateListIsNotNullAndNotEmpty(lstRestriccionesOpeOtc)){
				Participant sellerParticipant = otcOperationFacade.findParticipantFromId(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());
				Integer economicActivity = sellerParticipant.getEconomicActivity();
				Integer securityClass = securityObject.getSecurityClass();
				for (ParameterTable resOpeOtc : lstRestriccionesOpeOtc) {
					if(resOpeOtc.getIndicator4().equals(securityClass) && resOpeOtc.getIndicator5().equals(economicActivity)){
						throw new ServiceException(ErrorServiceType.ERRROR_MESSAGE_RESTRICTION_OPERATION_OTC);
					}
				}
			}*/
			
			if(securityObject.getSecurityClass().equals(SecurityClassType.DPF.getCode())) {
				mechanismOperationOtcSession.setStockQuantity(PporQ);
			}
			
		}catch(ServiceException ex){
			mechanismOperationOtcSession.setSecurities(new Security());
			mechanismOperationOtcSession.setCurrency(null);
			mechanismOperationOtcSession.setNominalValue(null);
			otcOperationParamSession.setIsVariableIncome(null);
			otcOperationParamSession.setOtcDiferentMoney(null);
			
			if(depositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				/*CLEAN MARKETfACT*/
				MechanismOperationMarketFact operationMarket = mechanismOperationOtcSession.getMechanismOperationMarketFacts().get(0);
				operationMarket.setMarketDate(operationMarket.getMarketDate());
				operationMarket.setMarketPrice(operationMarket.getMarketPrice());
				operationMarket.setMarketRate(operationMarket.getMarketRate());
			}
			
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		
		cleanStockQuantity();//Limpiamos el stockQuantity
		cleanPrices();
		
		HolderAccountOperation accountOperation = holdAccountOperationRequest; 
		OtcOperationParameters parameters = otcOperationParamSession;
		
		accountOperation.getHolderAccount().setHolder(new Holder());
		parameters.setHolderAccountsAssigment(null);
		parameters.setAccountSelectedAssigment(null);
		
		if(Validations.validateIsNotNull(mechanismOperationOtcSession.getSecurities())
				&& Validations.validateIsNotNull(mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk())
				&& Validations.validateIsNotNull(mechanismOperationOtcSession.getSecurities().getSecurityClass())) {
			Security sec = mechanismOperationOtcSession.getSecurities();
			
			if(Validations.validateIsNotNull(sec.getExpirationDate()) &&
					CommonsUtilities.diffDatesInDays(sec.getExpirationDate(), mechanismOperationOtcSession.getOperationDate()) >= 0) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Este instrumento financiero ya se encuentra vencido. Verifique.");
				JSFUtilities.showSimpleValidationDialog();
				
				mechanismOperationOtcSession.setSecurities(new Security());
				mechanismOperationOtcSession.setCurrency(null);
				mechanismOperationOtcSession.setNominalValue(null);
				otcOperationParamSession.setIsVariableIncome(null);
				otcOperationParamSession.setOtcDiferentMoney(null);
				
				return;
			}
			
			if(this.otcOperationFacade.checkIfSecurityHasAnExpiringCouponToday(sec.getIdSecurityCodePk())) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Este instrumento financiero tiene un cupon que vence hoy.");
				JSFUtilities.showSimpleValidationDialog();
			}
			
			if(sec.getSecurityClass().equals(SecurityClassType.DPF.getCode())
				|| sec.getSecurityClass().equals(SecurityClassType.PGS.getCode())) {
				HolderAccountBalance ownerAccountBalance = this.otcOperationFacade.getOwnerOfSecurity(sec.getIdSecurityCodePk(), mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());
				
				if(Validations.validateIsNotNull(ownerAccountBalance)) {
					HolderAccount ownerAccount = ownerAccountBalance.getHolderAccount();
					
					Holder owner = ownerAccount.getHolderAccountDetails().get(0).getHolder();
					
					mechanismOperationOtcSession.setStockQuantity(ownerAccountBalance.getAvailableBalance());
					mechanismOperationOtcSession.setCashAmount(GeneralConstants.ZERO);
					holdAccountOperationRequest.setHolderAccount(new HolderAccount());
					holdAccountOperationRequest.getHolderAccount().setHolder(owner);
					
					changeHolder();
					otcOperationParamSession.setAccountSelectedAssigment(ownerAccount);
					changeHolderAccount();
					mechanismOperationOtcSession.setCashAmount(null);
					
					disableStockQuantity = Boolean.TRUE;
				}
			}
		}
	}
	
	/**
	 * Search swap security handler.
	 */
	public void searchSwapSecurityHandler(){//Metodo para validar el valor consultado
		String isinCode = swapOperationSession.getSecurities().getIdSecurityCodePk();
		
		if(isinCode==null){//Si el isin no existe 
			swapOperationSession.setSecurities(new Security());
			cleanStockQuantitySwap();
			return;
		}
		
		SecurityTO securityTO = new SecurityTO();
		securityTO.setIdIsinCodePk(isinCode);
		
		Security securityTemp = null;
		try {
			securityTemp = helperComponentFacade.findSecurityComponentServiceFacade(securityTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		if(securityTemp==null || securityTemp.getCurrency()==null || CurrencyType.get(securityTemp.getCurrency())==null || securityTemp.getIndTraderSecondary()==null
		   || securityTemp.getCirculationAmount()==null || securityTemp.getInstrumentType()==null //Si el valor es Null, no tiene emision ni Emisor O si el valor no tiene moneda O si el monto en circulacion es null
		   || securityTemp.getSecurityInvestor()==null || securityTemp.getSecurityInvestor().getIndJuridical()==null || securityTemp.getSecurityInvestor().getIndNatural()==null ){//Si los indicadores que el valor lo puede negociar natural o juridico son nullos
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,"PF('cnfwMsgCustomValidationNeg').show()");
			swapOperationSession.setSecurities(new Security());
			cleanStockQuantitySwap();
			return;
		}
		if(securityTemp.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()) && securityTemp.getExpirationDate()==null){//Si es Rta fija y no tiene fecha expiracion
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,"PF('cnfwMsgCustomValidationNeg').show()");
			swapOperationSession.setSecurities(new Security());
			cleanStockQuantitySwap();
			return;
		}
		if(!securityTemp.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){//El valor no tiene estado registrado
			swapOperationSession.setSecurities(new Security());
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATIONS_VALIDATION_SECURITY_STATE,"PF('cnfwMsgCustomValidationNeg').show()");
			cleanStockQuantitySwap();
			return;
		}
		
		//1.- VALIDAMOS EL EMISOR
		if(securityTemp.getIssuer()!=null){//Validamos que el valor tenga al menos un emisor
			if(!securityTemp.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){//Si tiene estado diferente a REGISTRADO
				executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_ISSUER,"PF('cnfwMsgCustomValidationNeg').show()");
				swapOperationSession.setSecurities(new Security());
				cleanStockQuantitySwap();
				return;
			}
		}else{
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,"PF('cnfwMsgCustomValidationNeg').show()");
			swapOperationSession.setSecurities(new Security());
			cleanStockQuantitySwap();
			return;
		}
		//2.- VALIDAMOS LA EMISION
		if(securityTemp.getIssuance()!=null){//Validamos que el valor tenga al menos un emisor
			if(!securityTemp.getIssuance().getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){//Si tiene estado diferente a REGISTRADO
				executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_ISSUER,"PF('cnfwMsgCustomValidationNeg').show()");
				swapOperationSession.setSecurities(new Security());
				cleanStockQuantitySwap();
				return;
			}
		}else{
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,"PF('cnfwMsgCustomValidationNeg').show()");
			swapOperationSession.setSecurities(new Security());
			cleanStockQuantitySwap();
			return;
		}
		
		NegotiationModality negotiationModality = otcOperationFacade.findNegModality(mechanismOperationOtcSession, otcOperationParamSession.getMechanismModalityList());
		if(!securityTemp.getInstrumentType().equals(negotiationModality.getInstrumentType())){//El valor no coincide con el instrumento de la modalidad
			swapOperationSession.setSecurities(new Security());
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATIONS_VALIDATION_SECURITY_INSTRUMENT,"PF('cnfwMsgCustomValidationNeg').show()");
			cleanStockQuantitySwap();
			return;
		}
		
		Security securityByModality = null;
		try{
			securityByModality = otcOperationFacade.findSecurityIntoModality(mechanismOperationOtcSession,securityTemp);
		}catch(ServiceException e){
			e.printStackTrace();
		}
		if(securityByModality==null){//Validamos que el isin este habilitado para negociar en el mecanismo y modalidad
			swapOperationSession.setSecurities(new Security());
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_ISIN_MECHANISM,"PF('cnfwMsgCustomValidationNeg').show()");
			cleanStockQuantitySwap();
			return;
		}
		
		//Validamos si No es colocacion primaria Y el valor No es negociable en la segunda modalidad
		if(!NegotiationUtils.mechanismOperationIsPrimaryPlacement(mechanismOperationOtcSession)){
			try {
				if(otcOperationFacade.issuanceIsPlacementSegment(securityTemp)){//Validamos si la emision esta en colocacion primaria
					swapOperationSession.setSecurities(new Security());
					executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_PRIMARYPLACEMENT_FINISHED, "PF('cnfwMsgCustomValidationNeg').show()");
					cleanStockQuantitySwap();
					return;
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		
		//si el ISIN ingresado ya fue usado en una operacion anterior
		if(mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk()!=null){
			if(securityTemp.getIdSecurityCodePk().equals(mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk())){
				executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesConstants.OTC_OPERATION_VALIDATION_ISIN_SWAP_EXIST,"PF('cnfwMsgCustomValidationNeg').show()");
				swapOperationSession.setSecurities(new Security());
				cleanStockQuantitySwap();
				return;
			}
			if(!securityTemp.getCurrency().equals(mechanismOperationOtcSession.getCurrency())){
				otcOperationParamSession.setIsSwapDifCurr(Boolean.TRUE);
			}else{
				otcOperationParamSession.setIsSwapDifCurr(Boolean.FALSE);
			}
		}
		cleanStockQuantitySwap();
		swapOperationSession.setSecurities(securityTemp);
		swapOperationSession.setCurrency(securityTemp.getCurrency());
		JSFUtilities.resetComponent("frmOtcOperation:pnlSwapOperation");//Limpiamos espacio donde se encuentra maquetado el Helper con el Detalle
	}
	
	/**
	 * Validate reference number.
	 */
	public void validateReferenceNumber(){
		try{
			if(mechanismOperationOtcSession.getReferenceNumber()!=null){
				otcOperationFacade.validateReferenceNumber(mechanismOperationOtcSession);
			}
		}catch(ServiceException ex){
			if(ex.getErrorService()!=null){
				mechanismOperationOtcSession.setReferenceNumber(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	// SOME CALCULES AND VALIDATIONS
	/**
	 * Validate quantity securities handler.
	 */
	public void changeStockQuantity(){
		
		// validando si se ingreso un valor
		if(Validations.validateIsNull(mechanismOperationOtcSession.getSecurities())||Validations.validateIsNull(mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk())){
			mechanismOperationOtcSession.setTermSettlementDays(null);
			mechanismOperationOtcSession.setTermSettlementDate(null);
			mechanismOperationOtcSession.setStockQuantity(null);
			
			//lanzar mensaje de error
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("msg.validation.required.entered.security")
					);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		cleanPrices();
		
		holdAccountOperationRequest.setStockQuantity(mechanismOperationOtcSession.getStockQuantity());
		/*
		if(Validations.validateIsNull(mechanismOperationOtcSession.getCashPrice())) {
			mechanismOperationOtcSession.setStockQuantity(null);
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("msg.validation.required.cash.price")
					);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		PporQ = mechanismOperationOtcSession.getCashPrice().multiply(mechanismOperationOtcSession.getStockQuantity());
		mechanismOperationOtcSession.setCashAmount(PporQ);
		mechanismOperationOtcSession.setRealCashAmount(PporQ); 
		changeRealCashAmout();
		changeCashAmount();
//		--
		MechanismOperation otcOperation = mechanismOperationOtcSession;		
		try {
			if(otcOperation.getSecurities().getIdSecurityCodePk()==null){
				otcOperation.setStockQuantity(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.MCN_CORDIGOVALOR_NOTENTERED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			otcOperationFacade.validateStockQuantity(otcOperation, otcOperationParamSession);
		} catch (ServiceException ex) {
			otcOperation.setStockQuantity(null);
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}		
//		dejamos de limpiar, valores seteados internamente
//		if(otcOperation.getCashAmount()!=null || otcOperation.getRealCashAmount()!=null){
//			/**LIMPIAMOS LO REFERENTE AL PRECIO LIMPIO*/	
//			cleanCashAmount();
//		}
//		/**Clean Fields according with Cash Settlement Days*/
//		calculateAmountClean();
		//Validamos lo ultimo
		
	}
	/**
	 * Validate quantity securities swap handler.
	 */
	public void validateQuantitySecuritiesSwapHandler(){
		String isinA = mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk();
		BigDecimal cashAmountA =  mechanismOperationOtcSession.getCashAmount();
		
		swapOperationSession.setRealCashAmount(null);
		if(otcOperationParamSession.getInstrumentTypeSelected().equals(InstrumentType.VARIABLE_INCOME.getCode())){
			swapOperationSession.setRealCashPrice(null);
		}
		
		BigDecimal quantitySecurities = swapOperationSession.getStockQuantity();//Obtenemos la cantidad de valores
		BigDecimal circulationBalance = swapOperationSession.getSecurities().getCirculationBalance();
		if(quantitySecurities==null || quantitySecurities.equals(BigDecimal.ZERO)){
			swapOperationSession.setStockQuantity(null);
			return;
		}
		if(circulationBalance.compareTo(quantitySecurities)==-1){//Validamos si la cantidad ingresada es menor a los valores en circulacion
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_CIRCULATION, "PF('cnfwMsgCustomValidationNeg').show()");
			swapOperationSession.setStockQuantity(null);
			return;
		}
		
		if(isinA!=null && cashAmountA!=null && !otcOperationParamSession.getIsSwapDifCurr()){
			swapOperationSession.setCashAmount(mechanismOperationOtcSession.getCashAmount());
			calculateCashPriceSwap();
		}
	}
	/**
	 * Calculate clean cash price.
	 * metodo para calcular el precio limpio
	 */
	public void changeRealCashAmout(){
		try {
			if(mechanismOperationOtcSession.getStockQuantity()==null){
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.MCN_STOCKQUANTITY_NOT_NULL));
				throw new ServiceException();
			}
			
			otcOperationFacade.validateCleanCashAmount(mechanismOperationOtcSession);
			
			/*CALCULAMOS EL PRECIO LIMPIO Y ACTUALIZAMOS*/
//			if(mechanismOperationOtcSession.getSecurities().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				BigDecimal cleanCashAmount = mechanismOperationOtcSession.getRealCashAmount();
				//Cantidad a controlar
				BigDecimal stockQuantity = mechanismOperationOtcSession.getStockQuantity();
				BigDecimal realCashPrice = cleanCashAmount.divide(stockQuantity,8,RoundingMode.CEILING);
				mechanismOperationOtcSession.setRealCashPrice(realCashPrice);
//			}
		} catch (ServiceException e) {
			swapOperationSession.setRealCashAmount(null);
			swapOperationSession.setRealCashPrice(null);
			mechanismOperationOtcSession.setRealCashAmount(null);
			mechanismOperationOtcSession.setRealCashPrice(null);
			
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/**
	 * Calculate clean cash price swap.
	 */
	public void calculateCleanCashPriceSwap(){
		BigDecimal cleanCashAmount = swapOperationSession.getRealCashAmount();
		BigDecimal dirtyCashAmount = mechanismOperationOtcSession.getCashAmount();
		if(cleanCashAmount==null || cleanCashAmount.intValue()==BigDecimal.ZERO.intValue()){//Si es 0 o Null
			swapOperationSession.setRealCashAmount(null);
			if(swapOperationSession.getSecurities().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				swapOperationSession.setRealCashPrice(null);
			}
			return;
		}
		if(dirtyCashAmount!=null){//Si el monto sucio ya fue ingresado
			if(dirtyCashAmount.compareTo(cleanCashAmount)<0){//Si el monto limpio es mayor O igual al monto sucio
				executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_CLEAN_AMOUNT_GATHER, "PF('cnfwMsgCustomValidationNeg').show()");
				swapOperationSession.setRealCashAmount(null);
				if(swapOperationSession.getSecurities().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
					swapOperationSession.setRealCashPrice(null);
				}
				return;
			}
		}
		if(mechanismOperationOtcSession.getSecurities().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
			BigDecimal quantitySecurities = swapOperationSession.getStockQuantity();
			BigDecimal realCashPrice = cleanCashAmount.divide(quantitySecurities,8,RoundingMode.CEILING);
			swapOperationSession.setRealCashPrice(realCashPrice);
		}
	}
	/**
	 * Calculate cash price.//this is for dirty price from dirty amount
	 */
	public void changeCashAmount(){
		MechanismOperation operation = mechanismOperationOtcSession;		
		List<MechanismModality> modalities = otcOperationParamSession.getMechanismModalityList();
		OtcOperationParameters parameters = otcOperationParamSession;
		
		try{
			if(operation.getStockQuantity()==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.MCN_STOCKQUANTITY_NOT_NULL));
				JSFUtilities.showSimpleValidationDialog();
				throw new ServiceException();
			}
			
			BigDecimal cashAmount = operation.getCashAmount();
			BigDecimal realCashAmount = operation.getRealCashAmount();
			if(realCashAmount!=null){
				/**VALIDAMOS QUE EL MONTO SUCIO SEA MAYOR AL LIMPIO*/
				if(realCashAmount.compareTo(cashAmount)>0){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_CLEANAMOUNT_GATHER);
				}
			}
			
			/**CALCULAMOS EL PRECIO SUCIO*/
			BigDecimal quantitySecurities = operation.getStockQuantity();
			BigDecimal cashPrice = cashAmount.divide(quantitySecurities,8,RoundingMode.CEILING);
			operation.setCashPrice(cashPrice);
			
			/**SOME VALIDATIONS FOR SWAP OPERATION*/
			if(NegotiationUtils.mechanismOperationIsSwap(operation)){
				String isinCodeB = swapOperationSession.getSecurities().getIdSecurityCodePk();
				BigDecimal stockQuantityB = swapOperationSession.getStockQuantity();
				if(!otcOperationParamSession.getIsSwapDifCurr() && isinCodeB!=null && stockQuantityB!=null){
					swapOperationSession.setCashAmount(operation.getCashAmount());
					calculateCashPriceSwap();
				}
			}
			otcOperationFacade.validateSettDateWithModality(operation, operation.getCashSettlementDate(), 
															otcOperationParamSession);
			//Volvemos a leer las cuentas negociadoras, de acuerdo al participante logueado
			loadNewHolderAccountOperation();
			//add seller account
			otcOperationParamSession.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidated(mechanismOperationOtcSession, 
									swapOperationSession, userInfo, ViewOperationsType.get(getViewOperationType())));
			
			/**ASIGNAMOS EL NUEVO ROL DEL ACCOUNT_OPERATION PARA LA ASIGNACION*/
			Integer role = otcOperationUtils.getParticipantRoleByOrigin(mechanismOperationOtcSession, swapOperationSession, userInfo, 
											ViewOperationsType.get(this.getViewOperationType()));
			holdAccountOperationRequest.setRole(role);
			//no esta q actualiza
		}catch(ServiceException e){
			operation.setCashAmount(null);
			operation.setCashPrice(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		
		if(operation.getTermAmount()!=null){
			/**LIMPIAMOS INFORMACION PLAZO*/
			cleanTermAmount();
		}
		if(operation.getCashSettlementDays()!=null){
			/**LIMPIAMOS INFORMACION LIQUIDACION CONTADO*/
			if(operation.getIndPrimaryPlacement().equals(BooleanType.NO.getCode())){
				cleanCashSettlement();
			}else{
				for (Iterator<HolderAccountOperation> iter = operation.getHolderAccountOperations().iterator(); iter.hasNext();){
					HolderAccountOperation element = iter.next();
					if(element.getRole().equals(ParticipantRoleType.BUY.getCode())){
						iter.remove();
					}
				}
				ViewOperationsType viewOperationType = ViewOperationsType.get(getViewOperationType());
				List<HolderAccountOperation> newAccountOperations = otcOperationUtils.getAccountOperationValidated(operation,null, userInfo, viewOperationType);
				otcOperationParamSession.setHoldAccNegotiationValidated(newAccountOperations);
			}
		}

		/**Method to get MechanisModality based on modalitySelected*/
		MechanismModality mechModality = otcOperationFacade.findMechanismModality(operation, modalities);			
		Integer cashMiniDays = mechModality.getNegotiationModality().getCashMinSettlementDays();
		if(cashMiniDays.equals(BigDecimal.ZERO.intValue())){
			operation.setCashSettlementDays(cashMiniDays.longValue());
			operation.setCashSettlementDate(CommonsUtilities.currentDate());
		}
		/**Clean result to handle totals on amounts*/ 
		parameters.setAsigmentOperationList(OtcOperationUtils.getAssigmentOperationList(operation));
		mechanismOperationOtcSession = operation;
	}
	
	public void calculateCashSettlementDate() {
		MechanismOperation operation = mechanismOperationOtcSession;
		OtcOperationParameters parameters = otcOperationParamSession;
		List<MechanismModality> modalities = otcOperationParamSession.getMechanismModalityList();
		/**Method to get MechanisModality based on modalitySelected*/
		MechanismModality mechModality = otcOperationFacade.findMechanismModality(operation, modalities);			
		Integer cashMiniDays = mechModality.getNegotiationModality().getCashMinSettlementDays();
		if(cashMiniDays.equals(BigDecimal.ZERO.intValue())){
			operation.setCashSettlementDays(cashMiniDays.longValue());
			operation.setCashSettlementDate(CommonsUtilities.currentDate());
		}
		/**Clean result to handle totals on amounts*/ 
		parameters.setAsigmentOperationList(OtcOperationUtils.getAssigmentOperationList(operation));
	}
	
	/**
	 * Calculate cash price swap.
	 */
	public void calculateCashPriceSwap(){
		BigDecimal dirtyCashAmount = swapOperationSession.getCashAmount();
		if(dirtyCashAmount==null || dirtyCashAmount.intValue()==BigDecimal.ZERO.intValue()){//Si es 0 o null
			swapOperationSession.setCashAmount(null);
			swapOperationSession.setCashPrice(null);
			return;
		}
		BigDecimal cleanCashAmount = swapOperationSession.getRealCashAmount();//Obtenemos el precio limpio
		if(cleanCashAmount!=null){//Si el monto limpio es diferente a null
			if(dirtyCashAmount.compareTo(cleanCashAmount)<0){//Si el monto sucio es menor O igual al monto limpio
				executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_DIRTY_AMOUNT_LESS, "PF('cnfwMsgCustomValidationNeg').show()");
				swapOperationSession.setCashAmount(null);
				swapOperationSession.setCashPrice(null);
				return;
			}
		}
		BigDecimal quantitySecurities = swapOperationSession.getStockQuantity();
		BigDecimal cashPrice = dirtyCashAmount.divide(quantitySecurities,8,RoundingMode.CEILING);
		swapOperationSession.setCashPrice(cashPrice);
		if(otcOperationParamSession.getIsSwapDifCurr()){
			Integer currency = swapOperationSession.getCurrency();
			BigDecimal cashAmount = mechanismOperationOtcSession.getCashAmount();
			BigDecimal cashAmountSwap = swapOperationSession.getCashAmount();
			if(currency.equals(CurrencyType.USD.getCode())){
				swapOperationSession.setExchangeRate(cashAmount.divide(cashAmountSwap,8,RoundingMode.CEILING));
			}else{
				swapOperationSession.setExchangeRate(cashAmountSwap.divide(cashAmount,8,RoundingMode.CEILING));
			}
		}
	}
	/**
	 * Calculate settlement date.
	 */
	public void changeCashSettlementDays(){
		Long cashSettlementDays = mechanismOperationOtcSession.getCashSettlementDays();
		MechanismOperation operation = mechanismOperationOtcSession;
		OtcOperationParameters parameters = otcOperationParamSession;
		SwapOperation swapOperation = swapOperationSession;
		try{
			if((NegotiationUtils.mechanismOperationIsSwap(operation) && swapOperation.getCashAmount()==null) || operation.getCashAmount()==null){
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_CASH_AMOUNT_EMPTY));
				throw new ServiceException();
			}

			/*VALIDAMOS DIAS DE LIQUIDACION CONTADO*/
			otcOperationFacade.validateSettlementDate(cashSettlementDays, OperationPartType.CASH_PART.getCode(), operation, parameters.getMechanismModalityList());
			otcOperationFacade.validateSettDateWithSecurity(swapOperation, OperationPartType.CASH_PART.getCode(), operation);
			otcOperationFacade.validateSettDateWithModality(operation, operation.getCashSettlementDate(), parameters);
		}catch(ServiceException e){
			operation.setCashSettlementDays(null);
			operation.setCashSettlementDate(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		parameters.setIsInchargedDisabled(OtcOperationUtils.getInchargedDisabled(operation, swapOperation, userInfo, ViewOperationsType.get(getViewOperationType())));//Cargamos si el indicador de encargo esta en Disabled
	}
	
	/**
	 * Validate term settlement days.
	 * metodo para validar los dias plazo y calcular la fecha de liquidacion plazo 
	 */
	public void changeTermSettlementDays(){
		Long termSettlementDays = mechanismOperationOtcSession.getTermSettlementDays();
		MechanismOperation operation = mechanismOperationOtcSession;
		OtcOperationParameters parameters = otcOperationParamSession;
		SwapOperation swapOperation = swapOperationSession;
		try{
			
			// validando si se ingreso un valor
			if(Validations.validateIsNull(operation.getSecurities())||Validations.validateIsNull(operation.getSecurities().getIdSecurityCodePk())){
				operation.setTermSettlementDays(null);
				operation.setTermSettlementDate(null);
				
				//lanzar mensaje de error
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("msg.validation.required.entered.security")
						);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(operation.getCashSettlementDate()==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_SETTLEMENT_CASH_EMPTY));
				throw new ServiceException();
			}
			/*VALIDAMOS DIAS DE LIQUIDACION CONTADO*/
			otcOperationFacade.validateSettlementDate(termSettlementDays, OperationPartType.TERM_PART.getCode(), operation, parameters.getMechanismModalityList());
			otcOperationFacade.validateSettDateWithSecurity(swapOperation, OperationPartType.TERM_PART.getCode(), operation);
			otcOperationFacade.validateSettDateWithModality(operation, operation.getCashSettlementDate(), parameters);
			
			calculateTermRealCashAmount();
		}catch(ServiceException e){
			operation.setTermSettlementDays(null);
			operation.setTermSettlementDate(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Calculate term price handler.
	 * metodo para calcular el precio de la operacion con liquidacion plazo
	 */
	public void changeTermAmount(){
		MechanismOperation operation = mechanismOperationOtcSession;
		try {
			BigDecimal cashAmount = operation.getCashAmount();//Obtenemos el monto Contado de la Operacion
			if(cashAmount==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_CASH_AMOUNT_EMPTY));
				throw new ServiceException();
			}
			BigDecimal operationQuantity = operation.getStockQuantity();//Obtenemos la cantidad de valores de la operacion
			BigDecimal termAmount = operation.getTermAmount();//Obtenemos el monto plazo ingresado
			BigDecimal termPrice = termAmount.divide(operationQuantity,8,RoundingMode.CEILING);//Calculamos el precio plazo
			//BigDecimal efectiveTasa = OtcOperationUtils.getEfectiveTasa(termAmount, cashAmount);
			/*
			 * Se retira validacion de monto plazo menor a monto sucio. Redmine 1613
			 * if(cashAmount.compareTo(termAmount)>0){
					throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_TERMAMOUNT_LESS);
			}*/
			operation.setTermPrice(termPrice);
			//operation.setAmountRate(efectiveTasa);
		} catch (ServiceException e) {
			operation.setTermPrice(null);operation.setTermAmount(null);operation.setAmountRate(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
		//cleanHolderAccountOperations();//Limpiamos todas las cuentas ingresadas
	}
	/**
	 * Validate quantity stock on issuance handler.
	 * Validamos cantidad de valores  ingresada en la Cuenta de Negociacion.
	 */
	public void changeStockQuantityAssigment(){
		MechanismOperation operation = mechanismOperationOtcSession;
		SwapOperation swapOperation = swapOperationSession;
		HolderAccountOperation accountOperation = holdAccountOperationRequest;
		Double socialKapital = OtcOperationMgmt.KAPITAL_SOCIAL.doubleValue();
		Boolean amountExcedSocialKapital = otcOperationFacade.validateSocialKapital(accountOperation, operation, socialKapital);
		//Si el monto es mayor al % de Factor de Negociacion de la emision
		if(amountExcedSocialKapital){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_QUANTITY_SELL,new Object[]{socialKapital.toString()}));
			JSFUtilities.showSimpleValidationDialog();
		}
		try {
			if(operation.getCashAmount()==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_CASH_AMOUNT_EMPTY));
				JSFUtilities.showSimpleValidationDialog();
				throw new ServiceException();
			}
			
			otcOperationFacade.validateStockQuantAssigment(operation, swapOperation, accountOperation);
			otcOperationFacade.validateStockQuantitRange(accountOperation, operation);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
			accountOperation.setStockQuantity(null);accountOperation.setCashAmount(null);
		}
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		setRowsQuantityDataTable(10);
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		otcOperationTO = new OtcOperationTO();
		otcOperationParamSession = new OtcOperationParameters();//Inicializamos OTC Operation Parametros
		try {
			loadCboParticipants();
			loadMotiveCancelOperationList();
			loadSettlementType();
			loadSettlementSchemeType();
			loadCboDateSearchType();
			loadCurrencyList();
			loadCboOtcOperationStateType();
			loadCboOtcOriginRequestType();
			loadCboModalityAndParticipants();
			loadNegotiationFactors();
			loadUserSessionPrivileges();
			loadAccountTypeDescriptions();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		reLoadOtcOperationTO();
//		reLoadOtcOperationParameters();//Inicilizamos "otcOperationParamSession"
		evaluateMechanismModalityByInstrumentSearch();//Cargamos las modalidates de acuerdo al instrumento seleccionado para la busquedad
		optionSelectedOneRadio = GeneralConstants.ONE_VALUE_INTEGER;
	}
	
	/**
	 * Load account type descriptions.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAccountTypeDescriptions() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
		List<ParameterTable> lstAccountType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstAccountType){
			paramAccountTypeDesc.put(parameter.getParameterTablePk(), parameter.getParameterName());
		}
	}
	
	/**
	 * Load currency list.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCurrencyList() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		lstCurrency = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstCurrency){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
		}
	}

	/**
	 * Load settlement type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSettlementType() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SETTLEMENT_TYPE.getCode());
		parameterTableTO.setState(BooleanType.YES.getCode());
		lstSettlementType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstSettlementType)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
	}
	
	/**
	 * Load settlement scheme type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSettlementSchemeType() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SETTLEMENT_SCHEME_TYPE.getCode());
		lstSchemeType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstSchemeType)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
	}

	/**
	 * Load motive cancel operation list.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMotiveCancelOperationList() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.MOTIVOS_CANCELACION_OPERACION_MCN.getCode());
		motiveCancelOperationList = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: motiveCancelOperationList)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
	}

	/**
	 * Load user session privileges.
	 */
	public void loadUserSessionPrivileges(){
		Boolean userSessionStateIsActive = Boolean.TRUE;
		if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			
//			getIssuerDpfInstitution(userInfo);
			try {
				if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					idParticipantPkForIssuer=userInfo.getUserAccountSession().getParticipantCode();
					participantState = accountsFacade.getParticipantStateServiceFacade(userInfo.getUserAccountSession().getParticipantCode());
				}else{
					idParticipantPkForIssuer=userInfo.getUserAccountSession().getPartIssuerCode();
					if(Validations.validateIsNotNullAndPositive(idParticipantPkForIssuer)){
						participantState= accountsFacade.getParticipantStateServiceFacade(userInfo.getUserAccountSession().getPartIssuerCode());
					}
				}
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			if(Validations.validateIsNotNullAndPositive(idParticipantPkForIssuer))
			{	if(!participantState.getState().equals(ParticipantStateType.REGISTERED.getCode())){
					userSessionStateIsActive = Boolean.FALSE;
				}
			}else userSessionStateIsActive = Boolean.FALSE;
		}
		
		if(userSessionStateIsActive){
			PrivilegeComponent privilegeComponent = new PrivilegeComponent();
			privilegeComponent.setBtnApproveView(Boolean.TRUE);
			privilegeComponent.setBtnAnnularView(Boolean.TRUE);
			privilegeComponent.setBtnReview(Boolean.TRUE);
			privilegeComponent.setBtnRejectView(Boolean.TRUE);
			privilegeComponent.setBtnRegisterView(Boolean.TRUE);
			privilegeComponent.setBtnConfirmView(Boolean.TRUE);
			privilegeComponent.setBtnCancel(Boolean.TRUE);
			privilegeComponent.setBtnSettlement(Boolean.FALSE);
			privilegeComponent.setBtnAuthorize(Boolean.TRUE);
			userPrivilege.setPrivilegeComponent(privilegeComponent);
//			userInfo.setPrivilegeComponent(privilegeComponent);
		}else{
			userPrivilege.setPrivilegeComponent(new PrivilegeComponent());
//			userInfo.setPrivilegeComponent(new PrivilegeComponent());
		}
	}
	/**
	 * Re load otc operation parameters.
	 */
	public void reLoadOtcOperationParameters() {
		if(otcOperationParamSession==null){
			otcOperationParamSession = new OtcOperationParameters();//Inicializamos OTC Operation Parametros
		}
		otcOperationParamSession.setIsInchargedDisabled(Boolean.FALSE);
		otcOperationParamSession.setIsHoldAccNegDelButtEnabled(Boolean.FALSE);
		otcOperationParamSession.setIsSettlementPlaze(Boolean.FALSE);
		otcOperationParamSession.setIsSettlementPlazeDisabled(Boolean.FALSE);
		otcOperationParamSession.setInstrumentTypeSelected(InstrumentType.FIXED_INCOME.getCode());
		otcOperationParamSession.setIsHoldAccNegDelButtEnabled(Boolean.TRUE);
		otcOperationParamSession.setOtcSwapOperationResultTO(new ArrayList<OtcSwapOperationResultTO>());
		otcOperationParamSession.setCurrentDate(new Date());
		otcOperationParamSession.setIsInchargedDisabled(Boolean.FALSE);
		otcOperationParamSession.setIsIsinQuantityEquals(Boolean.FALSE);
		otcOperationParamSession.setIsSwap(Boolean.FALSE);
		otcOperationParamSession.setIsSwapDifCurr(Boolean.FALSE);
		otcOperationParamSession.setIsOriginRequestDisabled(Boolean.FALSE);
		otcOperationParamSession.setIsSecundaryReport(Boolean.FALSE);
		otcOperationParamSession.setSwapExchangeRate(null);
		otcOperationParamSession.setAsigmentOperationList(null);
		otcOperationParamSession.setHoldAccNegotiationValidated(new ArrayList<HolderAccountOperation>());
		otcOperationParamSession.setAccountSelectedAssigment(null);
		otcOperationParamSession.setHolderAccountsAssigment(null);
		changeOperationInstrument();//Evaluamos las modalidades de acuerdo al tipo instrumento, Por default renta fija seleccionado
	}
	
	/**
	 * Load mechanism operation otc session.
	 */
	public void loadMechanismOperationOtcSession(){
		//setting default value for request origin
		mechanismOperationOtcSession.setOriginRequest(OtcOperationOriginRequestType.SALE.getCode());
		//setting 0 as value by default because of every operation are settled to the end day
		mechanismOperationOtcSession.setCashSettlementDays(new Long(0));
	}
	/**
	 * Evaluate mechanism modality by instrument.
	 * metodo usado cuando se cambia el tipo de instrumento
	 */
	public void changeOperationInstrument(){
//		InstrumentType instrumentTypeSelected = InstrumentType.get(otcOperationParamSession.getInstrumentTypeSelected());
		
		Integer instrumentType = null;// otcOperationParamSession.getInstrumentTypeSelected();
		Long mechanismPk = NegotiationMechanismType.OTC.getCode();
//		Long participantPk = userInfo.getUserAccountSession().getParticipantCode();
		
		try {
			List<NegotiationModality> lstNegotiationModality = otcOperationFacade.getModalityByInstrumentType(mechanismPk, instrumentType, idParticipantPkForIssuer);
			otcOperationParamSession.setLstNegotiationModality(lstNegotiationModality);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
//		otcOperationParamSession.setMecModByInstrumentList(OtcOperationUtils.getMechanismModalityByInstrument(otcOperationParamSession.getMechanismModalityList(), instrumentTypeSelected,userInfo));
		if(isViewOperationRegister() && mechanismOperationOtcSession.getMechanisnModality().getId().getIdNegotiationModalityPk()!=null){
			cleanNegotiationModality();//Limpiamos la negociacion y modalidad
		}
	}
	
	/**
	 * Evaluate mechanism modality by instrument search.
	 */
	public void evaluateMechanismModalityByInstrumentSearch(){
		if(otcOperationParamSession.getInstrumentTypeSelectedSearch()==null){//Si el instrumento esta null, por default se debe considerar renta fija
			otcOperationParamSession.setInstrumentTypeSelectedSearch(InstrumentType.FIXED_INCOME.getCode());
		}
		InstrumentType instrumentTypeSelected = InstrumentType.get(otcOperationParamSession.getInstrumentTypeSelectedSearch());
		otcOperationParamSession.setMecModByInstrumentSearchList(OtcOperationUtils.getMechanismModalityByInstrument(otcOperationParamSession.getMechanismModalityList(), instrumentTypeSelected));
	}
	
	/**
	 * Adds the hold acc negotiation on grid handler.
	 */
	public void addNewAccountOperation(){
		MechanismOperation operation = mechanismOperationOtcSession;
		SwapOperation swapOperation = swapOperationSession;
		OtcOperationParameters parameters = otcOperationParamSession;
		HolderAccountOperation accountOperation = holdAccountOperationRequest;
		
		try {
			// issue 747: en caso que el uusario no seleccione una cuenta titular
			// lanzando mensaje de validacion de que debe seleccionar una cuenta titular
			HolderAccount account = parameters.getAccountSelectedAssigment();
			if (account == null || account.getIdHolderAccountPk() == null) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("msg.validation.required.selected.holderaccount"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(Validations.validateIsNull(operation) || Validations.validateIsNull(operation.getCashAmount())) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Falta agregar precio o monto. Verifique.");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			otcOperationFacade.validateNewAccountOperation(operation, swapOperation, accountOperation, parameters);
			otcOperationFacade.validateAccontOperationForSecundReport(operation, accountOperation, parameters);						
			/**VALIDAMOS SI AUN QUEDA ENCARGO PENDIENTE POR COLOCAR, SOLO PARA USUARIO CEVALDOM*/
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.BUY.getCode())){
					Participant participantInCharge = OtcOperationUtils.getParticipantInChargedByRole(operation, ParticipantRoleType.BUY.getCode());
					accountOperation.setIndIncharge(BooleanType.YES.getCode());
					accountOperation.setInchargeFundsParticipant(participantInCharge);
				}else if(OtcOperationUtils.haveInChargeByParticipantRole(operation, ParticipantRoleType.SELL.getCode())){
					if(OtcOperationUtils.haveStockPendientInChargedByRole(operation, swapOperation, accountOperation.getRole())){
						accountOperation.setIndIncharge(BooleanType.YES.getCode());
						Participant participantInCharge = OtcOperationUtils.getParticipantInChargedByRole(operation, accountOperation.getRole());
						accountOperation.setInchargeFundsParticipant(participantInCharge);
					}
				}
			}
			
			/*VOLVEMOS A REINICIAR HOLDER_ACCOUNT_OPERATION*/
			accountOperation = otcOperationFacade.getNewHolderAccountOperation(operation, false);
			parameters.setHolderAccountsAssigment(null);
			parameters.setAccountSelectedAssigment(null);
			
			/*METODO PARA REDONDEAR LOS MONTOS EN LAS CUENTAS*/
			//otcOperationFacade.roundMechanismOperation(operation);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		
		/*Asignamos el rol en la grilla*/
		accountOperation.setRole(otcOperationUtils.getParticipantRoleByOrigin(operation,swapOperation, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		parameters.setIsInchargedDisabled(OtcOperationUtils.getInchargedDisabled(operation, swapOperation, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		parameters.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidated(operation, swapOperation, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		
		parameters.setAsigmentOperationList(OtcOperationUtils.getAssigmentOperationList(operation));
		holdAccountOperationRequest = accountOperation;
//		if (Validations.validateListIsNotNullAndNotEmpty(parameters.getHoldAccNegotiationValidated())) {
//			holdAccountOperationRequest.setRole(ParticipantRoleType.BUY.getCode());
//		}
	}
	
	/**
	 * Removes the account on data table handler.
	 * dialog que elimina la cuenta negociadora de la grilla
	 *
	 * @param holderAccountOperation the holder account operation
	 */
	public void confirmRemoveAccountOnDataTableHandler(HolderAccountOperation holderAccountOperation){
		if(mechanismOperationOtcSession.getHolderAccountOperations().indexOf(holderAccountOperation)!=-1){//Validamos si el objeto enviado en la grilla, existe en la session
			setHoldAccountOperationSelected(holderAccountOperation);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_WARNING_DELETE_ACCOUNT));
			JSFUtilities.executeJavascriptFunction("PF('cnfwDeleteAccountNegotiation').show()");
		}
		if(NegotiationUtils.mechanismOperationIsSwap(mechanismOperationOtcSession)){
			if(swapOperationSession.getHolderAccountOperations().indexOf(holderAccountOperation)!=-1){//Validamos si el objeto enviado en la grilla, existe en la session
				setHoldAccountOperationSelected(holderAccountOperation);
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_WARNING_DELETE_ACCOUNT));
				JSFUtilities.executeJavascriptFunction("PF('cnfwDeleteAccountNegotiation').show()");
			}
		}
	}
	/**
	 * Removes the account on data table handler.
	 */
	public void removeAccountOnDataTableHandler(){
		Integer indiceHoldAccSelected = OtcOperationUtils.getIndicatorHoldAccOperation(mechanismOperationOtcSession, holdAccountOperationSelected);
		HolderAccountOperation holAccOperationRemoved = null;
		if(indiceHoldAccSelected!=null && indiceHoldAccSelected!=-1){//Validamos si el objeto enviado en la grilla, existe en la session
			holAccOperationRemoved = mechanismOperationOtcSession.getHolderAccountOperations().remove(indiceHoldAccSelected.intValue());
		}
		if(NegotiationUtils.mechanismOperationIsSwap(mechanismOperationOtcSession)){
			Integer indiceHoldAccSelectedSwap = OtcSwapOperationUtils.getIndicatorHoldAccOperation(swapOperationSession, this.holdAccountOperationSelected);
			if(indiceHoldAccSelectedSwap!=null && indiceHoldAccSelectedSwap!=-1){
				swapOperationSession.getHolderAccountOperations().remove(indiceHoldAccSelectedSwap.intValue());
			}
		}
		this.holdAccountOperationRequest.setRole(otcOperationUtils.getParticipantRoleByOrigin(mechanismOperationOtcSession,swapOperationSession, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		//Seteamos las cuentas negociadoras de acuerdo al participante logueado
		this.otcOperationParamSession.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidated(mechanismOperationOtcSession, swapOperationSession, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		if(NegotiationUtils.mechanismOperationIsSecundaryReport(mechanismOperationOtcSession) && OtcOperationUtils.haveStockPendientByRole(mechanismOperationOtcSession, swapOperationSession, ParticipantRoleType.SELL.getCode())){//Verificamos si la operacion es repo secundario Y si tiene cantidades pendiente como VENTA
			this.otcOperationParamSession.setIsQuantityPendientSecReport(Boolean.TRUE);//seteamos el valor true, porque se necesita agregar nuevas operaciones en la pantalla
			for(HolderAccountOperation holdAccOperation: this.otcOperationParamSession.getHoldAccNegotiationSellers()){//Rrecorremos los titulares vendedores plazo.(titulares de la operacion REPORTO original)
				if(holdAccOperation.getHolderAccount().equals(holAccOperationRemoved.getHolderAccount())){//si fue el que se elimino en repo secundario, Lo comparamos a nivel de cuentas(solo debe haber 1 cuenta diferente en cada operacion OTC)
					holdAccOperation.setIsSelected(Boolean.FALSE);//seteamos false para visualizarlo como No Chec
					holdAccOperation.setStockQuantity(null);//seteamos null para que la cantidad sea ingresada una vez mas
				}
			}
		}
		//Para el caso de que la modalidad tenga encargo, se reinicializa el indicador de manejar el Disabled O no.
		this.otcOperationParamSession.setIsInchargedDisabled(OtcOperationUtils.getInchargedDisabled(mechanismOperationOtcSession, swapOperationSession, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		otcOperationParamSession.setAsigmentOperationList(OtcOperationUtils.getAssigmentOperationList(mechanismOperationOtcSession));
	}
	/**
	 * Reset otc operation to.
	 * metodo para volver a inicializar OtcOperationTO
	 */
	public void reLoadOtcOperationTO(){
//		try{
			OtcOperationParameters parameters = otcOperationParamSession;
			List<Participant> allParticipant = parameters.getAllParticipantList();
			this.otcOperationParamSession.setInstrumentTypeSelectedSearch(InstrumentType.FIXED_INCOME.getCode());
			this.otcOperationTO = new OtcOperationTO();
			this.otcOperationTO.setInitialDate(CommonsUtilities.currentDate());
			this.otcOperationTO.setFinalDate(CommonsUtilities.currentDate());
			this.otcOperationTO.setIdSearchDateType(OtcOperationDateSearchType.OPERATION_DATE.getCode()); 
			this.otcOperationTO.setIndInCharge(0);
			if(userInfo.getUserAccountSession().isParticipantInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				this.otcOperationTO.setIdParticipantBuyer(idParticipantPkForIssuer);
				if(!allParticipant.contains(new Participant(otcOperationTO.getIdParticipantBuyer()))){//Si el participante no esta, Esta BLOQUEADO
					Participant participantTO = new Participant(otcOperationTO.getIdParticipantBuyer());
					try {
						if(Validations.validateIsNotNullAndPositive(idParticipantPkForIssuer)){
							List<Participant> lstParticipant = accountsFacade.getLisParticipantServiceBean(participantTO);
							if(participantList!=null && lstParticipant!=null) {
								participantList.addAll(lstParticipant);//Agregamos el participante BLOQUEADO a la lista
							}else {
								participantList = lstParticipant;
							}
							userInfo.getUserAccountSession().setInstitutionState(ParticipantStateType.BLOCKED.getCode());//Seteamos a userInfo que el participante esta bloqueado
						}	
					} catch (ServiceException e) {
						e.printStackTrace();
					}
				}
			}
	}
	/**
	 * Reset holder account operation request.
	 * metodo para limpiar HolderAccountOperation
	 */
	private void loadNewHolderAccountOperation(){//Limpiamos la Data de la cuenta de negociacion
		try {
			holdAccountOperationRequest = otcOperationFacade.getNewHolderAccountOperation(mechanismOperationOtcSession, isViewOperationReview());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Change holder.
	 */
	public void changeHolder(){
		HolderAccountOperation accountOperation = holdAccountOperationRequest; 
		OtcOperationParameters parameters = otcOperationParamSession;
		Holder holder = accountOperation.getHolderAccount().getHolder();
		if(Validations.validateIsEmpty(holder)) {
			accountOperation.getHolderAccount().setHolder(new Holder());
			parameters.setHolderAccountsAssigment(null);
			parameters.setAccountSelectedAssigment(null);
			return;
		}
		MechanismOperation operation = mechanismOperationOtcSession;
		SwapOperation swapOperation = swapOperationSession;
		Participant participant = otcOperationUtils.getPartToHoldAccNegotiation(operation, swapOperation, accountOperation, userInfo);
		try {
			if(holder.getIdHolderPk()==null){
				throw new ServiceException();
			}
			
//			if(operation.getCashSettlementDays()==null || operation.getCashSettlementDays()<0){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//									PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_SETTLEMENT_CASH_EMPTY));
//				JSFUtilities.showSimpleValidationDialog();
//				throw new ServiceException();
//			}
			
			List<HolderAccount> accountList = otcOperationFacade.findAccountsOnparticipant(holder, participant);
			if(accountList!=null && !accountList.isEmpty()){
				parameters.setHolderAccountsAssigment(new GenericDataModel<HolderAccount>(accountList));
				if(accountList.size()>0){
					for (int i = 0; i < accountList.size(); i++) {
						HolderAccount ha = accountList.get(i);
						ha.setAccountDescription(paramAccountTypeDesc.get(ha.getAccountType()));
						accountList.set(i, ha);
						//parameters.setAccountSelectedAssigment(accountList.get(i));
					}	
					
					if(accountList.size() == 1) {
						parameters.setAccountSelectedAssigment(accountList.get(0));
						changeHolderAccount();
					}
				}
			}else{
				otcOperationParamSession.setHolderAccountsAssigment(new GenericDataModel<HolderAccount>());
			}
			
			/**In case of sale add quantity automatly*/
//			if(accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
				accountOperation.setStockQuantity(operation.getStockQuantity());
				/**Call method to calculate cash amount according with stock quantity*/
				changeStockQuantityAssigment();
//			}
		} catch (ServiceException e) {
			accountOperation.getHolderAccount().setHolder(new Holder());
			parameters.setHolderAccountsAssigment(null);
			parameters.setAccountSelectedAssigment(null);
			
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	/**
	 * Evaluate participant sell handler.
	 */
	public void changeParticipantSeller(){
		if(OtcOperationUtils.otcOperationIsCrusade(mechanismOperationOtcSession)){
			otcOperationParamSession.setIsPartBuyerDisabled(Boolean.TRUE);
			if(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk() != null){
				mechanismOperationOtcSession.getBuyerParticipant().setIdParticipantPk(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk());
			}
		}else if(OtcOperationUtils.otcOperationIsSell(mechanismOperationOtcSession)){
			otcOperationParamSession.setIsPartBuyerDisabled(Boolean.FALSE);
			participantToBuyList = OtcOperationUtils.getParticipantsToBuy(mechanismOperationOtcSession, getParticipantList(),ViewOperationsType.get(this.getViewOperationType()));
		}
		
		if(mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk()!=null){
			cleanSecurity();
		}
	}
	/**
	 * Evaluate participant buy handler.
	 */
	public void changeParticipantBuyer(){
		if(OtcOperationUtils.otcOperationIsCrusade(mechanismOperationOtcSession)){
			if(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk() != null){
				mechanismOperationOtcSession.getSellerParticipant().setIdParticipantPk(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk());
			}
		}
//		if(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk()==null){
//			participantList = OtcOperationUtils.getParticipantsToBuy(mechanismOperationOtcSession, getParticipantList(),ViewOperationsType.get(getViewOperationType()));
//		}
		if(mechanismOperationOtcSession.getSecurities().getIdSecurityCodePk()!=null){
			cleanSecurity();
		}
	}
	/**
	 * Clean holder account negotiation handler.
	 * metodo para limpiar las cuentas negociadoras de acuerdo a rol seleccionado
	 */
	public void cleanHolderAccountNegotiationHandler(){
		if(!OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperationOtcSession, holdAccountOperationRequest.getRole()).isEmpty()){
			if(ParticipantRoleType.get(holdAccountOperationRequest.getRole()).equals(ParticipantRoleType.BUY)){//Si la participacion de la cuenta negociadora es compra
				mechanismOperationOtcSession.setHolderAccountOperations(OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode()));//Eliminamos la compra y traemos todas las ventas
				if(!OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode())){//Si la venta no tiene encargo
					this.holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//el indicador de encargo siempre debe aparecer en NO	
				}
			}else if(ParticipantRoleType.get(holdAccountOperationRequest.getRole()).equals(ParticipantRoleType.SELL)){//Si la participacion de la cuenta negociadora es Venta
				mechanismOperationOtcSession.setHolderAccountOperations(OtcOperationUtils.getHoldAccOperationsByNegotiatior(mechanismOperationOtcSession, ParticipantRoleType.BUY.getCode()));//Eliminamos las ventas y traemos todas las compras
			}
		}
//		JSFUtilities.resetComponent("frmOtcOperation:flsAccountNegotiation");
		holdAccountOperationRequest.setHolderAccount(new HolderAccount());
		holdAccountOperationRequest.getHolderAccount().setHolder(new Holder());
		holdAccountOperationRequest.setStockQuantity(null);
		holdAccountOperationRequest.setCashAmount(null);
		holdAccountOperationRequest.setInchargeFundsParticipant(new Participant());
//		//Limpiamos las cuentas Ingresadas, con el Tipo de Participacion Seleccionado
		otcOperationParamSession.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidated(mechanismOperationOtcSession, swapOperationSession, userInfo, ViewOperationsType.get(this.getViewOperationType())));
		otcOperationParamSession.setParticipantForChange(OtcOperationUtils.getParticipantForCharge(mechanismOperationOtcSession, this.participantList, holdAccountOperationRequest.getRole()));
	}
	/**
	 * Clean all form otc register.
	 */
	public void cleanAllFormOtcHandler(){//Para limpiar todo el formulario de OTC
		//clean swap operation
		swapOperationSession = new SwapOperation(new Security());
		swapOperationSession.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		
		BigDecimal dailyExchange = this.mechanismOperationOtcSession.getExchangeRate();//Obtenemos el tipo de cambio pactado inicialmente
		mechanismOperationOtcSession = otcOperationFacade.getNewMechanismOperation();//Tramos el Objeto MechanismOperation Limpio de data
		loadNewHolderAccountOperation();//Volvemos a cargar parametros OTC
		reLoadOtcOperationParameters();//Volvemos a cargar los parametros de otc
		loadMechanismOperationOtcSession();
		cleanSecurity();
		
		attachFileSelected.setDescription(null);
		attachFileSelected.setDocumentFile(null);
		observaciones = null;
		fileNameDisplay = null;
		
		JSFUtilities.resetComponent("frmOtcOperation");
		this.mechanismOperationOtcSession.setExchangeRate(dailyExchange);//Lo seteamos al objeto inicializado con nuevos campos
	}
	
	/**
	 * Clean search form otc handler.
	 */
	public void cleanSearchFormOtcHandler(){//Para limpiar formulario de busquedad OTC
		reLoadOtcOperationTO();
		this.otcOperationParamSession.setInstrumentTypeSelectedSearch(InstrumentType.FIXED_INCOME.getCode());
		evaluateMechanismModalityByInstrumentSearch();
		JSFUtilities.resetComponent("frmOtcOperationSearch:flsFilters");
		this.otcOperationsList = null;
	}
	/**
	 * Load cbo participants.
	 *
	 */
	//otcOperationParamSession
	public void beforeSaveNewMechanismOperation(){
		MechanismOperation operation = mechanismOperationOtcSession;
		SwapOperation swapOperation = swapOperationSession;
		/*MechanismOperationAdj aditionalInf = attachFileSelected;*/
		try {
			String messageProperties = "";
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	
					//if (Validations.validateIsNotNullAndNotEmpty(aditionalInf)) {
					//	if (observaciones != null && observaciones.trim().length()>0 
					//			&& aditionalInf.getDocumentFile() != null) {
							otcOperationFacade.validateBeforeSaveNewOperation(operation, swapOperation);
							messageProperties = PropertiesConstants.OTC_OPERATION_REGISTER_CONFIRM;break;
				   //     } 		
					//}				
				case APPROVE:	otcOperationFacade.validateBeforeApproveOperation(operation, swapOperation);
								messageProperties = PropertiesConstants.OTC_OPERATION_APPROVE_CONFIRM;break;
								
				case REVIEW:	otcOperationFacade.validateBeforeReviewOperation(operation, swapOperation);
								messageProperties = PropertiesConstants.OTC_OPERATION_REVIEW_CONFIRM;break;
								
				case CONFIRM:	otcOperationFacade.validateBeforeConfirmOperation(operation, swapOperation);
								messageProperties = PropertiesConstants.OTC_OPERATION_CONFIRM_CONFIRM;break;
								
				case CANCEL:	messageProperties = PropertiesConstants.OTC_OPERATION_CANCEL_CONFIRM;break;
				
				case SETTLEMENT:
								otcOperationFacade.validateBeforeSettlementOperation(operation, swapOperation);
								messageProperties = PropertiesConstants.OTC_OPERATION_SETTLEMENT_CONFIRM;
								break;							
				case AUTHORIZE:
								otcOperationFacade.validateBeforeAuthorizeOperation(operation, swapOperation);
								messageProperties = PropertiesConstants.OTC_OPERATION_AUTHORIZE_CONFIRM;
								break;
				default:break;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(messageProperties,operation.getIdMechanismOperationPk()));
			JSFUtilities.executeJavascriptFunction("PF('cnfwOtcOperations').show()");
		} catch(ServiceException e) {
			if(ViewOperationsType.get(getViewOperationType()).name() == "REGISTER") {
				/*if (Validations.validateIsNotNullAndNotEmpty(aditionalInf)) {
					if (observaciones != null && observaciones.trim().length()>0 
							&& aditionalInf.getDocumentFile() != null) {
						if(e.getErrorService()!=null){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
							JSFUtilities.showSimpleValidationDialog();
						}
			        } else {
			        	showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED_ADJUNTOS,null);
						JSFUtilities.showSimpleValidationDialog();
			        }
				} else {
					showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED_ADJUNTOS,null);
					JSFUtilities.showSimpleValidationDialog();
				}*/

				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			} else {
				if(e.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
	}
	
	/**
	 * Register otc operation.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	private void registerOtcOperation() throws ServiceException{
		//Obtenemos la modalidad de la operacion a registrar
		MechanismOperation mechanismOperation = mechanismOperationOtcSession;//Creamos la referencia al Objeto MechanisOperation a Grabar
		SwapOperation swapOperation = swapOperationSession;
		/*attachFileSelected.setDescription(observaciones);
		attachFileSelected.setFileName(fileNameDisplay);*/
		MechanismOperation newOperation = otcOperationFacade.registryNewMechanismOperation(mechanismOperation,swapOperation, attachFileSelected);
		
		// Send notification
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk())) {
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.OTC_OPERATION_REGISTER.getCode());			
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk(), 
					   new Object[]{newOperation.getIdMechanismOperationPk().toString()}, userInfo.getUserAccountSession());
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_REGISTER_CONFIRM_OK,
															new Object[]{newOperation.getIdMechanismOperationPk().toString()}));
		
	}
	
	public void documentAttachFile(FileUploadEvent event){
		String fDisplayName=fUploadValidateFile(event.getFile(),null, getfUploadFileDocumentsSize(),getfUploadFileDocumentsTypes());
		if(fDisplayName!=null){
			 fileNameDisplay = fDisplayName;
		}
		MechanismOperationAdj mechanismOperationAdj = new MechanismOperationAdj();
		mechanismOperationAdj.setDocumentFile(event.getFile().getContents());
		mechanismOperationAdj.setMechanismOperation(mechanismOperationOtcSession);
		attachFileSelected = mechanismOperationAdj;
	}
	
	public void authorizeOtcOperation() throws ServiceException {
		
		MechanismOperation mechanismOperation = mechanismOperationOtcSession;
		mechanismOperation.getTradeOperation().setAuthorizeUser(userInfo.getUserAccountSession().getUserName());
		mechanismOperation = otcOperationFacade.authorizeOtcOperationServiceFacade(mechanismOperation);
		
		// Send notification
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk())) {
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.OTC_OPERATION_AUTHORIZE.getCode());			
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, mechanismOperationOtcSession.getSellerParticipant().getIdParticipantPk(), 
					   new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()}, userInfo.getUserAccountSession());
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_AUTHORIZE_CONFIRM_OK,
												new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()}));
		
	}
	
	/**
	 * Confirm otc operation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void confirmOtcOperation() throws ServiceException{
		MechanismOperation mechanismOperation = this.mechanismOperationOtcSession;//Obtenemos el objeto a confirmar
		NegotiationModality negModality = otcOperationFacade.findNegModality(mechanismOperationOtcSession, otcOperationParamSession.getMechanismModalityList());
		mechanismOperation.getTradeOperation().setConfirmDate(CommonsUtilities.currentDateTime());
		mechanismOperation.getTradeOperation().setConfirmUser(userInfo.getUserAccountSession().getUserName());
		
		mechanismOperation = otcOperationFacade.confirmOtcOperationServiceFacade(mechanismOperation);
		
		BusinessProcess objBusinessProcess = new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode());
		Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(ComponentConstant.SETTLEMENT_SCHEMA, SettlementSchemaType.GROSS.getCode());
		batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
		
		// Send notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.OTC_OPERATION_CONFIRM.getCode());			
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
				   businessProcess, null, new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()});
		//notificacion xml via web services
		
		//issue 747: ver la documentacion del metodo: getAccountOperationValidatedToConfirmedOTC
		otcOperationParamSession.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidatedToConfirmedOTC(mechanismOperationOtcSession, swapOperationSession, ViewOperationsType.get(this.getViewOperationType())));
		
		//sendSettlementTransferOperationWebClient(mechanismOperation,otcOperationParamSession);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_CONFIRM_CONFIRM_OK,
												new Object[]{mechanismOperation.getIdMechanismOperationPk().toString(),
															 mechanismOperation.getOperationNumber(),
															 negModality.getModalityName()}));
		
	}
	
	private void sendSettlementTransferOperationWebClient(MechanismOperation mechanismOperation,OtcOperationParameters operationParameters){
		List<SettlementOperationTO> lstSettlementOperationTOs=new ArrayList<>();
		SettlementOperationTO operationTO=new SettlementOperationTO();
		operationTO.setIdSettlementOperation(SettlementSchemaType.GROSS.getCode().longValue());
		//operationTO.setIdSettlementOperation(BusinessProcessType.OTC_OPERATION_CONFIRM.getCode());
		operationTO.setIdMechanismOperation(mechanismOperation.getIdMechanismOperationPk());                        /////
		operationTO.setIdReferencedOperation(BusinessProcessType.OTC_OPERATION_CONFIRM.getCode());                                                                   /////
		operationTO.setIdMechanism(mechanismOperation.getIdMechanismOperationPk());                                                                             /////
		operationTO.setInstrumentType(mechanismOperation.getSecurities().getInstrumentType());
		operationTO.setIdSecurityCode(mechanismOperation.getSecurities().getIdSecurityCodePk());
		operationTO.setStockQuantity(mechanismOperation.getStockQuantity());
		operationTO.setSettlementType(mechanismOperation.getSettlementType());
		operationTO.setSettlementSchema(mechanismOperation.getSettlementSchema());
		operationTO.setCurrency(mechanismOperation.getCurrency());
		operationTO.setIndReportingBalance(mechanismOperation.getIndReportingBalance());
		operationTO.setIndPrincipalGuarantee(mechanismOperation.getIndPrimaryPlacement());
		operationTO.setIndMarginGuarantee(mechanismOperation.getIndMarginGuarantee());
		operationTO.setIndTermSettlement(mechanismOperation.getIndTermSettlement());
		operationTO.setIndCashStockBlock(mechanismOperation.getIndCashSettlementExchange());
		operationTO.setIndTermStockBlock(mechanismOperation.getIndTermStockBlock());
		operationTO.setIndPrimaryPlacement(mechanismOperation.getIndPrimaryPlacement());
		operationTO.setCurrentNominalValue(mechanismOperation.getNominalValue());
		operationTO.setInterfaceTransferCode(mechanismOperation.getMechanisnModality().getInterfaceTransferCode());                                                                 /////
		operationTO.setSecurityClass(mechanismOperation.getSecurities().getSecurityClass());
		operationTO.setOperationNumber(mechanismOperation.getOperationNumber());
		operationTO.setLstBuyerHolderAccounts(setLstBuyerHolderAccounts(operationParameters));         //comprador                                                 /////
		operationTO.setLstSellerHolderAccounts(setLstSellerHolderAccounts(operationParameters));       //vendedor
		//operationTO.setIndCompleteSettlement(1);                                                                    /////
		lstSettlementOperationTOs.add(operationTO);
		otcOperationFacade.sendSettlementOperationWebClient(lstSettlementOperationTOs);
	}
	
	public List<SettlementHolderAccountTO>  setLstSellerHolderAccounts(OtcOperationParameters operationParameters){
		HolderAccount holderAccount=operationParameters.getHoldAccNegotiationValidated().get(0).getHolderAccount();
		SettlementHolderAccountTO accountTO=new SettlementHolderAccountTO();
		accountTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		List<SettlementHolderAccountTO> lstBuyerHolderAccounts=new ArrayList<>();
		lstBuyerHolderAccounts.add(accountTO);
		return lstBuyerHolderAccounts;
	}
	public List<SettlementHolderAccountTO>  setLstBuyerHolderAccounts(OtcOperationParameters operationParameters){
		HolderAccount holderAccount=operationParameters.getHoldAccNegotiationValidated().get(1).getHolderAccount();
		SettlementHolderAccountTO accountTO=new SettlementHolderAccountTO();
		accountTO.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		List<SettlementHolderAccountTO> lstBuyerHolderAccounts=new ArrayList<>();
		lstBuyerHolderAccounts.add(accountTO);
		return lstBuyerHolderAccounts;
	}
	/**
	 * Save otc request handler.
	 */
	@LoggerAuditWeb
	public void saveOtcRequestHandler(){
		try{
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	registerOtcOperation();break;
				case CONFIRM:  	confirmOtcOperation();break;
				case APPROVE: 	approveOtcOperation();break;
				case REVIEW: 	reviewOtcOperation();break;
				case ANULATE: 	anulateOtcOperation();break;
				case REJECT: 	rejectOtcOperation();break;
				case CANCEL: 	cancelOtcOperation();break;
				case SETTLEMENT:settlementOtcOperation(); break;
				case AUTHORIZE:	authorizeOtcOperation(); break;
				default:		break;
			}
			if(otcOperationsList!=null){//si es null, ya se realizo una busquedad. procedemos a hacer un buscar automatico
				searchOtcOperationsHandler();//load otc operations again
			}
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog((PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)), 
									 PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
		}

		JSFUtilities.executeJavascriptFunction("PF('cnfwOtcOkOperations').show()");
		//JSFUtilities.showSimpleEndTransactionDialog("searchOtcOperation");
	}

	/**
	 * Search otc operations handler.
	 */
	@LoggerAuditWeb
	public void searchOtcOperationsHandler(){
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
				&& Validations.validateIsNullOrNotPositive(idParticipantPkForIssuer)){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
			return;// GeneralConstants.EMPTY_STRING;
		}
		OtcOperationTO otcOperationTO = new OtcOperationTO();
		otcOperationTO.setInitialDate(this.otcOperationTO.getInitialDate());
		otcOperationTO.setFinalDate(this.otcOperationTO.getFinalDate());
		//otcOperationTO.setIndInCharge(this.otcOperationTO.getIndInCharge());
		otcOperationTO.setIdSearchDateType(this.otcOperationTO.getIdSearchDateType());
		otcOperationTO.setIdModalityNegotiation(this.otcOperationTO.getIdModalityNegotiation());
		otcOperationTO.setOtcOperationState(this.otcOperationTO.getOtcOperationState());
		otcOperationTO.setIdParticipantBuyer(this.otcOperationTO.getIdParticipantBuyer());
		otcOperationTO.setIndDpf(ComponentConstant.ZERO); //issue 800
		//otcOperationTO.setInstrumentType(otcOperationParamSession.getInstrumentTypeSelectedSearch()); //Issue 778
		List<OtcOperationResultTO> otcOperationsTempList = null;
		try {
			//Buscamos todas las solicitudes OTC, con los criterios ingresados
			otcOperationsTempList = otcOperationFacade.getOtcOperationsServiceFacade(otcOperationTO,parameterDescription); 
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if(otcOperationsTempList==null){
			otcOperationsList = new GenericDataModel<OtcOperationResultTO>();
		}else{
			otcOperationsList = new GenericDataModel<OtcOperationResultTO>(otcOperationsTempList);
		}
	}
	
	/**
	 * Selected otc operation object handler.
	 * Metodo que lee el registro de la operacion OTC desde la BD
	 * method invocated from CommandLink on DataTable
	 *
	 * @param otcOperationResultTO the otc operation result to
	 */
	private void readOtcOperationObject(OtcOperationResultTO otcOperationResultTO){
		OtcOperationTO otcOperationTO = new OtcOperationTO();//Creamos TO para traer el Objeto OTC		
		otcOperationTO.getIdMechanismOperation();
		otcOperationTO.setIdMechanismOperation(otcOperationResultTO.getIdMechanismOperationRequestPk());
		otcOperationTO.setIdModalityNegotiation(otcOperationResultTO.getIdNegotiationModalityPk());
		otcOperationTO.setNeedAccounts(true);
		MechanismOperation mechanismOperation = null;
		MechanismOperationAdj mechanismOperationAdj = null;
		try {
			mechanismOperation = otcOperationFacade.getOtcOperationServiceFacade(otcOperationTO);//Buscamos el Objeto para ponerlo en la Session de la Vista
			mechanismOperationAdj = otcOperationFacade.getMechanismOperationAdj(otcOperationTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if(mechanismOperation!=null){
			if(Validations.validateIsNull(otcOperationTO)){
				this.otcOperationTO = otcOperationTO; //para reutilizar getMechanismOperationAdj
			}else{
				this.otcOperationTO.setIdMechanismOperation(otcOperationResultTO.getIdMechanismOperationRequestPk());
				this.otcOperationTO.setIdModalityNegotiation(otcOperationResultTO.getIdNegotiationModalityPk());
				this.otcOperationTO.setNeedAccounts(true);
			}
			
			mechanismOperationOtcSession = mechanismOperation;//Asignamos el Objeto a la session de la vista
			if(mechanismOperationAdj!=null){
				attachFileSelected = mechanismOperationAdj;
				observaciones = attachFileSelected.getDescription();
				fileNameDisplay = attachFileSelected.getFileName();
			}			
			loadNewHolderAccountOperation();
			holdAccountOperationRequest.setRole(otcOperationUtils.getParticipantRoleByOrigin(mechanismOperation,swapOperationSession, userInfo, ViewOperationsType.get(this.getViewOperationType())));
			otcOperationParamSession.setInstrumentTypeSelected(mechanismOperation.getSecurities().getInstrumentType());//Seteamos el instrumento al Objecto OTC
		}
		changeOperationInstrument();//Cargamos las modalidades, Por defecto renta fija esta seleccionado
	}
	/**
	 * @throws ServiceException */
	public StreamedContent getDocumentoAdjunto() throws ServiceException{
		MechanismOperationAdj mechanismOperationAdj = otcOperationFacade.getMechanismOperationAdj(otcOperationTO);
		byte[] bs = mechanismOperationAdj.getDocumentFile();
		 if(Validations.validateIsNotNull(bs)){						
			 InputStream stream = new ByteArrayInputStream(bs);
			 fileDownload = new DefaultStreamedContent(stream,null,mechanismOperationAdj.getFileName()); 
		 } 
		return fileDownload;
	}
	
	/**
	 * See otc operation object handler.
	 *
	 * @param otcOperationResultTO the otc operation result to
	 */
	public void seeOtcOperationObjectHandler(OtcOperationResultTO otcOperationResultTO){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());//Seteamos que la session se Ira a Modificar
		otcOperationParamSession.setIsHoldAccNegDelButtEnabled(Boolean.FALSE);//Seteamos el indicador, para NO poder eliminar cuentas desde la grilla
		readOtcOperationObject(otcOperationResultTO);
		loadOtcParameters();//Cargamos Algumos parametros de OTC 
	}
	
	/**
	 * Review otc operation action.
	 *
	 * @return the string
	 */
	public String anulateOtcOperationAction(){
		if(otcOperationSession==null){
			String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED);
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		List<String> lstOperationUser = validateIsValidUser(otcOperationSession,ViewOperationsType.ANULATE.getCode());
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		OtcOperationResultTO otcOperationResultTO = otcOperationSession;
		//Verificamos que el objeto seleccionado no es Null y que se encuentre registrado
		if(otcOperationResultTO!=null && (otcOperationResultTO.getOperationState().equals(OtcOperationStateType.REGISTERED.getCode()))){
			this.setViewOperationType(ViewOperationsType.ANULATE.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){//Si es cevaldom, puede hacer todo
				return "otcOperationMgmtRule";
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que compra
					if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode())){//Validamos si la operacion tiene Encargo
						executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT,"PF('cnfwMsgCustomValidationNeg').show()");
						return StringUtils.EMPTY;
					}else{
						holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.SELL.getCode())){//Se tiene encargo y es el encargado
					holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
					holdAccountOperationRequest.setInchargeFundsParticipant(participantState);//Seteamos el encargo al participante seleccionado
					return "otcOperationMgmtRule";
				}
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT));
		JSFUtilities.showSimpleValidationDialog();
		return StringUtils.EMPTY;
	}
	
	/**
	 * Reject otc operation action.
	 *
	 * @return the string
	 */
	public String rejectOtcOperationAction(){
		if(otcOperationSession==null){
			String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED);
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		List<String> lstOperationUser = validateIsValidUser(otcOperationSession,ViewOperationsType.REJECT.getCode());
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		OtcOperationResultTO otcOperationResultTO = otcOperationSession;
		//Verificamos que el objeto seleccionado no es Null y que se encuentre registrado
		List<Integer> otcOperationsStates = Arrays.asList(OtcOperationStateType.REVIEWED.getCode(),OtcOperationStateType.APROVED.getCode(), OtcOperationStateType.AUTHORIZED.getCode());
		if(otcOperationResultTO!=null && (otcOperationsStates.contains(otcOperationResultTO.getOperationState()))){
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){//Si es cevaldom, puede hacer todo
				return "otcOperationMgmtRule";
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(otcOperationResultTO.getOperationState().equals(OtcOperationStateType.REVIEWED.getCode())
						|| otcOperationResultTO.getOperationState().equals(OtcOperationStateType.APROVED.getCode())) {
					if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que compra
						if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.BUY.getCode())){//Validamos si la operacion tiene Encargo
							executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT,"PF('cnfwMsgCustomValidationNeg').show()");
							return StringUtils.EMPTY;
						}else{
							holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
							return "otcOperationMgmtRule";
						}
					}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.SELL.getCode())){//Se tiene encargo y es el encargado
						holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
						holdAccountOperationRequest.setInchargeFundsParticipant(new Participant(userInfo.getUserAccountSession().getParticipantCode()));//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				} else {
					if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que vende
						if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode())){//Validamos si la operacion tiene Encargo
							executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT,"PF('cnfwMsgCustomValidationNeg').show()");
							return StringUtils.EMPTY;
						}else{
							holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
							return "otcOperationMgmtRule";
						}
					}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.SELL.getCode())){//Se tiene encargo y es el encargado
						holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
						holdAccountOperationRequest.setInchargeFundsParticipant(new Participant(userInfo.getUserAccountSession().getParticipantCode()));//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				}
				
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT));
		JSFUtilities.showSimpleValidationDialog();
		return StringUtils.EMPTY;
	}
	
	/**
	 * Review otc operation action.
	 *
	 * @return the string
	 */
	public String reviewOtcOperationAction(){
		if(otcOperationSession==null){
			String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED);
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		OtcOperationResultTO otcOperationResultTO = this.otcOperationSession;
		//Verificamos que el objeto seleccionado no es Null y que se encuentre registrado
		if(otcOperationResultTO!=null && (otcOperationResultTO.getOperationState().equals(OtcOperationStateType.APROVED.getCode()))){
			this.setViewOperationType(ViewOperationsType.REVIEW.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){//Si es cevaldom, puede hacer todo
				return "otcOperationMgmtRule";
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que compra
					otcOperationParamSession.setIsHoldAccNegDelButtEnabled(Boolean.TRUE);
					if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.BUY.getCode())){//Validamos si la operacion tiene Encargo
						executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT,"PF('cnfwMsgCustomValidationNeg').show()");
						return StringUtils.EMPTY;
					}else{
						holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.BUY.getCode())){//Se tiene encargo y es el encargado
					holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
					holdAccountOperationRequest.setInchargeFundsParticipant(new Participant(idParticipantPkForIssuer));//Seteamos el encargo al participante seleccionado
					return "otcOperationMgmtRule";
				}
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT));
		JSFUtilities.showSimpleValidationDialog();
		return StringUtils.EMPTY;
	}
	
	/**
	 * Cancel otc operation action.
	 *
	 * @return the string
	 */
	public String cancelOtcOperationAction(){
		OtcOperationResultTO otcOperationResultTO = otcOperationSession;
		if(otcOperationResultTO==null){
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED,"PF('cnfwMsgCustomValidationNeg').show()");
			return StringUtils.EMPTY;
		}
		List<String> lstOperationUser = validateIsValidUser(otcOperationSession,ViewOperationsType.CANCEL.getCode());
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		if(otcOperationResultTO.getOperationState().equals(OtcOperationStateType.CONFIRMED.getCode())){
			setViewOperationType(ViewOperationsType.CANCEL.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC
			MechanismOperation operation = mechanismOperationOtcSession;
			
			List<Integer> statesToCancel = Arrays.asList(MechanismOperationStateType.ASSIGNED_STATE.getCode(), MechanismOperationStateType.CASH_SETTLED.getCode());
			if(statesToCancel.contains(operation.getOperationState())){
//				Date currDate = CommonsUtilities.currentDate();
//				if(operation.getOperationState().equals(MechanismOperationStateType.ASSIGNED_STATE.getCode())){
//					if((operation.getCashSettlementDate().after(currDate) && !BooleanType.YES.getCode().equals(operation.getIndCashExtended()) && 
//																		  !BooleanType.YES.getCode().equals(operation.getIndCashPrepaid())) || 
//						operation.getCashExtendedDate()!=null && operation.getCashExtendedDate().after(currDate) && BooleanType.YES.getCode().equals(operation.getIndCashExtended()) ||
//						operation.getCashPrepaidDate()!=null && operation.getCashPrepaidDate().after(currDate) && BooleanType.YES.getCode().equals(operation.getIndCashPrepaid())){
//						return "otcOperationMgmtRule";
//					}
//				}else if(operation.getMechanisnModality().getNegotiationModality().getIndTermSettlement().equals(BooleanType.YES.getCode())){
//					if((operation.getTermSettlementDate().after(currDate) && !BooleanType.YES.getCode().equals(operation.getIndTermExtended()) && 
//													  					  !BooleanType.YES.getCode().equals(operation.getIndTermPrepaid())) || 
//						operation.getTermExtendedDate()!=null && operation.getTermExtendedDate().after(currDate) && BooleanType.YES.getCode().equals(operation.getIndTermExtended()) ||
//						operation.getTermPrepaidDate()!=null && operation.getTermPrepaidDate().after(currDate) && BooleanType.YES.getCode().equals(operation.getIndTermPrepaid())){
//						return "otcOperationMgmtRule";
//					}
//				}
				return "otcOperationMgmtRule";
			}
		}
		executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT,"PF('cnfwMsgCustomValidationNeg').show()");
		return StringUtils.EMPTY;
	}
	
	/**
	 * Confirm operation action.
	 *
	 * @return the string
	 * @throws ServiceException 
	 */
	public String confirmOperationAction() throws ServiceException{
		
		if(otcOperationSession==null){
			String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED);
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}

		MechanismOperation mechanismOperation = new MechanismOperation();
		mechanismOperation.setOperationNumber(otcOperationSession.getOperationNumber()); 
		mechanismOperation.setOperationDate(otcOperationSession.getOperationDate());
		mechanismOperation.setIdMechanismOperationPk(otcOperationSession.getIdMechanismOperationRequestPk());
		mechanismOperation.setMechanisnModality(new MechanismModality(new MechanismModalityPK()));
		mechanismOperation.getMechanisnModality().getId().setIdNegotiationModalityPk(otcOperationSession.getIdNegotiationModalityPk());
		MechanismOperation temp = otcOperationFacade.findMechanismOperationAvailableBal(mechanismOperation);
		
		Participant participant = new Participant();
		HolderAccount holderAcc = null;
		Security security = new Security();
		participant.setIdParticipantPk(otcOperationSession.getIdPartSeller());
		List<HolderAccountOperation> list = temp.getHolderAccountOperations();
		for (HolderAccountOperation holderAccountOperation : list) {			
			if(holderAccountOperation.getRole().equals(2)){
				holderAcc = holderAccountOperation.getHolderAccount();
			}
		}		
		security.setIdSecurityCodePk(otcOperationSession.getSecurityCode());
		HolderAccountBalance hab = null;
		hab = otcOperationFacade.getHolderAccountBalance(participant, holderAcc, security);
		int res = hab.getAvailableBalance().compareTo(temp.getStockQuantity());
		if (hab != null && res == -1) {			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_AVAILABLE_BALANCE));
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;			
		}
		
		List<String> lstOperationUser = validateIsValidUser(otcOperationSession,ViewOperationsType.CONFIRM.getCode());
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		OtcOperationResultTO otcOperationResultTO = this.otcOperationSession;
		if(otcOperationResultTO!=null && 
				( (!otcOperationResultTO.getSettlementType().equals(SettlementType.FOP.getCode()) && otcOperationResultTO.getOperationState().equals(OtcOperationStateType.REVIEWED.getCode()))
				  || (otcOperationResultTO.getSettlementType().equals(SettlementType.FOP.getCode()) && otcOperationResultTO.getOperationState().equals(OtcOperationStateType.AUTHORIZED.getCode())))){
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC			

			if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if( otcOperationResultTO.getSettlementType().equals(SettlementType.FOP.getCode()) ) {
					if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que compra
						if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode())){//Validamos si la operacion tiene Encargo
							showExceptionMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT);
							JSFUtilities.showSimpleValidationDialog();
							return StringUtils.EMPTY;
						}else{
							holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
							return "otcOperationMgmtRule";
						}
					}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.SELL.getCode())){//Se tiene encargo y es el encargado
						holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
						holdAccountOperationRequest.setInchargeFundsParticipant(participantState);//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				} else {
					if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que compra
						if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.BUY.getCode())){//Validamos si la operacion tiene Encargo
							showExceptionMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT);
							JSFUtilities.showSimpleValidationDialog();
							return StringUtils.EMPTY;
						}else{
							holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
							return "otcOperationMgmtRule";
						}
					}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.BUY.getCode())){//Se tiene encargo y es el encargado
						holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
						holdAccountOperationRequest.setInchargeFundsParticipant(participantState);//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				}
			}
			else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				return "otcOperationMgmtRule";
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT));
		JSFUtilities.showSimpleValidationDialog();
		return StringUtils.EMPTY;
	}
	public boolean validaSaldoDisponible() {
		
		return true;
	}
	/**
	 * Review otc operation.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void reviewOtcOperation() throws ServiceException{
		MechanismOperation mechanismOperation = this.mechanismOperationOtcSession;
		mechanismOperation.getTradeOperation().setReviewUser(userInfo.getUserAccountSession().getUserName());
		mechanismOperation = otcOperationFacade.reviewOtcOpereationServiceFacade(mechanismOperation);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_REVIEW_CONFIRM_OK,
											new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()}));
		
	}
	
	/**
	 * Reject otc operation.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void rejectOtcOperation() throws ServiceException{
		MechanismOperation mechanismOperation = this.mechanismOperationOtcSession;
		mechanismOperation.getTradeOperation().setRejectUser(userInfo.getUserAccountSession().getUserName());
		mechanismOperation = otcOperationFacade.rejectOtcOperationServiceFacade(mechanismOperation);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_REJECT_CONFIRM_OK,
													new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()}));
		
	}
	
	/**
	 * Cancel otc operation.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void cancelOtcOperation() throws ServiceException{
		MechanismOperation mechanismOperation = this.mechanismOperationOtcSession;
		mechanismOperation.getTradeOperation().setRejectUser(userInfo.getUserAccountSession().getUserName());
		mechanismOperation.setCancelMotive(otcOperationParamSession.getMotiveCancel());
		mechanismOperation.setCancelMotiveOther(otcOperationParamSession.getOtherMotiveCancel());
		
		mechanismOperation = otcOperationFacade.cancelOtcOperationServiceFacade(mechanismOperation);

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_CANCEL_CONFIRM_OK,
															new Object[]{mechanismOperation.getOperationNumber()}));
		
	}
	
	/**
	 * Anulate otc operation.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void anulateOtcOperation() throws ServiceException{
		MechanismOperation mechanismOperation = this.mechanismOperationOtcSession;
		mechanismOperation.getTradeOperation().setRejectUser(userInfo.getUserAccountSession().getUserName());
		mechanismOperation = otcOperationFacade.anulateOtcOperationServiceFacade(mechanismOperation);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_ANNULATE_CONFIRM_OK,
														new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()}));
		
	}
	
	@LoggerAuditWeb
	public String authorizeOtcOperationAction() throws ServiceException{
		if(otcOperationSession==null){
			String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED);
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}

		MechanismOperation mechanismOperation = new MechanismOperation();
		mechanismOperation.setOperationNumber(otcOperationSession.getOperationNumber()); 
		mechanismOperation.setOperationDate(otcOperationSession.getOperationDate());
		mechanismOperation.setIdMechanismOperationPk(otcOperationSession.getIdMechanismOperationRequestPk());
		mechanismOperation.setMechanisnModality(new MechanismModality(new MechanismModalityPK()));
		mechanismOperation.getMechanisnModality().getId().setIdNegotiationModalityPk(otcOperationSession.getIdNegotiationModalityPk());
		MechanismOperation temp = otcOperationFacade.findMechanismOperationAvailableBal(mechanismOperation);
		
		Participant participant = new Participant();
		HolderAccount holderAcc = null;
		Security security = new Security();
		participant.setIdParticipantPk(otcOperationSession.getIdPartSeller());
		List<HolderAccountOperation> list = temp.getHolderAccountOperations();
		for (HolderAccountOperation holderAccountOperation : list) {			
			if(holderAccountOperation.getRole().equals(2)){
				holderAcc = holderAccountOperation.getHolderAccount();
			}
		}		
		security.setIdSecurityCodePk(otcOperationSession.getSecurityCode());
		HolderAccountBalance hab = null;
		hab = otcOperationFacade.getHolderAccountBalance(participant, holderAcc, security);
		int res = hab.getAvailableBalance().compareTo(temp.getStockQuantity());
		if (hab != null && res == -1) {			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_AVAILABLE_BALANCE));
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;			
		}
		
		List<String> lstOperationUser = validateIsValidUser(otcOperationSession,ViewOperationsType.CONFIRM.getCode());
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		OtcOperationResultTO otcOperationResultTO = this.otcOperationSession;
		if(otcOperationResultTO!=null && 
				( otcOperationResultTO.getSettlementType().equals(SettlementType.FOP.getCode()) && otcOperationResultTO.getOperationState().equals(OtcOperationStateType.REVIEWED.getCode()))){
			setViewOperationType(ViewOperationsType.AUTHORIZE.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(OtcOperationUtils.participantIsBuyer(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que compra
					if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.BUY.getCode())){//Validamos si la operacion tiene Encargo
						showExceptionMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT);
						JSFUtilities.showSimpleValidationDialog();
						return StringUtils.EMPTY;
					}else{
						holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.BUY.getCode())){//Se tiene encargo y es el encargado
					holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
					holdAccountOperationRequest.setInchargeFundsParticipant(participantState);//Seteamos el encargo al participante seleccionado
					return "otcOperationMgmtRule";
				}
			}
			else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				return "otcOperationMgmtRule";
			}
		}
		
		if(otcOperationResultTO!=null && !otcOperationResultTO.getSettlementType().equals(SettlementType.FOP.getCode())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_NOT_FOP));
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT));
		JSFUtilities.showSimpleValidationDialog();
		return StringUtils.EMPTY;
	}
	
	public String confirmSettlementOperationAction() throws ServiceException{
		
		if(otcOperationSession==null){
			String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED);
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}

		MechanismOperation mechanismOperation = new MechanismOperation();
		mechanismOperation.setOperationNumber(otcOperationSession.getOperationNumber()); 
		mechanismOperation.setOperationDate(otcOperationSession.getOperationDate());
		mechanismOperation.setIdMechanismOperationPk(otcOperationSession.getIdMechanismOperationRequestPk());
		mechanismOperation.setMechanisnModality(new MechanismModality(new MechanismModalityPK()));
		mechanismOperation.getMechanisnModality().getId().setIdNegotiationModalityPk(otcOperationSession.getIdNegotiationModalityPk());
		MechanismOperation temp = otcOperationFacade.findMechanismOperationAvailableBal(mechanismOperation);
		
		Participant participant = new Participant();
		HolderAccount holderAcc = null;
		Security security = new Security();
		participant.setIdParticipantPk(otcOperationSession.getIdPartSeller());
		List<HolderAccountOperation> list = temp.getHolderAccountOperations();
		for (HolderAccountOperation holderAccountOperation : list) {			
			if(holderAccountOperation.getRole().equals(2)){
				holderAcc = holderAccountOperation.getHolderAccount();
			}
		}		
		security.setIdSecurityCodePk(otcOperationSession.getSecurityCode());
		
		if(!temp.getSettlementType().equals(SettlementType.FOP.getCode())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_NOT_FOP));
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;	
		}
		
		List<String> lstOperationUser = validateIsValidSettlementUser(otcOperationSession,ViewOperationsType.SETTLEMENT.getCode());
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		OtcOperationResultTO otcOperationResultTO = this.otcOperationSession;
		if(otcOperationResultTO!=null && (otcOperationResultTO.getOperationState().equals(OtcOperationStateType.CONFIRMED.getCode()))){

			if(otcOperationResultTO.getSettlementState().equals(GeneralConstants.ONE_VALUE_INTEGER)) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_ALREADY_SETTLED));
				JSFUtilities.showSimpleValidationDialog();
				return StringUtils.EMPTY;
			}
			
			HolderAccountBalance hab = null;
			hab = otcOperationFacade.getHolderAccountBalance(participant, holderAcc, security, Boolean.FALSE);
			int res = hab.getSaleBalance().compareTo(temp.getStockQuantity());
			if (hab != null && res == -1) {			
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_SELL_PENDING_BALANCE));
				JSFUtilities.showSimpleValidationDialog();
				return StringUtils.EMPTY;			
			}
			
			setViewOperationType(ViewOperationsType.SETTLEMENT.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que compra
					if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode())){//Validamos si la operacion tiene Encargo
						showExceptionMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT);
						JSFUtilities.showSimpleValidationDialog();
						return StringUtils.EMPTY;
					}else{
						holdAccountOperationRequest.setIndIncharge(BooleanType.NO.getCode());//Seteamos el encargo al participante seleccionado
						return "otcOperationMgmtRule";
					}
				}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.SELL.getCode())){//Se tiene encargo y es el encargado
					holdAccountOperationRequest.setIndIncharge(BooleanType.YES.getCode());//Seteamos el encargo al participante seleccionado
					holdAccountOperationRequest.setInchargeFundsParticipant(participantState);//Seteamos el encargo al participante seleccionado
					return "otcOperationMgmtRule";
				}
			}
			else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				return "otcOperationMgmtRule";
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT));
		JSFUtilities.showSimpleValidationDialog();
		return StringUtils.EMPTY;
	}
	
	public void settlementOtcOperation() throws ServiceException{
		MechanismOperation mechanismOperation = this.mechanismOperationOtcSession;//Obtenemos el objeto a confirmar
		NegotiationModality negModality = otcOperationFacade.findNegModality(mechanismOperationOtcSession, otcOperationParamSession.getMechanismModalityList());
		mechanismOperation.getTradeOperation().setLastModifyDate(CommonsUtilities.currentDateTime());
		mechanismOperation.getTradeOperation().setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		
		mechanismOperation = otcOperationFacade.settleOtcOperationServiceFacade(mechanismOperation);
		
		BusinessProcess objBusinessProcess = new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode());
		Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(ComponentConstant.SETTLEMENT_SCHEMA, SettlementSchemaType.GROSS.getCode());
		batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
		
		// Send notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.OTC_OPERATION_SETTLEMENT.getCode());			
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
				   businessProcess, null, new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()});
		//notificacion xml via web services
		
		//issue 747: ver la documentacion del metodo: getAccountOperationValidatedToConfirmedOTC
		otcOperationParamSession.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidatedToConfirmedOTC(mechanismOperationOtcSession, swapOperationSession, ViewOperationsType.get(this.getViewOperationType())));
		
		Integer operationPart = ComponentConstant.CASH_PART;
		otcOperationFacade.markSettlementOperationAsSettled(operationPart, mechanismOperation);
		
		//sendSettlementTransferOperationWebClient(mechanismOperation,otcOperationParamSession);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_SETTLEMENT_CONFIRM_OK,
												new Object[]{mechanismOperation.getIdMechanismOperationPk().toString(),
															 mechanismOperation.getOperationNumber(),
															 negModality.getModalityName()}));
		
	}
	/**
	 * Show account operation market fact.
	 *
	 * @param accountOperation the account operation
	 * @throws ServiceException the service exception
	 */
	public void showAccountOperationMarketFact(HolderAccountOperation accountOperation) throws ServiceException{
		MechanismOperation operation = mechanismOperationOtcSession;
//		MechanismModality mechanismModality = otcOperationFacade.findMechanismModality(operation, parameters.getMechanismModalityList());
//		NegotiationModality negotiationModality = mechanismModality.getNegotiationModality();
//		if(negotiationModality.getIndPrimaryPlacement().equals(BooleanType.NO.getCode())){
//			AccountOperationMarketFact accountMarketFact = new AccountOperationMarketFact();
////			accountMarketFact.setMarketDate(CommonsUtilities.currentDate());
//			parameters.setAccountOperationMarketFact(accountOperation);
//			parameters.setAccountMarketFactDetail(accountMarketFact);
//			JSFUtilities.executeJavascriptFunction("PF('dialogMarketFactW').show()");
//		}
		
		MarketFactBalanceHelpTO markFactTO = new MarketFactBalanceHelpTO();
		markFactTO.setSecurityCodePk(operation.getSecurities().getIdSecurityCodePk());
		markFactTO.setHolderAccountPk(accountOperation.getHolderAccount().getIdHolderAccountPk());
		markFactTO.setParticipantPk(operation.getSellerParticipant().getIdParticipantPk());
		markFactTO.setUiComponentName("marketFactUI");
		markFactTO.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(), markFactTO);
	}
	
	/**
	 * Del account operation market fact.
	 *
	 * @param marketFact the market fact
	 * @throws ServiceException the service exception
	 */
	public void delAccountOperationMarketFact(AccountOperationMarketFact marketFact) throws ServiceException{
		OtcOperationParameters parameters = otcOperationParamSession;
		HolderAccountOperation accountOperation = parameters.getAccountOperationMarketFact();
		accountOperation.getAccountOperationMarketFacts().remove(marketFact);
	}
	
	/**
	 * Adds the account operation market fact.
	 */
	public void addAccountOperationMarketFact(){
		OtcOperationParameters parameters = otcOperationParamSession;
		HolderAccountOperation accountOperation = parameters.getAccountOperationMarketFact();
		AccountOperationMarketFact accountMarketFact = parameters.getAccountMarketFactDetail();
		
		try {
			otcOperationFacade.validateNewMarketFactOperation(accountMarketFact, accountOperation);
			accountMarketFact = new AccountOperationMarketFact();
			accountMarketFact.setMarketDate(CommonsUtilities.currentDate());
			parameters.setAccountMarketFactDetail(accountMarketFact);
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Approve otc operation object handler.
	 * metodo invocado desde la grilla de resultados(search.xhtml)
	 *
	 * @return the string
	 */
	public String approveOtcOperationAction(){
		if(otcOperationSession==null){
			String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED);
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		List<String> lstOperationUser = validateIsValidUser(otcOperationSession,ViewOperationsType.APPROVE.getCode());
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		OtcOperationResultTO otcOperationResultTO = otcOperationSession;
		//Verificamos que el objeto seleccionado no es Null y que se encuentre registrado
		if(otcOperationResultTO!=null && otcOperationResultTO.getOperationState().equals(OtcOperationStateType.REGISTERED.getCode())){
			this.setViewOperationType(ViewOperationsType.APPROVE.getCode());//Seteamos que la session se Ira a Modificar
			readOtcOperationObject(otcOperationResultTO);//Leemos la operacion oTC desde la bd
			loadOtcParameters();//Cargamos Algumos parametros de OTC
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){//Si es cevaldom, puede hacer todo
				return "otcOperationMgmtRule";
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(OtcOperationUtils.participantIsSeller(mechanismOperationOtcSession, participantState)){//Validamos si el participante es el que vende
					if(OtcOperationUtils.haveInChargeByParticipantRole(mechanismOperationOtcSession, ParticipantRoleType.SELL.getCode())){//Validamos si la operacion tiene Encargo
						showExceptionMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_INCHARGE_PENDIENT);
						JSFUtilities.showSimpleValidationDialog();
						return StringUtils.EMPTY;
					}else{
						return "otcOperationMgmtRule";
					}
				}else if(OtcOperationUtils.participantIsInChargeByRole(mechanismOperationOtcSession, participantState, ParticipantRoleType.SELL.getCode())){//Se tiene encargo y es el encargado
					holdAccountOperationRequest.setInchargeFundsParticipant(participantState);//Seteamos el encargo al participante seleccionado
					return "otcOperationMgmtRule";
				}
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT));
		JSFUtilities.showSimpleValidationDialog();
		return StringUtils.EMPTY;
	}
	/**
	 * Approve otc operation handler.
	 */
	public void beforeApproveOtcOperation(){
		setViewOperationType(ViewOperationsType.APPROVE.getCode());
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_APPROVE_CONFIRM,
													new Object[]{mechanismOperationOtcSession.getIdMechanismOperationPk().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfwOtcOperations').show()");
		
	}
	/**
	 * Before anulate otc operation.
	 */
	public void beforeAnulateOtcOperation(){
		setViewOperationType(ViewOperationsType.ANULATE.getCode());
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_ANNULATE_CONFIRM,
									new Object[]{mechanismOperationOtcSession.getIdMechanismOperationPk().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfwOtcOperations').show()");
	}
	/**
	 * Before reject otc operation.
	 */
	public void beforeRejectOtcOperation(){
		setViewOperationType(ViewOperationsType.REJECT.getCode());
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_REJECT_CONFIRM,
												new Object[]{mechanismOperationOtcSession.getIdMechanismOperationPk().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfwOtcOperations').show()");
	}
	/**
	 * Before approve otc operation.
	 */
	public void beforeReviewOtcOperation(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_REVIEW_CONFIRM,
												new Object[]{mechanismOperationOtcSession.getIdMechanismOperationPk().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfwOtcOperations').show()");
	}
	/**
	 * Before confirm otc operation.
	 */
	public void beforeConfirmOtcOperation(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_CONFIRM_CONFIRM,
												new Object[]{mechanismOperationOtcSession.getIdMechanismOperationPk().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfwOtcOperations').show()");
	}
	/**
	 * Before cancel otc operation.
	 */
	public void beforeCancelOtcOperation(){
		otcOperationParamSession.setMotiveCancel(null);
		otcOperationParamSession.setOtherMotiveCancel(null);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_CANCEL_CONFIRM,
												new Object[]{mechanismOperationOtcSession.getOperationNumber()}));
		JSFUtilities.executeJavascriptFunction("PF('dlgMotiveW').show()");
	}
	/**
	 * Approve otc operation.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void approveOtcOperation() throws ServiceException{
		MechanismOperation mechanismOperation = mechanismOperationOtcSession;
		mechanismOperation.getTradeOperation().setAprovalUser(userInfo.getUserAccountSession().getUserName());
		mechanismOperation = otcOperationFacade.approveOtcOpereationServiceFacade(mechanismOperation);
		
		// Send notification
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk())) {
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.OTC_OPERATION_APPROVE.getCode());			
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, mechanismOperationOtcSession.getBuyerParticipant().getIdParticipantPk(), 
					   new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()}, userInfo.getUserAccountSession());
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_APPROVE_CONFIRM_OK,
												new Object[]{mechanismOperation.getIdMechanismOperationPk().toString()}));
		
	}
	/**
	 * Review otc operation object handler.
	 *
	 * @param otcOperationResultTO the otc operation result to
	 */
	public void reviewOtcOperationObjectHandler(OtcOperationResultTO otcOperationResultTO){
		this.setViewOperationType(ViewOperationsType.MODIFY.getCode());//Seteamos que la session se Ira a Modificar
		readOtcOperationObject(otcOperationResultTO);
	}
	
	/**
	 * Evaluate modality selected.
	 */
	public void changeNegotiationModality(){
		Long idModalitySelected = mechanismOperationOtcSession.getMechanisnModality().getId().getIdNegotiationModalityPk();
		Long idMechanismSelected = NegotiationMechanismType.OTC.getCode();
		MechanismOperation operation = mechanismOperationOtcSession;
		OtcOperationParameters parameters = otcOperationParamSession;
		MechanismModality mechanismModality = null;
		otcOperationParamSession.setIsPartBuyerDisabled(Boolean.FALSE);
		
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			participantList = participantByModalities.get(idModalitySelected);
		}else{
			if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.CRUSADE.getCode())){
				otcOperationParamSession.setIsPartBuyerDisabled(Boolean.TRUE);
			}
		}
		participantList = OtcOperationUtils.getParticipantOrderByMnemonic(participantList);
		
		participantToBuyList = OtcOperationUtils.getParticipantsToBuy(operation, parameters.getAllParticipantList(),ViewOperationsType.get(getViewOperationType()));
		try{
			mechanismModality = otcOperationFacade.findMechanismModality(idMechanismSelected, idModalitySelected);
			parameters = otcOperationFacade.configModalityOnOtcOperation(parameters, operation,mechanismModality);
			Participant buyerParticipant = operation.getBuyerParticipant();
			Participant sellerParticipant = operation.getSellerParticipant();
			if(buyerParticipant.getIdParticipantPk()!=null || sellerParticipant.getIdParticipantPk()!=null){
				cleanParticipant();/*LIMPIAMOS INFORMACION DE PARTICIPANTE Y CAMPOS SEQUENCIALES*/
				/*SI LA OPERACION ES CRUZADA*/
				if(operation.getOriginRequest().equals(OtcOperationOriginRequestType.CRUSADE.getCode())){
					buyerParticipant.setIdParticipantPk(sellerParticipant.getIdParticipantPk());
				}
			}
			operation.setSettlementSchema(mechanismModality.getSettlementSchema());
			operation.setSettlementType(mechanismModality.getSettlementType());
			operation.setMechanisnModality(mechanismModality);
			
			mechanismOperationOtcSession = operation;
			mechanismOperationOtcSession.getMechanisnModality().getNegotiationModality().getInstrumentType();
		}catch(ServiceException ex){
			if(ex.getErrorService()!=null){
				operation.getMechanisnModality().getId().setIdNegotiationModalityPk(null);
				operation.setSettlementSchema(null);
				operation.setSettlementSchema(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	/**
	 * Gets the participant to sell list.
	 *
	 * @return the participant to sell list
	 */
	public List<Participant> getParticipantToBuyList(){
		return participantToBuyList;
	}
	/**
	 * Load cbo participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboParticipants() throws ServiceException{//Cargamos los Participantes
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		participantTO.setIndDepositary(BooleanType.YES.getCode());
		otcOperationParamSession.setAllParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
	}
	/**
	 * Load cbo modality.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboModalityAndParticipants() throws ServiceException{
		MechanismModalityTO mechanismModalityTO = new MechanismModalityTO();
		mechanismModalityTO.setIdNegotiationMechanismPK(NegotiationMechanismType.OTC.getCode());
		mechanismModalityTO.setNegotiationModalityState(MechanismModalityStateType.ACTIVE.getCode());
		if((userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isIssuerDpfInstitucion()
				|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) 
				&& isViewOperationRegister()){//Si es participante, setamos el codigo del participante Y se esta registrando
			mechanismModalityTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		List<MechanismModality> mechanismModalityList = otcOperationFacade.findMechanismModalityList(mechanismModalityTO,parameterDescription);//Obtenemos todas las modalidades y mecanismos
		otcOperationParamSession.setMechanismModalityList(mechanismModalityList);
		participantByModalities = otcOperationFacade.configModalitiesOnParticipantMap(mechanismModalityList, otcOperationParamSession.getAllParticipantList());
	}
	
	/**
	 * Adds the swap operation on grid.
	 * metodo para agregar las operaciones permutas a nivel de valor
	 */
	public void addSwapOperationOnGrid(){
		changeCashAmount();//Calculamos el precio sucio de acuerdo al monto sucio seteado en el sistema.
		Integer quantityIsinAdded = this.otcOperationParamSession.getOtcSwapOperationResultTO().size();
		if(quantityIsinAdded >= OtcOperationParameters.QUANTITY_SWAP_ISIN){
			return;
		}
		
		OtcSwapOperationResultTO otcLastSwapOperationResultTO = OtcSwapOperationUtils.getLastOtcSwapFromList(this.otcOperationParamSession.getOtcSwapOperationResultTO());//Obtenemos el ultimo swap ingresado
		if(otcLastSwapOperationResultTO!=null && !this.otcOperationParamSession.getIsSwapDifCurr()){//Si el ultimoi valor ingresado existio, y si es de moneda diferente
			if(otcLastSwapOperationResultTO.getRealCashAmount()!=null){//Si el monto limpio fue ingresado en el primer Swap
				if(mechanismOperationOtcSession.getRealCashAmount()==null){//Y si en este swap no se ingreso el monto limip
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_SWAP_REALCASH_AMOUNT_EMPTY));
					JSFUtilities.showSimpleValidationDialog();//Mostramos mensaje de advertencia, sobre origen de solicitud
					return;
				}
			}else{//Si el monto limpio en el primer swap no fue ingresado 
				if(mechanismOperationOtcSession.getRealCashAmount()!=null){//Y si en este swap SI se ingreso el monto limpio
					mechanismOperationOtcSession.setRealCashAmount(null);
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_SWAP_REALCASH_AMOUNT));
					JSFUtilities.showSimpleValidationDialog();//Mostramos mensaje de advertencia, sobre origen de solicitud
					return;
				}
			}
		}
		OtcSwapOperationResultTO otcSwapOperationResultTO = new OtcSwapOperationResultTO();//Creamos el objeto que me manejara la grilla del swap
		otcSwapOperationResultTO.setParticipationOperation(this.otcOperationParamSession.getParticipationSwap());//Seteamos el rol del valor COMPRA-VENTA
		otcSwapOperationResultTO.setIsinOperation(this.mechanismOperationOtcSession.getSecurities());//Seteamos el isin
		otcSwapOperationResultTO.setStockQuantity(this.mechanismOperationOtcSession.getStockQuantity());//Seteamos la cantidad de valores
		otcSwapOperationResultTO.setCashAmount(this.mechanismOperationOtcSession.getCashAmount());//Seteamos el monto sucio
		otcSwapOperationResultTO.setCashPrice(this.mechanismOperationOtcSession.getCashPrice());//Seteamos el precio sucio
		otcSwapOperationResultTO.setCurrency(this.mechanismOperationOtcSession.getSecurities().getCurrency());//Seteamos la moneda
		otcSwapOperationResultTO.setParticipantSeller(this.mechanismOperationOtcSession.getSellerParticipant());//Seteamos el participante vendedor
		otcSwapOperationResultTO.setParticipantBuyer(this.mechanismOperationOtcSession.getBuyerParticipant());//Seteamos el participante comprador
		otcSwapOperationResultTO.setRealCashAmount(this.mechanismOperationOtcSession.getRealCashAmount());
		otcSwapOperationResultTO.setRealPriceamount(this.mechanismOperationOtcSession.getRealCashPrice());
		otcOperationParamSession.getOtcSwapOperationResultTO().add(otcSwapOperationResultTO);//Agregamos swap a la lista que lo manejara
		if(otcOperationParamSession.getParticipationSwap().equals(ParticipantRoleType.SELL.getCode())){//Si fue venta, ahora es compra
			otcOperationParamSession.setParticipationSwap(ParticipantRoleType.BUY.getCode());//Seteamos el rol del valor como compra
		}else{//Caso contrario fue compra
			otcOperationParamSession.setParticipationSwap(ParticipantRoleType.SELL.getCode());//Ahora lo seteamos como venta
		}
		if(otcOperationParamSession.getOtcSwapOperationResultTO().size() == OtcOperationParameters.QUANTITY_SWAP_ISIN){//Si se llego la cantidad de isin a agregar
			otcOperationParamSession.setIsIsinQuantityEquals(Boolean.TRUE);
			if(!otcOperationParamSession.getIsSwapDifCurr()){//Si los valores son de monera distinta 
				otcOperationParamSession.setSwapExchangeRate(OtcSwapOperationUtils.getExchangeRateFromOtcSwapList(this.otcOperationParamSession.getOtcSwapOperationResultTO()));//Traemos el tipo de cambio, calculado por los montos
			}
		}else{//Do something in the future
		}
		mechanismOperationOtcSession.setSecurities(new Security());
		mechanismOperationOtcSession.setStockQuantity(null);
		mechanismOperationOtcSession.setCashAmount(null);
		mechanismOperationOtcSession.setCashPrice(null);
		mechanismOperationOtcSession.setCurrency(null);
		mechanismOperationOtcSession.setRealCashAmount(null);
		mechanismOperationOtcSession.setRealCashPrice(null);
	}
	
	public void changeWarrantyCause() {
		
	}
	
	public void changeTermInterestRate() {
		if(Validations.validateIsNotNullAndPositive(mechanismOperationOtcSession.getTermAmount())){
			calculateTermRealCashAmount();
		}
	}
	
	public void calculateTermRealCashAmount() {
		if(Validations.validateIsNull(mechanismOperationOtcSession.getWarrantyCause())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Aforo. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(mechanismOperationOtcSession.getTermInterestRate())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Interés Repo. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(mechanismOperationOtcSession.getTermSettlementDays())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Días Liquidación Plazo. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(mechanismOperationOtcSession.getRealCashPricePercentage())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Precio Dirty (%). Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(mechanismOperationOtcSession.getStockQuantity())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar la cantidad de títulos. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
				
		if (mechanismOperationOtcSession.isRateFixedModality()) {
			cleanCalculationPrices();
		}
		
		this.operationPriceCalculatorServiceFacade.calculateTermRealCashAmount(mechanismOperationOtcSession);
		
	}
	
	/**
	 * Load cbo date search type.
	 *
	 * @throws ServiceException the service exception
	 */	
	private void loadCboDateSearchType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SEARCH_DATE_TYPE.getCode());
		lstCboDateSearchType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstCboDateSearchType)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
	}
	/**
	 * Load negotiation factors.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadNegotiationFactors() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.NEGOTIATIONS_FACTORS.getCode());
		List<ParameterTable> negotiationsFactors = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameterTable: negotiationsFactors){
			if(parameterTable.getParameterTablePk().equals(NegotiationFactorsType.KAPITAL_SOC.getCode())){
				OtcOperationMgmt.KAPITAL_SOCIAL = parameterTable.getShortInteger();
				break;
			}
		}
	}
	/**
	 * Load cbo otc operation state type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboOtcOperationStateType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.OTC_OPERATION_STATE.getCode());
		lstCboOtcOperationStateType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstCboOtcOperationStateType)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
	}
	/**
	 * Load cbo otc origin request type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboOtcOriginRequestType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.OTC_REQUEST_ORIGING.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		lstCboOtcOriginRequestType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: lstCboOtcOriginRequestType){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
		}
	}
	/**
	 * Search participant on list from pk.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 */
	public Participant searchParticipantOnListFromPK(Long idParticipantPk){
		for(Participant participant: otcOperationParamSession.getAllParticipantList()){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}
	/**
	 * Find mechanism operation.
	 * metodo para buscar la operacion OTC repo, usado para repo secundario
	 */
	public void findMechanismOperation(){
		Participant participantSeller = mechanismOperationOtcSession.getSellerParticipant();
		MechanismOperation mechanismOperation = mechanismOperationOtcSession.getReferenceOperation();//Objeto que servira como TO, para enviar datos de transfer object
		MechanismOperation mechanismOperationAux = otcOperationParamSession.getMechanismOperationAuxiliar(); 
		mechanismOperation.setOperationNumber(mechanismOperationAux.getOperationNumber());
		MechanismOperation mechanismOperationFinded = null;//Objeto, encontrado de acuerdo a los parametros de busquedad
		if(otcOperationParamSession.getInstrumentTypeSelected().equals(InstrumentType.FIXED_INCOME.getCode())){//Si el instrumento de la modalida es Renta fija
			mechanismOperation.getMechanisnModality().getId().setIdNegotiationModalityPk(NegotiationModalityType.REPORT_FIXED_INCOME.getCode());//Asignamos a Repo Renta fija
		}else if(otcOperationParamSession.getInstrumentTypeSelected().equals(InstrumentType.VARIABLE_INCOME.getCode())){//Si el instrumento de la modalidad es Renta variable
			mechanismOperation.getMechanisnModality().getId().setIdNegotiationModalityPk(NegotiationModalityType.REPORT_EQUITIES.getCode());//Asignamos a Repo Renta Variable
		}
		try{//Buscamos el objeto desde la bd
			mechanismOperationFinded = otcOperationFacade.findMechanismOperationByNumberServiceFacade(mechanismOperation);
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				mechanismOperationOtcSession.getReferenceOperation().setOperationNumber(null);
				showExceptionMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), e.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		
		if(mechanismOperationFinded==null){
			mechanismOperationOtcSession.getReferenceOperation().setOperationNumber(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_REPOOPERATION_EXIST));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}else if(!mechanismOperationFinded.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode()) || !OtcOperationUtils.participantIsBuyer(mechanismOperationFinded, participantSeller)){
			//Validamos si la repo original ya fue liquidada en contado O si el participante vendedor plazo no es el mismo que vende en la repo secundario
			mechanismOperationOtcSession.getReferenceOperation().setOperationNumber(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_REPOOPERATION_SETLEMENT_CONTADO));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		Security securityTemp = mechanismOperationFinded.getSecurities();//Obtenemos el objeto security de la operacion
		if(securityTemp==null || securityTemp.getCurrency()==null || CurrencyType.get(securityTemp.getCurrency())==null){//Si el valor es Null, no tiene emision ni Emisor O si el valor no tiene moneda
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		mechanismOperationOtcSession.setReferenceOperation(mechanismOperationFinded);//Asignamos el objeto encontrado hacia. MechanismOperationAuxiliar
		otcOperationParamSession.setMechanismOperationAuxiliar(mechanismOperationFinded);
		//Seteamos algunos valores de la Repo Original, hacia la Repo secundaria
		mechanismOperationOtcSession.setSecurities(mechanismOperationOtcSession.getReferenceOperation().getSecurities());//Seteamos el valor de la repo original hacia el repo secundario
		mechanismOperationOtcSession.setNominalValue(securityTemp.getCurrentNominalValue());
		mechanismOperationOtcSession.setCurrency(securityTemp.getCurrency());
		otcOperationParamSession.setHoldAccNegotiationSellers(OtcOperationUtils.getAccountOperationFromOperation(mechanismOperationOtcSession.getReferenceOperation(), ParticipantRoleType.BUY));//seteamos los participantes vendedores plazo para la grilla de seleccion
	}
	
	/**
	 * Clean operation date on repo secundary handler.
	 */
	public void cleanOperationDateOnRepoSecundaryHandler(){
		mechanismOperationOtcSession.getReferenceOperation().setOperationNumber(null);
	}
	
	/**
	 * Evaluate hold acc operation on grid.
	 *
	 * @param holdAccOperation the hold acc operation
	 */
	public void evaluateHoldAccOperationOnGrid(HolderAccountOperation holdAccOperation){
		if(!holdAccOperation.getIsSelected()){
			holdAccOperation.setStockQuantity(null);
		}
	}
	
	/**
	 * Validate quantity hold acc operation on grid.
	 * metodo para validar las cantidad ingresadas en la grilla de repo secundario
	 * @param holdAccOperation the hold acc operation
	 */
	public void validateQuantityHoldAccOperationOnGrid(HolderAccountOperation holdAccOperation){
		BigDecimal stockQuantity = holdAccOperation.getStockQuantity();//Obtenemos cantidad de valores ingresado
		BigDecimal stockQuantityOringRepo = holdAccOperation.getRefAccountOperation().getStockQuantity();//Obtenemos la cantidad de la repo original
		BigDecimal securityNomValue = mechanismOperationOtcSession.getReferenceOperation().getNominalValue();//Obtenemos el valor nominal de la Repo Original
		if(stockQuantity==null || stockQuantity.equals(BigDecimal.ZERO)){//Si el dato ingresado es null O si es Zero
			holdAccOperation.setStockQuantity(null);
			return;
		}
		if(securityNomValue==null){//La operacion tiene data inconsistente,holdAccOperation.setStockQuantity(null);
			holdAccOperation.setIsSelected(Boolean.FALSE); 
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT, "PF('cnfwMsgCustomValidationNeg').show()");
			return;
		}
		if(stockQuantity.compareTo(stockQuantityOringRepo) > BigDecimal.ZERO.intValue()){//Si la cantidad de valores ingresada es mayor al repo original
			holdAccOperation.setStockQuantity(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_REPO_SECUNDARY_QUANTITY));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		/**Remove validation to compare nominal value with quantity
		 * 
		if(!CommonsUtilities.isMultipleOf(stockQuantity, securityNomValue)){
			holdAccOperation.setStockQuantity(null);
			executeJsFunctionAndShowMessage(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesConstants.OTC_OPERATIONS_VALIDATION_MULTIPLE_STOCK,"PF('cnfwMsgCustomValidationNeg').show()");
			return;
		}
		*/
	}

	/**
	 * Evaluate participant in charge.
	 */
	public void evaluateParticipantInCharge(){
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
		}
	}
	/**
	 * Select program interest coupon handler.
	 *
	 * @param programInterestCoupon the program interest coupon
	 */
	public void selectProgramInterestCouponHandler(ProgramInterestCoupon programInterestCoupon){
		for(ProgramInterestCoupon programCoupon: this.programInterestCouponList){
			if(!programInterestCoupon.getIdProgramInterestPk().equals(programCoupon.getIdProgramInterestPk())){
				programCoupon.setIsSelected(Boolean.FALSE);
			}
		}
		if(programInterestCoupon.getIsSelected()){
//			this.mechanismOperationOtcSession.setProgramInterestCoupon(programInterestCoupon);
		}else{
//			this.mechanismOperationOtcSession.setProgramInterestCoupon(null);
		}
		JSFUtilities.resetComponent("frmOtcOperation:pnlDialogSplit");
	}
	/**
	 * Cancel program interest coupon.
	 */
	public void cancelProgramInterestCoupon(){
		for(ProgramInterestCoupon programCoupon: this.programInterestCouponList){
			programCoupon.setIsSelected(Boolean.FALSE);
		}
//		this.mechanismOperationOtcSession.setProgramInterestCoupon(null);
		JSFUtilities.resetComponent("frmOtcOperation:pnlDialogSplit");
		JSFUtilities.executeJavascriptFunction("PF('dialolSplitCouponW').hide()");
	}
	
	/**
	 * Calculate amount clean.
	 */
	public void calculateAmountClean(){
		if(mechanismOperationOtcSession.getRealCashPrice()!=null && mechanismOperationOtcSession.getStockQuantity()!=null){
			mechanismOperationOtcSession.setRealCashAmount(mechanismOperationOtcSession.getRealCashPrice().multiply(
															mechanismOperationOtcSession.getStockQuantity()));
		}
	}
	
	public void validateOptionSelected(){
		this.cleanPrices();
	}
	
	public void cleanPrices() {
		mechanismOperationOtcSession.setCashPricePercentage(null);
		mechanismOperationOtcSession.setRealCashPricePercentage(null);
		mechanismOperationOtcSession.setCashPrice(null);
		mechanismOperationOtcSession.setRealCashPrice(null);
		mechanismOperationOtcSession.setRealCashAmount(null);
		mechanismOperationOtcSession.setCashAmount(null);
		
	}
	/**
	 * Gets the mechanism operation otc session.
	 *
	 * @return the mechanism operation otc session
	 */
	public MechanismOperation getMechanismOperationOtcSession() {
		return mechanismOperationOtcSession;
	}
	/**
	 * Sets the mechanism operation otc session.
	 *
	 * @param mechanismOperationOtcSession the new mechanism operation otc session
	 */
	public void setMechanismOperationOtcSession(MechanismOperation mechanismOperationOtcSession) {
		this.mechanismOperationOtcSession = mechanismOperationOtcSession;
	}
	/**
	 * Gets the otc operation to.
	 *
	 * @return the otc operation to
	 */
	public OtcOperationTO getOtcOperationTO() {
		return otcOperationTO;
	}
	/**
	 * Sets the otc operation to.
	 *
	 * @param otcOperationTO the new otc operation to
	 */
	public void setOtcOperationTO(OtcOperationTO otcOperationTO) {
		this.otcOperationTO = otcOperationTO;
	}
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}
	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the lst cbo date search type.
	 *
	 * @return the lst cbo date search type
	 */
	public List<ParameterTable> getLstCboDateSearchType() {
		return lstCboDateSearchType;
	}

	/**
	 * Sets the lst cbo date search type.
	 *
	 * @param lstCboDateSearchType the new lst cbo date search type
	 */
	public void setLstCboDateSearchType(List<ParameterTable> lstCboDateSearchType) {
		this.lstCboDateSearchType = lstCboDateSearchType;
	}
	/**
	 * Gets the lst cbo otc operation state type.
	 *
	 * @return the lst cbo otc operation state type
	 */
	public List<ParameterTable> getLstCboOtcOperationStateType() {
		return lstCboOtcOperationStateType;
	}
	/**
	 * Sets the lst cbo otc operation state type.
	 *
	 * @param lstCboOtcOperationStateType the new lst cbo otc operation state type
	 */
	public void setLstCboOtcOperationStateType(List<ParameterTable> lstCboOtcOperationStateType) {
		this.lstCboOtcOperationStateType = lstCboOtcOperationStateType;
	}
	/**
	 * Gets the lst cbo otc origin request type.
	 *
	 * @return the lst cbo otc origin request type
	 */
	public List<ParameterTable> getLstCboOtcOriginRequestType() {
		return lstCboOtcOriginRequestType;
	}
	/**
	 * Sets the lst cbo otc origin request type.
	 *
	 * @param lstCboOtcOriginRequestType the new lst cbo otc origin request type
	 */
	public void setLstCboOtcOriginRequestType(
			List<ParameterTable> lstCboOtcOriginRequestType) {
		this.lstCboOtcOriginRequestType = lstCboOtcOriginRequestType;
	}
	/**
	 * Gets the hold acc sell operation request.
	 *
	 * @return the hold acc sell operation request
	 */
	public HolderAccountOperation getHoldAccountOperationRequest() {
		return holdAccountOperationRequest;
	}
	/**
	 * Sets the hold acc sell operation request.
	 *
	 * @param holdAccSellOperationRequest the new hold acc sell operation request
	 */
	public void setHoldAccountOperationRequest(HolderAccountOperation holdAccSellOperationRequest) {
		this.holdAccountOperationRequest = holdAccSellOperationRequest;
	}
	/**
	 * Gets the hold account operation selected.
	 *
	 * @return the hold account operation selected
	 */
	public HolderAccountOperation getHoldAccountOperationSelected() {
		return holdAccountOperationSelected;
	}

	/**
	 * Sets the hold account operation selected.
	 *
	 * @param holdAccountOperationSelected the new hold account operation selected
	 */
	public void setHoldAccountOperationSelected(HolderAccountOperation holdAccountOperationSelected) {
		this.holdAccountOperationSelected = holdAccountOperationSelected;
	}

	/**
	 * Gets the otc operations list.
	 *
	 * @return the otc operations list
	 */
	public GenericDataModel<OtcOperationResultTO> getOtcOperationsList() {
		return otcOperationsList;
	}
	/**
	 * Sets the otc operations list.
	 *
	 * @param otcOperationsList the new otc operations list
	 */
	public void setOtcOperationsList(GenericDataModel<OtcOperationResultTO> otcOperationsList) {
		this.otcOperationsList = otcOperationsList;
	}
	/**
	 * Gets the otc operation session.
	 *
	 * @return the otc operation session
	 */
	public OtcOperationResultTO getOtcOperationSession() {
		return otcOperationSession;
	}
	/**
	 * Sets the otc operation session.
	 *
	 * @param otcOperationSession the new otc operation session
	 */
	public void setOtcOperationSession(OtcOperationResultTO otcOperationSession) {
		this.otcOperationSession = otcOperationSession;
	}
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	/**
	 * Sets the participant to buy list.
	 *
	 * @param participantToBuyList the new participant to buy list
	 */
	public void setParticipantToBuyList(List<Participant> participantToBuyList) {
		this.participantToBuyList = participantToBuyList;
	}
	/**
	 * Gets the otc operation param session.
	 *
	 * @return the otc operation param session
	 */
	public OtcOperationParameters getOtcOperationParamSession() {
		return otcOperationParamSession;
	}
	/**
	 * Sets the otc operation param session.
	 *
	 * @param otcOperationParamSession the new otc operation param session
	 */
	public void setOtcOperationParamSession(OtcOperationParameters otcOperationParamSession) {
		this.otcOperationParamSession = otcOperationParamSession;
	}
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/**
	 * Show isin detail.
	 *
	 * @param securityCode the security code
	 */
	public void showIsinDetail(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	/**
	 * Gets the program interest coupon list.
	 *
	 * @return the program interest coupon list
	 */
	public List<ProgramInterestCoupon> getProgramInterestCouponList() {
		return programInterestCouponList;
	}
	/**
	 * Sets the program interest coupon list.
	 *
	 * @param programInterestCouponList the new program interest coupon list
	 */
	public void setProgramInterestCouponList(List<ProgramInterestCoupon> programInterestCouponList) {
		this.programInterestCouponList = programInterestCouponList;
	}
	/**
	 * Clean amount entered.
	 */
	public void cleanAmountEntered(){
		this.holdAccountOperationRequest.setStockQuantity(null);
		this.holdAccountOperationRequest.setCashAmount(null);
	}
	/**
	 * Gets the currency types list.
	 *
	 * @return the currency types list
	 */
	public List<CurrencyType> getCurrencyTypesList(){
		return CurrencyType.listSomeElements(CurrencyType.PYG,CurrencyType.USD);
	}
	
	/**
	 * Clean negotiation modality.
	 */
	public void cleanNegotiationModality(){
		mechanismOperationOtcSession.getMechanisnModality().getId().setIdNegotiationModalityPk(null);
//		participantList = null;
		participantToBuyList = null;
		cleanParticipant();
	}
	
	/**
	 * Clean participant.
	 */
	public void cleanParticipant(){
		if(mechanismOperationOtcSession.getOriginRequest()!=null){
			mechanismOperationOtcSession.getBuyerParticipant().setIdParticipantPk(null);
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				mechanismOperationOtcSession.getSellerParticipant().setIdParticipantPk(null);
			}
		}
		cleanSecurity();
	}
	
	/**
	 * Clean security.
	 */
	private void cleanSecurity(){
		mechanismOperationOtcSession.setSecurities(new Security());
		mechanismOperationOtcSession.setSwapOperations(new ArrayList<SwapOperation>());
		swapOperationSession = new SwapOperation(new Security());
		otcOperationParamSession.setHolderAccountIssuer(null);
		otcOperationParamSession.setHoldAccNegotiationSellers(null);
		otcOperationParamSession.setIsIsinQuantityEquals(false);
		mechanismOperationOtcSession.setCurrency(null);
		mechanismOperationOtcSession.setNominalValue(null);
		if(depositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			MechanismOperationMarketFact marketOperation = mechanismOperationOtcSession.getMechanismOperationMarketFacts().get(0);
			marketOperation.setMarketDate(CommonsUtilities.currentDate());
			marketOperation.setMarketPrice(null);
			marketOperation.setMarketRate(null);
		}
		
		JSFUtilities.updateComponent(otcOperationParamSession.getSettlementPanel());
		JSFUtilities.resetViewRoot();
		cleanStockQuantity();
	}
	
	/**
	 * Clean stock quantity swap.
	 */
	private void cleanStockQuantitySwap(){
		swapOperationSession.setStockQuantity(null);
		swapOperationSession.setRealCashAmount(null);
		swapOperationSession.setRealCashPrice(null);
		swapOperationSession.setCashPrice(null);
		swapOperationSession.setCashAmount(null);
	}
	
	/**
	 * Clean stock quantity.
	 */
	private void cleanStockQuantity(){
		mechanismOperationOtcSession.setStockQuantity(null);
		mechanismOperationOtcSession.setRealCashAmount(null);
		mechanismOperationOtcSession.setRealCashPrice(null);
		if(!NegotiationUtils.mechanismOperationIsSwap(mechanismOperationOtcSession) || otcOperationParamSession.getOtcSwapOperationResultTO()==null || otcOperationParamSession.getOtcSwapOperationResultTO().isEmpty()){//Si no es permuta, y si no tiene algun swap operation agregado hasta el momento
			cleanCashAmount();
		}
	}
	
	/**
	 * Clean cash amount.
	 */
	private void cleanCashAmount(){
		mechanismOperationOtcSession.setRealCashAmount(null);
		mechanismOperationOtcSession.setRealCashPrice(null);
		mechanismOperationOtcSession.setCashAmount(null);
		mechanismOperationOtcSession.setCashPrice(null);
		if(mechanismOperationOtcSession.getCashSettlementDays()!=null){
			mechanismOperationOtcSession.setCashSettlementDays(null);
			mechanismOperationOtcSession.setCashSettlementDate(null);
		}
		cleanTermAmount();
	}
	
	/**
	 * Clean cash settlement.
	 */
	private void cleanCashSettlement(){
		mechanismOperationOtcSession.setCashSettlementDays(null);
		mechanismOperationOtcSession.setCashSettlementDate(null);
		cleanHolderAccountOperations();
	}
	
	/**
	 * Clean term amount.
	 */
	private void cleanTermAmount(){
		mechanismOperationOtcSession.setTermAmount(null);
		mechanismOperationOtcSession.setTermPrice(null);
		mechanismOperationOtcSession.setTermSettlementDays(null);
		mechanismOperationOtcSession.setTermSettlementDate(null);
		mechanismOperationOtcSession.setAmountRate(null);
		cleanHolderAccountOperations();
	}
	
	/**
	 * Clean holder account operations.
	 */
	private void cleanHolderAccountOperations(){
		JSFUtilities.updateComponent(otcOperationParamSession.getAccountOperationPanel());
		loadNewHolderAccountOperation();
		mechanismOperationOtcSession.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		swapOperationSession.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		otcOperationParamSession.setHoldAccNegotiationValidated(otcOperationUtils.getAccountOperationValidated(
				mechanismOperationOtcSession, swapOperationSession, userInfo, ViewOperationsType.get(getViewOperationType())));
		otcOperationParamSession.setIsInchargedDisabled(OtcOperationUtils.getInchargedDisabled(
				mechanismOperationOtcSession, swapOperationSession, userInfo, ViewOperationsType.get(getViewOperationType())));
//		holdAccountOperationRequest.setRole(OtcOperationUtils.getParticipantRoleByOrigin(
//				mechanismOperationOtcSession,swapOperationSession, userInfo, ViewOperationsType.get(this.getViewOperationType())));	
	}
	
	/**
	 * Validate is valid user.
	 *
	 * @param otcOperationSession the otc operation session
	 * @param viewOperationType the view operation type
	 * @return the list
	 */
	private List<String> validateIsValidUser(OtcOperationResultTO otcOperationSession,Integer viewOperationType) {
		  List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		  List<String> lstOperationUserResult; 
		  OperationUserTO operationUserTO = new OperationUserTO();  
		  operationUserTO.setOperNumber(otcOperationSession.getIdMechanismOperationRequestPk().toString());
		  operationUserTO.setUserName(viewOperationType.equals(ViewOperationsType.CONFIRM.getCode())? 
				  											   otcOperationSession.getReviewUser():
				  											   otcOperationSession.getRegisterUser());
		  lstOperationUserParam.add(operationUserTO);
		  lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		  return lstOperationUserResult; 
	}
	
	private List<String> validateIsValidSettlementUser(OtcOperationResultTO otcOperationSession,Integer viewOperationType) {
		  List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		  List<String> lstOperationUserResult; 
		  OperationUserTO operationUserTO = new OperationUserTO();  
		  operationUserTO.setOperNumber(otcOperationSession.getIdMechanismOperationRequestPk().toString());
		  operationUserTO.setUserName(viewOperationType.equals(ViewOperationsType.SETTLEMENT.getCode())? 
				  											   otcOperationSession.getReviewUser():
				  											   otcOperationSession.getRegisterUser());
		  lstOperationUserParam.add(operationUserTO);
		  lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		  return lstOperationUserResult; 
	}
	
	public Boolean isPriceCalculation() {
		return Validations.validateIsNotNull(optionSelectedOneRadio) && optionSelectedOneRadio.equals(GeneralConstants.ONE_VALUE_INTEGER);
	}
	
	public Boolean isAmountCalculation() {
		return Validations.validateIsNotNull(optionSelectedOneRadio) && optionSelectedOneRadio.equals(2);
	}
	/**
	 * Gets the swap operation session.
	 *
	 * @return the swap operation session
	 */
	public SwapOperation getSwapOperationSession() {
		return swapOperationSession;
	}

	/**
	 * Sets the swap operation session.
	 *
	 * @param swapOperationSession the new swap operation session
	 */
	public void setSwapOperationSession(SwapOperation swapOperationSession) {
		this.swapOperationSession = swapOperationSession;
	}

	/**
	 * Gets the motive cancel operation list.
	 *
	 * @return the motive cancel operation list
	 */
	public List<ParameterTable> getMotiveCancelOperationList() {
		return motiveCancelOperationList;
	}

	/**
	 * Sets the motive cancel operation list.
	 *
	 * @param motiveCancelOperationList the new motive cancel operation list
	 */
	public void setMotiveCancelOperationList(
			List<ParameterTable> motiveCancelOperationList) {
		this.motiveCancelOperationList = motiveCancelOperationList;
	}

	/**
	 * Gets the lst settlement type.
	 *
	 * @return the lst settlement type
	 */
	public List<ParameterTable> getLstSettlementType() {
		return lstSettlementType;
	}

	/**
	 * Sets the lst settlement type.
	 *
	 * @param lstSettlementType the new lst settlement type
	 */
	public void setLstSettlementType(List<ParameterTable> lstSettlementType) {
		this.lstSettlementType = lstSettlementType;
	}

	/**
	 * Gets the lst scheme type.
	 *
	 * @return the lst scheme type
	 */
	public List<ParameterTable> getLstSchemeType() {
		return lstSchemeType;
	}

	/**
	 * Sets the lst scheme type.
	 *
	 * @param lstSchemeType the new lst scheme type
	 */
	public void setLstSchemeType(List<ParameterTable> lstSchemeType) {
		this.lstSchemeType = lstSchemeType;
	}

	/**
	 * Gets the depositary setup.
	 *
	 * @return the depositary setup
	 */
	public IdepositarySetup getDepositarySetup() {
		return depositarySetup;
	}

	/**
	 * Sets the depositary setup.
	 *
	 * @param depositarySetup the new depositary setup
	 */
	public void setDepositarySetup(IdepositarySetup depositarySetup) {
		this.depositarySetup = depositarySetup;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the market fact component ui.
	 *
	 * @return the market fact component ui
	 */
	public MarketFactBalanceHelpTO getMarketFactComponentUI() {
		return marketFactComponentUI;
	}

	/**
	 * Sets the market fact component ui.
	 *
	 * @param marketFactComponentUI the new market fact component ui
	 */
	public void setMarketFactComponentUI(
			MarketFactBalanceHelpTO marketFactComponentUI) {
		this.marketFactComponentUI = marketFactComponentUI;
	}

	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}

	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}

	public Boolean getDisableStockQuantity() {
		return disableStockQuantity;
	}

	public void setDisableStockQuantity(Boolean disableStockQuantity) {
		this.disableStockQuantity = disableStockQuantity;
	}
	
	
}