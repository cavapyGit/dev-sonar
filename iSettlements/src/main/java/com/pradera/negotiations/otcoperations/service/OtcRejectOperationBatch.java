package com.pradera.negotiations.otcoperations.service;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.negotiations.sirtex.facade.SirtexOperationFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OtcRejectOperationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="OtcRejectOperationBatch")
@RequestScoped
public class OtcRejectOperationBatch implements JobExecution {

	/** The otc operation service. */
	@EJB
	OtcOperationServiceBean otcOperationService;
	
	/** The mcn operation service facade. */
	@EJB 
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The sirtex operation facade. */
	@EJB 
	SirtexOperationFacade sirtexOperationFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {

		try {
			//RECHAZO DE OTC
			otcOperationService.getOtcOperationToReject(CommonsUtilities.currentDate());
			
			//RECHAZO DE SIRTEX
			//issue 600
			List<TradeRequest> lstSirtexOper = otcOperationService.getSirtexOperationToReject(CommonsUtilities.currentDate());
			
			if(Validations.validateIsNotNullAndNotEmpty(lstSirtexOper)){
				for(TradeRequest sirtexOperation : lstSirtexOper ){
					if(sirtexOperation.getOperationState().equals(SirtexOperationStateType.REGISTERED.getCode())){
						sirtexOperationFacade.anulateSirtexOperation(sirtexOperation);
					}else if (sirtexOperation.getOperationState().equals(SirtexOperationStateType.APROVED.getCode())
							|| sirtexOperation.getOperationState().equals(SirtexOperationStateType.REVIEWED.getCode())){
						sirtexOperationFacade.rejectSirtexOperation(sirtexOperation);
					}
				}
			}
			
		} catch (ServiceException e) {
			e.printStackTrace();
		} 
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}