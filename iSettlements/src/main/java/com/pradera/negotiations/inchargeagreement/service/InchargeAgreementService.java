package com.pradera.negotiations.inchargeagreement.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.InchargeAgreement;
import com.pradera.model.negotiation.InchargeRule;
import com.pradera.model.negotiation.LoanableSecuritiesMarketfact;
import com.pradera.model.negotiation.LoanableSecuritiesOperation;
import com.pradera.model.negotiation.LoanableSecuritiesRequest;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.InchargeAgreementStateType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationRequestType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.assignmentrequest.to.AccountAssignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAssignmentRequestTO;
import com.pradera.negotiations.inchargeagreement.to.RegisterInchargeAgreementTO;
import com.pradera.negotiations.inchargeagreement.to.SearchInchargeAgreementTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InchargeAgreementService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class InchargeAgreementService extends CrudDaoServiceBean{

	/** The account assignment facade. */
	@Inject
	AccountAssignmentFacade accountAssignmentFacade;	
	
	/**
	 * Gets the incharge rule.
	 *
	 * @param role the role
	 * @return the incharge rule
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InchargeRule> getInchargeRule(Integer role) throws ServiceException{
		try{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ir");
		sbQuery.append("	FROM InchargeRule ir");
		sbQuery.append("	WHERE ir.inchargeRole = :role");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("role", role);
		return (List<InchargeRule>)query.getResultList();
		} catch(NonUniqueResultException ex){
			   return null;
		}
	}
	
	/**
	 * Gets the incharge agreement.
	 *
	 * @param idInchargeAgreement the id incharge agreement
	 * @return the incharge agreement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public InchargeAgreement getInchargeAgreement(Long idInchargeAgreement)throws ServiceException{
		try{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ia ");
		sbQuery.append("	FROM InchargeAgreement ia");
		sbQuery.append("	WHERE ia.idInchargeAgreementPk = :idInchargeAgreementPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idInchargeAgreementPk", idInchargeAgreement);
		return (InchargeAgreement)query.getSingleResult();
		} catch(NonUniqueResultException ex){
			   return null;
		}
	}
	
	/**
	 * Registry Loanable Securities Request.
	 *
	 * @param inchargeAgreement the incharge agreement
	 * @param logger object Logger User
	 * @return object loanable Securities Operation
	 * @throws ServiceException The service exception
	 */
	public InchargeAgreement registryInchargeAgreement(InchargeAgreement inchargeAgreement, LoggerUser logger) throws ServiceException{	
		try{
		Map<String,Object> param = new HashMap<String,Object>();		
		param.put("idHolderPkParam", inchargeAgreement.getHolder().getIdHolderPk());
		Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
		if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
		}		
		
		Map<String,Object> parama = new HashMap<String,Object>();		
		parama.put("idParticipantPkParam", inchargeAgreement.getTraderParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
		if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}	
		
		Map<String,Object> paramaAux = new HashMap<String,Object>();		
		paramaAux.put("idParticipantPkParam", inchargeAgreement.getInchargeParticipant().getIdParticipantPk());
		Participant participantAux = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramaAux);
		if(!participantAux.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}	
			
		List<HolderAccount> lstResultAux = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
				inchargeAgreement.getTraderParticipant().getIdParticipantPk());
		if(Validations.validateListIsNullOrEmpty(lstResultAux)){
			throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE);
		}
		
		List<HolderAccount> lstResult = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
				inchargeAgreement.getInchargeParticipant().getIdParticipantPk());
		if(Validations.validateListIsNullOrEmpty(lstResult)){
			throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE);
		}
		boolean rolBuy = false;
		boolean rolSell = false;
		if(inchargeAgreement.getIndPurchaseRole().equals(GeneralConstants.ONE_VALUE_INTEGER) 
				&& inchargeAgreement.getIndSaleRole().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			rolBuy=true;
			rolSell=true;
		}
		else if(inchargeAgreement.getIndPurchaseRole().equals(GeneralConstants.ONE_VALUE_INTEGER) 
				&& inchargeAgreement.getIndSaleRole().equals(GeneralConstants.ZERO_VALUE_INTEGER))
			rolBuy=true;
		else if(inchargeAgreement.getIndPurchaseRole().equals(GeneralConstants.ZERO_VALUE_INTEGER) 
				&& inchargeAgreement.getIndSaleRole().equals(GeneralConstants.ONE_VALUE_INTEGER))
			rolSell=true;
		List<InchargeAgreement>  lstInchargeAgreement = (List<InchargeAgreement>)getInchargeAgreementForFilter(
				inchargeAgreement.getNegotiationMechanism().getIdNegotiationMechanismPk(), 
				inchargeAgreement.getMegotiationModality().getIdNegotiationModalityPk(), rolBuy,rolSell,
				inchargeAgreement.getTraderParticipant().getIdParticipantPk(), 
				inchargeAgreement.getInchargeParticipant().getIdParticipantPk(),
				inchargeAgreement.getHolder().getIdHolderPk());
	  if(Validations.validateIsNotNull(lstInchargeAgreement) && lstInchargeAgreement.size() == GeneralConstants.ONE_VALUE_INTEGER){
		  throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
	  }
		inchargeAgreement.setRegisterUser(logger.getUserName());
		inchargeAgreement.setRegisterDate(CommonsUtilities.currentDateTime());
		
		create(inchargeAgreement);

		return inchargeAgreement;
		
		} catch(NonUniqueResultException ex){
			   return null;
		}
	}
	
	/**
	 * Search Incharge Agreement.
	 *
	 * @param searchInchargeAgreementTO the search incharge agreement to
	 * @return  object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({"unchecked" })
	public List<InchargeAgreement> searchInchargeAgreement(SearchInchargeAgreementTO searchInchargeAgreementTO)throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("	Select ia.idInchargeAgreementPk, ia.registerDate,	");//0,1
			stringBuilder.append("	par1.description, par1.idParticipantPk,	"); //2,3
			stringBuilder.append("	par2.description, par2.idParticipantPk,	"); //4,5
			stringBuilder.append("	ia.agreementState, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = ia.agreementState),  "); //6,7
			stringBuilder.append("	ho.idHolderPk, ho.fullName,	");	//8, 9
			stringBuilder.append("	ho.firstLastName,ho.secondLastName,	ho.name,ho.holderType,   ");//10,11,12,13
			stringBuilder.append("	ia.indFundsIncharge, ia.indStockIncharge,ia.indPurchaseRole,ia.indSaleRole,	");	//14,15,16,17
			stringBuilder.append("	par1.mnemonic, par2.mnemonic	"); //18,19
			stringBuilder.append("	from InchargeAgreement ia	");
			stringBuilder.append("	join ia.traderParticipant par1	");
			stringBuilder.append("	join ia.inchargeParticipant par2	");			
			stringBuilder.append("	join ia.holder ho	");
			stringBuilder.append("	Where 1 = 1	");	
			if(Validations.validateIsNotNull(searchInchargeAgreementTO.getIdInchargeAgreement()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchInchargeAgreementTO.getIdInchargeAgreement())))){
				stringBuilder.append("	AND	ia.idInchargeAgreementPk = :idInchargeAgreementPk	");
				parameters.put("idInchargeAgreementPk", searchInchargeAgreementTO.getIdInchargeAgreement());							
			}
			if(Validations.validateIsNotNull(searchInchargeAgreementTO.getNegoMechanismSelected()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchInchargeAgreementTO.getNegoMechanismSelected())))){
				stringBuilder.append("	AND	ia.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk	");
				parameters.put("idNegotiationMechanismPk", searchInchargeAgreementTO.getNegoMechanismSelected());							
			}
			if(Validations.validateIsNotNull(searchInchargeAgreementTO.getNegoModalitySelected()) && 
					Validations.validateIsPositiveNumber(searchInchargeAgreementTO.getNegoModalitySelected().intValue())){
				stringBuilder.append("	AND	ia.megotiationModality.idNegotiationModalityPk = :idNegotiationModalityPk	");
				parameters.put("idNegotiationModalityPk", searchInchargeAgreementTO.getNegoModalitySelected());							
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchInchargeAgreementTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(searchInchargeAgreementTO.getEndDate())){
				stringBuilder.append(" and  TRUNC(ia.registerDate) between :dateIni and :dateEnd ");
				parameters.put("dateIni", searchInchargeAgreementTO.getInitialDate());
				parameters.put("dateEnd", searchInchargeAgreementTO.getEndDate());			
			}
			if(Validations.validateIsNotNull(searchInchargeAgreementTO.getStateSelected()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchInchargeAgreementTO.getStateSelected())))){
				stringBuilder.append("	AND	ia.agreementState = :agreementState	");
				parameters.put("agreementState", Long.valueOf(searchInchargeAgreementTO.getStateSelected()));							
			}
			if(Validations.validateIsNotNull(searchInchargeAgreementTO.getParticipantTraderSelected()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchInchargeAgreementTO.getParticipantTraderSelected())))){
				stringBuilder.append("	AND	par1.idParticipantPk = :idParticipant1Pk	");
				parameters.put("idParticipant1Pk", searchInchargeAgreementTO.getParticipantTraderSelected());							
			}
			if(Validations.validateIsNotNull(searchInchargeAgreementTO.getParticipantChargedSelected()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchInchargeAgreementTO.getParticipantChargedSelected())))){
				stringBuilder.append("	AND	par2.idParticipantPk = :idParticipant2Pk	");
				parameters.put("idParticipant2Pk", searchInchargeAgreementTO.getParticipantChargedSelected());							
			}	
			if(searchInchargeAgreementTO.isRoleSelectedBuy()){
				stringBuilder.append("	AND	ia.indPurchaseRole = :indPurchaseRole 	");
				parameters.put("indPurchaseRole", GeneralConstants.ONE_VALUE_INTEGER);										
			}
			if(searchInchargeAgreementTO.isRoleSelectedSell()){
				stringBuilder.append("	AND	ia.indSaleRole = :indSaleRole	");
				parameters.put("indSaleRole", GeneralConstants.ONE_VALUE_INTEGER);
			}
			if(searchInchargeAgreementTO.isInchargeTypeFunds()){
				stringBuilder.append("	AND	ia.indFundsIncharge = :indFundsIncharge	");
				parameters.put("indFundsIncharge", GeneralConstants.ONE_VALUE_INTEGER);
			}
			if(searchInchargeAgreementTO.isInchargeTypeStock()){
				stringBuilder.append("	AND	ia.indStockIncharge = :indStockIncharge	");
				parameters.put("indStockIncharge", GeneralConstants.ONE_VALUE_INTEGER);
			}
			List<Object[]> lstInchargeAgreement =  findListByQueryString(stringBuilder.toString(), parameters);
			Map<Long,InchargeAgreement> inchargeAgreementPk = new HashMap<Long, InchargeAgreement>();
			for(Object[] object: lstInchargeAgreement){
				InchargeAgreement inchargeAgreement = new InchargeAgreement();
				inchargeAgreement.setIdInchargeAgreementPk(new Long(object[0].toString()));
				inchargeAgreement.setRegisterDate((Date) object[1]);
				Participant participant = new Participant();
				participant.setDescription(object[2].toString());
				participant.setIdParticipantPk(new Long(object[3].toString()));
				if(object[18]!=null)
					participant.setMnemonic(object[18].toString());
				inchargeAgreement.setTraderParticipant(participant);
				Participant participantAux = new Participant();
				participantAux.setDescription(object[4].toString());
				participantAux.setIdParticipantPk(new Long(object[5].toString()));
				if(object[19]!=null)
					participantAux.setMnemonic(object[19].toString());
				inchargeAgreement.setInchargeParticipant(participantAux);
				inchargeAgreement.setAgreementState(new Long(object[6].toString()));
				inchargeAgreement.setStateName(object[7].toString());
				Holder holder = new Holder();
				holder.setIdHolderPk(new Long(object[8].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(object[9]))
					holder.setFullName(object[9].toString());
				if(Validations.validateIsNotNullAndNotEmpty(object[10]))
					holder.setFirstLastName(object[10].toString());
				if(Validations.validateIsNotNullAndNotEmpty(object[11]))
				holder.setSecondLastName(object[11].toString());
				if(Validations.validateIsNotNullAndNotEmpty(object[12]))
					holder.setName(object[12].toString());
				holder.setHolderType(new Integer(object[13].toString()));
				inchargeAgreement.setHolder(holder);
				inchargeAgreement.setIndFundsIncharge(new Integer(object[14].toString()));
				inchargeAgreement.setIndStockIncharge(new Integer(object[15].toString()));
				inchargeAgreement.setIndPurchaseRole(new Integer(object[16].toString()));
				inchargeAgreement.setIndSaleRole(new Integer(object[17].toString()));
				String YES = BooleanType.YES.getValue().toString();
				String NO = BooleanType.NO.getValue().toString();
				if(inchargeAgreement.getIndPurchaseRole().equals(GeneralConstants.ONE_VALUE_INTEGER) 
						&& inchargeAgreement.getIndSaleRole().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
					inchargeAgreement.setRoleName1(YES);
					inchargeAgreement.setRoleName2(NO);
				}					
				if(inchargeAgreement.getIndPurchaseRole().equals(GeneralConstants.ZERO_VALUE_INTEGER) 
						&& inchargeAgreement.getIndSaleRole().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					inchargeAgreement.setRoleName1(NO);
					inchargeAgreement.setRoleName2(YES);
				}
				if(inchargeAgreement.getIndPurchaseRole().equals(GeneralConstants.ONE_VALUE_INTEGER) 
						&& inchargeAgreement.getIndSaleRole().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					inchargeAgreement.setRoleName1(YES);
					inchargeAgreement.setRoleName2(YES);
				}
				if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ZERO_VALUE_INTEGER) 
						&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					inchargeAgreement.setInchargeTypeName1(NO);
					inchargeAgreement.setInchargeTypeName2(YES);
				}
				if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER) 
						&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					inchargeAgreement.setInchargeTypeName1(YES);
					inchargeAgreement.setInchargeTypeName2(NO);
				}
				inchargeAgreementPk.put(inchargeAgreement.getIdInchargeAgreementPk(), inchargeAgreement);
			}
			return new ArrayList<InchargeAgreement>(inchargeAgreementPk.values());
			}catch(NoResultException ex){
				   return null;
			} catch(NonUniqueResultException ex){
				   return null;
			}	
	}
	
	/**
	 * Gets the incharge agreement.
	 *
	 * @param inchargeAgreement the incharge agreement
	 * @return the incharge agreement
	 * @throws ServiceException the service exception
	 */
	public RegisterInchargeAgreementTO getInchargeAgreement(InchargeAgreement inchargeAgreement)throws ServiceException{
		try{
		RegisterInchargeAgreementTO registerInchargeAgreementTO = new RegisterInchargeAgreementTO();
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select ia.idInchargeAgreementPk, ia.registerDate,	");//0,1
		stringBuilder.append("	par1.description, par1.idParticipantPk,	"); //2,3
		stringBuilder.append("	par2.description, par2.idParticipantPk,	"); //4,5
		stringBuilder.append("	ia.agreementState, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = ia.agreementState),  "); //6,7
		stringBuilder.append("	ho.idHolderPk, ho.fullName,	");	//8, 9
		stringBuilder.append("	ho.firstLastName,ho.secondLastName,	ho.name,ho.holderType,   ");//10,11,12,13
		stringBuilder.append("	ia.indFundsIncharge, ia.indStockIncharge,ia.indPurchaseRole,ia.indSaleRole,	");	//14,15,16,17
		stringBuilder.append("	ia.negotiationMechanism.idNegotiationMechanismPk, ia.megotiationModality.idNegotiationModalityPk	");	//18, 19
		stringBuilder.append("	from InchargeAgreement ia	");
		stringBuilder.append("	join ia.traderParticipant par1	");
		stringBuilder.append("	join ia.inchargeParticipant par2	");			
		stringBuilder.append("	join ia.holder ho	");
		stringBuilder.append("	Where 1 = 1	");	
		if(Validations.validateIsNotNull(inchargeAgreement.getIdInchargeAgreementPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(inchargeAgreement.getIdInchargeAgreementPk())))){
			stringBuilder.append("	AND	ia.idInchargeAgreementPk = :idInchargeAgreementPk	");
			parameters.put("idInchargeAgreementPk", inchargeAgreement.getIdInchargeAgreementPk());							
		}
		List<Object[]> lstInchargeAgreement =  findListByQueryString(stringBuilder.toString(), parameters);
		for(Object[] object: lstInchargeAgreement){
			registerInchargeAgreementTO.setParticipantTraderSelected(new Long(object[3].toString()));
			registerInchargeAgreementTO.setParticipantChargedSelected(new Long(object[5].toString()));
			registerInchargeAgreementTO.setStateSelected(new Long(object[6].toString()));
			Holder holder = new Holder();
			holder.setIdHolderPk(new Long(object[8].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(object[9]))
				holder.setFullName(object[9].toString());
			if(Validations.validateIsNotNullAndNotEmpty(object[10]))
				holder.setFirstLastName(object[10].toString());
			if(Validations.validateIsNotNullAndNotEmpty(object[11]))
			holder.setSecondLastName(object[11].toString());
			if(Validations.validateIsNotNullAndNotEmpty(object[12]))
				holder.setName(object[12].toString());
			holder.setHolderType(new Integer(object[13].toString()));
			registerInchargeAgreementTO.setHolder(holder);
			if(new Integer(object[14].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER) &&
					new Integer(object[15].toString()).equals(GeneralConstants.ZERO_VALUE_INTEGER)){
				registerInchargeAgreementTO.setInchargeTypeFunds(true);
				registerInchargeAgreementTO.setInchargeTypeStock(false);
			}else if(new Integer(object[14].toString()).equals(GeneralConstants.ZERO_VALUE_INTEGER) &&
					new Integer(object[15].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
				registerInchargeAgreementTO.setInchargeTypeFunds(false);
				registerInchargeAgreementTO.setInchargeTypeStock(true);
			}else	if(new Integer(object[14].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER) &&
					new Integer(object[15].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
				registerInchargeAgreementTO.setInchargeTypeFunds(true);
				registerInchargeAgreementTO.setInchargeTypeStock(true);
			}
			if(new Integer(object[16].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER) &&
					new Integer(object[17].toString()).equals(GeneralConstants.ZERO_VALUE_INTEGER)){
				registerInchargeAgreementTO.setRoleSelectedBuy(true);
				registerInchargeAgreementTO.setRoleSelectedSell(false);
			}else	if(new Integer(object[16].toString()).equals(GeneralConstants.ZERO_VALUE_INTEGER) &&
					new Integer(object[17].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
				registerInchargeAgreementTO.setRoleSelectedBuy(false);
				registerInchargeAgreementTO.setRoleSelectedSell(true);
			}else	if(new Integer(object[16].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER) &&
					new Integer(object[17].toString()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
				registerInchargeAgreementTO.setRoleSelectedBuy(true);
				registerInchargeAgreementTO.setRoleSelectedSell(true);
			}
			registerInchargeAgreementTO.setNegoMechanismSelected(new Long(object[18].toString()));
			registerInchargeAgreementTO.setNegoModalitySelected(new Long(object[19].toString()));
			
		}
		return registerInchargeAgreementTO;
		} catch(NonUniqueResultException ex){
			   return null;
		}
	}
	
	/**
	 * Gets the incharge agreement for filter.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idRolebuy the id rolebuy
	 * @param idRoleSell the id role sell
	 * @param idParticipant the id participant
	 * @param idParticipantAux the id participant aux
	 * @param idHolder the id holder
	 * @return the incharge agreement for filter
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InchargeAgreement> getInchargeAgreementForFilter(Long idMechanism,Long idModality,
			boolean idRolebuy,boolean idRoleSell,Long idParticipant,Long idParticipantAux,Long idHolder)throws ServiceException{
		try{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ia ");
		sbQuery.append("	FROM InchargeAgreement ia");
		sbQuery.append(" 	inner join fetch ia.traderParticipant tp ");
		sbQuery.append(" 	inner join fetch ia.inchargeParticipant ip ");
		sbQuery.append(" 	inner join fetch ia.negotiationMechanism nm ");
		sbQuery.append(" 	inner join fetch ia.megotiationModality mm ");
		sbQuery.append(" 	inner join fetch ia.holder ho ");
		sbQuery.append("	Where 1 = 1	");
		sbQuery.append("	and tp.idParticipantPk = :idParticipantPkAux");
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantAux))
			sbQuery.append("	and ip.idParticipantPk = :idParticipantPkAux2");
		sbQuery.append("	and nm.idNegotiationMechanismPk = :idNegotiationMechanismPkAux");
		sbQuery.append("	and mm.idNegotiationModalityPk = :idNegotiationModalityPkAux");
		sbQuery.append("	and ho.idHolderPk = :idHolderPkAux");
		if(idRolebuy==true && idRoleSell==true){
			sbQuery.append("	and (ia.indPurchaseRole = :indPurchaseRoleAux");
			sbQuery.append("	or ia.indSaleRole = :indSaleRoleAux)");
		}else if(idRolebuy)
			sbQuery.append("	and ia.indPurchaseRole = :indPurchaseRoleAux");
		else if(idRoleSell)
			sbQuery.append("	and ia.indSaleRole = :indSaleRoleAux");
		
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantAux)){
			sbQuery.append("	and ia.agreementState in (:agreementStateAux,:agreementStateAux2)");
		}else
			sbQuery.append("	and ia.agreementState = :agreementStateAux");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPkAux", idParticipant);
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantAux))
			query.setParameter("idParticipantPkAux2", idParticipantAux);
		query.setParameter("idNegotiationMechanismPkAux", idMechanism);
		query.setParameter("idNegotiationModalityPkAux", idModality);
		query.setParameter("idHolderPkAux", idHolder);
		if(idRolebuy==true && idRoleSell==true){
			query.setParameter("indPurchaseRoleAux", GeneralConstants.ONE_VALUE_INTEGER);
			query.setParameter("indSaleRoleAux", GeneralConstants.ONE_VALUE_INTEGER);
		}else if(idRolebuy)
			query.setParameter("indPurchaseRoleAux", GeneralConstants.ONE_VALUE_INTEGER);		
		else if(idRoleSell)
			query.setParameter("indSaleRoleAux", GeneralConstants.ONE_VALUE_INTEGER);
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantAux)){
			query.setParameter("agreementStateAux", Long.valueOf(InchargeAgreementStateType.CONFIRMED.getCode()));
			query.setParameter("agreementStateAux2", Long.valueOf(InchargeAgreementStateType.REGISTERED.getCode()));
		}else
			query.setParameter("agreementStateAux", Long.valueOf(InchargeAgreementStateType.CONFIRMED.getCode()));
		return (List<InchargeAgreement>)query.getResultList();
		} catch(NonUniqueResultException ex){
			   return null;
		}
	}
	
	/**
	 * Confirm Incharge Agreement.
	 *
	 * @param inchargeAgreement object Incharge Agreement
	 * @param loggerUser the logger user
	 * @return object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	public InchargeAgreement confirmInchargeAgreement(InchargeAgreement inchargeAgreement,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", inchargeAgreement.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
			Map<String,Object> paramaAux = new HashMap<String,Object>();		
			paramaAux.put("idParticipantPkParam", inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			Participant participantAux = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramaAux);
			if(!participantAux.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
			List<HolderAccount> lstResultAux = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
					inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			if(Validations.validateListIsNullOrEmpty(lstResultAux)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE);
			}
			
			List<HolderAccount> lstResult = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
					inchargeAgreement.getInchargeParticipant().getIdParticipantPk());
			if(Validations.validateListIsNullOrEmpty(lstResult)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE);
			}
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idInchargeAgreementPkParam", inchargeAgreement.getIdInchargeAgreementPk());
			InchargeAgreement inchargeAgreementAux = findObjectByNamedQuery(InchargeAgreement.INCHARGE_AGREEMENT_STATE, mapParams);
			if(!InchargeAgreementStateType.REGISTERED.getCode().equals(inchargeAgreementAux.getAgreementState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			inchargeAgreement = getInchargeAgreement(inchargeAgreement.getIdInchargeAgreementPk());
			inchargeAgreement.setAgreementState(InchargeAgreementStateType.CONFIRMED.getCode());
			inchargeAgreement.setConfirmUser(loggerUser.getUserName());
			inchargeAgreement.setConfirmDate(CommonsUtilities.currentDateTime());
			
			update(inchargeAgreement);
			return inchargeAgreement;
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Reject Incharge Agreement.
	 *
	 * @param inchargeAgreement object Incharge Agreement
	 * @param loggerUser the logger user
	 * @return object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	public InchargeAgreement rejectInchargeAgreement(InchargeAgreement inchargeAgreement,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", inchargeAgreement.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
			Map<String,Object> paramaAux = new HashMap<String,Object>();		
			paramaAux.put("idParticipantPkParam", inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			Participant participantAux = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramaAux);
			if(!participantAux.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
			List<HolderAccount> lstResultAux = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
					inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			if(Validations.validateListIsNullOrEmpty(lstResultAux)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE);
			}
			
			List<HolderAccount> lstResult = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
					inchargeAgreement.getInchargeParticipant().getIdParticipantPk());
			if(Validations.validateListIsNullOrEmpty(lstResult)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE);
			}
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idInchargeAgreementPkParam", inchargeAgreement.getIdInchargeAgreementPk());
			InchargeAgreement inchargeAgreementAux = findObjectByNamedQuery(InchargeAgreement.INCHARGE_AGREEMENT_STATE, mapParams);
			if(!InchargeAgreementStateType.REGISTERED.getCode().equals(inchargeAgreementAux.getAgreementState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			inchargeAgreement = getInchargeAgreement(inchargeAgreement.getIdInchargeAgreementPk());
			inchargeAgreement.setAgreementState(InchargeAgreementStateType.REJECTED.getCode());
			inchargeAgreement.setRejectUser(loggerUser.getUserName());
			inchargeAgreement.setRejectDate(CommonsUtilities.currentDateTime());
			
			update(inchargeAgreement);
			return inchargeAgreement;
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Cancel Incharge Agreement.
	 *
	 * @param inchargeAgreement object Incharge Agreement
	 * @param loggerUser the logger user
	 * @return object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	public InchargeAgreement cancelInchargeAgreement(InchargeAgreement inchargeAgreement,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", inchargeAgreement.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
			Map<String,Object> paramaAux = new HashMap<String,Object>();		
			paramaAux.put("idParticipantPkParam", inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			Participant participantAux = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramaAux);
			if(!participantAux.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
			List<HolderAccount> lstResultAux = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
					inchargeAgreement.getTraderParticipant().getIdParticipantPk());
			if(Validations.validateListIsNullOrEmpty(lstResultAux)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE);
			}
			
			List<HolderAccount> lstResult = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
					inchargeAgreement.getInchargeParticipant().getIdParticipantPk());
			if(Validations.validateListIsNullOrEmpty(lstResult)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE);
			}
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idInchargeAgreementPkParam", inchargeAgreement.getIdInchargeAgreementPk());
			InchargeAgreement inchargeAgreementAux = findObjectByNamedQuery(InchargeAgreement.INCHARGE_AGREEMENT_STATE, mapParams);
			if(!InchargeAgreementStateType.CONFIRMED.getCode().equals(inchargeAgreementAux.getAgreementState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			inchargeAgreement = getInchargeAgreement(inchargeAgreement.getIdInchargeAgreementPk());
			inchargeAgreement.setAgreementState(InchargeAgreementStateType.CANCELLED.getCode());
			inchargeAgreement.setCancelUser(loggerUser.getUserName());
			inchargeAgreement.setCancelDate(CommonsUtilities.currentDateTime());
			
			update(inchargeAgreement);
			return inchargeAgreement;
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
}
