package com.pradera.negotiations.inchargeagreement.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.InchargeAgreement;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.InchargeAgreementStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.inchargeagreement.facade.InchargeAgreementFacade;
import com.pradera.negotiations.inchargeagreement.to.RegisterInchargeAgreementTO;
import com.pradera.negotiations.inchargeagreement.to.SearchInchargeAgreementTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InchargeAgreementMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class InchargeAgreementMgmtBean extends GenericBaseBean implements Serializable{

	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The account assignment facade. */
	@Inject
	AccountAssignmentFacade accountAssignmentFacade;	
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The incharge agreement facade. */
	@EJB
	InchargeAgreementFacade inchargeAgreementFacade;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The bl depositary. */
	private boolean blDepositary;
	
	/** The selected incharge agreement. */
	private InchargeAgreement selectedInchargeAgreement;
	
	/** The search incharge agreement to. */
	SearchInchargeAgreementTO searchInchargeAgreementTO;
	
	/** The register incharge agreement to. */
	RegisterInchargeAgreementTO registerInchargeAgreementTO;
	
	/** The map parameters. */
	private Map<Integer, String> mapParameters = new HashMap<Integer, String>();
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try{
			searchInchargeAgreementTO = new SearchInchargeAgreementTO();
			searchInchargeAgreementTO.setInitialDate(getCurrentSystemDate());
			searchInchargeAgreementTO.setEndDate(getCurrentSystemDate());
			searchInchargeAgreementTO.setLstNegotiationMechanism(accountAssignmentFacade.getListNegotiationMechanism());
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				blDepositary = true;
			}else{
				blDepositary = false;
				searchInchargeAgreementTO.setParticipantChargedSelected(userInfo.getUserAccountSession().getParticipantCode());
				List<Participant> lstTemp = new ArrayList<Participant>();
				Participant participant = accountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				lstTemp.add(participant);
				searchInchargeAgreementTO.setLstParticipantCharged(lstTemp);
				//searchInchargeAgreementTO.setLstNegotiationMechanism(accountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));
			}
			fillCombos();
			fillMaps();
			showPrivilegeButtons();
			searchInchargeAgreementTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
			changeNegotiationMechanism();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);		
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnCancel(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Fill maps.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillMaps() throws ServiceException{
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.CURRENCY.getCode());
			masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());			
		parameterTableTO.setLstMasterTableFk(masterPks);
		
		List<ParameterTable> lst = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt.getParameterName());
		}
		
		//Estados de encargo
		for(ParameterTable paramTab:searchInchargeAgreementTO.getLstStates()){
			mapParameters.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
			
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		//estado de contrato de encargo
		ParameterTableTO parameterTableTO = new ParameterTableTO();			
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INCHARGE_AGREEMENT_STATE.getCode());
		searchInchargeAgreementTO.setLstStates(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
	}
	
	/**
	 * Load registration page.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadRegistrationPage() throws ServiceException{
		try{
		clearRegistrationData();
		registerInchargeAgreementTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clear registration data.
	 *
	 * @throws ServiceException the service exception
	 */
	private void clearRegistrationData() throws ServiceException{
		registerInchargeAgreementTO = new RegisterInchargeAgreementTO();
		if(!userInfo.getUserAccountSession().isDepositaryInstitution()){
			registerInchargeAgreementTO.setParticipantChargedSelected(userInfo.getUserAccountSession().getParticipantCode());
			List<Participant> lstTemp = new ArrayList<Participant>();
			Participant participant = accountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
			lstTemp.add(participant);
			registerInchargeAgreementTO.setLstParticipantCharged(lstTemp);
			registerInchargeAgreementTO.setLstNegotiationMechanism(accountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));			
		}else registerInchargeAgreementTO.setLstNegotiationMechanism(searchInchargeAgreementTO.getLstNegotiationMechanism());
		registerInchargeAgreementTO.setLstNegotiationModality(searchInchargeAgreementTO.getLstNegotiationModality());
	}
	
	/**
	 * To get Search Results.
	 */
	@LoggerAuditWeb
	public void searchInChargeAgreement(){
		try{
			cleanResults();
			searchInchargeAgreementTO.setBlNoResult(true);
			List<InchargeAgreement> lstHolderAccountOperations = inchargeAgreementFacade.searchInchargeAgreement(searchInchargeAgreementTO);				
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountOperations)){
				searchInchargeAgreementTO.setLstInchargeAgreement(new GenericDataModel<InchargeAgreement>(lstHolderAccountOperations));					
				searchInchargeAgreementTO.setBlNoResult(false);
			}
		}
		catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}		
	}	
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		
		InchargeAgreement inchargeAgreement = this.getSelectedInchargeAgreement(); 
		executeAction();
		if(inchargeAgreement==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(inchargeAgreement.getAgreementState().equals(InchargeAgreementStateType.REGISTERED.getCode())){			
			showIncharge(inchargeAgreement);
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			return "viewInchargeAgreement";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_VALIDATION_REGISTER));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
		
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		InchargeAgreement inchargeAgreement = this.getSelectedInchargeAgreement(); 
		executeAction();
		if(inchargeAgreement==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(inchargeAgreement.getAgreementState().equals(InchargeAgreementStateType.REGISTERED.getCode())){			
			showIncharge(inchargeAgreement);
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());
			return "viewInchargeAgreement";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_VALIDATION_REGISTER));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}	
	
	/**
	 * Validate cancel.
	 *
	 * @return the string
	 */
	public String validateCancel(){
		InchargeAgreement inchargeAgreement = this.getSelectedInchargeAgreement(); 
		executeAction();
		if(inchargeAgreement==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(inchargeAgreement.getAgreementState().equals(InchargeAgreementStateType.CONFIRMED.getCode())){			
			showIncharge(inchargeAgreement);
			this.setViewOperationType(ViewOperationsType.CANCEL.getCode());
			return "viewInchargeAgreement";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_VALIDATION_CONFIRM));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}	
	
	/**
	 * Before confirm handler.
	 */
	public void beforeConfirmHandler()
	{
		executeAction();
		if(this.getSelectedInchargeAgreement()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getSelectedInchargeAgreement().getAgreementState().equals(InchargeAgreementStateType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_CONFIRM,
				    		  new Object[]{this.getSelectedInchargeAgreement().getIdInchargeAgreementPk()}) );			
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_VALIDATION_REGISTER));	
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Before reject handler.
	 */
	public void beforeRejectHandler()
	{
		executeAction();
		if(this.getSelectedInchargeAgreement()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getSelectedInchargeAgreement().getAgreementState().equals(InchargeAgreementStateType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_REJECT,
				    		  new Object[]{this.getSelectedInchargeAgreement().getIdInchargeAgreementPk()}) );			
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_VALIDATION_REGISTER));	
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Before cancel handler.
	 */
	public void beforeCancelHandler()
	{
		executeAction();
		if(this.getSelectedInchargeAgreement()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getSelectedInchargeAgreement().getAgreementState().equals(InchargeAgreementStateType.CONFIRMED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CANCEL)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_CANCEL,
				    		  new Object[]{this.getSelectedInchargeAgreement().getIdInchargeAgreementPk()}) );			
			this.setViewOperationType(ViewOperationsType.CANCEL.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_VALIDATION_CONFIRM));	
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Process operation handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		switch(ViewOperationsType.lookup.get(getViewOperationType())){
			case CONFIRM : confirmInchargeAgreement();break;
			case REJECT: rejectInchargeAgreement();break;
			case CANCEL: cancelInchargeAgreement();break;
		}
	}
	
	/**
	 * Reject incharge agreement.
	 */
	@LoggerAuditWeb
	private void rejectInchargeAgreement() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  				
		InchargeAgreement inchargeAgreement = null;
		try {
			inchargeAgreement = inchargeAgreementFacade.rejectInchargeAgreementServiceFacade(selectedInchargeAgreement);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwReject').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(inchargeAgreement!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_REJECT_OK,
				    		  new Object[]{inchargeAgreement.getIdInchargeAgreementPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		searchInchargeAgreementHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Cancel incharge agreement.
	 */
	@LoggerAuditWeb
	private void cancelInchargeAgreement() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  				
		InchargeAgreement inchargeAgreement = null;
		try {
			inchargeAgreement = inchargeAgreementFacade.cancelInchargeAgreementServiceFacade(selectedInchargeAgreement);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwCancel').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(inchargeAgreement!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_CANCEL_OK,
				    		  new Object[]{inchargeAgreement.getIdInchargeAgreementPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		searchInchargeAgreementHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Confirm incharge agreement.
	 */
	@LoggerAuditWeb
	private void confirmInchargeAgreement() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
				
		InchargeAgreement inchargeAgreement = null;
		try {
			inchargeAgreement = inchargeAgreementFacade.confirmInchargeAgreementServiceFacade(selectedInchargeAgreement);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(inchargeAgreement!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_CONFIRM_OK,
				    		  new Object[]{inchargeAgreement.getIdInchargeAgreementPk().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		searchInchargeAgreementHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Search incharge agreement handler.
	 */
	@LoggerAuditWeb
	public void searchInchargeAgreementHandler()
	{
		try{
			cleanResults();
			searchInchargeAgreementTO.setBlNoResult(true);
			List<InchargeAgreement> lstHolderAccountOperations = inchargeAgreementFacade.searchInchargeAgreement(searchInchargeAgreementTO);				
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountOperations)){
				searchInchargeAgreementTO.setLstInchargeAgreement(new GenericDataModel<InchargeAgreement>(lstHolderAccountOperations));					
				searchInchargeAgreementTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Show incharge.
	 *
	 * @param inchargeAgreement the incharge agreement
	 * @return the string
	 */
	public String showIncharge(InchargeAgreement inchargeAgreement){
		executeAction();		
		RegisterInchargeAgreementTO auxRegisterInchargeAgreementTO=null;
		try{
			auxRegisterInchargeAgreementTO = inchargeAgreementFacade.getInchargeAgreement(inchargeAgreement);
			setDetailInchargeAgreement(auxRegisterInchargeAgreementTO);
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}			
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		return "viewInchargeAgreement";
	}
	
	/**
	 * Sets the detail incharge agreement.
	 *
	 * @param auxRegisterInchargeAgreementTO the new detail incharge agreement
	 */
	public void setDetailInchargeAgreement(RegisterInchargeAgreementTO auxRegisterInchargeAgreementTO){
		try{
			registerInchargeAgreementTO = new RegisterInchargeAgreementTO();
			registerInchargeAgreementTO.setLstStates(searchInchargeAgreementTO.getLstStates());
			registerInchargeAgreementTO.setLstNegotiationMechanism(searchInchargeAgreementTO.getLstNegotiationMechanism());
			//mecanismo			
			if(Validations.validateIsNotNullAndNotEmpty(auxRegisterInchargeAgreementTO.getNegoMechanismSelected())){
				registerInchargeAgreementTO.setNegoMechanismSelected(auxRegisterInchargeAgreementTO.getNegoMechanismSelected());
				if(blDepositary)
					registerInchargeAgreementTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(auxRegisterInchargeAgreementTO.getNegoMechanismSelected(), null, ComponentConstant.ONE));
				else
					registerInchargeAgreementTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(auxRegisterInchargeAgreementTO.getNegoMechanismSelected(),
							auxRegisterInchargeAgreementTO.getParticipantChargedSelected(),ComponentConstant.ONE));
			}	
			//modalidad
			if(Validations.validateIsNotNullAndNotEmpty(auxRegisterInchargeAgreementTO.getNegoModalitySelected())){
				registerInchargeAgreementTO.setNegoModalitySelected(auxRegisterInchargeAgreementTO.getNegoModalitySelected());
				if(blDepositary){					
					registerInchargeAgreementTO.setLstParticipantCharged(accountAssignmentFacade.getListParticipantsForNegoModality(auxRegisterInchargeAgreementTO.getNegoMechanismSelected()
																															, auxRegisterInchargeAgreementTO.getNegoModalitySelected(), ParticipantType.INDIRECT.getCode(), ComponentConstant.ONE));
				}
				registerInchargeAgreementTO.setLstParticipantTraders(accountAssignmentFacade.getListParticipantsForNegoModality(auxRegisterInchargeAgreementTO.getNegoMechanismSelected()
						, auxRegisterInchargeAgreementTO.getNegoModalitySelected(), ParticipantType.DIRECT.getCode(),null));
			}
			registerInchargeAgreementTO.setParticipantChargedSelected(auxRegisterInchargeAgreementTO.getParticipantChargedSelected());
			registerInchargeAgreementTO.setParticipantTraderSelected(auxRegisterInchargeAgreementTO.getParticipantTraderSelected());
			registerInchargeAgreementTO.setRoleSelectedBuy(auxRegisterInchargeAgreementTO.getRoleSelectedBuy());
			registerInchargeAgreementTO.setRoleSelectedSell(auxRegisterInchargeAgreementTO.getRoleSelectedSell());
			registerInchargeAgreementTO.setInchargeTypeFunds(auxRegisterInchargeAgreementTO.getInchargeTypeFunds());
			registerInchargeAgreementTO.setInchargeTypeStock(auxRegisterInchargeAgreementTO.getInchargeTypeStock());
			registerInchargeAgreementTO.setHolder(auxRegisterInchargeAgreementTO.getHolder());
			registerInchargeAgreementTO.setStateSelected(auxRegisterInchargeAgreementTO.getStateSelected());
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		searchInchargeAgreementTO.setInitialDate(getCurrentSystemDate());
		searchInchargeAgreementTO.setEndDate(getCurrentSystemDate());
		searchInchargeAgreementTO.setParticipantTraderSelected(null);
		searchInchargeAgreementTO.setLstParticipantTraders(null);
		searchInchargeAgreementTO.setLstNegotiationModality(null);
		searchInchargeAgreementTO.setNegoModalitySelected(null);
		searchInchargeAgreementTO.setNegoMechanismSelected(null);
		searchInchargeAgreementTO.setRoleSelectedBuy(false);
		searchInchargeAgreementTO.setRoleSelectedSell(false);
		searchInchargeAgreementTO.setInchargeTypeFunds(false);
		searchInchargeAgreementTO.setInchargeTypeStock(false);
		searchInchargeAgreementTO.setStateSelected(null);
		if(blDepositary){
			searchInchargeAgreementTO.setLstParticipantCharged(null);
			searchInchargeAgreementTO.setParticipantChargedSelected(null);
		}
		searchInchargeAgreementTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
		changeNegotiationMechanism();
	}
	
	/**
	 * Change negotiation mechanism.
	 */
	public void changeNegotiationMechanism(){
		try {
			JSFUtilities.resetViewRoot();
			cleanResults();
			searchInchargeAgreementTO.setLstNegotiationModality(null);
			searchInchargeAgreementTO.setNegoModalitySelected(null);
			searchInchargeAgreementTO.setParticipantTraderSelected(null);
			searchInchargeAgreementTO.setLstParticipantTraders(null);
			if(blDepositary){
				searchInchargeAgreementTO.setLstParticipantCharged(null);
				searchInchargeAgreementTO.setParticipantChargedSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchInchargeAgreementTO.getNegoMechanismSelected())){
				if(blDepositary)
					searchInchargeAgreementTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(searchInchargeAgreementTO.getNegoMechanismSelected(), null, ComponentConstant.ONE));
				else
					searchInchargeAgreementTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(searchInchargeAgreementTO.getNegoMechanismSelected(),
																													searchInchargeAgreementTO.getParticipantChargedSelected(),ComponentConstant.ONE));
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change negotiation mechanism for register.
	 */
	public void changeNegotiationMechanismForRegister(){
		try {
			JSFUtilities.resetViewRoot();
			registerInchargeAgreementTO.setLstNegotiationModality(null);
			registerInchargeAgreementTO.setNegoModalitySelected(null);
			registerInchargeAgreementTO.setParticipantTraderSelected(null);
			registerInchargeAgreementTO.setLstParticipantTraders(null);
			if(blDepositary){
				registerInchargeAgreementTO.setLstParticipantCharged(null);
				registerInchargeAgreementTO.setParticipantChargedSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(registerInchargeAgreementTO.getNegoMechanismSelected())){
				if(blDepositary)
					registerInchargeAgreementTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(registerInchargeAgreementTO.getNegoMechanismSelected(), null, ComponentConstant.ONE));
				else
					registerInchargeAgreementTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(registerInchargeAgreementTO.getNegoMechanismSelected(),
							registerInchargeAgreementTO.getParticipantChargedSelected(),ComponentConstant.ONE));
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change negotiation modality for register.
	 */
	public void changeNegotiationModalityForRegister(){
		try {			
			JSFUtilities.resetComponent("frmInchargeAgreement:cboIncharge");
			JSFUtilities.resetComponent("frmInchargeAgreement:cboTrader");
			registerInchargeAgreementTO.setLstParticipantTraders(null);
			registerInchargeAgreementTO.setParticipantTraderSelected(null);
			if(blDepositary){
				registerInchargeAgreementTO.setLstParticipantCharged(null);
				registerInchargeAgreementTO.setParticipantChargedSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(registerInchargeAgreementTO.getNegoModalitySelected())){
				if(blDepositary){					
					registerInchargeAgreementTO.setLstParticipantCharged(accountAssignmentFacade.getListParticipantsForNegoModality(registerInchargeAgreementTO.getNegoMechanismSelected()
																															, registerInchargeAgreementTO.getNegoModalitySelected(), ParticipantType.INDIRECT.getCode(), ComponentConstant.ONE));
				}
				registerInchargeAgreementTO.setLstParticipantTraders(accountAssignmentFacade.getListParticipantsForNegoModality(registerInchargeAgreementTO.getNegoMechanismSelected()
						, registerInchargeAgreementTO.getNegoModalitySelected(), ParticipantType.DIRECT.getCode(),null));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate rule incharge.
	 *
	 * @param num the num
	 * @throws ServiceException the service exception
	 */
	public void validateRuleIncharge(String num) throws ServiceException{
		
		if(Validations.validateIsNotNullAndPositive(registerInchargeAgreementTO.getParticipantChargedSelected())
				&& Validations.validateIsNotNullAndPositive(registerInchargeAgreementTO.getParticipantTraderSelected())){
			if(registerInchargeAgreementTO.getParticipantChargedSelected().equals(registerInchargeAgreementTO.getParticipantTraderSelected())){
				if(Integer.parseInt(num) == GeneralConstants.ONE_VALUE_INTEGER)
					registerInchargeAgreementTO.setParticipantChargedSelected(null);
				if(Integer.parseInt(num) == GeneralConstants.TWO_VALUE_INTEGER)
					registerInchargeAgreementTO.setParticipantTraderSelected(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_PARTICIPANT_COMBINATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
			
		if(Validations.validateIsNotNullAndPositive(registerInchargeAgreementTO.getParticipantChargedSelected())
				&& Validations.validateIsNotNullAndPositive(registerInchargeAgreementTO.getParticipantTraderSelected())
				&& (registerInchargeAgreementTO.getRoleSelectedBuy() == true || registerInchargeAgreementTO.getRoleSelectedSell()== true)
				&& (registerInchargeAgreementTO.getInchargeTypeFunds() == true || registerInchargeAgreementTO.getInchargeTypeStock()== true)){
			String keyInchargeRule = new StringBuilder(ComponentConstant.ZERO).append(ComponentConstant.ZERO).append(ComponentConstant.ZERO).toString();
			Map<String, String> mpInchargeRule = null;
			if(registerInchargeAgreementTO.getRoleSelectedBuy()== true && registerInchargeAgreementTO.getRoleSelectedSell()== true){
				mpInchargeRule = inchargeAgreementFacade.getInchargeRules(ParticipantRoleType.BUY.getCode());
				if(registerInchargeAgreementTO.getInchargeTypeFunds()== true && registerInchargeAgreementTO.getInchargeTypeStock()== true){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ZERO).append(ComponentConstant.ONE).append(ComponentConstant.ONE).toString();
				}else if(registerInchargeAgreementTO.getInchargeTypeStock()== true){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ONE).append(ComponentConstant.ZERO).append(ComponentConstant.ONE).toString();
				}
				if(Validations.validateIsNullOrEmpty(mpInchargeRule.get(keyInchargeRule))){
					if(Integer.parseInt(num) == GeneralConstants.ONE_VALUE_INTEGER)
						registerInchargeAgreementTO.setParticipantChargedSelected(null);
					if(Integer.parseInt(num) == GeneralConstants.TWO_VALUE_INTEGER)
						registerInchargeAgreementTO.setParticipantTraderSelected(null);
					if(Integer.parseInt(num) == GeneralConstants.THREE_VALUE_INTEGER)
						registerInchargeAgreementTO.setRoleSelectedBuy(false);							
					if(Integer.parseInt(num) == GeneralConstants.FOUR_VALUE_INTEGER)
						registerInchargeAgreementTO.setRoleSelectedSell(false);					
					if(Integer.parseInt(num) == 5)
						registerInchargeAgreementTO.setInchargeTypeFunds(false);
					if(Integer.parseInt(num) == 6){
						registerInchargeAgreementTO.setInchargeTypeStock(false);
						registerInchargeAgreementTO.setInchargeTypeFunds(false);
					}
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_COMBINATION));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}				
				mpInchargeRule = inchargeAgreementFacade.getInchargeRules(ParticipantRoleType.SELL.getCode());
				if(registerInchargeAgreementTO.getInchargeTypeFunds()== true && registerInchargeAgreementTO.getInchargeTypeStock()== true){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ZERO).append(ComponentConstant.ONE).append(ComponentConstant.ONE).toString();
				}else if(registerInchargeAgreementTO.getInchargeTypeStock()== true){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ONE).append(ComponentConstant.ZERO).append(ComponentConstant.ONE).toString();
				}
				if(Validations.validateIsNullOrEmpty(mpInchargeRule.get(keyInchargeRule))){
					if(Integer.parseInt(num) == GeneralConstants.ONE_VALUE_INTEGER)
						registerInchargeAgreementTO.setParticipantChargedSelected(null);
					if(Integer.parseInt(num) == GeneralConstants.TWO_VALUE_INTEGER)
						registerInchargeAgreementTO.setParticipantTraderSelected(null);
					if(Integer.parseInt(num) == GeneralConstants.THREE_VALUE_INTEGER)
						registerInchargeAgreementTO.setRoleSelectedBuy(false);							
					if(Integer.parseInt(num) == GeneralConstants.FOUR_VALUE_INTEGER)
						registerInchargeAgreementTO.setRoleSelectedSell(false);					
					if(Integer.parseInt(num) == 5)
						registerInchargeAgreementTO.setInchargeTypeFunds(false);
					if(Integer.parseInt(num) == 6){
						registerInchargeAgreementTO.setInchargeTypeStock(false);
						registerInchargeAgreementTO.setInchargeTypeFunds(false);
					}
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_COMBINATION));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}else{
				if(registerInchargeAgreementTO.getRoleSelectedBuy()== true)
					mpInchargeRule = inchargeAgreementFacade.getInchargeRules(ParticipantRoleType.BUY.getCode());
				if(registerInchargeAgreementTO.getRoleSelectedSell()== true)
					mpInchargeRule = inchargeAgreementFacade.getInchargeRules(ParticipantRoleType.SELL.getCode());				
				if(registerInchargeAgreementTO.getInchargeTypeFunds() == true && registerInchargeAgreementTO.getInchargeTypeStock()== true){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ZERO).append(ComponentConstant.ONE).append(ComponentConstant.ONE).toString();
				}else if(registerInchargeAgreementTO.getInchargeTypeStock()== true){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ONE).append(ComponentConstant.ZERO).append(ComponentConstant.ONE).toString();
				}
				if(Validations.validateIsNullOrEmpty(mpInchargeRule.get(keyInchargeRule))){
					if(Integer.parseInt(num) == GeneralConstants.ONE_VALUE_INTEGER)
						registerInchargeAgreementTO.setParticipantChargedSelected(null);
					if(Integer.parseInt(num) == GeneralConstants.TWO_VALUE_INTEGER)
						registerInchargeAgreementTO.setParticipantTraderSelected(null);
					if(Integer.parseInt(num) == GeneralConstants.THREE_VALUE_INTEGER)
						registerInchargeAgreementTO.setRoleSelectedBuy(false);							
					if(Integer.parseInt(num) == GeneralConstants.FOUR_VALUE_INTEGER)
						registerInchargeAgreementTO.setRoleSelectedSell(false);					
					if(Integer.parseInt(num) == 5)
						registerInchargeAgreementTO.setInchargeTypeFunds(false);
					if(Integer.parseInt(num) == 6){
						registerInchargeAgreementTO.setInchargeTypeStock(false);
						registerInchargeAgreementTO.setInchargeTypeFunds(false);
					}
						
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_COMBINATION));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		}
		if(Validations.validateIsNotNullAndPositive(registerInchargeAgreementTO.getParticipantTraderSelected())
				&& Validations.validateIsNotNullAndPositive(registerInchargeAgreementTO.getParticipantChargedSelected())
				&& Validations.validateIsNotNull(registerInchargeAgreementTO.getHolder()))
			validateHolder();
	}
	
	
	/**
	 * Change negotiation modality.
	 */
	public void changeNegotiationModality(){
		try {
			cleanResults();
			JSFUtilities.resetComponent("frmInchargeAgreement:cboIncharge");
			searchInchargeAgreementTO.setLstParticipantTraders(null);
			searchInchargeAgreementTO.setParticipantTraderSelected(null);
			if(blDepositary){
				searchInchargeAgreementTO.setLstParticipantCharged(null);
				searchInchargeAgreementTO.setParticipantChargedSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchInchargeAgreementTO.getNegoModalitySelected())){
				if(blDepositary){					
					searchInchargeAgreementTO.setLstParticipantCharged(accountAssignmentFacade.getListParticipantsForNegoModality(searchInchargeAgreementTO.getNegoMechanismSelected()
																															, searchInchargeAgreementTO.getNegoModalitySelected(), ParticipantType.INDIRECT.getCode(), ComponentConstant.ONE));
				}
				searchInchargeAgreementTO.setLstParticipantTraders(accountAssignmentFacade.getListParticipantsForNegoModality(searchInchargeAgreementTO.getNegoMechanismSelected()
						, searchInchargeAgreementTO.getNegoModalitySelected(), ParticipantType.DIRECT.getCode(),null));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		selectedInchargeAgreement = null;
		searchInchargeAgreementTO.setBlNoResult(false);
		searchInchargeAgreementTO.setLstInchargeAgreement(null);
	}
	
	/**
	 * Validate holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateHolder() throws ServiceException{
		
		if(Validations.validateIsNullOrNotPositive(registerInchargeAgreementTO.getParticipantChargedSelected())
				|| Validations.validateIsNullOrNotPositive(registerInchargeAgreementTO.getParticipantTraderSelected())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_SELECT));
			JSFUtilities.showSimpleValidationDialog();
			registerInchargeAgreementTO.setHolder(new Holder());
			return;
		}
		if(Validations.validateIsNotNullAndNotEmpty(registerInchargeAgreementTO.getHolder().getIdHolderPk())){
			if(HolderStateType.BLOCKED.getCode().equals(registerInchargeAgreementTO.getHolder().getStateHolder())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();
				registerInchargeAgreementTO.setHolder(new Holder());
				return;
			}
			
			List<HolderAccount> lstResultAux = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(registerInchargeAgreementTO.getHolder().getIdHolderPk(), 
					registerInchargeAgreementTO.getParticipantTraderSelected());
			if(Validations.validateListIsNullOrEmpty(lstResultAux)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE));
				JSFUtilities.showSimpleValidationDialog();
				registerInchargeAgreementTO.setHolder(new Holder());	
				return;
			}
			
			List<HolderAccount> lstResult = accountAssignmentFacade.findHolderAccountsForInchargeAgreement(registerInchargeAgreementTO.getHolder().getIdHolderPk(), 
					registerInchargeAgreementTO.getParticipantChargedSelected());
			if(Validations.validateListIsNullOrEmpty(lstResult)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE));
				JSFUtilities.showSimpleValidationDialog();
				registerInchargeAgreementTO.setHolder(new Holder());	
				return;
			}
		}
	}
	
	/**
	 * Before save incharge agreement.
	 */
	public void beforeSaveInchargeAgreement(){
		try{
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  if(Validations.validateIsNullOrNotPositive(registerInchargeAgreementTO.getNegoMechanismSelected())
				  || Validations.validateIsNullOrNotPositive(registerInchargeAgreementTO.getNegoModalitySelected())
				  || Validations.validateIsNullOrNotPositive(registerInchargeAgreementTO.getParticipantChargedSelected())
					|| Validations.validateIsNullOrNotPositive(registerInchargeAgreementTO.getParticipantTraderSelected())
					|| (registerInchargeAgreementTO.getRoleSelectedBuy()==false && registerInchargeAgreementTO.getRoleSelectedSell()==false)
					|| (registerInchargeAgreementTO.getInchargeTypeFunds()==false && registerInchargeAgreementTO.getInchargeTypeStock()==false)
					|| Validations.validateIsNull(registerInchargeAgreementTO.getHolder())
					|| Validations.validateIsNullOrNotPositive(registerInchargeAgreementTO.getHolder().getIdHolderPk()))
			{
				  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
				   JSFUtilities.showSimpleValidationDialog();
				   return;
			}
		  List<InchargeAgreement>  lstInchargeAgreement = (List<InchargeAgreement>)inchargeAgreementFacade.getInchargeAgreementForFilter
					(registerInchargeAgreementTO.getNegoMechanismSelected(),
							registerInchargeAgreementTO.getNegoModalitySelected(),
							registerInchargeAgreementTO.getRoleSelectedBuy(),
							registerInchargeAgreementTO.getRoleSelectedSell(),
							registerInchargeAgreementTO.getParticipantTraderSelected(),
							registerInchargeAgreementTO.getParticipantChargedSelected(), registerInchargeAgreementTO.getHolder().getIdHolderPk());
		  if(Validations.validateIsNotNull(lstInchargeAgreement) && lstInchargeAgreement.size() == GeneralConstants.ONE_VALUE_INTEGER){
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_DATA_EXIST));	
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_REGISTER));	
		  JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Save incharge agreement handler.
	 */
	@LoggerAuditWeb
	public void saveInchargeAgreementHandler()
	{
		InchargeAgreement inchargeAgreement=getInchargeAgreement(registerInchargeAgreementTO);				
		try {					
			inchargeAgreement=inchargeAgreementFacade.registryInchargeAgreementServiceFacade(inchargeAgreement);
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				cleanInchargeAgreementSearchHandler();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(Validations.validateIsNotNullAndNotEmpty(inchargeAgreement)){
			setViewOperationType(ViewOperationsType.CONSULT.getCode());	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_INCHARGE_AGREEMENT_REGISTER_OK,
				    		  new Object[]{inchargeAgreement.getIdInchargeAgreementPk().toString()}) );						
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}	
		cleanInchargeAgreementSearchHandler();
	}
	
	/**
	 * Clean incharge agreement search handler.
	 */
	public void cleanInchargeAgreementSearchHandler(){
		executeAction();
		cleanResults();
		searchInchargeAgreementTO.setInitialDate(getCurrentSystemDate());
		searchInchargeAgreementTO.setEndDate(getCurrentSystemDate());
		searchInchargeAgreementTO.setParticipantTraderSelected(null);
		searchInchargeAgreementTO.setLstParticipantTraders(null);
		searchInchargeAgreementTO.setLstNegotiationModality(null);
		searchInchargeAgreementTO.setNegoModalitySelected(null);
		searchInchargeAgreementTO.setNegoMechanismSelected(null);
		searchInchargeAgreementTO.setRoleSelectedBuy(false);
		searchInchargeAgreementTO.setRoleSelectedSell(false);
		searchInchargeAgreementTO.setInchargeTypeFunds(false);
		searchInchargeAgreementTO.setInchargeTypeStock(false);
		searchInchargeAgreementTO.setStateSelected(null);
		if(blDepositary){
			searchInchargeAgreementTO.setLstParticipantCharged(null);
			searchInchargeAgreementTO.setParticipantChargedSelected(null);
		}
	}
	
	/**
	 * Gets the incharge agreement.
	 *
	 * @param registerInchargeAgreementTO the register incharge agreement to
	 * @return the incharge agreement
	 */
	public InchargeAgreement getInchargeAgreement(RegisterInchargeAgreementTO registerInchargeAgreementTO){
		InchargeAgreement inchargeAgreement=new InchargeAgreement();
		inchargeAgreement.setAgreementState(InchargeAgreementStateType.REGISTERED.getCode());
		NegotiationMechanism negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(registerInchargeAgreementTO.getNegoMechanismSelected());
		inchargeAgreement.setNegotiationMechanism(negotiationMechanism);
		NegotiationModality negotiationModality = new NegotiationModality();
		negotiationModality.setIdNegotiationModalityPk(registerInchargeAgreementTO.getNegoModalitySelected());
		inchargeAgreement.setMegotiationModality(negotiationModality);
		Participant traderParticipant = new Participant();
		traderParticipant.setIdParticipantPk(registerInchargeAgreementTO.getParticipantTraderSelected());
		inchargeAgreement.setTraderParticipant(traderParticipant);
		Participant inchargeParticpant = new Participant();
		inchargeParticpant.setIdParticipantPk(registerInchargeAgreementTO.getParticipantChargedSelected());
		inchargeAgreement.setInchargeParticipant(inchargeParticpant);
		if(registerInchargeAgreementTO.getRoleSelectedBuy() && registerInchargeAgreementTO.getRoleSelectedSell()){
			inchargeAgreement.setIndPurchaseRole(GeneralConstants.ONE_VALUE_INTEGER);
			inchargeAgreement.setIndSaleRole(GeneralConstants.ONE_VALUE_INTEGER);
		}else 	if(registerInchargeAgreementTO.getRoleSelectedBuy()){
			inchargeAgreement.setIndPurchaseRole(GeneralConstants.ONE_VALUE_INTEGER);
			inchargeAgreement.setIndSaleRole(GeneralConstants.ZERO_VALUE_INTEGER);
		}else if(registerInchargeAgreementTO.getRoleSelectedSell()){
			inchargeAgreement.setIndSaleRole(GeneralConstants.ONE_VALUE_INTEGER);
			inchargeAgreement.setIndPurchaseRole(GeneralConstants.ZERO_VALUE_INTEGER);
		}
			
		if(registerInchargeAgreementTO.getInchargeTypeFunds() && registerInchargeAgreementTO.getInchargeTypeStock()){
			inchargeAgreement.setIndFundsIncharge(GeneralConstants.ONE_VALUE_INTEGER);
			inchargeAgreement.setIndStockIncharge(GeneralConstants.ONE_VALUE_INTEGER);
		}else if(registerInchargeAgreementTO.getInchargeTypeFunds()){
			inchargeAgreement.setIndFundsIncharge(GeneralConstants.ONE_VALUE_INTEGER);
			inchargeAgreement.setIndStockIncharge(GeneralConstants.ZERO_VALUE_INTEGER);
		}else if(registerInchargeAgreementTO.getInchargeTypeStock()){
			inchargeAgreement.setIndFundsIncharge(GeneralConstants.ZERO_VALUE_INTEGER);
			inchargeAgreement.setIndStockIncharge(GeneralConstants.ONE_VALUE_INTEGER);
		}
		inchargeAgreement.setHolder(registerInchargeAgreementTO.getHolder());
		return inchargeAgreement;
	}
	
	/**
	 * Clean incharge agreement handler.
	 */
	public void cleanInchargeAgreementHandler(){
		JSFUtilities.resetViewRoot();
		registerInchargeAgreementTO.setNegoMechanismSelected(null);
		registerInchargeAgreementTO.setNegoModalitySelected(null);
		registerInchargeAgreementTO.setRoleSelectedBuy(false);
		registerInchargeAgreementTO.setRoleSelectedSell(false);
		registerInchargeAgreementTO.setInchargeTypeFunds(false);
		registerInchargeAgreementTO.setInchargeTypeStock(false);
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
			registerInchargeAgreementTO.setParticipantChargedSelected(null);
		}
		registerInchargeAgreementTO.setParticipantTraderSelected(null);
		registerInchargeAgreementTO.setHolder(new Holder());
	}
	
	/**
	 * Gets the role description.
	 *
	 * @param role the role
	 * @return the role description
	 */
	public String getRoleDescription(Integer role){
		return ParticipantRoleType.get(role).getValue();
	}
	
	/**
	 * Gets the search incharge agreement to.
	 *
	 * @return the search incharge agreement to
	 */
	public SearchInchargeAgreementTO getSearchInchargeAgreementTO() {
		return searchInchargeAgreementTO;
	}
	
	/**
	 * Sets the search incharge agreement to.
	 *
	 * @param searchInchargeAgreementTO the new search incharge agreement to
	 */
	public void setSearchInchargeAgreementTO(
			SearchInchargeAgreementTO searchInchargeAgreementTO) {
		this.searchInchargeAgreementTO = searchInchargeAgreementTO;
	}
	
	/**
	 * Checks if is bl depositary.
	 *
	 * @return true, if is bl depositary
	 */
	public boolean isBlDepositary() {
		return blDepositary;
	}
	
	/**
	 * Sets the bl depositary.
	 *
	 * @param blDepositary the new bl depositary
	 */
	public void setBlDepositary(boolean blDepositary) {
		this.blDepositary = blDepositary;
	}
	
	/**
	 * Gets the selected incharge agreement.
	 *
	 * @return the selected incharge agreement
	 */
	public InchargeAgreement getSelectedInchargeAgreement() {
		return selectedInchargeAgreement;
	}
	
	/**
	 * Sets the selected incharge agreement.
	 *
	 * @param selectedInchargeAgreement the new selected incharge agreement
	 */
	public void setSelectedInchargeAgreement(
			InchargeAgreement selectedInchargeAgreement) {
		this.selectedInchargeAgreement = selectedInchargeAgreement;
	}
	
	/**
	 * Gets the register incharge agreement to.
	 *
	 * @return the register incharge agreement to
	 */
	public RegisterInchargeAgreementTO getRegisterInchargeAgreementTO() {
		return registerInchargeAgreementTO;
	}
	
	/**
	 * Sets the register incharge agreement to.
	 *
	 * @param registerInchargeAgreementTO the new register incharge agreement to
	 */
	public void setRegisterInchargeAgreementTO(
			RegisterInchargeAgreementTO registerInchargeAgreementTO) {
		this.registerInchargeAgreementTO = registerInchargeAgreementTO;
	}
	
	
}
