package com.pradera.negotiations.inchargeagreement.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterInchargeAgreementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterInchargeAgreementTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism> lstNegotiationMechanism;	
	
	/** The lst negotiation modality. */
	private List<NegotiationModality> lstNegotiationModality;
	
	/** The lst participant charged. */
	private List<Participant> lstParticipantTraders, lstParticipantCharged;
	
	/** The nego modality selected. */
	private Long negoMechanismSelected, negoModalitySelected;
	
	/** The lst states. */
	private List<ParameterTable> lstStates;
	
	/** The state selected. */
	private Long  stateSelected;
	
	/** The participant charged selected. */
	private Long participantTraderSelected, participantChargedSelected;
	
	/** The incharge type stock. */
	private boolean roleSelectedBuy = false,roleSelectedSell = false,inchargeTypeFunds = false,inchargeTypeStock = false;
	
	/** The holder. */
	private Holder holder = new Holder();
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}
	
	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(
			List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
	
	/**
	 * Gets the lst participant traders.
	 *
	 * @return the lst participant traders
	 */
	public List<Participant> getLstParticipantTraders() {
		return lstParticipantTraders;
	}
	
	/**
	 * Sets the lst participant traders.
	 *
	 * @param lstParticipantTraders the new lst participant traders
	 */
	public void setLstParticipantTraders(List<Participant> lstParticipantTraders) {
		this.lstParticipantTraders = lstParticipantTraders;
	}
	
	/**
	 * Gets the lst participant charged.
	 *
	 * @return the lst participant charged
	 */
	public List<Participant> getLstParticipantCharged() {
		return lstParticipantCharged;
	}
	
	/**
	 * Sets the lst participant charged.
	 *
	 * @param lstParticipantCharged the new lst participant charged
	 */
	public void setLstParticipantCharged(List<Participant> lstParticipantCharged) {
		this.lstParticipantCharged = lstParticipantCharged;
	}
	
	/**
	 * Gets the nego mechanism selected.
	 *
	 * @return the nego mechanism selected
	 */
	public Long getNegoMechanismSelected() {
		return negoMechanismSelected;
	}
	
	/**
	 * Sets the nego mechanism selected.
	 *
	 * @param negoMechanismSelected the new nego mechanism selected
	 */
	public void setNegoMechanismSelected(Long negoMechanismSelected) {
		this.negoMechanismSelected = negoMechanismSelected;
	}
	
	/**
	 * Gets the nego modality selected.
	 *
	 * @return the nego modality selected
	 */
	public Long getNegoModalitySelected() {
		return negoModalitySelected;
	}
	
	/**
	 * Sets the nego modality selected.
	 *
	 * @param negoModalitySelected the new nego modality selected
	 */
	public void setNegoModalitySelected(Long negoModalitySelected) {
		this.negoModalitySelected = negoModalitySelected;
	}
	
	/**
	 * Gets the participant trader selected.
	 *
	 * @return the participant trader selected
	 */
	public Long getParticipantTraderSelected() {
		return participantTraderSelected;
	}
	
	/**
	 * Sets the participant trader selected.
	 *
	 * @param participantTraderSelected the new participant trader selected
	 */
	public void setParticipantTraderSelected(Long participantTraderSelected) {
		this.participantTraderSelected = participantTraderSelected;
	}
	
	/**
	 * Gets the participant charged selected.
	 *
	 * @return the participant charged selected
	 */
	public Long getParticipantChargedSelected() {
		return participantChargedSelected;
	}
	
	/**
	 * Sets the participant charged selected.
	 *
	 * @param participantChargedSelected the new participant charged selected
	 */
	public void setParticipantChargedSelected(Long participantChargedSelected) {
		this.participantChargedSelected = participantChargedSelected;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the role selected buy.
	 *
	 * @return the role selected buy
	 */
	public boolean getRoleSelectedBuy() {
		return roleSelectedBuy;
	}
	
	/**
	 * Sets the role selected buy.
	 *
	 * @param roleSelectedBuy the new role selected buy
	 */
	public void setRoleSelectedBuy(boolean roleSelectedBuy) {
		this.roleSelectedBuy = roleSelectedBuy;
	}
	
	/**
	 * Gets the role selected sell.
	 *
	 * @return the role selected sell
	 */
	public boolean getRoleSelectedSell() {
		return roleSelectedSell;
	}
	
	/**
	 * Sets the role selected sell.
	 *
	 * @param roleSelectedSell the new role selected sell
	 */
	public void setRoleSelectedSell(boolean roleSelectedSell) {
		this.roleSelectedSell = roleSelectedSell;
	}
	
	/**
	 * Gets the incharge type funds.
	 *
	 * @return the incharge type funds
	 */
	public boolean getInchargeTypeFunds() {
		return inchargeTypeFunds;
	}
	
	/**
	 * Sets the incharge type funds.
	 *
	 * @param inchargeTypeFunds the new incharge type funds
	 */
	public void setInchargeTypeFunds(boolean inchargeTypeFunds) {
		this.inchargeTypeFunds = inchargeTypeFunds;
	}
	
	/**
	 * Gets the incharge type stock.
	 *
	 * @return the incharge type stock
	 */
	public boolean getInchargeTypeStock() {
		return inchargeTypeStock;
	}
	
	/**
	 * Sets the incharge type stock.
	 *
	 * @param inchargeTypeStock the new incharge type stock
	 */
	public void setInchargeTypeStock(boolean inchargeTypeStock) {
		this.inchargeTypeStock = inchargeTypeStock;
	}
	
	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}
	
	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}
	
	/**
	 * Gets the state selected.
	 *
	 * @return the state selected
	 */
	public Long getStateSelected() {
		return stateSelected;
	}
	
	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the new state selected
	 */
	public void setStateSelected(Long stateSelected) {
		this.stateSelected = stateSelected;
	}

}
