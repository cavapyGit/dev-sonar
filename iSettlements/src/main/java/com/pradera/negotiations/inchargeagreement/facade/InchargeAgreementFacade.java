package com.pradera.negotiations.inchargeagreement.facade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.InchargeAgreement;
import com.pradera.model.negotiation.InchargeRule;
import com.pradera.model.negotiation.LoanableSecuritiesRequest;
import com.pradera.negotiations.assignmentrequest.to.AccountAssignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAssignmentRequestTO;
import com.pradera.negotiations.inchargeagreement.service.InchargeAgreementService;
import com.pradera.negotiations.inchargeagreement.to.RegisterInchargeAgreementTO;
import com.pradera.negotiations.inchargeagreement.to.SearchInchargeAgreementTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InchargeAgreementFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InchargeAgreementFacade {

	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The incharge agreement service. */
	@EJB
	InchargeAgreementService inchargeAgreementService;
	
	/**
	 * Gets the incharge rules.
	 *
	 * @param role the role
	 * @return the incharge rules
	 * @throws ServiceException the service exception
	 */
	public Map<String, String> getInchargeRules(Integer role)throws ServiceException {
		Map<String, String> mpInchargeRule = new HashMap<String, String>();
		List<InchargeRule> lstInchargeRule = inchargeAgreementService.getInchargeRule(role);
		if(Validations.validateListIsNotNullAndNotEmpty(lstInchargeRule)){
			String keyInchargeRule = "";
			for(InchargeRule inchargeRule:lstInchargeRule){
				keyInchargeRule = inchargeRule.getIndTraderParticipant().toString() + inchargeRule.getIndStockParticipant().toString() + inchargeRule.getIndFundsParticipant().toString();
				mpInchargeRule.put(keyInchargeRule, BooleanType.YES.getCode().toString());
			}
		}
		return mpInchargeRule;
	}
	
	/**
	 * Registry Incharge Agreement.
	 *
	 * @param inchargeAgreement object Incharge Agreement
	 * @return  object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.INCHARGE_AGREEMENT_REGISTER)
	public InchargeAgreement registryInchargeAgreementServiceFacade(InchargeAgreement inchargeAgreement) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());       
        return inchargeAgreementService.registryInchargeAgreement(inchargeAgreement, loggerUser);
	}
	
	/**
	 * Search Incharge Agreement.
	 *
	 * @param searchInchargeAgreementTO the search incharge agreement to
	 * @return  object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.INCHARGE_AGREEMENT_QUERY)
	public List<InchargeAgreement> searchInchargeAgreement(SearchInchargeAgreementTO searchInchargeAgreementTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
        return inchargeAgreementService.searchInchargeAgreement(searchInchargeAgreementTO);
	}
	
	/**
	 * Gets the incharge agreement.
	 *
	 * @param inchargeAgreement the incharge agreement
	 * @return the incharge agreement
	 * @throws ServiceException the service exception
	 */
	public RegisterInchargeAgreementTO getInchargeAgreement(InchargeAgreement inchargeAgreement)throws ServiceException{
		return inchargeAgreementService.getInchargeAgreement(inchargeAgreement);
	}
	
	/**
	 * Gets the incharge agreement for filter.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idRolebuy the id rolebuy
	 * @param idRoleSell the id role sell
	 * @param idParticipant the id participant
	 * @param idParticipantAux the id participant aux
	 * @param idHolder the id holder
	 * @return the incharge agreement for filter
	 * @throws ServiceException the service exception
	 */
	public List<InchargeAgreement> getInchargeAgreementForFilter(Long idMechanism,Long idModality,boolean idRolebuy,boolean idRoleSell,Long idParticipant,Long idParticipantAux,Long idHolder)throws ServiceException{
		return inchargeAgreementService.getInchargeAgreementForFilter(idMechanism, idModality, idRolebuy,idRoleSell, idParticipant,idParticipantAux, idHolder);
	}
	
	/**
	 * Confirm Incharge Agreement.
	 *
	 * @param inchargeAgreement object Incharge Agreement
	 * @return object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.INCHARGE_AGREEMENT_CONFIRM)
	public InchargeAgreement confirmInchargeAgreementServiceFacade(InchargeAgreement inchargeAgreement) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());       
        return inchargeAgreementService.confirmInchargeAgreement(inchargeAgreement, loggerUser);
	}
	
	/**
	 * Reject Incharge Agreement.
	 *
	 * @param inchargeAgreement object Incharge Agreement
	 * @return object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.INCHARGE_AGREEMENT_REJECT)
	public InchargeAgreement rejectInchargeAgreementServiceFacade(InchargeAgreement inchargeAgreement) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());       
        return inchargeAgreementService.rejectInchargeAgreement(inchargeAgreement, loggerUser);
	}
	
	/**
	 * Cancel Incharge Agreement.
	 *
	 * @param inchargeAgreement object Incharge Agreement
	 * @return object Incharge Agreement
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.INCHARGE_AGREEMENT_CANCEL)
	public InchargeAgreement cancelInchargeAgreementServiceFacade(InchargeAgreement inchargeAgreement) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());       
        return inchargeAgreementService.cancelInchargeAgreement(inchargeAgreement, loggerUser);
	}
	
}
