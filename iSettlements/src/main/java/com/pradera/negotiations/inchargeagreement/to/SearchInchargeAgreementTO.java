package com.pradera.negotiations.inchargeagreement.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.InchargeAgreement;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchInchargeAgreementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchInchargeAgreementTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id incharge agreement. */
	private Long idInchargeAgreement;
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism> lstNegotiationMechanism;	
	
	/** The lst negotiation modality. */
	private List<NegotiationModality> lstNegotiationModality;
	
	/** The lst participant charged. */
	private List<Participant> lstParticipantTraders, lstParticipantCharged;
	
	/** The lst states. */
	private List<ParameterTable> lstStates;
	
	/** The nego modality selected. */
	private Long negoMechanismSelected, negoModalitySelected;
	
	/** The participant charged selected. */
	private Long participantTraderSelected, participantChargedSelected;
	
	/** The state selected. */
	private Integer  stateSelected;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The incharge type stock. */
	private boolean roleSelectedBuy = false,roleSelectedSell = false,inchargeTypeFunds = false,inchargeTypeStock = false;
	
	/** The lst incharge agreement. */
	GenericDataModel<InchargeAgreement> lstInchargeAgreement;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}
	
	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(
			List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
	
	/**
	 * Gets the lst participant traders.
	 *
	 * @return the lst participant traders
	 */
	public List<Participant> getLstParticipantTraders() {
		return lstParticipantTraders;
	}
	
	/**
	 * Sets the lst participant traders.
	 *
	 * @param lstParticipantTraders the new lst participant traders
	 */
	public void setLstParticipantTraders(List<Participant> lstParticipantTraders) {
		this.lstParticipantTraders = lstParticipantTraders;
	}
	
	/**
	 * Gets the lst participant charged.
	 *
	 * @return the lst participant charged
	 */
	public List<Participant> getLstParticipantCharged() {
		return lstParticipantCharged;
	}
	
	/**
	 * Sets the lst participant charged.
	 *
	 * @param lstParticipantCharged the new lst participant charged
	 */
	public void setLstParticipantCharged(List<Participant> lstParticipantCharged) {
		this.lstParticipantCharged = lstParticipantCharged;
	}
	
	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}
	
	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}
	
	/**
	 * Gets the nego mechanism selected.
	 *
	 * @return the nego mechanism selected
	 */
	public Long getNegoMechanismSelected() {
		return negoMechanismSelected;
	}
	
	/**
	 * Sets the nego mechanism selected.
	 *
	 * @param negoMechanismSelected the new nego mechanism selected
	 */
	public void setNegoMechanismSelected(Long negoMechanismSelected) {
		this.negoMechanismSelected = negoMechanismSelected;
	}
	
	/**
	 * Gets the nego modality selected.
	 *
	 * @return the nego modality selected
	 */
	public Long getNegoModalitySelected() {
		return negoModalitySelected;
	}
	
	/**
	 * Sets the nego modality selected.
	 *
	 * @param negoModalitySelected the new nego modality selected
	 */
	public void setNegoModalitySelected(Long negoModalitySelected) {
		this.negoModalitySelected = negoModalitySelected;
	}
	
	/**
	 * Gets the participant trader selected.
	 *
	 * @return the participant trader selected
	 */
	public Long getParticipantTraderSelected() {
		return participantTraderSelected;
	}
	
	/**
	 * Sets the participant trader selected.
	 *
	 * @param participantTraderSelected the new participant trader selected
	 */
	public void setParticipantTraderSelected(Long participantTraderSelected) {
		this.participantTraderSelected = participantTraderSelected;
	}
	
	/**
	 * Gets the participant charged selected.
	 *
	 * @return the participant charged selected
	 */
	public Long getParticipantChargedSelected() {
		return participantChargedSelected;
	}
	
	/**
	 * Sets the participant charged selected.
	 *
	 * @param participantChargedSelected the new participant charged selected
	 */
	public void setParticipantChargedSelected(Long participantChargedSelected) {
		this.participantChargedSelected = participantChargedSelected;
	}
	
	/**
	 * Gets the state selected.
	 *
	 * @return the state selected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}
	
	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the new state selected
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the lst incharge agreement.
	 *
	 * @return the lst incharge agreement
	 */
	public GenericDataModel<InchargeAgreement> getLstInchargeAgreement() {
		return lstInchargeAgreement;
	}
	
	/**
	 * Sets the lst incharge agreement.
	 *
	 * @param lstInchargeAgreement the new lst incharge agreement
	 */
	public void setLstInchargeAgreement(
			GenericDataModel<InchargeAgreement> lstInchargeAgreement) {
		this.lstInchargeAgreement = lstInchargeAgreement;
	}
	
	/**
	 * Gets the id incharge agreement.
	 *
	 * @return the id incharge agreement
	 */
	public Long getIdInchargeAgreement() {
		return idInchargeAgreement;
	}
	
	/**
	 * Sets the id incharge agreement.
	 *
	 * @param idInchargeAgreement the new id incharge agreement
	 */
	public void setIdInchargeAgreement(Long idInchargeAgreement) {
		this.idInchargeAgreement = idInchargeAgreement;
	}
	
	/**
	 * Checks if is role selected buy.
	 *
	 * @return true, if is role selected buy
	 */
	public boolean isRoleSelectedBuy() {
		return roleSelectedBuy;
	}
	
	/**
	 * Sets the role selected buy.
	 *
	 * @param roleSelectedBuy the new role selected buy
	 */
	public void setRoleSelectedBuy(boolean roleSelectedBuy) {
		this.roleSelectedBuy = roleSelectedBuy;
	}
	
	/**
	 * Checks if is role selected sell.
	 *
	 * @return true, if is role selected sell
	 */
	public boolean isRoleSelectedSell() {
		return roleSelectedSell;
	}
	
	/**
	 * Sets the role selected sell.
	 *
	 * @param roleSelectedSell the new role selected sell
	 */
	public void setRoleSelectedSell(boolean roleSelectedSell) {
		this.roleSelectedSell = roleSelectedSell;
	}
	
	/**
	 * Checks if is incharge type funds.
	 *
	 * @return true, if is incharge type funds
	 */
	public boolean isInchargeTypeFunds() {
		return inchargeTypeFunds;
	}
	
	/**
	 * Sets the incharge type funds.
	 *
	 * @param inchargeTypeFunds the new incharge type funds
	 */
	public void setInchargeTypeFunds(boolean inchargeTypeFunds) {
		this.inchargeTypeFunds = inchargeTypeFunds;
	}
	
	/**
	 * Checks if is incharge type stock.
	 *
	 * @return true, if is incharge type stock
	 */
	public boolean isInchargeTypeStock() {
		return inchargeTypeStock;
	}
	
	/**
	 * Sets the incharge type stock.
	 *
	 * @param inchargeTypeStock the new incharge type stock
	 */
	public void setInchargeTypeStock(boolean inchargeTypeStock) {
		this.inchargeTypeStock = inchargeTypeStock;
	}
	
	
}
