package com.pradera.negotiations.assignmentrequest.to;

import java.io.Serializable;


public class MechanismOperationForOpenTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String operationDate;
	private Long ballotNumber;
	private Long sequential;
	private String modalityText;
	private String idSecurityCodePk;
	private String currencyText;
	private String participantSeller;
	private String participantBuyer;
	private String stateText;
	private Integer stateOperation;
	
	/** para realizar un selected manual, por que el de primefaces tiene errores (no selecciona todas las paginas) */
	private boolean selected;
	
	// 1er constructor
	public MechanismOperationForOpenTO(){};
	
	// constructor para la query JPQL
	public MechanismOperationForOpenTO(Long id, String operationDate,
			Long ballotNumber, Long sequential, String modalityText,
			String idSecurityCodePk, String currencyText,
			String participantSeller, String participantBuyer, String stateText) {
		this.id = id;
		this.operationDate = operationDate;
		this.ballotNumber = ballotNumber;
		this.sequential = sequential;
		this.modalityText = modalityText;
		this.idSecurityCodePk = idSecurityCodePk;
		this.currencyText = currencyText;
		this.participantSeller = participantSeller;
		this.participantBuyer = participantBuyer;
		this.stateText = stateText;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(String operationDate) {
		this.operationDate = operationDate;
	}

	public Long getBallotNumber() {
		return ballotNumber;
	}

	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}

	public Long getSequential() {
		return sequential;
	}

	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}

	public String getModalityText() {
		return modalityText;
	}

	public void setModalityText(String modalityText) {
		this.modalityText = modalityText;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public String getCurrencyText() {
		return currencyText;
	}

	public void setCurrencyText(String currencyText) {
		this.currencyText = currencyText;
	}

	public String getParticipantSeller() {
		return participantSeller;
	}

	public void setParticipantSeller(String participantSeller) {
		this.participantSeller = participantSeller;
	}

	public String getParticipantBuyer() {
		return participantBuyer;
	}

	public void setParticipantBuyer(String participantBuyer) {
		this.participantBuyer = participantBuyer;
	}

	public String getStateText() {
		return stateText;
	}

	public void setStateText(String stateText) {
		this.stateText = stateText;
	}

	public Integer getStateOperation() {
		return stateOperation;
	}

	public void setStateOperation(Integer stateOperation) {
		this.stateOperation = stateOperation;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
