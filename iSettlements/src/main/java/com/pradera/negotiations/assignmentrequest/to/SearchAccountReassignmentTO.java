package com.pradera.negotiations.assignmentrequest.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchAccountReassignmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchAccountReassignmentTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The id currency. */
	private Integer idCurrency;
	
	/** The final date. */
	private Date finalDate;
	
	/** The id mechanism. */
	private Long idMechanism;
	
	/** The id modality. */
	private Long idModality;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The id state. */
	private Integer idState;
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The lst mechanism. */
	private List<NegotiationMechanism> lstMechanism;
	
	/** The lst modality. */
	private List<NegotiationModality> lstModality;
	
	/** The lst state. */
	private List<ParameterTable> lstState;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The lst account reassignment. */
	private GenericDataModel<AccountReassignmentTO> lstAccountReassignment;
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
	/**
	 * Gets the id currency.
	 *
	 * @return the id currency
	 */
	public Integer getIdCurrency() {
		return idCurrency;
	}
	
	/**
	 * Sets the id currency.
	 *
	 * @param idCurrency the new id currency
	 */
	public void setIdCurrency(Integer idCurrency) {
		this.idCurrency = idCurrency;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return idMechanism;
	}
	
	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}
	
	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}
	
	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}
	
	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}
	
	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}
	
	/**
	 * Gets the lst modality.
	 *
	 * @return the lst modality
	 */
	public List<NegotiationModality> getLstModality() {
		return lstModality;
	}
	
	/**
	 * Sets the lst modality.
	 *
	 * @param lstModality the new lst modality
	 */
	public void setLstModality(List<NegotiationModality> lstModality) {
		this.lstModality = lstModality;
	}
	
	/**
	 * Gets the id state.
	 *
	 * @return the id state
	 */
	public Integer getIdState() {
		return idState;
	}
	
	/**
	 * Sets the id state.
	 *
	 * @param idState the new id state
	 */
	public void setIdState(Integer idState) {
		this.idState = idState;
	}
	
	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	
	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the lst account reassignment.
	 *
	 * @return the lst account reassignment
	 */
	public GenericDataModel<AccountReassignmentTO> getLstAccountReassignment() {
		return lstAccountReassignment;
	}
	
	/**
	 * Sets the lst account reassignment.
	 *
	 * @param lstAccountReassignment the new lst account reassignment
	 */
	public void setLstAccountReassignment(
			GenericDataModel<AccountReassignmentTO> lstAccountReassignment) {
		this.lstAccountReassignment = lstAccountReassignment;
	}

}
