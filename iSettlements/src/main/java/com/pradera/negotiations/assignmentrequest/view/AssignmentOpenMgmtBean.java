package com.pradera.negotiations.assignmentrequest.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.AssignmentProcessOpen;
import com.pradera.model.negotiation.AssignmentProcessOpenFile;
import com.pradera.model.negotiation.MechanismOperationOpen;
import com.pradera.model.negotiation.type.AssignmentProcessOpenStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.assignmentrequest.to.AssignmentOpenModelTO;
import com.pradera.negotiations.assignmentrequest.to.MechanismOperationForOpenTO;
import com.pradera.settlements.utils.PropertiesConstants;

@LoggerCreateBean
@DepositaryWebBean

public class AssignmentOpenMgmtBean extends GenericBaseBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// issue 1161: navegacion establecida en el faces-config.xml
	public static final String PAGE_REGISTER="newAssignmentProcessOpen";
	public static final String PAGE_SEARCH="searchAssignmentProcessOpen";
	public static final String PAGE_VIEW="viewAssignmentProcessOpen";
	
	
	
	@Inject
	private UserInfo userInfo;
	
	
	@Inject
	private UserPrivilege userPrivilege;
	
	
	@Inject
	private transient PraderaLogger log;
	
	
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	
	@Inject
	protected Event<ExceptionToCatchEvent> excepcion;
	
	
	@EJB
	private AccountAssignmentFacade ejbAssignmentOpen;

	private AssignmentOpenModelTO modelTOReg;

	private AssignmentOpenModelTO modelTOSearch;

	private AssignmentOpenModelTO modelTOView;

	private boolean privilegeRegister;
	private boolean privilegeSearch;

	private boolean operationConfirm;
	private boolean operationReject;
	
	private AssignmentOpenDataModel searchAssignmentOpenDataModel = new AssignmentOpenDataModel();
	private List<AssignmentOpenModelTO> lstAssignmentOpenModelTO;
	
	private String fUploadFileTypesToOpenAssig = "jpg|jpeg|pdf|doc|docx|png|msg";
	private String fUploadsDisplayToOpenAssig = "*.jpg, *.jpeg, *.png, *.doc, *.docx, *.pdf, *.msg";
	private String docFileNameDisplay;
	
	@PostConstruct
	public void init(){
		loadSearch();
	}
	
	/** metodo para cargar la pantalla de busqueda(administracion) */
	public void loadSearch() {

		modelTOSearch = new AssignmentOpenModelTO();
		if (userPrivilege.getUserAcctions().isRegister()) {
			log.info("::: tiene privilegios de registro");
			privilegeRegister = true;
		}
		if (userPrivilege.getUserAcctions().isSearch()) {
			log.info("::: tiene privilegios de busqueda");
			privilegeSearch = true;
		}

		modelTOSearch.setLstState(getLstStateOpenGeneralAssignmet());

		modelTOSearch.setInitialDate(new Date());
		modelTOSearch.setFinalDate(new Date());
		
		lstAssignmentOpenModelTO = new ArrayList<>();
		searchAssignmentOpenDataModel = new AssignmentOpenDataModel();
	}
	
	/** realizando la busqueda segun los criterios seleccionados */
	public void btnSearch() {
		try {
			lstAssignmentOpenModelTO=ejbAssignmentOpen.lstAssignmentOpenModelTO(modelTOSearch);
			searchAssignmentOpenDataModel = new AssignmentOpenDataModel(lstAssignmentOpenModelTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** limpiando la pantalla de search */
	public void btnClearSearch() {
		loadSearch();
		JSFUtilities.resetViewRoot();
	}
	
	/** lleva al detalle (pantalla de vista) un registro mediante su link
	 * 
	 * @return */
	public String linkView(AssignmentOpenModelTO assignmentOpen) {
		modelTOView = (AssignmentOpenModelTO) assignmentOpen.clone();
		modelTOView.setLstState(getLstStateOpenGeneralAssignmet());

		// oteniendo los demas datos para la pantalla de vista
		try {
			AssignmentProcessOpen assignmentProcessOpenFull = ejbAssignmentOpen.findAssignmentProcessOpenFull(modelTOView.getId());
			
			if(Validations.validateIsNotNull(assignmentProcessOpenFull)){
				// obteniendo los archivos de justificacion
				if(Validations.validateIsNotNull(assignmentProcessOpenFull.getAssignmentProcessOpenFileList())){
					modelTOView.setLstAssignmentProcessOpenFile(assignmentProcessOpenFull.getAssignmentProcessOpenFileList());
				}
				// cargando las operaciones MCN que parasar a estado REGISTRADO a causa de la apertura de Cierre de Asignacion general
				if(Validations.validateIsNotNull(assignmentProcessOpenFull.getMechanismOperationOpenList())){
					for(MechanismOperationOpen mcnOpen :assignmentProcessOpenFull.getMechanismOperationOpenList()){
						modelTOView.getLstIdMechanismOperationToOpen().add(mcnOpen.getIdMechanismOperationFk().getIdMechanismOperationPk());
					}
					List<MechanismOperationForOpenTO> lstMcnOpen=ejbAssignmentOpen.lstMechanismOperationForOpenTO(modelTOView);
					modelTOView.setLstMechanismOperationForOpenTOAdded(lstMcnOpen);
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			log.info("::: se ha producido un error " + e.getMessage());
			modelTOView.setLstMechanismOperationForOpenTOAdded(null);
			modelTOView.setLstAssignmentProcessOpenFile(null);
		}

		renderFalseAllOperation();
		return PAGE_VIEW;
	}
	
	/** regresando a la pantalla de search desde la pantalla view
	 * 
	 * @return */
	public String backToSearchFromView() {
		btnSearch();
		return PAGE_SEARCH;
	}
	
	/** redirige a la pantalla search
	 * 
	 * @return */
	public String goPageSearch() {
		loadSearch();
		return PAGE_SEARCH;
	}
	
	/** redirige a la pagina de registro
	 * 
	 * @return */
	public String goPageRegister() {
		// primero verificamos si existe procesos para la fecha actual
		if(ejbAssignmentOpen.existAssigmentProcessClosedByDate(CommonsUtilities.currentDate())){
			loadRegiter();
			return PAGE_REGISTER;
		}else{
			Object[] arrBodyData = { CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.MSG_SEARCH_TO_REGISTER_VALIDATION,arrBodyData);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** limpiando la pantalla de registro */
	public void btnClearRegister() {
		loadRegiter();
		JSFUtilities.resetViewRoot();
	}
	
	/** cargando la pantalla de registro */
	public void loadRegiter() {
		modelTOReg = new AssignmentOpenModelTO();
		modelTOReg.setRegistryDate(CommonsUtilities.currentDate());
		
		try {
			// 1ro identificamos los AssignmentProcess de la fecha actual, 
			// 2do luego con eso obtenemos las op MCN canditadas para cambiar de estado (ASIGNADA -> REGISTRADA)
			// 3ro se filtra solo las mcn que esten en estado ASIGNADA
			List<MechanismOperationForOpenTO> lstMcn=new ArrayList<>();
			for(MechanismOperationForOpenTO mcn:ejbAssignmentOpen.lstMechanismOperationForOpenTO(modelTOReg)){
				if(mcn.getStateOperation().equals(MechanismOperationStateType.ASSIGNED_STATE.getCode())){
					lstMcn.add(mcn);
				}
			}
			modelTOReg.setLstMechanismOperationForOpenTO(lstMcn);
		} catch (ServiceException e) {
			e.printStackTrace();
			log.info("::: Se ha producido un error al obtener las operaciones MCN "+e.getMessage());
			modelTOReg.setLstMechanismOperationForOpenTO(new ArrayList<MechanismOperationForOpenTO>());
		}
		
		docFileNameDisplay = "";
	}
	
	/**
	 * issue 1161: metodo para realizar la carga de los archivos
	 * @param event
	 */
	public void documentAttachFile(FileUploadEvent event){
		
		String fDisplayName=getDisplayName(event.getFile());
		 
		 if(fDisplayName!=null){
			 docFileNameDisplay=fDisplayName;
		 }
		 
		 AssignmentProcessOpenFile assignmentProcessOpenFile = new AssignmentProcessOpenFile();
		 assignmentProcessOpenFile.setFilename(fUploadValidateFileNameLength(event.getFile().getFileName()));
		 assignmentProcessOpenFile.setDocumentFile(event.getFile().getContents());
	
		 insertFileItem(assignmentProcessOpenFile);
	}
	/** obtiene el maximo id(logico-transsient) de la lista de archivos */
	private int getMaxNumberFile(){
		int max=0;
		for(AssignmentProcessOpenFile apof:modelTOReg.getLstAssignmentProcessOpenFile()){
			if(apof.getNumFileUploaded()>max){
				max=apof.getNumFileUploaded();
			}
		}
		return max;
	}
	private void insertFileItem(AssignmentProcessOpenFile file){
		file.setNumFileUploaded(getMaxNumberFile()+1);
		modelTOReg.getLstAssignmentProcessOpenFile().add(file);
	}
	private String getDisplayName(UploadedFile fileUploaded){
		String fDisplayName = null;
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			fDisplayName = sbFileName.toString();
		}else{
			fDisplayName =  fileUploaded.getFileName();
		}
		return fDisplayName;
	}
	/**
	 * metodo para quitar un archivo de una tabla de archivos
	 * @param physicalCertFile
	 */
	public void removeFile(AssignmentProcessOpenFile file) {

		// no funciona, por alguna razon elimina el anterior
		//modelTOReg.getLstAssignmentProcessOpenFile().remove(file);
		
		// removiendo manualmente, buscando el indice correcto
		int index = 0;
		for(AssignmentProcessOpenFile apoFileToRemove:modelTOReg.getLstAssignmentProcessOpenFile()){
			if(apoFileToRemove.getNumFileUploaded() == file.getNumFileUploaded())
				break;
			index++;
		}
		modelTOReg.getLstAssignmentProcessOpenFile().remove(index);
		docFileNameDisplay = null;
	}

	/**
	 * metodo para seleccionar los registros MCN (manualmente porque el de priemefaces no pselecciona todas las paginas)
	 */
	public void selectAndUnselectAllRow(){		
		for(MechanismOperationForOpenTO row:modelTOReg.getLstMechanismOperationForOpenTO()){
			row.setSelected(modelTOReg.isSelectAllMCN());
		}
	}
	
	/** adicionada las Operaciones MCN que seran cambiadas de estado (ASIGNADA a REGISTRADO) */
	public void addMcnSelected(){
		StringBuilder operationsMcnWithChained=new StringBuilder();
		boolean firsAdd=true;
		String ballotInfoChained;
		for(MechanismOperationForOpenTO mcnSelected:modelTOReg.getLstMechanismOperationForOpenTO()){
			
			// TODO validando quel la opracion MCN no este en otra solicutd de apertura pendiente por modificar
			// pendiente por confirmar con el usuario
			
			if(mcnSelected.isSelected()){
				// evita que se agregue la misma operacion a la seccion de MCN agregados
				if(!modelTOReg.getLstMechanismOperationForOpenTOAdded().contains(mcnSelected)){
					modelTOReg.getLstMechanismOperationForOpenTOAdded().add(mcnSelected);
					
					// si la operacion tiene encadenamientos se la prepara para mostrar ala usuario
					ballotInfoChained=ballotNumberWithQuantityChains(mcnSelected);
					if(ballotInfoChained!=null){
						if(firsAdd){
							operationsMcnWithChained.append(ballotInfoChained);
							firsAdd=false;
						}else{
							operationsMcnWithChained.append(", ").append(ballotInfoChained);
						}
					}
				}	
			}
		}
		
		//verificando si se detectaron mcn con encadenaminetos, e informar al usuario de ello
		// SOLO ES IN MENSAJE INFORMATIVO
		if(operationsMcnWithChained.length()>0){
			Object[] arrBodyData = { operationsMcnWithChained };
			showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null,PropertiesConstants.MSG_ALERT_INFO_CHAINS_OPERATIONMCN, arrBodyData);
			JSFUtilities.showComponent("validationLocal");
		}else{
			JSFUtilities.hideComponent("validationLocal");
		}
		JSFUtilities.updateComponent("opnlDialogs");
	}
	private String ballotNumberWithQuantityChains(MechanismOperationForOpenTO mcnSlected){
		Integer quantityChained=ejbAssignmentOpen.quantityChainsOfMcn(mcnSlected.getId());
		if(quantityChained!=null && !quantityChained.equals(GeneralConstants.ZERO_VALUE_INTEGER)){
			Object[] arrBodyData = { mcnSlected.getBallotNumber().toString()+"/"+mcnSlected.getSequential().toString(), quantityChained };
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			return PropertiesUtilities.getMessage(locale, PropertiesConstants.MSG_ALERT_INFO_CHAINS_OPERATIONMCNDETAIL,arrBodyData);
		}
		return null;
	}
	public void removeMCNselected(MechanismOperationForOpenTO mcnSelected) {
		// si esto no funcoina debemos basarnor en el de custodia fisica, ahi tiene otra tecnica
		modelTOReg.getLstMechanismOperationForOpenTOAdded().remove(mcnSelected);
	}
	
	/** antes de guardar registro de la solicitud */
	public void beforeSaveRegister() {
		
		// validando si se subio al menos 1 archvio
		if(Validations.validateIsNull(modelTOReg.getLstAssignmentProcessOpenFile())
				|| modelTOReg.getLstAssignmentProcessOpenFile().isEmpty()
				|| Validations.validateIsNullOrNotPositive(modelTOReg.getLstAssignmentProcessOpenFile().size())){
			// lanzando mensaje que se requiere al menos 1 archvio
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_VALIDATION_REQUIRED_UPLOADFILE, null);
			JSFUtilities.showComponent("validationLocal");
			return;
		}
		
		// enviando la fecha al mensaje de confirmacion de registro
		Object[] arrBodyData = { CommonsUtilities.convertDatetoString(modelTOReg.getRegistryDate()) };
		
		if(modelTOReg.getLstMechanismOperationForOpenTOAdded().isEmpty() 
				|| modelTOReg.getLstMechanismOperationForOpenTOAdded().size() == 0){
			// mensaje de confirmacion cuando NO tiene operaciones MCN
			showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.MSG_CONFIRM_REGISTER_ASSIGNMENTOPEN_WITHOUTMCN, arrBodyData);
		}else{
			// mensaje de confirmacion cuando tiene operaciones MCN
			showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.MSG_CONFIRM_REGISTER_ASSIGNMENTOPEN, arrBodyData);
		}
		
		JSFUtilities.showComponent("cnfRegiterrr");
		
		JSFUtilities.hideComponent("validationLocal");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/** guardando en BBDD la solicutd de apertura de cierre de asigancion general */
	public void saveRegisterAssignmentOpen() {

		try {
			hideDialogsListener(null);

			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOReg.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOReg.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOReg.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeRegister().longValue());
			AssignmentProcessOpen assignmentProcessOpen = ejbAssignmentOpen.regiterAssignmentOpen(modelTOReg);

			// notificando al usuario correspondiente
//			String descriptionParticipat="";
//			for(Participant p:modelTOReg.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOReg.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { holderAnnulation.getIdAnnulationHolderPk().toString(), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_REGISTER.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOReg.getIdParticipantSelected(), parameters);
						
			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(assignmentProcessOpen.getIdAssignmentProcessOpenPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REGISTER_ASSIGNMENTOPEN, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** cargando datos para la pantalla de rechazo
	 * 
	 * @return */
	public String loadReject() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getId())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateRequest().equals(AssignmentProcessOpenStateType.REGISTRADO.getCode())) {
				
				modelTOView.setLstState(getLstStateOpenGeneralAssignmet());
				// oteniendo los demas datos para la pantalla de vista
				try {
					AssignmentProcessOpen assignmentProcessOpenFull = ejbAssignmentOpen.findAssignmentProcessOpenFull(modelTOView.getId());
					
					if(Validations.validateIsNotNull(assignmentProcessOpenFull)){
						// obteniendo los archivos de justificacion
						if(Validations.validateIsNotNull(assignmentProcessOpenFull.getAssignmentProcessOpenFileList())){
							modelTOView.setLstAssignmentProcessOpenFile(assignmentProcessOpenFull.getAssignmentProcessOpenFileList());
						}
						// cargando las operaciones MCN que parasar a estado REGISTRADO a causa de la apertura de Cierre de Asignacion general
						if(Validations.validateIsNotNull(assignmentProcessOpenFull.getMechanismOperationOpenList())){
							for(MechanismOperationOpen mcnOpen :assignmentProcessOpenFull.getMechanismOperationOpenList()){
								modelTOView.getLstIdMechanismOperationToOpen().add(mcnOpen.getIdMechanismOperationFk().getIdMechanismOperationPk());
							}
							List<MechanismOperationForOpenTO> lstMcnOpen=ejbAssignmentOpen.lstMechanismOperationForOpenTO(modelTOView);
							modelTOView.setLstMechanismOperationForOpenTOAdded(lstMcnOpen);
						}
					}
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error " + e.getMessage());
					modelTOView.setLstMechanismOperationForOpenTOAdded(null);
					modelTOView.setLstAssignmentProcessOpenFile(null);
				}
				renderBtnReject();
				return PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ASSIGNMENTOPEN_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ASSIGNMENTOPEN_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** confirmacion antes rechazar */
	public void beforeReject() {
		Object[] arrBodyData = { modelTOView.getId().toString() };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REJECT_ASSIGNMENTOPEN, arrBodyData);
		JSFUtilities.showComponent("cnfRegiterrr");
	}
	
	/** guardo en la BBDD cambio de estado rechazado */
	public void saveRejectAssignmentOpen() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeReject().longValue());
			
			modelTOView.setStateRequest(AssignmentProcessOpenStateType.RECHAZADO.getCode());
			AssignmentProcessOpen apo = ejbAssignmentOpen.changeStateAssignmentProcessOpen(modelTOView);

			// envio de notificacion que se realizo unA APROBACIONS de solicitud de apertura de canal
//			String descriptionParticipat="";
//			for(Participant p:modelTOView.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_APPROVAL.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(apo.getIdAssignmentProcessOpenPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REJECT_ASSIGNMENTOPEN, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** cargando la pantalla de confirmacion */
	public String loadConfirm() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getId())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateRequest().equals(AssignmentProcessOpenStateType.REGISTRADO.getCode())) {
				
				//Validar que la CONFIRMACION sea el dia de registro de la solicitud
				Date currentDate = CommonsUtilities.currentDate();
				if(modelTOView.getRegistryDate().compareTo(currentDate)!=0){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_REQUIRED_CURRENDDATE_FOR_CONFIRMED, null);
					JSFUtilities.updateComponent("opnlDialogs");
					JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
					return null;
				}
				
				modelTOView.setLstState(getLstStateOpenGeneralAssignmet());
				// oteniendo los demas datos para la pantalla de vista
				try {
					AssignmentProcessOpen assignmentProcessOpenFull = ejbAssignmentOpen.findAssignmentProcessOpenFull(modelTOView.getId());
					
					if(Validations.validateIsNotNull(assignmentProcessOpenFull)){
						// obteniendo los archivos de justificacion
						if(Validations.validateIsNotNull(assignmentProcessOpenFull.getAssignmentProcessOpenFileList())){
							modelTOView.setLstAssignmentProcessOpenFile(assignmentProcessOpenFull.getAssignmentProcessOpenFileList());
						}
						// cargando las operaciones MCN que parasar a estado REGISTRADO a causa de la apertura de Cierre de Asignacion general
						if(Validations.validateIsNotNull(assignmentProcessOpenFull.getMechanismOperationOpenList())){
							for(MechanismOperationOpen mcnOpen :assignmentProcessOpenFull.getMechanismOperationOpenList()){
								modelTOView.getLstIdMechanismOperationToOpen().add(mcnOpen.getIdMechanismOperationFk().getIdMechanismOperationPk());
							}
							List<MechanismOperationForOpenTO> lstMcnOpen=ejbAssignmentOpen.lstMechanismOperationForOpenTO(modelTOView);
							modelTOView.setLstMechanismOperationForOpenTOAdded(lstMcnOpen);
							
							// VALIDADO si aun existen encadenamientos pendientes por cancelar
							// si existen encadenamientos por confirmar, el sistema no debe permitir CONFIRMAR esta solicitud
							for(MechanismOperationForOpenTO mcnSelected:modelTOView.getLstMechanismOperationForOpenTOAdded()){
								// a requeriemiento del usuario solo pide informar que hay encadenamientos pendientes
								// y NO asi, especificar las papeletas y nro de encadenamientos.
								if(ballotNumberWithQuantityChains(mcnSelected)!=null){
									showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_REQUIRED_CANCELCHAINED, null);
									JSFUtilities.updateComponent("opnlDialogs");
									JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
									return null;
								}
							}
						}
					}
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error " + e.getMessage());
					modelTOView.setLstMechanismOperationForOpenTOAdded(null);
					modelTOView.setLstAssignmentProcessOpenFile(null);
				}
				renderBtnConfirm();
				return PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ASSIGNMENTOPEN_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_ASSIGNMENTOPEN_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** confirmacion antes de confirmar */
	public void beforeConfirm() {
		Object[] arrBodyData = { modelTOView.getId().toString() };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_CONFIRM_ASSIGNMENTOPEN, arrBodyData);
		JSFUtilities.showComponent("cnfRegiterrr");
	}
	
	/** guardando en la BBDD la confirmacion de la solicitud */
	public void saveConfirmAssignmentOpen() throws Exception {
		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeConfirm().longValue());
			
			modelTOView.setStateRequest(AssignmentProcessOpenStateType.CONFIRMADO.getCode());
			AssignmentProcessOpen apo = ejbAssignmentOpen.changeStateAssignmentProcessOpen(modelTOView);

			// envio de notificacion que se realizo unA APROBACIONS de solicitud de apertura de canal
//			String descriptionParticipat="";
//			for(Participant p:modelTOView.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_APPROVAL.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(apo.getIdAssignmentProcessOpenPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_CONFIRM_ASSIGNMENTOPEN, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** ocultando los dialogos */
	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		JSFUtilities.hideComponent("cnfRegiterrr");
		JSFUtilities.hideComponent("validationLocal");
		// JSFUtilities.hideComponent("detailRepresentativeDialog");
	}
	
	/** Ejecuata cualquier operacion en BBDD */
	public void saveOperationChangeState() throws Exception {
		if (operationConfirm) {
			saveConfirmAssignmentOpen();
			return;
		}
		if (operationReject) {
			saveRejectAssignmentOpen();
			return;
		}
	}
	// TODO metodos genericos para cargar las listas(combobox) necesarios para cualquier pantalla (registro, vista, search)
	public List<ParameterTable> getLstStateOpenGeneralAssignmet() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.OPEN_GENERAL_ASSIGNMENT.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	// TODO metodos para el renderizados de los botones de las operaciones (confirmado, rechzado)
	public void renderBtnConfirm() {
		renderFalseAllOperation();
		operationConfirm = true;
	}

	public void renderBtnReject() {
		renderFalseAllOperation();
		operationReject = true;
	}

	private void renderFalseAllOperation() {
		operationConfirm = false;
		operationReject = false;
	}

	public boolean isPrivilegeRegister() {
		return privilegeRegister;
	}

	public void setPrivilegeRegister(boolean privilegeRegister) {
		this.privilegeRegister = privilegeRegister;
	}

	public boolean isPrivilegeSearch() {
		return privilegeSearch;
	}

	public void setPrivilegeSearch(boolean privilegeSearch) {
		this.privilegeSearch = privilegeSearch;
	}

	public AssignmentOpenModelTO getModelTOSearch() {
		return modelTOSearch;
	}

	public void setModelTOSearch(AssignmentOpenModelTO modelTOSearch) {
		this.modelTOSearch = modelTOSearch;
	}

	public List<AssignmentOpenModelTO> getLstAssignmentOpenModelTO() {
		return lstAssignmentOpenModelTO;
	}

	public void setLstAssignmentOpenModelTO(List<AssignmentOpenModelTO> lstAssignmentOpenModelTO) {
		this.lstAssignmentOpenModelTO = lstAssignmentOpenModelTO;
	}

	public AssignmentOpenModelTO getModelTOReg() {
		return modelTOReg;
	}

	public void setModelTOReg(AssignmentOpenModelTO modelTOReg) {
		this.modelTOReg = modelTOReg;
	}

	public AssignmentOpenModelTO getModelTOView() {
		return modelTOView;
	}

	public void setModelTOView(AssignmentOpenModelTO modelTOView) {
		this.modelTOView = modelTOView;
	}

	public AssignmentOpenDataModel getSearchAssignmentOpenDataModel() {
		return searchAssignmentOpenDataModel;
	}

	public void setSearchAssignmentOpenDataModel(AssignmentOpenDataModel searchAssignmentOpenDataModel) {
		this.searchAssignmentOpenDataModel = searchAssignmentOpenDataModel;
	}

	public String getfUploadFileTypesToOpenAssig() {
		return fUploadFileTypesToOpenAssig;
	}

	public void setfUploadFileTypesToOpenAssig(String fUploadFileTypesToOpenAssig) {
		this.fUploadFileTypesToOpenAssig = fUploadFileTypesToOpenAssig;
	}

	public String getfUploadsDisplayToOpenAssig() {
		return fUploadsDisplayToOpenAssig;
	}

	public void setfUploadsDisplayToOpenAssig(String fUploadsDisplayToOpenAssig) {
		this.fUploadsDisplayToOpenAssig = fUploadsDisplayToOpenAssig;
	}

	public String getDocFileNameDisplay() {
		return docFileNameDisplay;
	}

	public void setDocFileNameDisplay(String docFileNameDisplay) {
		this.docFileNameDisplay = docFileNameDisplay;
	}

	public boolean isOperationConfirm() {
		return operationConfirm;
	}

	public void setOperationConfirm(boolean operationConfirm) {
		this.operationConfirm = operationConfirm;
	}

	public boolean isOperationReject() {
		return operationReject;
	}

	public void setOperationReject(boolean operationReject) {
		this.operationReject = operationReject;
	}
	
	
}