package com.pradera.negotiations.assignmentrequest.to;

import java.math.BigDecimal;

import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAssignmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountAssignmentTO extends MechanismOperationTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id settlement account operation. */
	private Long idSettlementAccountOperation;
	
	/** The id holder account operation. */
	private Long idHolderAccountOperation;
	
	/** The role. */
	private Integer role;
	
	/** The account stock quantity. */
	private BigDecimal accountStockQuantity;
	
	/** The account amount. */
	private BigDecimal accountAmount;
	
	/** The incharge state. */
	private Integer inchargeState;
	
	/** The id holder account. */
	private Long idHolderAccount;
	
	/** The incharge state description. */
	private String inchargeStateDescription;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/* (non-Javadoc)
	 * @see com.pradera.negotiations.accountassignment.to.MechanismOperationTO#getRole()
	 */
	public Integer getRole() {
		return role;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.negotiations.accountassignment.to.MechanismOperationTO#setRole(java.lang.Integer)
	 */
	public void setRole(Integer role) {
		this.role = role;
	}
	
	/**
	 * Gets the account stock quantity.
	 *
	 * @return the account stock quantity
	 */
	public BigDecimal getAccountStockQuantity() {
		return accountStockQuantity;
	}
	
	/**
	 * Sets the account stock quantity.
	 *
	 * @param accountStockQuantity the new account stock quantity
	 */
	public void setAccountStockQuantity(BigDecimal accountStockQuantity) {
		this.accountStockQuantity = accountStockQuantity;
	}
	
	/**
	 * Gets the account amount.
	 *
	 * @return the account amount
	 */
	public BigDecimal getAccountAmount() {
		return accountAmount;
	}
	
	/**
	 * Sets the account amount.
	 *
	 * @param accountAmount the new account amount
	 */
	public void setAccountAmount(BigDecimal accountAmount) {
		this.accountAmount = accountAmount;
	}
	
	/**
	 * Gets the id holder account operation.
	 *
	 * @return the id holder account operation
	 */
	public Long getIdHolderAccountOperation() {
		return idHolderAccountOperation;
	}
	
	/**
	 * Sets the id holder account operation.
	 *
	 * @param idHolderAccountOperation the new id holder account operation
	 */
	public void setIdHolderAccountOperation(Long idHolderAccountOperation) {
		this.idHolderAccountOperation = idHolderAccountOperation;
	}
	
	/**
	 * Gets the incharge state.
	 *
	 * @return the incharge state
	 */
	public Integer getInchargeState() {
		return inchargeState;
	}
	
	/**
	 * Sets the incharge state.
	 *
	 * @param inchargeState the new incharge state
	 */
	public void setInchargeState(Integer inchargeState) {
		this.inchargeState = inchargeState;
	}
	
	/**
	 * Gets the id settlement account operation.
	 *
	 * @return the id settlement account operation
	 */
	public Long getIdSettlementAccountOperation() {
		return idSettlementAccountOperation;
	}
	
	/**
	 * Sets the id settlement account operation.
	 *
	 * @param idSettlementAccountOperation the new id settlement account operation
	 */
	public void setIdSettlementAccountOperation(Long idSettlementAccountOperation) {
		this.idSettlementAccountOperation = idSettlementAccountOperation;
	}
	
	/**
	 * Gets the id holder account.
	 *
	 * @return the id holder account
	 */
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}
	
	/**
	 * Sets the id holder account.
	 *
	 * @param idHolderAccount the new id holder account
	 */
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}
	
	/**
	 * Gets the incharge state description.
	 *
	 * @return the incharge state description
	 */
	public String getInchargeStateDescription() {
		return inchargeStateDescription;
	}
	
	/**
	 * Sets the incharge state description.
	 *
	 * @param inchargeStateDescription the new incharge state description
	 */
	public void setInchargeStateDescription(String inchargeStateDescription) {
		this.inchargeStateDescription = inchargeStateDescription;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	
}
