package com.pradera.negotiations.assignmentrequest.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchAssignmentRequestTO.
 *
 * @author Pradera Technologies
 */
public class SearchAssignmentRequestTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism> lstNegotiationMechanism;	
	
	/** The lst negotiation modality. */
	private List<NegotiationModality> lstNegotiationModality;
	
	/** The lst participant charged. */
	private List<Participant> lstParticipantTraders, lstParticipantCharged;
	
	/** The lst states. */
	private List<ParameterTable> lstDateType, lstRoles, lstStates;
	
	/** The nego modality selected. */
	private Long negoMechanismSelected, negoModalitySelected;
	
	/** The participant charged selected. */
	private Long participantTraderSelected, participantChargedSelected;
	
	/** The state selected. */
	private Integer roleSelected, dateType, stateSelected;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The lst account assignments. */
	GenericDataModel<AccountAssignmentTO> lstAccountAssignments;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	/** The holder. */
	private Holder holder;
	
	/**  The Security. */
	private Security	security;
	
	/** Ind Role Buy. */
	private boolean roleBuy;
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}
	
	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(
			List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
	
	/**
	 * Gets the lst participant traders.
	 *
	 * @return the lst participant traders
	 */
	public List<Participant> getLstParticipantTraders() {
		return lstParticipantTraders;
	}
	
	/**
	 * Sets the lst participant traders.
	 *
	 * @param lstParticipantTraders the new lst participant traders
	 */
	public void setLstParticipantTraders(List<Participant> lstParticipantTraders) {
		this.lstParticipantTraders = lstParticipantTraders;
	}
	
	/**
	 * Gets the lst participant charged.
	 *
	 * @return the lst participant charged
	 */
	public List<Participant> getLstParticipantCharged() {
		return lstParticipantCharged;
	}
	
	/**
	 * Sets the lst participant charged.
	 *
	 * @param lstParticipantCharged the new lst participant charged
	 */
	public void setLstParticipantCharged(List<Participant> lstParticipantCharged) {
		this.lstParticipantCharged = lstParticipantCharged;
	}
	
	/**
	 * Gets the lst date type.
	 *
	 * @return the lst date type
	 */
	public List<ParameterTable> getLstDateType() {
		return lstDateType;
	}
	
	/**
	 * Sets the lst date type.
	 *
	 * @param lstDateType the new lst date type
	 */
	public void setLstDateType(List<ParameterTable> lstDateType) {
		this.lstDateType = lstDateType;
	}
	
	/**
	 * Gets the lst roles.
	 *
	 * @return the lst roles
	 */
	public List<ParameterTable> getLstRoles() {
		return lstRoles;
	}
	
	/**
	 * Sets the lst roles.
	 *
	 * @param lstRoles the new lst roles
	 */
	public void setLstRoles(List<ParameterTable> lstRoles) {
		this.lstRoles = lstRoles;
	}
	
	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}
	
	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}
	
	/**
	 * Gets the nego mechanism selected.
	 *
	 * @return the nego mechanism selected
	 */
	public Long getNegoMechanismSelected() {
		return negoMechanismSelected;
	}
	
	/**
	 * Sets the nego mechanism selected.
	 *
	 * @param negoMechanismSelected the new nego mechanism selected
	 */
	public void setNegoMechanismSelected(Long negoMechanismSelected) {
		this.negoMechanismSelected = negoMechanismSelected;
	}
	
	/**
	 * Gets the nego modality selected.
	 *
	 * @return the nego modality selected
	 */
	public Long getNegoModalitySelected() {
		return negoModalitySelected;
	}
	
	/**
	 * Sets the nego modality selected.
	 *
	 * @param negoModalitySelected the new nego modality selected
	 */
	public void setNegoModalitySelected(Long negoModalitySelected) {
		this.negoModalitySelected = negoModalitySelected;
	}
	
	/**
	 * Gets the participant trader selected.
	 *
	 * @return the participant trader selected
	 */
	public Long getParticipantTraderSelected() {
		return participantTraderSelected;
	}
	
	/**
	 * Sets the participant trader selected.
	 *
	 * @param participantTraderSelected the new participant trader selected
	 */
	public void setParticipantTraderSelected(Long participantTraderSelected) {
		this.participantTraderSelected = participantTraderSelected;
	}
	
	/**
	 * Gets the participant charged selected.
	 *
	 * @return the participant charged selected
	 */
	public Long getParticipantChargedSelected() {
		return participantChargedSelected;
	}
	
	/**
	 * Sets the participant charged selected.
	 *
	 * @param participantChargedSelected the new participant charged selected
	 */
	public void setParticipantChargedSelected(Long participantChargedSelected) {
		this.participantChargedSelected = participantChargedSelected;
	}
	
	/**
	 * Gets the role selected.
	 *
	 * @return the role selected
	 */
	public Integer getRoleSelected() {
		return roleSelected;
	}
	
	/**
	 * Sets the role selected.
	 *
	 * @param roleSelected the new role selected
	 */
	public void setRoleSelected(Integer roleSelected) {
		this.roleSelected = roleSelected;
	}
	
	/**
	 * Gets the date type.
	 *
	 * @return the date type
	 */
	public Integer getDateType() {
		return dateType;
	}
	
	/**
	 * Sets the date type.
	 *
	 * @param dateType the new date type
	 */
	public void setDateType(Integer dateType) {
		this.dateType = dateType;
	}
	
	/**
	 * Gets the state selected.
	 *
	 * @return the state selected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}
	
	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the new state selected
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the lst account assignments.
	 *
	 * @return the lst account assignments
	 */
	public GenericDataModel<AccountAssignmentTO> getLstAccountAssignments() {
		return lstAccountAssignments;
	}
	
	/**
	 * Sets the lst account assignments.
	 *
	 * @param lstAccountAssignments the new lst account assignments
	 */
	public void setLstAccountAssignments(
			GenericDataModel<AccountAssignmentTO> lstAccountAssignments) {
		this.lstAccountAssignments = lstAccountAssignments;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Checks if is role buy.
	 *
	 * @return the roleBuy
	 */
	public boolean isRoleBuy() {
		return roleBuy;
	}
	
	/**
	 * Sets the role buy.
	 *
	 * @param roleBuy the roleBuy to set
	 */
	public void setRoleBuy(boolean roleBuy) {
		this.roleBuy = roleBuy;
	}
	
	
	
}
