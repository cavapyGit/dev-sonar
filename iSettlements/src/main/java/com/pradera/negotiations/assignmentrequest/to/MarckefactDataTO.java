package com.pradera.negotiations.assignmentrequest.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class MarckefactDataTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal priceHM;
	private BigDecimal rateHM;
	private Date dateHM;
	private Long stockQuantityHM;
	public BigDecimal getPriceHM() {
		return priceHM;
	}
	public void setPriceHM(BigDecimal priceHM) {
		this.priceHM = priceHM;
	}
	public BigDecimal getRateHM() {
		return rateHM;
	}
	public void setRateHM(BigDecimal rateHM) {
		this.rateHM = rateHM;
	}
	public Date getDateHM() {
		return dateHM;
	}
	public void setDateHM(Date dateHM) {
		this.dateHM = dateHM;
	}
	public Long getStockQuantityHM() {
		return stockQuantityHM;
	}
	public void setStockQuantityHM(Long stockQuantityHM) {
		this.stockQuantityHM = stockQuantityHM;
	}
}
