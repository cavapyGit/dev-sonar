package com.pradera.negotiations.assignmentrequest.to;

import java.util.Date;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.constants.Constants;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountReassignmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AccountReassignmentTO extends MechanismOperationTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id reassignment state. */
	private Integer idReassignmentState;
	
	/** The reassignment state description. */
	private String reassignmentStateDescription;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The participant memonic. */
	private String participantMemonic;
	
	/** The participant description. */
	private String participantDescription;
	
	/** The participant code nemonic. */
	private String participantCodeNemonic;
	
	/** The id reassignment request. */
	private Long idReassignmentRequest;
	
	/** The register date. */
	private Date registerDate;
	
	/** The confirm date. */
	private Date confirmDate;
	
	/** The reject date. */
	private Date rejectDate;
	
	/** The role. */
	private Integer role;
	
	/** The request rason. */
	private Integer requestRason;
	
	/** The desc request reason. */
	private String descRequestReason;
	
	/**
	 * Gets the participant code nemonic.
	 *
	 * @return the participant code nemonic
	 */
	public String getParticipantCodeNemonic(){
    	if(Validations.validateIsNullOrEmpty(participantCodeNemonic)){
    		if(Validations.validateIsNotNullAndPositive(idParticipant) && Validations.validateIsNotNull(participantMemonic)){
    			StringBuffer sb = new StringBuffer();
        		sb.append(participantMemonic);
        		sb.append(Constants.DASH);
        		sb.append(idParticipant);    		
        		participantCodeNemonic = sb.toString();
        	}
    	}
    	return participantCodeNemonic;
    }

	/**
	 * Gets the id reassignment state.
	 *
	 * @return the id reassignment state
	 */
	public Integer getIdReassignmentState() {
		return idReassignmentState;
	}

	/**
	 * Sets the id reassignment state.
	 *
	 * @param idReassignmentState the new id reassignment state
	 */
	public void setIdReassignmentState(Integer idReassignmentState) {
		this.idReassignmentState = idReassignmentState;
	}

	/**
	 * Gets the reassignment state description.
	 *
	 * @return the reassignment state description
	 */
	public String getReassignmentStateDescription() {
		return reassignmentStateDescription;
	}

	/**
	 * Sets the reassignment state description.
	 *
	 * @param reassignmentStateDescription the new reassignment state description
	 */
	public void setReassignmentStateDescription(String reassignmentStateDescription) {
		this.reassignmentStateDescription = reassignmentStateDescription;
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the participant memonic.
	 *
	 * @return the participant memonic
	 */
	public String getParticipantMemonic() {
		return participantMemonic;
	}

	/**
	 * Sets the participant memonic.
	 *
	 * @param participantMemonic the new participant memonic
	 */
	public void setParticipantMemonic(String participantMemonic) {
		this.participantMemonic = participantMemonic;
	}

	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return participantDescription;
	}

	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}

	/**
	 * Sets the participant code nemonic.
	 *
	 * @param participantCodeNemonic the new participant code nemonic
	 */
	public void setParticipantCodeNemonic(String participantCodeNemonic) {
		this.participantCodeNemonic = participantCodeNemonic;
	}

	/**
	 * Gets the id reassignment request.
	 *
	 * @return the id reassignment request
	 */
	public Long getIdReassignmentRequest() {
		return idReassignmentRequest;
	}

	/**
	 * Sets the id reassignment request.
	 *
	 * @param idReassignmentRequest the new id reassignment request
	 */
	public void setIdReassignmentRequest(Long idReassignmentRequest) {
		this.idReassignmentRequest = idReassignmentRequest;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/* (non-Javadoc)
	 * @see com.pradera.negotiations.accountassignment.to.MechanismOperationTO#getRole()
	 */
	public Integer getRole() {
		return role;
	}

	/* (non-Javadoc)
	 * @see com.pradera.negotiations.accountassignment.to.MechanismOperationTO#setRole(java.lang.Integer)
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the request rason.
	 *
	 * @return the request rason
	 */
	public Integer getRequestRason() {
		return requestRason;
	}

	/**
	 * Sets the request rason.
	 *
	 * @param requestRason the new request rason
	 */
	public void setRequestRason(Integer requestRason) {
		this.requestRason = requestRason;
	}

	/**
	 * Gets the description request reason.
	 *
	 * @return the description request reason
	 */
	public String getDescRequestReason() {
		return descRequestReason;
	}

	/**
	 * Sets the desc request reason.
	 *
	 * @param descRequestReason the new desc request reason
	 */
	public void setDescRequestReason(String descRequestReason) {
		this.descRequestReason = descRequestReason;
	}

	
}
