package com.pradera.negotiations.assignmentrequest.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.AssignmentRequestMotiveRejectType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.assignmentrequest.to.AccountAssignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAssignmentRequestTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManagementInChargeMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ManagementInChargeMgmtBean extends GenericBaseBean implements Serializable {
	
	/** The account assignment facade. */
	@Inject
	AccountAssignmentFacade accountAssignmentFacade;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The search assignment request to. */
	private SearchAssignmentRequestTO searchAssignmentRequestTO;
	
	/** The selected account assignment. */
	private AccountAssignmentTO selectedAccountAssignment;
	
	/** The holder account operation. */
	private HolderAccountOperation holderAccountOperation;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;
	
	/** The holder. */
	private Holder holder;
	
	/** The lst holder account. */
	private List<HolderAccount> lstHolderAccount;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The bl view. */
	private boolean blConfirm, blReject, blConfirmMassive, blRejectMassive, blView;
	
	/** The bl role sell. */
	private boolean blRoleSell;
	
	/** The bl depositary. */
	private boolean blDepositary;
	
	/** The bl market fact. */
	private boolean blMarketFact;
	
	/** The bl fixed income. */
	private boolean blFixedIncome;
	
	/** The account operation market fact. */
	private AccountOperationMarketFact accountOperationMarketFact;
	
	/** The Constant VIEW_MAPPING. */
	private final static String VIEW_MAPPING = "viewAssignmentRequest";
	
	/** The map parameters. */
	private Map<Integer, String> mapParameters = new HashMap<Integer, String>();
	
	/** The lst reject motive. */
	private List<ParameterTable> lstRejectMotive;
	
	/** The bl others motive. */
	private boolean blOthersMotive;
	
	/** The list account assignment to. */
	private List<AccountAssignmentTO> listAccountAssignmentTO;
	
	/** The mode selection single. */
	private Boolean modeSelectionSingle;
	
	/** The mode selection multiple. */
	private Boolean modeSelectionMultiple;
	
	/** The bl help market fact. */
	private Boolean blHelpMarketFact;
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try{
			searchAssignmentRequestTO = new SearchAssignmentRequestTO();
			searchAssignmentRequestTO.setInitialDate(getCurrentSystemDate());
			searchAssignmentRequestTO.setEndDate(getCurrentSystemDate());	
			searchAssignmentRequestTO.setDateType(GeneralConstants.ZERO_VALUE_INTEGER);
			searchAssignmentRequestTO.setLstNegotiationMechanism(accountAssignmentFacade.getListNegotiationMechanism());
			searchAssignmentRequestTO.setHolder(new Holder());
			searchAssignmentRequestTO.setSecurity(new Security());
			searchAssignmentRequestTO.setStateSelected(HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode());
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				blDepositary = true;
			}else{
				searchAssignmentRequestTO.setParticipantChargedSelected(userInfo.getUserAccountSession().getParticipantCode());
				List<Participant> lstTemp = new ArrayList<Participant>();
				Participant participant = accountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				lstTemp.add(participant);
				searchAssignmentRequestTO.setLstParticipantCharged(lstTemp);
				//searchAssignmentRequestTO.setLstNegotiationMechanism(accountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));
			}
			fillCombos();
			fillMaps();
			showPrivilegeButtons();
			searchAssignmentRequestTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
			changeNegotiationMechanism();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	

	/**
	 * Change negotiation mechanism.
	 */
	public void changeNegotiationMechanism(){
		try {
			JSFUtilities.resetViewRoot();
			cleanResults();
			searchAssignmentRequestTO.setLstNegotiationModality(null);
			searchAssignmentRequestTO.setNegoModalitySelected(null);
			searchAssignmentRequestTO.setParticipantTraderSelected(null);
			searchAssignmentRequestTO.setLstParticipantTraders(null);
			if(blDepositary){
				searchAssignmentRequestTO.setLstParticipantCharged(null);
				searchAssignmentRequestTO.setParticipantChargedSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAssignmentRequestTO.getNegoMechanismSelected())){
				if(blDepositary)
					searchAssignmentRequestTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(searchAssignmentRequestTO.getNegoMechanismSelected(), null, ComponentConstant.ONE));
				else
					searchAssignmentRequestTO.setLstNegotiationModality(accountAssignmentFacade.getListNegotiationModality(searchAssignmentRequestTO.getNegoMechanismSelected(),
																													searchAssignmentRequestTO.getParticipantChargedSelected(),ComponentConstant.ONE));
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		selectedAccountAssignment = null;
		searchAssignmentRequestTO.setBlNoResult(false);
		searchAssignmentRequestTO.setLstAccountAssignments(null);
		listAccountAssignmentTO=new ArrayList<>();
	}
	
	/**
	 * Change negotiation modality.
	 */
	public void changeNegotiationModality(){
		try {
			cleanResults();
			JSFUtilities.resetComponent("frmAssignmentRequest:cboIncharge");
			searchAssignmentRequestTO.setLstParticipantTraders(null);
			searchAssignmentRequestTO.setParticipantTraderSelected(null);
			if(blDepositary){
				searchAssignmentRequestTO.setLstParticipantCharged(null);
				searchAssignmentRequestTO.setParticipantChargedSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAssignmentRequestTO.getNegoModalitySelected())){
				if(blDepositary){					
					searchAssignmentRequestTO.setLstParticipantCharged(accountAssignmentFacade.getListParticipantsForNegoModality(searchAssignmentRequestTO.getNegoMechanismSelected()
																															, searchAssignmentRequestTO.getNegoModalitySelected(), null, ComponentConstant.ONE));
				}
				searchAssignmentRequestTO.setLstParticipantTraders(accountAssignmentFacade.getListParticipantsForNegoModality(searchAssignmentRequestTO.getNegoMechanismSelected()
						, searchAssignmentRequestTO.getNegoModalitySelected(), ParticipantType.DIRECT.getCode(),null));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To reject the In charge Operation.
	 */
	public void beforeAction(){		
		if(blConfirm){
			if(Validations.validateIsNullOrEmpty(holderAccount)
					|| Validations.validateIsNullOrEmpty(holderAccount.getIdHolderAccountPk())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(blMarketFact && ParticipantRoleType.SELL.getCode().equals(holderAccountOperation.getRole())
					&& Validations.validateListIsNullOrEmpty(holderAccountOperation.getAccountOperationMarketFacts())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY, 
														holderAccount.getAccountNumber().toString()));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_ASK_CONFIRM));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}
		else if(blReject){
			JSFUtilities.resetViewRoot();
			blOthersMotive = false;
			holderAccountOperation.setMotiveReject(null);
			holderAccountOperation.setComments(null);
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT),null);			
		}
		
	}
	
	/**
	 * Do reject.
	 */
	public void doReject(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT), 
				PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_ASK_REJECT));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	 /**
 	 * Validate back.
 	 */
 	public void validateBack(){			
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountOperation.getMotiveReject())){			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getGenericMessage("conf.msg.back"));
			JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
		}else{
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		}
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		if(blConfirm){
			confirmIncharge();
		}else if(blReject){
			rejectIncharge();
		}else if(blConfirmMassive){
			confirmMassiveIncharge();
		}else if(blRejectMassive){
			rejectMassiveIncharge();
		}
	}

	/**
	 * To clear the search criteria.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		searchAssignmentRequestTO.setDateType(GeneralConstants.ZERO_VALUE_INTEGER);
		searchAssignmentRequestTO.setInitialDate(getCurrentSystemDate());
		searchAssignmentRequestTO.setEndDate(getCurrentSystemDate());
		searchAssignmentRequestTO.setParticipantTraderSelected(null);
		searchAssignmentRequestTO.setLstParticipantTraders(null);
		searchAssignmentRequestTO.setLstNegotiationModality(null);
		searchAssignmentRequestTO.setNegoModalitySelected(null);
		searchAssignmentRequestTO.setNegoMechanismSelected(null);
		if(blDepositary){
			searchAssignmentRequestTO.setLstParticipantCharged(null);
			searchAssignmentRequestTO.setParticipantChargedSelected(null);
		}
		searchAssignmentRequestTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
		changeNegotiationMechanism();
	}
	
	/**
	 * To get Search Results.
	 */
	@LoggerAuditWeb
	public void searchInChargeRequest(){
		try{
			cleanResults();
			searchAssignmentRequestTO.setBlNoResult(true);
			List<AccountAssignmentTO> lstHolderAccountOperations = accountAssignmentFacade.searchInchargeRequests(searchAssignmentRequestTO);				
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountOperations)){
				searchAssignmentRequestTO.setLstAccountAssignments(new GenericDataModel<AccountAssignmentTO>(lstHolderAccountOperations));					
				searchAssignmentRequestTO.setBlNoResult(false);
				if(validateMultipleIncharge(lstHolderAccountOperations,searchAssignmentRequestTO)&&
						lstHolderAccountOperations.size()>1){
					holder=new Holder();
					holder.setIdHolderPk(lstHolderAccountOperations.get(0).getIdHolderPk());
					blRoleSell=ParticipantRoleType.SELL.getCode().equals(lstHolderAccountOperations.get(0).getRole())?Boolean.TRUE:Boolean.FALSE;
					blFixedIncome=InstrumentType.FIXED_INCOME.getCode().equals(lstHolderAccountOperations.get(0).getInstrumentType())?Boolean.TRUE:Boolean.FALSE;
					
					modeSelectionMultiple=Boolean.TRUE;
					modeSelectionSingle=Boolean.FALSE;
					
				}else{
					modeSelectionMultiple=Boolean.FALSE;
					modeSelectionSingle=Boolean.TRUE;
				}
			}
		}
		catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}
	}
		
	
	
	/**
	 * To load the  Management In charge Confirm page or Reject Page based request  .
	 *
	 * @return String
	 */
	public String validateConfirm(){
		try {
			Boolean oneSelectionMultiple=modeSelectionMultiple&&
					(Validations.validateListIsNotNullAndNotEmpty(listAccountAssignmentTO)&&
							listAccountAssignmentTO.size()==1);
			
			if(modeSelectionSingle||oneSelectionMultiple){
				if(oneSelectionMultiple){
					selectedAccountAssignment=listAccountAssignmentTO.get(0);
				}
				if(Validations.validateIsNullOrEmpty(selectedAccountAssignment)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_RECORD_NOT_SELECTED));
					JSFUtilities.showSimpleValidationDialog();		
				}else{
					if(HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode().equals(selectedAccountAssignment.getInchargeState())){
						blConfirm = true;
						blReject = false;
						blConfirmMassive = false;
						blRejectMassive = false;
						blView = false;
						blMarketFact = false;
						blFixedIncome = false;
						holder = new Holder();
						lstHolderAccount = null;
						holderAccount = null;
						holderAccountOperation = accountAssignmentFacade.getHolderAccountOperationDetail(selectedAccountAssignment.getIdHolderAccountOperation());
						mechanismOperation = accountAssignmentFacade.getMechanismOperation(selectedAccountAssignment.getIdMechanismOperationPk());
						mechanismOperation.setCurrencyDescription(mapParameters.get(mechanismOperation.getCashSettlementCurrency()));
//						holderAccountOperation=new HolderAccountOperation();
						holderAccountOperation.setHolderAccount(accountAssignmentFacade.findHolderAccount(selectedAccountAssignment.getIdHolderAccount()));
						
						holderAccountOperation.setAccountOperationMarketFacts(null);
						if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())
								&& ParticipantRoleType.SELL.getCode().equals(holderAccountOperation.getRole())){
							blMarketFact = true;
							accountOperationMarketFact = new AccountOperationMarketFact();
							accountOperationMarketFact.setMarketDate(getCurrentSystemDate());
							accountOperationMarketFact.setHolderAccountOperation(holderAccountOperation);
							if(InstrumentType.FIXED_INCOME.getCode().equals(selectedAccountAssignment.getInstrumentType()))
								blFixedIncome = true;
						}
						return VIEW_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_INVALID_CONFIRM));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}else if(modeSelectionMultiple){
				
				if(Validations.validateListIsNotNullAndNotEmpty(listAccountAssignmentTO)){

					blConfirm = false;
					blReject = false;
					blConfirmMassive = true;
					blRejectMassive = false;
					blHelpMarketFact=Boolean.FALSE;
					
					BigDecimal stockQuantity= BigDecimal.ZERO;
					Participant participantInCharge=new Participant(searchAssignmentRequestTO.getParticipantChargedSelected());
					accountOperationMarketFact=new AccountOperationMarketFact();
					holderAccountOperation=new HolderAccountOperation();				
					
					for (AccountAssignmentTO accountAssignmentTO : listAccountAssignmentTO) {
						stockQuantity=CommonsUtilities.addTwoDecimal(stockQuantity, accountAssignmentTO.getAccountStockQuantity(), GeneralConstants.ONE_VALUE_INTEGER);
					}
					
					holderAccountOperation.setStockQuantity(stockQuantity);
					holderAccountOperation.setInchargeStockParticipant(participantInCharge);
					validateHolder();
						
					JSFUtilities.executeJavascriptFunction("PF('widgInchargeAssignmentConfirm').show();");
					JSFUtilities.updateComponent("opnlListHolderAccount");
					JSFUtilities.updateComponent("opnlMarketFactReg");
					
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_RECORD_NOT_SELECTED));
					JSFUtilities.showSimpleValidationDialog();	
				}
				
			}
			
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		try {
			
			
			if(modeSelectionSingle){

				if(Validations.validateIsNullOrEmpty(selectedAccountAssignment)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_RECORD_NOT_SELECTED));
					JSFUtilities.showSimpleValidationDialog();		
				}else{
					if(HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode().equals(selectedAccountAssignment.getInchargeState())){
						blConfirm = false;
						blReject = true;
						blConfirmMassive = false;
						blRejectMassive = false;
						blView = false;
						blMarketFact = false;
						blFixedIncome = false;
						holderAccountOperation = accountAssignmentFacade.getHolderAccountOperationDetail(selectedAccountAssignment.getIdHolderAccountOperation());
						mechanismOperation = accountAssignmentFacade.getMechanismOperation(selectedAccountAssignment.getIdMechanismOperationPk());
						mechanismOperation.setCurrencyDescription(mapParameters.get(mechanismOperation.getCashSettlementCurrency()));
						blOthersMotive = false;
						if(Validations.validateListIsNullOrEmpty(lstRejectMotive)){
							lstRejectMotive = new ArrayList<ParameterTable>();
							ParameterTableTO parameterTableTO = new ParameterTableTO();
							parameterTableTO.setMasterTableFk(MasterTableType.ASSIGNMENT_REQUEST_MOTIVE_REJECT.getCode());				
							lstRejectMotive = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
						}
						return VIEW_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_INVALID_REJECT));
						JSFUtilities.showSimpleValidationDialog();
					}
				}	
			
			}else if(modeSelectionMultiple){
				
				if(Validations.validateListIsNotNullAndNotEmpty(listAccountAssignmentTO)){
					blConfirm = false;
					blReject = false;
					blConfirmMassive = false;
					blRejectMassive = true;

					holderAccountOperation=new HolderAccountOperation();
					JSFUtilities.updateComponent("outPnlDialogs");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_MASSIVE_ASK_REJECT));
					JSFUtilities.executeJavascriptFunction("PF('cnfWRejectDialog').show()");
					
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_RECORD_NOT_SELECTED));
					JSFUtilities.showSimpleValidationDialog();	
				}
				
				
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}
		return null;
	}	
	
	/**
	 * Adds the market fact.
	 */
	public void addMarketFact(){
		if(blFixedIncome){
			if(Validations.validateIsNullOrEmpty(accountOperationMarketFact.getMarketRate())
					&& Validations.validateIsNullOrEmpty(accountOperationMarketFact.getOperationQuantity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtRate", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtOperationQuantity", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				return;
			}else if(Validations.validateIsNullOrEmpty(accountOperationMarketFact.getMarketRate())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtRate", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				return;
			}else if(Validations.validateIsNullOrEmpty(accountOperationMarketFact.getOperationQuantity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtOperationQuantity", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				return;
			}
		}else{
			if(Validations.validateIsNullOrEmpty(accountOperationMarketFact.getMarketPrice())
					&& Validations.validateIsNullOrEmpty(accountOperationMarketFact.getOperationQuantity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtPrice", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtOperationQuantity", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				return;
			}else if(Validations.validateIsNullOrEmpty(accountOperationMarketFact.getMarketPrice())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtPrice", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				return;
			}else if(Validations.validateIsNullOrEmpty(accountOperationMarketFact.getOperationQuantity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.addContextMessage("frmManagementInchargeRequest:txtOperationQuantity", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				return;
			}
		}

		if(Validations.validateListIsNullOrEmpty(holderAccountOperation.getAccountOperationMarketFacts())){
			holderAccountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
		}
		
		BigDecimal totalQuantityMarketFact = accountOperationMarketFact.getOperationQuantity();
		for(AccountOperationMarketFact accountMarketFact:holderAccountOperation.getAccountOperationMarketFacts()){
			totalQuantityMarketFact = totalQuantityMarketFact.add(accountMarketFact.getOperationQuantity());
		}
		
		if(totalQuantityMarketFact.compareTo(holderAccountOperation.getStockQuantity()) > 0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_QUANTITY_EXCEEDED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		holderAccountOperation.getAccountOperationMarketFacts().add(accountOperationMarketFact);
		
		accountOperationMarketFact = new AccountOperationMarketFact();
		accountOperationMarketFact.setHolderAccountOperation(holderAccountOperation);
		accountOperationMarketFact.setMarketDate(getCurrentSystemDate());
	}
	
	/**
	 * Removes the market fact.
	 *
	 * @param accountOperationMarketFact the account operation market fact
	 */
	public void removeMarketFact(AccountOperationMarketFact accountOperationMarketFact){
		
		holderAccountOperation.getAccountOperationMarketFacts().remove(accountOperationMarketFact);
		
	}
	
	/**
	 * Show incharge.
	 *
	 * @param accountAssignmentTO the account assignment to
	 * @return the string
	 */
	public String showIncharge(AccountAssignmentTO accountAssignmentTO){
		JSFUtilities.hideGeneralDialogues();
		try {
			blOthersMotive = false;
			blConfirm = false;
			blReject = false;
			blConfirmMassive = false;
			blRejectMassive = false;
			blView = false;
			blMarketFact = false;
			blFixedIncome = false;
			holderAccount = new HolderAccount();
			holder = new Holder();
			lstHolderAccount = null;
			holderAccountOperation = accountAssignmentFacade.getHolderAccountOperationDetail(accountAssignmentTO.getIdHolderAccountOperation());
			mechanismOperation = accountAssignmentFacade.getMechanismOperation(accountAssignmentTO.getIdMechanismOperationPk());
			mechanismOperation.setCurrencyDescription(mapParameters.get(mechanismOperation.getCashSettlementCurrency()));
//			holderAccountOperation=new HolderAccountOperation();
			if(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode().equals(holderAccountOperation.getInchargeState())){				
				holderAccount = accountAssignmentFacade.findHolderAccount(holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
				for(HolderAccountDetail holderAccountDetail:holderAccount.getHolderAccountDetails()){
					if(BooleanType.YES.getCode().equals(holderAccountDetail.getIndRepresentative())){
						holder = holderAccountDetail.getHolder();
						break;
					}						
				}
				if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact()) && ComponentConstant.SALE_ROLE.equals(holderAccountOperation.getRole())){
					blMarketFact = true;
					accountOperationMarketFact = new AccountOperationMarketFact();
					if(InstrumentType.FIXED_INCOME.getCode().equals(accountAssignmentTO.getInstrumentType()))
						blFixedIncome = true;
				}
				blView = true;
			}
			if(Validations.validateListIsNullOrEmpty(lstRejectMotive)){
				lstRejectMotive = new ArrayList<ParameterTable>();
				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.ASSIGNMENT_REQUEST_MOTIVE_REJECT.getCode());				
				lstRejectMotive = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
			if(AssignmentRequestMotiveRejectType.OTHERS_MOTIVES.getCode().equals(holderAccountOperation.getMotiveReject())){
				blOthersMotive = true;
			}
				
			return VIEW_MAPPING;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Validate holder.
	 */
	public void validateHolder(){
		try {
			lstHolderAccount = null;
			holderAccount = null;
			if(Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk())){
				if(HolderStateType.BLOCKED.getCode().equals(holder.getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_HOLDER_BLOCKED));
					JSFUtilities.showSimpleValidationDialog();
					holder = new Holder();
					return;
				}
				if(Validations.validateIsNullOrEmpty(holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					holder = new Holder();				
				}else{
					List<HolderAccount> lstResult = accountAssignmentFacade.findHolderAccounts(holder.getIdHolderPk()
																			, holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						for(HolderAccount holderAccount:lstResult){
							holderAccount.setAccountDescription(mapParameters.get(holderAccount.getAccountType()));
						}
						lstHolderAccount = lstResult;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						holder = new Holder();	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Validate same holder.
	 */
	public void validateSameHolder(){
		List<HolderAccountDetail> lstHolderAccountDetail = holderAccount.getHolderAccountDetails();
		
		List<HolderAccountDetail> lstOriginHolderAccountDetail = holderAccountOperation.getHolderAccount().getHolderAccountDetails();
		if(lstHolderAccountDetail.size() == lstOriginHolderAccountDetail.size()){
			Map<Long, Integer> mpHolder = new HashMap<Long, Integer>();
			Map<Long, Integer> mpOriginHolder = new HashMap<Long, Integer>();
			for(HolderAccountDetail holderAccountDetail:lstHolderAccountDetail)
				mpHolder.put(holderAccountDetail.getHolder().getIdHolderPk(), BooleanType.YES.getCode());
			for(HolderAccountDetail holderAccountDetail:lstOriginHolderAccountDetail)
				mpOriginHolder.put(holderAccountDetail.getHolder().getIdHolderPk(), BooleanType.YES.getCode());
			for(Long keyHolder:mpHolder.keySet()){
				if(Validations.validateIsNullOrEmpty(mpOriginHolder.get(keyHolder))){
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_NOT_SAME_HOLDERS));
					holderAccount  = null;
					return;
				}
			}
		}else{
			JSFUtilities.showSimpleValidationDialog();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_NOT_SAME_HOLDERS));
			holderAccount  = null;
		}
	}
	
	/**
	 * Selected radio holder account.
	 */
	public void selectedRadioHolderAccount(){
		holderAccountOperation.setHolderAccount(holderAccount);
		if(Validations.validateIsNotNull(holderMarketFactBalance)){
			holderMarketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		}
		blHelpMarketFact=Boolean.TRUE;
	}
	
	/**
	 * Confirm incharge.
	 */
	@LoggerAuditWeb
	private void confirmIncharge(){
		try{
			accountAssignmentFacade.confirmInchargeRequest(holderAccountOperation, holderAccount);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_SUCCESS_CONFIRM));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");			
			searchInChargeRequest();			
		}catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}
	
	/**
	 * Reject incharge.
	 */
	@LoggerAuditWeb
	private void rejectIncharge(){
		try{
			accountAssignmentFacade.rejectInchargeRequest(holderAccountOperation);			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_SUCCESS_REJECT));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			searchInChargeRequest();			
		}catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}
	
	
	
	
	/**
	 * Confirm massive incharge.
	 */
	@LoggerAuditWeb
	private void confirmMassiveIncharge(){
		try{
			
			
			List<AccountOperationMarketFact> listAccountOperationMarketFact = holderAccountOperation.getAccountOperationMarketFacts();
			Map<Long,List<AccountOperationMarketFact>> mapMarketFact=accountAssignmentFacade.calculateMarketFact(listAccountAssignmentTO,
					listAccountOperationMarketFact,blFixedIncome);
			
			for (AccountAssignmentTO accountAssignmentTO : listAccountAssignmentTO) {
				
				holderAccountOperation.setIdHolderAccountOperationPk(accountAssignmentTO.getIdHolderAccountOperation());
				holderAccountOperation.setRole(accountAssignmentTO.getRole());
				if(blRoleSell){
					holderAccountOperation.setAccountOperationMarketFacts(mapMarketFact.get(accountAssignmentTO.getIdHolderAccountOperation()));	
				}
				
				accountAssignmentFacade.confirmInchargeMassiveRequest(holderAccountOperation,holderAccountOperation.getHolderAccount());
			}
			
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_MASSIVE_REQUEST_SUCCESS_CONFIRM));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			searchInChargeRequest();			
		}catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}	
	
	/**
	 * Reject massive incharge.
	 */
	@LoggerAuditWeb
	private void rejectMassiveIncharge(){
		try{
			
			for (AccountAssignmentTO accountAssignmentTO : listAccountAssignmentTO) {
				holderAccountOperation.setIdHolderAccountOperationPk(accountAssignmentTO.getIdHolderAccountOperation());
				holderAccountOperation.setRole(accountAssignmentTO.getRole());
				accountAssignmentFacade.rejectInchargeRequest(holderAccountOperation);
			}
					
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_SUCCESS_REJECT));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			searchInChargeRequest();			
		}catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}	
	
	/**
	 * Change reject motive.
	 */
	public void changeRejectMotive(){
		blOthersMotive = false;
		holderAccountOperation.setComments(null);
		if(AssignmentRequestMotiveRejectType.OTHERS_MOTIVES.getCode().equals(holderAccountOperation.getMotiveReject())){
			blOthersMotive = true;
		}
	}	
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnConfirmView(true);		
		privilegeComponent.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(ParticipantRoleType.BUY.getCode());
		paramTab.setParameterName(ParticipantRoleType.BUY.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(ParticipantRoleType.SELL.getCode());
		paramTab.setParameterName(ParticipantRoleType.SELL.getValue());
		lstTemp.add(paramTab);
		searchAssignmentRequestTO.setLstRoles(lstTemp);
		lstTemp = new ArrayList<ParameterTable>();
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(GeneralConstants.ZERO_VALUE_INTEGER);
		paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.operationDate"));
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(GeneralConstants.ONE_VALUE_INTEGER);
		paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.cashSettlementDate"));
		lstTemp.add(paramTab);
		searchAssignmentRequestTO.setLstDateType(lstTemp);
		//Estados de encargo
		ParameterTableTO parameterTableTO = new ParameterTableTO();			
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INCHARGE_SETTLEMENT_STATE.getCode());
		searchAssignmentRequestTO.setLstStates(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
	}
	
	/**
	 * Fill maps.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillMaps() throws ServiceException{
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.CURRENCY.getCode());
			masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());			
		parameterTableTO.setLstMasterTableFk(masterPks);
		
		List<ParameterTable> lst = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt.getParameterName());
		}
		
		//Estados de encargo
		for(ParameterTable paramTab:searchAssignmentRequestTO.getLstStates()){
			mapParameters.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
			
	}
	
	/**
	 * idSecurityCodePk.
	 * Role
	 * CUI
	 * State(Registered).
	 * Range Date
	 *
	 * @param lstHolderAccountOperations the lst holder account operations
	 * @param searchAssignmentRequestTO the search assignment request to
	 * @return the boolean
	 */
	public Boolean validateMultipleIncharge(List<AccountAssignmentTO> lstHolderAccountOperations, SearchAssignmentRequestTO searchAssignmentRequestTO){
		
			
		for (AccountAssignmentTO accountAssignmentTO : lstHolderAccountOperations) {
			
			if(!HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode().equals(accountAssignmentTO.getInchargeState())){
				return false;
			 }
			
			for (AccountAssignmentTO accountAssignmentTO2 : lstHolderAccountOperations) {
				 if(!(accountAssignmentTO.getIdHolderPk().equals(accountAssignmentTO2.getIdHolderPk()))){
					 return false;
				 }
				 if(accountAssignmentTO.getRole().equals(ComponentConstant.SALE_ROLE) && 
				  !(accountAssignmentTO.getIdSecurityCodePk().equals(accountAssignmentTO2.getIdSecurityCodePk()))){
					 return false;
				 }
				 if(!(accountAssignmentTO.getRole().equals(accountAssignmentTO2.getRole()))){
					 return false;
				 }
			}
		}
		
		return true;
	}
	
	
	/**
	 * Validate confirm massive.
	 */
	public void validateConfirmMassive(){

		BigDecimal quantityMarketFact=BigDecimal.ZERO;

		if(!(Validations.validateIsNotNull(holderAccountOperation.getHolderAccount())&&
				Validations.validateIsNotNullAndNotEmpty(holderAccountOperation.getHolderAccount().getIdHolderAccountPk()))){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(blRoleSell){
			if(Validations.validateListIsNullOrEmpty(holderAccountOperation.getAccountOperationMarketFacts())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY,
										holderAccount.getAccountNumber().toString()));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			for (AccountOperationMarketFact accOperationMarketFact : holderAccountOperation.getAccountOperationMarketFacts()) {
				quantityMarketFact=CommonsUtilities.addTwoDecimal(quantityMarketFact, accOperationMarketFact.getOperationQuantity(), GeneralConstants.ONE_VALUE_INTEGER);
			}
			
			if(quantityMarketFact.compareTo(holderAccountOperation.getStockQuantity())!=0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SAME_QUANTITY_OPERATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
				PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_REQUEST_MASSIVE_ASK_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	/**
	 * Show market fact ui.
	 */
	public void showMarketFactUI(){
		MarketFactBalanceHelpTO marketFactBalanceTO = new MarketFactBalanceHelpTO();
		marketFactBalanceTO.setSecurityCodePk(searchAssignmentRequestTO.getSecurity().getIdSecurityCodePk());
		marketFactBalanceTO.setHolderAccountPk(holderAccount.getIdHolderAccountPk());
		marketFactBalanceTO.setParticipantPk(searchAssignmentRequestTO.getParticipantChargedSelected());
		marketFactBalanceTO.setUiComponentName("balancemarket1");
		marketFactBalanceTO.setMarketFacBalances(holderMarketFactBalance!=null?holderMarketFactBalance.getMarketFacBalances(): new ArrayList<MarketFactDetailHelpTO>());
		marketFactBalanceTO.setIndTotalBalance(BooleanType.YES.getCode());
		
		MarketFactBalanceHelpTO marketBalanceFinded= accountAssignmentFacade.findMarketBalance(marketFactBalanceTO);
		
		if(Validations.validateListIsNotNullAndNotEmpty(marketBalanceFinded.getMarketFacBalances())){
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalanceTO);
		}else{
			/**Alert que no tiene hechos de mercado*/
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_MARKEFACT_PARTICIPANT));
			JSFUtilities.showSimpleValidationDialog();
			
		}
		 
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalanceForBan(){
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		marketFactBalance.getMarketFacBalances();
		List<AccountOperationMarketFact> listAccountOperationMarketFact= new ArrayList<AccountOperationMarketFact>();
		AccountOperationMarketFact accountOperationMarketFact=new AccountOperationMarketFact();
		for(MarketFactDetailHelpTO mkfDetTO: marketFactBalance.getMarketFacBalances()){
			accountOperationMarketFact.setOperationQuantity(mkfDetTO.getEnteredBalance());
			accountOperationMarketFact.setMarketDate(mkfDetTO.getMarketDate());
			accountOperationMarketFact.setMarketPrice(mkfDetTO.getMarketPrice());
			accountOperationMarketFact.setMarketRate(mkfDetTO.getMarketRate());
			listAccountOperationMarketFact.add(accountOperationMarketFact);
		}
		
		holderAccountOperation.setAccountOperationMarketFacts(listAccountOperationMarketFact);
	}
	
	/**
	 * Change role assignment.
	 */
	public void changeRoleAssignment(){

		searchAssignmentRequestTO.setSecurity(new Security());
		if(ParticipantRoleType.BUY.getCode().equals(searchAssignmentRequestTO.getRoleSelected())){
			searchAssignmentRequestTO.setRoleBuy(true);
		}else{
			searchAssignmentRequestTO.setRoleBuy(false);
		}
		
		
	}
	
	/**
	 * Gets the role description.
	 *
	 * @param role the role
	 * @return the role description
	 */
	public String getRoleDescription(Integer role){
		return ParticipantRoleType.get(role).getValue();
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}
	
	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}
	
	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}
	
	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}
	
	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}
	
	/**
	 * Checks if is bl confirm massive.
	 *
	 * @return the blConfirmMassive
	 */
	public boolean isBlConfirmMassive() {
		return blConfirmMassive;
	}


	/**
	 * Sets the bl confirm massive.
	 *
	 * @param blConfirmMassive the blConfirmMassive to set
	 */
	public void setBlConfirmMassive(boolean blConfirmMassive) {
		this.blConfirmMassive = blConfirmMassive;
	}


	/**
	 * Checks if is bl reject massive.
	 *
	 * @return the blRejectMassive
	 */
	public boolean isBlRejectMassive() {
		return blRejectMassive;
	}


	/**
	 * Sets the bl reject massive.
	 *
	 * @param blRejectMassive the blRejectMassive to set
	 */
	public void setBlRejectMassive(boolean blRejectMassive) {
		this.blRejectMassive = blRejectMassive;
	}


	/**
	 * Checks if is bl role sell.
	 *
	 * @return the blRoleSell
	 */
	public boolean isBlRoleSell() {
		return blRoleSell;
	}


	/**
	 * Sets the bl role sell.
	 *
	 * @param blRoleSell the blRoleSell to set
	 */
	public void setBlRoleSell(boolean blRoleSell) {
		this.blRoleSell = blRoleSell;
	}


	/**
	 * Checks if is bl depositary.
	 *
	 * @return true, if is bl depositary
	 */
	public boolean isBlDepositary() {
		return blDepositary;
	}
	
	/**
	 * Sets the bl depositary.
	 *
	 * @param blDepositary the new bl depositary
	 */
	public void setBlDepositary(boolean blDepositary) {
		this.blDepositary = blDepositary;
	}
	
	/**
	 * Gets the search assignment request to.
	 *
	 * @return the search assignment request to
	 */
	public SearchAssignmentRequestTO getSearchAssignmentRequestTO() {
		return searchAssignmentRequestTO;
	}
	
	/**
	 * Sets the search assignment request to.
	 *
	 * @param searchAssignmentRequestTO the new search assignment request to
	 */
	public void setSearchAssignmentRequestTO(
			SearchAssignmentRequestTO searchAssignmentRequestTO) {
		this.searchAssignmentRequestTO = searchAssignmentRequestTO;
	}

	/**
	 * Checks if is bl view.
	 *
	 * @return true, if is bl view
	 */
	public boolean isBlView() {
		return blView;
	}
	
	/**
	 * Sets the bl view.
	 *
	 * @param blView the new bl view
	 */
	public void setBlView(boolean blView) {
		this.blView = blView;
	}
	
	/**
	 * Checks if is bl market fact.
	 *
	 * @return true, if is bl market fact
	 */
	public boolean isBlMarketFact() {
		return blMarketFact;
	}
	
	/**
	 * Sets the bl market fact.
	 *
	 * @param blMarketFact the new bl market fact
	 */
	public void setBlMarketFact(boolean blMarketFact) {
		this.blMarketFact = blMarketFact;
	}
	
	/**
	 * Checks if is bl fixed income.
	 *
	 * @return true, if is bl fixed income
	 */
	public boolean isBlFixedIncome() {
		return blFixedIncome;
	}
	
	/**
	 * Sets the bl fixed income.
	 *
	 * @param blFixedIncome the new bl fixed income
	 */
	public void setBlFixedIncome(boolean blFixedIncome) {
		this.blFixedIncome = blFixedIncome;
	}
	
	/**
	 * Gets the lst reject motive.
	 *
	 * @return the lst reject motive
	 */
	public List<ParameterTable> getLstRejectMotive() {
		return lstRejectMotive;
	}
	
	/**
	 * Sets the lst reject motive.
	 *
	 * @param lstRejectMotive the new lst reject motive
	 */
	public void setLstRejectMotive(List<ParameterTable> lstRejectMotive) {
		this.lstRejectMotive = lstRejectMotive;
	}
	
	/**
	 * Checks if is bl others motive.
	 *
	 * @return true, if is bl others motive
	 */
	public boolean isBlOthersMotive() {
		return blOthersMotive;
	}
	
	/**
	 * Sets the bl others motive.
	 *
	 * @param blOthersMotive the new bl others motive
	 */
	public void setBlOthersMotive(boolean blOthersMotive) {
		this.blOthersMotive = blOthersMotive;
	}


	/**
	 * Gets the selected account assignment.
	 *
	 * @return the selected account assignment
	 */
	public AccountAssignmentTO getSelectedAccountAssignment() {
		return selectedAccountAssignment;
	}


	/**
	 * Sets the selected account assignment.
	 *
	 * @param selectedAccountAssignment the new selected account assignment
	 */
	public void setSelectedAccountAssignment(
			AccountAssignmentTO selectedAccountAssignment) {
		this.selectedAccountAssignment = selectedAccountAssignment;
	}


	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getholderAccountOperation() {
		return holderAccountOperation;
	}


	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setholderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}


	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}


	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}


	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}


	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}


	/**
	 * Gets the account operation market fact.
	 *
	 * @return the account operation market fact
	 */
	public AccountOperationMarketFact getAccountOperationMarketFact() {
		return accountOperationMarketFact;
	}


	/**
	 * Sets the account operation market fact.
	 *
	 * @param accountOperationMarketFact the new account operation market fact
	 */
	public void setAccountOperationMarketFact(
			AccountOperationMarketFact accountOperationMarketFact) {
		this.accountOperationMarketFact = accountOperationMarketFact;
	}


	/**
	 * Gets the list account assignment to.
	 *
	 * @return the listAccountAssignmentTO
	 */
	public List<AccountAssignmentTO> getListAccountAssignmentTO() {
		return listAccountAssignmentTO;
	}


	/**
	 * Sets the list account assignment to.
	 *
	 * @param listAccountAssignmentTO the listAccountAssignmentTO to set
	 */
	public void setListAccountAssignmentTO(
			List<AccountAssignmentTO> listAccountAssignmentTO) {
		this.listAccountAssignmentTO = listAccountAssignmentTO;
	}


	/**
	 * Gets the mode selection single.
	 *
	 * @return the modeSelectionSingle
	 */
	public Boolean getModeSelectionSingle() {
		return modeSelectionSingle;
	}


	/**
	 * Sets the mode selection single.
	 *
	 * @param modeSelectionSingle the modeSelectionSingle to set
	 */
	public void setModeSelectionSingle(Boolean modeSelectionSingle) {
		this.modeSelectionSingle = modeSelectionSingle;
	}


	/**
	 * Gets the mode selection multiple.
	 *
	 * @return the modeSelectionMultiple
	 */
	public Boolean getModeSelectionMultiple() {
		return modeSelectionMultiple;
	}


	/**
	 * Sets the mode selection multiple.
	 *
	 * @param modeSelectionMultiple the modeSelectionMultiple to set
	 */
	public void setModeSelectionMultiple(Boolean modeSelectionMultiple) {
		this.modeSelectionMultiple = modeSelectionMultiple;
	}


	/**
	 * Gets the bl help market fact.
	 *
	 * @return the blHelpMarketFact
	 */
	public Boolean getBlHelpMarketFact() {
		return blHelpMarketFact;
	}


	/**
	 * Sets the bl help market fact.
	 *
	 * @param blHelpMarketFact the blHelpMarketFact to set
	 */
	public void setBlHelpMarketFact(Boolean blHelpMarketFact) {
		this.blHelpMarketFact = blHelpMarketFact;
	}


	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holderMarketFactBalance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}


	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the holderMarketFactBalance to set
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}



}