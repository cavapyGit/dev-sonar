package com.pradera.negotiations.assignmentrequest.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ParameterToRevertBalances implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idParticipant;
	private Long idHolderAccount;
	private String idSecurotyCodePk;
	private Long stockQuantity;
	private Date date;
	private Integer instrumentType;
	private Long idMechanismOperation;
	/** 1: Compra, 2: Venta */
	private Integer role;
	
	private Long idHolderAccountOperationPk;
	private List<MarckefactDataTO> lstMarckefactDataTO=new ArrayList<>();
	public Long getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}
	public String getIdSecurotyCodePk() {
		return idSecurotyCodePk;
	}
	public void setIdSecurotyCodePk(String idSecurotyCodePk) {
		this.idSecurotyCodePk = idSecurotyCodePk;
	}
	public Long getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(Long stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}
	public Integer getRole() {
		return role;
	}
	public void setRole(Integer role) {
		this.role = role;
	}
	public Long getIdHolderAccountOperationPk() {
		return idHolderAccountOperationPk;
	}
	public void setIdHolderAccountOperationPk(Long idHolderAccountOperationPk) {
		this.idHolderAccountOperationPk = idHolderAccountOperationPk;
	}
	public List<MarckefactDataTO> getLstMarckefactDataTO() {
		return lstMarckefactDataTO;
	}
	public void setLstMarckefactDataTO(List<MarckefactDataTO> lstMarckefactDataTO) {
		this.lstMarckefactDataTO = lstMarckefactDataTO;
	} 

}
