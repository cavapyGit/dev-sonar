package com.pradera.negotiations.assignmentrequest.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.negotiations.assignmentrequest.to.AssignmentOpenModelTO;

public class AssignmentOpenDataModel extends ListDataModel<AssignmentOpenModelTO> implements SelectableDataModel<AssignmentOpenModelTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// primer constructor
	public AssignmentOpenDataModel (){}
	
	// segundo constructor
	public AssignmentOpenDataModel(List<AssignmentOpenModelTO> lst){
		super(lst);
	}

	@Override
	public AssignmentOpenModelTO getRowData(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getRowKey(AssignmentOpenModelTO arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
