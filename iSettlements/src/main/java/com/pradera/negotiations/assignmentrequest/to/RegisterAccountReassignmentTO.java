package com.pradera.negotiations.assignmentrequest.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.ReassignmentRequestDetail;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterAccountReassignmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterAccountReassignmentTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The id currency. */
	private Long idCurrency;
	
	/** The id mechanism. */
	private Long idMechanism;
	
	/** The id modality. */
	private Long idModality;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The lst mechanism. */
	private List<NegotiationMechanism> lstMechanism;
	
	/** The lst modality. */
	private List<NegotiationModality> lstModality;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The lst settlement schema. */
	private List<SelectItem> lstSettlementSchema;
	
	/** The lst operation state. */
	private List<ParameterTable> lstOperationState;
	
	/** The lst roles. */
	private List<SelectItem> lstRoles; 
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The holder. */
	private Holder holder = new Holder();
	
	/** The lst holder account. */
	private List<HolderAccount> lstHolderAccount;
	
	/** The cross operation. */
	private boolean crossOperation;
	
	/** The max sale quantity. */
	private BigDecimal maxSaleQuantity = BigDecimal.ZERO;
	
	/** The sale quantity. */
	private BigDecimal saleQuantity = BigDecimal.ZERO;
	
	/** The sale amount. */
	private BigDecimal saleAmount = BigDecimal.ZERO; 
	
	/** The purchase quantity. */
	private BigDecimal purchaseQuantity = BigDecimal.ZERO;
	
	/** The max purchase quantity. */
	private BigDecimal maxPurchaseQuantity = BigDecimal.ZERO;
	
	/** The purchase amount. */
	private BigDecimal purchaseAmount = BigDecimal.ZERO;
	
	/** The role. */
	private Integer role;
	
	/** The selected operation. */
	private MechanismOperationTO selectedOperation;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;
	
	/** The holder account operation. */
	private HolderAccountOperation holderAccountOperation;
	
	/** The selected account operation. */
	private HolderAccountOperation selectedAccountOperation;
	
	/** The lst account operations. */
	private GenericDataModel<HolderAccountOperation> lstAccountOperations;
	
	/** The lst reassignment detail. */
	private GenericDataModel<ReassignmentRequestDetail> lstReassignmentDetail;
	
	/** The lst selected accounts. */
	private HolderAccountOperation[] lstSelectedAccounts;
	
	/** The lst deleted accounts. */
	private List<HolderAccountOperation> lstDeletedAccounts;
	
	/** The lst mechanism operations. */
	private GenericDataModel<MechanismOperationTO> lstMechanismOperations;
	
	/** The request reason. */
	private Integer requestReason;
	
	/** The list request reason. */
	private List<ParameterTable> lstRequestReason;
		
	/** The list settlement account operation. */
	private GenericDataModel<SettlementAccountMarketfact> lstSettlementAccountMarketfact;
	
	/**
	 * Clear amounts.
	 */
	public void clearAmounts(){
		//if(ComponentConstant.SALE_ROLE.equals(role)){
			maxSaleQuantity = BigDecimal.ZERO;
			saleQuantity = BigDecimal.ZERO;
			saleAmount = BigDecimal.ZERO; 
		//}else if (ComponentConstant.PURCHARSE_ROLE.equals(role)){
			maxPurchaseQuantity = BigDecimal.ZERO;
			purchaseQuantity = BigDecimal.ZERO;
			purchaseAmount = BigDecimal.ZERO;
		//}
	}
	
	/**
	 * Gets the participant by id.
	 *
	 * @param idParticipant the id participant
	 * @return the participant by id
	 */
	public Participant getParticipantById(Long idParticipant){
		if(lstParticipant!=null){
			for(Participant part : lstParticipant){
				if(part.getIdParticipantPk().equals(idParticipant)){
					return part;
				}
			}
		}
		return null;
	}
	
	/**
	 * Gets the sale quantity.
	 *
	 * @return the sale quantity
	 */
	public BigDecimal getSaleQuantity() {
		return saleQuantity;
	}
	
	/**
	 * Checks if is cross operation.
	 *
	 * @return true, if is cross operation
	 */
	public boolean isCrossOperation() {
		return crossOperation;
	}
	
	/**
	 * Sets the cross operation.
	 *
	 * @param crossOperation the new cross operation
	 */
	public void setCrossOperation(boolean crossOperation) {
		this.crossOperation = crossOperation;
	}
	
	/**
	 * Sets the sale quantity.
	 *
	 * @param saleQuantity the new sale quantity
	 */
	public void setSaleQuantity(BigDecimal saleQuantity) {
		this.saleQuantity = saleQuantity;
	}
	
	/**
	 * Gets the sale amount.
	 *
	 * @return the sale amount
	 */
	public BigDecimal getSaleAmount() {
		return saleAmount;
	}
	
	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Sets the sale amount.
	 *
	 * @param saleAmount the new sale amount
	 */
	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}
	
	/**
	 * Gets the purchase quantity.
	 *
	 * @return the purchase quantity
	 */
	public BigDecimal getPurchaseQuantity() {
		return purchaseQuantity;
	}
	
	/**
	 * Sets the purchase quantity.
	 *
	 * @param purchaseQuantity the new purchase quantity
	 */
	public void setPurchaseQuantity(BigDecimal purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}
	
	/**
	 * Gets the purchase amount.
	 *
	 * @return the purchase amount
	 */
	public BigDecimal getPurchaseAmount() {
		return purchaseAmount;
	}
	
	/**
	 * Sets the purchase amount.
	 *
	 * @param purchaseAmount the new purchase amount
	 */
	public void setPurchaseAmount(BigDecimal purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	
	/**
	 * Gets the lst mechanism operations.
	 *
	 * @return the lst mechanism operations
	 */
	public GenericDataModel<MechanismOperationTO> getLstMechanismOperations() {
		return lstMechanismOperations;
	}
	
	/**
	 * Sets the lst mechanism operations.
	 *
	 * @param lstMechanismOperations the new lst mechanism operations
	 */
	public void setLstMechanismOperations(
			GenericDataModel<MechanismOperationTO> lstMechanismOperations) {
		this.lstMechanismOperations = lstMechanismOperations;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
	/**
	 * Gets the id currency.
	 *
	 * @return the id currency
	 */
	public Long getIdCurrency() {
		return idCurrency;
	}
	
	/**
	 * Sets the id currency.
	 *
	 * @param idCurrency the new id currency
	 */
	public void setIdCurrency(Long idCurrency) {
		this.idCurrency = idCurrency;
	}
	
	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return idMechanism;
	}
	
	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}
	
	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}
	
	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}
	
	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the lst mechanism.
	 *
	 * @return the lst mechanism
	 */
	public List<NegotiationMechanism> getLstMechanism() {
		return lstMechanism;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}
	
	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	
	/**
	 * Sets the lst mechanism.
	 *
	 * @param lstMechanism the new lst mechanism
	 */
	public void setLstMechanism(List<NegotiationMechanism> lstMechanism) {
		this.lstMechanism = lstMechanism;
	}
	
	/**
	 * Gets the lst modality.
	 *
	 * @return the lst modality
	 */
	public List<NegotiationModality> getLstModality() {
		return lstModality;
	}
	
	/**
	 * Sets the lst modality.
	 *
	 * @param lstModality the new lst modality
	 */
	public void setLstModality(List<NegotiationModality> lstModality) {
		this.lstModality = lstModality;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}
	
	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the lst roles.
	 *
	 * @return the lst roles
	 */
	public List<SelectItem> getLstRoles() {
		return lstRoles;
	}
	
	/**
	 * Sets the lst roles.
	 *
	 * @param lstRoles the new lst roles
	 */
	public void setLstRoles(List<SelectItem> lstRoles) {
		this.lstRoles = lstRoles;
	}
	
	/**
	 * Gets the selected operation.
	 *
	 * @return the selected operation
	 */
	public MechanismOperationTO getSelectedOperation() {
		return selectedOperation;
	}

	/**
	 * Sets the selected operation.
	 *
	 * @param selectedOperation the new selected operation
	 */
	public void setSelectedOperation(MechanismOperationTO selectedOperation) {
		this.selectedOperation = selectedOperation;
	}
	
	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}
	
	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}
	
	/**
	 * Gets the lst account operations.
	 *
	 * @return the lst account operations
	 */
	public GenericDataModel<HolderAccountOperation> getLstAccountOperations() {
		return lstAccountOperations;
	}
	
	/**
	 * Sets the lst account operations.
	 *
	 * @param lstAccountOperations the new lst account operations
	 */
	public void setLstAccountOperations(
			GenericDataModel<HolderAccountOperation> lstAccountOperations) {
		this.lstAccountOperations = lstAccountOperations;
	}
	
	/**
	 * Gets the lst selected accounts.
	 *
	 * @return the lst selected accounts
	 */
	public HolderAccountOperation[] getLstSelectedAccounts() {
		return lstSelectedAccounts;
	}
	
	/**
	 * Sets the lst selected accounts.
	 *
	 * @param lstSelectedAccounts the new lst selected accounts
	 */
	public void setLstSelectedAccounts(HolderAccountOperation[] lstSelectedAccounts) {
		this.lstSelectedAccounts = lstSelectedAccounts;
	}
	
	/**
	 * Gets the lst deleted accounts.
	 *
	 * @return the lst deleted accounts
	 */
	public List<HolderAccountOperation> getLstDeletedAccounts() {
		return lstDeletedAccounts;
	}
	
	/**
	 * Sets the lst deleted accounts.
	 *
	 * @param lstDeletedAccounts the new lst deleted accounts
	 */
	public void setLstDeletedAccounts(
			List<HolderAccountOperation> lstDeletedAccounts) {
		this.lstDeletedAccounts = lstDeletedAccounts;
	}

	/**
	 * Gets the selected account operation.
	 *
	 * @return the selected account operation
	 */
	public HolderAccountOperation getSelectedAccountOperation() {
		return selectedAccountOperation;
	}

	/**
	 * Sets the selected account operation.
	 *
	 * @param selectedAccountOperation the new selected account operation
	 */
	public void setSelectedAccountOperation(
			HolderAccountOperation selectedAccountOperation) {
		this.selectedAccountOperation = selectedAccountOperation;
	}

	/**
	 * Gets the lst settlement schema.
	 *
	 * @return the lst settlement schema
	 */
	public List<SelectItem> getLstSettlementSchema() {
		return lstSettlementSchema;
	}

	/**
	 * Sets the lst settlement schema.
	 *
	 * @param lstSettlementSchema the new lst settlement schema
	 */
	public void setLstSettlementSchema(List<SelectItem> lstSettlementSchema) {
		this.lstSettlementSchema = lstSettlementSchema;
	}

	/**
	 * Gets the lst operation state.
	 *
	 * @return the lst operation state
	 */
	public List<ParameterTable> getLstOperationState() {
		return lstOperationState;
	}

	/**
	 * Sets the lst operation state.
	 *
	 * @param lstOperationState the new lst operation state
	 */
	public void setLstOperationState(List<ParameterTable> lstOperationState) {
		this.lstOperationState = lstOperationState;
	}

	/**
	 * Gets the lst reassignment detail.
	 *
	 * @return the lst reassignment detail
	 */
	public GenericDataModel<ReassignmentRequestDetail> getLstReassignmentDetail() {
		return lstReassignmentDetail;
	}

	/**
	 * Sets the lst reassignment detail.
	 *
	 * @param lstReassignmentDetail the new lst reassignment detail
	 */
	public void setLstReassignmentDetail(
			GenericDataModel<ReassignmentRequestDetail> lstReassignmentDetail) {
		this.lstReassignmentDetail = lstReassignmentDetail;
	}

	/**
	 * Gets the max sale quantity.
	 *
	 * @return the max sale quantity
	 */
	public BigDecimal getMaxSaleQuantity() {
		return maxSaleQuantity;
	}

	/**
	 * Sets the max sale quantity.
	 *
	 * @param maxSaleQuantity the new max sale quantity
	 */
	public void setMaxSaleQuantity(BigDecimal maxSaleQuantity) {
		this.maxSaleQuantity = maxSaleQuantity;
	}

	/**
	 * Gets the max purchase quantity.
	 *
	 * @return the max purchase quantity
	 */
	public BigDecimal getMaxPurchaseQuantity() {
		return maxPurchaseQuantity;
	}

	/**
	 * Sets the max purchase quantity.
	 *
	 * @param maxPurchaseQuantity the new max purchase quantity
	 */
	public void setMaxPurchaseQuantity(BigDecimal maxPurchaseQuantity) {
		this.maxPurchaseQuantity = maxPurchaseQuantity;
	}

	/**
	 * Gets the request reason.
	 *
	 * @return the requestReason
	 */
	public Integer getRequestReason() {
		return requestReason;
	}

	/**
	 * Sets the request reason.
	 *
	 * @param requestReason the requestReason to set
	 */
	public void setRequestReason(Integer requestReason) {
		this.requestReason = requestReason;
	}

	/**
	 * Gets the lst request reason.
	 *
	 * @return the lstRequestReason
	 */
	public List<ParameterTable> getLstRequestReason() {
		return lstRequestReason;
	}

	/**
	 * Sets the lst request reason.
	 *
	 * @param lstRequestReason the lstRequestReason to set
	 */
	public void setLstRequestReason(List<ParameterTable> lstRequestReason) {
		this.lstRequestReason = lstRequestReason;
	}

	/**
	 * Gets the lst settlement account marketfact.
	 *
	 * @return the lstSettlementAccountMarketfact
	 */
	public GenericDataModel<SettlementAccountMarketfact> getLstSettlementAccountMarketfact() {
		return lstSettlementAccountMarketfact;
	}

	/**
	 * Sets the lst settlement account marketfact.
	 *
	 * @param lstSettlementAccountMarketfact the lstSettlementAccountMarketfact to set
	 */
	public void setLstSettlementAccountMarketfact(
			GenericDataModel<SettlementAccountMarketfact> lstSettlementAccountMarketfact) {
		this.lstSettlementAccountMarketfact = lstSettlementAccountMarketfact;
	}	

}
