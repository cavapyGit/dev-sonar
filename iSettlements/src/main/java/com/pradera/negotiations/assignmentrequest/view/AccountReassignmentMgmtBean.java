package com.pradera.negotiations.assignmentrequest.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ReassignmentRequest;
import com.pradera.model.negotiation.ReassignmentRequestDetail;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.type.ReassignmentRequestMotiveType;
import com.pradera.model.settlement.type.ReassignmentRequestStateType;
import com.pradera.model.settlement.type.SettlementScheduleType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.accountassignment.view.MarketFactAssignmentMgmtBean;
import com.pradera.negotiations.assignmentrequest.to.AccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.RegisterAccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAccountReassignmentTO;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountReassignmentMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class AccountReassignmentMgmtBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The account assignment facade. */
	@EJB
	private AccountAssignmentFacade accountAssignmentFacade;
	
	/** The mcn operation service facade. */
	@EJB
	private McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	private IdepositarySetup idepositarySetup;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The market fact assignment mgmt bean. */
	@Inject
    private MarketFactAssignmentMgmtBean marketFactAssignmentMgmtBean;
	
	/** The search account reassignment to. */
	private SearchAccountReassignmentTO searchAccountReassignmentTO;
	
	/** The register account reassignment to. */
	private RegisterAccountReassignmentTO registerAccountReassignmentTO;
	
	/** The account reassignment to. */
	private AccountReassignmentTO accountReassignmentTO;
	
	/** The depositary user. */
	private boolean depositaryUser;
	
	/** The participant user. */
	private boolean participantUser;
	
	/** The bl market fact. */
	private boolean blMarketFact;
	
	/** The map parameters. */
	private HashMap<Integer, String> mapParameters = new HashMap<Integer,String>();
	
	/** The is fixed income. */
	boolean isFixedIncome;
	
	/** The show data market fact. */
	boolean showDataMarketFact;
	
	/** The lst settlement account marketfact aux. */
	private List<SettlementAccountMarketfact> lstSettlementAccountMarketfactAux;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			searchAccountReassignmentTO = new SearchAccountReassignmentTO();
			searchAccountReassignmentTO.setInitialDate(getCurrentSystemDate());
			searchAccountReassignmentTO.setFinalDate(getCurrentSystemDate());
			searchAccountReassignmentTO.setIdMechanism(NegotiationMechanismType.BOLSA.getCode());
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				depositaryUser = true;
				searchAccountReassignmentTO.setLstMechanism(accountAssignmentFacade.getListNegotiationMechanism());
				searchAccountReassignmentTO.setLstParticipant(accountAssignmentFacade.getListParticipantsForNegoMechanism(searchAccountReassignmentTO.getIdMechanism(), null));
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				participantUser = true;
				searchAccountReassignmentTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
				List<Participant> lstTemp = new ArrayList<Participant>();
				Participant participant = accountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				lstTemp.add(participant);
				searchAccountReassignmentTO.setLstParticipant(lstTemp);
				searchAccountReassignmentTO.setLstMechanism(accountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));
			}
			searchAccountReassignmentTO.setLstModality(accountAssignmentFacade.getListNegotiationModality(
					searchAccountReassignmentTO.getIdMechanism(), searchAccountReassignmentTO.getIdParticipant(), null));
			
			if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
				blMarketFact = true;
			}
			
			loadParameters();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParameters() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.ACCOUNT_OPERATION_STATE.getCode());
			masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());			
		parameterTableTO.setLstMasterTableFk(masterPks);
		
		List<ParameterTable> lst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt.getParameterName());
		}
		
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.REASSIGNMENT_REQUEST_STATE.getCode());
		searchAccountReassignmentTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		searchAccountReassignmentTO.setLstCurrency(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
	}
	
	/**
	 * Load view parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadViewParameters() throws ServiceException{
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		registerAccountReassignmentTO.setLstOperationState(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
		registerAccountReassignmentTO.setLstSettlementSchema(NegotiationUtils.getSettlementSchemas());
		
	}

	/**
	 * Load registration page.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String loadRegistrationPage() throws ServiceException{
		
		clearRegistrationData();
		
		return "registerReassignment";
	}
	
	/**
	 * Clear registration data.
	 */
	private void clearRegistrationData() {
		registerAccountReassignmentTO = new RegisterAccountReassignmentTO();
		registerAccountReassignmentTO.setLstCurrency(searchAccountReassignmentTO.getLstCurrency());
		registerAccountReassignmentTO.setIdMechanism(NegotiationMechanismType.BOLSA.getCode());
		registerAccountReassignmentTO.setLstMechanism(searchAccountReassignmentTO.getLstMechanism());
		registerAccountReassignmentTO.setLstModality(searchAccountReassignmentTO.getLstModality());
		registerAccountReassignmentTO.setSettlementDate(CommonsUtilities.currentDate());
		if(participantUser){
			registerAccountReassignmentTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
			
		}
		registerAccountReassignmentTO.setLstParticipant(searchAccountReassignmentTO.getLstParticipant());
		
		registerAccountReassignmentTO.setLstRoles(NegotiationUtils.getLstRoles());
			
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(MasterTableType.REQUEST_REASON_REASSIGNMENT.getCode());
			registerAccountReassignmentTO.setLstRequestReason(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
		} catch (ServiceException e) {			
			e.printStackTrace();
		}
	}

	/**
	 * On change mechanism search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeMechanismSearch() throws ServiceException{
		searchAccountReassignmentTO.setIdModality(null);
		searchAccountReassignmentTO.setIdParticipant(null);
		
		if(searchAccountReassignmentTO.getIdMechanism()!=null){
			if(depositaryUser){				
				searchAccountReassignmentTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModality(searchAccountReassignmentTO.getIdMechanism()));
			}else if(participantUser){
				searchAccountReassignmentTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
				searchAccountReassignmentTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
						searchAccountReassignmentTO.getIdMechanism(), searchAccountReassignmentTO.getIdParticipant()));		
			}
		}
	}
	
	/**
	 * On change modality search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeModalitySearch() throws ServiceException{
		if(depositaryUser && searchAccountReassignmentTO.getIdMechanism()!=null && searchAccountReassignmentTO.getIdModality()!=null){		
			searchAccountReassignmentTO.setLstParticipant(accountAssignmentFacade.getListParticipantsForNegoModality(
					searchAccountReassignmentTO.getIdMechanism(), searchAccountReassignmentTO.getIdModality(), null, null));
		}	
	}
	
	/**
	 * On change participant search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeParticipantSearch() throws ServiceException{
		if(depositaryUser && searchAccountReassignmentTO.getIdMechanism()!=null && searchAccountReassignmentTO.getIdParticipant()!=null){		
			searchAccountReassignmentTO.setLstModality(accountAssignmentFacade.getListNegotiationModality(
					searchAccountReassignmentTO.getIdMechanism(), searchAccountReassignmentTO.getIdParticipant(), null));
		}
	}
	
	/**
	 * On change participant register.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeParticipantRegister() throws ServiceException{
		if(depositaryUser && registerAccountReassignmentTO.getIdMechanism()!=null && registerAccountReassignmentTO.getIdParticipant()!=null){		
			searchAccountReassignmentTO.setLstModality(accountAssignmentFacade.getListNegotiationModality(
					registerAccountReassignmentTO.getIdMechanism(), registerAccountReassignmentTO.getIdParticipant(), null));
		}
	}
	
	/**
	 * Search account reassignments.
	 */
	public void searchAccountReassignments(){
		accountReassignmentTO = null;
		
		List<AccountReassignmentTO> lstAccountReassignment = accountAssignmentFacade.searchReassignmentRequests(searchAccountReassignmentTO);
		searchAccountReassignmentTO.setLstAccountReassignment(new GenericDataModel<AccountReassignmentTO>(lstAccountReassignment));
		
		showPrivilegeButtons();
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);	
		privilegeComponent.setBtnAuthorize(true);	//issue 561
		privilegeComponent.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * On change mechanism register.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeMechanismRegister() throws ServiceException{
		registerAccountReassignmentTO.setIdModality(null);
		registerAccountReassignmentTO.setIdParticipant(null);
		
		if(registerAccountReassignmentTO.getIdMechanism()!=null){
			if(depositaryUser){				
				registerAccountReassignmentTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModality(registerAccountReassignmentTO.getIdMechanism()));
			}else if(participantUser){
				registerAccountReassignmentTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
				registerAccountReassignmentTO.setLstModality(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
						registerAccountReassignmentTO.getIdMechanism(), registerAccountReassignmentTO.getIdParticipant()));		
			}
		}
	}
	
	/**
	 * On change modality register.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeModalityRegister() throws ServiceException{
		if(depositaryUser && registerAccountReassignmentTO.getIdMechanism()!=null && registerAccountReassignmentTO.getIdModality()!=null){				
			searchAccountReassignmentTO.setLstParticipant(accountAssignmentFacade.getListParticipantsForNegoModality(
					registerAccountReassignmentTO.getIdMechanism(), registerAccountReassignmentTO.getIdModality(), null, null));
		}	
	}
	
	/**
	 * Search mechanism operations.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchMechanismOperations() throws ServiceException{
		registerAccountReassignmentTO.setSelectedOperation(null);
		registerAccountReassignmentTO.setMechanismOperation(null);
		registerAccountReassignmentTO.setLstAccountOperations(null);
		List<MechanismOperationTO> lstMechanismOperation = accountAssignmentFacade.getOperations(registerAccountReassignmentTO);
		registerAccountReassignmentTO.setLstMechanismOperations(new GenericDataModel<MechanismOperationTO>(lstMechanismOperation));
		
		registerAccountReassignmentTO.setLstSettlementAccountMarketfact(null);
	}
	
	/**
	 * On select mechanism operation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onSelectMechanismOperation() throws ServiceException {
		registerAccountReassignmentTO.setLstAccountOperations(null);
		registerAccountReassignmentTO.setLstSelectedAccounts(null);
		registerAccountReassignmentTO.setHolderAccountOperation(new HolderAccountOperation());
		
		// Add motive reassignment				
		registerAccountReassignmentTO.setRequestReason(ReassignmentRequestMotiveType.MODIFY_HOLDER_ACCOUNT.getCode());
		registerAccountReassignmentTO.setLstSettlementAccountMarketfact(null);
		isFixedIncome = Boolean.FALSE;
		
		try{
			if(registerAccountReassignmentTO.getSelectedOperation()!=null){
				
				accountAssignmentFacade.checkPreviousReassignmentRequest(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanismOperationPk(), 
						registerAccountReassignmentTO.getIdParticipant());
				
				accountAssignmentFacade.checkAssignmentProcess(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanism(),
						registerAccountReassignmentTO.getSelectedOperation().getIdModality(),
						AssignmentProcessStateType.CLOSED.getCode(),
						CommonsUtilities.currentDate());
				
				accountAssignmentFacade.checkPendingSettlementProcess(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanism(),
						registerAccountReassignmentTO.getSelectedOperation().getIdModality(),
						registerAccountReassignmentTO.getSelectedOperation().getSettlementCurrency(),
						registerAccountReassignmentTO.getSelectedOperation().getCashSettlementDate());
				
				accountAssignmentFacade.checkFinishedSettlementProcess(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanism(),
						registerAccountReassignmentTO.getSelectedOperation().getIdModality(),
						registerAccountReassignmentTO.getSelectedOperation().getSettlementCurrency(),
						registerAccountReassignmentTO.getSelectedOperation().getCashSettlementDate(),
						SettlementScheduleType.MELOR.getCode());
				
				MechanismOperation mechanismOperation = accountAssignmentFacade.getMechanismOperation(registerAccountReassignmentTO.getSelectedOperation().getIdMechanismOperationPk());
				registerAccountReassignmentTO.setMechanismOperation(mechanismOperation);
				registerAccountReassignmentTO.clearAmounts();
				
				List<Integer> roles = new ArrayList<Integer>();
				Long idPurchaseParticipant = registerAccountReassignmentTO.getMechanismOperation().getBuyerParticipant().getIdParticipantPk();
				Long idSaleParticipant = registerAccountReassignmentTO.getMechanismOperation().getSellerParticipant().getIdParticipantPk();
				if(idPurchaseParticipant.equals(idSaleParticipant)){
					registerAccountReassignmentTO.setCrossOperation(true);
					roles.add(ComponentConstant.PURCHARSE_ROLE);
					roles.add(ComponentConstant.SALE_ROLE);
				} else {
					registerAccountReassignmentTO.setCrossOperation(false);
					if(idPurchaseParticipant.equals(registerAccountReassignmentTO.getIdParticipant())){
						registerAccountReassignmentTO.setRole(ComponentConstant.PURCHARSE_ROLE);
					}else if(idSaleParticipant.equals(registerAccountReassignmentTO.getIdParticipant())){
						registerAccountReassignmentTO.setRole(ComponentConstant.SALE_ROLE);
					}
					roles.add(registerAccountReassignmentTO.getRole());
				}
				
				List<HolderAccountOperation> accountOperations = accountAssignmentFacade.getHolderAccountOperations(
						registerAccountReassignmentTO.getMechanismOperation().getIdMechanismOperationPk(), roles, registerAccountReassignmentTO.getIdParticipant());
				
				for(HolderAccountOperation holderAccountOperation : accountOperations){
					fillHolderAccountOperation(holderAccountOperation);
					updateQuantityAssig(holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount(), 
							holderAccountOperation.getRole(), ComponentConstant.SUM, true);
				}
				
				registerAccountReassignmentTO.setLstAccountOperations(new GenericDataModel<HolderAccountOperation>(accountOperations));
			}
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			registerAccountReassignmentTO.setSelectedOperation(null);
		}
	}
	
	/**
	 * Fill holder account operation.
	 *
	 * @param holderAccountOperation the holder account operation
	 */
	private void fillHolderAccountOperation(HolderAccountOperation holderAccountOperation) {
		holderAccountOperation.setHolderAccountStateDesc(mapParameters.get(holderAccountOperation.getHolderAccountState()));	
		holderAccountOperation.getHolderAccount().setAccountDescription(mapParameters.get(holderAccountOperation.getHolderAccount().getAccountType()));
	}

	/**
	 * On change stock quantity.
	 */
	public void onChangeStockQuantity(){
		BigDecimal enteredQuantity = registerAccountReassignmentTO.getHolderAccountOperation().getStockQuantity();
		BigDecimal operationQuantity = registerAccountReassignmentTO.getMechanismOperation().getStockQuantity();
		if (Validations.validateIsNotNull(enteredQuantity)){
			if(enteredQuantity.compareTo(operationQuantity) > 0){
				registerAccountReassignmentTO.getHolderAccountOperation().setStockQuantity(null);
				registerAccountReassignmentTO.getHolderAccountOperation().setSettlementAmount(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_QUANTITY_EXCEEDED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			BigDecimal cashAmount = NegotiationUtils.getSettlementAmount(registerAccountReassignmentTO.getMechanismOperation().getCashSettlementPrice(), enteredQuantity); 
			registerAccountReassignmentTO.getHolderAccountOperation().setSettlementAmount(cashAmount);
			
		}
	}
	
	/**
	 * Validate holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateHolder() throws ServiceException{
		registerAccountReassignmentTO.setLstHolderAccount(null);
		registerAccountReassignmentTO.getHolderAccountOperation().setHolderAccount(null);
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountReassignmentTO.getHolder().getIdHolderPk())){
			if(HolderStateType.BLOCKED.getCode().equals(registerAccountReassignmentTO.getHolder().getStateHolder())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountReassignmentTO.setHolder(new Holder());
				return;
			}
			
			List<HolderAccount> lstResult = accountAssignmentFacade.findHolderAccounts(registerAccountReassignmentTO.getHolder().getIdHolderPk(), 
					registerAccountReassignmentTO.getIdParticipant());
			if(Validations.validateListIsNullOrEmpty(lstResult)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountReassignmentTO.setHolder(new Holder());	
				return;
			}
			
			for(HolderAccount holderAccount:lstResult){
				holderAccount.setAccountDescription(mapParameters.get(holderAccount.getAccountType()));
			}
			registerAccountReassignmentTO.setLstHolderAccount(lstResult);
		}
	}
	
	/**
	 * Assign holder account.
	 */
	public void assignHolderAccount(){
		
		try {
			Long idParticipant = registerAccountReassignmentTO.getIdParticipant();
			registerAccountReassignmentTO.getHolderAccountOperation().setRole(registerAccountReassignmentTO.getRole());
			registerAccountReassignmentTO.getHolderAccountOperation().setMechanismOperation(registerAccountReassignmentTO.getMechanismOperation());
			registerAccountReassignmentTO.getHolderAccountOperation().setHolderAccountState(HolderAccountOperationStateType.PENDING.getCode());
			registerAccountReassignmentTO.getHolderAccountOperation().setHolderAccountStateDesc(AssignmentRequestStateType.PENDING.getDescripcion());
			registerAccountReassignmentTO.getHolderAccountOperation().setInchargeFundsParticipant(registerAccountReassignmentTO.getParticipantById(idParticipant));
			registerAccountReassignmentTO.getHolderAccountOperation().setInchargeStockParticipant(registerAccountReassignmentTO.getParticipantById(idParticipant));

			HolderAccount selectedAccount = registerAccountReassignmentTO.getHolderAccountOperation().getHolderAccount();
			BigDecimal stockQuantity = registerAccountReassignmentTO.getHolderAccountOperation().getStockQuantity();
			Integer role = registerAccountReassignmentTO.getHolderAccountOperation().getRole();
			
			if(Validations.validateIsNullOrEmpty(selectedAccount)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			boolean incorrectQuantity = false; 
			if(ComponentConstant.SALE_ROLE.equals(role)){
				if(stockQuantity.compareTo(registerAccountReassignmentTO.getMaxSaleQuantity()) > 0){
					incorrectQuantity = true;
				}
			}else if(ComponentConstant.PURCHARSE_ROLE.equals(role)){
				if(stockQuantity.compareTo(registerAccountReassignmentTO.getMaxPurchaseQuantity()) > 0){
					incorrectQuantity = true;
				}
			}
			
			if(incorrectQuantity){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_QUANTITY_EXCEEDED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			String validatePrimaryPlace = NegotiationUtils.validateQuantityNegotitation(
					registerAccountReassignmentTO.getMechanismOperation().getIndPrimaryPlacement(),
					registerAccountReassignmentTO.getMechanismOperation().getSecurities(),
					registerAccountReassignmentTO.getHolderAccountOperation().getStockQuantity());
			
			if(Validations.validateIsNotNullAndNotEmpty(validatePrimaryPlace)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(validatePrimaryPlace));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			accountAssignmentFacade.validateAssignmentConditions(registerAccountReassignmentTO.getHolderAccountOperation(), 
					registerAccountReassignmentTO.getLstAccountOperations().getDataList());
			
			String error = updateQuantityAssig(registerAccountReassignmentTO.getHolderAccountOperation().getStockQuantity(), 
					registerAccountReassignmentTO.getHolderAccountOperation().getSettlementAmount(),
					registerAccountReassignmentTO.getHolderAccountOperation().getRole(), ComponentConstant.SUM, false);
			
			if(Validations.validateIsNotNullAndNotEmpty(error)){ 
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(error));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			registerAccountReassignmentTO.getLstAccountOperations().getDataList().add(registerAccountReassignmentTO.getHolderAccountOperation());
			registerAccountReassignmentTO.setHolderAccountOperation(new HolderAccountOperation());
			if(ComponentConstant.SALE_ROLE.equals(registerAccountReassignmentTO.getHolderAccountOperation().getRole())){
				registerAccountReassignmentTO.getHolderAccountOperation().setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
			}
			registerAccountReassignmentTO.setHolder(new Holder());
			registerAccountReassignmentTO.setLstHolderAccount(null);
			registerAccountReassignmentTO.getHolderAccountOperation().setHolderAccount(null);
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
		
	}
	
	/**
	 * Before remove account.
	 */
	public void beforeRemoveAccount(){
		HolderAccountOperation[] selectedAccountOperations = registerAccountReassignmentTO.getLstSelectedAccounts();
 		if(Validations.validateIsNotNull(selectedAccountOperations) && selectedAccountOperations.length > 0){
 			List<String> accountsNumber = new ArrayList<String>();
 			for(int i=0; i<selectedAccountOperations.length; i++){
 				
 				try {
					accountAssignmentFacade.checkExistChainedAccount(selectedAccountOperations[i].getIdHolderAccountOperationPk());
				} catch (ServiceException e) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
 				
 				String accNumber = selectedAccountOperations[i].getHolderAccount().getAccountNumber().toString();
 				accountsNumber.add(accNumber);
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_ASK_REMOVE, 
							new Object[]{StringUtils.join(accountsNumber.toArray(),GeneralConstants.STR_COMMA_WITHOUT_SPACE)}));		
			JSFUtilities.putViewMap("action", "remove");
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_REMOVE_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}		
	}
	
	/**
	 * Removes the account.
	 */
	public void removeAccount(){		
		for(int i=0;i<registerAccountReassignmentTO.getLstSelectedAccounts().length;i++){
			HolderAccountOperation selectedAccountOperation = registerAccountReassignmentTO.getLstSelectedAccounts()[i];
			updateQuantityAssig(selectedAccountOperation.getStockQuantity(), selectedAccountOperation.getSettlementAmount(),
						selectedAccountOperation.getRole(), ComponentConstant.SUBTRACTION, false);
			
			boolean removed = registerAccountReassignmentTO.getLstAccountOperations().getDataList().remove(selectedAccountOperation);
			if(removed){
				if(registerAccountReassignmentTO.getLstDeletedAccounts() == null){
					registerAccountReassignmentTO.setLstDeletedAccounts(new ArrayList<HolderAccountOperation>());
				}
				registerAccountReassignmentTO.getLstDeletedAccounts().add(selectedAccountOperation);
			}
		}
		
		if(registerAccountReassignmentTO.getLstSelectedAccounts().length > 1){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNTS));
		} else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNT));
		}
			
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Before save assignment.
	 */
	public void beforeSaveAssignment(){
		
		if(ReassignmentRequestMotiveType.MODIFY_MARKETFACT.getCode().equals(registerAccountReassignmentTO.getRequestReason())) {			
			int countNotChange = 0;
			int countNotSelected = 0;
			String strOperation = GeneralConstants.EMPTY_STRING;
			if(Validations.validateListIsNotNullAndNotEmpty(registerAccountReassignmentTO.getLstSettlementAccountMarketfact().getDataList())
					&& Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountMarketfactAux)){
				for(SettlementAccountMarketfact objSettlementAccountMarketfact : registerAccountReassignmentTO.getLstSettlementAccountMarketfact().getDataList()) {
					if(objSettlementAccountMarketfact.isSelected()) {
						countNotSelected++;
						for(SettlementAccountMarketfact objSettlementAccountMarketfactInitial : lstSettlementAccountMarketfactAux) {
							
							if(Validations.validateIsNullOrEmpty(objSettlementAccountMarketfact.getMarketDate())){
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_ASSIGN_MARKET_DATE_NULL));
								JSFUtilities.showSimpleValidationDialog();
								return;	
							}
							
							if(isFixedIncome) {
								
								if(Validations.validateIsNullOrEmpty(objSettlementAccountMarketfact.getMarketRate())){
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_ASSIGN_MARKET_RATE_NULL));
									JSFUtilities.showSimpleValidationDialog();
									return;	
								}
								
								if(objSettlementAccountMarketfact.getIdSettAccountMarketfactPk().equals(objSettlementAccountMarketfactInitial.getIdSettAccountMarketfactPk()) && 
										objSettlementAccountMarketfact.getMarketDate().compareTo(objSettlementAccountMarketfactInitial.getMarketDate()) == 0
										&& objSettlementAccountMarketfact.getMarketRate().compareTo(objSettlementAccountMarketfactInitial.getMarketRate()) == 0){	
									if(GeneralConstants.EMPTY_STRING.compareTo(strOperation) == 0){
										strOperation = GeneralConstants.EMPTY_STRING + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk();
									} else {
										strOperation = strOperation + GeneralConstants.STR_COMMA_WITHOUT_SPACE + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk()  ;
									}							
									//countNotChange++;
									objSettlementAccountMarketfact.setSelected(false);
									countNotSelected--;
								}
							} else {
								
								if(Validations.validateIsNullOrEmpty(objSettlementAccountMarketfact.getMarketPrice())){
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_ASSIGN_MARKET_PRICE_NULL));
									JSFUtilities.showSimpleValidationDialog();
									return;	
								}
								
								if(objSettlementAccountMarketfact.getIdSettAccountMarketfactPk().equals(objSettlementAccountMarketfactInitial.getIdSettAccountMarketfactPk()) &&
										objSettlementAccountMarketfact.getMarketDate().compareTo(objSettlementAccountMarketfactInitial.getMarketDate()) == 0
										&& objSettlementAccountMarketfact.getMarketPrice().compareTo(objSettlementAccountMarketfactInitial.getMarketPrice()) == 0){
									if(GeneralConstants.EMPTY_STRING.compareTo(strOperation) == 0){
										strOperation = GeneralConstants.EMPTY_STRING + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk();
									} else {
										strOperation = strOperation + GeneralConstants.STR_COMMA_WITHOUT_SPACE + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk()  ;
									}							
									//countNotChange++;
									objSettlementAccountMarketfact.setSelected(false);
									countNotSelected--;
								}
							}
						}
					}
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_MARKEFACT_PARTICIPANT));
				JSFUtilities.showSimpleValidationDialog();
				return;	
			}
			
			if(countNotSelected == 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_NO_MARKETFACT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;	
			}
			
			if(countNotChange > 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_NO_CHANGES_MARKETFACT, strOperation));
				JSFUtilities.showSimpleValidationDialog();
				return;	
			}
			
		} else {
			if(registerAccountReassignmentTO.getMechanismOperation()==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_NO_OPERATION_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(registerAccountReassignmentTO.getMaxPurchaseQuantity()!=null && 
					registerAccountReassignmentTO.getMaxPurchaseQuantity().compareTo(BigDecimal.ZERO)==1
					&& registerAccountReassignmentTO.getPurchaseQuantity().compareTo(BigDecimal.ZERO)==0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_QUANTITY_OPERATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(registerAccountReassignmentTO.getMaxSaleQuantity()!=null && 
					registerAccountReassignmentTO.getMaxSaleQuantity().compareTo(BigDecimal.ZERO)==1
					&& registerAccountReassignmentTO.getSaleQuantity().compareTo(BigDecimal.ZERO)==0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_QUANTITY_OPERATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(registerAccountReassignmentTO.getSaleQuantity().compareTo(registerAccountReassignmentTO.getMaxSaleQuantity()) != 0 ||
					registerAccountReassignmentTO.getPurchaseQuantity().compareTo(registerAccountReassignmentTO.getMaxPurchaseQuantity()) != 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SAME_QUANTITY_OPERATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			// check if theres at least one modification
			boolean modified = false;
			for(HolderAccountOperation holderAccountOperation : registerAccountReassignmentTO.getLstAccountOperations().getDataList()){
				if(holderAccountOperation.getIdHolderAccountOperationPk()==null){
					modified = true;
					break;
				}
			}
			if(!modified){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_NO_MODIFICATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(blMarketFact){
				for(HolderAccountOperation holderAccountOperation:registerAccountReassignmentTO.getLstAccountOperations()){
					if(ComponentConstant.SALE_ROLE.equals(holderAccountOperation.getRole()) &&
							!HolderAccountOperationStateType.CONFIRMED.getCode().equals(holderAccountOperation.getHolderAccountState())){
						if(Validations.validateListIsNullOrEmpty(holderAccountOperation.getAccountOperationMarketFacts())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY, 
																	holderAccountOperation.getHolderAccount().getAccountNumber().toString()));
							JSFUtilities.showSimpleValidationDialog();
							return;					
						}
						
						BigDecimal total = BigDecimal.ZERO;
						for(AccountOperationMarketFact accMktFact : holderAccountOperation.getAccountOperationMarketFacts()){
							total = total.add(accMktFact.getOperationQuantity() != null ? accMktFact.getOperationQuantity() : BigDecimal.ZERO);
						}
						if(total.compareTo(holderAccountOperation.getStockQuantity()) != 0){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY, 
																	holderAccountOperation.getHolderAccount().getAccountNumber().toString()));
							JSFUtilities.showSimpleValidationDialog();
							return;	
						}
					}
				}
			}
		}				
		
		JSFUtilities.putViewMap("action", "save");
		Participant part = registerAccountReassignmentTO.getParticipantById(registerAccountReassignmentTO.getIdParticipant());
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_REGISTER_CONFIRM,new Object[]{part.getDisplayCodeMnemonic()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");					
	}
	
	/**
	 * Save assignment.
	 */
	@LoggerAuditWeb
	public void saveAssignment() {
		
		try {
			
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountReassignmentTO.getRequestReason())) {
				
				if(ReassignmentRequestMotiveType.MODIFY_HOLDER_ACCOUNT.getCode().equals(registerAccountReassignmentTO.getRequestReason())) {
					ReassignmentRequest reassignmentRequest = new ReassignmentRequest();
					reassignmentRequest.setMechanismOperation(registerAccountReassignmentTO.getMechanismOperation());
					reassignmentRequest.setParticipant(registerAccountReassignmentTO.getParticipantById(registerAccountReassignmentTO.getIdParticipant()));
					reassignmentRequest.setRequestState(ReassignmentRequestStateType.REGISTERED.getCode());
					if(!registerAccountReassignmentTO.isCrossOperation()){
						reassignmentRequest.setRole(registerAccountReassignmentTO.getRole());
					}
					reassignmentRequest.setRequestReason(registerAccountReassignmentTO.getRequestReason());
					accountAssignmentFacade.saveReassignmentRequest(reassignmentRequest,
							registerAccountReassignmentTO.getLstAccountOperations().getDataList(),
							registerAccountReassignmentTO.getLstDeletedAccounts());
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_REGISTER_SUCCESS, 
									new Object[]{reassignmentRequest.getIdReassignmentRequestPk()}));
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					
				} else {
					ReassignmentRequest reassignmentRequest = new ReassignmentRequest();
					reassignmentRequest.setMechanismOperation(registerAccountReassignmentTO.getMechanismOperation());
					reassignmentRequest.setParticipant(registerAccountReassignmentTO.getParticipantById(registerAccountReassignmentTO.getIdParticipant()));
					reassignmentRequest.setRequestState(ReassignmentRequestStateType.REGISTERED.getCode());
					if(!registerAccountReassignmentTO.isCrossOperation()){
						reassignmentRequest.setRole(registerAccountReassignmentTO.getRole());
					}
					reassignmentRequest.setRequestReason(registerAccountReassignmentTO.getRequestReason());
					accountAssignmentFacade.saveReassignmentRequestMarketFact(reassignmentRequest,
							registerAccountReassignmentTO.getLstSettlementAccountMarketfact().getDataList(),
							lstSettlementAccountMarketfactAux, isFixedIncome);
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_REGISTER_SUCCESS, 
									new Object[]{reassignmentRequest.getIdReassignmentRequestPk()}));
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					
				}				
			}												
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Update quantity assig.
	 *
	 * @param stockQuantity the stock quantity
	 * @param settlementAmount the settlement amount
	 * @param role the role
	 * @param action the action
	 * @param setMax the set max
	 * @return the string
	 */
	private String updateQuantityAssig(BigDecimal stockQuantity, BigDecimal settlementAmount, Integer role, long action, boolean setMax) {
		
		BigDecimal totalQuantity = BigDecimal.ZERO;
		BigDecimal totalAmount = BigDecimal.ZERO;
		if(ComponentConstant.SALE_ROLE.equals(role)){
			totalQuantity = registerAccountReassignmentTO.getSaleQuantity();
			totalAmount = registerAccountReassignmentTO.getSaleAmount();
		} else if(ComponentConstant.PURCHARSE_ROLE.equals(role)){
			totalQuantity = registerAccountReassignmentTO.getPurchaseQuantity();
			totalAmount = registerAccountReassignmentTO.getPurchaseAmount();
		}
		
		BigDecimal newQuantity = BigDecimal.ZERO;
		BigDecimal newAmount = BigDecimal.ZERO;
		if(ComponentConstant.SUM == action){
			newQuantity = totalQuantity.add(stockQuantity);
			if(newQuantity.compareTo(registerAccountReassignmentTO.getMechanismOperation().getStockQuantity()) > 0){
				return PropertiesConstants.ASSIGNMENT_ACCOUNT_EXCEEDED_QUANTITY;
			}
 			newAmount = totalAmount.add(settlementAmount);
		}else if(ComponentConstant.SUBTRACTION == action){
			newQuantity = totalQuantity.subtract(stockQuantity);
			newAmount = totalAmount.subtract(settlementAmount);
		}
		
		if(ComponentConstant.SALE_ROLE.equals(role)){
			registerAccountReassignmentTO.setSaleQuantity(newQuantity);
			registerAccountReassignmentTO.setSaleAmount(newAmount);
			if(setMax){
				registerAccountReassignmentTO.setMaxSaleQuantity(newQuantity);
			}
		} else if(ComponentConstant.PURCHARSE_ROLE.equals(role)){
			registerAccountReassignmentTO.setPurchaseQuantity(newQuantity);
			registerAccountReassignmentTO.setPurchaseAmount(newAmount);
			if(setMax){
				registerAccountReassignmentTO.setMaxPurchaseQuantity(newQuantity);
			}
		}
		
		return null;
	}

	/**
	 * Show market fact.
	 *
	 * @param hao the hao
	 */
	public void showMarketFact(HolderAccountOperation hao){
		marketFactAssignmentMgmtBean.setViewOnly(false);
		marketFactAssignmentMgmtBean.setMaxQuantity(hao.getStockQuantity());
		marketFactAssignmentMgmtBean.setRemoteCommand("onCloseMarketFact");
		
		if(hao.getIdHolderAccountOperationPk()!=null){
			marketFactAssignmentMgmtBean.setViewOnly(true);
		}
		
		marketFactAssignmentMgmtBean.setLstAccountMarketfacts(NegotiationUtils.convertToMarketFactAccounts(hao.getAccountOperationMarketFacts()));
		if(InstrumentType.FIXED_INCOME.getCode().equals(registerAccountReassignmentTO.getMechanismOperation().getSecurities().getInstrumentType())){
			marketFactAssignmentMgmtBean.setFixedIncome(true);
		}
		marketFactAssignmentMgmtBean.showMarketFact();
		
		registerAccountReassignmentTO.setSelectedAccountOperation(hao);
	}
	
	/**
	 * On market fact close.
	 */
	public void onMarketFactClose(){
		registerAccountReassignmentTO.getSelectedAccountOperation().setAccountOperationMarketFacts(
				NegotiationUtils.convertToSettlementAccountMarketFacts(marketFactAssignmentMgmtBean.getLstAccountMarketfacts(),registerAccountReassignmentTO.getSelectedAccountOperation()));
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		//searchAccountReassignmentTO.setIdMechanism(null);
		searchAccountReassignmentTO.setIdModality(null);
		searchAccountReassignmentTO.setIdParticipant(null);
		searchAccountReassignmentTO.setBallotNumber(null);
		searchAccountReassignmentTO.setSequential(null);
		searchAccountReassignmentTO.setIdCurrency(null);
		searchAccountReassignmentTO.setLstAccountReassignment(null);
		isFixedIncome = Boolean.FALSE;
	}
	
	/**
	 * Clean register.
	 */
	public void cleanRegister(){
		JSFUtilities.resetViewRoot();
		clearRegistrationData();
		isFixedIncome = Boolean.FALSE;
	}
	
	/**
	 * View reassignment detail.
	 *
	 * @param selectedReassignment the selected reassignment
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String viewReassignmentDetail(AccountReassignmentTO selectedReassignment) throws ServiceException{
		
		accountReassignmentTO = selectedReassignment;
		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		showDataMarketFact =  Boolean.FALSE;
		isFixedIncome = Boolean.FALSE;
		return fillData();
	}
	
	/**
	 * Fill data.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String fillData() throws ServiceException{
		if(registerAccountReassignmentTO == null){
			registerAccountReassignmentTO = new RegisterAccountReassignmentTO();
		}
		loadViewParameters();
		
		MechanismOperation mechanismOperation = accountAssignmentFacade.getMechanismOperation(accountReassignmentTO.getIdMechanismOperationPk());
		registerAccountReassignmentTO.setMechanismOperation(mechanismOperation);
		registerAccountReassignmentTO.clearAmounts();
		
		if(InstrumentType.FIXED_INCOME.getCode().equals(registerAccountReassignmentTO.getMechanismOperation().getSecurities().getInstrumentType())) {
			isFixedIncome = Boolean.TRUE;
		}
		
		registerAccountReassignmentTO.setRequestReason(accountReassignmentTO.getRequestRason());
		if(ReassignmentRequestMotiveType.MODIFY_MARKETFACT.getCode().equals(accountReassignmentTO.getRequestRason())){
			showDataMarketFact =  Boolean.TRUE;
		}		
		
		List<Integer> roles = new ArrayList<Integer>();
		if(accountReassignmentTO.getRole() == null){
			roles.add(ComponentConstant.SALE_ROLE);
			roles.add(ComponentConstant.PURCHARSE_ROLE);
			registerAccountReassignmentTO.setCrossOperation(true);
		}else{
			roles.add(accountReassignmentTO.getRole());
			registerAccountReassignmentTO.setCrossOperation(false);
		}
		
		List<ReassignmentRequestDetail> accountOperations = accountAssignmentFacade.getReassignmentDetails(accountReassignmentTO.getIdReassignmentRequest());
		
		for(ReassignmentRequestDetail detail : accountOperations){
			HolderAccountOperation holderAccountOperation = detail.getHolderAccountOperation();
			fillHolderAccountOperation(detail.getHolderAccountOperation());
			if(HolderAccountOperationStateType.CONFIRMED.getCode().equals(detail.getHolderAccountState()) ||
					HolderAccountOperationStateType.REGISTERED.getCode().equals(detail.getHolderAccountState())){
				updateQuantityAssig(holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount(), 
						holderAccountOperation.getRole(), ComponentConstant.SUM, true);
			}
		}
		
		registerAccountReassignmentTO.setLstReassignmentDetail(new GenericDataModel<ReassignmentRequestDetail>(accountOperations));
		
		return "viewReassignment";
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateConfirm() throws ServiceException{
		return validateAction(ReassignmentRequestStateType.CONFIRMED.getCode());
	}
	
	/**
	 * Validate authorize.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateAuthorize() throws ServiceException{
		return validateAction(ReassignmentRequestStateType.AUTHORIZED.getCode());
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateReject() throws ServiceException{
		return validateAction(ReassignmentRequestStateType.REJECTED.getCode());
	}
	
	/**
	 * Validate action.
	 *
	 * @param newState the new state
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	private String validateAction(Integer newState) throws ServiceException {
		if(accountReassignmentTO == null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
			return null;
		}
		
		if(newState.equals(ReassignmentRequestStateType.REJECTED.getCode()) 
					&& (accountReassignmentTO.getIdReassignmentState().equals(ReassignmentRequestStateType.AUTHORIZED.getCode())
							|| accountReassignmentTO.getIdReassignmentState().equals(ReassignmentRequestStateType.REJECTED.getCode()))
				){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_INCORRECT_STATE));
			JSFUtilities.showValidationDialog();
			return null;
		}
		
		if(newState.equals(ReassignmentRequestStateType.CONFIRMED.getCode())
				&& !accountReassignmentTO.getIdReassignmentState().equals(ReassignmentRequestStateType.REGISTERED.getCode())
				){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_INCORRECT_STATE));
			JSFUtilities.showValidationDialog();
			return null;
		}
		
		if(newState.equals(ReassignmentRequestStateType.AUTHORIZED.getCode())){
			if(!ReassignmentRequestStateType.CONFIRMED.getCode().equals(accountReassignmentTO.getIdReassignmentState())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_INCORRECT_STATE));
				JSFUtilities.showValidationDialog();
				return null;
			}
		}
		
		if(ReassignmentRequestStateType.REJECTED.getCode().equals(newState)){
			setViewOperationType(ViewOperationsType.REJECT.getCode());
		} else if(ReassignmentRequestStateType.CONFIRMED.getCode().equals(newState)){
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		}else if(ReassignmentRequestStateType.AUTHORIZED.getCode().equals(newState)){
			setViewOperationType(ViewOperationsType.AUTHORIZE.getCode());
		}
		
		return fillData();
		
	}

	/**
	 * Before action.
	 */
	public void beforeAction(){
		if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
					PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_REJECT_CONFIRM, new Object[]{accountReassignmentTO.getIdReassignmentRequest()}));
		} else if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_CONFIRM_CONFIRM, new Object[]{accountReassignmentTO.getIdReassignmentRequest()}));
		} else if(getViewOperationType().equals(ViewOperationsType.AUTHORIZE.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_AUTHORIZE),
					PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_CONFIRM_AUTHORIZE, new Object[]{accountReassignmentTO.getIdReassignmentRequest()}));
		}
		
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		try{
			if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
				rejectReassignmentRequest();
			} else if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
				confirmReassignmentRequest();				
			} else if(getViewOperationType().equals(ViewOperationsType.AUTHORIZE.getCode())){
				authorizeReassignmentRequest();				
			} 
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Confirm reassignment request.
	 *
	 * @throws ServiceException the service exception
	 */
	public void authorizeReassignmentRequest() throws ServiceException{
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountReassignmentTO.getRequestReason())){
			if(ReassignmentRequestMotiveType.MODIFY_HOLDER_ACCOUNT.getCode().equals(registerAccountReassignmentTO.getRequestReason())) {
				accountAssignmentFacade.authorizeReassignmentRequest(accountReassignmentTO.getIdReassignmentRequest(), registerAccountReassignmentTO.getMechanismOperation(), 
						registerAccountReassignmentTO.getLstReassignmentDetail().getDataList());
			} else {
				accountAssignmentFacade.authorizeReassignmentRequestMarketFact(accountReassignmentTO.getIdReassignmentRequest(), 
						registerAccountReassignmentTO.getMechanismOperation(), 
						registerAccountReassignmentTO.getLstReassignmentDetail().getDataList(), isFixedIncome);
			}
		}
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_REASSIGN_CONFIRM.getCode());
		Object [] parId ={accountReassignmentTO.getIdReassignmentRequest()};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parId);
				
		BusinessProcess objBusinessProcess = new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode());
		Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(ComponentConstant.SETTLEMENT_SCHEMA, SettlementSchemaType.NET.getCode());
		batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_CONFIRM_AUTHORIZE, 
						new Object[]{accountReassignmentTO.getIdReassignmentRequest()}));
		searchAccountReassignments();
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}

	/**
	 * Confirm reassignment request.
	 *
	 * @throws ServiceException the service exception
	 */
	public void confirmReassignmentRequest() throws ServiceException{
		
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountReassignmentTO.getRequestReason())){
			if(ReassignmentRequestMotiveType.MODIFY_HOLDER_ACCOUNT.getCode().equals(registerAccountReassignmentTO.getRequestReason())) {
				accountAssignmentFacade.confirmReassignmentRequest(accountReassignmentTO.getIdReassignmentRequest(), registerAccountReassignmentTO.getMechanismOperation(), 
						registerAccountReassignmentTO.getLstReassignmentDetail().getDataList());
			} else {
				accountAssignmentFacade.confirmReassignmentRequestMarketFact(accountReassignmentTO.getIdReassignmentRequest(), registerAccountReassignmentTO.getMechanismOperation(), 
						registerAccountReassignmentTO.getLstReassignmentDetail().getDataList(), isFixedIncome);
			}
		}		
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_CONFIRM_SUCCESS, 
						new Object[]{accountReassignmentTO.getIdReassignmentRequest()}));
		searchAccountReassignments();
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}

	/**
	 * Reject reassignment request.
	 *
	 * @throws ServiceException the service exception
	 */
	public void rejectReassignmentRequest() throws ServiceException{
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountReassignmentTO.getRequestReason())){
			if(ReassignmentRequestMotiveType.MODIFY_HOLDER_ACCOUNT.getCode().equals(registerAccountReassignmentTO.getRequestReason())) {
				accountAssignmentFacade.rejectReassignmentRequest(accountReassignmentTO.getIdReassignmentRequest(), registerAccountReassignmentTO.getMechanismOperation(), 
						registerAccountReassignmentTO.getLstReassignmentDetail().getDataList());
			} else {
				accountAssignmentFacade.rejectReassignmentRequestMarketFact(accountReassignmentTO.getIdReassignmentRequest(), registerAccountReassignmentTO.getMechanismOperation(), 
						registerAccountReassignmentTO.getLstReassignmentDetail().getDataList(), isFixedIncome);
			}
		}		
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.REASSIGNMENT_REQUEST_REJECT_SUCCESS, 
						new Object[]{accountReassignmentTO.getIdReassignmentRequest()}));
		searchAccountReassignments();
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * On change request reason.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeRequestReason() throws ServiceException{
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountReassignmentTO.getRequestReason())){		
			isFixedIncome = Boolean.FALSE;
			if(ReassignmentRequestMotiveType.MODIFY_HOLDER_ACCOUNT.getCode().equals(registerAccountReassignmentTO.getRequestReason())){
				onSelectMechanismOperation();
			} else {
				onSelectMechanismOperationByMarketFact();				
			}
		}		
	}
	
	/**
	 * On select mechanism operation by market fact.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onSelectMechanismOperationByMarketFact() throws ServiceException {
		registerAccountReassignmentTO.setLstAccountOperations(null);
		registerAccountReassignmentTO.setLstSelectedAccounts(null);
		registerAccountReassignmentTO.setHolderAccountOperation(new HolderAccountOperation());
		registerAccountReassignmentTO.setLstSettlementAccountMarketfact(null);				
		if(InstrumentType.FIXED_INCOME.getCode().equals(registerAccountReassignmentTO.getMechanismOperation().getSecurities().getInstrumentType())) {
			isFixedIncome = Boolean.TRUE;
		}
		try{
			if(registerAccountReassignmentTO.getSelectedOperation() != null){
				
				accountAssignmentFacade.checkPreviousReassignmentRequest(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanismOperationPk(), 
						registerAccountReassignmentTO.getIdParticipant());
				
				accountAssignmentFacade.checkAssignmentProcess(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanism(),
						registerAccountReassignmentTO.getSelectedOperation().getIdModality(),
						AssignmentProcessStateType.CLOSED.getCode(),
						CommonsUtilities.currentDate());
				
				accountAssignmentFacade.checkPendingSettlementProcess(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanism(),
						registerAccountReassignmentTO.getSelectedOperation().getIdModality(),
						registerAccountReassignmentTO.getSelectedOperation().getSettlementCurrency(),
						registerAccountReassignmentTO.getSelectedOperation().getCashSettlementDate());
				
				accountAssignmentFacade.checkFinishedSettlementProcess(
						registerAccountReassignmentTO.getSelectedOperation().getIdMechanism(),
						registerAccountReassignmentTO.getSelectedOperation().getIdModality(),
						registerAccountReassignmentTO.getSelectedOperation().getSettlementCurrency(),
						registerAccountReassignmentTO.getSelectedOperation().getCashSettlementDate(),
						SettlementScheduleType.MELOR.getCode());
				
				MechanismOperation mechanismOperation = accountAssignmentFacade.getMechanismOperation(registerAccountReassignmentTO.getSelectedOperation().getIdMechanismOperationPk());
				registerAccountReassignmentTO.setMechanismOperation(mechanismOperation);
				registerAccountReassignmentTO.clearAmounts();
				
				List<Integer> lstRoles = new ArrayList<Integer>();
				Long idPurchaseParticipant = registerAccountReassignmentTO.getMechanismOperation().getBuyerParticipant().getIdParticipantPk();
				Long idSaleParticipant = registerAccountReassignmentTO.getMechanismOperation().getSellerParticipant().getIdParticipantPk();
				if(idPurchaseParticipant.equals(idSaleParticipant)){
					registerAccountReassignmentTO.setCrossOperation(true);					
					lstRoles.add(ComponentConstant.SALE_ROLE);
				} else {					
					registerAccountReassignmentTO.setCrossOperation(false);
					if(idPurchaseParticipant.equals(registerAccountReassignmentTO.getIdParticipant())){
						registerAccountReassignmentTO.setRole(ComponentConstant.PURCHARSE_ROLE);
					}else if(idSaleParticipant.equals(registerAccountReassignmentTO.getIdParticipant())){
						registerAccountReassignmentTO.setRole(ComponentConstant.SALE_ROLE);
					}
					if(ComponentConstant.SALE_ROLE.equals(registerAccountReassignmentTO.getRole())){
						lstRoles.add(registerAccountReassignmentTO.getRole());
					}																				
				}
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstRoles)){
					
					// Get list Settlement Operation
					
					List<SettlementAccountMarketfact> lstSettlementAccountMarketfact = accountAssignmentFacade.getSettlementAccountMarketfactByReassignment(
							registerAccountReassignmentTO.getMechanismOperation().getIdMechanismOperationPk(), lstRoles, registerAccountReassignmentTO.getIdParticipant());
					
					if(Validations.validateListIsNotNullAndNotEmpty(lstSettlementAccountMarketfact)){									
						lstSettlementAccountMarketfactAux = new ArrayList<SettlementAccountMarketfact>();
						try {
							for(SettlementAccountMarketfact settlementAccountMarketfact : lstSettlementAccountMarketfact){
								lstSettlementAccountMarketfactAux.add(settlementAccountMarketfact.clone());
								fillHolderAccountOperation(settlementAccountMarketfact.getSettlementAccountOperation().getHolderAccountOperation());
								updateQuantityAssig(settlementAccountMarketfact.getSettlementAccountOperation().getHolderAccountOperation().getStockQuantity(), settlementAccountMarketfact.getSettlementAccountOperation().getHolderAccountOperation().getSettlementAmount(), 
										settlementAccountMarketfact.getSettlementAccountOperation().getHolderAccountOperation().getRole(), ComponentConstant.SUM, true);
							}					
							registerAccountReassignmentTO.setLstSettlementAccountMarketfact(new GenericDataModel<SettlementAccountMarketfact>(lstSettlementAccountMarketfact));
						} catch (CloneNotSupportedException e) {							
							e.printStackTrace();
						}										
					}
				} else {
					registerAccountReassignmentTO.setLstSettlementAccountMarketfact(null);
				}
			}
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			registerAccountReassignmentTO.setSelectedOperation(null);
		}
	}		
	
	/**
	 * Before remove account Market Fact.
	 *
	 * @param objSettlementAccountMarketfact the obj settlement account marketfact
	 */
	public void beforeRemoveAccountMarketFact(SettlementAccountMarketfact objSettlementAccountMarketfact){
		HolderAccountOperation objHolderAccountOperation = objSettlementAccountMarketfact.getSettlementAccountOperation().getHolderAccountOperation();		
		if(objHolderAccountOperation != null) {
			try {
				accountAssignmentFacade.checkExistChainedAccount(objHolderAccountOperation.getIdHolderAccountOperationPk());				
			} catch (ServiceException e) {
				objSettlementAccountMarketfact.setSelected(Boolean.FALSE);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}				
	}

	/**
	 * Gets the cui holder.
	 *
	 * @param holderAccount the holder account
	 * @return the cui holder
	 */
	public static String getCuiHolder(HolderAccount holderAccount){
		return NegotiationUtils.getCuiHolder(holderAccount);
	}
	
	/**
	 * Gets the role description.
	 *
	 * @param role the role
	 * @return the role description
	 */
	public String getRoleDescription(Integer role){
		if(role != null){
			return ParticipantRoleType.get(role).getValue();
		}
		return null;
	}
	
	/**
	 * Gets the search account reassignment to.
	 *
	 * @return the search account reassignment to
	 */
	public SearchAccountReassignmentTO getSearchAccountReassignmentTO() {
		return searchAccountReassignmentTO;
	}

	/**
	 * Sets the search account reassignment to.
	 *
	 * @param searchAccountReassignmentTO the new search account reassignment to
	 */
	public void setSearchAccountReassignmentTO(
			SearchAccountReassignmentTO searchAccountReassignmentTO) {
		this.searchAccountReassignmentTO = searchAccountReassignmentTO;
	}

	/**
	 * Gets the register account reassignment to.
	 *
	 * @return the register account reassignment to
	 */
	public RegisterAccountReassignmentTO getRegisterAccountReassignmentTO() {
		return registerAccountReassignmentTO;
	}

	/**
	 * Sets the register account reassignment to.
	 *
	 * @param registerAccountReassignmentTO the new register account reassignment to
	 */
	public void setRegisterAccountReassignmentTO(
			RegisterAccountReassignmentTO registerAccountReassignmentTO) {
		this.registerAccountReassignmentTO = registerAccountReassignmentTO;
	}

	/**
	 * Checks if is bl market fact.
	 *
	 * @return true, if is bl market fact
	 */
	public boolean isBlMarketFact() {
		return blMarketFact;
	}

	/**
	 * Sets the bl market fact.
	 *
	 * @param blMarketFact the new bl market fact
	 */
	public void setBlMarketFact(boolean blMarketFact) {
		this.blMarketFact = blMarketFact;
	}

	/**
	 * Gets the account reassignment to.
	 *
	 * @return the account reassignment to
	 */
	public AccountReassignmentTO getAccountReassignmentTO() {
		return accountReassignmentTO;
	}

	/**
	 * Sets the account reassignment to.
	 *
	 * @param accountReassignmentTO the new account reassignment to
	 */
	public void setAccountReassignmentTO(AccountReassignmentTO accountReassignmentTO) {
		this.accountReassignmentTO = accountReassignmentTO;
	}

	/**
	 * Checks if is fixed income.
	 *
	 * @return the isFixedIncome
	 */
	public boolean isFixedIncome() {
		return isFixedIncome;
	}

	/**
	 * Sets the fixed income.
	 *
	 * @param isFixedIncome the isFixedIncome to set
	 */
	public void setFixedIncome(boolean isFixedIncome) {
		this.isFixedIncome = isFixedIncome;
	}

	/**
	 * Gets the lst settlement account marketfact aux.
	 *
	 * @return the lst settlement account marketfact aux
	 */
	public List<SettlementAccountMarketfact> getLstSettlementAccountMarketfactAux() {
		return lstSettlementAccountMarketfactAux;
	}

	/**
	 * Sets the lst settlement account marketfact aux.
	 *
	 * @param lstSettlementAccountMarketfactAux the new lst settlement account marketfact aux
	 */
	public void setLstSettlementAccountMarketfactAux(
			List<SettlementAccountMarketfact> lstSettlementAccountMarketfactAux) {
		this.lstSettlementAccountMarketfactAux = lstSettlementAccountMarketfactAux;
	}

	/**
	 * Checks if is show data market fact.
	 *
	 * @return the showDataMarketFact
	 */
	public boolean isShowDataMarketFact() {
		return showDataMarketFact;
	}

	/**
	 * Sets the show data market fact.
	 *
	 * @param showDataMarketFact the showDataMarketFact to set
	 */
	public void setShowDataMarketFact(boolean showDataMarketFact) {
		this.showDataMarketFact = showDataMarketFact;
	}
	
}
