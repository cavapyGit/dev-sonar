package com.pradera.negotiations.assignmentrequest.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.AssignmentProcessOpenFile;

public class AssignmentOpenModelTO implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private Long id;
    private String motiveOpen;
    private Date registryDate;
    private Integer stateRequest;
    private String stateRequestText;
    private AssignmentProcess idAssignmentProcessFk;
    
    /** todas las papeletas seleccionadas */
    private String ballotsNumber;
    private String userRegisterAssignmentOpen;
    private String userConfirmAssignmentOpen;

    private Date initialDate;
    private Date finalDate;
    
    private List<AssignmentProcessOpenFile> lstAssignmentProcessOpenFile=new ArrayList<>();
    
    private List<ParameterTable> lstState=new ArrayList<>();
    private List<Long> lstIdMechanismOperationToOpen=new ArrayList<>();
    private List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTO=new ArrayList<>();
    private List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTOFiltered=new ArrayList<>();
    //private List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTOSelected=new ArrayList<>();
    private List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTOAdded=new ArrayList<>();
    
    private boolean selectAllMCN;
    
    // para las pistas de auditoria manuales
    private String auditUserName;
    private String auditIpAddress;
    private Long auditIdPrivilege;
    
    //TODO constructor para la consulta JPQL
    
    /**
     * para la clonacion de objetos, asi evitamos la referencia
     */
    @Override
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMotiveOpen() {
		return motiveOpen;
	}

	public void setMotiveOpen(String motiveOpen) {
		this.motiveOpen = motiveOpen;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getStateRequest() {
		return stateRequest;
	}

	public void setStateRequest(Integer stateRequest) {
		this.stateRequest = stateRequest;
	}

	public String getStateRequestText() {
		return stateRequestText;
	}

	public void setStateRequestText(String stateRequestText) {
		this.stateRequestText = stateRequestText;
	}

	public AssignmentProcess getIdAssignmentProcessFk() {
		return idAssignmentProcessFk;
	}

	public void setIdAssignmentProcessFk(AssignmentProcess idAssignmentProcessFk) {
		this.idAssignmentProcessFk = idAssignmentProcessFk;
	}

	public String getBallotsNumber() {
		return ballotsNumber;
	}

	public void setBallotsNumber(String ballotsNumber) {
		this.ballotsNumber = ballotsNumber;
	}

	public String getUserRegisterAssignmentOpen() {
		return userRegisterAssignmentOpen;
	}

	public void setUserRegisterAssignmentOpen(String userRegisterAssignmentOpen) {
		this.userRegisterAssignmentOpen = userRegisterAssignmentOpen;
	}

	public String getUserConfirmAssignmentOpen() {
		return userConfirmAssignmentOpen;
	}

	public void setUserConfirmAssignmentOpen(String userConfirmAssignmentOpen) {
		this.userConfirmAssignmentOpen = userConfirmAssignmentOpen;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public List<AssignmentProcessOpenFile> getLstAssignmentProcessOpenFile() {
		return lstAssignmentProcessOpenFile;
	}

	public void setLstAssignmentProcessOpenFile(List<AssignmentProcessOpenFile> lstAssignmentProcessOpenFile) {
		this.lstAssignmentProcessOpenFile = lstAssignmentProcessOpenFile;
	}

	public List<ParameterTable> getLstState() {
		return lstState;
	}

	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	public List<Long> getLstIdMechanismOperationToOpen() {
		return lstIdMechanismOperationToOpen;
	}

	public void setLstIdMechanismOperationToOpen(List<Long> lstIdMechanismOperationToOpen) {
		this.lstIdMechanismOperationToOpen = lstIdMechanismOperationToOpen;
	}

	public List<MechanismOperationForOpenTO> getLstMechanismOperationForOpenTO() {
		return lstMechanismOperationForOpenTO;
	}

	public void setLstMechanismOperationForOpenTO(List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTO) {
		this.lstMechanismOperationForOpenTO = lstMechanismOperationForOpenTO;
	}

	public List<MechanismOperationForOpenTO> getLstMechanismOperationForOpenTOFiltered() {
		return lstMechanismOperationForOpenTOFiltered;
	}

	public void setLstMechanismOperationForOpenTOFiltered(
			List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTOFiltered) {
		this.lstMechanismOperationForOpenTOFiltered = lstMechanismOperationForOpenTOFiltered;
	}

	public List<MechanismOperationForOpenTO> getLstMechanismOperationForOpenTOAdded() {
		return lstMechanismOperationForOpenTOAdded;
	}

	public void setLstMechanismOperationForOpenTOAdded(
			List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTOAdded) {
		this.lstMechanismOperationForOpenTOAdded = lstMechanismOperationForOpenTOAdded;
	}

	public boolean isSelectAllMCN() {
		return selectAllMCN;
	}

	public void setSelectAllMCN(boolean selectAllMCN) {
		this.selectAllMCN = selectAllMCN;
	}

	public String getAuditUserName() {
		return auditUserName;
	}

	public void setAuditUserName(String auditUserName) {
		this.auditUserName = auditUserName;
	}

	public String getAuditIpAddress() {
		return auditIpAddress;
	}

	public void setAuditIpAddress(String auditIpAddress) {
		this.auditIpAddress = auditIpAddress;
	}

	public Long getAuditIdPrivilege() {
		return auditIdPrivilege;
	}

	public void setAuditIdPrivilege(Long auditIdPrivilege) {
		this.auditIdPrivilege = auditIdPrivilege;
	}

}
