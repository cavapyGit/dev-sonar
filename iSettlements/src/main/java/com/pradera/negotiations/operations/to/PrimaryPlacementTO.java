package com.pradera.negotiations.operations.to;

import java.io.Serializable;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrimaryPlacementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class PrimaryPlacementTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The placement segment. */
	private PlacementSegment placementSegment; // used for primary placement validation
	
	/** The issuer holder account. */
	private HolderAccount issuerHolderAccount; // used for negotiations in Primary Placement
	
	/** The placement participant. */
	private Participant placementParticipant; // user for negotiations in Primary Placement

	/**
	 * Gets the placement segment.
	 *
	 * @return the placementSegment
	 */
	public PlacementSegment getPlacementSegment() {
		return placementSegment;
	}

	/**
	 * Sets the placement segment.
	 *
	 * @param placementSegment the placementSegment to set
	 */
	public void setPlacementSegment(PlacementSegment placementSegment) {
		this.placementSegment = placementSegment;
	}

	/**
	 * Gets the issuer holder account.
	 *
	 * @return the issuerHolderAccount
	 */
	public HolderAccount getIssuerHolderAccount() {
		return issuerHolderAccount;
	}

	/**
	 * Sets the issuer holder account.
	 *
	 * @param issuerHolderAccount the issuerHolderAccount to set
	 */
	public void setIssuerHolderAccount(HolderAccount issuerHolderAccount) {
		this.issuerHolderAccount = issuerHolderAccount;
	}

	/**
	 * Gets the placement participant.
	 *
	 * @return the placementParticipant
	 */
	public Participant getPlacementParticipant() {
		return placementParticipant;
	}

	/**
	 * Sets the placement participant.
	 *
	 * @param placementParticipant the placementParticipant to set
	 */
	public void setPlacementParticipant(Participant placementParticipant) {
		this.placementParticipant = placementParticipant;
	}
	
	
	

}
