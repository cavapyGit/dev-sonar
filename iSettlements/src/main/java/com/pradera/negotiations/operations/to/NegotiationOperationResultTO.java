/*
 * 
 */
package com.pradera.negotiations.operations.to;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class NegotiationOperationResultTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant is blocked. */
	private Boolean participantSellerIsBlocked = Boolean.FALSE;
	
	/** The participant buyer is blocked. */
	private Boolean participantBuyerIsBlocked = Boolean.FALSE;
	
	/** The security is blocked. */
	private Boolean securityIsBlocked = Boolean.FALSE;
	
	/** The issuer is blocked. */
	private Boolean issuerIsBlocked = Boolean.FALSE;
	
	/** The issuance is blocked. */
	private Boolean issuanceIsBlocked = Boolean.FALSE;
	
	/** The holder account is blocked. */
	private Boolean holderAccountSellIsBlocked = Boolean.FALSE;
	
	/** The holder account buy is blocked. */
	private Boolean holderAccountBuyIsBlocked = Boolean.FALSE;
	
	/** The assignment process is closed *. */
	private Boolean assignmentProcessIsClosed = Boolean.FALSE;
	
	/** The security mechanis modality is blocked. */
	private Boolean securityMechanisModalityIsBlocked = Boolean.FALSE;
	
	/** The participant mec mod is blocked. */
	private Boolean participantSellMecModIsBlocked = Boolean.FALSE;
	
	/** The participant buy mec mod is blocked. */
	private Boolean participantBuyMecModIsBlocked = Boolean.FALSE;
	
	/** The holder is blocked. */
	private Boolean holderIsBlocked = Boolean.FALSE;

	/** The operation exists. */
	private Boolean operationExists = Boolean.FALSE;
	
	/**
	 * Gets the participant is blocked.
	 *
	 * @return the participant is blocked
	 */
	public Boolean getParticipantSellerIsBlocked() {
		return participantSellerIsBlocked;
	}

	/**
	 * Sets the participant is blocked.
	 *
	 * @param participantIsBlocked the new participant is blocked
	 */
	public void setParticipantSellerIsBlocked(Boolean participantIsBlocked) {
		this.participantSellerIsBlocked = participantIsBlocked;
	}

	/**
	 * Gets the security is blocked.
	 *
	 * @return the security is blocked
	 */
	public Boolean getSecurityIsBlocked() {
		return securityIsBlocked;
	}

	/**
	 * Sets the security is blocked.
	 *
	 * @param securityIsBlocked the new security is blocked
	 */
	public void setSecurityIsBlocked(Boolean securityIsBlocked) {
		this.securityIsBlocked = securityIsBlocked;
	}

	/**
	 * Gets the issuer is blocked.
	 *
	 * @return the issuer is blocked
	 */
	public Boolean getIssuerIsBlocked() {
		return issuerIsBlocked;
	}

	/**
	 * Sets the issuer is blocked.
	 *
	 * @param issuerIsBlocked the new issuer is blocked
	 */
	public void setIssuerIsBlocked(Boolean issuerIsBlocked) {
		this.issuerIsBlocked = issuerIsBlocked;
	}

	/**
	 * Gets the issuance is blocked.
	 *
	 * @return the issuance is blocked
	 */
	public Boolean getIssuanceIsBlocked() {
		return issuanceIsBlocked;
	}

	/**
	 * Sets the issuance is blocked.
	 *
	 * @param issuanceIsBlocked the new issuance is blocked
	 */
	public void setIssuanceIsBlocked(Boolean issuanceIsBlocked) {
		this.issuanceIsBlocked = issuanceIsBlocked;
	}

	/**
	 * Gets the holder account is blocked.
	 *
	 * @return the holder account is blocked
	 */
	public Boolean getHolderAccountSellIsBlocked() {
		return holderAccountSellIsBlocked;
	}

	/**
	 * Sets the holder account is blocked.
	 *
	 * @param holderAccountIsBlocked the new holder account is blocked
	 */
	public void setHolderAccountSellIsBlocked(Boolean holderAccountIsBlocked) {
		this.holderAccountSellIsBlocked = holderAccountIsBlocked;
	}

	/**
	 * Gets the assignment process is closed.
	 *
	 * @return the assignment process is closed
	 */
	public Boolean getAssignmentProcessIsClosed() {
		return assignmentProcessIsClosed;
	}
	/**
	 * Sets the assignment process is closed.
	 *
	 * @param assignmentProcessClosed the new assignment process is closed
	 */
	public void setAssignmentProcessIsClosed(Boolean assignmentProcessClosed) {
		this.assignmentProcessIsClosed = assignmentProcessClosed;
	}
	/**
	 * Gets the security mechanis modality is blocked.
	 *
	 * @return the security mechanis modality is blocked
	 */
	public Boolean getSecurityMechanisModalityIsBlocked() {
		return securityMechanisModalityIsBlocked;
	}
	/**
	 * Sets the security mechanis modality is blocked.
	 *
	 * @param securityMechanisModalityIsBlocked the new security mechanis modality is blocked
	 */
	public void setSecurityMechanisModalityIsBlocked(
			Boolean securityMechanisModalityIsBlocked) {
		this.securityMechanisModalityIsBlocked = securityMechanisModalityIsBlocked;
	}
	/**
	 * Gets the participant mec mod is blocked.
	 *
	 * @return the participant mec mod is blocked
	 */
	public Boolean getParticipantSellMecModIsBlocked() {
		return participantSellMecModIsBlocked;
	}
	/**
	 * Sets the participant mec mod is blocked.
	 *
	 * @param participantMecModIsBlocked the new participant mec mod is blocked
	 */
	public void setParticipantSellMecModIsBlocked(Boolean participantMecModIsBlocked) {
		this.participantSellMecModIsBlocked = participantMecModIsBlocked;
	}
	/**
	 * Gets the participant buyer is blocked.
	 *
	 * @return the participant buyer is blocked
	 */
	public Boolean getParticipantBuyerIsBlocked() {
		return participantBuyerIsBlocked;
	}
	/**
	 * Sets the participant buyer is blocked.
	 *
	 * @param participantBuyerIsBlocked the new participant buyer is blocked
	 */
	public void setParticipantBuyerIsBlocked(Boolean participantBuyerIsBlocked) {
		this.participantBuyerIsBlocked = participantBuyerIsBlocked;
	}
	/**
	 * Gets the participant buy mec mod is blocked.
	 *
	 * @return the participant buy mec mod is blocked
	 */
	public Boolean getParticipantBuyMecModIsBlocked() {
		return participantBuyMecModIsBlocked;
	}
	/**
	 * Sets the participant buy mec mod is blocked.
	 *
	 * @param participantBuyMecModIsBlocked the new participant buy mec mod is blocked
	 */
	public void setParticipantBuyMecModIsBlocked(Boolean participantBuyMecModIsBlocked) {
		this.participantBuyMecModIsBlocked = participantBuyMecModIsBlocked;
	}
	/**
	 * Gets the holder account buy is blocked.
	 *
	 * @return the holder account buy is blocked
	 */
	public Boolean getHolderAccountBuyIsBlocked() {
		return holderAccountBuyIsBlocked;
	}
	/**
	 * Sets the holder account buy is blocked.
	 *
	 * @param holderAccountBuyIsBlocked the new holder account buy is blocked
	 */
	public void setHolderAccountBuyIsBlocked(Boolean holderAccountBuyIsBlocked) {
		this.holderAccountBuyIsBlocked = holderAccountBuyIsBlocked;
	}
	/**
	 * Gets the holder is blocked.
	 *
	 * @return the holder is blocked
	 */
	public Boolean getHolderIsBlocked() {
		return holderIsBlocked;
	}
	/**
	 * Sets the holder is blocked.
	 *
	 * @param holderIsBlocked the new holder is blocked
	 */
	public void setHolderIsBlocked(Boolean holderIsBlocked) {
		this.holderIsBlocked = holderIsBlocked;
	}

	/**
	 * Gets the operation exists.
	 *
	 * @return the operationExists
	 */
	public Boolean getOperationExists() {
		return operationExists;
	}

	/**
	 * Sets the operation exists.
	 *
	 * @param operationExists the operationExists to set
	 */
	public void setOperationExists(Boolean operationExists) {
		this.operationExists = operationExists;
	}
}