package com.pradera.negotiations.operations.calculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class OperationPriceCalculatorServiceBean extends CrudDaoServiceBean {

	public ProgramInterestCoupon getMostRecentPayedCouponBySecurityPk(String idSecurityCodePk, Date operationDate ) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select pic from ProgramInterestCoupon pic");
			sb.append(" where pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
			//sb.append("  and pic.situationProgramInterest in :lstPayedStates");
			sb.append("  and TRUNC(pic.experitationDate) <= :operationDate");
			sb.append(" order by pic.experitationDate desc");
			
			Query q = em.createQuery(sb.toString());
			
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			q.setParameter("operationDate", CommonsUtilities.truncateDateTime(operationDate));
			
			/*List<Integer> lstPayedStates = new ArrayList<Integer>();
			lstPayedStates.add(ProgramScheduleSituationType.EDV_PAID.getCode());
			lstPayedStates.add(ProgramScheduleSituationType.ISSUER_PAID.getCode());
			q.setParameter("lstPayedStates", lstPayedStates);*/
			
			q.setMaxResults(1);
			
			return (ProgramInterestCoupon) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public ProgramInterestCoupon getNextCoupon(String idSecurityCodePk, Date operationDate) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select pic from ProgramInterestCoupon pic");
			sb.append(" where pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
			sb.append("  and pic.situationProgramInterest in :lstPayedStates");
			sb.append("  and trunc(pic.experitationDate) >= :operationDate");
			sb.append(" order by pic.experitationDate asc");
			
			Query q = em.createQuery(sb.toString());
			
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			q.setParameter("operationDate", CommonsUtilities.truncateDateTime(operationDate));
			
			List<Integer> lstPayedStates = new ArrayList<Integer>();
			lstPayedStates.add(ProgramScheduleSituationType.PENDING.getCode());
			q.setParameter("lstPayedStates", lstPayedStates);
			
			q.setMaxResults(1);
			
			return (ProgramInterestCoupon) q.getSingleResult();
		
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public BigDecimal getInterestAmountFromDateRange(String idSecurityCodePk, Date operationDate, Date realTermDate) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select sum(pic.couponAmount) from ProgramInterestCoupon pic");
			sb.append(" where pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
			sb.append("  and trunc(pic.experitationDate) > :operationDate and trunc(pic.experitationDate) <= :realTermDate");
			
			Query q = em.createQuery(sb.toString());
			
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			q.setParameter("operationDate", CommonsUtilities.truncateDateTime(operationDate));
			q.setParameter("realTermDate", CommonsUtilities.truncateDateTime(realTermDate));
			
			return (BigDecimal) q.getSingleResult();	
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
	}
	
	public BigDecimal getAmoritzationAmountFromDateRange(String idSecurityCodePk, Date operationDate, Date realTermDate) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select sum(pac.amortizationAmount) from ProgramAmortizationCoupon pac");
			sb.append(" where pac.amortizationPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
			sb.append("  and trunc(pac.expirationDate) > :operationDate and trunc(pac.expirationDate) <= :realTermDate");
			
			Query q = em.createQuery(sb.toString());
			
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			q.setParameter("operationDate", CommonsUtilities.truncateDateTime(operationDate));
			q.setParameter("realTermDate", CommonsUtilities.truncateDateTime(realTermDate));
			
			return (BigDecimal) q.getSingleResult();	
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
	}
}
