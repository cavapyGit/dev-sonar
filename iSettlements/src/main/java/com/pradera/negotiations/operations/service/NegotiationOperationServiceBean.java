package com.pradera.negotiations.operations.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.SwiftComponent;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticipaStruct;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentDetailStateType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.AccountTradeRequest;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.ParticipantAssignment;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.type.SettlementProcessType;
import com.pradera.negotiations.operations.to.NegotiationOperationResultTO;
import com.pradera.negotiations.operations.to.NegotiationOperationTO;
import com.pradera.negotiations.operations.to.PrimaryPlacementTO;
import com.pradera.negotiations.otcoperations.view.OtcOperationUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class NegotiationOperationServiceBean.
 *
 * @author Pradera Technologies
 * This class is a generic service for OTC and MCN Operation queries
 */
@Stateless
public class NegotiationOperationServiceBean extends CrudDaoServiceBean {
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(NegotiationOperationServiceBean.class);

	/** The account component service bean. */
	@EJB
	private HolderAccountComponentServiceBean accountComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/**
	 * Validate secundary on reporto.
	 *
	 * @param idReportOperation the id report operation
	 * @throws ServiceException the service exception
	 */
	public void validateSecundaryOnReporto(Long idReportOperation) throws ServiceException{
		List<Integer> operationsStates = Arrays.asList(
										OtcOperationStateType.REGISTERED.getCode(),OtcOperationStateType.APROVED.getCode(),OtcOperationStateType.REVIEWED.getCode(),
										MechanismOperationStateType.REGISTERED_STATE.getCode(),MechanismOperationStateType.ASSIGNED_STATE.getCode());
		StringBuilder queryReport = new StringBuilder();
		queryReport.append("SELECT count(mecOpe) FROM MechanismOperation mecOpe WHERE mecOpe.referenceOperation.idMechanismOperationPk = :iRepoOperationParam AND mecOpe.operationState IN :operationStatesParam");
		Query query = em.createQuery(queryReport.toString());
		query.setParameter("operationStatesParam", operationsStates);
		query.setParameter("iRepoOperationParam", idReportOperation);
		Long quantity = 0L;
		try{
			quantity = (Long) query.getSingleResult();
		}catch(NoResultException ex){
			return;
		}
		if(!quantity.equals(0L)){
			throw new ServiceException(ErrorServiceType.OPERATION_SECUNDARY_EXIST_REPORT);
		}
	}
	
	/**
	 * Gets the operation by reference number.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @return the operation by reference number
	 * @throws ServiceException the service exception
	 */
	public boolean chkOtcOperationReferenceNumber(MechanismOperation mechanismOperation) throws ServiceException{
		String referenceNumber = mechanismOperation.getReferenceNumber().trim();
		Date operationDate = mechanismOperation.getOperationDate();
		Long mechanismId = mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		Long sellerPart = mechanismOperation.getSellerParticipant().getIdParticipantPk();
		String queryValidate = "Select nvl(count(mecOp.idMechanismOperationPk),0) " +
				" From MechanismOperation mecOp " +
				" Where trunc(mecOp.operationDate) = trunc(:dateParam) AND" +
				" mecOp.referenceNumber = :referenceParam AND" +
				" mecOp.mechanisnModality.id.idNegotiationMechanismPk = :mechanismParam AND "+
				" mecOp.sellerParticipant.idParticipantPk  = :sellerPart and " +
				" mecOp.operationState not in :states  " ;
		
		Query query = em.createQuery(queryValidate);
		
		List<Integer> otcStates = Arrays.asList(
				OtcOperationStateType.ANULATE.getCode(),
				OtcOperationStateType.CANCELED.getCode(),
				OtcOperationStateType.REJECTED.getCode(),	
				MechanismOperationStateType.CANCELED_STATE.getCode());
		
		query.setParameter("sellerPart", sellerPart);
		query.setParameter("states", otcStates);
		query.setParameter("dateParam", operationDate);
		query.setParameter("referenceParam", referenceNumber);
		query.setParameter("mechanismParam", mechanismId);
		
		Long count =  (Long) query.getSingleResult();
		if(count > 0l){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Validate otc operation state.
	 *
	 * @param operation the operation
	 * @param stateToValidateList the state to validate list
	 * @throws ServiceException the service exception
	 */
	public void validateOtcOperationState(MechanismOperation operation, List<Integer> stateToValidateList) throws ServiceException{
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("idTradeOperation", operation.getIdMechanismOperationPk());
		TradeOperation operationState = findObjectByNamedQuery(TradeOperation.OPERATION_STATE, param);
		if(!stateToValidateList.contains(operationState.getOperationState())){
			throw new ServiceException(ErrorServiceType.OTC_OPERATION_STATE);
		}
	}
	
	/**
	 * Validate sirtex operation state.
	 *
	 * @param operationID the operation id
	 * @param stateToValidateList the state to validate list
	 * @throws ServiceException the service exception
	 */
	public void validateSirtexOperationState(Long operationID, List<Integer> stateToValidateList) throws ServiceException{
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("idTradeRequest", operationID);
		TradeRequest operationState = findObjectByNamedQuery(TradeRequest.OPERATION_STATE, param);
		if(!stateToValidateList.contains(operationState.getOperationState())){
			throw new ServiceException(ErrorServiceType.OTC_OPERATION_STATE);
		}
	}

	
	/**
	 * Validate holder assignment.
	 *
	 * @param isPrimaryPlacement the is primary placement
	 * @param holderAccount the holder account
	 * @param idIsinCodePk the id isin code pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void validateHolderAssignment(boolean isPrimaryPlacement, HolderAccount holderAccount,
			String idIsinCodePk, BigDecimal stockQuantity) throws ServiceException{
		
		Security security  = find(Security.class, idIsinCodePk);
		
		if(isPrimaryPlacement && stockQuantity!=null){

			BigDecimal nominalValue = security.getCurrentNominalValue();
			BigDecimal amountEntered = stockQuantity.multiply(nominalValue);
			if(security.getMinimumInvesment()!=null && security.getMinimumInvesment().compareTo(amountEntered)==1){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_RANGE_AMOUNTS,
						new Object[]{holderAccount.getAccountNumber().toString()});
			}else if(security.getMaximumInvesment()!=null &&  security.getMaximumInvesment().compareTo(amountEntered)==-1){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_RANGE_MAX_AMOUNTS,
						new Object[]{holderAccount.getAccountNumber().toString()});
			}
			return;
		}
		
		if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_NOT_REGISTERED,
					new Object[]{holderAccount.getAccountNumber().toString()});
		}
		
		for(HolderAccountDetail holderDetail : holderAccount.getHolderAccountDetails()){
			Holder holder = holderDetail.getHolder();
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED,
						new Object[]{holder.getIdHolderPk().toString()});
			}
			
			boolean isLocal = false;
			if(holder.getNationality().equals(countryResidence) || 
					(holder.getSecondNationality()!=null && holder.getSecondNationality().equals(countryResidence))){
				isLocal = true;
			}
			
			if(isLocal){
				if(!security.getSecurityInvestor().getIndLocal().equals(ComponentConstant.ONE)){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_SECURITY_INVESTOR,
							new Object[]{holderAccount.getAccountNumber().toString()});
				}
			}else{
				if(!security.getSecurityInvestor().getIndForeign().equals(ComponentConstant.ONE)){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_SECURITY_INVESTOR,
							new Object[]{holderAccount.getAccountNumber().toString()});
				}
			}

			if(holder.getHolderType().equals(PersonType.JURIDIC.getCode())){
				if(!security.getSecurityInvestor().getIndJuridical().equals(ComponentConstant.ONE)){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_SECURITY_INVESTOR,
							new Object[]{holderAccount.getAccountNumber().toString()});
				}
			}
			
			if(holder.getHolderType().equals(PersonType.NATURAL.getCode())){
				if(!security.getSecurityInvestor().getIndNatural().equals(ComponentConstant.ONE)){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_SECURITY_INVESTOR,
							new Object[]{holderAccount.getAccountNumber().toString()});
				}
			}
			
		}
		
	}
	
	/**
	 * Validate mechanism operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public void validateMechanismOperation(MechanismOperation mechanismOperation) throws ServiceException{
		NegotiationOperationResultTO negotiationOperationResultTO = new NegotiationOperationResultTO();
		Map<String,Object> param = new HashMap<String,Object>();
		
		Long negotiationMechanism = mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		
		//Validamos estado del participante
		param.put("idParticipantPkParam", mechanismOperation.getSellerParticipant().getIdParticipantPk());
		Participant participantSeller = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
		if(!participantSeller.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}
		
		param = new HashMap<String,Object>();
		param.put("idParticipantPkParam", mechanismOperation.getBuyerParticipant().getIdParticipantPk());
		Participant participantBuyer = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
		if(!participantBuyer.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}
		
		//   Validamos los participantes encargados
		if(mechanismOperation.getHolderAccountOperations()!=null){//Validamos si existen cuentas
			for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
				if(holderAccountOperation.getIndIncharge().equals(BooleanType.YES.getCode()) && holderAccountOperation.getInchargeFundsParticipant()!=null){
					param.put("idParticipantPkParam", holderAccountOperation.getInchargeFundsParticipant().getIdParticipantPk());
					Participant participantInCharge = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
					if(!participantInCharge.getState().equals(ParticipantStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
					}
				}
			}
		}
		
		//Validamos el estado del valor
		param = new HashMap<String,Object>();
		param.put("idSecurityCodePkParam", mechanismOperation.getSecurities().getIdSecurityCodePk());
		Security security = findObjectByNamedQuery(Security.SECURITY_STATE, param);
		if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
		}
		
		//Validamos el estado de la emision
		param = new HashMap<String,Object>();
		param.put("idIssuanceCodePkParam", mechanismOperation.getSecurities().getIssuance().getIdIssuanceCodePk());
		Issuance issuance = findObjectByNamedQuery(Issuance.ISSUANCE_STATE, param);
		if(!issuance.getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
		}
		
		//Validamos el estado del valor con la modalidad y mecanismo
		param = new HashMap<String,Object>();
		param.put("idSecurityCodePkParam", mechanismOperation.getSecurities().getIdSecurityCodePk());
		param.put("idNegotiationMechanismParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk());
		param.put("idNegotiationModalityParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		param.put("idStateParam", BooleanType.YES.getCode());
		Long secMechExist = findEntityByNamedQuery(Long.class,SecurityNegotiationMechanism.SEC_NEGOTIATION_STATE, param);
		if(secMechExist == 0L && !negotiationMechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_MECHANISM_BLOCK);
		}
		
		//Validamos el estado del participante con la modalidad y mecanismo
		//Validamos el participante vendedor con el mecanismo
		param = new HashMap<String,Object>();
		param.put("idParticipantParam", mechanismOperation.getSellerParticipant().getIdParticipantPk());
		param.put("idNegotiationMechanismParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk());
		param.put("idNegotiationModalityParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		param.put("idStateParam", ParticipantMechanismStateType.REGISTERED.getCode());
		
		Long partMechExist = findEntityByNamedQuery(Long.class,ParticipantMechanism.PARTICIPANT_MECHAN_STATE, param);
			
		if(partMechExist == 0L && !negotiationMechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
			if(mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk().equals(NegotiationMechanismType.OTC.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_OTC_MECHANISM_BLOCK);
			}else{
				throw new ServiceException(ErrorServiceType.PARTICIPANT_MECHANISM_BLOCK);
			}
			
		}
			
		//Validamos el participante comprador con el mecanismo
		param.put("idParticipantParam", mechanismOperation.getBuyerParticipant().getIdParticipantPk());
		Long buyMechExist = findEntityByNamedQuery(Long.class,ParticipantMechanism.PARTICIPANT_MECHAN_STATE, param);
		if(buyMechExist == 0L && !negotiationMechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_MECHANISM_BLOCK);
		}
		
		//Validamos el estado del emisor
		param = new HashMap<String,Object>();
		param.put("idIssuerPkParam", mechanismOperation.getSecurities().getIssuer().getIdIssuerPk());
		Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, param);
		if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
		}
		

		Long mechanismId = mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		Long modalityId = mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk();
		Integer settlementScheme = mechanismOperation.getSettlementSchema();
		Integer currency = mechanismOperation.getCurrency();
		
		//validate settlement process, diferent to close state
//		SettlementProcess settlementProcess = findSettlementProcess(mechanismId, modalityId,settlementScheme,currency);
//		if(settlementProcess!=null){
//			if(settlementScheme.equals(SettlementSchemaType.GROSS.getCode())){
//				if(settlementProcess.getProcessState().equals(SettlementProcessStateType.FINISHED.getCode())){
//					throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_CLOSED);
//				}
//			}else if(settlementScheme.equals(SettlementSchemaType.NET.getCode())){
//				throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_CLOSED);
//			}
//		}
		
		//Validamos el estado de las distintas cuentas de titular
		if (mechanismOperation.getHolderAccountOperations()!=null){//Validamos si existen cuentas
			for(HolderAccountOperation holderAccountOperation: mechanismOperation.getHolderAccountOperations()){
				if(holderAccountOperation.getHolderAccount()!=null){
					param = new HashMap<String,Object>();
					param.put("idHolderAccountPkParam", holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
					HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, param);
					if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
						if(holderAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
							negotiationOperationResultTO.setHolderAccountSellIsBlocked(Boolean.TRUE);
							throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
						}else if(holderAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
							negotiationOperationResultTO.setHolderAccountBuyIsBlocked(Boolean.TRUE);
							break;
						}
					}
					for(HolderAccountDetail holderAccountDetail: holderAccountOperation.getHolderAccount().getHolderAccountDetails()){
						param = new HashMap<String,Object>();
						param.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
						Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
						if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
							throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Validate sirtex operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public void validateSirtexOperation(TradeRequest mechanismOperation) throws ServiceException{
		NegotiationOperationResultTO negotiationOperationResultTO = new NegotiationOperationResultTO();
		Map<String,Object> param = new HashMap<String,Object>();
		
		Long negotiationMechanism = mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		
		//Validamos estado del participante
		param.put("idParticipantPkParam", mechanismOperation.getSellerParticipant().getIdParticipantPk());
		Participant participantSeller = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
		if(!participantSeller.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}
		
		param = new HashMap<String,Object>();
		param.put("idParticipantPkParam", mechanismOperation.getBuyerParticipant().getIdParticipantPk());
		Participant participantBuyer = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
		if(!participantBuyer.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}
		
		//   Validamos los participantes encargados
		if(mechanismOperation.getAccountSirtexOperations()!=null){//Validamos si existen cuentas
			for(AccountTradeRequest holderAccountOperation: mechanismOperation.getAccountSirtexOperations()){
				if(holderAccountOperation.getIndInCharge().equals(BooleanType.YES.getCode()) && holderAccountOperation.getInchargeStockParticipant()!=null){
					param.put("idParticipantPkParam", holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
					Participant participantInCharge = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
					if(!participantInCharge.getState().equals(ParticipantStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
					}
				}
			}
		}
		
		//Validamos el estado del valor
		param = new HashMap<String,Object>();
		param.put("idSecurityCodePkParam", mechanismOperation.getSecurities().getIdSecurityCodePk());
		Security security = findObjectByNamedQuery(Security.SECURITY_STATE, param);
		if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
		}
		
		//Validamos el estado de la emision
		param = new HashMap<String,Object>();
		param.put("idIssuanceCodePkParam", mechanismOperation.getSecurities().getIssuance().getIdIssuanceCodePk());
		Issuance issuance = findObjectByNamedQuery(Issuance.ISSUANCE_STATE, param);
		if(!issuance.getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
		}
		
		//Validamos el estado del valor con la modalidad y mecanismo
		param = new HashMap<String,Object>();
		param.put("idSecurityCodePkParam", mechanismOperation.getSecurities().getIdSecurityCodePk());
		param.put("idNegotiationMechanismParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk());
		param.put("idNegotiationModalityParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		param.put("idStateParam", BooleanType.YES.getCode());
		Long secMechExist = findEntityByNamedQuery(Long.class,SecurityNegotiationMechanism.SEC_NEGOTIATION_STATE, param);
		if(secMechExist == 0L && !negotiationMechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_MECHANISM_BLOCK);
		}
		
		//Validamos el estado del participante con la modalidad y mecanismo
		//Validamos el participante vendedor con el mecanismo
		param = new HashMap<String,Object>();
		param.put("idParticipantParam", mechanismOperation.getSellerParticipant().getIdParticipantPk());
		param.put("idNegotiationMechanismParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk());
		param.put("idNegotiationModalityParam", mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		param.put("idStateParam", ParticipantMechanismStateType.REGISTERED.getCode());
		
		Long partMechExist = findEntityByNamedQuery(Long.class,ParticipantMechanism.PARTICIPANT_MECHAN_STATE, param);
			
		if(partMechExist == 0L && !negotiationMechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
			if(mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk().equals(NegotiationMechanismType.OTC.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_OTC_MECHANISM_BLOCK);
			}else{
				throw new ServiceException(ErrorServiceType.PARTICIPANT_MECHANISM_BLOCK);
			}
			
		}
			
		//Validamos el participante comprador con el mecanismo
		param.put("idParticipantParam", mechanismOperation.getBuyerParticipant().getIdParticipantPk());
		Long buyMechExist = findEntityByNamedQuery(Long.class,ParticipantMechanism.PARTICIPANT_MECHAN_STATE, param);
		if(buyMechExist == 0L && !negotiationMechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_MECHANISM_BLOCK);
		}
		
		//Validamos el estado del emisor
		param = new HashMap<String,Object>();
		param.put("idIssuerPkParam", mechanismOperation.getSecurities().getIssuer().getIdIssuerPk());
		Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, param);
		if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
		}
		

		Long mechanismId = mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		Long modalityId = mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk();
		Integer settlementScheme = mechanismOperation.getSettlementScheme();
		Integer currency = mechanismOperation.getSettlementCurrency();
		
		//validate settlement process, diferent to close state
//		SettlementProcess settlementProcess = findSettlementProcess(mechanismId, modalityId,settlementScheme,currency);
//		if(settlementProcess!=null){
//			if(settlementScheme.equals(SettlementSchemaType.GROSS.getCode())){
//				if(settlementProcess.getProcessState().equals(SettlementProcessStateType.FINISHED.getCode())){
//					throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_CLOSED);
//				}
//			}else if(settlementScheme.equals(SettlementSchemaType.NET.getCode())){
//				throw new ServiceException(ErrorServiceType.SETTLEMENT_PROCESS_CLOSED);
//			}
//		}
		
		//Validamos el estado de las distintas cuentas de titular
		if (mechanismOperation.getAccountSirtexOperations()!=null){//Validamos si existen cuentas
			for(AccountTradeRequest holderAccountOperation: mechanismOperation.getAccountSirtexOperations()){
				if(holderAccountOperation.getHolderAccount()!=null){
					param = new HashMap<String,Object>();
					param.put("idHolderAccountPkParam", holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
					HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, param);
					if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
						if(holderAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
							negotiationOperationResultTO.setHolderAccountSellIsBlocked(Boolean.TRUE);
							throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
						}else if(holderAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
							negotiationOperationResultTO.setHolderAccountBuyIsBlocked(Boolean.TRUE);
							break;
						}
					}
					for(HolderAccountDetail holderAccountDetail: holderAccountOperation.getHolderAccount().getHolderAccountDetails()){
						param = new HashMap<String,Object>();
						param.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
						Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
						if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
							throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
						}
					}
				}
			}
		}
	}

	/**
	 * Gets the placement segment on issuance service bean.
	 *
	 * @param negotiationTO the otc operation to
	 * @return the placement segment on issuance service bean
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment getPlacementSegmentServiceBean(NegotiationOperationTO negotiationTO) throws ServiceException {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		CriteriaQuery<PlacementSegment> criteriaQuery = criteriaBuilder.createQuery(PlacementSegment.class);
		Root<PlacementSegment> placSegmentRoot = criteriaQuery.from(PlacementSegment.class);
		if(negotiationTO.getNeedAllPlacementObject()){
			criteriaQuery.select(placSegmentRoot).distinct(true);
		}else{
			criteriaQuery.multiselect(
					placSegmentRoot.get("idPlacementSegmentPk"),
					placSegmentRoot.get("amountToPlace"),
					placSegmentRoot.get("placedAmount"),
					placSegmentRoot.get("placementTerm"),
					placSegmentRoot.get("placementSegmentState"),
					placSegmentRoot.get("trancheNumber"),
					placSegmentRoot.get("fixedAmount"),
					placSegmentRoot.get("requestFixedAmount"),
					placSegmentRoot.get("floatingAmount"),
					placSegmentRoot.get("requestFloatingAmount"),
					placSegmentRoot.get("placementSegmentType")
					);
		}
		// there's only one placement segment opened by security	
		criteriaParameters.add(criteriaBuilder.equal(placSegmentRoot.get("placementSegmentState"), negotiationTO.getIdPlacementSegmentState()));
		criteriaParameters.add(criteriaBuilder.equal(placSegmentRoot.get("issuance").get("idIssuanceCodePk"), negotiationTO.getIdIssuanceCode()));
		if(negotiationTO.getIdSecurityCode()!=null){
			Join<PlacementSegmentDetail,PlacementSegment> placementDetJoin = placSegmentRoot.join("placementSegmentDetails");
			criteriaParameters.add(criteriaBuilder.equal(placementDetJoin.get("security").get("idSecurityCodePk"), negotiationTO.getIdSecurityCode()));
			criteriaParameters.add(criteriaBuilder.equal(placementDetJoin.get("statePlacementSegmentDet"), negotiationTO.getIdPlacementSegmentDetailState()));
		}
		if(negotiationTO.getIdParticipantPlacement()!=null){
			Join<PlacementSegParticipaStruct,PlacementSegment> placementStructJoin = placSegmentRoot.join("placementSegParticipaStructs");
			criteriaParameters.add(criteriaBuilder.equal(placementStructJoin.get("participant").get("idParticipantPk"), negotiationTO.getIdParticipantPlacement()));
		}
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		
		TypedQuery<PlacementSegment> placSegmentCriteria = em.createQuery(criteriaQuery);
		PlacementSegment placSegmentTemp = null;
		try{
			placSegmentTemp = placSegmentCriteria.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		if(negotiationTO.getNeedAllPlacementObject()){
			placSegmentTemp.getPlacementSegParticipaStructs().size();
			for(PlacementSegmentDetail placementSegmentDetail: placSegmentTemp.getPlacementSegmentDetails()){
				placementSegmentDetail.getSecurity().getIdSecurityCodePk();
			}
		}
		return placSegmentTemp;
	}
	/**
	 * Gets the security by modalities.
	 *
	 * @param negotiatioOperationTO the negotiatio operation to
	 * @return the security by modalities
	 * @throws ServiceException the service exception
	 */
	public Security getSecurityByModalities(NegotiationOperationTO negotiatioOperationTO) throws ServiceException{
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		CriteriaQuery<Security> criteriaQuery = criteriaBuilder.createQuery(Security.class);
		Root<Security> securityRoot = criteriaQuery.from(Security.class);
		criteriaQuery.distinct(Boolean.TRUE);//Hacemos el distint para el CODIGO DE VALOR
		criteriaQuery.multiselect(securityRoot.get("idSecurityCodePk"));
		if(negotiatioOperationTO.getIdNegotiationMechanismPk()!=null || negotiatioOperationTO.getIdNegotiationModalityPk()!=null){
			Join<SecurityNegotiationMechanism,Security> snmJoin = securityRoot.join("securityNegotiationMechanisms");
			criteriaParameters.add(criteriaBuilder.equal(snmJoin.get("securityNegotationState"),BooleanType.YES.getCode()));
			if(negotiatioOperationTO.getIdNegotiationMechanismPk()!=null){
				criteriaParameters.add(criteriaBuilder.equal(snmJoin.get("mechanismModality").get("id").get("idNegotiationMechanismPk"),
						negotiatioOperationTO.getIdNegotiationMechanismPk()));
			}
			if(negotiatioOperationTO.getIdNegotiationModalityPk()!=null){
				criteriaParameters.add(criteriaBuilder.equal(snmJoin.get("mechanismModality").get("id").get("idNegotiationModalityPk"),
						negotiatioOperationTO.getIdNegotiationModalityPk()));
			}
		}
		
		if(negotiatioOperationTO.getIdSecurityCode()!=null){
			criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("idSecurityCodePk"),negotiatioOperationTO.getIdSecurityCode()));
		}
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		TypedQuery<Security> otcOperationsCriteria = em.createQuery(criteriaQuery);
		Security securityFinded ;//object will return security
		try{
			securityFinded = otcOperationsCriteria.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		return securityFinded;
	}
	
	/**
	 * Gets the mechanism service bean.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the mechanism service bean
	 */
	public NegotiationMechanism getMechanismServiceBean(Long idNegotiationMechanismPk){
		return find(NegotiationMechanism.class,idNegotiationMechanismPk);
	}
	
	/**
	 * Gets the swift component.
	 *
	 * @param fundsOperationType the funds operation type
	 * @param currency the currency
	 * @return the swift component
	 */
	public SwiftComponent getSwiftComponentServiceBean(Long fundsOperationType, Integer currency)
	{
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT swift FROM SwiftComponent swift ");
		sbQuery.append(" where swift.fundsOperationType.idFundsOperationTypePk = :fundsOperationTypePk and ");
		sbQuery.append(" swift.currency = :currencyParam ");
		Query querySwift = em.createQuery(sbQuery.toString());
		querySwift.setParameter("fundsOperationTypePk", fundsOperationType);
		querySwift.setParameter("currencyParam", currency);
		
		SwiftComponent switComponent = null;
		try{
			switComponent = (SwiftComponent) querySwift.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		return switComponent;
	}
	

	
	
	/**
	 * Checking whether data exists in ASSIGNMENT_PROCESS table.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param cashSettlementDate the cash settlement date
	 * @return String
	 */ 
	public AssignmentProcess findAssignmentProcess(Long mechanismId,Long modalityId,Date cashSettlementDate){   
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select assPro from AssignmentProcess assPro ");
	    sbQuery.append(" where assPro.mechanisnModality.id.idNegotiationMechanismPk = :mechanismPK ");
	    sbQuery.append(" and assPro.mechanisnModality.id.idNegotiationModalityPk = :modalityPK");
		parameters.put("mechanismPK", mechanismId);
		parameters.put("modalityPK", modalityId);
		if (cashSettlementDate != null){
			sbQuery.append(" and assPro.settlementDate = :cashSettlementDate");
			parameters.put("cashSettlementDate", cashSettlementDate);
		}
		try {
			return (AssignmentProcess)findObjectByQueryString(sbQuery.toString(), parameters);
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Find settlement process.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param settlementScheme the settlement scheme
	 * @param currency the currency
	 * @return the settlement process
	 */
	public SettlementProcess findSettlementProcess(Long mechanismId, Long modalityId, Integer settlementScheme, Integer currency){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Select new com.pradera.model.settlement.SettlementProcess(setp.idSettlementProcessPk, setp.processType,setp.processState) ");
		sbQuery.append("From SettlementProcess setp ");
		sbQuery.append("Inner join setp.modalityGroup mog ");
		sbQuery.append("Inner join mog.modalityGroupDetail modet ");
		sbQuery.append("Where trunc(setp.settlementDate) = trunc(:dateParam) And ");
		sbQuery.append("	  modet.negotiationMechanism.idNegotiationMechanismPk = :mechanismIdParam And ");
		sbQuery.append("	  modet.negotiationModality.idNegotiationModalityPk = :modalityIdParam And ");
		sbQuery.append("	  setp.currency = :currency ");
		if(settlementScheme.equals(SettlementSchemaType.NET.getCode())){
			sbQuery.append("	And setp.processType = :processTypeParam ");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("dateParam", CommonsUtilities.currentDate());
		query.setParameter("mechanismIdParam", mechanismId);
		query.setParameter("modalityIdParam", modalityId);
		query.setParameter("currency", currency);
		query.setMaxResults(1);
		
		if(settlementScheme.equals(SettlementSchemaType.NET.getCode())){
			query.setParameter("processTypeParam", SettlementProcessType.FINAL.getCode());
		}
		
		try{
			return (SettlementProcess) query.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	
	/**
	 * Gets the participant assignment.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @param idParticipant the id participant
	 * @return the participant assignment
	 */
	public ParticipantAssignment getParticipantAssignment(Long idAssignmentProcess, Long idParticipant)
	{
		ParticipantAssignment objParticipantAssignment= null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT PA ");
			stringBuffer.append(" FROM ParticipantAssignment PA ");
			stringBuffer.append(" WHERE PA.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcess ");
			stringBuffer.append(" and PA.participant.idParticipantPk = :idParticipant ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idAssignmentProcess", idAssignmentProcess);
			query.setParameter("idParticipant", idParticipant);
			objParticipantAssignment = (ParticipantAssignment) query.getSingleResult();
		} catch (NoResultException e){
			logger.error("ParticipantAssignment not found");
		}
		
		return objParticipantAssignment;
	}
	
	
	/**
	 * Validate primary placement.
	 *
	 * @param negotiaOperationTO the negotia operation to
	 * @return the primary placement to
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public PrimaryPlacementTO validatePrimaryPlacement(NegotiationOperationTO negotiaOperationTO) throws ServiceException{
		PrimaryPlacementTO primaryPlacementTO = null;
		boolean exists =  validatePlacementSegmentDetail(negotiaOperationTO.getIdSecurityCode(), negotiaOperationTO.getIdIssuanceCode(), null);
		if(exists){ // si existe el tramo para el valor
			exists =  validatePlacementSegmentDetail(negotiaOperationTO.getIdSecurityCode(), negotiaOperationTO.getIdIssuanceCode(),negotiaOperationTO.getIdParticipantPlacement());
			if(exists){//Si es participante colocador, traemos la cuenta del emisor dentro del participante
							
				HolderAccount holderAccount = getIssuerHolderAccount(negotiaOperationTO.getIdSecurityCode(),negotiaOperationTO.getIdParticipantPlacement());
				
				if(Validations.validateIsNotNull(holderAccount)){
					PlacementSegment placSegmentTemp = getPlacementSegmentObject(negotiaOperationTO.getIdSecurityCode(), 
												negotiaOperationTO.getIdIssuanceCode(),
												negotiaOperationTO.getIdParticipantPlacement());
					
					primaryPlacementTO = new PrimaryPlacementTO();
					primaryPlacementTO.setPlacementSegment(placSegmentTemp);
					primaryPlacementTO.setIssuerHolderAccount(holderAccount);
				}else{
					throw new ServiceException(ErrorServiceType.PRIMARY_PLACEMENT_ACCOUNT_NOT_EXIST);
				}
			}else{//Si el participante vendedor no es un participante colocador en el tramo
				throw new ServiceException(ErrorServiceType.PRIMARY_PLACEMENT_PARTICIPANT_INCORRECT);
			}
		}else{//Si el tramo aun no existe o no esta aperturado
			throw new ServiceException(ErrorServiceType.PRIMARY_PLACEMENT_NOT_OPEN);
		}
		return primaryPlacementTO;
	}
	
	/**
	 * This method validates if the placement segment exists based on parameters (it does not get the object).
	 *
	 * @param securityValue the security value
	 * @param issuanceCode the issuance code
	 * @param participantStruct the participant struct
	 * @return boolean booleanPrimPlacement
	 */
	public boolean validatePlacementSegmentDetail(String securityValue, String issuanceCode , Long participantStruct) {
		Long count = 0l;
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select count(psd.idPlacementSegmentDetPk) ");
		sbQuery.append(" from PlacementSegment ps ");
		sbQuery.append(" inner join ps.placementSegmentDetails psd ");
		sbQuery.append(" inner join psd.security sec ");
		if(participantStruct !=null){
			sbQuery.append(" inner join ps.placementSegParticipaStructs struc ");
		}
	    sbQuery.append(" where sec.idSecurityCodePk = :isinCodePk ");
	    sbQuery.append(" and psd.statePlacementSegmentDet= :placementSegmentDet ");
	    sbQuery.append(" and ps.issuance.idIssuanceCodePk = :issuanceCode ");
	    sbQuery.append(" and ps.placementSegmentState = :placeSegState");
	    
	    if(participantStruct !=null){
	    	sbQuery.append(" and struc.participant.idParticipantPk = :participantStruct ");
	    	parameters.put("participantStruct", participantStruct);
	    }
	    
	    parameters.put("isinCodePk", securityValue);
	    parameters.put("issuanceCode", issuanceCode);
	    parameters.put("placementSegmentDet", PlacementSegmentDetailStateType.REGISTERED.getCode());
	    parameters.put("placeSegState", PlacementSegmentStateType.OPENED.getCode());
	    count = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
	    if(count>0l){
	    	return true;
	    }
	    return false;
	}	
	
	/**
	 * This method gets the objects with the necessary elements to do validations.
	 *
	 * @param securityValue the security value
	 * @param issuanceCode the issuance code
	 * @param participantStruct the participant struct
	 * @return PlacementSegment placementSegment
	 */
	public PlacementSegment getPlacementSegmentObject(String securityValue, String issuanceCode , Long participantStruct) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select ");
		sbQuery.append(" new PlacementSegment(	  ");
		sbQuery.append("   ps.idPlacementSegmentPk,  ");
		sbQuery.append("   ps.amountToPlace,  ");
		sbQuery.append("   ps.placedAmount,  ");
		sbQuery.append("   ps.placementTerm,  ");
		sbQuery.append("   ps.placementSegmentState,  ");
		sbQuery.append("   ps.trancheNumber,  ");
		sbQuery.append("   ps.fixedAmount,  ");
		sbQuery.append("   ps.requestFixedAmount,  ");
		sbQuery.append("   ps.floatingAmount,  ");
		sbQuery.append("   ps.requestFloatingAmount,  ");
		sbQuery.append("   ps.placementSegmentType,  ");
		sbQuery.append("   psd.placedAmount,  ");
		sbQuery.append("   psd.requestAmount,  ");
		sbQuery.append("   psd.statePlacementSegmentDet,  ");
		sbQuery.append("   psd.placementSegmentDetType,  ");
		sbQuery.append("   sec.idSecurityCodePk) ");
		sbQuery.append(" from PlacementSegment ps ");
		sbQuery.append(" inner join ps.placementSegmentDetails psd ");
		sbQuery.append(" inner join psd.security sec ");
		sbQuery.append(" inner join ps.placementSegParticipaStructs struc ");
	    sbQuery.append(" where sec.idSecurityCodePk = :isinCodePk ");
	    sbQuery.append(" and psd.statePlacementSegmentDet= :placementSegmentDet ");
	    sbQuery.append(" and ps.issuance.idIssuanceCodePk = :issuanceCode ");
	    sbQuery.append(" and ps.placementSegmentState = :placeSegState");
    	sbQuery.append(" and struc.participant.idParticipantPk = :participantStruct ");
    	
    	parameters.put("participantStruct", participantStruct);
	    parameters.put("isinCodePk", securityValue);
	    parameters.put("issuanceCode", issuanceCode);
	    parameters.put("placementSegmentDet", PlacementSegmentDetailStateType.REGISTERED.getCode());
	    parameters.put("placeSegState", PlacementSegmentStateType.OPENED.getCode());
	    return (PlacementSegment)findObjectByQueryString(sbQuery.toString(), parameters);
	}	
	
	/**
	 * Get List of holder account.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idParticipantPk the id participant pk
	 * @return List<HolderAccount>
	 */
	public HolderAccount getIssuerHolderAccount(String idSecurityCodePk, Long idParticipantPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ha from HolderAccount ha inner join ha.participant p");
		sbQuery.append("	join ha.holderAccountDetails had,Holder h,Issuer i join i.securities sec");
		sbQuery.append("	where had.holder.idHolderPk = h.idHolderPk");
		sbQuery.append("	and h.idHolderPk = i.holder.idHolderPk");
		sbQuery.append("	and p.idParticipantPk = :idParticipantPk");
		sbQuery.append(" 	and p.state = :state");
		sbQuery.append(" 	and h.stateHolder = :stateHolder");
		sbQuery.append("	and ha.stateAccount= :stateAccount");
		sbQuery.append("	and ha.accountGroup = :accountGroup");
		sbQuery.append("	and sec.idSecurityCodePk= :idSecurityCodePk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("stateHolder", HolderStateType.REGISTERED.getCode());
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
		query.setParameter("accountGroup", HolderAccountGroupType.ISSUER.getCode());
		try {
			return (HolderAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (NonUniqueResultException e) {
			return null;
		}
	}
	
	
	/**
	 * Gets the modality group.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 * @param settlementSchema the settlement schema
	 * @return the modality group service bean
	 */
	public ModalityGroup getModalityGroupServiceBean(Long idNegotiationMechanismPk, Long idNegotiationModalityPk,Integer settlementSchema) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select mg from ModalityGroupDetail mgd inner join mgd.modalityGroup mg where ");
		sbQuery.append(" mgd.mechanismModality.id.idNegotiationMechanismPk=:mechanism and ");
		sbQuery.append(" mgd.mechanismModality.id.idNegotiationModalityPk=:modality and ");
		sbQuery.append(" mg.settlementSchema = :schema ");
		parameters.put("mechanism", idNegotiationMechanismPk);
		parameters.put("modality", idNegotiationModalityPk);
		parameters.put("schema", settlementSchema);
		try {
			return (ModalityGroup)findObjectByQueryString(sbQuery.toString(), parameters);
		} catch (NonUniqueResultException e) {
			logger.error("mas de un modalityGroup encontrado ");
			return null;
		}catch (NoResultException e) {
			logger.error("no se encontro modalityGroup");
			return null;
		}
	}
	
	/**
	 * Generate and set the payment reference for the given mechanism operation 
	 * based on modality and settlement schema.
	 *
	 * @param mechanismOperation the mechanism operation
	 */
	public void generatePaymentReference(MechanismOperation mechanismOperation) {
		/* Update Swift component */
		SwiftComponent c = getSwiftComponentServiceBean(ParameterFundsOperationType.SETTLEMENT_OPERATIONS_FUND_DEPOSITS.getCode(), mechanismOperation.getCurrency());
		StringBuilder stringTrnField  = new StringBuilder();
		if(Validations.validateIsNotNull(c)){
			if (SettlementSchemaType.GROSS.getCode().equals(mechanismOperation.getSettlementSchema())) {
				stringTrnField.append(c.getTrnField());
				stringTrnField.append(".");
				stringTrnField.append(mechanismOperation.getIdMechanismOperationPk());
			}else if (SettlementSchemaType.NET.getCode().equals(mechanismOperation.getSettlementSchema())) {
				ModalityGroup mg = getModalityGroupServiceBean(mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk(),
															   mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk(),
															   mechanismOperation.getSettlementSchema());
				stringTrnField.append(c.getTrnField());
				stringTrnField.append(".");
				stringTrnField.append(mechanismOperation.getSettlementSchema());
				stringTrnField.append(mechanismOperation.getMechanisnModality().getNegotiationMechanism().getMechanismCode());
				stringTrnField.append(mg.getModalityGroupCode());
			}else{
				stringTrnField.append("NO_TRN");
			}
		}else{
			stringTrnField.append(mechanismOperation.getIdMechanismOperationPk().toString());
		}
		mechanismOperation.setPaymentReference(stringTrnField.toString());
	}
	
	/**
	 * Gets the account operation details.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the account operation details
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getAccountOperationDetails(Long idMechanismOperationPk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct ");
		sbQuery.append("	hao.idHolderAccountOperationPk, "); //0
		sbQuery.append("	mo.cashSettlementAmount, "); //1
		sbQuery.append("	mo.termSettlementAmount, "); //2
		sbQuery.append("	hao.holderAccount.idHolderAccountPk, "); //3
		sbQuery.append("	hao.stockQuantity, "); //4
		sbQuery.append("	hao.settlementAmount, "); //5 term amount
		sbQuery.append("	( select haos.settlementAmount from HolderAccountOperation haos ");
		sbQuery.append("	  where hao.refAccountOperation.idHolderAccountOperationPk = haos.idHolderAccountOperationPk ");
		sbQuery.append("	) as cashAmount, "); //6 cash amount
		sbQuery.append("	hao.refAccountOperation.idHolderAccountOperationPk "); //7
		sbQuery.append(" from HolderAccountOperation hao ");
		sbQuery.append(" inner join hao.mechanismOperation mo ");
		sbQuery.append(" where mo.mechanisnModality.negotiationModality.indMarginGuarantee = :indGuarantees ");
		sbQuery.append(" and hao.holderAccountState  = :holderAccountState ");
		sbQuery.append(" and hao.role = :buyRole ");
		sbQuery.append(" and hao.operationPart = :termPart ");
		sbQuery.append(" and mo.idMechanismOperationPk =:idMechanismOperationPk ");
		
		//parameters.put("operationState", MechanismOperationStateType.CASH_SETTLED.getCode());
		parameters.put("holderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("indGuarantees", ComponentConstant.ONE);
		parameters.put("buyRole", ComponentConstant.PURCHARSE_ROLE);
		//parameters.put("sellRole", ComponentConstant.SALE_ROLE);
		//parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("termPart", ComponentConstant.TERM_PART);
		parameters.put("idMechanismOperationPk", idMechanismOperationPk);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Calculate coverage amount.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void calculateCoverageAmount(Long idMechanismOperationPk, LoggerUser loggerUser) throws ServiceException{
			
 		List<Object[]> cashList = getAccountOperationDetails(idMechanismOperationPk);
		if(cashList.size() > 0){
			BigDecimal marginFactorPerc = parameterServiceBean.getGuaranteesMarginFactorParam();
			BigDecimal marginFactor = marginFactorPerc.divide(new BigDecimal(100));
			BigDecimal totalCoverageAmount = new BigDecimal(0);
			for (Object[] object : cashList) {
				BigDecimal accountTermSettlementAmount = object[5] == null? BigDecimal.ZERO : (BigDecimal)object[5];
				BigDecimal accountCashSettlementAmount  = object[6] == null? BigDecimal.ZERO : (BigDecimal)object[6];
			
				BigDecimal initialMargin = marginFactor.multiply(accountTermSettlementAmount);
				BigDecimal coverageAmount = accountCashSettlementAmount.add(initialMargin);
				
				logger.info(" calculate coverage amount "+ coverageAmount);
				
				//term part  // calculate to both
				if(object[0]!=null){
					Long idHolderAccountOp = (Long)object[0];
					updateHolderAccountCoverageAmount(idHolderAccountOp, coverageAmount, initialMargin, loggerUser);
				}
				
				//cash part
				if(object[7]!=null){ 
					Long idHolderAccountOp = (Long)object[7];
					updateHolderAccountCoverageAmount(idHolderAccountOp, coverageAmount, initialMargin, loggerUser);
				}
				
				totalCoverageAmount = totalCoverageAmount.add(coverageAmount);
			}
			
			updateOperationCoverageAmount(idMechanismOperationPk,totalCoverageAmount,loggerUser);
			
		}
	}
	
	/**
	 * Update holder account coverage amount.
	 *
	 * @param idHolderAccountOperationPk the id holder account operation pk
	 * @param coverageAmount the coverage amount
	 * @param initialMargin the initial margin
	 * @param loggerUser the logger user
	 * @return the int
	 */
	private int updateHolderAccountCoverageAmount(
			Long idHolderAccountOperationPk, BigDecimal coverageAmount,
			BigDecimal initialMargin, LoggerUser loggerUser) {
		StringBuilder stringBuffer = new StringBuilder();
		
		stringBuffer.append(" UPDATE HolderAccountOperation set  ");
		stringBuffer.append(" coverageAmount = :coverageAmount ");
		stringBuffer.append(" , initialMarginAmount = :initialMarginAmount ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idHolderAccountOperationPk = :idHolderAccountOperation");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idHolderAccountOperation", idHolderAccountOperationPk);
		query.setParameter("coverageAmount", coverageAmount);
		query.setParameter("initialMarginAmount", initialMargin);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();		
	}

	/**
	 * Gets the participant operation coverage amount.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param initialMarginAmount the initial margin amount
	 * @param loggerUser the logger user
	 * @return the participant operation coverage amount
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getParticipantOperationCoverageAmount(Long idMechanismOperationPk, BigDecimal initialMarginAmount, LoggerUser loggerUser) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("update ParticipantOperation ");
		sbQuery.append(" set initialMarginAmount = :initialMarginAmount ");
		sbQuery.append(" , lastModifyApp = :lastModifyApp ");
		sbQuery.append(" , lastModifyDate = :lastModifyDate ");
		sbQuery.append(" , lastModifyIp = :lastModifyIp ");
		sbQuery.append(" , lastModifyUser = :lastModifyUser ");
		sbQuery.append(" where ");
		sbQuery.append(" pao.inchargeType = :inchargeType ");
		sbQuery.append(" and pao.role = :sellRole ");
		sbQuery.append(" and pao.operationPart = :cashPart ");
		sbQuery.append(" and pao.mechanismOperation.idMechanismOperationPk =:idMechanismOperationPk ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("sellRole", ComponentConstant.SALE_ROLE);
		query.setParameter("inchargeType", NegotiationConstant.IND_INCHARGE_SAME_AS_TRADER_TYPE);
		query.setParameter("cashPart", ComponentConstant.CASH_PART);
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Update operation coverage amount.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param coverageAmount the coverage amount
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateOperationCoverageAmount(Long idMechanismOperation,BigDecimal coverageAmount, LoggerUser loggerUser) throws ServiceException
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE MechanismOperation set coverageAmount = :coverageAmount ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idMechanismOperationPk = :idMechanismOperation ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("coverageAmount", coverageAmount);
		query.setParameter("idMechanismOperation", idMechanismOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	
	/**
	 * Issuance is placement segment.
	 *
	 * @param security the security
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean issuanceIsPlacementSegment(Security security) throws ServiceException {
		if(security.getIssuance().getIndPrimaryPlacement().equals(BooleanType.YES.getCode())  && security.getIndTraderSecondary().equals(BooleanType.NO.getCode())){
			// si tiene colocacion primaria (tiene tramos) y no puede negociar secundario mientras su tramo este abierto
			// se hace la validacion
			if(OtcOperationUtils.issuanceIsLastPlacement(security.getIssuance())){
				NegotiationOperationTO negotiationOperationTO = new NegotiationOperationTO();//Creamos el objeto TO para traer el tramo Completo
				negotiationOperationTO.setIdIssuanceCode(security.getIssuance().getIdIssuanceCodePk());
				negotiationOperationTO.setIdSecurityCode(security.getIdSecurityCodePk());
				negotiationOperationTO.setIdPlacementSegmentState(PlacementSegmentStateType.CLOSED.getCode());
				negotiationOperationTO.setNeedAllPlacementObject(Boolean.FALSE);
				negotiationOperationTO.setIdPlacementSegmentDetailState(PlacementSegmentDetailStateType.REGISTERED.getCode());
				PlacementSegment placementSegment = getPlacementSegmentServiceBean(negotiationOperationTO);
				if(placementSegment!=null){
					if(placementSegment.getTrancheNumber().equals(security.getIssuance().getNumberTotalTranches())){
						return Boolean.FALSE;
					}
				}
			}
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public HolderAccountBalance getHolderAccountBalanceForChekQuantity(Long idHolderAccountPk, String idSecurityCodePk, Long idParticipantPk){
		StringBuilder builder = new StringBuilder();
		builder.append(" select hab from HolderAccountBalance hab ");
		builder.append(" where hab.participant.idParticipantPk = :idParticipantPk ");
		builder.append(" and hab.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		builder.append(" and hab.security.idSecurityCodePk = :idSecurityCodePk ");
		
		Query query = em.createQuery(builder.toString());
		
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		try {
			return (HolderAccountBalance) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param objCurrency the obj currency
	 * @param mechanismOperation the mechanism operation
	 * @param termPart the term part
	 * @throws ServiceException the service exception
	 */
	public void setCurrency(ParameterTable objCurrency,MechanismOperation mechanismOperation, Integer termPart) throws ServiceException {
		//parameterPK -> monera de valor
		//indicator4 -> moneda de la operacion
		//indicator5 -> moneda liquidacion
		
		mechanismOperation.setCurrency(objCurrency.getParameterTablePk());
		mechanismOperation.setCurrencyDescription(objCurrency.getParameterName()); 
		
		if(!objCurrency.getIndicator4().equals(objCurrency.getIndicator5())){
			// si la moneda del valor
			DailyExchangeRates exchangeRateDay = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),objCurrency.getIndicator4());
			if(exchangeRateDay==null){
				throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST);
			}
			mechanismOperation.setExchangeRate(exchangeRateDay.getBuyPrice());
		}
		
		mechanismOperation.setCashSettlementCurrency(objCurrency.getIndicator5());
		if(ComponentConstant.ONE.equals(termPart)){
			mechanismOperation.setTermSettlementCurrency(objCurrency.getIndicator5());
		}
	}
	
	public BigDecimal getPrimaryPlacementBalanceOperation(Date settlementDate, String idSecurityCode) {
		BigDecimal stocQuantity= BigDecimal.ZERO;
		try {
			StringBuilder stringBuilder= new StringBuilder();
			stringBuilder.append(" SELECT nvl(sum(MO.stockQuantity),0) ");
			stringBuilder.append(" FROM MechanismOperation MO ");
			stringBuilder.append(" WHERE MO.securities.idSecurityCodePk = :idSecurityCode ");
			stringBuilder.append(" and MO.cashSettlementDate = :settlementDate ");
			stringBuilder.append(" and MO.indPrimaryPlacement = :indPrimaryPlacement ");
			stringBuilder.append(" and MO.operationState in (:lstOperationState) ");
			
			Query query= em.createQuery(stringBuilder.toString());
			query.setParameter("idSecurityCode", idSecurityCode);
			query.setParameter("settlementDate", settlementDate);
			query.setParameter("indPrimaryPlacement", BooleanType.YES.getCode());
			List<Integer> lstOperationState= new ArrayList<Integer>();
			lstOperationState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
			lstOperationState.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
			query.setParameter("lstOperationState", lstOperationState);
			stocQuantity= (BigDecimal) query.getSingleResult();
		} catch (NoResultException ex) {
			
		}
		return stocQuantity;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> getHolderAccountBySecurity(String idSecurityCodePk, Long idParticipantPk) {
		
		try {
			StringBuilder sb = new StringBuilder();
			
			sb.append(" Select hab from HolderAccountBalance hab");
			sb.append("  inner join fetch hab.holderAccount ha");
			sb.append("  inner join fetch ha.holderAccountDetails had");
			sb.append("  inner join fetch had.holder ho");
			sb.append(" where hab.security.idSecurityCodePk = :idSecurityCodePk");
			sb.append("  and hab.participant.idParticipantPk = :idParticipantPk");
			sb.append("  and hab.availableBalance > 0 ");
			
			Query q = em.createQuery(sb.toString());
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			q.setParameter("idParticipantPk", idParticipantPk);
			
			return q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
	public Long countCouponExpiringByDate(Date searchDate, String idSecurityCodePk) {		
		try {
			StringBuilder sb = new StringBuilder();
			
			sb.append(" Select COUNT(1) FROM InterestPaymentSchedule ips");
			sb.append("  join ips.programInterestCoupons pic");
			sb.append(" WHERE TRUNC(pic.experitationDate) = TRUNC(:searchDate)");
			sb.append("  and ips.security.idSecurityCodePk = :idSecurityCodePk");
		
			Query q = em.createQuery(sb.toString());
			q.setParameter("searchDate", searchDate);
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			
			return (Long) q.getSingleResult();
		} catch (NoResultException e) {
			return GeneralConstants.ZERO_VALUE_LONG;
		}
	}
}