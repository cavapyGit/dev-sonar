package com.pradera.negotiations.operations.calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;

@Stateless
@Performance
public class OperationPriceCalculatorServiceFacade {

	@EJB
	OperationPriceCalculatorServiceBean operationPriceCalculatorServiceBean;
	
	public Integer getDaysPassedFromLastCoupon(Security sec, Date operationDate) {		
		Date issuanceDate = sec.getIssuanceDate();
		
		if(CommonsUtilities.diffDatesInDays(issuanceDate, operationDate) == 0) {
			return GeneralConstants.ZERO_VALUE_INTEGER;
		} else {
			ProgramInterestCoupon mostRecentC = operationPriceCalculatorServiceBean.getMostRecentPayedCouponBySecurityPk(sec.getIdSecurityCodePk(), operationDate);
			
			if(Validations.validateIsNull(mostRecentC)) {
				return GeneralConstants.ZERO_VALUE_INTEGER;
			}
			
			return CommonsUtilities.diffDatesInDays(mostRecentC.getExperitationDate(), operationDate);
		}
	}
	
	public Integer getDaysCurrentCoupon(Security sec, Date operationDate, ProgramInterestCoupon currentCoupon) {
		
		ProgramInterestCoupon nextCoupon = operationPriceCalculatorServiceBean.getNextCoupon(sec.getIdSecurityCodePk(), operationDate);
		
		if(Validations.validateIsNull(nextCoupon)) {
			return CommonsUtilities.diffDatesInDays( sec.getExpirationDate(), currentCoupon.getExperitationDate());
		} else {
			return CommonsUtilities.diffDatesInDays( nextCoupon.getExperitationDate(), currentCoupon.getExperitationDate());
		}
	}
	
	public BigDecimal calculateCleanPrice(Security sec, BigDecimal dirtyPrice, Date operationDate) {
		String issuerDocument = sec.getIssuer().getDocumentNumber().trim();
		String issuerName = sec.getIssuer().getBusinessName().trim();
		
		Integer daysPassed = getDaysPassedFromLastCoupon(sec, operationDate);
		
		if(issuerDocument.equals(GeneralConstants.MIN_HACIENDA_DOC) || issuerName.equals(GeneralConstants.MIN_HACIENDA_NAME)) {
			ProgramInterestCoupon mostRecentC = operationPriceCalculatorServiceBean.getMostRecentPayedCouponBySecurityPk(sec.getIdSecurityCodePk(), operationDate);
			
			Integer daysCurrentCoupon = getDaysCurrentCoupon(sec, operationDate, mostRecentC);
			
			return dirtyPrice.subtract(sec.getInterestRate().multiply(new BigDecimal(daysPassed.toString())).divide( BigDecimal.valueOf(2), MathContext.DECIMAL128).divide( new BigDecimal( daysCurrentCoupon.toString()) , MathContext.DECIMAL128 ) ).setScale(GeneralConstants.SHOW_PERCENTAGE_DECIMAL, RoundingMode.HALF_UP);
		} else {
			return dirtyPrice.subtract(sec.getInterestRate().multiply(new BigDecimal(daysPassed.toString()).divide(GeneralConstants.THREE_HUNDRED_SIXTY_FIVE, MathContext.DECIMAL128 ))).setScale(GeneralConstants.SHOW_PERCENTAGE_DECIMAL, RoundingMode.HALF_UP);
		}
	}
	
	public BigDecimal calculateTermAmount(BigDecimal cashRealAmount, BigDecimal rate, Integer termDays, Security sec, Date operationDate, Date realTermDate,
			BigDecimal stockQuantity) {
		BigDecimal interestAmount = this.operationPriceCalculatorServiceBean.getInterestAmountFromDateRange(
											sec.getIdSecurityCodePk(), operationDate, realTermDate);
		
		if(Validations.validateIsNull(interestAmount)) {
			interestAmount = BigDecimal.ZERO;
		}
		interestAmount = interestAmount.multiply(stockQuantity);
		
		BigDecimal amortizationAmount = this.operationPriceCalculatorServiceBean.getAmoritzationAmountFromDateRange(
											sec.getIdSecurityCodePk(), operationDate, realTermDate);
		
		if(Validations.validateIsNull(amortizationAmount)) {
			amortizationAmount = BigDecimal.ZERO;
		}
		amortizationAmount = amortizationAmount.multiply(stockQuantity);
		
		return cashRealAmount.multiply( 
				BigDecimal.ONE.add( rate.divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL, MathContext.DECIMAL128 ).multiply(new BigDecimal(termDays)).divide(GeneralConstants.THREE_HUNDRED_SIXTY_FIVE, MathContext.DECIMAL128 ) ) )
				.subtract(amortizationAmount.add(interestAmount));
	}
	
	public void cleanCalculationPrices(MechanismOperation op) {
		if(op.getSecurities().getIndIsCoupon().equals(BooleanType.NO.getCode())){
			op.setCashPricePercentage(
					this.calculateCleanPrice(
							op.getSecurities(), op.getRealCashPricePercentage(), op.getCashSettlementDate()));
			op.setCashPrice(op.getCashPricePercentage()
														.multiply(op.getSecurities().getCurrentNominalValue())
														.divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL, GeneralConstants.NEGO_PERCENTAGE_DECIMAL, RoundingMode.HALF_UP));
			op.setCashAmount( op.getCashPrice().multiply( op.getStockQuantity() ) );
		}
	}
	
	public void regularPricePercentageCalculation(MechanismOperation op) {
		BigDecimal nominalValue = op.getSecurities().getCurrentNominalValue();
		
		op.setRealCashPrice(
				op.getRealCashPricePercentage().multiply(nominalValue)
					.divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL, MathContext.DECIMAL128));
		op.setCashPricePercentage(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
		op.setCashPrice(nominalValue);
		
		op.setRealCashAmount(op.getRealCashPrice().multiply(op.getStockQuantity()));
		op.setCashAmount(op.getCashPrice().multiply(op.getStockQuantity()));
	}
	
	public void regularPriceCalculation(MechanismOperation op) {
		this.regularPriceCalculation(op,null);
	}
	
	public void regularPriceCalculation(MechanismOperation op, BigDecimal fixedCashPrice) {
		BigDecimal nominalValue = op.getSecurities().getCurrentNominalValue();
		if(Validations.validateIsNull(fixedCashPrice)) {
			fixedCashPrice = nominalValue;
		}
		
		BigDecimal cashPricePercentage = fixedCashPrice.divide(nominalValue, MathContext.DECIMAL128).multiply(new BigDecimal("100.00"));
		
		op.setRealCashPrice(
				op.getRealCashAmount().divide(op.getStockQuantity(), MathContext.DECIMAL128));
				
		op.setRealCashPricePercentage(
				op.getRealCashPrice().multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL)
					.divide(nominalValue, MathContext.DECIMAL128));
		op.setCashPricePercentage(cashPricePercentage);
		op.setCashPrice(fixedCashPrice);
		
		op.setCashAmount(op.getCashPrice().multiply(op.getStockQuantity()));
	}  
	     
	
	public void dirtyPriceChangeCalculation(MechanismOperation op) {
		if(op.isRateVariableModality()) {
			this.regularPricePercentageCalculation(op);
		} else if (op.isRateFixedModality()) {
			this.regularPricePercentageCalculation(op);
			
			this.cleanCalculationPrices(op);
		} else {		
			BigDecimal nominalValue = op.getSecurities().getCurrentNominalValue();
			BigDecimal rate = op.getCashPrice();
			op.setCashPrice( rate.multiply(nominalValue).divide(BigDecimal.valueOf(100d),4,RoundingMode.HALF_UP) );
		}
	}
	
	public void dirtyAmountChangeCalculation(MechanismOperation op) {
		if(op.isRateVariableModality()) {
			regularPriceCalculation(op);
		} else if (op.isRateFixedModality()) {
			regularPriceCalculation(op);
			cleanCalculationPrices(op);
		}
	}
	
	public void cleanPriceChange(MechanismOperation op) {
		if(Validations.validateIsNotNullAndPositive(op.getStockQuantity())
				&& Validations.validateIsNotNull(op.getCashPricePercentage())) {
			BigDecimal nominalValue = op.getSecurities().getCurrentNominalValue();
			
			op.setCashPrice(op.getCashPricePercentage()
					.multiply(nominalValue).divide(BigDecimal.valueOf(100d),4,RoundingMode.HALF_UP) );
			
			op.setCashAmount(op.getCashPrice()
					.multiply(op.getStockQuantity()));
		}
	}
	
	public void calculateTermRealCashAmount(MechanismOperation op) {
		Date realSettlementDate = CommonsUtilities.addDate(op.getOperationDate(),
				op.getTermSettlementDays().intValue());

		op.setTermAmount(this.calculateTermAmount(op.getRealCashAmount(), op.getTermInterestRate(),
				op.getTermSettlementDays().intValue(), op.getSecurities(), op.getOperationDate(), realSettlementDate,
				op.getStockQuantity()));

		op.setTermPrice(op.getTermAmount().divide(op.getStockQuantity(), GeneralConstants.NEGO_PERCENTAGE_DECIMAL,
				RoundingMode.HALF_UP));
	}
}
