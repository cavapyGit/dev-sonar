package com.pradera.negotiations.operations.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.GuaranteeMovement;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HelperComponentFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/01/2013
 */
@Stateless
public class MechanismOperationService extends CrudDaoServiceBean{
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	
	/**
	 * Gets the modality group.
	 *
	 * @param id the id
	 * @return the modality group
	 */
	public ModalityGroup getModalityGroup(MechanismModalityPK id) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("	SELECT mg");
		strQuery.append("	FROM ModalityGroup mg");
		strQuery.append("	WHERE mg.idModalityGroupPk = (SELECT mgd.modalityGroup.idModalityGroupPk");
		strQuery.append("		FROM ModalityGroupDetail mgd WHERE mgd.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		strQuery.append("		AND mgd.mechanismModality.id.idNegotiationModalityPk = :idNegotiationModalityPk");
		strQuery.append("		AND mgd.modGroupState = :modGroupState)");
		Query query = em.createQuery(strQuery.toString());
		query.setParameter("idNegotiationMechanismPk", id.getIdNegotiationMechanismPk());
		query.setParameter("idNegotiationModalityPk", id.getIdNegotiationModalityPk());
		query.setParameter("modGroupState", BooleanType.YES.getCode());
		try {
			return (ModalityGroup)query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Gets the mechanism operation service.
	 *
	 * @param mechanismOperationPk the mechanism operation pk
	 * @return the mechanism operation service
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation getMechanismOperationService(Long mechanismOperationPk) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT DISTINCT(mo) FROM MechanismOperation mo ");
		querySql.append(" inner join fetch mo.mechanisnModality mm ");
		querySql.append(" inner join fetch mm.negotiationMechanism nme ");
		querySql.append(" inner join fetch mm.negotiationModality nmo ");
		querySql.append(" inner join fetch mo.buyerParticipant bp ");
		querySql.append(" inner join fetch mo.sellerParticipant sp ");
		querySql.append(" inner join fetch mo.securities s  ");
		querySql.append("WHERE mo.idMechanismOperationPk = :idMechanismOperationPkParam ");
		
		Query queryJpql = em.createQuery(querySql.toString());
		queryJpql.setParameter("idMechanismOperationPkParam", mechanismOperationPk);
		
		MechanismOperation mechanismOperation = null;
		try{
			mechanismOperation = (MechanismOperation) queryJpql.getSingleResult();
			
			for(SwapOperation swapOperation: mechanismOperation.getSwapOperations()){
				swapOperation.getSecurities().getIdIsinCode();
				swapOperation.getSecurities().getIssuer().getIdIssuerPk();
				swapOperation.getSecurities().getIssuance().getIdIssuanceCodePk();
			}
			
			Long modalityId = mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
			if(NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(modalityId) ||
					NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(modalityId)){
				mechanismOperation.getReferenceOperation().getIdMechanismOperationPk();
			}
			
			//Cuando los participantes son iguales, la Operacion es Cruzada
			if(mechanismOperation.getBuyerParticipant().getIdParticipantPk().equals(mechanismOperation.getSellerParticipant().getIdParticipantPk())){
				mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.CRUSADE.getCode());
			}else{//Cuando los participantes son Diferentes, Siempre sera Venta
				mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.SALE.getCode());
			}
			
		}catch(NoResultException ex){
			return null;
		}
		
		return mechanismOperation;
	}
	
	/**
	 * Gets the settlement account operations.
	 *
	 * @param idSettlementOperation the id settlement operation
	 * @param role the role
	 * @param idStockParticipant the id stock participant
	 * @return the settlement account operations
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementAccountOperation> getSettlementAccountOperations(Long idSettlementOperation, Integer role, Long idStockParticipant) {

		List<Object[]> accountOperations = null;
		List<SettlementAccountOperation> settlementAccountOperations = new ArrayList<SettlementAccountOperation>();
		Map<String,Object> parameters = new HashMap<String,Object> ();
		Map<Long,HolderAccount> accounts = new HashMap<Long, HolderAccount>();
		
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT sao, hao.holderAccount.idHolderAccountPk FROM SettlementAccountOperation sao ");
		querySql.append(" inner join sao.holderAccountOperation hao ");
		querySql.append(" INNER JOIN FETCH sao.settlementOperation ");
		querySql.append(" where sao.settlementOperation.idSettlementOperationPk = :idSettlementOperation");
		querySql.append(" and sao.operationState = :confirmedState ");
		if(idStockParticipant!=null){
			querySql.append(" and hao.inchargeStockParticipant.idParticipantPk = :idStockParticipant  ");
			parameters.put("idStockParticipant", idStockParticipant);
		}
		if(role!=null){
			querySql.append(" and sao.role = :idRole ");
			parameters.put("idRole", role);
		}
		querySql.append(" order by sao.role");
		
		parameters.put("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("idSettlementOperation", idSettlementOperation);

		accountOperations = (List<Object[]>) findListByQueryString(querySql.toString(), parameters);
		
		for(Object[] accOperation: accountOperations){
			SettlementAccountOperation settlementAccountOperation = (SettlementAccountOperation) accOperation[0];
			Long idHolderAccount = (Long) accOperation[1];
			HolderAccount holderAccount = accounts.get(idHolderAccount);
			if(holderAccount==null){
				holderAccount = getHolderAccount(idHolderAccount);
				accounts.put(idHolderAccount, holderAccount);
			}
			settlementAccountOperation.getHolderAccountOperation().setHolderAccount(holderAccount);
			settlementAccountOperations.add(settlementAccountOperation);
		}
			
		return settlementAccountOperations;
	}

	/**
	 * Gets the account operations.
	 *
	 * @param accounts the accounts
	 * @param idSettlementOperationPk the id settlement operation pk
	 * @param fetchIncharge the fetch incharge
	 * @param role the role
	 * @param idParticipant the id participant
	 * @return the account operations
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementAccountOperation> getAccountOperations(Map<Long,HolderAccount> accounts, Long idSettlementOperationPk, boolean fetchIncharge, Integer role, Long idParticipant) {

		List<Object[]> accountOperations = null;
		List<SettlementAccountOperation> settlementAccountOperations = new ArrayList<SettlementAccountOperation>();
		Map<String,Object> parameters = new HashMap<String,Object> ();
		
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT sao, hao.holderAccount.idHolderAccountPk ");
		querySql.append("	FROM SettlementAccountOperation SAO inner join fetch SAO.holderAccountOperation hao ");
		if(fetchIncharge){
			querySql.append(" inner join fetch hao.inchargeFundsParticipant fundsP ");
			querySql.append(" inner join fetch hao.inchargeStockParticipant stockP ");
		}
		querySql.append(" where SAO.settlementOperation.idSettlementOperationPk = :idSettlementOperation");
		querySql.append(" and SAO.operationState in (:prepaidState, :confirmedState) ");
		if(idParticipant!=null){
			querySql.append(" and (hao.inchargeStockParticipant.idParticipantPk = :idParticipant or  ");
			querySql.append("      hao.inchargeFundsParticipant.idParticipantPk = :idParticipant) ");
			parameters.put("idParticipant", idParticipant);
		}
		if(role!=null){
			querySql.append(" and SAO.role = :idRole ");
			parameters.put("idRole", role);
		}
		querySql.append(" order by hao.operationPart, hao.role");
		
		parameters.put("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("prepaidState", HolderAccountOperationStateType.PREPAID.getCode());
		parameters.put("idSettlementOperation", idSettlementOperationPk);

		accountOperations = (List<Object[]>) findListByQueryString(querySql.toString(), parameters);
		
		for(Object[] accOperation: accountOperations){
			SettlementAccountOperation settlementAccountOperation = (SettlementAccountOperation) accOperation[0];
			Long idHolderAccount = (Long) accOperation[1];
			HolderAccount holderAccount = accounts.get(idHolderAccount);
			if(holderAccount==null){
				holderAccount = getHolderAccount(idHolderAccount);
				accounts.put(idHolderAccount, holderAccount);
			}
			settlementAccountOperation.getHolderAccountOperation().setHolderAccount(holderAccount);
			settlementAccountOperations.add(settlementAccountOperation);
		}
			
		return settlementAccountOperations;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param idHolderAccount the id holder account
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount(Long idHolderAccount){
		
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT ha FROM HolderAccount ha ");
		querySql.append(" inner join fetch ha.holderAccountDetails had ");
		querySql.append(" inner join fetch had.holder ho ");
		querySql.append(" where ha.idHolderAccountPk = :idHolderAccount");
		
		parameters.put("idHolderAccount", idHolderAccount);
		
		return  (HolderAccount) findObjectByQueryString(querySql.toString(), parameters);
	}
	

	/**
	 * Gets the lst swap operation.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the lst swap operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SwapOperation> getLstSwapOperation(Long idMechanismOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT so");
		sbQuery.append("	FROM SwapOperation so");
		sbQuery.append("	INNER JOIN FETCH so.securities");
		sbQuery.append("	WHERE so.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperationPk", idMechanismOperationPk);
		return (List<SwapOperation>)findListByQueryString(sbQuery.toString(), parameters);		
	}
	
	/**
	 * Gets the lst holder account operation swap.
	 *
	 * @param idSwapOperationPk the id swap operation pk
	 * @param idParticipantPk the id participant pk
	 * @param participantUser the participant user
	 * @return the lst holder account operation swap
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountOperation> getLstHolderAccountOperationSwap(Long idSwapOperationPk, Long idParticipantPk, boolean participantUser) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct hao");
		sbQuery.append("	FROM HolderAccountOperation hao");
		sbQuery.append("	INNER JOIN FETCH hao.holderAccount ha");
		sbQuery.append("	INNER JOIN FETCH hao.inchargeFundsParticipant");
		sbQuery.append("	INNER JOIN FETCH hao.inchargeStockParticipant");
		sbQuery.append("	LEFT JOIN FETCH hao.swapOperation");
		sbQuery.append("	WHERE hao.swapOperation.idSwapOperationPk = :idSwapOperationPk");
		sbQuery.append("	AND hao.holderAccountState = :holderAccountState");
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk) && participantUser)
			sbQuery.append("	AND hao.inchargeStockParticipant.idParticipantPk = :idParticipantPk");
		
		sbQuery.append("	ORDER BY hao.operationPart ASC");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSwapOperationPk", idSwapOperationPk);
		parameters.put("holderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk) && participantUser)
			parameters.put("idParticipantPk", idParticipantPk);
		return (List<HolderAccountOperation>)findListByQueryString(sbQuery.toString(), parameters);		
	}
	
	/**
	 * Checks for balance.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @param idSecurityCodePk the id security code pk
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean hasBalance(HolderAccountOperation holderAccountOperation, String idSecurityCodePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT nvl(count(hab),0) FROM HolderAccountBalance hab");
		sbQuery.append("	WHERE hab.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND	hab.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND hab.participant.idParticipantPk = :idParticipantPk");			
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idHolderAccountPk", holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
		query.setParameter("idParticipantPk", holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		
		Long count  = (Long)query.getSingleResult();	
		if(count >= 1){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param holderAccountOperation the holder account operation
	 * @param idSecurityCode the id security code
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getHolderAccountBalance(Long idMechanismOperationPk, HolderAccountOperation holderAccountOperation, String idSecurityCode) throws ServiceException{
		List<HolderAccountBalance>  balances = getHolderAccountBalance(holderAccountOperation, idSecurityCode);
		return balances;
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @param idSecurityCode the id security code
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> getHolderAccountBalance(HolderAccountOperation holderAccountOperation, String idSecurityCode) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hab FROM HolderAccountBalance hab");
		sbQuery.append("	WHERE hab.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND	hab.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND hab.participant.idParticipantPk = :idParticipantPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountPk", holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
		parameters.put("idParticipantPk", holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
		parameters.put("idSecurityCodePk", idSecurityCode);
		return (List<HolderAccountBalance>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the holder account movement.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param holderAccountOperation the holder account operation
	 * @param idSecurityCode the id security code
	 * @return the holder account movement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovement> getHolderAccountMovement(Long idMechanismOperationPk, HolderAccountOperation holderAccountOperation, String idSecurityCode) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ham FROM HolderAccountMovement ham												");
		sbQuery.append("	INNER JOIN FETCH ham.movementType mt 													");
		sbQuery.append("	WHERE ham.holderAccountBalance.holderAccount.idHolderAccountPk = :idHolderAccountPk 	");
		sbQuery.append("	AND	ham.holderAccountBalance.security.idSecurityCodePk = :idSecurityCodePk				");
		sbQuery.append("	AND ham.holderAccountBalance.participant.idParticipantPk = :idParticipantPk				");
		sbQuery.append("	AND ham.tradeOperation.idTradeOperationPk = :idTradeOperationPk							");
		sbQuery.append("	AND mt.indVisibility = :onePara 														");
		sbQuery.append("	ORDER BY ham.movementDate asc															");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountPk", holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
		parameters.put("idParticipantPk", holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
		parameters.put("idSecurityCodePk", idSecurityCode);
		parameters.put("idTradeOperationPk", idMechanismOperationPk);
		parameters.put("onePara", BooleanType.YES.getCode());
		return (List<HolderAccountMovement>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Checks for guarantee balances.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param guaranteeClass the guarantee class
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean hasGuaranteeBalances(Long idHolderAccountOperation, Integer guaranteeClass) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT nvl(count(agb.idAccountGuarBalancePk),0) ");
		sbQuery.append("	FROM AccountGuaranteeBalance agb ");
		sbQuery.append("	where agb.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperationPk");
		sbQuery.append("	AND agb.guaranteeClass = :guaranteeClass");			
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idHolderAccountOperationPk", idHolderAccountOperation);
		query.setParameter("guaranteeClass", guaranteeClass);		
		try {
			Long count=  (Long)query.getSingleResult();
			if(count > 0)
				return true;
		} catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Gets the guarantee balances.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param classType the class type
	 * @return the guarantee balances
	 */
	@SuppressWarnings("unchecked")
	public List<AccountGuaranteeBalance> getGuaranteeBalances(Long idHolderAccountOperation, Integer classType) {
		StringBuilder sbQuery = new StringBuilder();		
		if(classType.equals(GuaranteeClassType.SECURITIES.getCode())){
			sbQuery.append(" SELECT new AccountGuaranteeBalance ( ");
			sbQuery.append("	agb.idAccountGuarBalancePk, agb.totalGuarantee,agb.principalGuarantee, " );
			sbQuery.append("	agb.marginGuarantee, agb.principalDividendsGuarantee,agb.marginDividendsGuarantee, " );
			sbQuery.append("	agb.interestMarginGuarantee, agb.guaranteeClass,agb.currency, " );
			sbQuery.append("	(select pt.parameterName from ParameterTable pt where pt.parameterTablePk = agb.guaranteeClass ), " );
			sbQuery.append("	ha.idHolderAccountPk, ha.alternateCode, " );
			sbQuery.append("	pa.idParticipantPk, pa.mnemonic, " );
			sbQuery.append("	sec.idSecurityCodePk ");
			sbQuery.append("	) " );
			sbQuery.append("	FROM AccountGuaranteeBalance agb " );
			sbQuery.append("	inner join agb.securities sec " );
			sbQuery.append("	inner join agb.participant pa  " );
			sbQuery.append("	inner join agb.holderAccount ha  " );
			sbQuery.append("	where agb.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperationPk " );
			sbQuery.append("	AND agb.guaranteeClass = :guaranteeClass " );			
		}else if(classType.equals(GuaranteeClassType.FUNDS.getCode())){			
			sbQuery.append(" SELECT new AccountGuaranteeBalance ( ");
			sbQuery.append("  agb.idAccountGuarBalancePk, agb.totalGuarantee,agb.principalGuarantee, " );
			sbQuery.append("  agb.marginGuarantee, agb.principalDividendsGuarantee,agb.marginDividendsGuarantee, " );
			sbQuery.append("  agb.interestMarginGuarantee, agb.guaranteeClass,agb.currency, " );
			sbQuery.append("  (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = agb.guaranteeClass ), " );
			sbQuery.append("  (select pt.description from ParameterTable pt where pt.parameterTablePk = agb.currency ), " );
			sbQuery.append("  ica.idInstitutionCashAccountPk ");
			sbQuery.append(" ) " );
			sbQuery.append(" FROM AccountGuaranteeBalance agb " );
			sbQuery.append(" inner join agb.institutionCashAccount ica ");
			sbQuery.append(" where agb.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperationPk " );
			sbQuery.append(" AND agb.guaranteeClass = :guaranteeClass " );			
		}			
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idHolderAccountOperationPk", idHolderAccountOperation);
		query.setParameter("guaranteeClass", classType);		
		return (List<AccountGuaranteeBalance>)query.getResultList();
	}
	
	/**
	 * Gets the mechanism operation repo sec.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the mechanism operation repo sec
	 * @throws ServiceException the service exception
	 */
	public List<MechanismOperation> getMechanismOperationRepoSec(Long idMechanismOperationPk) throws ServiceException{
		List<MechanismOperation> lstTemp = getMechanismOperationRepoSecData(idMechanismOperationPk);
		for(MechanismOperation mechaOpeTemp:lstTemp){
			for(HolderAccountOperation holAccOpe:mechaOpeTemp.getHolderAccountOperations()){
				holAccOpe.getHolderAccount().getHolderAccountDetails().size();//Se recupera el detail de las cuentas
			}
		}
		return lstTemp;
	}
	
	/**
	 * Gets the mechanism operation repo sec data.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the mechanism operation repo sec data
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> getMechanismOperationRepoSecData(Long idMechanismOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct mo");
		sbQuery.append("	FROM MechanismOperation mo");
		sbQuery.append("	INNER JOIN FETCH mo.holderAccountOperations hao");
		sbQuery.append("	INNER JOIN FETCH hao.holderAccount ha");
		sbQuery.append("	INNER JOIN FETCH hao.inchargeFundsParticipant");
		sbQuery.append("	INNER JOIN FETCH hao.inchargeStockParticipant");
		sbQuery.append("	WHERE mo.referenceOperation.idMechanismOperationPk = :idMechanismOperationPk");
		sbQuery.append("	AND hao.holderAccountState = :holderAccountState");
		sbQuery.append("	AND mo.operationState = :cashSettled");
		sbQuery.append("	ORDER BY mo.operationDate DESC");		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperationPk", idMechanismOperationPk);
		parameters.put("holderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		parameters.put("cashSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
		return (List<MechanismOperation>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the guarantee movements.
	 *
	 * @param accountGuarBalancePk the account guar balance pk
	 * @return the guarantee movements
	 */
	@SuppressWarnings("unchecked")
	public List<GuaranteeMovement> getGuaranteeMovements(Long accountGuarBalancePk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT gmv from GuaranteeMovement gmv ");
		sbQuery.append("  inner join fetch gmv.movementType mt " );
		sbQuery.append(" where gmv.accountGuaranteeBalance.idAccountGuarBalancePk = :idAccountGuarBalancePk " );
		sbQuery.append(" order by gmv.movementDate asc " );			
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idAccountGuarBalancePk", accountGuarBalancePk);
		return (List<GuaranteeMovement>)query.getResultList();
	}
	
	/**
	 * Checks for repo sec.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean hasRepoSec(Long idMechanismOperationPk) throws ServiceException {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT COUNT(*)");
			sbQuery.append("	FROM MechanismOperation mo");
			sbQuery.append("	WHERE mo.referenceOperation.idMechanismOperationPk = :idMechanismOperationPk");
			sbQuery.append("	AND mo.operationState = :cashSettled");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
			query.setParameter("cashSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult())){
				if(new Integer(query.getSingleResult().toString()) > 0)
					return true;
				else
					return false;
			}
		} catch (NoResultException e) {
			return false;
		}
		return false;
	}
}