package com.pradera.negotiations.operations.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.guarantees.AccountGuaranteeBalance;
import com.pradera.model.guarantees.GuaranteeMovement;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.SwapOperation;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.negotiations.operations.service.MechanismOperationService;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.utils.PropertiesConstants;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SecuritiesHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@HelperBean
public class OperationBusinessBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5036009796727734519L;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;
	
	/** The cash settlement operation. */
	private SettlementOperation cashSettlementOperation;
	
	/** The term settlement operation. */
	private SettlementOperation termSettlementOperation;
	
	/** The prepaid settlement operations. */
	private List<SettlementOperation> prepaidSettlementOperations;
	
	/** The selected prepaid operation. */
	private SettlementOperation selectedPrepaidOperation;
	
	/** The settlement account operations. */
	private List<SettlementAccountOperation> settlementAccountOperations;
	
	/** The settlement account operation. */
	private SettlementAccountOperation settlementAccountOperation;
	
	/** The swap operation. */
	private SwapOperation swapOperation;
	
	/** The modality group. */
	private ModalityGroup modalityGroup;
	
	/** The id mechanism operation pk. */
	private Long idMechanismOperationPk;
	
	/** The lst holder account balances. */
	private List<HolderAccountBalance> lstHolderAccountBalances;
	
	/** The lst holder account movements. */
	private List<HolderAccountMovement> lstHolderAccountMovements;
	
	/** The lst securities guarantee balances. */
	private GenericDataModel<AccountGuaranteeBalance> lstSecuritiesGuaranteeBalances;
	
	/** The lst funds guarantee balances. */
	private GenericDataModel<AccountGuaranteeBalance> lstFundsGuaranteeBalances;
	
	/** The account guar balance. */
	private AccountGuaranteeBalance accountGuarBalance;
	
	/** The lst guarantee movements. */
	private GenericDataModel<GuaranteeMovement> lstGuaranteeMovements;
	
	/** The lst mechanism operation repo sec. */
	private List<MechanismOperation> lstMechanismOperationRepoSec;
	
	/** The show assignments. */
	private boolean showAssignments;
	
	/** The show prepaids. */
	private boolean showPrepaids;
	
	/** The show view details. */
	private boolean showViewDetails;
	
	/** The bl has repo sec. */
	private boolean blHasRepoSec;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The mechanism operation service. */
	@EJB
	private MechanismOperationService mechanismOperationService;
	
	/** The participant code. */
	Long participantCode = null;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency = new ArrayList<ParameterTable>();
	
	/** The operation request origin list. */
	private List<ParameterTable> operationRequestOriginList;
	
	/** The mp references. */
	/*Indicadores de FONDOS Y VALORES*/
	Map<Long, String> mpReferences = new HashMap<Long, String>();	
	
	/** The mp descriptions. */
	Map<Long, String> mpDescriptions = new HashMap<Long,  String>();
	
	/** The mp parameters. */
	Map<Integer, String> mpParameters = new HashMap<Integer,  String>();	
	
	/** The negotiation mechanism list. */
	private List<NegotiationMechanism> negotiationMechanismList;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try{
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				participantCode = userInfo.getUserAccountSession().getParticipantCode();
			}
			loadCboOtcOriginRequestType();
			loadParameters();
			negotiationMechanismList = settlementProcessService.getNegotiationMechanismService();
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParameters() throws ServiceException {

		ParameterTableTO paramTO = new ParameterTableTO();
		paramTO.setLstMasterTableFk(new ArrayList<Integer>());
		//paramTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.IND_STOCK_REFERENCE_TYPE.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.IND_FUNDS_REFERENCE_TYPE.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.IND_MARGIN_REFERENCE_TYPE.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.MOTIVOS_CANCELACION_OPERACION_MCN.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.INSTRUMENT_TYPE.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.CURRENCY.getCode());
		
		for(ParameterTable paramTab:generalParameterFacade.getListParameterTableServiceBean(paramTO)){			
			mpReferences.put(new Long(paramTab.getParameterTablePk()), paramTab.getText1());		
			mpDescriptions.put(new Long(paramTab.getParameterTablePk()), paramTab.getParameterName());
			if(MasterTableType.ESTADOS_OPERACION_MCN.getCode().equals(paramTab.getMasterTable().getMasterTablePk()) ||
					MasterTableType.INSTRUMENT_TYPE.getCode().equals(paramTab.getMasterTable().getMasterTablePk()) ||
					MasterTableType.MOTIVOS_CANCELACION_OPERACION_MCN.getCode().equals(paramTab.getMasterTable().getMasterTablePk()) ){
				mpParameters.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			}
			if(MasterTableType.CURRENCY.getCode().equals(paramTab.getMasterTable().getMasterTablePk())){
				lstCurrency.add(paramTab);
			}
		}
		
	}

	/**
	 * Load cbo otc origin request type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboOtcOriginRequestType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.OTC_REQUEST_ORIGING.getCode());
		List<ParameterTable> lstCboOtcOriginRequestType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		ParameterTable parameterTable = new ParameterTable();
		parameterTable.setParameterTablePk(OtcOperationOriginRequestType.PURCHASE.getCode().intValue());//Delete purchase, in OTC exist only Sell and Crusade
		lstCboOtcOriginRequestType.remove(parameterTable);
		operationRequestOriginList = lstCboOtcOriginRequestType;
	}

	/**
	 * Search mechanism operation.
	 */
	public void searchMechanismOperation(){
		
		settlementAccountOperation = null;
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		if(idMechanismOperationPk==null){
			ValueExpression mechanismPkExpresion = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.mechanismOperationPk}", Long.class);
			idMechanismOperationPk = (Long)mechanismPkExpresion.getValue(elContext);
		}
		
		if(idMechanismOperationPk!=null){
			try {

				mechanismOperation = mechanismOperationService.getMechanismOperationService(idMechanismOperationPk);
				mechanismOperation.setStateDescription(mpParameters.get(mechanismOperation.getOperationState()));
				mechanismOperation.getSecurities().setInstrumentTypeDescription(mpParameters.get(mechanismOperation.getSecurities().getInstrumentType()));

				if(mechanismOperation.getCancelMotive()!=null){
					mechanismOperation.setCancelDescription(mpParameters.get(mechanismOperation.getCancelMotive()));
				}

				modalityGroup = mechanismOperationService.getModalityGroup(mechanismOperation.getMechanisnModality().getId());
				
				cashSettlementOperation = getSettlementData(idMechanismOperationPk, ComponentConstant.CASH_PART);
				
				if(ComponentConstant.ONE.equals(mechanismOperation.getIndTermSettlement())){
					termSettlementOperation = getSettlementData(idMechanismOperationPk, ComponentConstant.TERM_PART);
				}
				
				if(showAssignments){
					
					Map<Long,HolderAccount> accounts = new HashMap<Long, HolderAccount>();
					settlementAccountOperations = new ArrayList<SettlementAccountOperation>();
					
					List<SettlementAccountOperation> accountOperations = getAccountOperationData(cashSettlementOperation,accounts);
					
					if(Validations.validateListIsNotNullAndNotEmpty(accountOperations)){
						settlementAccountOperations.addAll(accountOperations);
					}
					
					if(ComponentConstant.ONE.equals(mechanismOperation.getIndTermSettlement())){
						List<SettlementAccountOperation> termAccountOperations = getAccountOperationData(termSettlementOperation,accounts);
						
						if(Validations.validateListIsNotNullAndNotEmpty(termAccountOperations)){
							settlementAccountOperations.addAll(termAccountOperations);
						}
					}
				}
				
				if(showViewDetails){
					blHasRepoSec = mechanismOperationService.hasRepoSec(mechanismOperation.getIdMechanismOperationPk());
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Gets the account operation data.
	 *
	 * @param settlementOperation the settlement operation
	 * @param accounts the accounts
	 * @return the account operation data
	 */
	private List<SettlementAccountOperation> getAccountOperationData(SettlementOperation settlementOperation, Map<Long, HolderAccount> accounts) {
		List<SettlementAccountOperation> accountOperations =  mechanismOperationService.getAccountOperations(accounts, settlementOperation.getIdSettlementOperationPk(),true,null,participantCode);
		
		if(Validations.validateIsNotNullAndNotEmpty(accountOperations)){		
			for(SettlementAccountOperation settAccOpe : accountOperations){	
				settAccOpe.setSettlementAccountMarketfacts(null);
				if(Validations.validateIsNotNullAndNotEmpty(settAccOpe.getFundsReference())){
					//IND_FUNDS
					settAccOpe.setIndFundReference(mpReferences.get(settAccOpe.getFundsReference()));
					settAccOpe.setFundReferenceDesc(mpDescriptions.get(settAccOpe.getFundsReference()));
				}
				
				if(Validations.validateIsNotNullAndNotEmpty(settAccOpe.getStockReference())){
					//IND_STOCK
					settAccOpe.setIndStockReference(mpReferences.get(settAccOpe.getStockReference()));
					settAccOpe.setStockReferenceDesc(mpDescriptions.get(settAccOpe.getStockReference()));
				}	
				
				if(Validations.validateIsNotNullAndNotEmpty(settAccOpe.getHolderAccountOperation().getMarginReference())){
					//IND_STOCK
					settAccOpe.getHolderAccountOperation().setIndMarginReference(mpReferences.get(settAccOpe.getHolderAccountOperation().getMarginReference()));
					settAccOpe.getHolderAccountOperation().setIndMarginReference(mpDescriptions.get(settAccOpe.getHolderAccountOperation().getMarginReference()));
				}
				
				// Add indicator chained
				if(Validations.validateIsNotNullAndNotEmpty(settAccOpe.getChainedQuantity()) && 
						settAccOpe.getChainedQuantity().compareTo(GeneralConstants.ZERO_VALUE_BIGDECIMAL) > 0) {					
					settAccOpe.setIndChained(GeneralConstants.STR_IND_CHAINED);
					String strMessage = PropertiesUtilities.getMessage(new Locale("es"), 
							PropertiesConstants.CHAINS_QUANTITY_CHAINED, new Object[]{settAccOpe.getChainedQuantity()});
					settAccOpe.setChainedDesc(strMessage);
				} else {
					settAccOpe.setIndChained(null);
				}
			}
		}
		return accountOperations;
	}

	/**
	 * Gets the settlement data.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param operationPart the operation part
	 * @return the settlement data
	 */
	public SettlementOperation getSettlementData(Long idMechanismOperationPk, Integer operationPart) {
		SettlementOperation settlementOperation = (SettlementOperation) settlementProcessService.getSettlementOperation(idMechanismOperationPk, operationPart);
		
		if(Validations.validateIsNotNullAndNotEmpty(settlementOperation.getStockReference())){
			settlementOperation.setStockReferenceDesc(mpReferences.get(settlementOperation.getStockReference()) + 
													GeneralConstants.DASH + mpDescriptions.get(settlementOperation.getStockReference()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(settlementOperation.getFundsReference())){
			settlementOperation.setFundsReferenceDesc(mpReferences.get(settlementOperation.getFundsReference()) + 
													GeneralConstants.DASH + mpDescriptions.get(settlementOperation.getFundsReference()));
		}
		if(Validations.validateIsNotNull(settlementOperation.getIndForcedPurchase())){
			if(settlementOperation.getIndForcedPurchase().equals(BooleanType.NO.getCode())){
				settlementOperation.setForcedPurchase(Boolean.FALSE);
			}else{
				settlementOperation.setForcedPurchase(Boolean.TRUE);
			}
		}else{
			settlementOperation.setForcedPurchase(Boolean.FALSE);
		}
		return settlementOperation;
	}
	
	/**
	 * View market facts.
	 */
	public void viewMarketFacts(){
		if(validateSelected()){
			settlementAccountOperation.setSettlementAccountMarketfacts(
					settlementProcessService.getSettlementAccountMarketfacts(settlementAccountOperation.getIdSettlementAccountPk()));
			JSFUtilities.executeJavascriptFunction("PF('dlgWMarketFacts').show();");
		}
		
	}

	/**
	 * View balance and movements.
	 */
	public void viewBalanceAndMovements(){
		lstHolderAccountBalances = null;
		lstHolderAccountMovements = null;
		
		try {
			if(validateSelected()){
				HolderAccountOperation holderAccountOperation = settlementAccountOperation.getHolderAccountOperation();
				boolean hasBalance = false;
				hasBalance = mechanismOperationService.hasBalance(holderAccountOperation, mechanismOperation.getSecurities().getIdSecurityCodePk());
				
				if(hasBalance){
					
					String idSecurityCode = null;
					idSecurityCode = mechanismOperation.getSecurities().getIdSecurityCodePk();
					
					lstHolderAccountBalances = mechanismOperationService.getHolderAccountBalance(mechanismOperation.getIdMechanismOperationPk() , holderAccountOperation, idSecurityCode);
					if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalances)){
						lstHolderAccountMovements = mechanismOperationService.getHolderAccountMovement(idMechanismOperationPk, holderAccountOperation, idSecurityCode);
					}
					
					JSFUtilities.executeJavascriptFunction("PF('dlgWBalance').show();");
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.query.operations.noBalance"));
					JSFUtilities.showSimpleValidationDialog();
					holderAccountOperation = null;
				}
			}	
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * View prepaids.
	 *
	 * @throws ServiceException the service exception
	 */	
	public void viewPrepaids() throws ServiceException{
		selectedPrepaidOperation = null;
		
		prepaidSettlementOperations = (List<SettlementOperation>) settlementProcessService.getSettlementOperations(
				idMechanismOperationPk, ComponentConstant.TERM_PART, ComponentConstant.ONE);
		
		if(Validations.validateListIsNotNullAndNotEmpty(prepaidSettlementOperations)){
			for (SettlementOperation settlementOperation : prepaidSettlementOperations) {
				settlementOperation.setStateDescription(mpParameters.get(settlementOperation.getOperationState()));
				
				if(Validations.validateIsNotNullAndNotEmpty(settlementOperation.getStockReference())){
					settlementOperation.setStockReferenceDesc(mpReferences.get(settlementOperation.getStockReference()) + 
															GeneralConstants.DASH + mpDescriptions.get(settlementOperation.getStockReference()));
				}
				if(Validations.validateIsNotNullAndNotEmpty(settlementOperation.getFundsReference())){
					settlementOperation.setFundsReferenceDesc(mpReferences.get(settlementOperation.getFundsReference()) + 
															GeneralConstants.DASH + mpDescriptions.get(settlementOperation.getFundsReference()));
				}
				
			}
			
			JSFUtilities.executeJavascriptFunction("PF('dlgWPrepaids').show();");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("alt.query.operations.noPartialPrepaids"));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * On change prepaid operation.
	 */
	public void onChangePrepaidOperation(){
		if(selectedPrepaidOperation!=null){
			Map<Long, HolderAccount> accounts = new HashMap<Long, HolderAccount>();
			List<SettlementAccountOperation> accountOperations =  getAccountOperationData(selectedPrepaidOperation, accounts);
			selectedPrepaidOperation.setSettlementAccountOperations(accountOperations);
		}
		
	}
	
	/**
	 * View guarantees.
	 *
	 * @throws ServiceException the service exception
	 */
	public void viewGuarantees() throws ServiceException{
		if(validateSelected()){
			Long idHolderAccountOperation = settlementAccountOperation.getHolderAccountOperation().getIdHolderAccountOperationPk();
			lstGuaranteeMovements = null ;
			lstFundsGuaranteeBalances = null;
			lstSecuritiesGuaranteeBalances = null;
			lstHolderAccountBalances = null;
			boolean hasFunds = mechanismOperationService.hasGuaranteeBalances(idHolderAccountOperation,GuaranteeClassType.FUNDS.getCode());
			if(hasFunds){
				List<AccountGuaranteeBalance> returnList = mechanismOperationService.getGuaranteeBalances(idHolderAccountOperation,GuaranteeClassType.FUNDS.getCode());
				lstFundsGuaranteeBalances = new GenericDataModel<AccountGuaranteeBalance>(returnList);
			}
			
			boolean hasSecurities = mechanismOperationService.hasGuaranteeBalances(idHolderAccountOperation,GuaranteeClassType.SECURITIES.getCode());
			if(hasSecurities){
				List<AccountGuaranteeBalance> returnList = mechanismOperationService.getGuaranteeBalances(idHolderAccountOperation,GuaranteeClassType.SECURITIES.getCode());
				lstSecuritiesGuaranteeBalances = new GenericDataModel<AccountGuaranteeBalance>(returnList);
			}
			
			if(!hasFunds && !hasSecurities){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("alt.query.operations.noGuaranteeBalance"));
				JSFUtilities.showSimpleValidationDialog();
				settlementAccountOperation = null;
			}else{
				JSFUtilities.executeJavascriptFunction("PF('dlgWGuarantees').show();");
			}
			
		}
	}
	
	/**
	 * Hide details.
	 */
	public void hideDetails(){
		settlementAccountOperations = null;
		settlementAccountOperation = null;
		selectedPrepaidOperation = null;
		lstMechanismOperationRepoSec = null;
	}
	
	/**
	 * View repo sec.
	 */
	public void viewRepoSec(){
		if(Validations.validateIsNotNullAndNotEmpty(settlementAccountOperation))
			lstHolderAccountBalances = null;
		try {
			lstMechanismOperationRepoSec = mechanismOperationService.getMechanismOperationRepoSec(mechanismOperation.getIdMechanismOperationPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		JSFUtilities.executeJavascriptFunction("PF('dlgWRepoSec').show();");
	}
	
	/**
	 * View split.
	 */
	public void viewSplit(){
		if(validateSelected()){
			if(ParticipantRoleType.SELL.getCode().equals(settlementAccountOperation.getRole())){
				lstHolderAccountBalances = null;
				JSFUtilities.executeJavascriptFunction("PF('dlgWSplit').show();");
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("alt.query.operations.noSeller"));
				JSFUtilities.showSimpleValidationDialog();
				settlementAccountOperation = null;
			}
		}			
	}
	
	/**
	 * Validate selected.
	 *
	 * @return true, if successful
	 */
	private boolean validateSelected(){
		if(Validations.validateIsNotNullAndNotEmpty(settlementAccountOperation)){
			return true;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.query.no.record.selected"));
			JSFUtilities.showSimpleValidationDialog();
			return false;
		}
	}
	
	/**
	 * On acc guar selection.
	 *
	 * @param idAccountGuarBalancePk the id account guar balance pk
	 */
	public void onAccGuarSelection(Long idAccountGuarBalancePk){
		List<GuaranteeMovement> returnList = mechanismOperationService.getGuaranteeMovements(idAccountGuarBalancePk);
		if(returnList.size() > 0){
			lstGuaranteeMovements = new GenericDataModel<GuaranteeMovement>(returnList);
		}else{
			lstGuaranteeMovements = null;
		}
	}
	
	/**
	 * Gets the mechanism code.
	 *
	 * @param mechanism the mechanism
	 * @return the mechanism code
	 */
	public String getMechanismCode(Long mechanism){
		for(NegotiationMechanism negotiationMechanism: this.negotiationMechanismList){
			if(negotiationMechanism.getIdNegotiationMechanismPk().equals(mechanism)){
				return negotiationMechanism.getMechanismCode()+"-"+NegotiationMechanismType.get(mechanism);
			}
		}
		return null;
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	/**
	 * Gets the settlement schema type list.
	 *
	 * @return the settlement schema type list
	 */
	public List<SettlementSchemaType> getSettlementSchemaTypeList(){
		return SettlementSchemaType.list;
	}
	
	/**
	 * Gets the settlement type list.
	 *
	 * @return the settlement type list
	 */
	public List<SettlementType> getSettlementTypeList(){
		return SettlementType.list;
	}

	/**
	 * Gets the operation request origin list.
	 *
	 * @return the operation request origin list
	 */
	public List<ParameterTable> getOperationRequestOriginList() {
		return operationRequestOriginList;
	}

	/**
	 * Sets the operation request origin list.
	 *
	 * @param operationRequestOriginList the new operation request origin list
	 */
	public void setOperationRequestOriginList(List<ParameterTable> operationRequestOriginList) {
		this.operationRequestOriginList = operationRequestOriginList;
	}

	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the id mechanism operation pk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	
	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the new id mechanism operation pk
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}

	/**
	 * Gets the swap operation.
	 *
	 * @return the swap operation
	 */
	public SwapOperation getSwapOperation() {
		return swapOperation;
	}
	
	/**
	 * Sets the swap operation.
	 *
	 * @param swapOperation the new swap operation
	 */
	public void setSwapOperation(SwapOperation swapOperation) {
		this.swapOperation = swapOperation;
	}

	/**
	 * Gets the modality group.
	 *
	 * @return the modality group
	 */
	public ModalityGroup getModalityGroup() {
		return modalityGroup;
	}

	/**
	 * Sets the modality group.
	 *
	 * @param modalityGroup the new modality group
	 */
	public void setModalityGroup(ModalityGroup modalityGroup) {
		this.modalityGroup = modalityGroup;
	}

	/**
	 * Checks if is show assignments.
	 *
	 * @return true, if is show assignments
	 */
	public boolean isShowAssignments() {
		return showAssignments;
	}

	/**
	 * Sets the show assignments.
	 *
	 * @param showAssignments the new show assignments
	 */
	public void setShowAssignments(boolean showAssignments) {
		this.showAssignments = showAssignments;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Checks if is show view details.
	 *
	 * @return true, if is show view details
	 */
	public boolean isShowViewDetails() {
		return showViewDetails;
	}

	/**
	 * Sets the show view details.
	 *
	 * @param showViewDetails the new show view details
	 */
	public void setShowViewDetails(boolean showViewDetails) {
		this.showViewDetails = showViewDetails;
	}

	/**
	 * Checks if is bl has repo sec.
	 *
	 * @return true, if is bl has repo sec
	 */
	public boolean isBlHasRepoSec() {
		return blHasRepoSec;
	}

	/**
	 * Sets the bl has repo sec.
	 *
	 * @param blHasRepoSec the new bl has repo sec
	 */
	public void setBlHasRepoSec(boolean blHasRepoSec) {
		this.blHasRepoSec = blHasRepoSec;
	}
	
	/**
	 * Gets the lst securities guarantee balances.
	 *
	 * @return the lst securities guarantee balances
	 */
	public GenericDataModel<AccountGuaranteeBalance> getLstSecuritiesGuaranteeBalances() {
		return lstSecuritiesGuaranteeBalances;
	}

	/**
	 * Sets the lst securities guarantee balances.
	 *
	 * @param lstSecuritiesGuaranteeBalances the new lst securities guarantee balances
	 */
	public void setLstSecuritiesGuaranteeBalances(
			GenericDataModel<AccountGuaranteeBalance> lstSecuritiesGuaranteeBalances) {
		this.lstSecuritiesGuaranteeBalances = lstSecuritiesGuaranteeBalances;
	}

	/**
	 * Gets the lst funds guarantee balances.
	 *
	 * @return the lst funds guarantee balances
	 */
	public GenericDataModel<AccountGuaranteeBalance> getLstFundsGuaranteeBalances() {
		return lstFundsGuaranteeBalances;
	}

	/**
	 * Sets the lst funds guarantee balances.
	 *
	 * @param lstFundsGuaranteeBalances the new lst funds guarantee balances
	 */
	public void setLstFundsGuaranteeBalances(
			GenericDataModel<AccountGuaranteeBalance> lstFundsGuaranteeBalances) {
		this.lstFundsGuaranteeBalances = lstFundsGuaranteeBalances;
	}

	/**
	 * Gets the lst guarantee movements.
	 *
	 * @return the lst guarantee movements
	 */
	public GenericDataModel<GuaranteeMovement> getLstGuaranteeMovements() {
		return lstGuaranteeMovements;
	}

	/**
	 * Sets the lst guarantee movements.
	 *
	 * @param lstGuaranteeMovements the new lst guarantee movements
	 */
	public void setLstGuaranteeMovements(
			GenericDataModel<GuaranteeMovement> lstGuaranteeMovements) {
		this.lstGuaranteeMovements = lstGuaranteeMovements;
	}

	/**
	 * Gets the account guar balance.
	 *
	 * @return the account guar balance
	 */
	public AccountGuaranteeBalance getAccountGuarBalance() {
		return accountGuarBalance;
	}

	/**
	 * Sets the account guar balance.
	 *
	 * @param accountGuarBalance the new account guar balance
	 */
	public void setAccountGuarBalance(AccountGuaranteeBalance accountGuarBalance) {
		this.accountGuarBalance = accountGuarBalance;
	}

	/**
	 * Gets the lst mechanism operation repo sec.
	 *
	 * @return the lst mechanism operation repo sec
	 */
	public List<MechanismOperation> getLstMechanismOperationRepoSec() {
		return lstMechanismOperationRepoSec;
	}

	/**
	 * Sets the lst mechanism operation repo sec.
	 *
	 * @param lstMechanismOperationRepoSec the new lst mechanism operation repo sec
	 */
	public void setLstMechanismOperationRepoSec(
			List<MechanismOperation> lstMechanismOperationRepoSec) {
		this.lstMechanismOperationRepoSec = lstMechanismOperationRepoSec;
	}

	/**
	 * Gets the lst holder account balances.
	 *
	 * @return the lst holder account balances
	 */
	public List<HolderAccountBalance> getLstHolderAccountBalances() {
		return lstHolderAccountBalances;
	}

	/**
	 * Sets the lst holder account balances.
	 *
	 * @param lstHolderAccountBalances the new lst holder account balances
	 */
	public void setLstHolderAccountBalances(List<HolderAccountBalance> lstHolderAccountBalances) {
		this.lstHolderAccountBalances = lstHolderAccountBalances;
	}
	
	/**
	 * Gets the lst holder account movements.
	 *
	 * @return the lst holder account movements
	 */
	public List<HolderAccountMovement> getLstHolderAccountMovements() {
		return lstHolderAccountMovements;
	}

	/**
	 * Sets the lst holder account movements.
	 *
	 * @param lstHolderAccountMovements the new lst holder account movements
	 */
	public void setLstHolderAccountMovements(
			List<HolderAccountMovement> lstHolderAccountMovements) {
		this.lstHolderAccountMovements = lstHolderAccountMovements;
	}

	/**
	 * Checks if is show prepaids.
	 *
	 * @return true, if is show prepaids
	 */
	public boolean isShowPrepaids() {
		return showPrepaids;
	}

	/**
	 * Gets the cash settlement operation.
	 *
	 * @return the cash settlement operation
	 */
	public SettlementOperation getCashSettlementOperation() {
		return cashSettlementOperation;
	}

	/**
	 * Sets the cash settlement operation.
	 *
	 * @param cashSettlementOperation the new cash settlement operation
	 */
	public void setCashSettlementOperation(
			SettlementOperation cashSettlementOperation) {
		this.cashSettlementOperation = cashSettlementOperation;
	}


	/**
	 * Gets the term settlement operation.
	 *
	 * @return the term settlement operation
	 */
	public SettlementOperation getTermSettlementOperation() {
		return termSettlementOperation;
	}

	/**
	 * Sets the term settlement operation.
	 *
	 * @param termSettlementOperation the new term settlement operation
	 */
	public void setTermSettlementOperation(
			SettlementOperation termSettlementOperation) {
		this.termSettlementOperation = termSettlementOperation;
	}

	/**
	 * Sets the show prepaids.
	 *
	 * @param showPrepaids the new show prepaids
	 */
	public void setShowPrepaids(boolean showPrepaids) {
		this.showPrepaids = showPrepaids;
	}

	/**
	 * Gets the prepaid settlement operations.
	 *
	 * @return the prepaid settlement operations
	 */
	public List<SettlementOperation> getPrepaidSettlementOperations() {
		return prepaidSettlementOperations;
	}

	/**
	 * Sets the prepaid settlement operations.
	 *
	 * @param prepaidSettlementOperations the new prepaid settlement operations
	 */
	public void setPrepaidSettlementOperations(
			List<SettlementOperation> prepaidSettlementOperations) {
		this.prepaidSettlementOperations = prepaidSettlementOperations;
	}

	/**
	 * Gets the settlement account operation.
	 *
	 * @return the settlement account operation
	 */
	public SettlementAccountOperation getSettlementAccountOperation() {
		return settlementAccountOperation;
	}

	/**
	 * Sets the settlement account operation.
	 *
	 * @param settlementAccountOperation the new settlement account operation
	 */
	public void setSettlementAccountOperation(
			SettlementAccountOperation settlementAccountOperation) {
		this.settlementAccountOperation = settlementAccountOperation;
	}

	/**
	 * Gets the settlement account operations.
	 *
	 * @return the settlement account operations
	 */
	public List<SettlementAccountOperation> getSettlementAccountOperations() {
		return settlementAccountOperations;
	}

	/**
	 * Sets the settlement account operations.
	 *
	 * @param settlementAccountOperations the new settlement account operations
	 */
	public void setSettlementAccountOperations(
			List<SettlementAccountOperation> settlementAccountOperations) {
		this.settlementAccountOperations = settlementAccountOperations;
	}

	/**
	 * Gets the selected prepaid operation.
	 *
	 * @return the selected prepaid operation
	 */
	public SettlementOperation getSelectedPrepaidOperation() {
		return selectedPrepaidOperation;
	}

	/**
	 * Sets the selected prepaid operation.
	 *
	 * @param selectedPrepaidOperation the new selected prepaid operation
	 */
	public void setSelectedPrepaidOperation(
			SettlementOperation selectedPrepaidOperation) {
		this.selectedPrepaidOperation = selectedPrepaidOperation;
	}

	/**
	 * @return the negotiationMechanismList
	 */
	public List<NegotiationMechanism> getNegotiationMechanismList() {
		return negotiationMechanismList;
	}

	/**
	 * @param negotiationMechanismList the negotiationMechanismList to set
	 */
	public void setNegotiationMechanismList(
			List<NegotiationMechanism> negotiationMechanismList) {
		this.negotiationMechanismList = negotiationMechanismList;
	}

}