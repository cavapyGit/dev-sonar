package com.pradera.negotiations.operations.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class NegotiationOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation request pk. */
	private Long idMechanismOperationRequestPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The id negotiation mechanism pk. */
	private Long idNegotiationMechanismPk;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id part seller. */
	private Long idPartSeller;
	
	/** The id part buyer. */
	private Long idPartBuyer;
	
	/** The id part in charge. */
	private Long idPartInCharge;
	
	/** The mnemo part in charge. */
	private String mnemoPartInCharge;
	
	/** The isin state. */
	private Long isinState;
	
	/** The id participant placement. */
	private Long idParticipantPlacement;
	
	/** The id placement segment state. */
	private Integer idPlacementSegmentState;
	
	/** The id placement segment detail state. */
	private Integer idPlacementSegmentDetailState;
	
	/** The id issuance code. */
	private String idIssuanceCode;
	
	/** The get all placement object. */
	private Boolean needAllPlacementObject;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/**
	 * Gets the id mechanism operation request pk.
	 *
	 * @return the id mechanism operation request pk
	 */
	public Long getIdMechanismOperationRequestPk() {
		return idMechanismOperationRequestPk;
	}

	/**
	 * Sets the id mechanism operation request pk.
	 *
	 * @param idMechanismOperationRequestPk the new id mechanism operation request pk
	 */
	public void setIdMechanismOperationRequestPk(Long idMechanismOperationRequestPk) {
		this.idMechanismOperationRequestPk = idMechanismOperationRequestPk;
	}

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the id part seller.
	 *
	 * @return the id part seller
	 */
	public Long getIdPartSeller() {
		return idPartSeller;
	}

	/**
	 * Sets the id part seller.
	 *
	 * @param idPartSeller the new id part seller
	 */
	public void setIdPartSeller(Long idPartSeller) {
		this.idPartSeller = idPartSeller;
	}

	/**
	 * Gets the id part buyer.
	 *
	 * @return the id part buyer
	 */
	public Long getIdPartBuyer() {
		return idPartBuyer;
	}

	/**
	 * Sets the id part buyer.
	 *
	 * @param idPartBuyer the new id part buyer
	 */
	public void setIdPartBuyer(Long idPartBuyer) {
		this.idPartBuyer = idPartBuyer;
	}

	/**
	 * Gets the id part in charge.
	 *
	 * @return the id part in charge
	 */
	public Long getIdPartInCharge() {
		return idPartInCharge;
	}

	/**
	 * Sets the id part in charge.
	 *
	 * @param idPartInCharge the new id part in charge
	 */
	public void setIdPartInCharge(Long idPartInCharge) {
		this.idPartInCharge = idPartInCharge;
	}

	/**
	 * Gets the mnemo part in charge.
	 *
	 * @return the mnemo part in charge
	 */
	public String getMnemoPartInCharge() {
		return mnemoPartInCharge;
	}

	/**
	 * Sets the mnemo part in charge.
	 *
	 * @param mnemoPartInCharge the new mnemo part in charge
	 */
	public void setMnemoPartInCharge(String mnemoPartInCharge) {
		this.mnemoPartInCharge = mnemoPartInCharge;
	}

	/**
	 * Gets the isin state.
	 *
	 * @return the isin state
	 */
	public Long getIsinState() {
		return isinState;
	}
	/**
	 * Sets the isin state.
	 *
	 * @param isinState the new isin state
	 */
	public void setIsinState(Long isinState) {
		this.isinState = isinState;
	}
	/**
	 * Instantiates a new negotiation operation to.
	 */
	public NegotiationOperationTO() {
		super();
	}
	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	/**
	 * Gets the id participant placement.
	 *
	 * @return the id participant placement
	 */
	public Long getIdParticipantPlacement() {
		return idParticipantPlacement;
	}
	/**
	 * Sets the id participant placement.
	 *
	 * @param idParticipantPlacement the new id participant placement
	 */
	public void setIdParticipantPlacement(Long idParticipantPlacement) {
		this.idParticipantPlacement = idParticipantPlacement;
	}
	/**
	 * Gets the id placement segment state.
	 *
	 * @return the id placement segment state
	 */
	public Integer getIdPlacementSegmentState() {
		return idPlacementSegmentState;
	}
	/**
	 * Sets the id placement segment state.
	 *
	 * @param idPlacementSegmentState the new id placement segment state
	 */
	public void setIdPlacementSegmentState(Integer idPlacementSegmentState) {
		this.idPlacementSegmentState = idPlacementSegmentState;
	}
	/**
	 * Gets the id issuance code.
	 *
	 * @return the id issuance code
	 */
	public String getIdIssuanceCode() {
		return idIssuanceCode;
	}
	/**
	 * Sets the id issuance code.
	 *
	 * @param idIssuanceCode the new id issuance code
	 */
	public void setIdIssuanceCode(String idIssuanceCode) {
		this.idIssuanceCode = idIssuanceCode;
	}

	/**
	 * Gets the need all placement object.
	 *
	 * @return the need all placement object
	 */
	public Boolean getNeedAllPlacementObject() {
		return needAllPlacementObject;
	}

	/**
	 * Sets the need all placement object.
	 *
	 * @param needAllPlacementObject the new need all placement object
	 */
	public void setNeedAllPlacementObject(Boolean needAllPlacementObject) {
		this.needAllPlacementObject = needAllPlacementObject;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the id placement segment detail state.
	 *
	 * @return the idPlacementSegmentDetailState
	 */
	public Integer getIdPlacementSegmentDetailState() {
		return idPlacementSegmentDetailState;
	}

	/**
	 * Sets the id placement segment detail state.
	 *
	 * @param idPlacementSegmentDetailState the idPlacementSegmentDetailState to set
	 */
	public void setIdPlacementSegmentDetailState(Integer idPlacementSegmentDetailState) {
		this.idPlacementSegmentDetailState = idPlacementSegmentDetailState;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the idIssuerPk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the idIssuerPk to set
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
}