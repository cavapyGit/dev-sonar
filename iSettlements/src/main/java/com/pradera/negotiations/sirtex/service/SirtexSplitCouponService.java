package com.pradera.negotiations.sirtex.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SalePurchaseUpdateComponentService;
import com.pradera.integration.component.business.service.SplitCouponComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SplitCouponOperationTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.splitcoupons.SplitCouponMarketFact;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.custody.splitcouponstype.SplitCouponStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.CustodyOperationStateType;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.negotiation.AccountTradeMarketFact;
import com.pradera.model.negotiation.AccountTradeRequest;
import com.pradera.model.negotiation.CouponTradeRequest;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.settlements.core.facade.SettlementProcessFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SirtexSplitCouponService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SirtexSplitCouponService extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/** The sale purchase update component service. */
	@Inject Instance<SalePurchaseUpdateComponentService> salePurchaseUpdateComponentService;
	
	/** The accounts component service. */
	@Inject Instance<AccountsComponentService> accountsComponentService;
	
	/** The split coupon component service. */
	@Inject Instance<SplitCouponComponentService> splitCouponComponentService;
	
	/** The settlement process facade. */
	@EJB private SettlementProcessFacade settlementProcessFacade;
	
	/** The sirtex operation service. */
	@EJB private SirtexOperationService sirtexOperationService;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/**
	 * Creates the split request from trade raquest.
	 *
	 * @param tradeRequest the trade request
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SplitCouponRequest> createSplitRequestFromTradeRaquest(TradeRequest tradeRequest) throws ServiceException{
		String userName = userInfo.getUserAccountSession().getUserName();
		Date registryDate = CommonsUtilities.currentDateTime();
		
		Integer role = ParticipantRoleType.SELL.getCode();
		Integer operationPart = OperationPartType.CASH_PART.getCode();
		List<AccountTradeRequest> accountTradeList = tradeRequest.getAccountSirtexOperations();
		
		/**OBTENEMOS INVERSIONISTAS VENDEDORES EN LA SOLICITUD DE OPERACION, POR DEFAULT RETORNARA 1*/
		List<AccountTradeRequest> accountTradeSellers = findHolderAccountByRole(accountTradeList, role, operationPart);
		
		List<SplitCouponRequest> splitRequestList = new ArrayList<SplitCouponRequest>();
		
		for(AccountTradeRequest accountTrade: accountTradeSellers){
			
			SplitCouponRequest splitRequest = new SplitCouponRequest();
			splitRequest.setSplitCouponOperations(new ArrayList<SplitCouponOperation>());
			
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setRegistryDate(registryDate);
			custodyOperation.setRegistryUser(userName);
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_DETACHMENT.getCode());
			custodyOperation.setState(CustodyOperationStateType.REGISTERED.getCode());
			custodyOperation.setOperationNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_COUPON_GRANT_OPERATION));
			custodyOperation.setOperationDate(CommonsUtilities.currentDate());
			
			splitRequest.setCustodyOperation(custodyOperation);
			splitRequest.setRegistryDate(registryDate);
			splitRequest.setRegistryUser(userName);
			splitRequest.setRequestState(SplitCouponStateType.APPROVED.getCode());
			splitRequest.setSecurity(tradeRequest.getSecurities());
			splitRequest.setParticipant(tradeRequest.getSellerParticipant());
			splitRequest.setHolderAccount(accountTrade.getHolderAccount());
			
			/**BUSCAMOS EL TITULAR PRINCIPAL DE LA CUENTA*/
			for(HolderAccountDetail holderAccDetail: accountTrade.getHolderAccount().getHolderAccountDetails()){
				if(holderAccDetail.getIndRepresentative().equals(BooleanType.YES.getCode())){
					splitRequest.setHolder(holderAccDetail.getHolder());
				}
			}
			
			/**PERSISTIMOS OBJETO EN BD*/
			create(splitRequest);

			/**VERIFICAMOS SI HAY DESPRENDIMIENTO DE CAPITAL*/
			Boolean capitalIsDetach = capitalIsDetach(tradeRequest);
			/**CANTIDAD MAYOR EN CUPON A DESPRENDER*/
			BigDecimal greaterBal   = tradeRequest.getCouponsTradeRequestList().get(0).getAcceptedQuantity();
			/**LISTA PARA MANEJAR LA CUPONERA A DESPRENDER, EN EL CASO EXISTA CAPITAL ES NECESARIO DESPRENDER TODA LA CUPONERA VIGENTE*/
			List<CouponTradeRequest> couponTradeList = tradeRequest.getCouponsTradeRequestList();
			/**SI ES DESPRENDIMIENTO DE CAPITAL, GENERAMOS CUPONERA PARA DESPRENDER TODOS LOS CUPONES*/
			if(capitalIsDetach){
				couponTradeList = getAllCouponsFromSecurity(tradeRequest.getSecurities().getIdSecurityCodePk(), greaterBal);
			}
			
			/**ITERAMOS LOS CUPONES A DESPRENDER*/
			for(CouponTradeRequest sirtexCoupon: couponTradeList){
				if(!sirtexCoupon.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && sirtexCoupon.getAcceptedQuantity()!=null){
					SplitCouponOperation splitDetail = new SplitCouponOperation();
					
					splitDetail.setSplitCouponRequest(splitRequest);
					splitDetail.setProgramInterestCoupon(sirtexCoupon.getProgramInterestCoupon());
					
					/**SI EXISTE DESPRENDIMIENTO DE CAPITAL ES NECESARIO DESPRENDER TODOS LOS CUPONES*/
					if(capitalIsDetach){
						splitDetail.setOperationQuantity(greaterBal);
					}else{
						splitDetail.setOperationQuantity(sirtexCoupon.getAcceptedQuantity());
					}
					
					/**AGREGAMOS CUPON A SOLICITUD DE DESPRENDIMIENTO*/
					splitRequest.getSplitCouponOperations().add(splitDetail);
					
					/**PERSISTIMOS OBJETO EN BD*/
					create(splitDetail);
				}
			}
			
			/**VERIFICAMOS LOS HECHOS DE MERCADO*/
			List<MarketFactAccountTO> accountMarketFactList = new ArrayList<MarketFactAccountTO>();
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				for(AccountTradeMarketFact accountMarkFact: accountTrade.getAccountSirtexMarkectfacts()){
					SplitCouponMarketFact splitMarketFact = new SplitCouponMarketFact();
					splitMarketFact.setMarketDate(accountMarkFact.getMarketDate());
					splitMarketFact.setMarketRate(accountMarkFact.getMarketRate());
					splitMarketFact.setMarketPrice(accountMarkFact.getMarketPrice());
					splitMarketFact.setOperationQuantity(accountMarkFact.getMarketQuantity());
					splitMarketFact.setSplitCouponRequest(splitRequest);
					
					MarketFactAccountTO accountMarketFact = new MarketFactAccountTO();
					accountMarketFact.setMarketDate(accountMarkFact.getMarketDate());
					accountMarketFact.setMarketRate(accountMarkFact.getMarketRate());
					accountMarketFact.setMarketPrice(accountMarkFact.getMarketPrice()); 
					accountMarketFact.setQuantity(accountMarkFact.getMarketQuantity());
					accountMarketFactList.add(accountMarketFact);
				
					/**PERSISTIMOS OBJETO EN BD*/
					create(splitMarketFact);
				}
			}
			splitRequestList.add(splitRequest);
			
			/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
			List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(splitRequest, greaterBal, accountMarketFactList);
			accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(splitRequest.getIdSplitOperationPk(),
																	BusinessProcessType.COUPON_DETACHMENT_OPERATION_REGISTER.getCode(),
																	accountBalanceTOs, null, custodyOperation.getOperationType()));			
		}
		
		return splitRequestList;
	}
	
	/**
	 * *
	 * Method to generate all coupons from securityCode.
	 *
	 * @param securityCode the security code
	 * @param greatherQuantity the greather quantity
	 * @return the all coupons from security
	 * @throws ServiceException the service exception
	 */
	public List<CouponTradeRequest> getAllCouponsFromSecurity(String securityCode, BigDecimal greatherQuantity) throws ServiceException{
		List<CouponTradeRequest> allCoupons = new ArrayList<CouponTradeRequest>();

		/**GENERAMOS LA CUPONERA EN BASE A LOS CUPONES CON CANTIDAD MAYOR EN LA OPERACION*/
		Map<Integer,ProgramInterestCoupon> securityCoupons = sirtexOperationService.findInterestCoupons(securityCode);
		for(Map.Entry<Integer,ProgramInterestCoupon> entry : securityCoupons.entrySet()){
			ProgramInterestCoupon interestCoupon = entry.getValue();
			CouponTradeRequest couponTradeReq = new CouponTradeRequest();
			couponTradeReq.setProgramInterestCoupon(interestCoupon);
			couponTradeReq.setAcceptedQuantity(greatherQuantity);
			couponTradeReq.setCouponNumber(interestCoupon.getCouponNumber());
			allCoupons.add(couponTradeReq);
		}
		
		return allCoupons;
	}
	
	/**
	 * *
	 * Method to know if detachment is with capital or not.
	 *
	 * @param tradeRequest the trade request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean capitalIsDetach(TradeRequest tradeRequest) throws ServiceException{
		for(CouponTradeRequest couponTrade: tradeRequest.getCouponsTradeRequestList()){
			if(couponTrade.getIndIsCapital().equals(BooleanType.YES.getCode())){
				BigDecimal capitalAccepted = couponTrade.getAcceptedQuantity();
				if(capitalAccepted!=null && !capitalAccepted.equals(BigDecimal.ZERO)){
					return true;
				}
			}
		}
		return false; 
	}
	
	/**
	 * Find holder account by role.
	 *
	 * @param accountOperationList the account operation list
	 * @param role the role
	 * @param operationPart the operation part
	 * @return the list
	 */
	public List<AccountTradeRequest> findHolderAccountByRole(List<AccountTradeRequest> accountOperationList, Integer role, Integer operationPart){
		List<AccountTradeRequest> holderAccountOperationList = new ArrayList<AccountTradeRequest>();
		for(AccountTradeRequest _accountOperation: accountOperationList){
			if(_accountOperation.getRole().equals(role) && _accountOperation.getOperationPart().equals(operationPart)){
				holderAccountOperationList.add(_accountOperation);
			}
		}
		return holderAccountOperationList;
	}
	
	/**
	 * METHOD PARA CONFIRMAR SOLICITUDES DE DESPRENDIMIENTOS.
	 *
	 * @param splitRequests the split requests
	 * @throws ServiceException the service exception
	 */
	public void confirmSplitRequestList(List<SplitCouponRequest> splitRequests) throws ServiceException{
		for(SplitCouponRequest splitRequest: splitRequests){
			SplitCouponOperationTO splitCouponOperationTO = new SplitCouponOperationTO();
			splitCouponOperationTO.setIdSplitCouponOperationPk(splitRequest.getIdSplitOperationPk());
			splitCouponOperationTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
			splitCouponComponentService.get().confirmSplitCouponProcess(splitCouponOperationTO);
		}
	}
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param splitCouponRequest the split coupon request
	 * @param quantity the quantity
	 * @param lstMarketFactTO the lst market fact to
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(SplitCouponRequest splitCouponRequest,
										BigDecimal quantity, List<MarketFactAccountTO> lstMarketFactTO) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(splitCouponRequest.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(splitCouponRequest.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(splitCouponRequest.getSecurity().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(quantity);
		if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFactTO))
		holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactTO);
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param targetAccountBalanceTOs the target account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, 
			List<HolderAccountBalanceTO> accountBalanceTOs, List<HolderAccountBalanceTO> targetAccountBalanceTOs, Long operationType){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		if(Validations.validateListIsNotNullAndNotEmpty(targetAccountBalanceTOs))
		objAccountComponent.setLstTargetAccounts(targetAccountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		return objAccountComponent;
	}
	
	/**
	 * *
	 * Method to get SplitCouponOperation from couponTradeRequest.
	 *
	 * @param tradeRequest the trade request
	 * @param splitList the split list
	 * @return the split operation from trade
	 * @throws ServiceException the service exception
	 */
	public SplitCouponOperation getSplitOperationFromTrade(CouponTradeRequest tradeRequest, List<SplitCouponOperation> splitList) throws ServiceException{
		Integer couponNumber = tradeRequest.getCouponNumber();
		for(SplitCouponOperation splitCouponOperation: splitList){
			ProgramInterestCoupon interesCoupon = splitCouponOperation.getProgramInterestCoupon();
			if(couponNumber.equals(interesCoupon.getCouponNumber())){
				return splitCouponOperation;
			}
		}
		return null;
	}
}