
package com.pradera.negotiations.sirtex.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.HolderAccountSearchTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.operation.to.MechanismModalityTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.AccountTradeMarketFact;
import com.pradera.model.negotiation.AccountTradeRequest;
import com.pradera.model.negotiation.CouponTradeRequest;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.TradeMarketFact;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationDateSearchType;
import com.pradera.model.negotiation.type.OtcOperationOriginRequestType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.negotiations.otcoperations.facade.OtcOperationServiceFacade;
import com.pradera.negotiations.sirtex.facade.SirtexOperationFacade;
import com.pradera.negotiations.sirtex.service.SirtexOperationService;
import com.pradera.negotiations.sirtex.to.SirtexManagementTO;
import com.pradera.negotiations.sirtex.to.SirtexOperationResultTO;
import com.pradera.settlements.utils.PropertiesConstants;

import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SirtexOperationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
public class SirtexOperationBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant SIRTEX_RULE. */
	private static final String SIRTEX_RULE = "sirtexOperationRule";
	
	/** The Constant SIRTEX_TERM_RULE. */
	private static final String SIRTEX_TERM_RULE = "sirtexTermSettlementRule";
	
	/** The Constant marketFactUI. */
	private static final String marketFactUI = "marketFactUI";
	
	/** The Constant POSITION_SELLER. */
	private static final Integer POSITION_SELLER = 0;
	
	/** The Constant POSITION_BUYER. */
	private static final Integer POSITION_BUYER = 1;
	
	/** The Constant COUPON_ZERO. */
	private static final Integer COUPON_ZERO = 0;
	
	/** The Constant COUPON_FIVE_YEARS. */
	private static final Integer daysOfLifeCoupon = GeneralConstants.DAYS_OF_LIFE_COUPON_SIRTEX;
	
	/** The sirtex operation facade. */
	@EJB private SirtexOperationFacade sirtexOperationFacade;
	
	/** The sirtex operation facade. */
	@EJB private SirtexOperationService sirtexOperationService;
	
	/** The otc operation facade. */
	@EJB private OtcOperationServiceFacade otcOperationFacade;
	
	/** The general parameter facade. */
	@EJB private GeneralParametersFacade generalParameterFacade;
	
	/** The accounts facade. */
	@EJB private AccountsFacade accountsFacade;
	
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The sirtex register to. */
	private SirtexManagementTO sirtexRegisterTO;
	
	/** The sirtex consult to. */
	private SirtexManagementTO sirtexConsultTO;
	
	/** The participant logout. */
	private Long participantLogout;
//	private boolean blUserPartInv;
	
	/** The user info. */
@Inject private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
	
	/** The parameter description. */
	Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	
/** The id participant bc. */
//	@Inject @Configurable String idIssuerBC;
	@Inject @Configurable String idParticipantBC;
//	@Inject @Configurable String idHolderAccountBC;
	
	Integer personType;
	
	// objeto para el helper de Cuenta Titular
	private List<HolderAccountHelperResultTO> lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	private HolderAccountHelperResultTO sourceAccountTo = new HolderAccountHelperResultTO();
	
	private Long idHolderEnteredOld;
		
	
	private Long idNegotiationModalityPk;
	
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
 * Inits the.
 */
@PostConstruct
	public void init(){
		personType = PersonType.JURIDIC.getCode();
		sirtexConsultTO = new SirtexManagementTO();
		idHolderEnteredOld=null;
		try{
			loadCboParticipants();
			loadSirtexConsult();
			loadCboDateSearchType();
			loadSirtexOperationStateType();
			loadCboModalitySearch();
			loadPriviligies();
			loadParticipantPrivileges();
			loadSecurityClassBCB();
			loadMaxTermDays();
		}catch(ServiceException ex){
		}
	}
	
	/**
	 * Load max term days.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadMaxTermDays() throws ServiceException {
	// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.TRADE_REQUEST_SIRTEX_TERM.getCode());
		for(ParameterTable parameter: generalParameterFacade.getListParameterTableServiceBean(parameterTableTO))
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getShortInteger().toString());
	}

	/**
	 * Load security class bcb.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityClassBCB() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		sirtexConsultTO.setLstSecurityClassBcb(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable parameter: sirtexConsultTO.getLstSecurityClassBcb())
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getText3());
	}

	/**
	 * Load participant privileges.
	 */
	private void loadParticipantPrivileges() {
		if(userInfo.getUserAccountSession().isParticipantInstitucion()|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			participantLogout = userInfo.getUserAccountSession().getParticipantCode();
			sirtexConsultTO.getSirtexFilters().setIdParticipantBuyer(participantLogout);
		}
	}

	/**
	 * Load priviligies.
	 */
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnReview(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		if(userPrivilege.getUserAcctions().isSettlement()){
			/**WHEN THE USER HAS CONFIRM PRIVILEGE, THEN IT'S COULD SETTLEMENT OPERATIONS*/
			privilegeComponent.setBtnConfirmView(Boolean.TRUE);
			privilegeComponent.setBtnSettlement(Boolean.TRUE);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Load sirtex consult.
	 */
	private void loadSirtexConsult() {
		// TODO Auto-generated method stub
		SirtexOperationTO sirtexOperation = new SirtexOperationTO();
		sirtexOperation.setInitialDate(new Date());
		sirtexOperation.setFinalDate(new Date());
		sirtexOperation.setSecuritie(new Security());
		sirtexConsultTO.setSirtexFilters(sirtexOperation);
		sirtexConsultTO.setSirtexDataModel(null);
		sirtexConsultTO.setSirtexOperationSelected(new ArrayList<SirtexOperationResultTO>());
	}
	
	/**
	 * Show security financial data.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityFinancialData(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * Liquidar.
	 */
	@LoggerAuditWeb
	public void liquidar(){
		try {
//			sirtexOperationFacade.liquidarInconsistencia(3801L);
			sirtexOperationFacade.liquidarInconsistencia(3571L);
			sirtexOperationFacade.liquidarInconsistencia(3572L);
			sirtexOperationFacade.liquidarInconsistencia(3573L);
			sirtexOperationFacade.liquidarInconsistencia(3574L);
			sirtexOperationFacade.liquidarInconsistencia(3575L);
			sirtexOperationFacade.liquidarInconsistencia(3576L);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Change participant seller.
	 */
	public void changeParticipantSeller(){
		
		Participant participantBC = getParticipantBC();
		TradeRequest tradeRequest = sirtexRegisterTO.getSirtexOperation();
		Participant participantSell = tradeRequest.getSellerParticipant();
		//Se pueden elegir dentro de las cuentass del participante Issue 814, ya no solo de la cuenta matriz
		//sirtexRegisterTO.setAccountSeller(sirtexOperationFacade.findHolderAccountsFromParticipant(participantSell));
		sirtexRegisterTO.setAccountSeller(new HolderAccount());
		sirtexRegisterTO.setHolderSeller(new Holder());
		sirtexRegisterTO.getSirtexOperation().setSellerAccount(new HolderAccount());
		lstSourceAccountTo=new ArrayList<>();
		sourceAccountTo=new HolderAccountHelperResultTO();
		idHolderEnteredOld = null;
		//se debe setear nulo al titular
		//participantSell.setHolder(null);
		if(!participantSell.getIdParticipantPk().equals(participantBC.getIdParticipantPk())){
			sirtexRegisterTO.getSirtexOperation().setBuyerParticipant(participantBC);
		}
		
		sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
		sirtexRegisterTO.getSirtexOperation().setSettlementCurrency(null);
		sirtexRegisterTO.getSirtexOperation().setNominalValue(null);
		sirtexRegisterTO.setHolderAccountSeller(new ArrayList<HolderAccount>());			
		sirtexRegisterTO.setHolderAccountBuyers(new ArrayList<HolderAccount>());
		sirtexRegisterTO.getSirtexOperation().setCouponsTradeRequestList(new ArrayList<CouponTradeRequest>());
		sirtexRegisterTO.setBlIsSplit(false);
		
		JSFUtilities.updateComponent("frmSirtexOperation");
	}
	
	public void validHolderAccountSelected(){
		sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
		sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
		if(Validations.validateIsNotNull(sourceAccountTo) && Validations.validateIsNotNullAndPositive(sourceAccountTo.getAccountNumber()) ){
			HolderAccount holderAccountFind=sirtexOperationService.find(HolderAccount.class, sourceAccountTo.getAccountPk());
			if(holderAccountFind==null){
				sirtexRegisterTO.setAccountSeller(new HolderAccount());	
				sirtexRegisterTO.getSirtexOperation().setSellerAccount(new HolderAccount());
			}else{
				sirtexRegisterTO.setAccountSeller(holderAccountFind);
				sirtexRegisterTO.getSirtexOperation().setSellerAccount(holderAccountFind);
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("msg.alert.validation.sirtex.secutities.require.account"));
			JSFUtilities.updateComponent("frmSirtexOperation");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	
	/**
	 * metodo para validar el valor ingresado
	 */
	public void validSecurityEntered(){
		//sirtexOperationBean.sirtexConsultTO.sirtexFilters.securitie.idSecurityCodePk
		//sirtexConsultTO.getSirtexFilters().getSecuritie()
		if(Validations.validateIsNotNull(sirtexConsultTO.getSirtexFilters().getSecuritie())&&Validations.validateIsNotNull(sirtexConsultTO.getSirtexFilters().getSecuritie().getIdSecurityCodePk())){
			Security securityFind=sirtexOperationService.find(Security.class, sirtexConsultTO.getSirtexFilters().getSecuritie().getIdSecurityCodePk());
			if(securityFind==null){
				//mensaje de validacion
				sirtexConsultTO.getSirtexFilters().setSecuritie(new Security());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("msg.alert.validation.sirtex.search.securities.no.exist"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}else{
				sirtexConsultTO.getSirtexFilters().setSecuritie(securityFind);
			}
		}
	}
	
	public void changeHolderSearchV2() throws ServiceException{
		
		//sirtexRegisterTO.setAccountSeller(new HolderAccount());
		
		//holder
		if(sirtexRegisterTO.getHolderSeller().getIdHolderPk() == null){
			lstSourceAccountTo=new ArrayList<>();
			sourceAccountTo=new HolderAccountHelperResultTO();
			sirtexRegisterTO.setAccountSeller(new HolderAccount());
			return;
		}else{
			
			Holder sellerHolderSelected = sirtexRegisterTO.getHolderSeller();
			if(sellerHolderSelected.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
				sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER));
				JSFUtilities.updateComponent("frmSirtexOperation");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			//Validando que el CUI ingresado sea de tipo juridico
			// Validate if holder's type from database need to be equals than the
			// HolderAccount's type from SelectOneMenu . |
			if (sellerHolderSelected.getHolderType().equals(PersonType.NATURAL.getCode()) && personType == PersonType.JURIDIC.getCode()) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.SIRTEX_SETTLEMENT_CUI_PERSON_TYPE,
						new Object[] { String.valueOf(sellerHolderSelected.getIdHolderPk()), sellerHolderSelected.getFullName(), sellerHolderSelected.getHolderTypeDescription(), PersonType.JURIDIC.getValue() });
				//formateando el CUI y la  cuenta
				sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
				sirtexRegisterTO.setAccountSeller(new HolderAccount());
				sirtexRegisterTO.setHolderSeller(new Holder());
				lstSourceAccountTo=new ArrayList<>();
				sourceAccountTo=new HolderAccountHelperResultTO();
				JSFUtilities.updateComponent("frmSirtexOperation");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
//			issue 1016 si CUI esta habilitado para negociar sirtex
			if (sirtexOperationFacade.findIndSirtexNeg(sellerHolderSelected.getIdHolderPk()).equals(GeneralConstants.ZERO_VALUE_INT)) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_CUI_IS_NOT_ENABLED_TO_NEGOTIATE_SIRTEX));
				sirtexRegisterTO.setHolderSeller(new Holder());
				JSFUtilities.updateComponent("frmSirtexOperation");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			// si el holder ingresado es diferente al anterior entonces limpiar el valor y el dias plazo
			if(idHolderEnteredOld==null){
				sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
				sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
				sirtexRegisterTO.setAccountSeller(new HolderAccount());
				idHolderEnteredOld=sirtexRegisterTO.getHolderSeller().getIdHolderPk();
				JSFUtilities.updateComponent("frmSirtexOperation");
			}else{
				if(!idHolderEnteredOld.equals(sirtexRegisterTO.getHolderSeller().getIdHolderPk())){
					sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
					sirtexRegisterTO.setAccountSeller(new HolderAccount());
					sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);	
					idHolderEnteredOld=sirtexRegisterTO.getHolderSeller().getIdHolderPk();
					JSFUtilities.updateComponent("frmSirtexOperation");
				}
			}
		}
		Holder sellerHolderSelected = sirtexRegisterTO.getHolderSeller();
		if(sellerHolderSelected.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
			sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER));
			JSFUtilities.updateComponent("frmSirtexOperation");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}	
		
		HolderTO holderTO = new HolderTO();
		holderTO.setHolderId(sellerHolderSelected.getIdHolderPk());
		holderTO.setFlagAllHolders(true);
		holderTO.setParticipantFk(sirtexRegisterTO.getSirtexOperation().getSellerParticipant().getIdParticipantPk());
		
		try {
			List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
			if(lstAccountTo.size()>0){
				lstSourceAccountTo = lstAccountTo;
				if(lstAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
					sourceAccountTo=lstAccountTo.get(0);
					HolderAccount holderAccountFind=sirtexOperationService.find(HolderAccount.class, sourceAccountTo.getAccountPk());
					if(holderAccountFind==null){
						sirtexRegisterTO.setAccountSeller(new HolderAccount());
						sirtexRegisterTO.getSirtexOperation().setSellerAccount(new HolderAccount());
					}else{
						sirtexRegisterTO.setAccountSeller(holderAccountFind);
						sirtexRegisterTO.getSirtexOperation().setSellerAccount(holderAccountFind);
					}
				}
			}else{
				sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
				lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				sourceAccountTo = new HolderAccountHelperResultTO();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * Change holder search.
	 * @throws ServiceException 
	 */
	public void changeHolderSearch() throws ServiceException{
		
		//reseteando la cuenta y valor
		sirtexRegisterTO.setAccountSeller(new HolderAccount());
		sirtexRegisterTO.getSirtexOperation().setSellerAccount(new HolderAccount());
		sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
		sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
		JSFUtilities.updateComponent("frmSirtexOperation:pnlHelpSecurity");
		
		//holder
		if(sirtexRegisterTO.getHolderSeller() == null){
			JSFUtilities.updateComponent("frmSirtexOperation");
			return;
		}
		
		if(sirtexRegisterTO.getHolderSeller().getIdHolderPk() == null){
			JSFUtilities.updateComponent("frmSirtexOperation");
			return;
		}
		
		Holder sellerHolderSelected = sirtexRegisterTO.getHolderSeller();
		
		if(sellerHolderSelected.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
			sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER));
			JSFUtilities.updateComponent("frmSirtexOperation");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		//Validando que el CUI ingresado sea de tipo juridico
		// Validate if holder's type from database need to be equals than the
		// HolderAccount's type from SelectOneMenu . |
		if (sellerHolderSelected.getHolderType().equals(PersonType.NATURAL.getCode()) && personType == PersonType.JURIDIC.getCode()) {
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.SIRTEX_SETTLEMENT_CUI_PERSON_TYPE,
					new Object[] { String.valueOf(sellerHolderSelected.getIdHolderPk()), sellerHolderSelected.getFullName(), sellerHolderSelected.getHolderTypeDescription(), PersonType.JURIDIC.getValue() });
			//formateando el CUI y la  cuenta
			sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
			sirtexRegisterTO.setAccountSeller(new HolderAccount());
			sirtexRegisterTO.setHolderSeller(new Holder());
			JSFUtilities.updateComponent("frmSirtexOperation");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		// verificando si solo tiene una cuenta
		HolderAccountSearchTO holderAccountSearchTO = new HolderAccountSearchTO();
		holderAccountSearchTO.setHolder(sirtexRegisterTO.getHolderSeller());
		holderAccountSearchTO.setParticipant(sirtexRegisterTO.getSirtexOperation().getSellerParticipant());
		holderAccountSearchTO.setLstHolderAccount(null);
		holderAccountSearchTO.setBlNoResult(true);
		
		List<HolderAccount> holderAccounts = sirtexOperationService.findHolderAccounts(holderAccountSearchTO);				
		
		if(holderAccounts != null && holderAccounts.size()==1){
			
			if (holderAccounts.get(0).getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				sirtexRegisterTO.setAccountSeller(holderAccounts.get(0));
				sirtexRegisterTO.getSirtexOperation().setSellerAccount(holderAccounts.get(0));
			}else{
				sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
				
				holderAccounts = new ArrayList<HolderAccount>();				
					showMessageOnDialog(null, null,PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_REGISTER, null);
				JSFUtilities.updateComponent("frmSirtexOperation:holderHelper");
				JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
				return;
			}
		}
	}
	
	/**
	 * Change participant buyer.
	 */
	public void changeParticipantBuyer(){
		Participant participantBC = getParticipantBC();
		TradeRequest tradeRequest = sirtexRegisterTO.getSirtexOperation();
		Participant participantBuy = tradeRequest.getBuyerParticipant();
		
		participantBC.setHolder(sirtexRegisterTO.getSirtexOperation().getSellerParticipant().getHolder());
		//validando si no se trata del BCB
		if(!participantBuy.getIdParticipantPk().equals(participantBC.getIdParticipantPk())){
			sirtexRegisterTO.getSirtexOperation().setSellerParticipant(participantBC);
		}
		changeSecurity();
	}
	
	/**
	 * View sirtex operation detail.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the string
	 */
	public String viewSirtexOperationDetail(SirtexOperationResultTO sirtexOperation){
		return validateSirtexOperation(ViewOperationsType.CONSULT.getCode(),sirtexOperation);
	}

	/**
	 * New sirtex operation action.
	 *
	 * @return the string
	 */
	public String newSirtexOperationAction(){
		try{
			idHolderEnteredOld = null;
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			sirtexRegisterTO = new SirtexManagementTO();
			/*SETEAMOS POR DETAULT, RENTA FIJA*/
			sirtexRegisterTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
			
			/*CARGAMOS PARAMETROS PARA EL REGISTRO DE SIRTEX*/
			loadSettlementType();
			loadSettlementSchemeType();
			loadCboOriginRequestType();
			loadCboModalityAndParticipants();
			
			/*VALIDAMOS TIPO DE CAMBIO Y VALIDACIONES GENERICAS*/
			DailyExchangeRates dailyExchangeRates = sirtexOperationFacade.findDayExchangesRate(CurrencyType.USD.getCode());
			sirtexOperationFacade.validationsBeforeNewOperation(sirtexRegisterTO);
		
			/*INICIALIZAMOS VARIABLES*/
//			SirtexOperation sirtexOperation = sirtexOperationFacade.configNewMechanismOperation();
			TradeRequest sirtexOperation = sirtexOperationFacade.configNewSirtexOperation();
		
			/*CARGAMOS TIPO DE CAMBIO A ENTIDAD QUE MANEJARA LA OPERACION*/
			sirtexOperation.setExchangeRate(dailyExchangeRates.getBuyPrice());
			sirtexOperation.setOriginRequest(OtcOperationOriginRequestType.SALE.getCode());

			/*SETEAMOS EL MECHANISM OPERATION DENTRO DEL TO*/
			sirtexRegisterTO.setSirtexOperation(sirtexOperation);
			sirtexRegisterTO.setHolderSeller(new Holder());
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			sourceAccountTo = new HolderAccountHelperResultTO();
			JSFUtilities.updateComponent("frmSirtexOperation");			
			
			
			return SIRTEX_RULE;
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			return "";
		}
	}
	
	/**
	 * Clean all form.
	 */
	public void cleanAllForm(){
//		try {
//			SirtexManagementTO sirtexManagement = sirtexRegisterTO;
//			
//			TradeRequest operation = sirtexManagement.getSirtexOperation();
//			Long idModalitySelected = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
//			
//			TradeRequest sirtexOperation = sirtexOperationFacade.configNewSirtexOperation();
			newSirtexOperationAction();
			
//			sirtexOperation.getMechanisnModality().getId().setIdNegotiationModalityPk(idModalitySelected);
			/*SETEAMOS EL MECHANISM OPERATION DENTRO DEL TO*/
//			sirtexRegisterTO.setSirtexOperation(sirtexOperation);
//			sirtexRegisterTO.setAccountSeller(new HolderAccount());
//		} catch (ServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	/**
	 * Clean search form.
	 * @throws ServiceException 
	 */
	public void cleanSearchForm() throws ServiceException{
		loadSirtexConsult();
		loadCboDateSearchType();
		loadParticipantPrivileges();
	}
	
	/**
	 * Change holder account seller.
	 */
	public void changeHolderAccountSeller(){
		SirtexManagementTO sirtexmanagement = sirtexRegisterTO;
		TradeRequest operation = sirtexmanagement.getSirtexOperation();
		try {
			CouponTradeRequest coupon = sirtexOperationFacade.putMechanismCoupon(sirtexRegisterTO);
			operation.setAccountSirtexOperations(sirtexOperationFacade.putParticipantInCharge(sirtexmanagement, ParticipantRoleType.SELL.getCode()));
			operation.getCouponsTradeRequestList().add(coupon);
			
			Long participantId = operation.getSellerParticipant().getIdParticipantPk();
			Long accountId     = operation.getAccountSirtexOperations().get(POSITION_SELLER).getHolderAccount().getIdHolderAccountPk();
			String securityCode = operation.getSecurities().getIdSecurityCodePk();
			Integer markFactNumber = sirtexOperationFacade.numberOfMarketFact(participantId, accountId, securityCode);
			sirtexmanagement.setMarketFactNumber(markFactNumber);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		sirtexRegisterTO = sirtexmanagement;
	}
	
	/**
	 * Change holder account buyer.
	 */
	public void changeHolderAccountBuyer(){
		SirtexManagementTO sirtexmanagement = sirtexRegisterTO;
		TradeRequest operation = sirtexmanagement.getSirtexOperation();
		try {
			operation.setAccountSirtexOperations(sirtexOperationFacade.putParticipantInCharge(sirtexmanagement, ParticipantRoleType.BUY.getCode()));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Change split coupons.
	 */
	public void changeSplitCoupons(){
	}
	
	/**
	 * Activate split coupon.
	 */
	public void activateSplitCoupon(){
		CouponTradeRequest couponTrade = sirtexRegisterTO.getSirtexOperation().getCouponsTradeRequestList().get(COUPON_ZERO);
		couponTrade.setSuppliedQuantity(null);
		if(!sirtexRegisterTO.getBlIsSplit()){
			couponTrade.setCouponDescription(null);
		}
	}
	
	/**
	 * Show coupons to splited.
	 */
	public void showCouponsToSplited(){
		TradeRequest tradeRequest = sirtexRegisterTO.getSirtexOperation();
		Boolean isSplit = sirtexRegisterTO.getBlIsSplit();
		List<CouponTradeRequest>  couponsTradeFromNumbers = null;
		SirtexManagementTO sirtexManagement = sirtexRegisterTO;
		TradeRequest operation = sirtexManagement.getSirtexOperation();
		Long idModalitySelected = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
		Security security = sirtexRegisterTO.getSirtexOperation().getSecurities();
		if (idModalitySelected != null && security.getIdSecurityCodePk() != null) {
			//validation desprendimiento de cupones para valores publicos reporto sirtex
			if (idModalitySelected == 3 || idModalitySelected == 22) { //reporto - reporto reverso
				if (!(security.getSecurityClass().equals(SecurityClassType.DPF.getCode()) 
						|| security.getSecurityClass().equals(SecurityClassType.DPA.getCode()))) { //DPF - DPA     
					sirtexRegisterTO.setBlIsSplit(Boolean.FALSE);
					JSFUtilities.updateComponent("frmSirtexOperation");
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.MSG_SIRTEX_SPLIT_REPORTED_VALIDATION));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		}
		//
		try {
			// validando que se haya seleccionado un valor
			if(Validations.validateIsNotNull(tradeRequest.getSecurities())&&Validations.validateIsNotNull(tradeRequest.getSecurities().getIdSecurityCodePk())){
			}else{
				//sirtexOperationBean.sirtexRegisterTO.sirtexOperation
				sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
				sirtexRegisterTO.setBlIsSplit(false);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("msg.validation.sirtex.term.settlement.day"));
				JSFUtilities.showSimpleValidationDialog();return;
			}
			
			couponsTradeFromNumbers = sirtexOperationFacade.generateCouponTradeCoupons(tradeRequest,isSplit,parameterDescription);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		tradeRequest.setCouponsTradeRequestList(couponsTradeFromNumbers);
	}
	
	/**
	 * Validate coupons entered.
	 *
	 * @param couponTrade the coupon trade
	 */
	public void validateCouponsEntered(CouponTradeRequest couponTrade){
		TradeRequest tradeRequest = sirtexRegisterTO.getSirtexOperation();
		CouponTradeRequest couponZero = tradeRequest.getCouponsTradeRequestList().get(COUPON_ZERO);
		if(couponZero.getCouponDescription()==null){
			return;
		}
		List<CouponTradeRequest>  couponsTradeFromNumbers = null;
		try {
			couponsTradeFromNumbers = sirtexOperationFacade.generateCouponTradeFromCoupons(tradeRequest,couponTrade,parameterDescription);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			couponTrade.setCouponDescription(null);
			tradeRequest.setCouponsTradeRequestList(new ArrayList<CouponTradeRequest>());
			tradeRequest.getCouponsTradeRequestList().add(couponTrade);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		tradeRequest.setCouponsTradeRequestList(couponsTradeFromNumbers);
	}
	
	/**
	 * Change suplied quantity.
	 *
	 * @param sirtexCoupon the sirtex coupon
	 */
	public void changeSupliedQuantity(CouponTradeRequest sirtexCoupon){
		
		TradeRequest operation = sirtexRegisterTO.getSirtexOperation();
		BigDecimal cashPrice = operation.getCashPrice();
		BigDecimal termPrice = operation.getTermPrice();
		
		CouponTradeRequest operationCoupon = sirtexCoupon;
		BigDecimal stockCouponQuantity = operationCoupon.getSuppliedQuantity();
		
		try {
			sirtexOperationFacade.validateSuppliedQuantity(operationCoupon,operation.getCouponsTradeRequestList());
			
			if(cashPrice!=null && !cashPrice.equals(BigDecimal.ZERO)){
				operationCoupon.setCashAmount(cashPrice.multiply(stockCouponQuantity));
			}
			if(termPrice!=null && !termPrice.equals(BigDecimal.ZERO)){
				operationCoupon.setTermAmount(termPrice.multiply(stockCouponQuantity));
			}
			
			if(!(sirtexCoupon.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && sirtexCoupon.getIndIsCapital().equals(BooleanType.NO.getCode()))){
				BigDecimal greateCouponBal = sirtexOperationFacade.getGreatherCouponQuantity(operation.getCouponsTradeRequestList());
				
				if(greateCouponBal.equals(BigDecimal.ZERO)){
					operation.getCouponsTradeRequestList().get(BigDecimal.ZERO.intValue()).setSuppliedQuantity(null);
				}else{
					operation.getCouponsTradeRequestList().get(BigDecimal.ZERO.intValue()).setSuppliedQuantity(greateCouponBal);
					AccountTradeRequest accountSeller = operation.getAccountSirtexOperations().get(POSITION_SELLER);
					if(accountSeller.getAccountSirtexMarkectfacts()!=null && !accountSeller.getAccountSirtexMarkectfacts().isEmpty()){
						if(accountSeller.getAccountSirtexMarkectfacts().size() == BigDecimal.ONE.intValue()){
							accountSeller.getAccountSirtexMarkectfacts().get(BigDecimal.ZERO.intValue()).setMarketQuantity(greateCouponBal);
						}else{
							BigDecimal quantity = BigDecimal.ZERO, quantityFirsMArkFact = BigDecimal.ZERO;
							for(AccountTradeMarketFact transferMarketFact: accountSeller.getAccountSirtexMarkectfacts()){
								if(accountSeller.getAccountSirtexMarkectfacts().indexOf(transferMarketFact) == BigDecimal.ZERO.intValue()){
									quantityFirsMArkFact = transferMarketFact.getMarketQuantity();
								}
								quantity = quantity.add(transferMarketFact.getMarketQuantity());
							}
							quantity = greateCouponBal.subtract(quantity).add(quantityFirsMArkFact);
							accountSeller.getAccountSirtexMarkectfacts().get(BigDecimal.ZERO.intValue()).setMarketQuantity(quantity);
						}
					}
				}
			}

			if(sirtexCoupon.getSuppliedQuantity()!=null && sirtexCoupon.getSuppliedQuantity().equals(BigDecimal.ZERO)){
				sirtexCoupon.setSuppliedQuantity(null);
			}
		} catch (ServiceException e) {
			sirtexCoupon.setSuppliedQuantity(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(e.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Change accept quantity.
	 *
	 * @param sirtexCup the sirtex cup
	 */
	public void changeAcceptQuantity(CouponTradeRequest sirtexCup){
		TradeRequest operation = sirtexRegisterTO.getSirtexOperation();
		BigDecimal cashPrice = operation.getCashPrice();
		BigDecimal termPrice = operation.getTermPrice();
		
		CouponTradeRequest tradeCoupon = sirtexCup;
		BigDecimal acceptedQuantity = tradeCoupon.getAcceptedQuantity();
		try {
			sirtexOperationFacade.validateAcceptedQuantity(tradeCoupon);
			
//			if(acceptedQuantity.equals(sirtexCup.getSuppliedQuantity())){
//				sirtexCup.setSuppliedOk(Boolean.TRUE);
//			}else{
//				sirtexCup.setSuppliedOk(Boolean.FALSE);
//			}
			
			//Validamos la cantidad ingresada para desbloquar el campo editable
			if(Validations.validateIsNotNullAndNotEmpty(acceptedQuantity)){
				if(acceptedQuantity.intValue() == GeneralConstants.ZERO_VALUE_INTEGER
						|| acceptedQuantity.intValue() > tradeCoupon.getSuppliedQuantity().intValue()){
					sirtexCup.setAcceptedQuantity(null);
					sirtexCup.setSuppliedOk(Boolean.FALSE);
				}
			}else{
				sirtexCup.setSuppliedOk(Boolean.FALSE);
			}
			
			
			if(cashPrice!=null && !cashPrice.equals(BigDecimal.ZERO)){
				tradeCoupon.setCashAmount(cashPrice.multiply(acceptedQuantity));
			}
			if(termPrice!=null && !termPrice.equals(BigDecimal.ZERO)){
				tradeCoupon.setTermAmount(termPrice.multiply(acceptedQuantity));
			}
			
			if(!(sirtexCup.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && sirtexCup.getIndIsCapital().equals(BooleanType.NO.getCode()))){
				BigDecimal greateAcceptedQuantity = sirtexOperationFacade.getGreatherAcceptCouponQuantity(operation.getCouponsTradeRequestList());
				if(!greateAcceptedQuantity.equals(BigDecimal.ZERO)){
					operation.getCouponsTradeRequestList().get(BigDecimal.ZERO.intValue()).setAcceptedQuantity(greateAcceptedQuantity);
					operation.getCouponsTradeRequestList().get(BigDecimal.ZERO.intValue()).setSuppliedOk(Boolean.FALSE);
				}
			}
		} catch (ServiceException e) {
			sirtexCup.setAcceptedQuantity(null);
			sirtexCup.setSuppliedOk(Boolean.FALSE);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(e.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Before save new mechanism operation.
	 */
	public void beforeSaveNewMechanismOperation(){
		TradeRequest operation = sirtexRegisterTO.getSirtexOperation();
		String messageProperties = "";
		Boolean haveValidation = Boolean.FALSE;
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER: 	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REGISTER_CONFIRM;
							haveValidation 	  = validateBeforeRegister();break;
			case APPROVE:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_APPROVE_CONFIRM;break;
			case REVIEW:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REVIEW_CONFIRM;
							haveValidation    = validateBeforeReview(operation);break;
			case CONFIRM:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_CONFIRM_CONFIRM;break;
			case ANULATE:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_ANNULATE_CONFIRM;break;
			case REJECT:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REJECT_CONFIRM;break;
			case SETTLEMENT:messageProperties = PropertiesConstants.SIRTEX_OPERATION_SETTLEMENT_CONFIRM;break;
			default:break;
		}
		
		if(!haveValidation){
			Long tradeRequestId = operation.getIdTradeRequestPk();
			String tradeRequest = null;
			if(tradeRequestId!=null){
				tradeRequest = tradeRequestId.toString();
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(messageProperties,tradeRequest));
			JSFUtilities.executeJavascriptFunction("PF('cnfwSirtexOperations').show()");
		}
	}
	
	/**
	 * Before settlement term.
	 *
	 * @param cupTradeRequest the cup trade request
	 */
	public void beforeSettlementTerm(CouponTradeRequest cupTradeRequest){
		for(CouponTradeRequest tradeRequest: sirtexRegisterTO.getSirtexOperation().getCouponsTradeRequestList()){
			tradeRequest.setIndSettlementOpe(BooleanType.NO.getCode());
		}
		cupTradeRequest.setIndSettlementOpe(BooleanType.YES.getCode());
		
		TradeRequest operation = sirtexRegisterTO.getSirtexOperation();
		String requestPk = operation.getIdTradeRequestPk().toString();
		String securitykey = cupTradeRequest.getMechanismOperation().getSecurities().getIdSecurityCodePk();
		Date termDate = cupTradeRequest.getMechanismOperation().getTermSettlementDate();
		String keyMessage = null;
		
		if(getCurrentSystemDate().before(termDate)){
			keyMessage = PropertiesConstants.SIRTEX_SETTLEMENT_ANTICIPATED_CONFIRM;
		}else{
			keyMessage = PropertiesConstants.SIRTEX_OPERATION_SETTLEMENT_CONFIRM_SECURITY;
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(keyMessage,new Object[]{securitykey,requestPk}));
		JSFUtilities.executeJavascriptFunction("PF('cnfwSirtexOperations').show()");
	}
	
	/**
	 * Validate before register.
	 *
	 * @return the boolean
	 */
	public Boolean validateBeforeRegister(){
		SirtexManagementTO sirtexTO = sirtexRegisterTO;
		TradeRequest sirtexReq = sirtexTO.getSirtexOperation();
		Integer markfactNumbers = sirtexTO.getMarketFactNumber();
		Boolean isSplit = sirtexTO.getBlIsSplit();
		List<CouponTradeRequest> coupons = sirtexReq.getCouponsTradeRequestList(); 
		
		String propertieKey = null;
		/**VALIDATE IF MARKET FACT ARE NOT ENTERED*/
		if(Validations.validateIsNull(markfactNumbers) || markfactNumbers.equals(BigDecimal.ZERO.intValue())){
			propertieKey = "operation.validation.markfact.empty";
		}
		
		/**VALIDATE IS SIRTEX REQUEST HAS SPLIT*/
		if(isSplit!=null && isSplit){
			CouponTradeRequest couponTrade = coupons.get(COUPON_ZERO);
			if(propertieKey==null && couponTrade.getSuppliedQuantity()==null){
				propertieKey = "operation.validation.split.empty";
			}
			if(propertieKey==null && coupons.size()==BigDecimal.ONE.intValue()){
				propertieKey = "operation.validation.split.notpermited";
			}
		}else{
			/**VALIDATE IF COUPONS HAS ALL SUPPLIED QUANTITY*/
			for(CouponTradeRequest couponTrade: coupons){
				if(propertieKey==null && couponTrade.getSuppliedQuantity()==null){
					propertieKey = "operation.validation.supplied.empty";break;
				}
			}
		}
		
		if(propertieKey!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),PropertiesUtilities.getMessage(propertieKey));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}
		return false;
	}
	
	/**
	 * Validate before review.
	 *
	 * @param tradeRequest the trade request
	 * @return the boolean
	 */
	public Boolean validateBeforeReview(TradeRequest tradeRequest){
		List<CouponTradeRequest> coupons = tradeRequest.getCouponsTradeRequestList();
		/**VALIDATE IF COUPONS HAS ALL ACCEPTED QUANTITY*/
		String propertieKey = null;
		
		Iterator<CouponTradeRequest> iter = coupons.iterator();
		Boolean haveNext = Boolean.FALSE;
		do{
			CouponTradeRequest element = iter.next();
			haveNext = iter.hasNext(); 
			if(element.getAcceptedQuantity()==null){
				if(!haveNext){
					propertieKey = "operation.validation.accepted.empty";
				}
			}else{
				break;
			}
		}while(haveNext);
		
		TradeRequest operation = tradeRequest;		
		/**VALIDATE IF REQUEST IT'S THE SAME THAN REGISTER*/
		AccountTradeRequest accountSeller = operation.getAccountSirtexOperations().get(POSITION_SELLER);
		for(CouponTradeRequest couponTrade: operation.getCouponsTradeRequestList()){
			if(propertieKey == null && couponTrade.getCouponNumber().equals(BigDecimal.ZERO.intValue())){
				if(!couponTrade.getSuppliedQuantity().equals(couponTrade.getAcceptedQuantity())){
					List<AccountTradeMarketFact> markFactList = accountSeller.getAccountSirtexMarkectfacts();
					if(!markFactList.isEmpty() && markFactList.size() != BigDecimal.ONE.intValue()){
						JSFUtilities.executeJavascriptFunction("PF('cnfMarkFactIncompleteW').show()");
						return true;
					}
				}
			}
		}
		
		if(propertieKey!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),PropertiesUtilities.getMessage(propertieKey));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Show review dialog.
	 */
	public void showReviewDialog(){
		TradeRequest operation = sirtexRegisterTO.getSirtexOperation();
		AccountTradeRequest accountSeller = operation.getAccountSirtexOperations().get(POSITION_SELLER);
		for(CouponTradeRequest couponTrade: operation.getCouponsTradeRequestList()){
			if(couponTrade.getCouponNumber().equals(BigDecimal.ZERO.intValue())){
				BigDecimal modifyQuantity = BigDecimal.ZERO;
				BigDecimal acceptQuantity = couponTrade.getAcceptedQuantity();
				for(AccountTradeMarketFact markFact: accountSeller.getAccountSirtexMarkectfacts()){
					modifyQuantity = modifyQuantity.add(markFact.getQuantityModify());
				}
				if(!acceptQuantity.equals(modifyQuantity)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
										PropertiesUtilities.getMessage("operation.validation.markfact.quantity.review",new Object[]{acceptQuantity}));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				break;
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_REVIEW_CONFIRM,operation.getIdTradeRequestPk()));
		JSFUtilities.executeJavascriptFunction("PF('cnfwSirtexOperations').show()");
	}
	
	/**
	 * ***************TRANSACTIONAL METHODS TO GO AT DATABASSE*****************.
	 */
	@LoggerAuditWeb
	public void saveNewMechanismOperation(){
		try{
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	registerSirtexOperation();break;
				case CONFIRM:  	confirmSirtexOperation();break;
				case APPROVE: 	approveSirtexOperation();break;
				case REVIEW: 	reviewSirtexOperation();break;
				case ANULATE: 	anulateSirtexOperation();break;
				case REJECT: 	rejectSirtexOperation();break;
				case SETTLEMENT:
								if(sirtexRegisterTO.getSirtexOperation().getTermSettlementDate().equals(getCurrentSystemDate())){
									settlTermSirtexOperation();
								}else{
									settlTermAnticipadedOperation();
								}break;
				default:		break;
			}
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
		}
		JSFUtilities.showSimpleEndTransactionDialog("searchSirtexOperation");
		
		if(sirtexConsultTO.getSirtexDataModel()!=null){
			searchSirtexOperations();
		}
	}
	
	/**
	 * Register sirtex operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void registerSirtexOperation() throws ServiceException{
		//validando que se haya seleccionado una cuenta titular
		if(Validations.validateIsNotNull(sirtexRegisterTO.getAccountSeller()) && Validations.validateIsNotNull(sirtexRegisterTO.getAccountSeller().getIdHolderAccountPk())){
		}else{
			//sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("msg.validation.sirtex.secutities.require.account"));
			JSFUtilities.showSimpleValidationDialog();return;
		}
		
		// validando que se haya ingresado un valor
		if(Validations.validateIsNotNull(sirtexRegisterTO.getSirtexOperation().getSecurities()) &&Validations.validateIsNotNull(sirtexRegisterTO.getSirtexOperation().getSecurities().getIdSecurityCodePk())){
		}else{
			//sirtexOperationBean.sirtexRegisterTO.sirtexOperation
			sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("msg.validation.sirtex.term.settlement.day"));
			JSFUtilities.showSimpleValidationDialog();return;
		}
		
		//Obtenemos la modalidad de la operacion a registrar
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		
		sirtexOperation = sirtexOperationFacade.registerSirtexOperation(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_REGISTER_CONFIRM_OK,
															new Object[]{sirtexOperation.getIdTradeRequestPk().toString()}));
	}
	
	/**
	 * Approve sirtex operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void approveSirtexOperation() throws ServiceException{
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		sirtexOperation = sirtexOperationFacade.approveOtcOpereationServiceFacade(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_APPROVE_CONFIRM_OK,
															new Object[]{sirtexOperation.getIdTradeRequestPk().toString()}));
	}
	
	/**
	 * Anulate sirtex operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void anulateSirtexOperation() throws ServiceException{
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		sirtexOperation = sirtexOperationFacade.anulateSirtexOperation(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_ANNULATE_CONFIRM_OK,
															new Object[]{sirtexOperation.getIdTradeRequestPk().toString()}));
	}
	
	/**
	 * Review sirtex operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void reviewSirtexOperation() throws ServiceException{
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		sirtexOperation = sirtexOperationFacade.reviewSirtexOperation(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_REVIEW_CONFIRM_OK,
															new Object[]{sirtexOperation.getIdTradeRequestPk().toString()}));
	}
	
	/**
	 * Reject sirtex operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void rejectSirtexOperation() throws ServiceException{
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		sirtexOperation = sirtexOperationFacade.rejectSirtexOperation(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_REJECT_CONFIRM_OK,
															new Object[]{sirtexOperation.getIdTradeRequestPk().toString()}));
	}
	
	/**
	 * Confirm sirtex operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void confirmSirtexOperation() throws ServiceException{
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		sirtexOperation = sirtexOperationFacade.confirmSirtexOperation(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_CONFIRM_CONFIRM_OK,
															new Object[]{sirtexOperation.getIdTradeRequestPk().toString(),
																		 sirtexOperation.getMechanisnModality().getNegotiationModality().getModalityName()}));
	}
	
	/**
	 * Settl term sirtex operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void settlTermSirtexOperation() throws ServiceException{
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		sirtexOperation = sirtexOperationFacade.settlementTermSirtexOperation(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_SETTLEMENT_CONFIRM_OK,
															new Object[]{sirtexOperation.getIdTradeRequestPk().toString(),
																		 sirtexOperation.getMechanisnModality().getNegotiationModality().getModalityName()}));
	}
	
	/**
	 * Settl term anticipaded operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void settlTermAnticipadedOperation() throws ServiceException{
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
		CouponTradeRequest cupTradeRequest = sirtexOperationFacade.settlementTermAnticipadedOperation(sirtexOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_SETTLEMENT_ANTICIPATED_CONFIRM_OK,
												new Object[]{cupTradeRequest.getMechanismOperation().getSecurities().getIdSecurityCodePk(),
															 sirtexOperation.getIdTradeRequestPk().toString()}));
	}
	
	/**
	 * ***************END - TRANSACTIONAL METHODS TO GO AT DATABASSE*****************.
	 *
	 * @return the string
	 */
	
	/*****************METHODS TO HANDLE REDIRECT FROM SEARCH SCREEN TO DIFERENTS OPTIONS LIKE CONFIRM,APPROVE,ANULATE,REVIEE************/
	public String approveSirtexAction(SirtexOperationResultTO sirtexOperationOne){
		/**VERIFICA SI PERMITE APROBAR**/
//		SirtexOperationResultTO sirtexOperation = sirtexConsultTO.getSirtexOperationSelected();
		SirtexOperationResultTO sirtexOperation = sirtexOperationOne;
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.APPROVE.getCode(),sirtexOperation);
	}
	
	/**
	 * Annulate sirtex action.
	 *
	 * @return the string
	 */
	public String annulateSirtexAction(SirtexOperationResultTO sirtexOperationOne){
//		SirtexOperationResultTO sirtexOperation = sirtexConsultTO.getSirtexOperationSelected();
		SirtexOperationResultTO sirtexOperation = sirtexOperationOne;
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.ANULATE.getCode(),sirtexOperation);
	}
	
	/**
	 * Review sirtex action.
	 *
	 * @return the string
	 */
	public String reviewSirtexAction(SirtexOperationResultTO sirtexOperationOne){
//		SirtexOperationResultTO sirtexOperation = sirtexConsultTO.getSirtexOperationSelected();
		SirtexOperationResultTO sirtexOperation = sirtexOperationOne;
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REVISE,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.REVIEW.getCode(),sirtexOperation);
	}
	
	/**
	 * Reject sirtex action.
	 *
	 * @return the string
	 */
	public String rejectSirtexAction(SirtexOperationResultTO sirtexOperationOne){
//		SirtexOperationResultTO sirtexOperation = sirtexConsultTO.getSirtexOperationSelected();
		SirtexOperationResultTO sirtexOperation = sirtexOperationOne;
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REJECT,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.REJECT.getCode(),sirtexOperation);
	}
	
	/**
	 * Confirm sirtex action.
	 *
	 * @return the string
	 */
	public String confirmSirtexAction(SirtexOperationResultTO sirtexOperationOne){
//		SirtexOperationResultTO sirtexOperation = sirtexConsultTO.getSirtexOperationSelected();
		SirtexOperationResultTO sirtexOperation = sirtexOperationOne;
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.CONFIRM.getCode(),sirtexOperation);
	}
	
	/**
	 * Sett term sirtex action.
	 *
	 * @return the string
	 */
	public String settTermSirtexAction(SirtexOperationResultTO sirtexOperationOne){
//		SirtexOperationResultTO sirtexOperation = sirtexConsultTO.getSirtexOperationSelected();
		SirtexOperationResultTO sirtexOperation = sirtexOperationOne;
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_DESACTIVE,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.SETTLEMENT.getCode(),sirtexOperation);
	}
	
	/**  Variables estaticas *. */
	private static final String CONFIRM 	 = "confirm";
	
	/** The Constant REJECT. */
	private static final String ANNULATE 	 = "annulate";
	
	/** The Constant REJECT. */
	private static final String REJECT 	     = "reject";
	
	/** The Constant GENERATE. */
	private static final String APPROVE 	 = "approve";
	
	/** The Constant GENERATE. */
	private static final String REVIEW 		 = "review";
	
	/** The Constant GENERATE. */
	private static final String SETTERM 	 = "setTerm";
	
	String option = "";
	
	Integer setTermOption = null;
	
	
	
	/**
	 * Before to detail.
	 *
	 * @param button the button
	 * @throws InterruptedException the interrupted exception
	 * @throws ServiceException the service exception
	 */
	public String beforeToDetail(String button) throws InterruptedException, ServiceException{
		
		//Verificamos que se seleccionen registros 
		if(sirtexConsultTO.getSirtexOperationSelected().size() > GeneralConstants.ZERO_VALUE_INTEGER){
			
			//Si es mas de un registro comenzamos a validar y recien enviamos a procesar solicitud a solicitud
			if(sirtexConsultTO.getSirtexOperationSelected().size() > GeneralConstants.ONE_VALUE_INTEGER){
				
				//Primero validar q el usuario pueda ejecutar esa accion
				for (SirtexOperationResultTO  sirtexValidation : sirtexConsultTO.getSirtexOperationSelected()){
					if (validateActionUser(button, sirtexValidation).equals("")){
						return null; //issue 1113
					}
				}
				//Segundo validar que todas las opciones tengan el mismo estado valido para ejecutar la accion
				for (SirtexOperationResultTO listToValidate : sirtexConsultTO.getSirtexOperationSelected()){
					if(button.equals(APPROVE) || button.equals(ANNULATE)){
						if(!listToValidate.getOperationState().equals(SirtexOperationStateType.REGISTERED.getCode())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
									JSFUtilities.showSimpleValidationDialog();
									return null;
							}
					}else{
						if(button.equals(REVIEW)){
							if(!listToValidate.getOperationState().equals(SirtexOperationStateType.APROVED.getCode())){
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
										JSFUtilities.showSimpleValidationDialog();
										return null;
							}
						}else{
							if(button.equals(CONFIRM)){
								if(!listToValidate.getOperationState().equals(SirtexOperationStateType.REVIEWED.getCode())){
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
											JSFUtilities.showSimpleValidationDialog();
											return null;
								}
							}else{
								if(button.equals(REJECT)){
									if(!listToValidate.getOperationState().equals(SirtexOperationStateType.APROVED.getCode()) 
											&& !listToValidate.getOperationState().equals(SirtexOperationStateType.REVIEWED.getCode()) ){
										showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
												, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
												JSFUtilities.showSimpleValidationDialog();
												return null;
									}
								}else{
									if(button.equals(SETTERM)){
										if(!listToValidate.getOperationState().equals(SirtexOperationStateType.CASH_SETTLED.getCode()) 
												&& !listToValidate.getOperationState().equals(SirtexOperationStateType.CONFIRMED.getCode()) ){
											showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
													, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
													JSFUtilities.showSimpleValidationDialog();
													return null;
										}
										
										//Solo pueden vencerse operaciones de reporto
										if(listToValidate.getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_FIXED_INCOME.getCode())){
											showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
													, PropertiesUtilities.getMessage("msg.sirtex.multiple.action.cash.fixed"));
													JSFUtilities.showSimpleValidationDialog();
													return null;
										}
									}
								}
							}
						}
					}
				}
				
				//Cuando sea vencimiento validamos la fecha superior para ejecutar una u otra opcion
				if(button.equals(SETTERM)){
					
					Integer countMayor = 0;
					Integer countIgual = 0;
					//Reiniciamos indicador 
					setTermOption = null;
					
					//Verificamos que todas las fechas sean iguales
					for (SirtexOperationResultTO listToValidate : sirtexConsultTO.getSirtexOperationSelected()){
						for (SirtexOperationResultTO listToValidateDate : sirtexConsultTO.getSirtexOperationSelected()){
							if(!CommonsUtilities.truncateDateTime(listToValidate.getSettlementTermDate()).equals(
									CommonsUtilities.truncateDateTime(listToValidateDate.getSettlementTermDate()))){
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage("msg.sirtex.multiple.action.setterm.error"));
										JSFUtilities.showSimpleValidationDialog();
										return null;
							}
						}
					}
					
					//Contamos la cantidad de operaciones y verificamos que las fechas sean coherentes
					for (SirtexOperationResultTO listToValidate : sirtexConsultTO.getSirtexOperationSelected()){
						if(CommonsUtilities.truncateDateTime(listToValidate.getSettlementTermDate()).equals(getCurrentSystemDate())){
							countIgual = countIgual + GeneralConstants.ONE_VALUE_INTEGER;
						}
						if(CommonsUtilities.truncateDateTime(listToValidate.getSettlementTermDate()).after(getCurrentSystemDate())){
							countMayor = countMayor + GeneralConstants.ONE_VALUE_INTEGER;
						}
					}
					
					//Si todas las fechas de vencimiento coordinan con la fecha actual, mandamos vencimiento normal
					if(countIgual.equals(sirtexConsultTO.getSirtexOperationSelected().size())){
						setTermOption = GeneralConstants.ONE_VALUE_INTEGER;
					
					//Si todas las fechas son mayores a la fecha actual, mandamos vencimiento anticipado
					}else if(countMayor.equals(sirtexConsultTO.getSirtexOperationSelected().size())){
						setTermOption = GeneralConstants.TWO_VALUE_INTEGER;
						}else{
							//Si las fechas no son todas iguales o todas superiores enviamos mensaje de revision
							setTermOption = null;
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("msg.sirtex.multiple.action.setterm.error"));
									JSFUtilities.showSimpleValidationDialog();
									return null;
						}
				}
				
				//Validamos que las operacion no tienen valores con cupones LTS, LRS, LBS, CDS, CUP
				if(button.equals(REVIEW)){
					for (SirtexOperationResultTO  sirtexValidation : sirtexConsultTO.getSirtexOperationSelected()){
						if (!sirtexValidation.getSecurityCode().substring(0, 3).equals(SecurityClassType.LTS.getText1())
								&& !sirtexValidation.getSecurityCode().substring(0, 3).equals("LRS")
								&& !sirtexValidation.getSecurityCode().substring(0, 3).equals(SecurityClassType.LBS.getText1())
								&& !sirtexValidation.getSecurityCode().substring(0, 3).equals(SecurityClassType.CDS.getText1())
								&& !sirtexValidation.getSecurityCode().substring(0, 3).equals(SecurityClassType.CUP.getText1())
								&& !sirtexValidation.getSecurityCode().substring(0, 3).equals(SecurityClassType.DPF.getText1()) //issue 1113
								){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("msg.sirtex.multiple.action.multiple"));
									JSFUtilities.showSimpleValidationDialog();
									return null;
						}
					}
				}
				
				
				//Si pasa las validaciones enviamos mensaje de confirmacion
				String messageProperties = "";
				switch(button){
					case APPROVE:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_APPROVE_CONFIRM_MULTIPLE;
									option = APPROVE; break;
					case REVIEW:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REVIEW_CONFIRM_MULTIPLE;
									option = REVIEW; break;
					case CONFIRM:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_CONFIRM_CONFIRM_MULTIPLE;
									option = CONFIRM; break;
					case ANNULATE:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_ANNULATE_CONFIRM_MULTIPLE;
									option = ANNULATE; break;
					case REJECT:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REJECT_CONFIRM_MULTIPLE;
									option = REJECT; break;
					case SETTERM:	
								//Todos iguales 
								if(setTermOption.equals(GeneralConstants.ONE_VALUE_INTEGER)){
									messageProperties = PropertiesConstants.SIRTEX_OPERATION_SETTLEMENT_CONFIRM_MULTIPLE_EQUAL;
								
									//Todos mayores
								}else if(setTermOption.equals(GeneralConstants.TWO_VALUE_INTEGER)){
										messageProperties = PropertiesConstants.SIRTEX_OPERATION_SETTLEMENT_CONFIRM_MULTIPLE_UP;
										}
								option = SETTERM; break;
				}
					//Concatenamos los ID para mostrar en pantalla
					List <Integer> idsConcat = new ArrayList<>();
					for(SirtexOperationResultTO idConcat : sirtexConsultTO.getSirtexOperationSelected()){
						idsConcat.add(idConcat.getIdMechanismOperationRequestPk().intValue());
					}
				//Mostramos mensaje	
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(messageProperties,new Object[]{StringUtils.join(idsConcat,",")}));
				JSFUtilities.executeJavascriptFunction("PF('cnfwSirtexOperations').show()");
				
			}else{
				//Un solo registro seleccionado, procesamos por la accion seleccionada
				switch (button) {
					case CONFIRM:	return confirmSirtexAction(sirtexConsultTO.getSirtexOperationSelected().get(0));
					case ANNULATE:	return annulateSirtexAction(sirtexConsultTO.getSirtexOperationSelected().get(0));
					case REJECT:	return rejectSirtexAction(sirtexConsultTO.getSirtexOperationSelected().get(0));
					case APPROVE:	return approveSirtexAction(sirtexConsultTO.getSirtexOperationSelected().get(0));
					case REVIEW:	return reviewSirtexAction(sirtexConsultTO.getSirtexOperationSelected().get(0));
					
					// TODO: Cuando es uno solo validar q sea reporto
					case SETTERM:	return settTermSirtexAction(sirtexConsultTO.getSirtexOperationSelected().get(0));
				}
			}
		}else{
			//Si no se selecciona nungun registro enviamos mensaje
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		return null;
	}
	
	/**
	 * Metodo para ejecutar la accion seleccionada tras la confirmacion
	 * @param button
	 * @param listSelected
	 * @throws ServiceException
	 */
	@LoggerAuditWeb
	public void executeActionMultiple () {
		
		//Concatenamos los ID para mostrar en pantalla
		List <Integer> idsConcat = new ArrayList<>();
		
		//Realizamos la accion para cada solicitud
		for(SirtexOperationResultTO sirtexAction :	sirtexConsultTO.getSirtexOperationSelected()){
			
			//Concatenamos IDs
			idsConcat.add(sirtexAction.getIdMechanismOperationRequestPk().intValue());
			
			Long mechanismID = sirtexAction.getIdMechanismOperationRequestPk();
			TradeRequest mechanismOperation = new TradeRequest();
			try {
				mechanismOperation = sirtexOperationFacade.findSirtexOperationFromId(mechanismID,parameterDescription);
			
				sirtexRegisterTO = new SirtexManagementTO();			
				sirtexRegisterTO.setSirtexOperation(mechanismOperation);
				sirtexRegisterTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				if(mechanismOperation.getCouponsTradeRequestList().size() != BigDecimal.ONE.intValue()){
					sirtexRegisterTO.setBlIsSplit(Boolean.TRUE);
				}
				Map<Long,Participant> allParticipantMap = sirtexConsultTO.getAllParticipantMap();
				sirtexRegisterTO.setParticipantBuyers(Arrays.asList(allParticipantMap.get(mechanismOperation.getBuyerParticipant().getIdParticipantPk())));
				sirtexRegisterTO.setParticipantSellers(Arrays.asList(allParticipantMap.get(mechanismOperation.getSellerParticipant().getIdParticipantPk())));
				
				switch(option){
					case CONFIRM:  	sirtexOperationFacade.confirmSirtexOperation(mechanismOperation); break;
					case APPROVE: 	sirtexOperationFacade.approveOtcOpereationServiceFacade(mechanismOperation); break;
					case REVIEW: 	
									mechanismOperation.getCouponsTradeRequestList().get(0).setAcceptedQuantity(
											mechanismOperation.getCouponsTradeRequestList().get(0).getSuppliedQuantity());
									
									mechanismOperation.getCouponsTradeRequestList().get(0).setIndSuppliedAccepted(BooleanType.YES.getCode());
									mechanismOperation.getCouponsTradeRequestList().get(0).setSuppliedOk(Boolean.TRUE);
									
									sirtexOperationFacade.reviewSirtexOperation(mechanismOperation); 
									break;
					case ANNULATE: 	sirtexOperationFacade.anulateSirtexOperation(mechanismOperation); break;
					case REJECT: 	sirtexOperationFacade.rejectSirtexOperation(mechanismOperation); break;
					case SETTERM:
									for(CouponTradeRequest acceptCoupon : mechanismOperation.getCouponsTradeRequestList()){
										if(!acceptCoupon.getAcceptedQuantity().equals(null) 
												&& acceptCoupon.getIndSuppliedAccepted().equals(BooleanType.YES.getCode())   //Verifica que tenga el CHECK de Revisado
												){
											acceptCoupon.setIndSettlementOpe(BooleanType.YES.getCode());
										}
									}
									if(setTermOption.equals(GeneralConstants.ONE_VALUE_INTEGER)){
										sirtexOperationFacade.settlementTermSirtexOperation(mechanismOperation);
									}else if(setTermOption.equals(GeneralConstants.TWO_VALUE_INTEGER)){
										sirtexOperationFacade.settlementTermAnticipadedOperation(mechanismOperation);
									}
				}
			} catch (Exception e) {
				// TODO: no tengo ninguna excepcion
			}
		}
		
		String messageProperties = "";
		switch(option){
			case APPROVE:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_APPROVE_CONFIRM_OK_MULTIPLE;break;
			case REVIEW:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REVIEW_CONFIRM_OK_MULTIPLE;break;
			case CONFIRM:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_CONFIRM_CONFIRM_OK_MULTIPLE;break;
			case ANNULATE:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_ANNULATE_CONFIRM_OK_MULTIPLE;break;
			case REJECT:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REJECT_CONFIRM_OK_MULTIPLE;break;
			case SETTERM:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_SETTLEMENT_CONFIRM_OK_MULTIPLE;break;
		}
		
		//Mensaje de exito
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(messageProperties, new Object[]{StringUtils.join(idsConcat,",")}));
		
		//actualizamos la pantalla
		JSFUtilities.showSimpleEndTransactionDialog("searchSirtexOperation");
		searchSirtexOperations();
	}

	/**
	 * 
	 * @param action
	 * @param sirtexOperationOne
	 * @return
	 * @throws ServiceException 
	 */
	public String validateActionUser(String action,SirtexOperationResultTO sirtexOperationOne){
			
		//Cargamos la operacion seleccionada
		SirtexOperationResultTO sirtexOperation = sirtexOperationOne;
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
		//Si el usuario no puede ejecutar la accion
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			 String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			 //Enviamos el cuerpo del mensaje
			 //USUARIO NO TIENE PRIVILEGIOS PARA "ACTION" LAS SOLICITUDES 
			 String bodyMessage = null;
			    	switch (action) {
						case CONFIRM:	bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});break;
						case ANNULATE:	bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,new Object[]{StringUtils.join(lstOperationUser,",")});break;
						case REJECT:	bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REJECT,new Object[]{StringUtils.join(lstOperationUser,",")});break;
						case APPROVE:	bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,new Object[]{StringUtils.join(lstOperationUser,",")});break;
						case REVIEW:	bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REVISE,new Object[]{StringUtils.join(lstOperationUser,",")});break;
						case SETTERM:	bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_DESACTIVE,new Object[]{StringUtils.join(lstOperationUser,",")});break;
					}
			 showMessageOnDialog(headerMessage, bodyMessage);
			 JSFUtilities.showSimpleValidationDialog();
			 return "";
		   }
		//issue 1113
		Integer actionSelect = null;
		
		switch (action) {
			case CONFIRM:	actionSelect = ViewOperationsType.CONFIRM.getCode(); break;
			case ANNULATE:	actionSelect = ViewOperationsType.ANULATE.getCode(); break;
			case REJECT:	actionSelect = ViewOperationsType.REJECT.getCode(); break;
			case APPROVE:	actionSelect = ViewOperationsType.APPROVE.getCode(); break;
			case REVIEW:	actionSelect = ViewOperationsType.REVIEW.getCode(); break;
			case SETTERM:	actionSelect = ViewOperationsType.SETTLEMENT.getCode(); break;
		} 
		
		return validateSirtexOperation(actionSelect,sirtexOperation);
	}
			
	/**
	 * Validate sirtex operation.
	 *
	 * @param viewOperationType the view operation type
	 * @param sirtexOperation the sirtex operation
	 * @return the string
	 */
	public String validateSirtexOperation(Integer viewOperationType, SirtexOperationResultTO sirtexOperation){
		Long mechanismID = sirtexOperation.getIdMechanismOperationRequestPk();
		TradeRequest mechanismOperation = null;
		Boolean userIsParticipant = userInfo.getUserAccountSession().isParticipantInstitucion();
		Long participantCode = userInfo.getUserAccountSession().getParticipantCode();
		
		List<Integer> statesValidate = new ArrayList<Integer>();
		switch(ViewOperationsType.get(viewOperationType)){
			case APPROVE: case ANULATE:
					statesValidate.add(SirtexOperationStateType.REGISTERED.getCode());break;
			case REVIEW:
					statesValidate.add(SirtexOperationStateType.APROVED.getCode());break;
			case CONFIRM: 
					statesValidate.add(SirtexOperationStateType.REVIEWED.getCode());break;
			case REJECT:
				statesValidate.add(SirtexOperationStateType.REVIEWED.getCode());
				statesValidate.add(SirtexOperationStateType.APROVED.getCode());break;
			case SETTLEMENT:
				statesValidate.add(SirtexOperationStateType.CASH_SETTLED.getCode());
				statesValidate.add(SirtexOperationStateType.CONFIRMED.getCode());break;
			default:
				break;
		}
		
		try {
			/**GET THE MECHANISM OPERATION*/
			mechanismOperation = sirtexOperationFacade.findSirtexOperationFromId(mechanismID,parameterDescription);
			mechanismOperation.setOriginRequest(OtcOperationOriginRequestType.SALE.getCode());
			Integer operationState = mechanismOperation.getOperationState();
			
			if(!statesValidate.isEmpty() && !statesValidate.contains(operationState)){
				throw new ServiceException(ErrorServiceType.TEMPLATE_INCORRECT_STATE);
			}
			
			/**VERIFIED IF IT'S SETTLEMENT TERM*/
			if(viewOperationType.equals(ViewOperationsType.SETTLEMENT.getCode())){
				if(mechanismOperation.getMechanisnModality().getNegotiationModality().getIndTermSettlement().equals(BooleanType.NO.getCode())){
					throw new ServiceException(ErrorServiceType.TRADE_OPERATION_SETTLEMENT_TERM);
//				}else{
//					/**IF SETTLEMENT DATE IT'S NOT TODAY*/
//					if(!mechanismOperation.getTermSettlementDate().equals(CommonsUtilities.currentDate())){
//						throw new ServiceException(ErrorServiceType.TRADE_OPERATION_SETTLEMENT_TERM_DATE);
//					}
				}
			}
			
			setViewOperationType(viewOperationType);

			/**SI USUARIO ES PARTICIPANTE*/
			if(userIsParticipant){
				Long participantPermited = null;
				switch(SirtexOperationStateType.get(operationState)){
					case REGISTERED:
						participantPermited = mechanismOperation.getSellerParticipant().getIdParticipantPk();break;
					case APROVED: case REVIEWED: case CONFIRMED: //issue 1113
						participantPermited = mechanismOperation.getBuyerParticipant().getIdParticipantPk();break;
					default:
						break;
				}
				
				// issue 1127: si se trata de una operacion diferente a Consulta(View) entonces se evalua los privilegios del participante
				if(!viewOperationType.equals(ViewOperationsType.CONSULT.getCode()))
					if(participantPermited!=null && !participantPermited.equals(participantCode)){
						throw new ServiceException(ErrorServiceType.TRADE_OPERATION_PARTICIPANT_INCORRECT_ACTION);
					}
			}

			sirtexRegisterTO = new SirtexManagementTO();			
			sirtexRegisterTO.setSirtexOperation(mechanismOperation);
			sirtexRegisterTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
			//seteando la cuenta vendedor
			sirtexRegisterTO.setAccountSeller(mechanismOperation.getAccountSirtexOperations().get(0).getHolderAccount());
			sirtexRegisterTO.getSirtexOperation().setSellerAccount(sirtexRegisterTO.getAccountSeller());
			sirtexRegisterTO.setHolderSeller(sirtexOperationFacade.searchHolderByHolderAccount(sirtexRegisterTO.getAccountSeller().getIdHolderAccountPk()));
			
			if(mechanismOperation.getCouponsTradeRequestList().size() != BigDecimal.ONE.intValue()){
				sirtexRegisterTO.setBlIsSplit(Boolean.TRUE);
			}
			
			/**CARGAMOS PARAMETROS PARA EL REGISTRO DE SIRTEX*/
			loadSettlementType();
			loadSettlementSchemeType();
			loadCboOriginRequestType();
			loadCboModalityAndParticipants();
			
			//issue 1127: si la operacion es revision y se trata de un DPF entonces setear de manera cerrada el chek y cantidad (1)
			switch(ViewOperationsType.get(viewOperationType)){
					
			case REVIEW: 
				if(sirtexRegisterTO.getSirtexOperation().getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
					for(CouponTradeRequest ctr:sirtexRegisterTO.getSirtexOperation().getCouponsTradeRequestList()){
						ctr.setSuppliedOk(true);
						ctr.setAcceptedQuantity(ctr.getSuppliedQuantity());
					}
				}	
				break;
				
			}
			
			
			Map<Long,Participant> allParticipantMap = sirtexConsultTO.getAllParticipantMap();
			sirtexRegisterTO.setParticipantBuyers(Arrays.asList(allParticipantMap.get(mechanismOperation.getBuyerParticipant().getIdParticipantPk())));
			sirtexRegisterTO.setParticipantSellers(Arrays.asList(allParticipantMap.get(mechanismOperation.getSellerParticipant().getIdParticipantPk())));
			
			//issue 1127: aca hacemos las matufiadas para mostrar la grilla de holder account en vez del helper anterior a este issue
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(sirtexRegisterTO.getHolderSeller().getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setParticipantFk(sirtexRegisterTO.getSirtexOperation().getSellerParticipant().getIdParticipantPk());
			try {
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				if(lstAccountTo.size()>0){
					lstSourceAccountTo = lstAccountTo;
					
					for(HolderAccountHelperResultTO holderAccountTOSelected: lstSourceAccountTo){
						if(holderAccountTOSelected.getAccountPk().equals(sirtexRegisterTO.getAccountSeller().getIdHolderAccountPk())){
							sourceAccountTo = holderAccountTOSelected;
							break;
						}
					}
				}else{
					sirtexRegisterTO.getSirtexOperation().getSellerParticipant().setHolder(new Holder());
					sirtexRegisterTO.setHolderSeller(new Holder());
					lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					sourceAccountTo = new HolderAccountHelperResultTO();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			if(isViewOperationSettlement()){
				return SIRTEX_TERM_RULE;
			}else{
				return SIRTEX_RULE;
			}
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
		return "";
	}
	
	/**
	 * ***************END - METHODS TO HANDLE REDIRECT FROM SEARCH SCREEN TO DIFERENTS OPTIONS LIKE CONFIRM,APPROVE,ANULATE,REVIEE*****.
	 *
	 * @throws ServiceException the service exception
	 */

	private void loadSettlementType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SETTLEMENT_TYPE.getCode());
		sirtexRegisterTO.setLstSettlementType(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable parameter: sirtexRegisterTO.getLstSettlementType())
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
	}
	
	/**
	 * Load settlement scheme type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSettlementSchemeType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SETTLEMENT_SCHEME_TYPE.getCode());
		sirtexRegisterTO.setLstSchemeType(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable parameter: sirtexRegisterTO.getLstSchemeType())
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
	}
	
	/**
	 * Load cbo origin request type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboOriginRequestType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.OTC_REQUEST_ORIGING.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		sirtexRegisterTO.setLstCboOtcOriginRequestType(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable parameter: sirtexRegisterTO.getLstCboOtcOriginRequestType())
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
	}
	
	/**
	 * Load cbo modality and participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboModalityAndParticipants() throws ServiceException{
		MechanismModalityTO mechanismModalityTO = new MechanismModalityTO();
		mechanismModalityTO.setIdNegotiationMechanismPK(NegotiationMechanismType.SIRTEX.getCode());
		mechanismModalityTO.setNegotiationModalityState(MechanismModalityStateType.ACTIVE.getCode());
		
//		/**SI ES BC SOLO PUEDE NEGOCIAR EN REPORTO REVERSO*/
//		if(participantLogout!=null && participantLogout.equals(new Long(idParticipantBC))){
//			mechanismModalityTO.setIdNegotiationModalityPkList(Arrays.asList(NegotiationModalityType.REPO_REVERSAL_FIXED_INCOME.getCode()));
//		}
		
		/**OBTENEMOS TODAS LAS MODALIDADES Y MECANISMOS*/
		Integer role = ParticipantRoleType.BUY.getCode();
		List<MechanismModality> mechanismModalityBuyList = sirtexOperationFacade.findMechanismModalityList(mechanismModalityTO,parameterDescription,role);
		sirtexRegisterTO.setParticipantBuyByModalities(sirtexOperationFacade.configModalitiesOnParticipantMap(mechanismModalityBuyList));
		
		role = ParticipantRoleType.SELL.getCode();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			mechanismModalityTO.setIdParticipantPk(participantLogout);
		}
		List<MechanismModality> mechanismModalityList = sirtexOperationFacade.findMechanismModalityList(mechanismModalityTO,parameterDescription,role);
		sirtexRegisterTO.setParticipantSellByModalities(sirtexOperationFacade.configModalitiesOnParticipantMap(mechanismModalityList));
		
		sirtexRegisterTO.setMechanismModalityList(mechanismModalityList);
	}
	
	/**
	 * Load cbo modality search.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboModalitySearch() throws ServiceException{
		MechanismModalityTO mechanismModalityTO = new MechanismModalityTO();
		mechanismModalityTO.setIdNegotiationMechanismPK(NegotiationMechanismType.SIRTEX.getCode());
		mechanismModalityTO.setNegotiationModalityState(MechanismModalityStateType.ACTIVE.getCode());
		List<MechanismModality> mechanismModalityList = sirtexOperationFacade.findMechanismModalityList(mechanismModalityTO,parameterDescription,null);
		sirtexConsultTO.setModalitiesSearchList(mechanismModalityList);
	}
	
	/**
	 * Load cbo participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboParticipants() throws ServiceException{//Cargamos los Participantes
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		sirtexConsultTO.setAllParticipantList(accountsFacade.getLisParticipantServiceBean(participantTO));
		for(Participant p: sirtexConsultTO.getAllParticipantList()){
			if(sirtexConsultTO.getAllParticipantMap()==null){
				sirtexConsultTO.setAllParticipantMap(new HashMap<Long,Participant>());
			}
			sirtexConsultTO.getAllParticipantMap().put(p.getIdParticipantPk(), p);
		}
	}
	
	/**
	 * Gets the participant bc.
	 *
	 * @return the participant bc
	 */
	private Participant getParticipantBC(){
		Long participantBC =new Long(idParticipantBC);
		return new Participant(participantBC);
	}
	
	/**
	 * Load sirtex operation state type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSirtexOperationStateType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SIRTEX_OPERATION_STATE.getCode());
		List<ParameterTable> operationStates = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: operationStates)
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
		sirtexConsultTO.setLstCboOtcOperationStateType(operationStates);
		
		parameterTableTO.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		operationStates = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameter: operationStates){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
			//issue 677
			if(parameter.getParameterTablePk().equals(MechanismOperationStateType.TERM_SETTLED.getCode())){
				sirtexConsultTO.getLstCboOtcOperationStateType().add(parameter);
			}
		}
	}
	
	/**
	 * Load cbo date search type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboDateSearchType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SEARCH_DATE_TYPE.getCode());
		sirtexConsultTO.setLstCboDateSearchType(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		sirtexConsultTO.getSirtexFilters().setIdSearchDateType(OtcOperationDateSearchType.OPERATION_DATE.getCode());
		
		for(ParameterTable parameter: sirtexConsultTO.getLstCboDateSearchType())
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
	}
	
	/**
	 * Change negotiation modality.
	 */
	public void changeNegotiationModality(){
		SirtexManagementTO sirtexManagement = sirtexRegisterTO;
		TradeRequest operation = sirtexManagement.getSirtexOperation();
		MechanismModality mechanismModality = null;
		Long idModalitySelected = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
		
		Participant buyerParticipant = operation.getBuyerParticipant();
		Participant sellerParticipant = operation.getSellerParticipant();
		if(buyerParticipant.getIdParticipantPk()!=null || sellerParticipant.getIdParticipantPk()!=null){
			cleanParticipant();/**LIMPIAMOS INFORMACION DE PARTICIPANTE Y CAMPOS SEQUENCIALES*/
		}
		
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			sirtexManagement.setParticipantSellers(sirtexManagement.getParticipantSellByModalities().get(idModalitySelected));
			sirtexManagement.setParticipantBuyers(sirtexManagement.getParticipantBuyByModalities().get(idModalitySelected));
		}else if(participantLogout!=null){
			sirtexManagement.setParticipantSellers(sirtexManagement.getParticipantSellByModalities().get(idModalitySelected));
			sirtexManagement.setParticipantBuyers(new ArrayList<Participant>());
			operation.setSellerParticipant(new Participant(participantLogout));
			
			/**REMOVE THE PARTICIPANT LIKE SELLER*/
			for (Iterator<Participant> iter = sirtexManagement.getParticipantBuyByModalities().get(idModalitySelected).iterator(); iter.hasNext();){
				Participant element = iter.next();
				if(!element.getIdParticipantPk().equals(participantLogout)){
					sirtexManagement.getParticipantBuyers().add(element);
				}
			}
		}
		
		mechanismModality = sirtexOperationFacade.findMechanismModality(sirtexManagement);
				operation.setSettlementScheme(mechanismModality.getSettlementSchema());
		operation.setSettlementType(mechanismModality.getSettlementType());
		operation.setMechanisnModality(mechanismModality);
		sirtexManagement.setSirtexOperation(operation);
		sirtexRegisterTO = sirtexManagement;
		sirtexRegisterTO.setHolderSeller(new Holder());
		sirtexRegisterTO.getSirtexOperation().setSellerAccount(new HolderAccount());
		sirtexRegisterTO.setAccountSeller(new HolderAccount());
		sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
		sirtexRegisterTO.getSirtexOperation().setSettlementCurrency(null);
		sirtexRegisterTO.getSirtexOperation().setCouponsTradeRequestList(new ArrayList<CouponTradeRequest>());
		sirtexRegisterTO.setBlIsSplit(false);
		lstSourceAccountTo=new ArrayList<>();
		sourceAccountTo=new HolderAccountHelperResultTO();
		idHolderEnteredOld = null;
		
		JSFUtilities.updateComponent("frmSirtexOperation");
				
		if(participantLogout!=null){
			changeParticipantSeller();
		}
	}
	
	/**
	 * Change security.
	 */
	public void changeSecurity(){//Metodo para validar el valor consultado
		
		//por defecto limpiando el campo Dias Liquidacion Plazo
		sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
			SirtexManagementTO sirtexOperation = sirtexRegisterTO;
		TradeRequest operation = sirtexOperation.getSirtexOperation();
		operation.setCouponsTradeRequestList(new ArrayList<CouponTradeRequest>());
		sirtexOperation.setBlIsSplit(false);
		
		try{
			//validando que se haya seleccionado una cuenta titular
			if(Validations.validateIsNotNull(sirtexRegisterTO.getAccountSeller()) && Validations.validateIsNotNull(sirtexRegisterTO.getAccountSeller().getIdHolderAccountPk())){
				
				if(Validations.validateIsNotNull(sirtexOperation.getSirtexOperation().getSecurities())&&Validations.validateIsNotNull(sirtexOperation.getSirtexOperation().getSecurities().getIdSecurityCodePk())){
					Security securityFind=sirtexOperationService.find(Security.class, sirtexOperation.getSirtexOperation().getSecurities().getIdSecurityCodePk());
					if(securityFind==null){
						sirtexOperation.getSirtexOperation().setSecurities(new Security());
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage("msg.alert.validation.sirtex.search.securities.no.exist"));
						JSFUtilities.showSimpleValidationDialog();
						JSFUtilities.updateComponent("frmSirtexOperation");
						return;
					}else{
						operation.setSecurities(securityFind);	
					}
				}else {
					sirtexOperation.getSirtexOperation().setSecurities(new Security());
					JSFUtilities.updateComponent("frmSirtexOperation");
					return;}
				
			}else{
				sirtexRegisterTO.getSirtexOperation().setSecurities(new Security());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("msg.validation.sirtex.secutities.require.account"));
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.updateComponent("frmSirtexOperation");
				return;
			}
			
			/**APLICAMOS VALIDACIONES DEL VALOR, EN SIRTEX*/
			sirtexOperationFacade.validateSecurityForSirtex(sirtexRegisterTO);
			/**USAMOS DATOS DEL VALOR Y SETEAMOS A LA OPERACION*/
			Security security = sirtexOperation.getSirtexOperation().getSecurities();
			Date expirationDate = security.getExpirationDate();
			security.setPendientDays(new Long(CommonsUtilities.getDaysBetween(CommonsUtilities.currentDate(), expirationDate)));
			operation.setNominalValue(security.getCurrentNominalValue());
			operation.setSettlementCurrency(security.getCurrency());
			
			/**BUSCAMOS AL TITULAR VENDEDOR Y COMPRADOR*/
			Participant participantSeller = operation.getSellerParticipant();
			Holder holderSeller = sirtexOperation.getHolderSeller();
			
			Participant participantBuyer = operation.getBuyerParticipant();
			Holder holderBuyer = sirtexOperationFacade.findHolderFromParticipant(participantBuyer.getIdParticipantPk());

			//TODO no se debe setear
			//operation.getSellerParticipant().setHolder(holderSeller);
			operation.getBuyerParticipant().setHolder(holderBuyer);
			
			/**BUSCAMOS CUENTAS DE PARTICIPANTES*/
			Long partBC = new Long(idParticipantBC);
			Integer roleSell = ParticipantRoleType.SELL.getCode();
			Integer roleBuy = ParticipantRoleType.BUY.getCode(); 
			
			List<HolderAccount> sellerAccounts = new ArrayList<>();
			sellerAccounts.add(sirtexRegisterTO.getSirtexOperation().getSellerAccount());
			AccountTradeRequest accOperationSeller = operation.getAccountSirtexOperations().get(POSITION_SELLER);
			
			List<HolderAccount> buyerAccounts  = sirtexOperationFacade.findHolderAccountsFromBalances(participantBuyer, security, roleBuy,partBC);	
			AccountTradeRequest accOperationBuyer = operation.getAccountSirtexOperations().get(POSITION_BUYER);

			if(sellerAccounts.size() == BigDecimal.ONE.intValue()){
				accOperationSeller.setHolderAccount(sellerAccounts.get(0));
				changeHolderAccountSeller();
			}
			
			if(buyerAccounts.size() == BigDecimal.ONE.intValue()){
				accOperationBuyer.setHolderAccount(buyerAccounts.get(0));
				changeHolderAccountBuyer();
			}else{
				if(userInfo.getUserAccountSession().isParticipantInstitucion()){
					throw new ServiceException(ErrorServiceType.TRADE_OPERATION_ACCOUNTS_INCORRECT);
				}
			}

			/**SETEAMOS CUENTAS A TO PARA ADMINSITRARLO EN VISTA*/
			sirtexRegisterTO.setHolderAccountSeller(sellerAccounts);			
			sirtexRegisterTO.setHolderAccountBuyers(buyerAccounts);
			JSFUtilities.updateComponent("frmSirtexOperation");			
			
//			if(security.getIndSplitCoupon()!=null && security.getIndSplitCoupon().equals(BooleanType.YES.getCode())){
//				sirtexRegisterTO.setBlIsSplit(Boolean.TRUE);
//				showCouponsToSplited();
//			}			
		}catch(ServiceException ex){
			operation.setSecurities(new Security());
			operation.setSettlementCurrency(null);
			operation.setNominalValue(null);
			//JSFUtilities.resetComponent("frmSirtexOperation:idValorB:componentSecPaHolAccHelper");
			JSFUtilities.updateComponent("frmSirtexOperation:pnlHelpSecurity");
			
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(ex.getMessage(),ex.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		sirtexRegisterTO.setSirtexOperation(operation);
	}
	
	/**
	 * Load dialog mark fact.
	 */
	public void loadDialogMarkFact(){
		JSFUtilities.executeJavascriptFunction("PF('dlgMarkFactAssigmentW').show()");
	}
	
	/**
	 * Change stock quantity.
	 */
	public void changeStockQuantity(){
		SirtexManagementTO sirtexManagement = sirtexRegisterTO;
		TradeRequest operation = sirtexManagement.getSirtexOperation();
		try {
			if(operation.getSecurities().getIdSecurityCodePk()==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.MCN_CORDIGOVALOR_NOTENTERED));
				throw new ServiceException();
			}
			sirtexOperationFacade.validateStockQuantity(sirtexManagement);
		} catch (ServiceException ex) {
			operation.setStockQuantity(null);
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(ex.getMessage()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Change cash settlement days.
	 */
	public void changeCashSettlementDays(){
		SirtexManagementTO sirtexOperation = sirtexRegisterTO;
		TradeRequest operation = sirtexOperation.getSirtexOperation();
		Long cashSettlementDays = operation.getCashSettlementDays();
		try{
			// validando que se hya seleccionado un valor
			if(Validations.validateIsNotNull(operation.getSecurities())&&Validations.validateIsNotNull(operation.getSecurities().getIdSecurityCodePk()))
				/*VALIDAMOS DIAS DE LIQUIDACION CONTADO*/
				sirtexOperationFacade.validateSettlementDate(sirtexOperation, OperationPartType.CASH_PART.getCode(), cashSettlementDays,null);
			else{
				//sirtexOperationBean.sirtexRegisterTO.sirtexOperation
				sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("msg.validation.sirtex.term.settlement.day"));
				JSFUtilities.showSimpleValidationDialog();
			}
		}catch(ServiceException e){
			operation.setCashSettlementDays(null);
			operation.setCashSettlementDate(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Change term settlement days.
	 */
	public void changeTermSettlementDays(){
		SirtexManagementTO sirtexOperation = sirtexRegisterTO;
		TradeRequest operation = sirtexOperation.getSirtexOperation();
		Long termSettlementDays = operation.getTermSettlementDays();
		try{
			// validando que se hya seleccionado un valor
			if(Validations.validateIsNotNull(operation.getSecurities())&&Validations.validateIsNotNull(operation.getSecurities().getIdSecurityCodePk()))
				/*VALIDAMOS DIAS DE LIQUIDACION PLAZO*/
				sirtexOperationFacade.validateSettlementDate(sirtexOperation, OperationPartType.TERM_PART.getCode(), termSettlementDays,parameterDescription);
			else{
				//sirtexOperationBean.sirtexRegisterTO.sirtexOperation
				sirtexRegisterTO.getSirtexOperation().setTermSettlementDays(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("msg.validation.sirtex.term.settlement.day"));
				JSFUtilities.showSimpleValidationDialog();
			}		
		}catch(ServiceException e){
			operation.setTermSettlementDays(null);
			operation.setTermSettlementDate(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Clean participant.
	 */
	public void cleanParticipant(){
		TradeRequest operation = sirtexRegisterTO.getSirtexOperation();
		operation.getBuyerParticipant().setIdParticipantPk(null);
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			operation.getSellerParticipant().setIdParticipantPk(null);
		}
		sirtexRegisterTO.setSirtexOperation(operation);
//		cleanSecurity();
	}
	
	/**
	 * Search sirtex operations.
	 */
	@LoggerAuditWeb
	public void searchSirtexOperations(){
		sirtexConsultTO.setSirtexOperationSelected(new ArrayList<SirtexOperationResultTO>());
		SirtexOperationTO sirtexOperationSearch = sirtexConsultTO.getSirtexFilters();
		SirtexOperationTO sirtexOperationTO = new SirtexOperationTO();
		sirtexOperationTO.setInitialDate(sirtexOperationSearch.getInitialDate());
		sirtexOperationTO.setFinalDate(sirtexOperationSearch.getFinalDate());
		sirtexOperationTO.setIndInCharge(sirtexOperationSearch.getIndInCharge());
		sirtexOperationTO.setIdSearchDateType(sirtexOperationSearch.getIdSearchDateType());
		sirtexOperationTO.setIdModalityNegotiation(sirtexOperationSearch.getIdModalityNegotiation());
		sirtexOperationTO.setOtcOperationState(sirtexOperationSearch.getOtcOperationState());
		sirtexOperationTO.setIdParticipantBuyer(sirtexOperationSearch.getIdParticipantBuyer());
		sirtexOperationTO.setIdSecurityCodePk(sirtexOperationSearch.getSecuritie().getIdSecurityCodePk());
		List<SirtexOperationResultTO> sirtexOperationsList = null;
		try {
			if(!(userInfo.getUserAccountSession().isDepositaryInstitution() || userInfo.getUserAccountSession().isParticipantInstitucion()
					|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion())){
				throw new ServiceException(ErrorServiceType.USERSESSION_INCORRECT_ACTION);
			}
			
			//Buscamos todas las solicitudes SIRTEX, con los criterios ingresados
			sirtexOperationsList = sirtexOperationFacade.getSirtexOperationsServiceFacade(sirtexOperationTO,parameterDescription);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(ex.getMessage()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
		if(sirtexOperationsList==null){
			sirtexConsultTO.setSirtexDataModel(new GenericDataModel<SirtexOperationResultTO>());
		}else{
			sirtexConsultTO.setSirtexDataModel(new GenericDataModel<SirtexOperationResultTO>(sirtexOperationsList));
		}
	}
	
	/**
	 * Show market fact balance.
	 *
	 * @param sirtexCoupon the sirtex coupon
	 */
	public void showMarketFactBalance(CouponTradeRequest sirtexCoupon){
		TradeRequest sirtexOperation = sirtexCoupon.getTradeRequest();
		Participant participantSeller = sirtexOperation.getSellerParticipant();
		Security security = sirtexOperation.getSecurities();
		AccountTradeRequest accountSeller = sirtexOperation.getAccountSirtexOperations().get(POSITION_SELLER);
		
		Long idParticipantCode = participantSeller.getIdParticipantPk();
		Long idHolderAccountCode = accountSeller.getHolderAccount().getIdHolderAccountPk();
		String idSecurityCode = security.getIdSecurityCodePk();
		
		MarketFactBalanceHelpTO markFactTO = new MarketFactBalanceHelpTO();
		markFactTO.setSecurityCodePk(idSecurityCode);
		markFactTO.setHolderAccountPk(idHolderAccountCode);
		markFactTO.setParticipantPk(idParticipantCode);
		markFactTO.setUiComponentName(SirtexOperationBean.marketFactUI);
		markFactTO.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		if(sirtexCoupon.getSuppliedQuantity()!=null && !sirtexCoupon.getSuppliedQuantity().equals(BigDecimal.ZERO)){
			markFactTO.setBalanceResult(sirtexCoupon.getSuppliedQuantity());
		}
		
		
		if(markFactTO.getBalanceResult()!=null && !markFactTO.getBalanceResult().equals(BigDecimal.ZERO)){
			for(AccountTradeMarketFact transferMarketFact: accountSeller.getAccountSirtexMarkectfacts()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketDate(transferMarketFact.getMarketDate());
				marketDetail.setMarketRate(transferMarketFact.getMarketRate());
				marketDetail.setMarketPrice(transferMarketFact.getMarketPrice());
				marketDetail.setEnteredBalance(transferMarketFact.getMarketQuantity());
				markFactTO.getMarketFacBalances().add(marketDetail);
			}
		}

//		Integer numberOfMarketFact = sirtexOperationFacade.numberOfMarketFact(idParticipantCode, idHolderAccountCode, idSecurityCode);
//		if(numberOfMarketFact.equals(BigDecimal.ONE.intValue())){
//			if(accountSeller.getAccountSirtexMarkectfacts()!=null && markFactTO.getBalanceResult()!=null && markFactTO.getBalanceResult().equals(BigDecimal.ZERO)){
//				
//			}
//		}
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(), markFactTO);
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		
		TradeRequest sirtexOperation = sirtexRegisterTO.getSirtexOperation();
//		Boolean isSplit = sirtexRegisterTO.getBlIsSplit();
		MarketFactBalanceHelpTO marketFactBalance = sirtexRegisterTO.getMarketFactComponentUI();
//		try{
			for(CouponTradeRequest sirtexCoupon: sirtexOperation.getCouponsTradeRequestList()){
//				if(sirtexCoupon.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && sirtexCoupon.getIndIsCapital().equals(BooleanType.NO.getCode())){
//					if(isSplit!=null && isSplit){
//						if(!marketFactBalance.getBalanceResult().equals(sirtexCoupon.getSuppliedQuantity())){
//							throw new ServiceException(ErrorServiceType.TRADE_OPERATION_SPLIT_QUANTITY);
//						}
//					}
				//ISSUE 088
				
				if (sirtexRegisterTO.getBlIsSplit().equals(false)){
					sirtexCoupon.setSuppliedQuantity(marketFactBalance.getBalanceResult());
				}else{
					if (sirtexCoupon.getPendientDays()!=null){
						if (sirtexCoupon.getPendientDays() < GeneralConstants.DAYS_OF_LIFE_COUPON_SIRTEX 
							//	|| sirtexCoupon.getCouponNumber() == 0
								){
							sirtexCoupon.setSuppliedQuantity(marketFactBalance.getBalanceResult());
						}
					}else{
						sirtexCoupon.setSuppliedQuantity(marketFactBalance.getBalanceResult());
					}
				}
				
//				}
			}
			/*RECOGEMOS EL HECHO DE MERCADO SELECCIONADO*/
			if(!marketFactBalance.getBalanceResult().equals(BigDecimal.ZERO)){
				for(AccountTradeRequest accountOperation: sirtexOperation.getAccountSirtexOperations()){
					/*ASIGNAMOS HECHOS DE MERCADO EN CUENTA VENDEDORA*/
					if(accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
						Date marketDate = null;
						BigDecimal marketRate = null;
						BigDecimal marketPrice = null;
						
						accountOperation.setAccountSirtexMarkectfacts(new ArrayList<AccountTradeMarketFact>());
						/*ASIGNAMOS HECHOS DE MERCADO A CUENTAS VENDEDORAS*/
						for(MarketFactDetailHelpTO mkfDetTO: marketFactBalance.getMarketFacBalances()){
							AccountTradeMarketFact accOperatMarkFct = new AccountTradeMarketFact();
							accOperatMarkFct.setAccountTradeRequest(accountOperation);
							
							/*ASIGNAMOS HECHOS RELEVANTES*/
							accOperatMarkFct.setMarketQuantity(mkfDetTO.getEnteredBalance());
							accOperatMarkFct.setMarketDate(mkfDetTO.getMarketDate());
							accOperatMarkFct.setMarketPrice(mkfDetTO.getMarketPrice());
							accOperatMarkFct.setMarketRate(mkfDetTO.getMarketRate());
							accOperatMarkFct.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							accOperatMarkFct.setRegistryDate(CommonsUtilities.currentDate());
							marketDate = accOperatMarkFct.getMarketDate();
							marketRate = accOperatMarkFct.getMarketRate();
							marketPrice = accOperatMarkFct.getMarketPrice();
							accountOperation.getAccountSirtexMarkectfacts().add(accOperatMarkFct);
						}
						TradeMarketFact tradeMarketFact = sirtexOperation.getSirtexOperationMarketfacts().get(BigDecimal.ZERO.intValue());
						tradeMarketFact.setMarketDate(marketDate);
						tradeMarketFact.setMarketRate(marketRate);
						tradeMarketFact.setMarketPrice(marketPrice);
					}
				}
			}
			JSFUtilities.updateComponent("frmSirtexOperation:sirtexCouponTable");
//		}catch(ServiceException ex){
//			if(ex.getErrorService()!=null){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getExceptionMessage(ex.getMessage()));
//				JSFUtilities.showSimpleValidationDialog();
//			}
//		}
	}
	
	/**
	 * Accept supplied quantity.
	 *
	 * @param sirtexCoupon the sirtex coupon
	 * @throws ServiceException the service exception
	 */
	public void acceptSuppliedQuantity(CouponTradeRequest sirtexCoupon) throws ServiceException{
		SirtexManagementTO sirtexTO = sirtexRegisterTO;
		TradeRequest sirtexOperation = sirtexTO.getSirtexOperation();
		
		if(sirtexCoupon.getSuppliedOk()){
			if(sirtexTO.getBlIsSplit()!=null && sirtexTO.getBlIsSplit()){
				if(sirtexCoupon.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && sirtexCoupon.getIndIsCapital().equals(BooleanType.NO.getCode())){
					for(CouponTradeRequest coupon: sirtexOperation.getCouponsTradeRequestList()){
						coupon.setAcceptedQuantity(coupon.getSuppliedQuantity());
						coupon.setSuppliedOk(Boolean.TRUE);
					}
				}else{
					sirtexCoupon.setAcceptedQuantity(sirtexCoupon.getSuppliedQuantity());
					changeAcceptQuantity(sirtexCoupon);
				}
			}else{
				sirtexCoupon.setAcceptedQuantity(sirtexCoupon.getSuppliedQuantity());
			}
		}else{
			if(sirtexTO.getBlIsSplit()!=null && sirtexTO.getBlIsSplit()){
				if(sirtexCoupon.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && sirtexCoupon.getIndIsCapital().equals(BooleanType.NO.getCode())){
					for(CouponTradeRequest coupon: sirtexOperation.getCouponsTradeRequestList()){
//						if(!(coupon.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && sirtexCoupon.getIndIsCapital().equals(BooleanType.NO.getCode()))){
							coupon.setAcceptedQuantity(null);
							coupon.setSuppliedOk(Boolean.FALSE);
//						}
					}
				}
			}
			sirtexCoupon.setAcceptedQuantity(BigDecimal.ZERO);
			changeAcceptQuantity(sirtexCoupon);
			sirtexCoupon.setAcceptedQuantity(null);
		}
	}
	
	/**
	 * Validate is valid user.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the list
	 */
	private List<String> validateIsValidUser(SirtexOperationResultTO sirtexOperation ) { 
		  List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		  List<String> lstOperationUserResult; 
		  OperationUserTO operationUserTO = new OperationUserTO();  
		  operationUserTO.setOperNumber(sirtexOperation.getIdMechanismOperationRequestPk().toString());
		  operationUserTO.setUserName(sirtexOperation.getRegistryUser());
		  lstOperationUserParam.add(operationUserTO);
		  lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		  return lstOperationUserResult;
		 }
	
	/**
	 * Gets the market fact component ui.
	 *
	 * @return the market fact component ui
	 */
	public MarketFactBalanceHelpTO getMarketFactComponentUI(){
		return sirtexRegisterTO.getMarketFactComponentUI();
	}
	
	/**
	 * Sets the market fact component ui.
	 *
	 * @param marketFactBalanceHelpTO the new market fact component ui
	 */
	public void setMarketFactComponentUI(MarketFactBalanceHelpTO marketFactBalanceHelpTO) {
		sirtexRegisterTO.setMarketFactComponentUI(marketFactBalanceHelpTO);
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
		return lstSourceAccountTo;
	}

	public void setLstSourceAccountTo(List<HolderAccountHelperResultTO> lstSourceAccountTo) {
		this.lstSourceAccountTo = lstSourceAccountTo;
	}

	public HolderAccountHelperResultTO getSourceAccountTo() {
		return sourceAccountTo;
	}

	public void setSourceAccountTo(HolderAccountHelperResultTO sourceAccountTo) {
		this.sourceAccountTo = sourceAccountTo;
	}
	/**
	 * Gets the depositary setup.
	 *
	 * @return the depositary setup
	 */
	public IdepositarySetup getDepositarySetup() {
		return depositarySetup;
	}
	
	/**
	 * Sets the depositary setup.
	 *
	 * @param depositarySetup the new depositary setup
	 */
	public void setDepositarySetup(IdepositarySetup depositarySetup) {
		this.depositarySetup = depositarySetup;
	}

	/**
	 * Gets the sirtex register to.
	 *
	 * @return the sirtex register to
	 */
	public SirtexManagementTO getSirtexRegisterTO() {
		return sirtexRegisterTO;
	}

	/**
	 * Sets the sirtex register to.
	 *
	 * @param sirtexRegisterTO the new sirtex register to
	 */
	public void setSirtexRegisterTO(SirtexManagementTO sirtexRegisterTO) {
		this.sirtexRegisterTO = sirtexRegisterTO;
	}

	/**
	 * Gets the sirtex consult to.
	 *
	 * @return the sirtex consult to
	 */
	public SirtexManagementTO getSirtexConsultTO() {
		return sirtexConsultTO;
	}

	/**
	 * Sets the sirtex consult to.
	 *
	 * @param sirtexConsultTO the new sirtex consult to
	 */
	public void setSirtexConsultTO(SirtexManagementTO sirtexConsultTO) {
		this.sirtexConsultTO = sirtexConsultTO;
	}

	/**
	 * Gets the participant logout.
	 *
	 * @return the participant logout
	 */
	public Long getParticipantLogout() {
		return participantLogout;
	}

	/**
	 * Sets the participant logout.
	 *
	 * @param participantLogout the new participant logout
	 */
	public void setParticipantLogout(Long participantLogout) {
		this.participantLogout = participantLogout;
	}

	/**
	 * Gets the id participant bc.
	 *
	 * @return the id participant bc
	 */
	public Long getIdParticipantBC() {
		return new Long(idParticipantBC);
	}

	/**
	 * Sets the id participant bc.
	 *
	 * @param idParticipantBC the new id participant bc
	 */
	public void setIdParticipantBC(String idParticipantBC) {
		this.idParticipantBC = idParticipantBC;
	}

	/**
	 * Gets the parameter description.
	 *
	 * @return the parameter description
	 */
	public Map<Integer, String> getParameterDescription() {
		return parameterDescription;
	}

	/**
	 * Sets the parameter description.
	 *
	 * @param parameterDescription the parameter description
	 */
	public void setParameterDescription(Map<Integer, String> parameterDescription) {
		this.parameterDescription = parameterDescription;
	}

	public static Integer getDaysoflifecoupon() {
		return daysOfLifeCoupon;
	}

	public Integer getPersonType() {
		return personType;
	}

	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

}