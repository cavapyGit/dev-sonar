package com.pradera.negotiations.sirtex.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SirtexCouponTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SirtexCouponTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The security code. */
	private String securityCode;
	
	/** The security bcb. */
	private String securityBcb;
	
	/** The coupons number. */
	private String couponsNumber;
	
	/** The expired date. */
	private Date expiredDate;
	
	/** The available days. */
	private Integer availableDays;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The stock quantity request. */
	private BigDecimal stockQuantityRequest;
	
	/** The stock quantity acepted. */
	private BigDecimal stockQuantityAcepted;
	
	/** The ind coupon. */
	private Integer indCoupon;
	
	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	/**
	 * Gets the security bcb.
	 *
	 * @return the security bcb
	 */
	public String getSecurityBcb() {
		return securityBcb;
	}
	
	/**
	 * Sets the security bcb.
	 *
	 * @param securityBcb the new security bcb
	 */
	public void setSecurityBcb(String securityBcb) {
		this.securityBcb = securityBcb;
	}
	
	/**
	 * Gets the coupons number.
	 *
	 * @return the coupons number
	 */
	public String getCouponsNumber() {
		return couponsNumber;
	}
	
	/**
	 * Sets the coupons number.
	 *
	 * @param couponsNumber the new coupons number
	 */
	public void setCouponsNumber(String couponsNumber) {
		this.couponsNumber = couponsNumber;
	}
	
	/**
	 * Gets the expired date.
	 *
	 * @return the expired date
	 */
	public Date getExpiredDate() {
		return expiredDate;
	}
	
	/**
	 * Sets the expired date.
	 *
	 * @param expiredDate the new expired date
	 */
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}
	
	/**
	 * Gets the available days.
	 *
	 * @return the available days
	 */
	public Integer getAvailableDays() {
		return availableDays;
	}
	
	/**
	 * Sets the available days.
	 *
	 * @param availableDays the new available days
	 */
	public void setAvailableDays(Integer availableDays) {
		this.availableDays = availableDays;
	}
	
	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	
	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	/**
	 * Gets the stock quantity request.
	 *
	 * @return the stock quantity request
	 */
	public BigDecimal getStockQuantityRequest() {
		return stockQuantityRequest;
	}
	
	/**
	 * Sets the stock quantity request.
	 *
	 * @param stockQuantityRequest the new stock quantity request
	 */
	public void setStockQuantityRequest(BigDecimal stockQuantityRequest) {
		this.stockQuantityRequest = stockQuantityRequest;
	}
	
	/**
	 * Gets the stock quantity acepted.
	 *
	 * @return the stock quantity acepted
	 */
	public BigDecimal getStockQuantityAcepted() {
		return stockQuantityAcepted;
	}
	
	/**
	 * Sets the stock quantity acepted.
	 *
	 * @param stockQuantityAcepted the new stock quantity acepted
	 */
	public void setStockQuantityAcepted(BigDecimal stockQuantityAcepted) {
		this.stockQuantityAcepted = stockQuantityAcepted;
	}
	
	/**
	 * Gets the ind coupon.
	 *
	 * @return the ind coupon
	 */
	public Integer getIndCoupon() {
		return indCoupon;
	}
	
	/**
	 * Sets the ind coupon.
	 *
	 * @param indCoupon the new ind coupon
	 */
	public void setIndCoupon(Integer indCoupon) {
		this.indCoupon = indCoupon;
	}
}