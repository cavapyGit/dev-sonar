package com.pradera.negotiations.sirtex.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class SirtexRenewOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The participant pk. */
	private Long participantPk;
	
	/** The state. */
	private Integer state;
	
	/** The state description. */
	private String stateDescription;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The request type. */
	private Integer requestType;
	
	/**
	 * Gets the participant pk.
	 *
	 * @return the participant pk
	 */
	public Long getParticipantPk() {
		return participantPk;
	}
	
	/**
	 * Sets the participant pk.
	 *
	 * @param participantPk the new participant pk
	 */
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}
	
	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}
}