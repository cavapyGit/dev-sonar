package com.pradera.negotiations.sirtex.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.TradeSettlementRequest;
import com.pradera.model.settlement.type.SirtexTermSettlementType;
import com.pradera.model.settlement.type.TradeSettlementRequestStateType;
import com.pradera.model.settlement.type.TradeSettlementRequestType;
import com.pradera.negotiations.sirtex.facade.RenewSirtexFacade;
import com.pradera.negotiations.sirtex.to.SirtexRenewOperationTO;
import com.pradera.negotiations.sirtex.to.SirtexRenewResultTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RenewSirtexOperationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
public class RenewSirtexOperationBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The sirtex register to. */
	private SirtexRenewManagementTO sirtexRegisterTO;
	
	/** The sirtex consult to. */
	private SirtexRenewManagementTO sirtexConsultTO;
	
	/** The participant logout. */
	private Long participantLogout;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The accounts facade. */
	@EJB private AccountsFacade accountsFacade;
	
	/** The general parameter facade. */
	@EJB private GeneralParametersFacade generalParameterFacade;
	
	/** The renew sirtex facade. */
	@EJB private RenewSirtexFacade renewSirtexFacade;
	
	/** The Constant SIRTEX_RENIEW_VIEW. */
	private static final String SIRTEX_RENIEW_VIEW = "viewRenewSirtex";
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
	
	/** The parameter description. */
	Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	
	/** The parameter list. */
	private List<ParameterTable> parameterList;
	
	/** The id issuer bc. */
	@Inject @Configurable String idIssuerBC;
	
	/** The id participant bc. */
	@Inject @Configurable String idParticipantBC;
	
	/** The id holder account bc. */
	@Inject @Configurable String idHolderAccountBC;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		sirtexConsultTO = new SirtexRenewManagementTO();
		sirtexConsultTO.setRenewOperationTO(new SirtexRenewOperationTO());
		sirtexConsultTO.getRenewOperationTO().setInitialDate(getCurrentSystemDate());
		sirtexConsultTO.getRenewOperationTO().setFinalDate(getCurrentSystemDate());
		
		/**Method to load parameters*/
		try{
			loadSessionPrivilege();
			loadCboParticipantSearch();
			loadCboTradeRequestTypeSearch();
			loadCboTradeRequestStateType();
		}catch(ServiceException e){
		}
		sirtexConsultTO.getRenewOperationTO().setParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
	}
	
	/**
	 * Search renew sirtex operation.
	 */
	public void searchRenewSirtexOperation(){
		SirtexRenewOperationTO renewSirtexTO = sirtexConsultTO.getRenewOperationTO();		
		List<SirtexRenewResultTO> lstRenewResult = null;
		try {
			lstRenewResult = renewSirtexFacade.getSirtexRenewOperation(renewSirtexTO, parameterDescription) ;
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		if(lstRenewResult!=null && !lstRenewResult.isEmpty()){
			sirtexConsultTO.setLstRenewSirtexModel(new GenericDataModel<SirtexRenewResultTO>(lstRenewResult));
		}else{
			sirtexConsultTO.setLstRenewSirtexModel(new GenericDataModel<SirtexRenewResultTO>());
		}
	}

	/**
	 * Load cbo trade request state type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboTradeRequestStateType() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.TRADE_REQUEST_SIRTEX_RENEW_STATE.getCode());
		sirtexConsultTO.setLstTradeSettRequestState(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable parameter: sirtexConsultTO.getLstTradeSettRequestState()){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
		}
	}

	/**
	 * Load session privilege.
	 */
	private void loadSessionPrivilege() {
		// TODO Auto-generated method stub
		UserAccountSession userSession = userInfo.getUserAccountSession();
		if(userSession.isParticipantInstitucion()){
			participantLogout = userSession.getParticipantCode();
		}
		PrivilegeComponent privelegeComponent = new PrivilegeComponent();
		privelegeComponent.setBtnApproveView(true);
		privelegeComponent.setBtnAnnularView(true);
		privelegeComponent.setBtnReview(true);
		privelegeComponent.setBtnRejectView(true);
		privelegeComponent.setBtnConfirmView(true);
		privelegeComponent.setBtnApproveView(true);
		userPrivilege.setPrivilegeComponent(privelegeComponent);
	}
	
	/**
	 * Clean trade settlement request type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cleanTradeSettlementRequestType() throws ServiceException{
		Integer requestType = sirtexRegisterTO.getTradeSettlementRequest().getRequestType();
		newTradeSettlementRequest();
		sirtexRegisterTO.getTradeSettlementRequest().setRequestType(requestType);
	}
	
	/**
	 * New trade settlement request.
	 *
	 * @throws ServiceException the service exception
	 */
	public void newTradeSettlementRequest() throws ServiceException{
		TradeSettlementRequest tradeRequest = new TradeSettlementRequest();
		tradeRequest.setSourceParticipant(new Participant());
		tradeRequest.setTargetParticipant(new Participant());
		tradeRequest.setTradeRequest(new TradeRequest());
		tradeRequest.setRegisterUser(userInfo.getUserAccountSession().getUserName());
		tradeRequest.setRegisterDate(CommonsUtilities.currentDateTime());

		sirtexRegisterTO = new SirtexRenewManagementTO();
		sirtexRegisterTO.setTradeSettlementRequest(tradeRequest);
		sirtexRegisterTO.setModalitySelected(new NegotiationModality());
		sirtexRegisterTO.getTradeSettlementRequest().setSourceParticipant(new Participant());
		sirtexRegisterTO.getTradeSettlementRequest().setRegisterDate(CommonsUtilities.currentDate());
		sirtexRegisterTO.setMechanismSelected(NegotiationMechanismType.SIRTEX.getCode());
		
		loadCboTradeRequestType();
		loadTermSettlementDayType();
		loadNegotiationMechanism();
		loadNegotiationModality(userInfo.getUserAccountSession().getParticipantCode());
		
		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())) {
			Participant filter= new Participant(userInfo.getUserAccountSession().getParticipantCode());
			sirtexRegisterTO.setAllParticipants(accountsFacade.getLisParticipantServiceBean(filter));
			sirtexRegisterTO.getTradeSettlementRequest().getSourceParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
	}
	
	/**
	 * Liquidar inconsistencia.
	 */
	@LoggerAuditWeb
	public void liquidarInconsistencia(){
		try {
			renewSirtexFacade.liquidarInconsistencia(3033L);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load term settlement day type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTermSettlementDayType() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.TRADE_REQUEST_SIRTEX_TERM.getCode());
		sirtexRegisterTO.setLstTermSettlementDays(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable pTable: sirtexRegisterTO.getLstTermSettlementDays()){
			if(pTable.getParameterTablePk().equals(SirtexTermSettlementType.MIN_DAYS.getCode())){
				sirtexRegisterTO.setMinTermDays(pTable.getShortInteger().longValue());
			}else if(pTable.getParameterTablePk().equals(SirtexTermSettlementType.MAX_DAYS.getCode())){
				sirtexRegisterTO.setMaxTermDays(pTable.getShortInteger().longValue());
			}
		}
	}

	/**
	 * Load negotiation mechanism.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadNegotiationMechanism() throws ServiceException{
		// TODO Auto-generated method stub
		sirtexRegisterTO.setMechanismList(renewSirtexFacade.findNegotiationMechanism(NegotiationMechanismType.SIRTEX.getCode()));
	}

	/**
	 * Load cbo trade request type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboTradeRequestType() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.TRADE_REQUEST_SETTLEMENT_REQUEST.getCode());
		sirtexRegisterTO.setTradeSettlementRequestType(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable parameter: sirtexRegisterTO.getTradeSettlementRequestType()){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
		}
		if(sirtexRegisterTO.getTradeSettlementRequest().getRequestType()==null){
			sirtexRegisterTO.getTradeSettlementRequest().setRequestType(TradeSettlementRequestType.RENOVATION.getCode());
		}
	}
	
	/**
	 * Load negotiation modality.
	 *
	 * @param idParticipant the id participant
	 * @throws ServiceException the service exception
	 */
	private void loadNegotiationModality(Long idParticipant) throws ServiceException{
		sirtexRegisterTO.setModalityList(renewSirtexFacade.findNegotiationModality(NegotiationMechanismType.SIRTEX.getCode(), idParticipant));
	}
	
	/**
	 * Load cbo trade request type search.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboTradeRequestTypeSearch() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.TRADE_REQUEST_SETTLEMENT_REQUEST.getCode());
		sirtexConsultTO.setTradeSettlementRequestType(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable parameter: sirtexConsultTO.getTradeSettlementRequestType()){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getDescription());
		}
	}
	
	/**
	 * Load participant by modality.
	 */
	public void loadParticipantByModality() {
		NegotiationModality modality = sirtexRegisterTO.getModalitySelected();
		if(modality!=null && modality.getIdNegotiationModalityPk()!=null){
			Long modalityPk = modality.getIdNegotiationModalityPk();
			Long mechanismPk= NegotiationMechanismType.SIRTEX.getCode();
			try {
				if (Validations.validateIsNull(userInfo.getUserAccountSession().getParticipantCode())) {
					sirtexRegisterTO.setAllParticipants(renewSirtexFacade.getParticipantByModality(mechanismPk, modalityPk));
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		try {
			findTradeRequest();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load modality by participant.
	 */
	public void loadModalityByParticipant(){
		try {
			NegotiationModality modality = sirtexRegisterTO.getModalitySelected();
			Participant participant = sirtexRegisterTO.getTradeSettlementRequest().getSourceParticipant();
			if(modality==null){
				Long mechanismPk= NegotiationMechanismType.SIRTEX.getCode();
				Long participantPk = participant.getIdParticipantPk();
				sirtexRegisterTO.setModalityList(renewSirtexFacade.findNegotiationModality(mechanismPk, participantPk));
			}			
			findTradeRequest();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Find trade request.
	 *
	 * @throws ServiceException the service exception
	 */
	public void findTradeRequest() throws ServiceException{
		TradeSettlementRequest settlementRequest = sirtexRegisterTO.getTradeSettlementRequest();
		Participant participantSrc = settlementRequest.getSourceParticipant();
		Long participantPk = participantSrc.getIdParticipantPk();
		NegotiationModality modality = sirtexRegisterTO.getModalitySelected();
		Long modalityPk = modality.getIdNegotiationModalityPk();
		Long mechanismPk= NegotiationMechanismType.SIRTEX.getCode();
		if(modalityPk!=null && participantPk!=null){
			sirtexRegisterTO.setTradeRequestList(renewSirtexFacade.getTradeRequestByParticipant(participantPk, modalityPk, mechanismPk));
			sirtexRegisterTO.getTradeSettlementRequest().setLstSettlementRequest(null);
		}
	}
	
	/**
	 * Find trade operations.
	 *
	 * @throws ServiceException the service exception
	 */
	public void findTradeOperations() throws ServiceException{
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		sirtexRegisterTO.setIndActive(false);
		TradeRequest tradeRequest = tradeSettRequest.getTradeRequest();
		Long tradeRequestPk = tradeRequest.getIdTradeRequestPk();
		Integer operationPart = ComponentConstant.TERM_PART;
		
		List<SettlementRequest> lstSettlementRequest = renewSirtexFacade.getSettlementRequestList(tradeRequestPk, tradeSettRequest, operationPart);
		
		tradeSettRequest.setLstSettlementRequest(lstSettlementRequest);
	}
	
	/**
	 * Active buy enforced.
	 *
	 * @param settDateOperation the sett date operation
	 */
	public void activeBuyEnforced(SettlementDateOperation settDateOperation){
		
	}
	
	/**
	 * Active renew operation massively.
	 */
	public void activeRenewOperationMassively(){
		for(SettlementRequest settlementRequest: sirtexRegisterTO.getTradeSettlementRequest().getLstSettlementRequest()){
			SettlementDateOperation settlementDateOperation = settlementRequest.getLstSettlementDateOperations().get(0);
			settlementDateOperation.setSelected(sirtexRegisterTO.isIndActive());
			activeRenewOperation(settlementDateOperation);
		}
	}
	
	/**
	 * Active renew operation.
	 *
	 * @param settDateOperation the sett date operation
	 */
	public void activeRenewOperation(SettlementDateOperation settDateOperation){
		if(settDateOperation.isSelected()){
			Date finalWorkDate = renewSirtexFacade.getLastWorkingDate(sirtexRegisterTO.getMaxTermDays().intValue());
			settDateOperation.setSettlementDate(finalWorkDate);
			settDateOperation.setSettlementDays(new Long(CommonsUtilities.getDaysBetween(getCurrentSystemDate(),finalWorkDate)));
			settDateOperation.setStockQuantity(settDateOperation.getSettlementOperation().getStockQuantity());
		}else{
			settDateOperation.setStockQuantity(null);
			settDateOperation.setSettlementDate(null);
			settDateOperation.setSettlementDays(null);
		}
	}
	
	/**
	 * Validate stock quantity.
	 *
	 * @param settDateOperation the sett date operation
	 */
	public void validateStockQuantity(SettlementDateOperation settDateOperation){		
		try{
			BigDecimal stockQuantity = settDateOperation.getStockQuantity();
			SettlementOperation settOperation = settDateOperation.getSettlementOperation();			
			renewSirtexFacade.validateStockQuantityTerm(settOperation, stockQuantity);
		}catch(ServiceException e){
			/**Update current stock quantity*/
			settDateOperation.setStockQuantity(settDateOperation.getSettlementOperation().getInitialStockQuantity());
			
			/**Show message*/
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Validate term days.
	 *
	 * @param settDateOperation the sett date operation
	 */
	public void validateTermDays(SettlementDateOperation settDateOperation){		
		try{
			Long termDays = settDateOperation.getSettlementDays();
			Long maxTermDays = sirtexRegisterTO.getMaxTermDays();			
			renewSirtexFacade.validateTermSettlementDays(termDays.intValue(), maxTermDays);
			settDateOperation.setSettlementDate(CommonsUtilities.addDate(getCurrentSystemDate(), termDays.intValue()));
		}catch(ServiceException e){
			/**Update the final working day*/
			Date finalWorkDate = renewSirtexFacade.getLastWorkingDate(sirtexRegisterTO.getMaxTermDays().intValue());
			settDateOperation.setSettlementDate(finalWorkDate);
			settDateOperation.setSettlementDays(new Long(CommonsUtilities.getDaysBetween(getCurrentSystemDate(),finalWorkDate)));
			
			/**Show message*/
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * ***************METHODS TO HANDLE REDIRECT FROM SEARCH SCREEN TO DIFERENTS OPTIONS LIKE CONFIRM,APPROVE,ANULATE,REVIEE***********.
	 *
	 * @return the string
	 */
	public String approveSirtexAction(){
		SirtexRenewResultTO sirtexOperation = sirtexConsultTO.getRenewSirtesSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.APPROVE.getCode(),sirtexOperation);
	}
	
	/**
	 * Annulate sirtex action.
	 *
	 * @return the string
	 */
	public String annulateSirtexAction(){
		SirtexRenewResultTO sirtexOperation = sirtexConsultTO.getRenewSirtesSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.ANULATE.getCode(),sirtexOperation);
	}
	
	/**
	 * Review sirtex action.
	 *
	 * @return the string
	 */
	public String reviewSirtexAction(){
		SirtexRenewResultTO sirtexOperation = sirtexConsultTO.getRenewSirtesSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REVISE,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.REVIEW.getCode(),sirtexOperation);
	}
	
	/**
	 * Reject sirtex action.
	 *
	 * @return the string
	 */
	public String rejectSirtexAction(){
		SirtexRenewResultTO sirtexOperation = sirtexConsultTO.getRenewSirtesSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REJECT,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.REJECT.getCode(),sirtexOperation);
	}
	
	/**
	 * Confirm sirtex action.
	 *
	 * @return the string
	 */
	public String confirmSirtexAction(){
		
		SirtexRenewResultTO sirtexOperation = sirtexConsultTO.getRenewSirtesSelected();
		List<String> lstOperationUser = validateIsValidUser(sirtexOperation);
	    if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
		    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		    String bodyMessage = PropertiesUtilities.getMessage(
		    		PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
		    showMessageOnDialog(headerMessage, bodyMessage);
		    JSFUtilities.showSimpleValidationDialog();
		    return "";
	    }
		return validateSirtexOperation(ViewOperationsType.CONFIRM.getCode(),sirtexOperation);
	}
	
	/**
	 * View sirtex action.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the string
	 */
	public String viewSirtexAction(SirtexRenewResultTO sirtexOperation){
//		SirtexRenewResultTO sirtexOperation = sirtexConsultTO.getRenewSirtesSelected();
		return validateSirtexOperation(ViewOperationsType.CONSULT.getCode(),sirtexOperation);
	}
	
	/**
	 * Validate sirtex operation.
	 *
	 * @param viewOperationType the view operation type
	 * @param sirtexOperation the sirtex operation
	 * @return the string
	 */
	public String validateSirtexOperation(Integer viewOperationType, SirtexRenewResultTO sirtexOperation){
		Long requestPk = sirtexOperation.getRenewOperationPk();
		TradeSettlementRequest tradeSettlementReq = null;
		Boolean userIsParticipant = userInfo.getUserAccountSession().isParticipantInstitucion();
		Long participantCode = userInfo.getUserAccountSession().getParticipantCode();
		
		List<Integer> statesValidate = new ArrayList<Integer>();
		switch(ViewOperationsType.get(viewOperationType)){
			case APPROVE: case ANULATE:
					statesValidate.add(TradeSettlementRequestStateType.REGISTERED.getCode());break;
			case REVIEW: 
					statesValidate.add(TradeSettlementRequestStateType.APPROVE.getCode());break;
			case CONFIRM: case REJECT:
					statesValidate.add(TradeSettlementRequestStateType.REVIEW.getCode());break;
			default:
				break;
		}
		
		try {
			/**GET THE MECHANISM OPERATION*/
			tradeSettlementReq = renewSirtexFacade.getTradeSettlementRequest(requestPk);
			Integer operationState = tradeSettlementReq.getRequestState();
			
			if(!statesValidate.isEmpty() && !statesValidate.contains(operationState)){
				throw new ServiceException(ErrorServiceType.TEMPLATE_INCORRECT_STATE);
			}
			
			setViewOperationType(viewOperationType);

			/**SI USUARIO ES PARTICIPANTE*/
			if(userIsParticipant){
				Long participantPermited = null;
				switch(TradeSettlementRequestStateType.get(operationState)){
					case REGISTERED:
						participantPermited = tradeSettlementReq.getSourceParticipant().getIdParticipantPk();break;
					case APPROVE: case REVIEW:
						participantPermited = tradeSettlementReq.getTargetParticipant().getIdParticipantPk();break;
					default:
						break;
				}
				
				if(participantPermited!=null && !participantPermited.equals(participantCode)){
					throw new ServiceException(ErrorServiceType.TRADE_OPERATION_PARTICIPANT_INCORRECT_ACTION);
				}
			}

			sirtexRegisterTO = new SirtexRenewManagementTO();			
			sirtexRegisterTO.setTradeSettlementRequest(tradeSettlementReq);
			/**Load parameters to show in view*/
			loadCboTradeRequestType();
			
			return SIRTEX_RENIEW_VIEW;
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
		return "";
	}
	
	/**
	 * ***************END -- METHODS TO HANDLE REDIRECT FROM SEARCH SCREEN TO DIFERENTS OPTIONS LIKE CONFIRM,APPROVE,ANULATE,REVIEE***********.
	 */
	
	public void beforeSaveNewMechanismOperation(){
		String messageProperties = "";
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		Boolean haveValidation = Boolean.TRUE;
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER: 	messageProperties = PropertiesConstants.RENEW_SIRTEX_REGISTER_CONFIRM;
							haveValidation 	  = validateBeforeRegister();break;
			case APPROVE:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_APPROVE_CONFIRM;break;
			case REVIEW:	messageProperties = PropertiesConstants.SIRTEX_OPERATION_REVIEW_CONFIRM;break;
			case CONFIRM:	messageProperties = PropertiesConstants.RENEW_SIRTEX_CONFIRM_CONFIRM;break;
			case ANULATE:	messageProperties = PropertiesConstants.RENEW_SIRTEX_ANNULATE_CONFIRM;break;
			case REJECT:	messageProperties = PropertiesConstants.RENEW_SIRTEX_REJECT_CONFIRM;break;
			default:break;
		}
		
		/**validation about if sirtex request has been selected*/
		if(haveValidation){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(messageProperties,new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk()}));
			JSFUtilities.executeJavascriptFunction("PF('cnfwRenewSirtex').show()");
		}
	}
	
	/**
	 * Validate before register.
	 *
	 * @return the boolean
	 */
	public Boolean validateBeforeRegister(){
		List<SettlementRequest> lstSettlementRequest = sirtexRegisterTO.getTradeSettlementRequest().getLstSettlementRequest();
		if(lstSettlementRequest!=null){
			for(SettlementRequest settlementRequest: lstSettlementRequest){
				for(SettlementDateOperation settDateOperation: settlementRequest.getLstSettlementDateOperations()){
					if(settDateOperation.isSelected()){
						return Boolean.TRUE;
					}
				}
			}
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities.getMessage("sirtex.settlement.anticipated.validation"));
		JSFUtilities.showSimpleValidationDialog();																																																																																																																																																																																																																																																																																																																																																																									
		return Boolean.FALSE;
	}
	
	/**
	 * Save trade settlement request action.
	 */
	@LoggerAuditWeb
	public void saveTradeSettlementRequestAction() {
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		try{
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	saveTradeSettlementRequest();break;
				case CONFIRM:  	confirmTradeSettlementRequest();break;
				case APPROVE: 	approveTradeSettlementRequest();break;
				case REVIEW: 	reviewTradeSettlementRequest();break;
				case ANULATE: 	anulateTradeSettlementRequest();break;
				case REJECT: 	rejectTradeSettlementRequest();break;
			default:
				break;
			}
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),
																			new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk()}));
			}
		}
		
		if(sirtexConsultTO.getLstRenewSirtexModel()!=null){
			searchRenewSirtexOperation();
		}
		
		JSFUtilities.showSimpleEndTransactionDialog("searchRenewSirtex");
	}
	
	/**
	 * Save trade settlement request.
	 *
	 * @throws ServiceException the service exception
	 */
	private void saveTradeSettlementRequest() throws ServiceException{
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		tradeSettRequest = renewSirtexFacade.saveTradeSettlementRequest(tradeSettRequest);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.RENEW_SIRTEX_REGISTER_CONFIRM_OK,
															new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk().toString()}));
	}
	
	/**
	 * Approve trade settlement request.
	 *
	 * @throws ServiceException the service exception
	 */
	private void approveTradeSettlementRequest() throws ServiceException{
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		tradeSettRequest = renewSirtexFacade.approveTradeSettlementRequest(tradeSettRequest);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.RENEW_SIRTEX_APPROVE_CONFIRM_OK,
															new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk().toString()}));
	}
	
	/**
	 * Anulate trade settlement request.
	 *
	 * @throws ServiceException the service exception
	 */
	private void anulateTradeSettlementRequest() throws ServiceException{
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		tradeSettRequest = renewSirtexFacade.annulateTradeSettlementRequest(tradeSettRequest);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.RENEW_SIRTEX_ANNULATE_CONFIRM_OK,
															new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk().toString()}));
	}
	
	/**
	 * Review trade settlement request.
	 *
	 * @throws ServiceException the service exception
	 */
	private void reviewTradeSettlementRequest() throws ServiceException{
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		tradeSettRequest = renewSirtexFacade.reviewTradeSettlementRequest(tradeSettRequest);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.SIRTEX_OPERATION_REVIEW_CONFIRM_OK,
															new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk().toString()}));
	}
	
	/**
	 * Reject trade settlement request.
	 *
	 * @throws ServiceException the service exception
	 */
	private void rejectTradeSettlementRequest() throws ServiceException{
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		tradeSettRequest = renewSirtexFacade.rejectTradeSettlementRequest(tradeSettRequest);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.RENEW_SIRTEX_REJECT_CONFIRM_OK,
															new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk().toString()}));
	}
	
	/**
	 * Confirm trade settlement request.
	 *
	 * @throws ServiceException the service exception
	 */
	private void confirmTradeSettlementRequest() throws ServiceException{
		TradeSettlementRequest tradeSettRequest = sirtexRegisterTO.getTradeSettlementRequest();
		tradeSettRequest = renewSirtexFacade.confirmTradeSettlementRequest(tradeSettRequest);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.RENEW_SIRTEX_CONFIRM_CONFIRM_OK,
															new Object[]{tradeSettRequest.getIdTradeSettlementRequestPk().toString()}));
	}
	
	/**
	 * Load cbo participant search.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboParticipantSearch() throws ServiceException{
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		sirtexConsultTO.setAllParticipants(accountsFacade.getLisParticipantServiceBean(participantTO));
	}
	
	/**
	 * Validate is valid user.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the list
	 */
	private List<String> validateIsValidUser(SirtexRenewResultTO sirtexOperation ) { 
		  List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		  List<String> lstOperationUserResult; 
		  OperationUserTO operationUserTO = new OperationUserTO();  
		  operationUserTO.setOperNumber(sirtexOperation.getRenewOperationPk().toString());
		  operationUserTO.setUserName(sirtexOperation.getRegistryUser());
		  lstOperationUserParam.add(operationUserTO);
		  lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		  return lstOperationUserResult;
	}
	
	/**
	 * Gets the sirtex register to.
	 *
	 * @return the sirtex register to
	 */
	public SirtexRenewManagementTO getSirtexRegisterTO() {
		return sirtexRegisterTO;
	}

	/**
	 * Sets the sirtex register to.
	 *
	 * @param sirtexRegisterTO the new sirtex register to
	 */
	public void setSirtexRegisterTO(SirtexRenewManagementTO sirtexRegisterTO) {
		this.sirtexRegisterTO = sirtexRegisterTO;
	}

	/**
	 * Gets the sirtex consult to.
	 *
	 * @return the sirtex consult to
	 */
	public SirtexRenewManagementTO getSirtexConsultTO() {
		return sirtexConsultTO;
	}

	/**
	 * Sets the sirtex consult to.
	 *
	 * @param sirtexConsultTO the new sirtex consult to
	 */
	public void setSirtexConsultTO(SirtexRenewManagementTO sirtexConsultTO) {
		this.sirtexConsultTO = sirtexConsultTO;
	}

	/**
	 * Gets the participant logout.
	 *
	 * @return the participant logout
	 */
	public Long getParticipantLogout() {
		return participantLogout;
	}

	/**
	 * Sets the participant logout.
	 *
	 * @param participantLogout the new participant logout
	 */
	public void setParticipantLogout(Long participantLogout) {
		this.participantLogout = participantLogout;
	}

	/**
	 * Gets the parameter list.
	 *
	 * @return the parameter list
	 */
	public List<ParameterTable> getParameterList() {
		return parameterList;
	}

	/**
	 * Sets the parameter list.
	 *
	 * @param parameterList the new parameter list
	 */
	public void setParameterList(List<ParameterTable> parameterList) {
		this.parameterList = parameterList;
	}
}