package com.pradera.negotiations.sirtex.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.batch.SalePurchaseUpdateBatch;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.core.component.helperui.to.HolderAccountSearchTO;
import com.pradera.core.component.operation.to.MechanismModalityTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.AccountTradeMarketFact;
import com.pradera.model.negotiation.AccountTradeRequest;
import com.pradera.model.negotiation.CouponTradeRequest;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.ParticipantOperation;
import com.pradera.model.negotiation.TradeMarketFact;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.InChargeNegotiationType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.OtcOperationDateSearchType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.negotiations.otcoperations.service.OtcOperationServiceBean;
import com.pradera.negotiations.sirtex.to.SirtexOperationResultTO;
import com.pradera.negotiations.sirtex.view.SirtexOperationTO;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SirtexOperationService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SirtexOperationService extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sale purchase update batch. */
	@Inject @BatchProcess(name="SalePurchaseUpdateBatch")
	private SalePurchaseUpdateBatch salePurchaseUpdateBatch;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/** The sirtex settlement service. */
	@EJB private OtcAndSirtexSettlementService sirtexSettlementService;
	
	/** The otc operation service bean. */
	@EJB private OtcOperationServiceBean otcOperationServiceBean;
	
	/** The settlement service. */
	@EJB private SettlementProcessService settlementService;
	
	/** The settlement service. */
	@EJB private HolderQueryServiceBean holderQueryServiceBean;
	
	/**
	 * Find holder from participant.
	 *
	 * @param participantPK the participant pk
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderFromParticipant(Long participantPK) throws ServiceException{
		StringBuilder holderSQL = new StringBuilder();
		holderSQL.append("SELECT part.holder 								");
		holderSQL.append("FROM Participant part 							");
		holderSQL.append("WHERE part.idParticipantPk = :participantPK		");
		Query queryHolderPk = em.createQuery(holderSQL.toString());
		queryHolderPk.setParameter("participantPK", participantPK);
		return (Holder) queryHolderPk.getSingleResult(); 
	}
	
	/**
	 * find participant by id_participant_pk
	 * */
	public Participant getParticipantById(Long participantPK) throws ServiceException{
		StringBuilder participant = new StringBuilder();
		participant.append("SELECT part								        ");
		participant.append("FROM Participant part 							");
		participant.append("WHERE part.idParticipantPk = :participantPK		");
		Query queryPart = em.createQuery(participant.toString());
		queryPart.setParameter("participantPK", participantPK);
		return (Participant) queryPart.getSingleResult(); 
	}
	
	/**
	 * NEGOCIACION POR CUENTA PROPIA (1-SI, 2-NO) CUI - PARTICIPANTE - issue 1098
	 */
	public ParameterTable getParameterOwnAccount() {
		Integer rutePt=2449;
		ParameterTable objParameterTable=new ParameterTable();
        objParameterTable = find(ParameterTable.class, rutePt);
		return objParameterTable; 
	}
	
	/**
	 * find holder propio por id participante 
	 */
	public Holder checkParticipant(Long participantPK) throws ServiceException{
		StringBuilder holder = new StringBuilder();
		holder.append("SELECT p.holder.idHolderPk						");
		holder.append("FROM Participant p							");
		holder.append(" JOIN p.holder h 					");
		holder.append("WHERE p.idParticipantPk = :participantPK	");
		holder.append("  AND p.accountType = 26		            ");//agencias de bolsa - participant
//		holder.append("  AND h.economicActivity = 43		    ");//agencias de bolsa - holder
		Query queryHolder = em.createQuery(holder.toString());
		queryHolder.setParameter("participantPK", participantPK);
		
		Long objects = (Long) queryHolder.getSingleResult();
		Holder hObject = new Holder();
		hObject.setIdHolderPk((Long)objects);
		
		return hObject;
	}
	
	/**
	 * Gets the security negotiation mechanism.
	 *
	 * @param securityCode the security code
	 * @param idModality the id modality
	 * @return the security negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	public SecurityNegotiationMechanism getSecurityNegotiationMechanism(String securityCode, Long idModality) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select sec From SecurityNegotiationMechanism sec							");
		querySQL.append("Where 	sec.securityNegotationState = :oneParam 	And 					");
		querySQL.append("		sec.security.idSecurityCodePk = :securityPk	And						");
		querySQL.append("		sec.mechanismModality.id.idNegotiationMechanismPk = :sirtexPk	And	");
		querySQL.append("		sec.mechanismModality.id.idNegotiationModalityPk  = :modalitPk		");
		querySQL.append("		order by sec.idSecurityNegMechPk desc ");
		Query queryHolderPk = em.createQuery(querySQL.toString());
		queryHolderPk.setParameter("oneParam", BooleanType.YES.getCode());
		queryHolderPk.setParameter("securityPk", securityCode);
		queryHolderPk.setParameter("sirtexPk", NegotiationMechanismType.SIRTEX.getCode());
		queryHolderPk.setParameter("modalitPk", idModality);
		
		try{
			List<SecurityNegotiationMechanism> list = queryHolderPk.getResultList();
			if(list!=null&&list.size()>0)
				return list.get(0);
			else
				return null;
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the mechanism modality list service bean.
	 *
	 * @param mechanismModalityTO the mechanism modality to
	 * @param role the role
	 * @return the mechanism modality list service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismModality> getMechanismModalityList(MechanismModalityTO mechanismModalityTO, Integer role) throws ServiceException {
		StringBuilder selctSql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		StringBuilder orderBySql = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		selctSql.append("Select Distinct mo From MechanismModality mo						");
		selctSql.append("Inner Join Fetch mo.negotiationModality negmod						");
		selctSql.append("Inner Join Fetch mo.participantMechanisms part						");
		selctSql.append("Inner Join Fetch part.participant participante						");
		
		whereSql.append("Where 1 = 1														");
		if (mechanismModalityTO.getIdNegotiationMechanismPK() != null) {
			whereSql.append("And mo.id.idNegotiationMechanismPk = :negMechanismId			");
			parameters.put("negMechanismId",mechanismModalityTO.getIdNegotiationMechanismPK());
		}
		if(mechanismModalityTO.getIdNegotiationModalityPkList()!=null){
			whereSql.append("And mo.id.idNegotiationModalityPk IN :negModalityId			");
			parameters.put("negModalityId",mechanismModalityTO.getIdNegotiationModalityPkList());
		}
		if (mechanismModalityTO.getNegotiationModalityState() != null) {
			whereSql.append("And mo.stateMechanismModality = :stateMechanismMod				");
			parameters.put("stateMechanismMod",mechanismModalityTO.getNegotiationModalityState());
		}
		if(role!=null){
			if(role.equals(ParticipantRoleType.SELL.getCode())){
				whereSql.append("And part.indSaleRole = :role							");
			}else if(role.equals(ParticipantRoleType.BUY.getCode())){
				whereSql.append("And part.indPurchaseRole = :role								");
			}
			parameters.put("role",BooleanType.YES.getCode());
		}
		if (mechanismModalityTO.getIdParticipantPk() != null) {
			whereSql.append("And participante.idParticipantPk = :participantCode			");
			parameters.put("participantCode",mechanismModalityTO.getIdParticipantPk());
		}
		whereSql.append("And part.stateParticipantMechanism = :statePartMechanism			");
		whereSql.append("And participante.state = :statePart								");
		
		parameters.put("statePartMechanism",ParticipantMechanismStateType.REGISTERED.getCode());
		parameters.put("statePart",ParticipantStateType.REGISTERED.getCode());

		orderBySql.append("Order By negmod.modalityName Asc									");

		List<MechanismModality> mechanismModalityList = findListByQueryString(selctSql.toString().concat(whereSql.toString()).concat(orderBySql.toString()), parameters);
		return mechanismModalityList;
	}
	
	/**
	 * Find balances from participant.
	 *
	 * @param participant the participant
	 * @param security the security
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> findBalancesFromParticipant(Participant participant, Security security) throws ServiceException{
		Long participantPK = participant.getIdParticipantPk();
		
		/*BUSCAMOS CUENTA DE VALORES PROPIAS DEL PARTICIPANTE*/
		StringBuilder accountsSQL = new StringBuilder();
		accountsSQL.append("SELECT ha.idHolderAccountPk FROM HolderAccount 		ha			");
		accountsSQL.append("INNER JOIN ha.participant 							par 		");
		accountsSQL.append("INNER JOIN ha.holderAccountDetails 					had			");
		accountsSQL.append("WHERE par.idParticipantPk = :participantId			AND			");
		accountsSQL.append("	  par.holder.idHolderPk = had.holder.idHolderPk				");
		Query query = em.createQuery(accountsSQL.toString());
		query.setParameter("participantId", participantPK);

		try{
			/*ALMACENAMOS PK DE CUENTAS PROPIAS DEL PARTICIPANTE*/
			List<Long> holderAccounts = query.getResultList();
			
			if(holderAccounts==null || holderAccounts.isEmpty()){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_WITHOUT_ACCOUNTS, new Object[]{participant.getIdParticipantPk()});
			}
				
			
			/*BUSCAMOS SALDOS RELACIONADOS A LAS CUENTAS ENCONTRADAS*/
			StringBuilder balancesSQL = new StringBuilder();
			balancesSQL.append("SELECT hab FROM HolderAccountBalance hab								");
			balancesSQL.append("WHERE hab.participant.idParticipantPk 	  = :participantID		 AND	");
			balancesSQL.append("	  hab.holderAccount.idHolderAccountPk IN (:holderAccountID)	 AND	");
			balancesSQL.append("	  hab.security.idSecurityCodePk 	  = :securityID 		 AND	");
			balancesSQL.append("	  hab.availableBalance 	  			  > :zeroValue 		 			");
			
			query = em.createQuery(balancesSQL.toString());
			query.setParameter("participantID", participantPK);
			query.setParameter("securityID", security.getIdSecurityCodePk());
			query.setParameter("holderAccountID", holderAccounts);
			query.setParameter("zeroValue", BigDecimal.ZERO);
				
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<HolderAccountBalance>();
		}
	}
	
	/**
	 * Find balances from participant.
	 *
	 * @param participant the participant
	 * @param security the security
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> findBalancesFromHolderAccount(Participant participant, Security security, HolderAccount holderAccount) throws ServiceException{
		Long participantPK = participant.getIdParticipantPk();
		
		try{
			List<Long> holderAccounts = new ArrayList<Long>();
			holderAccounts.add(holderAccount.getIdHolderAccountPk());
					
			if(holderAccounts==null || holderAccounts.isEmpty()){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_WITHOUT_ACCOUNTS, new Object[]{participant.getIdParticipantPk()});
			}
				
			/*BUSCAMOS SALDOS RELACIONADOS A LAS CUENTAS ENCONTRADAS*/
			StringBuilder balancesSQL = new StringBuilder();
			balancesSQL.append("SELECT hab FROM HolderAccountBalance hab								");
			balancesSQL.append("WHERE hab.participant.idParticipantPk 	  = :participantID		 AND	");
			balancesSQL.append("	  hab.holderAccount.idHolderAccountPk IN (:holderAccountID)	 AND	");
			balancesSQL.append("	  hab.security.idSecurityCodePk 	  = :securityID 		 AND	");
			balancesSQL.append("	  hab.availableBalance 	  			  > :zeroValue 		 			");
			
			Query query = em.createQuery(balancesSQL.toString());
			query.setParameter("participantID", participantPK);
			query.setParameter("securityID", security.getIdSecurityCodePk());
			query.setParameter("holderAccountID", holderAccounts);
			query.setParameter("zeroValue", BigDecimal.ZERO);
				
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<HolderAccountBalance>();
		}
	}
	
	/**
	 * Find holder accounts from balances.
	 *
	 * @param participant the participant
	 * @param security the security
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccountsFromBalances(Participant participant, Security security) throws ServiceException{
		Long participantPK = participant.getIdParticipantPk();
		
		/*BUSCAMOS CUENTA DE VALORES PROPIAS DEL PARTICIPANTE*/
		StringBuilder accountsSQL = new StringBuilder();
		accountsSQL.append("SELECT ha.idHolderAccountPk FROM HolderAccount 		ha			");
		accountsSQL.append("INNER JOIN ha.participant 							par 		");
		accountsSQL.append("INNER JOIN ha.holderAccountDetails 					had			");
		accountsSQL.append("WHERE par.idParticipantPk = :participantId			AND			");
		accountsSQL.append("	  par.holder.idHolderPk = had.holder.idHolderPk				");
		Query query = em.createQuery(accountsSQL.toString());
		query.setParameter("participantId", participantPK);

		try{
			StringBuilder balancesSQL = new StringBuilder();
			balancesSQL.append("SELECT ha FROM HolderAccount 								ha			");
			balancesSQL.append("INNER JOIN ha.participant 									par 		");
			balancesSQL.append("INNER JOIN fetch ha.holderAccountDetails 					had			");
			balancesSQL.append("INNER JOIN fetch had.holder 								ho			");
			balancesSQL.append("INNER JOIN ha.holderAccountBalances 						hab			");
			balancesSQL.append("WHERE hab.participant.idParticipantPk 	  = :participantID		 AND	");
			balancesSQL.append("	  hab.security.idSecurityCodePk 	  = :securityID 		 AND	");
			balancesSQL.append("	  hab.availableBalance 	  			  > :zeroValue 		 	 AND	");
			balancesSQL.append("	  par.holder.idHolderPk = ho.idHolderPk						 		");	
			
			query = em.createQuery(balancesSQL.toString());
			query.setParameter("participantID", participantPK);
			query.setParameter("securityID", security.getIdSecurityCodePk());
			query.setParameter("zeroValue", BigDecimal.ZERO);
		
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<HolderAccount>();
		}
	}
	
	/**
	 * Find holder accounts from participant.
	 *
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccountsFromParticipant(Participant participant) throws ServiceException{
		Long participantPK = participant.getIdParticipantPk();
		
		/*BUSCAMOS CUENTA DE VALORES PROPIAS DEL PARTICIPANTE*/
		StringBuilder accountsSQL = new StringBuilder();
		accountsSQL.append("SELECT ha FROM HolderAccount 								ha			");
		accountsSQL.append("INNER JOIN ha.participant 									par 		");
		accountsSQL.append("INNER JOIN fetch ha.holderAccountDetails 					had			");
		accountsSQL.append("INNER JOIN fetch had.holder 								ho			");
		accountsSQL.append("WHERE par.idParticipantPk = :participantId					AND			");
		accountsSQL.append("	  par.holder.idHolderPk = had.holder.idHolderPk			AND			");
		accountsSQL.append("	  ha.stateAccount = :registerState									");
		Query query = em.createQuery(accountsSQL.toString());
		query.setParameter("participantId", participantPK);
		query.setParameter("registerState", HolderAccountStatusType.ACTIVE.getCode());
		return query.getResultList();
	}
	
	// get all Holder's accounts
	
	/**
	 * Search holder account.
	 *
	 * @param holder the holder
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> searchHolderAccount(Holder holder) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Building the query
		sbQuery.append("Select ");
		sbQuery.append("DISTINCT ha.idHolderAccountPk, ");
		sbQuery.append("ha.accountNumber, ");
		sbQuery.append("ha.participant.mnemonic, ");
		sbQuery.append("ha.participant.idParticipantPk, ");
		sbQuery.append("ha.stateAccount, ");	
		sbQuery.append("ha.participant.description, ");
		sbQuery.append("ha.alternateCode, ");
		sbQuery.append("had.holder.fullName, ");
		sbQuery.append("ha.participant.issuer.idIssuerPk, ");
		sbQuery.append("ha.accountType ");
		sbQuery.append("from HolderAccount ha ");
		sbQuery.append(" join ha.holderAccountDetails had ");
		sbQuery.append(" where HAD.holder.idHolderPk=:idHolderPkPrm");
		sbQuery.append(" and  ha.stateAccount=:stateAccount");
		//Setting the query
		Query query = em.createQuery(sbQuery.toString());
		//Setting parameters
		query.setParameter("idHolderPkPrm", holder.getIdHolderPk());
		query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
		//Getting the result
		List<Object> oList = query.getResultList();
		List<HolderAccount> ha = new ArrayList<HolderAccount>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating HolderAccount
			HolderAccount haObject = new HolderAccount();
			haObject.setIdHolderAccountPk((Long)objects[0]);
			haObject.setAccountNumber((Integer) objects[1]);
					
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk((String) objects[8]);
			
			//Creating Participant
			Participant participant = new Participant();
			participant.setIssuer(issuer);
			participant.setMnemonic((String) objects[2]);
			participant.setIdParticipantPk((Long) objects[3]);	
			participant.setDescription((String) objects[5]);
			haObject.setStateAccount((Integer) objects[4]);			
			haObject.setParticipant(participant);
			haObject.setAlternateCode((String)objects[6]);
			haObject.setFullName((String)objects[7]);
			haObject.setAccountType((Integer) objects[9]);
			ha.add(haObject);
		}
		return ha;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccounts(HolderAccountSearchTO holderAccountSearchTO) {
		Map <String,Object> parameter=new HashMap<>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct hadd.holderAccount.idHolderAccountPk");
		sbQuery.append("	FROM HolderAccountDetail hadd");
		sbQuery.append("	WHERE ");
		sbQuery.append("	hadd.holderAccount.participant.idParticipantPk = :idParticipantPk");
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder())
				&& Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder().getIdHolderPk()))
			sbQuery.append("	AND hadd.holder.idHolderPk = :idHolderPk");		
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getAccountType()))
			sbQuery.append("	AND hadd.holderAccount.accountType = :accountType");
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getState()))
			sbQuery.append("	AND hadd.holderAccount.stateAccount = :stateAccount");
		parameter.put("idParticipantPk", holderAccountSearchTO.getParticipant().getIdParticipantPk());
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder())
				&& Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder().getIdHolderPk())){
			parameter.put("idHolderPk", holderAccountSearchTO.getHolder().getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getAccountType())){
			parameter.put("accountType", holderAccountSearchTO.getAccountType());
		}
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getState())){
			parameter.put("stateAccount", holderAccountSearchTO.getState());
		}
			StringBuilder mainQuery = new StringBuilder();
			mainQuery.append("	SELECT distinct ha");
			mainQuery.append("	FROM HolderAccount ha");
			mainQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			mainQuery.append("	INNER JOIN FETCH had.holder");
			mainQuery.append("	WHERE ha.idHolderAccountPk in (");
			mainQuery.append(sbQuery.toString() + " ) ");
			mainQuery.append("	ORDER BY ha.idHolderAccountPk DESC");
			return (List<HolderAccount>)findListByQueryString(mainQuery.toString(),parameter);
	}
	
	/**
	 * Number of market fact.
	 *
	 * @param participantId the participant id
	 * @param accountId the account id
	 * @param idSecurityCode the id security code
	 * @return the integer
	 */
	public Integer numberOfMarketFact(Long participantId, Long accountId, String idSecurityCode) {
		StringBuilder accountsSQL = new StringBuilder();
		accountsSQL.append("SELECT count(hmb) FROM HolderMarketFactBalance hmb						");
		accountsSQL.append("WHERE  hmb.participant.idParticipantPk 	   = :idParticipant		and		");
		accountsSQL.append("	   hmb.holderAccount.idHolderAccountPk = :idAccount			and		");
		accountsSQL.append("	   hmb.security.idSecurityCodePk 	   = :idSecurityCode	and		");
		accountsSQL.append("	   hmb.totalBalance > :zeroParam									");
		
		Query query = em.createQuery(accountsSQL.toString());
		query.setParameter("idParticipant", participantId);
		query.setParameter("idAccount", accountId);
		query.setParameter("idSecurityCode", idSecurityCode);
		query.setParameter("zeroParam", BigDecimal.ZERO);
		
		try{
			return ((Long) query.getSingleResult()).intValue();
		}catch(NoResultException e){
			return BigDecimal.ZERO.intValue();
		}
	}
	
	/**
	 * Find indicator SIRTEX negotiation
	 *
	 * @param holder_pk 
	 * @return ind_sirtex_neg state
	 * @throws ServiceException the service exception
	 */
	public Integer findIndSirtexNeg (Long id_holder_pk) {
		StringBuilder querySql = new StringBuilder();
		querySql.append(" SELECT h.IND_SIRTEX_NEG FROM HOLDER h WHERE h.ID_HOLDER_PK = :idHolderPk ");
		
		Query query = em.createNativeQuery(querySql.toString());
//		Query query = em.createQuery(accountsSQL.toString());
		query.setParameter("idHolderPk", id_holder_pk);
		
//		ya sea 0 o null quiere decir que no negocia SIRTEX
		try{
			return ((BigDecimal) query.getSingleResult()).intValue();
		}catch(NoResultException e){
			return BigDecimal.ZERO.intValue();
		}
	}	
	
	/**
	 * Find market fact balance.
	 *
	 * @param participantId the participant id
	 * @param accountId the account id
	 * @param idSecurityCode the id security code
	 * @return the holder market fact balance
	 */
	public HolderMarketFactBalance findMarketFactBalance(Long participantId, Long accountId, String idSecurityCode) {
		StringBuilder accountsSQL = new StringBuilder();
		accountsSQL.append("SELECT hmb FROM HolderMarketFactBalance hmb						");
		accountsSQL.append("WHERE  hmb.participant.idParticipantPk 	   = :idParticipant		and		");
		accountsSQL.append("	   hmb.holderAccount.idHolderAccountPk = :idAccount			and		");
		accountsSQL.append("	   hmb.security.idSecurityCodePk 	   = :idSecurityCode	and		");
		accountsSQL.append("	   hmb.totalBalance > :zeroParam									");
		
		Query query = em.createQuery(accountsSQL.toString());
		query.setParameter("idParticipant", participantId);
		query.setParameter("idAccount", accountId);
		query.setParameter("idSecurityCode", idSecurityCode);
		query.setParameter("zeroParam", BigDecimal.ZERO);
		
		try{
			return (HolderMarketFactBalance) (query.getSingleResult());
		}catch(NoResultException e){
			return null;
		}catch(NonUniqueResultException ex){
			return null;
		}
	}
	
	/**
	 * Register mechanism operation.
	 *
	 * @param sirtexManagement the sirtex management
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation registerMechanismOperation(MechanismOperation sirtexManagement) throws ServiceException{
		MechanismOperation mechanismOperation = sirtexManagement;
		MechanismModality mechanismModality = mechanismOperation.getMechanisnModality();
		NegotiationModality negotiationModality = mechanismModality.getNegotiationModality();
		
		/**INDICADORES NOT NULL, DEBEN RECIBIR AL MENOS 1 VALOR */
		mechanismOperation.setIndCashSettlementExchange(BooleanType.NO.getCode());
		mechanismOperation.setCashSettlementCurrency(mechanismOperation.getCurrency());
		mechanismOperation.setCashSettlementAmount(mechanismOperation.getRealCashAmount());
		mechanismOperation.setCashSettlementPrice(mechanismOperation.getRealCashPrice());
		mechanismOperation.setIndPrincipalGuarantee(negotiationModality.getIndPrincipalGuarantee());
		mechanismOperation.setIndMarginGuarantee(negotiationModality.getIndMarginGuarantee());
		mechanismOperation.setIndReportingBalance(negotiationModality.getIndReportingBalance());
		mechanismOperation.setIndTermSettlement(negotiationModality.getIndTermSettlement());
		mechanismOperation.setIndCashStockBlock(negotiationModality.getIndCashStockBlock());
		mechanismOperation.setIndTermStockBlock(negotiationModality.getIndTermStockBlock());
		mechanismOperation.setIndPrimaryPlacement(negotiationModality.getIndPrimaryPlacement());
		mechanismOperation.setIndTermSettlementExchange(mechanismModality.getIndTermCurrencyExchange());

		if(mechanismOperation.getIndTermSettlement().equals(BooleanType.YES.getCode())){
			mechanismOperation.setIndTermSettlementExchange(BooleanType.NO.getCode());
			mechanismOperation.setTermSettlementCurrency(mechanismOperation.getCurrency());
			if(mechanismOperation.getTermPrice()!=null){
				BigDecimal stocQuantity = mechanismOperation.getStockQuantity();
				mechanismOperation.setTermSettlementAmount(NegotiationUtils.getSettlementAmount(mechanismOperation.getTermPrice(), stocQuantity));
				mechanismOperation.setTermSettlementPrice(mechanismOperation.getTermPrice());
			}else{
				mechanismOperation.setTermSettlementAmount(BigDecimal.ZERO);
				mechanismOperation.setTermSettlementPrice(BigDecimal.ZERO);
			}
		}

		mechanismOperation.setNominalValue(mechanismOperation.getSecurities().getCurrentNominalValue());
		mechanismOperation.setOperationNumber(otcOperationServiceBean.getCurrentOperationNumber(mechanismOperation));
		
		/**CREAMOS LA OPERACION*/
		create(mechanismOperation);
		
		if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			/**SI ES OPERACION DIFERENTE A REPORTO, REGISTRAMOS HECHOS DE MERCADO A NIVEL DE OPERACION*/
			if(negotiationModality.getIndTermSettlement().equals(BooleanType.NO.getCode())){
				List<MechanismOperationMarketFact> marketFatOperationList = mechanismOperation.getMechanismOperationMarketFacts();
				if(marketFatOperationList!=null && !marketFatOperationList.isEmpty()){
					MechanismOperationMarketFact marketFact = mechanismOperation.getMechanismOperationMarketFacts().get(0);
					create(marketFact);
				}
			}
		}
		
		/**CREAMOS LAS CUENTAS ASIGNADAS*/
		/**Config SettlementOperation*/
		SettlementOperation cashSettlementOperation = settlementService.populateSettlementOperation(mechanismOperation,ComponentConstant.CASH_PART);
		cashSettlementOperation.setOperationState(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		
		create(cashSettlementOperation);
		saveAll(cashSettlementOperation.getParticipantSettlements());
		
		for(HolderAccountOperation accountOperation: mechanismOperation.getHolderAccountOperations()){
			Long operationID = mechanismOperation.getIdMechanismOperationPk();
			SettlementAccountOperation settAccount = settlementService.populateSettlementAccountOperation(operationID, accountOperation, null);
			settAccount.setSettlementAccountMarketfacts(new ArrayList<SettlementAccountMarketfact>());
			create(settAccount);
			
			for(AccountOperationMarketFact markFact: accountOperation.getAccountOperationMarketFacts()){
				SettlementAccountMarketfact settMarkFact = settlementService.populateSettlementAccountMarketFact(markFact, settAccount);
				settAccount.getSettlementAccountMarketfacts().add(settMarkFact);
				create(settMarkFact);
			}
		}
		
		for(HolderAccountOperation accountOperation: mechanismOperation.getHolderAccountOperations()){
			/**REGISTRAMOS HOLDER_ACCOUNT_OPERATION*/
			create(accountOperation);			
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				/**ASIGNAMOS HECHOS DE MERCADO A CUENTAS COMPRADORAS*/
				for(AccountOperationMarketFact markFact: accountOperation.getAccountOperationMarketFacts()){
					create(markFact);
				}
			}
		}
		
		return mechanismOperation;
	}
	
	/**
	 * Register sirtex operation.
	 *
	 * @param sirtexManagement the sirtex management
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest registerSirtexOperation(TradeRequest sirtexManagement) throws ServiceException{
		TradeRequest sirtexOperation = sirtexManagement;
		sirtexOperation.setNominalValue(sirtexOperation.getSecurities().getCurrentNominalValue());
		
		/**CREAMOS LA OPERACION*/
		create(sirtexOperation);
		
		/**CREAMOS LOS COUPONS OPERATIONS*/
		for(CouponTradeRequest couponOperation: sirtexOperation.getCouponsTradeRequestList()){
			if(couponOperation.getSuppliedQuantity()!=null && !couponOperation.getSuppliedQuantity().equals(BigDecimal.ZERO)){
				couponOperation.setIndSuppliedAccepted(BooleanType.NO.getCode());
				create(couponOperation);
			}
		}
		
		Date marketDate = null;
		BigDecimal marketRate = null;
		BigDecimal marketPrice = null;
		/**CREAMOS LAS CUENTAS ASIGNADAS*/
		List<AccountTradeMarketFact> accountMarketFacts = new ArrayList<AccountTradeMarketFact>();
		for(AccountTradeRequest accountOperation: sirtexOperation.getAccountSirtexOperations()){
			accountOperation.setStockQuantity(sirtexOperation.getStockQuantity());
			accountOperation.setCashAmount(sirtexOperation.getCashAmount());
			
			/**ASIGNAMOS HECHOS DE MERCADO A CUENTAS COMPRADORAS*/
			if(accountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
				/**SI LA OPERACION ES REPORTO, TRANSFERIMOS HECHOS DE MERCADO DESDE VENDEDORES -> COMPRADORES*/
					for(AccountTradeMarketFact markFact: accountMarketFacts){
						AccountTradeMarketFact accountMarkFact = new AccountTradeMarketFact();
						accountMarkFact.setAccountTradeRequest(accountOperation);
						/**ASIGNAMOS HECHOS RELEVANTES A CUENTAS NEGOCIADORAS*/
						accountMarkFact.setMarketDate(markFact.getMarketDate());
						accountMarkFact.setMarketRate(markFact.getMarketRate());
						accountMarkFact.setMarketPrice(markFact.getMarketPrice());
						accountMarkFact.setMarketQuantity(markFact.getMarketQuantity());
						accountMarkFact.setRegistryUser(markFact.getRegistryUser());
						accountMarkFact.setRegistryDate(markFact.getRegistryDate());
						accountOperation.getAccountSirtexMarkectfacts().add(accountMarkFact);
					}
//				}
			}else if(accountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
				/**SI LA CUENTA MATRIZ TIENE SOLO 1 HECHO DE MERCADO*/
				if(accountOperation.getAccountSirtexMarkectfacts().isEmpty()){
					HolderMarketFactBalance marketFactBalance =  findMarketFactBalance(sirtexOperation.getSellerParticipant().getIdParticipantPk(),
																					   accountOperation.getHolderAccount().getIdHolderAccountPk(),
																					   sirtexOperation.getSecurities().getIdSecurityCodePk());
					if(marketFactBalance==null){
						throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
					}
					AccountTradeMarketFact accountMarkFact = new AccountTradeMarketFact();
					accountMarkFact.setMarketDate(marketFactBalance.getMarketDate());
					accountMarkFact.setMarketRate(marketFactBalance.getMarketRate());
					accountMarkFact.setMarketPrice(marketFactBalance.getMarketPrice());
					accountMarkFact.setMarketQuantity(accountOperation.getStockQuantity());
					accountMarkFact.setRegistryUser(accountOperation.getRegisterUser());
					accountMarkFact.setRegistryDate(accountOperation.getRegisterDate());
					accountMarkFact.setAccountTradeRequest(accountOperation);
					marketDate = accountMarkFact.getMarketDate();
					marketRate = accountMarkFact.getMarketRate();
					marketPrice = accountMarkFact.getMarketPrice();
					accountMarketFacts.add(accountMarkFact);
					accountOperation.getAccountSirtexMarkectfacts().add(accountMarkFact);
				}else{
					accountMarketFacts.addAll(accountOperation.getAccountSirtexMarkectfacts());
				}
			}

			/**REGISTRAMOS HOLDER_ACCOUNT_OPERATION*/
			create(accountOperation);
			
			/**ITERAMOS HECHOS DE MERCADO EN CUENTAS INVERSIONISTA*/
			for(AccountTradeMarketFact accountMarkFact: accountOperation.getAccountSirtexMarkectfacts()){
				/**REGISTRAMOS HECHOS DE MERCADO*/
				if(accountMarkFact.getAccountTradeRequest().getIdAccountTradePk().equals(accountOperation.getIdAccountTradePk())){
					create(accountMarkFact);
				}
			}
		}
		

		
		MechanismModality mechModality = sirtexOperation.getMechanisnModality();
		Integer termPart = mechModality.getNegotiationModality().getIndTermSettlement();
		
		if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			/**SI ES OPERACION DIFERENTE A REPORTO, REGISTRAMOS HECHOS DE MERCADO A NIVEL DE OPERACION*/
			if(termPart!=null && termPart.equals(BooleanType.NO.getCode())){
				List<TradeMarketFact> marketFatOperationList = sirtexOperation.getSirtexOperationMarketfacts();
				if(marketFatOperationList!=null && !marketFatOperationList.isEmpty()){
					TradeMarketFact marketFact = sirtexOperation.getSirtexOperationMarketfacts().get(0);
					if(marketFact.getMarketDate()==null){
						marketFact.setMarketDate(marketDate);
						marketFact.setMarketRate(marketRate);
						marketFact.setMarketPrice(marketPrice);
					}
					create(marketFact);
				}
			}
		}
		
		return sirtexOperation;
	}
	
	
	/**
	 * Approve sirtex opereation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest approveSirtexOpereation(TradeRequest sirtexOperation) throws ServiceException {
		TradeRequest sirtexRequest = find(TradeRequest.class, sirtexOperation.getIdTradeRequestPk());
		if(sirtexRequest.getOperationState().equals(SirtexOperationStateType.REGISTERED.getCode())){
			sirtexRequest.setOperationState(SirtexOperationStateType.APROVED.getCode());
			update(sirtexRequest);
		}
		return sirtexRequest;
	}
	
	/**
	 * Annulate sirtex opereation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest annulateSirtexOpereation(TradeRequest sirtexOperation) throws ServiceException {
		TradeRequest tradeRequest = find(TradeRequest.class, sirtexOperation.getIdTradeRequestPk());
		if(tradeRequest.getOperationState().equals(SirtexOperationStateType.REGISTERED.getCode())){
			tradeRequest.setOperationState(SirtexOperationStateType.ANULATE.getCode());
			update(tradeRequest);
		}
		
		for(TradeOperation tradeOperation: tradeRequest.getTradeOperationList()){
			MechanismOperation operation = find(MechanismOperation.class, tradeOperation.getIdTradeOperationPk());
			operation.setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
			update(operation);
			
			Long operationPk = operation.getIdMechanismOperationPk();
			SettlementOperation settlementOperation = (SettlementOperation) settlementService.getSettlementOperation(operationPk, ComponentConstant.CASH_PART);
			settlementOperation.setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
			update(settlementOperation);
		}
		
		return tradeRequest;
	}
	
	/**
	 * METODO PARA REVISAR SOLICITUDES SIRTEX.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @param stockQuantityChanged the stock quantity changed
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest reviewSirtexOpereation(TradeRequest sirtexOperation, Boolean stockQuantityChanged) throws ServiceException {
		TradeRequest tradeRequest = find(TradeRequest.class, sirtexOperation.getIdTradeRequestPk());
		
		if(stockQuantityChanged){
			Map<Long,AccountTradeRequest> accountsModified = new HashMap<Long,AccountTradeRequest>();
			for(AccountTradeRequest accTrade: sirtexOperation.getAccountSirtexOperations()){
				accountsModified.put(accTrade.getIdAccountTradePk(), accTrade);
			}
			
			for(AccountTradeRequest accountTrade: tradeRequest.getAccountSirtexOperations()){
				AccountTradeRequest newAccountTrade = accountsModified.get(accountTrade.getIdAccountTradePk());
				accountTrade.setStockQuantity(newAccountTrade.getStockQuantity());
				accountTrade.setCashAmount(sirtexOperation.getCashPrice().multiply(accountTrade.getStockQuantity()));
				
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					Map<Long,AccountTradeMarketFact> mapMarkFactMod = new HashMap<Long,AccountTradeMarketFact>();
					for(AccountTradeMarketFact accMarkFact: newAccountTrade.getAccountSirtexMarkectfacts()){
						mapMarkFactMod.put(accMarkFact.getIdAccountTradeMarketFactPk(), accMarkFact);
					}
					
					for(AccountTradeMarketFact accMarkFact: accountTrade.getAccountSirtexMarkectfacts()){
						AccountTradeMarketFact newMarkFact = mapMarkFactMod.get(accMarkFact.getIdAccountTradeMarketFactPk());
						accMarkFact.setMarketQuantity(newMarkFact.getMarketQuantity());
						update(accMarkFact);
					}
				}
				update(accountTrade);
			}
		}
		
//		TradeOperation tradeOperation = operation.getTradeOperation();
		if(tradeRequest.getOperationState().equals(SirtexOperationStateType.APROVED.getCode())){
			tradeRequest.setOperationState(SirtexOperationStateType.REVIEWED.getCode());
			
			update(tradeRequest);
		}

		for(CouponTradeRequest sirtexCoupon: sirtexOperation.getCouponsTradeRequestList()){
			update(sirtexCoupon);
		}		
		return tradeRequest;
	}

	/**
	 * METODO PARA RECHAZAR SOLICITUDES SIRTEX.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest rejectSirtexOpereation(TradeRequest sirtexOperation) throws ServiceException {
		TradeRequest tradeRequest = find(TradeRequest.class, sirtexOperation.getIdTradeRequestPk());
		if(tradeRequest.getOperationState().equals(SirtexOperationStateType.REVIEWED.getCode()) || 
		   tradeRequest.getOperationState().equals(SirtexOperationStateType.APROVED.getCode())){
			tradeRequest.setOperationState(SirtexOperationStateType.REJECTED.getCode());
			update(tradeRequest);
		}
		
		for(TradeOperation tradeOperation: tradeRequest.getTradeOperationList()){
			MechanismOperation operation = find(MechanismOperation.class, tradeOperation.getIdTradeOperationPk());
			operation.setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
			update(operation);
			
			Long operationPk = operation.getIdMechanismOperationPk();
			SettlementOperation settlementOperation = (SettlementOperation) settlementService.getSettlementOperation(operationPk, ComponentConstant.CASH_PART);
			settlementOperation.setOperationState(MechanismOperationStateType.CANCELED_STATE.getCode());
			update(settlementOperation);
		}
		
		return tradeRequest;
	}
	
	/**
	 * METODO PARA CONFIRMAR SOLICITUDES SIRTEX.
	 *
	 * @param sirtexOperationId the sirtex operation id
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest confirmSirtexOperation(Long sirtexOperationId) throws ServiceException {
		TradeRequest tradeOperation = find(TradeRequest.class, sirtexOperationId);
		if(tradeOperation.getOperationState().equals(SirtexOperationStateType.REVIEWED.getCode())){
			tradeOperation.setOperationState(SirtexOperationStateType.CONFIRMED.getCode());
			update(tradeOperation);
		}
		return tradeOperation;
	}
	
	/**
	 * METODO PARA CONFIRMAR SOLICITUDES SIRTEX.
	 *
	 * @param sirtexOperationId the sirtex operation id
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest settTermSirtexOperation(Long sirtexOperationId) throws ServiceException {
		TradeRequest tradeOperation = find(TradeRequest.class, sirtexOperationId);
		if(tradeOperation.getOperationState().equals(SirtexOperationStateType.CONFIRMED.getCode())){
			for(MechanismOperation operation: findOperationsTradeRequest(sirtexOperationId, null)){
				if(!operation.getOperationState().equals(MechanismOperationStateType.TERM_SETTLED.getCode())){
					return null;
				}
			}
			tradeOperation.setOperationState(SirtexOperationStateType.TERM_SETTLED.getCode());
			update(tradeOperation);
		}else{
			throw new ServiceException();
		}
		return tradeOperation;
	}
	

	/**
	 * Confirm mechanism operation.
	 *
	 * @param operationID the operation id
	 * @param indTermPart the ind term part
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation confirmMechanismOperation(Long operationID, Integer indTermPart) throws ServiceException{
		MechanismOperation operation = find(MechanismOperation.class,operationID);
		if(operation.getOperationNumber()==null){
			operation.setOperationNumber(otcOperationServiceBean.getCurrentOperationNumber(operation));
		}
		operation.setOperationState(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		
		if(indTermPart.equals(BooleanType.YES.getCode())){ 
			
			/**Populate and create SettlementOperation*/
			SettlementOperation termSettlementOperation = settlementService.populateSettlementOperation(operation,ComponentConstant.TERM_PART);
			termSettlementOperation.setOperationState(MechanismOperationStateType.ASSIGNED_STATE.getCode());
			create(termSettlementOperation);
			saveAll(termSettlementOperation.getParticipantSettlements());
			
			List<HolderAccountOperation> termAccountOperations = new ArrayList<HolderAccountOperation>();
			for (HolderAccountOperation cashAccountOperation : operation.getHolderAccountOperations()) { 
				/**CREATE TERM ACCOUNT_OPERATION FOR MECHANISM_OPERATION*/
				HolderAccountOperation termAccountOperation = new HolderAccountOperation();
				termAccountOperation.setMechanismOperation(cashAccountOperation.getMechanismOperation());
				/**NEW ACCOUNT_OPERATION ARE TO ADD  TERM ON OPERATION*/
				termAccountOperation.setOperationPart(OperationPartType.TERM_PART.getCode());
				if (cashAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())) {
					termAccountOperation.setRole(ParticipantRoleType.SELL.getCode());
				} else if (cashAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())) {
					termAccountOperation.setRole(ParticipantRoleType.BUY.getCode());
				}
				termAccountOperation.setHolderAccount(cashAccountOperation.getHolderAccount());
				termAccountOperation.setStockQuantity(cashAccountOperation.getStockQuantity());
				termAccountOperation.setHolderAccountState(cashAccountOperation.getHolderAccountState());
				termAccountOperation.setInchargeFundsParticipant(cashAccountOperation.getInchargeFundsParticipant());
				termAccountOperation.setInchargeStockParticipant(cashAccountOperation.getInchargeStockParticipant());
				termAccountOperation.setIndIncharge(cashAccountOperation.getIndIncharge());
				termAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
				termAccountOperation.setRegisterUser(cashAccountOperation.getRegisterUser());
				termAccountOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(operation.getTermPrice(), termAccountOperation.getStockQuantity()));
				termAccountOperation.setCashAmount(termAccountOperation.getSettlementAmount());
				/**ADD OBJECT REFERENCE TO OBJECT FROM BD*/
				termAccountOperation.setRefAccountOperation(cashAccountOperation);
				termAccountOperation.setConfirmDate(CommonsUtilities.currentDateTime());
				/**ADD ACCOUNT TERM ON ACCOUNT_OPERATION*/
				create(termAccountOperation);
				
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					termAccountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
					for(AccountOperationMarketFact markFact: cashAccountOperation.getAccountOperationMarketFacts()){
						AccountOperationMarketFact _markFact = new AccountOperationMarketFact();
						_markFact.setMarketDate(markFact.getMarketDate());
						_markFact.setMarketRate(markFact.getMarketRate());
						_markFact.setMarketPrice(markFact.getMarketPrice());
						_markFact.setOperationQuantity(markFact.getOperationQuantity());
						_markFact.setHolderAccountOperation(termAccountOperation);
						termAccountOperation.getAccountOperationMarketFacts().add(_markFact);
						create(_markFact);
					}
					
					/**Config SettlementAccountOperation and save on settlement Process*/
					Long cashAccountOperationID = cashAccountOperation.getIdHolderAccountOperationPk();
					SettlementAccountOperation settCashOperation = settlementService.getSettlementAccountOperation(cashAccountOperationID);
					SettlementAccountOperation settTermOperation = settlementService.populateSettlementAccountOperation
																							(operationID, termAccountOperation, settCashOperation);
					
					settTermOperation.setSettlementAccountMarketfacts(new ArrayList<SettlementAccountMarketfact>());
					create(settTermOperation);
					
					/**Create MarketFact and save to bd*/
					for(AccountOperationMarketFact markFact: termAccountOperation.getAccountOperationMarketFacts()){
						SettlementAccountMarketfact settMarkFact = settlementService.populateSettlementAccountMarketFact(markFact, settTermOperation);
						settTermOperation.getSettlementAccountMarketfacts().add(settMarkFact);
						create(settMarkFact);
					}
				}
				termAccountOperations.add(termAccountOperation);
			}
			operation.getHolderAccountOperations().addAll(termAccountOperations);
			
			/**GENERAMOS LOS PARTICIPANT OPERATION EN PARTE PLAZO**/
			List<ParticipantOperation> termParticipantOperations = createParticipantOperation(operation, OperationPartType.TERM_PART.getCode());
			for(ParticipantOperation partOperation: termParticipantOperations){
				create(partOperation);
			}
		}
		
		update(operation);
		return operation;
	}
	
	/**
	 * ruchva - issue 996 SIRTEX enable/disable state
	 */
	public Issuer getSirtexHabilitationState(String idIssuerPk) {		
		StringBuilder issuerSQL = new StringBuilder();
		issuerSQL.append("SELECT iss FROM Issuer iss			");
		issuerSQL.append("WHERE iss.idIssuerPk = :idIssuerPk	");
		
		Query query = em.createQuery(issuerSQL.toString());
		query.setParameter("idIssuerPk", idIssuerPk);
		return (Issuer) query.getSingleResult();
	}
	/**
	 * Get MechanismOperation
	 * @param idMechanismOperation
	 * @return
	 */
	public MechanismOperation getMechanismOperationbyId (long idMechanismOperation){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT mo FROM MechanismOperation mo	");
		sb.append("WHERE mo.idMechanismOperationPk = :idMechanismOperation	");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		return (MechanismOperation) query.getSingleResult();
	}
	
	public void updateSecuritySirtexNegotiation(Security security) throws ServiceException {
		List <SecurityNegotiationMechanism> secNegotiationMechanismList = null;
		//deshabilitar	
		secNegotiationMechanismList = security.getSecurityNegotiationMechanisms();
		for (SecurityNegotiationMechanism secNegotiationMech : secNegotiationMechanismList) {
			if(secNegotiationMech.getMechanismModality().getIdConverted().equals("5-1")) {
//				secNegotiationMech.setSecurityNegotationState(BooleanType.NO.getCode()); //compra venta RF
			}else
			if(secNegotiationMech.getMechanismModality().getIdConverted().equals("5-3")) {
				secNegotiationMech.setSecurityNegotationState(BooleanType.NO.getCode()); //reporto RF
			}else
			if(secNegotiationMech.getMechanismModality().getIdConverted().equals("5-22")) {
//				secNegotiationMech.setSecurityNegotationState(BooleanType.NO.getCode()); //reporto reverso RF
			}
		}
		if (security.isVariableInstrumentTypeAcc() || (security.isFixedInstrumentType() && security.isPhysicalIssuanceForm())) {
			security.setAmortizationPaymentSchedule(null);
			security.setInterestPaymentSchedule(null);
		}
		if(security.isCeroInterestType()){
			security.setInterestPaymentSchedule(null);
		}
		security = update(security);
	}
	
	/**
	 * Method to create participant operations.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @param operationPart the operation part
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<ParticipantOperation> createParticipantOperation(MechanismOperation sirtexOperation, Integer operationPart) throws ServiceException{
		Map<String,ParticipantOperation> partOperationMap = new HashMap<String,ParticipantOperation>();
		for(InChargeNegotiationType inChargeType:InChargeNegotiationType.listSomeElements(InChargeNegotiationType.INCHARGE_STOCK,InChargeNegotiationType.INCHARGE_FUNDS)){
			for(HolderAccountOperation accountOperation: sirtexOperation.getHolderAccountOperations()){
				if(operationPart.equals(accountOperation.getOperationPart())){
					Participant partInCharge = null;
					if(inChargeType.getCode().equals(InChargeNegotiationType.INCHARGE_STOCK.getCode())){
						partInCharge = accountOperation.getInchargeStockParticipant();
					}else if(inChargeType.getCode().equals(InChargeNegotiationType.INCHARGE_FUNDS.getCode())){
						partInCharge = accountOperation.getInchargeFundsParticipant();
					}
					String mapKey = GeneralConstants.EMPTY_STRING;
					if(partInCharge!=null){
						mapKey = partInCharge.getIdParticipantPk().toString().concat(inChargeType.getCode().toString());
					}					
					if(partOperationMap.get(mapKey)==null){
						partOperationMap.put(mapKey, populateParticipantOperation(accountOperation, inChargeType.getCode(), partInCharge));
					}else{
						ParticipantOperation partOperation = partOperationMap.get(mapKey);
						partOperation.setStockQuantity(partOperation.getStockQuantity().add(accountOperation.getStockQuantity()));
						partOperation.setCashAmount(partOperation.getCashAmount().add(accountOperation.getCashAmount()));
					}
				}
			}
		}
		return new ArrayList<ParticipantOperation>(partOperationMap.values());
	}
	
	/**
	 * Populate participant operation.
	 *
	 * @param accountOperation the account operation
	 * @param inchargeType the incharge type
	 * @param participant the participant
	 * @return the participant operation
	 */
	private ParticipantOperation populateParticipantOperation(HolderAccountOperation accountOperation, Integer inchargeType, Participant participant){
		MechanismOperation mechanismOperation = accountOperation.getMechanismOperation(); 
		Integer currency = mechanismOperation.getCurrency();
		ParticipantOperation participantOperation = new ParticipantOperation();
		participantOperation.setParticipant(participant);
		participantOperation.setCashAmount(accountOperation.getCashAmount());
		participantOperation.setStockQuantity(accountOperation.getStockQuantity());
		participantOperation.setCurrency(currency);
		participantOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
		participantOperation.setInchargeType(inchargeType);
		participantOperation.setIndIncharge(BooleanType.NO.getCode());
		participantOperation.setOperationPart(accountOperation.getOperationPart());
		participantOperation.setSettlementCurrency(currency);
		participantOperation.setSettlementAmount(accountOperation.getSettlementAmount());
		participantOperation.setRole(accountOperation.getRole());
		participantOperation.setBlockedStock(BigDecimal.ZERO);
		participantOperation.setInitialMarginAmount(BigDecimal.ZERO);
		participantOperation.setDepositedAmount(BigDecimal.ZERO);
		participantOperation.setDepositedMargin(BigDecimal.ZERO);
		participantOperation.setIndPayedBack(BooleanType.YES.getCode());
		participantOperation.setMechanismOperation(mechanismOperation);
		return participantOperation;
	}

	/**
	 * Find balance from account.
	 *
	 * @param securityCode the security code
	 * @param holderAccountPK the holder account pk
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance findBalanceFromAccount(String securityCode, Long holderAccountPK) throws ServiceException{
		StringBuilder balancesSQL = new StringBuilder();
		balancesSQL.append("SELECT hab FROM HolderAccountBalance hab								");
		balancesSQL.append("WHERE 																	");
		balancesSQL.append("	  hab.holderAccount.idHolderAccountPk = :holderAccountID	 AND	");
		balancesSQL.append("	  hab.security.idSecurityCodePk 	  = :securityID 		 		");
		
		Query query = em.createQuery(balancesSQL.toString());
		query.setParameter("securityID", securityCode);
		query.setParameter("holderAccountID", holderAccountPK);
		return (HolderAccountBalance) query.getSingleResult();
	}
	
	/**
	 * QUERY PARA LA PANTALLA DE SIRTEX Y TAMBIEN PARA EL WEBSERVICE DE CONSULTA DE SIRTEX
	 * 
	 * @param otcOperationTO the otc operation to
	 * @return the sirtex operations service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SirtexOperationResultTO> getSirtexOperationsServiceBean(SirtexOperationTO otcOperationTO) throws ServiceException {

		StringBuilder querySql = new StringBuilder();
		
		querySql.append("     SELECT                                                                                               ");	
		querySql.append("      DISTINCT TREQ.ID_TRADE_REQUEST_PK,                                                                  ");	
		querySql.append("      MOPE.ID_NEGOTIATION_MODALITY_FK,                                                                    ");
		querySql.append("      (select MODALITY_NAME from NEGOTIATION_MODALITY                                                      ");
		querySql.append("      where ID_NEGOTIATION_MODALITY_PK = MOPE.ID_NEGOTIATION_MODALITY_FK ) MODALITY_NAME,                  ");
		//querySql.append("      MMOD.ID_NEGOTIATION_MODALITY_PK,                                                                    ");	
		//querySql.append("      NMOD.MODALITY_NAME ,                                                                                ");	
		querySql.append("      TREQ.OPERATION_DATE,                                                                                ");	
		querySql.append("      TREQ.ID_SELLER_PARTICIPANT_FK ,                                                                     ");	
		querySql.append("      PAR1.MNEMONIC ,                                                                                     ");	
		querySql.append("      TREQ.ID_BUYER_PARTICIPANT_FK,                                                                       ");	
		querySql.append("      PAR2.MNEMONIC PAR2MNEMONIC,                                                                         ");	
		querySql.append("      TREQ.CASH_SETTLEMENT_DATE ,                                                                         ");	
		querySql.append("      TREQ.TERM_SETTLEMENT_DATE ,                                                                         ");	
		querySql.append("      TREQ.ID_SECURITY_CODE_FK ,                                                                          ");	
		querySql.append("      TREQ.OPERATION_STATE ,                                                                              ");	
		querySql.append("      TREQ.REGISTRY_USER ,                                                                                ");	
		//querySql.append("      TREQ.STOCK_QUANTITY ,                                                                               ");	
		querySql.append("      ATR.STOCK_QUANTITY ,                                                                                ");	
		querySql.append("      MOPE.TERM_SETTLEMENT_DAYS ,                                                                         ");	
		querySql.append("      MOPE.TERM_SETTLEMENT_DATE MOPEDATE_TERM                                                            ");	
//		querySql.append("      CASE WHEN MOPE.OPERATION_STATE = 616 THEN                                                           ");	
//		querySql.append("      (     SELECT (TRUNC(SO.REAL_SETTLEMENT_DATE) - TRUNC(TREQ.OPERATION_DATE))                          ");	
//		querySql.append("      FROM SETTLEMENT_OPERATION SO                                                                        ");	
//		querySql.append("      INNER JOIN MECHANISM_OPERATION MO ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK    ");	
//		querySql.append("      INNER JOIN TRADE_OPERATION TOP  ON MO.ID_MECHANISM_OPERATION_PK = TOP.ID_TRADE_OPERATION_PK         ");	
//		querySql.append("      INNER JOIN TRADE_REQUEST TR ON TOP.ID_TRADE_REQUEST_FK = TR.ID_TRADE_REQUEST_PK                     ");	
//		querySql.append("      WHERE SO.OPERATION_PART = 2                                                                         ");	
//		querySql.append("      AND (SELECT MAX(MO2.ID_MECHANISM_OPERATION_PK)                                                      ");	
//		querySql.append("      FROM MECHANISM_OPERATION MO2                                                                        ");	
//		querySql.append("      WHERE MO2.ID_MECHANISM_OPERATION_PK                                                                 ");	
//		querySql.append("      IN(     SELECT MEC.ID_MECHANISM_OPERATION_PK FROM                                                   ");	
//		querySql.append("      MECHANISM_OPERATION MEC                                                                             ");	
//		querySql.append("      INNER JOIN TRADE_OPERATION TOP  ON MEC.ID_MECHANISM_OPERATION_PK=TOP.ID_TRADE_OPERATION_PK          ");	
//		querySql.append("      INNER JOIN TRADE_REQUEST TR ON TOP.ID_TRADE_REQUEST_FK=TR.ID_TRADE_REQUEST_PK                       ");	
//		querySql.append("          WHERE TR.ID_TRADE_REQUEST_PK = TREQ.ID_TRADE_REQUEST_PK                                         ");	
//		querySql.append("      AND MO.OPERATION_STATE = MOPE.OPERATION_STATE )) = MO.ID_MECHANISM_OPERATION_PK )                   ");	
//		querySql.append("      ELSE NULL END FECHA_VENC  ,                                                                          ");	
//		querySql.append("      MOPE.ID_MECHANISM_OPERATION_PK,                                                                     ");
//		querySql.append("      SEC.IND_IS_COUPON                                                                                   ");
		querySql.append("  FROM   MECHANISM_OPERATION MOPE                                                                         ");	
		querySql.append("  INNER JOIN SECURITY SEC ON MOPE.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                		   ");	
		querySql.append("  INNER JOIN TRADE_OPERATION TOPE ON MOPE.ID_MECHANISM_OPERATION_PK=TOPE.ID_TRADE_OPERATION_PK  		   ");         
		querySql.append("  INNER JOIN TRADE_REQUEST TREQ ON TOPE.ID_TRADE_REQUEST_FK=TREQ.ID_TRADE_REQUEST_PK             		   ");
		querySql.append("  LEFT OUTER JOIN ACCOUNT_TRADE_REQUEST ATR on TREQ.ID_TRADE_REQUEST_PK=ATR.ID_TRADE_REQUEST_FK   		   ");   
		querySql.append("  INNER JOIN PARTICIPANT PAR1 ON TREQ.ID_SELLER_PARTICIPANT_FK=PAR1.ID_PARTICIPANT_PK             		   ");
		querySql.append("  INNER JOIN PARTICIPANT PAR2 ON TREQ.ID_BUYER_PARTICIPANT_FK=PAR2.ID_PARTICIPANT_PK               	   ");
		
//		querySql.append("      MECHANISM_OPERATION MOPE                                                                            ");	
//		querySql.append("      INNER JOIN TRADE_OPERATION TOPE ON MOPE.ID_MECHANISM_OPERATION_PK=TOPE.ID_TRADE_OPERATION_PK        ");	
//		querySql.append("      INNER JOIN TRADE_REQUEST TREQ ON TOPE.ID_TRADE_REQUEST_FK=TREQ.ID_TRADE_REQUEST_PK,                 ");	
//		querySql.append("      MECHANISM_MODALITY MMOD,                                                                            ");	
//		querySql.append("      NEGOTIATION_MODALITY NMOD,                                                                          ");	
//		querySql.append("      PARTICIPANT PAR1,                                                                                   ");	
//		querySql.append("      PARTICIPANT PAR2                                                                                    ");	
		
		querySql.append("  WHERE                                                                                                   ");	
		querySql.append("      TREQ.ID_NEGOTIATION_MECHANISM_FK=MOPE.ID_NEGOTIATION_MECHANISM_FK                                     ");	
		//querySql.append("      AND TREQ.ID_NEGOTIATION_MODALITY_FK=MMOD.ID_NEGOTIATION_MODALITY_PK                                 ");	
		//querySql.append("      AND MMOD.ID_NEGOTIATION_MODALITY_PK=NMOD.ID_NEGOTIATION_MODALITY_PK                                 ");	
		//querySql.append("      AND TREQ.ID_SELLER_PARTICIPANT_FK=PAR1.ID_PARTICIPANT_PK                                            ");	
		//querySql.append("      AND TREQ.ID_BUYER_PARTICIPANT_FK=PAR2.ID_PARTICIPANT_PK                                             ");	
		querySql.append("      AND TREQ.ID_NEGOTIATION_MECHANISM_FK=5                                                              ");	
		
				if (otcOperationTO.getIdSearchDateType() != null) {
					String operationDateType = null;
					switch (OtcOperationDateSearchType.get(otcOperationTO.getIdSearchDateType())) {
						case OPERATION_DATE:	operationDateType = "REGISTRY_DATE";break;
						case CASH_SETTLEM_DATE:	operationDateType = "CASH_SETTLEMENT_DATE";break;
						case TERM_SETTLEM_DATE:	operationDateType = "TERM_SETTLEMENT_DATE";break;
					}
					querySql.append("AND (TREQ.").append(operationDateType).append("	BETWEEN :initialDate And :finalDate)	");
				}
				
				if (otcOperationTO.getIdSecurityCodePk() != null) {
					querySql.append("And MOPE.ID_SECURITY_CODE_FK = :idSecurityCodePk					");
				}

				if (otcOperationTO.getOtcOperationState() != null) {
					querySql.append("And TREQ.OPERATION_STATE = :otcOperationState					");
				}
				if (otcOperationTO.getIdModalityNegotiation() != null) {
					querySql.append("And MOPE.ID_NEGOTIATION_MODALITY_FK = :negModalityid			");
				}

				if (otcOperationTO.getIdParticipantBuyer() != null) {
					querySql.append("And	(TREQ.ID_BUYER_PARTICIPANT_FK = :participCode 		    ");
					querySql.append("		 OR TREQ.ID_SELLER_PARTICIPANT_FK = :participCode )		");
				}
		
		querySql.append("  ORDER BY  TREQ.ID_TRADE_REQUEST_PK DESC        	     					");	

		Query query = em.createNativeQuery(querySql.toString());
		
		if (otcOperationTO.getIdSecurityCodePk() != null) {
			query.setParameter("idSecurityCodePk",otcOperationTO.getIdSecurityCodePk());
		}
		if (otcOperationTO.getIdParticipantBuyer() != null) {
			query.setParameter("participCode",otcOperationTO.getIdParticipantBuyer());
		}
		if (otcOperationTO.getIdModalityNegotiation() != null) {
			query.setParameter("negModalityid",otcOperationTO.getIdModalityNegotiation());
		}
		if (otcOperationTO.getOtcOperationState() != null) {
			query.setParameter("otcOperationState",otcOperationTO.getOtcOperationState());
		}
		if (otcOperationTO.getIdSearchDateType() != null) {
			query.setParameter("initialDate", otcOperationTO.getInitialDate());
			query.setParameter("finalDate", otcOperationTO.getFinalDate());
		}

		List<Object[]> result = query.getResultList();
		
		List<SirtexOperationResultTO> sirtexResultList = new ArrayList<SirtexOperationResultTO>();
		
		try {
			for(Object[] objectData: result){

				if(Integer.valueOf(objectData[11].toString()).equals(SirtexOperationStateType.REVIEWED.getCode()) ||
						Integer.valueOf(objectData[11].toString()).equals(SirtexOperationStateType.REVIEWED.getCode())){
					
					StringBuilder subQuerySql = new StringBuilder();
					
					subQuerySql.append("     SELECT CTR.* FROM                                                                                       ");	
					subQuerySql.append("     COUPON_TRADE_REQUEST CTR						                                                        ");	
					subQuerySql.append(" 	 WHERE CTR.ID_TRADE_REQUEST_FK = :idTrade        	     												");	

					Query subQuery = em.createNativeQuery(subQuerySql.toString());
					
					subQuery.setParameter("idTrade",Integer.valueOf(objectData[0].toString()));
					
					List<Object[]> result2 = subQuery.getResultList();
					
					BigDecimal quantity = new BigDecimal(0);
					
					for(Object[] results : result2){
						
						//Si de algun cupon la cantidad no es aceptada, entonces no se suma
						if(Validations.validateIsNotNullAndNotEmpty(results[8])){
							if((Integer.valueOf(results[9].toString())) == GeneralConstants.ZERO_VALUE_INTEGER){
								quantity = quantity.add(new BigDecimal(Integer.valueOf(results[8].toString())));
								break;
							}
						}
					}
					objectData[13] = quantity;
				}
				
				sirtexResultList.add(NegotiationUtils.populateSearchSirtexTO(objectData));
			}
			return sirtexResultList;
		} catch (NoResultException ex) {
			return null;
		}

	}
	
	/**
	 * Find mechanism operation from id.
	 *
	 * @param mechanismOperationID the mechanism operation id
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation findMechanismOperationFromId(Long mechanismOperationID) throws ServiceException{
		StringBuilder selectSQL = new StringBuilder();
		selectSQL.append("SELECT DISTINCT(mo) FROM MechanismOperation mo 					");
		selectSQL.append("INNER JOIN FETCH mo.tradeOperation trad 							");
		selectSQL.append("INNER JOIN FETCH mo.securities	 sec	 						");
		selectSQL.append("INNER JOIN FETCH sec.issuer		 isur							");
		selectSQL.append("INNER JOIN FETCH sec.issuance		 issu							");
		selectSQL.append("INNER JOIN FETCH mo.mechanisnModality mm 							");
		selectSQL.append("INNER JOIN FETCH mm.negotiationModality 							");
		selectSQL.append("INNER JOIN FETCH mm.negotiationMechanism 							");
		selectSQL.append("INNER JOIN FETCH mo.mechanismOperationMarketFacts 				");
		selectSQL.append("WHERE	 mo.idMechanismOperationPk = :operationID					");
		Query query = em.createQuery(selectSQL.toString());
		query.setParameter("operationID", mechanismOperationID);
		MechanismOperation operation = (MechanismOperation) query.getSingleResult();
		for (HolderAccountOperation holderAccountOperation : operation.getHolderAccountOperations()) {
			if (holderAccountOperation.getHolderAccount() != null) {
				for (HolderAccountDetail accountDetail : holderAccountOperation.getHolderAccount().getHolderAccountDetails()) {
					accountDetail.getHolder().getIdHolderPk();
				}
				for(AccountOperationMarketFact accountMarketFact: holderAccountOperation.getAccountOperationMarketFacts()){
					accountMarketFact.getMarketDate();
				}
			}
			holderAccountOperation.getInchargeFundsParticipant().getIdParticipantPk();
			holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk();
		}
		return operation;
	}
	
	/**
	 * Find sirtex operation from id.
	 *
	 * @param sirtexOperationID the sirtex operation id
	 * @param parameters the parameters
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest findSirtexOperationFromId(Long sirtexOperationID, Map<Integer,String> parameters) throws ServiceException{
		StringBuilder selectSQL = new StringBuilder();
		selectSQL.append("SELECT DISTINCT(mo) FROM TradeRequest mo 							");
		selectSQL.append("INNER JOIN FETCH mo.securities	 sec	 						");
		selectSQL.append("INNER JOIN FETCH mo.sellerParticipant	 sel	 					");
		selectSQL.append("INNER JOIN FETCH mo.buyerParticipant	 buy	 					");
		selectSQL.append("INNER JOIN FETCH sec.issuer		 isur							");
		selectSQL.append("INNER JOIN FETCH sec.issuance		 issu							");
		selectSQL.append("INNER JOIN FETCH sel.holder		 holsel							");
		selectSQL.append("INNER JOIN FETCH buy.holder		 holbuy							");
		selectSQL.append("INNER JOIN FETCH mo.mechanisnModality mm 							");
		selectSQL.append("INNER JOIN FETCH mm.negotiationModality 							");
		selectSQL.append("INNER JOIN FETCH mm.negotiationMechanism 							");
		selectSQL.append("LEFT JOIN FETCH mo.accountSirtexOperations 	 					");
		//selectSQL.append("LEFT JOIN FETCH mo.sirtexOperationMarketfacts	 					");
		selectSQL.append("WHERE	 mo.idTradeRequestPk = :operationID							");
		Query query = em.createQuery(selectSQL.toString());
		query.setParameter("operationID", sirtexOperationID);
		TradeRequest operation = (TradeRequest) query.getSingleResult();
		
		Security security = operation.getSecurityParent()!=null?operation.getSecurityParent():operation.getSecurities();
		
		for (AccountTradeRequest holderAccountOperation : operation.getAccountSirtexOperations()){
			if (holderAccountOperation.getHolderAccount() != null) {
				for (HolderAccountDetail accountDetail : holderAccountOperation.getHolderAccount().getHolderAccountDetails()) {
					accountDetail.getHolder().getIdHolderPk();
				}
				for(AccountTradeMarketFact accountMarketFact: holderAccountOperation.getAccountSirtexMarkectfacts()){
					accountMarketFact.getMarketDate();
				}
			}
			holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk();
		}
		
		for(CouponTradeRequest coupon: operation.getCouponsTradeRequestList()){
			if(coupon.getIndSuppliedAccepted()!=null && coupon.getIndSuppliedAccepted().equals(BooleanType.YES.getCode())){
				coupon.setSuppliedOk(Boolean.TRUE);
			}
			coupon.setSecurityCouponDescription(generateSecurityCouponDescription(security, coupon.getCouponNumber(),BooleanType.YES.getCode()));
			coupon.setSecurityCouponBcb(generateSecurityCouponBcb(security,coupon.getCouponNumber(),parameters));
			if(coupon.getMechanismOperation()!=null){
				coupon.getMechanismOperation().getSecurities().getIdSecurityCodePk();
			}
		}
		Date expirationDate = operation.getSecurities().getExpirationDate();
		operation.getSecurities().setPendientDays(new Long(CommonsUtilities.getDaysBetween(CommonsUtilities.currentDate(), expirationDate)));
		
		return operation;
	}
	
	/**
	 * Search holder by holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder searchHolderByHolderAccount(Long idHolderAccountPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append(" 	   HAD.holder.idHolderPk ");
		sbQuery.append("  FROM HolderAccountDetail HAD");
		sbQuery.append(" WHERE HAD.holderAccount.idHolderAccountPk=:idHolderAccountPk ");
		sbQuery.append("   AND HAD.indRepresentative=:indRepresentative");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("indRepresentative", GeneralConstants.ONE_VALUE_INTEGER.intValue());
				
		Long idHolderPk = (Long) query.getSingleResult();
		Holder holder = new Holder();
		holder.setIdHolderPk(idHolderPk);
		return holder;
	}
	
	/**
	 * Generate security coupon description.
	 *
	 * @param security the security
	 * @param couponNumber the coupon number
	 * @param indIsCupon the ind is cupon
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String generateSecurityCouponDescription(Security security, Integer couponNumber, Integer indIsCupon) throws ServiceException{
		StringBuffer securityCoupon = new StringBuffer();
		if(indIsCupon.equals(BooleanType.YES.getCode())){
			securityCoupon.append(SecurityClassType.CUP.name());
		}else{
			securityCoupon.append(SecurityClassType.get(security.getSecurityClass()).name());
		}
		securityCoupon.append(GeneralConstants.HYPHEN);
		securityCoupon.append(security.getIdSecurityCodeOnly());
		securityCoupon.append(GeneralConstants.HYPHEN);
		securityCoupon.append(String.format("%03d", couponNumber));		
		return securityCoupon.toString();
	}
	
	/**
	 * Generate security coupon bcb.
	 *
	 * @param security the security
	 * @param couponNumber the coupon number
	 * @param parameters the parameters
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String generateSecurityCouponBcb(Security security, Integer couponNumber, Map<Integer,String> parameters) throws ServiceException{
		StringBuffer couponBcb = new StringBuffer();
		Integer securityClass  = security.getSecurityClass();
		String secClassBcb 	   = parameters.get(securityClass);
		String securityCode	   = security.getIdSecurityCodeOnly();

		couponBcb.append(GeneralConstants.BRACKET_OPEN); 
		couponBcb.append(secClassBcb).append(GeneralConstants.BRACKET_CLOSE).append(GeneralConstants.BLANK_SPACE).append(securityCode);
		couponBcb.append(GeneralConstants.HYPHEN).append(String.format("%03d", couponNumber));
		return couponBcb.toString();
	}
	
	/**
	 * Config operation from coupon.
	 *
	 * @param tradeRequest the trade request
	 * @param operation the operation
	 * @param securityTrade the security trade
	 * @param stckQuantity the stck quantity
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation configOperationFromCoupon(TradeRequest tradeRequest, MechanismOperation operation, Security securityTrade, BigDecimal stckQuantity) 
																throws ServiceException{
		BigDecimal stockQuantity = stckQuantity;
		Security security = securityTrade;
		
		MechanismOperation mechanOperation = operation;
		/**OBTENEMOS SI LA MODALIDAD MANEJA PLAZO*/
		MechanismModality mechModality = tradeRequest.getMechanisnModality();
		Integer termPart = mechModality.getNegotiationModality().getIndTermSettlement();
		
		mechanOperation.getTradeOperation().setTradeRequest(tradeRequest);
		/*LLENAMOS LOS DATOS DE LA OPERACION*/
		mechanOperation.setSecurities(security);
		mechanOperation.setOperationDate(tradeRequest.getOperationDate());
		mechanOperation.setStockQuantity(stockQuantity);
		mechanOperation.setBuyerParticipant(tradeRequest.getBuyerParticipant());
		mechanOperation.setSellerParticipant(tradeRequest.getSellerParticipant());
		mechanOperation.setCashPrice(tradeRequest.getCashPrice());
		mechanOperation.setCashSettlementDays(tradeRequest.getCashSettlementDays());
		mechanOperation.setCashSettlementDate(tradeRequest.getCashSettlementDate());
		mechanOperation.setTermSettlementDays(tradeRequest.getTermSettlementDays());
		mechanOperation.setTermSettlementDate(tradeRequest.getTermSettlementDate());
		mechanOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		mechanOperation.setRegisterUser(tradeRequest.getRegistryUser());
		mechanOperation.setCashAmount(stockQuantity.multiply(tradeRequest.getCashPrice()));
		mechanOperation.setCashPrice(tradeRequest.getCashPrice());
		mechanOperation.setTermPrice(tradeRequest.getTermPrice());
		mechanOperation.setTermAmount(tradeRequest.getTermAmount());
		mechanOperation.setRealCashAmount(tradeRequest.getRealCashPrice().multiply(stockQuantity));
		mechanOperation.setRealCashPrice(tradeRequest.getRealCashPrice());
		mechanOperation.setMechanisnModality(tradeRequest.getMechanisnModality());
		mechanOperation.setCurrency(tradeRequest.getSettlementCurrency());
		mechanOperation.setSettlementType(tradeRequest.getSettlementType());
		mechanOperation.setSettlementSchema(tradeRequest.getSettlementScheme());
		mechanOperation.setSecurities(tradeRequest.getSecurities());
		mechanOperation.setExchangeRate(tradeRequest.getExchangeRate());
		mechanOperation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		
		mechanOperation.setMechanismOperationMarketFacts(new ArrayList<MechanismOperationMarketFact>());
		for(TradeMarketFact tradeMarketFact: tradeRequest.getSirtexOperationMarketfacts()){
			MechanismOperationMarketFact operationMarketFact = new MechanismOperationMarketFact();
			operationMarketFact.setMarketDate(tradeMarketFact.getMarketDate());
//			operationMarketFact.setMarketPrice(tradeMarketFact.getMarketPrice());
			operationMarketFact.setMarketPrice(BigDecimal.ZERO);
			operationMarketFact.setMarketRate(tradeMarketFact.getMarketRate());
			operationMarketFact.setMechanismOperation(mechanOperation);
			mechanOperation.getMechanismOperationMarketFacts().add(operationMarketFact);
		}		
		
		/*CONFIGURAMOS LOS ACCOUNT OPERATION*/
		mechanOperation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		for(AccountTradeRequest accountTradeRequest: tradeRequest.getAccountSirtexOperations()){
			HolderAccountOperation accountOperation = new HolderAccountOperation();
			accountOperation.setRegisterDate(accountTradeRequest.getRegisterDate());
			accountOperation.setRegisterUser(accountTradeRequest.getRegisterUser());
			accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
			
			accountOperation.setHolderAccount(accountTradeRequest.getHolderAccount());
			accountOperation.setIndIncharge(accountTradeRequest.getIndInCharge());
			accountOperation.setInchargeFundsParticipant(accountTradeRequest.getInchargeFundsParticipant());
			accountOperation.setInchargeStockParticipant(accountTradeRequest.getInchargeStockParticipant());
			accountOperation.setStockQuantity(stockQuantity);
			accountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
			accountOperation.setMechanismOperation(mechanOperation);
			accountOperation.setRole(accountTradeRequest.getRole());
			accountOperation.setOperationPart(accountTradeRequest.getOperationPart());
			accountOperation.setCashAmount(accountTradeRequest.getCashAmount());
			accountOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
			
			/**VERIFICAMOS SI EXISTEN HECHOS DE MERCADO*/
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				/**VARIABLE PARA MANEJAR EL SALDO DE LOS HECHOS DE MERCADO A NIVEL DE CUPON*/
				BigDecimal stockQuantityRem = accountOperation.getStockQuantity();
				
				/**SI ES COMPRADOR Y NO MANEJA PARTE PLAZO*/
				if(accountOperation.getRole().equals(ParticipantRoleType.BUY.getCode()) && termPart.equals(BooleanType.NO.getCode())){
					TradeMarketFact tradeMarketFact = tradeRequest.getSirtexOperationMarketfacts().get(0);
					AccountOperationMarketFact accountMarkFact = new AccountOperationMarketFact();
					accountMarkFact.setMarketDate(tradeMarketFact.getMarketDate());
					accountMarkFact.setMarketRate(tradeMarketFact.getMarketRate());
					accountMarkFact.setMarketPrice(tradeMarketFact.getMarketPrice());
					accountMarkFact.setOperationQuantity(stockQuantityRem);
					accountMarkFact.setHolderAccountOperation(accountOperation);
					accountOperation.getAccountOperationMarketFacts().add(accountMarkFact);
				}else{
					/**CONFIGURAMOS LOS HECHOS DE MERCADO*/
					for(AccountTradeMarketFact accTradeMarketFact: accountTradeRequest.getAccountSirtexMarkectfacts()){
						BigDecimal marketQuantity = accTradeMarketFact.getMarketQuantity();					
						AccountOperationMarketFact accountMarkFact = new AccountOperationMarketFact();
						accountMarkFact.setMarketDate(accTradeMarketFact.getMarketDate());
						accountMarkFact.setMarketRate(accTradeMarketFact.getMarketRate());
//						accountMarkFact.setMarketPrice(accTradeMarketFact.getMarketPrice());
						accountMarkFact.setMarketPrice(BigDecimal.ZERO);
						if(stockQuantityRem.compareTo(marketQuantity)>=0){
							accountMarkFact.setOperationQuantity(marketQuantity);
							accountMarkFact.setHolderAccountOperation(accountOperation);
							accountOperation.getAccountOperationMarketFacts().add(accountMarkFact);
						}else{
							accountMarkFact.setOperationQuantity(stockQuantityRem);
							accountMarkFact.setHolderAccountOperation(accountOperation);
							accountOperation.getAccountOperationMarketFacts().add(accountMarkFact);
							break;
						}
						stockQuantityRem = stockQuantityRem.subtract(marketQuantity);
					}
				}
			}
			mechanOperation.getHolderAccountOperations().add(accountOperation);
		}
		return operation;
	}
	
	/**
	 * Config trade request like operation.
	 *
	 * @param tradeRequest the trade request
	 * @param mechanismOperation the mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation configTradeRequestLikeOperation(TradeRequest tradeRequest, MechanismOperation mechanismOperation) throws ServiceException{
		MechanismOperation mechanOperation = mechanismOperation;
		mechanOperation.getTradeOperation().setTradeRequest(tradeRequest);

		/*LLENAMOS LOS DATOS DE LA OPERACION*/
		mechanOperation.setOperationDate(tradeRequest.getOperationDate());
		mechanOperation.setStockQuantity(tradeRequest.getStockQuantity());
		mechanOperation.setBuyerParticipant(tradeRequest.getBuyerParticipant());
		mechanOperation.setSellerParticipant(tradeRequest.getSellerParticipant());
		mechanOperation.setCashPrice(tradeRequest.getCashPrice());
		mechanOperation.setCashSettlementDays(tradeRequest.getCashSettlementDays());
		mechanOperation.setCashSettlementDate(tradeRequest.getCashSettlementDate());
		mechanOperation.setTermSettlementDays(tradeRequest.getTermSettlementDays());
		mechanOperation.setTermSettlementDate(tradeRequest.getTermSettlementDate());
		mechanOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		mechanOperation.setRegisterUser(tradeRequest.getRegistryUser());
		mechanOperation.setCashAmount(tradeRequest.getCashAmount());
		mechanOperation.setCashPrice(tradeRequest.getCashPrice());
		mechanOperation.setRealCashAmount(tradeRequest.getRealCashAmount());
		mechanOperation.setRealCashPrice(tradeRequest.getRealCashPrice());
		mechanOperation.setMechanisnModality(tradeRequest.getMechanisnModality());
		mechanOperation.setCurrency(tradeRequest.getSettlementCurrency());
		mechanOperation.setSettlementType(tradeRequest.getSettlementType());
		mechanOperation.setSettlementSchema(tradeRequest.getSettlementScheme());
		mechanOperation.setSecurities(tradeRequest.getSecurities());
		mechanOperation.setExchangeRate(tradeRequest.getExchangeRate());
		mechanOperation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		if(tradeRequest.getTermPrice()!=null){
			mechanOperation.setTermPrice(tradeRequest.getTermPrice());
			mechanOperation.setTermAmount(tradeRequest.getTermPrice().multiply(tradeRequest.getStockQuantity()).setScale(2, RoundingMode.HALF_UP));
		}else{
			mechanOperation.setTermPrice(BigDecimal.ZERO);
			mechanOperation.setTermAmount(BigDecimal.ZERO);
		}
		
		/**CONFIGURAMOS HECHOS DE MERCADO A NIVEL DE OPERACION*/
		mechanOperation.setMechanismOperationMarketFacts(new ArrayList<MechanismOperationMarketFact>());
		for(TradeMarketFact tradeMarketFact: tradeRequest.getSirtexOperationMarketfacts()){
			MechanismOperationMarketFact operationMarketFact = new MechanismOperationMarketFact();
			operationMarketFact.setMarketDate(tradeMarketFact.getMarketDate());
			operationMarketFact.setMarketPrice(tradeMarketFact.getMarketPrice());
			operationMarketFact.setMarketRate(tradeMarketFact.getMarketRate());
			operationMarketFact.setMechanismOperation(mechanOperation);
			mechanOperation.getMechanismOperationMarketFacts().add(operationMarketFact);
		}
		
		/**CONFIGURAMOS LOS ACCOUNT OPERATION*/
		mechanOperation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		for(AccountTradeRequest accountTradeRequest: tradeRequest.getAccountSirtexOperations()){
			HolderAccountOperation accountOperation = new HolderAccountOperation();
			accountOperation.setRegisterDate(accountTradeRequest.getRegisterDate());
			accountOperation.setRegisterUser(accountTradeRequest.getRegisterUser());
			accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());			
			accountOperation.setHolderAccount(accountTradeRequest.getHolderAccount());
			accountOperation.setIndIncharge(accountTradeRequest.getIndInCharge());
			accountOperation.setInchargeFundsParticipant(accountTradeRequest.getInchargeFundsParticipant());
			accountOperation.setInchargeStockParticipant(accountTradeRequest.getInchargeStockParticipant());
			accountOperation.setStockQuantity(accountTradeRequest.getStockQuantity());
			accountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
			accountOperation.setMechanismOperation(mechanOperation);
			accountOperation.setRole(accountTradeRequest.getRole());
			accountOperation.setOperationPart(accountTradeRequest.getOperationPart());
			accountOperation.setCashAmount(accountTradeRequest.getCashAmount());
			accountOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
			if(accountOperation.getOperationPart().equals(OperationPartType.CASH_PART.getCode())){
				accountOperation.setSettlementAmount(mechanOperation.getCashPrice().multiply(accountOperation.getStockQuantity()));
			}else if(accountOperation.getOperationPart().equals(OperationPartType.TERM_PART.getCode())){
				accountOperation.setSettlementAmount(mechanOperation.getTermPrice().multiply(accountOperation.getStockQuantity()));
			}
			
			/**VERIFICAMOS SI EXISTEN HECHOS DE MERCADO*/
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				/**CONFIGURAMOS LOS HECHOS DE MERCADO*/
				for(AccountTradeMarketFact accTradeMarketFact: accountTradeRequest.getAccountSirtexMarkectfacts()){
					AccountOperationMarketFact accountMarkFact = new AccountOperationMarketFact();
					accountMarkFact.setMarketDate(accTradeMarketFact.getMarketDate());
					accountMarkFact.setMarketRate(accTradeMarketFact.getMarketRate());
					accountMarkFact.setMarketPrice(accTradeMarketFact.getMarketPrice());
					accountMarkFact.setOperationQuantity(accTradeMarketFact.getMarketQuantity());
					accountMarkFact.setHolderAccountOperation(accountOperation);
					accountOperation.getAccountOperationMarketFacts().add(accountMarkFact);
				}
			}
			mechanOperation.getHolderAccountOperations().add(accountOperation);
		}
		return mechanOperation;
	}
	
	/**
	 * Convert trade request from split.
	 *
	 * @param tradeRequest the trade request
	 * @param splitDetail the split detail
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest convertTradeRequestFromSplit(TradeRequest tradeRequest, SplitCouponOperation splitDetail) throws ServiceException{
		return tradeRequest;
	}

	/**
	 * Find trade operation id.
	 *
	 * @param idTradeRequestPk the id trade request pk
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	/*METHOD TO HANDLE ID_TRADE_OPERATION FROM TRADE_REQUEST*/
	public Long findTradeOperationId(Long idTradeRequestPk) throws ServiceException{
		// TODO Auto-generated method stub
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT trad.idTradeOperationPk FROM TradeOperation 				");
		querySQL.append("trad WHERE trad.tradeRequest.idTradeRequestPk = :tradeRequestCode 	");
		Query query = em.createQuery(querySQL.toString());
		query.setParameter("tradeRequestCode", idTradeRequestPk);
		return (Long) query.getSingleResult();
	}
	
	/**
	 * Find interest coupons.
	 *
	 * @param idSecurityCode the id security code
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer,ProgramInterestCoupon> findInterestCoupons(String idSecurityCode) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT 															 						");
		querySQL.append("	prog.idProgramInterestPk, prog.couponNumber, prog.interestRate, prog.paymentDate, 		");
		querySQL.append("	prog.stateProgramInterest, prog.experitationDate, prog.registryDate, prog.beginingDate	");
		querySQL.append("FROM ProgramInterestCoupon prog 															");
		querySQL.append("WHERE 	prog.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityPar				");
		querySQL.append("And   	prog.stateProgramInterest = :pendintState 											");
		querySQL.append("And 	Trunc(prog.paymentDate) > trunc(:currentDate)										");
		querySQL.append("ORDER BY prog.couponNumber ASC																");
		Query query = em.createQuery(querySQL.toString());
		query.setParameter("idSecurityPar", idSecurityCode);
		query.setParameter("pendintState", ProgramScheduleStateType.PENDING.getCode());
		query.setParameter("currentDate", CommonsUtilities.currentDate());
		
		Map<Integer,ProgramInterestCoupon> mapInterest = new HashMap<Integer,ProgramInterestCoupon>();
		try{
			List<Object[]> objectDataList = query.getResultList(); 
			for(Object[] data: objectDataList){
				ProgramInterestCoupon programCoupon = new ProgramInterestCoupon();
				programCoupon.setIdProgramInterestPk((Long) data[0]);
				programCoupon.setCouponNumber((Integer) data[1]);
				programCoupon.setInterestRate((BigDecimal) data[2]);
				programCoupon.setPaymentDate((Date) data[3]);
				programCoupon.setStateProgramInterest((Integer) data[4]);
				programCoupon.setExperitationDate((Date) data[5]);
				programCoupon.setRegistryDate((Date) data[6]);
				programCoupon.setBeginingDate((Date) data[7]);
				mapInterest.put(programCoupon.getCouponNumber(), programCoupon);
			}
		}catch(NoResultException ex){
			throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		}
		return mapInterest;
	}
	
	/**
	 * Find operations trade request.
	 *
	 * @param idTradeRequest the id trade request
	 * @param operationPk the operation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> findOperationsTradeRequest(Long idTradeRequest, Long operationPk) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT new com.pradera.model.negotiation.MechanismOperation(mo.idMechanismOperationPk,mo.operationState)	");
		querySQL.append("FROM MechanismOperation mo 																			");
		querySQL.append("WHERE mo.tradeOperation.tradeRequest.idTradeRequestPk = :idTradReqPar									");
		querySQL.append("AND   mo.operationState = :cashSettState																");
		if(operationPk!=null){
			querySQL.append("AND   mo.idMechanismOperationPk = :operationPk														");
		}
		querySQL.append("ORDER BY mo.idMechanismOperationPk ASC																	");
		Query query = em.createQuery(querySQL.toString());
		query.setParameter("idTradReqPar", idTradeRequest);
		query.setParameter("cashSettState", MechanismOperationStateType.CASH_SETTLED.getCode());
		if(operationPk!=null){
			query.setParameter("operationPk", operationPk);
		}
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the settlement request.
	 *
	 * @param operation the operation
	 * @return the settlement request
	 * @throws ServiceException the service exception
	 */
	public SettlementRequest getSettlementRequest(MechanismOperation operation) throws ServiceException{
		Long operationPk = operation.getIdMechanismOperationPk();
		MechanismOperation sirtexOperation = sirtexSettlementService.find(MechanismOperation.class, operationPk);
		Participant partSeller = sirtexOperation.getSellerParticipant();
		Participant partBuyer  = sirtexOperation.getBuyerParticipant();
		SettlementOperation settTermOperation = (SettlementOperation) settlementService.getSettlementOperation(operationPk, ComponentConstant.TERM_PART);
		Long settOperationPk = settTermOperation.getIdSettlementOperationPk();
		
		SettlementRequest settlementRequest = new SettlementRequest();
		settlementRequest.setSourceParticipant(partBuyer);
		settlementRequest.setTargetParticipant(partSeller);
		settlementRequest.setRequestType(SettlementRequestType.SETTLEMENT_ANTICIPATION.getCode());
		settlementRequest.setRequestState(SettlementDateRequestStateType.CONFIRMED.getCode());
		settlementRequest.setLstSettlementDateOperations(new ArrayList<SettlementDateOperation>());
		settlementRequest.getLstSettlementDateOperations().add(settlementDateOperation(settlementRequest, settOperationPk));
		settlementRequest.setParticipantRole(ComponentConstant.SALE_ROLE);
		
		return settlementRequest;
	}
	
	/**
	 * Settlement date operation.
	 *
	 * @param settlementRequest the settlement request
	 * @param settOperationPk the sett operation pk
	 * @return the settlement date operation
	 * @throws ServiceException the service exception
	 */
	public SettlementDateOperation settlementDateOperation(SettlementRequest settlementRequest, Long settOperationPk) throws ServiceException{
		SettlementOperation settlementOperation = find(SettlementOperation.class, settOperationPk);
		
		Date newTermSettDate = CommonsUtilities.currentDate();
		Integer anticipationDays = CommonsUtilities.getDaysBetween(newTermSettDate, settlementOperation.getSettlementDate());
		
		SettlementDateOperation settDateOperation = new SettlementDateOperation();
		settDateOperation.setSettlementOperation(new SettlementOperation(settOperationPk));
		settDateOperation.setOperationPart(settlementOperation.getOperationPart());
		settDateOperation.setSettlementAmount(settlementOperation.getSettlementAmount());
		settDateOperation.setSettlementPrice(settlementOperation.getSettlementPrice());
		settDateOperation.setSettlementRequest(settlementRequest);
		settDateOperation.setSettlementDate(CommonsUtilities.currentDate());
		settDateOperation.setSettlementDays(anticipationDays.longValue());
		settDateOperation.setStockQuantity(settlementOperation.getStockQuantity());
		settDateOperation.setSettlementType(settlementOperation.getSettlementType());
		
		return settDateOperation;
	}
	
	/**
	 * Update stock settlement account operation.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @param idReferenceStock the id reference stock
	 * @return the int
	 */
	public int updateStockSettlementAccountOperation(Long idHolderAccountOperation, Long idReferenceStock)
	{			
		StringBuilder strQuery = new StringBuilder();
		
		strQuery.append(" UPDATE SettlementAccountOperation SET stockReference = :idReferenceStock, stockBlockDate= :updateDate ");
		strQuery.append(" WHERE holderAccountOperation.idHolderAccountOperationPk = :idSettlementAccountOperation ");
		
		Query query = em.createQuery(strQuery.toString());
		
		query.setParameter("idReferenceStock", idReferenceStock);
		query.setParameter("idSettlementAccountOperation", idHolderAccountOperation);
		query.setParameter("updateDate", CommonsUtilities.currentDateTime());
		
		return query.executeUpdate();	
	}
	
		
	@SuppressWarnings("unchecked")
	public List<String> getSirtexReportingOperation(Date processDate) throws ServiceException{		
   		try {
   			StringBuilder sbQuery = new StringBuilder();
   			sbQuery.append(" select distinct sec.idSecurityCodePk From TradeRequest tr ");
   			sbQuery.append(" inner join tr.securities sec ");
   			sbQuery.append("  WHERE 1=1 ");
   			sbQuery.append(" and tr.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk=5 ");
   			sbQuery.append(" and tr.mechanisnModality.negotiationModality.idNegotiationModalityPk=3 ");
   			
   			
   			if(Validations.validateIsNotNullAndNotEmpty( processDate )){
   				sbQuery.append(" and trunc(tr.termSettlementDate) = :parmTermSettlementDate ");
   			}	
   			Query query = em.createQuery(sbQuery.toString());    	    	
   			if(Validations.validateIsNotNullAndNotEmpty( processDate )){
   				query.setParameter("parmTermSettlementDate",  processDate);
   			}
   			   			   			
   	    	return  ((List<String>)query.getResultList());
   		} catch(NoResultException ex){
   			return null;
   		}		
   	}


}
	