package com.pradera.negotiations.sirtex.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.batch.SalePurchaseUpdateBatch;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.TradeSettlementRequest;
import com.pradera.model.settlement.type.SettlementAnticipationType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementRequestType;
import com.pradera.model.settlement.type.TradeSettlementRequestStateType;
import com.pradera.model.settlement.type.TradeSettlementRequestType;
import com.pradera.negotiations.otcoperations.service.OtcOperationServiceBean;
import com.pradera.negotiations.sirtex.to.SirtexRenewOperationTO;
import com.pradera.negotiations.sirtex.to.SirtexRenewResultTO;
import com.pradera.settlements.core.service.SettlementProcessService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RenewSirtexService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class RenewSirtexService extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sale purchase update batch. */
	@Inject @BatchProcess(name="SalePurchaseUpdateBatch")
	private SalePurchaseUpdateBatch salePurchaseUpdateBatch;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/** The sirtex settlement service. */
	@EJB private OtcAndSirtexSettlementService sirtexSettlementService;
	
	/** The otc operation service bean. */
	@EJB private OtcOperationServiceBean otcOperationServiceBean;
	
	/** The settlement service. */
	@EJB private SettlementProcessService settlementService;
	
	/** The user info. */
	@Inject private UserInfo userInfo;

	/**
	 * Find negotiation modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param participantPk the participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> findNegotiationModality(Long mechanismPk, Long participantPk) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Select neg From NegotiationModality 		neg				");
		stringBuilder.append("Inner Join neg.mechanismModalities		mod				");
		stringBuilder.append("Inner Join mod.negotiationMechanism 		mech			");
		
		if(participantPk!=null){
			stringBuilder.append("Inner Join mod.participantMechanisms 		part	");
		}
		
		stringBuilder.append("Where mech.idNegotiationMechanismPk = :mechanismPk And	");
		stringBuilder.append("		mod.stateMechanismModality = :registerState	 And	");
		stringBuilder.append("		neg.indTermSettlement = :oneParam					");
		
		if(participantPk!=null){
			stringBuilder.append("And part.participant.idParticipantPk = :partPk		");
			stringBuilder.append("And part.stateParticipantMechanism = :activeState		");
		}
		
		Query query = em.createQuery(stringBuilder.toString());
		query.setParameter("mechanismPk", mechanismPk);
		query.setParameter("registerState", MechanismModalityStateType.ACTIVE.getCode());
		query.setParameter("oneParam", BooleanType.YES.getCode());
		
		if(participantPk!=null){
			query.setParameter("partPk", participantPk);
			query.setParameter("activeState", ParticipantMechanismStateType.REGISTERED.getCode());
		}
		
		try{
			return query.getResultList();			
		}catch(NoResultException e){
			return new ArrayList<>();
		}
	}
	
	/**
	 * Find negotiation mechanism.
	 *
	 * @param mechanismPk the mechanism pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> findNegotiationMechanism(Long mechanismPk) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Select neg From NegotiationMechanism 		neg				");
		stringBuilder.append("Where neg.idNegotiationMechanismPk = :mechanismPk 		");
		Query query = em.createQuery(stringBuilder.toString());
		query.setParameter("mechanismPk", mechanismPk);

		try{
			return query.getResultList();			
		}catch(NoResultException e){
			return new ArrayList<>();
		}
	}
	
	/**
	 * Gets the participant by mechanism modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param modalityPk the modality pk
	 * @return the participant by mechanism modality
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipantByMechanismModality(Long mechanismPk, Long modalityPk) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Select part From Participant 					part			");
		stringBuilder.append("Inner Join part.participantMechanisms  		mech			");
		stringBuilder.append("Inner Join mech.mechanismModality 			mod				");
		stringBuilder.append("Where mod.id.idNegotiationMechanismPk = :mechanismPk And		");
		stringBuilder.append("		mod.id.idNegotiationModalityPk 	= :modalityPk 			");
		
		Query query = em.createQuery(stringBuilder.toString());
		query.setParameter("mechanismPk", mechanismPk);
		query.setParameter("modalityPk", modalityPk);
		
		try{
			return query.getResultList();
		}catch(NoResultException e){
			return new ArrayList<>();
		}
	}
	
	/**
	 * Gets the trade request by participant.
	 *
	 * @param participantPk the participant pk
	 * @param modalityPk the modality pk
	 * @param mechanismPk the mechanism pk
	 * @return the trade request by participant
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<TradeRequest> getTradeRequestByParticipant(Long participantPk, Long modalityPk, Long mechanismPk) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Select Distinct trad From TradeRequest 				trad		");
		stringBuilder.append("Inner Join trad.tradeOperationList 			tradOpe				");
		stringBuilder.append("Inner Join Fetch trad.securities 				sec					");
		stringBuilder.append("Inner Join trad.mechanisnModality 			mmod				");
		stringBuilder.append("Where mmod.id.idNegotiationMechanismPk = :mechanismPk 		And	");
		stringBuilder.append("		mmod.id.idNegotiationModalityPk 	= :modalityPk  		And	");
		stringBuilder.append("	   (trad.sellerParticipant.idParticipantPk = :partPk  	Or		");
		stringBuilder.append("	    trad.buyerParticipant.idParticipantPk  = :partPk  )	And		");
		stringBuilder.append("	    trad.operationState  = :confirmedState  			And		");
		stringBuilder.append("		tradOpe.idTradeOperationPk In(								");
		
		StringBuilder subQuery = new StringBuilder();
		subQuery.append("Select mo.idMechanismOperationPk From MechanismOperation mo			");
		subQuery.append("Inner Join mo.mechanisnModality   mmod									");
		subQuery.append("Where mmod.id.idNegotiationMechanismPk = :mechanismPk 		And			");
		subQuery.append("	   mmod.id.idNegotiationModalityPk 	= :modalityPk  		And			");
		subQuery.append("	   mo.operationState = :settledState								");
		
		stringBuilder.append(subQuery.toString()).append("	)									");
		stringBuilder.append("Order BY trad.idTradeRequestPk Desc 								");	

		Query query = em.createQuery(stringBuilder.toString());
		
		query.setParameter("mechanismPk", mechanismPk);
		query.setParameter("modalityPk", modalityPk);
		query.setParameter("partPk", participantPk);
		query.setParameter("confirmedState", SirtexOperationStateType.CONFIRMED.getCode());
		query.setParameter("settledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		
		try{
			return query.getResultList();
		}catch(NoResultException e){
			return null;
		}
	}
	
	/**
	 * Gets the settlement operations.
	 *
	 * @param tradeRequestPk the trade request pk
	 * @param operationPart the operation part
	 * @return the settlement operations
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementOperation> getSettlementOperations(Long tradeRequestPk, Integer operationPart) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Select settOp From SettlementOperation 		settOp				");
		stringBuilder.append("Inner Join Fetch settOp.mechanismOperation 	mech				");
		stringBuilder.append("Inner Join Fetch mech.securities				secu				");
		stringBuilder.append("Inner Join mech.tradeOperation 				tradOp				");
		stringBuilder.append("Inner Join tradOp.tradeRequest 				tradRq				");
		
		stringBuilder.append("Where tradRq.idTradeRequestPk = :tradeRequestPk	And				");
		stringBuilder.append("		mech.indTermSettlement 	= :oneParam			And				");
		stringBuilder.append("		mech.operationState 	= :cashSettled		And				");
		stringBuilder.append("		settOp.operationPart    = :operationPart	And				");
		stringBuilder.append("		settOp.operationState 	= :cashSettled						");
		
		Query query = em.createQuery(stringBuilder.toString());
		query.setParameter("tradeRequestPk", tradeRequestPk);
		query.setParameter("oneParam", BooleanType.YES.getCode());
		query.setParameter("cashSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
		query.setParameter("operationPart", operationPart);
		
		try{
			return query.getResultList();
		}catch(NoResultException e){
			return null;
		}
	}
	
	/**
	 * Gets the settlement date operation.
	 *
	 * @param settRequest the sett request
	 * @param settOperation the sett operation
	 * @return the settlement date operation
	 * @throws ServiceException the service exception
	 */
	public List<SettlementDateOperation> getSettlementDateOperation(SettlementRequest settRequest, SettlementOperation settOperation) throws ServiceException{		
		List<SettlementDateOperation> settDateOperationLst = new ArrayList<>();
		SettlementDateOperation settDateOperation = new SettlementDateOperation();
		settDateOperation.setSettlementOperation(settOperation);
		settDateOperation.setSettlementRequest(settRequest);
		settDateOperation.setSettlementDate(CommonsUtilities.currentDateTime());
		settDateOperation.setOperationPart(settOperation.getOperationPart());
		settDateOperationLst.add(settDateOperation);
		return settDateOperationLst;
	}
	
	/**
	 * Gets the settlement request list.
	 *
	 * @param tradeReqPk the trade req pk
	 * @param tradSettRequest the trad sett request
	 * @param opPart the op part
	 * @return the settlement request list
	 * @throws ServiceException the service exception
	 */
	public List<SettlementRequest> getSettlementRequestList(Long tradeReqPk, TradeSettlementRequest tradSettRequest, Integer opPart) throws ServiceException{		
		List<SettlementRequest> lstSettlementRequest = new ArrayList<>();
		
		for(SettlementOperation settOperation: getSettlementOperations(tradeReqPk, opPart)){
			SettlementRequest settlementRequest = getSettlementRequest(tradSettRequest, SettlementRequestType.SETTLEMENT_EXTENSION.getCode());
			settlementRequest.setLstSettlementDateOperations(getSettlementDateOperation(settlementRequest, settOperation));			
			lstSettlementRequest.add(settlementRequest);
		}
		return lstSettlementRequest;
	}
	
	/**
	 * Gets the settlement request.
	 *
	 * @param tradSettlRequest the trad settl request
	 * @param settRequestType the sett request type
	 * @return the settlement request
	 */
	public SettlementRequest getSettlementRequest(TradeSettlementRequest tradSettlRequest, Integer settRequestType){
		SettlementRequest settlementReq = new SettlementRequest();
		settlementReq.setTradeSettlementRequest(tradSettlRequest);
		settlementReq.setRequestType(settRequestType);
		settlementReq.setRequestState(SettlementDateRequestStateType.REGISTERED.getCode());
		settlementReq.setRegisterUser(userInfo.getUserAccountSession().getUserName());
		settlementReq.setRegisterDate(CommonsUtilities.currentDateTime());
		settlementReq.setSourceParticipant(tradSettlRequest.getSourceParticipant());
		settlementReq.setTargetParticipant(tradSettlRequest.getTargetParticipant());
		return settlementReq;
	}
	
	/**
	 * Gets the sirtex renew operation.
	 *
	 * @param sirtexRenewTO the sirtex renew to
	 * @return the sirtex renew operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SirtexRenewResultTO> getSirtexRenewOperation(SirtexRenewOperationTO sirtexRenewTO) throws ServiceException{
		Long participantPk = sirtexRenewTO.getParticipantPk();
		Integer state = sirtexRenewTO.getState();
		Date initialDate = sirtexRenewTO.getInitialDate();
		Date finalDate = sirtexRenewTO.getFinalDate();
		Integer requestType = sirtexRenewTO.getRequestType();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Select tradSett.idTradeSettlementRequestPk, tradSett.requestState,				");
		stringBuilder.append("		 tradSett.registerDate, securit.idSecurityCodePk, tradReq.idTradeRequestPk,	");
		stringBuilder.append("		 srcPart.idParticipantPk, srcPart.mnemonic, srcPart.description,			");
		stringBuilder.append("		 trgPart.idParticipantPk, trgPart.mnemonic, trgPart.description,			");
		stringBuilder.append("		 tradSett.registerUser														");
		stringBuilder.append("From TradeSettlementRequest 						tradSett						");
		stringBuilder.append("Inner Join tradSett.tradeRequest					tradReq							");
		stringBuilder.append("Inner Join tradReq.securities						securit							");
		stringBuilder.append("Inner Join tradSett.sourceParticipant				srcPart							");
		stringBuilder.append("Inner Join tradSett.targetParticipant				trgPart							");
		
		stringBuilder.append("Where 1 = 1 																		");
		
		if(participantPk!=null){
			stringBuilder.append("And	(srcPart.idParticipantPk = :participantPk	Or							");
			stringBuilder.append("		 trgPart.idParticipantPk = :participantPk		)						");
		}
		
		if(state!=null){
			stringBuilder.append("And	tradSett.requestState = :state											");
		}
		
		if(requestType!=null){
			stringBuilder.append("And	tradSett.requestType = :requestType										");
		}
		
		stringBuilder.append("And 	tradSett.registerDate >= :initDate 	And tradSett.registerDate <= :finDate	");
		
		Query query = em.createQuery(stringBuilder.toString());
		
		if(participantPk!=null){
			query.setParameter("participantPk", participantPk);
		}
		
		if(state!=null){
			query.setParameter("state", state);
		}
		
		if(requestType!=null){
			query.setParameter("requestType", requestType);
		}
		
		query.setParameter("initDate", initialDate);
		query.setParameter("finDate", finalDate);
		
		List<Object[]> dataList = query.getResultList();
		List<SirtexRenewResultTO> sirtexRenewList = new ArrayList<SirtexRenewResultTO>();
		
		for(Object[] data: dataList){
			SirtexRenewResultTO sirtexRenew = new SirtexRenewResultTO();
			sirtexRenew.setRenewOperationPk((Long) data[0]);
			sirtexRenew.setState((Integer) data[1]);
			sirtexRenew.setRegisterDate((Date) data[2]);
			sirtexRenew.setSecurityCodePk((String) data[3]);
			sirtexRenew.setTradeRequestFk((Long) data[4]);
			
			sirtexRenew.setSrcParticipant(new Participant());
			sirtexRenew.getSrcParticipant().setIdParticipantPk((Long) data[5]);
			sirtexRenew.getSrcParticipant().setMnemonic((String) data[6]);
			sirtexRenew.getSrcParticipant().setDescription((String) data[7]);
			
			sirtexRenew.setTrgParticipant(new Participant());
			sirtexRenew.getTrgParticipant().setIdParticipantPk((Long) data[8]);
			sirtexRenew.getTrgParticipant().setMnemonic((String) data[9]);
			sirtexRenew.getTrgParticipant().setDescription((String) data[10]);
			sirtexRenew.setRegistryUser((String) data[11]);
			
			sirtexRenewList.add(sirtexRenew);
		}
				
		return sirtexRenewList;
	}

	/**
	 * *
	 * Method to register TradeSettlementRequest and all dependencies.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	public TradeSettlementRequest saveTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		TradeRequest tradeRequest = find(TradeRequest.class, tradeSettRequest.getTradeRequest().getIdTradeRequestPk());
		Participant participantSrc = tradeSettRequest.getSourceParticipant();
		Integer role = null;
		
		/**Find participant Target*/
		if(participantSrc.getIdParticipantPk().equals(tradeRequest.getBuyerParticipant().getIdParticipantPk())){
			tradeSettRequest.setTargetParticipant(tradeRequest.getSellerParticipant());
			role = ComponentConstant.PURCHARSE_ROLE;
		}else if(participantSrc.getIdParticipantPk().equals(tradeRequest.getSellerParticipant().getIdParticipantPk())){
			tradeSettRequest.setTargetParticipant(tradeRequest.getBuyerParticipant());
			role = ComponentConstant.SALE_ROLE;
		}
		tradeSettRequest.setRequestState(TradeSettlementRequestStateType.REGISTERED.getCode());		
		
		/**Create Settlement Request*/
		create(tradeSettRequest);
		
		for(SettlementRequest settRequest: tradeSettRequest.getLstSettlementRequest()){
			/**Put participant source and target in request*/
			settRequest.setSourceParticipant(tradeSettRequest.getSourceParticipant());
			settRequest.setTargetParticipant(tradeSettRequest.getTargetParticipant());
			settRequest.setParticipantRole(role);
			
//			if(tradeSettRequest.getRequestType().equals(TradeSettlementRequestType.RENOVATION.getCode())){
			boolean isCheck = false;
			for(SettlementDateOperation settDateOperation: settRequest.getLstSettlementDateOperations()){
				if(settDateOperation.isSelected()){
					isCheck = true;
					
					if(tradeSettRequest.getRequestType().equals(TradeSettlementRequestType.RENOVATION.getCode())){
						/**Get data from SettlementOperation*/
						SettlementOperation settlementOperation = settDateOperation.getSettlementOperation();
						settDateOperation.setOperationPart(settlementOperation.getOperationPart());
						BigDecimal stockQuantity = settDateOperation.getStockQuantity();
						BigDecimal initStockQuant = settlementOperation.getMechanismOperation().getStockQuantity();
						
						/**If quantity are different anticipation must be partial*/
						if(!stockQuantity.equals(initStockQuant)){
							settRequest.setAnticipationType(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode());
						}
					}
				}
			}
			if(isCheck){
				create(settRequest);
				saveAll(settRequest.getLstSettlementDateOperations());
				isCheck = false;
			}
//			}else{
//				create(settRequest);
//				for(SettlementDateOperation settDateOperation: settRequest.getLstSettlementDateOperations()){
//					if(settDateOperation.isSelected()){
//						create(settDateOperation);
//					}
//				}
		}
//		}
		
		return tradeSettRequest;
	}
	
	/**
	 * Approve trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	public TradeSettlementRequest approveTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		Long key = tradeSettRequest.getIdTradeSettlementRequestPk();
		TradeSettlementRequest tradeSettlementReq = find(TradeSettlementRequest.class, key);
		
		if(tradeSettlementReq.getRequestState().equals(TradeSettlementRequestStateType.REGISTERED.getCode())){
			tradeSettlementReq.setRequestState(TradeSettlementRequestStateType.APPROVE.getCode());
		}else{
			throw new ServiceException();
		}
		
		update(tradeSettlementReq);
		return tradeSettlementReq;
	}
	
	/**
	 * Annular trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	public TradeSettlementRequest annularTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		Long key = tradeSettRequest.getIdTradeSettlementRequestPk();
		TradeSettlementRequest tradeSettlementReq = find(TradeSettlementRequest.class, key);
		
		if(tradeSettlementReq.getRequestState().equals(TradeSettlementRequestStateType.REGISTERED.getCode())){
			tradeSettlementReq.setRequestState(TradeSettlementRequestStateType.ANNULATE.getCode());
		}else{
			throw new ServiceException();
		}
		
		update(tradeSettlementReq);
		return tradeSettlementReq;
	}
	
	/**
	 * Revisar trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	public TradeSettlementRequest revisarTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		Long key = tradeSettRequest.getIdTradeSettlementRequestPk();
		TradeSettlementRequest tradeSettlementReq = find(TradeSettlementRequest.class, key);
		
		if(tradeSettlementReq.getRequestState().equals(TradeSettlementRequestStateType.APPROVE.getCode())){
			tradeSettlementReq.setRequestState(TradeSettlementRequestStateType.REVIEW.getCode());
		}else{
			throw new ServiceException();
		}
		
		update(tradeSettlementReq);
		return tradeSettlementReq;
	}
	
	/**
	 * Confirm trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	public TradeSettlementRequest confirmTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		Long key = tradeSettRequest.getIdTradeSettlementRequestPk();
		TradeSettlementRequest tradeSettlementReq = find(TradeSettlementRequest.class, key);
		
		if(tradeSettlementReq.getRequestState().equals(TradeSettlementRequestStateType.REVIEW.getCode())){
			tradeSettlementReq.setRequestState(TradeSettlementRequestStateType.CONFIRM.getCode());
		}else{
			throw new ServiceException();
		}
		
		update(tradeSettlementReq);
		return tradeSettlementReq;
	}
	
	/**
	 * Reject trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	public TradeSettlementRequest rejectTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		Long key = tradeSettRequest.getIdTradeSettlementRequestPk();
		TradeSettlementRequest tradeSettlementReq = find(TradeSettlementRequest.class, key);
		
		if(tradeSettlementReq.getRequestState().equals(TradeSettlementRequestStateType.APPROVE.getCode()) || 
		   tradeSettlementReq.getRequestState().equals(TradeSettlementRequestStateType.REVIEW.getCode())){
			tradeSettlementReq.setRequestState(TradeSettlementRequestStateType.REJECT.getCode());
		}else{
			throw new ServiceException();
		}
		
		update(tradeSettlementReq);
		return tradeSettlementReq;
	}
}