package com.pradera.negotiations.sirtex.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.accounts.Participant;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class SirtexRenewResultTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The state. */
	private Integer state;
	
	/** The state description. */
	private String stateDescription;
	
	/** The renew operation pk. */
	private Long renewOperationPk;
	
	/** The security code pk. */
	private String securityCodePk;
	
	/** The src participant. */
	private Participant srcParticipant;
	
	/** The trg participant. */
	private Participant trgParticipant;
	
	/** The register date. */
	private Date registerDate;
	
	/** The trade request fk. */
	private Long tradeRequestFk;
	
	/** The registry user. */
	private String registryUser;
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the renew operation pk.
	 *
	 * @return the renew operation pk
	 */
	public Long getRenewOperationPk() {
		return renewOperationPk;
	}
	
	/**
	 * Sets the renew operation pk.
	 *
	 * @param renewOperationPk the new renew operation pk
	 */
	public void setRenewOperationPk(Long renewOperationPk) {
		this.renewOperationPk = renewOperationPk;
	}
	
	/**
	 * Gets the security code pk.
	 *
	 * @return the security code pk
	 */
	public String getSecurityCodePk() {
		return securityCodePk;
	}
	
	/**
	 * Sets the security code pk.
	 *
	 * @param securityCodePk the new security code pk
	 */
	public void setSecurityCodePk(String securityCodePk) {
		this.securityCodePk = securityCodePk;
	}
	
	/**
	 * Gets the src participant.
	 *
	 * @return the src participant
	 */
	public Participant getSrcParticipant() {
		return srcParticipant;
	}
	
	/**
	 * Sets the src participant.
	 *
	 * @param srcParticipant the new src participant
	 */
	public void setSrcParticipant(Participant srcParticipant) {
		this.srcParticipant = srcParticipant;
	}
	
	/**
	 * Gets the trg participant.
	 *
	 * @return the trg participant
	 */
	public Participant getTrgParticipant() {
		return trgParticipant;
	}
	
	/**
	 * Sets the trg participant.
	 *
	 * @param trgParticipant the new trg participant
	 */
	public void setTrgParticipant(Participant trgParticipant) {
		this.trgParticipant = trgParticipant;
	}
	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the trade request fk.
	 *
	 * @return the trade request fk
	 */
	public Long getTradeRequestFk() {
		return tradeRequestFk;
	}
	
	/**
	 * Sets the trade request fk.
	 *
	 * @param tradeRequestFk the new trade request fk
	 */
	public void setTradeRequestFk(Long tradeRequestFk) {
		this.tradeRequestFk = tradeRequestFk;
	}
	
	/**
	 * Gets the registry user.
	 *
	 * @return the registryUser
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registryUser to set
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
}