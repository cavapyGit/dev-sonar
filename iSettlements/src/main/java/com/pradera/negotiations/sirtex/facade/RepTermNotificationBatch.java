package com.pradera.negotiations.sirtex.facade;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.settlements.utils.PropertiesConstants;

//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2015.</li>
* </ul>
* 
* The Class ValuatorExecutionBatch.
*
* @author PraderaTechnologies.
* @version 1.0 , 20-ago-2015
*/
@BatchProcess(name = "RepTermNotificationBatch")
@RequestScoped
public class RepTermNotificationBatch implements JobExecution, Serializable{
	@EJB private SirtexOperationFacade sirtexOperationFacade;
	@EJB private NotificationServiceFacade notificationServiceFacade;
	
	@Inject @Configurable String idParticipantBC;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		String quantity = GeneralConstants.EMPTY_STRING;
		String security = GeneralConstants.EMPTY_STRING;
		// TODO Auto-generated method stub
		Date processDate = CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate());
		List<String> lstTradeRequest;
		try {
			lstTradeRequest = sirtexOperationFacade.getSirtexReportingOperation( processDate);
			if(Validations.validateListIsNotNullAndNotEmpty(lstTradeRequest)){
				for(String strSecurity:lstTradeRequest){
					if(GeneralConstants.EMPTY_STRING.compareTo(security)==0){
						security = strSecurity;
					} else {
						security = security + ", "+ strSecurity;
					}
					
				}
				quantity = String.valueOf(lstTradeRequest.size());
				
				Object[] parameters = {	
						quantity,
						security
				};
				
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.PROCESS_NOTIFICATION_OF_REPORTED_SIRTEX.getCode());
				
				notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), 
						businessProcess, new Long(idParticipantBC), parameters);
		
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	

}
