package com.pradera.negotiations.sirtex.view;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.settlement.TradeSettlementRequest;
import com.pradera.negotiations.sirtex.to.SirtexRenewOperationTO;
import com.pradera.negotiations.sirtex.to.SirtexRenewResultTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SirtexRenewManagementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SirtexRenewManagementTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The trade settlement request. */
	private TradeSettlementRequest tradeSettlementRequest;
	
	/** The all participants. */
	private List<Participant> allParticipants;
	
	/** The lst trade sett request state. */
	private List<ParameterTable> tradeSettlementRequestType, lstTermSettlementDays, lstTradeSettRequestState;
	
	/** The modality list. */
	private List<NegotiationModality> modalityList;
	
	/** The mechanism list. */
	private List<NegotiationMechanism> mechanismList;
	
	/** The modality selected. */
	private NegotiationModality modalitySelected;
	
	/** The trade request list. */
	private List<TradeRequest> tradeRequestList;
	
	/** The trade request selected. */
	private TradeRequest tradeRequestSelected;
	
	/** The min term days. */
	private Long minTermDays;
	
	/** The max term days. */
	private Long maxTermDays;
	
	/** The renew operation to. */
	private SirtexRenewOperationTO renewOperationTO;
	
	/** The lst renew sirtex. */
	private List<SirtexRenewResultTO> lstRenewSirtex;
	
	/** The renew sirtes selected. */
	private SirtexRenewResultTO renewSirtesSelected;
	
	/** The lst renew sirtex model. */
	private GenericDataModel<SirtexRenewResultTO> lstRenewSirtexModel;
	
	/** The mechanism selected. */
	private Long mechanismSelected;
	
	/** The ind active. */
	private boolean indActive;
	
	/**
	 * Instantiates a new sirtex renew management to.
	 */
	public SirtexRenewManagementTO() {
		super();
	}

	/**
	 * Gets the trade settlement request.
	 *
	 * @return the trade settlement request
	 */
	public TradeSettlementRequest getTradeSettlementRequest() {
		return tradeSettlementRequest;
	}

	/**
	 * Sets the trade settlement request.
	 *
	 * @param tradeSettlementRequest the new trade settlement request
	 */
	public void setTradeSettlementRequest(TradeSettlementRequest tradeSettlementRequest) {
		this.tradeSettlementRequest = tradeSettlementRequest;
	}

	/**
	 * Gets the all participants.
	 *
	 * @return the all participants
	 */
	public List<Participant> getAllParticipants() {
		return allParticipants;
	}

	/**
	 * Sets the all participants.
	 *
	 * @param allParticipants the new all participants
	 */
	public void setAllParticipants(List<Participant> allParticipants) {
		this.allParticipants = allParticipants;
	}

	/**
	 * Gets the trade settlement request type.
	 *
	 * @return the trade settlement request type
	 */
	public List<ParameterTable> getTradeSettlementRequestType() {
		return tradeSettlementRequestType;
	}

	/**
	 * Sets the trade settlement request type.
	 *
	 * @param tradeSettlementRequestType the new trade settlement request type
	 */
	public void setTradeSettlementRequestType(List<ParameterTable> tradeSettlementRequestType) {
		this.tradeSettlementRequestType = tradeSettlementRequestType;
	}

	/**
	 * Gets the modality list.
	 *
	 * @return the modality list
	 */
	public List<NegotiationModality> getModalityList() {
		return modalityList;
	}

	/**
	 * Sets the modality list.
	 *
	 * @param modalityList the new modality list
	 */
	public void setModalityList(List<NegotiationModality> modalityList) {
		this.modalityList = modalityList;
	}

	/**
	 * Gets the trade request list.
	 *
	 * @return the trade request list
	 */
	public List<TradeRequest> getTradeRequestList() {
		return tradeRequestList;
	}

	/**
	 * Sets the trade request list.
	 *
	 * @param tradeRequestList the new trade request list
	 */
	public void setTradeRequestList(List<TradeRequest> tradeRequestList) {
		this.tradeRequestList = tradeRequestList;
	}

	/**
	 * Gets the trade request selected.
	 *
	 * @return the trade request selected
	 */
	public TradeRequest getTradeRequestSelected() {
		return tradeRequestSelected;
	}

	/**
	 * Sets the trade request selected.
	 *
	 * @param tradeRequestSelected the new trade request selected
	 */
	public void setTradeRequestSelected(TradeRequest tradeRequestSelected) {
		this.tradeRequestSelected = tradeRequestSelected;
	}

	/**
	 * Gets the modality selected.
	 *
	 * @return the modality selected
	 */
	public NegotiationModality getModalitySelected() {
		return modalitySelected;
	}

	/**
	 * Sets the modality selected.
	 *
	 * @param modalitySelected the new modality selected
	 */
	public void setModalitySelected(NegotiationModality modalitySelected) {
		this.modalitySelected = modalitySelected;
	}

	/**
	 * Gets the mechanism list.
	 *
	 * @return the mechanism list
	 */
	public List<NegotiationMechanism> getMechanismList() {
		return mechanismList;
	}

	/**
	 * Sets the mechanism list.
	 *
	 * @param mechanismList the new mechanism list
	 */
	public void setMechanismList(List<NegotiationMechanism> mechanismList) {
		this.mechanismList = mechanismList;
	}

	/**
	 * Gets the lst term settlement days.
	 *
	 * @return the lst term settlement days
	 */
	public List<ParameterTable> getLstTermSettlementDays() {
		return lstTermSettlementDays;
	}

	/**
	 * Sets the lst term settlement days.
	 *
	 * @param lstTermSettlementDays the new lst term settlement days
	 */
	public void setLstTermSettlementDays(List<ParameterTable> lstTermSettlementDays) {
		this.lstTermSettlementDays = lstTermSettlementDays;
	}

	/**
	 * Gets the min term days.
	 *
	 * @return the min term days
	 */
	public Long getMinTermDays() {
		return minTermDays;
	}

	/**
	 * Sets the min term days.
	 *
	 * @param minTermDays the new min term days
	 */
	public void setMinTermDays(Long minTermDays) {
		this.minTermDays = minTermDays;
	}

	/**
	 * Gets the max term days.
	 *
	 * @return the max term days
	 */
	public Long getMaxTermDays() {
		return maxTermDays;
	}

	/**
	 * Sets the max term days.
	 *
	 * @param maxTermDays the new max term days
	 */
	public void setMaxTermDays(Long maxTermDays) {
		this.maxTermDays = maxTermDays;
	}

	/**
	 * Gets the lst trade sett request state.
	 *
	 * @return the lst trade sett request state
	 */
	public List<ParameterTable> getLstTradeSettRequestState() {
		return lstTradeSettRequestState;
	}

	/**
	 * Sets the lst trade sett request state.
	 *
	 * @param lstTradeSettRequestState the new lst trade sett request state
	 */
	public void setLstTradeSettRequestState(List<ParameterTable> lstTradeSettRequestState) {
		this.lstTradeSettRequestState = lstTradeSettRequestState;
	}

	/**
	 * Gets the renew operation to.
	 *
	 * @return the renew operation to
	 */
	public SirtexRenewOperationTO getRenewOperationTO() {
		return renewOperationTO;
	}

	/**
	 * Sets the renew operation to.
	 *
	 * @param renewOperationTO the new renew operation to
	 */
	public void setRenewOperationTO(SirtexRenewOperationTO renewOperationTO) {
		this.renewOperationTO = renewOperationTO;
	}

	/**
	 * Gets the lst renew sirtex.
	 *
	 * @return the lst renew sirtex
	 */
	public List<SirtexRenewResultTO> getLstRenewSirtex() {
		return lstRenewSirtex;
	}

	/**
	 * Sets the lst renew sirtex.
	 *
	 * @param lstRenewSirtex the new lst renew sirtex
	 */
	public void setLstRenewSirtex(List<SirtexRenewResultTO> lstRenewSirtex) {
		this.lstRenewSirtex = lstRenewSirtex;
	}

	/**
	 * Gets the lst renew sirtex model.
	 *
	 * @return the lst renew sirtex model
	 */
	public GenericDataModel<SirtexRenewResultTO> getLstRenewSirtexModel() {
		return lstRenewSirtexModel;
	}

	/**
	 * Sets the lst renew sirtex model.
	 *
	 * @param lstRenewSirtexModel the new lst renew sirtex model
	 */
	public void setLstRenewSirtexModel(
			GenericDataModel<SirtexRenewResultTO> lstRenewSirtexModel) {
		this.lstRenewSirtexModel = lstRenewSirtexModel;
	}

	/**
	 * Gets the renew sirtes selected.
	 *
	 * @return the renew sirtes selected
	 */
	public SirtexRenewResultTO getRenewSirtesSelected() {
		return renewSirtesSelected;
	}

	/**
	 * Sets the renew sirtes selected.
	 *
	 * @param renewSirtesSelected the new renew sirtes selected
	 */
	public void setRenewSirtesSelected(SirtexRenewResultTO renewSirtesSelected) {
		this.renewSirtesSelected = renewSirtesSelected;
	}

	/**
	 * Gets the mechanism selected.
	 *
	 * @return the mechanism selected
	 */
	public Long getMechanismSelected() {
		return mechanismSelected;
	}

	/**
	 * Sets the mechanism selected.
	 *
	 * @param mechanismSelected the new mechanism selected
	 */
	public void setMechanismSelected(Long mechanismSelected) {
		this.mechanismSelected = mechanismSelected;
	}

	/**
	 * Checks if is ind active.
	 *
	 * @return true, if is ind active
	 */
	public boolean isIndActive() {
		return indActive;
	}

	/**
	 * Sets the ind active.
	 *
	 * @param indActive the new ind active
	 */
	public void setIndActive(boolean indActive) {
		this.indActive = indActive;
	}
}