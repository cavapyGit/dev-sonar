package com.pradera.negotiations.sirtex.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.negotiations.sirtex.view.SirtexOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SirtexManagementTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SirtexManagementTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The sirtex operation. */
	private TradeRequest sirtexOperation;
	
	/** The account seller. */
	private HolderAccount accountSeller;
	
	/** The holder seller. */
	private Holder holderSeller;
	
	/** The lst security class bcb. */
	private List<ParameterTable> lstCboDateSearchType,lstCboOtcOperationStateType,lstCboOtcOriginRequestType,motiveCancelOperationList, 
								 lstSettlementType, lstSchemeType, lstSecurityClassBcb;
	
	/** The program interest coupon list. */
	private List<ProgramInterestCoupon> programInterestCouponList;
	
	/** The sirtex operation to. */
	private SirtexOperationTO sirtexOperationTO;
	
	/** The participant sell by modalities. */
	private Map<Long,List<Participant>> participantSellByModalities;//Me permite almacenar los participants disponibles por modalidad
	
	/** The participant buy by modalities. */
	private Map<Long,List<Participant>> participantBuyByModalities;//Me permite almacenar los participants disponibles por modalidad
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The participant buyers. */
	private List<Participant> participantBuyers;
	
	/** The participant sellers. */
	private List<Participant> participantSellers;
	
	/** The holder account buyers. */
	private List<HolderAccount> holderAccountBuyers;
	
	/** The holder account seller. */
	private List<HolderAccount> holderAccountSeller;
	
	/** The bl is split. */
	private Boolean blIsSplit;
	
	/** The market fact component ui. */
	private MarketFactBalanceHelpTO marketFactComponentUI;
	
	/** The all participant list. */
	/*ATRIBUTOS PARA LA CONSULTA*/
	private List<Participant> allParticipantList;
	
	/** The all participant map. */
	private Map<Long,Participant> allParticipantMap;
	
	/** The sirtex filters. */
	private SirtexOperationTO sirtexFilters;
	
	/** The modalities search list. */
	private List<MechanismModality> modalitiesSearchList;
	
	/** The sirtex data model. */
	private GenericDataModel<SirtexOperationResultTO> sirtexDataModel;
	
	/** The sirtex operation selected. */
	private List<SirtexOperationResultTO> sirtexOperationSelected;
	
    /** The market fact number. */
    private Integer marketFactNumber;
	
	/**
	 * Instantiates a new sirtex management to.
	 */
	public SirtexManagementTO() {
		super();
	}

	/** The mechanism modality list. */
	private List<MechanismModality> mechanismModalityList;

	/**
	 * Gets the sirtex operation.
	 *
	 * @return the sirtex operation
	 */
	public TradeRequest getSirtexOperation() {
		return sirtexOperation;
	}

	/**
	 * Sets the sirtex operation.
	 *
	 * @param sirtexOperation the new sirtex operation
	 */
	public void setSirtexOperation(TradeRequest sirtexOperation) {
		this.sirtexOperation = sirtexOperation;
	}

	/**
	 * Gets the mechanism modality list.
	 *
	 * @return the mechanism modality list
	 */
	public List<MechanismModality> getMechanismModalityList() {
		return mechanismModalityList;
	}

	/**
	 * Sets the mechanism modality list.
	 *
	 * @param mechanismModalityList the new mechanism modality list
	 */
	public void setMechanismModalityList(List<MechanismModality> mechanismModalityList) {
		this.mechanismModalityList = mechanismModalityList;
	}

	/**
	 * Gets the lst cbo date search type.
	 *
	 * @return the lst cbo date search type
	 */
	public List<ParameterTable> getLstCboDateSearchType() {
		return lstCboDateSearchType;
	}

	/**
	 * Sets the lst cbo date search type.
	 *
	 * @param lstCboDateSearchType the new lst cbo date search type
	 */
	public void setLstCboDateSearchType(List<ParameterTable> lstCboDateSearchType) {
		this.lstCboDateSearchType = lstCboDateSearchType;
	}

	/**
	 * Gets the lst cbo otc operation state type.
	 *
	 * @return the lst cbo otc operation state type
	 */
	public List<ParameterTable> getLstCboOtcOperationStateType() {
		return lstCboOtcOperationStateType;
	}

	/**
	 * Sets the lst cbo otc operation state type.
	 *
	 * @param lstCboOtcOperationStateType the new lst cbo otc operation state type
	 */
	public void setLstCboOtcOperationStateType(List<ParameterTable> lstCboOtcOperationStateType) {
		this.lstCboOtcOperationStateType = lstCboOtcOperationStateType;
	}

	/**
	 * Gets the lst cbo otc origin request type.
	 *
	 * @return the lst cbo otc origin request type
	 */
	public List<ParameterTable> getLstCboOtcOriginRequestType() {
		return lstCboOtcOriginRequestType;
	}

	/**
	 * Sets the lst cbo otc origin request type.
	 *
	 * @param lstCboOtcOriginRequestType the new lst cbo otc origin request type
	 */
	public void setLstCboOtcOriginRequestType(List<ParameterTable> lstCboOtcOriginRequestType) {
		this.lstCboOtcOriginRequestType = lstCboOtcOriginRequestType;
	}

	/**
	 * Gets the motive cancel operation list.
	 *
	 * @return the motive cancel operation list
	 */
	public List<ParameterTable> getMotiveCancelOperationList() {
		return motiveCancelOperationList;
	}

	/**
	 * Sets the motive cancel operation list.
	 *
	 * @param motiveCancelOperationList the new motive cancel operation list
	 */
	public void setMotiveCancelOperationList(
			List<ParameterTable> motiveCancelOperationList) {
		this.motiveCancelOperationList = motiveCancelOperationList;
	}

	/**
	 * Gets the lst settlement type.
	 *
	 * @return the lst settlement type
	 */
	public List<ParameterTable> getLstSettlementType() {
		return lstSettlementType;
	}

	/**
	 * Sets the lst settlement type.
	 *
	 * @param lstSettlementType the new lst settlement type
	 */
	public void setLstSettlementType(List<ParameterTable> lstSettlementType) {
		this.lstSettlementType = lstSettlementType;
	}

	/**
	 * Gets the lst scheme type.
	 *
	 * @return the lst scheme type
	 */
	public List<ParameterTable> getLstSchemeType() {
		return lstSchemeType;
	}

	/**
	 * Sets the lst scheme type.
	 *
	 * @param lstSchemeType the new lst scheme type
	 */
	public void setLstSchemeType(List<ParameterTable> lstSchemeType) {
		this.lstSchemeType = lstSchemeType;
	}

	/**
	 * Gets the program interest coupon list.
	 *
	 * @return the program interest coupon list
	 */
	public List<ProgramInterestCoupon> getProgramInterestCouponList() {
		return programInterestCouponList;
	}

	/**
	 * Sets the program interest coupon list.
	 *
	 * @param programInterestCouponList the new program interest coupon list
	 */
	public void setProgramInterestCouponList(
			List<ProgramInterestCoupon> programInterestCouponList) {
		this.programInterestCouponList = programInterestCouponList;
	}

	/**
	 * Gets the sirtex operation to.
	 *
	 * @return the sirtex operation to
	 */
	public SirtexOperationTO getSirtexOperationTO() {
		return sirtexOperationTO;
	}

	/**
	 * Sets the sirtex operation to.
	 *
	 * @param sirtexOperationTO the new sirtex operation to
	 */
	public void setSirtexOperationTO(SirtexOperationTO sirtexOperationTO) {
		this.sirtexOperationTO = sirtexOperationTO;
	}

	/**
	 * Gets the participant sell by modalities.
	 *
	 * @return the participant sell by modalities
	 */
	public Map<Long, List<Participant>> getParticipantSellByModalities() {
		return participantSellByModalities;
	}

	/**
	 * Sets the participant sell by modalities.
	 *
	 * @param participantByModalities the participant by modalities
	 */
	public void setParticipantSellByModalities(Map<Long, List<Participant>> participantByModalities) {
		this.participantSellByModalities = participantByModalities;
	}

	/**
	 * Gets the participant buy by modalities.
	 *
	 * @return the participant buy by modalities
	 */
	public Map<Long, List<Participant>> getParticipantBuyByModalities() {
		return participantBuyByModalities;
	}

	/**
	 * Sets the participant buy by modalities.
	 *
	 * @param participantBuyByModalities the participant buy by modalities
	 */
	public void setParticipantBuyByModalities(
			Map<Long, List<Participant>> participantBuyByModalities) {
		this.participantBuyByModalities = participantBuyByModalities;
	}

	/**
	 * Gets the all participant list.
	 *
	 * @return the all participant list
	 */
	public List<Participant> getAllParticipantList() {
		return allParticipantList;
	}

	/**
	 * Sets the all participant list.
	 *
	 * @param allParticipantList the new all participant list
	 */
	public void setAllParticipantList(List<Participant> allParticipantList) {
		this.allParticipantList = allParticipantList;
	}
	
	/**
	 * Gets the instrument type list.
	 *
	 * @return the instrument type list
	 */
	public List<InstrumentType> getInstrumentTypeList(){
		return InstrumentType.list;
	}
	
	/**
	 * Gets the currency types list.
	 *
	 * @return the currency types list
	 */
	public List<CurrencyType> getCurrencyTypesList(){
		return CurrencyType.listSomeElements(CurrencyType.PYG,CurrencyType.USD);
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the participant buyers.
	 *
	 * @return the participant buyers
	 */
	public List<Participant> getParticipantBuyers() {
		return participantBuyers;
	}

	/**
	 * Sets the participant buyers.
	 *
	 * @param participantBuyers the new participant buyers
	 */
	public void setParticipantBuyers(List<Participant> participantBuyers) {
		this.participantBuyers = participantBuyers;
	}

	/**
	 * Gets the participant sellers.
	 *
	 * @return the participant sellers
	 */
	public List<Participant> getParticipantSellers() {
		return participantSellers;
	}

	/**
	 * Sets the participant sellers.
	 *
	 * @param participantSellers the new participant sellers
	 */
	public void setParticipantSellers(List<Participant> participantSellers) {
		this.participantSellers = participantSellers;
	}

	/**
	 * Gets the bl is split.
	 *
	 * @return the bl is split
	 */
	public Boolean getBlIsSplit() {
		return blIsSplit;
	}

	/**
	 * Sets the bl is split.
	 *
	 * @param blIsSplit the new bl is split
	 */
	public void setBlIsSplit(Boolean blIsSplit) {
		this.blIsSplit = blIsSplit;
	}

	/**
	 * Gets the holder account buyers.
	 *
	 * @return the holder account buyers
	 */
	public List<HolderAccount> getHolderAccountBuyers() {
		return holderAccountBuyers;
	}

	/**
	 * Sets the holder account buyers.
	 *
	 * @param holderAccountBuyers the new holder account buyers
	 */
	public void setHolderAccountBuyers(List<HolderAccount> holderAccountBuyers) {
		this.holderAccountBuyers = holderAccountBuyers;
	}

	/**
	 * Gets the holder account seller.
	 *
	 * @return the holder account seller
	 */
	public List<HolderAccount> getHolderAccountSeller() {
		return holderAccountSeller;
	}

	/**
	 * Sets the holder account seller.
	 *
	 * @param holderAccountSeller the new holder account seller
	 */
	public void setHolderAccountSeller(List<HolderAccount> holderAccountSeller) {
		this.holderAccountSeller = holderAccountSeller;
	}

	/**
	 * Gets the sirtex filters.
	 *
	 * @return the sirtex filters
	 */
	public SirtexOperationTO getSirtexFilters() {
		return sirtexFilters;
	}

	/**
	 * Sets the sirtex filters.
	 *
	 * @param sirtexFilters the new sirtex filters
	 */
	public void setSirtexFilters(SirtexOperationTO sirtexFilters) {
		this.sirtexFilters = sirtexFilters;
	}

	/**
	 * Gets the modalities search list.
	 *
	 * @return the modalities search list
	 */
	public List<MechanismModality> getModalitiesSearchList() {
		return modalitiesSearchList;
	}

	/**
	 * Sets the modalities search list.
	 *
	 * @param modalitiesSearchList the new modalities search list
	 */
	public void setModalitiesSearchList(List<MechanismModality> modalitiesSearchList) {
		this.modalitiesSearchList = modalitiesSearchList;
	}

	/**
	 * Gets the sirtex data model.
	 *
	 * @return the sirtex data model
	 */
	public GenericDataModel<SirtexOperationResultTO> getSirtexDataModel() {
		return sirtexDataModel;
	}

	/**
	 * Sets the sirtex data model.
	 *
	 * @param sirtexDataModel the new sirtex data model
	 */
	public void setSirtexDataModel(GenericDataModel<SirtexOperationResultTO> sirtexDataModel) {
		this.sirtexDataModel = sirtexDataModel;
	}

//	/**
//	 * Gets the sirtex operation selected.
//	 *
//	 * @return the sirtex operation selected
//	 */
//	public SirtexOperationResultTO getSirtexOperationSelected() {
//		return sirtexOperationSelected;
//	}
//
//	/**
//	 * Sets the sirtex operation selected.
//	 *
//	 * @param sirtexOperationSelected the new sirtex operation selected
//	 */
//	public void setSirtexOperationSelected(SirtexOperationResultTO sirtexOperationSelected) {
//		this.sirtexOperationSelected = sirtexOperationSelected;
//	}
	

	public List<SirtexOperationResultTO> getSirtexOperationSelected() {
		return sirtexOperationSelected;
	}

	public void setSirtexOperationSelected(
			List<SirtexOperationResultTO> sirtexOperationSelected) {
		this.sirtexOperationSelected = sirtexOperationSelected;
	}

	/**
	 * Gets the all participant map.
	 *
	 * @return the all participant map
	 */
	public Map<Long, Participant> getAllParticipantMap() {
		return allParticipantMap;
	}

	/**
	 * Sets the all participant map.
	 *
	 * @param allParticipantMap the all participant map
	 */
	public void setAllParticipantMap(Map<Long, Participant> allParticipantMap) {
		this.allParticipantMap = allParticipantMap;
	}

	/**
	 * Gets the market fact component ui.
	 *
	 * @return the market fact component ui
	 */
	public MarketFactBalanceHelpTO getMarketFactComponentUI() {
		return marketFactComponentUI;
	}

	/**
	 * Sets the market fact component ui.
	 *
	 * @param marketFactComponentUI the new market fact component ui
	 */
	public void setMarketFactComponentUI(MarketFactBalanceHelpTO marketFactComponentUI) {
		this.marketFactComponentUI = marketFactComponentUI;
	}

	/**
	 * Gets the market fact number.
	 *
	 * @return the market fact number
	 */
	public Integer getMarketFactNumber() {
		return marketFactNumber;
	}

	/**
	 * Sets the market fact number.
	 *
	 * @param marketFactNumber the new market fact number
	 */
	public void setMarketFactNumber(Integer marketFactNumber) {
		this.marketFactNumber = marketFactNumber;
	}

	/**
	 * Gets the lst security class bcb.
	 *
	 * @return the lst security class bcb
	 */
	public List<ParameterTable> getLstSecurityClassBcb() {
		return lstSecurityClassBcb;
	}

	/**
	 * Sets the lst security class bcb.
	 *
	 * @param lstSecurityClassBcb the new lst security class bcb
	 */
	public void setLstSecurityClassBcb(List<ParameterTable> lstSecurityClassBcb) {
		this.lstSecurityClassBcb = lstSecurityClassBcb;
	}

	/**
	 * Gets the account seller.
	 *
	 * @return the account seller
	 */
	public HolderAccount getAccountSeller() {
		return accountSeller;
	}

	/**
	 * Sets the account seller.
	 *
	 * @param accountSeller the new account seller
	 */
	public void setAccountSeller(HolderAccount accountSeller) {
		this.accountSeller = accountSeller;
	}

	public Holder getHolderSeller() {
		return holderSeller;
	}

	public void setHolderSeller(Holder holderSeller) {
		this.holderSeller = holderSeller;
	}
	
}