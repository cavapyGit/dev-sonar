package com.pradera.negotiations.sirtex.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.operation.to.MechanismModalityTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.AccountTradeMarketFact;
import com.pradera.model.negotiation.AccountTradeRequest;
import com.pradera.model.negotiation.CouponTradeRequest;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.TradeMarketFact;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.CashCalendarType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SirtexTermSettlementType;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.negotiations.otcoperations.service.OtcOperationServiceBean;
import com.pradera.negotiations.otcoperations.to.OtcDpfManagementTO;
import com.pradera.negotiations.sirtex.service.OtcAndSirtexSettlementService;
import com.pradera.negotiations.sirtex.service.SirtexOperationService;
import com.pradera.negotiations.sirtex.service.SirtexSplitCouponService;
import com.pradera.negotiations.sirtex.to.SirtexManagementTO;
import com.pradera.negotiations.sirtex.to.SirtexOperationResultTO;
import com.pradera.negotiations.sirtex.view.SirtexOperationTO;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.to.SettlementHolderAccountTO;
import com.pradera.settlements.core.to.SettlementOperationTO;
import com.pradera.settlements.prepaidextended.service.PrepaidExtendedService;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SirtexOperationFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SirtexOperationFacade {
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sirtex operation service. */
	@EJB private SirtexOperationService sirtexOperationService;
	
	/** The otc operation service bean. */
	@EJB private OtcOperationServiceBean otcOperationServiceBean;
	
	/** The negotiation validations. */
	@EJB private NegotiationOperationServiceBean negotiationValidations;
	
	/** The accounts facade. */
	@EJB private AccountsFacade accountsFacade;
	
	/** The holiday query service bean. */
	@EJB private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The sirtex split coupon service. */
	@EJB private SirtexSplitCouponService sirtexSplitCouponService;
	
	/** The sirtex settlement service. */
	@EJB private OtcAndSirtexSettlementService sirtexSettlementService;
	
	/** The parameter service bean. */
	@EJB private ParameterServiceBean parameterServiceBean;
	
	/** The prepaid extended service. */
	//@EJB private PrepaidExtendedFacade prepaidExtendedFacade;
	@EJB private PrepaidExtendedService prepaidExtendedService;
	
	@EJB
	private SettlementProcessService settlementProcessServiceBean;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup private IdepositarySetup idepositarySetup;
	
	/**
	 * Have day exchanges rate.
	 *
	 * @param currencyId the currency id
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public DailyExchangeRates findDayExchangesRate(Integer currencyId) throws ServiceException{
		DailyExchangeRates exchangeRateDay = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),currencyId);
		if(exchangeRateDay==null){
			throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST);
		}
		return exchangeRateDay;
	}
	
	/**
	 * Validations before new operation.
	 *
	 * @param sirtexOperationTO the sirtex operation to
	 * @throws ServiceException the service exception
	 */
	public void validationsBeforeNewOperation(SirtexManagementTO sirtexOperationTO) throws ServiceException{
		List<MechanismModality> mechanismModalities = sirtexOperationTO.getMechanismModalityList();
		Long idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
		
		/*VALIDAMOS EL ESTADO DEL PARTICIPANTE*/
		if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			Participant participant = accountsFacade.getParticipantStateServiceFacade(idParticipantPk);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK_PARAM, new Object[]{idParticipantPk.toString()});
			}
			
		}else if(!userInfo.getUserAccountSession().isDepositaryInstitution()){/*SI EL USUARIO LOGUEADO NO ES NI PARTICIPANTE NI IDEPOSITARY*/
			throw new ServiceException(ErrorServiceType.USERSESSION_INCORRECT_ACTION);
		}
		
		/*VALIDAMOS QUE EXISTA AL MENOS 1 MODALIDAD DISPONIBLE PARA NEGOCIAR*/
		List<MechanismModality> modalitiesList = new ArrayList<MechanismModality>();
		if(mechanismModalities!=null && !mechanismModalities.isEmpty()){
			if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				for(MechanismModality mecMod: mechanismModalities){
					for(ParticipantMechanism participantMechanism: mecMod.getParticipantMechanisms()){
						if(participantMechanism.getParticipant().getIdParticipantPk().equals(idParticipantPk)){
							modalitiesList.add(mecMod);
							break;
						}
					}
				}
			}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				modalitiesList.addAll(mechanismModalities);
			}
		}
		if(modalitiesList.isEmpty()){
			throw new ServiceException(ErrorServiceType.NEG_MODALITY_DONT_EXIST,new Object[]{NegotiationMechanismType.SIRTEX.getValue()});
		}
	}
	
	/**
	 * Config new sirtex operation.
	 *
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest configNewSirtexOperation() throws ServiceException{
		/**GET THE USER NAME OF SESSION*/
		String userNameOfSession = userInfo.getUserAccountSession().getUserName();
		
		/**INSTANCIAMOS EL OBJETO SIRTEX OPERATION POR DEFAULT TODOS LOS VALORES VACIOS*/
		TradeRequest sirtexOperation = new TradeRequest();
//		sirtexOperation.setTradeOperation(new TradeOperation());
		sirtexOperation.setAccountSirtexOperations(new ArrayList<AccountTradeRequest>());
		sirtexOperation.setMechanisnModality(new MechanismModality());
		sirtexOperation.getMechanisnModality().getId().setIdNegotiationMechanismPk(NegotiationMechanismType.SIRTEX.getCode());
		sirtexOperation.setOperationDate(CommonsUtilities.currentDate());
		sirtexOperation.setReferenceDate(CommonsUtilities.currentDateTime());
		sirtexOperation.setBuyerParticipant(new Participant());
		sirtexOperation.setSellerParticipant(new Participant());
		sirtexOperation.setSecurities(new Security());
		sirtexOperation.setStockQuantity(BigDecimal.ZERO);
		sirtexOperation.setOperationState(SirtexOperationStateType.REGISTERED.getCode());
		sirtexOperation.setCashSettlementDays(BigDecimal.ZERO.longValue());
		sirtexOperation.setCashSettlementDate(new Date());
		sirtexOperation.setCouponsTradeRequestList(new ArrayList<CouponTradeRequest>());
		sirtexOperation.getSellerParticipant().setHolder(new Holder());
		
		/**CONFIGURAMOS EL OBJETO PARA MANEJAR HECHOS DE MERCADO*/
		if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			TradeMarketFact operationMarketFact = new TradeMarketFact();
			
			/**AGREGAMOS USUARIO DE REGISTRO Y FECHA*/
			operationMarketFact.setRegistryUser(userNameOfSession);
			operationMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
			
			operationMarketFact.setTradeRequest(sirtexOperation);
			sirtexOperation.setSirtexOperationMarketfacts(new ArrayList<TradeMarketFact>());
			sirtexOperation.getSirtexOperationMarketfacts().add(operationMarketFact);
		}
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			Long idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
			sirtexOperation.getSellerParticipant().setIdParticipantPk(idParticipantPk);
			sirtexOperation.getBuyerParticipant().setIdParticipantPk(null);
		}
		
		/**REGISTRAMOS CUENTA VENDEDORA*/
		AccountTradeRequest accountSellerOperation = new AccountTradeRequest();
		accountSellerOperation.setTradeRequest(sirtexOperation);
		accountSellerOperation.setRole(ParticipantRoleType.SELL.getCode());
		accountSellerOperation.setOperationPart(OperationPartType.CASH_PART.getCode());
		accountSellerOperation.setHolderAccount(new HolderAccount());
		accountSellerOperation.setIndInCharge(BooleanType.NO.getCode());
		accountSellerOperation.setAccountSirtexMarkectfacts(new ArrayList<AccountTradeMarketFact>());
		accountSellerOperation.setRegisterUser(userNameOfSession);
		accountSellerOperation.setRegisterDate(CommonsUtilities.currentDate());
		
		/**REGISTRAMOS CUENTA COMPRADORA*/
		AccountTradeRequest accountBuyerOperation = new AccountTradeRequest();
		accountBuyerOperation.setTradeRequest(sirtexOperation);
		accountBuyerOperation.setRole(ParticipantRoleType.BUY.getCode());
		accountBuyerOperation.setOperationPart(OperationPartType.CASH_PART.getCode());
		accountBuyerOperation.setHolderAccount(new HolderAccount());
		accountBuyerOperation.setIndInCharge(BooleanType.NO.getCode());
		accountBuyerOperation.setAccountSirtexMarkectfacts(new ArrayList<AccountTradeMarketFact>());
		accountBuyerOperation.setRegisterUser(userNameOfSession);
		accountBuyerOperation.setRegisterDate(CommonsUtilities.currentDate());
		
		/**ASIGNAMOS CUENTAS DE NEGOCIACION EN LA OPERACION*/
		sirtexOperation.getAccountSirtexOperations().add(accountSellerOperation);
		sirtexOperation.getAccountSirtexOperations().add(accountBuyerOperation);
		
		return sirtexOperation;
	}
	
	/**
	 * Config new mechanism operation.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param modalityPk the modality pk
	 * @param partSession the part session
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation configNewMechanismOperation(Long mechanismPk, Long modalityPk, Long partSession) throws ServiceException{
		/**INSTANCIAMOS EL OBJETO MECHANISM OPERATION POR DEFAULT TODOS LOS VALORES VACIOS*/
		MechanismOperation mechanismOperation = new MechanismOperation();
		mechanismOperation.setTradeOperation(new TradeOperation());
		mechanismOperation.setMechanisnModality(new MechanismModality());
		mechanismOperation.getMechanisnModality().getId().setIdNegotiationMechanismPk(mechanismPk);
		mechanismOperation.getMechanisnModality().getId().setIdNegotiationModalityPk(modalityPk);
		mechanismOperation.setOperationDate(CommonsUtilities.currentDate());
		mechanismOperation.setReferenceDate(CommonsUtilities.currentDateTime());
		mechanismOperation.setBuyerParticipant(new Participant());
		mechanismOperation.setSellerParticipant(new Participant());
		mechanismOperation.setSecurities(new Security());
		mechanismOperation.setStockQuantity(BigDecimal.ZERO);
		mechanismOperation.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
		mechanismOperation.setOperationState(SirtexOperationStateType.REGISTERED.getCode());
		mechanismOperation.setCashSettlementDays(BigDecimal.ZERO.longValue());
		mechanismOperation.setCashSettlementDate(new Date());

		/**CONFIGURAMOS EL OBJETO PARA MANEJAR HECHOS DE MERCADO*/
		/**If this is sirtex*/
		if(mechanismPk.equals(NegotiationMechanismType.SIRTEX.getCode())){
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				MechanismOperationMarketFact operationMarketFact = new MechanismOperationMarketFact();
				operationMarketFact.setMechanismOperation(mechanismOperation);
				mechanismOperation.setMechanismOperationMarketFacts(new ArrayList<MechanismOperationMarketFact>());
				mechanismOperation.getMechanismOperationMarketFacts().add(operationMarketFact);
			}
		}
		
		if(partSession!=null ){
			Long idParticipantPk = partSession;
			mechanismOperation.getSellerParticipant().setIdParticipantPk(idParticipantPk);
			mechanismOperation.getBuyerParticipant().setIdParticipantPk(null);
		}
		
		/**CONFIGURAMOS HOLDER ACCOUNT OPERATION*/
		/**REGISTRAMOS CUENTA VENDEDORA*/
		HolderAccountOperation accountSellerOperation = new HolderAccountOperation();
		accountSellerOperation.setMechanismOperation(mechanismOperation);
		accountSellerOperation.setRole(ParticipantRoleType.SELL.getCode());
		accountSellerOperation.setOperationPart(OperationPartType.CASH_PART.getCode());
		accountSellerOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
		accountSellerOperation.setHolderAccount(new HolderAccount());
		accountSellerOperation.setIndIncharge(BooleanType.NO.getCode());
		accountSellerOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
		
		/**REGISTRAMOS CUENTA COMPRADORA*/
		HolderAccountOperation accountBuyerOperation = new HolderAccountOperation();
		accountBuyerOperation.setMechanismOperation(mechanismOperation);
		accountBuyerOperation.setRole(ParticipantRoleType.BUY.getCode());
		accountBuyerOperation.setOperationPart(OperationPartType.CASH_PART.getCode());
		accountBuyerOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
		accountBuyerOperation.setHolderAccount(new HolderAccount()); 
		accountBuyerOperation.setIndIncharge(BooleanType.NO.getCode());
		accountBuyerOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
		
		/**ASIGNAMOS CUENTAS DE NEGOCIACION EN LA OPERACION*/
		mechanismOperation.getHolderAccountOperations().add(accountSellerOperation);
		mechanismOperation.getHolderAccountOperations().add(accountBuyerOperation);
		return mechanismOperation;
	}
	
	/**
	 * Put mechanism coupon.
	 *
	 * @param sirtexManagement the sirtex management
	 * @return the coupon trade request
	 * @throws ServiceException the service exception
	 */
	public CouponTradeRequest putMechanismCoupon(SirtexManagementTO sirtexManagement) throws ServiceException{
		TradeRequest operation = sirtexManagement.getSirtexOperation();
		//Security security = operation.getSecurities();
		Security security = sirtexOperationService.find(Security.class, operation.getSecurities().getIdSecurityCodePk());
		
		AccountTradeRequest accountSeller = operation.getAccountSirtexOperations().get(0);
		HolderAccount account = accountSeller.getHolderAccount();
		HolderAccountBalance balance = sirtexOperationService.findBalanceFromAccount(security.getIdSecurityCodePk(), account.getIdHolderAccountPk());
		
		CouponTradeRequest coupon = new CouponTradeRequest();
		coupon.setOriginAvailableBalance(balance.getAvailableBalance());
		coupon.setCouponNumber(BigDecimal.ZERO.intValue());
		coupon.setExpiredDate(security.getExpirationDate());
		coupon.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		coupon.setRegistryDate(CommonsUtilities.currentDate());
		coupon.setIndIsCapital(BooleanType.NO.getCode());
		coupon.setDaysOfLife(CommonsUtilities.getDaysBetween(security.getIssuanceDate(), coupon.getExpiredDate()));
		if(security.getSecurityClass().equals(SecurityClassType.DPF.getCode()))
			coupon.setSuppliedQuantity(new BigDecimal(1));
		coupon.setTradeRequest(operation);
		return coupon;
	}

	/**
	 * Put participant in charge.
	 *
	 * @param sirtexManagement the sirtex management
	 * @param role the role
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccountTradeRequest> putParticipantInCharge(SirtexManagementTO sirtexManagement, Integer role) throws ServiceException{
		TradeRequest operation = sirtexManagement.getSirtexOperation();
		for(AccountTradeRequest accountOperation: operation.getAccountSirtexOperations()){
			Participant partInCharge = null;
			if(role.equals(ParticipantRoleType.SELL.getCode())){
				partInCharge = operation.getSellerParticipant();
			}else{
				partInCharge = operation.getBuyerParticipant();
			}
			
			if(accountOperation.getRole().equals(role)){
				accountOperation.setInchargeStockParticipant(partInCharge);
				accountOperation.setInchargeFundsParticipant(partInCharge);
			}
		}
		return operation.getAccountSirtexOperations();
	}
	
	/**
	 * Find mechanism modality list.
	 *
	 * @param mecModalityTO the mec modality to
	 * @param parametersDesc the parameters desc
	 * @param rol the rol
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<MechanismModality> findMechanismModalityList(MechanismModalityTO mecModalityTO, Map<Integer,String> parametersDesc, Integer rol) throws ServiceException{
		List<MechanismModality> mecModalities = sirtexOperationService.getMechanismModalityList(mecModalityTO,rol);
		for(MechanismModality mecModality: mecModalities){
			mecModality.setSettlementTypeDescription(SettlementType.get(mecModality.getSettlementType()).getValue());
			mecModality.setSettlementSchemaDescription(SettlementSchemaType.get(mecModality.getSettlementSchema()).getValue());
		}
		return mecModalities;
	}
	
	/**
	 * Find mechanism modality.
	 *
	 * @param sirtexManagement the sirtex management
	 * @return the mechanism modality
	 */
	public MechanismModality findMechanismModality(SirtexManagementTO sirtexManagement){
		TradeRequest sirtexOperation = sirtexManagement.getSirtexOperation();
		MechanismModality mecModality = sirtexOperation.getMechanisnModality();
		Long negotiationModalityID = mecModality.getId().getIdNegotiationModalityPk();
		try {
			return otcOperationServiceBean.getMechanismModality(NegotiationMechanismType.SIRTEX.getCode(), negotiationModalityID);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
//		for(MechanismModality mechanismModality: sirtexManagement.getMechanismModalityList()){//Recorremos las modalidades
//			if(mechanismModality.getId().getIdNegotiationMechanismPk().equals(NegotiationMechanismType.SIRTEX.getCode()) && 
//			   mechanismModality.getNegotiationModality().getIdNegotiationModalityPk().equals(negotiationModalityID)){
//				return mechanismModality;
//			}
//		}
		return null;
	}
	
	/**
	 * Config modalities on participant map.
	 *
	 * @param mecModList the mec mod list
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public Map<Long,List<Participant>> configModalitiesOnParticipantMap(List<MechanismModality> mecModList) throws ServiceException{
		Map<Long,List<Participant>> participantByModalities = new HashMap<Long,List<Participant>>();
		List<NegotiationModalityType> negModalityAvailables = new ArrayList<NegotiationModalityType>();
		
		_jump:
		for(MechanismModality mechanismModality: mecModList){
			Long idNegModality = mechanismModality.getNegotiationModality().getIdNegotiationModalityPk();
			participantByModalities.put(idNegModality, new ArrayList<Participant>());
			for(ParticipantMechanism partMechanism: mechanismModality.getParticipantMechanisms()){
				participantByModalities.get(idNegModality).add(partMechanism.getParticipant());
			}
			for(NegotiationModalityType negModalityType: negModalityAvailables){
				if(negModalityType.getCode().equals(idNegModality)){
					mechanismModality.setStateMechanismModality(BooleanType.YES.getCode());
					continue _jump;
				}
			}
		}
		return participantByModalities;
	}
	
	/**
	 * Validate security for sirtex.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @throws ServiceException the service exception
	 */
	public void validateSecurityForSirtex(SirtexManagementTO sirtexOperation) throws ServiceException {
	 	TradeRequest operation = sirtexOperation.getSirtexOperation();
		Integer instrumentType = sirtexOperation.getInstrumentType();
		
		if(operation.getSecurities().getIdSecurityCodePk()==null) throw new ServiceException();
		Security securityTrade  = otcOperationServiceBean.findSecurityFromCode(operation.getSecurities());
		Security securityFather = null;
		if(securityTrade!=null){
			securityFather = securityTrade.getSecurity()!=null?otcOperationServiceBean.findSecurityFromCode(securityTrade.getSecurity()):null;
		}		
		
		/**SI EL VALOR ES NULL, O NO TIENE EMISION NI EMISOR NI MONEDA NI MONTO EN CIRCULACION*/
		/**SI LOS INDICADORES DE NEGOCIACION COMO NATURAL O JURIDICO SON NULL*/
		if(securityTrade==null || securityTrade.getCurrency()==null || CurrencyType.get(securityTrade.getCurrency())==null ||
		   securityTrade.getIndTraderSecondary()==null || securityTrade.getCirculationAmount()==null ||
		   securityTrade.getInstrumentType()==null || securityTrade.getSecurityInvestor()==null ||
		   securityTrade.getSecurityInvestor().getIndJuridical()==null || securityTrade.getSecurityInvestor().getIndNatural()==null ){			
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}
		
		/**SI ES RTA FIJA Y NO TIENE FECHA DE EXPIRACION*/
		if(securityTrade.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()) && securityTrade.getExpirationDate()==null){
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}
		
		NegotiationModality modality = findNegModality(sirtexOperation);
		Long modalityId = modality.getIdNegotiationModalityPk();
		String securityCode = operation.getSecurities().getIdSecurityCodePk();
		
		/**si el valor esta habilitado para SIRTEX*/
		if(sirtexOperationService.getSecurityNegotiationMechanism(securityCode, modalityId)==null){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_MECHANISM);
		}
		
		/**validacion VALORES PRIVADOS en operaciones SIRTEX solo por AGENCIAS, issue 1075*/
		//if (modalityId == 3 || modalityId == 1) //reporto o compra venta
		if (modalityId.equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode())
			||modalityId.equals(NegotiationModalityType.CASH_FIXED_INCOME.getCode()))	
		{
//			si no es emitido por TGN o BCB
			if (!(securityTrade.getIssuer().getIdIssuerPk().equals("BO016") || securityTrade.getIssuer().getIdIssuerPk().equals("BO017"))) {
//				throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_PRIVATE);
				/** parametro valida negociacion por cuenta propia */
				ParameterTable param = sirtexOperationService.getParameterOwnAccount();
				if(param==null)
					return;
				if (Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getParticipantCode())) {
					Participant participant = sirtexOperationService.getParticipantById(userInfo.getUserAccountSession().getParticipantCode());
//					si no forma parte del proceso de compensacion y liquidacion
					if (participant.getAccountType() != 26) {
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_PRIVATE_AUTHORIZED);
					}
					if (param.getShortInteger() == 1) {
						/**no puede negociar por cuenta propia CUI - PARTICIPANTE - issue 1098*/
						Holder h = new Holder();
						h = sirtexOperationService.checkParticipant(participant.getIdParticipantPk());
						if(h.getIdHolderPk() == sirtexOperation.getHolderSeller().getIdHolderPk().intValue()) {
							throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_OWN_ACCOUNT);
						}
					}
			    } else {
			    	Participant participant = sirtexOperationService.getParticipantById(sirtexOperation.getSirtexOperation().getSellerParticipant().getIdParticipantPk());
//					si no forma parte del proceso de compensacion y liquidacion
					if (participant.getAccountType() != 26) {
						throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_PRIVATE_AUTHORIZED);
					}
					if (param.getShortInteger() == 1) {
						/**no puede negociar por cuenta propia CUI - PARTICIPANTE - issue 1098*/
						Holder h = new Holder();
						h = sirtexOperationService.checkParticipant(participant.getIdParticipantPk());
						if(h.getIdHolderPk() == sirtexOperation.getHolderSeller().getIdHolderPk().intValue()) {
							throw new ServiceException(ErrorServiceType.NEG_OPERATION_SECURITYCODE_OWN_ACCOUNT);
						}
					}
			    }
			} 
		}
		
		/**VERIFICAMOS QUE EL INSTRUMENTO DE LA MODALIDAD COINCIDA CON EL DEL VALOR*/
		NegotiationModality negModality = findNegModality(sirtexOperation);
		if(!securityTrade.getInstrumentType().equals(negModality.getInstrumentType())){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SEC_INSTRUMENT);
		}
		
		if(!securityTrade.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
		}
		
		/**VALIDAMOS EL EMISOR SEA DIFERENTE A NULL*/
		if(securityTrade.getIssuer()==null){//Validamos que el valor tenga al menos un emisor
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}else{
			/**VERIFICAMOS QUE EL ESTADO DEL EMISOR SEA REGISTRADO*/
			if(!securityTrade.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
			}
		}
		
		/**VALIDAMOS QUE LA EMISION SEA DIFERENTE A NULL*/
		if(securityTrade.getIssuance()==null){
			throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
		}else{
			/**VERIFICAMOS QUE EL ESTADO DE LA EMISION SEA DIFERENTE A NULL*/
			if(!securityTrade.getIssuance().getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
				throw new ServiceException(ErrorServiceType.ISSUANCE_BLOCK);
			}
		}
		
		/**VERIFICAMOS QUE EL INSTRUMENTO DE LA MODALIDAD COINCIDA CON EL DEL VALOR*/
		if(!securityTrade.getInstrumentType().equals(instrumentType)){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SEC_INSTRUMENT);
		}
		
		/**VERIFICAMOS SI EL PARTICIPANTE TIENE SALDOS DE DICHO VALOR EN CUENTAS PROPIAS*/
		Participant participantSeller = operation.getSellerParticipant();
		List<HolderAccountBalance> balances = sirtexOperationService.findBalancesFromHolderAccount(participantSeller, securityTrade, operation.getSellerAccount());
		if(balances==null || balances.isEmpty()){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_PARTICIPANT_WITHOUT_BALANCE);
		}
		operation.setSecurities(securityTrade);
		if(securityFather!=null){
			operation.setSecurityParent(securityFather);
		}
	}
	
	/**
	 * Find indicator SIRTEX negotiation
	 *
	 * @param holder_pk 
	 * @return ind_sirtex_neg state
	 * @throws ServiceException the service exception
	 */
	public Integer findIndSirtexNeg (Long idHolderPk) throws ServiceException {
		int ind_sirtex_neg = 0;
		ind_sirtex_neg = sirtexOperationService.findIndSirtexNeg (idHolderPk);		
		return ind_sirtex_neg;
	}
	
	/**
	 * Find holder accounts from balances.
	 *
	 * @param participant the participant
	 * @param security the security
	 * @param role the role
	 * @param idPartBc the id part bc
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccountsFromBalances(Participant participant, Security security, Integer role, Long idPartBc) 
			throws ServiceException{
		List<HolderAccount> accounts = null;
		if(role.equals(ParticipantRoleType.SELL.getCode())){
			accounts = sirtexOperationService.findHolderAccountsFromBalances(participant, security);
		}else if(role.equals(ParticipantRoleType.BUY.getCode())){
			accounts = sirtexOperationService.findHolderAccountsFromParticipant(participant);
		}
		return accounts;
	}
	
	/**
	 * Find holder accounts from participant.
	 *
	 * @param participant the participant
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount findHolderAccountsFromParticipant(Participant participant) throws ServiceException{
		List<HolderAccount> accounts = sirtexOperationService.findHolderAccountsFromParticipant(participant);
		if(accounts==null || accounts.size()==0){
			throw new ServiceException(ErrorServiceType.HOLDER_WITHOUT_HOLDERACCOUNTS);
		}
		return accounts.get(0);
	}
	
	/**
	 * Validate supplied quantity.
	 *
	 * @param coupon the coupon
	 * @param couponList the coupon list
	 * @throws ServiceException the service exception
	 */
	public void validateSuppliedQuantity(CouponTradeRequest coupon, List<CouponTradeRequest> couponList) throws ServiceException{
		BigDecimal suppliedQuantity = coupon.getSuppliedQuantity();
		BigDecimal availableBalance = coupon.getOriginAvailableBalance();
		
		if(suppliedQuantity!=null){
			if(suppliedQuantity.compareTo(availableBalance) > 0){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_SUPPLIED_AMOUNT);
			}
		}
	}
	
	/**
	 * Gets the greather coupon quantity.
	 *
	 * @param counponList the counpon list
	 * @return the greather coupon quantity
	 */
	public BigDecimal getGreatherCouponQuantity(List<CouponTradeRequest> counponList){
		BigDecimal greateCouponBal = BigDecimal.ZERO;
		for(CouponTradeRequest couponTrade: counponList){
			if(couponTrade.getSuppliedQuantity()!=null && 
			   !(couponTrade.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && couponTrade.getIndIsCapital().equals(BooleanType.NO.getCode()))){
				
				if(couponTrade.getSuppliedQuantity().compareTo(greateCouponBal)>0){
					greateCouponBal = couponTrade.getSuppliedQuantity();
				}
			}
		}
		return greateCouponBal;
	}
	
	/**
	 * Gets the greather accept coupon quantity.
	 *
	 * @param counponList the counpon list
	 * @return the greather accept coupon quantity
	 */
	public BigDecimal getGreatherAcceptCouponQuantity(List<CouponTradeRequest> counponList){
		BigDecimal greateCouponBal = BigDecimal.ZERO;
		for(CouponTradeRequest couponTrade: counponList){
			if(couponTrade.getAcceptedQuantity()!=null && 
			   !(couponTrade.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && couponTrade.getIndIsCapital().equals(BooleanType.NO.getCode()))){
				
				if(couponTrade.getAcceptedQuantity().compareTo(greateCouponBal)>0){
					greateCouponBal = couponTrade.getAcceptedQuantity();
				}
			}
		}
		return greateCouponBal;
	}
	
	/**
	 * Validate accepted quantity.
	 *
	 * @param coupon the coupon
	 * @throws ServiceException the service exception
	 */
	public void validateAcceptedQuantity(CouponTradeRequest coupon) throws ServiceException{
		BigDecimal suppliedQuantity = coupon.getSuppliedQuantity();
		BigDecimal acceptedQuantity = coupon.getAcceptedQuantity();
		
		if(acceptedQuantity!=null && acceptedQuantity.compareTo(suppliedQuantity) > 0){
			throw new ServiceException(ErrorServiceType.TRADE_OPERATION_ACEPTED_QUANTITY);
		}
	}
	
	/**
	 * Validate stock quantity.
	 *
	 * @param sirtexManagement the sirtex management
	 * @throws ServiceException the service exception
	 */
	public void validateStockQuantity(SirtexManagementTO sirtexManagement) throws ServiceException{
		TradeRequest operation = sirtexManagement.getSirtexOperation();
		/*OBTENEMOS LA CANTIDAD DE VALORES*/
		BigDecimal quantitySecurities = operation.getStockQuantity();
		BigDecimal circulationBalance = operation.getSecurities().getCirculationBalance();
		
		if(quantitySecurities==null || quantitySecurities.equals(BigDecimal.ZERO)){
			operation.setStockQuantity(null);
			return;
		}
		
		/*VALIDAMOS SI LA CANTIDAD INGRESADA ES MENOR A LOS VALORES EN CIRCULACION*/
		if(quantitySecurities.compareTo(circulationBalance)>=0){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_VALIDATION_CIRCULATION);
		}
	}
	
	/**
	 * Find neg modality.
	 *
	 * @param sirtexManagement the sirtex management
	 * @return the negotiation modality
	 */
	public NegotiationModality findNegModality(SirtexManagementTO sirtexManagement){
		return findMechanismModality(sirtexManagement).getNegotiationModality();
	}
	
	/**
	 * Validate settlement date.
	 *
	 * @param sirtexManagement the sirtex management
	 * @param operationPart the operation part
	 * @param settlementDays the settlement days
	 * @param params the params
	 * @throws ServiceException the service exception
	 */
	public void validateSettlementDate(SirtexManagementTO sirtexManagement, Integer operationPart, Long settlementDays, Map<Integer,String> params) throws ServiceException{ 
		NegotiationModality negModality = findNegModality(sirtexManagement);
		TradeRequest operation = sirtexManagement.getSirtexOperation();
		Security security = operation.getSecurities();
		Date settlementDate = null ;//Dato para almacenar la fecha de liquidacion contado
		Date expirationDate = security.getExpirationDate(); //obtenemos la fecha de expiracion del valor
		
		/*Indicador de dias maximo para liquidacion contado*/
		Integer minSettlemenDay = GeneralConstants.ZERO_VALUE_INTEGER, maxSettlemenDay =GeneralConstants.ZERO_VALUE_INTEGER;
		/*tipo de calendario en liquidacion */
		Integer settCalendarType = negModality.getIndTermCallendar();
		
		/*variables para manejar los dias maximo Y minimo para liquidacion*/
		if(operationPart.equals(OperationPartType.CASH_PART.getCode())){
			minSettlemenDay = negModality.getCashMinSettlementDays();
			maxSettlemenDay = negModality.getCashMaxSettlementDays();
		}else if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
//			minSettlemenDay = negModality.getTermMinSettlementDays();
//			maxSettlemenDay = negModality.getTermMaxSettlementDays();
			minSettlemenDay = Integer.parseInt(params.get(SirtexTermSettlementType.MIN_DAYS.getCode()));
			maxSettlemenDay = Integer.parseInt(params.get(SirtexTermSettlementType.MAX_DAYS.getCode()));
		}
		
		/*Si la cantidad de dias, NO esta en el rango de dias de la modalidad*/
		if(settlementDays!=null && (settlementDays<minSettlemenDay || settlementDays>maxSettlemenDay)){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTLEMENT_DAYS);
		}else if(settCalendarType==null ){//Si la modalidad no tiene un calendario fijado en la base datos
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_INCONSISTENT);
		}else  if(settCalendarType.equals(NegotiationConstant.WORKING_DAYS) ||
				  settCalendarType.equals(NegotiationConstant.WORKING_DAYS_PASS_WEEKENDS)){//Si los dias son calendario
			settlementDate = CommonsUtilities.addDate(operation.getOperationDate(), settlementDays.intValue());
			Calendar date = Calendar.getInstance();
			date.setTime(settlementDate);
			/**Si el dia de liquidacion es fin de semana O si es un feriado*/
			if(CommonsUtilities.isWeekend(date) || holidayQueryServiceBean.isNonWorkingDayServiceBean(settlementDate, Boolean.FALSE)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTDATE_NOTWORKING,
																new Object[]{OperationPartType.get(operationPart).getDescription()});
			}
		}else if(CashCalendarType.get(settCalendarType).equals(CashCalendarType.UTIL_DAYS)){//Si los dias son utiles
			if(settlementDays!=null && settlementDays.intValue()!=0){
				/**Calculamos el siguiente dia habil*/
				settlementDate = holidayQueryServiceBean.getCalculateDate(operation.getOperationDate(), settlementDays.intValue(), 1,null);
			}else{
				settlementDate = operation.getOperationDate();
			}
			
			Calendar date = Calendar.getInstance();
			if(settlementDate!=null){
				date.setTime(settlementDate);
			}			
			if(settlementDate==null || CommonsUtilities.isWeekend(date)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTDATE_NOTWORKING,
																new Object[]{OperationPartType.get(operationPart).getDescription()});
			}
		}
		
		if(operationPart.equals(OperationPartType.CASH_PART.getCode())){
			operation.setCashSettlementDate(settlementDate);
		}else if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
			Date cashSettlementDate = CommonsUtilities.truncateDateTime(operation.getCashSettlementDate());
			if(cashSettlementDate!=null && cashSettlementDate.after(settlementDate)){
				throw new ServiceException(ErrorServiceType.NEG_OPERATION_MODALITY_SETTLEMENT_DAYS);
			}
			/*SET TERM SETTLEMENT DATE TO OPERATION*/
			operation.setTermSettlementDate(settlementDate);
		}
		
		/*SI ES RTA FIJA Y FECHA CALCULADA ES MAYOR O IGUAL A LA FECHA QUE VENCE EL VALOR*/
		if((CommonsUtilities.addDate(settlementDate,1).after(expirationDate))){
			throw new ServiceException(ErrorServiceType.NEG_OPERATION_SETTLEMENTDATE_SECURITY_VENC);
		}
	}
	
	/**
	 * Find holder from participant.
	 *
	 * @param participantPK the participant pk
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderFromParticipant(Long participantPK) throws ServiceException{
		return sirtexOperationService.findHolderFromParticipant(participantPK);
	}
	
	/**
	 * Find balance from account.
	 *
	 * @param securityCode the security code
	 * @param holderAccountPK the holder account pk
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance findBalanceFromAccount(String securityCode, Long holderAccountPK) throws ServiceException{
		HolderAccountBalance balance = sirtexOperationService.findBalanceFromAccount(securityCode, holderAccountPK); 
		if(balance.getAvailableBalance().equals(BigDecimal.ZERO)){
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		}
		return balance;
	}
	
	/**
	 * Register sirtex operation.
	 *
	 * @param sirtexManagement the sirtex management
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_REGISTER)
	public TradeRequest registerSirtexOperation(TradeRequest sirtexManagement) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		TradeRequest sirtexOperation = sirtexManagement;
		
		MechanismModality mechanismModality = sirtexOperation.getMechanisnModality();
		NegotiationModality negotiationModality = mechanismModality.getNegotiationModality();
		Boolean indTermPart = negotiationModality.getIndReportingBalance().equals(BooleanType.YES.getCode());
		
		/**ASIGNAMOS MECHANISM OPERATION TO NEW OBJECT*/
		sirtexOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		sirtexOperation.setRegistryDate(CommonsUtilities.currentDate());

		/**CONFIG NEGOTIATION MODALITY*/
		NegotiationModalityType negModalityType = NegotiationModalityType.get(sirtexOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
		
		/**CREAMOS STOCK QUANTITY Y RECALCULAMOS MONTOS*/
		BigDecimal stockQuantity = BigDecimal.ZERO;
		BigDecimal cashPrice = sirtexOperation.getCashPrice();
		BigDecimal realCashPrice = sirtexOperation.getRealCashPrice();
		BigDecimal termPrice = sirtexOperation.getTermPrice();
		
		List<CouponTradeRequest> mechanismCoupons = sirtexOperation.getCouponsTradeRequestList(); 
		if(mechanismCoupons!=null && mechanismCoupons.size() >= BigDecimal.ONE.intValue()){
			stockQuantity = mechanismCoupons.get(0).getSuppliedQuantity();
		}
		
		if(cashPrice == null){
			cashPrice = BigDecimal.ZERO;
		}
		
		if(realCashPrice == null){
			realCashPrice = BigDecimal.ZERO;
		}
		
		if(indTermPart && termPrice == null){
			termPrice = BigDecimal.ZERO;
		}
		
		sirtexOperation.setStockQuantity(stockQuantity);
		sirtexOperation.setCashAmount(cashPrice.multiply(stockQuantity));
		sirtexOperation.setCashPrice(cashPrice);
		sirtexOperation.setRealCashAmount(realCashPrice.multiply(stockQuantity));
		sirtexOperation.setRealCashPrice(realCashPrice);
		
		if(termPrice != null){
			sirtexOperation.setTermPrice(termPrice);
			sirtexOperation.setTermAmount(termPrice.multiply(stockQuantity));
		}
		
		Long participantUser = userInfo.getUserAccountSession().getParticipantCode();
		
		/**CONFIG TRADE OPERATION y MECHANISM_OPERATION*/
		TradeOperation tradeOperation = new TradeOperation();
		tradeOperation.setRegisterUser(userInfo.getUserAccountSession().getUserName());
		tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		tradeOperation.setOperationState(SirtexOperationStateType.REGISTERED.getCode());
		tradeOperation.setOperationType(negModalityType.getParameterOperationType());
		tradeOperation.setAudit(loggerUser);
		MechanismOperation mechanismOperation = configNewMechanismOperation(sirtexOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk(),
																			sirtexOperation.getMechanisnModality().getId().getIdNegotiationModalityPk(),
																			participantUser);
		mechanismOperation.setTradeOperation(tradeOperation);
		/**REGISTRAMOS NUEVA OPERACION SIRTEX EN BD*/
		sirtexOperationService.registerSirtexOperation(sirtexOperation);
		/**CONFIGURAMOS MECHANISM OPERATION OBJETO FROM SIRTEX REQUEST*/
		mechanismOperation = sirtexOperationService.configTradeRequestLikeOperation(sirtexOperation, mechanismOperation);
		/**REGISTRAMOS NUEVA OPERACION DE NEGOCIACION SIRTEX EN BD*/
		sirtexOperationService.registerMechanismOperation(mechanismOperation);
		/**BLOQUEAMOS SALDOS COMPROMETIDOS EN VENTA*/
		Integer operationPart = OperationPartType.CASH_PART.getCode();
//		sirtexSettlementService.lockBalancesOfSirtexOperation(sirtexOperation.getIdTradeRequestPk(), operationPart, businessProcessType);
		sirtexSettlementService.lockBalancesOfOperation(mechanismOperation.getIdMechanismOperationPk(), operationPart);
		return  sirtexOperation;
	}
	
	/**
	 * Approve otc opereation service facade.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_APPROVE)
	public TradeRequest approveOtcOpereationServiceFacade(TradeRequest sirtexOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		/*VALIDAMOS EL OBJETO SIRTEX*/
		negotiationValidations.validateSirtexOperation(sirtexOperation);
		negotiationValidations.validateSirtexOperationState(sirtexOperation.getIdTradeRequestPk(), Arrays.asList(SirtexOperationStateType.REGISTERED.getCode()));
		
		/*APPROVE SIRTEX OPERATION ON TRANSACCTION*/
		return sirtexOperationService.approveSirtexOpereation(sirtexOperation);
	}
	
	/**
	 * Anulate sirtex operation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_ANULATE)
	public TradeRequest anulateSirtexOperation(TradeRequest sirtexOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		//Validate state again
		negotiationValidations.validateSirtexOperationState(sirtexOperation.getIdTradeRequestPk(), Arrays.asList(SirtexOperationStateType.REGISTERED.getCode()));
		
		/*GETTING INFORMATION ABOUT OPERATION*/
		TradeRequest operation = sirtexOperationService.annulateSirtexOpereation(sirtexOperation);;
		Long operationID = sirtexOperationService.findTradeOperationId(sirtexOperation.getIdTradeRequestPk());
		Long businessProcessType = BusinessProcessType.SIRTEX_OPERATION_ANULATE.getCode();
		
		/*DESBLOQUEAMOS LOS SALDOS COMPROMETIDOS EN VENTA Y COMPRA*/
		sirtexSettlementService.unLockBalancesOfOperation(operationID, OperationPartType.CASH_PART.getCode(),null,businessProcessType);
		
		return operation;
	}
	
	/**
	 * Reject sirtex operation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_REJECT)
	public TradeRequest rejectSirtexOperation(TradeRequest sirtexOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		//Validate state again
		negotiationValidations.validateSirtexOperationState(sirtexOperation.getIdTradeRequestPk(), 
				Arrays.asList(SirtexOperationStateType.APROVED.getCode(),SirtexOperationStateType.REVIEWED.getCode()));
		
		/*GETTING INFORMATION ABOUT OPERATION*/
		TradeRequest operation = sirtexOperationService.rejectSirtexOpereation(sirtexOperation);
		Long operationID = sirtexOperationService.findTradeOperationId(sirtexOperation.getIdTradeRequestPk());
		Long businessProcessType = BusinessProcessType.SIRTEX_OPERATION_REJECT.getCode();
		
		/*DESBLOQUEAMOS LOS SALDOS COMPROMETIDOS EN VENTA Y COMPRA*/
		sirtexSettlementService.unLockBalancesOfOperation(operationID, OperationPartType.CASH_PART.getCode(),null,businessProcessType);
		
		return operation;
	}
	
	/**
	 * Review sirtex operation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_REVIEW)
	public TradeRequest reviewSirtexOperation(TradeRequest sirtexOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());

		negotiationValidations.validateSirtexOperationState(sirtexOperation.getIdTradeRequestPk(), Arrays.asList(SirtexOperationStateType.APROVED.getCode()));

		/**VARIABLES PARA MANEJAR EL CAMBIO EN EL STOCK QUANTITY*/
		Boolean stockQuantityChanged = Boolean.FALSE;
		BigDecimal stockQuantity = BigDecimal.ZERO;
		for(CouponTradeRequest srtxCup: sirtexOperation.getCouponsTradeRequestList()){
			/**VERIFICAMOS SI ES EL COUPON 0*/
			if(srtxCup.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && srtxCup.getIndIsCapital().equals(BooleanType.NO.getCode())){
				if(!srtxCup.getAcceptedQuantity().equals(srtxCup.getSuppliedQuantity())){
					stockQuantityChanged = Boolean.TRUE;
					stockQuantity = srtxCup.getAcceptedQuantity();
				}
			}
			if(srtxCup.getSuppliedOk()!=null && srtxCup.getSuppliedOk()){
				srtxCup.setIndSuppliedAccepted(BooleanType.YES.getCode());
			}else{
				srtxCup.setIndSuppliedAccepted(BooleanType.NO.getCode());
			}
		}
		
		/**VERIFICAMOS Y ACTUALIZAMOS CANTIDADES DE HECHOS DE MERCADO EN CUENTAS DE NEGOCIACION*/
		if(stockQuantityChanged){
			for(AccountTradeRequest accountTrade: sirtexOperation.getAccountSirtexOperations()){
				accountTrade.setStockQuantity(stockQuantity);
				List<AccountTradeMarketFact> marketFactList = accountTrade.getAccountSirtexMarkectfacts();
				if(accountTrade.getRole().equals(ParticipantRoleType.SELL.getCode())){ 
					if(!marketFactList.isEmpty()){
						if(marketFactList.size() == BigDecimal.ONE.intValue()){
							marketFactList.get(BigDecimal.ZERO.intValue()).setMarketQuantity(stockQuantity);
						}else{
							BigDecimal quantityMarkFact = BigDecimal.ZERO;
							for(AccountTradeMarketFact markFac: marketFactList){
								markFac.setMarketQuantity(markFac.getQuantityModify());
								quantityMarkFact = quantityMarkFact.add(markFac.getMarketQuantity());
							}
							if(!quantityMarkFact.equals(stockQuantity)) throw new ServiceException(ErrorServiceType.TRADE_OPERATION_ACC_MARKETFACT_DIFFERENT);
						}
					}
				}else{
					marketFactList.get(BigDecimal.ZERO.intValue()).setMarketQuantity(stockQuantity);
				}
			}
		}
		
		/**IF EXISTEN CAMBIOS EN LA REVISION, HACEMOS RECALCULOS A NIVEL DE OPERACION*/
		if(stockQuantityChanged){
			BigDecimal newStockQuantity = stockQuantity;
			Long operationID = sirtexOperationService.findTradeOperationId(sirtexOperation.getIdTradeRequestPk());
			/**DESBLOQUEAMOS LOS SALDOS COMPROMETIDOS EN VENTA Y COMPRA*/
			Long businessProcessType = BusinessProcessType.SIRTEX_OPERATION_REVIEW.getCode();
			sirtexSettlementService.unLockBalancesOfOperation(operationID, OperationPartType.CASH_PART.getCode(),null,businessProcessType);
			/**RECALCULAMOS LOS MONTOS A NIVEL DE OPERACION */
			sirtexSettlementService.recalculateAmountsOfOperation(OperationPartType.CASH_PART.getCode(), newStockQuantity, operationID);
			/**RECALCULAMOS MONTOS A NIVEL DE CUENTAS, HOLDER_ACCOUNT_OPERATION y PARTICIPANT_OPERATION*/
			List<AccountTradeRequest> newAccountOperations =  sirtexOperation.getAccountSirtexOperations();
			sirtexSettlementService.recalculateAccountsOfOperation(OperationPartType.CASH_PART.getCode(), newStockQuantity, operationID,newAccountOperations);
			/**VOLVEMOS A BLOQUEAR LOS SALDOS*/
			sirtexSettlementService.lockBalancesOfOperation(operationID, OperationPartType.CASH_PART.getCode());
		}
		/**CAMBIAMOS EL ESTADO DE LA SOLICITUD*/
		sirtexOperation = sirtexOperationService.reviewSirtexOpereation(sirtexOperation,stockQuantityChanged);
		return sirtexOperation;
	}
	
	/**
	 * Settlement term sirtex operation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_SETT_TERM)
	public TradeRequest settlementTermSirtexOperation(TradeRequest sirtexOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		/**OBTENEMOS EL ID DE LA SOLICITUD*/
		Long sirtexOperationId = sirtexOperation.getIdTradeRequestPk();
		/**VALIDAMOS LAS ENTIDADES DE LA SOLICITUD SIRTEX*/
		negotiationValidations.validateSirtexOperation(sirtexOperation);
		/**VALIDAMOS EL ESTADO DE LA SOLICITUD*/
		negotiationValidations.validateSirtexOperationState(sirtexOperation.getIdTradeRequestPk(), Arrays.asList(SirtexOperationStateType.CONFIRMED.getCode()));
		/**ALMACENAMOS LAS OPERACIONES QUE SE LIQUIDARAN EN PLAZO*/
		List<Long> operationTermSett = new ArrayList<Long>();
		/**Verified if settlement it's partial*/
		Long operationPk = null;
		for(CouponTradeRequest couponTradeReq: sirtexOperation.getCouponsTradeRequestList()){
			if(couponTradeReq.getIndSettlementOpe().equals(BooleanType.YES.getCode())){
				operationPk = couponTradeReq.getMechanismOperation().getIdMechanismOperationPk();break;
			}
		}
		/**OBTENEMOS LAS OPERACIONES DE NEGOCIACION PENDIENTE DE LIQUIDAR PLAZO*/
		List<MechanismOperation> operationsFromTrade = sirtexOperationService.findOperationsTradeRequest(sirtexOperationId,operationPk);
		for(MechanismOperation operation: operationsFromTrade){
			Date termDate = sirtexOperation.getTermSettlementDate();
			Long operationId = operation.getIdMechanismOperationPk();
			/**INVOCAMOS AL LIQUIDADOR DE VALORES*/
			sirtexSettlementService.settlementBalancesOfOperation(operationId, OperationPartType.TERM_PART.getCode(), termDate);
			operationTermSett.add(operation.getIdMechanismOperationPk());
		}
		/**VALIDAMOS SI SOLICITUDES FUERON LIQUIDADAS SIN MAYOR PROBLEMA*/
		validateOperationSettlements(operationTermSett, OperationPartType.TERM_PART.getCode());
		/**ACTUALIZAMOS EL ESTADO DE LA SOLICITUD SIRTEX*/
		sirtexOperationService.settTermSirtexOperation(sirtexOperationId);
		return sirtexOperation;
	}
	
	/**
	 * Settlement term anticipaded operation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the coupon trade request
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_SETT_TERM)
	public CouponTradeRequest settlementTermAnticipadedOperation(TradeRequest sirtexOperation) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		
		/**OBTENEMOS EL CUPON A ANTICIPAR*/
		CouponTradeRequest couponTrade = null;
		for(CouponTradeRequest couponTradeReq: sirtexOperation.getCouponsTradeRequestList()){
			if(couponTradeReq.getIndSettlementOpe().equals(BooleanType.YES.getCode())){
				couponTrade = couponTradeReq;break;
			}
		}
		if(couponTrade!=null){
			/**VERIFICAMOS EL MECHANISM_OPERATION A LIQUIDAR*/
			MechanismOperation operation = couponTrade.getMechanismOperation();
			/**CREAMOS EL SETTLEMENT_REQUEST*/
			SettlementRequest settlementRequest = sirtexOperationService.getSettlementRequest(operation);
			/**REGISTRAMOS EL SETTLEMENT_REQUEST*/
			prepaidExtendedService.saveSettlementDateRequest(settlementRequest, null, false, loggerUser);
			Long settlementRequestPk = settlementRequest.getIdSettlementRequestPk();
			/**APROBAMOS,REVIESAMOS,CONFIRMAMOS Y AUTORIZAMOS EL SETTLEMENT_REQUEST PARA LA FECHA DE LIQUIDACION*/
			prepaidExtendedService.executeChangeStateSettlementRequest(settlementRequestPk, SettlementDateRequestStateType.APPROVED.getCode(), null, null, null , false , loggerUser);
			
			prepaidExtendedService.executeChangeStateSettlementRequest(settlementRequestPk, SettlementDateRequestStateType.REVIEW.getCode(), null, null, null , false , loggerUser);
			
			prepaidExtendedService.executeChangeStateSettlementRequest(settlementRequestPk, SettlementDateRequestStateType.CONFIRMED.getCode(), null, null, null , false , loggerUser);
//			prepaidExtendedFacade.authorizeRequest(settlementRequestPk, settlementRequest.getLstSettlementDateOperations(),Boolean.FALSE);		
			/**OBTENEMOS LA FECHA FINAL DE LIQUIDACION PLAZO*/
			SettlementDateOperation settlementDateOperation = settlementRequest.getLstSettlementDateOperations().get(0);
			SettlementOperation settlementOperation = sirtexOperationService.find(SettlementOperation.class,settlementDateOperation.getSettlementOperation().getIdSettlementOperationPk());
			Date termDate = settlementDateOperation.getSettlementDate();
			settlementOperation.setSettlementDate(termDate);
			settlementOperation.setIndPrepaid(BooleanType.YES.getCode());
			settlementOperation.setIndPartial(BooleanType.NO.getCode());
			/**Update settlementOperation*/
			sirtexOperationService.update(settlementOperation);
			/**OBTENEMOS EL ID DEL MECHANISM_OPERATION*/
			Long operationId = operation.getIdMechanismOperationPk();
			/**INVOCAMOS AL LIQUIDADOR DE VALORES*/
			sirtexSettlementService.settlementBalancesOfOperation(operationId, OperationPartType.TERM_PART.getCode(), termDate);
			/**VALIDAMOS SI SOLICITUDES FUERON LIQUIDADAS SIN MAYOR PROBLEMA*/
			validateOperationSettlements(Arrays.asList(operationId), OperationPartType.TERM_PART.getCode());
			/**Method to verified if anticipated settlement it's ok*/
			sirtexOperationService.settTermSirtexOperation(couponTrade.getTradeRequest().getIdTradeRequestPk());

			/* SENDING THE CHANGE OWNERSHIP BY WEB SERVICE issue 1202 */
			sirtexOperation.setTradeOperationList(new ArrayList<TradeOperation>());
			sirtexOperation.getTradeOperationList().add(new TradeOperation());
			sirtexOperation.getTradeOperationList().get(0).setIdTradeOperationPk(operation.getIdMechanismOperationPk());
			sendSettlementTransferOperationWebClient(sirtexOperation,true);
		}	
		return couponTrade;
	}
	
	/**
	 * Confirm sirtex operation.
	 *
	 * @param sirtexOperation the sirtex operation
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_CONFIRM)
	public TradeRequest confirmSirtexOperation(TradeRequest sirtexOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		/**OBTENEMOS EL ID DE LA OPERACION RELACIONADA A LA SOLICITUD SIRTEX*/
		Long operationId = sirtexOperationService.findTradeOperationId(sirtexOperation.getIdTradeRequestPk());
		Long sirtexOperationId = sirtexOperation.getIdTradeRequestPk();
		/**VALIDAMOS LAS ENTIDADES DE LA SOLICITUD SIRTEX*/
		negotiationValidations.validateSirtexOperation(sirtexOperation);
		/**VALIDAMOS EL ESTADO DE LA SOLICITUD*/
		negotiationValidations.validateSirtexOperationState(sirtexOperation.getIdTradeRequestPk(), Arrays.asList(SirtexOperationStateType.REVIEWED.getCode()));
		/**CONFIRMAMOS LA SOLICITUD SIRTEX Y POSTERIORMENTE EL MECHANISM OPERATION*/
		sirtexOperation = sirtexOperationService.confirmSirtexOperation(sirtexOperationId);
		/**MANEJAMOS VARIABLE PARA DETERMINAR SI SOLICITUD SIRTEX TIENE DESPRENDIMIENTO ASOCIADO*/
		Boolean tradeHaveSplit = Boolean.FALSE;
		for(CouponTradeRequest tradeCup: sirtexOperation.getCouponsTradeRequestList()){
			if(!(tradeCup.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && tradeCup.getIndIsCapital().equals(BooleanType.NO.getCode()))){
				tradeHaveSplit = Boolean.TRUE;break;
			}
		}
		
		/**OBTENEMOS EL MECHANISM_MODALITY PARA DETERMINAR SI MANEJA PLAZO*/
		MechanismModality negModality = sirtexOperation.getMechanisnModality();
		Integer termPart = negModality.getNegotiationModality().getIndTermSettlement();
		/**VARIABLE PARA ALMACENAR TEMPORALMENTE EL VALOR RELACIONADO A SIRTEX*/
		Security securityTradeRequest = sirtexOperation.getSecurities();
		
		List<Long> operationsSettlement = new ArrayList<Long>(); 
		
		/**SI EXISTE DESPRENDIMIENTO DE CUPONES, ANULAMOS MECHANISM_OPERATION ASOCIADO Y EJECUTAMOS EL DESPRENDIMIENTO*/
		if(tradeHaveSplit){
			/**DESBLOQUEAMOS LOS SALDOS COMPROMETIDOS EN VENTA Y COMPRA*/
			Long businessProcessType = BusinessProcessType.SIRTEX_OPERATION_REJECT.getCode();
			sirtexSettlementService.unLockBalancesOfOperation(operationId, OperationPartType.CASH_PART.getCode(),null,businessProcessType);
			/**CREAMOS SOLICITUDES DE DESPRENDIMIENTO Y LO CONFIRMAMOS PARA LIBERAR VALORES Y SUBPRODUCTOS*/
			List<SplitCouponRequest> splitRequests = sirtexSplitCouponService.createSplitRequestFromTradeRaquest(sirtexOperation); 
													 sirtexSplitCouponService.confirmSplitRequestList(splitRequests);
													 
													 
			for(CouponTradeRequest couponTrade: sirtexOperation.getCouponsTradeRequestList()){
				/**VERIFICAMOS TODOS LOS CUPONES Y CAPITAL*/
				if(!(couponTrade.getCouponNumber().equals(BigDecimal.ZERO.intValue()) && couponTrade.getIndIsCapital().equals(BooleanType.NO.getCode()))){
					BigDecimal acceptQuantity =	couponTrade.getAcceptedQuantity(); 
					if(acceptQuantity!=null && !acceptQuantity.equals(BigDecimal.ZERO)){
						/**OBTENEMOS LOS SPLIT_OPERATION DESDE LA SOLICITUD REGISTRADA*/
						List<SplitCouponOperation> allSplitList = splitRequests.get(BigDecimal.ZERO.intValue()).getSplitCouponOperations();
						/**BUSCAMOS EL SPLIT_OPERATION EN BASE AL CUPON A NEGOCIAR DESDE LA SOLICITUD SIRTEX*/
						SplitCouponOperation splitDetail = sirtexSplitCouponService.getSplitOperationFromTrade(couponTrade, allSplitList);
						/**CANTIDAD ACEPTADA QUE FORMARA PARTE DEL PROCESO DE LIQUIDACION*/
						BigDecimal stockQuantity = couponTrade.getAcceptedQuantity();
						/**EN EL CASO QUE SEA CUPON EL VALOR ES EL CUPON DESPRENDIDO*/
						Security security = null;
						/**EN EL CASO QUE SEA EL CAPITAL, NECESITAMOS TRAER EL VALOR DESDE EL SUBPRODUCTO GENERADO*/
						if(couponTrade.getIndIsCapital().equals(BooleanType.YES.getCode())){
							security = splitRequests.get(BigDecimal.ZERO.intValue()).getSecurityProduct();
						}else{
							splitDetail = sirtexSettlementService.find(SplitCouponOperation.class, splitDetail.getIdSplitDetailPk());
							ProgramInterestCoupon coupon = sirtexSettlementService.find(ProgramInterestCoupon.class, splitDetail.getProgramInterestCoupon().getIdProgramInterestPk());
							security = coupon.getSplitSecurities();
						}
						NegotiationModalityType negModalityType = NegotiationModalityType.get(sirtexOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
						
						/**Get participantCode from session*/
						Long participantSession = userInfo.getUserAccountSession().getParticipantCode();
						
						/**CONFIG TRADE OPERATION y MECHANISM_OPERATION*/
						TradeOperation tradeOperation = new TradeOperation();
						tradeOperation.setRegisterUser(userInfo.getUserAccountSession().getUserName());
						tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
						tradeOperation.setOperationState(SirtexOperationStateType.REGISTERED.getCode());
						tradeOperation.setOperationType(negModalityType.getParameterOperationType());
						tradeOperation.setAudit(loggerUser);
						/**CREAMOS EL NUEVO MECHANISM_OPERATION*/
						MechanismOperation mechanismOperation = configNewMechanismOperation(negModality.getId().getIdNegotiationMechanismPk(),
																							negModality.getId().getIdNegotiationModalityPk(),
																							participantSession);
						mechanismOperation.setTradeOperation(tradeOperation);
						TradeRequest tradeRequest = sirtexOperation;
						tradeRequest.setSecurities(security);
						tradeRequest.setStockQuantity(stockQuantity);
						/**SI LA CANTIDAD ES DIFERENTE A LA CANTIDAD DE LA OPERACION*/
						if(!tradeRequest.getStockQuantity().equals(stockQuantity)){
							/*AQUI FALTA HACER EL RECALCULO DE ACCOUNT_TRADE_REQUEST A NIVEL DE STOCK_QUANTITY*/
							/*AQUI FALTA HACER EL RECALCULO DE ACCOUNT_TRADE_REQUEST A NIVEL DE STOCK_QUANTITY*/
						}
						/**CONFIGURAMOS EL NUEVO MECHANISM_OPERATION EN BASE AL TRADE_REQUEST*/
						mechanismOperation = sirtexOperationService.configOperationFromCoupon(tradeRequest, mechanismOperation, security,stockQuantity);
						/**REGISTRAMOS EL MECHANISM_OPERATION*/
						sirtexOperationService.registerMechanismOperation(mechanismOperation);
						/**CONFIRMAMOS EL MECHANISM_OPERATION Y LO DEJAMOS COMO ASIGNACION CONFIRMADA*/
						sirtexOperationService.confirmMechanismOperation(mechanismOperation.getIdMechanismOperationPk(),termPart);
						/**BLOQUEAMOS SALDOS COMPROMETIDOS EN VENTA*/
						sirtexSettlementService.lockBalancesOfOperation(mechanismOperation.getIdMechanismOperationPk(), ComponentConstant.CASH_PART);
						/**LIQUIDAMOS EL MECHANISM_OPERATION */
						Date date = mechanismOperation.getCashSettlementDate();
						sirtexSettlementService.settlementBalancesOfOperation(mechanismOperation.getIdMechanismOperationPk(), ComponentConstant.CASH_PART, date);
						couponTrade.setMechanismOperation(new MechanismOperation(mechanismOperation.getIdMechanismOperationPk()));
						operationsSettlement.add(mechanismOperation.getIdMechanismOperationPk());
					}
				}
			}
		}else{
			/**CONFIRMAMOS EL MECHANISM_OPERATION ASOCIADO A LA SOLICITUD SIRTEX*/
			sirtexOperationService.confirmMechanismOperation(operationId,termPart);
			/**OBTENEMOS LA FECHA DE LIQUIDACION CONTADO*/
			Date date = sirtexOperation.getCashSettlementDate();
			/**LIQUIDAMOS EL MECHANISM_OPERATION RELACIONADO A LA SOLICITUD SIRTEX*/
			sirtexSettlementService.settlementBalancesOfOperation(operationId, OperationPartType.CASH_PART.getCode(), date);
			sirtexOperation.getCouponsTradeRequestList().get(BigDecimal.ZERO.intValue()).setMechanismOperation(new MechanismOperation(operationId));
			operationsSettlement.add(operationId);
		}
		sirtexOperation.setSecurities(securityTradeRequest);
		/**VALIDATE IF ALL OPERATION ARE SETTLED SUCESSFULLY*/
		validateOperationSettlements(operationsSettlement, OperationPartType.CASH_PART.getCode());
		/**ACTUALIZAMOS EL MECHANISM_OPERATION RELACIONADO AL CUPON DESPRENDIDO*/
		sirtexOperationService.updateList(sirtexOperation.getCouponsTradeRequestList());
		
		//issue 996 - SIRTEX reporto - issuer habilitado/dehabilitado
		Security securityEnable = sirtexOperation.getSecurities();
		String idIssuer = securityEnable.getIssuer().getIdIssuerPk();
		Issuer issuerObj = sirtexOperationService.getSirtexHabilitationState(idIssuer);
		if(issuerObj.getIndSirtexNegotiate() != null) {
			if(issuerObj.getIndSirtexNegotiate().equals("0")) {
				//actualizar si esta disabled 
				sirtexOperationService.updateSecuritySirtexNegotiation(securityEnable);
			}
		}
		
		/* SENDING THE CHANGE OWNERSHIP BY WEB SERVICE issue 1202 */
		sendSettlementTransferOperationWebClient(sirtexOperation,false);
		
		return sirtexOperation; 
	}
	
	public void sendSettlementTransferOperationWebClient(TradeRequest mechanismOperation, boolean anticipate){
		List<SettlementOperationTO> lstSettlementOperationTOs = new ArrayList<>();
		SettlementOperationTO operationTO = new SettlementOperationTO();
		MechanismOperation mechanismOperationAux;
		
		//Obteniendo Comprador y Vendedor
		List<SettlementHolderAccountTO> lstSellerHolderAccounts = new ArrayList<>();
		List<SettlementHolderAccountTO> lstBuyerHolderAccounts = new ArrayList<>();
		SettlementHolderAccountTO accountTO;
		
		for(AccountTradeRequest holderAccountOperation: mechanismOperation.getAccountSirtexOperations()){
			if(holderAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
				accountTO = new SettlementHolderAccountTO();
				accountTO.setIdHolderAccount(holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
				if(anticipate)lstBuyerHolderAccounts.add(accountTO);
				else lstSellerHolderAccounts.add(accountTO);
			}else if(holderAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
				accountTO = new SettlementHolderAccountTO();
				accountTO.setIdHolderAccount(holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
				if(anticipate)lstSellerHolderAccounts.add(accountTO);
				else lstBuyerHolderAccounts.add(accountTO);
			}
		}
		mechanismOperationAux = sirtexOperationService.getMechanismOperationbyId(mechanismOperation.getTradeOperationList().get(0).getIdTradeOperationPk());
		operationTO.setIdSettlementOperation(BusinessProcessType.SIRTEX_OPERATION_CONFIRM.getCode()); 
		operationTO.setIdMechanismOperation(mechanismOperationAux.getIdMechanismOperationPk());
		operationTO.setInterfaceTransferCode(mechanismOperationAux.getMechanisnModality().getInterfaceTransferCode());
		operationTO.setIdSecurityCode(mechanismOperation.getSecurities().getIdSecurityCodePk());
		operationTO.setSecurityClass(mechanismOperation.getSecurities().getSecurityClass());
		operationTO.setOperationNumber(mechanismOperationAux.getOperationNumber());
		operationTO.setLstBuyerHolderAccounts(lstBuyerHolderAccounts);   //comprador                                                 /////
		operationTO.setLstSellerHolderAccounts(lstSellerHolderAccounts); //vendedor
		
		lstSettlementOperationTOs.add(operationTO);
		this.sendSettlementOperationWebClient(lstSettlementOperationTOs);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendSettlementOperationWebClient(List<SettlementOperationTO> lstSettlementOperationTOs) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		} catch (Exception e) {
			System.out.println("ERROR EN PRIVILEGIOS");
		}
		settlementProcessServiceBean.sendSettlementOperationWebClient(lstSettlementOperationTOs, loggerUser);
	}
	
	/**
	 * Validate operation settlements.
	 *
	 * @param idOperationSettled the id operation settled
	 * @param operationPart the operation part
	 * @throws ServiceException the service exception
	 */
	public void validateOperationSettlements(List<Long> idOperationSettled, Integer operationPart) throws ServiceException{
		for(Integer state: sirtexSettlementService.getSettledOperationState(idOperationSettled)){
			if(operationPart.equals(OperationPartType.CASH_PART.getCode())){
				if(!state.equals(MechanismOperationStateType.CASH_SETTLED.getCode())){
					throw new ServiceException(ErrorServiceType.TRADE_OPERATION_SETTLEMENT_ERROR);
				}
			}else if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
				if(!state.equals(MechanismOperationStateType.TERM_SETTLED.getCode())){
					throw new ServiceException(ErrorServiceType.TRADE_OPERATION_SETTLEMENT_ERROR);
				}
			}
		}
	}
	
	/**
	 * Liquidar inconsistencia.
	 *
	 * @param operationID the operation id
	 * @throws ServiceException the service exception
	 */
	public void liquidarInconsistencia(Long operationID) throws ServiceException{
		Date date = CommonsUtilities.currentDate();
//		date = CommonsUtilities.addDate(date, -1);
//		sirtexSettlementService.lockBalancesOfOperation(operationID, OperationPartType.CASH_PART.getCode());
		sirtexSettlementService.settlementBalancesOfOperation(operationID, OperationPartType.CASH_PART.getCode(), date);
	}
	
	/**
	 * Gets the sirtex operations service facade.
	 *
	 * @param sirtexOperationTO the sirtex operation to
	 * @param parDesc the par desc
	 * @return the sirtex operations service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_OPERATION_QUERY)
	public List<SirtexOperationResultTO> getSirtexOperationsServiceFacade(SirtexOperationTO sirtexOperationTO, Map<Integer,String> parDesc) throws ServiceException{
		//LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	
		List<SirtexOperationResultTO> sirtexTOList = sirtexOperationService.getSirtexOperationsServiceBean(sirtexOperationTO); 
		for(SirtexOperationResultTO sirtexTO: sirtexTOList){
			sirtexTO.setOperationStateDesc((String) parDesc.get(sirtexTO.getOperationState()));
		}
		return sirtexTOList;
	}
	
	/**
	 * Find mechanism operation from id.
	 *
	 * @param mechanismOperationID the mechanism operation id
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation findMechanismOperationFromId(Long mechanismOperationID) throws ServiceException{
		return sirtexOperationService.findMechanismOperationFromId(mechanismOperationID);
	}
	
	/**
	 * Find sirtex operation from id.
	 *
	 * @param sirtexOperationID the sirtex operation id
	 * @param parameters the parameters
	 * @return the trade request
	 * @throws ServiceException the service exception
	 */
	public TradeRequest findSirtexOperationFromId(Long sirtexOperationID, Map<Integer,String> parameters) throws ServiceException{
		return sirtexOperationService.findSirtexOperationFromId(sirtexOperationID,parameters);
	}
	
	/**
	 * Search holder by holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder searchHolderByHolderAccount(Long idHolderAccountPk) throws ServiceException{
		return sirtexOperationService.searchHolderByHolderAccount(idHolderAccountPk);
	}
	
	/**
	 * Generate coupon trade coupons.
	 *
	 * @param tradeRequest the trade request
	 * @param isSplit the is split
	 * @param params the params
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CouponTradeRequest> generateCouponTradeCoupons(TradeRequest tradeRequest, boolean isSplit, Map<Integer,String> params) throws ServiceException{
		List<CouponTradeRequest> coupons = new ArrayList<CouponTradeRequest>();
		CouponTradeRequest mainTradeRequest = tradeRequest.getCouponsTradeRequestList().get(0);
		mainTradeRequest.setSuppliedQuantity(null);
		coupons.add(mainTradeRequest);
		
		if(isSplit){
			Security security = tradeRequest.getSecurities();
			BigDecimal availableBalance = mainTradeRequest.getOriginAvailableBalance();
			CouponTradeRequest capitalTradeReq = getCouponTradeForCapital(security, availableBalance, tradeRequest, params ); 
			if(security.getSecurityClass().equals(SecurityClassType.DPF.getCode()))
				capitalTradeReq.setSuppliedQuantity(new BigDecimal(1));
			coupons.add(capitalTradeReq);
			
			Map<Integer,ProgramInterestCoupon> securityCoupons = sirtexOperationService.findInterestCoupons(security.getIdSecurityCodePk());
			
			for(Map.Entry<Integer,ProgramInterestCoupon> entry : securityCoupons.entrySet()){
				ProgramInterestCoupon interestCoupon = entry.getValue();
				/**GENERATE COUPON-TRADE REQUEST FROM INTEREST COUPON*/
				CouponTradeRequest couponTradeReq = getCouponTradeFromInteres(interestCoupon, security, availableBalance,params);
				couponTradeReq.setTradeRequest(tradeRequest);
				coupons.add(couponTradeReq);
			}
			Collections.sort(coupons,COMPARE_BY_COUPON_NUMBER);
		}
		return coupons;
	}
	
	/**
	 * Generate coupon trade from coupons.
	 *
	 * @param tradeRequest the trade request
	 * @param couponTrade the coupon trade
	 * @param params the params
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CouponTradeRequest> generateCouponTradeFromCoupons(TradeRequest tradeRequest, CouponTradeRequest couponTrade, Map<Integer,String> params)
																	throws ServiceException{
		String[] couponNumbers = couponTrade.getCouponDescription().split(",");
		List<CouponTradeRequest> coupons = new ArrayList<CouponTradeRequest>();
		coupons.add(couponTrade);
		
		Security security = tradeRequest.getSecurities();
		Security securityFather = tradeRequest.getSecurityParent();/**OBTENEMOS EL VALOR PAPA EN EL CASO QUE SEA SUB-PRODUCTO*/
		
		Map<Integer,ProgramInterestCoupon> securityCoupons = sirtexOperationService.findInterestCoupons(security.getIdSecurityCodePk());
		Map<Integer,CouponTradeRequest> couponsMap = new HashMap<Integer,CouponTradeRequest>();
		
		for(String coupon: couponNumbers){
			Integer couponNumber = new Integer(coupon.toString());
			ProgramInterestCoupon interestCoupon = securityCoupons.get(couponNumber);
			if(interestCoupon!=null){
				/**SI ES SUB-PRODUCTO, SE NECESITA NEGOCIAR LA CUPONERA DEL VALOR PADRE*/
				if(securityFather!=null){
					security = securityFather;
				}
				
				if(!interestCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
					throw new ServiceException(ErrorServiceType.TRADE_OPERATION_COUPON_NOT_PENDIENT, new Object[]{couponNumber});
				}
				
				/**GENERATE COUPON-TRADE REQUEST FROM INTEREST COUPON*/
				CouponTradeRequest couponTradeReq = getCouponTradeFromInteres(interestCoupon, security, couponTrade.getOriginAvailableBalance(),params);
				if(security.getSecurityClass().equals(SecurityClassType.DPF.getCode()))
					couponTradeReq.setSuppliedQuantity(new BigDecimal(1));
				couponTradeReq.setTradeRequest(tradeRequest);
				couponsMap.put(couponNumber, couponTradeReq);
			}else{
				throw new ServiceException(ErrorServiceType.TRADE_OPERATION_COUPON_NOTEXIST);
			}
		}
		coupons.addAll(couponsMap.values());
		Collections.sort(coupons,COMPARE_BY_COUPON_NUMBER);
		return coupons;
	}
	
	 /** The compare by coupon number. */
 	public static Comparator<CouponTradeRequest> COMPARE_BY_COUPON_NUMBER = new Comparator<CouponTradeRequest>() {
		 public int compare(CouponTradeRequest o1, CouponTradeRequest o2) {
				/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
				int resultado = o1.getCouponNumber()-o2.getCouponNumber();
		        if ( resultado != 0 ) { return resultado; }
				return resultado;
			}
	    };
	
	/**
	 * Gets the coupon trade from interes.
	 *
	 * @param interestCoupon the interest coupon
	 * @param security the security
	 * @param availableBal the available bal
	 * @param param the param
	 * @return the coupon trade from interes
	 * @throws ServiceException the service exception
	 */
	public CouponTradeRequest getCouponTradeFromInteres(ProgramInterestCoupon interestCoupon, Security security, BigDecimal availableBal, Map<Integer,String> param) 
																throws ServiceException{
		Long MILLSECS_PER_DAY = new Long  (24 * 60 * 60 * 1000);
		Long test=(interestCoupon.getExperitationDate().getTime()-CommonsUtilities.currentDate().getTime())/MILLSECS_PER_DAY;
		CouponTradeRequest newCouponTrade = new CouponTradeRequest();
		newCouponTrade.setRegistryDate(CommonsUtilities.currentDate());
		newCouponTrade.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		newCouponTrade.setProgramInterestCoupon(interestCoupon);
		newCouponTrade.setExpiredDate(interestCoupon.getExperitationDate());
		newCouponTrade.setDaysOfLife(CommonsUtilities.getDaysBetween(interestCoupon.getBeginingDate(),interestCoupon.getExperitationDate()));
		newCouponTrade.setCouponNumber(interestCoupon.getCouponNumber());
		newCouponTrade.setIndIsCapital(BooleanType.NO.getCode());
		newCouponTrade.setOriginAvailableBalance(availableBal);
		newCouponTrade.setPendientDays(test.intValue());
		String securityCouponDesc = sirtexOperationService.generateSecurityCouponDescription(security, interestCoupon.getCouponNumber(),BooleanType.YES.getCode());
		newCouponTrade.setSecurityCouponDescription(securityCouponDesc);
		newCouponTrade.setSecurityCouponBcb(sirtexOperationService.generateSecurityCouponBcb(security, interestCoupon.getCouponNumber(), param));
		return newCouponTrade;
	}
	
	/**
	 * Gets the coupon trade for capital.
	 *
	 * @param security the security
	 * @param availableBal the available bal
	 * @param tradeRequest the trade request
	 * @param param the param
	 * @return the coupon trade for capital
	 * @throws ServiceException the service exception
	 */
	public CouponTradeRequest getCouponTradeForCapital(Security security, BigDecimal availableBal, TradeRequest tradeRequest, Map<Integer,String> param) 
																throws ServiceException{
		Long MILLSECS_PER_DAY = new Long  (24 * 60 * 60 * 1000);
		Long test=(security.getExpirationDate().getTime()-CommonsUtilities.currentDate().getTime())/MILLSECS_PER_DAY;
		CouponTradeRequest newCouponTrade = new CouponTradeRequest();
		newCouponTrade.setRegistryDate(CommonsUtilities.currentDate());
		newCouponTrade.setRegistryUser(userInfo.getUserAccountSession().getUserName());
//		newCouponTrade.setProgramInterestCoupon(interestCoupon);
		newCouponTrade.setExpiredDate(security.getExpirationDate());
		newCouponTrade.setDaysOfLife(CommonsUtilities.getDaysBetween(security.getRegistryDate(),security.getExpirationDate()));
		newCouponTrade.setCouponNumber(BigDecimal.ZERO.intValue());
		newCouponTrade.setIndIsCapital(BooleanType.YES.getCode());
		newCouponTrade.setOriginAvailableBalance(availableBal);
		newCouponTrade.setTradeRequest(tradeRequest);
		newCouponTrade.setPendientDays(test.intValue());
		String securityCouponDesc = sirtexOperationService.generateSecurityCouponDescription(security, newCouponTrade.getCouponNumber(),BooleanType.NO.getCode());
		newCouponTrade.setSecurityCouponDescription(securityCouponDesc);
		newCouponTrade.setSecurityCouponBcb(sirtexOperationService.generateSecurityCouponBcb(security, BigDecimal.ZERO.intValue(), param)); 
		return newCouponTrade;
	}
	
	/**
	 * Number of market fact.
	 *
	 * @param participantId the participant id
	 * @param accountId the account id
	 * @param idSecurityCode the id security code
	 * @return the integer
	 */
	public Integer numberOfMarketFact(Long participantId, Long accountId, String idSecurityCode) {
		return sirtexOperationService.numberOfMarketFact(participantId, accountId, idSecurityCode);
	}
	
	public List<String> getSirtexReportingOperation(Date processDate) throws ServiceException{	
		return sirtexOperationService.getSirtexReportingOperation(processDate);	
		
	}
		
}