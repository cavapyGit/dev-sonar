package com.pradera.negotiations.sirtex.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SalePurchaseUpdateComponentService;
import com.pradera.integration.component.business.to.AccountOperationTO;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.AccountTradeMarketFact;
import com.pradera.model.negotiation.AccountTradeRequest;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.SettlementProcessType;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OtcAndSirtexSettlementService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class OtcAndSirtexSettlementService extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/** The sale purchase update component service. */
	@Inject Instance<SalePurchaseUpdateComponentService> salePurchaseUpdateComponentService;
	
	/** The accounts component service. */
	@Inject Instance<AccountsComponentService> accountsComponentService;
	
	/** The settlement process service. */
	@EJB private SettlementProcessService settlementProcessService;
	
	/** The sirtex operation service. */
	@EJB private SirtexOperationService sirtexOperationService;
	
	/** The settlement process facade. */
	@EJB private SettlementsComponentSingleton settlementProcessFacade;
	
	/** The settlement service. */
	@EJB private SettlementProcessService settlementService;
	
	
	/**
	 * Recalculate amounts of operation.
	 *
	 * @param operationPart the operation part
	 * @param stockQuantity the stock quantity
	 * @param operationID the operation id
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void recalculateAmountsOfOperation(Integer operationPart, BigDecimal stockQuantity, Long operationID) throws ServiceException{
		// TODO Auto-generated method stub
		MechanismOperation operation = find(MechanismOperation.class,operationID);
		BigDecimal price = operationPart.equals(OperationPartType.CASH_PART.getCode())?operation.getCashPrice():operation.getTermPrice();
		BigDecimal realPrice = operation.getRealCashPrice();

		operation.setStockQuantity(stockQuantity);
		if(operationPart.equals(OperationPartType.CASH_PART.getCode())){
			if(price!=null){
				operation.setCashAmount(stockQuantity.multiply(price));
			}
			
			if(realPrice!=null){
				operation.setRealCashAmount(stockQuantity.multiply(realPrice));
			}
		}else if(operationPart.equals(OperationPartType.TERM_PART.getCode())){
			if(price!=null){
				operation.setTermAmount(stockQuantity.multiply(price));
			}
		}
		
		List<SettlementOperation> settOperationList = new ArrayList<SettlementOperation>();
		SettlementOperation settCashOperation = (SettlementOperation) settlementService.getSettlementOperation(operationID,ComponentConstant.CASH_PART);
		SettlementOperation settTermOperation = (SettlementOperation) settlementService.getSettlementOperation(operationID,ComponentConstant.TERM_PART);
		settOperationList.add(settCashOperation);
		if(settTermOperation!=null){
			settOperationList.add(settTermOperation);
		}
		
		for(SettlementOperation settOperation: settOperationList){
			settOperation.setStockQuantity(stockQuantity);
			settOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(price, stockQuantity));
			update(settOperation);
			
			/**Find Participant Settlement && update*/
			List<ParticipantSettlement> partSettlementList = (ArrayList<ParticipantSettlement>)
															 settlementService.getParticipantSettlements(settOperation.getIdSettlementOperationPk(),null,null);
			
			for(ParticipantSettlement partSettlement: partSettlementList){
				partSettlement.setStockQuantity(stockQuantity);
				partSettlement.setSettlementAmount(NegotiationUtils.getSettlementAmount(price, stockQuantity));
				update(partSettlement);
			}
		}
		
		update(operation);
	}
	
//	public void recalculateAmountsOfRequest(Integer operationPart, )

	/**
 * Recalculate accounts of operation.
 *
 * @param operationPart the operation part
 * @param stockQuantity the stock quantity
 * @param operationId the operation id
 * @param accounts the accounts
 * @throws ServiceException the service exception
 */
	public void recalculateAccountsOfOperation(Integer operationPart, BigDecimal stockQuantity, Long operationId, 
											   List<AccountTradeRequest> accounts) throws ServiceException{
		MechanismOperation operation = find(MechanismOperation.class,operationId);
		BigDecimal price = operationPart.equals(OperationPartType.CASH_PART.getCode())?operation.getCashPrice():operation.getTermPrice();
		
		/**ITERAMOS LAS HOLDER_ACCOUNT_OPERATION Y MANEJAMOS DENTRO DE UN MAP*/
		Map<Long,AccountTradeRequest> mapOfAccountOperation = new HashMap<Long,AccountTradeRequest>();
		for(AccountTradeRequest accountOperation: accounts){
			Long accountOperationId = accountOperation.getHolderAccount().getIdHolderAccountPk(); 
			mapOfAccountOperation.put(accountOperationId, accountOperation);
		}
		
		/**FIRST ACTUALIZAMOS LOS HOLDER_ACCOUNT_OPERATION*/
		for(HolderAccountOperation oldAccountOperation: operation.getHolderAccountOperations()){
			
			/**Config SettlementAccountOperation && Update stockQuantity*/
			SettlementAccountOperation settAccOperation = settlementService.getSettlementAccountOperation(oldAccountOperation.getIdHolderAccountOperationPk());
			settAccOperation.setStockQuantity(stockQuantity);
			if(price!=null && stockQuantity!=null){
				settAccOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(price, stockQuantity));
			}			
			update(settAccOperation);
			
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				if(oldAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
					for(AccountOperationMarketFact accountMarkFact: oldAccountOperation.getAccountOperationMarketFacts()){
						accountMarkFact.setOperationQuantity(stockQuantity);
						update(accountMarkFact);
					}
					
					/**Config SettlementMarketFact && update stockQuantity*/
					for(SettlementAccountMarketfact settAccMarketFact: settAccOperation.getSettlementAccountMarketfacts()){
						settAccMarketFact.setMarketQuantity(stockQuantity);
						update(settAccMarketFact);
					}
				}else if(oldAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
					AccountTradeRequest newAccOperation = mapOfAccountOperation.get(oldAccountOperation.getHolderAccount().getIdHolderAccountPk());
					_$JUMP:/**VARIABLE TO JUMP WHEN EXIST SOME VALIDATION*/
					for(Iterator<AccountOperationMarketFact> oldIterat = oldAccountOperation.getAccountOperationMarketFacts().iterator(); oldIterat.hasNext();){
						AccountOperationMarketFact oldMarkFact = oldIterat.next();						
						for(Iterator<AccountTradeMarketFact> newIterat = newAccOperation.getAccountSirtexMarkectfacts().iterator(); newIterat.hasNext();){
							AccountTradeMarketFact newMarkFact = newIterat.next();							
							if(oldMarkFact.getMarketDate().equals(newMarkFact.getMarketDate())){
								if(oldMarkFact.getMarketRate()!=null && oldMarkFact.getMarketRate().equals(newMarkFact.getMarketRate())){
									oldMarkFact.setOperationQuantity(newMarkFact.getMarketQuantity());
									update(oldMarkFact);
									continue _$JUMP;
								}
								if(oldMarkFact.getMarketPrice()!=null && oldMarkFact.getMarketPrice().equals(newMarkFact.getMarketPrice())){
									oldMarkFact.setOperationQuantity(newMarkFact.getMarketQuantity());
									update(oldMarkFact);
									continue _$JUMP;
								}
							}
						}
					}
					
					__$JUMP:
					for(Iterator<SettlementAccountMarketfact> oldIterat = settAccOperation.getSettlementAccountMarketfacts().iterator(); oldIterat.hasNext();){
						SettlementAccountMarketfact oldMarkFact = oldIterat.next();						
						for(Iterator<AccountTradeMarketFact> newIterat = newAccOperation.getAccountSirtexMarkectfacts().iterator(); newIterat.hasNext();){
							AccountTradeMarketFact newMarkFact = newIterat.next();							
							if(oldMarkFact.getMarketDate().equals(newMarkFact.getMarketDate())){
								if(oldMarkFact.getMarketRate()!=null && oldMarkFact.getMarketRate().equals(newMarkFact.getMarketRate())){
									oldMarkFact.setMarketQuantity(newMarkFact.getMarketQuantity());
									update(oldMarkFact);
									continue __$JUMP;
								}
								if(oldMarkFact.getMarketPrice()!=null && oldMarkFact.getMarketPrice().equals(newMarkFact.getMarketPrice())){
									oldMarkFact.setMarketQuantity(newMarkFact.getMarketQuantity());
									update(oldMarkFact);
									continue __$JUMP;
								}
							}
						}
					}
				}
			}
			
			/**UPDATE HOLDER_ACCOUNT_OPERATION*/
			if(oldAccountOperation.getOperationPart().equals(operationPart)){
				oldAccountOperation.setStockQuantity(stockQuantity);
				if(price!=null){
					oldAccountOperation.setCashAmount(stockQuantity.multiply(price));
				}
				update(oldAccountOperation);
			}
		}
		
//		/**SECOND, ACTUALIZAMOS LOS PARTICIPANT OPERATIONS*/
//		for(ParticipantOperation participantOperation: operation.getParticipantOperations()){
//			if(participantOperation.getOperationPart().equals(operationPart)){
//				participantOperation.setStockQuantity(stockQuantity);
//				if(price!=null){
//					participantOperation.setCashAmount(stockQuantity.multiply(price));
//				}
//				update(participantOperation);
//			}
//		}
	}
	
	public SettlementOperation getSettlementOperation(Long operationID, Integer operationPart) {
		
		return (SettlementOperation) settlementProcessService.getSettlementOperation(operationID, operationPart);
		
	}
	
	public Integer getUnSettlementOperationState(Long operationID) {
		
		return settlementProcessService.getUnsettledListSettlementOperation(operationID);
		
	}
	/**
	 * Lock balances of operation.
	 *
	 * @param operationID the operation id
	 * @param operationPart the operation part
	 * @throws ServiceException the service exception
	 */
	public void lockBalancesOfOperation(Long operationID, Integer operationPart) throws ServiceException{
		MechanismOperation operation = find(MechanismOperation.class, operationID);
		Security security = operation.getSecurities();
		SettlementOperation settlementOperation = (SettlementOperation) settlementProcessService.getSettlementOperation(operationID, operationPart);
		
		Long idMechanism = operation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		Long businessProcess = BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode();
		Long idModality = operation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		Long idSettOper = settlementOperation.getIdSettlementOperationPk();
		
		List<SettlementAccountOperation> settlementAccountOperations = settlementProcessService.getSettlementAccountOperations(idSettOper, null);
		for(SettlementAccountOperation settlementAccountOperation : settlementAccountOperations){
			AccountOperationTO accountOperationTO = new AccountOperationTO();
			accountOperationTO.setTotalStockQuantity(settlementAccountOperation.getHolderAccountOperation().getStockQuantity());
			accountOperationTO.setIdMechanism(idMechanism);
			accountOperationTO.setIdModality(idModality);
			accountOperationTO.setIdHolderAccount(settlementAccountOperation.getHolderAccountOperation().getHolderAccount().getIdHolderAccountPk());
			accountOperationTO.setIdHolderAccountOperation(settlementAccountOperation.getHolderAccountOperation().getIdHolderAccountOperationPk());
			accountOperationTO.setIdMechanismOperation(operationID);
			accountOperationTO.setIdParticipant(settlementAccountOperation.getHolderAccountOperation().getInchargeStockParticipant().getIdParticipantPk());
			accountOperationTO.setIdRole(settlementAccountOperation.getRole());
			accountOperationTO.setIdSecurityCode(security.getIdSecurityCodePk());
			accountOperationTO.setIdSettlementAccountOperation(settlementAccountOperation.getIdSettlementAccountPk());
			accountOperationTO.setIdSettlementOperation(settlementOperation.getIdSettlementOperationPk());
			accountOperationTO.setStockQuantity(settlementAccountOperation.getHolderAccountOperation().getStockQuantity());
			accountOperationTO.setIndInchain(ComponentConstant.ZERO);
			accountOperationTO.setInstrumentType(security.getInstrumentType());
			accountOperationTO.setBlockQuantity(settlementAccountOperation.getHolderAccountOperation().getStockQuantity());
			accountOperationTO.setOperationPart(operationPart);
			
			Long settlementAccountOperationPk = settlementAccountOperation.getIdSettlementAccountPk();
			for(SettlementAccountMarketfact settlementMrkFact: settlementProcessService.getSettlementAccountMarketfacts(settlementAccountOperationPk)){
				accountOperationTO.setCashPrice(settlementMrkFact.getMarketPrice());
			}
			
			salePurchaseUpdateComponentService.get().blockSalePurchaseBalances(accountOperationTO, businessProcess, idepositarySetup.getIndMarketFact());
		}
	}
	
	/**
	 * Lock balances of sirtex operation.
	 *
	 * @param operationID the operation id
	 * @param operationPart the operation part
	 * @param businessProcessType the business process type
	 * @throws ServiceException the service exception
	 */
	public void lockBalancesOfSirtexOperation(Long operationID, Integer operationPart, Long businessProcessType) throws ServiceException{
		TradeRequest operation = find(TradeRequest.class, operationID);
		/*LISTAS PARA MANEJAR CUENTAS VENDEDORAS - COMPRADORES como ORIGEN - DESTINO*/
		List<HolderAccountBalanceTO> accountsSellers = new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> accountsBuyers = new ArrayList<HolderAccountBalanceTO>();
		
		/*ITERAMOS CUENTAS INVERSIONISTAS*/
		for(AccountTradeRequest sirtexAccount: operation.getAccountSirtexOperations()){
			if(operationPart.equals(sirtexAccount.getOperationPart())){
				/*GENERAMOS LOS ACCOUNT_BALANCE_TO, PARA INVOKAR AL COMPONENTE DE SALDOS*/
				switch(ParticipantRoleType.get(sirtexAccount.getRole())){
					case SELL: accountsSellers.addAll(getAccountSirtexBalanceTOs(sirtexAccount,sirtexAccount.getStockQuantity()));break;
					case BUY:  accountsBuyers.addAll(getAccountSirtexBalanceTOs(sirtexAccount,sirtexAccount.getStockQuantity()));break; 
				}
			}
		}
		
		/*VARIABLES PARA OBTENER EL OPERATION TYPE*/
		Long modalityID = operation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		Long operationType = NegotiationModalityType.get(modalityID).getParameterOperationType();
		
		/*CREAMOS EL ACCOUNT COMPONENTE CON EL NUEVO OPERATION TYPE*/
		AccountsComponentTO unLockBalances = getAccountsComponentTO(operationType,businessProcessType,operationID,accountsSellers,accountsBuyers,operationPart);
		
		/*INVOCAMOS A COMPONENTE DE CUENTAS PARA BLOQUEAR LOS SALDOS A COMPROMETIDOS EN VENTA Y COMPRA*/
		accountsComponentService.get().executeAccountsComponent(unLockBalances);
	}
	
	/**
	 * Settlement balances of operation.
	 *
	 * @param operationID the operation id
	 * @param operationPart the operation part
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void settlementBalancesOfOperation(Long operationID, Integer operationPart, Date settlementDate) throws ServiceException{
		this.settlementBalancesOfOperation(operationID, operationPart, settlementDate, Boolean.TRUE);
	}
	
	public void settlementBalancesOfOperation(Long operationID, Integer operationPart, Date settlementDate, Boolean excludeFOP) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		MechanismOperation operation = find(MechanismOperation.class, operationID);
		
		Long mechanismID = operation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
		Long modalityID = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
		Long modGroupID = findModalityGroup(mechanismID, modalityID).getIdModalityGroupPk();
		Integer settlScheme = operation.getSettlementSchema();
		Integer currency = operation.getCurrency();
		SettlementProcess settlementProcess = findSettlementProcess(mechanismID, currency, settlScheme, modGroupID);
		if(settlementProcess==null){
			Integer partialProc = SettlementProcessType.PARTIAL.getCode();
			settlementProcess = settlementProcessService.saveSettlementProcess(mechanismID, modGroupID, currency, settlementDate, settlScheme, 0L,null, partialProc, null);
		}
		Long settProcessID = settlementProcess.getIdSettlementProcessPk();

		SettlementOperation settlementOperation = (SettlementOperation) settlementProcessService.getSettlementOperation(operationID, operationPart);
		
		/*INVOCAMOS EL LIQUIDADOR DE VALORES*/
		settlementProcessService.updateSettlementProcessState(settProcessID,SettlementProcessStateType.IN_PROCESS.getCode(),null,loggerUser,null);
		Long settlementOperationId = settlementOperation.getIdSettlementOperationPk();
		settlementProcessFacade.settleGrossMechanismOperations(settProcessID, mechanismID, modGroupID, settlementDate, currency, operationPart,null, settlScheme,loggerUser, settlementOperationId, excludeFOP);
		settlementProcessService.updateSettlementProcessState(settProcessID,SettlementProcessStateType.WAITING.getCode(),null,loggerUser,null);
//		Boolean isFinalProcess = Boolean.FALSE;	
//		settlementProcessFacade.executeGrossSettlementProcess(mechanismID, modGroupID, currency, settlementDate, settProcessID, isFinalProcess,null,loggerUser, operationID);
	}
	
	/**
	 * Find modality group.
	 *
	 * @param mechanismID the mechanism id
	 * @param modalityID the modality id
	 * @return the modality group
	 * @throws ServiceException the service exception
	 */
	public ModalityGroup findModalityGroup(Long mechanismID, Long modalityID) throws ServiceException{
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("SELECT mgd.modalityGroup 													");
		stringQuery.append("FROM ModalityGroupDetail mgd 												");
		stringQuery.append("WHERE mgd.negotiationModality.idNegotiationModalityPk = :modalityID		AND	");
		stringQuery.append("	  mgd.negotiationMechanism.idNegotiationMechanismPk = :mechanismID		");
		Query query = em.createQuery(stringQuery.toString());
		query.setParameter("modalityID", modalityID);
		query.setParameter("mechanismID", mechanismID);
		return (ModalityGroup) query.getSingleResult();
	}
	
	/**
	 * Find settlement process.
	 *
	 * @param mechanismID the mechanism id
	 * @param currency the currency
	 * @param settlementScheme the settlement scheme
	 * @param modalityGroup the modality group
	 * @return the settlement process
	 * @throws ServiceException the service exception
	 */
	public SettlementProcess findSettlementProcess(Long mechanismID, Integer currency, Integer settlementScheme, Long modalityGroup) throws ServiceException{
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("SELECT settle FROM SettlementProcess settle											");
		stringQuery.append("WHERE settle.negotiationMechanism.idNegotiationMechanismPk = :mechanismID		AND	");
		stringQuery.append("	  settle.modalityGroup.idModalityGroupPk 				= :modalityGroupID	AND	");
		stringQuery.append("	  settle.currency 										= :currency			AND	");
		stringQuery.append("	  settle.settlementSchema 								= :settlementScheme	AND	");
		stringQuery.append("	  settle.settlementDate 								= :date					");
		
		Query query = em.createQuery(stringQuery.toString());
		query.setParameter("mechanismID", mechanismID);
		query.setParameter("modalityGroupID", modalityGroup);
		query.setParameter("currency", currency);
		query.setParameter("settlementScheme", settlementScheme);
		query.setParameter("date", CommonsUtilities.currentDate());
		
		try{
			return (SettlementProcess) query.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Un lock balances of operation.
	 *
	 * @param operationID the operation id
	 * @param operationPart the operation part
	 * @param operationType the operation type
	 * @param businessProcessType the business process type
	 * @throws ServiceException the service exception
	 */
	public void unLockBalancesOfOperation(Long operationID, Integer operationPart, Long operationType, Long businessProcessType) throws ServiceException{
		MechanismOperation operation = find(MechanismOperation.class, operationID);
		
		/*LISTAS PARA MANEJAR CUENTAS VENDEDORAS - COMPRADORES como ORIGEN - DESTINO*/
		List<HolderAccountBalanceTO> accountsSellers = new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> accountsBuyers = new ArrayList<HolderAccountBalanceTO>();
		
		/*ITERAMOS CUENTAS INVERSIONISTAS*/
		for(HolderAccountOperation accountOperation: operation.getHolderAccountOperations()){
			if(operationPart.equals(accountOperation.getOperationPart())){
				/*GENERAMOS LOS ACCOUNT_BALANCE_TO, PARA INVOKAR AL COMPONENTE DE SALDOS*/
				switch(ParticipantRoleType.get(accountOperation.getRole())){
					case SELL: accountsSellers.addAll(getHolderAccountBalanceTOs(accountOperation,accountOperation.getStockQuantity()));break;
					case BUY:  accountsBuyers.addAll(getHolderAccountBalanceTOs(accountOperation,accountOperation.getStockQuantity()));break; 
				}
				//we update the stock reference about Settlement account operation
				sirtexOperationService.updateStockSettlementAccountOperation(accountOperation.getIdHolderAccountOperationPk(), null);
			}
		}

//		Long mechanismID = operation.getMechanisnModality().getId().getIdNegotiationMechanismPk();
//		Long modalityID = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
//		Long modGroupID = findModalityGroup(mechanismID, modalityID).getIdModalityGroupPk();
//		Integer settlScheme = operation.getSettlementSchema();
		
//		SettlementProcess settlementProcess = findSettlementProcess(operationID, operation.getCurrency(), settlScheme, modGroupID);
//		Long settProcessID = settlementProcess.getIdSettlementProcessPk();
//		SettlementOperation settlementOperation = (SettlementOperation) settlementProcessService.getSettlementOperation(settProcessID, operationPart);
		
		/*VARIABLES PARA OBTENER EL OPERATION TYPE*/
		if(operationType==null){
			Long negotiationModality = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
			/*VALIDAMOS EL OPERATION TYPE DEPENDIENDO LA MODALIDAD DE LA OPERACION Y ASIGNAMOS A VARIABLE*/
			switch(NegotiationModalityType.get(negotiationModality)){
				case CASH_FIXED_INCOME	: operationType = ParameterOperationType.SIRTEX_CASH_FIXED_INCOME_MODIFY.getCode();break;
				case CASH_EQUITIES	  	: operationType = ParameterOperationType.SIRTEX_CASH_EQUITIES_MODIFY.getCode();break;
				case REPORT_FIXED_INCOME: operationType = ParameterOperationType.SIRTEX_REPORTO_FIXED_INCOME_MODIFY.getCode();break;
				case REPORT_EQUITIES	: operationType = ParameterOperationType.SIRTEX_REPORTO_EQUITIES_MODIFY.getCode();break;
				case REPO_REVERSAL_FIXED_INCOME: operationType = ParameterOperationType.SIRTEX_REPORTO_EQUITIES_MODIFY.getCode();break;
			default:break;
			}
		}
		
		/*CREAMOS EL ACCOUNT COMPONENTE CON EL NUEVO OPERATION TYPE*/
		AccountsComponentTO unLockBalances = getAccountsComponentTO(operationType,businessProcessType,operationID,accountsSellers,accountsBuyers,operationPart);
		
		/*INVOCAMOS A COMPONENTE DE CUENTAS PARA DESBLOQUEAR LOS SALDOS COMPROMETIDOS EN VENTA Y COMPRA*/
		accountsComponentService.get().executeAccountsComponent(unLockBalances);
				
		/*ACTUALIZAMOS EL STOCK REFERENCE DE PARTICIPANT OPERATION*/
//		for(ParticipantOperation partOperation: operation.getParticipantOperations()){
//			if(partOperation.getOperationPart().equals(operationPart)){
//				if(partOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
//					partOperation.setBlockedStock(BigDecimal.ZERO);
//					update(partOperation);
//				}
//			}
//		}
	}

	/**
	 * Gets the accounts component to.
	 *
	 * @param operationType the operation type
	 * @param businessProcessType the business process type
	 * @param operationId the operation id
	 * @param sourceAccounts the source accounts
	 * @param targetAccounts the target accounts
	 * @param operationPart the operation part
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long operationType, Long businessProcessType, Long operationId, 
													   List<HolderAccountBalanceTO> sourceAccounts, List<HolderAccountBalanceTO> targetAccounts, Integer operationPart){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdTradeOperation(operationId);
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		objAccountComponent.setLstTargetAccounts(targetAccounts);
		objAccountComponent.setLstSourceAccounts(sourceAccounts);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setOperationPart(operationPart);
		return objAccountComponent;
	}
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param objBlockOperation the obj block operation
	 * @param stockQuantity the stock quantity
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(HolderAccountOperation objBlockOperation, BigDecimal stockQuantity) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(objBlockOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(objBlockOperation.getInchargeStockParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(objBlockOperation.getMechanismOperation().getSecurities().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(objBlockOperation.getStockQuantity());
		
		/*SETEAMOS HECHOS DE MERCADO ASOCIADOS A LAS CUENTAS INVERSIONISTAS*/
		if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			holderAccountBalanceTO.setLstMarketFactAccounts(getAccountMarketFact(objBlockOperation));
		}
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the account sirtex balance t os.
	 *
	 * @param sirtexAccountOperation the sirtex account operation
	 * @param stockQuantity the stock quantity
	 * @return the account sirtex balance t os
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getAccountSirtexBalanceTOs(AccountTradeRequest sirtexAccountOperation, BigDecimal stockQuantity) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(sirtexAccountOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(sirtexAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(sirtexAccountOperation.getTradeRequest().getSecurities().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(sirtexAccountOperation.getStockQuantity());
		
		/*SETEAMOS HECHOS DE MERCADO ASOCIADOS A LAS CUENTAS INVERSIONISTAS*/
		if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			holderAccountBalanceTO.setLstMarketFactAccounts(getAccountSirtexMarketFact(sirtexAccountOperation));
		}
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}

	/**
	 * Gets the account market fact.
	 *
	 * @param objBlockOperation the obj block operation
	 * @return the account market fact
	 */
	private List<MarketFactAccountTO> getAccountMarketFact(HolderAccountOperation objBlockOperation) {
		// TODO Auto-generated method stub
		List<MarketFactAccountTO> accountMarkFactList = new ArrayList<MarketFactAccountTO>();
		for(AccountOperationMarketFact accountMarkFact: objBlockOperation.getAccountOperationMarketFacts()){
			MarketFactAccountTO markFact = new MarketFactAccountTO();
			markFact.setMarketDate(accountMarkFact.getMarketDate());
			markFact.setMarketPrice(accountMarkFact.getMarketPrice());
			markFact.setMarketRate(accountMarkFact.getMarketRate());
			markFact.setQuantity(accountMarkFact.getOperationQuantity());
			accountMarkFactList.add(markFact);
		}
		return accountMarkFactList;
	}
	
	/**
	 * Gets the account sirtex market fact.
	 *
	 * @param objBlockOperation the obj block operation
	 * @return the account sirtex market fact
	 */
	private List<MarketFactAccountTO> getAccountSirtexMarketFact(AccountTradeRequest objBlockOperation) {
		// TODO Auto-generated method stub
		List<MarketFactAccountTO> accountMarkFactList = new ArrayList<MarketFactAccountTO>();
		for(AccountTradeMarketFact accountMarkFact: objBlockOperation.getAccountSirtexMarkectfacts()){
			MarketFactAccountTO markFact = new MarketFactAccountTO();
			markFact.setMarketDate(accountMarkFact.getMarketDate());
			markFact.setMarketPrice(accountMarkFact.getMarketPrice());
			markFact.setMarketRate(accountMarkFact.getMarketRate());
			markFact.setQuantity(accountMarkFact.getMarketQuantity());
			accountMarkFactList.add(markFact);
		}
		return accountMarkFactList;
	}
	
	/**
	 * Gets the settled operation state.
	 *
	 * @param idOperationsSettled the id operations settled
	 * @return the settled operation state
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> getSettledOperationState(List<Long> idOperationsSettled) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		selectSQL.append("Select ope.operationState																");
		selectSQL.append("From MechanismOperation ope															");
		whereSQL.append ("Where ope.idMechanismOperationPk IN :mechanismId										");
		parameters.put("mechanismId", idOperationsSettled);
		return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()), parameters);
	}
}