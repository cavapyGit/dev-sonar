package com.pradera.negotiations.sirtex.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementDateAccount;
import com.pradera.model.settlement.SettlementDateMarketfact;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.SettlementRequest;
import com.pradera.model.settlement.TradeSettlementRequest;
import com.pradera.model.settlement.type.SettlementAnticipationType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.model.settlement.type.SettlementProcessType;
import com.pradera.negotiations.sirtex.service.RenewSirtexService;
import com.pradera.negotiations.sirtex.service.OtcAndSirtexSettlementService;
import com.pradera.negotiations.sirtex.to.SirtexRenewOperationTO;
import com.pradera.negotiations.sirtex.to.SirtexRenewResultTO;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;
import com.pradera.settlements.prepaidextended.facade.PrepaidExtendedFacade;
import com.pradera.settlements.prepaidextended.service.PrepaidExtendedService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RenewSirtexFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class RenewSirtexFacade {
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sirtex operation service. */
	@EJB private RenewSirtexService renewSirtexService;
	
	/** The holiday query service bean. */
	@EJB private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The prepaid extended facade. */
	@EJB private PrepaidExtendedFacade prepaidExtendedFacade;
	
	/** The prepaid extended service. */
	@EJB private PrepaidExtendedService prepaidExtendedService;
	
	/** The settlement process service. */
	@EJB private SettlementProcessService settlementProcessService;
	
	/** The settlement process facade. */
	@EJB private SettlementsComponentSingleton settlementProcessFacade;
	
	/** The sirtex settlement service. */
	@EJB private OtcAndSirtexSettlementService sirtexSettlementService;
	
	/** The user info. */
//	@Inject private UserInfo userInfo;
	@Inject @DepositarySetup private IdepositarySetup idepositarySetup;

	
	/**
	 * Find negotiation modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param participantPk the participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> findNegotiationModality(Long mechanismPk, Long participantPk) throws ServiceException{
		return renewSirtexService.findNegotiationModality(mechanismPk, participantPk);
	}
	
	/**
	 * Find negotiation mechanism.
	 *
	 * @param mechanismPk the mechanism pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> findNegotiationMechanism(Long mechanismPk) throws ServiceException{
		return renewSirtexService.findNegotiationMechanism(mechanismPk);
	}
	
	/**
	 * Gets the participant by modality.
	 *
	 * @param mechanismPk the mechanism pk
	 * @param modalityPk the modality pk
	 * @return the participant by modality
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantByModality(Long mechanismPk, Long modalityPk) throws ServiceException{
		return renewSirtexService.getParticipantByMechanismModality(mechanismPk, modalityPk);
	}
	
	/**
	 * Gets the trade request by participant.
	 *
	 * @param participantPk the participant pk
	 * @param modalityPk the modality pk
	 * @param mechanismPk the mechanism pk
	 * @return the trade request by participant
	 * @throws ServiceException the service exception
	 */
	public List<TradeRequest> getTradeRequestByParticipant(Long participantPk, Long modalityPk, Long mechanismPk) throws ServiceException{
		return renewSirtexService.getTradeRequestByParticipant(participantPk, modalityPk, mechanismPk);
	}
	
	/**
	 * Gets the settlement request list.
	 *
	 * @param tradeReqPk the trade req pk
	 * @param tradSettRequest the trad sett request
	 * @param opPart the op part
	 * @return the settlement request list
	 * @throws ServiceException the service exception
	 */
	public List<SettlementRequest> getSettlementRequestList(Long tradeReqPk, TradeSettlementRequest tradSettRequest, Integer opPart) throws ServiceException{
		return renewSirtexService.getSettlementRequestList(tradeReqPk, tradSettRequest, opPart);
	}
	
	/**
	 * Gets the last working date.
	 *
	 * @param numberDays the number days
	 * @return the last working date
	 */
	public Date getLastWorkingDate(Integer numberDays) {
		Date initialDate = CommonsUtilities.currentDate();
		Date finalDate   = CommonsUtilities.addDate(initialDate, numberDays);
		Date realFinDate = holidayQueryServiceBean.getLastWorkingDayBetweenDates(initialDate, finalDate);
		return realFinDate;
	}
	
	/**
	 * Validate term settlement days.
	 *
	 * @param numberDays the number days
	 * @param maxTermDays the max term days
	 * @throws ServiceException the service exception
	 */
	public void validateTermSettlementDays(Integer numberDays, Long maxTermDays) throws ServiceException{
		Date initialDate = CommonsUtilities.currentDate();
		Date finalDate   = CommonsUtilities.addDate(initialDate, numberDays);
		if(initialDate.equals(finalDate)){
			throw new ServiceException(ErrorServiceType.TRADE_OPERATION_QUANTITY_LESS_ZERO);
		}
		if(holidayQueryServiceBean.isNonWorkingDayServiceBean(finalDate, true)){
			throw new ServiceException(ErrorServiceType.TRADE_REQUEST_TERM_DAYS);
		}
		if(numberDays.intValue() > maxTermDays.intValue()){
			throw new ServiceException(ErrorServiceType.TRADE_REQUEST_TERM_DAYS_MAX);
		}
	}
	
	/**
	 * Validate stock quantity term.
	 *
	 * @param settlementOperation the settlement operation
	 * @param newStockQuantity the new stock quantity
	 * @throws ServiceException the service exception
	 */
	public void validateStockQuantityTerm(SettlementOperation settlementOperation, BigDecimal newStockQuantity) throws ServiceException{
		BigDecimal currentStockQuantity = settlementOperation.getStockQuantity();
		if(newStockQuantity.equals(BigDecimal.ZERO)){
			throw new ServiceException(ErrorServiceType.TRADE_OPERATION_QUANTITY_LESS_ZERO);
		}
		if(newStockQuantity.compareTo(currentStockQuantity)>0){
			throw new ServiceException(ErrorServiceType.TRADE_REQUEST_STOCK_QUANTITY);
		}
	}
	
	/**
	 * Save trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_REGISTER_RENEW_OPERATION)
	public TradeSettlementRequest saveTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return renewSirtexService.saveTradeSettlementRequest(tradeSettRequest);
	}
	
	/**
	 * Gets the sirtex renew operation.
	 *
	 * @param sirtexRenewTO the sirtex renew to
	 * @param parameters the parameters
	 * @return the sirtex renew operation
	 * @throws ServiceException the service exception
	 */
	public List<SirtexRenewResultTO> getSirtexRenewOperation(SirtexRenewOperationTO sirtexRenewTO, Map<Integer,String> parameters) throws ServiceException{
		List<SirtexRenewResultTO> sirtexRenewLst = renewSirtexService.getSirtexRenewOperation(sirtexRenewTO); 
		for(SirtexRenewResultTO sirtexRenew: sirtexRenewLst){
			sirtexRenew.setStateDescription(parameters.get(sirtexRenew.getState()));
		}
		return sirtexRenewLst;
	}
	
	/**
	 * Approve trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_APPROBAR_RENEW_OPERATION)
	public TradeSettlementRequest approveTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		return renewSirtexService.approveTradeSettlementRequest(tradeSettRequest);
	}
	
	/**
	 * Annulate trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_ANULATE_RENEW_OPERATION)
	public TradeSettlementRequest annulateTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		return renewSirtexService.annularTradeSettlementRequest(tradeSettRequest);
	}
	
	/**
	 * Review trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_REVIEW_RENEW_OPERATION)
	public TradeSettlementRequest reviewTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
		return renewSirtexService.revisarTradeSettlementRequest(tradeSettRequest);
	}
	
	/**
	 * Confirm trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_CONFIRM_RENEW_OPERATION)
	public TradeSettlementRequest confirmTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		for(SettlementRequest settRequest: tradeSettRequest.getLstSettlementRequest()){
			Long settRequestPk = settRequest.getIdSettlementRequestPk();
			SettlementDateOperation settDateOperation = prepaidExtendedFacade.getSettlementDateOperations(settRequestPk, false, null).get(0);
			
			prepaidExtendedService.executeChangeStateSettlementRequest(settRequestPk, SettlementDateRequestStateType.APPROVED.getCode(), null, null, null , false , loggerUser);
			
			prepaidExtendedService.executeChangeStateSettlementRequest(settRequestPk, SettlementDateRequestStateType.REVIEW.getCode(), null, null, null , false , loggerUser);
			
			prepaidExtendedService.executeChangeStateSettlementRequest(settRequestPk, SettlementDateRequestStateType.CONFIRMED.getCode(), null, null, null , false , loggerUser);
			
			/**Update Extension date to SettlementOperation*/
			settlementProcessService.updateSettlementOperationDate(settDateOperation.getSettlementOperation().getIdSettlementOperationPk(), 
    				ComponentConstant.ONE, null, settDateOperation.getSettlementDate(), loggerUser);
			
			/**Update Extension date to mechanismOperation*/
//			settlementProcessService.updateMechanismOperationDate(settDateOperation.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk(), 
//    				ComponentConstant.ONE, null, settDateOperation.getSettlementDate(), settDateOperation.getOperationPart(), loggerUser);
			
			/**Detach object and write data in tables*/
			settDateOperation.getSettlementOperation().setSettlementDate(settDateOperation.getSettlementDate());;
			settDateOperation.getSettlementOperation().setIndExtended(ComponentConstant.ONE);
			
			if(settRequest.getAnticipationType()!=null && settRequest.getAnticipationType().equals(SettlementAnticipationType.SETTLEMENT_PARTIAL.getCode())){
				SettlementOperation settlementOperation = settDateOperation.getSettlementOperation();
				MechanismOperation mechanismOperation   = settlementOperation.getMechanismOperation();
				
				BigDecimal stockQuantitySettOp = settlementOperation.getStockQuantity();
				
				/**Create new SettlementDateOperation for partial settlement*/
				SettlementDateOperation settAnticipOperation = new SettlementDateOperation();
				settAnticipOperation.setSettlementOperation(settlementOperation);
				settAnticipOperation.setStockQuantity(stockQuantitySettOp.subtract(settDateOperation.getStockQuantity()));
				settAnticipOperation.setSettlementDate(CommonsUtilities.currentDate());
				settAnticipOperation.setSettlementDays(BigDecimal.ZERO.longValue());
				settAnticipOperation.setSettlementCurrency(settlementOperation.getSettlementCurrency());
				settAnticipOperation.setOperationPart(settDateOperation.getOperationPart());
				settAnticipOperation.setSettlementPrice(BigDecimal.ZERO);
				settAnticipOperation.setSettlementAmount(BigDecimal.ZERO);
				settAnticipOperation.setSettlementDateAccounts(getSettlementDateAccount(settAnticipOperation));
				settAnticipOperation.setSettlementRequest(settRequest);
				
				/**Insert SettlementDateOperation at database*/
				renewSirtexService.create(settAnticipOperation);
				/**Insert SettlementDateAccount at database*/
				renewSirtexService.saveAll(settAnticipOperation.getSettlementDateAccounts());
				for(SettlementDateAccount settDateAccount: settAnticipOperation.getSettlementDateAccounts()){
					/**Insert SettlementDateAccount's marketFact at database*/
					renewSirtexService.saveAll(settDateAccount.getSettlementDateMarketfacts());
				}
				
				/**Config SettlementOperation to execute partialSettlement*/
				prepaidExtendedService.createSettlementDateOperation(settAnticipOperation);
				/**Settlement of balances*/
				Integer operationPart = settAnticipOperation.getOperationPart();
				Date settlementDate	  = settAnticipOperation.getSettlementDate();
				Long mechOperationPk  = mechanismOperation.getIdMechanismOperationPk();
				SettlementOperation settOperationPartial = settlementProcessService.getSettlementOperations(mechOperationPk, operationPart, ComponentConstant.ONE).get(0);
				settlementBalancesOfOperation(settOperationPartial.getIdSettlementOperationPk(), operationPart, settlementDate);
			}else{
				SettlementOperation settlementOperation = settDateOperation.getSettlementOperation();
				Integer operationPart = settlementOperation.getOperationPart();
				Date settlementDate	  = settlementOperation.getSettlementDate();
				settlementProcessService.updateSettlementOperationToForcedPurchase(Arrays.asList(settlementOperation.getIdSettlementOperationPk()), loggerUser);
				settlementBalancesOfOperation(settlementOperation.getIdSettlementOperationPk(), operationPart, settlementDate);
			}
		}
		return renewSirtexService.confirmTradeSettlementRequest(tradeSettRequest);
	}
	
	/**
	 * Liquidar inconsistencia.
	 *
	 * @param settlementOperationId the settlement operation id
	 * @throws ServiceException the service exception
	 */
	public void liquidarInconsistencia(Long settlementOperationId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		settlementBalancesOfOperation(settlementOperationId, ComponentConstant.TERM_PART, CommonsUtilities.currentDate());;
	}
	
	/**
	 * Gets the settlement date account.
	 *
	 * @param settDateOperation the sett date operation
	 * @return the settlement date account
	 * @throws ServiceException the service exception
	 */
	public List<SettlementDateAccount> getSettlementDateAccount(SettlementDateOperation settDateOperation) throws ServiceException{
		List<SettlementDateAccount> lstSettDateAccount = new ArrayList<>();
		Long mechanismPk = settDateOperation.getSettlementOperation().getMechanismOperation().getIdMechanismOperationPk();		
		BigDecimal stockQuantity = settDateOperation.getStockQuantity();
		Integer termPart = ComponentConstant.TERM_PART;
		for(ParticipantRoleType partRole: ParticipantRoleType.list){
			Integer role = partRole.getCode();
			List<SettlementAccountOperation> settAccountOperationLst = settlementProcessService.getSettlementAccountOperationsByRole(mechanismPk, role, termPart);
			BigDecimal remainQuantity = stockQuantity;
			
			for(SettlementAccountOperation settAccountOperation: settAccountOperationLst){				
				if(remainQuantity.compareTo(BigDecimal.ZERO)>0){
					SettlementDateAccount settDateAccount = new SettlementDateAccount();
					settDateAccount.setSettlementDateOperation(settDateOperation);
					settDateAccount.setStockQuantity(remainQuantity);
					settDateAccount.setSettlementAmount(BigDecimal.ZERO);
					settDateAccount.setSettlementAccountOperation(settAccountOperation);
					
					Long settAccountOperatPk = settAccountOperation.getIdSettlementAccountPk();
					BigDecimal remainMarkQuantity = settDateAccount.getStockQuantity();
					List<SettlementDateMarketfact> lstSettMarketFact = new ArrayList<>();
					
					for(SettlementAccountMarketfact accMarketFact: settlementProcessService.getSettlementAccountMarketfacts(settAccountOperatPk)){
						if(remainMarkQuantity.compareTo(BigDecimal.ZERO)>0){
							SettlementDateMarketfact settDatMarketFact = new SettlementDateMarketfact();
							settDatMarketFact.setSettlementDateAccount(settDateAccount);
							settDatMarketFact.setMarketDate(accMarketFact.getMarketDate());
							settDatMarketFact.setMarketRate(accMarketFact.getMarketRate());
							settDatMarketFact.setMarketPrice(accMarketFact.getMarketPrice());
							
							if(remainMarkQuantity.compareTo(accMarketFact.getMarketQuantity())>=0){
								settDatMarketFact.setMarketQuantity(accMarketFact.getMarketQuantity());
								remainMarkQuantity = remainMarkQuantity.subtract(accMarketFact.getMarketQuantity());
							}else{
								settDatMarketFact.setMarketQuantity(remainMarkQuantity);
								remainMarkQuantity = BigDecimal.ZERO;
							}
							
							lstSettMarketFact.add(settDatMarketFact);
						}
					}
					settDateAccount.setSettlementDateMarketfacts(lstSettMarketFact);
					remainQuantity = remainQuantity.subtract(settDateAccount.getStockQuantity());
					lstSettDateAccount.add(settDateAccount);
				}
			}
		}
		return lstSettDateAccount;
	}
	
	/**
	 * Settlement balances of operation.
	 *
	 * @param operationID the operation id
	 * @param operationPart the operation part
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void settlementBalancesOfOperation(Long operationID, Integer operationPart, Date settlementDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		SettlementOperation settlementOperation = renewSirtexService.find(SettlementOperation.class, operationID) ;
		MechanismOperation operation = settlementOperation.getMechanismOperation();
		
		Long mechanismID = NegotiationMechanismType.SIRTEX.getCode();
		Long modalityID = operation.getMechanisnModality().getId().getIdNegotiationModalityPk();
		Long modGroupID = sirtexSettlementService.findModalityGroup(mechanismID, modalityID).getIdModalityGroupPk();
		Integer settlScheme = operation.getSettlementSchema();
		Integer currency = operation.getCurrency();
		SettlementProcess settlementProcess = sirtexSettlementService.findSettlementProcess(mechanismID, currency, settlScheme, modGroupID);
		if(settlementProcess==null){
			Integer partialProc = SettlementProcessType.PARTIAL.getCode();
			settlementProcess = settlementProcessService.saveSettlementProcess(mechanismID, modGroupID, currency, settlementDate, settlScheme, 0L,null, partialProc, null);
		}
		Long settProcessID = settlementProcess.getIdSettlementProcessPk();

//		SettlementOperation operationSettlement = (SettlementOperation) settlementProcessService.getSettlementOperation(operationID, operationPart);
		
		/*INVOCAMOS EL LIQUIDADOR DE VALORES*/
		settlementProcessService.updateSettlementProcessState(settProcessID,SettlementProcessStateType.IN_PROCESS.getCode(),null,loggerUser,null);
		Long settlementOperationId = settlementOperation.getIdSettlementOperationPk();
		settlementProcessFacade.settleGrossMechanismOperations(settProcessID, mechanismID, modGroupID, settlementDate, currency, operationPart,null, settlScheme,loggerUser, settlementOperationId, Boolean.FALSE);
		settlementProcessService.updateSettlementProcessState(settProcessID,SettlementProcessStateType.WAITING.getCode(),null,loggerUser,null);
	}
	
	/**
	 * Reject trade settlement request.
	 *
	 * @param tradeSettRequest the trade sett request
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_REJECT_RENEW_OPERATION)
	public TradeSettlementRequest rejectTradeSettlementRequest(TradeSettlementRequest tradeSettRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		return renewSirtexService.rejectTradeSettlementRequest(tradeSettRequest);
	}
	
	/**
	 * Gets the trade settlement request.
	 *
	 * @param tradeSettRequestPk the trade sett request pk
	 * @return the trade settlement request
	 * @throws ServiceException the service exception
	 */
	public TradeSettlementRequest getTradeSettlementRequest(Long tradeSettRequestPk) throws ServiceException{
		TradeSettlementRequest tradeSettRequest = renewSirtexService.find(TradeSettlementRequest.class, tradeSettRequestPk);
		tradeSettRequest.getSourceParticipant().getIdParticipantPk();
		tradeSettRequest.getTargetParticipant().getIdParticipantPk();
		tradeSettRequest.getTradeRequest().getSecurities().getIdSecurityCodePk();
		
		for(SettlementRequest settRequest: tradeSettRequest.getLstSettlementRequest()){
			for(SettlementDateOperation settDateOperation: settRequest.getLstSettlementDateOperations()){
				settDateOperation.getSettlementOperation().getMechanismOperation().getSecurities().getIdSecurityCodePk();
			}
		}
		
		return tradeSettRequest;
	}
}