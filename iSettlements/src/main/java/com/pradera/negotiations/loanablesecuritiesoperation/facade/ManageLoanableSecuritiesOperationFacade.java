package com.pradera.negotiations.loanablesecuritiesoperation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.HolderBalanceMovementsTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.LoanableSecuritiesOperation;
import com.pradera.model.negotiation.LoanableSecuritiesRequest;
import com.pradera.negotiations.loanablesecuritiesoperation.service.ManageLoanableSecuritiesOperationServiceBean;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchLoanableSecuritiesOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageLoanableSecuritiesOperationFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageLoanableSecuritiesOperationFacade {

	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** The manage Loanable Securities Operation service bean. */
	@EJB
	ManageLoanableSecuritiesOperationServiceBean manageLoanableSecuritiesOperationServiceBean;
	
	/** The Holder Account service bean. */
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	/** The Parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/**
	 * Gets the list participant service bean.
	 *
	 * @param filter the filter
	 * @return the list participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLisParticipantServiceBean(Participant filter) throws ServiceException{
		return participantServiceBean.getLisParticipantServiceBean(filter);
	}

	/**
	 * Get Holder Account Component Service Bean.
	 *
	 * @param holderAccountTO object holderAccountTO
	 * @return object HolderAccount
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccountComponentServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		return holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(holderAccountTO);
	}
	
	/**
	 * Get Holder Account Component Service Bean.
	 *
	 * @param holderAccountTO object holderAccountTO
	 * @return object HolderAccount
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListForParticipantAndHolder(HolderAccountTO holderAccountTO) throws ServiceException {
		return manageLoanableSecuritiesOperationServiceBean.getHolderAccountListForParticipantAndHolder(holderAccountTO);
	}
	
	/**
	 * Get Holder Service Facade.
	 *
	 * @param idHolderPk id Holder
	 * @return object Holder
	 * @throws ServiceException the service exception
	 */
	public Holder getHolderServiceFacade(Long idHolderPk) throws ServiceException{
	       Holder holder =   manageLoanableSecuritiesOperationServiceBean.getHolderServiceBean(idHolderPk);
	       
	       List<RepresentedEntity> lstRepresentedEntity;
	       lstRepresentedEntity = holder.getRepresentedEntity();
		   holder.setRepresentedEntity(null);
		   List<RepresentedEntity> lstRepresentedEntityResult = new ArrayList<RepresentedEntity>();
		   
		   
		   for(RepresentedEntity rE : lstRepresentedEntity){
			   if(!rE.getStateRepresented().equals(RepresentativeEntityStateType.DELETED.getCode())){
				   lstRepresentedEntityResult.add(rE);
			   }
		   }
		   
		   holder.setRepresentedEntity(lstRepresentedEntityResult);
		   
		          
	       for(int i=0;i<holder.getRepresentedEntity().size();i++){
	    	   holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk();
	    	   
	    	   if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile()!=null){
	    		   holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().size();
	    	   }
	       }
	       if(holder.getHolderFile()!=null){    	   
	    	     
	    	     List<HolderFile> lstHolderFile;    			 
				 lstHolderFile = holder.getHolderFile();
				 holder.setHolderFile(null);
			     List<HolderFile> lstHolderFileResult  = new ArrayList<HolderFile>();
				 
				 for(HolderFile hF:lstHolderFile){
					 if(!hF.getStateFile().equals(HolderFileStateType.DELETED.getCode())){
						 lstHolderFileResult.add(hF);
					 }
				 }
				 
				 holder.setHolderFile(lstHolderFileResult);
				 holder.getHolderFile().size();
	       }
	       
	       if(holder.getParticipant()!=null){
	    	   holder.getParticipant().getIdParticipantPk();
	       }
	       
	       return holder;
	         
	     }
	
	/**
	 * Get Holder Account Balance By Filter.
	 *
	 * @param filter object filter
	 * @return object Holder Account Balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getHolderAccountBalanceByFilterServiceBean(HolderBalanceMovementsTO filter) throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.getHolderAccountBalanceByFilterServiceBean(filter);
	}
	
	/**
	 * Get Loanable securities operation By Filter.
	 *
	 * @param filter object filter
	 * @return object Loanable Securities Operation
	 * @throws ServiceException the service exception
	 */
	public List<LoanableSecuritiesOperation> getLoanableSecuritiesOperationBalanceByFilterServiceBean(SearchLoanableSecuritiesOperationTO filter) throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.getLoanableSecuritiesOperationBalanceByFilterServiceBean(filter);
	}
	
	/**
	 * Get Holder MarketFac Balance Filter.
	 *
	 * @param filter filter Holder Balance
	 * @return list holder marketfac balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderMarketFactBalance> getHolderMarketFacBalanceFilterServiceBean(HolderBalanceMovementsTO filter) throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.getHolderMarketFacBalanceFilterServiceBean(filter);
	}
	
	/**
	 * Registry Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return  object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_REGISTER)
	public LoanableSecuritiesRequest registryLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());       
        return manageLoanableSecuritiesOperationServiceBean.registryLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);
	}
	
	/**
	 * Registry Retirement Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return  object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_RETIREMENT_REGISTER)
	public LoanableSecuritiesRequest registryRetirementLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());       
        return manageLoanableSecuritiesOperationServiceBean.registryRetirementLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);
	}
	
	/**
	 * Find Affectation With MarketFact.
	 *
	 * @param accountBalance List Holder Account Balance
	 * @return List Holder Account Balance
	 * @throws ServiceException  the service exception
	 */
	public List<HolderAccountBalanceTO> findAffectationWithMarketFact(List<HolderAccountBalanceTO> accountBalance) throws ServiceException{
		
		Map<String, Integer> balanceKeyMap = parameterServiceBean.balanceWithMarketFactQuantity(accountBalance);
		
		for(HolderAccountBalanceTO hab: accountBalance){
			StringBuilder balanceKey = new StringBuilder();
			balanceKey.append(hab.getIdParticipant());
			balanceKey.append(hab.getIdHolderAccount());
			balanceKey.append(hab.getIdSecurityCode());
			
			Integer quantityMarketFact = balanceKeyMap.get(balanceKey.toString());
			if(quantityMarketFact!=null && quantityMarketFact.equals(BigDecimal.ONE.intValue())){
				hab.setMarketLock(Boolean.FALSE);
			}else{
				hab.setMarketLock(Boolean.TRUE);
			}
		}
		return accountBalance;
	}
	
	/**
	 * Get Loanable Securities Request Filter.
	 *
	 * @param searchLoanableSecuritiesOperationTO the search loanable securities operation to
	 * @return List Loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_QUERY)
	public List<LoanableSecuritiesRequest> getListLoanableSecuritiesRequestFilter(SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO)throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.getListLoanableSecuritiesRequestFilter(searchLoanableSecuritiesOperationTO);
	}
	
	/**
	 * Get Loanable Securities Request Filter .
	 *
	 * @param searchLoanableSecuritiesOperationTO filter Search Loanable Securities Operation
	 * @return object Loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	public LoanableSecuritiesRequest getLoanableSecuritiesRequestFilter(SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO)throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.getLoanableSecuritiesRequestFilter(searchLoanableSecuritiesOperationTO);
	}
	
	/**
	 * Exist Securities TVR.
	 *
	 * @param idSecurityCodePk Security Code
	 * @return flag Exist
	 * @throws ServiceException the service exception
	 */
	public Boolean existSecuritiesTVR(String idSecurityCodePk)throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.existSecuritiesTVR(idSecurityCodePk);
	}
	
	/**
	 * Annul Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_CANCEL)
	public LoanableSecuritiesRequest annulLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());       
        return manageLoanableSecuritiesOperationServiceBean.annulLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);
	}
	
	/**
	 * Approve Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_APPROVE)
	public LoanableSecuritiesRequest approveLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());       
        return manageLoanableSecuritiesOperationServiceBean.approveLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);
	}
	
	/**
	 * Reject Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_REJECT)
	public LoanableSecuritiesRequest rejectLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());       
        return manageLoanableSecuritiesOperationServiceBean.rejectLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);
	}
	
	/**
	 * Reject Retirement Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_RETIREMENT_REJECT)
	public LoanableSecuritiesRequest rejectRetirementLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());       
        return manageLoanableSecuritiesOperationServiceBean.rejectRetirementLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);
	}
	
	/**
	 * Confirm Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_CONFIRM)
	public LoanableSecuritiesRequest confirmLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());       
        return manageLoanableSecuritiesOperationServiceBean.confirmLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);
	}
	
	/**
	 * Confirm Retirement Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @param flagAutomatic the flag automatic
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.LOANABLE_SECURITIES_RETIREMENT_CONFIRM)
	public LoanableSecuritiesRequest confirmRetirementLoanableSecuritiesRequestServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest,Boolean flagAutomatic) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());       
        return manageLoanableSecuritiesOperationServiceBean.confirmRetirementLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser,flagAutomatic);
	}
	
	/**
	 * Is Insufficient Balance Loanable Operation.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return validate true or false
	 * @throws ServiceException the service exception
	 */
	public Boolean isInsufficientBalanceLoanableOperationServiceFacade(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.isInsufficientBalanceLoanableOperation(loanableSecuritiesRequest);
	}
	
	/**
	 * Find Security By Id.
	 *
	 * @param idSecurity code Security
	 * @return object security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityById(String idSecurity)throws ServiceException{
		return manageLoanableSecuritiesOperationServiceBean.findSecurityById(idSecurity);
	}
}
