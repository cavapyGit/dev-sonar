package com.pradera.negotiations.loanablesecuritiesoperation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchHolderAccountBalanceTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchHolderAccountBalanceTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	private String id;
	
	/** The id Participant pk. */
	private Long idParticipantPk;
	
	/** The loanable securities operation pk. */
	private Long idLoanableSecuritiesOperationPk;
	
	/** The id Holder Account pk. */
	private Long idHolderAccountPk;
	
	/** The id Security Code Pk. */
	private String idSecurityCodePk;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The loanable balance. */
	private BigDecimal loanableBalance;
	
	/** The transfer Amount. */
	private BigDecimal transferAmount;
	
	/** The selected. */
	private Boolean selected = false;
	
	/** The market lock. */
	private Boolean marketLock;
	
	/** The block market fact detail. */
	private List<SearchTransferMarketFactTO> transferMarketFactDetail;
	 
	/**
	 * 	The Constructor.
	 */
	public SearchHolderAccountBalanceTO(){
		
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the transfer amount.
	 *
	 * @return the transfer amount
	 */
	public BigDecimal getTransferAmount() {
		return transferAmount;
	}

	/**
	 * Sets the transfer amount.
	 *
	 * @param transferAmount the new transfer amount
	 */
	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}

	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public Boolean getSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	/**
	 * Gets the market lock.
	 *
	 * @return the market lock
	 */
	public Boolean getMarketLock() {
		return marketLock;
	}

	/**
	 * Sets the market lock.
	 *
	 * @param marketLock the new market lock
	 */
	public void setMarketLock(Boolean marketLock) {
		this.marketLock = marketLock;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the transfer market fact detail.
	 *
	 * @return the transfer market fact detail
	 */
	public List<SearchTransferMarketFactTO> getTransferMarketFactDetail() {
		return transferMarketFactDetail;
	}

	/**
	 * Sets the transfer market fact detail.
	 *
	 * @param transferMarketFactDetail the new transfer market fact detail
	 */
	public void setTransferMarketFactDetail(
			List<SearchTransferMarketFactTO> transferMarketFactDetail) {
		this.transferMarketFactDetail = transferMarketFactDetail;
	}

	/**
	 * Gets the loanable balance.
	 *
	 * @return the loanable balance
	 */
	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	/**
	 * Sets the loanable balance.
	 *
	 * @param loanableBalance the new loanable balance
	 */
	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	/**
	 * Gets the id loanable securities operation pk.
	 *
	 * @return the id loanable securities operation pk
	 */
	public Long getIdLoanableSecuritiesOperationPk() {
		return idLoanableSecuritiesOperationPk;
	}

	/**
	 * Sets the id loanable securities operation pk.
	 *
	 * @param idLoanableSecuritiesOperationPk the new id loanable securities operation pk
	 */
	public void setIdLoanableSecuritiesOperationPk(
			Long idLoanableSecuritiesOperationPk) {
		this.idLoanableSecuritiesOperationPk = idLoanableSecuritiesOperationPk;
	}
	
}
