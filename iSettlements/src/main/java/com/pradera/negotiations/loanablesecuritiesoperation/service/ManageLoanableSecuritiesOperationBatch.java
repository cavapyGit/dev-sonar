package com.pradera.negotiations.loanablesecuritiesoperation.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageLoanableSecuritiesOperationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name = "ManageLoanableSecuritiesOperationBatch")
@RequestScoped
public class ManageLoanableSecuritiesOperationBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The manage loanable securities operation service bean. */
	@EJB 
	ManageLoanableSecuritiesOperationServiceBean manageLoanableSecuritiesOperationServiceBean;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		try {
			manageLoanableSecuritiesOperationServiceBean.retirementLoanableSecuritiesOperation(CommonsUtilities.currentDate());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
