package com.pradera.negotiations.loanablesecuritiesoperation.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.HolderBalanceMovementsTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.LoanableSecuritiesMarketfact;
import com.pradera.model.negotiation.LoanableSecuritiesOperation;
import com.pradera.model.negotiation.LoanableSecuritiesRequest;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationMotiveAnnulType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationMotiveRejectType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationRequestType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationStateType;
import com.pradera.negotiations.loanablesecuritiesoperation.facade.ManageLoanableSecuritiesOperationFacade;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchHolderAccountBalanceTO;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchLoanableSecuritiesOperationTO;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchTransferMarketFactTO;
import com.pradera.settlements.utils.MotiveController;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageLoanableSecuritiesOperationMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ManageLoanableSecuritiesOperationMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** 	 the Search loanable securities operation TO Header. */
	private SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO;
	
	/** 	 the Search loanable securities operation TO. */
	private SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTOHeader;
	
	/** The loanable Securities Request session. */
	private LoanableSecuritiesRequest loanableSecuritiesRequestSession;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The placement segment list. */
	private GenericDataModel<LoanableSecuritiesRequest> searchLoanableSecuritiesRequestModel;
	
	/**  List Holder Account. */
	private List<HolderAccount> lstHolderAccount;
	
	/** The general parameter facade. */
	@EJB
	GeneralParametersFacade generalParameterFacade;
	
	/** The  manage loanable securities operation facade. */
	@EJB
	ManageLoanableSecuritiesOperationFacade manageLoanableSecuritiesOperationFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The all participant list. */
	private List<Participant> lstAllParticipant;
	
	/** The Holder Account Balance list. */
	private  List<HolderAccountBalance> lstholderAccountBalance =null;	
	
	 /** The search Holder Account Balance Data Model. */
	private GenericDataModel<SearchHolderAccountBalanceTO> searchHolderAccountBalanceTODataModel;
		
	 /** The holder Account Balance Session object. */
	private HolderAccountBalance holderAccountBalanceSession;
	
	/**  The marketFact Balance Help. */
	private MarketFactBalanceHelpTO marketFactBalanceHelpTO,marketFactBalanceDetail;
	
	/** Code Security. */
	private String securityCode;
	
	/** The motive controller annul. */
	private MotiveController motiveControllerAnnul;
	
	/** The motive controller reject. */
	private MotiveController motiveControllerReject;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		beginConversation();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		createObject();
		try {
			loadAllParticipant();
			loadState();
			loadLoanableSecuritiesMotiveAnnul();
			loadLoanableSecuritiesMotiveReject();
			loadHolidays();
			loadUserValidation();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create Object.
	 */
	public void createObject()
	{
		searchLoanableSecuritiesOperationTO = new SearchLoanableSecuritiesOperationTO(CommonsUtilities.currentDate(),CommonsUtilities.currentDate());
		searchLoanableSecuritiesOperationTO.setCUIHolder(new Holder());
		searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		motiveControllerAnnul = new MotiveController();
		motiveControllerReject = new MotiveController();
	}
	
	/**
	 * Create Object Loanable Securities Operation Session.
	 */
	public void createObjectLoanableSecuritiesOperationSession()
	{
		loanableSecuritiesRequestSession = new LoanableSecuritiesRequest();
		loanableSecuritiesRequestSession.setParticipant(new Participant());
		loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		loanableSecuritiesRequestSession.setHolder(new Holder());
	}
	/**
	 * Registry loanable securities request handler.
	 */
	public void registryLoanableSecuritiesRequestHandler()
	{
		executeAction();
		cleanModelRegister();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		createObjectLoanableSecuritiesOperationSession();
		createObjectRegister();
		try {
			loadAllParticipant();
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			loanableSecuritiesRequestSession.getParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
	}
	
	/**
	 * Create Object Register.
	 */
	public void createObjectRegister()
	{
		searchLoanableSecuritiesOperationTOHeader = new SearchLoanableSecuritiesOperationTO();
		this.lstAllParticipant = new ArrayList<Participant>();
		lstHolderAccount = new ArrayList<HolderAccount>();
	}
	/**
	 * Load all participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllParticipant() throws ServiceException{
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			searchLoanableSecuritiesOperationTO.setLstAllParticipant(manageLoanableSecuritiesOperationFacade.getLisParticipantServiceBean(participantTO));
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			this.setLstAllParticipant(manageLoanableSecuritiesOperationFacade.getLisParticipantServiceBean(participantTO));
	}
	/**
	 * Load state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadState() throws ServiceException{
		
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.LOANABLE_SECURITIES_OPERATION_STATE.getCode());
		List<ParameterTable> lstStateParameterTable = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstStateParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstStateParameterTable)
		{
			if(Long.valueOf(parameterTable.getParameterTablePk()).equals(LoanableSecuritiesOperationStateType.EXPIRED.getCode()));
			else	lstStateParameterTableAux.add(parameterTable);
		}
		searchLoanableSecuritiesOperationTO.setLstState(lstStateParameterTableAux);
		
	}
	
	/**
	 * Load User Validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchLoanableSecuritiesOperationTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
		}
	}
	/**
	 * Load Loanable Securities Motive Annul.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLoanableSecuritiesMotiveAnnul() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.LOANABLE_SECURITIES_OPERATION_ANNUL_MOTIVE.getCode());
		motiveControllerAnnul.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO), MasterTableType.LOANABLE_SECURITIES_OPERATION_ANNUL_MOTIVE);
	}
	/**
	 * Load Loanable Securities Motive Reject.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLoanableSecuritiesMotiveReject() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.LOANABLE_SECURITIES_OPERATION_REJECT_MOTIVE.getCode());
		motiveControllerReject.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO), MasterTableType.LOANABLE_SECURITIES_OPERATION_REJECT_MOTIVE);
	}
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParameterFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Before Approve action.
	 *
	 * @return the string
	 */
	public String beforeApproveAction(){
		LoanableSecuritiesRequest loanableSecuritiesRequest = this.getLoanableSecuritiesRequestSession(); 
		executeAction();
		if(loanableSecuritiesRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(loanableSecuritiesRequest.getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){			
			selectLoanableSecuritiesOperationHandler(loanableSecuritiesRequest);
			this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
			return "loanableSecuritiesDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	/**
	 * Before Annular action.
	 *
	 * @return the string
	 */
	public String beforeAnnularAction(){

		LoanableSecuritiesRequest loanableSecuritiesRequest = this.getLoanableSecuritiesRequestSession(); 
		executeAction();
		if(loanableSecuritiesRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(loanableSecuritiesRequest.getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){			
			selectLoanableSecuritiesOperationHandler(loanableSecuritiesRequest);
			this.setViewOperationType(ViewOperationsType.ANULATE.getCode());
			return "loanableSecuritiesDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	/**
	 * Before Confirm action.
	 *
	 * @return the string
	 */
	public String beforeConfirmAction(){

		LoanableSecuritiesRequest loanableSecuritiesRequest = this.getLoanableSecuritiesRequestSession(); 
		executeAction();
		if(loanableSecuritiesRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(loanableSecuritiesRequest.getRequestState().equals(LoanableSecuritiesOperationStateType.APPROVED.getCode())){			
			selectLoanableSecuritiesOperationHandler(loanableSecuritiesRequest);
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			return "loanableSecuritiesDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_APPROVE));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
		
	}
	/**
	 * Before Reject action.
	 *
	 * @return the string
	 */
	public String beforeRejectAction(){

		LoanableSecuritiesRequest loanableSecuritiesRequest = this.getLoanableSecuritiesRequestSession(); 
		executeAction();
		if(loanableSecuritiesRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(loanableSecuritiesRequest.getRequestState().equals(LoanableSecuritiesOperationStateType.APPROVED.getCode())){			
			selectLoanableSecuritiesOperationHandler(loanableSecuritiesRequest);
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());
			return "loanableSecuritiesDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_APPROVE));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
		
	}
	/**
	 * Before approve handler.
	 */
	public void beforeApproveHandler(){
		executeAction();
		if(this.getLoanableSecuritiesRequestSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getLoanableSecuritiesRequestSession().getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_APPROVE,
				    		  new Object[]{this.getLoanableSecuritiesRequestSession().getIdLoanableSecuritiesRequestPk()}) );			
			this.setViewOperationType(ViewOperationsType.APPROVE.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Before annular handler.
	 */
	public void beforeAnnularHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(this.getLoanableSecuritiesRequestSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getLoanableSecuritiesRequestSession().getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Before reject handler.
	 */
	public void beforeRejectHandler()
	{
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(this.getLoanableSecuritiesRequestSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getLoanableSecuritiesRequestSession().getRequestState().equals(LoanableSecuritiesOperationStateType.APPROVED.getCode())){
			setViewOperationType(ViewOperationsType.REJECT.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_APPROVE));	
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Before confirm handler.
	 */
	public void beforeConfirmHandler()
	{
		executeAction();
		if(this.getLoanableSecuritiesRequestSession()==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			 JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getLoanableSecuritiesRequestSession().getRequestState().equals(LoanableSecuritiesOperationStateType.APPROVED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_CONFIRM,
				    		  new Object[]{this.getLoanableSecuritiesRequestSession().getIdLoanableSecuritiesRequestPk()}) );			
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_APPROVE));	
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Change action loanable securities request handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		switch(ViewOperationsType.lookup.get(getViewOperationType())){
			case APPROVE : approveLoanableSecuritiesRequest();break;
			case ANULATE: annularLoanableSecuritiesRequest();break;
			case CONFIRM : confirmLoanableSecuritiesRequest();break;
			case REJECT: rejectLoanableSecuritiesRequest();break;
		}
		//cleanLoanableSecuritiesOperationSearchHandler(null);
	}
	/**
	 * Approve loanable securities request.
	 */
	@LoggerAuditWeb
	private void approveLoanableSecuritiesRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.APPROVED.getCode());
				
		LoanableSecuritiesRequest loanableSecuritiesRequest = null;
		try {
			loanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.approveLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(loanableSecuritiesRequest!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_APPROVE_OK,
				    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		searchLoanableSecuritiesOperationHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		
	}
	
	/**
	 * Return search action.
	 *
	 * @return the string
	 */
	public String returnSearchAction()
	{
		searchLoanableSecuritiesOperationHandler();
		return "searchLoanableSecuritiesOperationMgmt";
	}
	/**
	 * Confirm loanable securities request.
	 */
	@LoggerAuditWeb
	private void confirmLoanableSecuritiesRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.CONFIRMED.getCode());
				
		LoanableSecuritiesRequest loanableSecuritiesRequest = null;
		try {
			loanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.confirmLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(loanableSecuritiesRequest!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_CONFIRM_OK,
				    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		searchLoanableSecuritiesOperationHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Annul placement request.
	 */
	@LoggerAuditWeb
	private void annularLoanableSecuritiesRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.CANCELLED.getCode());
		loanableSecuritiesRequestSession.setAnnulmentMotive(Integer.parseInt(""+motiveControllerAnnul.getIdMotivePK()));
		loanableSecuritiesRequestSession.setAnnulmentOtherMotive(motiveControllerAnnul.getMotiveText());
		
		LoanableSecuritiesRequest loanableSecuritiesRequest = null;
		try {
			loanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.annulLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(loanableSecuritiesRequest!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ANNULAR_OK,
				    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		searchLoanableSecuritiesOperationHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Reject placement request.
	 */
	@LoggerAuditWeb
	private void rejectLoanableSecuritiesRequest() {
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.REJECTED.getCode());
		loanableSecuritiesRequestSession.setRejectMotive(Integer.parseInt(""+motiveControllerReject.getIdMotivePK()));
		loanableSecuritiesRequestSession.setRejectOtherMotive(motiveControllerReject.getMotiveText());
		
		LoanableSecuritiesRequest loanableSecuritiesRequest = null;
		try {
			loanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.rejectLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
		if(loanableSecuritiesRequest!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_REJECT_OK,
				    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );				
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		searchLoanableSecuritiesOperationHandler();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveAnnulHandler(){
		if(this.getMotiveControllerAnnul().getIdMotivePK()==Long.parseLong(LoanableSecuritiesOperationMotiveAnnulType.OTHER.getCode().toString())){
			this.getMotiveControllerAnnul().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerAnnul().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Change motive Reject handler.
	 */
	public void changeMotiveRejectHandler(){
		if(this.getMotiveControllerReject().getIdMotivePK()==Long.parseLong(LoanableSecuritiesOperationMotiveRejectType.OTHER.getCode().toString())){
			this.getMotiveControllerReject().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveControllerReject().setShowMotiveText(Boolean.FALSE);
		}
	}
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveAnnulHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerAnnul.getIdMotivePK()==Long.parseLong(LoanableSecuritiesOperationMotiveAnnulType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerAnnul().getMotiveText())){
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').hide();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ANNULAR,
			    		  new Object[]{this.getLoanableSecuritiesRequestSession().getIdLoanableSecuritiesRequestPk()}) );			
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	/**
	 * Confirm motive Reject handler.
	 */
	public void confirmMotiveRejectHandler(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();  
		if(motiveControllerReject.getIdMotivePK()==Long.parseLong(LoanableSecuritiesOperationMotiveRejectType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveControllerReject().getMotiveText())){
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
				return;
			}
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').hide();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_REJECT,
			    		  new Object[]{this.getLoanableSecuritiesRequestSession().getIdLoanableSecuritiesRequestPk()}) );			
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	
	/**
	 * before Save Loanable Securities Operation Handler.
	 */
	public void beforeLoanableSecuritiesOperationHandler()
	{
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  if (Validations.validateIsNull(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk()) || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk().intValue())
				  || Validations.validateIsNull(loanableSecuritiesRequestSession.getHolder().getIdHolderPk()) || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolder().getIdHolderPk().intValue())
				  || Validations.validateIsNull(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk()) || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk().intValue())
				  || Validations.validateIsNull(loanableSecuritiesRequestSession.getInterestRate()) || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getInterestRate() .intValue())
				  || Validations.validateIsNull(loanableSecuritiesRequestSession.getValidityDays())|| !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getValidityDays() .intValue())
				  || Validations.validateIsNullOrEmpty(searchHolderAccountBalanceTODataModel) || Validations.validateListIsNullOrEmpty(searchHolderAccountBalanceTODataModel.getDataList())) {			   
			   showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
			   JSFUtilities.showSimpleValidationDialog();
			   return;
			  }			 		  
		 
		  Boolean validate=false;
		  int cont=0;
		  for(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO : searchHolderAccountBalanceTODataModel.getDataList())
		  {				
			  if(searchHolderAccountBalanceTO.getSelected())
			  {		
				  cont++;
				  if(searchHolderAccountBalanceTO.getMarketLock())//mas de un hecho de mercado
				  {
					  if(Validations.validateIsNullOrEmpty(searchHolderAccountBalanceTO.getTransferAmount()))
					  {
						  validate=Boolean.TRUE;
						  break;
					  }
				  }
				  else //un solo hecho de mercado
				  {
					  if(Validations.validateIsNullOrEmpty(searchHolderAccountBalanceTO.getTransferAmount()))
					  {
						  validate=Boolean.TRUE;
						  break;
					  }
					  else 
					  {
						  if(Validations.validateIsNullOrEmpty(marketFactBalanceHelpTO))
							  validate = selectMarketFactBalance(searchHolderAccountBalanceTO);					 
					  }
				  }
			  }
		  }		  
		  
		  if(validate || cont==0)
		  {			 
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));			 
			  JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_REGISTER));	
		  JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
	}
	
	/**
	 * Select MarketFact Balance Bean.
	 *
	 * @param searchHolderAccountBalanceTO object search Holder Account Balance TO
	 * @return the boolean
	 */
	public Boolean selectMarketFactBalance(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO){		
		for(SearchHolderAccountBalanceTO habh: searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO()){
				if(habh.getIdParticipantPk().equals(searchHolderAccountBalanceTO.getIdParticipantPk()) &&
				   habh.getIdHolderAccountPk().equals(searchHolderAccountBalanceTO.getIdHolderAccountPk()) &&
				   habh.getIdSecurityCodePk().equals(searchHolderAccountBalanceTO.getIdSecurityCodePk())){
					
					if(!searchHolderAccountBalanceTO.getTransferAmount().equals(BigDecimal.ZERO)){
						habh.setTransferAmount(searchHolderAccountBalanceTO.getTransferAmount());
						habh.setTransferMarketFactDetail(new ArrayList<SearchTransferMarketFactTO>());
						
					  HolderBalanceMovementsTO holderBalanceMovementsTOAux = new HolderBalanceMovementsTO();
					  holderBalanceMovementsTOAux.setObjParticipant(loanableSecuritiesRequestSession.getParticipant());
					  holderBalanceMovementsTOAux.setAccountNumber(Integer.valueOf(searchHolderAccountBalanceTO.getIdHolderAccountPk().toString()));					  
					  holderBalanceMovementsTOAux.setIdIsinCodePk(searchHolderAccountBalanceTO.getIdSecurityCodePk());
					
					  try {
						  List<HolderMarketFactBalance> lstHolderMarketFacBalance = manageLoanableSecuritiesOperationFacade.getHolderMarketFacBalanceFilterServiceBean(holderBalanceMovementsTOAux);
						  if(Validations.validateListIsNotNullAndNotEmpty(lstHolderMarketFacBalance))
						  {							 
							  for(HolderMarketFactBalance holderMarketFactBalance : lstHolderMarketFacBalance)
							  {
								  SearchTransferMarketFactTO searchTransferMarketFactTO = new SearchTransferMarketFactTO();
								  searchTransferMarketFactTO.setIdHolderMarketPk(holderMarketFactBalance.getIdMarketfactBalancePk());
								  searchTransferMarketFactTO.setMarketDate(holderMarketFactBalance.getMarketDate());
								  searchTransferMarketFactTO.setMarketPrice(holderMarketFactBalance.getMarketPrice());
								  searchTransferMarketFactTO.setMarketRate(holderMarketFactBalance.getMarketRate());
								  searchTransferMarketFactTO.setTransferAmount(searchHolderAccountBalanceTO.getTransferAmount());
								  habh.getTransferMarketFactDetail().add(searchTransferMarketFactTO);
							  }							  
						  }
						  else
						  {
							  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));			
							  JSFUtilities.showSimpleValidationDialog();
							   return Boolean.TRUE;
						  }
					  } catch (Exception e) {
					   e.printStackTrace();
					  }														
						validateAmountTransferForBean(habh);
					}else{
						clearQuantitiesTransfer(habh);
					}				
				}			
		}
		return Boolean.FALSE;
	}
	/**
	 * Save Loanable Securities Operation handler.
	 */
	@LoggerAuditWeb
	public void saveLoanableSecuritiesOperationHandler()
	{
		LoanableSecuritiesRequest loanableSecuritiesRequest=null;	
		loanableSecuritiesRequestSession.setRequestType(LoanableSecuritiesOperationRequestType.ENTRY_REQUEST_TYPE.getCode());
		loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.REGISTERED.getCode());
		List <LoanableSecuritiesOperation> lstLoanableSecuritiesOperation = new ArrayList<LoanableSecuritiesOperation>();
		for(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO : searchHolderAccountBalanceTODataModel.getDataList())
		{
			if(searchHolderAccountBalanceTO.getSelected())
			{
				LoanableSecuritiesOperation  loanableSecuritiesOperation = new LoanableSecuritiesOperation();
				loanableSecuritiesOperation.setParticipant(loanableSecuritiesRequestSession.getParticipant());
				loanableSecuritiesOperation.setHolderAccount(loanableSecuritiesRequestSession.getHolderAccount());
				Security security = new Security();
				security.setIdSecurityCodePk(searchHolderAccountBalanceTO.getIdSecurityCodePk());
				loanableSecuritiesOperation.setSecurity(security);
				loanableSecuritiesOperation.setInitialQuantity(searchHolderAccountBalanceTO.getTransferAmount());
				loanableSecuritiesOperation.setOperationState(LoanableSecuritiesOperationStateType.REGISTERED.getCode());
				loanableSecuritiesOperation.setInterestRate(loanableSecuritiesRequestSession.getInterestRate());
				loanableSecuritiesOperation.setInitialDate(getCurrentSystemDate());
				loanableSecuritiesOperation.setValidityDays(loanableSecuritiesRequestSession.getValidityDays());
				loanableSecuritiesOperation.setCurrentQuantity(searchHolderAccountBalanceTO.getTransferAmount());
				loanableSecuritiesOperation.setLoanedBalance(new BigDecimal(0));
				List <LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact=new ArrayList<LoanableSecuritiesMarketfact>();
				for (SearchTransferMarketFactTO searchTransferMarketFactTO : searchHolderAccountBalanceTO.getTransferMarketFactDetail())
				{
					LoanableSecuritiesMarketfact loanableSecuritiesMarketfact = new LoanableSecuritiesMarketfact();
					loanableSecuritiesMarketfact.setMarketDate(searchTransferMarketFactTO.getMarketDate());
					loanableSecuritiesMarketfact.setMarketPrice(searchTransferMarketFactTO.getMarketPrice());
					loanableSecuritiesMarketfact.setInitialMarketQuantity(searchTransferMarketFactTO.getTransferAmount());
					loanableSecuritiesMarketfact.setCurrentMarketQuantity(searchTransferMarketFactTO.getTransferAmount());
					loanableSecuritiesMarketfact.setMarketRate(searchTransferMarketFactTO.getMarketRate());
					lstLoanableSecuritiesMarketfact.add(loanableSecuritiesMarketfact);
				}
				loanableSecuritiesOperation.setLstLoanableSecuritiesMarketfact(lstLoanableSecuritiesMarketfact);
				lstLoanableSecuritiesOperation.add(loanableSecuritiesOperation);
			}
		}	
		loanableSecuritiesRequestSession.setLstLoanableSecuritiesOperation(lstLoanableSecuritiesOperation);	
				
		try {					
			loanableSecuritiesRequest=manageLoanableSecuritiesOperationFacade.registryLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession);
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				cleanLoanableSecuritiesOperationSearchHandler(null);
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequest)){
			setViewOperationType(ViewOperationsType.CONSULT.getCode());	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_REGISTER_OK,
				    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );						
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}	
		cleanLoanableSecuritiesOperationSearchHandler(null);
	}
	
	/**
	 * View Security Financial.
	 *
	 * @param securityCode the security code
	 */
	public void viewSecurityFinancial(String securityCode)
	{
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		  securityData.setSecurityCodePk(securityCode);
		  securityData.setUiComponentName("securityHelp");
		  showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * Validate Holder Handler.
	 */
	public void validateHolderHandler() {  
		  executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		  cleanModelRegister();
		  lstHolderAccount= new ArrayList<HolderAccount>();
		  if (loanableSecuritiesRequestSession.getHolder().getIdHolderPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolder().getIdHolderPk().intValue())) {
		   loanableSecuritiesRequestSession.getHolder().setIdHolderPk(null);
//		   showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND));		  
//		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  // Get holder from database.
		  Holder holder = null;
		  try {
		   holder = manageLoanableSecuritiesOperationFacade.getHolderServiceFacade(loanableSecuritiesRequestSession.getHolder().getIdHolderPk());
		   holder.setFullName(holder.getDescriptionHolder());
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holder)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND) );		   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER) );			  	   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  loanableSecuritiesRequestSession.setHolder(holder);
		  
		  if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk())){
			  HolderAccountTO holderAccountTO = new HolderAccountTO();
			  holderAccountTO.setParticipantTO(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk());
			  holderAccountTO.setIdHolderPk(holder.getIdHolderPk());
			  try{
			  lstHolderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountListForParticipantAndHolder(holderAccountTO);
			  } catch (Exception e) {
				   e.printStackTrace();
				  }
		  }

		 }
	
	/**
	 * Validate Holder Handler.
	 */
	public void validateHolderHandlerSearch() {  
		  executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		  cleanDataModel();
		  if (searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk() == null || !Validations.validateIsPositiveNumber(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk().intValue())) {
			  searchLoanableSecuritiesOperationTO.getCUIHolder().setIdHolderPk(null);
//		   showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND));		  
//		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  // Get holder from database.
		  Holder holder = null;
		  try {
		   holder = manageLoanableSecuritiesOperationFacade.getHolderServiceFacade(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk());
		   holder.setFullName(holder.getDescriptionHolder());
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holder)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND) );		   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER) );			  	   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  searchLoanableSecuritiesOperationTO.setCUIHolder(holder);

		 }
	
	/**
	 * Validate Holder Account Hanlder.
	 */
	public void validateHolderAccountHanlder()
	{
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  cleanModelRegister();
		  if (loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk().intValue())) {
			this.loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));	
			 JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  HolderAccountTO holderAccountTO = new HolderAccountTO();
		  holderAccountTO.setParticipantTO(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk());
		  holderAccountTO.setIdHolderPk(loanableSecuritiesRequestSession.getHolder().getIdHolderPk());
		  holderAccountTO.setHolderAccountNumber(loanableSecuritiesRequestSession.getHolderAccount().getAccountNumber());
		  // Get holder from database.
		  HolderAccount holderAccount = null;
		  try {
			  holderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountComponentServiceBean(holderAccountTO);
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holderAccount)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));		 
		   this.loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_REGISTER));			 
		   this.loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  loanableSecuritiesRequestSession.setHolderAccount(holderAccount);
	}
	
	/**
	 * Validate Holder Account Hanlder.
	 */
	public void validateHolderAccountHanlderSearch()
	{
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  cleanDataModel();
		  if (searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk() == null || !Validations.validateIsPositiveNumber(searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk().intValue())) {
			this.searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));	
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  HolderAccountTO holderAccountTO = new HolderAccountTO();
		  holderAccountTO.setParticipantTO(searchLoanableSecuritiesOperationTO.getIdParticipant());
		  holderAccountTO.setIdHolderPk(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk());
		  holderAccountTO.setHolderAccountNumber(searchLoanableSecuritiesOperationTO.getHolderAccount().getAccountNumber());
		  // Get holder from database.
		  HolderAccount holderAccount = null;
		  try {
			  holderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountComponentServiceBean(holderAccountTO);
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holderAccount)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));		 
		   this.searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_REGISTER));			 
		   this.searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  searchLoanableSecuritiesOperationTO.setHolderAccount(holderAccount);
	}
	
	/**
	 * Clean Holder Account Balances Handler.
	 *
	 * @param e the e
	 */
	public void cleanHolderAccountBalancesHandler(ActionEvent e)
	{
		executeAction();
		loanableSecuritiesRequestSession = new LoanableSecuritiesRequest();
		if(!userInfo.getUserAccountSession().isParticipantInstitucion())
			loanableSecuritiesRequestSession.setParticipant(new Participant());
		loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		loanableSecuritiesRequestSession.setHolder(new Holder());
		cleanModelRegister();
		lstHolderAccount = new ArrayList<HolderAccount>();
		marketFactBalanceHelpTO=null;
	}
	
	/**
	 * Search Holder Account Balances Handler.
	 */
	public void searchHolderAccountBalancesHandler()
	{
		 	executeAction();
		  JSFUtilities.hideGeneralDialogues();  
		  try{
		  if (loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk().intValue())
				  ||loanableSecuritiesRequestSession.getHolder().getIdHolderPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolder().getIdHolderPk().intValue())
						  ||loanableSecuritiesRequestSession.getHolderAccount()==null||
				  loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk().intValue())) {			   
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
			  JSFUtilities.showSimpleValidationDialog();
			   return;
			  }		 
		  HolderBalanceMovementsTO holderBalanceMovementsTO = new HolderBalanceMovementsTO();
		  holderBalanceMovementsTO.setObjParticipant(loanableSecuritiesRequestSession.getParticipant());
		  holderBalanceMovementsTO.setAccountNumber(Integer.valueOf(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk().toString()));	  
		 
			  lstholderAccountBalance = manageLoanableSecuritiesOperationFacade.getHolderAccountBalanceByFilterServiceBean(holderBalanceMovementsTO);
		  
		  if (Validations.validateIsNull(lstholderAccountBalance)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_BALANCES_NOT_FOUND));
			  JSFUtilities.showSimpleValidationDialog();
			   return;
			  }
		  else
		  {		
			 
			  List<SearchHolderAccountBalanceTO> lstSearchHolderAccountBalanceTO = new ArrayList<SearchHolderAccountBalanceTO>();
			  SearchHolderAccountBalanceTO searchHolderAccountBalanceTO = null;
			  List<HolderAccountBalanceTO> lstHolAccBalTemp = new ArrayList<HolderAccountBalanceTO>();
			  HolderAccountBalanceTO holderAccountBalanceTO=null;			 
			  for( HolderAccountBalance holderAccountBalanceAux : lstholderAccountBalance)			  {
				  	  
				  searchHolderAccountBalanceTO = new SearchHolderAccountBalanceTO();
				  holderAccountBalanceTO = new HolderAccountBalanceTO();
				  
				  searchHolderAccountBalanceTO.setId(holderAccountBalanceAux.getId().toString());
				  searchHolderAccountBalanceTO.setIdParticipantPk(holderAccountBalanceAux.getId().getIdParticipantPk());
				  searchHolderAccountBalanceTO.setIdHolderAccountPk(holderAccountBalanceAux.getId().getIdHolderAccountPk());
				  searchHolderAccountBalanceTO.setIdSecurityCodePk(holderAccountBalanceAux.getId().getIdSecurityCodePk());
				  searchHolderAccountBalanceTO.setTotalBalance(holderAccountBalanceAux.getTotalBalance());
				  searchHolderAccountBalanceTO.setAvailableBalance(holderAccountBalanceAux.getAvailableBalance());	
				 
				  if(manageLoanableSecuritiesOperationFacade.existSecuritiesTVR(searchHolderAccountBalanceTO.getIdSecurityCodePk()))
				  {
					  lstSearchHolderAccountBalanceTO.add(searchHolderAccountBalanceTO);	
					  holderAccountBalanceTO.setIdHolderAccount(holderAccountBalanceAux.getId().getIdHolderAccountPk());
					  holderAccountBalanceTO.setIdParticipant(holderAccountBalanceAux.getId().getIdParticipantPk());
					  holderAccountBalanceTO.setIdSecurityCode(holderAccountBalanceAux.getId().getIdSecurityCodePk());
					  lstHolAccBalTemp.add(holderAccountBalanceTO);
				  }						 
			  }
			  
			  searchLoanableSecuritiesOperationTOHeader.setLstSearchHolderAccountBalanceTO(lstSearchHolderAccountBalanceTO);
			  
			  if(Validations.validateListIsNotNullAndNotEmpty(lstHolAccBalTemp))
			  {
				  lstHolAccBalTemp = manageLoanableSecuritiesOperationFacade.findAffectationWithMarketFact(lstHolAccBalTemp);
				  
				  for( HolderAccountBalanceTO holderAccountBalanceTOAux : lstHolAccBalTemp)
				  {				
					  String idHolderAccountBalanceAux = holderAccountBalanceTOAux.getIdParticipant()+"-"+holderAccountBalanceTOAux.getIdHolderAccount()+"-"+holderAccountBalanceTOAux.getIdSecurityCode();
					  for( SearchHolderAccountBalanceTO searchHolderAccountBalanceTOAux : searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO())
					  {
						  searchHolderAccountBalanceTOAux.setSelected(Boolean.FALSE);
						  if(idHolderAccountBalanceAux.equals(searchHolderAccountBalanceTOAux.getId()))
							  searchHolderAccountBalanceTOAux.setMarketLock(holderAccountBalanceTOAux.getMarketLock());	
					  }  			  
				  }
			  }
			  
			  searchHolderAccountBalanceTODataModel = new GenericDataModel<SearchHolderAccountBalanceTO>(searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO());
		  }	
		  } catch (Exception e) {
			   e.printStackTrace();
		}
	}
	
	/**
	 * Evaluate Balance To Pay Handler.
	 *
	 * @param searchHolderAccountBalanceTO the search holder account balance to
	 */
	public void evaluateBalanceToPayHandler(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO)
	{
		BigDecimal currentQuantity = searchHolderAccountBalanceTO.getTransferAmount();
		executeAction();
		if(currentQuantity==null || currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ONE.negate().intValue() 
				|| currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ZERO.intValue()){
			searchHolderAccountBalanceTO.setTransferAmount(null);
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_ZERO));	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}		
		BigDecimal amountAvailable = searchHolderAccountBalanceTO.getAvailableBalance();
		if(currentQuantity.compareTo(amountAvailable) == BigDecimal.ONE.intValue())
		{
			searchHolderAccountBalanceTO.setTransferAmount(null);
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_AVAILABLE_BALANCE));	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}					
	}	
	
	/**
	 * Select Balance To Pay Detail Handler.
	 *
	 * @param searchHolderAccountBalanceTO the search holder account balance to
	 */
	public void selectBalanceToPayDetailHandler(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO)
	{
		searchHolderAccountBalanceTO.setTransferAmount(null);
		marketFactBalanceHelpTO=null;
	}
	
	/**
	 * Select Participant Handler.
	 */
	public void selectParticipantHandler()
	{
		loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		cleanModelRegister();
		lstHolderAccount= new ArrayList<HolderAccount>();
		 if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk())
				 && Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getHolder().getIdHolderPk())){
			  HolderAccountTO holderAccountTO = new HolderAccountTO();
			  holderAccountTO.setParticipantTO(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk());
			  holderAccountTO.setIdHolderPk(loanableSecuritiesRequestSession.getHolder().getIdHolderPk());
			  try{
			  lstHolderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountListForParticipantAndHolder(holderAccountTO);
			  } catch (Exception e) {
				   e.printStackTrace();
				  }
		  }
	}
/**
 * Clean Model Register.
 */
	public void cleanModelRegister()
	{
		searchHolderAccountBalanceTODataModel=null;
	}
	/**
	 * Clean placement segment search handler.
	 *
	 * @param e the e
	 */
	public void cleanLoanableSecuritiesOperationSearchHandler(ActionEvent e){
		executeAction();
		searchLoanableSecuritiesOperationTO.setInitialDate(CommonsUtilities.currentDate());
		searchLoanableSecuritiesOperationTO.setFinalDate(CommonsUtilities.currentDate());
		if(!userInfo.getUserAccountSession().isParticipantInstitucion())
			searchLoanableSecuritiesOperationTO.setIdParticipant(null);
		searchLoanableSecuritiesOperationTO.setIdState(null);
		searchLoanableSecuritiesOperationTO.setRequestNumber(null);
		searchLoanableSecuritiesOperationTO.setCUIHolder(new Holder());
		searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		searchLoanableSecuritiesOperationTO.setRequestType(null);
		searchLoanableSecuritiesOperationTO.setLstSearchHolderAccountBalanceTO(new ArrayList<SearchHolderAccountBalanceTO>());
		cleanDataModel();
	}
	
	/**
	 * Search Loanable Securities Operation Handler.
	 */
	@LoggerAuditWeb
	public void searchLoanableSecuritiesOperationHandler()
	{
		JSFUtilities.hideGeneralDialogues();  
		if(Validations.validateIsNullOrNotPositive(searchLoanableSecuritiesOperationTO.getIdParticipant()))
		{
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		}
		List <LoanableSecuritiesRequest> lstLoanableSecuritiesRequest=null;
		try{
			searchLoanableSecuritiesOperationTO.setRequestType(LoanableSecuritiesOperationRequestType.ENTRY_REQUEST_TYPE.getCode());
			lstLoanableSecuritiesRequest =  manageLoanableSecuritiesOperationFacade.getListLoanableSecuritiesRequestFilter(searchLoanableSecuritiesOperationTO);
			searchLoanableSecuritiesRequestModel = new GenericDataModel<LoanableSecuritiesRequest>(lstLoanableSecuritiesRequest);
			settingSearchData();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Setting search data.
	 */
	public void settingSearchData()
	{
		if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getRequestNumber())
				&&searchLoanableSecuritiesOperationTO.getRequestNumber().equals(GeneralConstants.ZERO_VALUE_LONG))
			searchLoanableSecuritiesOperationTO.setRequestNumber(null);
		if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getCUIHolder())
				&& Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk())
				&&searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk().equals(GeneralConstants.ZERO_VALUE_LONG))
				searchLoanableSecuritiesOperationTO.setCUIHolder(new Holder());
		if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getHolderAccount())
				&& Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk())
				&& searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk().equals(GeneralConstants.ZERO_VALUE_LONG))
			searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
	}
	
	/**
	 * Select Loanable Securities Operation Handler.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 */
	public void selectLoanableSecuritiesOperationHandler(LoanableSecuritiesRequest loanableSecuritiesRequest)
	{
		executeAction();
		cleanModelRegister();
		SearchLoanableSecuritiesOperationTO objSearchLoanableSecuritiesOperationTO = new SearchLoanableSecuritiesOperationTO();
		objSearchLoanableSecuritiesOperationTO.setRequestNumber(loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());		
		LoanableSecuritiesRequest tempLoanableSecuritiesRequest=null;
		createObjectRegister();
		try{
			tempLoanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.getLoanableSecuritiesRequestFilter(objSearchLoanableSecuritiesOperationTO);
			setViewOperationType(ViewOperationsType.REGISTER.getCode());//for load participant
			loadAllParticipant();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}		
		setLoanableSecuritiesRequestSession(tempLoanableSecuritiesRequest);	
		
		List<SearchHolderAccountBalanceTO> lstSearchHolderAccountBalanceTO = new ArrayList<SearchHolderAccountBalanceTO>();
		  SearchHolderAccountBalanceTO searchHolderAccountBalanceTO = null;
		  List<HolderAccountBalanceTO> lstHolAccBalTemp = new ArrayList<HolderAccountBalanceTO>();
		  HolderAccountBalanceTO holderAccountBalanceTO=null;		
		  if(tempLoanableSecuritiesRequest!=null){
			  for( HolderAccountBalance holderAccountBalanceAux : tempLoanableSecuritiesRequest.getHolderAccount().getHolderAccountBalances())			  {
			  	  
				  searchHolderAccountBalanceTO = new SearchHolderAccountBalanceTO();
				  holderAccountBalanceTO = new HolderAccountBalanceTO();
				  
				  searchHolderAccountBalanceTO.setId(holderAccountBalanceAux.getId().toString());
				  searchHolderAccountBalanceTO.setIdParticipantPk(holderAccountBalanceAux.getId().getIdParticipantPk());
				  searchHolderAccountBalanceTO.setIdHolderAccountPk(holderAccountBalanceAux.getId().getIdHolderAccountPk());
				  searchHolderAccountBalanceTO.setIdSecurityCodePk(holderAccountBalanceAux.getId().getIdSecurityCodePk());
				  searchHolderAccountBalanceTO.setTotalBalance(holderAccountBalanceAux.getTotalBalance());
				  searchHolderAccountBalanceTO.setAvailableBalance(holderAccountBalanceAux.getAvailableBalance());	
				  searchHolderAccountBalanceTO.setTransferAmount(holderAccountBalanceAux.getTransferAmmount());
				  
				  List<SearchTransferMarketFactTO> lsttransferMarketFactDetail= new ArrayList<SearchTransferMarketFactTO>();
				  for(HolderMarketFactBalance holderMarketFactBalance : holderAccountBalanceAux.getHolderMarketfactBalance())
				  {
					  SearchTransferMarketFactTO searchTransferMarketFactTO = new SearchTransferMarketFactTO();
					  searchTransferMarketFactTO.setMarketDate(holderMarketFactBalance.getMarketDate());
					  searchTransferMarketFactTO.setMarketPrice(holderMarketFactBalance.getMarketPrice());
					  searchTransferMarketFactTO.setMarketRate(holderMarketFactBalance.getMarketRate());
					  searchTransferMarketFactTO.setTotalBalance(holderMarketFactBalance.getTotalBalance());
					  searchTransferMarketFactTO.setAvailableBalance(holderMarketFactBalance.getAvailableBalance());
					  searchTransferMarketFactTO.setTransferAmount(holderMarketFactBalance.getTransferAmmount());
					  lsttransferMarketFactDetail.add(searchTransferMarketFactTO);
				  }
				  searchHolderAccountBalanceTO.setTransferMarketFactDetail(lsttransferMarketFactDetail);			  
				  
				  lstSearchHolderAccountBalanceTO.add(searchHolderAccountBalanceTO);				 
				  
				  holderAccountBalanceTO.setIdHolderAccount(holderAccountBalanceAux.getId().getIdHolderAccountPk());
				  holderAccountBalanceTO.setIdParticipant(holderAccountBalanceAux.getId().getIdParticipantPk());
				  holderAccountBalanceTO.setIdSecurityCode(holderAccountBalanceAux.getId().getIdSecurityCodePk());
				  lstHolAccBalTemp.add(holderAccountBalanceTO);
			  }
		  }	
		  
		  searchLoanableSecuritiesOperationTOHeader.setLstSearchHolderAccountBalanceTO(lstSearchHolderAccountBalanceTO);
		  
		 
		  searchHolderAccountBalanceTODataModel = new GenericDataModel<SearchHolderAccountBalanceTO>(searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO());
		  settingSearchData();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Get Search Loanable Securities Operation TO.
	 *
	 * @return object search Loanable Securities Operation TO
	 */
	public SearchLoanableSecuritiesOperationTO getSearchLoanableSecuritiesOperationTO() {
		return searchLoanableSecuritiesOperationTO;
	}
	
	/**
	 * Set Search Loanable Securities Operation TO.
	 *
	 * @param searchLoanableSecuritiesOperationTO search Loanable Securities Operation TO
	 */
	public void setSearchLoanableSecuritiesOperationTO(
			SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO) {
		this.searchLoanableSecuritiesOperationTO = searchLoanableSecuritiesOperationTO;
	}	
	
	/**
	 * Get Search Loanable Securities Operation TO Header.
	 *
	 * @return object search Loanable Securities Operation TO Header
	 */
	public SearchLoanableSecuritiesOperationTO getSearchLoanableSecuritiesOperationTOHeader() {
		return searchLoanableSecuritiesOperationTOHeader;
	}
	
	/**
	 * Set Search Loanable Securities Operation TO Header.
	 *
	 * @param searchLoanableSecuritiesOperationTOHeader the new search loanable securities operation to header
	 */
	public void setSearchLoanableSecuritiesOperationTOHeader(
			SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTOHeader) {
		this.searchLoanableSecuritiesOperationTOHeader = searchLoanableSecuritiesOperationTOHeader;
	}
	
	/**
	 * Get Search Loanable Securities Request Model.
	 *
	 * @return Model Loanable Securities Request
	 */
	public GenericDataModel<LoanableSecuritiesRequest> getSearchLoanableSecuritiesRequestModel() {
		return searchLoanableSecuritiesRequestModel;
	}
	
	/**
	 * Set Search Loanable Securities Request Model.
	 *
	 * @param searchLoanableSecuritiesRequestModel Model Loanable Securities Request
	 */
	public void setSearchLoanableSecuritiesRequestModel(
			GenericDataModel<LoanableSecuritiesRequest> searchLoanableSecuritiesRequestModel) {
		this.searchLoanableSecuritiesRequestModel = searchLoanableSecuritiesRequestModel;
	}
	
	/**
	 * Get Loanable Securities Request Session.
	 *
	 * @return obejct Loanable Securities Request
	 */
	public LoanableSecuritiesRequest getLoanableSecuritiesRequestSession() {
		return loanableSecuritiesRequestSession;
	}
	
	/**
	 * Set Loanable Securities Request Session.
	 *
	 * @param loanableSecuritiesRequestSession object Loanable Securities Request
	 */
	public void setLoanableSecuritiesRequestSession(
			LoanableSecuritiesRequest loanableSecuritiesRequestSession) {
		this.loanableSecuritiesRequestSession = loanableSecuritiesRequestSession;
	}
	
	/**
	 * Get List All Participant.
	 *
	 * @return List All Participant
	 */
	public List<Participant> getLstAllParticipant() {
		return lstAllParticipant;
	}
	
	/**
	 * Set List All Participant.
	 *
	 * @param lstAllParticipant List All Participant
	 */
	public void setLstAllParticipant(List<Participant> lstAllParticipant) {
		this.lstAllParticipant = lstAllParticipant;
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		searchLoanableSecuritiesRequestModel = null;
	}
	
	/**
	 * Get Holder Account Balance Session.
	 *
	 * @return object  Holder Account Balance
	 */
	public HolderAccountBalance getHolderAccountBalanceSession() {
		return holderAccountBalanceSession;
	}
	
	/**
	 * Set Holder Account Balance Session.
	 *
	 * @param holderAccountBalanceSession object Holder Account Balance
	 */
	public void setHolderAccountBalanceSession(
			HolderAccountBalance holderAccountBalanceSession) {
		this.holderAccountBalanceSession = holderAccountBalanceSession;
	}
	
	/**
	 * Get Security Code.
	 *
	 * @return code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Set Security Code.
	 *
	 * @param securityCode code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	/**
	 * Select MarketFact Balance Bean.
	 */
	public void selectMarketFactBalanceBean(){
		MarketFactBalanceHelpTO marketFactBalance = this.marketFactBalanceHelpTO;
		for(SearchHolderAccountBalanceTO habh: searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO()){
				if(habh.getIdParticipantPk().equals(marketFactBalance.getParticipantPk()) &&
				   habh.getIdHolderAccountPk().equals(marketFactBalance.getHolderAccountPk()) &&
				   habh.getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())){
					
					if(!marketFactBalance.getBalanceResult().equals(BigDecimal.ZERO)){
						habh.setTransferAmount(marketFactBalance.getBalanceResult());
						habh.setTransferMarketFactDetail(new ArrayList<SearchTransferMarketFactTO>());
						
						for(MarketFactDetailHelpTO mkfDetTO: marketFactBalance.getMarketFacBalances()){
							SearchTransferMarketFactTO marketFactTO = new SearchTransferMarketFactTO();
							marketFactTO.setIdHolderMarketPk(mkfDetTO.getMarketFactBalancePk());
							marketFactTO.setTransferAmount(mkfDetTO.getEnteredBalance());
							marketFactTO.setMarketDate(mkfDetTO.getMarketDate());
							marketFactTO.setMarketPrice(mkfDetTO.getMarketPrice());
							marketFactTO.setMarketRate(mkfDetTO.getMarketRate());
							habh.getTransferMarketFactDetail().add(marketFactTO);
						}
						validateAmountTransferForBean(habh);
					}else{
						clearQuantitiesTransfer(habh);
					}
					JSFUtilities.updateComponent("frmLoanableSecuritiesOperation:opnlResult");
					return;
				}			
		}
	}
	
	/**
	 * Validate Amount Transfer .
	 *
	 * @param objHolAccBalTO object for Holder Account Balance
	 */
	public void validateAmountTransferForBean(SearchHolderAccountBalanceTO objHolAccBalTO){
		
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getTransferAmount())){	
			BigDecimal quantityTransfer = objHolAccBalTO.getTransferAmount();			
			BigDecimal available = objHolAccBalTO.getAvailableBalance();
			if(available.compareTo(quantityTransfer) >= 0){
				objHolAccBalTO.setTransferAmount(quantityTransfer);					
			}else{						
				clearQuantitiesTransfer(objHolAccBalTO);
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_AVAILABLE_BALANCE));	
				JSFUtilities.showSimpleValidationDialog();
			}						
		}else{			
			clearQuantitiesTransfer(objHolAccBalTO);
		}
	}
	/**
	 * Clear quantities blocks.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	private void clearQuantitiesTransfer(SearchHolderAccountBalanceTO objHolAccBalTO){
		objHolAccBalTO.setTransferAmount(null);
	}
	
	/**
	 * Show MarketFact UI.
	 *
	 * @param searchHolderAccountBalanceTO the search holder account balance to
	 */
	public void showMarketFactUI(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO){
		
		BigDecimal currentQuantity = searchHolderAccountBalanceTO.getTransferAmount();
		executeAction();
		if(!searchHolderAccountBalanceTO.getMarketLock())
		{
			if(currentQuantity==null || currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ONE.negate().intValue() 
					|| currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ZERO.intValue()){
				searchHolderAccountBalanceTO.setTransferAmount(null);
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_ZERO));	
				JSFUtilities.showSimpleValidationDialog();
				return;
			}		
			BigDecimal amountAvailable = searchHolderAccountBalanceTO.getAvailableBalance();
			if(currentQuantity.compareTo(amountAvailable) == BigDecimal.ONE.intValue())
			{
				searchHolderAccountBalanceTO.setTransferAmount(null);
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_AVAILABLE_BALANCE));	
				JSFUtilities.showSimpleValidationDialog();
				return;
			}		
		}				
	 
	  MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(searchHolderAccountBalanceTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(searchHolderAccountBalanceTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(searchHolderAccountBalanceTO.getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		if(searchHolderAccountBalanceTO.getTransferAmount()!=null && !searchHolderAccountBalanceTO.getTransferAmount().equals(BigDecimal.ZERO)){
			marketFactBalance.setBalanceResult(searchHolderAccountBalanceTO.getTransferAmount());
		}

		if(searchHolderAccountBalanceTO.getTransferMarketFactDetail()!=null){
			for(SearchTransferMarketFactTO transferMarketFact: searchHolderAccountBalanceTO.getTransferMarketFactDetail()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(transferMarketFact.getIdHolderMarketPk());
				marketDetail.setEnteredBalance(transferMarketFact.getTransferAmount());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 	  	
	}
	
	/**
	 * Show MarketFact UI Detail.
	 *
	 * @param searchHolderAccountBalanceTO object  Holder Account Balance TO
	 */
public void showMarketFactUIDetail(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO){
	 try{
	  marketFactBalanceDetail = new MarketFactBalanceHelpTO();
	  Security security = manageLoanableSecuritiesOperationFacade.findSecurityById(searchHolderAccountBalanceTO.getIdSecurityCodePk());
	  marketFactBalanceDetail.setSecurityCodePk(security.getIdSecurityCodePk());	  
	  marketFactBalanceDetail.setSecurityDescription(security.getDescription());
	  marketFactBalanceDetail.setInstrumentDescription(security.getInstrumentTypeDescription());
	  marketFactBalanceDetail.setInstrumentType(security.getInstrumentType());
	  marketFactBalanceDetail.setHolderAccountPk(searchHolderAccountBalanceTO.getIdHolderAccountPk());
	  marketFactBalanceDetail.setParticipantPk(searchHolderAccountBalanceTO.getIdParticipantPk());
	  marketFactBalanceDetail.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
	  marketFactBalanceDetail.setBalanceResult(searchHolderAccountBalanceTO.getTransferAmount());		

		if(searchHolderAccountBalanceTO.getTransferMarketFactDetail()!=null){
			for(SearchTransferMarketFactTO transferMarketFact: searchHolderAccountBalanceTO.getTransferMarketFactDetail()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(transferMarketFact.getIdHolderMarketPk());				
				marketDetail.setMarketDate(transferMarketFact.getMarketDate());
				if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
				{
					marketDetail.setMarketRate(transferMarketFact.getMarketRate());
					if(Validations.validateIsNotNullAndNotEmpty(marketFactBalanceDetail.getLastMarketDate()))
					{
						if(marketDetail.getMarketDate().compareTo(marketFactBalanceDetail.getLastMarketDate())==-1);
						else {
							marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
							marketFactBalanceDetail.setLastMarketRate(marketDetail.getMarketRate());
						}							
					}else {
						marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
						marketFactBalanceDetail.setLastMarketRate(marketDetail.getMarketRate());
					}					
				}
				if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				{
					marketDetail.setMarketPrice(transferMarketFact.getMarketPrice());
					if(Validations.validateIsNotNullAndNotEmpty(marketFactBalanceDetail.getLastMarketDate()))
					{
						if(marketDetail.getMarketDate().compareTo(marketFactBalanceDetail.getLastMarketDate())==-1);
						else {
							marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
							marketFactBalanceDetail.setLastMarketPrice(marketDetail.getMarketPrice());
						}							
					}else {
						marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
						marketFactBalanceDetail.setLastMarketPrice(marketDetail.getMarketPrice());
					}		
				}
				marketDetail.setTotalBalance(transferMarketFact.getTotalBalance());
				marketDetail.setAvailableBalance(transferMarketFact.getAvailableBalance());
				marketDetail.setEnteredBalance(transferMarketFact.getTransferAmount());
				marketFactBalanceDetail.getMarketFacBalances().add(marketDetail);
			}
		}
		
		JSFUtilities.executeJavascriptFunction("PF('dialogWMarketfactBalance').show()"); 	
	 } catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Validate Zero.
	 */
	public void validateZero(){
		if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getInterestRate())){
			if(loanableSecuritiesRequestSession.getInterestRate().compareTo(BigDecimal.ZERO)==0)
				loanableSecuritiesRequestSession.setInterestRate(null);
		}
		if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getValidityDays())){
			if(BigDecimal.valueOf(loanableSecuritiesRequestSession.getValidityDays()).compareTo(BigDecimal.ZERO)==0)
				loanableSecuritiesRequestSession.setValidityDays(null);
		}
	}
	
	/**
	 * Get MarketFact Balance Help TO.
	 *
	 * @return object MarketFact Balance Help
	 */
	public MarketFactBalanceHelpTO getMarketFactBalanceHelpTO() {
		return marketFactBalanceHelpTO;
	}
	
	/**
	 * Set MarketFact Balance Help TO.
	 *
	 * @param marketFactBalanceHelpTO object MarketFact Balance Help
	 */
	public void setMarketFactBalanceHelpTO(
			MarketFactBalanceHelpTO marketFactBalanceHelpTO) {
		this.marketFactBalanceHelpTO = marketFactBalanceHelpTO;
	}
	
	/**
	 * Get Search Holder Account Balance TO Data Model.
	 *
	 * @return object Data Model
	 */
	public GenericDataModel<SearchHolderAccountBalanceTO> getSearchHolderAccountBalanceTODataModel() {
		return searchHolderAccountBalanceTODataModel;
	}
	
	/**
	 * Set Search Holder Account Balance TO Data Model.
	 *
	 * @param searchHolderAccountBalanceTODataModel object Data Model
	 */
	public void setSearchHolderAccountBalanceTODataModel(
			GenericDataModel<SearchHolderAccountBalanceTO> searchHolderAccountBalanceTODataModel) {
		this.searchHolderAccountBalanceTODataModel = searchHolderAccountBalanceTODataModel;
	}
	
	/**
	 * Get Motive Controller Annul.
	 *
	 * @return object Motive Controller Annul
	 */
	public MotiveController getMotiveControllerAnnul() {
		return motiveControllerAnnul;
	}
	
	/**
	 * Set Motive Controller Annul.
	 *
	 * @param motiveControllerAnnul object Motive Controller Annul
	 */
	public void setMotiveControllerAnnul(MotiveController motiveControllerAnnul) {
		this.motiveControllerAnnul = motiveControllerAnnul;
	}
	
	/**
	 * Get Motive Controller Reject.
	 *
	 * @return object Motive Controller Reject
	 */
	public MotiveController getMotiveControllerReject() {
		return motiveControllerReject;
	}
	
	/**
	 * Set Motive Controller Reject.
	 *
	 * @param motiveControllerReject object Motive Controller Reject
	 */
	public void setMotiveControllerReject(MotiveController motiveControllerReject) {
		this.motiveControllerReject = motiveControllerReject;
	}
	
	/**
	 * Gets the market fact balance detail.
	 *
	 * @return the market fact balance detail
	 */
	public MarketFactBalanceHelpTO getMarketFactBalanceDetail() {
		return marketFactBalanceDetail;
	}
	
	/**
	 * Sets the market fact balance detail.
	 *
	 * @param marketFactBalanceDetail the new market fact balance detail
	 */
	public void setMarketFactBalanceDetail(
			MarketFactBalanceHelpTO marketFactBalanceDetail) {
		this.marketFactBalanceDetail = marketFactBalanceDetail;
	}
	
	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}
	
	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	
}
