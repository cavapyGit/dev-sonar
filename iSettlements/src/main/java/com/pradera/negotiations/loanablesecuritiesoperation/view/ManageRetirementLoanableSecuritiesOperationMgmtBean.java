package com.pradera.negotiations.loanablesecuritiesoperation.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.HolderBalanceMovementsTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.LoanableSecuritiesOperation;
import com.pradera.model.negotiation.LoanableSecuritiesRequest;
import com.pradera.model.negotiation.LoanableUnblockMarketfact;
import com.pradera.model.negotiation.LoanableUnblockOperation;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationMotiveRejectRetirementType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationRequestType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationStateType;
import com.pradera.model.negotiation.type.LoanableUnblockMotiveType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.negotiations.loanablesecuritiesoperation.facade.ManageLoanableSecuritiesOperationFacade;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchHolderAccountBalanceTO;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchLoanableSecuritiesOperationTO;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchTransferMarketFactTO;
import com.pradera.settlements.utils.MotiveController;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageRetirementLoanableSecuritiesOperationMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ManageRetirementLoanableSecuritiesOperationMgmtBean  extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** 	 the Search loanable securities operation TO Header. */
	private SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO;
	
	 /** The search Holder Account Balance Data Model. */
	private GenericDataModel<SearchHolderAccountBalanceTO> searchHolderAccountBalanceTODataModel;
	
	/** 	 the Search loanable securities operation TO. */
	private SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTOHeader;
	
	/** The  manage loanable securities operation facade. */
	@EJB
	ManageLoanableSecuritiesOperationFacade manageLoanableSecuritiesOperationFacade;
	
	/** The general parameter facade. */
	@EJB
	GeneralParametersFacade generalParameterFacade;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The motive controller reject. */
	private MotiveController motiveControllerReject;
	
	/** The all participant list. */
	private List<Participant> lstAllParticipant;
	
	/** The placement segment list. */
	private GenericDataModel<LoanableSecuritiesRequest> searchLoanableSecuritiesRequestModel;
	
	/** The loanable Securities Request session. */
	private LoanableSecuritiesRequest loanableSecuritiesRequestSession;
	
	/** The Loanable Securities Operation list. */
	private  List<LoanableSecuritiesOperation> lstLoanableSecuritiesOperation =null;	
	
	 /** The Loanable Securities Operation Session object. */
	private LoanableSecuritiesOperation loanableSecuritiesOperationSession;
	
	/**  The marketFact Balance Help. */
	private MarketFactBalanceHelpTO marketFactBalanceHelpTO,marketFactBalanceDetail;
	
	/**  List Holder Account. */
	private List<HolderAccount> lstHolderAccount;
	
	/** Code Security. */
	private String securityCode;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		beginConversation();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		createObject();
		try {
			loadAllParticipant();
			loadState();
			loadLoanableSecuritiesMotiveReject();
			loadHolidays();
			loadUserValidation();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load User Validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			searchLoanableSecuritiesOperationTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
		}
	}
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParameterFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create Object.
	 */
	public void createObject()
	{
		searchLoanableSecuritiesOperationTO = new SearchLoanableSecuritiesOperationTO(CommonsUtilities.currentDate(),CommonsUtilities.currentDate());
		searchLoanableSecuritiesOperationTO.setCUIHolder(new Holder());
		searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		motiveControllerReject = new MotiveController();
	}
	/**
	 * Load all participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllParticipant() throws ServiceException{
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			searchLoanableSecuritiesOperationTO.setLstAllParticipant(manageLoanableSecuritiesOperationFacade.getLisParticipantServiceBean(participantTO));
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			this.setLstAllParticipant(manageLoanableSecuritiesOperationFacade.getLisParticipantServiceBean(participantTO));
	}
	/**
	 * Load state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadState() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.LOANABLE_SECURITIES_OPERATION_STATE.getCode());
		List<ParameterTable> lstStateParameterTable = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstStateParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstStateParameterTable)
		{
			if(Long.valueOf(parameterTable.getParameterTablePk()).equals(LoanableSecuritiesOperationStateType.APPROVED.getCode())
					|| Long.valueOf(parameterTable.getParameterTablePk()).equals(LoanableSecuritiesOperationStateType.CANCELLED.getCode())
					|| Long.valueOf(parameterTable.getParameterTablePk()).equals(LoanableSecuritiesOperationStateType.EXPIRED.getCode()));
			else	lstStateParameterTableAux.add(parameterTable);
		}
		searchLoanableSecuritiesOperationTO.setLstState(lstStateParameterTableAux);
	}
	/**
	 * Load Loanable Securities Motive Reject.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLoanableSecuritiesMotiveReject() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.LOANABLE_SECURITIES_OPERATION_REJECT_MOTIVE_RETIREMENT.getCode());
		motiveControllerReject.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO), MasterTableType.LOANABLE_SECURITIES_OPERATION_REJECT_MOTIVE_RETIREMENT);
	}
	/**
	 * Registry loanable securities request handler.
	 */
	public void registryLoanableSecuritiesRequestHandler()
	{
		executeAction();
		cleanModelRegister();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		createObjectLoanableSecuritiesOperationSession();
		createObjectRegister();
		try {
			loadAllParticipant();
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			loanableSecuritiesRequestSession.getParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
	}
	
	/**
	 * Create Object Loanable Securities Operation Session.
	 */
	public void createObjectLoanableSecuritiesOperationSession()
	{
		loanableSecuritiesRequestSession = new LoanableSecuritiesRequest();
		loanableSecuritiesRequestSession.setParticipant(new Participant());
		loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		loanableSecuritiesRequestSession.setHolder(new Holder());
	}
	
	/**
	 * Create Object Register.
	 */
	public void createObjectRegister()
	{
		searchLoanableSecuritiesOperationTOHeader = new SearchLoanableSecuritiesOperationTO();
		this.lstAllParticipant = new ArrayList<Participant>();
		lstHolderAccount = new ArrayList<HolderAccount>();
	}
	
	/**
	 * Clean Model Register.
	 */
	public void cleanModelRegister()
	{
		searchHolderAccountBalanceTODataModel=null;
	}
	
	/**
	 * Validate Holder Handler.
	 */
	public void validateHolderHandler() {  
		  executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		  cleanModelRegister();
		  lstHolderAccount= new ArrayList<HolderAccount>();
		  if (loanableSecuritiesRequestSession.getHolder().getIdHolderPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolder().getIdHolderPk().intValue())) {
		   loanableSecuritiesRequestSession.getHolder().setIdHolderPk(null);
//		   showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND));		  
//		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  // Get holder from database.
		  Holder holder = null;
		  try {
		   holder = manageLoanableSecuritiesOperationFacade.getHolderServiceFacade(loanableSecuritiesRequestSession.getHolder().getIdHolderPk());
		   holder.setFullName(holder.getDescriptionHolder());
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holder)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND) );		   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER) );			  	   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  loanableSecuritiesRequestSession.setHolder(holder);

		  if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk())){
			  HolderAccountTO holderAccountTO = new HolderAccountTO();
			  holderAccountTO.setParticipantTO(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk());
			  holderAccountTO.setIdHolderPk(holder.getIdHolderPk());
			  try{
			  lstHolderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountListForParticipantAndHolder(holderAccountTO);
			  } catch (Exception e) {
				   e.printStackTrace();
				  }
		  }
		 }
	
	/**
	 * Validate Holder Account Hanlder.
	 */
	public void validateHolderAccountHanlder()
	{
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  cleanModelRegister();
		  if (loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk().intValue())) {
			this.loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));	
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  HolderAccountTO holderAccountTO = new HolderAccountTO();
		  holderAccountTO.setParticipantTO(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk());
		  holderAccountTO.setIdHolderPk(loanableSecuritiesRequestSession.getHolder().getIdHolderPk());
		  holderAccountTO.setHolderAccountNumber(loanableSecuritiesRequestSession.getHolderAccount().getAccountNumber());
		  // Get holder from database.
		  HolderAccount holderAccount = null;
		  try {
			  holderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountComponentServiceBean(holderAccountTO);
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holderAccount)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));		 
		   this.loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_REGISTER));			 
		   this.loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  loanableSecuritiesRequestSession.setHolderAccount(holderAccount);
	}
	
	/**
	 * Search Holder Account Balances Handler.
	 */
	public void searchHolderAccountBalancesHandler()
	{
		 	executeAction();
		  JSFUtilities.hideGeneralDialogues();  
		  try{
		  if (loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk().intValue())
				  ||loanableSecuritiesRequestSession.getHolder().getIdHolderPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolder().getIdHolderPk().intValue())
						  ||loanableSecuritiesRequestSession.getHolderAccount() == null
				  ||loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk() == null || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk().intValue())) {			   
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
			   JSFUtilities.showSimpleValidationDialog();
			   return;
			  }		 	  
		  SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTOAux=new SearchLoanableSecuritiesOperationTO();
		  searchLoanableSecuritiesOperationTOAux.setIdParticipant(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk());
		  searchLoanableSecuritiesOperationTOAux.setHolderAccount(loanableSecuritiesRequestSession.getHolderAccount());
		  lstLoanableSecuritiesOperation = manageLoanableSecuritiesOperationFacade.getLoanableSecuritiesOperationBalanceByFilterServiceBean(searchLoanableSecuritiesOperationTOAux);
		  
		  if (Validations.validateIsNull(lstLoanableSecuritiesOperation)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_BALANCES_LOANABLE_NOT_FOUND));
			   JSFUtilities.showSimpleValidationDialog();
			   return;
			  }
		  else
		  {		
			 
			  List<SearchHolderAccountBalanceTO> lstSearchHolderAccountBalanceTO = new ArrayList<SearchHolderAccountBalanceTO>();
			  SearchHolderAccountBalanceTO searchHolderAccountBalanceTO = null;
			  List<HolderAccountBalanceTO> lstHolAccBalTemp = new ArrayList<HolderAccountBalanceTO>();
			  HolderAccountBalanceTO holderAccountBalanceTO=null;			 
			  for( LoanableSecuritiesOperation loanableSecuritiesOperation : lstLoanableSecuritiesOperation)			  {
				  	  
				  searchHolderAccountBalanceTO = new SearchHolderAccountBalanceTO();
				  holderAccountBalanceTO = new HolderAccountBalanceTO();
				  
				  searchHolderAccountBalanceTO.setId( loanableSecuritiesOperation.getParticipant().getIdParticipantPk()+"-"+loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk()+"-"+loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				  searchHolderAccountBalanceTO.setIdParticipantPk(loanableSecuritiesOperation.getParticipant().getIdParticipantPk());
				  searchHolderAccountBalanceTO.setIdHolderAccountPk(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk());
				  searchHolderAccountBalanceTO.setIdSecurityCodePk(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				  searchHolderAccountBalanceTO.setTotalBalance(loanableSecuritiesOperation.getInitialQuantity());
				  searchHolderAccountBalanceTO.setLoanableBalance(loanableSecuritiesOperation.getCurrentQuantity());	
				  searchHolderAccountBalanceTO.setIdLoanableSecuritiesOperationPk(loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());
				  
				  if(manageLoanableSecuritiesOperationFacade.existSecuritiesTVR(searchHolderAccountBalanceTO.getIdSecurityCodePk()))
				  {
					  lstSearchHolderAccountBalanceTO.add(searchHolderAccountBalanceTO);	
					  holderAccountBalanceTO.setIdHolderAccount(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk());
					  holderAccountBalanceTO.setIdParticipant(loanableSecuritiesOperation.getParticipant().getIdParticipantPk());
					  holderAccountBalanceTO.setIdSecurityCode(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
					  lstHolAccBalTemp.add(holderAccountBalanceTO);
				  }						 
			  }
			  
			  searchLoanableSecuritiesOperationTOHeader.setLstSearchHolderAccountBalanceTO(lstSearchHolderAccountBalanceTO);
			  
			  if(Validations.validateListIsNotNullAndNotEmpty(lstHolAccBalTemp))
			  {
				  lstHolAccBalTemp = manageLoanableSecuritiesOperationFacade.findAffectationWithMarketFact(lstHolAccBalTemp);
				  
				  for( HolderAccountBalanceTO holderAccountBalanceTOAux : lstHolAccBalTemp)
				  {				
					  String idHolderAccountBalanceAux = holderAccountBalanceTOAux.getIdParticipant()+"-"+holderAccountBalanceTOAux.getIdHolderAccount()+"-"+holderAccountBalanceTOAux.getIdSecurityCode();
					  for( SearchHolderAccountBalanceTO searchHolderAccountBalanceTOAux : searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO())
					  {
						  searchHolderAccountBalanceTOAux.setSelected(Boolean.FALSE);
						  if(idHolderAccountBalanceAux.equals(searchHolderAccountBalanceTOAux.getId()))
							  searchHolderAccountBalanceTOAux.setMarketLock(holderAccountBalanceTOAux.getMarketLock());	
					  }  			  
				  }
			  }
			  
			  searchHolderAccountBalanceTODataModel = new GenericDataModel<SearchHolderAccountBalanceTO>(searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO());
		  }	
		  } catch (Exception e) {
			   e.printStackTrace();
		}
	}
	
	/**
	 * Evaluate Balance To Pay Handler.
	 *
	 * @param searchHolderAccountBalanceTO the search holder account balance to
	 */
	public void evaluateBalanceToPayHandler(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO)
	{
		BigDecimal currentQuantity = searchHolderAccountBalanceTO.getTransferAmount();
		executeAction();
		if(currentQuantity==null || currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ONE.negate().intValue() 
				|| currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ZERO.intValue()){
			searchHolderAccountBalanceTO.setTransferAmount(null);
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_RETIREMENT_BALANCE_ZERO));	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}		
		BigDecimal amountLoanable = searchHolderAccountBalanceTO.getLoanableBalance();
		if(currentQuantity.compareTo(amountLoanable) == BigDecimal.ONE.intValue())
		{
			searchHolderAccountBalanceTO.setTransferAmount(null);
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_LOANABLE_BALANCE));	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}					
	}	
	
	/**
	 * Select Balance To Pay Detail Handler.
	 *
	 * @param searchHolderAccountBalanceTO the search holder account balance to
	 */
	public void selectBalanceToPayDetailHandler(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO)
	{
		searchHolderAccountBalanceTO.setTransferAmount(null);
		marketFactBalanceHelpTO=null;
	}
	
	/**
	 * View Security Financial.
	 *
	 * @param securityCode the security code
	 */
	public void viewSecurityFinancial(String securityCode)
	{
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		  securityData.setSecurityCodePk(securityCode);
		  securityData.setUiComponentName("securityHelp");
		  showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * Show MarketFact UI.
	 *
	 * @param searchHolderAccountBalanceTO the search holder account balance to
	 */
	public void showMarketFactUI(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO){
		
		BigDecimal currentQuantity = searchHolderAccountBalanceTO.getTransferAmount();
		executeAction();
		if(!searchHolderAccountBalanceTO.getMarketLock())
		{
			if(currentQuantity==null || currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ONE.negate().intValue() 
					|| currentQuantity.compareTo(BigDecimal.ZERO) == BigDecimal.ZERO.intValue()){
				searchHolderAccountBalanceTO.setTransferAmount(null);
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_RETIREMENT_BALANCE_ZERO));	
				JSFUtilities.showSimpleValidationDialog();
				return;
			}		
			BigDecimal amountLoanable = searchHolderAccountBalanceTO.getLoanableBalance();
			if(currentQuantity.compareTo(amountLoanable) == BigDecimal.ONE.intValue())
			{
				searchHolderAccountBalanceTO.setTransferAmount(null);
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_LOANABLE_BALANCE));	
				JSFUtilities.showSimpleValidationDialog();
				return;
			}		
		}				
	 
	  MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(searchHolderAccountBalanceTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(searchHolderAccountBalanceTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(searchHolderAccountBalanceTO.getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		if(searchHolderAccountBalanceTO.getTransferAmount()!=null && !searchHolderAccountBalanceTO.getTransferAmount().equals(BigDecimal.ZERO)){
			marketFactBalance.setBalanceResult(searchHolderAccountBalanceTO.getTransferAmount());
		}

		if(searchHolderAccountBalanceTO.getTransferMarketFactDetail()!=null){
			for(SearchTransferMarketFactTO transferMarketFact: searchHolderAccountBalanceTO.getTransferMarketFactDetail()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(transferMarketFact.getIdHolderMarketPk());
				marketDetail.setEnteredBalance(transferMarketFact.getTransferAmount());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}
		
		marketFactBalance.setIndLonableBalance(new Integer(1));
		marketFactBalance.setLonableOperationId(searchHolderAccountBalanceTO.getIdLoanableSecuritiesOperationPk());
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 	  	
	}
	
	/**
	 * before Save Loanable Securities Operation Handler.
	 */
	public void beforeLoanableSecuritiesOperationHandler()
	{
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  if (Validations.validateIsNull(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk()) || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk().intValue())
				  || Validations.validateIsNull(loanableSecuritiesRequestSession.getHolder().getIdHolderPk()) || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolder().getIdHolderPk().intValue())
				  || Validations.validateIsNull(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk()) || !Validations.validateIsPositiveNumber(loanableSecuritiesRequestSession.getHolderAccount().getIdHolderAccountPk().intValue())			 
				  || Validations.validateIsNullOrEmpty(searchHolderAccountBalanceTODataModel) || Validations.validateListIsNullOrEmpty(searchHolderAccountBalanceTODataModel.getDataList())) {			   
			   showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
			   JSFUtilities.showSimpleValidationDialog();
			   return;
			  }			 		  
		 
		  Boolean validate=false;
		  int cont=0;
		  for(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO : searchHolderAccountBalanceTODataModel.getDataList())
		  {				
			  if(searchHolderAccountBalanceTO.getSelected())
			  {		
				  cont++;
				  if(searchHolderAccountBalanceTO.getMarketLock())//mas de un hecho de mercado
				  {
					  if(Validations.validateIsNullOrEmpty(searchHolderAccountBalanceTO.getTransferAmount()))
					  {
						  validate=Boolean.TRUE;
						  break;
					  }
				  }
				  else //un solo hecho de mercado
				  {
					  if(Validations.validateIsNullOrEmpty(searchHolderAccountBalanceTO.getTransferAmount()))
					  {
						  validate=Boolean.TRUE;
						  break;
					  }
					  else 
					  {
						  if(Validations.validateIsNullOrEmpty(marketFactBalanceHelpTO))
							  validate = selectMarketFactBalance(searchHolderAccountBalanceTO);					 
					  }
				  }
			  }
		  }		  
		  
		  if(validate || cont==0)
		  {			 
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));			 
			   JSFUtilities.showSimpleValidationDialog();
			   return;
		  }
		  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_REGISTER_RETIREMENT));	
		  JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
	}
	/**
	 * Save Loanable Securities Operation handler.
	 */
	@LoggerAuditWeb
	public void saveLoanableSecuritiesOperationHandler()
	{
		LoanableSecuritiesRequest loanableSecuritiesRequest=null;	
		loanableSecuritiesRequestSession.setRequestType(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode());
		loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.REGISTERED.getCode());
		List <LoanableUnblockOperation> lstLoanableUnblockOperation = new ArrayList<LoanableUnblockOperation>();
		for(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO : searchHolderAccountBalanceTODataModel.getDataList())
		{
			if(searchHolderAccountBalanceTO.getSelected())
			{
				LoanableUnblockOperation  loanableUnblockOperation = new LoanableUnblockOperation();	
				loanableUnblockOperation.setUnblockMotive(new Long(LoanableUnblockMotiveType.MANUAL_PROCESS.getCode()));
				loanableUnblockOperation.setCurrentQuantity(searchHolderAccountBalanceTO.getLoanableBalance());
				loanableUnblockOperation.setUnblockQuantity(searchHolderAccountBalanceTO.getTransferAmount());	
				LoanableSecuritiesOperation loanableSecuritiesOperation = new LoanableSecuritiesOperation();
				loanableSecuritiesOperation.setIdLoanableSecuritiesOperationPk(searchHolderAccountBalanceTO.getIdLoanableSecuritiesOperationPk());
				loanableUnblockOperation.setLoanableSecuritiesOperation(loanableSecuritiesOperation);
				
				List <LoanableUnblockMarketfact> lstLoanableSecuritiesMarketfact=new ArrayList<LoanableUnblockMarketfact>();
				for (SearchTransferMarketFactTO searchTransferMarketFactTO : searchHolderAccountBalanceTO.getTransferMarketFactDetail())
				{
					LoanableUnblockMarketfact loanableUnblockMarketfact = new LoanableUnblockMarketfact();
					loanableUnblockMarketfact.setMarketDate(searchTransferMarketFactTO.getMarketDate());
					loanableUnblockMarketfact.setMarketPrice(searchTransferMarketFactTO.getMarketPrice());
					loanableUnblockMarketfact.setMarketQuantity(searchTransferMarketFactTO.getTransferAmount());
					loanableUnblockMarketfact.setMarketRate(searchTransferMarketFactTO.getMarketRate());
					lstLoanableSecuritiesMarketfact.add(loanableUnblockMarketfact);
				}
				loanableUnblockOperation.setLstLoanableUnblockMarketfact(lstLoanableSecuritiesMarketfact);
				lstLoanableUnblockOperation.add(loanableUnblockOperation);
			}
		}	
		loanableSecuritiesRequestSession.setLstLoanableUnblockOperation(lstLoanableUnblockOperation);	
				
		try {					
			loanableSecuritiesRequest=manageLoanableSecuritiesOperationFacade.registryRetirementLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession);
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
				cleanLoanableSecuritiesOperationSearchHandler(null);
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequest)){
			setViewOperationType(ViewOperationsType.CONSULT.getCode());	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_REGISTER_RETIREMENT_OK,
				    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );						
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}	
		cleanLoanableSecuritiesOperationSearchHandler(null);
	}
	
	/**
	 * Select MarketFact Balance Bean.
	 *
	 * @param searchHolderAccountBalanceTO object search Holder Account Balance TO
	 * @return the boolean
	 */
	public Boolean selectMarketFactBalance(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO){		
		for(SearchHolderAccountBalanceTO habh: searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO()){
				if(habh.getIdParticipantPk().equals(searchHolderAccountBalanceTO.getIdParticipantPk()) &&
				   habh.getIdHolderAccountPk().equals(searchHolderAccountBalanceTO.getIdHolderAccountPk()) &&
				   habh.getIdSecurityCodePk().equals(searchHolderAccountBalanceTO.getIdSecurityCodePk())){
					
					if(!searchHolderAccountBalanceTO.getTransferAmount().equals(BigDecimal.ZERO)){
						habh.setTransferAmount(searchHolderAccountBalanceTO.getTransferAmount());
						habh.setTransferMarketFactDetail(new ArrayList<SearchTransferMarketFactTO>());
						
					  HolderBalanceMovementsTO holderBalanceMovementsTOAux = new HolderBalanceMovementsTO();
					  holderBalanceMovementsTOAux.setObjParticipant(loanableSecuritiesRequestSession.getParticipant());
					  holderBalanceMovementsTOAux.setAccountNumber(Integer.valueOf(searchHolderAccountBalanceTO.getIdHolderAccountPk().toString()));					  
					  holderBalanceMovementsTOAux.setIdIsinCodePk(searchHolderAccountBalanceTO.getIdSecurityCodePk());
					
					  try {
						  List<HolderMarketFactBalance> lstHolderMarketFacBalance = manageLoanableSecuritiesOperationFacade.getHolderMarketFacBalanceFilterServiceBean(holderBalanceMovementsTOAux);
						  if(Validations.validateListIsNotNullAndNotEmpty(lstHolderMarketFacBalance))
						  {							 
							  for(HolderMarketFactBalance holderMarketFactBalance : lstHolderMarketFacBalance)
							  {
								  SearchTransferMarketFactTO searchTransferMarketFactTO = new SearchTransferMarketFactTO();
								  searchTransferMarketFactTO.setIdHolderMarketPk(holderMarketFactBalance.getIdMarketfactBalancePk());
								  searchTransferMarketFactTO.setMarketDate(holderMarketFactBalance.getMarketDate());
								  searchTransferMarketFactTO.setMarketPrice(holderMarketFactBalance.getMarketPrice());
								  searchTransferMarketFactTO.setMarketRate(holderMarketFactBalance.getMarketRate());
								  searchTransferMarketFactTO.setTransferAmount(searchHolderAccountBalanceTO.getTransferAmount());
								  habh.getTransferMarketFactDetail().add(searchTransferMarketFactTO);
							  }							  
						  }
						  else
						  {
							  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));			
							   JSFUtilities.showSimpleValidationDialog();
							   return Boolean.TRUE;
						  }
					  } catch (Exception e) {
					   e.printStackTrace();
					  }														
						validateAmountTransferForBean(habh);
					}else{
						clearQuantitiesTransfer(habh);
					}				
				}			
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Clean Holder Account Balances Handler.
	 *
	 * @param e the e
	 */
	public void cleanHolderAccountBalancesHandler(ActionEvent e)
	{
		executeAction();
		loanableSecuritiesRequestSession = new LoanableSecuritiesRequest();
		if(!userInfo.getUserAccountSession().isParticipantInstitucion())
			loanableSecuritiesRequestSession.setParticipant(new Participant());
		loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		loanableSecuritiesRequestSession.setHolder(new Holder());
		cleanModelRegister();
		lstHolderAccount = new ArrayList<HolderAccount>();
		marketFactBalanceHelpTO=null;
	}
	
	/**
	 * Select MarketFact Balance Bean.
	 */
	public void selectMarketFactBalanceBean(){
		MarketFactBalanceHelpTO marketFactBalance = this.marketFactBalanceHelpTO;
		for(SearchHolderAccountBalanceTO habh: searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO()){
				if(habh.getIdParticipantPk().equals(marketFactBalance.getParticipantPk()) &&
				   habh.getIdHolderAccountPk().equals(marketFactBalance.getHolderAccountPk()) &&
				   habh.getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk()) && 
				   habh.getIdLoanableSecuritiesOperationPk().equals(marketFactBalance.getLonableOperationId())){
					
					if(!marketFactBalance.getBalanceResult().equals(BigDecimal.ZERO)){
						habh.setTransferAmount(marketFactBalance.getBalanceResult());
						habh.setTransferMarketFactDetail(new ArrayList<SearchTransferMarketFactTO>());
						
						for(MarketFactDetailHelpTO mkfDetTO: marketFactBalance.getMarketFacBalances()){
							SearchTransferMarketFactTO marketFactTO = new SearchTransferMarketFactTO();
							marketFactTO.setIdHolderMarketPk(mkfDetTO.getMarketFactBalancePk());
							marketFactTO.setTransferAmount(mkfDetTO.getEnteredBalance());
							marketFactTO.setMarketDate(mkfDetTO.getMarketDate());
							marketFactTO.setMarketPrice(mkfDetTO.getMarketPrice());
							marketFactTO.setMarketRate(mkfDetTO.getMarketRate());
							habh.getTransferMarketFactDetail().add(marketFactTO);
						}
						validateAmountTransferForBean(habh);
					}else{
						clearQuantitiesTransfer(habh);
					}
					JSFUtilities.updateComponent("frmLoanableSecuritiesOperation:opnlResult");
					return;
				}			
		}
	}
	
	/**
	 * Validate Amount Transfer .
	 *
	 * @param objHolAccBalTO object for Holder Account Balance
	 */
	public void validateAmountTransferForBean(SearchHolderAccountBalanceTO objHolAccBalTO){
		
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getTransferAmount())){	
			BigDecimal quantityTransfer = objHolAccBalTO.getTransferAmount();			
			BigDecimal loanable = objHolAccBalTO.getLoanableBalance();
			if(loanable.compareTo(quantityTransfer) >= 0){
				objHolAccBalTO.setTransferAmount(quantityTransfer);					
			}else{						
				clearQuantitiesTransfer(objHolAccBalTO);
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_TO_PAY_BALANCE_LOANABLE_BALANCE));	
				JSFUtilities.showSimpleValidationDialog();
			}						
		}else{			
			clearQuantitiesTransfer(objHolAccBalTO);
		}
	}
	/**
	 * Clear quantities blocks.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	private void clearQuantitiesTransfer(SearchHolderAccountBalanceTO objHolAccBalTO){
		objHolAccBalTO.setTransferAmount(null);
	}
	
	/**
	 * Validate Holder Handler.
	 */
	public void validateHolderHandlerSearch() {  
		  executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		  cleanDataModel();
		  if (searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk() == null || !Validations.validateIsPositiveNumber(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk().intValue())) {
			  searchLoanableSecuritiesOperationTO.getCUIHolder().setIdHolderPk(null);
//		   showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND));		  
//		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  // Get holder from database.
		  Holder holder = null;
		  try {
		   holder = manageLoanableSecuritiesOperationFacade.getHolderServiceFacade(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk());
		   holder.setFullName(holder.getDescriptionHolder());
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holder)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_CUI_NOT_FOUND) );		   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER) );			  	   
		   loanableSecuritiesRequestSession.setHolder(new Holder());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  searchLoanableSecuritiesOperationTO.setCUIHolder(holder);

		 }
	
	/**
	 * Validate Holder Account Hanlder.
	 */
	public void validateHolderAccountHanlderSearch()
	{
		executeAction();
		  JSFUtilities.hideGeneralDialogues();
		  cleanDataModel();
		  if (searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk() == null || !Validations.validateIsPositiveNumber(searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk().intValue())) {
			this.searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));	
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }		  
		  HolderAccountTO holderAccountTO = new HolderAccountTO();
		  holderAccountTO.setParticipantTO(searchLoanableSecuritiesOperationTO.getIdParticipant());
		  holderAccountTO.setIdHolderPk(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk());
		  holderAccountTO.setHolderAccountNumber(searchLoanableSecuritiesOperationTO.getHolderAccount().getAccountNumber());
		  // Get holder from database.
		  HolderAccount holderAccount = null;
		  try {
			  holderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountComponentServiceBean(holderAccountTO);
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  if (Validations.validateIsNull(holderAccount)) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND));		 
		   this.searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_ACCOUNT_NOT_REGISTER));			 
		   this.searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		  }
		  
		  searchLoanableSecuritiesOperationTO.setHolderAccount(holderAccount);
	}
	
	/**
	 * Search Loanable Securities Operation Handler.
	 */
	@LoggerAuditWeb
	public void searchLoanableSecuritiesOperationHandler()
	{
		JSFUtilities.hideGeneralDialogues();  
		if(Validations.validateIsNullOrNotPositive(searchLoanableSecuritiesOperationTO.getIdParticipant()))
		{
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_NOT_FOUND));	
		   JSFUtilities.showSimpleValidationDialog();
		   return;
		}
		List <LoanableSecuritiesRequest> lstLoanableSecuritiesRequest=null;
		try{
			searchLoanableSecuritiesOperationTO.setRequestType(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode());
			lstLoanableSecuritiesRequest =  manageLoanableSecuritiesOperationFacade.getListLoanableSecuritiesRequestFilter(searchLoanableSecuritiesOperationTO);
			searchLoanableSecuritiesRequestModel = new GenericDataModel<LoanableSecuritiesRequest>(lstLoanableSecuritiesRequest);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	/**
	 * Clean placement segment search handler.
	 *
	 * @param e the e
	 */
	public void cleanLoanableSecuritiesOperationSearchHandler(ActionEvent e){
		executeAction();
		searchLoanableSecuritiesOperationTO.setInitialDate(CommonsUtilities.currentDate());
		searchLoanableSecuritiesOperationTO.setFinalDate(CommonsUtilities.currentDate());
		if(!userInfo.getUserAccountSession().isParticipantInstitucion())
			searchLoanableSecuritiesOperationTO.setIdParticipant(null);
		searchLoanableSecuritiesOperationTO.setIdState(null);
		searchLoanableSecuritiesOperationTO.setRequestNumber(null);
		searchLoanableSecuritiesOperationTO.setCUIHolder(new Holder());
		searchLoanableSecuritiesOperationTO.setHolderAccount(new HolderAccount());
		searchLoanableSecuritiesOperationTO.setRequestType(null);
		searchLoanableSecuritiesOperationTO.setLstSearchHolderAccountBalanceTO(new ArrayList<SearchHolderAccountBalanceTO>());
		cleanDataModel();
	}
	/**
	 * Before Confirm action.
	 *
	 * @return the string
	 */
	public String beforeConfirmAction(){

		LoanableSecuritiesRequest loanableSecuritiesRequest = this.getLoanableSecuritiesRequestSession(); 
		executeAction();
		if(loanableSecuritiesRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(loanableSecuritiesRequest.getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){			
			selectLoanableSecuritiesOperationHandler(loanableSecuritiesRequest);
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			return "loanableRetirementSecuritiesDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
		
	}
	/**
	 * Before Reject action.
	 *
	 * @return the string
	 */
	public String beforeRejectAction(){

		LoanableSecuritiesRequest loanableSecuritiesRequest = this.getLoanableSecuritiesRequestSession(); 
		executeAction();
		if(loanableSecuritiesRequest==null){
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(loanableSecuritiesRequest.getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){			
			selectLoanableSecuritiesOperationHandler(loanableSecuritiesRequest);
			this.setViewOperationType(ViewOperationsType.REJECT.getCode());
			return "loanableRetirementSecuritiesDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
		JSFUtilities.showSimpleValidationDialog();
		return "";
		
	}
	
	/**
	 * Select Loanable Securities Operation Handler.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 */
	public void selectLoanableSecuritiesOperationHandler(LoanableSecuritiesRequest loanableSecuritiesRequest)
	{
		executeAction();
		cleanModelRegister();
		SearchLoanableSecuritiesOperationTO objSearchLoanableSecuritiesOperationTO = new SearchLoanableSecuritiesOperationTO();
		objSearchLoanableSecuritiesOperationTO.setRequestNumber(loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());		
		LoanableSecuritiesRequest tempLoanableSecuritiesRequest=null;
		createObjectRegister();
		try{
			tempLoanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.getLoanableSecuritiesRequestFilter(objSearchLoanableSecuritiesOperationTO);
			setViewOperationType(ViewOperationsType.REGISTER.getCode());//for load participant
			loadAllParticipant();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}		
		setLoanableSecuritiesRequestSession(tempLoanableSecuritiesRequest);	
		
		List<SearchHolderAccountBalanceTO> lstSearchHolderAccountBalanceTO = new ArrayList<SearchHolderAccountBalanceTO>();
		  SearchHolderAccountBalanceTO searchHolderAccountBalanceTO = null;
		  List<HolderAccountBalanceTO> lstHolAccBalTemp = new ArrayList<HolderAccountBalanceTO>();
		  HolderAccountBalanceTO holderAccountBalanceTO=null;	
		  if(tempLoanableSecuritiesRequest!=null){
			  for( HolderAccountBalance holderAccountBalanceAux : tempLoanableSecuritiesRequest.getHolderAccount().getHolderAccountBalances())			  {
			  	  
				  searchHolderAccountBalanceTO = new SearchHolderAccountBalanceTO();
				  holderAccountBalanceTO = new HolderAccountBalanceTO();
				  
				  searchHolderAccountBalanceTO.setId(holderAccountBalanceAux.getId().toString());
				  searchHolderAccountBalanceTO.setIdParticipantPk(holderAccountBalanceAux.getId().getIdParticipantPk());
				  searchHolderAccountBalanceTO.setIdHolderAccountPk(holderAccountBalanceAux.getId().getIdHolderAccountPk());
				  searchHolderAccountBalanceTO.setIdSecurityCodePk(holderAccountBalanceAux.getId().getIdSecurityCodePk());
				  searchHolderAccountBalanceTO.setTotalBalance(holderAccountBalanceAux.getTotalBalance());
				  searchHolderAccountBalanceTO.setLoanableBalance(holderAccountBalanceAux.getLoanableBalance());	
				  searchHolderAccountBalanceTO.setTransferAmount(holderAccountBalanceAux.getTransferAmmount());
				  
				  List<SearchTransferMarketFactTO> lsttransferMarketFactDetail= new ArrayList<SearchTransferMarketFactTO>();
				  for(HolderMarketFactBalance holderMarketFactBalance : holderAccountBalanceAux.getHolderMarketfactBalance())
				  {
					  SearchTransferMarketFactTO searchTransferMarketFactTO = new SearchTransferMarketFactTO();
					  searchTransferMarketFactTO.setMarketDate(holderMarketFactBalance.getMarketDate());
					  searchTransferMarketFactTO.setMarketPrice(holderMarketFactBalance.getMarketPrice());
					  searchTransferMarketFactTO.setMarketRate(holderMarketFactBalance.getMarketRate());
					  searchTransferMarketFactTO.setTotalBalance(holderMarketFactBalance.getTotalBalance());
					  searchTransferMarketFactTO.setAvailableBalance(holderMarketFactBalance.getAvailableBalance());
					  searchTransferMarketFactTO.setLoanableBalance(holderMarketFactBalance.getLoanableBalance());
					  searchTransferMarketFactTO.setTransferAmount(holderMarketFactBalance.getTransferAmmount());
					  lsttransferMarketFactDetail.add(searchTransferMarketFactTO);
				  }
				  searchHolderAccountBalanceTO.setTransferMarketFactDetail(lsttransferMarketFactDetail);	
				  
				  lstSearchHolderAccountBalanceTO.add(searchHolderAccountBalanceTO);				 
				  
				  holderAccountBalanceTO.setIdHolderAccount(holderAccountBalanceAux.getId().getIdHolderAccountPk());
				  holderAccountBalanceTO.setIdParticipant(holderAccountBalanceAux.getId().getIdParticipantPk());
				  holderAccountBalanceTO.setIdSecurityCode(holderAccountBalanceAux.getId().getIdSecurityCodePk());
				  lstHolAccBalTemp.add(holderAccountBalanceTO);
			  }
		  }
				  
		  searchLoanableSecuritiesOperationTOHeader.setLstSearchHolderAccountBalanceTO(lstSearchHolderAccountBalanceTO);
		  
		 
		  searchHolderAccountBalanceTODataModel = new GenericDataModel<SearchHolderAccountBalanceTO>(searchLoanableSecuritiesOperationTOHeader.getLstSearchHolderAccountBalanceTO());
		  
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Show MarketFact UI Detail.
	 *
	 * @param searchHolderAccountBalanceTO object  Holder Account Balance TO
	 */
public void showMarketFactUIDetail(SearchHolderAccountBalanceTO searchHolderAccountBalanceTO){
		try{
	    marketFactBalanceDetail = new MarketFactBalanceHelpTO();		
		Security security = manageLoanableSecuritiesOperationFacade.findSecurityById(searchHolderAccountBalanceTO.getIdSecurityCodePk());
		  marketFactBalanceDetail.setSecurityCodePk(security.getIdSecurityCodePk());	  
		  marketFactBalanceDetail.setSecurityDescription(security.getDescription());
		  marketFactBalanceDetail.setInstrumentDescription(security.getInstrumentTypeDescription());
		  marketFactBalanceDetail.setInstrumentType(security.getInstrumentType());
		  marketFactBalanceDetail.setHolderAccountPk(searchHolderAccountBalanceTO.getIdHolderAccountPk());
		  marketFactBalanceDetail.setParticipantPk(searchHolderAccountBalanceTO.getIdParticipantPk());
		  marketFactBalanceDetail.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		  marketFactBalanceDetail.setBalanceResult(searchHolderAccountBalanceTO.getTransferAmount());	
		  
		  if(searchHolderAccountBalanceTO.getTransferMarketFactDetail()!=null){
				for(SearchTransferMarketFactTO transferMarketFact: searchHolderAccountBalanceTO.getTransferMarketFactDetail()){
					MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
					marketDetail.setMarketFactBalancePk(transferMarketFact.getIdHolderMarketPk());				
					marketDetail.setMarketDate(transferMarketFact.getMarketDate());
					if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
					{
						marketDetail.setMarketRate(transferMarketFact.getMarketRate());
						if(Validations.validateIsNotNullAndNotEmpty(marketFactBalanceDetail.getLastMarketDate()))
						{
							if(marketDetail.getMarketDate().compareTo(marketFactBalanceDetail.getLastMarketDate())==-1);
							else {
								marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
								marketFactBalanceDetail.setLastMarketRate(marketDetail.getMarketRate());
							}							
						}else {
							marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
							marketFactBalanceDetail.setLastMarketRate(marketDetail.getMarketRate());
						}					
					}
					if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
					{
						marketDetail.setMarketPrice(transferMarketFact.getMarketPrice());
						if(Validations.validateIsNotNullAndNotEmpty(marketFactBalanceDetail.getLastMarketDate()))
						{
							if(marketDetail.getMarketDate().compareTo(marketFactBalanceDetail.getLastMarketDate())==-1);
							else {
								marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
								marketFactBalanceDetail.setLastMarketPrice(marketDetail.getMarketPrice());
							}							
						}else {
							marketFactBalanceDetail.setLastMarketDate(marketDetail.getMarketDate());
							marketFactBalanceDetail.setLastMarketPrice(marketDetail.getMarketPrice());
						}		
					}
					marketDetail.setTotalBalance(transferMarketFact.getTotalBalance());
					marketDetail.setAvailableBalance(transferMarketFact.getAvailableBalance());
					marketDetail.setLoanableBalance(transferMarketFact.getLoanableBalance());
					marketDetail.setEnteredBalance(transferMarketFact.getTransferAmount());
					marketFactBalanceDetail.getMarketFacBalances().add(marketDetail);
				}
			}
			
			JSFUtilities.executeJavascriptFunction("PF('dialogWMarketfactBalance').show()"); 	
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
/**
 * Before reject handler.
 */
public void beforeRejectHandler()
{
	executeAction();
	JSFUtilities.hideGeneralDialogues();  
	if(this.getLoanableSecuritiesRequestSession()==null){
		 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
		JSFUtilities.showSimpleValidationDialog();
		return;
	}
	if(this.getLoanableSecuritiesRequestSession().getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_LOANABLE_SECURITIES_REJECT,
			    		  new Object[]{this.getLoanableSecuritiesRequestSession().getIdLoanableSecuritiesRequestPk()}) );	
		setViewOperationType(ViewOperationsType.REJECT.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
		return;
	}
	showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
			, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
	JSFUtilities.showSimpleValidationDialog();
}
/**
 * Before confirm handler.
 */
public void beforeConfirmHandler()
{
	executeAction();
	if(this.getLoanableSecuritiesRequestSession()==null){
		 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_ERROR_RECORD_REQUIRED));	
		JSFUtilities.showSimpleValidationDialog();
		return;
	}
	if(isInsufficientBalanceLoanableOperation())
	{
		 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_INSUFFFICIENT_BALANCE_LOANABLE_OPERATION));	
		JSFUtilities.showSimpleValidationDialog();
		return;
	}
	if(this.getLoanableSecuritiesRequestSession().getRequestState().equals(LoanableSecuritiesOperationStateType.REGISTERED.getCode())){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_LOANABLE_SECURITIES_CONFIRM,
			    		  new Object[]{this.getLoanableSecuritiesRequestSession().getIdLoanableSecuritiesRequestPk()}) );			
		this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
		return;
	}
	showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
			, PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOANABLE_SECURITIES_VALIDATION_REGISTERED));	
	JSFUtilities.showSimpleValidationDialog();
}

/**
 * Is Insufficient Balance Loanable Operation.
 *
 * @return validate true or false
 */
public Boolean isInsufficientBalanceLoanableOperation()
{
	try{
	return manageLoanableSecuritiesOperationFacade.isInsufficientBalanceLoanableOperationServiceFacade(loanableSecuritiesRequestSession);
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		if(e.getErrorService()!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
			cleanLoanableSecuritiesOperationSearchHandler(null);
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		}else{
			excepcion.fire( new ExceptionToCatchEvent(e));
		}
		return Boolean.TRUE;
	}
}
/**
 * Change action loanable securities request handler.
 */
@LoggerAuditWeb
public void processOperationHandler(){
	switch(ViewOperationsType.lookup.get(getViewOperationType())){		
		case CONFIRM : confirmLoanableSecuritiesRequest();break;
		case REJECT: rejectLoanableSecuritiesRequest();break;
	}
	cleanLoanableSecuritiesOperationSearchHandler(null);
}
/**
 * Confirm loanable securities request.
 */
@LoggerAuditWeb
private void confirmLoanableSecuritiesRequest() {
	executeAction();
	JSFUtilities.hideGeneralDialogues();  
	loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.CONFIRMED.getCode());
			
	LoanableSecuritiesRequest loanableSecuritiesRequest = null;
	try {
		loanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.confirmRetirementLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession,Boolean.FALSE);
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		if(e.getErrorService()!=null){
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		}else{
			excepcion.fire( new ExceptionToCatchEvent(e));
		}
		return;
	}
	JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
	if(loanableSecuritiesRequest!=null){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_LOANABLE_SECURITIES_CONFIRM_OK,
			    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );				
		JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
	}
	setViewOperationType(ViewOperationsType.CONSULT.getCode());
}
/**
 * Reject LoanableSecurities request.
 */
@LoggerAuditWeb
private void rejectLoanableSecuritiesRequest() {
	executeAction();
	JSFUtilities.hideGeneralDialogues();  
	loanableSecuritiesRequestSession.setRequestState(LoanableSecuritiesOperationStateType.REJECTED.getCode());
	loanableSecuritiesRequestSession.setAnnulmentMotive(Integer.parseInt(""+motiveControllerReject.getIdMotivePK()));
	loanableSecuritiesRequestSession.setAnnulmentOtherMotive(motiveControllerReject.getMotiveText());
	
	LoanableSecuritiesRequest loanableSecuritiesRequest = null;
	try {
		loanableSecuritiesRequest = manageLoanableSecuritiesOperationFacade.rejectRetirementLoanableSecuritiesRequestServiceFacade(loanableSecuritiesRequestSession);
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		if(e.getErrorService()!=null){
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");	
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		}else{
			excepcion.fire( new ExceptionToCatchEvent(e));
		}
		return;
	}
	JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').hide()");
	if(loanableSecuritiesRequest!=null){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
			      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_LOANABLE_SECURITIES_REJECT_OK,
			    		  new Object[]{loanableSecuritiesRequest.getHolderAccount().getAccountNumber().toString()}) );				
		JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
	}
	setViewOperationType(ViewOperationsType.CONSULT.getCode());
}
/**
 * Change motive Reject handler.
 */
public void changeMotiveRejectHandler(){
	if(this.getMotiveControllerReject().getIdMotivePK()==Long.parseLong(LoanableSecuritiesOperationMotiveRejectRetirementType.OTHER.getCode().toString())){
		this.getMotiveControllerReject().setShowMotiveText(Boolean.TRUE);
	}else{
		this.getMotiveControllerReject().setShowMotiveText(Boolean.FALSE);
	}
}
/**
 * Confirm motive Reject handler.
 */
public void confirmMotiveRejectHandler(){
	executeAction();
	JSFUtilities.hideGeneralDialogues();  
	if(motiveControllerReject.getIdMotivePK()==Long.parseLong(LoanableSecuritiesOperationMotiveRejectRetirementType.OTHER.getCode().toString())){
		if(Validations.validateIsNullOrEmpty(this.getMotiveControllerReject().getMotiveText())){
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').show()");
			return;
		}
	}
	JSFUtilities.executeJavascriptFunction("PF('dialogWMotiveReject').hide();");
	showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
		      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_LOANABLE_SECURITIES_REJECT,
		    		  new Object[]{this.getLoanableSecuritiesRequestSession().getIdLoanableSecuritiesRequestPk()}) );			
	JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
}
	
	/**
	 * Select Participant Handler.
	 */
	public void selectParticipantHandler()
	{
		loanableSecuritiesRequestSession.setHolderAccount(new HolderAccount());
		cleanModelRegister();
		lstHolderAccount= new ArrayList<HolderAccount>();
		 if(Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk())
				 && Validations.validateIsNotNullAndNotEmpty(loanableSecuritiesRequestSession.getHolder().getIdHolderPk())){
			  HolderAccountTO holderAccountTO = new HolderAccountTO();
			  holderAccountTO.setParticipantTO(loanableSecuritiesRequestSession.getParticipant().getIdParticipantPk());
			  holderAccountTO.setIdHolderPk(loanableSecuritiesRequestSession.getHolder().getIdHolderPk());
			  try{
			  lstHolderAccount = manageLoanableSecuritiesOperationFacade.getHolderAccountListForParticipantAndHolder(holderAccountTO);
			  } catch (Exception e) {
				   e.printStackTrace();
				  }
		  }
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		searchLoanableSecuritiesRequestModel = null;
	}
	
	/**
	 * Gets the motive controller reject.
	 *
	 * @return the motive controller reject
	 */
	public MotiveController getMotiveControllerReject() {
		return motiveControllerReject;
	}
	
	/**
	 * Sets the motive controller reject.
	 *
	 * @param motiveControllerReject the new motive controller reject
	 */
	public void setMotiveControllerReject(MotiveController motiveControllerReject) {
		this.motiveControllerReject = motiveControllerReject;
	}
	
	/**
	 * Gets the lst all participant.
	 *
	 * @return the lst all participant
	 */
	public List<Participant> getLstAllParticipant() {
		return lstAllParticipant;
	}
	
	/**
	 * Sets the lst all participant.
	 *
	 * @param lstAllParticipant the new lst all participant
	 */
	public void setLstAllParticipant(List<Participant> lstAllParticipant) {
		this.lstAllParticipant = lstAllParticipant;
	}
	
	/**
	 * Gets the search loanable securities operation to.
	 *
	 * @return the search loanable securities operation to
	 */
	public SearchLoanableSecuritiesOperationTO getSearchLoanableSecuritiesOperationTO() {
		return searchLoanableSecuritiesOperationTO;
	}
	
	/**
	 * Sets the search loanable securities operation to.
	 *
	 * @param searchLoanableSecuritiesOperationTO the new search loanable securities operation to
	 */
	public void setSearchLoanableSecuritiesOperationTO(
			SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO) {
		this.searchLoanableSecuritiesOperationTO = searchLoanableSecuritiesOperationTO;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Gets the search loanable securities request model.
	 *
	 * @return the search loanable securities request model
	 */
	public GenericDataModel<LoanableSecuritiesRequest> getSearchLoanableSecuritiesRequestModel() {
		return searchLoanableSecuritiesRequestModel;
	}
	
	/**
	 * Sets the search loanable securities request model.
	 *
	 * @param searchLoanableSecuritiesRequestModel the new search loanable securities request model
	 */
	public void setSearchLoanableSecuritiesRequestModel(
			GenericDataModel<LoanableSecuritiesRequest> searchLoanableSecuritiesRequestModel) {
		this.searchLoanableSecuritiesRequestModel = searchLoanableSecuritiesRequestModel;
	}
	
	/**
	 * Gets the loanable securities request session.
	 *
	 * @return the loanable securities request session
	 */
	public LoanableSecuritiesRequest getLoanableSecuritiesRequestSession() {
		return loanableSecuritiesRequestSession;
	}
	
	/**
	 * Sets the loanable securities request session.
	 *
	 * @param loanableSecuritiesRequestSession the new loanable securities request session
	 */
	public void setLoanableSecuritiesRequestSession(
			LoanableSecuritiesRequest loanableSecuritiesRequestSession) {
		this.loanableSecuritiesRequestSession = loanableSecuritiesRequestSession;
	}
	
	/**
	 * Gets the search holder account balance to data model.
	 *
	 * @return the search holder account balance to data model
	 */
	public GenericDataModel<SearchHolderAccountBalanceTO> getSearchHolderAccountBalanceTODataModel() {
		return searchHolderAccountBalanceTODataModel;
	}
	
	/**
	 * Sets the search holder account balance to data model.
	 *
	 * @param searchHolderAccountBalanceTODataModel the new search holder account balance to data model
	 */
	public void setSearchHolderAccountBalanceTODataModel(
			GenericDataModel<SearchHolderAccountBalanceTO> searchHolderAccountBalanceTODataModel) {
		this.searchHolderAccountBalanceTODataModel = searchHolderAccountBalanceTODataModel;
	}
	
	/**
	 * Gets the search loanable securities operation to header.
	 *
	 * @return the search loanable securities operation to header
	 */
	public SearchLoanableSecuritiesOperationTO getSearchLoanableSecuritiesOperationTOHeader() {
		return searchLoanableSecuritiesOperationTOHeader;
	}
	
	/**
	 * Sets the search loanable securities operation to header.
	 *
	 * @param searchLoanableSecuritiesOperationTOHeader the new search loanable securities operation to header
	 */
	public void setSearchLoanableSecuritiesOperationTOHeader(
			SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTOHeader) {
		this.searchLoanableSecuritiesOperationTOHeader = searchLoanableSecuritiesOperationTOHeader;
	}
	
	/**
	 * Gets the loanable securities operation session.
	 *
	 * @return the loanable securities operation session
	 */
	public LoanableSecuritiesOperation getLoanableSecuritiesOperationSession() {
		return loanableSecuritiesOperationSession;
	}
	
	/**
	 * Sets the loanable securities operation session.
	 *
	 * @param loanableSecuritiesOperationSession the new loanable securities operation session
	 */
	public void setLoanableSecuritiesOperationSession(
			LoanableSecuritiesOperation loanableSecuritiesOperationSession) {
		this.loanableSecuritiesOperationSession = loanableSecuritiesOperationSession;
	}
	
	/**
	 * Gets the market fact balance help to.
	 *
	 * @return the market fact balance help to
	 */
	public MarketFactBalanceHelpTO getMarketFactBalanceHelpTO() {
		return marketFactBalanceHelpTO;
	}
	
	/**
	 * Sets the market fact balance help to.
	 *
	 * @param marketFactBalanceHelpTO the new market fact balance help to
	 */
	public void setMarketFactBalanceHelpTO(
			MarketFactBalanceHelpTO marketFactBalanceHelpTO) {
		this.marketFactBalanceHelpTO = marketFactBalanceHelpTO;
	}
	
	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	/**
	 * Test batch process.
	 */
	@LoggerAuditWeb
	public void testBatchProcess(){
		JSFUtilities.hideGeneralDialogues();  
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.LOANABLE_SECURITIES_AUTOMATIC_RETIREMENT.getCode());
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, new HashMap<String, Object>());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		JSFUtilities.executeJavascriptFunction("PF('batchWOk').show()");
	}
	
	/**
	 * Gets the market fact balance detail.
	 *
	 * @return the market fact balance detail
	 */
	public MarketFactBalanceHelpTO getMarketFactBalanceDetail() {
		return marketFactBalanceDetail;
	}
	
	/**
	 * Sets the market fact balance detail.
	 *
	 * @param marketFactBalanceDetail the new market fact balance detail
	 */
	public void setMarketFactBalanceDetail(
			MarketFactBalanceHelpTO marketFactBalanceDetail) {
		this.marketFactBalanceDetail = marketFactBalanceDetail;
	}
	
	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}
	
	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	
}
