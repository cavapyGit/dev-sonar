package com.pradera.negotiations.loanablesecuritiesoperation.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.HolderBalanceMovementsTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.LoanableSecuritiesMarketfact;
import com.pradera.model.negotiation.LoanableSecuritiesOperation;
import com.pradera.model.negotiation.LoanableSecuritiesRequest;
import com.pradera.model.negotiation.LoanableUnblockMarketfact;
import com.pradera.model.negotiation.LoanableUnblockOperation;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationRequestType;
import com.pradera.model.negotiation.type.LoanableSecuritiesOperationStateType;
import com.pradera.model.negotiation.type.LoanableUnblockMotiveType;
import com.pradera.negotiations.loanablesecuritiesoperation.to.SearchLoanableSecuritiesOperationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageLoanableSecuritiesOperationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ManageLoanableSecuritiesOperationServiceBean extends CrudDaoServiceBean{

	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The holiday query service bean. */
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;
	
	/**
	 * Get Holder .
	 *
	 * @param idHolderPk id Holder
	 * @return object holder
	 * @throws ServiceException service exception
	 */
	public Holder getHolderServiceBean(Long idHolderPk) throws ServiceException{
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM Holder h WHERE h.idHolderPk = :idHolderPk");
	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	  
	
		 
	      return (Holder) query.getSingleResult();
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}
  }
	
	/**
	 * Get Holder Account Balance By Filter.
	 *
	 * @param filter object filter
	 * @return List Holder Account Balance
	 * @throws ServiceException service exception
	 */
	public List<HolderAccountBalance> getHolderAccountBalanceByFilterServiceBean(HolderBalanceMovementsTO filter) throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select HAB From HolderAccountBalance HAB ");
			sbQuery.append(" where HAB.participant.idParticipantPk=" + filter.getObjParticipant().getIdParticipantPk());
			sbQuery.append(" and HAB.holderAccount.idHolderAccountPk=" + filter.getAccountNumber());
			sbQuery.append(" and HAB.security.stateSecurity=" + SecurityStateType.REGISTERED.getCode());	
			sbQuery.append(" and HAB.availableBalance > 0");		
					
			Query query = em.createQuery(sbQuery.toString());
		
			return (List<HolderAccountBalance>) query.getResultList();
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}	
	
	/**
	 * Get Loanable securities operation By Filter.
	 *
	 * @param filter object filter
	 * @return List Loanable securities operation
	 * @throws ServiceException service exception
	 */
	public List<LoanableSecuritiesOperation> getLoanableSecuritiesOperationBalanceByFilterServiceBean(SearchLoanableSecuritiesOperationTO filter) throws ServiceException{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select hol.idHolderAccountPk,par.idParticipantPk,sec.idSecurityCodePk,loa.initialQuantity,loa.currentQuantity,loa.idLoanableSecuritiesOperationPk  ");//0,1,2,3,4,5
			sbQuery.append(" From LoanableSecuritiesOperation loa	");
			sbQuery.append(" join loa.holderAccount hol	");
			sbQuery.append(" join loa.participant par	");		
			sbQuery.append(" join loa.security sec	");
			sbQuery.append(" where par.idParticipantPk= :idParticipantPk ");
			parameters.put("idParticipantPk", filter.getIdParticipant());
			sbQuery.append(" and hol.idHolderAccountPk= :idHolderAccountPk ");
			parameters.put("idHolderAccountPk", filter.getHolderAccount().getIdHolderAccountPk());
			sbQuery.append(" and sec.stateSecurity= :stateSecurity ");
			parameters.put("stateSecurity", SecurityStateType.REGISTERED.getCode());
			sbQuery.append(" and loa.operationState= :operationState ");
			parameters.put("operationState", LoanableSecuritiesOperationStateType.CONFIRMED.getCode());
			sbQuery.append(" and loa.currentQuantity > 0");										
			
			List<Object[]> lstLoanableSecuritiesOperation =  findListByQueryString(sbQuery.toString(), parameters);
			Map<Long,LoanableSecuritiesOperation> loanableSecuritiesOperationPK = new HashMap<Long, LoanableSecuritiesOperation>();
			for(Object[] object: lstLoanableSecuritiesOperation)
			{
				LoanableSecuritiesOperation loanableSecuritiesOperation = new LoanableSecuritiesOperation();
				loanableSecuritiesOperation.setIdLoanableSecuritiesOperationPk(new Long(object[5].toString()));
				loanableSecuritiesOperation.setCurrentQuantity(new BigDecimal(object[4].toString()));
				loanableSecuritiesOperation.setInitialQuantity(new BigDecimal(object[3].toString()));
				Security security = new Security();
				security.setIdSecurityCodePk(object[2].toString());
				loanableSecuritiesOperation.setSecurity(security);
				Participant participant = new Participant();
				participant.setIdParticipantPk(new Long(object[1].toString()));
				loanableSecuritiesOperation.setParticipant(participant);
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(new Long(object[0].toString()));
				loanableSecuritiesOperation.setHolderAccount(holderAccount);
				loanableSecuritiesOperationPK.put(loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk(), loanableSecuritiesOperation);
			}
				
			return new ArrayList<LoanableSecuritiesOperation>(loanableSecuritiesOperationPK.values());
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Holder MarketFac Balance Filter.
	 *
	 * @param filter Holder Balance
	 * @return object Holder MarketFact Balance
	 * @throws ServiceException service exception
	 */
	public List<HolderMarketFactBalance>getHolderMarketFacBalanceFilterServiceBean(HolderBalanceMovementsTO filter)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select HAB From HolderMarketFactBalance HAB ");
			sbQuery.append(" where HAB.participant.idParticipantPk=" + filter.getObjParticipant().getIdParticipantPk());
			sbQuery.append(" and HAB.holderAccount.idHolderAccountPk=" + filter.getAccountNumber());
			sbQuery.append(" and HAB.security.idSecurityCodePk='" + filter.getIdIsinCodePk().toString()+"'");		
					
			Query query = em.createQuery(sbQuery.toString());
		
			return (List<HolderMarketFactBalance>) query.getResultList();
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Registry Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest the loanable securities request
	 * @param logger object Logger User
	 * @return object loanable Securities Operation
	 * @throws ServiceException The service exception
	 */
	public LoanableSecuritiesRequest registryLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest, LoggerUser logger) throws ServiceException{	
		try{
		Map<String,Object> param = new HashMap<String,Object>();		
		param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
		Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
		if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
		}		
		
		Map<String,Object> parama = new HashMap<String,Object>();		
		parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
		if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}	
		
		Map<String,Object> paramat= new HashMap<String,Object>();		
		paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
		HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
		if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
		}	
		
		loanableSecuritiesRequest.setRegistryUser(logger.getUserName());
		loanableSecuritiesRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		
		create(loanableSecuritiesRequest);
		
		for(LoanableSecuritiesOperation loanableSecuritiesOperation : loanableSecuritiesRequest.getLstLoanableSecuritiesOperation())
		{
			loanableSecuritiesOperation.setExpirationDate(holidayQueryServiceBean.getCalculateDate(
					loanableSecuritiesOperation.getInitialDate(), loanableSecuritiesOperation.getValidityDays(), 1,null));
			loanableSecuritiesOperation.setLoanableSecuritiesRequest(loanableSecuritiesRequest);
						
			Map<String,Object> paramate = new HashMap<String,Object>();		
			paramate.put("idSecurityCodePkParam", loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
			Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
			if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
			}	
			
			if(!existSecuritiesTVR(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			
			TradeOperation tradeOperation = new TradeOperation();
			tradeOperation.setOperationType(ParameterOperationType.LOANABLE_SECURITIES_REGISTER.getCode());
			tradeOperation.setRegisterUser(logger.getUserName());
			tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
			tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesOperation.getOperationState().toString()));
			create(tradeOperation);
			
			loanableSecuritiesOperation.setTradeOperation(tradeOperation);
			
			create(loanableSecuritiesOperation);
			
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))
			{
				for(LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : loanableSecuritiesOperation.getLstLoanableSecuritiesMarketfact())
				{
					loanableSecuritiesMarketfact.setLoanableSecuritiesOperation(loanableSecuritiesOperation);					
					create(loanableSecuritiesMarketfact);
				}
			}
			
			List<HolderAccountBalanceTO> lstHolderAccountBalanceTO = new ArrayList<HolderAccountBalanceTO>();	
			
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdParticipant(loanableSecuritiesOperation.getParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdHolderAccount(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdSecurityCode(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(loanableSecuritiesOperation.getCurrentQuantity());
			
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))
			{
				List<MarketFactAccountTO> lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();	
				for(LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : loanableSecuritiesOperation.getLstLoanableSecuritiesMarketfact())
				{
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setMarketDate(loanableSecuritiesMarketfact.getMarketDate());
					marketFactAccountTO.setMarketPrice(loanableSecuritiesMarketfact.getMarketPrice());
					marketFactAccountTO.setMarketRate(loanableSecuritiesMarketfact.getMarketRate());
					marketFactAccountTO.setQuantity(loanableSecuritiesMarketfact.getCurrentMarketQuantity());
					lstMarketFactAccounts.add(marketFactAccountTO);
				}
				holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccounts);
			}
						
			lstHolderAccountBalanceTO.add(holderAccountBalanceTO);
						
			AccountsComponentTO  accountsComponentTO = new AccountsComponentTO();
			accountsComponentTO.setIdBusinessProcess(BusinessProcessType.LOANABLE_SECURITIES_REGISTER.getCode());		
			accountsComponentTO.setIdOperationType(ParameterOperationType.LOANABLE_SECURITIES_REGISTER.getCode());			
			accountsComponentTO.setIdTradeOperation(tradeOperation.getIdTradeOperationPk());
			accountsComponentTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
			accountsComponentTO.setLstSourceAccounts(lstHolderAccountBalanceTO);
		
			accountsComponentService.get().executeAccountsComponent(accountsComponentTO);
			
		}
		return loanableSecuritiesRequest;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}		
	}
	
	/**
	 * Registry Retirement Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest the loanable securities request
	 * @param logger object Logger User
	 * @return object loanable Securities Operation
	 * @throws ServiceException The service exception
	 */
	public LoanableSecuritiesRequest registryRetirementLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest, LoggerUser logger) throws ServiceException{	
		try{
		Map<String,Object> param = new HashMap<String,Object>();		
		param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
		Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
		if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
		}		
		
		Map<String,Object> parama = new HashMap<String,Object>();		
		parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
		if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
		}	
		
		Map<String,Object> paramat= new HashMap<String,Object>();		
		paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
		HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
		if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
		}	
		
		loanableSecuritiesRequest.setRegistryUser(logger.getUserName());
		loanableSecuritiesRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		
		create(loanableSecuritiesRequest);
		
		for(LoanableUnblockOperation loanableUnblockOperation : loanableSecuritiesRequest.getLstLoanableUnblockOperation())
		{			
			loanableUnblockOperation.setLoanableSecuritiesRequest(loanableSecuritiesRequest);
			LoanableSecuritiesOperation	loanableSecuritiesOperationAux = find(LoanableSecuritiesOperation.class,loanableUnblockOperation.getLoanableSecuritiesOperation().getIdLoanableSecuritiesOperationPk());
			
			Map<String,Object> paramate = new HashMap<String,Object>();		
			paramate.put("idSecurityCodePkParam", loanableSecuritiesOperationAux.getSecurity().getIdSecurityCodePk());
			Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
			if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
			}	
			
			if(!existSecuritiesTVR(loanableSecuritiesOperationAux.getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			
			TradeOperation tradeOperation = new TradeOperation();
			tradeOperation.setOperationType(ParameterOperationType.LOANABLE_SECURITIES_RETIREMENT.getCode());
			tradeOperation.setRegisterUser(logger.getUserName());
			tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
			tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesRequest.getRequestState().toString()));
			create(tradeOperation);
			
			loanableUnblockOperation.setTradeOperation(tradeOperation);
			
			create(loanableUnblockOperation);
			
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))
			{
				for(LoanableUnblockMarketfact loanableUnblockMarketfact : loanableUnblockOperation.getLstLoanableUnblockMarketfact())
				{
					loanableUnblockMarketfact.setLoanableUnblockOperation(loanableUnblockOperation);					
					create(loanableUnblockMarketfact);
				}
			}		
		}
		return loanableSecuritiesRequest;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}		
	}
	
	/**
	 * Get Loanable Securities Request Filter.
	 *
	 * @param searchLoanableSecuritiesOperationTO the search loanable securities operation to
	 * @return List Loanable Securities Request
	 * @throws ServiceException service exception
	 */
	@SuppressWarnings({"unchecked" })
	public List<LoanableSecuritiesRequest> getListLoanableSecuritiesRequestFilter(SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO)throws ServiceException
	{
		try{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select loa.idLoanableSecuritiesRequestPk, loa.registryDate,	");//0,1
		stringBuilder.append("	par.description, par.idParticipantPk,	"); //2,3
		stringBuilder.append("	hol.accountNumber, loa.requestState	, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = loa.requestState),  "); //4,5,6
		stringBuilder.append("	ho.idHolderPk, ho.fullName,	");	//7, 8
		stringBuilder.append("	ho.firstLastName,ho.secondLastName,	ho.name,ho.holderType,");	//9,10,11,12
		stringBuilder.append("	loa.requestType	");	//13
		stringBuilder.append("	from LoanableSecuritiesRequest loa	");
		stringBuilder.append("	join loa.holderAccount hol	");
		stringBuilder.append("	join loa.participant par	");		
		stringBuilder.append("	join hol.holderAccountDetails hold	");	
		stringBuilder.append("	join hold.holder ho	");
		stringBuilder.append("	Where 1 = 1	");	
		if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getRequestType()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchLoanableSecuritiesOperationTO.getRequestType())))){
			stringBuilder.append("	AND	loa.requestType = :requestType	");
			parameters.put("requestType", searchLoanableSecuritiesOperationTO.getRequestType());							
		}
		if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getRequestNumber()) && 
				Validations.validateIsPositiveNumber(searchLoanableSecuritiesOperationTO.getRequestNumber().intValue())){
			stringBuilder.append("	AND	loa.idLoanableSecuritiesRequestPk = :idLoanableSecuritiesRequestPk	");
			parameters.put("idLoanableSecuritiesRequestPk", searchLoanableSecuritiesOperationTO.getRequestNumber());							
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchLoanableSecuritiesOperationTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(searchLoanableSecuritiesOperationTO.getFinalDate())){
			stringBuilder.append(" and  TRUNC(loa.registryDate) between :dateIni and :dateEnd ");
			parameters.put("dateIni", searchLoanableSecuritiesOperationTO.getInitialDate());
			parameters.put("dateEnd", searchLoanableSecuritiesOperationTO.getFinalDate());			
		}
		if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getIdState()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchLoanableSecuritiesOperationTO.getIdState())))){
			stringBuilder.append("	AND	loa.requestState = :requestState	");
			parameters.put("requestState", searchLoanableSecuritiesOperationTO.getIdState());							
		}
		if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getIdParticipant()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchLoanableSecuritiesOperationTO.getIdParticipant())))){
			stringBuilder.append("	AND	par.idParticipantPk = :idParticipantPk	");
			parameters.put("idParticipantPk", searchLoanableSecuritiesOperationTO.getIdParticipant());							
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk())&& Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk())))){
			stringBuilder.append("	AND	ho.idHolderPk = :idHolderPk	");
			parameters.put("idHolderPk", searchLoanableSecuritiesOperationTO.getCUIHolder().getIdHolderPk());			
		}	
		if(Validations.validateIsNotNullAndNotEmpty(searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk())&& Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk())))){
			stringBuilder.append("	AND	hol.idHolderAccountPk = :idHolderAccountPk	");
			parameters.put("idHolderAccountPk", searchLoanableSecuritiesOperationTO.getHolderAccount().getIdHolderAccountPk());			
		}	
		List<Object[]> lstLoanableSecuritiesRequest =  findListByQueryString(stringBuilder.toString(), parameters);
		Map<Long,LoanableSecuritiesRequest> loanableSecuritiesRequestPk = new HashMap<Long, LoanableSecuritiesRequest>();
		for(Object[] object: lstLoanableSecuritiesRequest)
		{
			if(loanableSecuritiesRequestPk.get(object[0]) == null)
			{
				LoanableSecuritiesRequest loanableSecuritiesRequest = new LoanableSecuritiesRequest();
				loanableSecuritiesRequest.setIdLoanableSecuritiesRequestPk(new Long(object[0].toString()));
				loanableSecuritiesRequest.setRegistryDate((Date) object[1]);	
				Participant participant = new Participant();
				participant.setDescription(object[2].toString());
				participant.setIdParticipantPk(new Long(object[3].toString()));
				loanableSecuritiesRequest.setParticipant(participant);
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setAccountNumber(new Integer(object[4].toString()));
				holderAccount.setHolderAccountDetails(new ArrayList<HolderAccountDetail>());
				holderAccount.setHolderAccountBalances(new ArrayList<HolderAccountBalance>());
				loanableSecuritiesRequest.setHolderAccount(holderAccount);
				loanableSecuritiesRequest.setRequestState(new Long(object[5].toString()));
				loanableSecuritiesRequest.setRequestNameState(object[6].toString());				
				loanableSecuritiesRequest.setRequestType(new Long(object[13].toString()));
				
				Map<String, Object> parametersAux = new HashMap<String, Object>();
				StringBuilder stringBuilderAux = new StringBuilder();
				stringBuilderAux.append("	Select se.idSecurityCodePk, ");//0
				if(loanableSecuritiesRequest.getRequestType().equals(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode()))
						stringBuilderAux.append("	loas.unblockQuantity	"); //1		
				else 	stringBuilderAux.append("	loa.currentQuantity	"); //1		
				if(loanableSecuritiesRequest.getRequestType().equals(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode()))
				{
					stringBuilderAux.append("	from LoanableUnblockOperation loas	");		
					stringBuilderAux.append("	join loas.loanableSecuritiesOperation loa	");				
					stringBuilderAux.append("	join loas.loanableSecuritiesRequest loar	");
				}
				else
				{
					stringBuilderAux.append("	from LoanableSecuritiesOperation loa	");				
					stringBuilderAux.append("	join loa.loanableSecuritiesRequest loar	");
				}				
				stringBuilderAux.append("	join loa.security se	");			
				stringBuilderAux.append("	Where 1 = 1	");	
				stringBuilderAux.append("	AND	loar.idLoanableSecuritiesRequestPk = :idLoanableSecuritiesRequestPk	");
				parametersAux.put("idLoanableSecuritiesRequestPk", new Long(object[0].toString()));	
				List<Object[]> lstLoanableSecuritiesOperation =  findListByQueryString(stringBuilderAux.toString(), parametersAux);
				
				for(Object[] objectAux: lstLoanableSecuritiesOperation)
				{				
					HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
					Security security = new Security();
					security.setIdSecurityCodePk(objectAux[0].toString());
					holderAccountBalance.setSecurity(security);
					holderAccountBalance.setTransferAmmount(new BigDecimal(objectAux[1].toString()));
					loanableSecuritiesRequest.getHolderAccount().getHolderAccountBalances().add(holderAccountBalance);
				}							
				
				loanableSecuritiesRequestPk.put(loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk(), loanableSecuritiesRequest);
			}
			LoanableSecuritiesRequest loanableSecuritiesRequest = loanableSecuritiesRequestPk.get(object[0]);
			HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
			Holder holder = new Holder();						
			holder.setIdHolderPk(new Long(object[7].toString()));	
			if(Validations.validateIsNotNullAndNotEmpty(object[8]))
				holder.setFullName(object[8].toString());
			if(Validations.validateIsNotNullAndNotEmpty(object[9]))
				holder.setFirstLastName(object[9].toString());
			if(Validations.validateIsNotNullAndNotEmpty(object[10]))
			holder.setSecondLastName(object[10].toString());
			if(Validations.validateIsNotNullAndNotEmpty(object[11]))
				holder.setName(object[11].toString());
			holder.setHolderType(new Integer(object[12].toString()));
			holderAccountDetail.setHolder(holder);
			loanableSecuritiesRequest.getHolderAccount().getHolderAccountDetails().add(holderAccountDetail);
						
		}
		return new ArrayList<LoanableSecuritiesRequest>(loanableSecuritiesRequestPk.values());
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
		}
	
	/**
	 * Get Holder Account Component Service Bean.
	 *
	 * @param holderAccountTO object holderAccountTO
	 * @return object HolderAccount
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListForParticipantAndHolder(HolderAccountTO holderAccountTO) throws ServiceException {
		try{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("	Select hol.idHolderAccountPk,hol.accountNumber, ");//0,1
		stringBuilder.append("	(Select p.parameterName From ParameterTable p Where p.parameterTablePk = hol.stateAccount),  "); //2		
		stringBuilder.append("	hol.alternateCode,holde.idHolderPk,holde.fullName	");	//3,4,5
		stringBuilder.append("	from HolderAccount hol	");	
		stringBuilder.append("	join hol.participant par	");	
		stringBuilder.append("	join hol.holderAccountDetails hold	");	
		stringBuilder.append("	join hold.holder holde	");
		stringBuilder.append("	Where 1 = 1	");	
		if(Validations.validateIsNotNull(holderAccountTO.getParticipantTO()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getParticipantTO())))){
			stringBuilder.append("	AND	par.idParticipantPk = :idParticipantPk	");
			parameters.put("idParticipantPk", holderAccountTO.getParticipantTO());							
		}	
		if(Validations.validateIsNotNull(holderAccountTO.getIdHolderPk()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(holderAccountTO.getIdHolderPk())))){
			stringBuilder.append("	AND	holde.idHolderPk = :idHolderPk	");
			parameters.put("idHolderPk", holderAccountTO.getIdHolderPk());							
		}	
		stringBuilder.append("	AND	hol.stateAccount = :stateAccount	");
		parameters.put("stateAccount", HolderAccountStatusType.ACTIVE.getCode());	
		List<Object[]> lstHolderAccount =  findListByQueryString(stringBuilder.toString(), parameters);
		Map<Long,HolderAccount> holderAccountPk = new HashMap<Long, HolderAccount>();
		for(Object[] object: lstHolderAccount){
			if(holderAccountPk.get(object[0]) == null){
				HolderAccount lolderAccount = new HolderAccount();
				lolderAccount.setIdHolderAccountPk(new Long(object[0].toString()));
				lolderAccount.setAccountNumber(new Integer(object[1].toString()));
				lolderAccount.setStateAccountName(object[2].toString());
				lolderAccount.setAlternateCode(object[3].toString());
				lolderAccount.setHolderAccountDetails(new  ArrayList<HolderAccountDetail>());
				holderAccountPk.put(lolderAccount.getIdHolderAccountPk(), lolderAccount);
			}
			HolderAccount holderAccount = holderAccountPk.get(object[0]);
			HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
			Holder holder = new Holder();						
			holder.setIdHolderPk(new Long(object[4].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(object[5]))
				holder.setFullName(object[5].toString());
			holderAccountDetail.setHolder(holder);
			holderAccount.getHolderAccountDetails().add(holderAccountDetail);
		}
		return new ArrayList<HolderAccount>(holderAccountPk.values());
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Loanable Securities Request Filter.
	 *
	 * @param searchLoanableSecuritiesOperationTO filter Search Loanable Securities Operation
	 * @return object Loanable Securities Operation
	 * @throws ServiceException service exception
	 */
	@SuppressWarnings({"unchecked" })
	public LoanableSecuritiesRequest getLoanableSecuritiesRequestFilter(SearchLoanableSecuritiesOperationTO searchLoanableSecuritiesOperationTO)throws ServiceException{
		try{
			LoanableSecuritiesRequest objLoanableSecuritiesRequest=null;
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("	Select loa.idLoanableSecuritiesRequestPk, loa.registryDate,	");//0,1
			stringBuilder.append("	par.description, par.idParticipantPk,	"); //2,3
			stringBuilder.append("	hol.accountNumber, loa.requestState	, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = loa.requestState),  "); //4,5,6
			stringBuilder.append("	ho.idHolderPk,loa.requestType, loa.interestRate,loa.validityDays,ho.fullName,hol.alternateCode,	");	//7,8,9,10,11,12
			stringBuilder.append("	holde.idHolderPk,holde.fullName,	");	//13,14
			stringBuilder.append("	ho.firstLastName,ho.secondLastName,	ho.name,ho.holderType,   ");	//15,16,17,18
			stringBuilder.append("	holde.firstLastName,holde.secondLastName,holde.name,holde.holderType,	");	//19,20,21,22
			stringBuilder.append("	loa.registryUser,loa.approvalDate,loa.approvalUser,loa.annulmentDate,	");	//23,24,25,26
			stringBuilder.append("	loa.annulmentUser,loa.confirmDate,loa.confirmUser,loa.rejectDate,	");	//27,28,29,30
			stringBuilder.append("	loa.rejectUser,loa.annulmentMotive,loa.rejectMotive,loa.annulmentOtherMotive,loa.rejectOtherMotive,hol.idHolderAccountPk	");	//31,32,33,34,35,36
			stringBuilder.append("	from LoanableSecuritiesRequest loa	");
			stringBuilder.append("	join loa.holderAccount hol	");
			stringBuilder.append("	join loa.participant par	");		
			stringBuilder.append("	join loa.holder ho	");
			stringBuilder.append("	join hol.holderAccountDetails hold	");	
			stringBuilder.append("	join hold.holder holde	");
			stringBuilder.append("	Where 1 = 1	");				
			if(Validations.validateIsNotNull(searchLoanableSecuritiesOperationTO.getRequestNumber()) && Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(searchLoanableSecuritiesOperationTO.getRequestNumber())))){
				stringBuilder.append("	AND	loa.idLoanableSecuritiesRequestPk = :idLoanableSecuritiesRequestPk	");
				parameters.put("idLoanableSecuritiesRequestPk", searchLoanableSecuritiesOperationTO.getRequestNumber());							
			}	
			List<Object[]> lstLoanableSecuritiesRequest =  findListByQueryString(stringBuilder.toString(), parameters);
			Map<Long,LoanableSecuritiesRequest> loanableSecuritiesRequestPk = new HashMap<Long, LoanableSecuritiesRequest>();
			for(Object[] object: lstLoanableSecuritiesRequest)
			{				
				if(loanableSecuritiesRequestPk.get(object[0]) == null)
				{
					LoanableSecuritiesRequest loanableSecuritiesRequest = new LoanableSecuritiesRequest();
					loanableSecuritiesRequest.setIdLoanableSecuritiesRequestPk(new Long(object[0].toString()));
					loanableSecuritiesRequest.setRegistryDate((Date) object[1]);	
					Participant participant = new Participant();
					participant.setDescription(object[2].toString());
					participant.setIdParticipantPk(new Long(object[3].toString()));
					loanableSecuritiesRequest.setParticipant(participant);
					HolderAccount holderAccount = new HolderAccount();
					holderAccount.setIdHolderAccountPk(new Long(object[36].toString()));
					holderAccount.setAccountNumber(new Integer(object[4].toString()));
					holderAccount.setAlternateCode(object[12].toString());
					holderAccount.setHolderAccountDetails(new ArrayList<HolderAccountDetail>());
					holderAccount.setHolderAccountBalances(new ArrayList<HolderAccountBalance>());
					loanableSecuritiesRequest.setHolderAccount(holderAccount);
					loanableSecuritiesRequest.setRequestState(new Long(object[5].toString()));
					loanableSecuritiesRequest.setRequestNameState(object[6].toString());
					Holder holder = new Holder();
					holder.setIdHolderPk(new Long(object[7].toString()));
					if(Validations.validateIsNotNullAndNotEmpty(object[11]))
						holder.setFullName(object[11].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[15]))
						holder.setFirstLastName(object[15].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[16]))
					holder.setSecondLastName(object[16].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[17]))
						holder.setName(object[17].toString());
					holder.setHolderType(new Integer(object[18].toString()));
					loanableSecuritiesRequest.setHolder(holder);	
					loanableSecuritiesRequest.setRequestType(new Long(object[8].toString()));
					if(Validations.validateIsNotNullAndNotEmpty(object[9]))
						loanableSecuritiesRequest.setInterestRate(new BigDecimal(object[9].toString()));
					if(Validations.validateIsNotNullAndNotEmpty(object[10]))
						loanableSecuritiesRequest.setValidityDays(new Integer(object[10].toString()));
					loanableSecuritiesRequest.setRegistryUser(object[23].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[24]))
						loanableSecuritiesRequest.setApprovalDate((Date)object[24]);
					if(Validations.validateIsNotNullAndNotEmpty(object[25]))
						loanableSecuritiesRequest.setApprovalUser(object[25].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[26]))
						loanableSecuritiesRequest.setAnnulmentDate((Date)object[26]);
					if(Validations.validateIsNotNullAndNotEmpty(object[27]))
						loanableSecuritiesRequest.setAnnulmentUser(object[27].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[28]))
						loanableSecuritiesRequest.setConfirmDate((Date)object[28]);
					if(Validations.validateIsNotNullAndNotEmpty(object[29]))
						loanableSecuritiesRequest.setConfirmUser(object[29].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[30]))
						loanableSecuritiesRequest.setRejectDate((Date)object[30]);
					if(Validations.validateIsNotNullAndNotEmpty(object[31]))
						loanableSecuritiesRequest.setRejectUser(object[31].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[32]))
						loanableSecuritiesRequest.setAnnulmentMotive(new Integer(object[32].toString()));
					if(Validations.validateIsNotNullAndNotEmpty(object[33]))
						loanableSecuritiesRequest.setRejectMotive(new Integer(object[33].toString()));
					if(Validations.validateIsNotNullAndNotEmpty(object[34]))
						loanableSecuritiesRequest.setAnnulmentOtherMotive(object[34].toString());
					if(Validations.validateIsNotNullAndNotEmpty(object[35]))
						loanableSecuritiesRequest.setRejectOtherMotive(object[35].toString());
					
					Map<String, Object> parametersAux = new HashMap<String, Object>();
					StringBuilder stringBuilderAux = new StringBuilder();
					stringBuilderAux.append("	Select se.idSecurityCodePk, ");//0
					if(loanableSecuritiesRequest.getRequestType().equals(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode()))
							stringBuilderAux.append("	loas.unblockQuantity,hol.idHolderAccountPk,par.idParticipantPk,loas.idLoanableUnblockOperationPk	"); //1,2,3,4		
					else 	stringBuilderAux.append("	loa.currentQuantity,hol.idHolderAccountPk,par.idParticipantPk,loa.idLoanableSecuritiesOperationPk	"); //1,2,3,4			
					if(loanableSecuritiesRequest.getRequestType().equals(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode()))
					{
						stringBuilderAux.append("	from LoanableUnblockOperation loas	");		
						stringBuilderAux.append("	join loas.loanableSecuritiesOperation loa	");				
						stringBuilderAux.append("	join loas.loanableSecuritiesRequest loar	");
					}
					else
					{
						stringBuilderAux.append("	from LoanableSecuritiesOperation loa	");
						stringBuilderAux.append("	join loa.loanableSecuritiesRequest loar	");
					}				
					stringBuilderAux.append("	join loa.security se	");	
					stringBuilderAux.append("	join loa.holderAccount hol	");
					stringBuilderAux.append("	join loa.participant par	");		
					stringBuilderAux.append("	Where 1 = 1	");	
					stringBuilderAux.append("	AND	loar.idLoanableSecuritiesRequestPk = :idLoanableSecuritiesRequestPk	");
					parametersAux.put("idLoanableSecuritiesRequestPk", new Long(object[0].toString()));	
					List<Object[]> lstLoanableSecuritiesOperation =  findListByQueryString(stringBuilderAux.toString(), parametersAux);
					
					for(Object[] objectAux: lstLoanableSecuritiesOperation)
					{			
						HolderAccountBalance holderAccountBalance=null;
						if(loanableSecuritiesRequest.getRequestType().equals(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode()))
						{
							StringBuilder sbQuery = new StringBuilder();
							sbQuery.append("Select loa From LoanableUnblockOperation  loa ");
							sbQuery.append(" where loa.loanableSecuritiesOperation.participant.idParticipantPk=" + new Long(objectAux[3].toString()));
							sbQuery.append(" and loa.loanableSecuritiesOperation.holderAccount.idHolderAccountPk=" + new Long(objectAux[2].toString()));
							sbQuery.append(" and loa.loanableSecuritiesOperation.security.idSecurityCodePk='" + objectAux[0].toString()+"'");	
							sbQuery.append(" and loa.idLoanableUnblockOperationPk=" +new Long(objectAux[4].toString()));	
									
							Query query = em.createQuery(sbQuery.toString());
							
							LoanableUnblockOperation loanableUnblockOperation= (LoanableUnblockOperation) query.getSingleResult();
							
							holderAccountBalance = new 	HolderAccountBalance();
							HolderAccountBalancePK holderAccountBalancePK = new HolderAccountBalancePK();
							holderAccountBalancePK.setIdParticipantPk(loanableUnblockOperation.getLoanableSecuritiesOperation().getParticipant().getIdParticipantPk());
							holderAccountBalancePK.setIdHolderAccountPk(loanableUnblockOperation.getLoanableSecuritiesOperation().getHolderAccount().getIdHolderAccountPk());
							holderAccountBalancePK.setIdSecurityCodePk(loanableUnblockOperation.getLoanableSecuritiesOperation().getSecurity().getIdSecurityCodePk());
							holderAccountBalance.setId(holderAccountBalancePK);
							holderAccountBalance.setTotalBalance(loanableUnblockOperation.getLoanableSecuritiesOperation().getInitialQuantity());
							holderAccountBalance.setLoanableBalance(loanableUnblockOperation.getLoanableSecuritiesOperation().getCurrentQuantity());	
							
							List<HolderMarketFactBalance> lstHolderMarketFactBalance=null;
							StringBuilder sbQueryA = new StringBuilder();
							sbQueryA.append("Select HAB From HolderMarketFactBalance HAB ");
							sbQueryA.append(" where HAB.participant.idParticipantPk=" + new Long(objectAux[3].toString()));
							sbQueryA.append(" and HAB.holderAccount.idHolderAccountPk=" + new Long(objectAux[2].toString()));
							sbQueryA.append(" and HAB.security.idSecurityCodePk='" + objectAux[0].toString()+"'");								
							Query queryA = em.createQuery(sbQueryA.toString());							
							lstHolderMarketFactBalance = (List<HolderMarketFactBalance>)queryA.getResultList();
							
							List<LoanableUnblockMarketfact> lstLoanableUnblockMarketfact=null;
							StringBuilder sbQueryAs = new StringBuilder();
							sbQueryAs.append("Select HAB From LoanableUnblockMarketfact HAB ");
							sbQueryAs.append(" where HAB.loanableUnblockOperation.idLoanableUnblockOperationPk=" + new Long(objectAux[4].toString()));									
							Query queryAs = em.createQuery(sbQueryAs.toString());
							lstLoanableUnblockMarketfact = (List<LoanableUnblockMarketfact>)queryAs.getResultList();
							
							Security security = find(Security.class, objectAux[0].toString());
							for(HolderMarketFactBalance holderMarketFactBalance :lstHolderMarketFactBalance){
								for(LoanableUnblockMarketfact loanableUnblockMarketfact : lstLoanableUnblockMarketfact){
									if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
									{
										if(holderMarketFactBalance.getMarketDate().equals(loanableUnblockMarketfact.getMarketDate())
												&& holderMarketFactBalance.getMarketRate().equals(loanableUnblockMarketfact.getMarketRate()))
										{
											holderMarketFactBalance.setLoanableBalance(loanableUnblockMarketfact.getLoanableUnblockOperation().getLoanableSecuritiesOperation().getCurrentQuantity());
											holderMarketFactBalance.setTransferAmmount(loanableUnblockMarketfact.getMarketQuantity());
										}											
									}
									else
									{
										if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
										{
											if(holderMarketFactBalance.getMarketDate().equals(loanableUnblockMarketfact.getMarketDate())
													&& holderMarketFactBalance.getMarketPrice().equals(loanableUnblockMarketfact.getMarketPrice()))
											{
												holderMarketFactBalance.setLoanableBalance(loanableUnblockMarketfact.getLoanableUnblockOperation().getLoanableSecuritiesOperation().getCurrentQuantity());
												holderMarketFactBalance.setTransferAmmount(loanableUnblockMarketfact.getMarketQuantity());
											}
										}
									}
								}
							}
							
							holderAccountBalance.setHolderMarketfactBalance(lstHolderMarketFactBalance);
						}
						else
						{
							StringBuilder sbQuery = new StringBuilder();
							sbQuery.append("Select HAB From HolderAccountBalance HAB ");
							sbQuery.append(" where HAB.participant.idParticipantPk=" + new Long(objectAux[3].toString()));
							sbQuery.append(" and HAB.holderAccount.idHolderAccountPk=" + new Long(objectAux[2].toString()));
							sbQuery.append(" and HAB.security.idSecurityCodePk='" + objectAux[0].toString()+"'");								
							Query query = em.createQuery(sbQuery.toString());						
							holderAccountBalance= (HolderAccountBalance) query.getSingleResult();
							
							List<HolderMarketFactBalance> lstHolderMarketFactBalance=null;
							StringBuilder sbQueryA = new StringBuilder();
							sbQueryA.append("Select HAB From HolderMarketFactBalance HAB ");
							sbQueryA.append(" where HAB.participant.idParticipantPk=" + new Long(objectAux[3].toString()));
							sbQueryA.append(" and HAB.holderAccount.idHolderAccountPk=" + new Long(objectAux[2].toString()));
							sbQueryA.append(" and HAB.security.idSecurityCodePk='" + objectAux[0].toString()+"'");								
							Query queryA = em.createQuery(sbQueryA.toString());							
							lstHolderMarketFactBalance = (List<HolderMarketFactBalance>)queryA.getResultList();
							
							List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact=null;
							StringBuilder sbQueryAs = new StringBuilder();
							sbQueryAs.append("Select HAB From LoanableSecuritiesMarketfact HAB ");
							sbQueryAs.append(" where HAB.loanableSecuritiesOperation.idLoanableSecuritiesOperationPk=" + new Long(objectAux[4].toString()));									
							Query queryAs = em.createQuery(sbQueryAs.toString());
							lstLoanableSecuritiesMarketfact = (List<LoanableSecuritiesMarketfact>)queryAs.getResultList();
							
							Security security = find(Security.class, objectAux[0].toString());
							for(HolderMarketFactBalance holderMarketFactBalance :lstHolderMarketFactBalance){
								for(LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : lstLoanableSecuritiesMarketfact){
									if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
									{
										if(holderMarketFactBalance.getMarketDate().equals(loanableSecuritiesMarketfact.getMarketDate())
												&& holderMarketFactBalance.getMarketRate().equals(loanableSecuritiesMarketfact.getMarketRate()))
											holderMarketFactBalance.setTransferAmmount(loanableSecuritiesMarketfact.getInitialMarketQuantity());
									}
									else
									{
										if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
										{
											if(holderMarketFactBalance.getMarketDate().equals(loanableSecuritiesMarketfact.getMarketDate())
													&& holderMarketFactBalance.getMarketPrice().equals(loanableSecuritiesMarketfact.getMarketPrice()))
												holderMarketFactBalance.setTransferAmmount(loanableSecuritiesMarketfact.getInitialMarketQuantity());
										}
									}
								}
							}
								
							holderAccountBalance.setHolderMarketfactBalance(lstHolderMarketFactBalance);
						}				
									
						holderAccountBalance.setTransferAmmount(new BigDecimal(objectAux[1].toString()));
						loanableSecuritiesRequest.getHolderAccount().getHolderAccountBalances().add(holderAccountBalance);
					}				
					
					loanableSecuritiesRequestPk.put(loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk(), loanableSecuritiesRequest);
				}
				LoanableSecuritiesRequest loanableSecuritiesRequest = loanableSecuritiesRequestPk.get(object[0]);
				HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
				Holder holder = new Holder();						
				holder.setIdHolderPk(new Long(object[13].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(object[14]))
					holder.setFullName(object[14].toString());
				if(Validations.validateIsNotNullAndNotEmpty(object[19]))
					holder.setFirstLastName(object[19].toString());
				if(Validations.validateIsNotNullAndNotEmpty(object[20]))
					holder.setSecondLastName(object[20].toString());
				if(Validations.validateIsNotNullAndNotEmpty(object[21]))
					holder.setName(object[21].toString());
				holder.setHolderType(new Integer(object[22].toString()));
				holderAccountDetail.setHolder(holder);
				loanableSecuritiesRequest.getHolderAccount().getHolderAccountDetails().add(holderAccountDetail);
															
			}		
			List<LoanableSecuritiesRequest> lstAuxLoanableSecuritiesRequest=new ArrayList<LoanableSecuritiesRequest>(loanableSecuritiesRequestPk.values());
			objLoanableSecuritiesRequest=lstAuxLoanableSecuritiesRequest.get(0);
			return objLoanableSecuritiesRequest ;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Exist Securities TVR.
	 *
	 * @param idSecurityCodePk Security Code
	 * @return flag Exist
	 * @throws ServiceException the service exception
	 */
	public Boolean existSecuritiesTVR(String idSecurityCodePk)throws ServiceException{ 
		try{
			
		StringBuilder stringBuilderSql = new StringBuilder();
//		Integer idGroup;
//		stringBuilderSql.append("Select se.idGroupFk FROM Security se Where se.idSecurityCodePk ='"+idSecurityCodePk+"'");
		
		stringBuilderSql.append("Select gvcList.idSecurityGvcPk FROM SecurityGvcList gvcList					");
//		stringBuilderSql.append("Inner Join gvcList.gvcList gvc 												");
		stringBuilderSql.append("Where gvcList.security.idSecurityCodePk = :securityCode	And					");
		stringBuilderSql.append("	   gvcList.securityGvcState = :enabledCode									");
		
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("enabledCode", BooleanType.YES.getCode());
		query.setParameter("securityCode", idSecurityCodePk);
		
		try{
			query.getSingleResult();
			return Boolean.TRUE;
		}catch(NoResultException ex){
			return Boolean.FALSE;
		}
		
//		if(Validations.validateIsNullOrEmpty(idGroup) || Validations.validateIsNullOrNotPositive(idGroup))
//			return Boolean.FALSE;
//		else
//		{
//			StringBuilder stringBuilder = new StringBuilder();
//			Integer idGroupAux;	
//			stringBuilder.append("Select tvr.id_group_pk FROM TvrList tvr Where tvr.id_group_pk = " +idGroup);
//			Query queryAux = em.createQuery(stringBuilder.toString());			
//			if(Validations.validateIsNullOrEmpty(queryAux.getSingleResult()))
//				return Boolean.FALSE;
//			else
//			{
//				idGroupAux=new Integer(queryAux.getSingleResult().toString());
//				if(Validations.validateIsNullOrEmpty(idGroupAux) || Validations.validateIsNullOrNotPositive(idGroupAux))
//					return Boolean.FALSE;
//				else	return Boolean.TRUE;
//			}	
//		}		

		} catch(NoResultException ex){
			   return Boolean.FALSE;
		} catch(NonUniqueResultException ex){
			   return Boolean.FALSE;
		}	
	}
	
	/**
	 * Annul Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @param loggerUser object logger User
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	public LoanableSecuritiesRequest annulLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}		
			
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}	
			
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
			}	
			
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idLoanableSecuritiesRequestPkParam", loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());
			LoanableSecuritiesRequest loanableSecuritiesRequestAux = findObjectByNamedQuery(LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST_STATE, mapParams);
			if(!LoanableSecuritiesOperationStateType.REGISTERED.getCode().equals(loanableSecuritiesRequestAux.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			loanableSecuritiesRequest.setAnnulmentUser(loggerUser.getUserName());
			loanableSecuritiesRequest.setAnnulmentDate(CommonsUtilities.currentDateTime());
			
			update(loanableSecuritiesRequest);
			
			List<LoanableSecuritiesOperation>  lstLoanableSecuritiesOperation=null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select loa From LoanableSecuritiesOperation loa ");
			sbQuery.append(" where loa.loanableSecuritiesRequest.idLoanableSecuritiesRequestPk=" + loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());					
			Query query = em.createQuery(sbQuery.toString());
			lstLoanableSecuritiesOperation = (List<LoanableSecuritiesOperation>) query.getResultList();
			
			for(LoanableSecuritiesOperation loanableSecuritiesOperation : lstLoanableSecuritiesOperation)
			{
				loanableSecuritiesOperation.setOperationState(LoanableSecuritiesOperationStateType.CANCELLED.getCode());
				
				Map<String,Object> paramate = new HashMap<String,Object>();		
				paramate.put("idSecurityCodePkParam", loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
				if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
				}								
				
				if(!existSecuritiesTVR(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				
				TradeOperation tradeOperation = find(TradeOperation.class,loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());				
				
				tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesOperation.getOperationState().toString()));
				tradeOperation.setAnnulateDate(CommonsUtilities.currentDateTime());
				tradeOperation.setAnnulateUser(loggerUser.getUserName());
				tradeOperation.setAnnulateMotive(loanableSecuritiesRequest.getAnnulmentMotive());
				tradeOperation.setAnnulateMotiveOther(loanableSecuritiesRequest.getAnnulmentOtherMotive());
				update(tradeOperation);
				
				loanableSecuritiesOperation.setTradeOperation(tradeOperation);
				
				update(loanableSecuritiesOperation);
				
				List<HolderAccountBalanceTO> lstHolderAccountBalanceTO = new ArrayList<HolderAccountBalanceTO>();	
				
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdParticipant(loanableSecuritiesOperation.getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdHolderAccount(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdSecurityCode(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(loanableSecuritiesOperation.getCurrentQuantity());		
				
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))
				{
					List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact=null;
					StringBuilder sbQueryA = new StringBuilder();
					sbQueryA.append("Select lmf From LoanableSecuritiesMarketfact lmf ");
					sbQueryA.append(" where lmf.loanableSecuritiesOperation.tradeOperation.idTradeOperationPk=" + loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());					
					Query queryA = em.createQuery(sbQueryA.toString());				
					lstLoanableSecuritiesMarketfact = (List<LoanableSecuritiesMarketfact>) queryA.getResultList();
					
					List<MarketFactAccountTO> lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();	
					for(LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : lstLoanableSecuritiesMarketfact)
					{
						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
						marketFactAccountTO.setMarketDate(loanableSecuritiesMarketfact.getMarketDate());
						marketFactAccountTO.setMarketPrice(loanableSecuritiesMarketfact.getMarketPrice());
						marketFactAccountTO.setMarketRate(loanableSecuritiesMarketfact.getMarketRate());
						marketFactAccountTO.setQuantity(loanableSecuritiesMarketfact.getCurrentMarketQuantity());
						lstMarketFactAccounts.add(marketFactAccountTO);
					}
					holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccounts);
				}
				
				lstHolderAccountBalanceTO.add(holderAccountBalanceTO);
				
				AccountsComponentTO  accountsComponentTO = new AccountsComponentTO();
				accountsComponentTO.setIdBusinessProcess(BusinessProcessType.LOANABLE_SECURITIES_CANCEL.getCode());		
				accountsComponentTO.setIdOperationType(ParameterOperationType.LOANABLE_SECURITIES_REGISTER.getCode());			
				accountsComponentTO.setIdTradeOperation(tradeOperation.getIdTradeOperationPk());
				accountsComponentTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
				accountsComponentTO.setLstSourceAccounts(lstHolderAccountBalanceTO);
			
				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);				
			}
			
			return loanableSecuritiesRequest;
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Reject Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @param loggerUser object logger User
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	public LoanableSecuritiesRequest rejectLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}		
			
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}	
			
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
			}	
			
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idLoanableSecuritiesRequestPkParam", loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());
			LoanableSecuritiesRequest loanableSecuritiesRequestAux = findObjectByNamedQuery(LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST_STATE, mapParams);
			if(!LoanableSecuritiesOperationStateType.APPROVED.getCode().equals(loanableSecuritiesRequestAux.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			loanableSecuritiesRequest.setRejectUser(loggerUser.getUserName());
			loanableSecuritiesRequest.setRejectDate(CommonsUtilities.currentDateTime());
			
			update(loanableSecuritiesRequest);
			
			List<LoanableSecuritiesOperation>  lstLoanableSecuritiesOperation=null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select loa From LoanableSecuritiesOperation loa ");
			sbQuery.append(" where loa.loanableSecuritiesRequest.idLoanableSecuritiesRequestPk=" + loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());					
			Query query = em.createQuery(sbQuery.toString());
			lstLoanableSecuritiesOperation = (List<LoanableSecuritiesOperation>) query.getResultList();
			
			for(LoanableSecuritiesOperation loanableSecuritiesOperation : lstLoanableSecuritiesOperation)
			{
				loanableSecuritiesOperation.setOperationState(LoanableSecuritiesOperationStateType.REJECTED.getCode());
				
				Map<String,Object> paramate = new HashMap<String,Object>();		
				paramate.put("idSecurityCodePkParam", loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
				if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
				}								
							
				if(!existSecuritiesTVR(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				
				TradeOperation tradeOperation = find(TradeOperation.class,loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());				
				
				tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesOperation.getOperationState().toString()));
				tradeOperation.setRejectDate(CommonsUtilities.currentDateTime());
				tradeOperation.setRejectUser(loggerUser.getUserName());
				tradeOperation.setRejectMotive(loanableSecuritiesRequest.getRejectMotive());
				tradeOperation.setRejectMotiveOther(loanableSecuritiesRequest.getRejectOtherMotive());
				update(tradeOperation);
				
				loanableSecuritiesOperation.setTradeOperation(tradeOperation);
				
				update(loanableSecuritiesOperation);
				
				List<HolderAccountBalanceTO> lstHolderAccountBalanceTO = new ArrayList<HolderAccountBalanceTO>();	
				
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdParticipant(loanableSecuritiesOperation.getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdHolderAccount(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdSecurityCode(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(loanableSecuritiesOperation.getCurrentQuantity());		
				
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))
				{
					List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact=null;
					StringBuilder sbQueryA = new StringBuilder();
					sbQueryA.append("Select lmf From LoanableSecuritiesMarketfact lmf ");
					sbQueryA.append(" where lmf.loanableSecuritiesOperation.tradeOperation.idTradeOperationPk=" + loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());					
					Query queryA = em.createQuery(sbQueryA.toString());				
					lstLoanableSecuritiesMarketfact = (List<LoanableSecuritiesMarketfact>) queryA.getResultList();
					
					List<MarketFactAccountTO> lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();	
					for(LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : lstLoanableSecuritiesMarketfact)
					{
						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
						marketFactAccountTO.setMarketDate(loanableSecuritiesMarketfact.getMarketDate());
						marketFactAccountTO.setMarketPrice(loanableSecuritiesMarketfact.getMarketPrice());
						marketFactAccountTO.setMarketRate(loanableSecuritiesMarketfact.getMarketRate());
						marketFactAccountTO.setQuantity(loanableSecuritiesMarketfact.getCurrentMarketQuantity());
						lstMarketFactAccounts.add(marketFactAccountTO);
					}
					holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccounts);
				}
				
				lstHolderAccountBalanceTO.add(holderAccountBalanceTO);
				
				AccountsComponentTO  accountsComponentTO = new AccountsComponentTO();
				accountsComponentTO.setIdBusinessProcess(BusinessProcessType.LOANABLE_SECURITIES_REJECT.getCode());		
				accountsComponentTO.setIdOperationType(ParameterOperationType.LOANABLE_SECURITIES_REGISTER.getCode());			
				accountsComponentTO.setIdTradeOperation(tradeOperation.getIdTradeOperationPk());
				accountsComponentTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
				accountsComponentTO.setLstSourceAccounts(lstHolderAccountBalanceTO);
			
				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);				
			}
			
			return loanableSecuritiesRequest;
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Reject Retirement Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @param loggerUser object logger User
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	public LoanableSecuritiesRequest rejectRetirementLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}		
			
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}	
			
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
			}	
			
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idLoanableSecuritiesRequestPkParam", loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());
			LoanableSecuritiesRequest loanableSecuritiesRequestAux = findObjectByNamedQuery(LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST_STATE, mapParams);
			if(!LoanableSecuritiesOperationStateType.REGISTERED.getCode().equals(loanableSecuritiesRequestAux.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			loanableSecuritiesRequest.setRejectUser(loggerUser.getUserName());
			loanableSecuritiesRequest.setRejectDate(CommonsUtilities.currentDateTime());
			
			update(loanableSecuritiesRequest);
			
			List<LoanableUnblockOperation>  lstLoanableUnblockOperation=null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select loa From LoanableUnblockOperation loa ");
			sbQuery.append(" where loa.loanableSecuritiesRequest.idLoanableSecuritiesRequestPk=" + loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());					
			Query query = em.createQuery(sbQuery.toString());
			lstLoanableUnblockOperation = (List<LoanableUnblockOperation>) query.getResultList();
			
			for(LoanableUnblockOperation loanableUnblockOperation : lstLoanableUnblockOperation)
			{										
				Map<String,Object> paramate = new HashMap<String,Object>();		
				paramate.put("idSecurityCodePkParam", loanableUnblockOperation.getLoanableSecuritiesOperation().getSecurity().getIdSecurityCodePk());
				Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
				if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
				}								
							
				if(!existSecuritiesTVR(loanableUnblockOperation.getLoanableSecuritiesOperation().getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				
				TradeOperation tradeOperation = find(TradeOperation.class,loanableUnblockOperation.getIdLoanableUnblockOperationPk());				
				
				tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesRequest.getRequestState().toString()));
				tradeOperation.setRejectDate(CommonsUtilities.currentDateTime());
				tradeOperation.setRejectUser(loggerUser.getUserName());
				tradeOperation.setRejectMotive(loanableSecuritiesRequest.getAnnulmentMotive());
				tradeOperation.setRejectMotiveOther(loanableSecuritiesRequest.getAnnulmentOtherMotive());
				update(tradeOperation);
				
				loanableUnblockOperation.setTradeOperation(tradeOperation);
				
				update(loanableUnblockOperation);				
			}
			
			return loanableSecuritiesRequest;
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Approve Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @param loggerUser object logger User
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	public LoanableSecuritiesRequest approveLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}		
			
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}	
			
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
			}	
			
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idLoanableSecuritiesRequestPkParam", loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());
			LoanableSecuritiesRequest loanableSecuritiesRequestAux = findObjectByNamedQuery(LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST_STATE, mapParams);
			if(!LoanableSecuritiesOperationStateType.REGISTERED.getCode().equals(loanableSecuritiesRequestAux.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			loanableSecuritiesRequest.setApprovalUser(loggerUser.getUserName());
			loanableSecuritiesRequest.setApprovalDate(CommonsUtilities.currentDateTime());
			
			update(loanableSecuritiesRequest);
			
			List<LoanableSecuritiesOperation>  lstLoanableSecuritiesOperation=null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select loa From LoanableSecuritiesOperation loa ");
			sbQuery.append(" where loa.loanableSecuritiesRequest.idLoanableSecuritiesRequestPk=" + loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());					
			Query query = em.createQuery(sbQuery.toString());
			lstLoanableSecuritiesOperation = (List<LoanableSecuritiesOperation>) query.getResultList();
			
			for(LoanableSecuritiesOperation loanableSecuritiesOperation : lstLoanableSecuritiesOperation)
			{
				loanableSecuritiesOperation.setOperationState(LoanableSecuritiesOperationStateType.APPROVED.getCode());
				
				Map<String,Object> paramate = new HashMap<String,Object>();		
				paramate.put("idSecurityCodePkParam", loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
				if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
				}								
				
				if(!existSecuritiesTVR(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				
				TradeOperation tradeOperation = find(TradeOperation.class,loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());				
				
				tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesOperation.getOperationState().toString()));
				tradeOperation.setAprovalDate(CommonsUtilities.currentDateTime());
				tradeOperation.setAprovalUser(loggerUser.getUserName());				
				update(tradeOperation);
				
				loanableSecuritiesOperation.setTradeOperation(tradeOperation);
				
				update(loanableSecuritiesOperation);

			}
			
			return loanableSecuritiesRequest;
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Confirm Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @param loggerUser object logger User
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	public LoanableSecuritiesRequest confirmLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest,LoggerUser loggerUser) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}		
			
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}	
			
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
			}	
			
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idLoanableSecuritiesRequestPkParam", loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());
			LoanableSecuritiesRequest loanableSecuritiesRequestAux = findObjectByNamedQuery(LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST_STATE, mapParams);
			if(!LoanableSecuritiesOperationStateType.APPROVED.getCode().equals(loanableSecuritiesRequestAux.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			loanableSecuritiesRequest.setConfirmUser(loggerUser.getUserName());
			loanableSecuritiesRequest.setConfirmDate(CommonsUtilities.currentDateTime());
			
			update(loanableSecuritiesRequest);
			
			List<LoanableSecuritiesOperation>  lstLoanableSecuritiesOperation=null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select loa From LoanableSecuritiesOperation loa ");
			sbQuery.append(" where loa.loanableSecuritiesRequest.idLoanableSecuritiesRequestPk=" + loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());					
			Query query = em.createQuery(sbQuery.toString());
			lstLoanableSecuritiesOperation = (List<LoanableSecuritiesOperation>) query.getResultList();
			
			for(LoanableSecuritiesOperation loanableSecuritiesOperation : lstLoanableSecuritiesOperation)
			{
				loanableSecuritiesOperation.setOperationState(LoanableSecuritiesOperationStateType.CONFIRMED.getCode());
				
				Map<String,Object> paramate = new HashMap<String,Object>();		
				paramate.put("idSecurityCodePkParam", loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
				if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
				}								
							
				if(!existSecuritiesTVR(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				
				TradeOperation tradeOperation = find(TradeOperation.class,loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());				
				
				tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesOperation.getOperationState().toString()));
				tradeOperation.setConfirmDate(CommonsUtilities.currentDateTime());
				tradeOperation.setConfirmUser(loggerUser.getUserName());				
				update(tradeOperation);
				
				loanableSecuritiesOperation.setTradeOperation(tradeOperation);
				
				update(loanableSecuritiesOperation);
				
				List<HolderAccountBalanceTO> lstHolderAccountBalanceTO = new ArrayList<HolderAccountBalanceTO>();	
				
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdParticipant(loanableSecuritiesOperation.getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdHolderAccount(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdSecurityCode(loanableSecuritiesOperation.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(loanableSecuritiesOperation.getCurrentQuantity());		
				
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))
				{
					List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact=null;
					StringBuilder sbQueryA = new StringBuilder();
					sbQueryA.append("Select lmf From LoanableSecuritiesMarketfact lmf ");
					sbQueryA.append(" where lmf.loanableSecuritiesOperation.tradeOperation.idTradeOperationPk=" + loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());					
					Query queryA = em.createQuery(sbQueryA.toString());				
					lstLoanableSecuritiesMarketfact = (List<LoanableSecuritiesMarketfact>) queryA.getResultList();
					
					List<MarketFactAccountTO> lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();	
					for(LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : lstLoanableSecuritiesMarketfact)
					{
						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
						marketFactAccountTO.setMarketDate(loanableSecuritiesMarketfact.getMarketDate());
						marketFactAccountTO.setMarketPrice(loanableSecuritiesMarketfact.getMarketPrice());
						marketFactAccountTO.setMarketRate(loanableSecuritiesMarketfact.getMarketRate());
						marketFactAccountTO.setQuantity(loanableSecuritiesMarketfact.getCurrentMarketQuantity());
						lstMarketFactAccounts.add(marketFactAccountTO);
					}
					holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccounts);
				}
				
				lstHolderAccountBalanceTO.add(holderAccountBalanceTO);
				
				AccountsComponentTO  accountsComponentTO = new AccountsComponentTO();
				accountsComponentTO.setIdBusinessProcess(BusinessProcessType.LOANABLE_SECURITIES_CONFIRM.getCode());		
				accountsComponentTO.setIdOperationType(ParameterOperationType.LOANABLE_SECURITIES_REGISTER.getCode());			
				accountsComponentTO.setIdTradeOperation(tradeOperation.getIdTradeOperationPk());
				accountsComponentTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
				accountsComponentTO.setLstSourceAccounts(lstHolderAccountBalanceTO);
			
				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);				
			}
			
			return loanableSecuritiesRequest;
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Confirm Retirement Loanable Securities Request.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @param loggerUser object logger User
	 * @param flagAutomatic the flag automatic
	 * @return object loanable Securities Request
	 * @throws ServiceException the service exception
	 */
	public LoanableSecuritiesRequest confirmRetirementLoanableSecuritiesRequest(LoanableSecuritiesRequest loanableSecuritiesRequest,LoggerUser loggerUser,Boolean flagAutomatic) throws ServiceException{
		try{
			Map<String,Object> param = new HashMap<String,Object>();		
			param.put("idHolderPkParam", loanableSecuritiesRequest.getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}		
			
			Map<String,Object> parama = new HashMap<String,Object>();		
			parama.put("idParticipantPkParam", loanableSecuritiesRequest.getParticipant().getIdParticipantPk());
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, parama);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}	
			
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idHolderAccountPkParam", loanableSecuritiesRequest.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, paramat);
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK);
			}	
			
			Map<String,Object> mapParams = new HashMap<String, Object>();
			mapParams.put("idLoanableSecuritiesRequestPkParam", loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());
			LoanableSecuritiesRequest loanableSecuritiesRequestAux = findObjectByNamedQuery(LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST_STATE, mapParams);
			if(!LoanableSecuritiesOperationStateType.REGISTERED.getCode().equals(loanableSecuritiesRequestAux.getRequestState())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			if(isInsufficientBalanceLoanableOperation(loanableSecuritiesRequestAux))	
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			
			loanableSecuritiesRequest.setConfirmUser(loggerUser.getUserName());
			loanableSecuritiesRequest.setConfirmDate(CommonsUtilities.currentDateTime());
			
			if(flagAutomatic.equals(Boolean.TRUE))
				loanableSecuritiesRequest.setRequestState(LoanableSecuritiesOperationStateType.CONFIRMED.getCode());
			
			update(loanableSecuritiesRequest);
			
			List<LoanableUnblockOperation>  lstLoanableUnblockOperation=null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select loa From LoanableUnblockOperation loa ");
			sbQuery.append(" where loa.loanableSecuritiesRequest.idLoanableSecuritiesRequestPk=" + loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());					
			Query query = em.createQuery(sbQuery.toString());
			lstLoanableUnblockOperation = (List<LoanableUnblockOperation>) query.getResultList();
			
			for(LoanableUnblockOperation loanableUnblockOperation : lstLoanableUnblockOperation)
			{			
				Security security = find(Security.class, loanableUnblockOperation.getLoanableSecuritiesOperation().getSecurity().getIdSecurityCodePk());
				if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
				}								
				
				if(!existSecuritiesTVR(loanableUnblockOperation.getLoanableSecuritiesOperation().getSecurity().getIdSecurityCodePk()))
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				
				TradeOperation tradeOperation = find(TradeOperation.class,loanableUnblockOperation.getIdLoanableUnblockOperationPk());				
				
				tradeOperation.setOperationState(Integer.valueOf(loanableSecuritiesRequest.getRequestState().toString()));
				tradeOperation.setConfirmDate(CommonsUtilities.currentDateTime());
				tradeOperation.setConfirmUser(loggerUser.getUserName());				
				update(tradeOperation);
				
				loanableUnblockOperation.setTradeOperation(tradeOperation);
				
				update(loanableUnblockOperation);
				
				LoanableSecuritiesOperation loanableSecuritiesOperation = loanableUnblockOperation.getLoanableSecuritiesOperation();
				loanableSecuritiesOperation.setCurrentQuantity(loanableSecuritiesOperation.getCurrentQuantity().subtract(loanableUnblockOperation.getUnblockQuantity()));
				if(new Integer(loanableUnblockOperation.getUnblockMotive().toString()).equals(LoanableUnblockMotiveType.AUTOMATIC_PROCESS.getCode()))
					loanableSecuritiesOperation.setOperationState(LoanableSecuritiesOperationStateType.EXPIRED.getCode());
				update(loanableSecuritiesOperation);
				
				List<HolderAccountBalanceTO> lstHolderAccountBalanceTO = new ArrayList<HolderAccountBalanceTO>();	
				
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdParticipant(loanableUnblockOperation.getLoanableSecuritiesOperation().getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdHolderAccount(loanableUnblockOperation.getLoanableSecuritiesOperation().getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdSecurityCode(loanableUnblockOperation.getLoanableSecuritiesOperation().getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(loanableUnblockOperation.getUnblockQuantity());
				
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))//LoanableSecuritiesMarketfact
				{	
					List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact=null;
					StringBuilder sbQueryAs = new StringBuilder();
					sbQueryAs.append("Select lmf From LoanableSecuritiesMarketfact lmf ");
					sbQueryAs.append(" where lmf.loanableSecuritiesOperation.tradeOperation.idTradeOperationPk=" + loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());					
					Query queryAs = em.createQuery(sbQueryAs.toString());				
					lstLoanableSecuritiesMarketfact = (List<LoanableSecuritiesMarketfact>) queryAs.getResultList();
					
					List<LoanableUnblockMarketfact> lstLoanableUnblockMarketfact=null;
					StringBuilder sbQueryA = new StringBuilder();
					sbQueryA.append("Select lmf From LoanableUnblockMarketfact lmf ");
					sbQueryA.append(" where lmf.loanableUnblockOperation.tradeOperation.idTradeOperationPk=" + loanableUnblockOperation.getIdLoanableUnblockOperationPk());					
					Query queryA = em.createQuery(sbQueryA.toString());				
					lstLoanableUnblockMarketfact = (List<LoanableUnblockMarketfact>) queryA.getResultList();
					
					List<MarketFactAccountTO> lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();	
					for(LoanableUnblockMarketfact loanableUnblockMarketfact : lstLoanableUnblockMarketfact)
					{
						
						for(LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : lstLoanableSecuritiesMarketfact)
						{
							if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
							{
								if(loanableUnblockMarketfact.getMarketDate().equals(loanableSecuritiesMarketfact.getMarketDate())
										&& loanableUnblockMarketfact.getMarketRate().equals(loanableSecuritiesMarketfact.getMarketRate()))
								{
									loanableSecuritiesMarketfact.setCurrentMarketQuantity(loanableSecuritiesMarketfact.getCurrentMarketQuantity().subtract(loanableUnblockMarketfact.getMarketQuantity()));
									update(loanableSecuritiesMarketfact);
								}
							}else{
								if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
								{
									if(loanableUnblockMarketfact.getMarketDate().equals(loanableSecuritiesMarketfact.getMarketDate())
											&& loanableUnblockMarketfact.getMarketPrice().equals(loanableSecuritiesMarketfact.getMarketPrice()))
									{
										loanableSecuritiesMarketfact.setCurrentMarketQuantity(loanableSecuritiesMarketfact.getCurrentMarketQuantity().subtract(loanableUnblockMarketfact.getMarketQuantity()));
										update(loanableSecuritiesMarketfact);
									}
								}								
							}							
						}
						
						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
						marketFactAccountTO.setMarketDate(loanableUnblockMarketfact.getMarketDate());
						marketFactAccountTO.setMarketPrice(loanableUnblockMarketfact.getMarketPrice());
						marketFactAccountTO.setMarketRate(loanableUnblockMarketfact.getMarketRate());
						marketFactAccountTO.setQuantity(loanableUnblockMarketfact.getMarketQuantity());
						lstMarketFactAccounts.add(marketFactAccountTO);
					}
					holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccounts);
				}
							
				lstHolderAccountBalanceTO.add(holderAccountBalanceTO);
							
				AccountsComponentTO  accountsComponentTO = new AccountsComponentTO();
				accountsComponentTO.setIdBusinessProcess(BusinessProcessType.LOANABLE_SECURITIES_RETIREMENT_CONFIRM.getCode());		
				accountsComponentTO.setIdOperationType(ParameterOperationType.LOANABLE_SECURITIES_RETIREMENT.getCode());			
				accountsComponentTO.setIdTradeOperation(tradeOperation.getIdTradeOperationPk());
				accountsComponentTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
				accountsComponentTO.setLstSourceAccounts(lstHolderAccountBalanceTO);

				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);				
			}
			
			return loanableSecuritiesRequest;
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Is Insufficient Balance Loanable Operation.
	 *
	 * @param loanableSecuritiesRequest object loanable Securities Request
	 * @return validate true or false
	 * @throws ServiceException the service exception
	 */
	public Boolean isInsufficientBalanceLoanableOperation(LoanableSecuritiesRequest loanableSecuritiesRequest) throws ServiceException{
		Boolean flag=Boolean.FALSE;
		try{			
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select loa.unblockQuantity, loas.currentQuantity From LoanableUnblockOperation loa ");
			sbQuery.append("	join loa.loanableSecuritiesOperation loas	");	
			sbQuery.append(" where loa.loanableSecuritiesRequest.idLoanableSecuritiesRequestPk= :idLoanableSecuritiesRequestPk" );	
			parameters.put("idLoanableSecuritiesRequestPk", loanableSecuritiesRequest.getIdLoanableSecuritiesRequestPk());	
			List<Object[]> lstObject =  findListByQueryString(sbQuery.toString(), parameters);
			for(Object[] object: lstObject)
			{
				BigDecimal retirementQuantity = new BigDecimal(object[0].toString());
				BigDecimal currentQuantity = new BigDecimal(object[1].toString());
				if(retirementQuantity.compareTo(currentQuantity)==1)
				{
					flag=Boolean.TRUE;
					break;
				}
			}
			return flag;
		} catch(NoResultException ex){
			   return flag;
		} catch(NonUniqueResultException ex){
			   return flag;
		}	
	}
	
	/**
	 * Find Security By Id.
	 *
	 * @param idSecurity code security
	 * @return object security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityById(String idSecurity)throws ServiceException{
		try{
			return find(Security.class, idSecurity);
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Retirement Loanable Securities Operation.
	 *
	 * @param currentDate the current date
	 * @throws ServiceException the service exception
	 */
	public void retirementLoanableSecuritiesOperation(Date currentDate)throws ServiceException{
		try{		
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			
			//obtener todas las operaciones prestables en estado confirmado y expiradas
			List<LoanableSecuritiesOperation> lstLoanableSecuritiesOperation= null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select HAB From LoanableSecuritiesOperation HAB ");
			sbQuery.append(" where  HAB.operationState = :operationState ");	
			sbQuery.append(" AND (HAB.currentQuantity = :currentQuantity  " );	
			sbQuery.append(" OR TRUNC(HAB.expirationDate) = :dateParam ) " );	
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("operationState", LoanableSecuritiesOperationStateType.CONFIRMED.getCode());
			query.setParameter("currentQuantity", BigDecimal.ZERO);			
			query.setParameter("dateParam", currentDate);
			lstLoanableSecuritiesOperation = (List<LoanableSecuritiesOperation>) query.getResultList();
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstLoanableSecuritiesOperation))
			{				
				//Agrupalos en bloques filtrado por codigo de cuenta
				List<LoanableSecuritiesOperation> lstLoanableSecuritiesOperationAux = null;
				Map<Long,List<LoanableSecuritiesOperation>> loanableSecuritiesOperationPk = new HashMap<Long, List<LoanableSecuritiesOperation>>();
				for(LoanableSecuritiesOperation loanableSecuritiesOperation : lstLoanableSecuritiesOperation){
					if(loanableSecuritiesOperationPk.get(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk()) == null)
					{
						lstLoanableSecuritiesOperationAux = new ArrayList<LoanableSecuritiesOperation>();
						lstLoanableSecuritiesOperationAux.add(loanableSecuritiesOperation);
						loanableSecuritiesOperationPk.put(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk(), lstLoanableSecuritiesOperationAux);
					}
					else
					{
						List<LoanableSecuritiesOperation> lstLoanableSecuritiesOperationAs = loanableSecuritiesOperationPk.get(loanableSecuritiesOperation.getHolderAccount().getIdHolderAccountPk());
						lstLoanableSecuritiesOperationAs.add(loanableSecuritiesOperation);
					}
				}
				//recorrer los bloques y realizar registro y confirmacion de solicitud de retiro de valores prestables
				for(List<LoanableSecuritiesOperation> lstLoanableSecuritiesOperationA : loanableSecuritiesOperationPk.values()){
					LoanableSecuritiesRequest loanableSecuritiesRequest = new LoanableSecuritiesRequest();
					loanableSecuritiesRequest.setRequestType(LoanableSecuritiesOperationRequestType.RETIREMENT_REQUEST_TYPE.getCode());
					loanableSecuritiesRequest.setRequestState(LoanableSecuritiesOperationStateType.REGISTERED.getCode());
					loanableSecuritiesRequest.setLstLoanableUnblockOperation(new ArrayList<LoanableUnblockOperation>());
					BigDecimal count = BigDecimal.ZERO;
					for(LoanableSecuritiesOperation loanableSecuritiesOperation :lstLoanableSecuritiesOperationA){//Actualizamos estado
						if(loanableSecuritiesOperation.getCurrentQuantity().compareTo(BigDecimal.ZERO)==0){
							loanableSecuritiesOperation.setOperationState(LoanableSecuritiesOperationStateType.EXPIRED.getCode());
							update(loanableSecuritiesOperation);
						}
						else{// llenar lista  operaciones de prestamo 
							if(loanableSecuritiesOperation.getCurrentQuantity().compareTo(BigDecimal.ZERO)==1){
								
								if(count.compareTo(BigDecimal.ZERO)==0)
								{							
									List<HolderAccountDetail> lstHolderAccountDetail = loanableSecuritiesOperation.getHolderAccount().getHolderAccountDetails();
									for(HolderAccountDetail holderAccountDetail : lstHolderAccountDetail)
									{
										if(holderAccountDetail.getIndRepresentative().equals(new Integer(1)))
											loanableSecuritiesRequest.setHolder(holderAccountDetail.getHolder());
									}									
									loanableSecuritiesRequest.setParticipant(loanableSecuritiesOperation.getParticipant());
									loanableSecuritiesRequest.setHolderAccount(loanableSecuritiesOperation.getHolderAccount());
									count = count.add(BigDecimal.ONE);
								}
								
								LoanableUnblockOperation loanableUnblockOperation = new LoanableUnblockOperation();
								loanableUnblockOperation.setUnblockMotive(new Long(LoanableUnblockMotiveType.AUTOMATIC_PROCESS.getCode()));
								loanableUnblockOperation.setCurrentQuantity(loanableSecuritiesOperation.getCurrentQuantity());
								loanableUnblockOperation.setUnblockQuantity(loanableSecuritiesOperation.getCurrentQuantity());								
								loanableUnblockOperation.setLoanableSecuritiesOperation(loanableSecuritiesOperation);
								
								if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode()))
								{
									List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfactA= null;
									StringBuilder sbQueryA = new StringBuilder();
									sbQueryA.append("Select HAB From LoanableSecuritiesMarketfact HAB ");
									sbQueryA.append(" where HAB.loanableSecuritiesOperation.idLoanableSecuritiesOperationPk =" + loanableSecuritiesOperation.getIdLoanableSecuritiesOperationPk());				
									Query queryA = em.createQuery(sbQueryA.toString());
									lstLoanableSecuritiesMarketfactA = (List<LoanableSecuritiesMarketfact>) queryA.getResultList();
									
									List <LoanableUnblockMarketfact> lstLoanableSecuritiesMarketfact=new ArrayList<LoanableUnblockMarketfact>();
									for (LoanableSecuritiesMarketfact loanableSecuritiesMarketfact : lstLoanableSecuritiesMarketfactA)
									{
										LoanableUnblockMarketfact loanableUnblockMarketfact = new LoanableUnblockMarketfact();
										loanableUnblockMarketfact.setMarketDate(loanableSecuritiesMarketfact.getMarketDate());
										loanableUnblockMarketfact.setMarketPrice(loanableSecuritiesMarketfact.getMarketPrice());
										loanableUnblockMarketfact.setMarketQuantity(loanableSecuritiesMarketfact.getCurrentMarketQuantity());
										loanableUnblockMarketfact.setMarketRate(loanableSecuritiesMarketfact.getMarketRate());
										lstLoanableSecuritiesMarketfact.add(loanableUnblockMarketfact);
									}
									loanableUnblockOperation.setLstLoanableUnblockMarketfact(lstLoanableSecuritiesMarketfact);
								}								
								
								loanableSecuritiesRequest.getLstLoanableUnblockOperation().add(loanableUnblockOperation);							
							}
						}
					}
					if(Validations.validateListIsNotNullAndNotEmpty(loanableSecuritiesRequest.getLstLoanableUnblockOperation())){
						loanableSecuritiesRequest= registryRetirementLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser);						
						confirmRetirementLoanableSecuritiesRequest(loanableSecuritiesRequest, loggerUser,Boolean.TRUE);
					}				
				}
			}
			
		} catch(NoResultException ex){
			 
		} catch(NonUniqueResultException ex){
			   
		}	
	}
}
