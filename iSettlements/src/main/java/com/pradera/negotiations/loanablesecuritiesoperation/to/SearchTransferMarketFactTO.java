package com.pradera.negotiations.loanablesecuritiesoperation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchTransferMarketFactTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchTransferMarketFactTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id holder market pk. */
	private Long idHolderMarketPk;
	
	/** The transfer amount. */
	private BigDecimal transferAmount;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The loanable balance. */
	private BigDecimal loanableBalance;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market date. */
	private Date marketDate;

	/**
	 * Instantiates a new search transfer market fact to.
	 */
	public SearchTransferMarketFactTO() {
		super();
	}	
	
	/**
	 * Instantiates a new search transfer market fact to.
	 *
	 * @param idHolderMarketPk the id holder market pk
	 * @param transferAmount the transfer amount
	 * @param totalBalance the total balance
	 * @param availableBalance the available balance
	 * @param loanableBalance the loanable balance
	 * @param marketPrice the market price
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 */
	public SearchTransferMarketFactTO(Long idHolderMarketPk,
			BigDecimal transferAmount, BigDecimal totalBalance,
			BigDecimal availableBalance, BigDecimal loanableBalance,
			BigDecimal marketPrice, BigDecimal marketRate, Date marketDate) {
		super();
		this.idHolderMarketPk = idHolderMarketPk;
		this.transferAmount = transferAmount;
		this.totalBalance = totalBalance;
		this.availableBalance = availableBalance;
		this.loanableBalance = loanableBalance;
		this.marketPrice = marketPrice;
		this.marketRate = marketRate;
		this.marketDate = marketDate;
	}

	/**
	 * Gets the id holder market pk.
	 *
	 * @return the id holder market pk
	 */
	public Long getIdHolderMarketPk() {
		return idHolderMarketPk;
	}

	/**
	 * Sets the id holder market pk.
	 *
	 * @param idHolderMarketPk the new id holder market pk
	 */
	public void setIdHolderMarketPk(Long idHolderMarketPk) {
		this.idHolderMarketPk = idHolderMarketPk;
	}

	/**
	 * Gets the transfer amount.
	 *
	 * @return the transfer amount
	 */
	public BigDecimal getTransferAmount() {
		return transferAmount;
	}

	/**
	 * Sets the transfer amount.
	 *
	 * @param transferAmount the new transfer amount
	 */
	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}


	/**
	 * Gets the loanable balance.
	 *
	 * @return the loanable balance
	 */
	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}


	/**
	 * Sets the loanable balance.
	 *
	 * @param loanableBalance the new loanable balance
	 */
	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}
	
}
