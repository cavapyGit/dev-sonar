package com.pradera.negotiations.loanablesecuritiesoperation.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchLoanableSecuritiesOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchLoanableSecuritiesOperationTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The CUI Holder. */
	private Holder CUIHolder;
	
	/**  The Holder Account. */
	private HolderAccount holderAccount;
	
	/**  The Request Number. */
	private Long requestNumber;
	
	/** The id state. */
	private Long idState;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/**  The Request Type. */
	private Long requestType;
	
	/** The all participant list. */
	private List<Participant> lstAllParticipant;
	
	/** The state list. */
	private List<ParameterTable> lstState;
	
	/** The Holder Account Balance List. */
	private List<SearchHolderAccountBalanceTO> lstSearchHolderAccountBalanceTO;
	
	/**
	 * The Constructor.
	 */
	public SearchLoanableSecuritiesOperationTO() {
		super();
	}
	/**
	 * Instantiates a new placement segment to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 */
	public SearchLoanableSecuritiesOperationTO(Date initialDate, Date finalDate) {
		super();
		this.initialDate = initialDate;
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the CUI holder.
	 *
	 * @return the CUI holder
	 */
	public Holder getCUIHolder() {
		return CUIHolder;
	}
	
	/**
	 * Sets the CUI holder.
	 *
	 * @param cUIHolder the new CUI holder
	 */
	public void setCUIHolder(Holder cUIHolder) {
		CUIHolder = cUIHolder;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}
	
	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}	
	
	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Long getRequestType() {
		return requestType;
	}
	
	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Long requestType) {
		this.requestType = requestType;
	}
	
	/**
	 * Gets the id state.
	 *
	 * @return the id state
	 */
	public Long getIdState() {
		return idState;
	}
	
	/**
	 * Sets the id state.
	 *
	 * @param idState the new id state
	 */
	public void setIdState(Long idState) {
		this.idState = idState;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the lst all participant.
	 *
	 * @return the lst all participant
	 */
	public List<Participant> getLstAllParticipant() {
		return lstAllParticipant;
	}
	
	/**
	 * Sets the lst all participant.
	 *
	 * @param lstAllParticipant the new lst all participant
	 */
	public void setLstAllParticipant(List<Participant> lstAllParticipant) {
		this.lstAllParticipant = lstAllParticipant;
	}
	
	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	
	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	
	/**
	 * Gets the lst search holder account balance to.
	 *
	 * @return the lst search holder account balance to
	 */
	public List<SearchHolderAccountBalanceTO> getLstSearchHolderAccountBalanceTO() {
		return lstSearchHolderAccountBalanceTO;
	}
	
	/**
	 * Sets the lst search holder account balance to.
	 *
	 * @param lstSearchHolderAccountBalanceTO the new lst search holder account balance to
	 */
	public void setLstSearchHolderAccountBalanceTO(
			List<SearchHolderAccountBalanceTO> lstSearchHolderAccountBalanceTO) {
		this.lstSearchHolderAccountBalanceTO = lstSearchHolderAccountBalanceTO;
	}

}
