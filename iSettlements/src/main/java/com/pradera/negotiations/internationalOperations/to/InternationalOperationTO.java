package com.pradera.negotiations.internationalOperations.to;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OtcOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class InternationalOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The id internat depositary. */
	private Long idInternatDepositary;
	
	/** The id internat depositary state. */
	private Integer idInternatDepositaryState;
	
	/** The id negotiation mechanism pk. */
	private Long idNegotiationMechanismPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id inter participant pk. */
	private Long idInterParticipantPk;
	
	/** The id settlement depositary pk. */
	private Long idSettlementDepositaryPk;
	
	/** The id trade depositary pk. */
	private Long idTradeDepositaryPk;

	/**
	 * Gets the id internat depositary.
	 *
	 * @return the id internat depositary
	 */
	public Long getIdInternatDepositary() {
		return idInternatDepositary;
	}

	/**
	 * Sets the id internat depositary.
	 *
	 * @param idInternatDepositary the new id internat depositary
	 */
	public void setIdInternatDepositary(Long idInternatDepositary) {
		this.idInternatDepositary = idInternatDepositary;
	}

	/**
	 * Gets the id internat depositary state.
	 *
	 * @return the id internat depositary state
	 */
	public Integer getIdInternatDepositaryState() {
		return idInternatDepositaryState;
	}

	/**
	 * Sets the id internat depositary state.
	 *
	 * @param idInternatDepositaryState the new id internat depositary state
	 */
	public void setIdInternatDepositaryState(Integer idInternatDepositaryState) {
		this.idInternatDepositaryState = idInternatDepositaryState;
	}

	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}

	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}

	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id inter participant pk.
	 *
	 * @return the id inter participant pk
	 */
	public Long getIdInterParticipantPk() {
		return idInterParticipantPk;
	}

	/**
	 * Sets the id inter participant pk.
	 *
	 * @param idInterParticipantPk the new id inter participant pk
	 */
	public void setIdInterParticipantPk(Long idInterParticipantPk) {
		this.idInterParticipantPk = idInterParticipantPk;
	}

	/**
	 * Gets the id settlement depositary pk.
	 *
	 * @return the id settlement depositary pk
	 */
	public Long getIdSettlementDepositaryPk() {
		return idSettlementDepositaryPk;
	}

	/**
	 * Sets the id settlement depositary pk.
	 *
	 * @param idSettlementDepositaryPk the new id settlement depositary pk
	 */
	public void setIdSettlementDepositaryPk(Long idSettlementDepositaryPk) {
		this.idSettlementDepositaryPk = idSettlementDepositaryPk;
	}

	/**
	 * Gets the id trade depositary pk.
	 *
	 * @return the id trade depositary pk
	 */
	public Long getIdTradeDepositaryPk() {
		return idTradeDepositaryPk;
	}

	/**
	 * Sets the id trade depositary pk.
	 *
	 * @param idTradeDepositaryPk the new id trade depositary pk
	 */
	public void setIdTradeDepositaryPk(Long idTradeDepositaryPk) {
		this.idTradeDepositaryPk = idTradeDepositaryPk;
	}
}