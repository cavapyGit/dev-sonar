package com.pradera.negotiations.internationalOperations.facade;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.exception.ServiceException;
import com.pradera.negotiations.internationalOperations.service.NegotiationSchedulerServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NegotiationSchedulerServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
public class NegotiationSchedulerServiceFacade {

	/** The scheduler service bean. */
	@EJB
	NegotiationSchedulerServiceBean schedulerServiceBean;
	
	/**
	 * Update timer.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @throws ServiceException the service exception
	 */
	public <T> void updateTimer(T t) throws ServiceException {
		schedulerServiceBean.updateTimer(t);
	}
	
	/**
	 * Delete timer.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @throws ServiceException the service exception
	 */
	public <T> void deleteTimer(T t) throws ServiceException {
		schedulerServiceBean.requestDelete(t);
	}
}
