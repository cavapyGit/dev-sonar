package com.pradera.negotiations.internationalOperations.view;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.OperationType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.DepositaryOperationFile;
import com.pradera.model.negotiation.InternationalMarketFactOperation;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.InternationalParticipant;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.model.negotiation.type.InternationalOperationTransferType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.negotiations.internationalOperations.facade.InternationalOperationServiceFacade;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationFilter;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationTO;
import com.pradera.settlements.utils.PropertiesConstants;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InternationalOperationMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/08/2013
 */
@DepositaryWebBean
public class InternationalOperationMgmtBean extends GenericBaseBean implements Serializable{
	
	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The internationalOperationServiceFacade. */
	@EJB
	private InternationalOperationServiceFacade internationalOperationServiceFacade;
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	/** The securitiesHelperBean. */
	@Inject
	SecuritiesHelperBean securitiesHelperBean;
	
	/** The helper component facade. */
	@EJB
	/** The helperComponentFacade. */
	HelperComponentFacade 	 helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The internationalOperation. */
	private InternationalOperation internationalOperation;
	
	/** The internationalOperationFilter. */
	private InternationalOperationFilter internationalOperationFilter;
	
	/**The internationalOperationDataModel. */
	private GenericDataModel<InternationalOperationSearch> internationalOperationDataModel;
	
	/** The internationa operation generate. */
	private GenericDataModel<InternationalOperationSearch> internationalOperationGenerate;
	
	/**The internationalOperationSearch. */
	private InternationalOperationSearch internationalOperationSearch;
	
	/** The intern operation multiple. */
	private InternationalOperationSearch[] internOperationMultiple;

	/** The operations internat attributtes. */
	private OperationsInternatAttributtes operationsInternatAttributtes;
	
	/** The reject reason list. */
	private List<ParameterTable> rejectReasonList;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The participant service bean. */
	@Inject
	private ParticipantServiceBean participantServiceBean;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;

	/** The view other motive. */
	private boolean viewOtherMotive;
	
	/** The lst source account to. */
	private List<HolderAccountHelperResultTO> lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	
	/** The source account to. */
	private HolderAccountHelperResultTO sourceAccountTo = new HolderAccountHelperResultTO();
	
	/** The map state holder account. */
	private Map<Integer, String> mapStateHolderAccount = new HashMap<Integer,String>();
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/** The is sending securities. */
	private boolean isSendingSecurities;
	
	/** The holder settlement details. */
	private Holder holderSettlementDetails;
	
	/** The international market fact operation. */
	private InternationalMarketFactOperation internationalMarketFactOperation;
	
	/** The market service bean. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;
	
	/** The bl has market fact. */
	private boolean blHasMarketFact;
	
	/**
	 * Default Constructor.
	 */
	public InternationalOperationMgmtBean() {
	}

	/**
	 * init Method.
	 */
	@PostConstruct
	public void init() {
		this.operationsInternatAttributtes = new OperationsInternatAttributtes();//Initialize Objeto who handle All attributtes
		try{
			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
			loadLocalParticipants();//Cargamos los participantes
			loadInternationalDepositary();//Cargamos las depositarias con relacion a los participantes
			loadIntertionalParticipant();
			loadPrivilegeUser();
			loadOperationsType();
			loadOperationsStateType();
		}catch(ServiceException ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		this.internationalOperationFilter = new InternationalOperationFilter();
		this.internationalOperationFilter.setSecurity(new Security());
		this.internationalOperationFilter.setLocalParticipant(new Participant());
		this.internationalOperationFilter.setHolderAccount(new HolderAccount());
		this.internationalOperationFilter.setStartDate(new Date());
		this.internationalOperationFilter.setEndDate(new Date());
		
		this.internationalOperation = new InternationalOperation();
		this.internationalOperation.setTradeOperation(new TradeOperation());
		this.internationalOperation.setLocalParticipant(new Participant());
		this.internationalOperation.setInternationalParticipant(new InternationalParticipant());
		this.internationalOperation.setSettlementDepository(new InternationalDepository());
		this.internationalOperation.setHolderAccount(new HolderAccount());
		this.internationalOperation.setOperationDate(new Date());
		this.internationalOperation.setSettlementDate(new Date());
		this.internationalOperation.setTradeDepository(new InternationalDepository());
		this.internationalOperation.setSecurities(new Security());
		this.holderSettlementDetails = new Holder();
		this.rejectReasonList = new ArrayList<ParameterTable>();
		
		try{			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				this.internationalOperation.getLocalParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				this.internationalOperationFilter.getLocalParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			}
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Check motive listener.
	 */
	public void checkMotiveListener(){
		this.viewOtherMotive = false;
		if(Validations.validateIsNotNullAndPositive(this.internationalOperation.getTradeOperation().getRejectMotive()) &&			
		   (this.internationalOperation.getTradeOperation().getRejectMotive().equals(MasterTableType.PARAMETER_TABLE_ANULATE_OTHER_MOTIVE.getLngCode().intValue()) ||
		    this.internationalOperation.getTradeOperation().getRejectMotive().equals(MasterTableType.PARAMETER_TABLE_REJECT_OTHER_MOTIVE.getLngCode().intValue()) ||
		    this.internationalOperation.getTradeOperation().getRejectMotive().equals(MasterTableType.PARAMETER_TABLE_CANCEL_OTHER_MOTIVE.getLngCode().intValue()))){
			
			this.viewOtherMotive = true;
		}
	}
	
	/**
	 * This method is called before saving international operation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void saveInternationalOperationListener() throws ServiceException{
		this.rejectReasonList = new ArrayList<ParameterTable>();
		this.internationalOperation.getTradeOperation().setRejectMotive(null);
		this.internationalOperation.getTradeOperation().setRejectMotiveOther(null);
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		viewOtherMotive = false;
		boolean showMotive = false;
		
		if(Validations.validateIsNull(internationalOperation.getHolderAccount())||
			Validations.validateIsNull(internationalOperation.getHolderAccount().getIdHolderAccountPk())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_MSG_VALID_HOLDER_ACCOUNT,null);
			JSFUtilities.showSimpleValidationDialog();	
			return;
		}
		
		if((ViewOperationsType.REGISTER.getCode().equals(this.getViewOperationType()) ||
			ViewOperationsType.MODIFY.getCode().equals(this.getViewOperationType())) && 
			!isSendingSecurities){
				
			ArrayList<InternationalMarketFactOperation> lstMarketFact = new ArrayList<InternationalMarketFactOperation>();
			this.internationalMarketFactOperation.setQuantity(this.internationalOperation.getStockQuantity());
			lstMarketFact.add(internationalMarketFactOperation);
			this.internationalOperation.setInternationalMarketFactOperations(lstMarketFact);					
		}	
					
		if(this.getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
			showMessageOnDialogOnlyKey(PropertiesConstants.CONFIRM_HEADER_RECORD, PropertiesConstants.INTER_REGISTER_CONFIRM);
			JSFUtilities.executeJavascriptFunction("PF('idSaveDialogW').show()");
		  return;
		}
		switch(ViewOperationsType.get(this.getViewOperationType())){
			case APPROVE: 
				showMessageOnDialog(PropertiesConstants.CONFIRM_HEADER_RECORD,null, PropertiesConstants.INTER_APPROVE_CONFIRM,new Object[]{this.internationalOperation.getOperationNumber()});break;
			case ANULATE:
				parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_ANULATE_MOTIVE_INT_OPERATION.getCode());				
				showMotive = true;
			case CONFIRM:
				showMessageOnDialog(PropertiesConstants.CONFIRM_HEADER_RECORD,null, PropertiesConstants.INTER_CONFIRM_CONFIRM,new Object[]{this.internationalOperation.getOperationNumber()});break;
			case REJECT:
				parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_REJECT_MOTIVE_INT_OPERATION.getCode());
				showMotive = true;
			case SETTLEMENT:
				showMessageOnDialog(PropertiesConstants.CONFIRM_HEADER_RECORD,null, PropertiesConstants.INTER_SETTLE_CONFIRM,new Object[]{this.internationalOperation.getOperationNumber()});break;
			case CANCEL:
				parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_CANCEL_MOTIVE_INT_OPERATION.getCode());
				showMotive = true;
			case MODIFY: 
				showMessageOnDialog(PropertiesConstants.CONFIRM_HEADER_RECORD,null, PropertiesConstants.INTER_MODIFY_CONFIRM,new Object[]{this.internationalOperation.getOperationNumber()});break;
		}
		
		if(showMotive){
			rejectReasonList = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
			JSFUtilities.executeJavascriptFunction("PF('idMotiveDialogW').show()");
			//showMessageOnDialog(PropertiesConstants.MOTIVE,null, null,null);
		}else{		
			JSFUtilities.executeJavascriptFunction("PF('idSaveDialogW').show()");
		}

	}
	
	/**
	 * Check motive fields listener.
	 */
	public void checkMotiveFieldsListener(){
		if(Validations.validateIsNotNullAndPositive(this.internationalOperation.getTradeOperation().getRejectMotive())){
			switch(ViewOperationsType.get(this.getViewOperationType())){
			case ANULATE:
				showMessageOnDialog(PropertiesConstants.CONFIRM_HEADER_RECORD,null, PropertiesConstants.INTER_REVERSE_CONFIRM,new Object[]{this.internationalOperation.getOperationNumber()});break;
			case REJECT:
				showMessageOnDialog(PropertiesConstants.CONFIRM_HEADER_RECORD,null, PropertiesConstants.INTER_REJECT_CONFIRM,new Object[]{this.internationalOperation.getOperationNumber()});break;
			case CANCEL:
				showMessageOnDialog(PropertiesConstants.CONFIRM_HEADER_RECORD,null, PropertiesConstants.INTER_CANCEL_CONFIRM,new Object[]{this.internationalOperation.getOperationNumber()});break;
			}
			
			//IF A OTHER MOTIVE IS SELECTED
			if(this.internationalOperation.getTradeOperation().getRejectMotive().equals(MasterTableType.PARAMETER_TABLE_ANULATE_OTHER_MOTIVE.getLngCode().intValue()) ||
			   this.internationalOperation.getTradeOperation().getRejectMotive().equals(MasterTableType.PARAMETER_TABLE_REJECT_OTHER_MOTIVE.getLngCode().intValue()) ||
			   this.internationalOperation.getTradeOperation().getRejectMotive().equals(MasterTableType.PARAMETER_TABLE_CANCEL_OTHER_MOTIVE.getLngCode().intValue())){
					//IF OTHER MOTIVE FIELD IS EMPTY
					if(Validations.validateIsNotNullAndNotEmpty(this.internationalOperation.getTradeOperation().getRejectMotiveOther())){
						JSFUtilities.executeJavascriptFunction("PF('idSaveDialogW').show()");
					}else{
						JSFUtilities.addContextMessage("frmInternationalOperations:idOtherMotive",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.OTHER_MOTIVE_EMPTY),
						PropertiesUtilities.getMessage(PropertiesConstants.OTHER_MOTIVE_EMPTY));
					}
			}else{
				JSFUtilities.executeJavascriptFunction("PF('idSaveDialogW').show()");
			}
		}else{
			JSFUtilities.addContextMessage("frmInternationalOperations:cboReason",FacesMessage.SEVERITY_ERROR,PropertiesUtilities.getMessage(PropertiesConstants.MOTIVE_NOT_SELECTED),
			PropertiesUtilities.getMessage(PropertiesConstants.MOTIVE_NOT_SELECTED));
		}
	}
	
	/**
	 * This method saves international operation.
	 */
	@LoggerAuditWeb
	public void saveInternationalOprRegistration(){
		try{		
			if(this.getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
				InternationalOperation internationalOperationSaved = internationalOperationServiceFacade.registerInternationalOperation(internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTERNATIONAL_OPERATION_REGISTER_SUCCESS, new Object[]{internationalOperationSaved.getOperationNumber()});
			}else if(this.getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
				InternationalOperation interOperation =  internationalOperationServiceFacade.modifyInternationalOperation(this.internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTERNATIONAL_OPERATION_MODIFY_SUCCESS, new Object[]{interOperation.getOperationNumber()});
			}else if(this.getViewOperationType().equals(ViewOperationsType.APPROVE.getCode())){
				InternationalOperation interOperation =  internationalOperationServiceFacade.approveInternationalOperation(this.internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTER_APPROVE_SUCCESS, new Object[]{interOperation.getOperationNumber()});
			}else if(this.getViewOperationType().equals(ViewOperationsType.ANULATE.getCode())){
				InternationalOperation interOperation =  internationalOperationServiceFacade.anulateInternationalOperation(this.internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTER_REVERSE_SUCCESS, new Object[]{interOperation.getOperationNumber()});
			}else if(this.getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
				InternationalOperation interOperation =  internationalOperationServiceFacade.confirmInternationalOperation(this.internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTER_CONFIRM_SUCCESS, new Object[]{interOperation.getOperationNumber()});
			}else if(this.getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
				InternationalOperation interOperation =  internationalOperationServiceFacade.rejectInternationalOperation(this.internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTER_REJECT_SUCCESS, new Object[]{interOperation.getOperationNumber()});
			}else if(this.getViewOperationType().equals(ViewOperationsType.SETTLEMENT.getCode())){
				InternationalOperation interOperation =  internationalOperationServiceFacade.settlementInternationalOperation(this.internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTER_SETTLEMENT_SUCCESS, new Object[]{interOperation.getOperationNumber()});
			}else if(this.getViewOperationType().equals(ViewOperationsType.CANCEL.getCode())){
				InternationalOperation interOperation =  internationalOperationServiceFacade.cancelInternationalOperation(this.internationalOperation);
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.INTER_CANCEL_SUCCESS, new Object[]{interOperation.getOperationNumber()});
			}
		}catch (ServiceException se) {
			switch (se.getErrorService()) {
			case NEGATIVE_BALANCE:
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.INTERNATIONAL_MSG_NEGATIVE_BALANCE, null);
				break;

			default:
				break;
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		searchInternationalOperation();
		if(this.securitiesHelperBean.getSecurityDescription()!=null){
			this.securitiesHelperBean.setSecurityDescription(null);
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfwInterEndTransactionSamePage').show()");
	}
	
	/**
	 * This method search international operation according to search criteria.
	 */ 
	public void searchInternationalOperation(){
		try {
			if(isViewOperationRegister()){
				cleanInterOperations("SEARCH");
			}else{
				internationalOperationDataModel = new GenericDataModel<InternationalOperationSearch>(internationalOperationServiceFacade.searchInternationalOperation(internationalOperationFilter));
			}			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clears International operations.
	 *
	 * @param val the val
	 */
	public void cleanInterOperations(String val){
		securitiesHelperBean.setSecurityDescription(null);
		if(val.equals("SEARCH")){
			internationalOperationFilter = new InternationalOperationFilter();
			internationalOperationFilter.setSecurity(new Security());
			internationalOperationFilter.setLocalParticipant(new Participant());
			internationalOperationFilter.setHolderAccount(new HolderAccount());
			internationalOperationSearch = new InternationalOperationSearch();
			internationalOperationDataModel  = null;			
			internationalOperationFilter.setTransferType(null);
			internationalOperationFilter.setIdSettlementDeposiroty(null);
			internationalOperationFilter.setIdInterParticipantPk(null);
			internationalOperationFilter.setIdState(null);
			internationalOperationFilter.setIdSettlementType(null);
			internationalOperationFilter.setStartDate(new Date());
			internationalOperationFilter.setEndDate(new Date());
			internationalOperationFilter.setIdIsinCodePk(null);
			
			if(!isViewOperationRegister()){
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idLocalParticipant");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idOperationType");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idSettlement");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idInterParticipant");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idState");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idSettlementType");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idStartDate");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idEndDate");
				JSFUtilities.resetComponent("frmSearchInternationalOpr:idSourceSecurity");
			}
		}
		if(val.equals("REGISTER")){
			internationalOperation = new InternationalOperation();		
			internationalOperation.setLocalParticipant(new Participant());
			internationalOperation.setInternationalParticipant(new InternationalParticipant());
			internationalOperation.setSettlementDepository(new InternationalDepository());
			internationalOperation.setTradeDepository(new InternationalDepository());
			internationalOperation.setHolderAccount(new HolderAccount());
			internationalOperation.setOperationDate(new Date());
			internationalOperation.setSecurities(new Security());
			internationalOperation.setSettlementDate(new Date());
			internationalOperation.setStockQuantity(null);
			internationalOperation.setCashAmount(null);
			internationalOperation.setOperationPrice(null);
			internationalOperation.setSettlementAmount(null);
			internationalOperation.setTradeOperation(new TradeOperation());
			internationalOperation.getTradeDepository().setIdInternationalDepositoryPk(NegotiationConstant.CLEARSTREAM);
			internationalMarketFactOperation = null;
			holderSettlementDetails = new Holder();
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();		
			sourceAccountTo = new HolderAccountHelperResultTO();
			isSendingSecurities = true;
			
			JSFUtilities.resetComponent("frmInternationalOperations:idNegotiation");
			JSFUtilities.resetComponent("frmInternationalOperations:idParticipant");
			JSFUtilities.resetComponent("frmInternationalOperations:idInternationalOperation");
			JSFUtilities.resetComponent("frmInternationalOperations:idSettlement");
			JSFUtilities.resetComponent("frmInternationalOperations:idInterParticipant");
			JSFUtilities.resetComponent("frmInternationalOperations:idSourceSecurity");
			JSFUtilities.resetComponent("frmInternationalOperations:txtSecurityStock");
			JSFUtilities.resetComponent("frmInternationalOperations:txtInternationalAmount");
			JSFUtilities.resetComponent("frmInternationalOperations:txtInternationalPrice");
			JSFUtilities.resetComponent("frmInternationalOperations:txtSettlementAmount");
			JSFUtilities.resetComponent("frmInternationalOperations:idCuiSourceHolder");
		}
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			this.internationalOperation.getLocalParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			this.internationalOperationFilter.getLocalParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
	}
	
	/**
	 * Load local participants.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadLocalParticipants() throws ServiceException{
		Participant participantFilter = new Participant();
		participantFilter.setState(ParticipantStateType.REGISTERED.getCode());
		
//		List<Participant> participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
		List<Participant> participantList = participantServiceBean.getLisParticipantServiceBean(participantFilter);
		
		participantFilter.setState(ParticipantStateType.BLOCKED.getCode());
		if(participantList!=null && !participantList.isEmpty()){
//			participantList.addAll(accountsFacade.getLisParticipantServiceBean(participantFilter));
			participantList.addAll(participantServiceBean.getLisParticipantServiceBean(participantFilter));
		}else{
//			participantList = accountsFacade.getLisParticipantServiceBean(participantFilter);
			participantList = participantServiceBean.getLisParticipantServiceBean(participantFilter);
		}
		this.operationsInternatAttributtes.setParticipantLocal(participantList);
	}
	/**
	 * Load international depositary.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadInternationalDepositary() throws ServiceException{
		InternationalOperationTO  internationalOperationTO  = new InternationalOperationTO ();
		internationalOperationTO.setIdInternatDepositaryState(InternationalDepositoryStateType.ACTIVE.getCode());
		List<InternationalDepository> interDepositories =  this.internationalOperationServiceFacade.internationalDepositoriesServiceFacade(internationalOperationTO);
		this.operationsInternatAttributtes.setInterDepositories(interDepositories);
	}
	/**
	 * Load intertional participant.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadIntertionalParticipant() throws ServiceException{
		List<InternationalParticipant> intParticipantList = this.internationalOperationServiceFacade.getInternationalParticipantListServiceFacade();
		this.operationsInternatAttributtes.setInterParticipantList(intParticipantList);
		this.operationsInternatAttributtes.setInterParticipantByDepostary(new ArrayList<InternationalParticipant>());
	}
	/**
	 * Gets the operations internat attributtes.
	 *
	 * @return the operations internat attributtes
	 */
	public OperationsInternatAttributtes getOperationsInternatAttributtes() {
		return operationsInternatAttributtes;
	}
	/**
	 * Sets the operations internat attributtes.
	 *
	 * @param operationsInternatAttributtes the new operations internat attributtes
	 */
	public void setOperationsInternatAttributtes(OperationsInternatAttributtes operationsInternatAttributtes) {
		this.operationsInternatAttributtes = operationsInternatAttributtes;
	}
	
	/**
	 * Validate holder account handler.
	 */
	public void validateHolderAccountHandler(){
		
		this.internationalOperation.getHolderAccount().setIdHolderAccountPk(sourceAccountTo.getAccountPk());
		this.internationalOperation.getHolderAccount().setAccountNumber(sourceAccountTo.getAccountNumber());
		
		HolderAccountTO holderAccountTO = new HolderAccountTO();
		Integer accountNumber = this.internationalOperation.getHolderAccount().getAccountNumber();
		if(accountNumber==null){
			this.internationalOperation.setHolderAccount(new HolderAccount());
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_HOLDERACCOUNT,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		holderAccountTO.setHolderAccountNumber(accountNumber);
		holderAccountTO.setNeedHolder(Boolean.TRUE);
		holderAccountTO.setNeedParticipant(Boolean.TRUE);
		holderAccountTO.setNeedBanks(Boolean.FALSE);
		holderAccountTO.setParticipantTO(internationalOperation.getLocalParticipant().getIdParticipantPk());
		
		HolderAccount holderAccountTemp = null; 
		try {
			holderAccountTemp = accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		if(holderAccountTemp!=null && !(holderAccountTemp.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()))){//Validate holderaccount state
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_HOLDERACCOUNT_STATE,null);
			JSFUtilities.showSimpleValidationDialog();	
			this.internationalOperation.setHolderAccount(new HolderAccount());
			return;
		}
		if(holderAccountTemp!=null && !holderAccountTemp.validateIfRntIsRegistered()){//validate rnt state
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_HOLDERACCOUNT_HOLDER_STATE,null);
			JSFUtilities.showSimpleValidationDialog();	
			this.internationalOperation.setHolderAccount(new HolderAccount());
			return;
		}
		internationalMarketFactOperation =  new InternationalMarketFactOperation();
		internationalMarketFactOperation.setMarketDate(this.getCurrentSystemDate());
		
		MarketFactBalanceHelpTO marketFactBalance = fillMarketFactData();
		if(!internationalOperation.getTransferType().equals(InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode()))
			verifyExistMarketFact(marketFactBalance);
		JSFUtilities.resetComponent("frmInternationalOperations:plnIssuerCode");
	}
	
	
	/**
	 * Valid security search.
	 */
	public void validSecuritySearch(){
		String securityCode = internationalOperationFilter.getSecurity().getIdSecurityCodePk();
		if(securityCode == null){			
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,null,
					PropertiesConstants.OTC_OPERATIONS_VALIDATION_SECURITY_NULL,null);
			JSFUtilities.showSimpleValidationDialog();			
			securitiesHelperBean.setSecurityDescription(null);
			internationalOperationFilter.setIdIsinCodePk(null);
			return;
		}else{
			internationalOperationFilter.setIdIsinCodePk(securityCode);	
		}
	}
	
	/**
	 * Validate security handler.
	 */
	public void validateSecurityHandler(){
		String isinCode = this.internationalOperation.getSecurities().getIdSecurityCodePk();
		if(isinCode==null){//Validamos si el valor existe 
//			showMessageOnDialog(
//					PropertiesConstants.MESSAGES_ALERT,null,
//					PropertiesConstants.OTC_OPERATIONS_VALIDATION_SECURITY_NULL,null);
//			JSFUtilities.showSimpleValidationDialog();			
//			cleanSettlementFields();
			return;
		}
		/*
		Integer sizeIsinCode = Integer.parseInt(PropertiesUtilities.getConstantsMessage(PropertiesConstants.SECURITY_ISIN_LENGTH));
		if(isinCode.length()==0){//Si no se ingreso nada
			return;
		}else if(isinCode.length()!=sizeIsinCode){//Si el tamaño del codigo ISIN coincide con el ingresado
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_CARACTERS,null);
			JSFUtilities.showSimpleValidationDialog();	
			cleanSettlementFields();
			return;
		}*/

		SecurityTO securityTO = new SecurityTO();
		securityTO.setIdSecurityCodePk(isinCode);
		
		Security securityTemp = null;
		try {
			securityTemp = helperComponentFacade.findSecurityComponentServiceFacade(securityTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		Date settlementDate = this.internationalOperation.getSettlementDate();
		boolean blError=false;
		if(securityTemp==null || securityTemp.getCurrency()==null || CurrencyType.get(securityTemp.getCurrency())==null || securityTemp.getIndTraderSecondary()==null ||
		   ( InstrumentType.FIXED_INCOME.getCode().equals(securityTemp.getInstrumentType()) && securityTemp.getExpirationDate()==null)){
			//Si el valor es Null, no tiene emision ni Emisor O si el valor no tiene moneda O si la fecha de emision O fecha de experiacion es null
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,null);
			JSFUtilities.showSimpleValidationDialog();	
			blError=true;
			return;
		}else if(!securityTemp.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){//Validamos si el valor esta REGISTRADO
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATIONS_VALIDATION_SECURITY_STATE,null);
			JSFUtilities.showSimpleValidationDialog();
			blError=true;
			return;
		}else if(!securityTemp.getSecuritySource().equals(SecuritySourceType.FOREIGN.getCode())){//Validamos si el valor es EXTRANJERO
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_VALIDATION_SECURITY_FOREIGN,null);
			JSFUtilities.showSimpleValidationDialog();
			blError=true;
			return;
		}else if(settlementDate!=null && InstrumentType.FIXED_INCOME.getCode().equals(securityTemp.getInstrumentType())){
			if(securityTemp.getExpirationDate().before(settlementDate)){//Validamos la fecha de expiracion del valor menor ala fecha de liquidacion
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_VALIDATION_SETTLEMENT_DATE,null);
				JSFUtilities.showSimpleValidationDialog();
				blError=true;
				return;
			}
		}
		//1.- VALIDAMOS EL EMISOR
		if(securityTemp.getIssuer()!=null){
			//Validamos que el valor tenga al menos un emisor
			if(!securityTemp.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){//Si tiene estado diferente a REGISTRADO
				blError=true;
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_ISSUER,null);
				JSFUtilities.showSimpleValidationDialog();	
				return;
			}
		}else{
			blError=true;
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,null);
			JSFUtilities.showSimpleValidationDialog();	
			return;
		}
		//2.- VALIDAMOS LA EMISION
		if(securityTemp.getIssuance()!=null){//Validamos que el valor tenga al menos un emisor
			if(!securityTemp.getIssuance().getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){//Si tiene emisor con estado diferente a REGISTRADO
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_ISSUER,null);
				JSFUtilities.showSimpleValidationDialog();
				blError=true;
				return;
			}
		}else{
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,
					PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,null);
			JSFUtilities.showSimpleValidationDialog();
			blError=true;
			return;
		}
		
		Security securityDepositary = null;
		try{
			InternationalOperationTO internationalOperationTO = new InternationalOperationTO();
			internationalOperationTO.setIdIsinCodePk(securityTemp.getIdSecurityCodePk());
			internationalOperationTO.setIdInternatDepositary(this.internationalOperation.getSettlementDepository().getIdInternationalDepositoryPk());
			securityDepositary = internationalOperationServiceFacade.getSecurityDepositaryServiceFacade(internationalOperationTO);
		}catch(ServiceException e){
		}
		
		if(securityDepositary==null){//Validamos que el valor esta habilitado para negociar con la depositaria
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.SECURITY_DEPOSITARY_EXIST,null);
			JSFUtilities.showSimpleValidationDialog();
			blError=true;
			return;
		}
		
		cleanSettlementFields(blError);
		if(securityTemp.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
			this.operationsInternatAttributtes.setSecurityIsFixedInstrument(Boolean.TRUE);
		}else{
			this.operationsInternatAttributtes.setSecurityIsFixedInstrument(Boolean.FALSE);
		}
		//JSFUtilities.resetComponent("frmInternationalOperations:pnlValor");
		this.internationalOperation.setSecurities(securityTemp);//Asignate security finded with object International Operation
	}
	
	/**
	 * Clean settlement fields.
	 *
	 * @param blError the bl error
	 */
	public void cleanSettlementFields(boolean blError){
		if(blError){
			securitiesHelperBean.setSecurityDescription(null);
			this.internationalOperation.setSecurities(new Security());
		}
		this.internationalOperation.setHolderAccount(new HolderAccount());
		this.internationalOperation.setStockQuantity(null);
		this.internationalOperation.setCashAmount(null);
		this.internationalOperation.setOperationPrice(null);
		this.internationalOperation.setSettlementAmount(null);
		holderSettlementDetails=new Holder();
		lstSourceAccountTo= new ArrayList<HolderAccountHelperResultTO>();
	}
	
	/**
	 * Validate stock quantity handler.
	 */
	public void validateStockQuantityHandler(){
		BigDecimal stockQuantity = this.internationalOperation.getStockQuantity();
		if (Validations.validateIsNotNullAndNotEmpty(stockQuantity) && stockQuantity.compareTo(BigDecimal.ZERO) >0)
		{
			InternationalOperationTO internationalOperationTO = new InternationalOperationTO();
			internationalOperationTO.setIdHolderAccountPk(this.internationalOperation.getHolderAccount().getIdHolderAccountPk());
			internationalOperationTO.setIdParticipantPk(this.internationalOperation.getLocalParticipant().getIdParticipantPk());
			internationalOperationTO.setIdIsinCodePk(this.internationalOperation.getSecurities().getIdSecurityCodePk());
			
			BigDecimal dematerializationBalance = this.internationalOperation.getSecurities().getDesmaterializedBalance();
			BigDecimal physicalBalance = this.internationalOperation.getSecurities().getPhysicalBalance();
			BigDecimal nominalValue = this.internationalOperation.getSecurities().getCurrentNominalValue();
			if(Validations.validateIsNull(nominalValue)){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_INCONSISTENT,null);
				JSFUtilities.showSimpleValidationDialog();	
				this.internationalOperation.setStockQuantity(null);
				return;
			}
			if(InstrumentType.FIXED_INCOME.getCode().equals(internationalOperation.getSecurities().getInstrumentType())){
				internationalOperation.setCashAmount(stockQuantity.multiply(nominalValue));
				if(this.isViewOperationModify()){
					validateOperationPriceHandler();
				}
			}
			if(internationalOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
				if(dematerializationBalance!=null && stockQuantity.intValue()>dematerializationBalance.intValue()){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_VALIDATION_DEMATERIALIZED_QUANTITY,null);
					JSFUtilities.showSimpleValidationDialog();	
					internationalOperation.setStockQuantity(null);
					return;					
				}				
			}else if(internationalOperation.getTransferType().equals(InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode())){
				if(physicalBalance!=null && stockQuantity.intValue()>physicalBalance.intValue()){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_VALIDATION_PHYSICAL_QUANTITY,null);
					JSFUtilities.showSimpleValidationDialog();	
					internationalOperation.setStockQuantity(null);
					return;					
				}
			}
		} else {
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_STOCK_QUANTITY,null);
			JSFUtilities.showSimpleValidationDialog();	
			this.internationalOperation.setStockQuantity(null);
			return;
		}
		
	}
	
	/**
	 * Validate cash amount handler.
	 */
	public void validateCashAmountHandler(){
		BigDecimal cashAmount = this.internationalOperation.getCashAmount();
		BigDecimal nominalValue = this.internationalOperation.getSecurities().getCurrentNominalValue();
		if (Validations.validateIsNotNullAndNotEmpty(cashAmount) && cashAmount.compareTo(BigDecimal.ZERO) >0 &&
			Validations.validateIsNotNullAndNotEmpty(nominalValue) && nominalValue.compareTo(BigDecimal.ZERO) >0 &&
			!InstrumentType.FIXED_INCOME.getCode().equals(this.internationalOperation.getSecurities().getInstrumentType())){
				if(cashAmount.intValue()>=nominalValue.intValue()){
					this.internationalOperation.setStockQuantity(cashAmount.divide(nominalValue, 2, RoundingMode.HALF_UP));
					//security amount can't be decimal number
					String strStockQ = internationalOperation.getStockQuantity().toString();
					int posDec = strStockQ.indexOf(".");
					String strSecurityAmount= strStockQ.substring(posDec, strStockQ.length());
					
					if(!strSecurityAmount.equals(".00")){
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_CASH_AMOUNT_INVALID,null);
						JSFUtilities.showSimpleValidationDialog();
						internationalOperation.setStockQuantity(null);
						internationalOperation.setCashAmount(null);
						return;
					}
					// || this.isViewOperationRegister()
					if(this.isViewOperationModify() || Validations.validateIsNotNullAndNotEmpty(internationalOperation.getOperationPrice())){
						validateOperationPriceHandler();
					}
				}else{
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_CASH_AMOUNT_INVALID,null);
					JSFUtilities.showSimpleValidationDialog();	
					internationalOperation.setCashAmount(null);
					internationalOperation.setStockQuantity(null);
					internationalOperation.setOperationPrice(null);
					internationalOperation.setSettlementAmount(null);
					return;
				}
		} else {
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_STOCK_QUANTITY,null);
			JSFUtilities.showSimpleValidationDialog();	
			this.internationalOperation.setStockQuantity(null);
			return;
		}
	}
	
	/**
	 * Validate operation price handler.
	 */
	public void validateOperationPriceHandler(){
		BigDecimal stockQuantity = this.internationalOperation.getStockQuantity();
		BigDecimal operationPrice = this.internationalOperation.getOperationPrice();
		if (Validations.validateIsNotNullAndNotEmpty(stockQuantity) && stockQuantity.compareTo(BigDecimal.ZERO) >0 &&
			Validations.validateIsNotNullAndNotEmpty(operationPrice) && operationPrice.compareTo(BigDecimal.ZERO) >0 ) {
			
			this.internationalOperation.setSettlementAmount(stockQuantity.multiply(operationPrice));
		} else {
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_STOCK_QUANTITY,null);
			JSFUtilities.showSimpleValidationDialog();	
			this.internationalOperation.setStockQuantity(null);
		}
	}
	
	/**
	 * Selected international operation.
	 *
	 * @param idInternationOperationPk the id internation operation pk
	 * @return the string
	 */
	public String selectedInternationalOperation(Long idInternationOperationPk){
		blHasMarketFact=false;
		InternationalOperation interOperation = null;		
		try{
			interOperation = internationalOperationServiceFacade.getInternationalOperationServiceFacade(idInternationOperationPk);
		}catch(ServiceException ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		if(interOperation!=null){
			this.internationalOperation = interOperation;
			if(InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode().equals(this.internationalOperation.getTransferType())){
				this.internationalMarketFactOperation = this.internationalOperation.getInternationalMarketFactOperations().get(0);
				isSendingSecurities = false;
			} else{
				isSendingSecurities = true;
			}
			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				Participant localParticipant = new Participant();
				localParticipant.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				this.internationalOperation.setLocalParticipant(localParticipant);
				this.operationsInternatAttributtes.setInterDepositoriesByParticipant(this.operationsInternatAttributtes.getInterDepositories());
				this.operationsInternatAttributtes.setParticipantByDepositary(this.operationsInternatAttributtes.getParticipantLocal());
				this.operationsInternatAttributtes.setInterParticipantByDepostary(this.operationsInternatAttributtes.getInterParticipantList());
			}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				loadInternationalAttributes();
			}

			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
			return "registerInternationalOperation";
		}
		return "";
	}
	
	/**
	 * Gets the international participant.
	 *
	 * @param idIntParticipantPk the id int participant pk
	 * @return the international participant
	 */
	public InternationalParticipant internationalParticipantFromPk(Long idIntParticipantPk){
		if(idIntParticipantPk!=null){
			for(InternationalParticipant interParticipant: this.operationsInternatAttributtes.getInterParticipantList()){
				if(interParticipant.getIdInterParticipantPk().equals(idIntParticipantPk)){
					return interParticipant;
				}
			}
		}
		return null;
	}
	/**
	 * Load privilege user.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadPrivilegeUser() throws ServiceException{
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			Participant participant = accountsFacade.getParticipantStateServiceFacade(userInfo.getUserAccountSession().getParticipantCode());
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				this.userPrivilege.setPrivilegeComponent(new PrivilegeComponent());
				return;
			}
		}
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		privilegeComponent.setBtnCancel(Boolean.TRUE);
		privilegeComponent.setBtnSettlement(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	/**
	 * Confirm action.
	 *
	 * @return the string
	 */
	public String confirmAction(){
		return getInternationalSelectedAction(ViewOperationsType.CONFIRM);
	}
	/**
	 * Modify action.
	 *
	 * @return the string
	 */
	public String modifyAction(){
		return getInternationalSelectedAction(ViewOperationsType.MODIFY);
	}
	/**
	 * Anulate action.
	 *
	 * @return the string
	 */
	public String anulateAction(){
		return getInternationalSelectedAction(ViewOperationsType.ANULATE);
	}
	/**
	 * Reject action.
	 *
	 * @return the string
	 */
	public String rejectAction(){
		return getInternationalSelectedAction(ViewOperationsType.REJECT);
	}
	/**
	 * Approve action.
	 *
	 * @return the string
	 */
	public String approveAction(){
		return getInternationalSelectedAction(ViewOperationsType.APPROVE);
	}
	/**
	 * Settlement action.
	 *
	 * @return the string
	 */
	public String settlementAction(){
		InternationalOperationSearch interSearch = this.internationalOperationSearch;
		if(interSearch!=null){
			if(interSearch.getSettlementDate().equals(new Date()) || interSearch.getSettlementDate().after(new Date())){//Si la fecha de liquidacion es hoy O es anterior
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_OPERATION_SETTLEMENT_DATE_,null);
				JSFUtilities.showSimpleValidationDialog();	
				return "";
			}
		}
		return getInternationalSelectedAction(ViewOperationsType.SETTLEMENT);
	}
	/**
	 * Cancel action.
	 *
	 * @return the string
	 */
	public String cancelAction(){
		return getInternationalSelectedAction(ViewOperationsType.CANCEL);
	}
	
	/**
	 * Gets the international selected action.
	 *
	 * @param viewOperationsType the view operations type
	 * @return the international selected action
	 */
	public String getInternationalSelectedAction(ViewOperationsType viewOperationsType){
		InternationalOperationSearch interSearch = this.internationalOperationSearch;
		Boolean canExecuteAction = Boolean.FALSE;
		if(interSearch!=null && interSearch.getOperationState()!=null){
			InternationalOperationStateType intOperationStateType = InternationalOperationStateType.get(interSearch.getOperationState());
			switch(viewOperationsType){
			case MODIFY : case APPROVE: case ANULATE: 
					if(intOperationStateType.equals(InternationalOperationStateType.REGISTRADA)){
						canExecuteAction = Boolean.TRUE;
					}break;
			case CONFIRM: case REJECT:
					if(intOperationStateType.equals(InternationalOperationStateType.APROBADA)){
						canExecuteAction = Boolean.TRUE;
					}break;
			case SETTLEMENT: case CANCEL:
				if(intOperationStateType.equals(InternationalOperationStateType.CONFIRMADA)){
					canExecuteAction = Boolean.TRUE;
				}break;
			}
		}
		if(canExecuteAction){
			this.setViewOperationType(viewOperationsType.getCode());
			try {
				this.internationalOperation = internationalOperationServiceFacade.getInternationalOperationServiceFacade(interSearch.getIdInternationalOperationPk());
				if(InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode().equals(this.internationalOperation.getTransferType())){
					this.internationalMarketFactOperation = this.internationalOperation.getInternationalMarketFactOperations().get(0);
					isSendingSecurities = false;
				} else{
					isSendingSecurities = true;
				}
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			try {
				loadHolderAccountState();
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			loadInternationalAttributes();
			return "registerInternationalOperation";
		}else{
			if(interSearch!=null){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.OTC_OPERATION_VALIDATION_PARTICIPATION_INCORRECT,null);
			}else{
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.REQUEST_EMPTY_NOT_SELECTED,null);
			}
			JSFUtilities.showSimpleValidationDialog();	
			return null;
		}
	}
	/**
	 * Load international attributes.
	 */
	public void loadInternationalAttributes(){
		List<Participant> partListTemp = OperationsInternatUtils.getParticipantFromDepositary(
				internationalOperation.getSettlementDepository().getIdInternationalDepositoryPk(),
				operationsInternatAttributtes.getInterDepositories(),
				operationsInternatAttributtes.getParticipantLocal());
		
		Collections.sort(partListTemp, new Comparator<Participant>(){
			public int compare(Participant s1, Participant s2) {
			    return s1.getMnemonic().compareToIgnoreCase(s2.getMnemonic());
			}
		});

		operationsInternatAttributtes.setParticipantByDepositary(partListTemp);
		if(!isViewOperationRegister()){
			operationsInternatAttributtes.setInterParticipantByDepostary(operationsInternatAttributtes.getInterParticipantList());
			operationsInternatAttributtes.setInterDepositoriesByParticipant(operationsInternatAttributtes.getInterDepositories());
			
			if(InstrumentType.FIXED_INCOME.getCode().equals(internationalOperation.getSecurities().getInstrumentType())){
				operationsInternatAttributtes.setSecurityIsFixedInstrument(Boolean.TRUE);
			}else{
				operationsInternatAttributtes.setSecurityIsFixedInstrument(Boolean.FALSE);
			}
			for(HolderAccountDetail holderAccountDetail : internationalOperation.getHolderAccount().getHolderAccountDetails()){
				if(Integer.valueOf(1).equals(holderAccountDetail.getIndRepresentative())){
					holderSettlementDetails = holderAccountDetail.getHolder();
				}
			}
			
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(holderSettlementDetails.getIdHolderPk());
			holderTO.setParticipantFk(internationalOperation.getLocalParticipant().getIdParticipantPk());
				
			List<HolderAccountHelperResultTO> lstAccountTo;
			try {
				lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>(); 
				for(HolderAccountHelperResultTO accountHelperResultTO : lstAccountTo){
					if(accountHelperResultTO.getAccountPk().equals(internationalOperation.getHolderAccount().getIdHolderAccountPk())){
						lstSourceAccountTo.add(accountHelperResultTO);
					}
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Gets the settlement type list.
	 *
	 * @return the settlement type list
	 */
	public List<SettlementType> getSettlementTypeList(){
		return SettlementType.listSomeElements(SettlementType.DVP,SettlementType.FOP);
	}
	
	/**
	 * Gets the instrument type list.
	 *
	 * @return the instrument type list
	 */
	public List<InstrumentType> getInstrumentTypeList(){
		return InstrumentType.list;
	}
	/**
	 * Load operations type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadOperationsType() throws ServiceException{
		List<OperationType> operationTypeList = internationalOperationServiceFacade.getOperationsTypeListFacade();
		this.operationsInternatAttributtes.setOperationsTypeList(operationTypeList);
	}
	
	/**
	 * Load operations state type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadOperationsStateType() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.PARAMETER_TABLE_OPERATIONS_INTERNATIONAL_STATE.getCode());
		List<ParameterTable> lstCboDateSearchType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO) ;
		this.operationsInternatAttributtes.setInterOperationsState(lstCboDateSearchType);
	}
	
	/**
	 * Search international operation to.
	 */
	public void searchInternationalOperationTO(){
		this.internationalOperationDataModel = null;
		List<InternationalOperationSearch> interFinded = null;
		try {
			interFinded = internationalOperationServiceFacade.searchInternationalOperation(internationalOperationFilter);
			if(interFinded!=null && !interFinded.isEmpty()){
				internationalOperationDataModel = new GenericDataModel<InternationalOperationSearch>(interFinded);
			}else{
				internationalOperationDataModel = new GenericDataModel<InternationalOperationSearch>();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search international operation generate to.
	 */
	public void searchInternationalOperationGenerateTO(){
		this.internationalOperationGenerate = null;
		List<InternationalOperationSearch> interFinded = null;
		InternationalOperationFilter internationalOperationFilter = new InternationalOperationFilter();
		internationalOperationFilter.setStartDate(this.internationalOperationFilter.getStartGenerateDate());
		internationalOperationFilter.setEndDate(this.internationalOperationFilter.getEndGenerateDate());
		try {
			interFinded = internationalOperationServiceFacade.searchInternationalOperation(internationalOperationFilter);
			if(interFinded!=null && !interFinded.isEmpty()){
				internationalOperationGenerate = new GenericDataModel<InternationalOperationSearch>(interFinded);
			}else{
				internationalOperationGenerate = new GenericDataModel<InternationalOperationSearch>();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	/**
	 * Show isin detail.
	 *
	 * @param isinCode the isin code
	 */
	public void showIsinDetail(String isinCode){
		securityHelpBean.setSecurityCode(isinCode);
		securityHelpBean.setName("helpSecurity");
		securityHelpBean.searchSecurityHandler();
	}

	/**
	 * Gets the international operation generate.
	 *
	 * @return the international operation generate
	 */
	public GenericDataModel<InternationalOperationSearch> getInternationalOperationGenerate() {
		return internationalOperationGenerate;
	}

	/**
	 * Sets the international operation generate.
	 *
	 * @param internationalOperationGenerate the new international operation generate
	 */
	public void setInternationalOperationGenerate(GenericDataModel<InternationalOperationSearch> internationalOperationGenerate) {
		this.internationalOperationGenerate = internationalOperationGenerate;
	}
	/**
	 * On change settlement depositary.
	 */
	public void onChangeSettlementDepositary(){
		List<InternationalParticipant> inteParticipants = new ArrayList<InternationalParticipant>();
		List<Participant> localPartByDepositary = OperationsInternatUtils.getParticipantFromDepositary(
				internationalOperation.getSettlementDepository().getIdInternationalDepositoryPk(),
				operationsInternatAttributtes.getInterDepositories(),
				operationsInternatAttributtes.getParticipantLocal());
		
		Collections.sort(localPartByDepositary, new Comparator<Participant>(){
			public int compare(Participant s1, Participant s2) {
			    return s1.getMnemonic().compareToIgnoreCase(s2.getMnemonic());
			}
		});
		
		
		Long idSettlementDepositary = internationalOperation.getSettlementDepository().getIdInternationalDepositoryPk();
		for(InternationalParticipant intParticipant: operationsInternatAttributtes.getInterParticipantList()){
			if(intParticipant.getInternationalDepository().getIdInternationalDepositoryPk().equals(idSettlementDepositary)){
				inteParticipants.add(intParticipant);
			}
		}
		operationsInternatAttributtes.setInterParticipantByDepostary(inteParticipants);
		
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			operationsInternatAttributtes.setParticipantByDepositary(localPartByDepositary);
			internationalOperation.getLocalParticipant().setIdParticipantPk(null);
		}else if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			//notthing, maybe in the future
		}
		
		internationalOperation.getInternationalParticipant().setIdInterParticipantPk(null);
		internationalOperation.setHolderAccount(new HolderAccount());
		internationalOperation.setStockQuantity(null);
		internationalOperation.setCashAmount(null);
		internationalOperation.setOperationPrice(null);
		internationalOperation.setSettlementAmount(null);
	}
	
	/**
	 * Register international operation action.
	 *
	 * @return the string
	 */
	public String registerInternationalOperationAction(){
		this.internationalOperation = new InternationalOperation();
		this.holderSettlementDetails = new Holder();
		this.internationalOperation.setLocalParticipant(new Participant());
		this.internationalOperation.setInternationalParticipant(new InternationalParticipant());
		this.internationalOperation.setSettlementDepository(new InternationalDepository());
		this.internationalOperation.setHolderAccount(new HolderAccount());
		this.internationalOperation.setOperationDate(new Date());
		this.internationalOperation.setSettlementDate(new Date());
		this.internationalOperation.setTradeDepository(new InternationalDepository());
		this.internationalOperation.setSecurities(new Security());
		this.internationalOperation.setTradeOperation(new TradeOperation());
		if(this.securitiesHelperBean.getSecurityDescription()!=null){
			this.securitiesHelperBean.setSecurityDescription(null);
		}
		
		try{			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				this.internationalOperation.getLocalParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				Long idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
				List<InternationalDepository> inteDepositoriesAll = this.operationsInternatAttributtes.getInterDepositories();
				List<InternationalDepository> intDepositByParticipant = OperationsInternatUtils.getInternatDepositaryByParticipant(idParticipantPk, inteDepositoriesAll);
				this.operationsInternatAttributtes.setInterDepositoriesByParticipant(intDepositByParticipant);
				this.operationsInternatAttributtes.setParticipantByDepositary(this.operationsInternatAttributtes.getParticipantLocal());
			}else{
				this.operationsInternatAttributtes.setInterDepositoriesByParticipant(this.operationsInternatAttributtes.getInterDepositories());
			}			
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		this.internationalOperation.getTradeDepository().setIdInternationalDepositoryPk(NegotiationConstant.CLEARSTREAM);
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();		
		sourceAccountTo = new HolderAccountHelperResultTO();
		return "registerInternationalOperation";
	}
	/**
	 * Generate inter page.
	 *
	 * @return the string
	 */
	public String generateInterPage(){
		this.internationalOperationGenerate = null;
		this.internationalOperationFilter.setStartGenerateDate(new Date());
		this.internationalOperationFilter.setEndGenerateDate(new Date());
		this.internOperationMultiple = null;
		return "generateOperationPage";
	}
	/**
	 * Change local participant.
	 */
	public void changeLocalParticipant(){
		if(isViewOperationConsult()){
			internationalOperationFilter.setHolderAccount(new HolderAccount());
			internationalOperationFilter.setSecurity(new Security());
			internationalOperationSearch = new InternationalOperationSearch();
			internationalOperationDataModel  = null;
			internationalOperationFilter.setTransferType(null);
			internationalOperationFilter.setIdSettlementDeposiroty(null);
			internationalOperationFilter.setIdInterParticipantPk(null);
			internationalOperationFilter.setIdState(null);
			internationalOperationFilter.setIdSettlementType(null);
			internationalOperationFilter.setStartDate(new Date());
			internationalOperationFilter.setEndDate(new Date());
			internationalOperationFilter.setIdIsinCodePk(null);
			
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idOperationType");
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idSettlement");
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idInterParticipant");
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idState");
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idSettlementType");
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idStartDate");
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idEndDate");
			JSFUtilities.resetComponent("frmSearchInternationalOpr:idSourceSecurity");
		}else{
			this.internationalOperation.setHolderAccount(new HolderAccount());
			this.internationalOperation.setStockQuantity(null);
			this.internationalOperation.setCashAmount(null);
			this.internationalOperation.setOperationPrice(null);
			this.internationalOperation.setSettlementAmount(null);
			this.holderSettlementDetails = new  Holder();
			this.lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		}
	}
	/**
	 * Change operation transfer type.
	 */
	public void changeOperationTransferType(){
		isSendingSecurities = false;
		InternationalOperationTransferType intTransferType = InternationalOperationTransferType.get(this.internationalOperation.getTransferType());
		if(intTransferType.equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES)){
			isSendingSecurities = true;
		}
		this.internationalOperation.setStockQuantity(null);
		this.internationalOperation.setCashAmount(null);
		this.internationalOperation.setOperationPrice(null);
		this.internationalOperation.setSettlementAmount(null);
	}
	/**
	 * Generate excel sheet.
	 */
	@LoggerAuditWeb
	public void generateExcelSheet(){
		List<InternationalOperation> internationalOperations = OperationsInternatUtils.getInternOperationFromArray(this.getInternOperationMultiple());
		if(internationalOperations!=null && !internationalOperations.isEmpty()){
			try {
				DepositaryOperationFile deOperationFile = internationalOperationServiceFacade.generateInternatOperationFileServiceFacade(internationalOperations);
				if(deOperationFile!=null){
//					String extension = deOperationFile.getFileName().substring(deOperationFile.getFileName().lastIndexOf(".")+1,deOperationFile.getFileName().length());
//					String fileName = deOperationFile.getFileName().substring(0,deOperationFile.getFileName().indexOf("."));
//					DefaultStreamedContent(is,"application/pdf",holderAccountFile.getFilename());
					this.operationsInternatAttributtes.setStreamedContent(getStreamedContentFromFile(deOperationFile.getDepositaryFile(),"application/pdf",deOperationFile.getFileName()));
					JSFUtilities.executeJavascriptFunction("PF('idGenerateFileW').show()");
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Change international participant.
	 */
	public void changeInternationalParticipant(){
		Long idInterParti = this.internationalOperation.getInternationalParticipant().getIdInterParticipantPk();
		InternationalParticipant interParticipant = OperationsInternatUtils.getIntParticipantFromPk(this.operationsInternatAttributtes.getInterParticipantList(), idInterParti);
		if(interParticipant!=null){
			if(interParticipant.getAccountNumber()==null){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_PARTICIPANT_HOLDERACCOUNT_EMPTY,null);
				JSFUtilities.showSimpleValidationDialog();
				this.internationalOperation.getInternationalParticipant().setIdInterParticipantPk(null);
			}
		}
	}
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the international operation.
	 *
	 * @return the international operation
	 */
	public InternationalOperation getInternationalOperation() {
		return internationalOperation;
	}

	/**
	 * Sets the international operation.
	 *
	 * @param internationalOperation the new international operation
	 */
	public void setInternationalOperation(
			InternationalOperation internationalOperation) {
		this.internationalOperation = internationalOperation;
	}

	/**
	 * Gets the international operation filter.
	 *
	 * @return the international operation filter
	 */
	public InternationalOperationFilter getInternationalOperationFilter() {
		return internationalOperationFilter;
	}

	/**
	 * Sets the international operation filter.
	 *
	 * @param internationalOperationFilter the new international operation filter
	 */
	public void setInternationalOperationFilter(
			InternationalOperationFilter internationalOperationFilter) {
		this.internationalOperationFilter = internationalOperationFilter;
	}

	/**
	 * Gets the international operation data model.
	 *
	 * @return the international operation data model
	 */
	public GenericDataModel<InternationalOperationSearch> getInternationalOperationDataModel() {
		return internationalOperationDataModel;
	}

	/**
	 * Sets the international operation data model.
	 *
	 * @param internationalOperationDataModel the new international operation data model
	 */
	public void setInternationalOperationDataModel(
			GenericDataModel<InternationalOperationSearch> internationalOperationDataModel) {
		this.internationalOperationDataModel = internationalOperationDataModel;
	}

	/**
	 * Gets the international operation search.
	 *
	 * @return the international operation search
	 */
	public InternationalOperationSearch getInternationalOperationSearch() {
		return internationalOperationSearch;
	}

	/**
	 * Sets the international operation search.
	 *
	 * @param internationalOperationSearch the new international operation search
	 */
	public void setInternationalOperationSearch(
			InternationalOperationSearch internationalOperationSearch) {
		this.internationalOperationSearch = internationalOperationSearch;
	}

	/**
	 * Gets the intern operation multiple.
	 *
	 * @return the intern operation multiple
	 */
	public InternationalOperationSearch[] getInternOperationMultiple() {
		return internOperationMultiple;
	}

	/**
	 * Sets the intern operation multiple.
	 *
	 * @param internOperationMultiple the new intern operation multiple
	 */
	public void setInternOperationMultiple(
			InternationalOperationSearch[] internOperationMultiple) {
		this.internOperationMultiple = internOperationMultiple;
	}
	
	/**
	 * Gets the internat depositary.
	 *
	 * @param idInterDepPk the id inter dep pk
	 * @return the internat depositary
	 */
	public InternationalDepository findInternatDepositary(Long idInterDepPk){
		for(InternationalDepository internationalDepository: this.operationsInternatAttributtes.getInterDepositories()){
			if(idInterDepPk.equals(internationalDepository.getIdInternationalDepositoryPk())){
				return internationalDepository;
			}
		}
		return null;
	}

	/**
	 * Gets the reject reason list.
	 *
	 * @return the reject reason list
	 */
	public List<ParameterTable> getRejectReasonList() {
		return rejectReasonList;
	}

	/**
	 * Sets the reject reason list.
	 *
	 * @param rejectReasonList the new reject reason list
	 */
	public void setRejectReasonList(List<ParameterTable> rejectReasonList) {
		this.rejectReasonList = rejectReasonList;
	}

	/**
	 * Checks if is view other motive.
	 *
	 * @return true, if is view other motive
	 */
	public boolean isViewOtherMotive() {
		return viewOtherMotive;
	}

	/**
	 * Sets the view other motive.
	 *
	 * @param viewOtherMotive the new view other motive
	 */
	public void setViewOtherMotive(boolean viewOtherMotive) {
		this.viewOtherMotive = viewOtherMotive;
	}
	
	/**
	 * Valid source holder.
	 */
	public void validSourceHolder(){		
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		this.sourceAccountTo = new HolderAccountHelperResultTO();
		this.internationalOperation.setStockQuantity(null);
		this.internationalOperation.setCashAmount(null);
		this.internationalOperation.setOperationPrice(null);
		this.internationalOperation.setSettlementAmount(null);
		this.internationalOperation.setHolderAccount(new HolderAccount());
		
		if(Validations.validateIsNotNull(holderSettlementDetails) && Validations.validateIsNotNullAndPositive(holderSettlementDetails.getIdHolderPk())){
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(holderSettlementDetails.getIdHolderPk());
			holderTO.setParticipantFk(internationalOperation.getLocalParticipant().getIdParticipantPk());
			try {				
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				if(lstAccountTo.size()>0){
					lstSourceAccountTo = lstAccountTo;
				}
				loadHolderAccountState();
			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}			
		}
	}

	/**
	 * Gets the lst source account to.
	 *
	 * @return the lst source account to
	 */
	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
		return lstSourceAccountTo;
	}

	/**
	 * Sets the lst source account to.
	 *
	 * @param lstSourceAccountTo the new lst source account to
	 */
	public void setLstSourceAccountTo(
			List<HolderAccountHelperResultTO> lstSourceAccountTo) {
		this.lstSourceAccountTo = lstSourceAccountTo;
	}

	/**
	 * Gets the source account to.
	 *
	 * @return the source account to
	 */
	public HolderAccountHelperResultTO getSourceAccountTo() {
		return sourceAccountTo;
	}

	/**
	 * Sets the source account to.
	 *
	 * @param sourceAccountTo the new source account to
	 */
	public void setSourceAccountTo(HolderAccountHelperResultTO sourceAccountTo) {
		this.sourceAccountTo = sourceAccountTo;
	}
	
	/**
	 * Load holder account state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadHolderAccountState() throws ServiceException{
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			mapStateHolderAccount.put(param.getParameterTablePk(), param.getDescription());
		}
	}
	
	/**
	 * Gets the map holder account state.
	 *
	 * @param code the code
	 * @return the map holder account state
	 */
	public String getMapHolderAccountState(Integer code){
		return mapStateHolderAccount.get(code);
	}
	
	/**
	 * Verify exist market fact.
	 *
	 * @param marketFactBalance the market fact balance
	 */
	public void verifyExistMarketFact(MarketFactBalanceHelpTO marketFactBalance){
		try {
			MarketFactBalanceHelpTO marketBalanceFinded = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
			if(Validations.validateIsNotNullAndNotEmpty(marketBalanceFinded)){
				blHasMarketFact=true;
			}
		} catch (ServiceException e) {
			//clean textboxes
			internationalOperation.setStockQuantity(null);
			internationalOperation.setCashAmount(null);
			switch(e.getErrorService()){
				case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT: case MARKETFACT_UI_ERROR:
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
													 PropertiesUtilities.getExceptionMessage(e.getMessage()));
					JSFUtilities.showSimpleValidationDialog();
					break;
				default:
					break;
			}
		}
	}
	
	/**
	 * Fill market fact data.
	 *
	 * @return the market fact balance help to
	 */
	public MarketFactBalanceHelpTO fillMarketFactData(){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(internationalOperation.getSecurities().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(internationalOperation.getHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(internationalOperation.getLocalParticipant().getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket");
		
		return marketFactBalance;
	}
	/**
	 * Show market fact ui.
	 */
	public void showMarketFactUI(){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		
		if(internationalOperation.getSecurities().getIdSecurityCodePk() != null &&
			internationalOperation.getHolderAccount().getIdHolderAccountPk() != null &&
			internationalOperation.getLocalParticipant().getIdParticipantPk() != null){

			marketFactBalance = fillMarketFactData();
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
		}else{
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.INTERNATIONAL_MSG_VALID_MARKETFACT,null);
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		List<InternationalMarketFactOperation> internationalMarketFactOperations =  new ArrayList<InternationalMarketFactOperation>();
		BigDecimal totalStockQuantity = BigDecimal.ZERO;
		for(MarketFactDetailHelpTO marketFactDetailHelpTO : holderMarketFactBalance.getMarketFacBalances()){
			InternationalMarketFactOperation marketFact = new InternationalMarketFactOperation();
			
			marketFact.setInternationalOperation(this.internationalOperation);
			marketFact.setMarketDate(marketFactDetailHelpTO.getMarketDate());
			marketFact.setMarketRate(marketFactDetailHelpTO.getMarketRate());
			marketFact.setMarketPrice(marketFactDetailHelpTO.getMarketPrice());
			marketFact.setQuantity(marketFactDetailHelpTO.getEnteredBalance());

			totalStockQuantity = totalStockQuantity.add(marketFactDetailHelpTO.getEnteredBalance());
			internationalMarketFactOperations.add(marketFact);
			
		}
		BigDecimal currentNV=new BigDecimal(0);
		if(internationalOperation.getSecurities()!=null && internationalOperation.getSecurities().getCurrentNominalValue()!=null){
			currentNV=internationalOperation.getSecurities().getCurrentNominalValue();
		}
		this.internationalOperation.setStockQuantity(totalStockQuantity);
		this.internationalOperation.setCashAmount(internationalOperation.getStockQuantity().multiply(currentNV));
		this.internationalOperation.setInternationalMarketFactOperations(internationalMarketFactOperations);		
		validateStockQuantityHandler();
		JSFUtilities.updateComponent("frmInternationalOperations:flsSettlement");
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/**
	 * Checks if is sending securities.
	 *
	 * @return true, if is sending securities
	 */
	public boolean isSendingSecurities() {
		return isSendingSecurities;
	}

	/**
	 * Sets the sending securities.
	 *
	 * @param isSendingSecurities the new sending securities
	 */
	public void setSendingSecurities(boolean isSendingSecurities) {
		this.isSendingSecurities = isSendingSecurities;
	}

	/**
	 * Gets the holder settlement details.
	 *
	 * @return the holder settlement details
	 */
	public Holder getHolderSettlementDetails() {
		return holderSettlementDetails;
	}

	/**
	 * Sets the holder settlement details.
	 *
	 * @param holderSettlementDetails the new holder settlement details
	 */
	public void setHolderSettlementDetails(Holder holderSettlementDetails) {
		this.holderSettlementDetails = holderSettlementDetails;
	}

	/**
	 * Gets the international market fact operation.
	 *
	 * @return the international market fact operation
	 */
	public InternationalMarketFactOperation getInternationalMarketFactOperation() {
		return internationalMarketFactOperation;
	}

	/**
	 * Sets the international market fact operation.
	 *
	 * @param internationalMarketFactOperation the new international market fact operation
	 */
	public void setInternationalMarketFactOperation(
			InternationalMarketFactOperation internationalMarketFactOperation) {
		this.internationalMarketFactOperation = internationalMarketFactOperation;
	}

	/**
	 * Checks if is bl has market fact.
	 *
	 * @return true, if is bl has market fact
	 */
	public boolean isBlHasMarketFact() {
		return blHasMarketFact;
	}

	/**
	 * Sets the bl has market fact.
	 *
	 * @param blHasMarketFact the new bl has market fact
	 */
	public void setBlHasMarketFact(boolean blHasMarketFact) {
		this.blHasMarketFact = blHasMarketFact;
	}
}