package com.pradera.negotiations.internationalOperations.service;

import static com.pradera.model.negotiation.type.InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES;
import static com.pradera.model.negotiation.type.InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.XMLUtils;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.OperationType;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.DepositaryOperationFile;
import com.pradera.model.negotiation.InternationalFileDetail;
import com.pradera.model.negotiation.InternationalMarketFactOperation;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.InternationalParticipant;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.model.negotiation.type.InternationalOperationTransferType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationFilter;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationTO;
// TODO: Auto-generated Javadoc
/**
* The Class InternationalOperationServiceBean.
* @author PraderaTechnologies.
*/
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class InternationalOperationServiceBean extends CrudDaoServiceBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * to get the Participant Details by Id.
	 *
	 * @param participantId the participant id
	 * @return List<Object[]>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getParticipantDetails(Long participantId) throws ServiceException{
		return (List<Object[]>)findListByQueryString("select p.idParticipantPk,p.mnemonic from Participant p where p.idParticipantPk="+participantId);
	}

	/**
	 *  
	 *  Gets list of local participant.
	 *
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getLocalParticipant(){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select p.idParticipantPk,p.description from Participant p where p.state=:state order by p.idParticipantPk");
		parameters.put("state", ParticipantStateType.REGISTERED.getCode());
		return (List<Object[]>)findListByQueryString(sbQuery.toString(),parameters);
	}
	
	/**
	 *  
	 *  Gets list of negotiation depository.
	 *
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getTradingDepositary(){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery = new StringBuffer();
		sbQuery.append("select idInternationalDepositoryPk,description from InternationalDepository");
		return (List<Object[]>)findListByQueryString(sbQuery.toString());		
	}
	
	/**
	 *  
	 *  Gets list of international participant.
	 *
	 * @param idInternationalDepositoryPk the id international depository pk
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getInternationalParticipant(Long idInternationalDepositoryPk){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select ip.idInterParticipantPk,ip.description from InternationalParticipant ip where ");
		sbQuery.append("ip.internationalDepository.idInternationalDepositoryPk = :interDepositoryPK order by ip.idInterParticipantPk");
		parameters.put("interDepositoryPK", idInternationalDepositoryPk);
		return (List<Object[]>)findListByQueryString(sbQuery.toString(),parameters);
	}
	
	/**
	 * Gets the international participant list.
	 *
	 * @return the international participant list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalParticipant> getInternationalParticipantListServiceBean() throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("select ip FROM InternationalParticipant ip INNER JOIN FETCH ip.internationalDepository order by ip.description");
		return (List<InternationalParticipant>)findListByQueryString(sbQuery.toString());
	}
	
	
	/**
	 *  
	 *  Gets BIC Code of international participant.
	 *
	 * @param idInternationalDepositoryPk the id international depository pk
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBicCodeDemo(Long idInternationalDepositoryPk){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select id.bicCode,id.stateIntDepositary from InternationalDepository id where id.idInternationalDepositoryPk = :idDepositoryPk");
		parameters.put("idDepositoryPk",idInternationalDepositoryPk);
		return (List<Object[]>)findListByQueryString(sbQuery.toString(),parameters);
		
	 }
	
	/**
	 *  
	 *  Gets Account Number of international participant.
	 *
	 * @param idInterParticipantPk the id inter participant pk
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getAccountNumberDemo(Long idInterParticipantPk){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select ha.accountNumber,ha.closingDate  from HolderAccount ha where ha.participant.idParticipantPk = :participantPK");
		parameters.put("participantPK",idInterParticipantPk);
		return (List<Object[]>)findListByQueryString(sbQuery.toString(),parameters);
		
	 }
		
	
	/**
	 * This method gets holder account details for account number and local participant.
	 *
	 * @param accountNumber the account number
	 * @param idLocalParticipantPk the id local participant pk
	 * @return List<Object[]>
	 */ 
	@SuppressWarnings("unchecked")
	public List<Object[]> getHoldetAccountDtlsDemo(Integer accountNumber,Long idLocalParticipantPk){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("Select ha.alternateCode,ha.alternateCode,ha.accountGroup,ha.stateAccount from HolderAccount ha ");
	    sbQuery.append("where ha.accountNumber = :accountNumber and ha.participant.idParticipantPk = :participantPk");
		parameters.put("accountNumber", accountNumber);
		parameters.put("participantPk", idLocalParticipantPk);
		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
	 }
	
	/**
	 *  
	 * This method generates operation number using transferType and idLocalParticipantPk of internationalOperation .
	 *
	 * @param transferType the transfer type
	 * @param idLocalParticipantPK the id local participant pk
	 * @return Long
	 */
	@SuppressWarnings("unchecked")
	public Long getOperationNumber(Long transferType,Long idLocalParticipantPK){
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();	 
	    sbQuery.append("select ip.transferType,ip.localParticipant.idParticipantPk from InternationalOperation ip ");
	    sbQuery.append("where ip.transferType = :transType and ip.localParticipant.idParticipantPk = :participantPK");
	    parameters.put("transType", transferType);
	    parameters.put("participantPK", idLocalParticipantPK);
	    objects = findListByQueryString(sbQuery.toString(), parameters);
	    return new Long(objects.size()+1);
	}
	
	/**
	 *  
	 * Gets Holder Account object.
	 *
	 * @param accountNumber the account number
	 * @param idLocalParticipantPk the id local participant pk
	 * @return HolderAccount
	 */
	public HolderAccount getHolderAccountObject(Integer accountNumber,Long idLocalParticipantPk){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
	    sbQuery.append("Select ha from HolderAccount ha where ha.accountNumber = :accountNumber and ha.participant.idParticipantPk = :participantPk");
		parameters.put("accountNumber", accountNumber);
		parameters.put("participantPk", idLocalParticipantPk);
		return (HolderAccount) findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 *  
	 * search international operation according to search criteria.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchGenerateInterOpration(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select interOpr.idInternationalOperationPk,interOpr.operationNumber,interOpr.operationDate,interOpr.transferType,");
		sbQuery.append("interOpr.settlementType from InternationalOperation interOpr  where ");
		sbQuery.append("trunc(interOpr.operationDate) between :startDate and :endDate order by trunc(interOpr.operationDate) desc");
		/*parameters.put("settlementDepositoryPk", internationalOperationFilter.getIdSettlementDeposiroty())*/;
		parameters.put("startDate", internationalOperationFilter.getStartGenerateDate());
		parameters.put("endDate", internationalOperationFilter.getEndGenerateDate());
		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 *  
	 * search international operation according to search criteria.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchInternationalOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();		
		sbQuery.append("SELECT interOpr.idInternationalOperationPk,interOpr.operationNumber,interOpr.registerDate,interOpr.settlementType,");
		sbQuery.append("interOpr.tradeDepository.description,interOpr.internationalParticipant.description,interOpr.localParticipant.mnemonic, ");
		sbQuery.append("interOpr.securities.idSecurityCodePk,interOpr.settlementAmount,interOpr.stockQuantity,interOpr.cashAmount,interOpr.securities.currentNominalValue, ");
		sbQuery.append("interOpr.operationState,to_char(interOpr.operationDate,'dd/MM/YYYY'),interOpr.localParticipant.idParticipantPk, interOpr.transferType, interOpr.settlementDate ");
		sbQuery.append("FROM InternationalOperation interOpr ");
		sbQuery.append("WHERE trunc(interOpr.operationDate) between :startDate and :endDate ");
		parameters.put("startDate", internationalOperationFilter.getStartDate());
		parameters.put("endDate", internationalOperationFilter.getEndDate());
		
		if(internationalOperationFilter.getTransferType()!=null){
			sbQuery.append(" and interOpr.transferType = :transferType  ");
			parameters.put("transferType", internationalOperationFilter.getTransferType());
		}
		
		if(internationalOperationFilter.getIdSettlementDeposiroty()!=null){
			sbQuery.append(" and interOpr.settlementDepository.idInternationalDepositoryPk = :settlementDepositoryPk ");
			parameters.put("settlementDepositoryPk", internationalOperationFilter.getIdSettlementDeposiroty());
		}
		if(internationalOperationFilter.getIdState()!=null){
			sbQuery.append(" and interOpr.operationState = :state ");
			parameters.put("state", internationalOperationFilter.getIdState());	
		}
		
		if(internationalOperationFilter.getIdInterParticipantPk()!=null){
			sbQuery.append(" and interOpr.internationalParticipant.idInterParticipantPk = :idInterParticipant");
			parameters.put("idInterParticipant", internationalOperationFilter.getIdInterParticipantPk());
		}
		if(Validations.validateIsNotNull(internationalOperationFilter.getIdSettlementType()) && internationalOperationFilter.getIdSettlementType().intValue()>0){
			sbQuery.append(" and interOpr.settlementType = :settlementType");
			parameters.put("settlementType", internationalOperationFilter.getIdSettlementType().intValue());
		}
		if(internationalOperationFilter.getLocalParticipant()!=null && internationalOperationFilter.getLocalParticipant().getIdParticipantPk()!=null){
			sbQuery.append(" and interOpr.localParticipant.idParticipantPk = :localParticipantPk");
			parameters.put("localParticipantPk", internationalOperationFilter.getLocalParticipant().getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(internationalOperationFilter.getIdIsinCodePk()) && internationalOperationFilter.getIdIsinCodePk().length()>0){
			sbQuery.append(" and interOpr.securities.idSecurityCodePk = :isinCode");
			parameters.put("isinCode", internationalOperationFilter.getIdIsinCodePk());
		}
		if(internationalOperationFilter.getHolderAccount()!=null && internationalOperationFilter.getHolderAccount().getAccountNumber()!=null){
			sbQuery.append(" and interOpr.holderAccount.accountNumber = :accNumber");
			parameters.put("accNumber", internationalOperationFilter.getHolderAccount().getAccountNumber());
		}
		sbQuery.append(" order by interOpr.operationNumber DESC, trunc(interOpr.registerDate) desc");
		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
		
	}
	
	/**
	 *  
	 * Gets international operation detail using pirimary key.
	 *
	 * @param internationalOperationPk the international operation pk
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getDetailInterOperation(Long internationalOperationPk){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select inter.tradeDepository.idInternationalDepositoryPk,inter.localParticipant.idParticipantPk,inter.settlementType,inter.transferType,");
		sbQuery.append("inter.settlementDepository.idInternationalDepositoryPk,inter.internationalParticipant.idInterParticipantPk,inter.operationDate,inter.settlementDate,");
		sbQuery.append("inter.holderAccount.accountNumber,inter.holderAccount.alternateCode,inter.holderAccount.alternateCode,inter.securities.idSecurityCodePk,");
		sbQuery.append("inter.securities.description,inter.securities.securityClass,inter.securities.instrumentType,inter.securities.issuer.businessName,");
		sbQuery.append("inter.securities.currentNominalValue,inter.securities.currency,inter.cashAmount,inter.stockQuantity,inter.settlementAmount,inter.operationNumber");
		sbQuery.append(" from InternationalOperation inter where inter.idInternationalOperationPk = :interOprPk");
		parameters.put("interOprPk", internationalOperationPk);
		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
		
	}
	
	/**
	 *  
	 * Updating Trade operation to approve international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */ 
	public void approveTradeOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update TradeOperation to set to.aprovalDate = :approveDt,to.aprovalUser = :approveUsr,to.lastModifyApp = :lastMdApp,");
		sbQuery.append("to.lastModifyDate = :lastMdDate,to.lastModifyIp=:lastMdIp,to.lastModifyUser=:lastMdUsr where to.idTradeOperationPk in (:idTrPk)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("approveDt", internationalOperationFilter.getApproveDate());
		query.setParameter("approveUsr", internationalOperationFilter.getApproveUser());
		query.setParameter("lastMdApp", internationalOperationFilter.getLastModifyApp());
		query.setParameter("lastMdDate", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("idTrPk", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  
	 * Approve international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */ 
	public void approveInternationalOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		//Changing the state on international operation to Approved		
		sbQuery.append("Update InternationalOperation interOpr set interOpr.operationState = :state,interOpr.lastModifyApp=:lastMdApp,interOpr.lastModifyDate=:lastMdDt, ");
		sbQuery.append("interOpr.lastModifyIp=:lastMdIp,interOpr.lastModifyUser=:lastMdUsr where interOpr.idInternationalOperationPk in (:idInterOprPkList)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastMdApp", new BigDecimal(internationalOperationFilter.getLastModifyApp()));
		query.setParameter("lastMdDt", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("state",  InternationalOperationStateType.APROBADA.getCode());
		query.setParameter("idInterOprPkList", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  
	 * Updating Trade operation to confirm international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */ 
	public void confirmTradeOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update TradeOperation to set to.confirmDate = :confirmDt,to.confirmUser = :confirmUsr,to.lastModifyApp = :lastMdApp,");
		sbQuery.append("to.lastModifyDate = :lastMdDate,to.lastModifyIp=:lastMdIp,to.lastModifyUser=:lastMdUsr where to.idTradeOperationPk in (:idTrPk)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("confirmDt", internationalOperationFilter.getConfirmDate());
		query.setParameter("confirmUsr", internationalOperationFilter.getConfirmUser());
		query.setParameter("lastMdApp", internationalOperationFilter.getLastModifyApp());
		query.setParameter("lastMdDate", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("idTrPk", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  
	 * Confirm international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */
	public void confirmInternationalOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update InternationalOperation interOpr set interOpr.operationState = :state,interOpr.lastModifyApp=:lastMdApp,interOpr.lastModifyDate=:lastMdDt, ");
		sbQuery.append("interOpr.lastModifyIp=:lastMdIp,interOpr.lastModifyUser=:lastMdUsr where interOpr.idInternationalOperationPk in (:idInterOprPkList)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastMdApp", new BigDecimal(internationalOperationFilter.getLastModifyApp()));
		query.setParameter("lastMdDt", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("state",  InternationalOperationStateType.CONFIRMADA.getCode());
		query.setParameter("idInterOprPkList", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  
	 * Updating Trade operation to reverse international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */ 
	public void reverseTradeOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update TradeOperation to set to.rejectDate = :rejectDt,to.rejectUser = :rejectUsr,to.rejectMotive = :rejectMd,to.rejectMotiveOther=:rejectMdOthr,");
		sbQuery.append("to.lastModifyApp = :lastMdApp,to.lastModifyDate = :lastMdDate,to.lastModifyIp=:lastMdIp,to.lastModifyUser=:lastMdUsr where to.idTradeOperationPk in (:idTrPk)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("rejectDt", internationalOperationFilter.getReverseDate());
		query.setParameter("rejectUsr", internationalOperationFilter.getReverseUser());
		query.setParameter("rejectMd", internationalOperationFilter.getReverseMotive());
		query.setParameter("rejectMdOthr", internationalOperationFilter.getReverseMotiveOther());
		query.setParameter("lastMdApp", internationalOperationFilter.getLastModifyApp());
		query.setParameter("lastMdDate", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("idTrPk", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  
	 * Reverse international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return boolean
	 */
	public void reverseInternationalOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update InternationalOperation interOpr set interOpr.operationState = :state,interOpr.lastModifyApp=:lastMdApp,interOpr.lastModifyDate=:lastMdDt, ");
		sbQuery.append("interOpr.lastModifyIp=:lastMdIp,interOpr.lastModifyUser=:lastMdUsr where interOpr.idInternationalOperationPk in (:idInterOprPkList)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastMdApp", new BigDecimal(internationalOperationFilter.getLastModifyApp()));
		query.setParameter("lastMdDt", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("state",  InternationalOperationStateType.ANULADA.getCode());
		query.setParameter("idInterOprPkList", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  v
	 * Updating Trade operation to reject international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */ 
	public void rejectTradeOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update TradeOperation to set to.rejectDate = :rejectDt,to.rejectUser = :rejectUsr,to.rejectMotive = :rejectMd,to.rejectMotiveOther=:rejectMdOthr,");
		sbQuery.append("to.lastModifyApp = :lastMdApp,to.lastModifyDate = :lastMdDate,to.lastModifyIp=:lastMdIp,to.lastModifyUser=:lastMdUsr where to.idTradeOperationPk in (:idTrPk)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("rejectDt", internationalOperationFilter.getRejectDate());
		query.setParameter("rejectUsr", internationalOperationFilter.getRejectUser());
		query.setParameter("rejectMd", internationalOperationFilter.getRejectMotive());
		query.setParameter("rejectMdOthr", internationalOperationFilter.getRejectMotiveOther());
		query.setParameter("lastMdApp", internationalOperationFilter.getLastModifyApp());
		query.setParameter("lastMdDate", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("idTrPk", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  
	 * Reject international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */
	public void rejectInternationalOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update InternationalOperation interOpr set interOpr.operationState = :state,interOpr.lastModifyApp=:lastMdApp,interOpr.lastModifyDate=:lastMdDt, ");
		sbQuery.append("interOpr.lastModifyIp=:lastMdIp,interOpr.lastModifyUser=:lastMdUsr where interOpr.idInternationalOperationPk in (:idInterOprPkList)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastMdApp", new BigDecimal(internationalOperationFilter.getLastModifyApp()));
		query.setParameter("lastMdDt", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("state",  InternationalOperationStateType.RECHAZADA.getCode());
		query.setParameter("idInterOprPkList", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();

	}
	
	/**
	 *  
	 * Updating Trade operation to cancel international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */ 
	public void cancelTradeOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update TradeOperation to set to.rejectDate = :rejectDt,to.rejectUser = :rejectUsr,to.rejectMotive = :rejectMd,to.rejectMotiveOther=:rejectMdOthr,");
		sbQuery.append("to.lastModifyApp = :lastMdApp,to.lastModifyDate = :lastMdDate,to.lastModifyIp=:lastMdIp,to.lastModifyUser=:lastMdUsr where to.idTradeOperationPk in (:idTrPk)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("rejectDt", internationalOperationFilter.getCancelDate());
		query.setParameter("rejectUsr", internationalOperationFilter.getCancelUser());
		query.setParameter("rejectMd", internationalOperationFilter.getCancelMotive());
		query.setParameter("rejectMdOthr", internationalOperationFilter.getCancelMotiveOther());
		query.setParameter("lastMdApp", internationalOperationFilter.getLastModifyApp());
		query.setParameter("lastMdDate", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("idTrPk", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}	
	
	/**
	 *  
	 * Cancel international operation .
	 *
	 * @param internationalOperationFilter the international operation filter
	 */
	public void cancelInternationalOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Update InternationalOperation interOpr set interOpr.operationState = :state,interOpr.lastModifyApp=:lastMdApp,interOpr.lastModifyDate=:lastMdDt, ");
		sbQuery.append("interOpr.lastModifyIp=:lastMdIp,interOpr.lastModifyUser=:lastMdUsr where interOpr.idInternationalOperationPk in (:idInterOprPkList)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastMdApp", new BigDecimal(internationalOperationFilter.getLastModifyApp()));
		query.setParameter("lastMdDt", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("state",  InternationalOperationStateType.CANCELADA.getCode());
		query.setParameter("idInterOprPkList", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();

	}
	
	/**
	 *  
	 * Settle international operation .
	 *
	 * @param internationalOperationFilter the new tle operation
	 */
	public void settleOperation(InternationalOperationFilter internationalOperationFilter){
		StringBuffer sbQuery = new StringBuffer();
		//updating two or more records in international operation
		sbQuery.append("Update InternationalOperation interOpr set interOpr.operationState = :state,interOpr.lastModifyApp=:lastMdApp,interOpr.lastModifyDate=:lastMdDt, ");
		sbQuery.append("interOpr.lastModifyIp=:lastMdIp,interOpr.lastModifyUser=:lastMdUsr where interOpr.idInternationalOperationPk in (:idInterOprPkList)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastMdApp", new BigDecimal(internationalOperationFilter.getLastModifyApp()));
		query.setParameter("lastMdDt", internationalOperationFilter.getLastModifyDate());
		query.setParameter("lastMdIp", internationalOperationFilter.getLastModifyIp());
		query.setParameter("lastMdUsr", internationalOperationFilter.getLastModifyUser());
		query.setParameter("state",  InternationalOperationStateType.LIQUIDADA.getCode());
		query.setParameter("idInterOprPkList", internationalOperationFilter.getInternationalOprPkList());
		query.executeUpdate();
	}
	
	/**
	 *  
	 * Checks Whether Security code exists .
	 *
	 * @param isinCode the isin code
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> chkSecurityCodeExists(String isinCode){
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select sec.stateSecurity,sec.idSecurityCodePk from Security sec where sec.idSecurityCodePk = :IsinCodePk ");
		parameters.put("IsinCodePk", isinCode);
		objects = findListByQueryString(sbQuery.toString(), parameters);
		return objects;
	}
	
	/**
	 *  
	 * Checks Whether Security code is registered or not .
	 *
	 * @param isinCode the isin code
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> chkSecurityRegistered(String isinCode){
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select sec.stateSecurity,sec.idSecurityCodePk from Security sec where sec.stateSecurity in ");
		sbQuery.append("(select param.parameterTablePk from ParameterTable param where param.masterTable.masterTablePk = :masterPK ");
		sbQuery.append("and param.parameterTablePk !=:parameterPk) and sec.idSecurityCodePk = :IsinCodePk");
		parameters.put("masterPK", MasterTableType.ESTADOS_OPERACION_MCN.getCode().intValue());
		parameters.put("parameterPk", MechanismOperationStateType.REGISTERED_STATE.getCode().intValue());
		parameters.put("IsinCodePk", isinCode);
		objects = findListByQueryString(sbQuery.toString(), parameters);
		return objects;
	}
	
	/**
	 *  
	 * Checks Whether Security code is registered or not .
	 *
	 * @param isinCode the isin code
	 * @return List<Object[]>
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getSecurityDetails(String isinCode){
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select sec.description,param_ins.parameterName,sec.issuer.businessName,param_curr.parameterName,sec.securityClass,sec.currentNominalValue ");
		sbQuery.append("from Security sec,ParameterTable param_ins,ParameterTable param_curr where sec.idSecurityCodePk = :IsinCodePk ");
		sbQuery.append("and sec.instrumentType = param_ins.parameterTablePk and sec.currency = param_curr.parameterTablePk");
		parameters.put("IsinCodePk",isinCode);
		objects = findListByQueryString(sbQuery.toString(), parameters);
		return objects;
	}
	
	/**
	 * Register international operation.
	 *
	 * @param internationalOperation the international operation
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation registerInternationalOperation(InternationalOperation internationalOperation) throws ServiceException{
		InternationalOperation interOperation = internationalOperation;
		interOperation.setOperationNumber(getInternationalOperationRequestNumber(internationalOperation));
		create(interOperation);
		return interOperation;
	}
	
	/**
	 * Gets the international operation request number.
	 *
	 * @param internationalOperation the international operation
	 * @return the international operation request number
	 * @throws ServiceException the service exception
	 */
	public synchronized Long getInternationalOperationRequestNumber(InternationalOperation internationalOperation) throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT MAX(operation_number) FROM international_operation	"); 
		stringBuilder.append("WHERE		");
		stringBuilder.append("transfer_type = :transferTypeParam AND		"); 
		stringBuilder.append("to_char(operation_date,'yyyy') = to_char(CURRENT_DATE, 'yyyy')	");
		Query query = em.createNativeQuery(stringBuilder.toString());
		query.setParameter("transferTypeParam", internationalOperation.getTransferType());
		try{
			Long requestNumber = ((BigDecimal) query.getSingleResult()).longValue();
			if(requestNumber!=null){
				return requestNumber+1;
			}else{
				return 1L;
			}
		}catch(NullPointerException e){
			return 1L;
		}
	}
	
	/**
	 * Gets the international operation.
	 *
	 * @param idInternationalOperation the id international operation
	 * @return the international operation
	 */
	public InternationalOperation getInternationalOperation(Long idInternationalOperation){	
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select io from InternationalOperation io ");
		stringBuilder.append("inner join fetch io.tradeDepository ");
		stringBuilder.append("inner join fetch io.settlementDepository ");
		stringBuilder.append("inner join fetch io.localParticipant lp ");
		stringBuilder.append("inner join fetch io.internationalParticipant "); 	
		stringBuilder.append("inner join fetch io.securities ");
		stringBuilder.append("inner join fetch io.holderAccount ha ");
		stringBuilder.append("left join io.internationalMarketFactOperations imfo ");
		stringBuilder.append("inner join fetch ha.holderAccountDetails had ");
		stringBuilder.append("inner join fetch had.holder h ");
		stringBuilder.append("where ");
		stringBuilder.append("io.idInternationalOperationPk = :idInternationalOperationPkPrm");
		Query query = em.createQuery(stringBuilder.toString());
		query.setParameter("idInternationalOperationPkPrm", idInternationalOperation);
		
		InternationalOperation objInternationalOperation = null;
		objInternationalOperation = (InternationalOperation) query.getSingleResult();
		objInternationalOperation.getInternationalMarketFactOperations().size();
		return objInternationalOperation;
	}
	
	/**
	 * International depositories service bean.
	 *
	 * @param internationalOperationTO the international operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalDepository> internationalDepositoriesServiceBean(InternationalOperationTO internationalOperationTO) throws ServiceException{
		String query = "SELECT DISTINCT(intDep) FROM InternationalDepository intDep LEFT JOIN FETCH intDep.participantIntDepositaries" +
					   "	WHERE intDep.stateIntDepositary = :idStateDepoParam ";
		Query emQuery = em.createQuery(query);
		emQuery.setParameter("idStateDepoParam", internationalOperationTO.getIdInternatDepositaryState());
		try{
			return emQuery.getResultList();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Gets the security with international depositary.
	 *
	 * @param internationalOperationTO the international operation to
	 * @return the security with international depositary
	 * @throws ServiceException the service exception
	 */
	public Security getSecurityWithInternationalDepositary(InternationalOperationTO internationalOperationTO) throws ServiceException{
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		CriteriaQuery<Security> criteriaQuery = criteriaBuilder.createQuery(Security.class);
		Root<Security> securityRoot = criteriaQuery.from(Security.class);
		criteriaQuery.distinct(Boolean.TRUE);//Hacemos el distint para el ISIN
		criteriaQuery.multiselect(securityRoot.get("idSecurityCodePk"));
		if(internationalOperationTO.getIdInternatDepositary()!=null){
			Join<InternationalDepository,Security> snmJoin = securityRoot.join("securityForeignDepositories");
			criteriaParameters.add(criteriaBuilder.equal(snmJoin.get("id").get("idInternationalDepositoryPk"),internationalOperationTO.getIdInternatDepositary()));
		}
		
		if(internationalOperationTO.getIdIsinCodePk()!=null){
			criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("idSecurityCodePk"),internationalOperationTO.getIdIsinCodePk()));
		}
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		TypedQuery<Security> otcOperationsCriteria = em.createQuery(criteriaQuery);
		Security securityFinded ;//object will return security
		try{
			securityFinded = otcOperationsCriteria.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		return securityFinded;
	}
	
	/**
	 * Gets the balance service bean.
	 *
	 * @param interOperationTO the inter operation to
	 * @return the balance service bean
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance getBalanceServiceBean(InternationalOperationTO interOperationTO) throws ServiceException{
		if(interOperationTO.getIdParticipantPk()==null || interOperationTO.getIdHolderAccountPk()==null || interOperationTO.getIdIsinCodePk()==null){
			return null;
		}
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		CriteriaQuery<HolderAccountBalance> criteriaQuery = criteriaBuilder.createQuery(HolderAccountBalance.class);
		Root<HolderAccountBalance> securityRoot = criteriaQuery.from(HolderAccountBalance.class);
		
		criteriaQuery.distinct(Boolean.TRUE);//Hacemos el distint para el ISIN
		criteriaQuery.multiselect(
				securityRoot.get("accreditationBalance"),
				securityRoot.get("availableBalance"),
				securityRoot.get("banBalance"),
				securityRoot.get("borrowerBalance"),
				securityRoot.get("buyBalance"),
				securityRoot.get("lenderBalance"),
				securityRoot.get("loanableBalance"),
				securityRoot.get("marginBalance"),
				securityRoot.get("oppositionBalance"),
				securityRoot.get("otherBlockBalance"),
				securityRoot.get("pawnBalance"),
				securityRoot.get("reportedBalance"),
				securityRoot.get("reportingBalance"),
				securityRoot.get("reserveBalance"),
				securityRoot.get("sellBalance"),
				securityRoot.get("totalBalance"),
				securityRoot.get("transitBalance")
				);
		
		criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("id").get("idParticipantPk"),interOperationTO.getIdParticipantPk()));
		criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("id").get("idHolderAccountPk"),interOperationTO.getIdHolderAccountPk()));
		criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("id").get("idSecurityCodePk"),interOperationTO.getIdIsinCodePk()));
		
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		TypedQuery<HolderAccountBalance> otcOperationsCriteria = em.createQuery(criteriaQuery);
		HolderAccountBalance balanceFinded ;//object will return security
		try{
			balanceFinded = otcOperationsCriteria.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		return balanceFinded;
	}
	
	/**
	 * Gets the international operation service bean.
	 *
	 * @param idInterNationPk the id inter nation pk
	 * @return the international operation service bean
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation getInternationalOperationServiceBean(Long idInterNationPk) throws ServiceException{
		InternationalOperation internationalOperation = em.find(InternationalOperation.class, idInterNationPk);
		internationalOperation.getSecurities().getIdSecurityCodePk();
		internationalOperation.getHolderAccount().getIdHolderAccountPk();
		internationalOperation.getLocalParticipant().getIdParticipantPk();
		internationalOperation.getInternationalParticipant().getIdInterParticipantPk();
		internationalOperation.getSettlementDepository().getIdInternationalDepositoryPk();
		internationalOperation.getTradeDepository().getIdInternationalDepositoryPk();
		return internationalOperation;
	}
	
	/**
	 *  
	 * 
	 * Modify international operation .
	 *
	 * @param internationalOperationParam the international operation param
	 * @return boolean
	 * @throws ServiceException the service exception
	 */ 
	public InternationalOperation modifyInternationalOperation(InternationalOperation internationalOperationParam) throws ServiceException{
		InternationalOperation internationalOperation = getInternationalOperation(internationalOperationParam.getIdInternationalOperationPk());
		internationalOperation.setInternationalMarketFactOperations(internationalOperationParam.getInternationalMarketFactOperations());
		internationalOperation.setStockQuantity(internationalOperationParam.getStockQuantity());
		internationalOperation.setCashAmount(internationalOperationParam.getCashAmount());
		internationalOperation.setOperationPrice(internationalOperationParam.getOperationPrice());
		internationalOperation.setSettlementAmount(internationalOperationParam.getSettlementAmount());
		update(internationalOperation);
		return internationalOperation;
	}
	/**
	 * Approber international operation.
	 *
	 * @param interOperation the inter operation
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation approberInternationalOperation(InternationalOperation interOperation) throws ServiceException{
		InternationalOperation internationalOperation = getInternationalOperation(interOperation.getIdInternationalOperationPk());
		internationalOperation.setOperationState(InternationalOperationStateType.APROBADA.getCode());
		internationalOperation.setTradeOperation(interOperation.getTradeOperation());
		update(internationalOperation);
		return internationalOperation;
	}
	/**
	 * Annulate international operation.
	 *
	 * @param interOperation the inter operation
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation annulateInternationalOperation(InternationalOperation interOperation) throws ServiceException{
		InternationalOperation internationalOperation = getInternationalOperation(interOperation.getIdInternationalOperationPk());
		internationalOperation.setOperationState(InternationalOperationStateType.ANULADA.getCode());
		internationalOperation.setTradeOperation(interOperation.getTradeOperation());
		update(internationalOperation);
		return internationalOperation;
	}
	
	/**
	 * Confirm international operation.
	 *
	 * @param interOperation the inter operation
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation confirmInternationalOperation(InternationalOperation interOperation) throws ServiceException{
		InternationalOperation internationalOperation = getInternationalOperation(interOperation.getIdInternationalOperationPk());
		internationalOperation.setOperationState(InternationalOperationStateType.CONFIRMADA.getCode());
		internationalOperation.setTradeOperation(interOperation.getTradeOperation());
		update(internationalOperation);
		return internationalOperation;
	}
	
	/**
	 * Confirm international operation.
	 *
	 * @param interOperation the inter operation
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation rejectInternationalOperation(InternationalOperation interOperation) throws ServiceException{
		InternationalOperation internationalOperation = getInternationalOperation(interOperation.getIdInternationalOperationPk());
		internationalOperation.setOperationState(InternationalOperationStateType.RECHAZADA.getCode());
		internationalOperation.setTradeOperation(interOperation.getTradeOperation());
		update(internationalOperation);
		return internationalOperation;
	}
	
	/**
	 * Settlement international operation.
	 *
	 * @param interOperation the inter operation
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation settlementInternationalOperation(InternationalOperation interOperation) throws ServiceException{
		InternationalOperation internationalOperation = getInternationalOperation(interOperation.getIdInternationalOperationPk());
		internationalOperation.setOperationState(InternationalOperationStateType.LIQUIDADA.getCode());
		internationalOperation.setTradeOperation(interOperation.getTradeOperation());
		update(internationalOperation);
		return internationalOperation;
	}
	
	/**
	 * Creates the securities operation.
	 *
	 * @param securitiesOperation the securities operation
	 * @return the securities operation
	 * @throws ServiceException the service exception
	 */
	public SecuritiesOperation createSecuritiesOperation(SecuritiesOperation securitiesOperation) throws ServiceException{
		create (securitiesOperation);
		return securitiesOperation;
	}
	
	/**
	 * Cancel international operation.
	 *
	 * @param interOperation the inter operation
	 * @return the international operation
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation cancelInternationalOperation(InternationalOperation interOperation) throws ServiceException{
		InternationalOperation internationalOperation = getInternationalOperation(interOperation.getIdInternationalOperationPk());
		internationalOperation.setOperationState(InternationalOperationStateType.CANCELADA.getCode());
		internationalOperation.setTradeOperation(interOperation.getTradeOperation());
		update(internationalOperation);
		return internationalOperation;
	}
	
	/**
	 * Gets the operations type list.
	 *
	 * @return the operations type list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<OperationType> getOperationsTypeList() throws ServiceException{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT new com.pradera.model.component.OperationType(operType.idOperationTypePk, operType.operationName) FROM OperationType operType ");
		stringBuilder.append("	WHERE operType.idOperationTypePk IN :idOperationsListParam");
		List<Long> idOperationPkList = Arrays.asList(RECEPTION_INTERNATIONAL_SECURITIES.getCode(),SENDING_INTERNATIONAL_SECURITIES.getCode());
		Query query = em.createQuery(stringBuilder.toString());
		query.setParameter("idOperationsListParam", idOperationPkList);
		return (List<OperationType>)query.getResultList(); 
	}
	
	/**
	 * Generate internat operation file service bean.
	 *
	 * @param internOperations the intern operations
	 * @return the boolean
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public DepositaryOperationFile generateInternatOperationFileServiceBean(List<InternationalOperation> internOperations) throws ServiceException, IOException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		DepositaryOperationFile depositaryOperationFile = new DepositaryOperationFile();
		List<InternationalFileDetail> interFileDetails = new ArrayList<InternationalFileDetail>();
		Long totalOperations = 0L;
		
		StringBuilder fileName = new StringBuilder();
		fileName.append(NegotiationConstant.INTERN_OPERATION_FILE);
		fileName.append(CommonsUtilities.convertDateToString(new Date(), CommonsUtilities.DATE_PATTERN_OPTIONAL));
		fileName.append("_");
		fileName.append(CommonsUtilities.convertDateToString(new Date(), CommonsUtilities.HOUR_PATTERN_OPTIONAL));
		
		
		File res = File.createTempFile(fileName.toString(), NegotiationConstant.INTERN_OPERATION_FILE_EXTENSION);
		FileOutputStream is = new FileOutputStream(res);
        OutputStreamWriter osw = new OutputStreamWriter(is);   
        Writer w = new BufferedWriter(osw);
		
        w.write(generateHeaderForInternationalFile());
		for(InternationalOperation internationalOperation: internOperations){
			w.write("\n");
			InternationalFileDetail interDetail = new InternationalFileDetail();
			InternationalOperation interOperation = em.find(InternationalOperation.class, internationalOperation.getIdInternationalOperationPk());
			interDetail.setInternationalOperation(interOperation);
			interDetail.setDepositaryOperationFile(depositaryOperationFile);
			interDetail.setRegistryUser(loggerUser.getUserName());
			interDetail.setRegistyDate(new Date());
			interDetail.setAudit(loggerUser);
			interFileDetails.add(interDetail);
			totalOperations = totalOperations + 1;
			w.write(generateRowForInternationaFIle(interOperation));
		}
		w.close();
		byte[] fileBytes = XMLUtils.getBytesFromFile(res);
		fileName.append(NegotiationConstant.INTERN_OPERATION_FILE_EXTENSION);
		
		depositaryOperationFile.setGenerationDate(new Date());
		depositaryOperationFile.setTotalOperations(totalOperations);
		depositaryOperationFile.setFileName(fileName.toString());
		depositaryOperationFile.setDepositaryFile(fileBytes);
//		depositaryOperationFile.setRegistryUser(loggerUser.getUserName());
		depositaryOperationFile.setRegistryUser(new Date());
		depositaryOperationFile.setRegistyDate(new Date());
		depositaryOperationFile.setInternationalFileDetail(interFileDetails);
		
		create(depositaryOperationFile);
 		return depositaryOperationFile;
	}
	
	/**
	 * Generate header for international file.
	 *
	 * @return the string
	 */
	private String generateHeaderForInternationalFile() {
		// TODO Auto-generated method stub
		String[] header = {"Type_of_Settlement_Transaction","Delivery_Without_Matching","Instruction_Type","Financial_Instrument_ISIN","Financial_Instrument_Common",
						   "Place_of_Safekeeping","Senders_Reference","Deal_Reference","Common_Reference","Safekeeping_Account",
						   "Quantity_of_Financial_Instrument","Settlement_Amount","Settlement_Amount_Currency","Requested_Settlement_Date","Trade_Date",
						   "Trade_Time","Deal_Price_Type","Deal_Price_Amount","Deal_Price_Currency","Pool_Reference",
						   "Priority","Processing_Indicator","Hold_Release","FX_Currency","Physical_Transaction",
						   "Immediate_Release","PSET_BIC","PSET_Country","DEAG_REAG_Account","DEAG_REAG_Party_Format",
						   "DEAG_REAG_Party","DEAG_REAG_Narrative","BUYER_SELLER_Account","BUYER_SELLER_Party_Format","BUYER_SELLER_Party",
						   "BUYER_SELLER_Narrative","DECU_Account","DECU_BIC","RECU_Account","RECU_BIC",
						   "SPRO","Registration_Details","Trade_Amount_Amount","Trade_Amount_Currency","BOI",
						   "Stamp_Duty"};
		String dataHeader = "";
		for(String data: header){
			dataHeader = dataHeader+data+",";
		}
		return dataHeader;
	}

	/**
	 * Generate row for internationa f ile.
	 *
	 * @param interOperation the inter operation
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String generateRowForInternationaFIle(InternationalOperation interOperation) throws IOException{
		String COMA = ",";
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Trade Settlement").append(COMA);
		stringBuilder.append("NO").append(COMA);
		
		if(interOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
			stringBuilder.append(InternOperationFileAttibutesType.INSTRUCTION_TYPE_SEND.getValue());
		}else if(interOperation.getTransferType().equals(InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode())){
			stringBuilder.append(InternOperationFileAttibutesType.INSTRUCTION_TYPE_RECEPT.getValue());
		}
		
		if(interOperation.getSettlementType().equals(SettlementType.DVP.getCode())){
			stringBuilder.append(InternOperationFileAttibutesType.FIL_SETTLEMENT_DVD.getValue()).append(COMA);
		}else if(interOperation.getSettlementType().equals(SettlementType.FOP.getCode())){
			stringBuilder.append(InternOperationFileAttibutesType.FIL_SETTLEMENT_FOP.getValue()).append(COMA);
		}
		
		stringBuilder.append(interOperation.getSecurities().getIdSecurityCodePk()).append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(interOperation.getOperationNumber()).append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append("15862").append(COMA);
		stringBuilder.append(interOperation.getCashAmount()).append(COMA);
		stringBuilder.append(interOperation.getSettlementAmount()).append(COMA);
		stringBuilder.append(CurrencyType.get(interOperation.getCurrency()).getCodeIso()).append(COMA);
		stringBuilder.append(CommonsUtilities.convertDatetoString(interOperation.getSettlementDate())).append(COMA);
		stringBuilder.append(CommonsUtilities.convertDatetoString(interOperation.getOperationDate())).append(COMA);
		stringBuilder.append("").append(COMA);
		stringBuilder.append("").append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(interOperation.getTradeDepository().getBicCode()).append(COMA);
		stringBuilder.append(COMA);
//		stringBuilder.append(interOperation.getSettlementDepository().getDescription()).append("/").append(interOperation.getInternationalParticipant().getAccountNumber()).append(COMA);
		stringBuilder.append("").append(COMA);
		stringBuilder.append(interOperation.getSettlementDepository().getDescription()).append(COMA);
		stringBuilder.append(interOperation.getInternationalParticipant().getDescription()).append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		stringBuilder.append(COMA);
		return stringBuilder.toString();
	}
}//End Class

