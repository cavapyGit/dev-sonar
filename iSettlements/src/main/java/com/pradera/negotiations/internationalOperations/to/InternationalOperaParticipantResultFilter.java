package com.pradera.negotiations.internationalOperations.to;

import java.io.Serializable;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.negotiation.InternationalParticipant;


// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperaParticipantResultFilter.
 *
 * @author : PraderaTechnologies.
 */
public class InternationalOperaParticipantResultFilter implements Serializable {
	
	/** Added default serial version id. */
	private static final long serialVersionUID = 1L;
	
	/** The international depository search. */
	private InternationalDepository internationalDepositorySearch;
	
	/** The international participan search. */
	private InternationalParticipant internationalParticipanSearch;
	
	/**
	 * Gets the international depository search.
	 *
	 * @return the internationalDepositorySearch
	 */
	public InternationalDepository getInternationalDepositorySearch() {
		return internationalDepositorySearch;
	}
	
	/**
	 * Sets the international depository search.
	 *
	 * @param internationalDepositorySearch the internationalDepositorySearch to set
	 */
	public void setInternationalDepositorySearch(
			InternationalDepository internationalDepositorySearch) {
		this.internationalDepositorySearch = internationalDepositorySearch;
	}
	
	/**
	 * Gets the international participan search.
	 *
	 * @return the internationalParticipanSearch
	 */
	public InternationalParticipant getInternationalParticipanSearch() {
		return internationalParticipanSearch;
	}
	
	/**
	 * Sets the international participan search.
	 *
	 * @param internationalParticipanSearch the internationalParticipanSearch to set
	 */
	public void setInternationalParticipanSearch(
			InternationalParticipant internationalParticipanSearch) {
		this.internationalParticipanSearch = internationalParticipanSearch;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		boolean flag=CommonsUtilities.currentDateTime().before(CommonsUtilities.nextDate());
		System.out.println("flag "+flag);
	}
}
