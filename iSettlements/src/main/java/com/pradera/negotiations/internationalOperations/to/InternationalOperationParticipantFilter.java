package com.pradera.negotiations.internationalOperations.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.generalparameter.GeographicLocation;


// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperationParticipantFilter.
 *
 * @author : PraderaTechnologies.
 */
public class InternationalOperationParticipantFilter implements Serializable {
	
	/** Added default serial version id. */
	private static final long serialVersionUID = 1L;
	
	/**  The idInternationalDepositoryPk. */
	private Long idInternationalDepositoryPk;	
	
	/** The country. */
	private Long country;	
	
	/** The description. */
	private String description;
	
	/** The state. */
	private Long state;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The result depost lst. */
	private List<InternationalDepository> resultDepostLst;
	
	/** The result country lst. */
	private List<GeographicLocation> resultCountryLst;
	
	/**
	 * Gets the id international depository pk.
	 *
	 * @return the idInternationalDepositoryPk
	 */
	public Long getIdInternationalDepositoryPk() {
		return idInternationalDepositoryPk;
	}

	/**
	 * Sets the id international depository pk.
	 *
	 * @param idInternationalDepositoryPk the idInternationalDepositoryPk to set
	 */
	public void setIdInternationalDepositoryPk(Long idInternationalDepositoryPk) {
		this.idInternationalDepositoryPk = idInternationalDepositoryPk;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public Long getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the country to set
	 */
	public void setCountry(Long country) {
		this.country = country;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Long getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Long state) {
		this.state = state;
	}

	

	/**
	 * Gets the account number.
	 *
	 * @return the accountNumber
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the result depost lst.
	 *
	 * @return the resultDepostLst
	 */
	public List<InternationalDepository> getResultDepostLst() {
		return resultDepostLst;
	}

	/**
	 * Sets the result depost lst.
	 *
	 * @param resultDepostLst the resultDepostLst to set
	 */
	public void setResultDepostLst(List<InternationalDepository> resultDepostLst) {
		this.resultDepostLst = resultDepostLst;
	}

	/**
	 * Gets the result country lst.
	 *
	 * @return the resultCountryLst
	 */
	public List<GeographicLocation> getResultCountryLst() {
		return resultCountryLst;
	}

	/**
	 * Sets the result country lst.
	 *
	 * @param resultCountryLst the resultCountryLst to set
	 */
	public void setResultCountryLst(List<GeographicLocation> resultCountryLst) {
		this.resultCountryLst = resultCountryLst;
	}

	
}
