package com.pradera.negotiations.internationalOperations.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.primefaces.model.StreamedContent;

import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.OperationType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.InternationalParticipant;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OperationsInternatAttributtes.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/08/2013
 */
public class OperationsInternatAttributtes implements Serializable{
	
	/** The participant local. */
	private List<Participant> participantLocal;
	
	/** The participant by depositary. */
	private List<Participant> participantByDepositary;
	
	/** The inter depositories. */
	private List<InternationalDepository> interDepositories;
	
	/** The inter depositories by participant. */
	private List<InternationalDepository> interDepositoriesByParticipant;
	
	/** The inter participant list. */
	private List<InternationalParticipant> interParticipantList;
	
	/** The inter participant by depostary. */
	private List<InternationalParticipant> interParticipantByDepostary;
	
	/** The current date. */
	private Date currentDate = new Date();
	
	/** The security is fixed instrument. */
	private Boolean securityIsFixedInstrument;
	
	/** The operations type list. */
	private List<OperationType> operationsTypeList;
	
	/** The inter operations state. */
	private List<ParameterTable> interOperationsState;
	
	/** The streamed content. */
	private StreamedContent streamedContent;
	
	/** The fil operation type send. */
	public static String FIL_OPERATION_TYPE_SEND = "Deliver";
	
	/** The fil operation type recept. */
	public static String FIL_OPERATION_TYPE_RECEPT = "Receive";
	
	/** The fil settlement fop. */
	public static String FIL_SETTLEMENT_FOP = "Free payment";
	
	/** The fil settlement dvd. */
	public static String FIL_SETTLEMENT_DVD = "Against payment";
	
	
	/**
	 * Gets the participant local.
	 *
	 * @return the participant local
	 */
	public List<Participant> getParticipantLocal() {
		return participantLocal;
	}
	/**
	 * Sets the participant local.
	 *
	 * @param participantLocal the new participant local
	 */
	public void setParticipantLocal(List<Participant> participantLocal) {
		this.participantLocal = participantLocal;
	}
	/**
	 * Gets the participant by depositary.
	 *
	 * @return the participant by depositary
	 */
	public List<Participant> getParticipantByDepositary() {
		return participantByDepositary;
	}
	/**
	 * Sets the participant by depositary.
	 *
	 * @param participantByDepositary the new participant by depositary
	 */
	public void setParticipantByDepositary(List<Participant> participantByDepositary) {
		this.participantByDepositary = participantByDepositary;
	}
	/**
	 * Gets the inter depositories.
	 *
	 * @return the inter depositories
	 */
	public List<InternationalDepository> getInterDepositories() {
		return interDepositories;
	}
	/**
	 * Sets the inter depositories.
	 *
	 * @param interDepositories the new inter depositories
	 */
	public void setInterDepositories(List<InternationalDepository> interDepositories) {
		this.interDepositories = interDepositories;
	}
	/**
	 * Gets the inter depositories by participant.
	 *
	 * @return the inter depositories by participant
	 */
	public List<InternationalDepository> getInterDepositoriesByParticipant() {
		return interDepositoriesByParticipant;
	}
	/**
	 * Sets the inter depositories by participant.
	 *
	 * @param interDepositoriesByParticipant the new inter depositories by participant
	 */
	public void setInterDepositoriesByParticipant(List<InternationalDepository> interDepositoriesByParticipant) {
		this.interDepositoriesByParticipant = interDepositoriesByParticipant;
	}
	/**
	 * Gets the inter participant list.
	 *
	 * @return the inter participant list
	 */
	public List<InternationalParticipant> getInterParticipantList() {
		return interParticipantList;
	}
	/**
	 * Sets the inter participant list.
	 *
	 * @param interParticipantList the new inter participant list
	 */
	public void setInterParticipantList(List<InternationalParticipant> interParticipantList) {
		this.interParticipantList = interParticipantList;
	}
	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}
	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	/**
	 * Gets the security is fixed instrument.
	 *
	 * @return the security is fixed instrument
	 */
	public Boolean getSecurityIsFixedInstrument() {
		return securityIsFixedInstrument;
	}
	/**
	 * Sets the security is fixed instrument.
	 *
	 * @param securityIsFixedInstrument the new security is fixed instrument
	 */
	public void setSecurityIsFixedInstrument(Boolean securityIsFixedInstrument) {
		this.securityIsFixedInstrument = securityIsFixedInstrument;
	}
	/**
	 * Gets the operations type list.
	 *
	 * @return the operations type list
	 */
	public List<OperationType> getOperationsTypeList() {
		return operationsTypeList;
	}
	/**
	 * Sets the operations type list.
	 *
	 * @param operationsTypeList the new operations type list
	 */
	public void setOperationsTypeList(List<OperationType> operationsTypeList) {
		this.operationsTypeList = operationsTypeList;
	}
	/**
	 * Gets the inter participant by depostary.
	 *
	 * @return the inter participant by depostary
	 */
	public List<InternationalParticipant> getInterParticipantByDepostary() {
		return interParticipantByDepostary;
	}
	/**
	 * Sets the inter participant by depostary.
	 *
	 * @param interParticipantByDepostary the new inter participant by depostary
	 */
	public void setInterParticipantByDepostary(List<InternationalParticipant> interParticipantByDepostary) {
		this.interParticipantByDepostary = interParticipantByDepostary;
	}
	/**
	 * Gets the inter operations state.
	 *
	 * @return the inter operations state
	 */
	public List<ParameterTable> getInterOperationsState() {
		return interOperationsState;
	}
	/**
	 * Sets the inter operations state.
	 *
	 * @param interOperationsState the new inter operations state
	 */
	public void setInterOperationsState(List<ParameterTable> interOperationsState) {
		this.interOperationsState = interOperationsState;
	}
	
	/**
	 * Gets the streamed content.
	 *
	 * @return the streamed content
	 */
	public StreamedContent getStreamedContent() {
		return streamedContent;
	}
	
	/**
	 * Sets the streamed content.
	 *
	 * @param streamedContent the new streamed content
	 */
	public void setStreamedContent(StreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}
}