package com.pradera.negotiations.internationalOperations.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.negotiation.InternationalParticipant;
import com.pradera.model.negotiation.type.InternationalOperationParticipantStateType;
import com.pradera.negotiations.internationalOperations.facade.InternationalOperationParticipantFacade;
import com.pradera.negotiations.internationalOperations.to.InternationalOperaParticipantResultFilter;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationParticipantFilter;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ReteirementDepositeBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaNegotiations
 * @Creation_Date :
 */
@DepositaryWebBean
public class InternationalOperationParticipantBean extends GenericBaseBean {
	
	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The internationalOperationServiceFacade. */
	@EJB
	private InternationalOperationParticipantFacade internationalOperationParticipantFacade;
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** selected depository. */
	private Long depositSelected;
	
	/** list of active depositories. */
	private List<InternationalDepository> activeDepostLst;
	
	/** list of Geographical Locations. */
	private List<GeographicLocation> lstGeographicalLoc;
	
	/** selected country. */
	private Integer countrySelected;
	
	/** selected description. */
	private String descrpitionSelected;
	
	/** selected account number. */
	private Integer accountNumberSelected;	
	
	/** The lstSearchParticipantModel. */
	private GenericDataModel<InternationalOperaParticipantResultFilter> lstSearchParticipantModel;
	
	/** The internationalPartSelected. */
	private InternationalOperaParticipantResultFilter internationalPartSelected;
	
	/** The internationalPartSelected. */
	private InternationalOperaParticipantResultFilter viewDetail;
	
	/** validate modify opearion. */
	private boolean modifyOperation;
	
	/** search selected depository. */
	private Long searchDepositSelected;	
	
	/** search selected country. */
	private Integer searchcountrySelected;
	
	/** search selected description. */
	private String searchDescrpitionSelected;
	
	/** search selected account number. */
	private Integer searchAccountNumberSelected;
	
	/** lstCountry. */
	private List<GeographicLocation> lstCountry;
	
	
	/**
	 * Gets the lst country.
	 *
	 * @return the lstCountry
	 */
	public List<GeographicLocation> getLstCountry() {
		return lstCountry;
	}

	/**
	 * Sets the lst country.
	 *
	 * @param lstCountry the lstCountry to set
	 */
	public void setLstCountry(List<GeographicLocation> lstCountry) {
		this.lstCountry = lstCountry;
	}
	
	/**
	 * Checks if is modify operation.
	 *
	 * @return the modifyOperation
	 */
	public boolean isModifyOperation() {
		return modifyOperation;
	}

	/**
	 * Sets the modify operation.
	 *
	 * @param modifyOperation the modifyOperation to set
	 */
	public void setModifyOperation(boolean modifyOperation) {
		this.modifyOperation = modifyOperation;
	}

	/**
	 * Gets the international part selected.
	 *
	 * @return the internationalPartSelected
	 */
	public InternationalOperaParticipantResultFilter getInternationalPartSelected() {
		return internationalPartSelected;
	}

	/**
	 * Sets the international part selected.
	 *
	 * @param internationalPartSelected the internationalPartSelected to set
	 */
	public void setInternationalPartSelected(
			InternationalOperaParticipantResultFilter internationalPartSelected) {
		this.internationalPartSelected = internationalPartSelected;
	}

	/**
	 * Gets the deposit selected.
	 *
	 * @return the depositSelected
	 */
	public Long getDepositSelected() {
		return depositSelected;
	}

	/**
	 * Sets the deposit selected.
	 *
	 * @param depositSelected the depositSelected to set
	 */
	public void setDepositSelected(Long depositSelected) {
		this.depositSelected = depositSelected;
	}

	/**
	 * Gets the active depost lst.
	 *
	 * @return the activeDepostLst
	 */
	public List<InternationalDepository> getActiveDepostLst() {
		return activeDepostLst;
	}

	/**
	 * Sets the active depost lst.
	 *
	 * @param activeDepostLst the activeDepostLst to set
	 */
	public void setActiveDepostLst(List<InternationalDepository> activeDepostLst) {
		this.activeDepostLst = activeDepostLst;
	}

	
	/**
	 * Gets the lst geographical loc.
	 *
	 * @return the lstGeographicalLoc
	 */
	public List<GeographicLocation> getLstGeographicalLoc() {
		return lstGeographicalLoc;
	}

	/**
	 * Sets the lst geographical loc.
	 *
	 * @param lstGeographicalLoc the lstGeographicalLoc to set
	 */
	public void setLstGeographicalLoc(List<GeographicLocation> lstGeographicalLoc) {
		this.lstGeographicalLoc = lstGeographicalLoc;
	}
	

	/**
	 * Gets the country selected.
	 *
	 * @return the countrySelected
	 */
	public Integer getCountrySelected() {
		return countrySelected;
	}

	/**
	 * Sets the country selected.
	 *
	 * @param countrySelected the countrySelected to set
	 */
	public void setCountrySelected(Integer countrySelected) {
		this.countrySelected = countrySelected;
	}

	
	/**
	 * Gets the descrpition selected.
	 *
	 * @return the descrpitionSelected
	 */
	public String getDescrpitionSelected() {
		return descrpitionSelected;
	}

	/**
	 * Sets the descrpition selected.
	 *
	 * @param descrpitionSelected the descrpitionSelected to set
	 */
	public void setDescrpitionSelected(String descrpitionSelected) {
		this.descrpitionSelected = descrpitionSelected;
	}

	

	
	
	/**
	 * Gets the account number selected.
	 *
	 * @return the accountNumberSelected
	 */
	public Integer getAccountNumberSelected() {
		return accountNumberSelected;
	}

	/**
	 * Sets the account number selected.
	 *
	 * @param accountNumberSelected the accountNumberSelected to set
	 */
	public void setAccountNumberSelected(Integer accountNumberSelected) {
		this.accountNumberSelected = accountNumberSelected;
	}

	/**
	 * Gets the lst search participant model.
	 *
	 * @return the lstSearchParticipantModel
	 */
	public GenericDataModel<InternationalOperaParticipantResultFilter> getLstSearchParticipantModel() {
		return lstSearchParticipantModel;
	}

	/**
	 * Sets the lst search participant model.
	 *
	 * @param lstSearchParticipantModel the lstSearchParticipantModel to set
	 */
	public void setLstSearchParticipantModel(
			GenericDataModel<InternationalOperaParticipantResultFilter> lstSearchParticipantModel) {
		this.lstSearchParticipantModel = lstSearchParticipantModel;
	}

	/**
	 * Gets the view detail.
	 *
	 * @return the viewDetail
	 */
	public InternationalOperaParticipantResultFilter getViewDetail() {
		return viewDetail;
	}

	/**
	 * Sets the view detail.
	 *
	 * @param viewDetail the viewDetail to set
	 */
	public void setViewDetail(InternationalOperaParticipantResultFilter viewDetail) {
		this.viewDetail = viewDetail;
	}

	/**
	 * Checks if is modify oper.
	 *
	 * @return the modifyOperation
	 */
	public boolean isModifyOper() {
		return modifyOperation;
	}

	/**
	 * Sets the modify oper.
	 *
	 * @param modifyOperation the modifyOperation to set
	 */
	public void setModifyOper(boolean modifyOperation) {
		this.modifyOperation = modifyOperation;
	}

	
	

	/**
	 * Gets the search deposit selected.
	 *
	 * @return the searchDepositSelected
	 */
	public Long getSearchDepositSelected() {
		return searchDepositSelected;
	}

	/**
	 * Sets the search deposit selected.
	 *
	 * @param searchDepositSelected the searchDepositSelected to set
	 */
	public void setSearchDepositSelected(Long searchDepositSelected) {
		this.searchDepositSelected = searchDepositSelected;
	}

	/**
	 * Gets the searchcountry selected.
	 *
	 * @return the searchcountrySelected
	 */
	public Integer getSearchcountrySelected() {
		return searchcountrySelected;
	}

	/**
	 * Sets the searchcountry selected.
	 *
	 * @param searchcountrySelected the searchcountrySelected to set
	 */
	public void setSearchcountrySelected(Integer searchcountrySelected) {
		this.searchcountrySelected = searchcountrySelected;
	}

	/**
	 * Gets the search descrpition selected.
	 *
	 * @return the searchDescrpitionSelected
	 */
	public String getSearchDescrpitionSelected() {
		return searchDescrpitionSelected;
	}

	/**
	 * Sets the search descrpition selected.
	 *
	 * @param searchDescrpitionSelected the searchDescrpitionSelected to set
	 */
	public void setSearchDescrpitionSelected(String searchDescrpitionSelected) {
		this.searchDescrpitionSelected = searchDescrpitionSelected;
	}

	/**
	 * Gets the search account number selected.
	 *
	 * @return the searchAccountNumberSelected
	 */
	public Integer getSearchAccountNumberSelected() {
		return searchAccountNumberSelected;
	}

	/**
	 * Sets the search account number selected.
	 *
	 * @param searchAccountNumberSelected the searchAccountNumberSelected to set
	 */
	public void setSearchAccountNumberSelected(Integer searchAccountNumberSelected) {
		if(searchAccountNumberSelected!=null && searchAccountNumberSelected==0){
			searchAccountNumberSelected=null;
		}
		this.searchAccountNumberSelected = searchAccountNumberSelected;
	}

	/**
	 * Default Constructor.
	 */
	public InternationalOperationParticipantBean() {
		depositSelected=-1l;
	}

	/**
	 * init Method.
	 */
	@PostConstruct
	public void init() {
		
		try{
			if (userInfo.getUserAccountSession().isDepositaryInstitution()) {
				activeDepostLst=lstDepository();
				lstGeographicalLoc=lstCountry();
			}
			
			PrivilegeComponent privilegeComponent = new PrivilegeComponent();
			privilegeComponent.setBtnModifyView(true);
			userInfo.setPrivilegeComponent(privilegeComponent);
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * It gives active state depositories.
	 *
	 * @return the List<InternationalDepository>
	 * @throws ServiceException the service exception
	 */
	private List<InternationalDepository> lstDepository()throws ServiceException {
		return internationalOperationParticipantFacade.lstDepositoryFacade(InternationalDepositoryStateType.ACTIVE.getCode());
		 
	}
	
	/**
	 * It gives active state participant in the corresponding depository.
	 *
	 * @return the List<InternationalOperaParticipantResultFilter>
	 */	
	
	public void searchInterParticipants() {
		hideDialogs();
		try{
			InternationalOperationParticipantFilter partFilter=new InternationalOperationParticipantFilter();
			partFilter.setIdInternationalDepositoryPk(searchDepositSelected);
			partFilter.setCountry(Long.valueOf(searchcountrySelected));
			partFilter.setDescription(searchDescrpitionSelected);
			partFilter.setAccountNumber(searchAccountNumberSelected);		
			partFilter.setResultDepostLst(activeDepostLst);
			partFilter.setResultCountryLst(lstGeographicalLoc);
			partFilter.setState(InternationalOperationParticipantStateType.REGISTERED.getCode());
			List<InternationalOperaParticipantResultFilter> lstSerachResult= internationalOperationParticipantFacade.searchInterParticipantsFacade(partFilter);
			if(lstSerachResult.size()>=1){
				lstSearchParticipantModel=new GenericDataModel<InternationalOperaParticipantResultFilter>(lstSerachResult);
			}else{
				lstSearchParticipantModel = new GenericDataModel<InternationalOperaParticipantResultFilter>();
			}
			if(searchAccountNumberSelected!=null && searchAccountNumberSelected==0){
				searchAccountNumberSelected=null;
			}		
		}catch (Exception e) {
           excepcion.fire(new ExceptionToCatchEvent(e));
		}
		 
	}
	
	/**
	 * It gives active state depositories.
	 *
	 * @return the List<InternationalDepository>
	 */
	public List<GeographicLocation> lstCountry() 
	{
		try{
			lstCountry =  internationalOperationParticipantFacade.lstCountryFacade(1,1);
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		return lstCountry;
	}
	
	/**
	 * It shows selected participant details.
	 *
	 * @param detailsParticipant the InternationalOperaParticipantResultFilter
	 * @return the string
	 */
	public String showDetails(InternationalOperaParticipantResultFilter detailsParticipant){
		 viewDetail=detailsParticipant;
		 return "internationalPartDetail";
	}
	
	/**
	 * It checks selected record for modification or not.
	 *
	 * @return page name the String type
	 */
	public String modifyInterParticipants(){
		hideDialogs();
		this.setViewOperationType(ViewOperationsType.MODIFY.getCode());
		String page=null;
		if(internationalPartSelected!=null){
		modifyOperation=true;
		countrySelected=internationalPartSelected.getInternationalParticipanSearch().getCountry().intValue();
		descrpitionSelected=internationalPartSelected.getInternationalParticipanSearch().getDescription();
		accountNumberSelected=internationalPartSelected.getInternationalParticipanSearch().getAccountNumber();	
		depositSelected=searchDepositSelected;
		page="internationalPartModify";
		}else{
			// not selected the record to modify then it show dialog
			JSFUtilities.showComponent(":idnotsel");
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,null, 
					PropertiesConstants.MSG_PARTICIPANT_MODIFY_NOTSEL, null);
		}
		return page;
	}
	
	/**
	 * It saves the international participant data.
	 */
	@LoggerAuditWeb
	public void saveInternationalParticipant(){
		hideDialogs();
		try{
			this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
			InternationalParticipant interPart=new InternationalParticipant();
			InternationalDepository interDeposit=new InternationalDepository();
			// save selected depository
			interDeposit.setIdInternationalDepositoryPk(depositSelected);
			interPart.setInternationalDepository(interDeposit);
			interPart.setCountry(Long.valueOf(countrySelected));
			interPart.setDescription(descrpitionSelected);
			interPart.setAccountNumber(accountNumberSelected);
			interPart.setState(InternationalOperationParticipantStateType.REGISTERED.getCode());
			internationalOperationParticipantFacade.saveInternationalParticipantFacade(interPart);
			JSFUtilities.showComponent(":idSaveSucc");
			JSFUtilities.showMessageOnDialog(
				PropertiesConstants.MESSAGES_SUCCESFUL,null, 
				PropertiesConstants.MSG_PARTICIPANT_SAVE_SUCC, new Object[]{String.valueOf(accountNumberSelected)});
		}catch (Exception e) {
	           excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * It fires when we return from register window.
	 */
	public void showBackDialog(){
		hideDialogs();
		//return confirmation dialog
		JSFUtilities.showComponent(":idBackCnf");
		JSFUtilities.showMessageOnDialog(
				PropertiesConstants.MESSAGES_ALERT,null, 
				PropertiesConstants.MSG_PARTICIPANT_BACK, null);		
	}
	
	/**
	 * It hides all the dialogs.
	 */
	public void hideDialogs(){
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent(":idSaveCnf");
		JSFUtilities.hideComponent(":idSaveSucc");
		JSFUtilities.hideComponent(":idBackCnf");
		JSFUtilities.hideComponent(":idModifyVerify");
		JSFUtilities.hideComponent(":idModifyVerifySucCnf");
		JSFUtilities.hideComponent(":idSerachEmpty");
		JSFUtilities.hideComponent(":idnotsel");
		JSFUtilities.hideComponent(":idModifySUCC");
	}
	
	/**
	 * Change deposit selected.
	 */
	public void changeDepositSelected(){
		executeAction();
		hideDialogs();
		
		if(getViewOperationType() != null && isViewOperationRegister()){
			countrySelected=null;
			descrpitionSelected=null;
			accountNumberSelected=null;
		
			JSFUtilities.resetComponent("frmInternationalOperations:idParticipantCountry");
			JSFUtilities.resetComponent("frmInternationalOperations:iddesc");
			JSFUtilities.resetComponent("frmInternationalOperations:idAccount");
		}else{
			searchAccountNumberSelected=null;
			searchcountrySelected=null;
			searchDescrpitionSelected=null;
			lstSearchParticipantModel=null;
			
			JSFUtilities.resetComponent("frmConsultaMovTitulares:idInterParticipantCountry");
			JSFUtilities.resetComponent("frmConsultaMovTitulares:idaccnum");
			JSFUtilities.resetComponent("frmConsultaMovTitulares:idpartname");
		}		
	}
	
	/**
	 * Validate account number.
	 */
	@LoggerAuditWeb
	public void validateAccountNumber(){
		try {
			executeAction();
			hideDialogs();
			
			if(Validations.validateIsNotNull(accountNumberSelected) && accountNumberSelected.equals(GeneralConstants.ONE_VALUE_INTEGER)){
				accountNumberSelected = null;				
				JSFUtilities.addContextMessage("frmInternationalOperations:idAccount",FacesMessage.SEVERITY_ERROR,null,null);
				return;
			}
			
			if(validateExistsAccountNumberAccountNumber()){
				String depositDescription = null;
				for (InternationalDepository item : activeDepostLst) {
					if(item.getIdInternationalDepositoryPk().equals(depositSelected)) {
						depositDescription = item.getDescription();
						break;
					}
				}				
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,null,
						PropertiesConstants.PARTICIPANT_ACCOUNT_NUMBER_REPEATED,new Object[]{depositDescription});
				JSFUtilities.showSimpleValidationDialog();
								
				JSFUtilities.addContextMessage("frmInternationalOperations:idAccount",FacesMessage.SEVERITY_ERROR,null,null);
				accountNumberSelected = null;
			}
		
		
		}catch (Exception e) {
	        excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Validate exists account number account number.
	 *
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	private boolean validateExistsAccountNumberAccountNumber() throws ServiceException {
		InternationalParticipant interPart=new InternationalParticipant();
		InternationalDepository interDeposit=new InternationalDepository();
		// save selected depository
		interDeposit.setIdInternationalDepositoryPk(depositSelected);
		interPart.setInternationalDepository(interDeposit);
		interPart.setAccountNumber(accountNumberSelected);
		
		if(Validations.validateIsNotNull(internationalPartSelected) && 
				Validations.validateIsNotNull(internationalPartSelected.getInternationalParticipanSearch()) &&
				Validations.validateIsNotNullAndPositive(internationalPartSelected.getInternationalParticipanSearch().getIdInterParticipantPk())) {
			interPart.setIdInterParticipantPk(internationalPartSelected.getInternationalParticipanSearch().getIdInterParticipantPk());
		}			
		
		interPart = internationalOperationParticipantFacade.validateExistsAccountNumber(interPart);
		
		return Validations.validateIsNotNull(interPart); 
	}
	
	
	
	/**
	 * It shows before save the data.
	 */
	public void beforeSaveDialog(){
		
		try {
			hideDialogs();
			
			if(countrySelected==null ||(countrySelected!=null && countrySelected == -1)){
				JSFUtilities.addContextMessage("frmInternationalOperations:idParticipantCountry",FacesMessage.SEVERITY_ERROR,"","");			
				JSFUtilities.showRequiredDialog();
				return;
			}
			
			if(validateExistsAccountNumberAccountNumber()){
				String depositDescription = null;
				for (InternationalDepository item : activeDepostLst) {
					if(item.getIdInternationalDepositoryPk().equals(depositSelected)) {
						depositDescription = item.getDescription();
						break;
					}
				}				
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,null,
						PropertiesConstants.PARTICIPANT_ACCOUNT_NUMBER_REPEATED,new Object[]{depositDescription});
				JSFUtilities.showSimpleValidationDialog();
								
				JSFUtilities.addContextMessage("frmInternationalOperations:idAccount",FacesMessage.SEVERITY_ERROR,null,null);
				accountNumberSelected = null;				
				return;
			}
			
			//before save the data based on the user action dialog
			JSFUtilities.showComponent(":idSaveCnf");
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT_CONFIRM,null, 
					PropertiesConstants.MSG_PARTICIPANT_SAVE, null);
		
		}catch (Exception e) {
	        excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * It returns to search page.
	 *
	 * @return the string
	 */
	public String showSearch(){
		hideDialogs();
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		return	"searchInternationalParticipant";
	}
	
	/**
	 * It checks existed data modified or not.
	 */
	public void saveModifyInterParticipants(){
		try {
			hideDialogs();
			boolean notModifySave=false;
			boolean accNumberModify=false;
			if(internationalPartSelected.getInternationalParticipanSearch().getAccountNumber()!=null){
			accNumberModify=internationalPartSelected.getInternationalParticipanSearch().getAccountNumber().equals(accountNumberSelected);
			}
			
			if(internationalPartSelected.getInternationalParticipanSearch().getCountry().intValue()==countrySelected.intValue() && internationalPartSelected.getInternationalParticipanSearch().getDescription().equals(descrpitionSelected) && accNumberModify ){
				notModifySave=true;
			}
			if(notModifySave){
				//if user is not modified, show alert
				JSFUtilities.showComponent(":idModifyVerify");		
				JSFUtilities.showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT, null, 
						PropertiesConstants.MSG_PARTICIPANT_MODIFY_NOTMODIFY,null);
			}else{
				
				if(validateExistsAccountNumberAccountNumber()){
					JSFUtilities.addContextMessage("frmInternationalOperations:idAccount",FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_ACCOUNT_NUMBER_REPEATED),
							PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_ACCOUNT_NUMBER_REPEATED));
					
					String depositDescription = null;
					for (InternationalDepository item : activeDepostLst) {
						if(item.getIdInternationalDepositoryPk().equals(depositSelected)) {
							depositDescription = item.getDescription();
							break;
						}
					}
					
					JSFUtilities.showMessageOnDialog(
							PropertiesConstants.MESSAGES_ALERT,null, 
							PropertiesConstants.PARTICIPANT_ACCOUNT_NUMBER_REPEATED, new Object[]{depositDescription});
					JSFUtilities.showComponent("cnfMsgCustomValidation");			
					
					accountNumberSelected = null;
					
					return;
				}
				
				//save the modified data based on user action
				JSFUtilities.showComponent(":idModifyVerifySucCnf");
				JSFUtilities.showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT_CONFIRM, null,
						PropertiesConstants.MSG_PARTICIPANT_MODIFY_CONFIRM,null);
			}
		
		}catch (Exception e) {
	        excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * It updated the modified data.
	 */
	@LoggerAuditWeb
	public void modifyDetailsSucc(){
		hideDialogs();
		List<String> modifyInfo=new ArrayList<String>();
		InternationalParticipant interPart=new InternationalParticipant();
		InternationalDepository interDeposit=new InternationalDepository();
		// save selected depository in international participant table
		interDeposit.setIdInternationalDepositoryPk(depositSelected);
		interPart.setInternationalDepository(interDeposit);
		interPart.setCountry(Long.valueOf(countrySelected));
		interPart.setDescription(descrpitionSelected);
		interPart.setAccountNumber(accountNumberSelected);
		interPart.setIdInterParticipantPk(internationalPartSelected.getInternationalParticipanSearch().getIdInterParticipantPk());
		interPart.setState(InternationalOperationParticipantStateType.REGISTERED.getCode());
		try{ 
			//saving the modify data
		internationalOperationParticipantFacade.modifyInternationalParticipantFacade(interPart);
		modifyInfo.add(String.valueOf(accountNumberSelected)+" - "+descrpitionSelected);
		
		internationalPartSelected=null;		
		modifyOperation=false;		
		searchInterParticipants();
		clear();
		//modification success message
		JSFUtilities.showComponent(":idModifySUCC");		
		JSFUtilities.showMessageOnDialog(
				PropertiesConstants.MESSAGES_SUCCESFUL, null,
				PropertiesConstants.MSG_PARTICIPANT_MODIFY_SUCC,new Object[]{modifyInfo});
		
		}catch (Exception e) {
	           excepcion.fire(new ExceptionToCatchEvent(e));
			}	
		
	}
	
	/**
	 * It opens register page.
	 *
	 * @return register page String type
	 */
	public String openRegisterPage(){
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
		depositSelected=null;
		countrySelected=null;
		descrpitionSelected=null;
		accountNumberSelected=null;
		modifyOperation=false;
		internationalPartSelected=null;
		return "registerInternationalPart";
	}
	
	/**
	 * It clears the input data.
	 */
	public void clearInputData(){
		searchAccountNumberSelected=null;
		searchcountrySelected=null;
		searchDepositSelected=null;
		searchDescrpitionSelected=null;
		//search result list
		lstSearchParticipantModel=null;
		
		JSFUtilities.resetComponent("frmConsultaMovTitulares:idInterParticipant");
		JSFUtilities.resetComponent("frmConsultaMovTitulares:idInterParticipantCountry");
		JSFUtilities.resetComponent("frmConsultaMovTitulares:idaccnum");
		JSFUtilities.resetComponent("frmConsultaMovTitulares:idpartname");
	}
	
	/**
	 * It clears search and register page data.
	 */
	public void clear(){
		depositSelected=null;
		countrySelected=null;
		descrpitionSelected=null;
		accountNumberSelected=null;
		
		if(getViewOperationType() != null && isViewOperationRegister()){
			JSFUtilities.resetComponent("frmInternationalOperations:idinterPart");
			JSFUtilities.resetComponent("frmInternationalOperations:idParticipantCountry");
			JSFUtilities.resetComponent("frmInternationalOperations:iddesc");
			JSFUtilities.resetComponent("frmInternationalOperations:idAccount");
		}
	}
	
	/**
	 * It fires when click on the return button.
	 *
	 * @return page info String type
	 */
	 public String backSucc(){
		if(searchAccountNumberSelected!=null && searchAccountNumberSelected==0){
			searchAccountNumberSelected=null;
		}
		modifyOperation=false;
		return "searchInternationalParticipant";
 	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
}