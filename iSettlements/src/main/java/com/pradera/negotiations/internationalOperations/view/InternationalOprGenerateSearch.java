package com.pradera.negotiations.internationalOperations.view;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOprGenerateSearch.
 *
 * @author : PraderaTechnologies.
 */

public class InternationalOprGenerateSearch  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The idInternationalOperationPk. */
	private Long idInternationalOperationPk;
	/** The operationNumber. */
	private Long operationNumber;
	/** The operationDate. */
	private Date operationDate;
	/** The operationType. */
	private Long operationType;
	/** The strSettlementType. */
	private String strSettlementType;
	/** The strTransferType. */
	private String strTransferType;
	
	/**
	 * Gets the str transfer type.
	 *
	 * @return the strTransferType
	 */
	public String getStrTransferType() {
		return strTransferType;
	}
	
	/**
	 * Sets the str transfer type.
	 *
	 * @param strTransferType the strTransferType to set
	 */
	public void setStrTransferType(String strTransferType) {
		this.strTransferType = strTransferType;
	}
	
	/**
	 * Gets the str settlement type.
	 *
	 * @return the strSettlementType
	 */
	public String getStrSettlementType() {
		return strSettlementType;
	}
	
	/**
	 * Sets the str settlement type.
	 *
	 * @param strSettlementType the strSettlementType to set
	 */
	public void setStrSettlementType(String strSettlementType) {
		this.strSettlementType = strSettlementType;
	}
	
	/**
	 * Gets the id international operation pk.
	 *
	 * @return the idInternationalOperationPk
	 */
	public Long getIdInternationalOperationPk() {
		return idInternationalOperationPk;
	}
	
	/**
	 * Sets the id international operation pk.
	 *
	 * @param idInternationalOperationPk the idInternationalOperationPk to set
	 */
	public void setIdInternationalOperationPk(Long idInternationalOperationPk) {
		this.idInternationalOperationPk = idInternationalOperationPk;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the operation type.
	 *
	 * @return the operationType
	 */
	public Long getOperationType() {
		return operationType;
	}
	
	/**
	 * Sets the operation type.
	 *
	 * @param operationType the operationType to set
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}


}
