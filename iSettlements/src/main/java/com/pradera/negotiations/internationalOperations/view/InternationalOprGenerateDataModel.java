package com.pradera.negotiations.internationalOperations.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOprGenerateDataModel.
 *
 * @author : PraderaTechnologies.
 */

public class InternationalOprGenerateDataModel extends ListDataModel<InternationalOprGenerateSearch> implements SelectableDataModel<InternationalOprGenerateSearch>{

	/**  The size. */
	private int size;
	
	/**
	 *  The default constructor.
	 */
	public InternationalOprGenerateDataModel(){
		super();
	}
	
	/**
	 *  
	 *  The Constructor .
	 *
	 * @param data  
	 */
	public InternationalOprGenerateDataModel(List<InternationalOprGenerateSearch> data){
		super(data);
	}
	
	/**
	 *  Gets particular data of selected row.
	 *
	 * @param rowKey the row key
	 * @return the row data
	 */
	
	@Override
	@SuppressWarnings("unchecked")
	public InternationalOprGenerateSearch getRowData(String rowKey) {
		List<InternationalOprGenerateSearch> interOpr=(List<InternationalOprGenerateSearch>)getWrappedData();
        for(InternationalOprGenerateSearch internationalOprGenerateSearch : interOpr) {  
            if(String.valueOf(internationalOprGenerateSearch.getIdInternationalOperationPk()).equals(rowKey))
                return internationalOprGenerateSearch;  
        }  
		return null;
	}

	/**
	 *  Gets particular row key of selected row.
	 *
	 * @param interOpr the inter opr
	 * @return the row key
	 */
	@Override
	public Object getRowKey(InternationalOprGenerateSearch interOpr) {
		return interOpr.getIdInternationalOperationPk();
	}

	/**
	 *  Gets size of data table.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public int getSize() {
		List<InternationalOprGenerateSearch> mechaOpr=(List<InternationalOprGenerateSearch>)getWrappedData();
		if(mechaOpr==null)
			return 0;
		return mechaOpr.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
}
