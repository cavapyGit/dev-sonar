package com.pradera.negotiations.internationalOperations.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.negotiations.internationalOperations.to.TimerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NegotiationSchedulerServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class NegotiationSchedulerServiceBean extends CrudDaoServiceBean{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The timer service. */
	@Resource
	TimerService timerService;

	/** The log. */
	@Inject
	PraderaLogger log;
	
	/**
	 * Load timers.
	 */
	@PostConstruct
	public void loadTimers() {		
		try{			
			for(InternationalOperation internationalOperation : getInternationalOperations()){
				registerTimer(internationalOperation);
			}
		} catch (ServiceException ex) {
			log.info("Error loading InternationalOperation timers :" + ex.getMessage());
		}
	}
	
	/**
	 * Register timer.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public <T> void registerTimer (T t) {
		TimerConfig timerConfig = null;
		Calendar cal = Calendar.getInstance();
		
		if(t.getClass().getName().equals(InternationalOperation.class.getName())){
			InternationalOperation internationalOperation = (InternationalOperation)t;
			TimerTO timerTO = new TimerTO(internationalOperation.getIdInternationalOperationPk(), 
					internationalOperation.getClass().getName(), internationalOperation.getOperationState());
			timerConfig = new TimerConfig(timerTO, Boolean.FALSE);
			
			
			Date settlementDate = internationalOperation.getSettlementDate();
			if(!settlementDate.equals(CommonsUtilities.currentDate())){
				cal.setTime(settlementDate);
			}else{
				Date newDate = CommonsUtilities.addDaysToDate(settlementDate, BigDecimal.ONE.intValue());
				cal.setTime(newDate);
			}
			
		}
		
		if(timerConfig != null){
			timerService.createSingleActionTimer(cal.getTime(), timerConfig);
		}
	}
	
	/**
	 * Execute timer.
	 *
	 * @param timer the timer
	 */
	@Timeout
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeTimer(Timer timer) {	
		TimerTO timerTO = (TimerTO) timer.getInfo();
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		
		if(timerTO.getEntityClass().equals(InternationalOperation.class.getName())){
			sbQuery.append(" update InternationalOperation io");
			sbQuery.append(" set io.operationState = :operationStatePrm");
			sbQuery.append(" where io.idInternationalOperationPk = :idInternationalOperationPkPrm");
			
			query = em.createQuery(sbQuery.toString());
			
			if(InternationalOperationStateType.REGISTRADA.getCode().equals(timerTO.getEntityState())){
				query.setParameter("operationStatePrm", InternationalOperationStateType.ANULADA.getCode());
			}else if (InternationalOperationStateType.APROBADA.getCode().equals(timerTO.getEntityState())){
				query.setParameter("operationStatePrm", InternationalOperationStateType.RECHAZADA.getCode());
			}else{
				query.setParameter("operationStatePrm", InternationalOperationStateType.CANCELADA.getCode());
			}
			query.setParameter("idInternationalOperationPkPrm", timerTO.getEntityId());
		}
		
		if(query != null){
			try {
				query.executeUpdate();
			} catch (NoResultException ex) {
				log.info("Error in IntOpeSchedulerServiceBean, method executeTimer : " + ex.getMessage());
			}
		}
	}
	
	/**
	 * Update timer.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @throws ServiceException the service exception
	 */
	public <T> void updateTimer(T t) throws ServiceException {
		TimerTO timerTO = null;
		
		if(t.getClass().getName().equals(InternationalOperation.class.getName())){
			InternationalOperation internationalOperation = (InternationalOperation)t;
			timerTO = new TimerTO(internationalOperation.getIdInternationalOperationPk(), 
					internationalOperation.getClass().getName(), internationalOperation.getOperationState());
		}
		
		if(timerTO != null){
			Timer timer = getTimer(timerTO);
			if(timer != null){
				deleteTimer(timerTO);				
			}
			registerTimer(t);
		}
	}
	
	/**
	 * Request delete.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @throws ServiceException the service exception
	 */
	public <T> void requestDelete(T t) throws ServiceException {
		TimerTO timerTO = null;
		
		if(t.getClass().getName().equals(InternationalOperation.class.getName())){
			InternationalOperation internationalOperation = (InternationalOperation)t;
			timerTO = new TimerTO(internationalOperation.getIdInternationalOperationPk(), 
					internationalOperation.getClass().getName(), internationalOperation.getOperationState());
		}
		
		if(timerTO != null){
			Timer timer = getTimer(timerTO);
			if(timer != null){
				deleteTimer(timerTO);				
			}
		}
	}
	
	/**
	 * Gets the timer.
	 *
	 * @param timerTO the timer to
	 * @return the timer
	 */
	private Timer getTimer(TimerTO timerTO) {
		Collection<Timer> timers = timerService.getTimers();
		for (Timer t : timers) {
			if (timerTO.equals((TimerTO) t.getInfo())) {
				return t;
			}
		}
		return null;
	}
	
	/**
	 * Delete timer.
	 *
	 * @param timerTO the timer to
	 */
	public void deleteTimer(TimerTO timerTO) {
		Timer timer = getTimer(timerTO);
		if (timer != null) {
			timer.cancel();
		}
	}
	
	/**
	 * Gets the international operations.
	 *
	 * @return the international operations
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalOperation> getInternationalOperations() throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select new com.pradera.model.negotiation.InternationalOperation( io.idInternationalOperationPk, io.settlementDate, io.operationState )");
		sbQuery.append(" from InternationalOperation io");
		sbQuery.append(" where io.settlementDate <= :settlementDatePrm");
		sbQuery.append(" and io.operationState in :operationStateListPrm");

		Query query = em.createQuery(sbQuery.toString());

		Calendar cal = Calendar.getInstance();
		query.setParameter("settlementDatePrm", cal.getTime());
		
		List<Integer> listState = new ArrayList<Integer>();
		listState.add(InternationalOperationStateType.REGISTRADA.getCode());
		listState.add(InternationalOperationStateType.APROBADA.getCode());
		listState.add(InternationalOperationStateType.CONFIRMADA.getCode());
		query.setParameter("operationStateListPrm", listState);
		
		List<InternationalOperation> internationalOperations = new ArrayList<InternationalOperation>();
		
		try {
			internationalOperations = (List<InternationalOperation>)query.getResultList();
		} catch (NoResultException ex) {
			log.info("Error in IntOpeSchedulerServiceBean, method getInternationalOperation : " + ex.getMessage());
		}
		
		for(InternationalOperation internationalOperation : internationalOperations){
			TimerTO timerTO = new TimerTO(internationalOperation.getIdInternationalOperationPk(),
					internationalOperation.getClass().getName(), internationalOperation.getOperationState());
			Timer timer = getTimer(timerTO);
			if(timer != null){
				internationalOperations.remove(internationalOperation);
			}
		}
		
		return internationalOperations;
	}
}
