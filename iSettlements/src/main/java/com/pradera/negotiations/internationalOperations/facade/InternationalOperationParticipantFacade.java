package com.pradera.negotiations.internationalOperations.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.negotiation.InternationalParticipant;
import com.pradera.negotiations.internationalOperations.service.InternationalOperationParticipantServiceBean;
import com.pradera.negotiations.internationalOperations.to.InternationalOperaParticipantResultFilter;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationParticipantFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperationParticipantFacade.
 *
 * @author PraderaTechnologies
 * The Class McnManualServiceFacade
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InternationalOperationParticipantFacade implements Serializable {

	
	/** Added default serial version id. */
	private static final long serialVersionUID = 1L;
	
	/** The mcnManualServiceBean. */
	@EJB
	InternationalOperationParticipantServiceBean internationalOperationParticipantServiceBean;
	
	/** Holds current logger user details. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * It gives active state repositories.
	 *
	 * @param state the state
	 * @return the List<InternationalDepository>
	 * @throws ServiceException the service exception
	 */
	public List<InternationalDepository> lstDepositoryFacade(Integer state ) throws ServiceException{
		return internationalOperationParticipantServiceBean.lstDepositoryService(state);		 
	}
	
	/**
	 * It gives active state participant in the corresponding depository.
	 *
	 * @param partFilterData the InternationalOperationParticipantFilter
	 * @return the List<InternationalOperaParticipantResultFilter>
	 * @throws ServiceException the service exception
	 */
	public List<InternationalOperaParticipantResultFilter> searchInterParticipantsFacade(InternationalOperationParticipantFilter partFilterData)throws ServiceException {
		List<InternationalOperaParticipantResultFilter> lstPart = new ArrayList<InternationalOperaParticipantResultFilter>();
		List<GeographicLocation> countryLst = partFilterData.getResultCountryLst();
		List<Object[]> resultParticipant =	internationalOperationParticipantServiceBean.searchInterParticipantsService(partFilterData);
		for (Object[] obj : resultParticipant)
		{
			InternationalOperaParticipantResultFilter partFilter = new InternationalOperaParticipantResultFilter();		
			InternationalParticipant intPart=(InternationalParticipant) obj[0];
			if(intPart.getCountry()!=null)
			{
				if (countryLst != null) 
				{
					for (GeographicLocation curCountry : countryLst)
					{
						if (intPart.getCountry().intValue()==curCountry.getIdGeographicLocationPk().intValue()) 
						{
							intPart.setCountryDesc(curCountry.getName());
						}
					}
				}
			}
			partFilter.setInternationalParticipanSearch(intPart);			
			partFilter.setInternationalDepositorySearch((InternationalDepository) obj[1]);			
			lstPart.add(partFilter);
		}

	return lstPart;
	}
	
	/**
	 * Used to get Selected Participant country.
	 *
	 * @param state the state
	 * @param loc the loc
	 * @return the String type
	 * @throws ServiceException the service exception
	 */
	/*private String findParticipantCounty(Long curentCountryId) {
		if(curentCountryId!=null){				
		if (countryLst != null) {
			for (GeographicLocation curCountry : countryLst) {
				if (curentCountryId.intValue()==curCountry.getIdGeographicLocationPk().intValue()) {
					return curCountry.getName();
				}
			}
		}
		}
		return null;
	}*/
	/**
	 * It gives geographical locations
	 * 
	 * @param State
	 *            Integer type
	 * @param State
	 *            Integer type
	 * @return the List<GeographicLocation>
	 * @throws ServiceException
	 */	
	public List<GeographicLocation> lstCountryFacade(Integer state,Integer loc )throws ServiceException{
		return internationalOperationParticipantServiceBean.lstCountryService(state,loc);		 
	}
	
	/**
	 * It saves the international participant.
	 *
	 * @param savePartData the InternationalParticipant
	 * @throws ServiceException the service exception
	 */
	public void saveInternationalParticipantFacade(InternationalParticipant savePartData)throws ServiceException{
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		 internationalOperationParticipantServiceBean.saveInternationalParticipantService(savePartData);		 
	}
	
	/**
	 * It update the internationalParticipant table data.
	 *
	 * @param modifyPartData the InternationalParticipant modified data
	 * @throws ServiceException the service exception
	 */
	public void modifyInternationalParticipantFacade(InternationalParticipant modifyPartData) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		internationalOperationParticipantServiceBean.modifyInternationalParticipantService(modifyPartData);	
	}
	
	/**
	 * Validate exists account number.
	 *
	 * @param interPart the inter part
	 * @return the international participant
	 * @throws ServiceException the service exception
	 */
	public InternationalParticipant validateExistsAccountNumber(InternationalParticipant interPart) throws ServiceException{
		return internationalOperationParticipantServiceBean.validateExistsAccountNumber(interPart);
	}
	
}
