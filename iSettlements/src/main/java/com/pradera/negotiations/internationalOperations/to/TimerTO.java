package com.pradera.negotiations.internationalOperations.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class TimerTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class TimerTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The entity id. */
	private Long entityId;
	
	/** The entity class. */
	private String entityClass;
	
	/** The entity state. */
	private Integer entityState;
	
	/**
	 * Instantiates a new timer to.
	 */
	public TimerTO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Instantiates a new timer to.
	 *
	 * @param entityId the entity id
	 * @param entityClass the entity class
	 * @param entityState the entity state
	 */
	public TimerTO(Long entityId, String entityClass, Integer entityState) {
		this.entityId = entityId;
		this.entityClass = entityClass;
		this.entityState = entityState;
	}

	/**
	 * Gets the entity id.
	 *
	 * @return the entity id
	 */
	public Long getEntityId() {
		return entityId;
	}

	/**
	 * Sets the entity id.
	 *
	 * @param entityId the new entity id
	 */
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	/**
	 * Gets the entity class.
	 *
	 * @return the entity class
	 */
	public String getEntityClass() {
		return entityClass;
	}

	/**
	 * Sets the entity class.
	 *
	 * @param entityClass the new entity class
	 */
	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * Gets the entity state.
	 *
	 * @return the entity state
	 */
	public Integer getEntityState() {
		return entityState;
	}

	/**
	 * Sets the entity state.
	 *
	 * @param entityState the new entity state
	 */
	public void setEntityState(Integer entityState) {
		this.entityState = entityState;
	}
	
}
