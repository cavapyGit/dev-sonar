package com.pradera.negotiations.internationalOperations.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperationFilter.
 *
 * @author : PraderaTechnologies.
 */
public class InternationalOperationFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/**  The idSettlementDeposiroty. */
	private Long idSettlementDeposiroty;
	
	/**  The idGenSettleDeposiroty. */
	private Long idGenSettleDeposiroty;
	
	/**  The startGenerateDate. */
	private Date startGenerateDate;
	
	/**  The endGenerateDate. */
	private Date endGenerateDate;
	
	/**  The idTradingDeposiroty. */
	private Long idTradingDeposiroty;	
	
	/**  The idInterParticipantPk. */
	private Long idInterParticipantPk;	
	
	/**  The idInterOperationPk. */
	private Long idInterOperationPk;
	
	/**  The idState. */
	private Integer idState;
	
	/**  The idSettlementType. */
	private Long idSettlementType;
	
	/**  The startDate. */
	private Date startDate;
	
	/**  The endDate. */
	private Date endDate;
	
	/**  The transferType. */
	private Long transferType;
	
	/**  The idIsinCodePk. */
	private String idIsinCodePk;//change idIsinCodePk
	
	/**  The accountNumber. */
	private Integer accountNumber;
	
	/**  The settlementDate. */
	private Date settlementDate;
	
	/**  The settlementAmount. */
	private BigDecimal settlementAmount;
	
	/**  The operationNumber. */
	private Long operationNumber;
	
	/**  The rejectMotive. */
	private Long rejectMotive;
	
	/**  The rejectMotiveOther. */
	private String rejectMotiveOther;
	
	/**  The internationalOprPkList. */
	private List<Long> internationalOprPkList;
	
	/**  The rejectUser. */
	private String rejectUser;
	
	/**  The rejectDate. */
	private Date rejectDate;
	
	/**  The reverseUser. */
	private String reverseUser;
	
	/**  The reverseDate. */
	private Date reverseDate;
	
	/**  The confirmUser. */
	private String confirmUser;
	
	/**  The confirmDate. */
	private Date confirmDate;
	
	/**  The cancelUser. */
	private String cancelUser;
	
	/**  The cancelDate. */
	private Date cancelDate;
	/** The lastModifyApp. */
	private Integer lastModifyApp;
	/** The lastModifyDate. */
	private Date lastModifyDate;
	/** The lastModifyIp. */
	private String lastModifyIp;
	/** The lastModifyUser. */
	private String lastModifyUser;
	
	/**  The reverseMotive. */
	private Long reverseMotive;
	
	/**  The reverseMotiveOther. */
	private String reverseMotiveOther;
	
	/**  The cancelMotive. */
	private Long cancelMotive;
	
	/**  The cancelMotiveOther. */
	private String cancelMotiveOther;
	
	/**  The approveUser. */
	private String approveUser;
	
	/**  The approveDate. */
	private Date approveDate;
	/**The holderAccount. */
	private HolderAccount holderAccount;
	
	/**  The participant. */
	private Participant localParticipant;
	
	/**  The securityDesc. */
	private String securityDesc; 
	
	/** The security. */
	private Security security;

	/**
	 * Gets the security desc.
	 *
	 * @return the securityDesc
	 */
	public String getSecurityDesc() {
		return securityDesc;
	}

	/**
	 * Sets the security desc.
	 *
	 * @param securityDesc the securityDesc to set
	 */
	public void setSecurityDesc(String securityDesc) {
		this.securityDesc = securityDesc;
	}

	/**
	 * Gets the local participant.
	 *
	 * @return the localParticipant
	 */
	public Participant getLocalParticipant() {
		return localParticipant;
	}

	/**
	 * Sets the local participant.
	 *
	 * @param localParticipant the localParticipant to set
	 */
	public void setLocalParticipant(Participant localParticipant) {
		this.localParticipant = localParticipant;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the id gen settle deposiroty.
	 *
	 * @return the id gen settle deposiroty
	 */
	public Long getIdGenSettleDeposiroty() {
		return idGenSettleDeposiroty;
	}

	/**
	 * Sets the id gen settle deposiroty.
	 *
	 * @param idGenSettleDeposiroty the new id gen settle deposiroty
	 */
	public void setIdGenSettleDeposiroty(Long idGenSettleDeposiroty) {
		this.idGenSettleDeposiroty = idGenSettleDeposiroty;
	}

	/**
	 * Gets the start generate date.
	 *
	 * @return the start generate date
	 */
	public Date getStartGenerateDate() {
		return startGenerateDate;
	}

	/**
	 * Sets the start generate date.
	 *
	 * @param startGenerateDate the new start generate date
	 */
	public void setStartGenerateDate(Date startGenerateDate) {
		this.startGenerateDate = startGenerateDate;
	}

	/**
	 * Gets the end generate date.
	 *
	 * @return the end generate date
	 */
	public Date getEndGenerateDate() {
		return endGenerateDate;
	}

	/**
	 * Sets the end generate date.
	 *
	 * @param endGenerateDate the new end generate date
	 */
	public void setEndGenerateDate(Date endGenerateDate) {
		this.endGenerateDate = endGenerateDate;
	}

	/**
	 * Gets the confirm user.
	 *
	 * @return the confirm user
	 */
	public String getConfirmUser() {
		return confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the new confirm user
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the approve user.
	 *
	 * @return the approve user
	 */
	public String getApproveUser() {
		return approveUser;
	}

	/**
	 * Sets the approve user.
	 *
	 * @param approveUser the new approve user
	 */
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}

	/**
	 * Gets the approve date.
	 *
	 * @return the approve date
	 */
	public Date getApproveDate() {
		return approveDate;
	}

	/**
	 * Sets the approve date.
	 *
	 * @param approveDate the new approve date
	 */
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}


	/**
	 * Gets the reverse user.
	 *
	 * @return the reverseUser
	 */
	public String getReverseUser() {
		return reverseUser;
	}

	/**
	 * Sets the reverse user.
	 *
	 * @param reverseUser the reverseUser to set
	 */
	public void setReverseUser(String reverseUser) {
		this.reverseUser = reverseUser;
	}

	/**
	 * Gets the reverse date.
	 *
	 * @return the reverseDate
	 */
	public Date getReverseDate() {
		return reverseDate;
	}

	/**
	 * Sets the reverse date.
	 *
	 * @param reverseDate the reverseDate to set
	 */
	public void setReverseDate(Date reverseDate) {
		this.reverseDate = reverseDate;
	}

	/**
	 * Gets the cancel user.
	 *
	 * @return the cancelUser
	 */
	public String getCancelUser() {
		return cancelUser;
	}

	/**
	 * Sets the cancel user.
	 *
	 * @param cancelUser the cancelUser to set
	 */
	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}

	/**
	 * Gets the cancel date.
	 *
	 * @return the cancelDate
	 */
	public Date getCancelDate() {
		return cancelDate;
	}

	/**
	 * Sets the cancel date.
	 *
	 * @param cancelDate the cancelDate to set
	 */
	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	/**
	 * Gets the cancel motive.
	 *
	 * @return the cancelMotive
	 */
	public Long getCancelMotive() {
		return cancelMotive;
	}

	/**
	 * Sets the cancel motive.
	 *
	 * @param cancelMotive the cancelMotive to set
	 */
	public void setCancelMotive(Long cancelMotive) {
		this.cancelMotive = cancelMotive;
	}

	/**
	 * Gets the cancel motive other.
	 *
	 * @return the cancelMotiveOther
	 */
	public String getCancelMotiveOther() {
		return cancelMotiveOther;
	}

	/**
	 * Sets the cancel motive other.
	 *
	 * @param cancelMotiveOther the cancelMotiveOther to set
	 */
	public void setCancelMotiveOther(String cancelMotiveOther) {
		this.cancelMotiveOther = cancelMotiveOther;
	}

	/**
	 * Gets the reverse motive.
	 *
	 * @return the reverseMotive
	 */
	public Long getReverseMotive() {
		return reverseMotive;
	}

	/**
	 * Sets the reverse motive.
	 *
	 * @param reverseMotive the reverseMotive to set
	 */
	public void setReverseMotive(Long reverseMotive) {
		this.reverseMotive = reverseMotive;
	}

	/**
	 * Gets the reverse motive other.
	 *
	 * @return the reverseMotiveOther
	 */
	public String getReverseMotiveOther() {
		return reverseMotiveOther;
	}

	/**
	 * Sets the reverse motive other.
	 *
	 * @param reverseMotiveOther the reverseMotiveOther to set
	 */
	public void setReverseMotiveOther(String reverseMotiveOther) {
		this.reverseMotiveOther = reverseMotiveOther;
	}


	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the rejectDate
	 */
	public Date getRejectDate() {
		return rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the rejectDate to set
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the rejectUser
	 */
	public String getRejectUser() {
		return rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the rejectUser to set
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the international opr pk list.
	 *
	 * @return the internationalOprPkList
	 */
	public List<Long> getInternationalOprPkList() {
		return internationalOprPkList;
	}

	/**
	 * Sets the international opr pk list.
	 *
	 * @param internationalOprPkList the internationalOprPkList to set
	 */
	public void setInternationalOprPkList(List<Long> internationalOprPkList) {
		this.internationalOprPkList = internationalOprPkList;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the rejectMotive
	 */
	public Long getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the rejectMotive to set
	 */
	public void setRejectMotive(Long rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the rejectMotiveOther
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the rejectMotiveOther to set
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the id inter operation pk.
	 *
	 * @return the idInterOperationPk
	 */
	public Long getIdInterOperationPk() {
		return idInterOperationPk;
	}

	/**
	 * Sets the id inter operation pk.
	 *
	 * @param idInterOperationPk the idInterOperationPk to set
	 */
	public void setIdInterOperationPk(Long idInterOperationPk) {
		this.idInterOperationPk = idInterOperationPk;
	}

	/**
	 * Gets the settlement date.
	 *
	 * @return the settlementDate
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}

	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the settlementDate to set
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlementAmount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the settlementAmount to set
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the accountNumber
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the id isin code pk.
	 *
	 * @return the idIsinCodePk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}

	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the idIsinCodePk to set
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}

	/**
	 * Gets the transfer type.
	 *
	 * @return the transferType
	 */
	public Long getTransferType() {
		return transferType;
	}

	/**
	 * Sets the transfer type.
	 *
	 * @param transferType the transferType to set
	 */
	public void setTransferType(Long transferType) {
		this.transferType = transferType;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the id settlement type.
	 *
	 * @return the idSettlementType
	 */
	public Long getIdSettlementType() {
		return idSettlementType;
	}

	/**
	 * Sets the id settlement type.
	 *
	 * @param idSettlementType the idSettlementType to set
	 */
	public void setIdSettlementType(Long idSettlementType) {
		this.idSettlementType = idSettlementType;
	}

	/**
	 * Gets the id state.
	 *
	 * @return the idState
	 */
	public Integer getIdState() {
		return idState;
	}

	/**
	 * Sets the id state.
	 *
	 * @param idState the idState to set
	 */
	public void setIdState(Integer idState) {
		this.idState = idState;
	}

	/**
	 * Gets the id trading deposiroty.
	 *
	 * @return the idTradingDeposiroty
	 */
	public Long getIdTradingDeposiroty() {
		return idTradingDeposiroty;
	}

	/**
	 * Sets the id trading deposiroty.
	 *
	 * @param idTradingDeposiroty the idTradingDeposiroty to set
	 */
	public void setIdTradingDeposiroty(Long idTradingDeposiroty) {
		this.idTradingDeposiroty = idTradingDeposiroty;
	}

	

	/**
	 * Gets the id settlement deposiroty.
	 *
	 * @return the idSettlementDeposiroty
	 */
	public Long getIdSettlementDeposiroty() {
		return idSettlementDeposiroty;
	}

	/**
	 * Sets the id settlement deposiroty.
	 *
	 * @param idSettlementDeposiroty the idSettlementDeposiroty to set
	 */
	public void setIdSettlementDeposiroty(Long idSettlementDeposiroty) {
		this.idSettlementDeposiroty = idSettlementDeposiroty;
	}

	/**
	 * Gets the id inter participant pk.
	 *
	 * @return the idInterParticipantPk
	 */
	public Long getIdInterParticipantPk() {
		return idInterParticipantPk;
	}

	/**
	 * Sets the id inter participant pk.
	 *
	 * @param idInterParticipantPk the idInterParticipantPk to set
	 */
	public void setIdInterParticipantPk(Long idInterParticipantPk) {
		this.idInterParticipantPk = idInterParticipantPk;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
}