package com.pradera.negotiations.internationalOperations.service;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum InternOperationFileAttibutesType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public enum InternOperationFileAttibutesType {

	/** The instruction type send. */
	INSTRUCTION_TYPE_SEND(1, "Deliver"),
	
	/** The instruction type recept. */
	INSTRUCTION_TYPE_RECEPT(2, "Receive"),
	
	/** The fil settlement fop. */
	FIL_SETTLEMENT_FOP (3,"Free payment"),
	
	/** The fil settlement dvd. */
	FIL_SETTLEMENT_DVD (4,"Against payment"),
	
	/** The cicgsolp. */
	CICGSOLP(Integer.valueOf(948),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<InternOperationFileAttibutesType> list = new ArrayList<InternOperationFileAttibutesType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InternOperationFileAttibutesType> lookup = new HashMap<Integer, InternOperationFileAttibutesType>();

	static {
		for (InternOperationFileAttibutesType s : EnumSet
				.allOf(InternOperationFileAttibutesType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new intern operation file attibutes type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InternOperationFileAttibutesType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the intern operation file attibutes type
	 */
	public static InternOperationFileAttibutesType get(Integer code) {
		return lookup.get(code);
	}
}