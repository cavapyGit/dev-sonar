package com.pradera.negotiations.internationalOperations.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.TypedQuery;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.negotiation.InternationalParticipant;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationParticipantFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperationServiceBean.
 * 
 * @author PraderaTechnologies.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class InternationalOperationParticipantServiceBean extends
		CrudDaoServiceBean {	
	
	

	/**
	 * It gives active state depositories.
	 *
	 * @param State Integer type
	 * @return the List<InternationalDepository>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalDepository> lstDepositoryService(Integer State) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("select idep from InternationalDepository idep ");
		sbQuery.append(" where idep.stateIntDepositary=:stateIntDep ");
		parameters.put("stateIntDep", State);
		List<InternationalDepository> lstResultDeposit = (List<InternationalDepository>) findListByQueryString(
				sbQuery.toString(), parameters);
		return lstResultDeposit;
	}

	/**
	 * It gives active state participant in the corresponding depository.
	 *
	 * @param partFilterData the InternationalOperationParticipantFilter
	 * @return the List<InternationalOperaParticipantResultFilter>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchInterParticipantsService(
			InternationalOperationParticipantFilter partFilterData) throws ServiceException{		
		
		Long idDepost = partFilterData.getIdInternationalDepositoryPk();
		Long sate = partFilterData.getState();
		Integer accNo = partFilterData.getAccountNumber();
		String strDescrption = partFilterData.getDescription();
		Long idcountry = partFilterData.getCountry();
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer sbQuery = new StringBuffer();
	//	sbQuery.append("select ipart,idep,ipart.accountNumber from InternationalDepository idep, ");
		sbQuery.append("select ipart ,idep from InternationalDepository idep, ");
		sbQuery.append("InternationalParticipant ipart ");
		sbQuery.append(" where idep.idInternationalDepositoryPk=ipart.internationalDepository.idInternationalDepositoryPk");
		sbQuery.append(" and ipart.state=:statePart ");
		parameters.put("statePart", sate);
		if (Validations.validateIsNotNull(idDepost) && idDepost > 0l) {
			sbQuery.append(" and idep.idInternationalDepositoryPk=:idepost");
			parameters.put("idepost", idDepost);
		}
		if (Validations.validateIsNotNull(accNo)
				&& accNo>0) {
			sbQuery.append(" and ipart.accountNumber=:accNoValue ");
			parameters.put("accNoValue", accNo);
		}
		if (Validations.validateIsNotNull(strDescrption)
				&& Validations.validateIsNotEmpty(strDescrption)) {
			sbQuery.append(" and ipart.description like :desc");
			parameters.put("desc", "%"+strDescrption+"%");	
			
		}
		if (Validations.validateIsNotNull(idcountry) && idcountry > 0l) {
			sbQuery.append(" and ipart.country=:country ");
			parameters.put("country", idcountry);
		}
		sbQuery.append(" order by ipart.accountNumber ");
		
		 return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
		
	}

	/**
	 * It gives geographical locations.
	 *
	 * @param State Integer type
	 * @param typeGeographic the type geographic
	 * @return the List<GeographicLocation>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<GeographicLocation> lstCountryService(Integer State,
			Integer typeGeographic) throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("select gc from GeographicLocation gc");
		sbQuery.append(" where gc.typeGeographicLocation=:itypeGeographic ");
		sbQuery.append(" and gc.state=:istate order by gc.name");
		parameters.put("itypeGeographic", typeGeographic);
		parameters.put("istate", State);
		List<GeographicLocation> lstResulLocation = findListByQueryString(
				sbQuery.toString(), parameters);
		return lstResulLocation;
	}

	/**
	 * It saves the international participant.
	 *
	 * @param savePartData the InternationalParticipant
	 * @throws ServiceException the service exception
	 */
	public void saveInternationalParticipantService(InternationalParticipant savePartData) throws ServiceException{
		create(savePartData);		
	}
	
	
	/**
	 * It update the internationalParticipant table data.
	 *
	 * @param modifyPartData the InternationalParticipant modified data
	 * @throws ServiceException the service exception
	 */
	public void modifyInternationalParticipantService(InternationalParticipant modifyPartData) throws  ServiceException{ 
		update(modifyPartData);
	}

	/**
	 * Validate exists account number.
	 *
	 * @param interPart the inter part
	 * @return the international participant
	 * @throws ServiceException the service exception
	 */
	public InternationalParticipant validateExistsAccountNumber(
			InternationalParticipant interPart) throws  ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("Select IntPart From InternationalParticipant IntPart ");
		sbQuery.append(" where IntPart.internationalDepository.idInternationalDepositoryPk = :idInternationalDepositoryPrm ");
		sbQuery.append(" And IntPart.accountNumber = :accountNumberPrm ");
		
		if(Validations.validateIsNotNullAndPositive(interPart.getIdInterParticipantPk())){
			sbQuery.append(" And IntPart.idInterParticipantPk != :idInterParticipantPrm ");
		}
		
		TypedQuery<InternationalParticipant> query = em.createQuery(sbQuery.toString(),InternationalParticipant.class);
		
		query.setParameter("idInternationalDepositoryPrm", interPart.getInternationalDepository().getIdInternationalDepositoryPk());
		query.setParameter("accountNumberPrm", interPart.getAccountNumber());
		
		if(Validations.validateIsNotNullAndPositive(interPart.getIdInterParticipantPk())){
			query.setParameter("idInterParticipantPrm", interPart.getIdInterParticipantPk());
		}
		
		List<InternationalParticipant> lstResult = query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)) {
			return lstResult.get(0);
		} else {
			return null;
		}
	}
	
}
