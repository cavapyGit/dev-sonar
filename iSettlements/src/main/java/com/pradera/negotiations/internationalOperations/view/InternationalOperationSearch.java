/*
 * 
 */
package com.pradera.negotiations.internationalOperations.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.model.negotiation.type.InternationalOperationTransferType;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperationSearch.
 *
 * @author : PraderaTechnologies.
 */

	/** The Constant serialVersionUID. */
public class InternationalOperationSearch implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The idInternationalOperationPk. */
	private Long idInternationalOperationPk;
	/** The operationNumber. */
	private Long operationNumber;
	/** The registerDate. */
	private Date registerDate;
	/** The settlementType. */
	private String settlementType;
	/** The tradeDepository. */
	private String tradeDepository;
	/** The internationalParticipant. */
	private String internationalParticipant;
	
	/** The id transfer type pk. */
	private Long idTransferTypePk;
	
	/** The id local participant pk. */
	private Long idLocalParticipantPk;
	
	/** The localParticipant. */
	private String localParticipant;
	/** The isinCode. */
	private String isinCode;
	/** The settlementAmount. */
	private BigDecimal settlementAmount;
	/** The stockQuantity. */
	private BigDecimal stockQuantity;
	/** The operationState. */
	private Integer operationState;
	/** The operationDate. */
	private String operationDate;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public String getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(String operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the operation state.
	 *
	 * @return the operationState
	 */
	public Integer getOperationState() {
		return operationState;
	}
	
	/**
	 * Gets the operation state description.
	 *
	 * @return the operation state description
	 */
	public String getOperationStateDescription() {
		return InternationalOperationStateType.get(operationState).getDescripcion();
	}
	
	/**
	 * Sets the operation state.
	 *
	 * @param operationState the operationState to set
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}
	
	/**
	 * Gets the id international operation pk.
	 *
	 * @return the idInternationalOperationPk
	 */
	public Long getIdInternationalOperationPk() {
		return idInternationalOperationPk;
	}
	
	/**
	 * Sets the id international operation pk.
	 *
	 * @param idInternationalOperationPk the idInternationalOperationPk to set
	 */
	public void setIdInternationalOperationPk(Long idInternationalOperationPk) {
		this.idInternationalOperationPk = idInternationalOperationPk;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the register date.
	 *
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the settlement type.
	 *
	 * @return the settlementType
	 */
	public String getSettlementType() {
		return settlementType;
	}
	
	/**
	 * Sets the settlement type.
	 *
	 * @param settlementType the settlementType to set
	 */
	public void setSettlementType(String settlementType) {
		this.settlementType = settlementType;
	}
	
	/**
	 * Gets the trade depository.
	 *
	 * @return the tradeDepository
	 */
	public String getTradeDepository() {
		return tradeDepository;
	}
	
	/**
	 * Sets the trade depository.
	 *
	 * @param tradeDepository the tradeDepository to set
	 */
	public void setTradeDepository(String tradeDepository) {
		this.tradeDepository = tradeDepository;
	}
	
	/**
	 * Gets the international participant.
	 *
	 * @return the internationalParticipant
	 */
	public String getInternationalParticipant() {
		return internationalParticipant;
	}
	
	/**
	 * Sets the international participant.
	 *
	 * @param internationalParticipant the internationalParticipant to set
	 */
	public void setInternationalParticipant(String internationalParticipant) {
		this.internationalParticipant = internationalParticipant;
	}
	
	/**
	 * Gets the local participant.
	 *
	 * @return the localParticipant
	 */
	public String getLocalParticipant() {
		return localParticipant;
	}
	
	/**
	 * Sets the local participant.
	 *
	 * @param localParticipant the localParticipant to set
	 */
	public void setLocalParticipant(String localParticipant) {
		this.localParticipant = localParticipant;
	}
	
	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlementAmount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}
	
	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the settlementAmount to set
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	
	/**
	 * Gets the isin code.
	 *
	 * @return the isinCode
	 */
	public String getIsinCode() {
		return isinCode;
	}
	
	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the isinCode to set
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	/**
	 * Instantiates a new international operation search.
	 */
	public InternationalOperationSearch() {
		super();
	}
	/**
	 * Instantiates a new international operation search.
	 *
	 * @param idInternationalOperationPk the id international operation pk
	 */
	public InternationalOperationSearch(Long idInternationalOperationPk) {
		super();
		this.idInternationalOperationPk = idInternationalOperationPk;
	}
	
	/**
	 * Gets the id local participant pk.
	 *
	 * @return the id local participant pk
	 */
	public Long getIdLocalParticipantPk() {
		return idLocalParticipantPk;
	}
	
	/**
	 * Sets the id local participant pk.
	 *
	 * @param idLocalParticipantPk the new id local participant pk
	 */
	public void setIdLocalParticipantPk(Long idLocalParticipantPk) {
		this.idLocalParticipantPk = idLocalParticipantPk;
	}
	
	/**
	 * Gets the id transfer type pk.
	 *
	 * @return the id transfer type pk
	 */
	public Long getIdTransferTypePk() {
		return idTransferTypePk;
	}
	
	/**
	 * Gets the id transfer type description.
	 *
	 * @return the id transfer type description
	 */
	public String getIdTransferTypeDescription(){
		return InternationalOperationTransferType.get(this.idTransferTypePk).getValue();
	}
	
	/**
	 * Sets the id transfer type pk.
	 *
	 * @param idTransferTypePk the new id transfer type pk
	 */
	public void setIdTransferTypePk(Long idTransferTypePk) {
		this.idTransferTypePk = idTransferTypePk;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
}