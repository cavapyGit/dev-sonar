package com.pradera.negotiations.internationalOperations.facade;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.ServletException;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.OperationType;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.DepositaryOperationFile;
import com.pradera.model.negotiation.InternationalMarketFactOperation;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.InternationalParticipant;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.type.InternationalMotiveCancelType;
import com.pradera.model.negotiation.type.InternationalOperationStateType;
import com.pradera.model.negotiation.type.InternationalOperationTransferType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.negotiations.internationalOperations.service.InternationalOperationServiceBean;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationFilter;
import com.pradera.negotiations.internationalOperations.to.InternationalOperationTO;
import com.pradera.negotiations.internationalOperations.view.InternationalOperationSearch;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InternationalOperationServiceFacade.
 *
 * @author PraderaTechnologies
 * The Class InternationalOperationServiceFacade
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class InternationalOperationServiceFacade{

	/** The mcnManualServiceBean. */
	@EJB
	InternationalOperationServiceBean internationalOperationServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The securities component service. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentService;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;  
	
	/** The scheduler service facade. */
	@EJB
	private NegotiationSchedulerServiceFacade schedulerServiceFacade;
	
	/** The idepositary setup. */
	@Inject
	@DepositarySetup
	IdepositarySetup idepositarySetup;
    
    /**
     * Gets the international participant list service facade.
     *
     * @return the international participant list service facade
     * @throws ServiceException the service exception
     */
    public List<InternationalParticipant> getInternationalParticipantListServiceFacade() throws ServiceException{
    	return internationalOperationServiceBean.getInternationalParticipantListServiceBean();
    }
    
    /**
     * This method generates operation number using transferType and idLocalParticipantPk of internationalOperation.
     *
     * @param transferType the transfer type
     * @param idLocalParticipantPK the id local participant pk
     * @return Long
     * @throws ServletException the servlet exception
     */
    public Long getOperationNumber(Long transferType,Long idLocalParticipantPK) throws ServletException{
    	return internationalOperationServiceBean.getOperationNumber(transferType,idLocalParticipantPK);
    }
    
    /**
     * This method gets holder account object for account number and local participant.
     *
     * @param accountNumber the account number
     * @param idLocalParticipantPk the id local participant pk
     * @return HolderAccount
     * @throws ServletException the servlet exception
     */
    public HolderAccount getHolderAccountObject(Integer accountNumber,Long idLocalParticipantPk) throws ServletException{
    	return internationalOperationServiceBean.getHolderAccountObject(accountNumber,idLocalParticipantPk);
    }
    

	/**
	 * Search international operation.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return List<InternationalOperationSearch>
	 * @throws ServiceException the service exception
	 */	
	public List<InternationalOperationSearch> searchInternationalOperation(InternationalOperationFilter internationalOperationFilter) throws ServiceException{
		List<InternationalOperationSearch> lstInterOperationSearch = new ArrayList<InternationalOperationSearch>();
		InternationalOperationSearch internationalOperationSearch =null;
		List<Object[]> objects= internationalOperationServiceBean.searchInternationalOperation(internationalOperationFilter);
		if(objects.size() > 0)
		{
			for (Object[] var: objects) 
			{
				internationalOperationSearch = new InternationalOperationSearch();
				if(var[0]!=null)//idInternationalOperationPk
				{
					internationalOperationSearch.setIdInternationalOperationPk((Long)var[0]);
				}
				if(var[1]!=null)//operationNumber
				{
					internationalOperationSearch.setOperationNumber((Long)var[1]);
				}
				if(var[2]!=null)//registerDate
				{
					internationalOperationSearch.setRegisterDate((Date)var[2]);
				}
				if(var[3]!=null)//settlementType
				{
					internationalOperationSearch.setSettlementType(SettlementType.get(((Integer)var[3]).intValue()).getValue());
				}
				if(var[4]!=null)//tradeDepository.description
				{
					internationalOperationSearch.setTradeDepository(var[4].toString());
				}
				if(var[5]!=null)//internationalParticipant.description
				{
					internationalOperationSearch.setInternationalParticipant(var[5].toString());
				}
				if(var[6]!=null)//localParticipant.mnemonic
				{
					internationalOperationSearch.setLocalParticipant(var[6].toString());
				}
				if(var[7]!=null)//securities.idIsinCodePk
				{
					internationalOperationSearch.setIsinCode(var[7].toString());
				}
				if(var[8]!=null)//settlementAmount
				{
					internationalOperationSearch.setSettlementAmount(((BigDecimal)var[8]).setScale(2, BigDecimal.ROUND_UP));
				}
				if(var[9]!=null)//stockQuantity
				{
					internationalOperationSearch.setStockQuantity(((BigDecimal)var[9]).setScale(2, BigDecimal.ROUND_UP));
				}
				else
				{
					/*calculate stockQuantity using cash amount
					 var[10] --- Cash Amount
					 var[11] --- currentNominalValue of Security */
					if(var[10]!=null && var[11]!=null)
					{
						internationalOperationSearch.setStockQuantity(((BigDecimal)var[10]).divide(((BigDecimal)var[11]),2,BigDecimal.ROUND_UP));
					}
				}
				internationalOperationSearch.setOperationState((Integer)var[12]);
				if(var[13]!=null)
				{
					internationalOperationSearch.setOperationDate(var[13].toString());
				}
				internationalOperationSearch.setIdLocalParticipantPk(Long.parseLong(var[14].toString()));
				internationalOperationSearch.setIdTransferTypePk(Long.parseLong(var[15].toString()));
				internationalOperationSearch.setSettlementDate((Date)var[16]);
				lstInterOperationSearch.add(internationalOperationSearch);
			}
		}		
		 return lstInterOperationSearch;
	}
	
	/**
	 * Gets international operation detail.
	 *
	 * @param internationalOperationPk the international operation pk
	 * @return Map<Integer,Object>
	 * @throws ServiceException the service exception
	 */	
	public Map<Integer,Object> getDetailInterOperation(Long internationalOperationPk)  throws ServiceException{
	    Map<Integer,Object> dtlOperationMap = new HashMap<Integer, Object>();
	    List<Object[]> objects = internationalOperationServiceBean.getDetailInterOperation(internationalOperationPk);
		if(objects.size() > 0)
		{
			for (Object[] var: objects) 
			{
				dtlOperationMap.put(new Integer(0), var[0]);//tradeDepository.idInternationalDepositoryPk
				dtlOperationMap.put(new Integer(1), var[1]);//localParticipant.idParticipantPk
				dtlOperationMap.put(new Integer(2), var[2]);//settlementType
				dtlOperationMap.put(new Integer(3), var[3]);//transferType
				dtlOperationMap.put(new Integer(4), var[4]);//settlementDepository.idInternationalDepositoryPk
				dtlOperationMap.put(new Integer(5), var[5]);//internationalParticipant.idInterParticipantPk
				dtlOperationMap.put(new Integer(6), var[6]);//operationDate
				dtlOperationMap.put(new Integer(7), var[7]);//settlementDate
				dtlOperationMap.put(new Integer(8), var[8]);//holderAccount.accountNumber
				dtlOperationMap.put(new Integer(9), var[9]);//holderAccount.alternateCode
				dtlOperationMap.put(new Integer(10), var[10]);//holderAccount.alternateCode
				dtlOperationMap.put(new Integer(11), var[11]);//securities.idIsinCodePk
				dtlOperationMap.put(new Integer(12), var[12]);//securities.description
				if(var[13]!=null)//securities.securityClass---[SecuritieClassType enum]
				{
					dtlOperationMap.put(new Integer(13), SecurityClassType.get(((Integer)var[13]).intValue()).getValue());
				}
				else
				{
					dtlOperationMap.put(new Integer(13), "");
				}
				if(var[14]!=null)//securities.instrumentType---[InstrumentType enum]
				{
					dtlOperationMap.put(new Integer(14), InstrumentType.get(((Integer)var[14]).intValue()).getValue());
				}
				else
				{
					dtlOperationMap.put(new Integer(14), "");
				}
				dtlOperationMap.put(new Integer(15), var[15]);//securities.issuer.businessName
				if(var[16]!=null)//securities.currentNominalValue
				{
					dtlOperationMap.put(new Integer(16), ((BigDecimal)var[16]).setScale(2, BigDecimal.ROUND_UP));
				}
				else
				{
					dtlOperationMap.put(new Integer(16), new BigDecimal(0).setScale(2, BigDecimal.ROUND_UP));					
				}
				if(var[17]!=null)//Security.currency name----[CurrencyType Enum]
				{
					dtlOperationMap.put(new Integer(17), CurrencyType.get(((Integer)var[17]).intValue()).getValue());
				}
				else
				{
					dtlOperationMap.put(new Integer(17), "");
				}
				if(var[18]!=null)//cashAmount
				{
					dtlOperationMap.put(new Integer(18), ((BigDecimal)var[18]).setScale(2, BigDecimal.ROUND_UP));
					dtlOperationMap.put(new Integer(19), InstrumentType.FIXED_INCOME.getCode());//Id for Cant Instrum Financ
				}
				else
				{
					dtlOperationMap.put(new Integer(18), null);
				}	
				if(var[19]!=null)//stockQuantity
				{
					dtlOperationMap.put(new Integer(20), ((BigDecimal)var[19]).setScale(2, BigDecimal.ROUND_UP));
					dtlOperationMap.put(new Integer(19), InstrumentType.VARIABLE_INCOME.getCode());//Id for Cant Instrum Financ(InstrumentType)
				}
				else
				{
					dtlOperationMap.put(new Integer(20), null);
				}
				if(var[20]!=null)//settlementAmount
				{
					dtlOperationMap.put(new Integer(21), ((BigDecimal)var[20]).setScale(2, BigDecimal.ROUND_UP));
				}
				else
				{
					dtlOperationMap.put(new Integer(21), new BigDecimal(0).setScale(2, BigDecimal.ROUND_UP));
				}
				if(var[21]!=null)
				{
					dtlOperationMap.put(new Integer(22), var[21]);
				}
				else
				{
					dtlOperationMap.put(new Integer(22),null);
				}
				
				
			}
		}	
		return dtlOperationMap;
	}
	
	/**
	 * Confirm international operation.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return boolean
	 * @throws ServiceException the service exception
	 */	
	public void confirmOperation(InternationalOperationFilter internationalOperationFilter) throws ServiceException{
		internationalOperationServiceBean.confirmTradeOperation(internationalOperationFilter);
		internationalOperationServiceBean.confirmInternationalOperation(internationalOperationFilter);
	}
	
	/**
	 * Approve international operation.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return boolean
	 * @throws ServiceException the service exception
	 */	
	public void approveOperation(InternationalOperationFilter internationalOperationFilter) throws ServiceException{
		internationalOperationServiceBean.approveTradeOperation(internationalOperationFilter);
		internationalOperationServiceBean.approveInternationalOperation(internationalOperationFilter);
	}
	
	/**
	 * Reverse international operation.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return boolean
	 * @throws ServiceException the service exception
	 */	
	public void reverseOperation(InternationalOperationFilter internationalOperationFilter) throws ServiceException
	{	
		List<Long> lstIdInternationalOperations= internationalOperationFilter.getInternationalOprPkList();
		for (Long idInternationalOperation: lstIdInternationalOperations)
		{
			internationalOperationServiceBean.reverseTradeOperation(internationalOperationFilter);
			internationalOperationServiceBean.reverseInternationalOperation(internationalOperationFilter);
			InternationalOperation objInternationalOperation= internationalOperationServiceBean.getInternationalOperation(idInternationalOperation);
			//execute the component
			executeComponent(objInternationalOperation, BusinessProcessType.INTERNATIONAL_OPERATION_REVERSE.getCode());
		}
		
	}
	
	/**
	 * Reject International operation.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return boolean
	 * @throws ServiceException the service exception
	 */	
	public void rejectOperation(InternationalOperationFilter internationalOperationFilter) throws ServiceException{
		internationalOperationServiceBean.rejectTradeOperation(internationalOperationFilter);
		internationalOperationServiceBean.rejectInternationalOperation(internationalOperationFilter);
	}
	
	/**
	 * Cancel International operation.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return boolean
	 * @throws ServiceException the service exception
	 */	
	public void cancelOperation(InternationalOperationFilter internationalOperationFilter) throws ServiceException{
		internationalOperationServiceBean.cancelTradeOperation(internationalOperationFilter);
		internationalOperationServiceBean.cancelInternationalOperation(internationalOperationFilter);
	}
	
	/**
	 * Settle International operation.
	 *
	 * @param internationalOperationFilter the new tle operation
	 * @return boolean
	 * @throws ServiceException the service exception
	 */	
	public void settleOperation(InternationalOperationFilter internationalOperationFilter) throws ServiceException{
		internationalOperationServiceBean.settleOperation(internationalOperationFilter);
	}
	
	
	/**
	 * Execute component.
	 *
	 * @param objInternationalOperation the obj international operation
	 * @param idBusinessProcess the id business process
	 * @throws ServiceException the service exception
	 */
	private void executeComponent(InternationalOperation objInternationalOperation, Long idBusinessProcess) throws ServiceException{
		AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
    	objAccountsComponentTO.setIdBusinessProcess(idBusinessProcess);
    	
    	if( BusinessProcessType.INTERNATIONAL_OPERATION_CANCEL.getCode().equals(idBusinessProcess))
    	{
    		if(objInternationalOperation.getTradeOperation().getRejectMotive()!=null &&
    		    !objInternationalOperation.getTradeOperation().getRejectMotive().equals(InternationalMotiveCancelType.FOREIGN_CANCELATION.getCode()))
    		{
    			if(objInternationalOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
            		objAccountsComponentTO.setIdOperationType(ParameterOperationType.UPDATE_SENDING_INTERNATIONAL_SECURITIES.getCode());
            	}else{
            		objAccountsComponentTO.setIdOperationType(ParameterOperationType.UPDATE_RECEPTION_INTERNATIONAL_SECURITIES.getCode());
            	}
    		} else {
    			objAccountsComponentTO.setIdOperationType(objInternationalOperation.getTransferType());
    		}
    	}else{
    		objAccountsComponentTO.setIdOperationType(objInternationalOperation.getTransferType());
    	}
    	 	
    	objAccountsComponentTO.setIdTradeOperation(objInternationalOperation.getIdInternationalOperationPk());
    	
    	List<HolderAccountBalanceTO> lstHolderAccountBalanceTOs= new ArrayList<HolderAccountBalanceTO>();
    	HolderAccountBalanceTO objHolderAccountBalanceTO= new HolderAccountBalanceTO();
    	objHolderAccountBalanceTO.setIdParticipant(objInternationalOperation.getLocalParticipant().getIdParticipantPk());
    	objHolderAccountBalanceTO.setIdHolderAccount(objInternationalOperation.getHolderAccount().getIdHolderAccountPk());
    	objHolderAccountBalanceTO.setIdSecurityCode(objInternationalOperation.getSecurities().getIdSecurityCodePk());
    	objHolderAccountBalanceTO.setStockQuantity(objInternationalOperation.getStockQuantity());
    	
    	List<MarketFactAccountTO> marketFactAccountTOs = new ArrayList<MarketFactAccountTO>();
    	
    	if(objInternationalOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
    		for(InternationalMarketFactOperation marketfact : objInternationalOperation.getInternationalMarketFactOperations()){
        		MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
        		
        		marketFactAccountTO.setMarketDate(marketfact.getMarketDate());
        		marketFactAccountTO.setMarketPrice(marketfact.getMarketPrice());
        		marketFactAccountTO.setMarketRate(marketfact.getMarketRate());
        		marketFactAccountTO.setQuantity(marketfact.getQuantity());
        		
        		marketFactAccountTOs.add(marketFactAccountTO);
        	}
    	}else{
    		MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
    		
    		if(InstrumentType.FIXED_INCOME.getCode().equals(objInternationalOperation.getSecurities().getInstrumentType())){
    			marketFactAccountTO.setMarketRate(objInternationalOperation.getOperationPrice());
    		}else{
    			marketFactAccountTO.setMarketPrice(objInternationalOperation.getOperationPrice());
    		}
    		
    		marketFactAccountTO.setMarketDate(objInternationalOperation.getOperationDate());
    		marketFactAccountTO.setQuantity(objInternationalOperation.getStockQuantity());
    		
    		marketFactAccountTOs.add(marketFactAccountTO);
    	}
    	
    	objHolderAccountBalanceTO.setLstMarketFactAccounts(marketFactAccountTOs);
    	
    	lstHolderAccountBalanceTOs.add(objHolderAccountBalanceTO);
    	objAccountsComponentTO.setLstSourceAccounts(lstHolderAccountBalanceTOs);
    	objAccountsComponentTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
    	accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
	}
	
	/**
	 * International depositories service facade.
	 *
	 * @param internationalOperationTO the international operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InternationalDepository> internationalDepositoriesServiceFacade(InternationalOperationTO internationalOperationTO) throws ServiceException{
		return internationalOperationServiceBean.internationalDepositoriesServiceBean(internationalOperationTO);
	}
	
	/**
	 * Gets the security depositary service facade.
	 *
	 * @param internationalOperationTO the international operation to
	 * @return the security depositary service facade
	 * @throws ServiceException the service exception
	 */
	public Security getSecurityDepositaryServiceFacade(InternationalOperationTO internationalOperationTO) throws ServiceException{
		return internationalOperationServiceBean.getSecurityWithInternationalDepositary(internationalOperationTO);
	}
	
	/**
	 * Gets the balance service facade.
	 *
	 * @param interOperationTO the inter operation to
	 * @return the balance service facade
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance getBalanceServiceFacade(InternationalOperationTO interOperationTO) throws ServiceException{
		return internationalOperationServiceBean.getBalanceServiceBean(interOperationTO);
	}
	
	/**
	 * Gets the international operation service facade.
	 *
	 * @param idInterNationPk the id inter nation pk
	 * @return the international operation service facade
	 * @throws ServiceException the service exception
	 */
	public InternationalOperation getInternationalOperationServiceFacade(Long idInterNationPk) throws ServiceException{
		return internationalOperationServiceBean.getInternationalOperation(idInterNationPk);
	}
	
	/**
	 * Modify international operation.
	 *
	 * @param internationalOperationFilter the international operation filter
	 * @return boolean
	 * @throws ServiceException the service exception
	 */	
	@ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_MODIFICATION)
	public InternationalOperation modifyInternationalOperation(InternationalOperation internationalOperationFilter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
    	
    	if(Validations.validateIsNotNull(internationalOperationFilter.getInternationalMarketFactOperations())){
			for(InternationalMarketFactOperation marketFactOperation : internationalOperationFilter.getInternationalMarketFactOperations()){
				marketFactOperation.setRegistryUser(loggerUser.getUserName());
				marketFactOperation.setRegisterDate(CommonsUtilities.currentDateTime());
				marketFactOperation.setInternationalOperation(internationalOperationFilter);
				Auditable auditMarketFact = marketFactOperation;
				auditMarketFact.setAudit(loggerUser);
			}
		}
		return internationalOperationServiceBean.modifyInternationalOperation(internationalOperationFilter);
	}
    
    /**
     * This method saves international operation.
     *
     * @param internationalOperationParam the international operation param
     * @return boolean
     * @throws ServiceException the service exception
     */
	@ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_REGISTER)
    public InternationalOperation registerInternationalOperation(InternationalOperation internationalOperationParam) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	
		TradeOperation tradeOperation = new TradeOperation();
		tradeOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		tradeOperation.setOperationState(InternationalOperationStateType.REGISTRADA.getCode());
		tradeOperation.setRegisterUser(loggerUser.getUserName());
		
		if(internationalOperationParam.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
			tradeOperation.setOperationType(ParameterOperationType.SENDING_INTERNATIONAL_SECURITIES.getCode());
		}else{
			tradeOperation.setOperationType(ParameterOperationType.RECEPTION_INTERNATIONAL_SECURITIES.getCode());
		}

		InternationalOperation internationalOperation = internationalOperationParam;
		internationalOperation.setTradeOperation(tradeOperation);
		internationalOperation.setOperationState(InternationalOperationStateType.REGISTRADA.getCode());
		internationalOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		internationalOperation.setNominalValue(internationalOperationParam.getSecurities().getCurrentNominalValue());
		internationalOperation.setCurrency(internationalOperationParam.getSecurities().getCurrency());
    	
    	Auditable audit = internationalOperation.getTradeOperation();
		audit.setAudit(loggerUser);
				
		
		if(Validations.validateIsNotNull(internationalOperation.getInternationalMarketFactOperations())){
			for(InternationalMarketFactOperation marketFactOperation : internationalOperation.getInternationalMarketFactOperations()){
				marketFactOperation.setRegistryUser(loggerUser.getUserName());
				marketFactOperation.setRegisterDate(CommonsUtilities.currentDateTime());
				marketFactOperation.setInternationalOperation(internationalOperation);
				Auditable auditMarketFact = marketFactOperation;
				auditMarketFact.setAudit(loggerUser);
			}
		}
		
    	InternationalOperation internaOperation =  internationalOperationServiceBean.registerInternationalOperation(internationalOperation);
    	
    	if(internaOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
    		executeComponent(internaOperation, BusinessProcessType.INTERNATIONAL_OPERATION_REGISTER.getCode());
    	}
    	
    	//add timer for settlement date expiration
    	schedulerServiceFacade.updateTimer(internaOperation);

    	BusinessProcess businessProc = new BusinessProcess();
    	businessProc.setIdBusinessProcessPk(BusinessProcessType.INTERNATIONAL_OPERATION_REGISTER.getCode());
    	Long pcode = internaOperation.getLocalParticipant().getIdParticipantPk();
    	Object[] parameters = new Object[]{internaOperation.getIdInternationalOperationPk()};
    	
    	notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, pcode, parameters);
    	return internaOperation;
    }
    
    /**
     * Register international operation.
     *
     * @param internationalOperation the international operation
     * @return the international operation
     * @throws ServiceException the service exception
     */
	@ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_REJECT)
    public InternationalOperation anulateInternationalOperation(InternationalOperation internationalOperation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
    	Auditable audit = internationalOperation.getTradeOperation();
		audit.setAudit(loggerUser);
		internationalOperation.getTradeOperation().setRejectUser(loggerUser.getUserName());
		internationalOperation.getTradeOperation().setRejectDate(CommonsUtilities.getDefaultDateWithCurrentTime());
		
    	InternationalOperation internaOperation = internationalOperationServiceBean.annulateInternationalOperation(internationalOperation);
    	
    	
    	if(internaOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
    		executeComponent(internaOperation, BusinessProcessType.INTERNATIONAL_OPERATION_REVERSE.getCode());
    	}
    	
    	//delete timer for settlement date expiration
    	schedulerServiceFacade.deleteTimer(internaOperation);
    	
    	BusinessProcess businessProc = new BusinessProcess();
    	businessProc.setIdBusinessProcessPk(BusinessProcessType.INTERNATIONAL_OPERATION_REVERSE.getCode());
    	Long pcode = internaOperation.getLocalParticipant().getIdParticipantPk();
    	Object[] parameters = new Object[]{internaOperation.getIdInternationalOperationPk()};
    	
    	return internaOperation;
    }
    /**
     * Approve international operation.
     *
     * @param internationalOperation the international operation
     * @return the international operation
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_APPROVE)
    public InternationalOperation approveInternationalOperation(InternationalOperation internationalOperation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
    	Auditable audit = internationalOperation.getTradeOperation();
		audit.setAudit(loggerUser);
		internationalOperation.getTradeOperation().setAprovalUser(loggerUser.getUserName());
		internationalOperation.getTradeOperation().setAprovalDate(CommonsUtilities.currentDateTime());
    	InternationalOperation internaOperation =  internationalOperationServiceBean.approberInternationalOperation(internationalOperation);
    	
    	BusinessProcess businessProc = new BusinessProcess();
    	businessProc.setIdBusinessProcessPk(BusinessProcessType.INTERNATIONAL_OPERATION_APPROVE.getCode());
    	Long pcode = internaOperation.getLocalParticipant().getIdParticipantPk();
    	Object[] parameters = new Object[]{internaOperation.getIdInternationalOperationPk()};
    	
    	notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, pcode, parameters);
    	return internaOperation;
    }
    
    /**
     * Confirm international operation.
     *
     * @param internationalOperation the international operation
     * @return the international operation
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_CONFIRM)
    public InternationalOperation confirmInternationalOperation(InternationalOperation internationalOperation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
    	Auditable audit = internationalOperation.getTradeOperation();
		audit.setAudit(loggerUser);
		internationalOperation.getTradeOperation().setConfirmUser(loggerUser.getUserName());
		internationalOperation.getTradeOperation().setConfirmDate(CommonsUtilities.currentDateTime());
    	InternationalOperation internaOperation =  internationalOperationServiceBean.confirmInternationalOperation(internationalOperation);
      	
    	BusinessProcess businessProc = new BusinessProcess();
    	businessProc.setIdBusinessProcessPk(BusinessProcessType.INTERNATIONAL_OPERATION_CONFIRM.getCode());
    	Long pcode = internaOperation.getLocalParticipant().getIdParticipantPk();
    	Object[] parameters = new Object[]{internaOperation.getIdInternationalOperationPk()};
    	
    	notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, pcode, parameters);
    	return internaOperation;
    }
    
    /**
     * Reject international operation.
     *
     * @param internationalOperation the international operation
     * @return the international operation
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_REJECT)
    public InternationalOperation rejectInternationalOperation(InternationalOperation internationalOperation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
    	Auditable audit = internationalOperation.getTradeOperation();
		audit.setAudit(loggerUser);
		internationalOperation.getTradeOperation().setRejectUser(loggerUser.getUserName());
		internationalOperation.getTradeOperation().setRejectDate(CommonsUtilities.getDefaultDateWithCurrentTime());
    	
		InternationalOperation internaOperation = internationalOperationServiceBean.rejectInternationalOperation(internationalOperation);
		
    	if(internaOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
    		executeComponent(internaOperation, BusinessProcessType.INTERNATIONAL_OPERATION_REJECT.getCode());
    	}
    	
    	//delete timer for settlement date expiration
    	schedulerServiceFacade.deleteTimer(internaOperation);
    	
    	BusinessProcess businessProc = new BusinessProcess();
    	businessProc.setIdBusinessProcessPk(BusinessProcessType.INTERNATIONAL_OPERATION_REJECT.getCode());
    	Long pcode = internaOperation.getLocalParticipant().getIdParticipantPk();
    	Object[] parameters = new Object[]{internaOperation.getIdInternationalOperationPk()};
    	
    	notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, pcode, parameters);
    	return internaOperation;
    }
    
    /**
     * Settlement international operation.
     *
     * @param internationalOperation the international operation
     * @return the international operation
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_SETTLEMENT)
    public InternationalOperation settlementInternationalOperation(InternationalOperation internationalOperation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	Auditable audit = internationalOperation.getTradeOperation();
		audit.setAudit(loggerUser);
		internationalOperation.getTradeOperation().setConfirmUser(loggerUser.getUserName());
		internationalOperation.getTradeOperation().setConfirmDate(CommonsUtilities.currentDateTime());
		
    	InternationalOperation internaOperation =  internationalOperationServiceBean.settlementInternationalOperation(internationalOperation);
    	
   		executeComponent(internaOperation, BusinessProcessType.INTERNATIONAL_OPERATION_SETTLEMENT.getCode());
   		executeSecurityComponent(internaOperation);
   		
   		//delete timer for settlement date expiration
    	schedulerServiceFacade.deleteTimer(internaOperation);
   		
   		BusinessProcess businessProc = new BusinessProcess();
    	businessProc.setIdBusinessProcessPk(BusinessProcessType.INTERNATIONAL_OPERATION_SETTLEMENT.getCode());
    	Long pcode = internaOperation.getLocalParticipant().getIdParticipantPk();
    	Object[] parameters = new Object[]{internaOperation.getIdInternationalOperationPk()};
    	
    	notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, pcode, parameters);
    	return internaOperation;
    }
    
    /**
     * Execute security component.
     *v
     * @param objInternationalOperation the obj international operation
     * @throws ServiceException the service exception
     */
    private void executeSecurityComponent(InternationalOperation objInternationalOperation) throws ServiceException {
		// TODO Auto-generated method stub
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		securitiesOperation.setSecurities(objInternationalOperation.getSecurities());
		securitiesOperation.setStockQuantity(objInternationalOperation.getStockQuantity());
		securitiesOperation.setCashAmount(objInternationalOperation.getStockQuantity().multiply(objInternationalOperation.getSecurities().getCurrentNominalValue()));
		securitiesOperation.setTradeOperation(objInternationalOperation.getTradeOperation());
		securitiesOperation.setOperationDate(CommonsUtilities.currentDateTime());
		if(objInternationalOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
			securitiesOperation.setOperationType(ParameterOperationType.SEC_SENDING_INTERNATIONAL_SECURITIES.getCode());
		}else if(objInternationalOperation.getTransferType().equals(InternationalOperationTransferType.RECEPTION_INTERNATIONAL_SECURITIES.getCode())){
			securitiesOperation.setOperationType(ParameterOperationType.SEC_RECEPTION_INTERNATIONAL_SECURITIES.getCode());
		}
		internationalOperationServiceBean.createSecuritiesOperation(securitiesOperation);
		
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
		securitiesComponentTO.setIdBusinessProcess(BusinessProcessType.INTERNATIONAL_OPERATION_SETTLEMENT.getCode());
		securitiesComponentTO.setIdOperationType(securitiesOperation.getOperationType());
		securitiesComponentTO.setIdSecurityCode(securitiesOperation.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setIdSecuritiesOperation(securitiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setDematerializedBalance(objInternationalOperation.getStockQuantity());
		securitiesComponentTO.setPhysicalBalance(objInternationalOperation.getStockQuantity());
		securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);
	}




	/**
     * Cancel international operation.
     *
     * @param internationalOperation the international operation
     * @return the international operation
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.INTERNATIONAL_OPERATION_CANCEL)
    public InternationalOperation cancelInternationalOperation(InternationalOperation internationalOperation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	Auditable audit = internationalOperation.getTradeOperation();
		audit.setAudit(loggerUser);
		internationalOperation.getTradeOperation().setRejectUser(loggerUser.getUserName());
		internationalOperation.getTradeOperation().setRejectDate(CommonsUtilities.getDefaultDateWithCurrentTime());
    	InternationalOperation internaOperation =  internationalOperationServiceBean.cancelInternationalOperation(internationalOperation);
    	
    	if(internaOperation.getTransferType().equals(InternationalOperationTransferType.SENDING_INTERNATIONAL_SECURITIES.getCode())){
    		executeComponent(internaOperation, BusinessProcessType.INTERNATIONAL_OPERATION_CANCEL.getCode());
    	}
    	
    	//delete timer for settlement date expiration
    	schedulerServiceFacade.deleteTimer(internaOperation);
    	
    	BusinessProcess businessProc = new BusinessProcess();
    	businessProc.setIdBusinessProcessPk(BusinessProcessType.INTERNATIONAL_OPERATION_CANCEL.getCode());
    	Long pcode = internaOperation.getLocalParticipant().getIdParticipantPk();
    	Object[] parameters = new Object[]{internaOperation.getIdInternationalOperationPk()};
    	
    	notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProc, pcode, parameters);
    	
    	return internaOperation;
    }
    
    /**
     * Gets the operations type list facade.
     *
     * @return the operations type list facade
     * @throws ServiceException the service exception
     */
    public List<OperationType> getOperationsTypeListFacade() throws ServiceException{
    	return internationalOperationServiceBean.getOperationsTypeList();
    }
    
    /**
     * Generate internat operation file service facade.
     *
     * @param internOperations the intern operations
     * @return the boolean
     * @throws ServiceException the service exception
     */
    public DepositaryOperationFile generateInternatOperationFileServiceFacade(List<InternationalOperation> internOperations) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	try {
			return internationalOperationServiceBean.generateInternatOperationFileServiceBean(internOperations);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
}