package com.pradera.negotiations.internationalOperations.view;

import java.util.ArrayList;
import java.util.List;

import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantIntDepositary;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.negotiation.InternationalOperation;
import com.pradera.model.negotiation.InternationalParticipant;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class OperationsInternatAttributtes.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/08/2013
 */
public class OperationsInternatUtils{
	/**
	 * Gets the participant from depositary.
	 *
	 * @param idintDepositaryPk the idint depositary pk
	 * @param depositaryList the depositary list
	 * @param participantList the participant list
	 * @return the participant from depositary
	 */
	public static List<Participant> getParticipantFromDepositary(Long idintDepositaryPk, List<InternationalDepository> depositaryList, List<Participant> participantList){
		if(depositaryList==null || participantList==null ){
			return null;
		}
		List<Participant> participantReturn = new ArrayList<Participant>();
		for(InternationalDepository internationalDepository: depositaryList){
			if(idintDepositaryPk.equals(internationalDepository.getIdInternationalDepositoryPk())){
				for(ParticipantIntDepositary partic: internationalDepository.getParticipantIntDepositaries()){
					Participant participantSelected = getParticipantFromPk(participantList, partic.getId().getIdParticipantPk());
					if(participantSelected!=null && participantSelected.getState().equals(ParticipantStateType.REGISTERED.getCode())){
						participantReturn.add(participantSelected);
					}
				}
			}
		}
		return participantReturn;
	}
	/**
	 * Gets the participant from pk.
	 *
	 * @param participantList the participant list
	 * @param idParticipantPk the id participant pk
	 * @return the participant from pk
	 */
	public static Participant getParticipantFromPk(List<Participant> participantList, Long idParticipantPk){
		if(participantList==null || idParticipantPk==null){
			return null;
		}
		for(Participant participant: participantList){
			if(participant.getIdParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}
	
	/**
	 * Gets the int participant from pk.
	 *
	 * @param participantList the participant list
	 * @param idParticipantPk the id participant pk
	 * @return the int participant from pk
	 */
	public static InternationalParticipant getIntParticipantFromPk(List<InternationalParticipant> participantList, Long idParticipantPk){
		if(participantList==null || idParticipantPk==null){
			return null;
		}
		for(InternationalParticipant participant: participantList){
			if(participant.getIdInterParticipantPk().equals(idParticipantPk)){
				return participant;
			}
		}
		return null;
	}
	
	/**
	 * Gets the internat depositary by participant.
	 *
	 * @param idPartLong the id part long
	 * @param intDepositories the int depositories
	 * @return the internat depositary by participant
	 */
	public static List<InternationalDepository> getInternatDepositaryByParticipant(Long idPartLong, List<InternationalDepository> intDepositories){
		List<InternationalDepository> intDepositoriesReturn = new ArrayList<InternationalDepository>();
		
		JUMP:
		for(InternationalDepository internationalDepository: intDepositories){
			for(ParticipantIntDepositary participant: internationalDepository.getParticipantIntDepositaries()){
				if(participant.getId().getIdParticipantPk().equals(idPartLong)){
					intDepositoriesReturn.add(internationalDepository);
					continue JUMP;
				}
			}
		}
		return intDepositoriesReturn;
	}
	
	/**
	 * Gets the intern operation from array.
	 *
	 * @param interSearchs the inter searchs
	 * @return the intern operation from array
	 */
	public static List<InternationalOperation> getInternOperationFromArray(InternationalOperationSearch[] interSearchs){
		if(interSearchs==null || interSearchs.length==0){
			return null;
		}
		
		List<InternationalOperation> interOperationsList = new ArrayList<InternationalOperation>();
		for(InternationalOperationSearch intSearch: interSearchs){
			InternationalOperation internationalOperation = new InternationalOperation();
			internationalOperation.setIdInternationalOperationPk(intSearch.getIdInternationalOperationPk());
			interOperationsList.add(internationalOperation);
		}
		return interOperationsList;
	}
}