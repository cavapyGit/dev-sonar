package com.pradera.negotiations.internationalOperations.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

// TODO: Auto-generated Javadoc
/**
 * The Class InternationalOperationDataModel.
 *
 * @author : PraderaTechnologies.
 */

public class InternationalOperationDataModel extends ListDataModel<InternationalOperationSearch> implements SelectableDataModel<InternationalOperationSearch>{
	
	/**  The size. */
	private int size;
	
	/**
	 *  The default constructor.
	 */
	public InternationalOperationDataModel(){
		super();
	}
	
	/**
	 *  
	 *  The Constructor .
	 *
	 * @param data  
	 */
	public InternationalOperationDataModel(List<InternationalOperationSearch> data){
		super(data);
	}
	
	/**
	 *  Gets particular data of selected row.
	 *
	 * @param rowKey the row key
	 * @return the row data
	 */
	
	@Override
	@SuppressWarnings("unchecked")
	public InternationalOperationSearch getRowData(String rowKey) {
		List<InternationalOperationSearch> interOpr=(List<InternationalOperationSearch>)getWrappedData();
        for(InternationalOperationSearch internationalOperationSearch : interOpr) {  
            if(String.valueOf(internationalOperationSearch.getIdInternationalOperationPk()).equals(rowKey))
                return internationalOperationSearch;  
        }  
		return null;
	}

	/**
	 *  Gets particular row key of selected row.
	 *
	 * @param interOpr the inter opr
	 * @return the row key
	 */
	@Override
	public Object getRowKey(InternationalOperationSearch interOpr) {
		return interOpr.getIdInternationalOperationPk();
	}

	/**
	 *  Gets size of data table.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public int getSize() {
		List<InternationalOperationSearch> mechaOpr=(List<InternationalOperationSearch>)getWrappedData();
		if(mechaOpr==null)
			return 0;
		return mechaOpr.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}
}