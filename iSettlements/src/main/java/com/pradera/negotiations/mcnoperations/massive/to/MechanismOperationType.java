package com.pradera.negotiations.mcnoperations.massive.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.SettlementPlaceType;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MechanismOperationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class MechanismOperationType extends ValidationOperationType implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;
	
	/** The reference number. */
	private String referenceNumber; //extended
	
	/** The reference date. */
	private Date referenceDate; //extended
	
	/** The reference date. */
	private Date referenceHour; //extended
	
	/** The operation class. */
	private String operationClass;
	
	/** The ballot number. */
	private Long ballotNumber; // extended
	
	/** The sequential. */
	private Long sequential; // extended
	
	/** The buyer part mnemonic. */
	private String buyerPartMnemonic;
	
	/** The seller part mnemonic. */
	private String sellerPartMnemonic;
	
	/** The security class code. */
	private String securityClassCode; // in
	
	/** The security code. */
	private String securityCode; // cv
	
	/** The term settlement days. */
	private Long termSettlementDays; // extended - cantidad de dias liquidacion
	
	/** The stock quantity. */
	private BigDecimal stockQuantity; //extended
	
	/** The rate. */
	private BigDecimal rate;
	
	/** The cash price. */
	private BigDecimal cashPrice; // extended
	
	/** The cash settlement days. */
	private Long cashSettlementDays; // extended
	
	/** The currency code. */
	private String currencyCode;
	
	/** The settlement place. */
	private String settlementPlace;
	
	// --- not yet used ---
	
	/** The ballot number ori. */
	private Long ballotNumberOri;
	
	/** The sequential ori. */
	private Long sequentialOri;
	
	/** The part mnemonic ori. */
	private String partMnemonicOri;
	
	/** The amount rate ori. */
	private BigDecimal amountRateOri;
	
	/** The term settlement days ori. */
	private Long termSettlementDaysOri;
		
	/** The cash price ori. */
	private BigDecimal cashPriceOri;
	
	/** The operation number ref. */
	private Long operationNumberRef;
	
	/** The security serial. */
	private String securitySerial;
	
	private BigDecimal realCashPrice;
	
	private BigDecimal realCashAmount;
	
	private Date cashSettlementDate;
	
	private Date termSettlementDate;

	private BigDecimal termPrice;
	
	private BigDecimal termAmount;
	
	private String sellFlagStr;
	
	private BigDecimal warrantyCause;
	
	/**
	 * Builds the mechanism operation.
	 */
	public void buildMechanismOperation() {
		
		securitySerial = securityCode;
		//securitySerial = securityCode;
		
		//fecha de operacion
		mechanismOperation = new MechanismOperation();
		mechanismOperation.setReferenceNumber(referenceNumber);
		mechanismOperation.setBallotNumber(Long.valueOf(referenceNumber));
		mechanismOperation.setSequential(Long.valueOf(referenceNumber));
		mechanismOperation.setReferenceDate(CommonsUtilities.setTimeToDate(referenceDate, referenceHour));
		mechanismOperation.setCashSettlementDays(cashSettlementDays);
		mechanismOperation.setTermSettlementDays(termSettlementDays);
		mechanismOperation.setCashPrice(cashPrice);
		mechanismOperation.setStockQuantity(stockQuantity);
		
		//En Cavapy, no existe lugares diferentes de registro, todos son EDV
		settlementPlace = SettlementPlaceType.EDV.getCode();
		
		mechanismOperation.setOperationDate(cashSettlementDate);
		//monto sucio = cantidad * precio sucio 
		mechanismOperation.setCashAmount(NegotiationUtils.getSettlementAmount(mechanismOperation.getCashPrice(), mechanismOperation.getStockQuantity()));
		
		if(Validations.validateIsNotNull(realCashPrice))
			mechanismOperation.setRealCashPrice(realCashPrice);
		
		if(Validations.validateIsNotNull(realCashAmount))
			mechanismOperation.setRealCashAmount(realCashAmount);
		
		if(Validations.validateIsNotNull(cashSettlementDate))
			mechanismOperation.setCashSettlementDate(cashSettlementDate);

		if(Validations.validateIsNotNull(termSettlementDays))
			mechanismOperation.setTermSettlementDays(termSettlementDays);
		
		if(Validations.validateIsNotNull(termSettlementDate))
			mechanismOperation.setTermSettlementDate(termSettlementDate);
	
		if(Validations.validateIsNotNull(termPrice))
			mechanismOperation.setTermPrice(termPrice);
		
		if(Validations.validateIsNotNull(termAmount))
			mechanismOperation.setTermAmount(termAmount);
		
		mechanismOperation.setWarrantyCause(warrantyCause);
		mechanismOperation.setTermInterestRate(rate);
	}
	
	/**
	 * Gets the reference number.
	 *
	 * @return the reference number
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * Sets the reference number.
	 *
	 * @param referenceNumber the new reference number
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	/**
	 * Gets the reference date.
	 *
	 * @return the reference date
	 */
	public Date getReferenceDate() {
		return referenceDate;
	}

	/**
	 * Sets the reference date.
	 *
	 * @param referenceDate the new reference date
	 */
	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}

	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}

	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}

	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}

	/**
	 * Gets the term settlement days.
	 *
	 * @return the term settlement days
	 */
	public Long getTermSettlementDays() {
		return termSettlementDays;
	}

	/**
	 * Sets the term settlement days.
	 *
	 * @param termSettlementDays the new term settlement days
	 */
	public void setTermSettlementDays(Long termSettlementDays) {
		this.termSettlementDays = termSettlementDays;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the cash price.
	 *
	 * @return the cash price
	 */
	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	/**
	 * Sets the cash price.
	 *
	 * @param cashPrice the new cash price
	 */
	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	/**
	 * Gets the cash settlement days.
	 *
	 * @return the cash settlement days
	 */
	public Long getCashSettlementDays() {
		return cashSettlementDays;
	}

	/**
	 * Sets the cash settlement days.
	 *
	 * @param cashSettlementDays the new cash settlement days
	 */
	public void setCashSettlementDays(Long cashSettlementDays) {
		this.cashSettlementDays = cashSettlementDays;
	}

	/**
	 * Gets the operation class.
	 *
	 * @return the operation class
	 */
	public String getOperationClass() {
		return operationClass;
	}

	/**
	 * Sets the operation class.
	 *
	 * @param operationClass the new operation class
	 */
	public void setOperationClass(String operationClass) {
		this.operationClass = operationClass;
	}

	/**
	 * Gets the seller part mnemonic.
	 *
	 * @return the seller part mnemonic
	 */
	public String getSellerPartMnemonic() {
		return sellerPartMnemonic;
	}

	/**
	 * Sets the seller part mnemonic.
	 *
	 * @param sellerPartMnemonic the new seller part mnemonic
	 */
	public void setSellerPartMnemonic(String sellerPartMnemonic) {
		this.sellerPartMnemonic = sellerPartMnemonic;
	}

	/**
	 * Gets the buyer part mnemonic.
	 *
	 * @return the buyer part mnemonic
	 */
	public String getBuyerPartMnemonic() {
		return buyerPartMnemonic;
	}

	/**
	 * Sets the buyer part mnemonic.
	 *
	 * @param buyerPartMnemonic the new buyer part mnemonic
	 */
	public void setBuyerPartMnemonic(String buyerPartMnemonic) {
		this.buyerPartMnemonic = buyerPartMnemonic;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the currency code.
	 *
	 * @return the currency code
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * Sets the currency code.
	 *
	 * @param currencyCode the new currency code
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * Gets the settlement place.
	 *
	 * @return the settlement place
	 */
	public String getSettlementPlace() {
		return settlementPlace;
	}

	/**
	 * Sets the settlement place.
	 *
	 * @param settlementPlace the new settlement place
	 */
	public void setSettlementPlace(String settlementPlace) {
		this.settlementPlace = settlementPlace;
	}

	/**
	 * Gets the ballot number ori.
	 *
	 * @return the ballot number ori
	 */
	public Long getBallotNumberOri() {
		return ballotNumberOri;
	}

	/**
	 * Sets the ballot number ori.
	 *
	 * @param ballotNumberOri the new ballot number ori
	 */
	public void setBallotNumberOri(Long ballotNumberOri) {
		this.ballotNumberOri = ballotNumberOri;
	}

	/**
	 * Gets the sequential ori.
	 *
	 * @return the sequential ori
	 */
	public Long getSequentialOri() {
		return sequentialOri;
	}

	/**
	 * Sets the sequential ori.
	 *
	 * @param sequentialOri the new sequential ori
	 */
	public void setSequentialOri(Long sequentialOri) {
		this.sequentialOri = sequentialOri;
	}

	/**
	 * Gets the part mnemonic ori.
	 *
	 * @return the part mnemonic ori
	 */
	public String getPartMnemonicOri() {
		return partMnemonicOri;
	}

	/**
	 * Sets the part mnemonic ori.
	 *
	 * @param partMnemonicOri the new part mnemonic ori
	 */
	public void setPartMnemonicOri(String partMnemonicOri) {
		this.partMnemonicOri = partMnemonicOri;
	}

	/**
	 * Gets the amount rate ori.
	 *
	 * @return the amount rate ori
	 */
	public BigDecimal getAmountRateOri() {
		return amountRateOri;
	}

	/**
	 * Sets the amount rate ori.
	 *
	 * @param amountRateOri the new amount rate ori
	 */
	public void setAmountRateOri(BigDecimal amountRateOri) {
		this.amountRateOri = amountRateOri;
	}

	/**
	 * Gets the cash price ori.
	 *
	 * @return the cash price ori
	 */
	public BigDecimal getCashPriceOri() {
		return cashPriceOri;
	}

	/**
	 * Sets the cash price ori.
	 *
	 * @param cashPriceOri the new cash price ori
	 */
	public void setCashPriceOri(BigDecimal cashPriceOri) {
		this.cashPriceOri = cashPriceOri;
	}

	/**
	 * Gets the term settlement days ori.
	 *
	 * @return the term settlement days ori
	 */
	public Long getTermSettlementDaysOri() {
		return termSettlementDaysOri;
	}

	/**
	 * Sets the term settlement days ori.
	 *
	 * @param termSettlementDaysOri the new term settlement days ori
	 */
	public void setTermSettlementDaysOri(Long termSettlementDaysOri) {
		this.termSettlementDaysOri = termSettlementDaysOri;
	}

	/**
	 * Gets the security class code.
	 *
	 * @return the security class code
	 */
	public String getSecurityClassCode() {
		return securityClassCode;
	}

	/**
	 * Sets the security class code.
	 *
	 * @param securityClassCode the new security class code
	 */
	public void setSecurityClassCode(String securityClassCode) {
		this.securityClassCode = securityClassCode;
	}

	/**
	 * Gets the rate.
	 *
	 * @return the rate
	 */
	public BigDecimal getRate() {
		return rate;
	}

	/**
	 * Sets the rate.
	 *
	 * @param rate the new rate
	 */
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	/**
	 * Gets the operation number ref.
	 *
	 * @return the operation number ref
	 */
	public Long getOperationNumberRef() {
		return operationNumberRef;
	}

	/**
	 * Sets the operation number ref.
	 *
	 * @param operationNumberRef the new operation number ref
	 */
	public void setOperationNumberRef(Long operationNumberRef) {
		this.operationNumberRef = operationNumberRef;
	}

	/**
	 * Gets the security serial.
	 *
	 * @return the security serial
	 */
	public String getSecuritySerial() {
		return securitySerial;
	}

	/**
	 * Sets the security serial.
	 *
	 * @param securitySerial the new security serial
	 */
	public void setSecuritySerial(String securitySerial) {
		this.securitySerial = securitySerial;
	}

	public BigDecimal getRealCashPrice() {
		return realCashPrice;
	}

	public void setRealCashPrice(BigDecimal realCashPrice) {
		this.realCashPrice = realCashPrice;
	}

	public BigDecimal getRealCashAmount() {
		return realCashAmount;
	}

	public void setRealCashAmount(BigDecimal realCashAmount) {
		this.realCashAmount = realCashAmount;
	}

	public Date getCashSettlementDate() {
		return cashSettlementDate;
	}

	public void setCashSettlementDate(Date cashSettlementDate) {
		this.cashSettlementDate = cashSettlementDate;
	}

	public Date getTermSettlementDate() {
		return termSettlementDate;
	}

	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}

	public BigDecimal getTermPrice() {
		return termPrice;
	}

	public void setTermPrice(BigDecimal termPrice) {
		this.termPrice = termPrice;
	}

	public BigDecimal getTermAmount() {
		return termAmount;
	}

	public void setTermAmount(BigDecimal termAmount) {
		this.termAmount = termAmount;
	}

	public String getSellFlagStr() {
		return sellFlagStr;
	}

	public void setSellFlagStr(String sellFlagStr) {
		this.sellFlagStr = sellFlagStr;
	}

	public Date getReferenceHour() {
		return referenceHour;
	}

	public void setReferenceHour(Date referenceHour) {
		this.referenceHour = referenceHour;
	}

	public BigDecimal getWarrantyCause() {
		return warrantyCause;
	}

	public void setWarrantyCause(BigDecimal warrantyCause) {
		this.warrantyCause = warrantyCause;
	}
	

}
