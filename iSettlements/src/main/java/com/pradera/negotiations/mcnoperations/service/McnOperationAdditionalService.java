package com.pradera.negotiations.mcnoperations.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.massive.type.BbvAssignmentLabel;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.AssignmentRequest;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.McnProcessFile;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationAdj;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.negotiations.accountassignment.service.AccountAssignmentService;
import com.pradera.negotiations.mcnoperations.massive.to.AssignmentOperationType;
import com.pradera.negotiations.mcnoperations.massive.to.MechanismOperationType;
import com.pradera.negotiations.otcoperations.service.OtcOperationServiceBean;
import com.pradera.settlements.chainedoperation.service.ChainedOperationsServiceBean;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class McnOperationAdditionalService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Singleton
public class McnOperationAdditionalService extends CrudDaoServiceBean {
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(McnOperationAdditionalService.class);
	
	/** The mcn operations service bean. */
	@EJB
	McnOperationServiceBean mcnOperationsServiceBean;
	
	/** The otc operation service bean. */
	@EJB
	OtcOperationServiceBean otcOperationServiceBean;
	
	/** The settlements component singleton. */
	@EJB
	SettlementsComponentSingleton settlementsComponentSingleton;

	/** The holder account assignment service. */
	@EJB
	AccountAssignmentService holderAccountAssignmentService;
	
	/** The report generation service. */
	@Inject
	Instance<ReportGenerationService> reportGenerationService;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The chained operations service bean. */
	@EJB
	private ChainedOperationsServiceBean chainedOperationsServiceBean;
	
	/**
	 * Save single mechanism operation.
	 *
	 * @param operation the operation
	 * @param mcnProcessFile the mcn process file
	 * @param indMarketFact the ind market fact
	 * @return the record validation type
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public synchronized RecordValidationType saveSingleMechanismOperation(MechanismOperationType operation, McnProcessFile mcnProcessFile, Integer indMarketFact) {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        MechanismOperation mechanismOperation = operation.getMechanismOperation();
        
		mechanismOperation.setMcnProcessFile(mcnProcessFile);
		mechanismOperation.setOperationState(MechanismOperationStateType.REGISTERED_STATE.getCode());
		mechanismOperation.setRegisterUser(loggerUser.getUserName());
		mechanismOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		mechanismOperation.setSettlementSchema(mechanismOperation.getMechanisnModality().getSettlementSchema());
		mechanismOperation.setSettlementType(mechanismOperation.getMechanisnModality().getSettlementType());
		
		if(indMarketFact.equals(ComponentConstant.ONE)){
			mechanismOperation.setMechanismOperationMarketFacts(new ArrayList<MechanismOperationMarketFact>());
			MechanismOperationMarketFact mechanismOperationMarketFact = new MechanismOperationMarketFact();
			mechanismOperationMarketFact.setMarketDate(mechanismOperation.getOperationDate());
			if(mechanismOperation.getSecurities().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				mechanismOperationMarketFact.setMarketPrice(mechanismOperation.getCashPrice());
			}else{ // mixed or fixed
				mechanismOperationMarketFact.setMarketRate(operation.getRate());
			}
			mechanismOperation.getMechanismOperationMarketFacts().add(mechanismOperationMarketFact);
		}
						
		try {
			
			if(operation.getIdFileMechanismPk().equals(NegotiationMechanismType.OTC.getCode())){
				
				NegotiationModalityType negModalityType = NegotiationModalityType.get(
						mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
				mechanismOperation.setOperationState(OtcOperationStateType.REGISTERED.getCode());
				TradeOperation tradeOperation = new TradeOperation();
				tradeOperation.setRegisterUser(loggerUser.getUserName());
				tradeOperation.setRegisterDate(CommonsUtilities.currentDate());
				tradeOperation.setOperationState(MechanismOperationStateType.REGISTERED_STATE.getCode());
				tradeOperation.setOperationType(negModalityType.getParameterOperationType());
				mechanismOperation.setTradeOperation(tradeOperation);
				
				// set account operations
				//mechanismOperation.setHolderAccountOperations(mechanismOperation.getAccounts());
				MechanismOperationAdj mechanismOperationAdj = new MechanismOperationAdj();//empty obj for no errors
				otcOperationServiceBean.createNewMechanismOperation(mechanismOperation, mechanismOperationAdj);
			}else{
				
//				//todavia no se implementa solapamiento
//				// check if mechanism operations exists, to be replaced
//				MechanismOperation existingOperation = mcnOperationsServiceBean.getMechanismOperation(
//						mechanismOperation.getBallotNumber(), mechanismOperation.getSequential(), mechanismOperation.getOperationDate());
//				if(existingOperation!=null){
//					mcnOperationsServiceBean.cancelMcnOperation(existingOperation, 1, null);
//				}
				
				settlementsComponentSingleton.saveMechanismOperation(mechanismOperation, loggerUser);
			}
		    
		} catch (ServiceException se) {
			RecordValidationType e = new RecordValidationType();
			if(Validations.validateIsNotNullAndNotEmpty(se.getParams()) && se.getParams().length > 0) {
				e.setErrorDescription(PropertiesUtilities.getMessage(
						GeneralConstants.PROPERTY_FILE_EXCEPTIONS,operation.getLocale(), se.getErrorService().getMessage(), se.getParams()));
			} else {
				e.setErrorDescription(PropertiesUtilities.getMessage(
						GeneralConstants.PROPERTY_FILE_EXCEPTIONS,operation.getLocale(), se.getErrorService().getMessage()));
			}	
			e.setOperationRef(operation);
			e.setLineNumber(operation.getLineNumber());
			return e;
		} catch (Exception e) {
			logger.error("Error inesperado al guardar operacion ref Num: "+ mechanismOperation.getOperationNumber());
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Save mcn process file tx.
	 *
	 * @param mcnProcessFileTO the mcn process file to
	 * @param loggerUser the logger user
	 * @return the mcn process file
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public McnProcessFile saveMcnProcessFileTx(ProcessFileTO mcnProcessFileTO, LoggerUser loggerUser) {        
        McnProcessFile mcnProcessFile = new McnProcessFile();
        mcnProcessFile.setRegisterUser(loggerUser.getUserName());
		mcnProcessFile.setTotalOperations(new BigDecimal(mcnProcessFileTO.getTotalOperations()));
		mcnProcessFile.setFileName(mcnProcessFileTO.getFileName());
		mcnProcessFile.setIndAutomacticProcess(mcnProcessFileTO.getIndAutomaticProcess());
		NegotiationMechanism nm = new NegotiationMechanism();
		nm.setIdNegotiationMechanismPk(mcnProcessFileTO.getIdNegotiationMechanismPk());
		mcnProcessFile.setNegotiationMechanism(nm);
		//only for OTC.
		if(mcnProcessFileTO.getIdParticipantPk()!=null){
			mcnProcessFile.setParticipant(new Participant(mcnProcessFileTO.getIdParticipantPk()));
		}
		mcnProcessFile.setProcessDate(CommonsUtilities.currentDateTime());
		mcnProcessFile.setProcessedFile(mcnProcessFileTO.getProcessFile());
		mcnProcessFile.setProcessType(mcnProcessFileTO.getProcessType());
		mcnProcessFile.setProcessState(ExternalInterfaceReceptionStateType.PENDING.getCode());
		Long sequenceNumber = mcnOperationsServiceBean.nextMcnFileSequenceNumber(mcnProcessFile.getProcessDate(),mcnProcessFileTO.getIdParticipantPk(),mcnProcessFile.getProcessType());
		mcnProcessFile.setSequenceNumber(sequenceNumber);
		mcnProcessFile.setAudit(loggerUser);
		mcnOperationsServiceBean.create(mcnProcessFile);
		return mcnProcessFile;
	}
	
	/**
	 * Update mcn process file tx.
	 *
	 * @param mcnProcessFile the mcn process file
	 * @param loggerUser the logger user
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateMcnProcessFileTx(McnProcessFile mcnProcessFile, LoggerUser loggerUser) {
		StringBuilder stringSQL = new StringBuilder();
		stringSQL.append("UPDATE McnProcessFile ");
		stringSQL.append(" SET rejectedFile = :rejectedFileParam ");
		stringSQL.append(" , acceptedFile = :acceptedFileParam ");
		stringSQL.append(" , rejectedOperations = :rejectedOperationsParam ");
		stringSQL.append(" , acceptedOperations = :acceptedOperationsParam ");
		stringSQL.append(" , totalOperations = :totalOperationsParam ");
		stringSQL.append(" , processState = :processStateParam ");
		stringSQL.append(" , lastModifyApp = :lastModifyApp ");
		stringSQL.append(" , lastModifyDate = :lastModifyDate ");
		stringSQL.append(" , lastModifyIp = :lastModifyIp ");
		stringSQL.append(" , lastModifyUser = :lastModifyUser ");
		stringSQL.append(" WHERE idMcnProcessFilePk = :idMcnProcessFilePkParam ");
		Query queryJpql = em.createQuery(stringSQL.toString());
		queryJpql.setParameter("idMcnProcessFilePkParam", mcnProcessFile.getIdMcnProcessFilePk());
		queryJpql.setParameter("rejectedFileParam", mcnProcessFile.getRejectedFile());
		queryJpql.setParameter("acceptedFileParam", mcnProcessFile.getAcceptedFile());
		queryJpql.setParameter("rejectedOperationsParam", mcnProcessFile.getRejectedOperations());
		queryJpql.setParameter("acceptedOperationsParam", mcnProcessFile.getAcceptedOperations());
		queryJpql.setParameter("totalOperationsParam", mcnProcessFile.getTotalOperations());
		queryJpql.setParameter("processStateParam", mcnProcessFile.getProcessState());
		queryJpql.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		queryJpql.setParameter("lastModifyDate", loggerUser.getAuditTime());
		queryJpql.setParameter("lastModifyIp", loggerUser.getIpAddress());
		queryJpql.setParameter("lastModifyUser", loggerUser.getUserName());
		queryJpql.executeUpdate();
	}

	/**
	 * Send report file.
	 *
	 * @param reportId the report id
	 * @param reportUser the report user
	 * @param parameters the parameters
	 * @param parameterDetails the parameter details
	 * @param loggerUser the logger user
	 * @param files the files
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendReportFile(Long reportId, ReportUser reportUser,  Map<String,String> parameters, Map<String,String> parameterDetails,
			LoggerUser loggerUser, List<ReportFile> files) {
		try {
			reportGenerationService.get().saveReportFile(reportId , reportUser ,  parameters, parameterDetails, loggerUser, files );
		} catch (Exception e) {
			logger.error("error al enviar a bandeja de usuario -> "+e.getMessage());
			//e.printStackTrace();
		}
	}

	/**
	 * Save single holder account operation.
	 *
	 * @param assignmenOperation the assignmen operation
	 * @param mcnProcessFile the mcn process file
	 * @param indMarketFact the ind market fact
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType saveSingleHolderAccountOperation(AssignmentOperationType assignmenOperation, McnProcessFile mcnProcessFile,
			Integer indMarketFact) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        try{
        	HolderAccountOperation holderAccountOperation = assignmenOperation.getHolderAccountOperation();
        	MechanismOperation mechanismOperation = holderAccountOperation.getMechanismOperation();
        	AssignmentRequest assignmentRequest = holderAccountOperation.getAssignmentRequest();
        	Integer role = holderAccountOperation.getRole();
        	holderAccountOperation.setMcnProcessFile(mcnProcessFile);
        	
        	// remove previous accounts registered if exists
        	if(assignmentRequest.getRequestState().equals(AssignmentRequestStateType.REGISTERED.getCode())){
        		List<HolderAccountOperation> registeredAccounts = holderAccountAssignmentService.
        				getHolderAccountOperationRegistered(mechanismOperation.getIdMechanismOperationPk(),role,mcnProcessFile.getIdMcnProcessFilePk());
        		if(registeredAccounts.size() > 0){
        			holderAccountAssignmentService.removeHolderAccountOperations(registeredAccounts, mechanismOperation,loggerUser);
        		}
        	}
        	
        	holderAccountAssignmentService.saveHolderAccountAssignment(assignmenOperation.getHolderAccountOperation(), indMarketFact,loggerUser);

        } catch (ServiceException se) {
			RecordValidationType e = new RecordValidationType();
			e.setField(BbvAssignmentLabel.COMITENTE.toString());
			e.setErrorDescription(PropertiesUtilities.getMessage(
					GeneralConstants.PROPERTY_FILE_EXCEPTIONS,assignmenOperation.getLocale(), se.getErrorService().getMessage(),se.getParams()));
			e.setOperationRef(assignmenOperation);
			e.setLineNumber(assignmenOperation.getLineNumber());
			return e;
		} catch (Exception e) {
			logger.error("Error inesperado al guardar asignacion con registro num: "+ assignmenOperation.getReferenceNumber());
			e.printStackTrace();
			return null;
		}
        
		return null;
	}
	
	

}
