package com.pradera.negotiations.mcnoperations.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MechanismOperationSearch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class MechanismOperationSearch implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The idMechanismOperationPk. */
	private Long  idMechanismOperationPk;
	/** The modalityName. */
	private String modalityName;
	/** The operationDate. */
	private Date operationDate;
	/** The operationNumber. */
	private Long operationNumber;
	/** The idIsinCodePk. */
	private String idIsinCodePk;
	/** The stockQuantity. */
	private BigDecimal stockQuantity;
	/** The cashSettlementDate. */
	private Date cashSettlementDate;
	/** The termSettlementDate. */
	private Date termSettlementDate;
	/** The sellerDescription. */
	private String sellerDescription;
	/** The buyerDescription. */
	private String buyerDescription;
	/** The state. */
	private String state;
	
	/**  The instrument. */
	private String instrument;
	
	/**  The issuer. */
	private String issuer;
	
	/**  The currency. */
	private String currency;
	
	/**  The currentNominalValue. */
	private BigDecimal currentNominalValue;
	
	/**  The securityClass. */
	private String securityClass;
	
	/**  The descValor. */
	private String descValor;
	
	/**  The cashSettlementDays. */
	private Long cashSettlementDays;
	
	/**  The stateOperation. */
	private Long stateOperation;
	
	/**  The swapInstrument. */
	private String swapInstrument;
	
	/**  The swapIssuer. */
	private String swapIssuer;
	
	/**  The swapCurrentNominalValue. */
	private BigDecimal swapCurrentNominalValue;
	
	/**  The swapSecurityClass. */
	private String swapSecurityClass;
	
	/**  The swapCurrency. */
	private String swapCurrency;
	
	/**  The swapDescValor. */
	private String swapDescValor;
	
	/** The id mcn process fk. */
	private Long idMcnProcessFk;
	
	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the idMechanismOperationPk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	
	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the idMechanismOperationPk to set
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}
	
	/**
	 * Gets the modality name.
	 *
	 * @return the modalityName
	 */
	public String getModalityName() {
		return modalityName;
	}
	
	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the modalityName to set
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the idIsinCodePk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the idIsinCodePk to set
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the cash settlement date.
	 *
	 * @return the cashSettlementDate
	 */
	public Date getCashSettlementDate() {
		return cashSettlementDate;
	}
	
	/**
	 * Sets the cash settlement date.
	 *
	 * @param cashSettlementDate the cashSettlementDate to set
	 */
	public void setCashSettlementDate(Date cashSettlementDate) {
		this.cashSettlementDate = cashSettlementDate;
	}
	
	/**
	 * Gets the term settlement date.
	 *
	 * @return the termSettlementDate
	 */
	public Date getTermSettlementDate() {
		return termSettlementDate;
	}
	
	/**
	 * Sets the term settlement date.
	 *
	 * @param termSettlementDate the termSettlementDate to set
	 */
	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}
	
	/**
	 * Gets the seller description.
	 *
	 * @return the sellerDescription
	 */
	public String getSellerDescription() {
		return sellerDescription;
	}
	
	/**
	 * Sets the seller description.
	 *
	 * @param sellerDescription the sellerDescription to set
	 */
	public void setSellerDescription(String sellerDescription) {
		this.sellerDescription = sellerDescription;
	}
	
	/**
	 * Gets the buyer description.
	 *
	 * @return the buyerDescription
	 */
	public String getBuyerDescription() {
		return buyerDescription;
	}
	
	/**
	 * Sets the buyer description.
	 *
	 * @param buyerDescription the buyerDescription to set
	 */
	public void setBuyerDescription(String buyerDescription) {
		this.buyerDescription = buyerDescription;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the instrument.
	 *
	 * @return the instrument
	 */
	public String getInstrument() {
		return instrument;
	}
	
	/**
	 * Sets the instrument.
	 *
	 * @param instrument the instrument to set
	 */
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the current nominal value.
	 *
	 * @return the currentNominalValue
	 */
	public BigDecimal getCurrentNominalValue() {
		return currentNominalValue;
	}
	
	/**
	 * Sets the current nominal value.
	 *
	 * @param currentNominalValue the currentNominalValue to set
	 */
	public void setCurrentNominalValue(BigDecimal currentNominalValue) {
		this.currentNominalValue = currentNominalValue;
	}
	
	/**
	 * Gets the security class.
	 *
	 * @return the securityClass
	 */
	public String getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the desc valor.
	 *
	 * @return the descValor
	 */
	public String getDescValor() {
		return descValor;
	}
	
	/**
	 * Sets the desc valor.
	 *
	 * @param descValor the descValor to set
	 */
	public void setDescValor(String descValor) {
		this.descValor = descValor;
	}
	
	/**
	 * Gets the cash settlement days.
	 *
	 * @return the cashSettlementDays
	 */
	public Long getCashSettlementDays() {
		return cashSettlementDays;
	}
	
	/**
	 * Sets the cash settlement days.
	 *
	 * @param cashSettlementDays the cashSettlementDays to set
	 */
	public void setCashSettlementDays(Long cashSettlementDays) {
		this.cashSettlementDays = cashSettlementDays;
	}
	
	/**
	 * Gets the state operation.
	 *
	 * @return the stateOperation
	 */
	public Long getStateOperation() {
		return stateOperation;
	}
	
	/**
	 * Sets the state operation.
	 *
	 * @param stateOperation the stateOperation to set
	 */
	public void setStateOperation(Long stateOperation) {
		this.stateOperation = stateOperation;
	}
	
	/**
	 * Gets the swap instrument.
	 *
	 * @return the swapInstrument
	 */
	public String getSwapInstrument() {
		return swapInstrument;
	}
	
	/**
	 * Sets the swap instrument.
	 *
	 * @param swapInstrument the swapInstrument to set
	 */
	public void setSwapInstrument(String swapInstrument) {
		this.swapInstrument = swapInstrument;
	}
	
	/**
	 * Gets the swap issuer.
	 *
	 * @return the swapIssuer
	 */
	public String getSwapIssuer() {
		return swapIssuer;
	}
	
	/**
	 * Sets the swap issuer.
	 *
	 * @param swapIssuer the swapIssuer to set
	 */
	public void setSwapIssuer(String swapIssuer) {
		this.swapIssuer = swapIssuer;
	}
	
	/**
	 * Gets the swap current nominal value.
	 *
	 * @return the swapCurrentNominalValue
	 */
	public BigDecimal getSwapCurrentNominalValue() {
		return swapCurrentNominalValue;
	}
	
	/**
	 * Sets the swap current nominal value.
	 *
	 * @param swapCurrentNominalValue the swapCurrentNominalValue to set
	 */
	public void setSwapCurrentNominalValue(BigDecimal swapCurrentNominalValue) {
		this.swapCurrentNominalValue = swapCurrentNominalValue;
	}
	
	/**
	 * Gets the swap security class.
	 *
	 * @return the swapSecurityClass
	 */
	public String getSwapSecurityClass() {
		return swapSecurityClass;
	}
	
	/**
	 * Sets the swap security class.
	 *
	 * @param swapSecurityClass the swapSecurityClass to set
	 */
	public void setSwapSecurityClass(String swapSecurityClass) {
		this.swapSecurityClass = swapSecurityClass;
	}
	
	/**
	 * Gets the swap currency.
	 *
	 * @return the swapCurrency
	 */
	public String getSwapCurrency() {
		return swapCurrency;
	}
	
	/**
	 * Sets the swap currency.
	 *
	 * @param swapCurrency the swapCurrency to set
	 */
	public void setSwapCurrency(String swapCurrency) {
		this.swapCurrency = swapCurrency;
	}
	
	/**
	 * Gets the swap desc valor.
	 *
	 * @return the swapDescValor
	 */
	public String getSwapDescValor() {
		return swapDescValor;
	}
	
	/**
	 * Sets the swap desc valor.
	 *
	 * @param swapDescValor the swapDescValor to set
	 */
	public void setSwapDescValor(String swapDescValor) {
		this.swapDescValor = swapDescValor;
	}
	
	/**
	 * Gets the id mcn process fk.
	 *
	 * @return the idMcnProcessFk
	 */
	public Long getIdMcnProcessFk() {
		return idMcnProcessFk;
	}
	
	/**
	 * Sets the id mcn process fk.
	 *
	 * @param idMcnProcessFk the idMcnProcessFk to set
	 */
	public void setIdMcnProcessFk(Long idMcnProcessFk) {
		this.idMcnProcessFk = idMcnProcessFk;
	}
	


}
