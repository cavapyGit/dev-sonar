package com.pradera.negotiations.mcnoperations.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterMCNOperationsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterMCNOperationsTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The bl others. */
	private boolean blCancel, blOthers;
	
	/** The bl term part. */
	private boolean blTermPart;
	
	/** The bl same currency. */
	private boolean blSameCurrency;
	
	/** The bl operation repo. */
	private boolean blOperationRepo;
	
	/** The bl view repo. */
	private boolean blViewRepo;
	
	/** The bl rate. */
	private boolean blRate;
	
	/** The operation repo date. */
	private Date operationRepoDate;
	
	/** The operation repo number. */
	private Long operationRepoNumber;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;
	
	/** The lst negotiation mechanisms. */
	private List<NegotiationMechanism> lstNegotiationMechanisms;
	
	/** The lst negotiation modalities. */
	private List<NegotiationModality> lstNegotiationModalities;
	
	/** The lst purchase participants. */
	private List<Participant> lstPurchaseParticipants;
	
	/** The lst sale participants. */
	private List<Participant> lstSaleParticipants;
	
	/** The lst cancel motives. */
	private List<ParameterTable> lstCurrency, lstSettlementType, lstSettlementSchema, lstStates, lstCancelMotives;
	
	/** The bl fixed income. */
	private boolean blMarketFact, blFixedIncome;
	
	private Boolean useFixedRateCalculation;
	
	private BigDecimal dailyInterest, accruedInterest;
	
	private Integer daysDifference;
	
	private MCNPricesCalculatorTO pricesCalculatorTO;
	
	public RegisterMCNOperationsTO() {
		dailyInterest = BigDecimal.ZERO;
	}
	
	public void setSecurityDataForCalculator(Security sec, ProgramInterestCoupon lastPayedCoupon, ProgramInterestCoupon nextNonPayedCoupon) {
		if(Validations.validateIsNull(pricesCalculatorTO)) {
			pricesCalculatorTO = new MCNPricesCalculatorTO();
		}
		
		pricesCalculatorTO.setIssuerName(sec.getIssuer().getBusinessName());
		pricesCalculatorTO.setIdSecurityCodePk(sec.getIdSecurityCodePk());
		pricesCalculatorTO.setCurrency(sec.getCurrency());
		pricesCalculatorTO.setNominalValue(sec.getCurrentNominalValue());
		pricesCalculatorTO.setNominalRate(sec.getInterestRate());
		pricesCalculatorTO.setPeriodicity(sec.getPeriodicity());
		pricesCalculatorTO.setIssuanceDate(sec.getIssuanceDate());
		pricesCalculatorTO.setExpirationDate(sec.getExpirationDate());
		
		pricesCalculatorTO.setOperationDate(new Date());
		pricesCalculatorTO.setDailyRate(pricesCalculatorTO.getNominalRate()
									.divide(GeneralConstants.THREE_HUNDRED_SIXTY_FIVE, MathContext.DECIMAL32));
		if(Validations.validateIsNotNull(lastPayedCoupon)) {
			pricesCalculatorTO.setLastCoupon(lastPayedCoupon.getExperitationDate());
		} else {
			pricesCalculatorTO.setLastCoupon(pricesCalculatorTO.getIssuanceDate());
		}
		try {
			pricesCalculatorTO.setElapsedDays(CommonsUtilities.diffDatesInDays(pricesCalculatorTO.getLastCoupon(), pricesCalculatorTO.getOperationDate()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(Validations.validateIsNotNull(nextNonPayedCoupon)) {
			try {
				pricesCalculatorTO.setNonPayedCouponDuration(CommonsUtilities.diffDatesInDays(nextNonPayedCoupon.getBeginingDate(), nextNonPayedCoupon.getExperitationDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void fixedRateCalculations() {
		pricesCalculatorTO.setQuantity(mechanismOperation.getStockQuantity());
		pricesCalculatorTO.setPxq(pricesCalculatorTO.getQuantity().multiply(pricesCalculatorTO.getNominalValue()));
		pricesCalculatorTO.setCleanPrice(mechanismOperation.getCashPrice());
		pricesCalculatorTO.setCleanAmount(pricesCalculatorTO.getPxq().multiply(pricesCalculatorTO.getCleanPrice()).divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL, MathContext.DECIMAL32));
		this.mechanismOperation.setCashPrice(pricesCalculatorTO.getCleanAmount());
		this.dailyInterest = pricesCalculatorTO.getDailyRate().setScale(4, BigDecimal.ROUND_HALF_UP);
		this.daysDifference = pricesCalculatorTO.getElapsedDays();
						
		//Interes devengado
		String issuerDocument = mechanismOperation.getSecurities().getIssuer().getDocumentNumber().trim();
		String issuerName = mechanismOperation.getSecurities().getIssuer().getBusinessName().trim();
		
		if(issuerDocument.equals(GeneralConstants.MIN_HACIENDA_DOC) || issuerName.equals(GeneralConstants.MIN_HACIENDA_NAME)) {
			if(Validations.validateIsNotNull(pricesCalculatorTO.getNonPayedCouponDuration())) {
				pricesCalculatorTO.setAccruedInterest(pricesCalculatorTO.getPxq()
									.multiply(pricesCalculatorTO.getNominalRate().divide(new BigDecimal("2"),  MathContext.DECIMAL32))
									.multiply(new BigDecimal(pricesCalculatorTO.getElapsedDays()).divide( new BigDecimal (pricesCalculatorTO.getNonPayedCouponDuration()), MathContext.DECIMAL32)));
			} else {
				pricesCalculatorTO.setAccruedInterest(pricesCalculatorTO.getPxq()
									.multiply(pricesCalculatorTO.getNominalRate().divide(new BigDecimal("2"),  MathContext.DECIMAL32))
									.multiply(new BigDecimal(pricesCalculatorTO.getElapsedDays())));
			}
		} else {
			pricesCalculatorTO.setAccruedInterest(pricesCalculatorTO.getPxq()
									.multiply(pricesCalculatorTO.getDailyRate().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL, MathContext.DECIMAL32))
									.multiply(new BigDecimal(pricesCalculatorTO.getElapsedDays())));
		}
		
		this.accruedInterest = pricesCalculatorTO.getAccruedInterest();
				
		BigDecimal cashPricePlusAccrued = pricesCalculatorTO.getCleanAmount().add(pricesCalculatorTO.getAccruedInterest());
		
		pricesCalculatorTO.setDirtyPrice(cashPricePlusAccrued.divide(pricesCalculatorTO.getCleanAmount(), MathContext.DECIMAL64).multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL).setScale(10, BigDecimal.ROUND_HALF_UP));
		this.mechanismOperation.setRealCashPrice(pricesCalculatorTO.getDirtyPrice());
		pricesCalculatorTO.setDirtyAmount(cashPricePlusAccrued);
		this.mechanismOperation.setRealCashAmount(cashPricePlusAccrued);
	}
	
	/**
	 * Checks if is bl term part.
	 *
	 * @return true, if is bl term part
	 */
	public boolean isBlTermPart() {
		return blTermPart;
	}
	
	/**
	 * Sets the bl term part.
	 *
	 * @param blTermPart the new bl term part
	 */
	public void setBlTermPart(boolean blTermPart) {
		this.blTermPart = blTermPart;
	}
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}
	
	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}
	
	/**
	 * Gets the lst negotiation mechanisms.
	 *
	 * @return the lst negotiation mechanisms
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanisms() {
		return lstNegotiationMechanisms;
	}
	
	/**
	 * Sets the lst negotiation mechanisms.
	 *
	 * @param lstNegotiationMechanisms the new lst negotiation mechanisms
	 */
	public void setLstNegotiationMechanisms(
			List<NegotiationMechanism> lstNegotiationMechanisms) {
		this.lstNegotiationMechanisms = lstNegotiationMechanisms;
	}
	
	/**
	 * Gets the lst negotiation modalities.
	 *
	 * @return the lst negotiation modalities
	 */
	public List<NegotiationModality> getLstNegotiationModalities() {
		return lstNegotiationModalities;
	}
	
	/**
	 * Sets the lst negotiation modalities.
	 *
	 * @param lstNegotiationModalities the new lst negotiation modalities
	 */
	public void setLstNegotiationModalities(
			List<NegotiationModality> lstNegotiationModalities) {
		this.lstNegotiationModalities = lstNegotiationModalities;
	}
	
	/**
	 * Gets the lst purchase participants.
	 *
	 * @return the lst purchase participants
	 */
	public List<Participant> getLstPurchaseParticipants() {
		return lstPurchaseParticipants;
	}
	
	/**
	 * Sets the lst purchase participants.
	 *
	 * @param lstPurchaseParticipants the new lst purchase participants
	 */
	public void setLstPurchaseParticipants(List<Participant> lstPurchaseParticipants) {
		this.lstPurchaseParticipants = lstPurchaseParticipants;
	}
	
	/**
	 * Gets the lst sale participants.
	 *
	 * @return the lst sale participants
	 */
	public List<Participant> getLstSaleParticipants() {
		return lstSaleParticipants;
	}
	
	/**
	 * Sets the lst sale participants.
	 *
	 * @param lstSaleParticipants the new lst sale participants
	 */
	public void setLstSaleParticipants(List<Participant> lstSaleParticipants) {
		this.lstSaleParticipants = lstSaleParticipants;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the lst settlement type.
	 *
	 * @return the lst settlement type
	 */
	public List<ParameterTable> getLstSettlementType() {
		return lstSettlementType;
	}
	
	/**
	 * Sets the lst settlement type.
	 *
	 * @param lstSettlementType the new lst settlement type
	 */
	public void setLstSettlementType(List<ParameterTable> lstSettlementType) {
		this.lstSettlementType = lstSettlementType;
	}
	
	/**
	 * Gets the lst settlement schema.
	 *
	 * @return the lst settlement schema
	 */
	public List<ParameterTable> getLstSettlementSchema() {
		return lstSettlementSchema;
	}
	
	/**
	 * Sets the lst settlement schema.
	 *
	 * @param lstSettlementSchema the new lst settlement schema
	 */
	public void setLstSettlementSchema(List<ParameterTable> lstSettlementSchema) {
		this.lstSettlementSchema = lstSettlementSchema;
	}
	
	/**
	 * Checks if is bl cancel.
	 *
	 * @return true, if is bl cancel
	 */
	public boolean isBlCancel() {
		return blCancel;
	}
	
	/**
	 * Sets the bl cancel.
	 *
	 * @param blCancel the new bl cancel
	 */
	public void setBlCancel(boolean blCancel) {
		this.blCancel = blCancel;
	}
	
	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}
	
	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}
	
	/**
	 * Gets the lst cancel motives.
	 *
	 * @return the lst cancel motives
	 */
	public List<ParameterTable> getLstCancelMotives() {
		return lstCancelMotives;
	}
	
	/**
	 * Sets the lst cancel motives.
	 *
	 * @param lstCancelMotives the new lst cancel motives
	 */
	public void setLstCancelMotives(List<ParameterTable> lstCancelMotives) {
		this.lstCancelMotives = lstCancelMotives;
	}
	
	/**
	 * Checks if is bl others.
	 *
	 * @return true, if is bl others
	 */
	public boolean isBlOthers() {
		return blOthers;
	}
	
	/**
	 * Sets the bl others.
	 *
	 * @param blOthers the new bl others
	 */
	public void setBlOthers(boolean blOthers) {
		this.blOthers = blOthers;
	}
	
	/**
	 * Checks if is bl same currency.
	 *
	 * @return true, if is bl same currency
	 */
	public boolean isBlSameCurrency() {
		return blSameCurrency;
	}
	
	/**
	 * Sets the bl same currency.
	 *
	 * @param blSameCurrency the new bl same currency
	 */
	public void setBlSameCurrency(boolean blSameCurrency) {
		this.blSameCurrency = blSameCurrency;
	}
	
	/**
	 * Checks if is bl operation repo.
	 *
	 * @return true, if is bl operation repo
	 */
	public boolean isBlOperationRepo() {
		return blOperationRepo;
	}
	
	/**
	 * Sets the bl operation repo.
	 *
	 * @param blOperationRepo the new bl operation repo
	 */
	public void setBlOperationRepo(boolean blOperationRepo) {
		this.blOperationRepo = blOperationRepo;
	}
	
	/**
	 * Gets the operation repo date.
	 *
	 * @return the operation repo date
	 */
	public Date getOperationRepoDate() {
		return operationRepoDate;
	}
	
	/**
	 * Sets the operation repo date.
	 *
	 * @param operationRepoDate the new operation repo date
	 */
	public void setOperationRepoDate(Date operationRepoDate) {
		this.operationRepoDate = operationRepoDate;
	}
	
	/**
	 * Gets the operation repo number.
	 *
	 * @return the operation repo number
	 */
	public Long getOperationRepoNumber() {
		return operationRepoNumber;
	}
	
	/**
	 * Sets the operation repo number.
	 *
	 * @param operationRepoNumber the new operation repo number
	 */
	public void setOperationRepoNumber(Long operationRepoNumber) {
		this.operationRepoNumber = operationRepoNumber;
	}
	
	/**
	 * Checks if is bl market fact.
	 *
	 * @return true, if is bl market fact
	 */
	public boolean isBlMarketFact() {
		return blMarketFact;
	}
	
	/**
	 * Sets the bl market fact.
	 *
	 * @param blMarketFact the new bl market fact
	 */
	public void setBlMarketFact(boolean blMarketFact) {
		this.blMarketFact = blMarketFact;
	}
	
	/**
	 * Checks if is bl fixed income.
	 *
	 * @return true, if is bl fixed income
	 */
	public boolean isBlFixedIncome() {
		return blFixedIncome;
	}
	
	/**
	 * Sets the bl fixed income.
	 *
	 * @param blFixedIncome the new bl fixed income
	 */
	public void setBlFixedIncome(boolean blFixedIncome) {
		this.blFixedIncome = blFixedIncome;
	}
	
	/**
	 * Checks if is bl view repo.
	 *
	 * @return true, if is bl view repo
	 */
	public boolean isBlViewRepo() {
		return blViewRepo;
	}
	
	/**
	 * Sets the bl view repo.
	 *
	 * @param blViewRepo the new bl view repo
	 */
	public void setBlViewRepo(boolean blViewRepo) {
		this.blViewRepo = blViewRepo;
	}
	
	/**
	 * Checks if is bl rate.
	 *
	 * @return true, if is bl rate
	 */
	public boolean isBlRate() {
		return blRate;
	}
	
	/**
	 * Sets the bl rate.
	 *
	 * @param blRate the new bl rate
	 */
	public void setBlRate(boolean blRate) {
		this.blRate = blRate;
	}

	public Boolean getUseFixedRateCalculation() {
		return useFixedRateCalculation;
	}

	public void setUseFixedRateCalculation(Boolean useFixedRateCalculation) {
		this.useFixedRateCalculation = useFixedRateCalculation;
	}

	public BigDecimal getDailyInterest() {
		return dailyInterest;
	}

	public void setDailyInterest(BigDecimal dailyInterest) {
		this.dailyInterest = dailyInterest;
	}

	public Integer getDaysDifference() {
		return daysDifference;
	}

	public void setDaysDifference(Integer daysDifference) {
		this.daysDifference = daysDifference;
	}

	public BigDecimal getAccruedInterest() {
		return accruedInterest;
	}

	public void setAccruedInterest(BigDecimal accruedInterest) {
		this.accruedInterest = accruedInterest;
	}

	public MCNPricesCalculatorTO getPricesCalculatorTO() {
		return pricesCalculatorTO;
	}

	public void setPricesCalculatorTO(MCNPricesCalculatorTO pricesCalculatorTO) {
		this.pricesCalculatorTO = pricesCalculatorTO;
	}
	
}
