package com.pradera.negotiations.mcnoperations.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;

	
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchMCNOperationsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchMCNOperationsTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The lst negotiation mechanisms. */
	private List<NegotiationMechanism> lstNegotiationMechanisms;
	
	/** The lst negotiation modalities. */
	private List<NegotiationModality> lstNegotiationModalities;
	
	/** The negotiation modality selected. */
	private Long negotiationMechanismSelected, negotiationModalitySelected;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The lst states. */
	private List<ParameterTable> lstStates = new ArrayList<ParameterTable>();
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency = new ArrayList<ParameterTable>();
	
	/** The lst date type. */
	private List<ParameterTable> lstDateType = new ArrayList<ParameterTable>();
	
	private List<ParameterTable> lstSecurityClass = new ArrayList<ParameterTable>();
	
	/** The lst settlement schema. */
	private List<ParameterTable> lstSettlementType, lstSettlementSchema;
	
	/** The date type selected. */
	private Integer stateSelected, dateTypeSelected, securityClassSelected;
	
	/** The currency selected. */
	private Integer currencySelected;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The lst mechanism operation. */
	private GenericDataModel<MechanismOperationTO> lstMechanismOperation;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The participant selected. */
	private Long participantSelected;
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	private String idSecurityCodePk;
	
	private String idIssuerPk;
	
	private Holder holder = new Holder();

	private HolderAccount holderAccount = new HolderAccount();
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public Integer getSecurityClassSelected() {
		return securityClassSelected;
	}

	public void setSecurityClassSelected(Integer securityClassSelected) {
		this.securityClassSelected = securityClassSelected;
	}

	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
	/**
	 * Gets the lst negotiation mechanisms.
	 *
	 * @return the lst negotiation mechanisms
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanisms() {
		return lstNegotiationMechanisms;
	}
	
	/**
	 * Sets the lst negotiation mechanisms.
	 *
	 * @param lstNegotiationMechanisms the new lst negotiation mechanisms
	 */
	public void setLstNegotiationMechanisms(
			List<NegotiationMechanism> lstNegotiationMechanisms) {
		this.lstNegotiationMechanisms = lstNegotiationMechanisms;
	}
	
	/**
	 * Gets the lst negotiation modalities.
	 *
	 * @return the lst negotiation modalities
	 */
	public List<NegotiationModality> getLstNegotiationModalities() {
		return lstNegotiationModalities;
	}
	
	/**
	 * Sets the lst negotiation modalities.
	 *
	 * @param lstNegotiationModalities the new lst negotiation modalities
	 */
	public void setLstNegotiationModalities(
			List<NegotiationModality> lstNegotiationModalities) {
		this.lstNegotiationModalities = lstNegotiationModalities;
	}
	
	/**
	 * Gets the negotiation mechanism selected.
	 *
	 * @return the negotiation mechanism selected
	 */
	public Long getNegotiationMechanismSelected() {
		return negotiationMechanismSelected;
	}
	
	/**
	 * Sets the negotiation mechanism selected.
	 *
	 * @param negotiationMechanismSelected the new negotiation mechanism selected
	 */
	public void setNegotiationMechanismSelected(Long negotiationMechanismSelected) {
		this.negotiationMechanismSelected = negotiationMechanismSelected;
	}
	
	/**
	 * Gets the negotiation modality selected.
	 *
	 * @return the negotiation modality selected
	 */
	public Long getNegotiationModalitySelected() {
		return negotiationModalitySelected;
	}
	
	/**
	 * Sets the negotiation modality selected.
	 *
	 * @param negotiationModalitySelected the new negotiation modality selected
	 */
	public void setNegotiationModalitySelected(Long negotiationModalitySelected) {
		this.negotiationModalitySelected = negotiationModalitySelected;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}
	
	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}
	
	/**
	 * Gets the lst date type.
	 *
	 * @return the lst date type
	 */
	public List<ParameterTable> getLstDateType() {
		return lstDateType;
	}
	
	/**
	 * Sets the lst date type.
	 *
	 * @param lstDateType the new lst date type
	 */
	public void setLstDateType(List<ParameterTable> lstDateType) {
		this.lstDateType = lstDateType;
	}
	
	/**
	 * Gets the state selected.
	 *
	 * @return the state selected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}
	
	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the new state selected
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}
	
	/**
	 * Gets the date type selected.
	 *
	 * @return the date type selected
	 */
	public Integer getDateTypeSelected() {
		return dateTypeSelected;
	}
	
	/**
	 * Sets the date type selected.
	 *
	 * @param dateTypeSelected the new date type selected
	 */
	public void setDateTypeSelected(Integer dateTypeSelected) {
		this.dateTypeSelected = dateTypeSelected;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the lst mechanism operation.
	 *
	 * @return the lst mechanism operation
	 */
	public GenericDataModel<MechanismOperationTO> getLstMechanismOperation() {
		return lstMechanismOperation;
	}
	
	/**
	 * Sets the lst mechanism operation.
	 *
	 * @param lstMechanismOperation the new lst mechanism operation
	 */
	public void setLstMechanismOperation(
			GenericDataModel<MechanismOperationTO> lstMechanismOperation) {
		this.lstMechanismOperation = lstMechanismOperation;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}
	
	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}
	
	/**
	 * Gets the lst settlement type.
	 *
	 * @return the lst settlement type
	 */
	public List<ParameterTable> getLstSettlementType() {
		return lstSettlementType;
	}
	
	/**
	 * Sets the lst settlement type.
	 *
	 * @param lstSettlementType the new lst settlement type
	 */
	public void setLstSettlementType(List<ParameterTable> lstSettlementType) {
		this.lstSettlementType = lstSettlementType;
	}
	
	/**
	 * Gets the lst settlement schema.
	 *
	 * @return the lst settlement schema
	 */
	public List<ParameterTable> getLstSettlementSchema() {
		return lstSettlementSchema;
	}
	
	/**
	 * Sets the lst settlement schema.
	 *
	 * @param lstSettlementSchema the new lst settlement schema
	 */
	public void setLstSettlementSchema(List<ParameterTable> lstSettlementSchema) {
		this.lstSettlementSchema = lstSettlementSchema;
	}
	
	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}
	
	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	//issue 825
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}	
}
