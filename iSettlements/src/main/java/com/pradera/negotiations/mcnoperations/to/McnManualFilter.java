package com.pradera.negotiations.mcnoperations.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.negotiation.constant.NegotiationConstant;

// TODO: Auto-generated Javadoc
/**
 * The Class McnOperationsFilter.
 *
 * @author : PraderaTechnologies.
 */
public class McnManualFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/**  The mecanism. */
	private Long mecanism;
	
	/**  The modalidad. */
	private Long modality;
	
	/** The reference modality. */
	private Long referenceModality;
	
	/** The reference modality name. */
	private String referenceModalityName;
	
	/**  The typeDate. */
	private Integer typeDate=ComponentConstant.ONE;
	
	/**  The startDate. */
	private Date startDate;
	
	/**  The endDate. */
	private Date endDate;
	
	/**  The operationNumber. */
	private String operationNumber;
	
	/**  The state. */
	private Long state;
	
	/**  The transmitter. */
	private String transmitter;
	
	/**  The instrumentType. */
	private String instrumentType;
	
	/**  The cancelReason. */
	private Long cancelReason;
	
	/**  The cancelOtherReason. */
	private String cancelOtherReason;
	
	/**  The idMechanismOperationPk. */
	private Long idMechanismOperationPk;
	
	/**  The reportOperationDate. */
	private Date reportOperationDate;
	
	/**  The reportOperationNumber. */
	private Long reportOperationNumber;
	
	/** The term settlement date. */
	private Date termSettlementDate;
	
	/**  The idReportDtlPk. */
	private Long idReportDtlPk;
	
	/**  The operationState. */
	private Long operationState;
	
	/**  The reportStockQuantity. */
	private BigDecimal reportStockQuantity; 
	
	/**  The buyerParticipantId. */
	private Long buyerParticipantId;
	
	/**  The booleanReport. */
	private boolean booleanReport;

	/**
	 * Gets the type date.
	 *
	 * @return the typeDate
	 */
	public Integer getTypeDate() {
		return typeDate;
	}
	
	/**
	 * Sets the type date.
	 *
	 * @param typeDate the typeDate to set
	 */
	public void setTypeDate(Integer typeDate) {
		this.typeDate = typeDate;
	}
	
	/**
	 * Checks if is boolean report.
	 *
	 * @return the booleanReport
	 */
	public boolean isBooleanReport() {
		return booleanReport;
	}
	
	/**
	 * Sets the boolean report.
	 *
	 * @param booleanReport the booleanReport to set
	 */
	public void setBooleanReport(boolean booleanReport) {
		this.booleanReport = booleanReport;
	}
	
	/**
	 * Gets the buyer participant id.
	 *
	 * @return the buyerParticipantId
	 */
	public Long getBuyerParticipantId() {
		return buyerParticipantId;
	}
	
	/**
	 * Sets the buyer participant id.
	 *
	 * @param buyerParticipantId the buyerParticipantId to set
	 */
	public void setBuyerParticipantId(Long buyerParticipantId) {
		this.buyerParticipantId = buyerParticipantId;
	}
	
	/**
	 * Gets the report stock quantity.
	 *
	 * @return the reportStockQuantity
	 */
	public BigDecimal getReportStockQuantity() {
		return reportStockQuantity;
	}
	
	/**
	 * Sets the report stock quantity.
	 *
	 * @param reportStockQuantity the reportStockQuantity to set
	 */
	public void setReportStockQuantity(BigDecimal reportStockQuantity) {
		this.reportStockQuantity = reportStockQuantity;
	}
	
	/**
	 * Gets the operation state.
	 *
	 * @return the operationState
	 */
	public Long getOperationState() {
		return operationState;
	}
	
	/**
	 * Sets the operation state.
	 *
	 * @param operationState the operationState to set
	 */
	public void setOperationState(Long operationState) {
		this.operationState = operationState;
	}
	
	/**
	 * Gets the id report dtl pk.
	 *
	 * @return the idReportDtlPk
	 */
	public Long getIdReportDtlPk() {
		return idReportDtlPk;
	}
	
	/**
	 * Sets the id report dtl pk.
	 *
	 * @param idReportDtlPk the idReportDtlPk to set
	 */
	public void setIdReportDtlPk(Long idReportDtlPk) {
		this.idReportDtlPk = idReportDtlPk;
	}
	
	/**
	 * Gets the report operation number.
	 *
	 * @return the reportOperationNumber
	 */
	public Long getReportOperationNumber() {
		return reportOperationNumber;
	}
	
	/**
	 * Sets the report operation number.
	 *
	 * @param reportOperationNumber the reportOperationNumber to set
	 */
	public void setReportOperationNumber(Long reportOperationNumber) {
		this.reportOperationNumber = reportOperationNumber;
	}
	
	/**
	 * Gets the report operation date.
	 *
	 * @return the reportOperationDate
	 */
	public Date getReportOperationDate() {
		return reportOperationDate;
	}
	
	/**
	 * Sets the report operation date.
	 *
	 * @param reportOperationDate the reportOperationDate to set
	 */
	public void setReportOperationDate(Date reportOperationDate) {
		this.reportOperationDate = reportOperationDate;
	}
	
	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the idMechanismOperationPk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	
	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the idMechanismOperationPk to set
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}
	
	/**
	 * Gets the cancel reason.
	 *
	 * @return the cancelReason
	 */
	public Long getCancelReason() {
		return cancelReason;
	}
	
	/**
	 * Sets the cancel reason.
	 *
	 * @param cancelReason the cancelReason to set
	 */
	public void setCancelReason(Long cancelReason) {
		this.cancelReason = cancelReason;
	}
	
	/**
	 * Gets the cancel other reason.
	 *
	 * @return the cancelOtherReason
	 */
	public String getCancelOtherReason() {
		return cancelOtherReason;
	}
	
	/**
	 * Sets the cancel other reason.
	 *
	 * @param cancelOtherReason the cancelOtherReason to set
	 */
	public void setCancelOtherReason(String cancelOtherReason) {
		this.cancelOtherReason = cancelOtherReason;
	}
	
	/**
	 * Gets the transmitter.
	 *
	 * @return the transmitter
	 */
	public String getTransmitter() {
		return transmitter;
	}
	
	/**
	 * Sets the transmitter.
	 *
	 * @param transmitter the transmitter to set
	 */
	public void setTransmitter(String transmitter) {
		this.transmitter = transmitter;
	}
	
	/**
	 * Gets the instrument type.
	 *
	 * @return the instrumentType
	 */
	public String getInstrumentType() {
		return instrumentType;
	}
	
	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}
	
	/**
	 * Gets the mecanism.
	 *
	 * @return the mecanism
	 */
	public Long getMecanism() {
		return mecanism;
	}
	
	/**
	 * Sets the mecanism.
	 *
	 * @param mecanism the mecanism to set
	 */
	public void setMecanism(Long mecanism) {
		this.mecanism = mecanism;
	}
	
	/**
	 * Gets the modality.
	 *
	 * @return the modality
	 */
	public Long getModality() {
		return modality;
	}
	
	/**
	 * Sets the modality.
	 *
	 * @param modality the modality to set
	 */
	public void setModality(Long modality) {
		this.modality = modality;
	}
	
	/**
	 * Gets the start date.
	 *
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	
	/**
	 * Sets the start date.
	 *
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public String getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Long getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Long state) {
		this.state = state;
	}
	
	/**
	 * Gets the term settlement date.
	 *
	 * @return the termSettlementDate
	 */
	public Date getTermSettlementDate() {
		return termSettlementDate;
	}
	
	/**
	 * Sets the term settlement date.
	 *
	 * @param termSettlementDate the termSettlementDate to set
	 */
	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}
	
	/**
	 * Gets the reference modality.
	 *
	 * @return the referenceModality
	 */
	public Long getReferenceModality() {
		return referenceModality;
	}
	
	/**
	 * Sets the reference modality.
	 *
	 * @param referenceModality the referenceModality to set
	 */
	public void setReferenceModality(Long referenceModality) {
		this.referenceModality = referenceModality;
	}
	
	/**
	 * Gets the reference modality name.
	 *
	 * @return the referenceModalityName
	 */
	public String getReferenceModalityName() {
		return referenceModalityName;
	}
	
	/**
	 * Sets the reference modality name.
	 *
	 * @param referenceModalityName the referenceModalityName to set
	 */
	public void setReferenceModalityName(String referenceModalityName) {
		this.referenceModalityName = referenceModalityName;
	}
	

}
