package com.pradera.negotiations.mcnoperations.view;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationGroup;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryGroupConversation;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationMarketFact;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.McnCancelReasonType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.negotiations.mcnoperations.to.RegisterMCNOperationsTO;
import com.pradera.negotiations.mcnoperations.to.SearchMCNOperationsTO;
import com.pradera.negotiations.operations.calculator.OperationPriceCalculatorServiceFacade;
import com.pradera.negotiations.operations.view.OperationBusinessBean;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;


// TODO: Auto-generated Javadoc
/**
 * The Class McnManualMagementBean.
 *
 * @author : PraderaTechnologies.
 */
@DepositaryWebBean
@LoggerCreateBean
public class McnManualMgmtBean extends GenericBaseBean{
	
	/** The mcn operation service facade. */
	@EJB
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;	
	
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The mcn massive mgmt bean. */
	@Inject
	@ConversationGroup(DepositaryGroupConversation.class)
	McnMassiveMgmtBean mcnMassiveMgmtBean;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The user info. */
	@Inject
	UserInfo userInfo; 
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege; 
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	@EJB
	OperationPriceCalculatorServiceFacade operationPriceCalculatorServiceFacade;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The bl depositary. */
	private boolean blDepositary;
	
	/** The bl cancel. */
	private boolean blCancel;
	
	/** The search mcn operations to. */
	private SearchMCNOperationsTO searchMCNOperationsTO;
	
	/** The register mcn operations to. */
	private RegisterMCNOperationsTO registerMCNOperationsTO;
	
	/** The mechanism operation. */
	private MechanismOperationTO mechanismOperation;
	
	/** The Constant VIEW_MAPPING. */
	private final static String VIEW_MAPPING = "viewMCNOperations";
	
	/** The Constant REGISTER_MANUAL_MAPPING. */
	private final static String REGISTER_MANUAL_MAPPING = "registerMCNManual";
	
	/** The Constant REGISTER_MASSIVE_MAPPING. */
	private final static String REGISTER_MASSIVE_MAPPING = "registerMCNMassive";
	
	/** The operation business comp. */
	@Inject
	private OperationBusinessBean operationBusinessComp;
	
	/** The mp mechanism modality. */
	private Map<String, String> mpMechanismModality;
	
	/** The map parameters. */
	private Map<Integer, ParameterTable> mapParameters = new HashMap<Integer, ParameterTable>();
	
	/**
	 * init Method.
	 */
	@PostConstruct
	public void init() {		
		searchMCNOperationsTO = new SearchMCNOperationsTO();
		searchMCNOperationsTO.setInitialDate(getCurrentSystemDate());
		searchMCNOperationsTO.setEndDate(getCurrentSystemDate());
		searchMCNOperationsTO.setDateTypeSelected(GeneralConstants.ZERO_VALUE_INTEGER);
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		try{
			listHolidays();
			fillCombos();
			fillMaps();
			searchMCNOperationsTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(ComponentConstant.ONE));
			Long idDefaultMechanism = null; 
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				blDepositary = true;
				idDefaultMechanism = NegotiationMechanismType.BOLSA.getCode();
			}else{
				if(InstitutionType.BOLSA.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					idDefaultMechanism = NegotiationMechanismType.BOLSA.getCode();
				}else if(InstitutionType.BCR.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					idDefaultMechanism = NegotiationMechanismType.BC.getCode();
				}
			}
			searchMCNOperationsTO.setNegotiationMechanismSelected(idDefaultMechanism);
			searchMCNOperationsTO.setLstNegotiationModalities(mcnOperationServiceFacade.getListNegotiationModality(idDefaultMechanism));
			showPrivilegeButtons();
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Change negotiation mechanism search.
	 */
	public void changeNegotiationMechanismSearch(){
		try {
			cleanSearchResult();
			searchMCNOperationsTO.setNegotiationModalitySelected(null);
			searchMCNOperationsTO.setLstNegotiationModalities(null);
			if(Validations.validateIsNotNullAndNotEmpty(searchMCNOperationsTO.getNegotiationMechanismSelected())){
				searchMCNOperationsTO.setLstNegotiationModalities(mcnOperationServiceFacade.getListNegotiationModality(searchMCNOperationsTO.getNegotiationMechanismSelected()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
		
	/**
	 * Search mcn operations.
	 */
	@LoggerAuditWeb
	public void searchMCNOperations(){
		try {
			cleanSearchResult();
			List<MechanismOperationTO> lstResult = mcnOperationServiceFacade.searchMechanismOperations(searchMCNOperationsTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				searchMCNOperationsTO.setBlNoResult(false);
				searchMCNOperationsTO.setLstMechanismOperation(new GenericDataModel<MechanismOperationTO>(lstResult));
			}else{
				searchMCNOperationsTO.setBlNoResult(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean search result.
	 */
	public void cleanSearchResult(){
		mechanismOperation = null;
		searchMCNOperationsTO.setLstMechanismOperation(null);
		searchMCNOperationsTO.setBlNoResult(false);
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		if(blDepositary){
			searchMCNOperationsTO.setLstNegotiationModalities(null);
			searchMCNOperationsTO.setNegotiationMechanismSelected(null);
		}
		searchMCNOperationsTO.setNegotiationModalitySelected(null);
		searchMCNOperationsTO.setInitialDate(getCurrentSystemDate());
		searchMCNOperationsTO.setEndDate(getCurrentSystemDate());
		searchMCNOperationsTO.setBallotNumber(null);
		searchMCNOperationsTO.setSequential(null);
		searchMCNOperationsTO.setDateTypeSelected(GeneralConstants.ZERO_VALUE_INTEGER);
		searchMCNOperationsTO.setOperationNumber(null);
		searchMCNOperationsTO.setStateSelected(null);
		cleanSearchResult();
	}
	
	/**
	 * View mechanism operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the string
	 */
	public String viewMechanismOperation(Long idMechanismOperation){
		try {
			blCancel = false;
			fillView(idMechanismOperation);
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate cancel.
	 *
	 * @return the string
	 */
	public String validateCancel(){
		try {
			if(Validations.validateIsNullOrEmpty(mechanismOperation)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				if(MechanismOperationStateType.REGISTERED_STATE.getCode().equals(mechanismOperation.getOperationState())
						|| MechanismOperationStateType.PENDING_CANCEL_STATE.getCode().equals(mechanismOperation.getOperationState())){
					blCancel = true;
					fillView(mechanismOperation.getIdMechanismOperationPk());
					return VIEW_MAPPING;	
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_CANCEL_NOT_REGISTERED));
					JSFUtilities.showSimpleValidationDialog();
				}				
			}			
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate cancel.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		try {
			if(Validations.validateIsNullOrEmpty(mechanismOperation)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				if(MechanismOperationStateType.PENDING_CANCEL_STATE.getCode().equals(mechanismOperation.getOperationState())){
					blCancel = true;
					fillView(mechanismOperation.getIdMechanismOperationPk());
					return VIEW_MAPPING;	
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_CANCEL_NOT_REGISTERED));
					JSFUtilities.showSimpleValidationDialog();
				}				
			}			
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Change cancel motive.
	 */
	public void changeCancelMotive(){
		registerMCNOperationsTO.setBlOthers(false);
		registerMCNOperationsTO.getMechanismOperation().setCancelMotiveOther(null);
		if(McnCancelReasonType.OTHERS.getCode().equals(registerMCNOperationsTO.getMechanismOperation().getCancelMotive()))
			registerMCNOperationsTO.setBlOthers(true);
	}
	
	/**
	 * Before cancel.
	 */
	public void beforeCancel(){
		String mechanismName = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationMechanism().getMechanismName();
		String modalityName = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality().getModalityName();
		Date operationDate = registerMCNOperationsTO.getMechanismOperation().getOperationDate();
		Long operationNumber = registerMCNOperationsTO.getMechanismOperation().getOperationNumber();
		String desc = NegotiationUtils.getOperationDescription(mechanismName, modalityName, operationDate, operationNumber);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CANCEL), 
				PropertiesUtilities.getMessage(PropertiesConstants.MCN_CANCEL_CONFIRMER, new Object[]{desc}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Cancel operation.
	 */
	@LoggerAuditWeb
	public void cancelOperation(){
		try {
			
			Integer indPrimaryPlacement = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality().getIndPrimaryPlacement();
			
			mcnOperationServiceFacade.cancelMcnOperations(registerMCNOperationsTO.getMechanismOperation().getIdMechanismOperationPk(),
					registerMCNOperationsTO.getMechanismOperation().getCancelMotive(),
					registerMCNOperationsTO.getMechanismOperation().getCancelMotiveOther(),
					indPrimaryPlacement);
			
			String mechanismName = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationMechanism().getMechanismName();
			String modalityName = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality().getModalityName();
			Date operationDate = registerMCNOperationsTO.getMechanismOperation().getOperationDate();
			Long operationNumber = registerMCNOperationsTO.getMechanismOperation().getOperationNumber();
			String desc = NegotiationUtils.getOperationDescription(mechanismName, modalityName, operationDate, operationNumber);
			
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					,PropertiesUtilities.getMessage(PropertiesConstants.MCN_CANCEL_SUCCESS, new Object[]{desc}));
			searchMCNOperations();
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				return;
			}
			excepcion.fire(new ExceptionToCatchEvent(e));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
						,PropertiesConstants.MCN_CANCEL_NOT_REGISTERED);
		}
	}
	
	/**
	 * Validate security.
	 */
	public void validateSecurity(){
		try {
			registerMCNOperationsTO.setBlFixedIncome(false);
			registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(null);
			registerMCNOperationsTO.getMechanismOperation().setCashSettlementDate(null);
			registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
			registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
			registerMCNOperationsTO.getMechanismOperation().setExchangeRate(null);
			registerMCNOperationsTO.getMechanismOperation().setStockQuantity(null);
			
			Long modalityId = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk();
			Security security = registerMCNOperationsTO.getMechanismOperation().getSecurities(); 
			
			if(Validations.validateIsNullOrEmpty(modalityId)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("alt.mcn.search.modality.required"));
				JSFUtilities.showSimpleValidationDialog();
				registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
				return;
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				if(!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())){
					ParameterTable state = mapParameters.get(security.getStateSecurity());
					String des = null;
					if(state!=null){
						des = state.getParameterName();
					}
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_CORDIGOVALOR_NOTREGISTERED,des));
					JSFUtilities.showSimpleValidationDialog();
					registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
					return;
				}
				if(!mcnOperationServiceFacade.validateSecurityNegoMechanism(security.getIdSecurityCodePk(), registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_SECURITY_NOTALLOWED_MODALITY));
					JSFUtilities.showSimpleValidationDialog();
					registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
					return;
				}
				Issuance issuance = mcnOperationServiceFacade.findIssuanceAndIssuer(security.getIdSecurityCodePk());
				security.setIssuance(issuance);
				security.setIssuer(issuance.getIssuer());
				if(!IssuerStateType.REGISTERED.getCode().equals(issuance.getIssuer().getStateIssuer())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_SECURITY_ISSUER_NOT_REGISTERED));
					JSFUtilities.showSimpleValidationDialog();
					registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
					return;
				}
				if(!IssuanceStateType.AUTHORIZED.getCode().equals(issuance.getStateIssuance())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_SECURITY_ISSUER_NOT_REGISTERED));
					JSFUtilities.showSimpleValidationDialog();
					registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
					return;
				}
				//VALIDATE IF THE ISSUANCE DATE IS GREATER OR EQUAL THAN CURRENT DATE
				if(issuance.getIssuanceDate().after(registerMCNOperationsTO.getMechanismOperation().getOperationDate())){			
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_ISSUANCE_DATE_GREATHER_CURRENT));
					JSFUtilities.showSimpleValidationDialog();
					registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
					return;
				}
				//Validamos si No es colocacion primaria Y el valor No es negociable en la segunda modalidad
				Integer indPrimaryPlacement = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality().getIndPrimaryPlacement();
				if(!ComponentConstant.ONE.equals(indPrimaryPlacement)){
					if(mcnOperationServiceFacade.issuanceIsPlacementSegment(security)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_PRIMARYPLACEMENT_FINISHED));
						JSFUtilities.showSimpleValidationDialog();
						registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
						return;	
					}
				}
				
				ParameterTable objCurrency = mapParameters.get(security.getCurrency());
				if(objCurrency!=null){
					security.setCurrencyName(objCurrency.getParameterName());
					mcnOperationServiceFacade.setCurrency(objCurrency,registerMCNOperationsTO.getMechanismOperation(),
							registerMCNOperationsTO.isBlTermPart()?ComponentConstant.ONE:ComponentConstant.ZERO);
				}
				
				registerMCNOperationsTO.getMechanismOperation().setCurrency(security.getCurrency());
				if(InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType())){
					registerMCNOperationsTO.setBlFixedIncome(true);
				}
				
				registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(new Long(0));
				if(!getCashSettlementDate()){
					registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
				}
				
				if(registerMCNOperationsTO.getUseFixedRateCalculation()) {
					ProgramInterestCoupon lastCoupon = this.mcnOperationServiceFacade.getLastCouponPayed(security.getIdSecurityCodePk(), new Date());
					ProgramInterestCoupon nonPayedCoupon = this.mcnOperationServiceFacade.getNextNonPayedCoupon(security.getIdSecurityCodePk(), new Date());
					registerMCNOperationsTO.setSecurityDataForCalculator(security, lastCoupon, nonPayedCoupon);
				}
				
			}
		} catch (ServiceException e) {
			registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
			JSFUtilities.showSimpleValidationDialog();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			return;
		}
	}
	
	/**
	 * This method is invoked when user click on manual registration.
	 * 
	 * @return String
	 */
	public String manualRegister() {
		try {
			registerMCNOperationsTO = new RegisterMCNOperationsTO();
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
				registerMCNOperationsTO.setBlMarketFact(true);
			}
			registerMCNOperationsTO.setMechanismOperation(new MechanismOperation());
			registerMCNOperationsTO.getMechanismOperation().setBuyerParticipant(new Participant());
			registerMCNOperationsTO.getMechanismOperation().setSellerParticipant(new Participant());
			registerMCNOperationsTO.getMechanismOperation().setMechanisnModality(new MechanismModality());
			registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().setId(new MechanismModalityPK());
			registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
			registerMCNOperationsTO.getMechanismOperation().setOperationDate(getCurrentSystemDate());
			registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
			registerMCNOperationsTO.setLstNegotiationMechanisms(searchMCNOperationsTO.getLstNegotiationMechanisms());
			registerMCNOperationsTO.setLstCurrency(searchMCNOperationsTO.getLstCurrency());
			
			
			fillCombosForMCNOperations();
			
			if(!blDepositary){			
				if(InstitutionType.BOLSA.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
					registerMCNOperationsTO.setLstNegotiationModalities(mcnOperationServiceFacade.getListNegotiationModality(NegotiationMechanismType.BOLSA.getCode()));
				}else if(InstitutionType.BCR.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().setIdNegotiationMechanismPk(NegotiationMechanismType.BC.getCode());
					registerMCNOperationsTO.setLstNegotiationModalities(mcnOperationServiceFacade.getListNegotiationModality(NegotiationMechanismType.BC.getCode()));
				}
			}
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			return REGISTER_MANUAL_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		}
		return null;
	}
	
	/**
	 * Clean manual register.
	 */
	public void cleanManualRegister(){
		JSFUtilities.resetViewRoot();
		registerMCNOperationsTO.setMechanismOperation(new MechanismOperation());
		registerMCNOperationsTO.getMechanismOperation().setBuyerParticipant(new Participant());
		registerMCNOperationsTO.getMechanismOperation().setSellerParticipant(new Participant());
		registerMCNOperationsTO.getMechanismOperation().setMechanisnModality(new MechanismModality());
		registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().setId(new MechanismModalityPK());
		registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
		registerMCNOperationsTO.getMechanismOperation().setOperationDate(getCurrentSystemDate());
		registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
		registerMCNOperationsTO.setLstPurchaseParticipants(null);
		registerMCNOperationsTO.setLstSaleParticipants(null);
		if(!blDepositary){
			if(InstitutionType.BOLSA.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
				registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
			}else if(InstitutionType.BCR.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
				registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().setIdNegotiationMechanismPk(NegotiationMechanismType.BC.getCode());				
			}
		}else{
			registerMCNOperationsTO.setLstNegotiationModalities(null);
		}
		registerMCNOperationsTO.setBlCancel(false);
		registerMCNOperationsTO.setBlOthers(false);
		registerMCNOperationsTO.setBlTermPart(false);
		registerMCNOperationsTO.setBlSameCurrency(false);
		registerMCNOperationsTO.setBlOperationRepo(false);
		registerMCNOperationsTO.setOperationRepoDate(null);
		registerMCNOperationsTO.setOperationRepoNumber(null);		
		registerMCNOperationsTO.setBlRate(false);
	}
	
	/**
	 * Change negotiation mechanism register.
	 */
	public void changeNegotiationMechanismRegister(){
		try {
			Long negoMechanism = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk();
			JSFUtilities.resetViewRoot();
			registerMCNOperationsTO.setBlOperationRepo(false);
			registerMCNOperationsTO.setBlViewRepo(false);
			registerMCNOperationsTO.setOperationRepoDate(null);
			registerMCNOperationsTO.setOperationRepoNumber(null);
			registerMCNOperationsTO.setBlTermPart(false);
			registerMCNOperationsTO.setBlSameCurrency(false);
			registerMCNOperationsTO.setLstNegotiationModalities(null);
			registerMCNOperationsTO.setLstPurchaseParticipants(null);
			registerMCNOperationsTO.setLstSaleParticipants(null);
			registerMCNOperationsTO.setMechanismOperation(new MechanismOperation());
			registerMCNOperationsTO.getMechanismOperation().setOperationDate(getCurrentSystemDate());
			registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
			registerMCNOperationsTO.getMechanismOperation().setBuyerParticipant(new Participant());
			registerMCNOperationsTO.getMechanismOperation().setSellerParticipant(new Participant());
			registerMCNOperationsTO.getMechanismOperation().setMechanisnModality(new MechanismModality());
			registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().setId(new MechanismModalityPK());
			registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().setIdNegotiationMechanismPk(negoMechanism);
			registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
			if(Validations.validateIsNotNullAndNotEmpty(registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk())){
				registerMCNOperationsTO.setLstNegotiationModalities(mcnOperationServiceFacade.getListNegotiationModality(registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change negotiation modality.
	 */
	public void changeNegotiationModality(){
		try {
			JSFUtilities.resetViewRoot();
			Long negoMechanism = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk();
			Long negoModality = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk();
			registerMCNOperationsTO.setBlOperationRepo(false);
			registerMCNOperationsTO.setBlViewRepo(false);
			registerMCNOperationsTO.setOperationRepoDate(null);
			registerMCNOperationsTO.setOperationRepoNumber(null);
			registerMCNOperationsTO.setBlTermPart(false);
			registerMCNOperationsTO.setBlSameCurrency(false);
			registerMCNOperationsTO.setBlRate(false);
			registerMCNOperationsTO.setLstPurchaseParticipants(null);
			registerMCNOperationsTO.setLstSaleParticipants(null);
			registerMCNOperationsTO.setMechanismOperation(new MechanismOperation());
			registerMCNOperationsTO.getMechanismOperation().setOperationDate(getCurrentSystemDate());
			registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
			registerMCNOperationsTO.getMechanismOperation().setBuyerParticipant(new Participant());
			registerMCNOperationsTO.getMechanismOperation().setSellerParticipant(new Participant());
			registerMCNOperationsTO.getMechanismOperation().setMechanisnModality(new MechanismModality());
			registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().setId(new MechanismModalityPK());
			registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().setIdNegotiationMechanismPk(negoMechanism);
			registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().setIdNegotiationModalityPk(negoModality);
			registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
			
			if(Validations.validateIsNotNullAndNotEmpty(negoModality)){
				
				MechanismModality mechanismModality = mcnOperationServiceFacade.getMechanismModality(negoMechanism,negoModality);
				registerMCNOperationsTO.getMechanismOperation().setMechanisnModality(mechanismModality);
				
				registerMCNOperationsTO.setLstPurchaseParticipants(mcnOperationServiceFacade.getListParticipants(negoMechanism, negoModality, null, ComponentConstant.ONE));
				
				registerMCNOperationsTO.setLstSaleParticipants(mcnOperationServiceFacade.getListParticipants(negoMechanism, negoModality, ComponentConstant.ONE, null));
				
				String key = mechanismModality.getId().getIdNegotiationMechanismPk().toString() + GeneralConstants.HYPHEN + negoModality.toString();
				String content = mpMechanismModality.get(key);
				String[] body = content.split(GeneralConstants.HYPHEN);
				Integer settlementSchema = new Integer(body[0]);
				Integer settlementType = new Integer(body[1]);
				Integer indSecuritiesCurrency = new Integer(body[2]);
				
				registerMCNOperationsTO.getMechanismOperation().setSettlementSchema(settlementSchema);
				registerMCNOperationsTO.getMechanismOperation().setSettlementType(settlementType);
				
				// ind_securities_currency is 1, same currency.
				if(BooleanType.YES.getCode().equals(indSecuritiesCurrency)){
					registerMCNOperationsTO.setBlSameCurrency(true);
				}
					
				if(BooleanType.YES.getCode().equals(mechanismModality.getNegotiationModality().getIndTermSettlement())){
					registerMCNOperationsTO.setBlTermPart(true);
				}			
				
				if(NegotiationUtils.mechanismOperationIsViewReport(registerMCNOperationsTO.getMechanismOperation())){
					registerMCNOperationsTO.setBlViewRepo(true);
				}
				
				if(NegotiationUtils.mechanismOperationIsSecundaryReport(registerMCNOperationsTO.getMechanismOperation())){
					registerMCNOperationsTO.setBlOperationRepo(true);
					registerMCNOperationsTO.setOperationRepoDate(getCurrentSystemDate());
				}
				
				if(registerMCNOperationsTO.isBlTermPart() 
						|| (InstrumentType.FIXED_INCOME.getCode().equals(mechanismModality.getNegotiationModality().getInstrumentType())
								&& !NegotiationModalityType.CASH_FIXED_INCOME.getCode().equals(negoModality))){
					registerMCNOperationsTO.setBlRate(true);
				}
				
				registerMCNOperationsTO.setUseFixedRateCalculation(Boolean.FALSE);
				if(InstrumentType.FIXED_INCOME.getCode().equals(mechanismModality.getNegotiationModality().getInstrumentType())){
					registerMCNOperationsTO.setUseFixedRateCalculation(Boolean.TRUE);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Find operation repo.
	 */
	public void findOperationRepo(){
		MechanismModality repoMechModality = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality();
		Long mechanismId = repoMechModality.getId().getIdNegotiationMechanismPk();
		Long modalityId = repoMechModality.getId().getIdNegotiationModalityPk();
		registerMCNOperationsTO.setMechanismOperation(new MechanismOperation());
		registerMCNOperationsTO.getMechanismOperation().setBuyerParticipant(new Participant());
		registerMCNOperationsTO.getMechanismOperation().setSellerParticipant(new Participant());
		registerMCNOperationsTO.getMechanismOperation().setMechanisnModality(repoMechModality);
		registerMCNOperationsTO.getMechanismOperation().setSecurities(new Security());
		registerMCNOperationsTO.getMechanismOperation().setOperationDate(getCurrentSystemDate());
		registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
		registerMCNOperationsTO.setBlFixedIncome(false);
		JSFUtilities.resetViewRoot();
		try{
			if(Validations.validateIsNullOrEmpty(registerMCNOperationsTO.getOperationRepoNumber())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.addContextMessage("frmManualRegisterMCN:txtOperationRepoNumber", FacesMessage.SEVERITY_ERROR, GeneralConstants.EMPTY_STRING, GeneralConstants.EMPTY_STRING);
				JSFUtilities.showRequiredValidationDialog();		
				return;
			}
			
			MechanismOperation mechanismOperation = mcnOperationServiceFacade.findOperationRepo(mechanismId, modalityId, 
					registerMCNOperationsTO.getOperationRepoDate() , registerMCNOperationsTO.getOperationRepoNumber());
			
			if(Validations.validateIsNullOrEmpty(mechanismOperation)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage("alt.mcn.repoSec.operation.not.exist"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(!MechanismOperationStateType.CASH_SETTLED.getCode().equals(mechanismOperation.getOperationState())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage("alt.mcn.repoSec.operation.invalid.state"));
				JSFUtilities.showSimpleValidationDialog();		
				return;
			}
			
			registerMCNOperationsTO.getMechanismOperation().setReferenceOperation(mechanismOperation);
			Participant buyerParticipant  = null;
			buyerParticipant=NegotiationUtils.containsParticipant(registerMCNOperationsTO.getLstPurchaseParticipants(),mechanismOperation.getBuyerParticipant().getIdParticipantPk());	
			if(buyerParticipant == null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.MCN_PARTICIPANT_NOTALLOWED_MODALITY,mechanismOperation.getBuyerParticipant().getDisplayCodeMnemonic()));
				JSFUtilities.showSimpleValidationDialog();		
				return;
			}
			registerMCNOperationsTO.getMechanismOperation().setBuyerParticipant(buyerParticipant);
			registerMCNOperationsTO.getMechanismOperation().setSellerParticipant(buyerParticipant);
			registerMCNOperationsTO.getMechanismOperation().setSettlementSchema(mechanismOperation.getSettlementSchema());
			registerMCNOperationsTO.getMechanismOperation().setSettlementType(mechanismOperation.getSettlementType());
			//registerMCNOperationsTO.getMechanismOperation().setBallotNumber(mechanismOperation.getBallotNumber());
			//registerMCNOperationsTO.getMechanismOperation().setSequential(mechanismOperation.getSequential());
			registerMCNOperationsTO.getMechanismOperation().setSecurities(mechanismOperation.getSecurities());
			ParameterTable objCurrency = mapParameters.get(registerMCNOperationsTO.getMechanismOperation().getSecurities().getCurrency());
			if(objCurrency!=null){
				registerMCNOperationsTO.getMechanismOperation().getSecurities().setCurrencyName(objCurrency.getParameterName());
				mcnOperationServiceFacade.setCurrency(objCurrency,registerMCNOperationsTO.getMechanismOperation(),registerMCNOperationsTO.isBlTermPart()?1:0);
			}
			if(InstrumentType.FIXED_INCOME.getCode().equals(mechanismOperation.getSecurities().getInstrumentType())){
				registerMCNOperationsTO.setBlFixedIncome(true);
			}
			registerMCNOperationsTO.getMechanismOperation().setStockQuantity(mechanismOperation.getStockQuantity());
			registerMCNOperationsTO.getMechanismOperation().setCurrency(mechanismOperation.getCurrency());
			registerMCNOperationsTO.getMechanismOperation().setExchangeRate(mechanismOperation.getExchangeRate());
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	
	/**
	 * Validate reference number.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateReferenceNumber() throws ServiceException{
		//valido que no sea 0, de serlo asi se limpia
		//cleanReferenceNumber();
		//...
		//continua con su flujo normal
		Long negoMechamisn = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk();
		String referenceNumber = registerMCNOperationsTO.getMechanismOperation().getReferenceNumber();
		if(Validations.validateIsNotNullAndNotEmpty(negoMechamisn) && Validations.validateIsNotNullAndNotEmpty(referenceNumber)){
			boolean blExist = mcnOperationServiceFacade.chkOperationNumberMechanism(negoMechamisn,referenceNumber);
			if(blExist){
				registerMCNOperationsTO.getMechanismOperation().setReferenceNumber(null);
				registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
				registerMCNOperationsTO.getMechanismOperation().setBallotNumber(null);
				registerMCNOperationsTO.getMechanismOperation().setSequential(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_INVALID_OPERATION_NUMBER_BY_MECHANISM));
			}else{
				registerMCNOperationsTO.getMechanismOperation().setReferenceDate(getCurrentSystemDate());

				try {
					registerMCNOperationsTO.getMechanismOperation().setBallotNumber(Long.valueOf(referenceNumber));
					registerMCNOperationsTO.getMechanismOperation().setSequential(Long.valueOf(referenceNumber));
				} catch(NumberFormatException e) {
					registerMCNOperationsTO.getMechanismOperation().setReferenceNumber(null);
					registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
					registerMCNOperationsTO.getMechanismOperation().setBallotNumber(null);
					registerMCNOperationsTO.getMechanismOperation().setSequential(null);
					
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_INVALID_OPERATION_NUMBER_WRONG_REGEX));
				}
			}			
		}else{
			registerMCNOperationsTO.getMechanismOperation().setReferenceNumber(null);
			registerMCNOperationsTO.getMechanismOperation().setReferenceDate(null);
		}
	}
	
	/**
	 * Validate stock quantity.
	 */
	
	public void cleanPrices() {
		registerMCNOperationsTO.getMechanismOperation().setRealCashAmount(null);
		registerMCNOperationsTO.getMechanismOperation().setRealCashPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setRealCashPricePercentage(null);
		registerMCNOperationsTO.getMechanismOperation().setCashPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setCashAmount(null);
		registerMCNOperationsTO.getMechanismOperation().setTermAmount(null);
		registerMCNOperationsTO.getMechanismOperation().setTermPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setCashPricePercentage(null);
		
		JSFUtilities.resetComponent("frmManualRegisterMCN:txtRealCashPrice");
		JSFUtilities.resetComponent("frmManualRegisterMCN:txtCashAmount");	
	}
	
	public void changeStockQuantity() {
		cleanPrices();
		validateStockQuantity();
	}
	
	public Boolean validateStockQuantity(){	
		
		Security security = registerMCNOperationsTO.getMechanismOperation().getSecurities();
		BigDecimal stockQuantity = registerMCNOperationsTO.getMechanismOperation().getStockQuantity();
		
		if(Validations.validateIsNotNullAndNotEmpty(stockQuantity) && stockQuantity.compareTo(BigDecimal.ZERO) == 1){
			
			if(Validations.validateIsNullOrEmpty(security.getIdSecurityCodePk())){
				registerMCNOperationsTO.getMechanismOperation().setStockQuantity(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_CORDIGOVALOR_NOTENTERED));
				return Boolean.FALSE;
			}			
			
			// validar cantidad no es mayor a balance en circulacion, no PP
			Integer indPrimaryPlacement = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality().getIndPrimaryPlacement();
			if(!ComponentConstant.ONE.equals(indPrimaryPlacement)){
			    if(Validations.validateIsNullOrEmpty(security.getCirculationBalance()) || stockQuantity.compareTo(security.getCirculationBalance()) > 0){
			    	registerMCNOperationsTO.getMechanismOperation().setStockQuantity(null);
			    	JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_CIRCULATION));
					return Boolean.FALSE;
	            }
			}else{				
				/*
				// validar min investment  (max invesment is per inversionist ) 
				BigDecimal minInvesment = security.getMinimumInvesment();
				BigDecimal nominalValue = security.getCurrentNominalValue();				    
			    BigDecimal amountEntered = registerMCNOperationsTO.getMechanismOperation().getStockQuantity().multiply(nominalValue);
				if(minInvesment!=null && minInvesment.compareTo(amountEntered) == 1){
					registerMCNOperationsTO.getMechanismOperation().setStockQuantity(null);
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_RANGE_AMOUTNS));
					return;
				}*/
			}

			if(NegotiationUtils.mechanismOperationIsSecundaryReport(registerMCNOperationsTO.getMechanismOperation())){
				BigDecimal realStockQuantity = registerMCNOperationsTO.getMechanismOperation().getReferenceOperation().getStockQuantity();
				if(stockQuantity.compareTo(realStockQuantity) == 1){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.mcn.repoSec.quantity.exceeded"));					
					JSFUtilities.showSimpleValidationDialog();
					registerMCNOperationsTO.getMechanismOperation().setStockQuantity(null);
				}
			}						
		}else{
			registerMCNOperationsTO.getMechanismOperation().setStockQuantity(null);
		}
		
		return Boolean.TRUE;
	}
	
	/**
	 * Massive register.
	 *
	 * @return the string
	 */
	public String massiveRegister(){
		mcnMassiveMgmtBean.cleanAll();
		return REGISTER_MASSIVE_MAPPING;
	}

	
	
	/** This method is called before MCN manual registration and performs required validations.*/
	public void beforeSave() {
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
				PropertiesUtilities.getMessage("mcn.msg.register.confirm"));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Save operation.
	 */
	@LoggerAuditWeb
	public void saveOperation(){
		try{
			String mechanismName = null;
			String modalityName = null;
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
				List<MechanismOperationMarketFact> lstMechanismOperationMarketFact = new ArrayList<MechanismOperationMarketFact>();
				MechanismOperationMarketFact mechanismOperationMarketFact = new MechanismOperationMarketFact();
				mechanismOperationMarketFact.setMarketDate(registerMCNOperationsTO.getMechanismOperation().getOperationDate());
				if(registerMCNOperationsTO.isBlFixedIncome()){
					mechanismOperationMarketFact.setMarketRate(registerMCNOperationsTO.getMechanismOperation().getAmountRate());
				}else{
					mechanismOperationMarketFact.setMarketPrice(registerMCNOperationsTO.getMechanismOperation().getCashPrice());
				}
				lstMechanismOperationMarketFact.add(mechanismOperationMarketFact);
				registerMCNOperationsTO.getMechanismOperation().setMechanismOperationMarketFacts(lstMechanismOperationMarketFact);
			}
			MechanismOperation mechanismOperationTemp = mcnOperationServiceFacade.saveMechanismOperation(registerMCNOperationsTO.getMechanismOperation());	
			for(NegotiationMechanism negoMecha:registerMCNOperationsTO.getLstNegotiationMechanisms()){
				if(negoMecha.getIdNegotiationMechanismPk().equals(mechanismOperationTemp.getMechanisnModality().getId().getIdNegotiationMechanismPk())){
					mechanismName = negoMecha.getMechanismName();
					break;
				}
			}
			for(NegotiationModality negoModa:registerMCNOperationsTO.getLstNegotiationModalities()){
				if(negoModa.getIdNegotiationModalityPk().equals(mechanismOperationTemp.getMechanisnModality().getId().getIdNegotiationModalityPk())){
					modalityName = negoModa.getModalityName();
					break;
				}
			}
			
			Date operationDate = registerMCNOperationsTO.getMechanismOperation().getOperationDate();
			Long operationNumber = registerMCNOperationsTO.getMechanismOperation().getOperationNumber();
			String desc = NegotiationUtils.getOperationDescription(mechanismName, modalityName, operationDate, operationNumber);
			
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.MCN_MANUAL_REGISTER_SUCCESS, new Object[]{desc}));
		}
		catch (ServiceException e) {
			if(e.getErrorService()!=null){
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
						, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				return;
			}
			excepcion.fire(new ExceptionToCatchEvent(e));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
						,PropertiesConstants.MCN_REGISTRATION_FAILED);
		}
	}
	
	/**
	 * On change rate.
	 */
	public void onChangeRate() {
		JSFUtilities.setValidViewComponent("frmManualRegisterMCN");
		BigDecimal enteredRate = registerMCNOperationsTO.getMechanismOperation().getCashPricePercentage();
		
//		if(enteredRate==null){
//			registerMCNOperationsTO.getMechanismOperation().setAmountRate(null);
//			JSFUtilities.showSimpleValidationDialog();	
//			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//					, PropertiesUtilities.getMessage(PropertiesConstants.MCN_RATE_NOT_NULL_OR_ZERO));
//			return;
//		}
		
		registerMCNOperationsTO.getMechanismOperation().setCashPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setCashAmount(null);
		registerMCNOperationsTO.getMechanismOperation().setRealCashPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setRealCashAmount(null);
		registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
		registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
		registerMCNOperationsTO.getMechanismOperation().setTermPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setTermAmount(null);
	}
	
	/**
	 * Gets the real cash amount.
	 *
	 * @return the real cash amount
	 */
	public void getRealCashAmount(){
		registerMCNOperationsTO.getMechanismOperation().setRealCashAmount(null);
		BigDecimal realCashPrice = registerMCNOperationsTO.getMechanismOperation().getRealCashPrice();
		BigDecimal stockQuantity = registerMCNOperationsTO.getMechanismOperation().getStockQuantity();
		if(Validations.validateIsNotNullAndNotEmpty(realCashPrice) && realCashPrice.compareTo(BigDecimal.ZERO) == 1){
			
			//validate if stock quantity is not null
			if(Validations.validateIsNullOrEmpty(stockQuantity)){
				registerMCNOperationsTO.getMechanismOperation().setRealCashPrice(null);
				JSFUtilities.showSimpleValidationDialog();	
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_STOCKQUANTITY_NOT_NULL));
				return;
			}
			
			BigDecimal realCashAmount = NegotiationUtils.getSettlementAmount(realCashPrice, stockQuantity) ;
			
			//the cash amount must be greater than real cash amount
//			if(Validations.validateIsNotNullAndNotEmpty(cashAmount) && realCashAmount.compareTo(cashAmount) > 0){
//				registerMCNOperationsTO.getMechanismOperation().setRealCashPrice(null);
//				JSFUtilities.showSimpleValidationDialog();
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_REAL_CASH_AMOUNT_VAL));
//			}else{
			registerMCNOperationsTO.getMechanismOperation().setRealCashAmount(realCashAmount);
//			}
		}else{
			registerMCNOperationsTO.getMechanismOperation().setRealCashPrice(null);
		}
	}
	
	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public void getCashAmount(){
		registerMCNOperationsTO.getMechanismOperation().setCashAmount(null);
		registerMCNOperationsTO.getMechanismOperation().setTermAmount(null);
		registerMCNOperationsTO.getMechanismOperation().setTermPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
		registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
		
		BigDecimal cashPrice = registerMCNOperationsTO.getMechanismOperation().getCashPrice();
		BigDecimal stockQuantity = registerMCNOperationsTO.getMechanismOperation().getStockQuantity();
		BigDecimal realCashAmount = registerMCNOperationsTO.getMechanismOperation().getRealCashAmount();
		if(Validations.validateIsNotNull(cashPrice) && cashPrice.compareTo(BigDecimal.ZERO) > 0){
			
			//validate if stock quantity is not null
			if(Validations.validateIsNull(stockQuantity)){
				registerMCNOperationsTO.getMechanismOperation().setCashPrice(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(PropertiesConstants.MCN_STOCKQUANTITY_NOT_NULL));
				return;
			}
			
			BigDecimal cashAmount = NegotiationUtils.getSettlementAmount(cashPrice,stockQuantity);
			
			//precio sucio no debe ser menor a precio limpio
			if (Validations.validateIsNotNull(realCashAmount) && realCashAmount.compareTo(cashAmount) > 0 && !registerMCNOperationsTO.getUseFixedRateCalculation()){
				registerMCNOperationsTO.getMechanismOperation().setCashPrice(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(PropertiesConstants.MCN_REAL_CASH_AMOUNT_VAL));
				return;
			}
			
			registerMCNOperationsTO.getMechanismOperation().setCashAmount(cashAmount);
			
			if(registerMCNOperationsTO.getUseFixedRateCalculation()) {
				if(Validations.validateIsNull(registerMCNOperationsTO.getMechanismOperation().getSecurities())) {
					registerMCNOperationsTO.getMechanismOperation().setCashPrice(null);
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(PropertiesConstants.MCN_CORDIGOVALOR_NOTENTERED));
					return;
				}
				
				registerMCNOperationsTO.getMechanismOperation().setRealCashAmount(null);
				registerMCNOperationsTO.getMechanismOperation().setRealCashPrice(null);
				
				//this.mcnOperationServiceFacade.fixedRateCalculations(registerMCNOperationsTO);				
				
			}
			
		}else{
			registerMCNOperationsTO.getMechanismOperation().setCashPrice(null);
		}		
	}	
			
	/**
	 * no usado.
	 *
	 * @return the effective rate
	 */
	public void getEffectiveRate(){
		registerMCNOperationsTO.getMechanismOperation().setTermAmount(null);
		BigDecimal termPrice = registerMCNOperationsTO.getMechanismOperation().getTermPrice();
		BigDecimal stockQuantity = registerMCNOperationsTO.getMechanismOperation().getStockQuantity();
		BigDecimal cashAmount = registerMCNOperationsTO.getMechanismOperation().getCashAmount();
		if(Validations.validateIsNotNull(termPrice) && termPrice.compareTo(BigDecimal.ZERO) > 0 ){
			//validate if stockQuantity is not null
			if(Validations.validateIsNullOrEmpty(stockQuantity)){
				registerMCNOperationsTO.getMechanismOperation().setTermPrice(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_STOCKQUANTITY_NOT_NULL));
				return;
			}
			
			BigDecimal termAmount = NegotiationUtils.getSettlementAmount(termPrice, stockQuantity);
			BigDecimal rate = NegotiationUtils.getEfectiveRate(termAmount, cashAmount);		
			
			registerMCNOperationsTO.getMechanismOperation().setAmountRate(rate);
			registerMCNOperationsTO.getMechanismOperation().setTermAmount(termAmount);

		}else{
			registerMCNOperationsTO.getMechanismOperation().setTermPrice(null);
		}
	}	

	/**
	 * Gets the term settlement date.
	 *
	 * @return the term settlement date
	 */
	public void getTermSettlementDate() {
		registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
		registerMCNOperationsTO.getMechanismOperation().setTermPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setTermAmount(null);
		if(Validations.validateIsNotNullAndNotEmpty(registerMCNOperationsTO.getMechanismOperation().getTermSettlementDays())){
			if(Validations.validateIsNotNullAndNotEmpty(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDate()) 
					&& Validations.validateIsNotNullAndNotEmpty(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDays())){
				// si los dias contado es >= a los dias plazo
				if(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDays() >= 
						registerMCNOperationsTO.getMechanismOperation().getTermSettlementDays()){
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
								, PropertiesUtilities.getMessage(PropertiesConstants.MCN_CASH_DAYS_GREATER_TERM_DAYS));
					registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
					return;
				}
				
				NegotiationModality negotiationModality = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality();
			
				try{				
					mcnOperationServiceFacade.setSettlementDate(negotiationModality, registerMCNOperationsTO.getMechanismOperation(), ComponentConstant.TERM_PART);
				}catch (ServiceException e){
					registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
					return;
				}
				
				//Si es rta fija y la fecha calculada es mayor a la fecha que vence el valor
				Security security = registerMCNOperationsTO.getMechanismOperation().getSecurities();
				if(InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType()) && 
				   Validations.validateIsNotNullAndNotEmpty(security.getExpirationDate()))
				{	
					Date dayBeforeExpiration = holidayQueryServiceBean.getCalculateDateServiceBean(security.getExpirationDate(), 
																									1, 0,NegotiationConstant.WORKING_DAYS);
					if(registerMCNOperationsTO.getMechanismOperation().getTermSettlementDate().after(dayBeforeExpiration)){
						//we verify if the security expiration date is weekend or holiday
						boolean isNonWorkingDate= holidayQueryServiceBean.isNonWorkingDayServiceBean(security.getExpirationDate(), true);
						if (isNonWorkingDate) {
							// we forward the expiration date to working date 
							Date newDate= holidayQueryServiceBean.getCalculateDateServiceBean(security.getExpirationDate(), 
																								1, 1,NegotiationConstant.WORKING_DAYS);
							if (newDate.compareTo(registerMCNOperationsTO.getMechanismOperation().getTermSettlementDate()) != 0) {
								registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
								registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
								JSFUtilities.showSimpleValidationDialog();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS));
								return;
							}
						} else {
							registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
							registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
							JSFUtilities.showSimpleValidationDialog();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS));
							return;
						}
					}						
				}
				
				//calcular precio y monto
				if(registerMCNOperationsTO.isBlTermPart()){
					
					//validate if rate is not null
					BigDecimal cashPrice = registerMCNOperationsTO.getMechanismOperation().getCashPrice();
					BigDecimal stockQuantity = registerMCNOperationsTO.getMechanismOperation().getStockQuantity();
					BigDecimal rate = registerMCNOperationsTO.getMechanismOperation().getCashPricePercentage();
					
					//calculate term price based on days between settlement days considering weekends
					Long termSettlementDays = new Long(CommonsUtilities.getDaysBetween(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDate(), 
							registerMCNOperationsTO.getMechanismOperation().getTermSettlementDate()));
					
					if(Validations.validateIsNull(rate)){
						registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
						registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
						JSFUtilities.showSimpleValidationDialog();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(PropertiesConstants.MCN_RATE_NOT_NULL));
						return;
					}
					
					calculateTermRealCashAmount();
				}
				
			}else{
				registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_NO_CASH_SETTLEMENT_DATE));
			}
		}
	}

	/**
	 * Gets the cash settlement date.
	 *
	 * @return the cash settlement date
	 */
	public boolean getCashSettlementDate() {
		registerMCNOperationsTO.getMechanismOperation().setCashSettlementDate(null);
		registerMCNOperationsTO.getMechanismOperation().setTermSettlementDays(null);
		registerMCNOperationsTO.getMechanismOperation().setTermSettlementDate(null);
		registerMCNOperationsTO.getMechanismOperation().setTermPrice(null);
		registerMCNOperationsTO.getMechanismOperation().setTermAmount(null);
		NegotiationModality negotiationModality = registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality();
		
		if(Validations.validateIsNotNullAndNotEmpty(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDays())){
			if(Validations.validateIsNullOrEmpty(negotiationModality == null)){
				registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_CORDIGOVALOR_MODALITY));
				return false;
			}
				
			try {
				mcnOperationServiceFacade.setSettlementDate(negotiationModality, registerMCNOperationsTO.getMechanismOperation(), ComponentConstant.CASH_PART);
				
			} catch (ServiceException e) {
				registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(null);
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				return false;
			}
			Security security = registerMCNOperationsTO.getMechanismOperation().getSecurities();
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				
				//Si es rta fija y la fecha calculada es mayor a la fecha T-1 que vence el valor
				if(InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType()) && Validations.validateIsNotNullAndNotEmpty(security.getExpirationDate())){							
					//Date utilBeforeExpiration = mcnOperationServiceFacade.getCalculateDate(security.getExpirationDate(), 1, 0,null);							
					if(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDate().after(security.getExpirationDate())){
						registerMCNOperationsTO.getMechanismOperation().setCashSettlementDate(null);
						registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(null);
						JSFUtilities.showSimpleValidationDialog();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS));
						return false;
					}							
				}	
				
				// Si es Repo Secundario, max cash Date es T-1 de la FL2 del reporto original
				if(NegotiationUtils.mechanismOperationIsSecundaryReport(registerMCNOperationsTO.getMechanismOperation()) 
						&& registerMCNOperationsTO.getMechanismOperation().getReferenceOperation().getTermSettlementDate()!=null 
						&& registerMCNOperationsTO.getMechanismOperation().getCashSettlementDate().after(
								CommonsUtilities.addDaysToDate(registerMCNOperationsTO.getMechanismOperation().getReferenceOperation().getTermSettlementDate(),-1))){
					registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(null);
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.MCN_SETTLEMENT_DATE_REPO));
					return false;
				}

//				// maximo de dias para liquidar repo a la vista
//				if(NegotiationUtils.mechanismOperationIsViewReport(registerMCNOperationsTO.getMechanismOperation())){
//					Date maxSettlementTermDate = CommonsUtilities.addDaysToDate(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDate(), 
//							negotiationModality.getTermMaxSettlementDays());
//					Date lastWorkingDate = generalParametersFacade.getLastWorkingDayBetweenDates(registerMCNOperationsTO.getMechanismOperation().getCashSettlementDate(), maxSettlementTermDate);
//					if(Validations.validateIsNotNullAndNotEmpty(lastWorkingDate)){
//						registerMCNOperationsTO.getMechanismOperation().setMaxSettlementTermDate(lastWorkingDate);
//					}else{//no se pudo calcular el maximo dia para liquidacion plazo
//						registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(null);
//						JSFUtilities.showSimpleValidationDialog();
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//								, PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATIONS_VALIDATION_MAX_SETTLEMENT_DATE));
//						return false;
//					}
//				}						
			}else{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.MCN_CORDIGOVALOR_NOTENTERED));
				registerMCNOperationsTO.getMechanismOperation().setCashSettlementDate(null);
				registerMCNOperationsTO.getMechanismOperation().setCashSettlementDays(null);
				JSFUtilities.showSimpleValidationDialog();
				return false;
			}			
			
		}
		return true;
	}
	
	/** This method is called before MCN manual registration and performs required validations.*/
	public void openCalculator() {
		JSFUtilities.executeJavascriptFunction("PF('cnfWCalculator').show();");
	}
	
	/**
	 * Fill maps.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillMaps() throws ServiceException{
		
//		mpReferences = new HashMap<Long, String>();	
//		mpDescriptions = new HashMap<Long,  String>();
//		
//		ParameterTableTO paramTO = new ParameterTableTO();
//		paramTO.setLstMasterTableFk(new ArrayList<Integer>());
//		paramTO.getLstMasterTableFk().add(MasterTableType.IND_STOCK_REFERENCE_TYPE.getCode());
//		paramTO.getLstMasterTableFk().add(MasterTableType.IND_FUNDS_REFERENCE_TYPE.getCode());
//		paramTO.getLstMasterTableFk().add(MasterTableType.IND_MARGIN_REFERENCE_TYPE.getCode());
//		
//		List<ParameterTable> lstIndicators = generalParametersFacade.getListParameterTableServiceBean(paramTO);
//		
//		for(ParameterTable paramTab:lstIndicators){			
//			mpReferences.put(new Long(paramTab.getParameterTablePk()), paramTab.getText1());		
//			mpDescriptions.put(new Long(paramTab.getParameterTablePk()), paramTab.getParameterName());
//		}
		
		ParameterTableTO paramTabTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
			masterPks.add(MasterTableType.CURRENCY.getCode());
			masterPks.add(MasterTableType.MOTIVOS_CANCELACION_OPERACION_MCN.getCode());
			masterPks.add(MasterTableType.SECURITIES_STATE.getCode());
		paramTabTO.setLstMasterTableFk(masterPks);
		List<ParameterTable> params = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:params){
			mapParameters.put(paramTab.getParameterTablePk(), paramTab);
		}
		
		List<MechanismModality> lstResult = mcnOperationServiceFacade.getListMechanismModality();
		mpMechanismModality = new HashMap<String, String>();
		for(MechanismModality mechanismModality:lstResult){
			String key = mechanismModality.getId().getIdNegotiationMechanismPk().toString() + 
							GeneralConstants.HYPHEN + mechanismModality.getId().getIdNegotiationModalityPk().toString();
			String content = mechanismModality.getSettlementSchema().toString() + 
								GeneralConstants.HYPHEN + mechanismModality.getSettlementType().toString() +
								GeneralConstants.HYPHEN + mechanismModality.getIndSecuritiesCurrency().toString();
			mpMechanismModality.put(key, content);
		}
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		searchMCNOperationsTO.setLstStates(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		List<ParameterTable> lstDateType = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(GeneralConstants.ZERO_VALUE_INTEGER);
		paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.operationDate"));
		lstDateType.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(GeneralConstants.ONE_VALUE_INTEGER);
		paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.cashSettlementDate"));
		lstDateType.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(GeneralConstants.TWO_VALUE_INTEGER);
		paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.termSettlementDate"));
		lstDateType.add(paramTab);
		searchMCNOperationsTO.setLstDateType(lstDateType);
		
		paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		searchMCNOperationsTO.setLstCurrency(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));	
	}

	/**
	 * Fill combos for mcn operations.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombosForMCNOperations() throws ServiceException{
		
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
		paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
		paramTab.setParameterName(SettlementSchemaType.NET.getValue());
		lstTemp.add(paramTab);
		registerMCNOperationsTO.setLstSettlementSchema(lstTemp);
		lstTemp = new ArrayList<ParameterTable>();
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementType.DVP.getCode());
		paramTab.setParameterName(SettlementType.DVP.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementType.ECE.getCode());
		paramTab.setParameterName(SettlementType.ECE.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementType.FOP.getCode());
		paramTab.setParameterName(SettlementType.FOP.getValue());
		lstTemp.add(paramTab);
		registerMCNOperationsTO.setLstSettlementType(lstTemp);
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Fill view.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @throws ServiceException the service exception
	 */
	private void fillView(Long idMechanismOperation) throws ServiceException{
		
		operationBusinessComp.setIdMechanismOperationPk(idMechanismOperation);
		operationBusinessComp.setShowAssignments(true);
		operationBusinessComp.setShowViewDetails(false);
		operationBusinessComp.searchMechanismOperation();
		
		registerMCNOperationsTO = new RegisterMCNOperationsTO();
		registerMCNOperationsTO.setMechanismOperation(operationBusinessComp.getMechanismOperation());
		
		//mechanismOperation = mcnOperationServiceFacade.loadMechanismOperation(mechanismOperation, null,false , mpReferences, mpDescriptions);
		
//		ParameterTable param = mapParameters.get(mechanismOperation.getCurrency());
//		if(param!=null){
//			mechanismOperation.setCurrencyDescription(param.getParameterName());
//		}
//		param = mapParameters.get(mechanismOperation.getSecurities().getCurrency());
//		if(param!=null){
//			mechanismOperation.getSecurities().setCurrencyName(param.getParameterName());
//		}
		
		//fillCombosForMCNOperations();
		
//		ParameterTable param = null;
//		if(MechanismOperationStateType.CANCELED_STATE.getCode().equals(mechanismOperation.getOperationState())){
//			param = mapParameters.get(mechanismOperation.getCancelMotive());
//			if(param!=null){
//				mechanismOperation.setCancelDescription(param.getParameterName());
//			}
//			registerMCNOperationsTO.setBlCancel(true);
//		}else{
//			registerMCNOperationsTO.getMechanismOperation().setCancelMotive(null);
//		}
			
		//si la accion es cancelar
		if(blCancel){
			
			ParameterTableTO paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
			paramTabTO.setMasterTableFk(MasterTableType.MOTIVOS_CANCELACION_OPERACION_MCN.getCode());
			
			if(blCancel){
				List<ParameterTable> lstResult = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
				registerMCNOperationsTO.setLstCancelMotives(lstResult);
			}
		}
		
		if(ComponentConstant.ONE.equals(registerMCNOperationsTO.getMechanismOperation().getMechanisnModality().getNegotiationModality().getIndTermSettlement())){
			registerMCNOperationsTO.setBlTermPart(true);
		}
		
	}
	
	public void dirtyPriceChange() {		
		if(validateStockQuantity() && isPriceCalculation()) {			
			this.operationPriceCalculatorServiceFacade.dirtyPriceChangeCalculation(registerMCNOperationsTO.getMechanismOperation());
		}
	}
	
	public void dirtyAmountChange() {
		if(validateStockQuantity() && isAmountCalculation()) {
			this.operationPriceCalculatorServiceFacade.dirtyAmountChangeCalculation(registerMCNOperationsTO.getMechanismOperation());
		} 
	}
	
	public Boolean isPriceCalculation() {
		return Validations.validateIsNotNull(registerMCNOperationsTO.getMechanismOperation().getRealCashPricePercentage());
	}
	
	public Boolean isAmountCalculation() {
		return Validations.validateIsNotNull(registerMCNOperationsTO.getMechanismOperation().getRealCashAmount());
	}
	
	public void cleanPriceChange() {
		this.operationPriceCalculatorServiceFacade.cleanPriceChange(registerMCNOperationsTO.getMechanismOperation());
	}
	
	public void changeWarrantyCause() {
		
	}
	
	public void changeTermInterestRate() {
		if(Validations.validateIsNotNullAndPositive(registerMCNOperationsTO.getMechanismOperation().getTermAmount())){
			calculateTermRealCashAmount();
		}
	}
	
	public void calculateTermRealCashAmount() {
		if(Validations.validateIsNull(registerMCNOperationsTO.getMechanismOperation().getWarrantyCause())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Aforo. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(registerMCNOperationsTO.getMechanismOperation().getTermInterestRate())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Interés Repo. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(registerMCNOperationsTO.getMechanismOperation().getTermSettlementDays())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Días Liquidación Plazo. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(registerMCNOperationsTO.getMechanismOperation().getRealCashPricePercentage())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar Precio Dirty (%). Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		if(Validations.validateIsNull(registerMCNOperationsTO.getMechanismOperation().getStockQuantity())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Falta ingresar la cantidad de títulos. Verifique.");
			JSFUtilities.showSimpleValidationDialog();			
			return;
		}
		
		this.operationPriceCalculatorServiceFacade.calculateTermRealCashAmount(registerMCNOperationsTO.getMechanismOperation());
	}
	
	public void changeTermAmount(){
		MechanismOperation operation = registerMCNOperationsTO.getMechanismOperation();
		try {
			BigDecimal cashAmount = operation.getCashAmount();//Obtenemos el monto Contado de la Operacion
			if(cashAmount==null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.OTC_OPERATION_VALIDATION_CASH_AMOUNT_EMPTY));
				throw new ServiceException();
			}
			BigDecimal operationQuantity = operation.getStockQuantity();//Obtenemos la cantidad de valores de la operacion
			BigDecimal termAmount = operation.getTermAmount();//Obtenemos el monto plazo ingresado
			BigDecimal termPrice = termAmount.divide(operationQuantity,8,RoundingMode.CEILING);//Calculamos el precio plazo

			operation.setTermPrice(termPrice);
		} catch (ServiceException e) {
			operation.setTermPrice(null);operation.setTermAmount(null);operation.setAmountRate(null);
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			}
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnCancel(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 *  CLEAN INPUTS 0 *.
	 */
	
	//Nro de papeleta
	public void cleanBallotNumber(){
		if(Validations.validateIsNull(registerMCNOperationsTO.getMechanismOperation()) ||
		   Validations.validateIsNullOrNotPositive(registerMCNOperationsTO.getMechanismOperation().getBallotNumber()) ){
			registerMCNOperationsTO.getMechanismOperation().setBallotNumber(null);
		}
	}
	
	/**
	 * ****************.
	 *
	 * @return the user info
	 */
	
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Gets the mcn manual service facade.
	 *
	 * @return the mcn manual service facade
	 */
	public McnOperationServiceFacade getMcnManualServiceFacade() {
		return mcnOperationServiceFacade;
	}
	
	/**
	 * Sets the mcn manual service facade.
	 *
	 * @param mcnManualServiceFacade the new mcn manual service facade
	 */
	public void setMcnManualServiceFacade(McnOperationServiceFacade mcnManualServiceFacade) {
		this.mcnOperationServiceFacade = mcnManualServiceFacade;
	}
	
	/**
	 * Gets the reg operaciones mcn service facade.
	 *
	 * @return the reg operaciones mcn service facade
	 */
	public McnOperationServiceFacade getRegOperacionesMCNServiceFacade() {
		return mcnOperationServiceFacade;
	}
	
	/**
	 * Sets the reg operaciones mcn service facade.
	 *
	 * @param mcnManualServiceFacade the new reg operaciones mcn service facade
	 */
	public void setRegOperacionesMCNServiceFacade(
			McnOperationServiceFacade mcnManualServiceFacade) {
		this.mcnOperationServiceFacade = mcnManualServiceFacade;
	}
	
	/**
	 * Gets the search mcn operations to.
	 *
	 * @return the search mcn operations to
	 */
	public SearchMCNOperationsTO getSearchMCNOperationsTO() {
		return searchMCNOperationsTO;
	}
	
	/**
	 * Sets the search mcn operations to.
	 *
	 * @param searchMCNOperationsTO the new search mcn operations to
	 */
	public void setSearchMCNOperationsTO(SearchMCNOperationsTO searchMCNOperationsTO) {
		this.searchMCNOperationsTO = searchMCNOperationsTO;
	}
	
	/**
	 * Checks if is bl depositary.
	 *
	 * @return true, if is bl depositary
	 */
	public boolean isBlDepositary() {
		return blDepositary;
	}
	
	/**
	 * Sets the bl depositary.
	 *
	 * @param blDepositary the new bl depositary
	 */
	public void setBlDepositary(boolean blDepositary) {
		this.blDepositary = blDepositary;
	}

	/**
	 * Gets the map parameters.
	 *
	 * @return the map parameters
	 */
	public Map<Integer, ParameterTable> getMapParameters() {
		return mapParameters;
	}

	/**
	 * Sets the map parameters.
	 *
	 * @param mapParameters the map parameters
	 */
	public void setMapParameters(Map<Integer, ParameterTable> mapParameters) {
		this.mapParameters = mapParameters;
	}

	/**
	 * Checks if is bl cancel.
	 *
	 * @return true, if is bl cancel
	 */
	public boolean isBlCancel() {
		return blCancel;
	}
	
	/**
	 * Sets the bl cancel.
	 *
	 * @param blCancel the new bl cancel
	 */
	public void setBlCancel(boolean blCancel) {
		this.blCancel = blCancel;
	}
	
	/**
	 * Gets the register mcn operations to.
	 *
	 * @return the register mcn operations to
	 */
	public RegisterMCNOperationsTO getRegisterMCNOperationsTO() {
		return registerMCNOperationsTO;
	}
	
	/**
	 * Sets the register mcn operations to.
	 *
	 * @param registerMCNOperationsTO the new register mcn operations to
	 */
	public void setRegisterMCNOperationsTO(
			RegisterMCNOperationsTO registerMCNOperationsTO) {
		this.registerMCNOperationsTO = registerMCNOperationsTO;
	}
//	public Map<String, String> getMpMechanismModality() {
//		return mpMechanismModality;
//	}
//	public void setMpMechanismModality(Map<String, String> mpMechanismModality) {
//		this.mpMechanismModality = mpMechanismModality;
//	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperationTO getMechanismOperation() {
		return mechanismOperation;
	}

	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperationTO mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}
	
}
