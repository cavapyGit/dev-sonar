package com.pradera.negotiations.mcnoperations.view;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountOperationSearch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class HolderAccountOperationSearch implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The alternate code. */
	private String alternateCode; 
	
	/** The incharge. */
	private String incharge;
	/** The registryUser. */
	private String holders;
	/** The role. */
	private String role; 
	/** The stockQuantity. */
	private String stockQuantity; 
	/** The stockParticipant. */
	private String stockParticipant;
	
	/** The id stock participant. */
	private String idStockParticipant;
	
	/** The stock participant description. */
	private String stockParticipantDescription;
	/** The fundsParticipant. */
	private String fundsParticipant;
	
	/** The id funds participant. */
	private String idFundsParticipant;
	
	/** The funds participant description. */
	private String fundsParticipantDescription;
	
	/** The operation part. */
	private String operationPart;
	
	/** The margin guarantees. */
	private BigDecimal marginGuarantees;
	
	/** The ind stock description. */
	private String indStockDescription;
	
	/** The ind funds description. */
	private String indFundsDescription;

	/**
	 * Gets the holders.
	 *
	 * @return the holders
	 */
	public String getHolders() {
		return holders;
	}
	
	/**
	 * Sets the holders.
	 *
	 * @param holders the holders to set
	 */
	public void setHolders(String holders) {
		this.holders = holders;
	}
	
	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * Sets the role.
	 *
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stockQuantity
	 */
	public String getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(String stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the stock participant.
	 *
	 * @return the stockParticipant
	 */
	public String getStockParticipant() {
		return stockParticipant;
	}
	
	/**
	 * Sets the stock participant.
	 *
	 * @param stockParticipant the stockParticipant to set
	 */
	public void setStockParticipant(String stockParticipant) {
		this.stockParticipant = stockParticipant;
	}
	
	/**
	 * Gets the funds participant.
	 *
	 * @return the fundsParticipant
	 */
	public String getFundsParticipant() {
		return fundsParticipant;
	}
	
	/**
	 * Sets the funds participant.
	 *
	 * @param fundsParticipant the fundsParticipant to set
	 */
	public void setFundsParticipant(String fundsParticipant) {
		this.fundsParticipant = fundsParticipant;
	}
	
	/**
	 * Gets the ind stock description.
	 *
	 * @return the ind stock description
	 */
	public String getIndStockDescription() {
		return indStockDescription;
	}
	
	/**
	 * Sets the ind stock description.
	 *
	 * @param indStockDescription the new ind stock description
	 */
	public void setIndStockDescription(String indStockDescription) {
		this.indStockDescription = indStockDescription;
	}
	
	/**
	 * Gets the ind funds description.
	 *
	 * @return the ind funds description
	 */
	public String getIndFundsDescription() {
		return indFundsDescription;
	}
	
	/**
	 * Sets the ind funds description.
	 *
	 * @param indFundsDescription the new ind funds description
	 */
	public void setIndFundsDescription(String indFundsDescription) {
		this.indFundsDescription = indFundsDescription;
	}
	
	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public String getOperationPart() {
		return operationPart;
	}
	
	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(String operationPart) {
		this.operationPart = operationPart;
	}
	
	/**
	 * Gets the margin guarantees.
	 *
	 * @return the margin guarantees
	 */
	public BigDecimal getMarginGuarantees() {
		return marginGuarantees;
	}
	
	/**
	 * Sets the margin guarantees.
	 *
	 * @param marginGuarantees the new margin guarantees
	 */
	public void setMarginGuarantees(BigDecimal marginGuarantees) {
		this.marginGuarantees = marginGuarantees;
	}
	
	/**
	 * Gets the alternate code.
	 *
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return alternateCode;
	}
	
	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	
	/**
	 * Gets the incharge.
	 *
	 * @return the incharge
	 */
	public String getIncharge() {
		return incharge;
	}
	
	/**
	 * Sets the incharge.
	 *
	 * @param incharge the new incharge
	 */
	public void setIncharge(String incharge) {
		this.incharge = incharge;
	}
	
	/**
	 * Gets the stock participant description.
	 *
	 * @return the stock participant description
	 */
	public String getStockParticipantDescription() {
		return stockParticipantDescription;
	}
	
	/**
	 * Sets the stock participant description.
	 *
	 * @param stockParticipantDescription the new stock participant description
	 */
	public void setStockParticipantDescription(String stockParticipantDescription) {
		this.stockParticipantDescription = stockParticipantDescription;
	}
	
	/**
	 * Gets the funds participant description.
	 *
	 * @return the funds participant description
	 */
	public String getFundsParticipantDescription() {
		return fundsParticipantDescription;
	}
	
	/**
	 * Sets the funds participant description.
	 *
	 * @param fundsParticipantDescription the new funds participant description
	 */
	public void setFundsParticipantDescription(String fundsParticipantDescription) {
		this.fundsParticipantDescription = fundsParticipantDescription;
	}
	
	/**
	 * Gets the id stock participant.
	 *
	 * @return the id stock participant
	 */
	public String getIdStockParticipant() {
		return idStockParticipant;
	}
	
	/**
	 * Sets the id stock participant.
	 *
	 * @param idStockParticipant the new id stock participant
	 */
	public void setIdStockParticipant(String idStockParticipant) {
		this.idStockParticipant = idStockParticipant;
	}
	
	/**
	 * Gets the id funds participant.
	 *
	 * @return the id funds participant
	 */
	public String getIdFundsParticipant() {
		return idFundsParticipant;
	}
	
	/**
	 * Sets the id funds participant.
	 *
	 * @param idFundsParticipant the new id funds participant
	 */
	public void setIdFundsParticipant(String idFundsParticipant) {
		this.idFundsParticipant = idFundsParticipant;
	}
	
}
