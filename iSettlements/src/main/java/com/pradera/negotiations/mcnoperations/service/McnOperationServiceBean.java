package com.pradera.negotiations.mcnoperations.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.massive.type.MechanismFileProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.InChargeNegotiationType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.OtcOperationStateType;
import com.pradera.model.negotiation.type.ParticipantOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.OperationSettlement;
import com.pradera.model.settlement.OperationUnfulfillment;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.SettlementReport;
import com.pradera.model.settlement.SettlementUnfulfillment;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.model.settlement.type.SettlementProcessStateType;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.accountassignment.to.SettlementReportTO;
import com.pradera.negotiations.mcnoperations.to.SearchMCNOperationsTO;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.negotiations.positioncommission.to.PositionParticipantTO;
import com.pradera.negotiations.positioncommission.to.SearchPositionCommissionTO;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;
import com.pradera.settlements.core.to.NetParticipantPositionTO;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;
import com.pradera.settlements.core.to.NetSettlementOperationTO;
import com.pradera.settlements.prepaidextended.to.RegisterPrepaidExtendedTO;
import com.pradera.settlements.utils.NegotiationUtils;

// TODO: Auto-generated Javadoc
/**
* The Class McnManualServiceBean.
* @author PraderaTechnologies.
*/
@Stateless
public class McnOperationServiceBean extends CrudDaoServiceBean implements Serializable {
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(McnOperationServiceBean.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The negotiation operation service bean. */
	@EJB
	NegotiationOperationServiceBean negotiationOperationServiceBean;
	
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The notification service. */
	@EJB
	private NotificationServiceBean notificationService;
	
	/** The settlement process service bean. */
	@EJB
	SettlementProcessService settlementProcessServiceBean;
	
	/** The settlements component singleton. */
	@EJB
	SettlementsComponentSingleton settlementsComponentSingleton;
	
	/**
	 * Gets the security operation params.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the security operation params
	 */
	public Object[] getSecurityOperationParams(Long idMechanismOperation){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select (select pt.text1 from ParameterTable pt where pt.parameterTablePk = mo.securities.securityClass ), mo.securities.idSecurityCodeOnly "); 
		sbQuery.append(" from MechanismOperation mo ");
		sbQuery.append(" where mo.idMechanismOperationPk = :idMechanismOperation ");
		
		parameters.put("idMechanismOperation",idMechanismOperation );
		
		return (Object[]) findObjectByQueryString(sbQuery.toString(),parameters);
	}
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @param ballotNumber the ballot number
	 * @param sequential the sequential
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation(Long ballotNumber, Long sequential){
		StringBuffer sbQuery = new StringBuffer();
		MechanismOperation mechanismOperation = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select mo "); 
		sbQuery.append(" from MechanismOperation mo ");
		sbQuery.append(" inner join fetch mo.sellerParticipant ");
		sbQuery.append(" inner join fetch mo.buyerParticipant ");
		sbQuery.append(" inner join fetch mo.mechanisnModality mm ");
		sbQuery.append(" inner join fetch mm.negotiationMechanism nme ");
		sbQuery.append(" inner join fetch mm.negotiationModality nmo ");
		//sbQuery.append(" where trunc(mo.operationDate) = trunc(:operationDate) ");
		sbQuery.append(" where mo.ballotNumber = :ballotNumber ");
		sbQuery.append(" and mo.sequential = :sequential ");
		sbQuery.append(" and mo.operationState in :states ");
		
		List<Integer> states = new ArrayList<Integer>();
		states.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		parameters.put("states",states );
		parameters.put("ballotNumber", ballotNumber);
		parameters.put("sequential", sequential);
		
		try{
			mechanismOperation = (MechanismOperation) findObjectByQueryString(sbQuery.toString(), parameters);
		}catch(NoResultException ne){
			logger.error("MechanismOperation not found with parameters ballot:"+ballotNumber+",sequential:"+sequential);
		}
		return mechanismOperation; 
	}

	/**
	 * Generates unique number for Number Operation field in Manual Registration.
	 *
	 * @param operationDate the operation date
	 * @param mechaninmId the mechaninm id
	 * @param modalityId the modality id
	 * @return Long
	 */
	public Long getMcnUniqueNumber(Date operationDate,Long mechaninmId,Long modalityId) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select nvl(max(mech.operationNumber),0)+1 "); 
		sbQuery.append(" from MechanismOperation mech ");
		sbQuery.append(" where 1 = 1 ");
		sbQuery.append(" and trunc(mech.operationDate) = trunc(:oprDate) ");
		sbQuery.append(" and mech.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = :negoMech ");
		sbQuery.append(" and mech.mechanisnModality.negotiationModality.idNegotiationModalityPk = :negoModa ");
		
		parameters.put("oprDate", operationDate);
		parameters.put("negoMech", mechaninmId);
		parameters.put("negoModa", modalityId);
		return (Long) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * This method checks Operation Number Mechanism exists for particular mechanism in MECHANISM_OPERATION table
	 * If number already exists, return true, .
	 *
	 * @param mechanism the mechanism
	 * @param referenceNumber the reference number
	 * @param referenceDate the reference date
	 * @return boolean
	 */
	public boolean chkOperationNumberMechanism(Long mechanism,String referenceNumber){
		StringBuffer sbQuery = new StringBuffer();	
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select nvl(count(mo.idMechanismOperationPk),0) ");
		sbQuery.append(" from MechanismOperation mo ");
		sbQuery.append(" where mo.mechanisnModality.id.idNegotiationMechanismPk = :mechanismId and mo.referenceNumber = :referNumber ");
		sbQuery.append(" and mo.operationState not in :states ");
		parameters.put("mechanismId", mechanism);
		parameters.put("referNumber", referenceNumber);
		List<Integer> states = new ArrayList<Integer>();
		states.add(MechanismOperationStateType.CANCELED_STATE.getCode());
		states.add(OtcOperationStateType.CANCELED.getCode());
		states.add(OtcOperationStateType.REJECTED.getCode());
		states.add(OtcOperationStateType.ANULATE.getCode());
		parameters.put("states",states );
		Long count = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
		if(count > 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Chk operation ballot sequential.
	 *
	 * @param ballotNumber the ballot number
	 * @param sequential the sequential
	 * @return true, if successful
	 */
	public boolean chkOperationBallotSequential(Long ballotNumber, Long sequential) {
		StringBuffer sbQuery = new StringBuffer();	
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select nvl(count(mo.idMechanismOperationPk),0) ");
		sbQuery.append(" from MechanismOperation mo ");
		sbQuery.append(" where ");
		sbQuery.append(" mo.ballotNumber  = :ballotNumber ");
		sbQuery.append(" and mo.sequential =  :sequential ");

//		sbQuery.append(" and mo.operationState not in :states ");  // No importa el estado, una vez registrada la papeleta + su secuencial no es posible solapar esa operacion issue 0000050
		//parameters.put("mechanismId", mechanism);
		parameters.put("ballotNumber", ballotNumber);
		parameters.put("sequential", sequential);
//		List<Integer> states = new ArrayList<Integer>();
//		states.add(MechanismOperationStateType.CANCELED_STATE.getCode());
//		states.add(OtcOperationStateType.CANCELED.getCode());
//		states.add(OtcOperationStateType.REJECTED.getCode());
//		states.add(OtcOperationStateType.ANULATE.getCode());
//		parameters.put("states",states );
		Long count = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
		if(count > 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 *  
	 * This method gets MechanismModality object .
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityCode the modality code
	 * @param instrumentType the instrument type
	 * @param indPrimaryPlacement the ind primary placement
	 * @return MechanismModality
	 */
	public MechanismModality getMechanismModality(Long mechanismId,String modalityCode,Integer instrumentType, String sellFlagStr){
		List<Long> lstSaleModalities = new ArrayList<Long>();
		lstSaleModalities.add(NegotiationModalityType.SALE_EQUITIES.getCode());
		lstSaleModalities.add(NegotiationModalityType.SALE_FIXED_INCOME.getCode());
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
	    sbQuery.append("Select mm from MechanismModality mm   ");
	    sbQuery.append(" inner join fetch mm.negotiationMechanism nme  ");
	    sbQuery.append(" inner join fetch mm.negotiationModality nmo  ");
	    sbQuery.append(" where mm.id.idNegotiationMechanismPk = :MechanismPk ");
		parameters.put("MechanismPk", mechanismId);
		if(Validations.validateIsNotNull(instrumentType)){
			sbQuery.append(" and nmo.instrumentType = :instrumentType ");
			parameters.put("instrumentType", instrumentType);
		}
		if(Validations.validateIsNotNull(modalityCode)){
			sbQuery.append(" and mm.massiveModalityName = :modalityCode ");
			parameters.put("modalityCode", modalityCode);
		}
		if(sellFlagStr.trim().toUpperCase().equals(BooleanType.YES.getValue())) {
			sbQuery.append(" and nmo.idNegotiationModalityPk in :lstSaleModalities ");
			parameters.put("lstSaleModalities", lstSaleModalities);			
		} else {
			sbQuery.append(" and nmo.idNegotiationModalityPk not in :lstSaleModalities ");
			parameters.put("lstSaleModalities", lstSaleModalities);
		}
		return (MechanismModality)findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
		
	/**
	 * Gets the mcn processed files information.
	 *
	 * @param parameters the parameters
	 * @return the mcn processed files information
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getMcnProcessedFilesInformation(Map<String, Object> parameters){
		List<Object[]> objects = new ArrayList<Object[]>();
		StringBuffer sbQuery = new StringBuffer();
		
		sbQuery.append("select file.idMcnProcessFilePk, ");//0
		sbQuery.append("       mech.idNegotiationMechanismPk, ");//1
		sbQuery.append("       mech.description, ");//2
		sbQuery.append("       mech.mechanismName, ");//3
		sbQuery.append("       file.fileName, ");//4
		sbQuery.append("       file.indAutomacticProcess, ");//5
		sbQuery.append("       file.processDate, ");//6
		sbQuery.append("       file.processType, ");//7
		sbQuery.append("       file.acceptedOperations, ");//8
		sbQuery.append("       file.rejectedOperations, ");//9
		sbQuery.append("       file.totalOperations, ");//10
		sbQuery.append("       file.sequenceNumber, ");//11
		sbQuery.append("       (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = file.processState ) ");//12
		sbQuery.append(" from McnProcessFile file inner join file.negotiationMechanism mech ");
		sbQuery.append(" where 1 = 1");
		if(Validations.validateIsNotNullAndPositive((Long)parameters.get("idNegotiationMechanismPk"))){
			sbQuery.append(" and mech.idNegotiationMechanismPk = :idNegotiationMechanismPk ");
		} else{
			parameters.remove("idNegotiationMechanismPk");
		}
		if(Validations.validateIsNotNull((Integer)parameters.get("processType"))){
			sbQuery.append(" and file.processType = :processType ");
		} else{
			parameters.remove("processType");
		}
		if(Validations.validateIsNotNull((Long)parameters.get("idParticipantPk"))){
			sbQuery.append(" and file.participant.idParticipantPk = :idParticipantPk ");
		} else{
			parameters.remove("idParticipantPk");
		}
		sbQuery.append(" and trunc(file.processDate) = :processDate ");
		sbQuery.append(" order by file.processDate desc ");
		
		objects = findListByQueryString(sbQuery.toString(), parameters);
		
		return objects;
	}
 
	
	/**
	 * Next mcn file sequence number.
	 *
	 * @param processDate the process date
	 * @param idParticipantPk the id participant pk
	 * @param processType the process type
	 * @return the long
	 */
	public Long nextMcnFileSequenceNumber(Date processDate,Long idParticipantPk, Integer processType) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select nvl(max(f.sequenceNumber),0) from McnProcessFile f ");
		sbQuery.append(" where trunc(f.processDate) = trunc(:processDate)  ");
		sbQuery.append(" and f.processType = :processType  ");
//		if(idParticipantPk!=null){
//			sbQuery.append(" and f.participant.idParticipantPk = :idParticipantPk  ");
//			parameters.put("idParticipantPk",idParticipantPk);
//		}
		parameters.put("processDate",processDate);
		parameters.put("processType",processType);
		Long sq = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
		sq++;
		return sq;
	}
	
	/**
	 * Gets the processed mcn file content.
	 *
	 * @param idProcessFilePk the id process file pk
	 * @return the processed mcn file content
	 */
	public byte[] getProcessedMcnFileContent(Long idProcessFilePk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select mpf.processedFile ");
		sbQuery.append(" from McnProcessFile mpf where mpf.idMcnProcessFilePk = :idMcnProcessFilePk ");
		parameters.put("idMcnProcessFilePk", idProcessFilePk);
		return (byte[])findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the accepted mcn file content.
	 *
	 * @param idProcessFilePk the id process file pk
	 * @return the accepted mcn file content
	 */
	public byte[] getAcceptedMcnFileContent(Long idProcessFilePk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select mpf.acceptedFile ");
		sbQuery.append(" from McnProcessFile mpf where mpf.idMcnProcessFilePk = :idMcnProcessFilePk ");
		parameters.put("idMcnProcessFilePk", idProcessFilePk);
		return (byte[])findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the rejected mcn file content.
	 *
	 * @param idProcessFilePk the id process file pk
	 * @return the rejected mcn file content
	 */
	public byte[] getRejectedMcnFileContent(Long idProcessFilePk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select mpf.rejectedFile ");
		sbQuery.append(" from McnProcessFile mpf where mpf.idMcnProcessFilePk = :idMcnProcessFilePk ");
		parameters.put("idMcnProcessFilePk", idProcessFilePk);
		return (byte[])findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @param indMcnProcess the ind mcn process
	 * @return the lst negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getLstNegotiationMechanism(Integer indMcnProcess) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT nm FROM NegotiationMechanism nm");
		sbQuery.append("	WHERE nm.stateMechanism = :state");
		if(indMcnProcess!=null){
			sbQuery.append("	AND nm.indMcnProcess = :indMcn");
			parameters.put("indMcn", indMcnProcess); 
		}
		sbQuery.append("	ORDER BY nm.mechanismName ASC");
		parameters.put("state", NegotiationMechanismStateType.ACTIVE.getCode());
		return findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the list negotiation modality.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the list negotiation modality
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getListNegotiationModality(Long idNegotiationMechanismPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mm.negotiationModality FROM MechanismModality mm");
		sbQuery.append("	WHERE mm.stateMechanismModality = :state");
		sbQuery.append("	AND mm.negotiationModality.modalityState = :modalityState");
		sbQuery.append("	AND mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	ORDER BY mm.negotiationModality.modalityName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", MechanismModalityStateType.ACTIVE.getCode());
		query.setParameter("modalityState", NegotiationModalityStateType.ACTIVE.getCode());
		query.setParameter("idNegotiationMechanismPk", idNegotiationMechanismPk);
		return (List<NegotiationModality>)query.getResultList();
	}
	
	/**
	 * Gets the list negotiation modality.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the list negotiation modality
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getListNegotiationModalityReport(Long idNegotiationMechanismPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mm.negotiationModality FROM MechanismModality mm");
		sbQuery.append("	WHERE mm.stateMechanismModality = :state");
		sbQuery.append("	AND mm.negotiationModality.modalityState = :modalityState");
		sbQuery.append("	AND mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	AND mm.negotiationModality.idNegotiationModalityPk in (3,4)");
		sbQuery.append("	ORDER BY mm.negotiationModality.modalityName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", MechanismModalityStateType.ACTIVE.getCode());
		query.setParameter("modalityState", NegotiationModalityStateType.ACTIVE.getCode());
		query.setParameter("idNegotiationMechanismPk", idNegotiationMechanismPk);
		return (List<NegotiationModality>)query.getResultList();
	}
	
	/**
	 * Issuer list. //issue 825
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Issuer> issuerList() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("	Select su	");		
		sbQuery.append("	FROM Issuer su	");
		sbQuery.append("	where su.stateIssuer=:stateIs	");
		sbQuery.append("	order by su.mnemonic	");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("stateIs", IssuerStateType.REGISTERED.getCode());
		return (List<Issuer>) query.getResultList();

	}
	/**
	 * Search mechanism operations.
	 *
	 * @param searchMCNOperationsTO the search mcn operations to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> searchMechanismOperations(SearchMCNOperationsTO searchMCNOperationsTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT ");
		sbQuery.append("	nme.idNegotiationMechanismPk, nme.mechanismName, "); // 0 1 
		sbQuery.append("	nmo.idNegotiationModalityPk, nmo.modalityName, "); // 2 3 
		sbQuery.append("	mo.idMechanismOperationPk, mo.operationNumber,  "); // 4 5 
		sbQuery.append("	mo.operationDate, mo.ballotNumber, mo.sequential,  ");// 67 8
		sbQuery.append("	mo.securities.idSecurityCodePk, ");//9
		sbQuery.append("	mo.stockQuantity, ");//10
		sbQuery.append("	mo.cashSettlementDate, mo.termSettlementDate, ");//11 12
		sbQuery.append("	mo.buyerParticipant.idParticipantPk, mo.buyerParticipant.mnemonic, ");//13 14
		sbQuery.append(" 	mo.sellerParticipant.idParticipantPk, mo.sellerParticipant.mnemonic, "); // 15 16 
		sbQuery.append(" 	mo.operationState, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.operationState ), ");// 17 18
		sbQuery.append(" 	mo.mcnProcessFile.idMcnProcessFilePk, ");// 19
		sbQuery.append(" 	(select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.currency ), ");// 20
		sbQuery.append(" 	(select count(so) from SettlementOperation so where so.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk and so.indForcedPurchase = :indForcedPurchase), ");// 21
		sbQuery.append("    mo.termSettlementDays, mo.amountRate, mo.cashSettlementPrice ");//22 23 24
		sbQuery.append("	FROM MechanismOperation mo");
		sbQuery.append("	inner join mo.mechanisnModality mm");
		sbQuery.append("	inner join mm.negotiationMechanism nme");
		sbQuery.append("	inner join mm.negotiationModality nmo");
		sbQuery.append("	WHERE mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		if(ComponentConstant.ZERO.equals(searchMCNOperationsTO.getDateTypeSelected())){
			sbQuery.append("	AND TRUNC(mo.registerDate) BETWEEN :initialDate AND :endDate");
		} else if(ComponentConstant.ONE.equals(searchMCNOperationsTO.getDateTypeSelected())){
			sbQuery.append("	AND TRUNC(mo.cashSettlementDate) between :initialDate and :endDate");
		}else{
			sbQuery.append("	AND TRUNC(mo.termSettlementDate) between :initialDate and :endDate");
		}
		if(Validations.validateIsNotNullAndPositive(searchMCNOperationsTO.getNegotiationModalitySelected())){
			sbQuery.append("	AND mm.id.idNegotiationModalityPk = :idNegotiationModalityPk");
			parameters.put("idNegotiationModalityPk", searchMCNOperationsTO.getNegotiationModalitySelected());
		}
		if(Validations.validateIsNotNullAndPositive(searchMCNOperationsTO.getStateSelected())){
			sbQuery.append("	AND mo.operationState = :operationState");
			parameters.put("operationState", searchMCNOperationsTO.getStateSelected());
		}
		if(Validations.validateIsNotNullAndPositive(searchMCNOperationsTO.getOperationNumber())){
			sbQuery.append("	AND mo.operationNumber = :operationNumber");
			parameters.put("operationNumber", searchMCNOperationsTO.getOperationNumber());
		}
		if(Validations.validateIsNotNull(searchMCNOperationsTO.getBallotNumber())){
			sbQuery.append("	AND mo.ballotNumber = :ballotNumber");
			parameters.put("ballotNumber", searchMCNOperationsTO.getBallotNumber());
		}
		if(Validations.validateIsNotNull(searchMCNOperationsTO.getSequential())){
			sbQuery.append("	AND mo.sequential = :sequential");
			parameters.put("sequential", searchMCNOperationsTO.getSequential());
		}
		if(Validations.validateIsNotNull(searchMCNOperationsTO.getCurrencySelected())){
			sbQuery.append("	AND mo.currency = :currency");
			parameters.put("currency", searchMCNOperationsTO.getCurrencySelected());
		}
		sbQuery.append("	ORDER BY mm.negotiationModality.modalityName, mo.operationNumber DESC");
		parameters.put("indForcedPurchase", BooleanType.YES.getCode());
		parameters.put("initialDate", searchMCNOperationsTO.getInitialDate());
		parameters.put("endDate", searchMCNOperationsTO.getEndDate());
		parameters.put("idNegotiationMechanismPk", searchMCNOperationsTO.getNegotiationMechanismSelected());

		List<Object[]> operationObjects = findListByQueryString(sbQuery.toString(), parameters);
		List<MechanismOperationTO> mechanismOperationTOs = new ArrayList<MechanismOperationTO>();
		for (Object[] objOperation : operationObjects) {
			MechanismOperationTO mechanismOperationTO = NegotiationUtils.populateSearchOperations(objOperation);
			mechanismOperationTOs.add(mechanismOperationTO);
		}
		return mechanismOperationTOs;
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param saleRole the sale role
	 * @param purchaseRole the purchase role
	 * @param idParticipant the id participant
	 * @return the lst participants
	 */
	@SuppressWarnings("unchecked")
	public Object getLstParticipants(Long mechanismId, Long modalityId, Integer saleRole, Integer purchaseRole, Long idParticipant, Integer indDepositary){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT p FROM ParticipantMechanism pm inner join pm.participant p ");
		sbQuery.append("	WHERE pm.stateParticipantMechanism = :state");
		sbQuery.append("	AND pm.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	AND pm.mechanismModality.id.idNegotiationModalityPk = :idNegotiationModalityPk");
		sbQuery.append("	AND pm.mechanismModality.stateMechanismModality = :stateMechanismModality");	
		sbQuery.append("	AND p.state = :participantState");
		
		if(mechanismId!=null && !mechanismId.equals(NegotiationMechanismType.SIRTEX.getCode())){
			sbQuery.append("	AND p.accountType = :accountType ");
		}
		
		if(ComponentConstant.ONE.equals(saleRole)){
			sbQuery.append("	AND pm.indSaleRole = :saleRole ");
			parameters.put("saleRole", saleRole);
		}
		if(ComponentConstant.ONE.equals(purchaseRole)){
			sbQuery.append("	AND pm.indPurchaseRole = :purchaseRole ");
			parameters.put("purchaseRole", purchaseRole);
		}
		if(idParticipant!=null){
			sbQuery.append("	AND p.idParticipantPk = :idParticipant ");
			parameters.put("idParticipant", idParticipant);
		}

		if(indDepositary!=null){
			sbQuery.append("	AND p.indDepositary = :indDepositary ");
			parameters.put("indDepositary", indDepositary);
		}
		
		sbQuery.append("	ORDER BY p.mnemonic ASC");
		
		if(mechanismId!=null && !mechanismId.equals(NegotiationMechanismType.SIRTEX.getCode())){
			parameters.put("accountType", ParticipantType.DIRECT.getCode());
		}
		
		parameters.put("stateMechanismModality", MechanismModalityStateType.ACTIVE.getCode());
		parameters.put("state", ParticipantMechanismStateType.REGISTERED.getCode());
		parameters.put("participantState", ParticipantStateType.REGISTERED.getCode());
		parameters.put("idNegotiationMechanismPk", mechanismId);
		parameters.put("idNegotiationModalityPk", modalityId);
		
		if(idParticipant!=null){
			return (Participant)findObjectByQueryString(sbQuery.toString(),parameters);
		}else{
			return (List<Participant>)findListByQueryString(sbQuery.toString(),parameters);
		}
		
	}

	/**
	 * Gets the list mechanism modality.
	 *
	 * @return the list mechanism modality
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismModality> getListMechanismModality() {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mm FROM MechanismModality mm");
		sbQuery.append("	WHERE mm.stateMechanismModality = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", MechanismModalityStateType.ACTIVE.getCode());
		return (List<MechanismModality>)query.getResultList();
	}

	/**
	 * Find issuance and issuer.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuance
	 */
	public Issuance findIssuanceAndIssuer(String idSecurityCodePk){
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ice FROM Issuance ice ");
			sbQuery.append("	inner join fetch ice.issuer ");
			sbQuery.append("    inner join ice.securities sec ");
			sbQuery.append("	WHERE sec.idSecurityCodePk = :idSecurityCodePk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			return (Issuance)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Validate security nego mechanism.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean validateSecurityNegoMechanism(String idSecurityCodePk, MechanismModalityPK id) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT COUNT(snm) FROM SecurityNegotiationMechanism snm");
		sbQuery.append("	WHERE snm.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND snm.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	AND snm.mechanismModality.id.idNegotiationModalityPk = :idNegotiationModalityPk");
		sbQuery.append("	AND snm.securityNegotationState = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idNegotiationMechanismPk", id.getIdNegotiationMechanismPk());
		query.setParameter("idNegotiationModalityPk", id.getIdNegotiationModalityPk());
		query.setParameter("state", BooleanType.YES.getCode());
		Integer count = new Integer(query.getSingleResult().toString());
		if(GeneralConstants.ZERO_VALUE_INTEGER.equals(count))
			return false;
		return true;
	}

	/**
	 * Find operation repo.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @return the mechanism operation
	 */
	public MechanismOperation findOperationRepo(Long mechanismId , Long modalityId, Date operationDate, Long operationNumber) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT mo");
			sbQuery.append("	FROM MechanismOperation mo");
			sbQuery.append("	INNER JOIN FETCH mo.buyerParticipant");
			sbQuery.append("	INNER JOIN FETCH mo.sellerParticipant");
			sbQuery.append("	INNER JOIN FETCH mo.securities se");
			sbQuery.append("	INNER JOIN FETCH mo.mechanisnModality");
			sbQuery.append("	INNER JOIN FETCH se.issuance");
			sbQuery.append("	INNER JOIN FETCH se.issuer");
			sbQuery.append("	WHERE mo.mechanisnModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
			sbQuery.append("	AND mo.mechanisnModality.id.idNegotiationModalityPk in ( ");
			sbQuery.append("	    select parentModality.referenceModality.idNegotiationModalityPk from NegotiationModality parentModality where  ");
			sbQuery.append("	 	parentModality.idNegotiationModalityPk = :idNegotiationModalityPk ");
			sbQuery.append("	)  ");
			sbQuery.append("	AND mo.operationNumber = :operationNumber");
			sbQuery.append("	AND TRUNC(mo.operationDate) = TRUNC(:operationDate)");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idNegotiationMechanismPk", mechanismId);
			query.setParameter("idNegotiationModalityPk", modalityId);
			query.setParameter("operationDate", operationDate);
			query.setParameter("operationNumber", operationNumber);
			return (MechanismOperation)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the list negotiation modality.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @param inCharge the in charge
	 * @return the list negotiation modality
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getListNegotiationModality(Long idNegotiationMechanismPk, Integer inCharge) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT nm");
		sbQuery.append("	FROM NegotiationModality nm");
		sbQuery.append("	WHERE nm.idNegotiationModalityPk IN");
		sbQuery.append("	(SELECT mm.id.idNegotiationModalityPk");
		sbQuery.append("		FROM MechanismModality mm");
		sbQuery.append("		WHERE mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("		AND mm.stateMechanismModality = :stateMechanismModality");
		if(inCharge!=null){
			sbQuery.append("		and mm.indIncharge = :inCharge ");
			parameters.put("inCharge", inCharge);
		}
		sbQuery.append("		GROUP BY mm.id.idNegotiationModalityPk)");
		sbQuery.append("	AND nm.modalityState = :modalityState");
		sbQuery.append("	ORDER BY nm.modalityName ASC");
		
		parameters.put("modalityState", NegotiationModalityStateType.ACTIVE.getCode());
		parameters.put("stateMechanismModality", MechanismModalityStateType.ACTIVE.getCode());
		parameters.put("idNegotiationMechanismPk", idNegotiationMechanismPk);
		return (List<NegotiationModality>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the lst negotiation modality for participant.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @param idParticipantPk the id participant pk
	 * @param inCharge the in charge
	 * @return the lst negotiation modality for participant
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getLstNegotiationModalityForParticipant(Long idNegotiationMechanismPk, Long idParticipantPk, Integer inCharge) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT nm");
		sbQuery.append("	FROM NegotiationModality nm");
		sbQuery.append("	WHERE nm.idNegotiationModalityPk IN");
		sbQuery.append("	(SELECT pm.mechanismModality.id.idNegotiationModalityPk");
		sbQuery.append("		FROM ParticipantMechanism pm");
		sbQuery.append("		WHERE pm.participant.idParticipantPk = :idParticipantPk");
		if(inCharge!=null){
			sbQuery.append("		and pm.mechanismModality.indIncharge = :inCharge ");
			parameters.put("inCharge", inCharge);
		}
		sbQuery.append("		AND pm.stateParticipantMechanism = :stateParticipantMechanism");
		sbQuery.append("		AND pm.mechanismModality.stateMechanismModality = :stateMechanismModality");
		sbQuery.append("		AND pm.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("		GROUP BY pm.mechanismModality.id.idNegotiationModalityPk) ");
		sbQuery.append(					"	AND nm.modalityState = :modalityState");
		sbQuery.append("	ORDER BY nm.modalityName ASC");
		
		parameters.put("idParticipantPk", idParticipantPk);
		parameters.put("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		parameters.put("modalityState", NegotiationModalityStateType.ACTIVE.getCode());
		parameters.put("stateMechanismModality", MechanismModalityStateType.ACTIVE.getCode());
		parameters.put("idNegotiationMechanismPk", idNegotiationMechanismPk);
		return (List<NegotiationModality>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the lst negotiation mechanism by participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the lst negotiation mechanism by participant
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getLstNegotiationMechanismByParticipant(Long idParticipantPk) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT nm");
		sbQuery.append("	FROM NegotiationMechanism nm");
		sbQuery.append("	WHERE nm.idNegotiationMechanismPk IN");
		sbQuery.append("	(SELECT pm.mechanismModality.id.idNegotiationMechanismPk");
		sbQuery.append("		FROM ParticipantMechanism pm");
		sbQuery.append("		WHERE pm.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("		AND pm.stateParticipantMechanism = :stateParticipantMechanism");
		sbQuery.append("		AND pm.mechanismModality.stateMechanismModality = :stateMechanismModality");							
		sbQuery.append("		GROUP BY pm.mechanismModality.id.idNegotiationMechanismPk) ");
		sbQuery.append("	AND nm.stateMechanism = :stateMechanism");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idParticipantPk", idParticipantPk);
		parameters.put("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		parameters.put("stateMechanismModality", MechanismModalityStateType.ACTIVE.getCode());
		parameters.put("stateMechanism", NegotiationMechanismStateType.ACTIVE.getCode());
		return (List<NegotiationMechanism>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @param accountType the account type
	 * @return the lst participants
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getLstParticipants(Integer accountType) throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT p");
		sbQuery.append("	FROM Participant p");
		sbQuery.append("	WHERE p.state = :state");
		if(accountType!=null){
			sbQuery.append("	and p.accountType = :accountType");	
		}		
		sbQuery.append("	ORDER BY p.mnemonic ASC");
		if(accountType!=null){
			parameters.put("accountType", accountType);
		}		
		parameters.put("state", ParticipantStateType.REGISTERED.getCode());
		return (List<Participant>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the participant object.
	 *
	 * @param idParticipantCode the id participant code
	 * @return the participant object
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipantObject(Long idParticipantCode) throws ServiceException{
		Participant participant = find(Participant.class,idParticipantCode);
		return participant;
	}
	
	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public Boolean searchOperationChained(Long idMechanismOperationPk, Date date) {

		Boolean result = false;
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("   select MO.ID_MECHANISM_OPERATION_PK                                                                   	 	     ");
		querySql.append("   from CHAINED_HOLDER_OPERATION CHO                                                                                ");
		querySql.append("   inner join HOLDER_CHAIN_DETAIL HCD on HCD.ID_CHAINED_HOLDER_OPERATION_FK = CHO.ID_CHAINED_HOLDER_OPERATION_PK    ");
		querySql.append("   inner join SETTLEMENT_ACCOUNT_MARKETFACT SAM on HCD.ID_ACCOUNT_MARKETFACT_FK=SAM.ID_ACCOUNT_MARKETFACT_PK        ");
		querySql.append("   inner join SETTLEMENT_ACCOUNT_OPERATION SAO on SAM.ID_SETTLEMENT_ACCOUNT_FK=SAO.ID_SETTLEMENT_ACCOUNT_PK         ");
		querySql.append("   inner join SETTLEMENT_OPERATION SO on SAO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK               ");
		querySql.append("   inner join MECHANISM_OPERATION MO on SO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK                   ");
		querySql.append("   where 1 = 1                                                                                                      ");
		querySql.append("   and trunc(CHO.REGISTER_DATE) = :date							                                                 ");
		querySql.append("   and CHO.CHAIN_STATE = 1959                                                                                       ");
		querySql.append("   AND MO.ID_MECHANISM_OPERATION_PK = :idMechanismOperationPk                                                       ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("date", date);
		@SuppressWarnings("unchecked")
		List<Object[]> lstRestul = query.getResultList();
		
		if (lstRestul.size()>0){
			result = true;
		}
		
		return result;
	
	}
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> searchOperations(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT distinct ");
		sbQuery.append("	nme.idNegotiationMechanismPk, nme.mechanismName, "); // 0 1 
		sbQuery.append("	nmo.idNegotiationModalityPk, nmo.modalityName, "); // 2 3 
		sbQuery.append("	mo.idMechanismOperationPk, mo.operationNumber,  "); // 4 5 
		sbQuery.append("	mo.operationDate, mo.ballotNumber, mo.sequential,  ");// 6 7 8
		sbQuery.append("	mo.securities.idSecurityCodePk, ");//9
		sbQuery.append("	mo.stockQuantity, ");//10
		sbQuery.append("	mo.cashSettlementDate, mo.termSettlementDate, ");//11 12
		sbQuery.append("	mo.buyerParticipant.idParticipantPk, mo.buyerParticipant.mnemonic, ");//13 14
		sbQuery.append(" 	mo.sellerParticipant.idParticipantPk, mo.sellerParticipant.mnemonic, "); // 15 16 
		sbQuery.append(" 	mo.operationState, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.operationState ), ");// 17 18
		sbQuery.append(" 	mo.mcnProcessFile.idMcnProcessFilePk, ");// 19
		sbQuery.append(" 	(select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.currency ), ");// 20
		sbQuery.append(" 	(select count(so) from SettlementOperation so where so.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk and so.indForcedPurchase = :indForcedPurchase), ");// 21
		sbQuery.append("    mo.termSettlementDays, mo.amountRate, mo.cashSettlementPrice ");//22 23 24
		sbQuery.append("	FROM MechanismOperation mo");
		sbQuery.append("	inner join mo.mechanisnModality mm");
		sbQuery.append("	inner join mm.negotiationMechanism nme");
		sbQuery.append("	inner join mm.negotiationModality nmo");
		sbQuery.append("	WHERE mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		
		//issue 1044 adicoion de filtros por CUI y HolderAccount
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolder()))
			if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolder().getIdHolderPk())){
				sbQuery.append(" 	and :cuiParameter in (");
				sbQuery.append(" 		select had.holder.idHolderPk");
				sbQuery.append(" 		from  HolderAccountDetail had, HolderAccountOperation hao");
				sbQuery.append(" 		where hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk");
				sbQuery.append(" 		and had.holderAccount.idHolderAccountPk = hao.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		)");
				parameters.put("cuiParameter", searchOperationTO.getHolder().getIdHolderPk());
			}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolderAccount()))
			if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolderAccount().getIdHolderAccountPk())){
				sbQuery.append(" 	and :hAccountParameter in (");
				sbQuery.append(" 		select had.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		from  HolderAccountDetail had, HolderAccountOperation hao");
				sbQuery.append(" 		where hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk");
				sbQuery.append(" 		and had.holderAccount.idHolderAccountPk = hao.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		)");
				parameters.put("hAccountParameter", searchOperationTO.getHolderAccount().getIdHolderAccountPk());
			}
		
		if(searchOperationTO.getNegotiationModalitySelected()!=null){
			sbQuery.append("	and mm.id.idNegotiationModalityPk = :idNegotiationModalityPk");
			parameters.put("idNegotiationModalityPk", searchOperationTO.getNegotiationModalitySelected());
		}
		
		
		parameters.put("indForcedPurchase", BooleanType.YES.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getParticipantSelected())){
			sbQuery.append("	AND (mo.sellerParticipant.idParticipantPk = :idParticipantPk ");
			sbQuery.append("		OR mo.buyerParticipant.idParticipantPk = :idParticipantPk)");
//			sbQuery.append("	OR (hao.inchargeFundsParticipant.idParticipantPk = :idParticipantPk");
//			sbQuery.append("		OR hao.inchargeStockParticipant.idParticipantPk = :idParticipantPk) )");
			parameters.put("idParticipantPk", searchOperationTO.getParticipantSelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getStateSelected())){
			sbQuery.append("	AND mo.operationState = :state");
			parameters.put("state", searchOperationTO.getStateSelected());
		}else{
			sbQuery.append("	AND mo.operationState not in (:stateOTC)");
			List<Integer> lstStateOTC = new ArrayList<Integer>();
			lstStateOTC.add(OtcOperationStateType.ANULATE.getCode());
			lstStateOTC.add(OtcOperationStateType.APROVED.getCode());
			lstStateOTC.add(OtcOperationStateType.CANCELED.getCode());
			lstStateOTC.add(OtcOperationStateType.CONFIRMED.getCode());
			lstStateOTC.add(OtcOperationStateType.REGISTERED.getCode());
			lstStateOTC.add(OtcOperationStateType.REJECTED.getCode());
			lstStateOTC.add(OtcOperationStateType.REVIEWED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REGISTERED.getCode());
			lstStateOTC.add(SirtexOperationStateType.ANULATE.getCode());
			lstStateOTC.add(SirtexOperationStateType.APROVED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REJECTED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REVIEWED.getCode());
			lstStateOTC.add(SirtexOperationStateType.CONFIRMED.getCode());
			parameters.put("stateOTC", lstStateOTC);
		}
			
		if(GeneralConstants.ZERO_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){//OPERATION DATE
			sbQuery.append("	AND TRUNC(mo.operationDate) between :initialDate and :endDate");
		}else if(GeneralConstants.ONE_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){//CASH SETTLEMENT DATE
			sbQuery.append("	AND TRUNC(mo.cashSettlementDate) between :initialDate and :endDate ");
		}else if(GeneralConstants.TWO_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){	//TERM SETTLEMENT DATE
			sbQuery.append("	AND TRUNC(mo.termSettlementDate)  between :initialDate and :endDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getOperationNumber())){
			sbQuery.append("	AND mo.operationNumber = :operationNumber");
			parameters.put("operationNumber", searchOperationTO.getOperationNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getBallotNumber())){
			sbQuery.append("	AND mo.ballotNumber = :ballotNumber");
			parameters.put("ballotNumber", searchOperationTO.getBallotNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getCurrencySelected())){
			sbQuery.append("	AND mo.currency = :currency");
			parameters.put("currency", searchOperationTO.getCurrencySelected());
		}
		//issue 825
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdSecurityCodePk())){
			sbQuery.append("	AND mo.securities.idSecurityCodePk = :idSecurityCodePk");
			parameters.put("idSecurityCodePk", searchOperationTO.getIdSecurityCodePk());
		}
		
		// issue 1044 adicon de filtro por clase de valor
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSecurityClassSelected())){
			sbQuery.append("	AND mo.securities.securityClass = :securityClass");
			parameters.put("securityClass", searchOperationTO.getSecurityClassSelected());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdIssuerPk())){
			sbQuery.append("	AND mo.securities.issuer.idIssuerPk = :idIssuerPk");
			parameters.put("idIssuerPk", searchOperationTO.getIdIssuerPk());
		}
			
		sbQuery.append("	ORDER BY mo.operationDate, mo.operationNumber DESC");
		
		parameters.put("idNegotiationMechanismPk", searchOperationTO.getNegotiationMechanismSelected());
		parameters.put("initialDate", searchOperationTO.getInitialDate());
		parameters.put("endDate", searchOperationTO.getEndDate()); 


		List<Object[]> operationObjects = findListByQueryString(sbQuery.toString(), parameters);
		List<MechanismOperationTO> mechanismOperationTOs = new ArrayList<MechanismOperationTO>();
		Object[] objRes;
		BigDecimal stockQuantity;
		for (Object[] objOperation : operationObjects) {
			MechanismOperationTO mechanismOperationTO = NegotiationUtils.populateSearchOperations(objOperation);
			stockQuantity=mechanismOperationTO.getStockQuantity();
			mechanismOperationTO.setIsIndPrepaid(false);
			mechanismOperationTO.setStockQuantityRemmaing(stockQuantity);
			// si es REPORTO RF o REPORTO RV se evalua para la asignacion de anticipacion parcial y el monto restante para liquidar
			if (mechanismOperationTO.getIdModality().equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode())
					|| mechanismOperationTO.getIdModality().equals(NegotiationModalityType.REPORT_EQUITIES.getCode())) {
				// evaluar para las columnas anticipacion parcial
				// evaluar para la cantidad de restante a liqudarse
				objRes = verifyIndPrepaidAndStockQuantity(mechanismOperationTO.getIdMechanismOperationPk());
				mechanismOperationTO.setIsIndPrepaid((Boolean) objRes[0]);
				mechanismOperationTO.setStockQuantityRemmaing(new BigDecimal(objRes[1].toString()));
			}
			
			mechanismOperationTOs.add(mechanismOperationTO);
		}
		return mechanismOperationTOs;
	}
	
	
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> searchOperationsReport(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT distinct ");
		sbQuery.append("	nme.idNegotiationMechanismPk, nme.mechanismName, "); // 0 1 
		sbQuery.append("	nmo.idNegotiationModalityPk, nmo.modalityName, "); // 2 3 
		sbQuery.append("	mo.idMechanismOperationPk, mo.operationNumber,  "); // 4 5 
		sbQuery.append("	mo.operationDate, mo.ballotNumber, mo.sequential,  ");// 6 7 8
		sbQuery.append("	mo.securities.idSecurityCodePk, ");//9
		sbQuery.append("	mo.stockQuantity, ");//10
		sbQuery.append("	mo.cashSettlementDate, mo.termSettlementDate, ");//11 12
		sbQuery.append("	mo.buyerParticipant.idParticipantPk, mo.buyerParticipant.mnemonic, ");//13 14
		sbQuery.append(" 	mo.sellerParticipant.idParticipantPk, mo.sellerParticipant.mnemonic, "); // 15 16 
		sbQuery.append(" 	mo.operationState, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.operationState ), ");// 17 18
		sbQuery.append(" 	mo.mcnProcessFile.idMcnProcessFilePk, ");// 19
		sbQuery.append(" 	(select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.currency ), ");// 20
		sbQuery.append(" 	(select count(so) from SettlementOperation so where so.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk and so.indForcedPurchase = :indForcedPurchase), ");// 21
		sbQuery.append("    mo.termSettlementDays, mo.amountRate, mo.cashSettlementPrice ");//22 23 24
		sbQuery.append("	FROM SettlementOperation so");
		sbQuery.append("	inner join so.mechanismOperation mo");
		sbQuery.append("	inner join mo.mechanisnModality mm");
		sbQuery.append("	inner join mm.negotiationMechanism nme");
		sbQuery.append("	inner join mm.negotiationModality nmo");
		sbQuery.append("	WHERE mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	and so.operationPart = 2");
		sbQuery.append("	and mo.operationState = 615");
		
		//issue 1044 adicoion de filtros por CUI y HolderAccount
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolder()))
			if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolder().getIdHolderPk())){
				sbQuery.append(" 	and :cuiParameter in (");
				sbQuery.append(" 		select had.holder.idHolderPk");
				sbQuery.append(" 		from  HolderAccountDetail had, HolderAccountOperation hao");
				sbQuery.append(" 		where hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk");
				sbQuery.append(" 		and had.holderAccount.idHolderAccountPk = hao.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		)");
				parameters.put("cuiParameter", searchOperationTO.getHolder().getIdHolderPk());
			}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolderAccount()))
			if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolderAccount().getIdHolderAccountPk())){
				sbQuery.append(" 	and :hAccountParameter in (");
				sbQuery.append(" 		select had.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		from  HolderAccountDetail had, HolderAccountOperation hao");
				sbQuery.append(" 		where hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk");
				sbQuery.append(" 		and had.holderAccount.idHolderAccountPk = hao.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		)");
				parameters.put("hAccountParameter", searchOperationTO.getHolderAccount().getIdHolderAccountPk());
			}
		
		if(searchOperationTO.getNegotiationModalitySelected()!=null){
			sbQuery.append("	and mm.id.idNegotiationModalityPk = :idNegotiationModalityPk");
			parameters.put("idNegotiationModalityPk", searchOperationTO.getNegotiationModalitySelected());
		}else {
			sbQuery.append("	and mm.id.idNegotiationModalityPk in (3,4) ");
		}
		
		
		parameters.put("indForcedPurchase", BooleanType.YES.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getParticipantSelected())){
			sbQuery.append("	AND (mo.sellerParticipant.idParticipantPk = :idParticipantPk ");
			sbQuery.append("		OR mo.buyerParticipant.idParticipantPk = :idParticipantPk)");
//			sbQuery.append("	OR (hao.inchargeFundsParticipant.idParticipantPk = :idParticipantPk");
//			sbQuery.append("		OR hao.inchargeStockParticipant.idParticipantPk = :idParticipantPk) )");
			parameters.put("idParticipantPk", searchOperationTO.getParticipantSelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getStateSelected())){
			sbQuery.append("	AND mo.operationState = :state");
			parameters.put("state", searchOperationTO.getStateSelected());
		}else{
			sbQuery.append("	AND mo.operationState not in (:stateOTC)");
			List<Integer> lstStateOTC = new ArrayList<Integer>();
			lstStateOTC.add(OtcOperationStateType.ANULATE.getCode());
			lstStateOTC.add(OtcOperationStateType.APROVED.getCode());
			lstStateOTC.add(OtcOperationStateType.CANCELED.getCode());
			lstStateOTC.add(OtcOperationStateType.CONFIRMED.getCode());
			lstStateOTC.add(OtcOperationStateType.REGISTERED.getCode());
			lstStateOTC.add(OtcOperationStateType.REJECTED.getCode());
			lstStateOTC.add(OtcOperationStateType.REVIEWED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REGISTERED.getCode());
			lstStateOTC.add(SirtexOperationStateType.ANULATE.getCode());
			lstStateOTC.add(SirtexOperationStateType.APROVED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REJECTED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REVIEWED.getCode());
			lstStateOTC.add(SirtexOperationStateType.CONFIRMED.getCode());
			parameters.put("stateOTC", lstStateOTC);
		}
			
		/*if(GeneralConstants.ZERO_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){//OPERATION DATE
			sbQuery.append("	AND TRUNC(mo.operationDate) between :initialDate and :endDate");
		}else if(GeneralConstants.ONE_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){//CASH SETTLEMENT DATE
			sbQuery.append("	AND TRUNC(mo.cashSettlementDate) between :initialDate and :endDate ");
		}else if(GeneralConstants.TWO_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){	//TERM SETTLEMENT DATE*/
			sbQuery.append("	AND TRUNC(mo.termSettlementDate)  between :initialDate and :endDate ");
		/*}*/
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getOperationNumber())){
			sbQuery.append("	AND mo.operationNumber = :operationNumber");
			parameters.put("operationNumber", searchOperationTO.getOperationNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getBallotNumber())){
			sbQuery.append("	AND mo.ballotNumber = :ballotNumber");
			parameters.put("ballotNumber", searchOperationTO.getBallotNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getCurrencySelected())){
			sbQuery.append("	AND mo.currency = :currency");
			parameters.put("currency", searchOperationTO.getCurrencySelected());
		}
		//issue 825
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdSecurityCodePk())){
			sbQuery.append("	AND mo.securities.idSecurityCodePk = :idSecurityCodePk");
			parameters.put("idSecurityCodePk", searchOperationTO.getIdSecurityCodePk());
		}
		
		// issue 1044 adicon de filtro por clase de valor
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSecurityClassSelected())){
			sbQuery.append("	AND mo.securities.securityClass = :securityClass");
			parameters.put("securityClass", searchOperationTO.getSecurityClassSelected());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdIssuerPk())){
			sbQuery.append("	AND mo.securities.issuer.idIssuerPk = :idIssuerPk");
			parameters.put("idIssuerPk", searchOperationTO.getIdIssuerPk());
		}
			
		sbQuery.append("	ORDER BY mo.operationDate, mo.operationNumber DESC");
		
		parameters.put("idNegotiationMechanismPk", searchOperationTO.getNegotiationMechanismSelected());
		parameters.put("initialDate", searchOperationTO.getInitialDate());
		parameters.put("endDate", searchOperationTO.getEndDate()); 


		List<Object[]> operationObjects = findListByQueryString(sbQuery.toString(), parameters);
		List<MechanismOperationTO> mechanismOperationTOs = new ArrayList<MechanismOperationTO>();
		Object[] objRes;
		BigDecimal stockQuantity;
		for (Object[] objOperation : operationObjects) {
			MechanismOperationTO mechanismOperationTO = NegotiationUtils.populateSearchOperations(objOperation);
			stockQuantity=mechanismOperationTO.getStockQuantity();
			mechanismOperationTO.setIsIndPrepaid(false);
			mechanismOperationTO.setStockQuantityRemmaing(stockQuantity);
			// si es REPORTO RF o REPORTO RV se evalua para la asignacion de anticipacion parcial y el monto restante para liquidar
			if (mechanismOperationTO.getIdModality().equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode())
					|| mechanismOperationTO.getIdModality().equals(NegotiationModalityType.REPORT_EQUITIES.getCode())) {
				// evaluar para las columnas anticipacion parcial
				// evaluar para la cantidad de restante a liqudarse
				objRes = verifyIndPrepaidAndStockQuantity(mechanismOperationTO.getIdMechanismOperationPk());
				mechanismOperationTO.setIsIndPrepaid((Boolean) objRes[0]);
				mechanismOperationTO.setStockQuantityRemmaing(new BigDecimal(objRes[1].toString()));
			}
			
			mechanismOperationTOs.add(mechanismOperationTO);
		}
		return mechanismOperationTOs;
	}
	
	
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> searchOperationsUnfulfillment(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT distinct ");
		sbQuery.append("	nme.idNegotiationMechanismPk, nme.mechanismName, "); // 0 1 
		sbQuery.append("	nmo.idNegotiationModalityPk, nmo.modalityName, "); // 2 3 
		sbQuery.append("	mo.idMechanismOperationPk, mo.operationNumber,  "); // 4 5 
		sbQuery.append("	mo.operationDate, mo.ballotNumber, mo.sequential,  ");// 6 7 8
		sbQuery.append("	mo.securities.idSecurityCodePk, ");//9
		sbQuery.append("	mo.stockQuantity, ");//10
		sbQuery.append("	mo.cashSettlementDate, mo.termSettlementDate, ");//11 12
		sbQuery.append("	mo.buyerParticipant.idParticipantPk, mo.buyerParticipant.mnemonic, ");//13 14
		sbQuery.append(" 	mo.sellerParticipant.idParticipantPk, mo.sellerParticipant.mnemonic, "); // 15 16 
		sbQuery.append(" 	mo.operationState, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.operationState ), ");// 17 18
		sbQuery.append(" 	mo.mcnProcessFile.idMcnProcessFilePk, ");// 19
		sbQuery.append(" 	(select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = mo.currency ), ");// 20
		sbQuery.append(" 	(select count(so) from SettlementOperation so where so.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk and so.indForcedPurchase = :indForcedPurchase), ");// 21
		sbQuery.append("    mo.termSettlementDays, mo.amountRate, mo.cashSettlementPrice ");//22 23 24
		sbQuery.append("    ,ou.idOperationUnfulfillmentPk ");//25
		
		sbQuery.append("	FROM OperationUnfulfillment ou");
		sbQuery.append("	inner join ou.settlementOperation so");
		sbQuery.append("	inner join so.mechanismOperation mo");
		sbQuery.append("	inner join mo.mechanisnModality mm");
		sbQuery.append("	inner join mm.negotiationMechanism nme");
		sbQuery.append("	inner join mm.negotiationModality nmo");
		
		sbQuery.append("	WHERE mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	and so.operationPart = 2");
		sbQuery.append("	and mo.operationState = 1849");
		
		/*//issue 1044 adicoion de filtros por CUI y HolderAccount
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolder()))
			if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolder().getIdHolderPk())){
				sbQuery.append(" 	and :cuiParameter in (");
				sbQuery.append(" 		select had.holder.idHolderPk");
				sbQuery.append(" 		from  HolderAccountDetail had, HolderAccountOperation hao");
				sbQuery.append(" 		where hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk");
				sbQuery.append(" 		and had.holderAccount.idHolderAccountPk = hao.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		)");
				parameters.put("cuiParameter", searchOperationTO.getHolder().getIdHolderPk());
			}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolderAccount()))
			if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getHolderAccount().getIdHolderAccountPk())){
				sbQuery.append(" 	and :hAccountParameter in (");
				sbQuery.append(" 		select had.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		from  HolderAccountDetail had, HolderAccountOperation hao");
				sbQuery.append(" 		where hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk");
				sbQuery.append(" 		and had.holderAccount.idHolderAccountPk = hao.holderAccount.idHolderAccountPk");
				sbQuery.append(" 		)");
				parameters.put("hAccountParameter", searchOperationTO.getHolderAccount().getIdHolderAccountPk());
			}*/
		
		if(searchOperationTO.getNegotiationModalitySelected()!=null){
			sbQuery.append("	and mm.id.idNegotiationModalityPk = :idNegotiationModalityPk");
			parameters.put("idNegotiationModalityPk", searchOperationTO.getNegotiationModalitySelected());
		}else {
			sbQuery.append("	and mm.id.idNegotiationModalityPk in (3,4) ");
		}
		
		
		parameters.put("indForcedPurchase", BooleanType.YES.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getParticipantSelected())){
			sbQuery.append("	AND (mo.sellerParticipant.idParticipantPk = :idParticipantPk ");
			sbQuery.append("		OR mo.buyerParticipant.idParticipantPk = :idParticipantPk)");
//			sbQuery.append("	OR (hao.inchargeFundsParticipant.idParticipantPk = :idParticipantPk");
//			sbQuery.append("		OR hao.inchargeStockParticipant.idParticipantPk = :idParticipantPk) )");
			parameters.put("idParticipantPk", searchOperationTO.getParticipantSelected());
		}
		/*if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getStateSelected())){
			sbQuery.append("	AND mo.operationState = :state");
			parameters.put("state", searchOperationTO.getStateSelected());
		}else{
			sbQuery.append("	AND mo.operationState not in (:stateOTC)");
			List<Integer> lstStateOTC = new ArrayList<Integer>();
			lstStateOTC.add(OtcOperationStateType.ANULATE.getCode());
			lstStateOTC.add(OtcOperationStateType.APROVED.getCode());
			lstStateOTC.add(OtcOperationStateType.CANCELED.getCode());
			lstStateOTC.add(OtcOperationStateType.CONFIRMED.getCode());
			lstStateOTC.add(OtcOperationStateType.REGISTERED.getCode());
			lstStateOTC.add(OtcOperationStateType.REJECTED.getCode());
			lstStateOTC.add(OtcOperationStateType.REVIEWED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REGISTERED.getCode());
			lstStateOTC.add(SirtexOperationStateType.ANULATE.getCode());
			lstStateOTC.add(SirtexOperationStateType.APROVED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REJECTED.getCode());
			lstStateOTC.add(SirtexOperationStateType.REVIEWED.getCode());
			lstStateOTC.add(SirtexOperationStateType.CONFIRMED.getCode());
			parameters.put("stateOTC", lstStateOTC);
		}*/
			
		/*if(GeneralConstants.ZERO_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){//OPERATION DATE
			sbQuery.append("	AND TRUNC(mo.operationDate) between :initialDate and :endDate");
		}else if(GeneralConstants.ONE_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){//CASH SETTLEMENT DATE
			sbQuery.append("	AND TRUNC(mo.cashSettlementDate) between :initialDate and :endDate ");
		}else if(GeneralConstants.TWO_VALUE_INTEGER.equals(searchOperationTO.getDateTypeSelected())){	//TERM SETTLEMENT DATE*/
			sbQuery.append("	AND TRUNC(ou.unfulfillmentDate)  between :initialDate and :endDate ");
		/*}*/
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getOperationNumber())){
			sbQuery.append("	AND mo.operationNumber = :operationNumber");
			parameters.put("operationNumber", searchOperationTO.getOperationNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getBallotNumber())){
			sbQuery.append("	AND mo.ballotNumber = :ballotNumber");
			parameters.put("ballotNumber", searchOperationTO.getBallotNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getCurrencySelected())){
			sbQuery.append("	AND mo.currency = :currency");
			parameters.put("currency", searchOperationTO.getCurrencySelected());
		}
		//issue 825
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdSecurityCodePk())){
			sbQuery.append("	AND mo.securities.idSecurityCodePk = :idSecurityCodePk");
			parameters.put("idSecurityCodePk", searchOperationTO.getIdSecurityCodePk());
		}
		
		// issue 1044 adicon de filtro por clase de valor
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSecurityClassSelected())){
			sbQuery.append("	AND mo.securities.securityClass = :securityClass");
			parameters.put("securityClass", searchOperationTO.getSecurityClassSelected());
		}
		
		/*if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdIssuerPk())){
			sbQuery.append("	AND mo.securities.issuer.idIssuerPk = :idIssuerPk");
			parameters.put("idIssuerPk", searchOperationTO.getIdIssuerPk());
		}*/
		
		//issue 1376
		List<Long> operations = searchOperationExists();
		
		if(operations.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			sbQuery.append("	AND mo.idMechanismOperationPk not in (:lstMechan) ");
			parameters.put("lstMechan", operations);
		}
			
		sbQuery.append("	ORDER BY mo.operationDate, mo.operationNumber DESC");
		
		parameters.put("idNegotiationMechanismPk", searchOperationTO.getNegotiationMechanismSelected());
		parameters.put("initialDate", searchOperationTO.getInitialDate());
		parameters.put("endDate", searchOperationTO.getEndDate()); 

		List<Object[]> operationObjects = findListByQueryString(sbQuery.toString(), parameters);
		
		List<MechanismOperationTO> mechanismOperationTOs = new ArrayList<MechanismOperationTO>();
		
		Object[] objRes;
		BigDecimal stockQuantity;
		
		for (Object[] objOperation : operationObjects) {
			
			MechanismOperationTO mechanismOperationTO = NegotiationUtils.populateSearchOperationsUnfulfillment(objOperation);
			stockQuantity=mechanismOperationTO.getStockQuantity();
			mechanismOperationTO.setIsIndPrepaid(false);
			mechanismOperationTO.setStockQuantityRemmaing(stockQuantity);
			// si es REPORTO RF o REPORTO RV se evalua para la asignacion de anticipacion parcial y el monto restante para liquidar
			if (mechanismOperationTO.getIdModality().equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode())
					|| mechanismOperationTO.getIdModality().equals(NegotiationModalityType.REPORT_EQUITIES.getCode())) {
				// evaluar para las columnas anticipacion parcial
				// evaluar para la cantidad de restante a liqudarse
				objRes = verifyIndPrepaidAndStockQuantity(mechanismOperationTO.getIdMechanismOperationPk());
				mechanismOperationTO.setIsIndPrepaid((Boolean) objRes[0]);
				mechanismOperationTO.setStockQuantityRemmaing(new BigDecimal(objRes[1].toString()));
			}
			
			mechanismOperationTOs.add(mechanismOperationTO);
		}
		return mechanismOperationTOs;
	}
	
	
	/** metodo para verificar si una operacion AnticipacionParcil y tambien retona la cantidad Valores que quedan por liquidarse
	 * 
	 * @param idMechanismOperationPk
	 * @return */
	private Object[] verifyIndPrepaidAndStockQuantity(Long idMechanismOperationPk) {
		// por defecto con ANTICIPACION=false
		// y MONTO RESTANTE POR LIQUIDAR = 0
		Object[] objRes = { false, BigDecimal.ZERO };
		List<SettlementOperation> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select so");
		sd.append(" from SettlementOperation so");
		sd.append(" where so.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk");
		try {
			Query q = em.createQuery(sd.toString());
			q.setParameter("idMechanismOperationPk", idMechanismOperationPk);
			lst = q.getResultList();
			if (!lst.isEmpty())
				for (SettlementOperation so : lst) {
					// si es LIQUIDADA PLAZO y PARTE A PLAZO es ANTICIPADO=true
					if (so.getOperationState().equals(MechanismOperationStateType.TERM_SETTLED.getCode())
							&& so.getOperationPart().equals(OperationPartType.TERM_PART.getCode()))
						objRes[0] = true;

					// si es LIQUIDADA CONTADO y PARTE A PLAZO entonces se extrae el monto restante
					if (so.getOperationState().equals(MechanismOperationStateType.CASH_SETTLED.getCode())
							&& so.getOperationPart().equals(OperationPartType.TERM_PART.getCode()))
						objRes[1] = new BigDecimal(so.getStockQuantity().toString());
				}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
		}
		return objRes;
	}

	/**
	 * Gets the modality group.
	 *
	 * @param id the id
	 * @return the modality group
	 */
	public ModalityGroup getModalityGroup(MechanismModalityPK id) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mg");
		sbQuery.append("	FROM ModalityGroup mg");
		sbQuery.append("	WHERE mg.idModalityGroupPk = (SELECT mgd.modalityGroup.idModalityGroupPk");
		sbQuery.append("		FROM ModalityGroupDetail mgd WHERE mgd.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("		AND mgd.mechanismModality.id.idNegotiationModalityPk = :idNegotiationModalityPk");
		sbQuery.append("		AND mgd.modGroupState = :modGroupState)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idNegotiationMechanismPk", id.getIdNegotiationMechanismPk());
		query.setParameter("idNegotiationModalityPk", id.getIdNegotiationModalityPk());
		query.setParameter("modGroupState", BooleanType.YES.getCode());
		try {
			return (ModalityGroup)query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Gets the modality groups.
	 *
	 * @param idMechanism the id mechanism
	 * @param settlementSchema the settlement schema
	 * @return the modality groups
	 */
	@SuppressWarnings("unchecked")
	public List<ModalityGroup> getModalityGroups(Long idMechanism, Integer settlementSchema) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT mg");
		sbQuery.append("	FROM ModalityGroup mg");
		sbQuery.append("	WHERE mg.groupState  = :groupState ");
		
		if(settlementSchema!=null){
			sbQuery.append("	and mg.settlementSchema = :settlementSchema");
			parameters.put("settlementSchema", settlementSchema);
		}
		
		if(idMechanism!=null){
			sbQuery.append("	and mg.negotiationMechanism.idNegotiationMechanismPk  = :idMechanism ");
			parameters.put("idMechanism", idMechanism);
		}
		parameters.put("groupState", ComponentConstant.ONE);
		
		return findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	WHERE ha.idHolderAccountPk = :idHolderAccountPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			return (HolderAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Gets the mechanism modality.
	 *
	 * @param negoMechanism the nego mechanism
	 * @param negoModality the nego modality
	 * @return the mechanism modality
	 */
	public MechanismModality getMechanismModality(Long negoMechanism, Long negoModality) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
	    sbQuery.append("Select mm from MechanismModality mm   ");
	    sbQuery.append(" inner join fetch mm.negotiationMechanism nme  ");
	    sbQuery.append(" inner join fetch mm.negotiationModality nmo  ");
	    sbQuery.append(" where mm.id.idNegotiationMechanismPk = :MechanismPk ");
	    sbQuery.append(" and mm.id.idNegotiationModalityPk = :ModalityPk ");
		parameters.put("MechanismPk", negoMechanism);
		parameters.put("ModalityPk", negoModality);
		return (MechanismModality)findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Metodo para buscar las solicitudes SEDIR
	 * @param ballotNumber
	 * @param sequential
	 * @param operationDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> searchOperationsSedir (SearchMCNOperationsTO searchOperationTO) {
		
		StringBuffer sbQuery = new StringBuffer();
		
		SettlementReport settlementReport = null;
		
		List<SettlementReportTO> settlementReportTO = null;
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append("select sr, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = sr.requestState ) "); 
		sbQuery.append(" from SettlementReport sr ");
		sbQuery.append(" inner join fetch sr.mechanismOperation mo  ");
		sbQuery.append(" inner join fetch sr.mechanismOperation.buyerParticipant bp  ");
		sbQuery.append(" inner join fetch sr.mechanismOperation.sellerParticipant sp  ");
		sbQuery.append(" inner join fetch sr.mechanismOperation.securities sec  ");
		sbQuery.append(" where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getOperationNumber())) {
			sbQuery.append(" and sr.idSettlementReportPk = :operationNumber ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSecurityClassSelected())) {
			sbQuery.append(" and mo.securities.securityClass = :securityClass ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdSecurityCodePk())) {
			sbQuery.append(" and mo.securities.idSecurityCodePk = :security ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getStateSelected())) {
			sbQuery.append(" and sr.requestState = :stateSelected ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getBallotNumber())) {
			sbQuery.append(" and  mo.ballotNumber = :ballotNumber ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSequential())) {
			sbQuery.append(" and  mo.sequential = :sequential ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getCurrencySelected())) {
			sbQuery.append(" and  mo.operationCurrency = :currency ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getParticipantSelected())){
			sbQuery.append("	AND (mo.sellerParticipant.idParticipantPk = :idParticipantPk ");
			sbQuery.append("		OR mo.buyerParticipant.idParticipantPk = :idParticipantPk)");
		}
		
		sbQuery.append(" and  TRUNC(sr.registerDate) between :initialDate and :endDate ");
		sbQuery.append("	ORDER BY sr.idSettlementReportPk DESC");
		
		parameters.put("initialDate", searchOperationTO.getInitialDate());
		parameters.put("endDate", searchOperationTO.getEndDate());
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getOperationNumber())) {
			parameters.put("operationNumber", searchOperationTO.getOperationNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSecurityClassSelected())) {
			parameters.put("securityClass", searchOperationTO.getSecurityClassSelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdSecurityCodePk())) {
			parameters.put("security", searchOperationTO.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getStateSelected())) {
			parameters.put("stateSelected", searchOperationTO.getStateSelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getBallotNumber())) {
			parameters.put("ballotNumber", searchOperationTO.getBallotNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSequential())) {
			parameters.put("sequential", searchOperationTO.getSequential());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getCurrencySelected())) {
			parameters.put("currency", searchOperationTO.getCurrencySelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getParticipantSelected())){
			parameters.put("idParticipantPk", searchOperationTO.getParticipantSelected());
		}
		
		
		try{
			List<Object[]> operationObjects = findListByQueryString(sbQuery.toString(), parameters);
			
			if(Validations.validateIsNotNullAndNotEmpty(operationObjects)) {
				
				settlementReportTO = new ArrayList<SettlementReportTO>();
				
				for(Object[] lstResult : operationObjects ) {
					
					settlementReport = (SettlementReport)lstResult[0];
					
					SettlementReportTO settlRep = new SettlementReportTO();
					
					settlRep.setSettlementReport(settlementReport);
					settlRep.setIdSettlementReportPk(settlementReport.getIdSettlementReportPk());
					settlRep.setQuantityDays(settlementReport.getQuantityDays());
					settlRep.setRegisterDate(settlementReport.getRegisterDate());
					settlRep.setOriginalDate(settlementReport.getOriginalDate());
					settlRep.setMechanismOperation(settlementReport.getMechanismOperation());
					settlRep.setRequestState(settlementReport.getRequestState());
					settlRep.setStateDescription(lstResult[1].toString());
					
					if (Validations.validateIsNotNullAndNotEmpty(settlementReport.getNewDate())) {
						settlRep.setOriginalExpirationDate(settlementReport.getNewDate());
					}else {
						
						settlRep.setOriginalExpirationDate(CommonsUtilities.addDate(settlementReport.getOriginalDate(), settlRep.getQuantityDays()));
						
						while(holidayQueryServiceBean.isNonWorkingDayServiceBean(settlRep.getOriginalExpirationDate(),true)){
							
							settlRep.setOriginalExpirationDate(CommonsUtilities.addDate(settlRep.getOriginalExpirationDate(),1));
						
						}
					}
					
					settlementReportTO.add(settlRep);
				}
			}
			
		}catch(NoResultException ne){
			logger.error("SettlementReport not found ");
		}
		return settlementReportTO; 
	}
	

	/**
	 * Metodo para buscar las solicitudes SEDIR
	 * @param ballotNumber
	 * @param sequential
	 * @param operationDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementReportTO> searchOperationsUnfulfillmentSearch(SearchMCNOperationsTO searchOperationTO) {
		
		StringBuffer sbQuery = new StringBuffer();
		
		SettlementUnfulfillment settlementUnfulfillment = null;
		
		List<SettlementReportTO> settlementReportTO = null;
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append("select su, (select paramState.parameterName from ParameterTable paramState where paramState.parameterTablePk = su.requestState ) "); 
		sbQuery.append(" from SettlementUnfulfillment su ");
		sbQuery.append(" inner join fetch su.operationUnfulfillment ou  ");
		sbQuery.append(" inner join fetch ou.settlementOperation so  ");
		sbQuery.append(" inner join fetch so.mechanismOperation mo  ");
		sbQuery.append(" inner join fetch mo.securities sec  ");
		sbQuery.append(" where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getOperationNumber())) {
			sbQuery.append(" and su.idSettlementUnfulfillmentPk = :operationNumber ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSecurityClassSelected())) {
			sbQuery.append(" and mo.securities.securityClass = :securityClass ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdSecurityCodePk())) {
			sbQuery.append(" and mo.securities.idSecurityCodePk = :security ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getStateSelected())) {
			sbQuery.append(" and su.requestState = :stateSelected ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getBallotNumber())) {
			sbQuery.append(" and  mo.ballotNumber = :ballotNumber ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSequential())) {
			sbQuery.append(" and  mo.sequential = :sequential ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getCurrencySelected())) {
			sbQuery.append(" and  mo.currency = :currency ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getParticipantSelected())){
			sbQuery.append("	AND (mo.sellerParticipant.idParticipantPk = :idParticipantPk ");
			sbQuery.append("		OR mo.buyerParticipant.idParticipantPk = :idParticipantPk)");
		}
		
		sbQuery.append(" and  TRUNC(su.registerDate) between :initialDate and :endDate ");
		sbQuery.append("	ORDER BY su.idSettlementUnfulfillmentPk DESC");
		
		parameters.put("initialDate", searchOperationTO.getInitialDate());
		parameters.put("endDate", searchOperationTO.getEndDate());
		
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getOperationNumber())) {
			parameters.put("operationNumber", searchOperationTO.getOperationNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSecurityClassSelected())) {
			parameters.put("securityClass", searchOperationTO.getSecurityClassSelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getIdSecurityCodePk())) {
			parameters.put("security", searchOperationTO.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getStateSelected())) {
			parameters.put("stateSelected", searchOperationTO.getStateSelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getBallotNumber())) {
			parameters.put("ballotNumber", searchOperationTO.getBallotNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getSequential())) {
			parameters.put("sequential", searchOperationTO.getSequential());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getCurrencySelected())) {
			parameters.put("currency", searchOperationTO.getCurrencySelected());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchOperationTO.getParticipantSelected())){
			parameters.put("idParticipantPk", searchOperationTO.getParticipantSelected());
		}
		
		
		try{
			List<Object[]> operationObjects = findListByQueryString(sbQuery.toString(), parameters);
			
			if(Validations.validateIsNotNullAndNotEmpty(operationObjects)) {
				
				settlementReportTO = new ArrayList<SettlementReportTO>();
				
				for(Object[] lstResult : operationObjects ) {
					
					settlementUnfulfillment = (SettlementUnfulfillment)lstResult[0];
					
					SettlementReportTO settlRep = new SettlementReportTO();
					
					settlRep.setIdSettlementReportPk(settlementUnfulfillment.getIdSettlementUnfulfillmentPk());
					settlRep.setQuantityDays(settlementUnfulfillment.getIndBuyerSeller());
					settlRep.setRegisterDate(settlementUnfulfillment.getRegisterDate());
					settlRep.setMechanismOperation(settlementUnfulfillment.getOperationUnfulfillment().getSettlementOperation().getMechanismOperation());
					settlRep.setRequestState(settlementUnfulfillment.getRequestState());
					settlRep.setStateDescription(lstResult[1].toString());
					
					if(settlementUnfulfillment.getIndBuyerSeller().equals(GeneralConstants.ONE_VALUE_INTEGER)) {
						settlRep.setBalanceDescription("VENDEDOR");
					}else {
						settlRep.setBalanceDescription("COMPRADOR");
					}
					
					/*
					if (Validations.validateIsNotNullAndNotEmpty(settlementReport.getNewDate())) {
						settlRep.setOriginalExpirationDate(settlementReport.getNewDate());
					}else {
						
						settlRep.setOriginalExpirationDate(CommonsUtilities.addDate(settlementReport.getOriginalDate(), settlRep.getQuantityDays()));
						
						while(holidayQueryServiceBean.isNonWorkingDayServiceBean(settlRep.getOriginalExpirationDate(),true)){
							
							settlRep.setOriginalExpirationDate(CommonsUtilities.addDate(settlRep.getOriginalExpirationDate(),1));
						
						}
					}*/
					
					settlementReportTO.add(settlRep);
				}
			}
			
		}catch(NoResultException ne){
			logger.error("SettlementReport not found ");
		}
		return settlementReportTO; 
	}
	
	/**
	 * Metodo para 
	 * @param ballotNumber
	 * @param sequential
	 * @param operationDate
	 * @return
	 */
	public OperationUnfulfillment getOperationUnfulfillment(Long operationUnfulfillmentPK) {
		StringBuffer sbQuery = new StringBuffer();
		OperationUnfulfillment operationUnfulfillment = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select ou "); 
		sbQuery.append(" from OperationUnfulfillment ou ");
		sbQuery.append(" where ou.idOperationUnfulfillmentPk = :operationUnfulfillmentPK ");
		
		parameters.put("operationUnfulfillmentPK", operationUnfulfillmentPK);
		
		try{
			operationUnfulfillment = (OperationUnfulfillment) findObjectByQueryString(sbQuery.toString(), parameters);
		}catch(NoResultException ne){
			logger.error("MechanismOperation not found ");
		}
		return operationUnfulfillment; 
	}
	

	/**
	 * Metodo para 
	 * @param ballotNumber
	 * @param sequential
	 * @param operationDate
	 * @return
	 */
	public MechanismOperation getMechanismOperationSedir (Long mechanismOperationPk) {
		StringBuffer sbQuery = new StringBuffer();
		MechanismOperation mechanismOperation = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select mo "); 
		sbQuery.append(" from MechanismOperation mo ");
		sbQuery.append(" where mo.idMechanismOperationPk = :operationDate ");
		
		parameters.put("operationDate", mechanismOperationPk);
		
		try{
			mechanismOperation = (MechanismOperation) findObjectByQueryString(sbQuery.toString(), parameters);
		}catch(NoResultException ne){
			logger.error("MechanismOperation not found ");
		}
		return mechanismOperation; 
	}
	

	/**
	 * Metodo para 
	 * @param ballotNumber
	 * @param sequential
	 * @param operationDate
	 * @return
	 */
	public Integer getLoggerUser (String user) {
		
		StringBuilder querySql = new StringBuilder();
		Integer institution = null;
		
		querySql.append("  SELECT ID_SECURITY_INSTITUTION_FK from SECURITY.USER_ACCOUNT WHERE LOGIN_USER = :user           ");
		
		Query query = em.createNativeQuery(querySql.toString());
		
		query.setParameter("user", user);
		
		BigDecimal quantity = (BigDecimal)query.getSingleResult();
		
		institution = Integer.valueOf(quantity.toString());
		
		return institution;
		
	}
	
	
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @param ballotNumber the ballot number
	 * @param sequential the sequential
	 * @param operationDate the operation date
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation(Long ballotNumber,Long sequential, Date operationDate) {
		StringBuffer sbQuery = new StringBuffer();
		MechanismOperation mechanismOperation = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select mo "); 
		sbQuery.append(" from MechanismOperation mo ");
		sbQuery.append(" inner join fetch mo.assignmentRequests ");
		sbQuery.append(" where trunc(mo.operationDate) = trunc(:operationDate) ");
		sbQuery.append(" and mo.ballotNumber = :ballotNumber ");
		sbQuery.append(" and mo.sequential = :sequential ");
		sbQuery.append(" and mo.operationState = :operationState ");
		
		parameters.put("operationDate", operationDate);
		parameters.put("ballotNumber", ballotNumber);
		parameters.put("sequential", sequential);
		parameters.put("operationState", MechanismOperationStateType.REGISTERED_STATE.getCode());
		
		try{
			mechanismOperation = (MechanismOperation) findObjectByQueryString(sbQuery.toString(), parameters);
		}catch(NoResultException ne){
			logger.error("MechanismOperation not found ");
		}
		return mechanismOperation; 
	}

	/**
	 * Send operations notification.
	 *
	 * @param idParticipantId the id participant id
	 * @param businessProcessId the business process id
	 * @param mechanismOperation the mechanism operation
	 * @param loggerUser the logger user
	 */
	public void sendOperationsNotification(Long idParticipantId, Long businessProcessId, MechanismOperation mechanismOperation, LoggerUser loggerUser) {
		//send reopen notification
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(businessProcessId);
		Object[] params = new Object[3];
		params[0] =  mechanismOperation.getBuyerParticipant().getDisplayDescriptionCode();
		params[1] =  mechanismOperation.getOperationNumberBallotSequential();
		params[2] =  mechanismOperation.getMechanisnModality().getNegotiationModality().getModalityName();
		notificationService.sendNotification(loggerUser, loggerUser.getUserName(), businessProcess, idParticipantId, params);
	}
	

	/**
	 * Search position by participant.
	 *
	 * @param searchPositionCommissionTO the search position commission to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchPositionByParticipant(SearchPositionCommissionTO searchPositionCommissionTO) throws ServiceException{
		StringBuffer stringBuilderSql = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		stringBuilderSql.append(" 	select " );
		stringBuilderSql.append(" 	ps.participant.idParticipantPk, " ); //0
		stringBuilderSql.append(" 	ps.participant.description, " );//1
		stringBuilderSql.append(" 	ps.participant.mnemonic, " );//2
		stringBuilderSql.append(" 	MGD.modalityGroup.idModalityGroupPk, " );//3
		stringBuilderSql.append(" 	MGD.modalityGroup.groupName, " );//4
		stringBuilderSql.append(" 	MO.settlementSchema, " );//5
		stringBuilderSql.append(" 	SO.settlementCurrency, " );//6
		stringBuilderSql.append(" 	MM.negotiationMechanism.idNegotiationMechanismPk, " );//7
		stringBuilderSql.append(" 	MM.negotiationMechanism.mechanismName, " );//8
		stringBuilderSql.append(" 	ps.settlementAmount, " ); //9
		stringBuilderSql.append(" 	ps.role, " ); //10
		stringBuilderSql.append(" 	(select parameterName from ParameterTable where parameterTablePk = SO.settlementCurrency) " ); //11
	    stringBuilderSql.append(" 	from ParticipantSettlement ps, SettlementOperation SO, MechanismOperation MO, ");
	    stringBuilderSql.append(" 		 MechanismModality MM , ModalityGroupDetail MGD ");
	    stringBuilderSql.append(" 	where MO.mechanisnModality.id.idNegotiationMechanismPk = MM.id.idNegotiationMechanismPk ");
	    stringBuilderSql.append(" 	and   MO.mechanisnModality.id.idNegotiationModalityPk = MM.id.idNegotiationModalityPk ");
	    stringBuilderSql.append(" 	and   MGD.mechanismModality.id.idNegotiationMechanismPk = MM.id.idNegotiationMechanismPk ");
	    stringBuilderSql.append(" 	and   MGD.mechanismModality.id.idNegotiationModalityPk = MM.id.idNegotiationModalityPk ");
	    stringBuilderSql.append("	and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
	    stringBuilderSql.append("	and ps.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
	    stringBuilderSql.append("	and MGD.modalityGroup.settlementSchema = SO.settlementSchema ");
	    stringBuilderSql.append(" 	and MM.id.idNegotiationMechanismPk = :idMechanism ");
	    if(searchPositionCommissionTO.getModalityGroupSelected()!=null){
	    	stringBuilderSql.append(" 	and MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		    parameters.put("idModalityGroup", searchPositionCommissionTO.getModalityGroupSelected());
	    }
	    if(searchPositionCommissionTO.getParticipantSelected()!=null){
	    	stringBuilderSql.append(" 	and ps.participant.idParticipantPk = :idParticipant ");
		    parameters.put("idParticipant", searchPositionCommissionTO.getParticipantSelected());
	    }
	    
	    stringBuilderSql.append("	and SO.settlementCurrency = :idCurrency ");
	    stringBuilderSql.append("	and SO.settlementType = :settlementType ");
	    stringBuilderSql.append("	and SO.settlementSchema = :idSchema ");
	    stringBuilderSql.append("	and SO.indExtended = :indExtended ");
	    stringBuilderSql.append("	and PS.operationState in (:partSettStates ) ");
	    //stringBuilderSql.append("	and PS.role = :purchaseRole ");
		stringBuilderSql.append("	and SO.settlementDate = :settlementDate ");
		stringBuilderSql.append("	and ( ( SO.operationPart = :cashPart and SO.operationState in (:cashStates) ) ");
		stringBuilderSql.append("	   or ( SO.operationPart = :termPart and SO.operationState in (:termStates) ) ) ");
		stringBuilderSql.append(" 	 " );
		
		ArrayList<Integer> cashStates = new ArrayList<Integer>();
		cashStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
	    cashStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
	    
	    ArrayList<Integer> termStates = new ArrayList<Integer>();
	    termStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		    
		
		parameters.put("cashStates", cashStates);
		parameters.put("termStates", termStates);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
	    parameters.put("termPart", ComponentConstant.TERM_PART);
	    
	    ArrayList<Integer> partSettstates = new ArrayList<Integer>();
	    partSettstates.add(ParticipantOperationStateType.CONFIRM.getCode());
	    
	    parameters.put("partSettStates",partSettstates);
	    parameters.put("idSchema",searchPositionCommissionTO.getSettlementSchemaSelected());
	    parameters.put("idMechanism", searchPositionCommissionTO.getNegoMechanismSelected());
	    parameters.put("idCurrency", searchPositionCommissionTO.getCurrencySelected());
	    parameters.put("settlementDate", searchPositionCommissionTO.getSettlementDate());
	    if(searchPositionCommissionTO.isExtended()){
	    	parameters.put("indExtended", ComponentConstant.ONE);
	    }else{
	    	parameters.put("indExtended", ComponentConstant.ZERO);
	    }
	    parameters.put("settlementType", SettlementType.DVP.getCode());
	    
	    return (List<Object[]>) findListByQueryString(stringBuilderSql.toString(),parameters);
	}
	
	/**
	 * Gets the cash amount by role.
	 *
	 * @param positionByParticipant the position by participant
	 * @param searchPositionCommissionTO the search position commission to
	 * @param role the role
	 * @return the cash amount by role
	 */
	public BigDecimal getCashAmountByRole(PositionParticipantTO positionByParticipant, SearchPositionCommissionTO searchPositionCommissionTO, Integer role){
		StringBuilder sbQuery = new StringBuilder();
		/** SE HACE NATIVO YA QUE EL JPA NO SOPORTA "UNION" **/
		sbQuery.append("	SELECT NVL(SUM(pos.amount),0)");
		sbQuery.append("	FROM (SELECT NVL(SUM(po.settlement_amount),0) as amount");
		sbQuery.append("			FROM participant_operation po, mechanism_operation mo,"); 
		sbQuery.append("				modality_group mg, modality_group_detail mgd");
		sbQuery.append("			WHERE po.id_mechanism_operation_fk = mo.id_mechanism_operation_pk");
		sbQuery.append("			AND mo.id_negotiation_mechanism_fk = mgd.id_negotiation_mechanism_fk");
		sbQuery.append("			AND mo.id_negotiation_modality_fk = mgd.id_negotiation_modality_fk");
		sbQuery.append("			AND mgd.id_modality_group_fk = mg.id_modality_group_pk");
		sbQuery.append("			and po.id_participant_fk = :idParticipantPk");
		sbQuery.append("			AND po.role = :role");
		sbQuery.append("			AND po.SETTLEMENT_CURRENCY = :currency");
		sbQuery.append("			AND po.incharge_type = :inchargeType");
		sbQuery.append("			AND mo.settlement_type = :settlementType");
		sbQuery.append("			AND mg.ID_MODALITY_GROUP_PK = :idModalityGroupPk");
		sbQuery.append("			AND mo.id_negotiation_mechanism_fk = :idNegotiationMechanismPk");
		sbQuery.append("			AND ( ( (mo.operation_state = :assigState and po.operation_part = :cashPart)");
		sbQuery.append("					AND ( (mo.ind_cash_extended = :indNo AND mo.ind_cash_prepaid = :indNo");
		sbQuery.append("					AND trunc(mo.cash_settlement_Date) = trunc(:settlementDate))");
		sbQuery.append("					OR (mo.ind_cash_prepaid = :indYes AND trunc(mo.CASH_PREPAID_DATE) = trunc(:settlementDate))");
		sbQuery.append("					OR (mo.ind_cash_extended = :indYes AND trunc(mo.CASH_EXTENDED_DATE) = trunc(:settlementDate)) ) )");
		sbQuery.append("				OR ( (mo.operation_state = :settCashState and po.operation_part = :termPart)");
		sbQuery.append("					AND ( (mo.IND_TERM_PREPAID = :indNo and mo.IND_TERM_EXTENDED = :indNo");
		sbQuery.append("					AND trunc(mo.term_settlement_date) = trunc(:settlementDate))");
		sbQuery.append("					OR (mo.IND_TERM_PREPAID = :indYes AND trunc(mo.TERM_PREPAID_DATE) = trunc(:settlementDate))");
		sbQuery.append("					OR (mo.IND_TERM_EXTENDED = :indYes AND trunc(mo.TERM_EXTENDED_DATE) = trunc(:settlementDate)) ) ) )");
		sbQuery.append("	UNION ALL");
		sbQuery.append("	SELECT NVL(SUM(mo.cash_settlement_amount),0) as amount");
		sbQuery.append("	FROM mechanism_operation mo, modality_group mg,"); 
		sbQuery.append("		modality_group_detail mgd");        
		sbQuery.append("	WHERE mo.id_negotiation_mechanism_fk = mgd.id_negotiation_mechanism_fk");
		sbQuery.append("	AND mo.id_negotiation_modality_fk = mgd.id_negotiation_modality_fk");		
		sbQuery.append("	AND mgd.id_modality_group_fk = mg.id_modality_group_pk");
		sbQuery.append("	AND mg.ID_MODALITY_GROUP_PK = :idModalityGroupPk");
		sbQuery.append("	AND mo.id_negotiation_mechanism_fk = :idNegotiationMechanismPk");
		if(ParticipantRoleType.BUY.getCode().equals(role))
			sbQuery.append("	AND mo.id_buyer_participant_fk = :idParticipantPk");
		else
			sbQuery.append("	AND mo.id_seller_participant_fk = :idParticipantPk");
		sbQuery.append("	AND mo.operation_state = :regState");
		sbQuery.append("	AND mo.CASH_SETTLEMENT_CURRENCY = :currency");
		sbQuery.append("	AND mo.settlement_type = :settlementType");
		sbQuery.append("	AND trunc(mo.cash_settlement_Date) = trunc(:settlementDate)");
		sbQuery.append("	) pos");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idParticipantPk", positionByParticipant.getIdParticipantPk());
		query.setParameter("role", role);
		query.setParameter("currency", searchPositionCommissionTO.getCurrencySelected());
		query.setParameter("idModalityGroupPk", positionByParticipant.getIdModalityGroupPk());
		query.setParameter("idNegotiationMechanismPk", searchPositionCommissionTO.getNegoMechanismSelected());
		query.setParameter("inchargeType", InChargeNegotiationType.INCHARGE_FUNDS.getCode());
		query.setParameter("settlementDate", searchPositionCommissionTO.getSettlementDate());
		query.setParameter("settlementType", SettlementType.DVP.getCode());
		query.setParameter("regState", MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.setParameter("assigState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("settCashState", MechanismOperationStateType.CASH_SETTLED.getCode());
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		query.setParameter("termPart", OperationPartType.TERM_PART.getCode());
		query.setParameter("indYes", BooleanType.YES.getCode());
		query.setParameter("indNo", BooleanType.NO.getCode());
		return (BigDecimal)query.getSingleResult();
	}
	
	/**
	 * Gets the cash amount movement by role.
	 *
	 * @param positionByParticipant the position by participant
	 * @param searchPositionCommissionTO the search position commission to
	 * @param moveClass the move class
	 * @return the cash amount movement by role
	 */
	public BigDecimal getCashAmountMovementByRole(PositionParticipantTO positionByParticipant, SearchPositionCommissionTO searchPositionCommissionTO, Integer moveClass){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT NVL(SUM(cam.movementAmount),0)");
		sbQuery.append("	FROM CashAccountMovement cam");
		sbQuery.append("	WHERE cam.currency = :currency");
		sbQuery.append("	AND cam.institutionCashAccount.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	AND cam.institutionCashAccount.modalityGroup.idModalityGroupPk = :idModalityGroupPk");
		sbQuery.append("	AND cam.institutionCashAccount.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	AND cam.fundsOperation.fundsOperationGroup = :fundsOperationGroup");
		sbQuery.append("	AND cam.movementClass = :class");
		sbQuery.append("	AND TRUNC(cam.movementDate) = :settlementDate");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("currency", searchPositionCommissionTO.getCurrencySelected());
		query.setParameter("idParticipantPk", positionByParticipant.getIdParticipantPk());
		query.setParameter("idModalityGroupPk", positionByParticipant.getIdModalityGroupPk());
		query.setParameter("idNegotiationMechanismPk", searchPositionCommissionTO.getNegoMechanismSelected());
		query.setParameter("class", moveClass);	
		query.setParameter("settlementDate", searchPositionCommissionTO.getSettlementDate());
		query.setParameter("fundsOperationGroup", FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode());
		return (BigDecimal)query.getSingleResult();
	}	
		
	/**
	 * Search position by role.
	 *
	 * @param idParticipant the id participant
	 * @param idModalityGroup the id modality group
	 * @param searchPositionCommissionTO the search position commission to
	 * @param role the role
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> searchPositionByRole(Long idParticipant, Long idModalityGroup, SearchPositionCommissionTO searchPositionCommissionTO, Integer role) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT 	MO.mechanisnModality.negotiationModality.modalityName,");//0
		sbQuery.append("	 	mo.operationNumber,");//1
		sbQuery.append("		mo.operationDate,");//2
		sbQuery.append("		so.stockQuantity,");//3
		sbQuery.append("		ps.settlementAmount,");//4
		sbQuery.append("		mo.securities.idSecurityCodePk,");//5
		sbQuery.append("		so.settlementPrice,");//6
		sbQuery.append("		mo.buyerParticipant.idParticipantPk,");//7
		sbQuery.append("		mo.buyerParticipant.description,");//8
		sbQuery.append("		mo.buyerParticipant.mnemonic ,");//9
		sbQuery.append("		mo.sellerParticipant.idParticipantPk,");//10
		sbQuery.append("		mo.sellerParticipant.description,");//11
		sbQuery.append("		mo.sellerParticipant.mnemonic,");//12
		sbQuery.append("		so.operationPart,");//13
		sbQuery.append("		so.indPartial ,");//14
		sbQuery.append("		(SELECT parameterName FROM ParameterTable where parameterTablePk = so.operationState) ,");//15
		sbQuery.append("		mo.idMechanismOperationPk, ");//16
		sbQuery.append("		so.idSettlementOperationPk, ");//17
		sbQuery.append("		mo.ballotNumber,");//18
		sbQuery.append("		mo.sequential ");//19
		sbQuery.append(" 	from ParticipantSettlement ps, SettlementOperation SO, MechanismOperation MO, MechanismModality MM , ModalityGroupDetail MGD ");
		sbQuery.append(" 	where MO.mechanisnModality.id.idNegotiationMechanismPk = MM.id.idNegotiationMechanismPk ");
		sbQuery.append(" 	and   MO.mechanisnModality.id.idNegotiationModalityPk = MM.id.idNegotiationModalityPk ");
		sbQuery.append(" 	and   MGD.mechanismModality.id.idNegotiationMechanismPk = MM.id.idNegotiationMechanismPk ");
		sbQuery.append(" 	and   MGD.mechanismModality.id.idNegotiationModalityPk = MM.id.idNegotiationModalityPk ");
		sbQuery.append("	and SO.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		sbQuery.append("	and ps.settlementOperation.idSettlementOperationPk = SO.idSettlementOperationPk ");
		sbQuery.append(" 	and MM.id.idNegotiationMechanismPk = :idMechanism ");
		sbQuery.append(" 	and SO.settlementCurrency = :idCurrency ");
		sbQuery.append("	AND ps.role = :idRole ");
		sbQuery.append("	AND so.settlementType = :settlementType");
		sbQuery.append("	AND so.indExtended = :indExtended");
		sbQuery.append("	AND mo.settlementSchema = :settlementSchema ");
		sbQuery.append("	AND ps.participant.idParticipantPk = :idParticipant ");
		sbQuery.append("	AND MGD.modalityGroup.idModalityGroupPk = :idModalityGroup ");
		sbQuery.append("	and PS.operationState in (:partSettStates ) ");
		sbQuery.append("	and SO.settlementDate = :settlementDate ");
		sbQuery.append("	and ( ( SO.operationPart = :cashPart and SO.operationState in (:cashStates) ) ");
		sbQuery.append("	   or ( SO.operationPart = :termPart and SO.operationState in (:termStates) ) ) ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters .put("idCurrency", searchPositionCommissionTO.getCurrencySelected());
		parameters.put("idRole", role);
		parameters.put("idParticipant", idParticipant);
		parameters.put("idMechanism", searchPositionCommissionTO.getNegoMechanismSelected());
		parameters.put("idModalityGroup", idModalityGroup);
		//parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("settlementDate", searchPositionCommissionTO.getSettlementDate());
		parameters.put("settlementType", SettlementType.DVP.getCode());
		parameters.put("settlementSchema", searchPositionCommissionTO.getSettlementSchemaSelected());
		
		ArrayList<Integer> cashStates = new ArrayList<Integer>();
		cashStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
	    cashStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
	    
	    ArrayList<Integer> termStates = new ArrayList<Integer>();
	    termStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		    
	    ArrayList<Integer> partSettstates = new ArrayList<Integer>();
	    partSettstates.add(ParticipantOperationStateType.CONFIRM.getCode());
	    
	    parameters.put("partSettStates",partSettstates);
		parameters.put("cashStates", cashStates);
		parameters.put("termStates", termStates);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
	    parameters.put("termPart", ComponentConstant.TERM_PART);
	    if(searchPositionCommissionTO.isExtended()){
	    	parameters.put("indExtended", ComponentConstant.ONE);
	    }else{
	    	parameters.put("indExtended", ComponentConstant.ZERO);
	    }
	    
		return findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the list modality group.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the list modality group
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ModalityGroup> getListModalityGroup(Long idNegotiationMechanismPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct mg FROM ModalityGroup mg");
		sbQuery.append("	WHERE mg.groupState = :state");
		sbQuery.append("	AND mg.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	ORDER BY mg.groupName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ComponentConstant.ONE);
		query.setParameter("idNegotiationMechanismPk", idNegotiationMechanismPk);
		return (List<ModalityGroup>)query.getResultList();
	}
	
	
	/**
	 * Issue 2829
	 * Get Operations With Same Security .
	 *
	 * @param operationDate the operation date
	 * @return the operations with same security
	 */
	public List<Object[]> getOperationsWithSameSecurity(Date operationDate){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" select T.participante,T.fecha,T.valor, ");
		sbQuery.append(" 		listagg(T.operacion, ', ') ");
		sbQuery.append(" 		within group (order by T.operacion) as operacion ");
		sbQuery.append(" from ");
		sbQuery.append(" ( ");
		sbQuery.append("       select  ");
		sbQuery.append("             (part_seller.id_participant_pk || ' - ' || part_seller.description) as participante, ");
		sbQuery.append("             trunc(mo.operation_date) as fecha, ");
		sbQuery.append("             (mo.ballot_number || '/' || mo.sequential) as operacion, ");
		sbQuery.append("             mo.id_security_code_fk as valor, ");
		sbQuery.append("             'VENTA' AS descripcion ");
		sbQuery.append("       from mechanism_operation mo 														 ");
		sbQuery.append(" 					 inner join participant part_seller on part_seller.id_participant_pk=mo.id_seller_participant_fk 	 ");
		sbQuery.append("            inner join security se on se.id_security_code_pk = mo.id_security_code_fk 			 ");
		sbQuery.append("        where mo.id_negotiation_mechanism_fk=1 													 ");
		sbQuery.append("           and se.security_class not in (420, 421) ");
		sbQuery.append(" 					and trunc(mo.operation_date) = trunc(:operationDate) ");
		sbQuery.append("           and mo.id_seller_participant_fk <> mo.id_buyer_participant_fk ");
		sbQuery.append("           and exists ( ");
		sbQuery.append("               select 1 ");
		sbQuery.append("                 from mechanism_operation mo_sub 														 ");
		sbQuery.append("                  inner join security se_sub on se_sub.id_security_code_pk = mo_sub.id_security_code_fk 			 ");
		sbQuery.append("                 where mo_sub.id_negotiation_mechanism_fk=1							 ");
		sbQuery.append("                 and se_sub.security_class not in (420, 421) ");
		sbQuery.append("                 and trunc(mo_sub.operation_date) = trunc(:operationDate) ");
		sbQuery.append("                 and mo_sub.id_seller_participant_fk <> mo_sub.id_buyer_participant_fk ");
		sbQuery.append("                 and mo_sub.id_security_code_fk = mo.id_security_code_fk ");
		sbQuery.append("                 and mo_sub.id_buyer_participant_fk = part_seller.id_participant_pk ");
		sbQuery.append("           ) ");
		sbQuery.append("  ");
		sbQuery.append(" union all ");
		sbQuery.append("  ");
		sbQuery.append("       select  ");
		sbQuery.append("           (part_buyer.id_participant_pk || ' - ' || part_buyer.description) as participante, ");
		sbQuery.append("           trunc(mo.operation_date) as fecha, ");
		sbQuery.append("           (mo.ballot_number || '/' || mo.sequential) as operacion, ");
		sbQuery.append("           mo.id_security_code_fk as valor,  ");
		sbQuery.append("           'COMPRA' AS descripcion ");
		sbQuery.append("       from mechanism_operation mo 														 ");
		sbQuery.append("            inner join participant part_buyer on part_buyer.id_participant_pk=mo.id_buyer_participant_fk 	 ");
		sbQuery.append("            inner join security se on se.id_security_code_pk = mo.id_security_code_fk 			 ");
		sbQuery.append("       where mo.id_negotiation_mechanism_fk=1 													 ");
		sbQuery.append("           and se.security_class not in (420, 421) ");
		sbQuery.append(" 					and trunc(mo.operation_date) = trunc(:operationDate) ");
		sbQuery.append("           and mo.id_seller_participant_fk <> mo.id_buyer_participant_fk ");
		sbQuery.append("           and exists ( ");
		sbQuery.append("               select 1 ");
		sbQuery.append("                 from mechanism_operation mo_sub2 														 ");
		sbQuery.append("                  inner join security se_sub2 on se_sub2.id_security_code_pk = mo_sub2.id_security_code_fk 			 ");
		sbQuery.append("                 where mo_sub2.id_negotiation_mechanism_fk=1							 ");
		sbQuery.append("                 and se_sub2.security_class not in (420, 421) ");
		sbQuery.append("                 and trunc(mo_sub2.operation_date) = trunc(:operationDate) ");
		sbQuery.append("                 and mo_sub2.id_seller_participant_fk <> mo_sub2.id_buyer_participant_fk ");
		sbQuery.append("                 and mo_sub2.id_security_code_fk = mo.id_security_code_fk ");
		sbQuery.append("                 and mo_sub2.id_seller_participant_fk = part_buyer.id_participant_pk ");
		sbQuery.append("           ) ");
		sbQuery.append(" ) T ");
		sbQuery.append(" group by  ");
		sbQuery.append(" T.participante,T.fecha,T.valor  ");
		sbQuery.append(" order by  ");
		sbQuery.append(" T.participante,T.fecha,T.valor ");
		sbQuery.append("  ");
				
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("operationDate", operationDate);
		
		return query.getResultList();
	}
		
	/**
	 * Calculate net participant positions.
	 *
	 * @param mechanismId the mechanism id
	 * @param settlementDate the settlement date
	 * @param modalityGroupId the modality group id
	 * @param currency the currency
	 * @param scheduleType the schedule type
	 * @param loggerUser the logger user
	 * @param autoRetire the auto retire
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantPosition> calculateNetParticipantPositions(Long mechanismId, Date settlementDate, Long modalityGroupId, 
			Integer currency, boolean autoRetire,NetSettlementAttributesTO netSettlementAttributesTO) throws ServiceException {
				
		SettlementProcess settlementProcess = new SettlementProcess();				

		ModalityGroup objModalityGroup = new ModalityGroup();
		objModalityGroup.setIdModalityGroupPk(modalityGroupId);
		settlementProcess.setModalityGroup(objModalityGroup);
		NegotiationMechanism objNegotiationMechanism = new NegotiationMechanism();
		objNegotiationMechanism.setIdNegotiationMechanismPk(mechanismId);
		settlementProcess.setNegotiationMechanism(objNegotiationMechanism);
		settlementProcess.setCurrency(currency);
		settlementProcess.setSettlementDate(settlementDate);		
		settlementProcess.setPendingOperations(new Long(0));
		settlementProcess.setSettledOperations(new Long(0));
		settlementProcess.setCanceledOperations(new Long(0));
		settlementProcess.setTotalOperation(new Long(0));		
		settlementProcess.setProcessState(SettlementProcessStateType.IN_PROCESS.getCode());	
		
		settlementProcess.setOperationSettlements(new ArrayList<OperationSettlement>());
		settlementProcess.setParticipantPositions(new ArrayList<ParticipantPosition>());
		
		netSettlementAttributesTO.setSettlementProcess(settlementProcess);
		
		populateNetSettlementOperations(settlementDate,mechanismId,modalityGroupId,currency,netSettlementAttributesTO);
		
		//populate participant positions and operations WITHOUT considering retired operations
		Map<Long, NetParticipantPositionTO> mapParticipantPositions = NegotiationUtils.populateParticipantPositions(
																				netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		
		//we verify and remove the chained operation by stock 
		settlementProcessServiceBean.removeOperationSettlementProcessByStock(netSettlementAttributesTO);
		
		// we recalculate net positions and create new participantPositions
		return settlementsComponentSingleton.recalculateParticipantPositions(netSettlementAttributesTO ,autoRetire);				
	}
	
	public void populateNetSettlementOperations(Date settlementDate, Long mechanismId, 
			Long modalityGroupId, Integer currency, NetSettlementAttributesTO netSettlementAttributesTO) throws ServiceException {
		
		//remove operations by securities
		SettlementProcess settlementProcess = netSettlementAttributesTO.getSettlementProcess();
		
		boolean extended = false;
		if(netSettlementAttributesTO.getSettlementSchedule() != null && 
				ComponentConstant.ONE.equals(netSettlementAttributesTO.getSettlementSchedule().getIndExtendSettlement())){
			extended = true;
		}
		boolean settlement = false;
		if(netSettlementAttributesTO.getSettlementSchedule() != null && 
				ComponentConstant.ONE.equals(netSettlementAttributesTO.getSettlementSchedule().getIndSettlement())){
			settlement = true;
		}
		
		//Get id Participant filter search
		Long idParticipantPk = netSettlementAttributesTO.getIdParticipantPk();
		
		List<Object[]> pendingByStockObjects = settlementProcessServiceBean.getListNetPendingOperations(settlementDate, mechanismId, modalityGroupId, currency,true,false,false,settlement,extended, idParticipantPk);
		
		List<NetSettlementOperationTO> operationsExcludedByStocks = NegotiationUtils.populateSettlementOperationsAndParticipant(pendingByStockObjects);
		
		for(NetSettlementOperationTO settlementOperation: operationsExcludedByStocks){
			OperationSettlement operationSettlement = new OperationSettlement();
			operationSettlement.setSettlementProcess(settlementProcess);
			operationSettlement.setOperationState(OperationSettlementSateType.WITHDRAWN_BY_SECURITIES.getCode());
			operationSettlement.setInitialState(operationSettlement.getOperationState());
			operationSettlement.setSettlementOperation(new SettlementOperation(settlementOperation.getIdSettlementOperation()));
			settlementOperation.setOperationSettlement(operationSettlement);
			netSettlementAttributesTO.getLstStockRemovedOperations().add(settlementOperation);
			settlementProcess.getOperationSettlements().add(operationSettlement);
		}
		
		//remove operations by guarantees
		List<Object[]> pendingByGuaranteesObjects = settlementProcessServiceBean.getListNetPendingOperations(settlementDate, mechanismId, modalityGroupId, currency, false,true,false,settlement,extended, idParticipantPk);
		List<NetSettlementOperationTO> operationsExcludedByGuarantees = NegotiationUtils.populateSettlementOperationsAndParticipant(pendingByGuaranteesObjects);
		
		for(NetSettlementOperationTO settlementOperation: operationsExcludedByGuarantees){
			OperationSettlement operationSettlement = new OperationSettlement();
			operationSettlement.setSettlementProcess(settlementProcess);
			operationSettlement.setOperationState(OperationSettlementSateType.WITHDRAWN_BY_GUARANTEES.getCode());
			operationSettlement.setInitialState(operationSettlement.getOperationState());
			operationSettlement.setSettlementOperation(new SettlementOperation(settlementOperation.getIdSettlementOperation()));
			settlementOperation.setOperationSettlement(operationSettlement);
			netSettlementAttributesTO.getLstGuaranteeRemovedOperations().add(settlementOperation);
			settlementProcess.getOperationSettlements().add(operationSettlement);
		}
		
		// get funds pending operations
		List<Object[]> pendingByFundsObjects = settlementProcessServiceBean.getListNetPendingOperations(settlementDate, mechanismId,modalityGroupId, currency, false,false,true,settlement,extended, idParticipantPk);
		//we add the chained operations
		pendingByFundsObjects.addAll(settlementProcessServiceBean.getListChainedOperation(settlementDate, mechanismId, modalityGroupId, currency,settlement,extended));
		List<NetSettlementOperationTO> operationsExcludedByFunds = NegotiationUtils.populateSettlementOperationsAndParticipant(pendingByFundsObjects);
		
		for(NetSettlementOperationTO settlementOperation: operationsExcludedByFunds){
			OperationSettlement operationSettlement = new OperationSettlement();
			operationSettlement.setSettlementProcess(settlementProcess);
			operationSettlement.setOperationState(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode());
			operationSettlement.setInitialState(operationSettlement.getOperationState());
			operationSettlement.setSettlementOperation(new SettlementOperation(settlementOperation.getIdSettlementOperation()));
			settlementOperation.setOperationSettlement(operationSettlement);
			netSettlementAttributesTO.getLstPendingSettlementOperations().add(settlementOperation);
			settlementProcess.getOperationSettlements().add(operationSettlement);
		}
		
		Long pending = (long) operationsExcludedByFunds.size();
		Long removed = (long) (operationsExcludedByGuarantees.size()+operationsExcludedByStocks.size());
		Long total = pending + removed;
		
		settlementProcess.setCanceledOperations(0l);
		settlementProcess.setPendingOperations(pending);
		settlementProcess.setRemovedOperations(removed);
		settlementProcess.setSettledOperations(0l);
		settlementProcess.setTotalOperation(total);
		
		return;
	}
	
	
	/**
	 * Gets the list mechanismOperation operation manual registered 
	 * 
	 * Issue 0096
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> findOperationManual () throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mcn FROM MechanismOperation mcn");
		sbQuery.append("	WHERE 1=1 ");
		sbQuery.append("	AND mcn.mcnProcessFile.idMcnProcessFilePk is null");
		sbQuery.append("	AND trunc(mcn.registerDate) = :registerDate ");
		sbQuery.append("	AND mcn.operationState = :state ");
		sbQuery.append("	AND mcn.mechanisnModality.negotiationModality.idNegotiationModalityPk in (:modality) ");
		// sbQuery.append("	ORDER BY mg.groupName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.setParameter("registerDate", CommonsUtilities.currentDate());
		
		 ArrayList<Long> lstModality = new ArrayList<Long>();
		 lstModality.add(NegotiationModalityType.REPORT_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.REPORT_EQUITIES.getCode());
		 query.setParameter("modality", lstModality);
		
		return (List<MechanismOperation>)query.getResultList();
	}
	
	/**
	 * Gets the list mechanismOperation operation manual registered 
	 * 
	 * Issue 0096
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public Boolean existPendingCancelOperations (String username) throws ServiceException{
		
		Boolean result = false;
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mcn FROM MechanismOperation mcn");
		sbQuery.append("	inner join fetch mcn.buyerParticipant bp  ");
		sbQuery.append("	inner join fetch mcn.sellerParticipant sp  ");
		sbQuery.append("	WHERE 1=1 ");
		sbQuery.append("	AND mcn.mcnProcessFile.idMcnProcessFilePk is not null");
		sbQuery.append("	AND trunc(mcn.registerDate) = :registerDate ");
		sbQuery.append("	AND mcn.operationState = :state ");
		sbQuery.append("	AND mcn.mechanisnModality.negotiationModality.idNegotiationModalityPk in (:modality) ");
		// sbQuery.append("	ORDER BY mg.groupName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", MechanismOperationStateType.PENDING_CANCEL_STATE.getCode());
		query.setParameter("registerDate", CommonsUtilities.currentDate());
		
		 ArrayList<Long> lstModality = new ArrayList<Long>();
		 
		 lstModality.add(NegotiationModalityType.REPORT_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.REPORT_EQUITIES.getCode());
		 lstModality.add(NegotiationModalityType.CASH_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.CASH_EQUITIES.getCode());
		 lstModality.add(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode());
		 
		 query.setParameter("modality", lstModality);
		
		 List<MechanismOperation> resl = new ArrayList<MechanismOperation>();
		 
		 resl = (List<MechanismOperation>)query.getResultList();
		 
		 if(resl.size() > GeneralConstants.ZERO_VALUE_INTEGER) {
			 
			 //Solo si el usuario que intenta registrar pertenece a una agencia que tenga operaciones pendientes de cancelar
			 for(MechanismOperation opera : resl) {
				 if(opera.getBuyerParticipant().getMnemonic().equals(username.substring(0, 3))
						 || opera.getSellerParticipant().getMnemonic().equals(username.substring(0, 3)) ) {
					 result = true;
					 break;
				 }
			 }
		 }
		 
		return result;
	}
	
	/**
	 * Gets the list mechanismOperation operation manual registered 
	 * 
	 * Issue 0096
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> findOperationsRegisteredToday () throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mcn FROM MechanismOperation mcn");
		sbQuery.append("	WHERE 1=1 ");
		sbQuery.append("	AND mcn.mcnProcessFile.idMcnProcessFilePk is not null");
		sbQuery.append("	AND trunc(mcn.registerDate) = :registerDate ");
		sbQuery.append("	AND mcn.operationState in ( :state ) ");
		sbQuery.append("	AND mcn.mechanisnModality.negotiationModality.idNegotiationModalityPk in (:modality) ");
		// sbQuery.append("	ORDER BY mg.groupName ASC");
		Query query = em.createQuery(sbQuery.toString());
		
		ArrayList<Integer> lstState = new ArrayList<Integer>();
		lstState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		//lstState.add(MechanismOperationStateType.PENDING_CANCEL_STATE.getCode());
		 
		query.setParameter("state", lstState);
		
		query.setParameter("registerDate", CommonsUtilities.currentDate());
		
		 ArrayList<Long> lstModality = new ArrayList<Long>();
		 
		 lstModality.add(NegotiationModalityType.REPORT_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.REPORT_EQUITIES.getCode());
		 lstModality.add(NegotiationModalityType.CASH_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.CASH_EQUITIES.getCode());
		 lstModality.add(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode());
		 
		 query.setParameter("modality", lstModality);
		
		return (List<MechanismOperation>)query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> findOperationsPendingToday () throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mcn FROM MechanismOperation mcn");
		sbQuery.append("	WHERE 1=1 ");
		sbQuery.append("	AND mcn.mcnProcessFile.idMcnProcessFilePk is not null");
		sbQuery.append("	AND trunc(mcn.registerDate) = :registerDate ");
		sbQuery.append("	AND mcn.operationState in ( :state ) ");
		sbQuery.append("	AND mcn.mechanisnModality.negotiationModality.idNegotiationModalityPk in (:modality) ");
		// sbQuery.append("	ORDER BY mg.groupName ASC");
		Query query = em.createQuery(sbQuery.toString());
		
		ArrayList<Integer> lstState = new ArrayList<Integer>();
		//lstState.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstState.add(MechanismOperationStateType.PENDING_CANCEL_STATE.getCode());
		 
		query.setParameter("state", lstState);
		
		query.setParameter("registerDate", CommonsUtilities.currentDate());
		
		 ArrayList<Long> lstModality = new ArrayList<Long>();
		 
		 lstModality.add(NegotiationModalityType.REPORT_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.REPORT_EQUITIES.getCode());
		 lstModality.add(NegotiationModalityType.CASH_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.CASH_EQUITIES.getCode());
		 lstModality.add(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode());
		 lstModality.add(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode());
		 
		 query.setParameter("modality", lstModality);
		
		return (List<MechanismOperation>)query.getResultList();
	}
	
	/**
	 * issue 1141 adicionar un dia al vencimiento de reportos por contingencia MECHANISM_OPERATION
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateOperationPendingCancel (Long idMechanismOperationPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update MECHANISM_OPERATION ");
		sbQuery.append("		set OPERATION_STATE = :state ");
		sbQuery.append("	where ID_MECHANISM_OPERATION_PK = :idMechanismOperationPk ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("state", MechanismOperationStateType.PENDING_CANCEL_STATE.getCode());
		query.executeUpdate();
	}
	
	/**
	 * issue 1141 adicionar un dia al vencimiento de reportos por contingencia MECHANISM_OPERATION
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateOperationRegisterCancel (Long idMechanismOperationPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update MECHANISM_OPERATION ");
		sbQuery.append("		set OPERATION_STATE = :state ");
		sbQuery.append("	where ID_MECHANISM_OPERATION_PK = :idMechanismOperationPk ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("state", MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.executeUpdate();
	}
	
	
	
	
	/**
	 * Metodo que verifica si un archivo de asignaciones se encuentra en proceso
	 * @param processFileTO
	 * @return
	 */
	public boolean verifyFileAssignmentProcess(ProcessFileTO processFileTO) {
		
		Boolean inProcess = false;
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mpf FROM McnProcessFile mpf");
		sbQuery.append("	WHERE 1=1 ");
		sbQuery.append("	AND trunc(mpf.processDate) = :registerDate ");
		sbQuery.append("	AND mpf.processState = :state ");
		sbQuery.append("	AND mpf.processType = :type ");
		sbQuery.append("	AND mpf.negotiationMechanism.idNegotiationMechanismPk =:idMechanism ");
		sbQuery.append("	AND mpf.fileName = :fileName ");
		
		if (Validations.validateIsNotNullAndNotEmpty(processFileTO.getIdParticipantPk())){
			sbQuery.append("	AND mpf.participant.idParticipantPk = :participant ");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("type", MechanismFileProcessType.MCN_ASSIGNMENT_UPLOAD.getCode());
		query.setParameter("state", ExternalInterfaceReceptionStateType.PENDING.getCode());
		query.setParameter("registerDate", CommonsUtilities.currentDate());
		query.setParameter("idMechanism", processFileTO.getIdNegotiationMechanismPk());
		query.setParameter("fileName", processFileTO.getFileName());
		
		if (Validations.validateIsNotNullAndNotEmpty(processFileTO.getIdParticipantPk())){
			query.setParameter("participant", processFileTO.getIdParticipantPk());
		}
		
		query.getResultList();
		
		if(query.getResultList().size() > GeneralConstants.ZERO_VALUE_INT){
			inProcess = true;
		}
		return inProcess;
	}
	
	/**
	 * issue 1141 adicionar un dia al vencimiento de reportos por contingencia MECHANISM_OPERATION
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateMechanismOperation (Long idMechanismOperationPk, Date settlementDateAdd, Long settlementDaysAdd,
			BigDecimal settlementPriceAdd, BigDecimal settlementAmountAdd) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update MECHANISM_OPERATION ");
		sbQuery.append("		set TERM_SETTLEMENT_DATE = :settlementDateAdd, ");
		sbQuery.append("			TERM_PRICE = :settlementPriceAdd, ");
		sbQuery.append("			TERM_AMOUNT = :settlementAmountAdd, ");
		sbQuery.append("			TERM_SETTLEMENT_DAYS = :settlementDaysAdd, ");
		sbQuery.append("			TERM_SETTLEMENT_PRICE = :settlementPriceAdd, ");
		sbQuery.append("			TERM_SETTLEMENT_AMOUNT = :settlementAmountAdd ");
		sbQuery.append("	where ID_MECHANISM_OPERATION_PK = :idMechanismOperationPk ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("settlementDateAdd", settlementDateAdd);
		query.setParameter("settlementPriceAdd", settlementPriceAdd);
		query.setParameter("settlementAmountAdd", settlementAmountAdd);
		query.setParameter("settlementDaysAdd", settlementDaysAdd);
		query.executeUpdate();
	}		
	
	
	
	
		
	
	
	
	
	/**
	 * Metodo para actualizar el estado de las SEDIR
	 * @param idSettlementReport
	 */
	public void updateSettlementReport (Long idSettlementReport, Integer state, String userName, Date date, Date newDate) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update SETTLEMENT_REPORT ");
		sbQuery.append("		set REQUEST_STATE = :state ");
		
		if(state.equals(SettlementDateRequestStateType.APPROVED.getCode())) {
			sbQuery.append("		, APPROVAL_USER = :userName ");
			sbQuery.append("		, APPROVAL_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.ANNULATE.getCode())) {
			sbQuery.append("		, ANNUL_USER = :userName ");
			sbQuery.append("		, ANNUL_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.REVIEW.getCode())) {
			sbQuery.append("		, REVIEW_USER = :userName ");
			sbQuery.append("		, REVIEW_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.REJECTED.getCode())) {
			sbQuery.append("		, REJECT_USER = :userName ");
			sbQuery.append("		, REJECT_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.CONFIRMED.getCode())) {
			sbQuery.append("		, CONFIRM_USER = :userName ");
			sbQuery.append("		, CONFIRM_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.AUTHORIZE.getCode())) {
			sbQuery.append("		, AUTHORIZE_USER = :userName ");
			sbQuery.append("		, AUTHORIZE_DATE = :date ");
			sbQuery.append("		, NEW_DATE = :newDate ");
		}
		
		sbQuery.append("	where ID_SETTLEMENT_REPORT_PK = :idSettlementReport ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idSettlementReport", idSettlementReport);
		query.setParameter("state", state);
		query.setParameter("userName", userName);
		query.setParameter("date", date);
		
		if(Validations.validateIsNotNullAndNotEmpty(newDate)) {
			query.setParameter("newDate", newDate);
		}
		
		query.executeUpdate();
	}	
	
	/**
	 * Metodo para actualizar el estado de las SEDIR
	 * @param idSettlementReport
	 */
	public void updateUnfulfillmentMechOperaSett (Long idMechanismOperation, Long idSettlementOperation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update SETTLEMENT_OPERATION ");
		sbQuery.append("		set OPERATION_STATE = :state ");
		sbQuery.append("	where ID_SETTLEMENT_OPERATION_PK = :idSettlementReport ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idSettlementReport", idMechanismOperation);
		query.setParameter("state", MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
		query.executeUpdate();
		
		StringBuilder sbQueryV = new StringBuilder();
		sbQueryV.append("	update MECHANISM_OPERATION ");
		sbQueryV.append("		set OPERATION_STATE = :state ");
		sbQueryV.append("	where ID_MECHANISM_OPERATION_PK = :idSettlementReport ");
		Query queryV = em.createNativeQuery(sbQueryV.toString());
		queryV.setParameter("idSettlementReport", idSettlementOperation);
		queryV.setParameter("state", MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
		queryV.executeUpdate();
		
	}	
	
	/**
	 * 
	 */
	public SettlementUnfulfillment getSettlementOperation (Long idSett) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT SU FROM SettlementUnfulfillment SU");
		sbQuery.append("	inner join fetch SU.operationUnfulfillment OU ");
		sbQuery.append("	inner join fetch OU.settlementOperation SO ");
		sbQuery.append("	WHERE 1=1 ");
		sbQuery.append("	AND SU.idSettlementUnfulfillmentPk = :idSett ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSett", idSett);
		
		return (SettlementUnfulfillment)query.getSingleResult();
	}
	
	
	/**
	 * 
	 * @param idMechanism
	 * @return
	 */
	public Boolean searchOperationExist (Long idMechanism) {
		
		Boolean result = false;
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  select MO.ID_MECHANISM_OPERATION_PK                                                                             ");
		querySql.append("  from SETTLEMENT_UNFULFILLMENT SU                                                                                ");
		querySql.append("  INNER JOIN OPERATION_UNFULFILLMENT OU ON OU.ID_OPERATION_UNFULFILLMENT_PK = SU.ID_OPERATION_UNFULFILLMENT_FK    ");
		querySql.append("  INNER JOIN SETTLEMENT_OPERATION SO on SO.ID_SETTLEMENT_OPERATION_PK = OU.ID_SETTLEMENT_OPERATION_FK             ");
		querySql.append("  INNER JOIN MECHANISM_OPERATION MO ON SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                ");
		querySql.append("  WHERE MO.ID_MECHANISM_OPERATION_PK = :idMechanism                                                               ");
		querySql.append("  AND SU.REQUEST_STATE IN ( 1474, 1482, 1543, 1227, 1229)                                                         ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idMechanism", idMechanism);

		@SuppressWarnings("unchecked")
		List<Object[]> lstRestul = query.getResultList();
		
		List<Long> test = new ArrayList<Long>();
		
		if(lstRestul.size() > GeneralConstants.ZERO_VALUE_INTEGER) {
			result = true;
		}
		
		return result;
	}
	

	/**
	 * 
	 * @param idMechanism
	 * @return
	 */
	public List<Long> searchOperationExists () {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  select MO.ID_MECHANISM_OPERATION_PK                                                                             ");
		querySql.append("  from SETTLEMENT_UNFULFILLMENT SU                                                                                ");
		querySql.append("  INNER JOIN OPERATION_UNFULFILLMENT OU ON OU.ID_OPERATION_UNFULFILLMENT_PK = SU.ID_OPERATION_UNFULFILLMENT_FK    ");
		querySql.append("  INNER JOIN SETTLEMENT_OPERATION SO on SO.ID_SETTLEMENT_OPERATION_PK = OU.ID_SETTLEMENT_OPERATION_FK             ");
		querySql.append("  INNER JOIN MECHANISM_OPERATION MO ON SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                ");
		querySql.append("  WHERE 1 = 1                                                           ");
		//querySql.append("  WHERE MO.ID_MECHANISM_OPERATION_PK = :idMechanism                                                               ");
		querySql.append("  AND SU.REQUEST_STATE IN ( 1474, 1482, 1543, 1227, 1229)                                                         ");
		
		Query query = em.createNativeQuery(querySql.toString());
		//query.setParameter("idMechanism", idMechanism);

		List<BigDecimal> lstRestul = (List<BigDecimal>)query.getResultList();
		
		List<Long> test = new ArrayList<Long>();
		List<String> testx = new ArrayList<String>();
		
		for (int i = 0; i < lstRestul.size(); i++) {
			//testx.add(lstRestul.get(i).toString());
			test.add(Long.valueOf(lstRestul.get(i).toString()));
		}
		
		return test;
	}
	
	
	/**
	 * Metodo para actualizar el estado de las SEDIR
	 * @param idSettlementReport
	 */
	public void updateSettlementUnfulfillment (Long idSettlementReport, Integer state, String userName, Date date) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update SETTLEMENT_UNFULFILLMENT ");
		sbQuery.append("		set REQUEST_STATE = :state ");
		
		if(state.equals(SettlementDateRequestStateType.APPROVED.getCode())) {
			sbQuery.append("		, APPROVAL_USER = :userName ");
			sbQuery.append("		, APPROVAL_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.ANNULATE.getCode())) {
			sbQuery.append("		, ANNUL_USER = :userName ");
			sbQuery.append("		, ANNUL_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.REVIEW.getCode())) {
			sbQuery.append("		, REVIEW_USER = :userName ");
			sbQuery.append("		, REVIEW_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.REJECTED.getCode())) {
			sbQuery.append("		, REJECT_USER = :userName ");
			sbQuery.append("		, REJECT_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.CONFIRMED.getCode())) {
			sbQuery.append("		, CONFIRM_USER = :userName ");
			sbQuery.append("		, CONFIRM_DATE = :date ");
		}else if(state.equals(SettlementDateRequestStateType.AUTHORIZE.getCode())) {
			sbQuery.append("		, AUTHORIZE_USER = :userName ");
			sbQuery.append("		, AUTHORIZE_DATE = :date ");
			//sbQuery.append("		, NEW_DATE = :newDate ");
		}
		
		sbQuery.append("	where ID_SETTLEMENT_UNFULFILLMENT_PK = :idSettlementReport ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idSettlementReport", idSettlementReport);
		query.setParameter("state", state);
		query.setParameter("userName", userName);
		query.setParameter("date", date);
		
		query.executeUpdate();
	}	
	
	
	
	/**
	 * issue 1141 adicionar un dia al vencimiento de reportos por contingencia SETTLEMENT_OPERATION
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateSettlementOperation (Long idMechanismOperationPk, Date settlementDateAdd, Long settlementDaysAdd,
			BigDecimal settlementPriceAdd, BigDecimal settlementAmountAdd) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update SETTLEMENT_OPERATION ");
		sbQuery.append("		set SETTLEMENT_DATE = :settlementDateAdd, ");
		sbQuery.append("			SETTLEMENT_AMOUNT = :settlementAmountAdd, ");
		sbQuery.append("			SETTLEMENT_PRICE = :settlementPriceAdd, ");
		sbQuery.append("			SETTLEMENT_DAYS = :settlementDaysAdd, ");
		sbQuery.append("			INITIAL_SETTLEMENT_PRICE = :settlementPriceAdd ");
		sbQuery.append("	where ID_MECHANISM_OPERATION_FK = :idMechanismOperationPk ");
		sbQuery.append("		and OPERATION_PART = 2 ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("settlementDateAdd", settlementDateAdd);
		query.setParameter("settlementAmountAdd", settlementAmountAdd);
		query.setParameter("settlementPriceAdd", settlementPriceAdd);
		query.setParameter("settlementDaysAdd", settlementDaysAdd);
		query.executeUpdate();
	}
	
	/**
	 * issue 1184 adicionar un dia al vencimiento de reportos por contingencia SETTLEMENT_OPERATION
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateHolderAccountOperation (Long idMechanismOperationPk, Date settlementDateAdd, Long settlementDaysAdd,
			BigDecimal settlementPriceAdd, BigDecimal settlementAmountAdd) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update HOLDER_ACCOUNT_OPERATION ");
		sbQuery.append("		set CASH_AMOUNT = :settlementAmountAdd, ");
		sbQuery.append("			SETTLEMENT_AMOUNT = :settlementAmountAdd ");
		sbQuery.append("	where ID_MECHANISM_OPERATION_FK = :idMechanismOperationPk ");
		sbQuery.append("		and OPERATION_PART = 2 ");
		sbQuery.append("		and HOLDER_ACCOUNT_STATE = 625 ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("settlementAmountAdd", settlementAmountAdd);
		query.executeUpdate();
	}
	
	/**
	 * issue 1184 adicionar un dia al vencimiento de reportos por contingencia SETTLEMENT_OPERATION
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateSettlementAccountOperation (Long idMechanismOperationPk, Date settlementDateAdd, Long settlementDaysAdd,
			BigDecimal settlementPriceAdd, BigDecimal settlementAmountAdd) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update SETTLEMENT_ACCOUNT_OPERATION ");
		sbQuery.append("		set SETTLEMENT_AMOUNT = :settlementAmountAdd ");
		sbQuery.append("	where ID_SETTLEMENT_ACCOUNT_PK IN ("
				+ " SELECT SAO.ID_SETTLEMENT_ACCOUNT_PK "
				+ " FROM IDEPOSITARY.SETTLEMENT_ACCOUNT_OPERATION SAO "
				+ " INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK "
				+ " WHERE 1 = 1 "
				+ " AND HAO.ID_MECHANISM_OPERATION_FK =:idMechanismOperationPk "
				+ " AND HAO.OPERATION_PART = 2 "
				+ " AND HAO.HOLDER_ACCOUNT_STATE = 625 "
				+ ") ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("settlementAmountAdd", settlementAmountAdd);
		query.executeUpdate();
	}
	
	/**
	 * issue 1184 adicionar un dia al vencimiento de reportos por contingencia SETTLEMENT_OPERATION
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateParticipantSettlement (Long idMechanismOperationPk, Date settlementDateAdd, Long settlementDaysAdd,
			BigDecimal settlementPriceAdd, BigDecimal settlementAmountAdd) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	update PARTICIPANT_SETTLEMENT ");
		sbQuery.append("		set SETTLEMENT_AMOUNT = :settlementAmountAdd ");
		sbQuery.append("	where ID_SETTLEMENT_OPERATION_FK IN ("
				+ " SELECT SO.ID_SETTLEMENT_OPERATION_PK "
				+ " FROM MECHANISM_OPERATION MO "
				+ " INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK "
				+ " WHERE SO.OPERATION_PART = 2 "
				+ " AND MO.ID_MECHANISM_OPERATION_PK =:idMechanismOperationPk "
				+ ") ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("settlementAmountAdd", settlementAmountAdd);
		query.executeUpdate();
	}
	
	/**
	 * 
	 * @return
	 */

	public Integer findDaysMaxAmp(SettlementDateOperation settlementDateOperation, RegisterPrepaidExtendedTO registerPrepaidExtendedTO) {		
		StringBuilder sbQuery = new StringBuilder();
		Integer maxDaysAmp = null;
		sbQuery.append(" SELECT mcn.maxDaysAmp FROM MechanismModality mcn ");
		sbQuery.append(" WHERE settlementSchema = :settlementSchema ");
		sbQuery.append(" AND mcn.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk ");
		sbQuery.append(" AND mcn.negotiationModality.idNegotiationModalityPk = :idNegotiationModalityPk ");
		sbQuery.append(" AND mcn.stateMechanismModality = :stateMechanismModality ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("settlementSchema", settlementDateOperation.getSettlementSchema());
		query.setParameter("idNegotiationMechanismPk",registerPrepaidExtendedTO.getIdNegotiationMechanismPk());
		query.setParameter("idNegotiationModalityPk", registerPrepaidExtendedTO.getIdNegotiationModalityPk());
		query.setParameter("stateMechanismModality", MechanismModalityStateType.ACTIVE.getCode());
		
		try {
			Integer quantity = (Integer) query.getSingleResult();

			maxDaysAmp = Integer.valueOf(quantity.toString());

		} catch (NoResultException e) {
			maxDaysAmp = 1;
		}	
		
		return  maxDaysAmp;		
	}
	
}




