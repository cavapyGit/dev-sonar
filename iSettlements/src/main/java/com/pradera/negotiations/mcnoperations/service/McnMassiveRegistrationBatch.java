package com.pradera.negotiations.mcnoperations.service;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.massive.type.MechanismFileProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class McnMassiveRegistrationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="McnMassiveRegistrationBatch")
@RequestScoped
public class McnMassiveRegistrationBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mcn operation service facade. */
	@EJB
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		System.out.println("************************************* BATCH SERVICE WAS CALLED ******************************************************");
		
		String mappingFile=null;
		String filePath=null;
		Long idMechanismPk = null;
		Long participantId = null;
		String locale=null;
		String fileName=null;
		
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE)){
				filePath = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_STREAM)){
				mappingFile = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_MECHANISM)){
				idMechanismPk = new Long(detail.getParameterValue());
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_LOCALE)){
				locale = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE_NAME)){
				fileName = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_OTC_PART_ID)){
				participantId = Long.parseLong(detail.getParameterValue());
			}
		}
		
		try {
			
			ProcessFileTO processFileTO = new ProcessFileTO();
			processFileTO.setStreamFileDir(mappingFile);
			processFileTO.setTempProcessFile(new File(filePath));
			processFileTO.setProcessFile(FileUtils.readFileToByteArray(processFileTO.getTempProcessFile()));
			processFileTO.setIdNegotiationMechanismPk(idMechanismPk);
			processFileTO.setLocale(new Locale(locale));
			processFileTO.setFileName(fileName);
			processFileTO.setIndAutomaticProcess(GeneralConstants.TWO_VALUE_INTEGER);
			processFileTO.setIdParticipantPk(participantId);
			processFileTO.setInterfaceName(ComponentConstant.INTERFACE_BBV_OPERATIONS);
			processFileTO.setProcessType(MechanismFileProcessType.MCN_OPERATIONS_UPLOAD.getCode());
			processFileTO.setRootTag(NegotiationConstant.MASSIVE_XML_BBV_ROOT_TAG);
			
			CommonsUtilities.validateFileOperationStruct(processFileTO);
				
			Object[] operationsProcess = mcnOperationServiceFacade.saveMechanismOperationsMassively(processFileTO, idepositarySetup.getIndMarketFact());

			Integer count = new Integer(0);
			String messageDuplicated= null;
			
			BigDecimal operationTotal = new BigDecimal(0);
			BigDecimal operationAccepted = new BigDecimal(0);
			BigDecimal operationRejected = new BigDecimal(0);
			
			for (Object operationsProcessValue :operationsProcess) {
				count=count+1;
				if(count==1){
					operationTotal=(BigDecimal) operationsProcessValue;
				}
				if(count==2){
					operationAccepted=(BigDecimal) operationsProcessValue;
				}
				if(count==3){
					operationRejected=(BigDecimal) operationsProcessValue;
				}
				if(count==4){
					messageDuplicated=(String) operationsProcessValue;
				}
			}
			
			Object[] operationsProcessA = {operationTotal,operationAccepted,operationRejected};
			
			/* Notifications, Issue 2829 PERU MANTIS*/
			mcnOperationServiceFacade.notificationConcatenation(processLogger);
			
			/* Notifications, Issue 0096  */
			if (messageDuplicated!=null){
//				Object[] operationsProcessB = {messageDuplicated};
				mcnOperationServiceFacade.notificationDuplicatedSecurity(processLogger,messageDuplicated);
			}
			// Send notifications 			
			sendNotificationProcess(processLogger, operationsProcessA);
				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Send notification process.
	 *
	 * @param processLogger the process logger
	 * @param operationsProcess the operations process
	 */
	public void sendNotificationProcess(ProcessLogger processLogger, Object[] operationsProcess) {		
		notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), null, operationsProcess);				
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
