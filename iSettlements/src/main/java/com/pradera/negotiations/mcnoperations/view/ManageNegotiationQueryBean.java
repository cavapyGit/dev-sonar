package com.pradera.negotiations.mcnoperations.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.negotiations.mcnoperations.to.SearchMCNOperationsTO;
import com.pradera.negotiations.operations.view.OperationBusinessBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageNegotiationQueryBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@LoggerCreateBean
@DepositaryWebBean
public class ManageNegotiationQueryBean extends GenericBaseBean implements Serializable{

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The mcn operation service facade. */
	@EJB
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The max days of custody request. */
	@Inject @Configurable
	Integer maxDaysOfCustodyRequest;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The search operation to. */
	private SearchMCNOperationsTO searchOperationTO;
	
	/** The mechanism user. */
	private boolean cevaldomUser, participantUser, mechanismUser;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The mechanism operation data model. */
	private GenericDataModel<MechanismOperationTO> mechanismOperationDataModel;
	/** objeto lista para obtener el resumen de compras forzosas */
	private GenericDataModel<MechanismOperationTO> mechanismOperationDataModelForcedPurcharse;
	
	/** The Constant SEARCH_MAPPING. */
	private static final String SEARCH_MAPPING = "searchQueryOperation";

	/** The mp parameters. */
	private Map<Integer, String> mpParameters;
	
	/** The operation business comp. */
	@Inject
	private OperationBusinessBean operationBusinessComp;
	
	/** The security. */
	private Security security;
	
	/** The issuer name. */
	private String issuerName;
	
	/**  The issuer. */
	private List<Issuer> listIssuer;
	
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			//issue 825
			searchOperationTO = new SearchMCNOperationsTO();	
			issuerName = null;
			security = new Security();
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				cevaldomUser = true;
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(null));
			}else if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())){
				participantUser = true;
				searchOperationTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				searchOperationTO.setLstParticipant(mcnOperationServiceFacade.getLstParticipants(null));
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanismByParticipant(searchOperationTO.getParticipantSelected()));
			}else {
				mechanismUser = true;
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(null));						
				if(InstitutionType.BOLSA.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					searchOperationTO.setNegotiationMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
				}
				/*else if(InstitutionType.BCR.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					searchOperationTO.setNegotiationMechanismSelected(NegotiationMechanismType.BC.getCode());
				}
				/*else if(InstitutionType.MIN_HAC.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					searchOperationTO.setNegotiationMechanismSelected(NegotiationMechanismType.HAC.getCode());
				}*/
				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModality(
																											searchOperationTO.getNegotiationMechanismSelected()));				
			}			
			searchOperationTO.setInitialDate(getCurrentSystemDate());
			searchOperationTO.setEndDate(getCurrentSystemDate());
			searchOperationTO.setDateTypeSelected(GeneralConstants.ZERO_VALUE_INTEGER);
			setListIssuer(mcnOperationServiceFacade.issuerList());
			fillCombos();
			fillParameters();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fill combos.
	 */
	private void fillCombos(){
			ParameterTable paramTab = new ParameterTable();
			paramTab.setParameterTablePk(GeneralConstants.ZERO_VALUE_INTEGER);
			paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.operationDate"));
			searchOperationTO.getLstDateType().add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(GeneralConstants.ONE_VALUE_INTEGER);
			paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.cashSettlementDate"));			
			searchOperationTO.getLstDateType().add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(GeneralConstants.TWO_VALUE_INTEGER);
			paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.termSettlementDate"));
			searchOperationTO.getLstDateType().add(paramTab);
			
			List<ParameterTable> lstSettlementType = new ArrayList<ParameterTable>();
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementType.DVP.getCode());
			paramTab.setParameterName(SettlementType.DVP.getValue());
			lstSettlementType.add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementType.FOP.getCode());
			paramTab.setParameterName(SettlementType.FOP.getValue());
			lstSettlementType.add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementType.ECE.getCode());
			paramTab.setParameterName(SettlementType.ECE.getValue());
			lstSettlementType.add(paramTab);
			searchOperationTO.setLstSettlementType(lstSettlementType);
			List<ParameterTable> lstSettlementSchema = new ArrayList<ParameterTable>();
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
			paramTab.setParameterName(SettlementSchemaType.NET.getValue());
			lstSettlementSchema.add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
			paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
			lstSettlementSchema.add(paramTab);
			paramTab = new ParameterTable();
			searchOperationTO.setLstSettlementSchema(lstSettlementSchema);
	}
	
	/**
	 * Fill parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	public void fillParameters() throws ServiceException{
		/*Indicadores de FONDOS Y VALORES*/
		mpParameters = new HashMap<Integer,  String>();

		ParameterTableTO paramTO = new ParameterTableTO();
		paramTO.setLstMasterTableFk(new ArrayList<Integer>());
		paramTO.getLstMasterTableFk().add(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.INSTRUMENT_TYPE.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.CURRENCY.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.SECURITIES_CLASS.getCode());
		
		List<ParameterTable> lstParams = generalPametersFacade.getListParameterTableServiceBean(paramTO);
		
		for(ParameterTable paramTab:lstParams){		
			if(MasterTableType.ESTADOS_OPERACION_MCN.getCode().equals(paramTab.getMasterTable().getMasterTablePk())){
				searchOperationTO.getLstStates().add(paramTab);
			}else if(MasterTableType.CURRENCY.getCode().equals(paramTab.getMasterTable().getMasterTablePk())){
				
				if(Validations.validateIsNotNull(paramTab.getParameterTablePk()) 
						&& !paramTab.getParameterTablePk().equals(CurrencyType.DMV.getCode())
						&& !paramTab.getParameterTablePk().equals(CurrencyType.UFV.getCode())) {
					
					searchOperationTO.getLstCurrency().add(paramTab);
				}
				
				
			} else if (MasterTableType.SECURITIES_CLASS.getCode().equals(paramTab.getMasterTable().getMasterTablePk()))
				if (paramTab.getText1() != null)
					searchOperationTO.getLstSecurityClass().add(paramTab);
			mpParameters.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
	}
	
	/**
	 * Change participant search.
	 */
	public void changeParticipantSearch(){
		searchOperationTO.setHolder(new Holder());
		searchOperationTO.setHolderAccount(new HolderAccount());

		blNoResult = false;
		mechanismOperationDataModel = null;
		mechanismOperationDataModelForcedPurcharse = null;
	}
	
	/**
	 * Change holder search.
	 */
	public void changeHolderSearch(){
		searchOperationTO.setHolderAccount(new HolderAccount());
	}
	
	/**
	 * On change negotiation mechanism.
	 */
	public void onChangeNegotiationMechanism(){
		try {
			searchOperationTO.setNegotiationModalitySelected(null);
			if(cevaldomUser){				
				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModality(
																						searchOperationTO.getNegotiationMechanismSelected()));
				searchOperationTO.setParticipantSelected(null);
				searchOperationTO.setLstParticipant(null);
			}else if(participantUser){
				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
																						searchOperationTO.getNegotiationMechanismSelected(), 
																						searchOperationTO.getParticipantSelected()));		
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean.
	 */
	public void clean(){
		JSFUtilities.resetViewRoot();
		blNoResult = false;
		mechanismOperationDataModel = null;
		mechanismOperationDataModelForcedPurcharse = null;
		init();
	}
	
	/**
	 * On change negotitation modality.
	 */
	public void onChangeNegotitationModality(){
		try {
			if(!participantUser){
				searchOperationTO.setParticipantSelected(null);
				searchOperationTO.setLstParticipant(mcnOperationServiceFacade.getListParticipants(searchOperationTO.getNegotiationMechanismSelected()
																					, searchOperationTO.getNegotiationModalitySelected(),null,null));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search operations.
	 */
	@LoggerAuditWeb
	public void searchOperations(){
		try {
			//issue 825
			if(Validations.validateIsNotNullAndNotEmpty(issuerName)){
				searchOperationTO.setIdIssuerPk(issuerName);
			}else{
				searchOperationTO.setIdIssuerPk(null);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				searchOperationTO.setIdSecurityCodePk(security.getIdSecurityCodePk());
			}else{
				searchOperationTO.setIdSecurityCodePk(null);
			}
			
			List<MechanismOperationTO> lstResult = mcnOperationServiceFacade.searchOperations(searchOperationTO);
			
			//issue 1044 : lista para obtener el resumen de compras forzosas
			List<MechanismOperationTO> lstResultForcedPurchase=new ArrayList<>();
			for (MechanismOperationTO moResult : lstResult)
				if (moResult.getForcedPurchase())
					lstResultForcedPurchase.add(moResult);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				blNoResult = false;
				mechanismOperationDataModel = new GenericDataModel<MechanismOperationTO>(lstResult);
				mechanismOperationDataModelForcedPurcharse = new GenericDataModel<MechanismOperationTO>(lstResultForcedPurchase);
			}else{
				mechanismOperationDataModel = null;
				mechanismOperationDataModelForcedPurcharse = null;
				blNoResult = true;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * View mechanism operation.
	 *
	 * @param event the event
	 */
	public void viewMechanismOperation(ActionEvent event){
		Long idMechanismOperation = (Long)event.getComponent().getAttributes().get("mechanismOperation");
		
		operationBusinessComp.setIdMechanismOperationPk(idMechanismOperation);
		operationBusinessComp.setShowAssignments(true);
		operationBusinessComp.setShowViewDetails(true);
		operationBusinessComp.searchMechanismOperation();
	}

	/**
	 * Back to manage.
	 *
	 * @return the string
	 */
	public String backToManage(){
		return SEARCH_MAPPING;
	}
	
	/**
	 * Gets the search operation to.
	 *
	 * @return the search operation to
	 */
	public SearchMCNOperationsTO getSearchOperationTO() {
		return searchOperationTO;
	}

	/**
	 * Sets the search operation to.
	 *
	 * @param searchOperationTO the new search operation to
	 */
	public void setSearchOperationTO(SearchMCNOperationsTO searchOperationTO) {
		this.searchOperationTO = searchOperationTO;
	}

	/**
	 * Checks if is cevaldom user.
	 *
	 * @return true, if is cevaldom user
	 */
	public boolean isCevaldomUser() {
		return cevaldomUser;
	}
	
	/**
	 * Sets the cevaldom user.
	 *
	 * @param cevaldomUser the new cevaldom user
	 */
	public void setCevaldomUser(boolean cevaldomUser) {
		this.cevaldomUser = cevaldomUser;
	}
	
	/**
	 * Checks if is participant user.
	 *
	 * @return true, if is participant user
	 */
	public boolean isParticipantUser() {
		return participantUser;
	}
	
	/**
	 * Sets the participant user.
	 *
	 * @param participantUser the new participant user
	 */
	public void setParticipantUser(boolean participantUser) {
		this.participantUser = participantUser;
	}
	
	/**
	 * Checks if is mechanism user.
	 *
	 * @return true, if is mechanism user
	 */
	public boolean isMechanismUser() {
		return mechanismUser;
	}
	
	/**
	 * Sets the mechanism user.
	 *
	 * @param mechanismUser the new mechanism user
	 */
	public void setMechanismUser(boolean mechanismUser) {
		this.mechanismUser = mechanismUser;
	}
	
	/**
	 * Gets the mechanism operation data model.
	 *
	 * @return the mechanism operation data model
	 */
	public GenericDataModel<MechanismOperationTO> getMechanismOperationDataModel() {
		return mechanismOperationDataModel;
	}

	/**
	 * Sets the mechanism operation data model.
	 *
	 * @param mechanismOperationDataModel the new mechanism operation data model
	 */
	public void setMechanismOperationDataModel(
			GenericDataModel<MechanismOperationTO> mechanismOperationDataModel) {
		this.mechanismOperationDataModel = mechanismOperationDataModel;
	}

	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}

	/**
	 * Gets the mp parameters.
	 *
	 * @return the mp parameters
	 */
	public Map<Integer, String> getMpParameters() {
		return mpParameters;
	}

	/**
	 * Sets the mp parameters.
	 *
	 * @param mpParameters the mp parameters
	 */
	public void setMpParameters(Map<Integer, String> mpParameters) {
		this.mpParameters = mpParameters;
	}

	/**
	 * Sets the mp parameters.
	 *
	 * @param mpParameters the mp parameters
	 */
	public void setMpParameters(HashMap<Integer, String> mpParameters) {
		this.mpParameters = mpParameters;
	}
	//issue 825
	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public List<Issuer> getListIssuer() {
		return listIssuer;
	}

	public void setListIssuer(List<Issuer> listIssuer) {
		this.listIssuer = listIssuer;
	}

	public GenericDataModel<MechanismOperationTO> getMechanismOperationDataModelForcedPurcharse() {
		return mechanismOperationDataModelForcedPurcharse;
	}

	public void setMechanismOperationDataModelForcedPurcharse(GenericDataModel<MechanismOperationTO> mechanismOperationDataModelForcedPurcharse) {
		this.mechanismOperationDataModelForcedPurcharse = mechanismOperationDataModelForcedPurcharse;
	}
	
	
	
	
}
