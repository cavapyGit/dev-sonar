package com.pradera.negotiations.mcnoperations.facade;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.beanio.BeanWriter;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.massive.type.BbvOperationLabel;
import com.pradera.commons.massive.type.OperationCorrectHeaderLabel;
import com.pradera.commons.massive.type.OperationIncorrectHeaderLabel;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.BeanIOUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.billing.service.BillingComponentService;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.McnProcessFile;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.SettlementPlaceType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.settlement.OperationUnfulfillment;
import com.pradera.model.settlement.SettlementDateOperation;
import com.pradera.model.settlement.SettlementReport;
import com.pradera.model.settlement.SettlementUnfulfillment;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.accountassignment.to.SettlementReportTO;
import com.pradera.negotiations.mcnoperations.massive.to.MechanismOperationType;
import com.pradera.negotiations.mcnoperations.service.McnOperationAdditionalService;
import com.pradera.negotiations.mcnoperations.service.McnOperationServiceBean;
import com.pradera.negotiations.mcnoperations.to.SearchMCNOperationsTO;
import com.pradera.negotiations.operations.calculator.OperationPriceCalculatorServiceFacade;
import com.pradera.negotiations.operations.service.MechanismOperationService;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.negotiations.operations.to.NegotiationOperationTO;
import com.pradera.negotiations.operations.to.PrimaryPlacementTO;
import com.pradera.negotiations.otcoperations.facade.OtcOperationServiceFacade;
import com.pradera.negotiations.otcoperations.service.OtcOperationServiceBean;
import com.pradera.negotiations.otcoperations.view.OtcOperationUtils;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;
import com.pradera.settlements.prepaidextended.to.RegisterPrepaidExtendedTO;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class McnOperationServiceFacade.
 *
 * @author PraderaTechnologies
 * The Class McnManualServiceFacade
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class McnOperationServiceFacade {
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(McnOperationServiceFacade.class);
	
	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;	
	
	/** The mcn operations service bean. */
	@EJB
	private McnOperationServiceBean mcnOperationsServiceBean;	
	
	/** The mcn operation additional service. */
	@EJB
	private McnOperationAdditionalService mcnOperationAdditionalService;	
	
	/** The otc operation service facade. */
	@EJB
	private OtcOperationServiceFacade otcOperationServiceFacade;	
	
	/** The negotiations service bean. */
	@EJB
	private NegotiationOperationServiceBean negotiationsServiceBean;	
	
	/** The securities query service. */
	@EJB
	private SecuritiesQueryServiceBean securitiesQueryService;	
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;	
	
	/** The holder account component service bean. */
	@EJB
	private HolderAccountComponentServiceBean holderAccountComponentServiceBean;	
	
	/** The otc operation service bean. */
	@EJB
	private OtcOperationServiceBean otcOperationServiceBean;	
	
	/** The holiday query service bean. */
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The parameter service bean. */
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	/** The settlements component singleton. */
	@EJB
	private SettlementsComponentSingleton settlementsComponentSingleton;
	
	/** The mechanism operation service. */
	@EJB
	private MechanismOperationService mechanismOperationService;
	
	@EJB
	private OperationPriceCalculatorServiceFacade operationPriceCalculatorServiceFacade;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade 				notificationServiceFacade;
	
	@EJB
	private BillingComponentService billingComponentService;
	
	/**
	 * Saving Mcn Cancel Operations .
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param cancelMotive the cancel motive
	 * @param cancelMotiveOther the cancel motive other
	 * @param indPrimaryPlacement the ind primary placement
	 * @return String
	 * @throws ServiceException the service exception
	 */ 
	public void cancelMcnOperations(Long idMechanismOperation, Integer cancelMotive, String cancelMotiveOther, Integer indPrimaryPlacement) throws ServiceException {		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
        
		settlementsComponentSingleton.cancelMcnOperation(idMechanismOperation, cancelMotive, cancelMotiveOther ,indPrimaryPlacement, loggerUser);
		
	}
		
	/**
	 * This method checks Operation Number Mechanism exists for particular mechanism in MECHANISM_OPERATION table.
	 *
	 * @param mechanism the mechanism
	 * @param referenceNumber the reference number
	 * @param referenceDate the reference date
	 * @return boolean
	 * @throws ServiceException the service exception
	 */
	public boolean chkOperationNumberMechanism(Long mechanism, String referenceNumber) throws ServiceException{
		return mcnOperationsServiceBean.chkOperationNumberMechanism(mechanism, referenceNumber);
	}
	
	/**
	 * Chk operation ballot sequential.
	 *
	 * @param ballotNumber the ballot number
	 * @param sequential the sequential
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean chkOperationBallotSequential(Long ballotNumber, Long sequential) throws ServiceException{
		return mcnOperationsServiceBean.chkOperationBallotSequential(ballotNumber, sequential);
	}

	/**
	 * Gets the calculate date.
	 *
	 * @param initialDate the initial date
	 * @param daysNumber the days number
	 * @param indAddSub the ind add sub
	 * @param calendar the calendar
	 * @return the calculate date
	 */
	public Date getCalculateDate(Date initialDate, Integer daysNumber, Integer indAddSub, Integer calendar){
		//return holidayQueryServiceBean.getCalculateDate(initialDate, daysNumber, indAddSub, calendar);
		return holidayQueryServiceBean.getCalculateDateServiceBean(initialDate, daysNumber, indAddSub, calendar);
	}
	
	/**
	 * Gets the processed mcn file content.
	 *
	 * @param idProcessFilePk the id process file pk
	 * @return the processed mcn file content
	 */
	public byte[] getProcessedMcnFileContent(Long idProcessFilePk) {
		return mcnOperationsServiceBean.getProcessedMcnFileContent(idProcessFilePk);
	}

	/**
	 * Gets the accepted mcn file content.
	 *
	 * @param idProcessFilePk the id process file pk
	 * @return the accepted mcn file content
	 */
	public byte[] getAcceptedMcnFileContent(Long idProcessFilePk) {
		return mcnOperationsServiceBean.getAcceptedMcnFileContent(idProcessFilePk);
	}
	
	/**
	 * Gets the rejected mcn file content.
	 *
	 * @param idProcessFilePk the id process file pk
	 * @return the rejected mcn file content
	 */
	public byte[] getRejectedMcnFileContent(Long idProcessFilePk) {
		return mcnOperationsServiceBean.getRejectedMcnFileContent(idProcessFilePk);
	}

	/**
	 * Validate and build operations.
	 *
	 * @param operation the operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<RecordValidationType> validateAndBuildOperations(MechanismOperationType operation, Map<String, BigDecimal> mpPrimaryPlacement) throws ServiceException {
		List<RecordValidationType> lstErrors = new ArrayList<RecordValidationType>(0);
		operation.buildMechanismOperation();
		// General Validation
		mcnGeneralValidation(lstErrors,operation);
		// Validate security
		securityValidation(lstErrors,operation); // atp Security is filled.		
		// Validate modality
		modalityValidation(lstErrors,operation); // atp MechanismModality is filled.
		// Validation date
		dateValidation(lstErrors,operation);
		// Validate buyer participant
		buyerParticipantValidation(lstErrors,operation); // atp buyer Participant is filled.
		// Validate seller participant		
		sellerParticipantValidation(lstErrors,operation); // atp seller Participant is filled.
		// Validate security mechanism
		securityMechanismValidation(lstErrors,operation);
		//Validate amount 
		amountValidation(lstErrors,operation, mpPrimaryPlacement);
		// Validate primary placement
		primaryPlacementValidation(lstErrors, operation, mpPrimaryPlacement);
		return lstErrors;
	}
	
	/**
	 * Mcn general validation.
	 *
	 * @param lstErrors the lst errors
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	private void mcnGeneralValidation(List<RecordValidationType> lstErrors,
			MechanismOperationType operation) throws ServiceException {
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		Date referenceDate = CommonsUtilities.truncateDateTime(mechanismOperation.getReferenceDate());
		
		if(operation.getSettlementPlace().equals(SettlementPlaceType.FIS.getCode())){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.NRO_REGISTRO.toString(),PropertiesConstants.MCN_MASSIVE_OPER_FIS_NOT_VALID));
			return;
		}
		
		//En Cavapy el numero de orden no esta atado a la fecha
		/*boolean validation  =  NegotiationUtils.validateMechanismNumber(mechanismOperation.getReferenceNumber(),mechanismOperation.getReferenceDate());
		if(!validation){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.NRO_REGISTRO.toString(),PropertiesConstants.MCN_MASSIVE_OPER_NUMBER_INVALID));
		}*/
		 	
		boolean exists = mcnOperationsServiceBean.chkOperationNumberMechanism(
					operation.getIdFileMechanismPk(),mechanismOperation.getReferenceNumber());
		if(exists){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.NRO_REGISTRO.toString(),PropertiesConstants.MCN_MASSIVE_OPER_NUMBER_EXISTS));
		}
		
		// Validate unique ballot/sequential
		/*boolean existsSq = mcnOperationsServiceBean.chkOperationBallotSequential(mechanismOperation.getBallotNumber(),mechanismOperation.getSequential());
		if(existsSq){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.NRO_REGISTRO.toString(),PropertiesConstants.MCN_MASSIVE_OPER_BALLOT_SEQ_EXISTS));
		}*/
		
		
		// si la fecha de transaccion no es un dia no laborable
		if(holidayQueryServiceBean.isNonWorkingDayServiceBean(referenceDate, true)){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.FECHA.toString(),PropertiesConstants.MCN_MASSIVE_TRANS_DATE_NW_DAY));
		}
		
		//si la fecha de transaccion no esta dentro del rango
		//Date minRefDate = holidayQueryServiceBean.getCalculateDate(CommonsUtilities.currentDate(), 1, 0, NegotiationConstant.WORKING_DAYS);
		Date minRefDate = CommonsUtilities.currentDate();
		if(referenceDate.after(mechanismOperation.getOperationDate()) || referenceDate.before(minRefDate)){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.FECHA.toString(),PropertiesConstants.MCN_MASSIVE_TRANS_DATE_INVALID));
		}
		
		
	}
	
	
	/**
	 * Modality validation.
	 *
	 * @param lstErrors the lst errors
	 * @param operation the operation
	 */
	private void modalityValidation(List<RecordValidationType> lstErrors,
			MechanismOperationType operation) {
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		
		if(Validations.validateIsNullOrEmpty(operation.getSellFlagStr()) || 
				!(operation.getSellFlagStr().trim().toUpperCase().equals(BooleanType.YES.getValue())
					|| operation.getSellFlagStr().trim().toUpperCase().equals(BooleanType.NO.getValue()))) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.FLAG_SUBASTA.toString(),PropertiesConstants.MCN_MASSIVE_MODALITY_SELL_FLAG_INCORRECT));
		} else { 			
			if(mechanismOperation.getSecurities() != null){
				/*Integer indPrimaryPlacement= BooleanType.NO.getCode();
				if(GeneralConstants.STOCK_EXCHANGE_OPERATION_CLASS_CVT.equalsIgnoreCase(operation.getOperationClass())) {
					logger.info("stockExchangeMechanism: " + operation.getReferenceNumber().substring(6, 8));
					Long stockExchangeMechanism= new Long(operation.getReferenceNumber().substring(6, 8)); //indicator to know if belong to primary o secondary market
					if ( stockExchangeMechanism.equals(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode()) ||
							stockExchangeMechanism.equals(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode()) ) {
						indPrimaryPlacement = BooleanType.YES.getCode();
					} else if ( stockExchangeMechanism.equals(NegotiationModalityType.REPORT_EQUITIES.getCode()) ||
							stockExchangeMechanism.equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode()) ) {
						//we get the placement segment for the security code
						boolean exists =  negotiationsServiceBean.validatePlacementSegmentDetail(mechanismOperation.getSecurities().getIdSecurityCodePk(), 
																				mechanismOperation.getSecurities().getIssuance().getIdIssuanceCodePk(), null);
						if (exists) {
							indPrimaryPlacement = BooleanType.YES.getCode();
						}
					}
				}*/
				
				MechanismModality mm = mcnOperationsServiceBean.getMechanismModality(operation.getIdFileMechanismPk(), operation.getOperationClass(), 
																			mechanismOperation.getSecurities().getInstrumentType(), operation.getSellFlagStr());
	
				boolean valid = false;
				if(mm != null ){
					if(mm.getIndMassiveLoad().equals(ComponentConstant.ONE)){
						valid = true;
					}
				}
				if(!valid){
					lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.CLASE.toString(),PropertiesConstants.MCN_MASSIVE_MODALITY_NOT_PERMITTED));
				}else{
					mechanismOperation.setMechanisnModality(mm);
				}
			}
		}		
	}
	
	/**
	 * Date validation.
	 *
	 * @param lstErrors the lst errors
	 * @param operation the operation
	 */
	private void dateValidation(List<RecordValidationType> lstErrors, MechanismOperationType operation) {
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		if(mechanismOperation.getMechanisnModality()!=null){
			Date originalCashSettlementDate = mechanismOperation.getCashSettlementDate();
			try {
				setSettlementDate(mechanismOperation.getMechanisnModality().getNegotiationModality(), mechanismOperation, ComponentConstant.CASH_PART);
				mechanismOperation.setCashSettlementDate(originalCashSettlementDate);
			} catch (ServiceException e) {
				RecordValidationType error = new RecordValidationType(operation,BbvOperationLabel.FECHA.toString());
				error.setErrorDescription(PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, 
						operation.getLocale(), e.getErrorService().getMessage(),e.getParams()));
				lstErrors.add(error);
				return;
			}
			
			// si la fecha de operacion (hoy) es mayor a la fecha de liquidacion
			if(CommonsUtilities.truncateDateTime(mechanismOperation.getOperationDate()).after(mechanismOperation.getCashSettlementDate())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.FECHA.toString(),PropertiesConstants.MCN_MASSIVE_SETTLE_DATE_GREATER_TODAY));
			}
			
			//si la fecha de liquidacion es un dia no laborable
			if(holidayQueryServiceBean.isNonWorkingDayServiceBean(mechanismOperation.getCashSettlementDate(), true)){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.FECHA.toString(),PropertiesConstants.MCN_MASSIVE_SETTLE_DATE_NW_DAY));
			}
			
			if(ComponentConstant.ONE.equals(mechanismOperation.getMechanisnModality().getNegotiationModality().getIndTermSettlement())){
				Date originalTermSettlementDate = mechanismOperation.getTermSettlementDate();
				try {
					setSettlementDate(mechanismOperation.getMechanisnModality().getNegotiationModality(), mechanismOperation, ComponentConstant.TERM_PART);
					mechanismOperation.setTermSettlementDate(originalTermSettlementDate);
				} catch (ServiceException e) {
					RecordValidationType error = new RecordValidationType(operation,BbvOperationLabel.FECHA.toString());
					error.setErrorDescription(PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, 
							operation.getLocale(), e.getErrorService().getMessage(),e.getParams()));
					lstErrors.add(error);
				}
				
			}
		}

	}
	
	/**
	 * Buyer participant validation.
	 *
	 * @param lstErrors the lst errors
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	private void buyerParticipantValidation(List<RecordValidationType> lstErrors, MechanismOperationType operation) throws ServiceException {
		MechanismOperation mechanismOperation = operation.getMechanismOperation();		
		Participant bPart = new Participant();
		bPart.setMnemonic(operation.getBuyerPartMnemonic());
		
		bPart = participantServiceBean.getParticipantByPkWithUniqueResultValidation(bPart);
		if(bPart == null){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.COMPRADOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NOT_FOUND));
		} else if (bPart.getIdParticipantPk().equals(GeneralConstants.ZERO_VALUE_LONG)) {
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.COMPRADOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NON_UNIQUE_RESULT));
		} else {
			if(!bPart.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.COMPRADOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NOT_REGISTERED));
				return;
			}
			
			if(mechanismOperation.getMechanisnModality()!=null){
				Participant participantObject =  (Participant)this.getLstParticipantsAndCheck(
						mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk(), 
						mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk(),
						null, ComponentConstant.ONE,
						bPart.getIdParticipantPk());
				if(participantObject==null){
					lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.COMPRADOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NOT_IN_MODALITY));
				}else{
					mechanismOperation.setBuyerParticipant(bPart);
				}
			}
			
		}
	}
	
	/**
	 * Seller participant validation.
	 *
	 * @param lstErrors the lst errors
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	private void sellerParticipantValidation(List<RecordValidationType> lstErrors,
			MechanismOperationType operation) throws ServiceException {
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		
		if(Validations.validateIsNotNullAndNotEmpty(operation.getSellerPartMnemonic())){
			Participant sPart = new Participant();
			sPart.setMnemonic(operation.getSellerPartMnemonic());
		
			sPart = participantServiceBean.getParticipantByPkWithUniqueResultValidation(sPart);
			if(sPart == null){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.VENDEDOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NOT_FOUND));
			} else if (sPart.getIdParticipantPk().equals(GeneralConstants.ZERO_VALUE_LONG)) {
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.VENDEDOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NON_UNIQUE_RESULT));
			} else {
				if(!sPart.getState().equals(ParticipantStateType.REGISTERED.getCode())){
					lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.VENDEDOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NOT_REGISTERED));
				}
					
				if(mechanismOperation.getMechanisnModality()!=null){
					Participant participantObject =  (Participant)this.getLstParticipantsAndCheck(
							mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk(), 
							mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk(),
							ComponentConstant.ONE , null ,
							sPart.getIdParticipantPk());
					if(participantObject == null){
						lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.VENDEDOR.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NOT_IN_MODALITY));
					}else{
						mechanismOperation.setSellerParticipant(sPart);
					}
				}
				
			}
		}
	}
	
	/**
	 * Security validation.
	 *
	 * @param lstErrors the list errors
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	private void securityValidation(List<RecordValidationType> lstErrors, 
			MechanismOperationType operation) throws ServiceException {

		SecurityTO securityTO = new SecurityTO();
		securityTO.setIdSecurityCodePk(operation.getSecurityCode());
		
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		
		Security objSecurity = securitiesQueryService.findSecuritiesById(securityTO);
		if(objSecurity==null){
			lstErrors.add(CommonsUtilities.populateRecordValidation(operation, BbvOperationLabel.SERIE.toString(), PropertiesConstants.MCN_MASSIVE_SECURITY_NOT_FOUND));
		} else {			
			if(!objSecurity.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.SERIE.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_NOT_REGISTERED));
			}			
			if(!objSecurity.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.SERIE.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_ISSUER_NOT_REGISTERED));
			}			
			if(!objSecurity.getIssuance().getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.SERIE.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_ISSUANCE_NOT_REGISTERED));
			}			
			if(lstErrors.size() <= 0){
				mechanismOperation.setSecurities(objSecurity);
			}			
			
			//validate securityClass
			/*ParameterTableTO paramSecClass = new ParameterTableTO();
			paramSecClass.setText1(operation.getSecurityClassCode());
			paramSecClass.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			List<ParameterTable> secClasses = parameterServiceBean.getListParameterTableServiceBean(paramSecClass);
			if(secClasses.size() == 0){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.INSTRUMENTO.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_CLASS_NOT_FOUND));
			}else{
				ParameterTable securityClass = secClasses.get(0);
				if(!securityClass.getParameterTablePk().equals(objSecurity.getSecurityClass())){
					lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.INSTRUMENTO.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_CLASS_NOT_VALID));
				}
			}*/
			
		}
	}
	
	/**
	 * Security mechanism validation.
	 *
	 * @param lstErrors the list errors
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	private void securityMechanismValidation(List<RecordValidationType> lstErrors, MechanismOperationType operation) throws ServiceException {
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		
		if(mechanismOperation.getMechanisnModality()!=null){
			NegotiationOperationTO negotiatioTO = new NegotiationOperationTO();
			negotiatioTO.setIdNegotiationMechanismPk(mechanismOperation.getMechanisnModality().getId().getIdNegotiationMechanismPk());
			negotiatioTO.setIdNegotiationModalityPk(mechanismOperation.getMechanisnModality().getId().getIdNegotiationModalityPk());
			negotiatioTO.setIdSecurityCode(operation.getSecuritySerial());
			Security s = negotiationsServiceBean.getSecurityByModalities(negotiatioTO);
			if(s==null){
				lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.SERIE.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_NOT_IN_MODALITY));
			}
			
			if(mechanismOperation.getSecurities()!=null){
				// instrument type must be the same as the one in modality
				if(!mechanismOperation.getSecurities().getInstrumentType().equals(mechanismOperation.getMechanisnModality().getNegotiationModality().getInstrumentType())){
					lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.SERIE.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_INS_TYPE_NOT_VALID));
				}else{
					
					// fixed income: operation cash settlement must not be greater than expiration date
					if(mechanismOperation.getSecurities().getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()) && 
							mechanismOperation.getSecurities().getExpirationDate()!=null ){
						
						//Date dayBeforeExpiration = holidayQueryServiceBean.getCalculateDate(mechanismOperation.getSecurities().getExpirationDate(), 1, 0,NegotiationConstant.WORKING_DAYS);
						Date dayBeforeExpiration = holidayQueryServiceBean.getCalculateDateServiceBean(mechanismOperation.getSecurities().getExpirationDate(), 1, 0,NegotiationConstant.WORKING_DAYS);
						
						if(mechanismOperation.getCashSettlementDate() != null &&
								mechanismOperation.getCashSettlementDate().after(dayBeforeExpiration)){
							lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.FECHA.toString(),PropertiesConstants.OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS));
						}
						
						// fixed income: operation term settlement must not be greater than expiration date
						
						if(ComponentConstant.ONE.equals(mechanismOperation.getIndTermSettlement()) && mechanismOperation.getTermSettlementDate()!=null && 
								mechanismOperation.getTermSettlementDate().after(dayBeforeExpiration)){
							//we verify if the security expiration date is weekend or holiday
							boolean isNonWorkingDate= holidayQueryServiceBean.isNonWorkingDayServiceBean(mechanismOperation.getSecurities().getExpirationDate(), true);
							if (isNonWorkingDate) {
								// we forward the expiration date to working date 
								Date newDate= holidayQueryServiceBean.getCalculateDateServiceBean(mechanismOperation.getSecurities().getExpirationDate(), 
																									1, 1,NegotiationConstant.WORKING_DAYS);
								if (newDate.compareTo(mechanismOperation.getTermSettlementDate()) != 0) {
									//The working expiration date must be equals to the term settlement date
									lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.PLAZO_RPT.toString(),
																							PropertiesConstants.OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS));
								}
							} else {
								lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.PLAZO_RPT.toString(),
																						PropertiesConstants.OTC_OPERATIONS_VALIDATION_FIXED_EXPIRATION_DAYS));
							}
						}
					}
				}
				
				//Validate currency Code from security
				ParameterTable objCurrency = null;
				ParameterTableTO param = new ParameterTableTO();
				param.setText2(operation.getCurrencyCode());
				param.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				List<ParameterTable> currencies = parameterServiceBean.getListParameterTableServiceBean(param);
				if(currencies.size() == 0){
					lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.MONEDA.toString(),PropertiesConstants.MCN_MASSIVE_CURRENCY_NOT_FOUND));
				}else{
					// validate if file currency is the same as security currency
					objCurrency = currencies.get(0);
//					if(!objCurrency.getParameterTablePk().equals(mechanismOperation.getSecurities().getCurrency())){
//						RecordValidationType error = new MechanismOperationValidationType(operation,BbvOperationLabel.MONEDA.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_CURRENCY_DIFF_ISSUANCE);
//						lstErrors.add(error);
//					}else{
						
					//validate when currency is need to be converted
					if(!objCurrency.getIndicator4().equals(objCurrency.getIndicator5())){
						DailyExchangeRates dailyER = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),objCurrency.getIndicator4());
						if(dailyER==null){
							lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.MONEDA.toString(),PropertiesConstants.MCN_MASSIVE_EXCHANGE_RATE_NOT_FOUND));
						}else{
							mechanismOperation.setExchangeRate(dailyER.getBuyPrice());
						}
					}
					
					mechanismOperation.setCurrency(objCurrency.getParameterTablePk()); 
					mechanismOperation.setCashSettlementCurrency(objCurrency.getIndicator5());
					if(ComponentConstant.ONE.equals(mechanismOperation.getMechanisnModality().getNegotiationModality().getIndTermSettlement())){
						mechanismOperation.setTermSettlementCurrency(objCurrency.getIndicator5());
					}
//					}
				}
				
				// modality currency 1: operation and security currency must be the same
				// modality currency 0: operation and security currency can be different
				if(mechanismOperation.getCurrency()!=null){
					Integer indModalityCurrency = mechanismOperation.getMechanisnModality().getIndSecuritiesCurrency();
					
					if(indModalityCurrency.equals(ComponentConstant.ONE)){
						if(!mechanismOperation.getCurrency().equals(mechanismOperation.getSecurities().getCurrency())){
							lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.MONEDA.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_CURRENCY_DIFF_OP));
						}
					}
				}
				
				if(!ComponentConstant.ONE.equals(mechanismOperation.getMechanisnModality().getNegotiationModality().getIndPrimaryPlacement())){
					if(negotiationsServiceBean.issuanceIsPlacementSegment(mechanismOperation.getSecurities())){//Validamos si la emision esta en colocacion primaria
						lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.SERIE.toString(),PropertiesConstants.OTC_OPERATION_VALIDATION_PRIMARYPLACEMENT_FINISHED));
						return;	
					}
				}
			}
		}
	}

	/**
	 * Amount validation.
	 *
	 * @param lstErrors the lst errors
	 * @param operation the operation
	 */
	private void amountValidation(List<RecordValidationType> lstErrors, MechanismOperationType operation, Map<String, BigDecimal> mpPrimaryPlacement) {
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		
		if(mechanismOperation.getMechanisnModality()!=null){
			
			this.operationPriceCalculatorServiceFacade.regularPriceCalculation(mechanismOperation, mechanismOperation.getCashPrice());
			
			if(ComponentConstant.ONE.equals(mechanismOperation.getMechanisnModality().getNegotiationModality().getIndTermSettlement())){
				
				if(mechanismOperation.getCashPrice()!=null && mechanismOperation.getAmountRate()!=null && mechanismOperation.getTermSettlementDays() !=null){
					
					Long termSettlementDays = new Long(CommonsUtilities.getDaysBetween(mechanismOperation.getCashSettlementDate(), 
							mechanismOperation.getTermSettlementDate()));
					// precio limpio 
					mechanismOperation.setTermPrice(NegotiationUtils.getTermPrice(mechanismOperation.getCashPrice(), mechanismOperation.getAmountRate(),termSettlementDays));
					mechanismOperation.setTermAmount(NegotiationUtils.getSettlementAmount(mechanismOperation.getTermPrice(),mechanismOperation.getStockQuantity()));
					
					//verify price is not zero
					if(mechanismOperation.getTermPrice().compareTo(BigDecimal.ZERO) <= 0){
						lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.PRECIO.toString(),PropertiesConstants.MCN_MASSIVE_INVALID_TERM_PRICE));
					}
				}
				
				if(mechanismOperation.getAmountRate() != null && (""+mechanismOperation.getAmountRate().longValue()).length() > 4){
					lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.PRECIO.toString(),PropertiesConstants.MCN_MASSIVE_RATE_LARGER_THAN_PERMITTED));
				}
				
			}
			
			Security objSecurity = mechanismOperation.getSecurities();
			
			if(objSecurity != null){
				if(!NegotiationUtils.mechanismOperationIsPrimaryPlacement(mechanismOperation)){
					//we verify if exists primary placement operations to be settled today
					BigDecimal primaryPlacementQuantity= negotiationsServiceBean.getPrimaryPlacementBalanceOperation(
																mechanismOperation.getCashSettlementDate(), objSecurity.getIdSecurityCodePk());
					//we verify if exists primary placement operations inside the same processed file
					BigDecimal primaryPlacementOperations= BigDecimal.ZERO;
					if (mpPrimaryPlacement.containsKey(objSecurity.getIdSecurityCodePk())) {
						primaryPlacementOperations= mpPrimaryPlacement.get(objSecurity.getIdSecurityCodePk());
					}
					BigDecimal futureCirculationBalance= objSecurity.getCirculationBalance().add(primaryPlacementOperations).add(primaryPlacementQuantity);
					//we compare the stock quantity operation with the future circulation balance
					if(mechanismOperation.getStockQuantity().compareTo(futureCirculationBalance)>0){
						lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.CANTIDAD.toString(),PropertiesConstants.OTC_OPERATION_VALIDATION_SECURITY_CIRCULATION));
					}
				}else{
					/*
					// validar min y max invesment 
					BigDecimal minInvesment = objSecurity.getMinimumInvesment();
					//BigDecimal maxInvesment = security.getMaximumInvesment()!=null?(BigDecimal)security.getMaximumInvesment():new BigDecimal(0);
					BigDecimal nominalValue = objSecurity.getCurrentNominalValue();
				    
				    BigDecimal amountEntered = mechanismOperation.getStockQuantity().multiply(nominalValue);
					if(minInvesment!=null && minInvesment.compareTo(amountEntered)==1){ 
						lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.CANTIDAD.toString(),PropertiesConstants.OTC_OPERATION_VALIDATION_RANGE_AMOUTNS));
					}*/
				}
			}
			
			
		}
	}

	/**
	 * Primary placement validation.
	 *
	 * @param lstErrors the lst errors
	 * @param operation the operation
	 */
	private void primaryPlacementValidation(List<RecordValidationType> lstErrors, MechanismOperationType operation, Map<String, BigDecimal> mpPrimaryPlacement) {
		if(CommonsUtilities.hasPreviousErrors(lstErrors)){
			return;
		}
		MechanismOperation mechanismOperation = operation.getMechanismOperation();
		if(mechanismOperation.getMechanisnModality()!=null && mechanismOperation.getSellerParticipant()!=null && mechanismOperation.getSecurities()!=null){
			Integer indPrimaryPlacement = mechanismOperation.getMechanisnModality().getNegotiationModality().getIndPrimaryPlacement();
			if(ComponentConstant.ONE.equals(indPrimaryPlacement)){
				
				NegotiationOperationTO negotiationTO = new NegotiationOperationTO();
				negotiationTO.setIdIssuanceCode(mechanismOperation.getSecurities().getIssuance().getIdIssuanceCodePk());
				negotiationTO.setIdSecurityCode(mechanismOperation.getSecurities().getIdSecurityCodePk());
				negotiationTO.setIdParticipantPlacement(mechanismOperation.getSellerParticipant().getIdParticipantPk());
				negotiationTO.setIdIssuerPk(mechanismOperation.getSecurities().getIssuer().getIdIssuerPk());
				
				try {
					PrimaryPlacementTO pp = negotiationsServiceBean.validatePrimaryPlacement(negotiationTO);
					
					if(!OtcOperationUtils.isinHadBalanceAvailableOnPlacementSegment(pp.getPlacementSegment(), mechanismOperation)){
						lstErrors.add(CommonsUtilities.populateRecordValidation(operation,BbvOperationLabel.CANTIDAD.toString(),
																				PropertiesConstants.MCN_MASSIVE_AMOUNT_LARGER_THAN_PLACEMENT));
					} else {
						// we accumulate the stock quantity primary placement operation for each security code. this will be used later on amount validation 
						BigDecimal primaryStockQuantity= mpPrimaryPlacement.get(mechanismOperation.getSecurities().getIdSecurityCodePk());
						if (Validations.validateIsNotNull(primaryStockQuantity)) {
							mpPrimaryPlacement.put(mechanismOperation.getSecurities().getIdSecurityCodePk(), 
													primaryStockQuantity.add(mechanismOperation.getStockQuantity()));
						} else {
							mpPrimaryPlacement.put(mechanismOperation.getSecurities().getIdSecurityCodePk(), mechanismOperation.getStockQuantity());
						}
					}
					
				} catch (ServiceException e) {
					RecordValidationType error = new RecordValidationType(operation,BbvOperationLabel.VENDEDOR.toString());
					if(Validations.validateIsNotNullAndNotEmpty(operation.getLineNumber())) {
						error.setLineNumber(operation.getLineNumber());
					}					
					error.setErrorDescription(PropertiesUtilities.getMessage(operation.getLocale(),e.getErrorService().getMessage()));
					lstErrors.add(error);
				}
				
			}			
		}
		
	}

	/**
	 * Gets the mcn processed files information.
	 *
	 * @param parameters the parameters
	 * @return the mcn processed files information
	 */
	public List<ProcessFileTO> getMcnProcessedFilesInformation(
			Map<String, Object> parameters) {
		List<ProcessFileTO> mcnProcessFiles = new ArrayList<ProcessFileTO>(); 
		List<Object[]> objects = mcnOperationsServiceBean.getMcnProcessedFilesInformation(parameters);
		for (Object[] object : objects) {
			ProcessFileTO mcnFile = new ProcessFileTO();
			mcnFile.setIdProcessFilePk((Long)object[0]);
			mcnFile.setIdNegotiationMechanismPk((Long)object[1]);
			mcnFile.setDescription(object[2]!=null?(String)object[2]:null);
			mcnFile.setMechanismName(object[3]!=null?(String)object[3]:null);
			mcnFile.setFileName(object[4]!=null?(String)object[4]:null);
			//mcnFile.seti(object[5]!=null?(Integer)object[5]:null);
			mcnFile.setProcessDate((Date)object[6]);
			//mcnFile.setProcessTypeDescription(processDate)
			mcnFile.setAcceptedOperations(object[8] == null ? 0 :((BigDecimal)object[8]).intValue());
			mcnFile.setRejectedOperations(object[9] == null ? 0 : ((BigDecimal)object[9]).intValue());
			mcnFile.setTotalOperations(((BigDecimal)object[10]).intValue());
			mcnFile.setSequenceNumber((Long)object[11]);
			mcnFile.setProcessStateDescription((String)object[12]);
			
			mcnProcessFiles.add(mcnFile);
		}
		return mcnProcessFiles;
	}

	/**
	 * Save mechanism operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation saveMechanismOperation(MechanismOperation mechanismOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());		
        mechanismOperation.setRegisterUser(loggerUser.getUserName());
        mechanismOperation.setRegisterDate(CommonsUtilities.currentDateTime());        
        return settlementsComponentSingleton.saveMechanismOperation(mechanismOperation, loggerUser);
	}
	
	/**
	 * Metodo para registrar la SEDIR
	 * @param settlementReport
	 * @return
	 * @throws ServiceException
	 */
	public SettlementReport saveSettlementReport(SettlementReport settlementReport) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());		
        settlementReport.setRegisterUser(loggerUser.getUserName());
        settlementReport.setRegisterDate(CommonsUtilities.currentDateTime());        
        return settlementsComponentSingleton.saveSettlementReport(settlementReport, loggerUser);
	}

	/**
	 * Metodo para buscar MechanismOperation por PK
	 * @param negoMechanism
	 * @param negoModality
	 * @return
	 */
	public MechanismOperation getMechanismOperationSedir(Long mechanismOperationPk) {
		return mcnOperationsServiceBean.getMechanismOperationSedir(mechanismOperationPk);
	}
	
	/**
	 * Metodo para registrar la SEDIR
	 * @param settlementReport
	 * @return
	 * @throws ServiceException
	 */
	public SettlementUnfulfillment saveSettlementUnfulfillment(SettlementUnfulfillment settlementUnfulfillment) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());		
        settlementUnfulfillment.setRegisterUser(loggerUser.getUserName());
        settlementUnfulfillment.setRegisterDate(CommonsUtilities.currentDateTime());        
        return settlementsComponentSingleton.saveSettlementUnfulfillment(settlementUnfulfillment, loggerUser);
	}
	
	/**
	 * 
	 * @return
	 */
	@ProcessAuditLogger
	public Object[] getLoggerUser() {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		Integer test = mcnOperationsServiceBean.getLoggerUser(loggerUser.getUserName());
		
		Object[] tests = {test,loggerUser.getUserName()};
		
		return tests;
	}


	/**
	 * Metodo para buscar OperationUnfulfillment por PK
	 * @param negoMechanism
	 * @param negoModality
	 * @return
	 */
	public OperationUnfulfillment getOperationUnfulfillment(Long operationUnfulfillmentPk) {
		return mcnOperationsServiceBean.getOperationUnfulfillment(operationUnfulfillmentPk);
	}
	
	
	/**
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	public Boolean existPendingCancelOperations () throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return mcnOperationsServiceBean.existPendingCancelOperations(loggerUser.getUserName());
	}
	
	
	/**
	 * Save mechanism operations massively.
	 *
	 * @param mcnProcessFileTO the mcn process file to
	 * @param indMarketFact the ind market fact
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES, value=30)
	public Object[] saveMechanismOperationsMassively(ProcessFileTO mcnProcessFileTO, Integer indMarketFact) {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        ReportUser reportUser = new ReportUser();
        reportUser.setUserName(loggerUser.getUserName());
        
		List<ReportFile> files = new ArrayList<ReportFile>(0);
		Long reportId = ReportIdType.MASSIVE_MCN_REGISTRATION_RESPONSE_FILES.getCode();
		Integer numberAcceptedOps=0;
		// #1 separate transaction: save file
		ValidationProcessTO validationTO = mcnProcessFileTO.getValidationProcessTO();
		McnProcessFile mcnProcessFile = mcnOperationAdditionalService.saveMcnProcessFileTx(mcnProcessFileTO,loggerUser);
		
		BigDecimal quantityAccepted = BigDecimal.ZERO;
		BigDecimal quantityRejected = BigDecimal.ZERO;
		String messageSecurity = null;			
		try {
			Map<String, BigDecimal> mpPrimaryPlacement= new HashMap<String, BigDecimal>(); // we'll accumulate the stock quantity primary placement operations 
			// #2 separate transaction: save operations independently
			
			HashMap<String, String> duplicatedSecurity = new HashMap<>();
		
			List<MechanismOperation> operationManualList = mcnOperationsServiceBean.findOperationManual();
			
			//Primero traemos la lista de todas las operaciones pendientes de cancelar hoy
			List<MechanismOperation> operationsPendingToday = mcnOperationsServiceBean.findOperationsPendingToday();
			
			//Recorremos la lista de operaciones del archivo cargado
			for(MechanismOperation operatCompare : operationsPendingToday) {
				
				String operatComp = operatCompare.getBallotNumber().toString() + operatCompare.getSequential().toString();
				
				Boolean existOperat = false;
				
				for(ValidationOperationType operation : validationTO.getLstOperations()){
					
					MechanismOperationType mechanismOperation = (MechanismOperationType) operation;
					String operat = mechanismOperation.getBallotNumber().toString() + mechanismOperation.getSequential().toString();
					
					if(operatComp.equals(operat)) {
						existOperat = true;
						break;
					}
				}
				
				if(existOperat.equals(true)) {
					//Cambiamos el estado de la operacion a Registrada
					mcnOperationsServiceBean.updateOperationRegisterCancel(operatCompare.getIdMechanismOperationPk());
				}
			}
			
			
			
			//Primero traemos la lista de todas las operaciones registradas hoy
			List<MechanismOperation> operationsRegisterToday = mcnOperationsServiceBean.findOperationsRegisteredToday();
			
			//Recorremos la lista de operaciones del archivo cargado
			for(MechanismOperation operatCompare : operationsRegisterToday) {
				
				String operatComp = operatCompare.getBallotNumber().toString();
				
				Boolean existOperat = false;
				 
				for(ValidationOperationType operation : validationTO.getLstOperations()){
					
					MechanismOperationType mechanismOperation = (MechanismOperationType) operation;
					String operat = mechanismOperation.getReferenceNumber();
					
					if(operatComp.equals(operat)) {
						existOperat = true;
						break;
					}
				}
				/*
				if(existOperat) {
					//Cambiamos el estado de la operacion a Pendiente de Cancelar
					mcnOperationsServiceBean.updateOperationPendingCancel(operatCompare.getIdMechanismOperationPk());
				}*/
			}
			
			for(ValidationOperationType operation : validationTO.getLstOperations()){
				
				Integer count = new Integer(0);
				
				MechanismOperationType mechanismOperation = (MechanismOperationType) operation;
				
				if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getSecurityCode())){
					
					Security sec = securitiesQueryService.find(Security.class, mechanismOperation.getSecurityCode());
					
					if(Validations.validateIsNotNull(sec)) {
						mechanismOperation.setSecurityClassCode(SecurityClassType.get(sec.getSecurityClass()).getText1());
						
						if (mechanismOperation.getSecurityClassCode().equals(SecurityClassType.DPF.getText1()) 
								|| mechanismOperation.getSecurityClassCode().equals(SecurityClassType.DPA.getText1()) 
								|| mechanismOperation.getSecurityClassCode().equals(SecurityClassType.PGS.getText1())){
							
							for(ValidationOperationType comparative : validationTO.getLstOperations()){
							
								MechanismOperationType mechanismOperation2 = (MechanismOperationType) comparative;
								
								
								if (mechanismOperation.getSecurityCode().equals(mechanismOperation2.getSecurityCode())
										&& !mechanismOperation.getReferenceNumber().equals(mechanismOperation2.getReferenceNumber()) 
										){
										count = count + 1 ;
										if (count>0){
											duplicatedSecurity.put("Valor: "+mechanismOperation.getSecurityCode() +" Nro. Op: "+ mechanismOperation.getReferenceNumber() + " en el archivo", " VR ");
										}
									}
								}
							
							if(operationManualList.size()>0){
								for (MechanismOperation operationManualListCompare :operationManualList) {
									if(mechanismOperation.getSecurityCode().equals(operationManualListCompare.getSecurities().getIdSecurityCodePk())){
										duplicatedSecurity.put("Valor: "+mechanismOperation.getSecurityCode() +" Nro. Op: "+ mechanismOperation.getReferenceNumber() + " en registro manual", " VR ");
									}
								}
							}
						}
					}
				}
				
				Log.info("=================================================================================");
				Log.info("NUMERO DE OPERACION  --  "+mechanismOperation.getReferenceNumber());
				Log.info("=================================================================================");
				
				List<RecordValidationType> errs = validateAndBuildOperations(mechanismOperation, mpPrimaryPlacement)  ;
				
				if(errs.size() == 0){
					RecordValidationType e = mcnOperationAdditionalService.saveSingleMechanismOperation(mechanismOperation,mcnProcessFile,indMarketFact);
					mechanismOperation.setOperationNumberRef(mechanismOperation.getMechanismOperation().getOperationNumber());
					
					if(e != null){
						validationTO.getLstValidations().add(e);
						operation.setHasError(true);
					}else{
						numberAcceptedOps++;
					}
				}else{
					if(validationTO.getLstValidations().addAll(errs)){
						operation.setHasError(true);
					}
				}
			}
			
			if (duplicatedSecurity.size()>0){
			
				String messageDup="";
				for (Entry keyHash: duplicatedSecurity.entrySet()) {
					//    System.out.println(e.getKey() + " " + e.getValue());
					messageDup=messageDup + keyHash.getKey().toString() + " | ";
					}
				messageSecurity=messageDup;
			}
			
			validationTO.countOperations();     
			
			// save accepted operations file
			if(validationTO.getAcceptedOps() > 0){
				InputStream mappingFile = Thread.currentThread().getContextClassLoader().
						getResourceAsStream(NegotiationConstant.ACCEPTED_OPERATIONS_STREAM);
				StringWriter sw = new StringWriter(); 
				
				//BeanIOUtils.write(val.getAcceptedOperations(),mappingFile, "file", sw);
				BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
				
				out.write("header",new OperationCorrectHeaderLabel());
				for (ValidationOperationType object : validationTO.getAcceptedOperations()) {
					out.write("operation",object);
				}
			    out.flush();
			    out.close();
			    
				mcnProcessFile.setAcceptedFile(sw.toString().getBytes("UTF-8"));
				sw.flush();
				sw.close();
				mappingFile.close();
				
				ReportFile file = new ReportFile();
				file.setFile(mcnProcessFile.getAcceptedFile());
				file.setPhysicalName("ACCEPTED_FILES"+CommonsUtilities.convertDateToString(new Date(),CommonsUtilities.DATETIME_PATTERN));
				file.setReportType(ReportFormatType.TXT.getCode());
				files.add(file);
			}
			
			// save rejected operations file
			if(validationTO.getRejectedOps() > 0 && Validations.validateListIsNotNullAndNotEmpty(validationTO.getLstValidations())){
				InputStream mappingFile = Thread.currentThread().getContextClassLoader().
						getResourceAsStream(NegotiationConstant.REJECTED_OPERATIONS_STREAM);
				StringWriter sw = new StringWriter();
				
				//BeanIOUtils.write(val.getLstErrors(),inputFile, "file", sw);
				BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
				out.write("header",new OperationIncorrectHeaderLabel());
				for (RecordValidationType object : validationTO.getLstValidations()) {
					out.write("error",object);
				}
			    out.flush();
			    out.close();
			    
				mcnProcessFile.setRejectedFile(sw.toString().getBytes("UTF-8"));
				sw.flush();
				sw.close();
				mappingFile.close();
				
				ReportFile file = new ReportFile();
				file.setFile(mcnProcessFile.getRejectedFile());
				file.setPhysicalName("REJECTED_FILES"+CommonsUtilities.convertDateToString(new Date(),CommonsUtilities.DATETIME_PATTERN));
				file.setReportType(ReportFormatType.TXT.getCode());
				files.add(file);
			}
			
			BigDecimal acc = new BigDecimal(validationTO.getAcceptedOps());
			BigDecimal rej = new BigDecimal(validationTO.getRejectedOps());
			mcnProcessFile.setAcceptedOperations(acc);
			mcnProcessFile.setRejectedOperations(rej);
			mcnProcessFile.setTotalOperations(acc.add(rej));
			mcnProcessFile.setProcessState(ExternalInterfaceReceptionStateType.CORRECT.getCode());
			
			quantityAccepted = mcnProcessFile.getAcceptedOperations();
			quantityRejected = mcnProcessFile.getRejectedOperations();
			
			//#3 separate transaction: update file independently
			mcnOperationAdditionalService.updateMcnProcessFileTx(mcnProcessFile, loggerUser);
			
			mcnOperationAdditionalService.sendReportFile(reportId , reportUser , null, null, loggerUser, files );						
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			if(validationTO.getAcceptedOps()==0){//No llego a terminar el Bucle(For) con las operaciones
				BigDecimal acc = new BigDecimal(numberAcceptedOps);
				BigDecimal total = new BigDecimal(validationTO.getLstOperations().size());
				BigDecimal rej = CommonsUtilities.subtractTwoDecimal(total, acc, 2);
				mcnProcessFile.setAcceptedOperations(acc);
				mcnProcessFile.setRejectedOperations(rej);
				mcnProcessFile.setTotalOperations(total);
			} else if(Validations.validateIsNotNullAndPositive(validationTO.getAcceptedOps())
					&& Validations.validateIsNotNullAndPositive(validationTO.getRejectedOps())){
				BigDecimal acc = new BigDecimal(validationTO.getAcceptedOps());
				BigDecimal rej = new BigDecimal(validationTO.getRejectedOps());
				mcnProcessFile.setAcceptedOperations(acc);
				mcnProcessFile.setRejectedOperations(rej);
				mcnProcessFile.setTotalOperations(acc.add(rej));
			}			
			
			quantityAccepted = mcnProcessFile.getAcceptedOperations();
			quantityRejected = mcnProcessFile.getRejectedOperations();
			
			mcnProcessFile.setProcessState(ExternalInterfaceReceptionStateType.FAILED.getCode());
			
			e.printStackTrace();
			
		} finally {
			mcnOperationAdditionalService.updateMcnProcessFileTx(mcnProcessFile, loggerUser);
		}
		
		return new Object[]{quantityAccepted.add(quantityRejected), quantityAccepted, quantityRejected,messageSecurity};
		
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param negotiationModality the negotiation modality
	 * @param mechanismOperation the mechanism operation
	 * @param cashOrTerm the cash or term
	 * @throws ServiceException the service exception
	 */
	public void setSettlementDate(NegotiationModality negotiationModality, MechanismOperation mechanismOperation, Integer cashOrTerm) throws ServiceException {
		Integer maxSettleDays = null; // dias de liquidacion maximo
		Integer minSettleDays = null; // dias de liquidacion minimo
		Integer settlementDays = null; // dias de liquidacion ingresado por usuario
		Integer calendar = null; // indicador dias calendario o habiles
		Date initialDate = null; // fecha inicial sea parte plazo o contado
		Date settlementDate = null;
		
		if(cashOrTerm.equals(ComponentConstant.CASH_PART)){
			maxSettleDays = negotiationModality.getCashMaxSettlementDays();
			minSettleDays = negotiationModality.getCashMinSettlementDays();
			settlementDays = mechanismOperation.getCashSettlementDays().intValue();
			calendar = negotiationModality.getIndCashCallendar();
			initialDate = CommonsUtilities.truncateDateTime(mechanismOperation.getOperationDate());
			
		}else if(cashOrTerm.equals(ComponentConstant.TERM_PART)){
			maxSettleDays = negotiationModality.getTermMaxSettlementDays();
			minSettleDays = negotiationModality.getTermMinSettlementDays();
			settlementDays = mechanismOperation.getTermSettlementDays().intValue();
			calendar = negotiationModality.getIndTermCallendar();
			initialDate = mechanismOperation.getCashSettlementDate();
		}
		
		if(Validations.validateIsNull(minSettleDays) || Validations.validateIsNull(maxSettleDays)){
			throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		}
		if(settlementDays.intValue() > maxSettleDays.intValue() || settlementDays.intValue() < minSettleDays.intValue()){
			throw new ServiceException(ErrorServiceType.SETTLEMENT_DAYS_OUT_OF_RANGE,
					new Object[]{minSettleDays.intValue(),maxSettleDays.intValue()});
		}
		if(calendar.equals(NegotiationConstant.CALEDAR_DAYS)){
			settlementDate= CommonsUtilities.addDaysToDate(initialDate, settlementDays);
			if(holidayQueryServiceBean.isNonWorkingDayServiceBean(settlementDate, true)){
				throw new ServiceException(ErrorServiceType.SETTLEMENT_NOT_WORKING_DAY,new Object[]{CommonsUtilities.convertDatetoString(settlementDate)});
			}
		}else if(calendar.equals(NegotiationConstant.WORKING_DAYS)  || calendar.equals(NegotiationConstant.WORKING_DAYS_PASS_WEEKENDS)){
			//settlementDate = holidayQueryServiceBean.getCalculateDate(initialDate,settlementDays, 1,calendar);
			settlementDate = holidayQueryServiceBean.getCalculateDateServiceBean(initialDate,settlementDays, 1,calendar);
		}
		if(cashOrTerm.equals(ComponentConstant.CASH_PART)){
			mechanismOperation.setCashSettlementDate(settlementDate);
		}else if(cashOrTerm.equals(ComponentConstant.TERM_PART)){
			mechanismOperation.setTermSettlementDate(settlementDate);
		}
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @param indMcnProcess the ind mcn process
	 * @return the lst negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism(Integer indMcnProcess) throws ServiceException{
		return mcnOperationsServiceBean.getLstNegotiationMechanism(indMcnProcess);
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param participantCode the participant code
	 * @return the participant
	 */
	public Participant getParticipant(Long participantCode){
		return mcnOperationsServiceBean.find(Participant.class, participantCode);
	}
	
	/**
	 * Gets the lst negotiation mechanism by participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the lst negotiation mechanism by participant
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanismByParticipant(Long idParticipantPk) throws ServiceException{
		return mcnOperationsServiceBean.getLstNegotiationMechanismByParticipant(idParticipantPk);
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the lst negotiation modality
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getLstNegotiationModality(Long idNegotiationMechanismPk) throws ServiceException{
		return mcnOperationsServiceBean.getListNegotiationModality(idNegotiationMechanismPk);
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the lst negotiation modality
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getLstNegotiationModalityReport(Long idNegotiationMechanismPk) throws ServiceException{
		return mcnOperationsServiceBean.getListNegotiationModalityReport(idNegotiationMechanismPk);
	}
	
	/**
	 * Issuer list. //issue 825
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> issuerList()throws ServiceException{
		return mcnOperationsServiceBean.issuerList();
	}
	

	/**
	 * Gets the lst negotiation modality for participant.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @param idParticipantPk the id participant pk
	 * @return the lst negotiation modality for participant
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getLstNegotiationModalityForParticipant(Long idNegotiationMechanismPk, Long idParticipantPk) throws ServiceException{
		return mcnOperationsServiceBean.getLstNegotiationModalityForParticipant(idNegotiationMechanismPk, idParticipantPk,null);
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @param accountType the account type
	 * @return the lst participants
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLstParticipants(Integer accountType) throws ServiceException{
		return mcnOperationsServiceBean.getLstParticipants(accountType);
	}
	
	/**
	 * Gets the participant object.
	 *
	 * @param idParticipantCode the id participant code
	 * @return the participant object
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipantObject(Long idParticipantCode) throws ServiceException{
		return mcnOperationsServiceBean.getParticipantObject(idParticipantCode);
	}

	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	
	public List<MechanismOperationTO> searchOperationsReport(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperationsReport(searchOperationTO);
	}
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public List<MechanismOperationTO> searchOperationsUnfulfillment(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperationsUnfulfillment(searchOperationTO);
	}
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public Boolean searchOperationExist(Long idMechanism) throws ServiceException{
		return mcnOperationsServiceBean.searchOperationExist(idMechanism);
	}
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public List<MechanismOperationTO> searchOperations(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperations(searchOperationTO);
	}
	
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public List<SettlementReportTO> searchOperationsSedir(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperationsSedir(searchOperationTO);
	}
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public List<SettlementReportTO> searchOperationsUnfulfillmentSearch(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperationsUnfulfillmentSearch(searchOperationTO);
	}
	
//	/**
//	 * Search operations.
//	 *
//	 * @param searchOperationTO the search operation to
//	 * @return the list
//	 * @throws ServiceException the service exception
//	 */
//	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
//	public List<SettlementReportTO> searchOperationsSedirV(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
////		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
////        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
//		return mcnOperationsServiceBean.searchOperationsSedir(searchOperationTO);
//	}
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public List<SettlementReportTO> searchOperationsUnfulfillmentV(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperationsUnfulfillmentSearch(searchOperationTO);
	}
	
//	/**
//	 * 
//	 * @param searchOperationTO
//	 * @return
//	 * @throws ServiceException
//	 */
//	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
//	public List<SettlementReportTO> searchOperationsSedir(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
//		return mcnOperationsServiceBean.searchOperationsSedir(searchOperationTO);
//	}
	
	/**
	 * Search operations.
	 *
	 * @param searchOperationTO the search operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public List<SettlementReportTO> searchOperationsSedirV(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperationsSedir(searchOperationTO);
	}
	
	/**
	 * 
	 * @param searchOperationTO
	 * @return
	 * @throws ServiceException
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public List<MechanismOperationTO> searchOperationsIssue1141(SearchMCNOperationsTO searchOperationTO) throws ServiceException{
		//LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        //loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperations(searchOperationTO);
	}
	
	/**
	 * 
	 * @param searchOperationTO
	 * @return
	 * @throws ServiceException
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MCN_OPERATION_QUERY)
	public Boolean searchOperationChained(Long idMechanismOperationPk, Date date){
		//LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        //loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return mcnOperationsServiceBean.searchOperationChained(idMechanismOperationPk, date);
	}

	/**
	 * Gets the modality group.
	 *
	 * @param id the id
	 * @return the modality group
	 * @throws ServiceException the service exception
	 */
	public ModalityGroup getModalityGroup(MechanismModalityPK id) throws ServiceException{
		return mcnOperationsServiceBean.getModalityGroup(id);
	}
	
	/**
	 * Issuance is placement segment.
	 *
	 * @param security the security
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean issuanceIsPlacementSegment(Security security) throws ServiceException{
		return negotiationsServiceBean.issuanceIsPlacementSegment(security);
	}

	/**
	 * Gets the list negotiation modality.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 * @return the list negotiation modality
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getListNegotiationModality(Long idNegotiationMechanismPk) throws ServiceException{
		return mcnOperationsServiceBean.getListNegotiationModality(idNegotiationMechanismPk);
	}

	/**
	 * Search mechanism operations.
	 *
	 * @param searchMCNOperationsTO the search mcn operations to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<MechanismOperationTO> searchMechanismOperations(SearchMCNOperationsTO searchMCNOperationsTO) throws ServiceException{
		return mcnOperationsServiceBean.searchMechanismOperations(searchMCNOperationsTO);
	}

	/**
	 * Gets the list participants.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param saleRole the sale role
	 * @param purchaseRole the purchase role
	 * @return the list participants
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getListParticipants(Long mechanismId, Long modalityId , Integer saleRole, Integer purchaseRole) throws ServiceException{
		
		return (List<Participant>)this.getLstParticipantsAndCheck(mechanismId,modalityId,saleRole,purchaseRole,null);
		
	}

	/**
	 * Gets the list mechanism modality.
	 *
	 * @return the list mechanism modality
	 * @throws ServiceException the service exception
	 */
	public List<MechanismModality> getListMechanismModality() throws ServiceException{
		return mcnOperationsServiceBean.getListMechanismModality();
	}

	/**
	 * Find issuance and issuer.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuance
	 */
	public Issuance findIssuanceAndIssuer(String idSecurityCodePk) {
		return mcnOperationsServiceBean.findIssuanceAndIssuer(idSecurityCodePk);
	}

	/**
	 * Validate security nego mechanism.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean validateSecurityNegoMechanism(String idSecurityCodePk, MechanismModalityPK id) {
		return mcnOperationsServiceBean.validateSecurityNegoMechanism(idSecurityCodePk, id);
	}

	/**
	 * Find operation repo.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param operationDate the operation date
	 * @param operationNumber the operation number
	 * @return the mechanism operation
	 */
	public MechanismOperation findOperationRepo(Long mechanismId , Long modalityId, Date operationDate, Long operationNumber) {
		return mcnOperationsServiceBean.findOperationRepo(mechanismId,modalityId,operationDate,operationNumber);
	}


	/**
	 * Gets the mechanism modality.
	 *
	 * @param negoMechanism the nego mechanism
	 * @param negoModality the nego modality
	 * @return the mechanism modality
	 */
	public MechanismModality getMechanismModality(Long negoMechanism,Long negoModality) {
		return mcnOperationsServiceBean.getMechanismModality(negoMechanism,negoModality);
	}


	/**
	 * Sets the currency.
	 *
	 * @param objCurrency the obj currency
	 * @param mechanismOperation the mechanism operation
	 * @param termPart the term part
	 * @throws ServiceException the service exception
	 */
	public void setCurrency(ParameterTable objCurrency, MechanismOperation mechanismOperation, Integer termPart) throws ServiceException {
		
		negotiationsServiceBean.setCurrency(objCurrency,mechanismOperation,termPart);
	}

	/**
	 * Gets the modality groups.
	 *
	 * @param idMechanism the id mechanism
	 * @param settlementSchema the settlement schema
	 * @return the modality groups
	 */
	public List<ModalityGroup> getModalityGroups(Long idMechanism, Integer settlementSchema) {
		return mcnOperationsServiceBean.getModalityGroups(idMechanism, settlementSchema);
	}

	/**
	 * Gets the negotiation mechanism.
	 *
	 * @param idMechanism the id mechanism
	 * @return the negotiation mechanism
	 */
	public NegotiationMechanism getNegotiationMechanism(Long idMechanism) {
		return mcnOperationsServiceBean.find(NegotiationMechanism.class, idMechanism);
	}
	
	/**
	 * Issue 2829
	 * 
	 * 1.- Find Participants with same Security Code in Operations 
	 * 
	 * 2.- Notifications email the users LFLIMA, MSTRAMPFER, CROJAS, RDURAN, CVILLAVICENCIO
	 * 
	 */
	public void notificationConcatenation(ProcessLogger processLogger){
		
		String message=getStringOperations(getOperationsWithSameSecurity(CommonsUtilities.currentDate()));
		
		if(Validations.validateIsNotNullAndNotEmpty(message)){
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_SAME_PARTICIPANT_OPERATION.getCode());
			List<UserAccountSession> lstUserAccount=billingComponentService.getListUserByBusinessProcess(BusinessProcessType.NOTIFICATION_SAME_PARTICIPANT_OPERATION.getCode()); 

			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), businessProcessNotification, lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, message, NotificationType.EMAIL);
		}
	}
	
	/**
	 * Issue 0096
	 * 
	 * 1.- Find Participants with same Security Code in Operations 
	 * 
	 * 2.- Notifications email the users LFLIMA, MSTRAMPFER, CROJAS, RDURAN, CVILLAVICENCIO
	 * 
	 */
	public void notificationDuplicatedSecurity(ProcessLogger processLogger, String duplicatedSecurity){
		
		String notification = messageConfiguration.getProperty("notification.message.duplicated.security");
		
		String message=notification+duplicatedSecurity +" Verifique.";
		
		if(Validations.validateIsNotNullAndNotEmpty(message)){
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_DUPLICATED_SECURITY.getCode());
			List<UserAccountSession> lstUserAccount=billingComponentService.getListUserByBusinessProcess(BusinessProcessType.NOTIFICATION_DUPLICATED_SECURITY.getCode()); 

			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), businessProcessNotification, lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, message, NotificationType.EMAIL);
		}
	}
	
	
	/**
	 * 
	 * @return Object [participant, Operation Date, Operations, Security Code]
	 * 
	 */
	public List<Object[]> getOperationsWithSameSecurity(Date operationDate){
		return mcnOperationsServiceBean.getOperationsWithSameSecurity(operationDate);
	}
	
	/**
	 * 
	 * @param listOperations
	 * @return
	 */
	public String getStringOperations(List<Object[]> listOperations){
		StringBuilder operations=new StringBuilder();
		
		/*
		 * El Participante XXX (Nemonico + Codigo) hoy XXX (fecha de la operativa DD/MM/AAAA) 
		 * tiene operaciones 999/1 (nro de Papeleta/Secuencial separados por comas) con el valor seriado XXX. 
		 * Verifique encadenamientos a realizarse.
		 * participant, Operation Date, Operations, Security Code
		 */
		for (Object[] objects : listOperations) {
			String message = messageConfiguration.getProperty("notification.message.participant.same.operation");					 
			message = MessageFormat.format(message, objects[0],objects[1],objects[3],objects[2]);
			operations.append(message).append(GeneralConstants.STR_BR);
				
		}
		
		return operations.toString();
	}
	
	/**
	 * Metodo que verifica si un archivo de asignaciones se encuentra en proceso
	 * @param processFileTO
	 * @return
	 */
	public boolean verifyFileAssignmentProcess(ProcessFileTO processFileTO) {
			return mcnOperationsServiceBean.verifyFileAssignmentProcess(processFileTO);
	}
	
	/**
	 * Se debe actualizar los nuevos datos en 4 tablas
	 * @param idMechanismOperationPk
	 * @param settlementDateAdd
	 * @param settlementDaysAdd
	 * @param settlementPriceAdd
	 * @param settlementAmountAdd
	 */
	public void updateMechanismOperationSettlement(Long idMechanismOperationPk, Date settlementDateAdd, Long settlementDaysAdd,
			BigDecimal settlementPriceAdd, BigDecimal settlementAmountAdd) {
		
		mcnOperationsServiceBean.updateMechanismOperation(idMechanismOperationPk,settlementDateAdd,settlementDaysAdd,settlementPriceAdd,settlementAmountAdd);
		mcnOperationsServiceBean.updateSettlementOperation(idMechanismOperationPk,settlementDateAdd,settlementDaysAdd,settlementPriceAdd,settlementAmountAdd);
		//issue 1184
		mcnOperationsServiceBean.updateHolderAccountOperation(idMechanismOperationPk,settlementDateAdd,settlementDaysAdd,settlementPriceAdd,settlementAmountAdd);
		mcnOperationsServiceBean.updateSettlementAccountOperation(idMechanismOperationPk,settlementDateAdd,settlementDaysAdd,settlementPriceAdd,settlementAmountAdd);
		mcnOperationsServiceBean.updateParticipantSettlement(idMechanismOperationPk,settlementDateAdd,settlementDaysAdd,settlementPriceAdd,settlementAmountAdd);
	}
	
	
	
	/**
	 * Metodo para actualizar el estado de las SEDIR
	 * @param idSettlementReport
	 */
	public void updateSettlementReport(Long idSettlementReport, Integer state, Date newDate) {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
				
		mcnOperationsServiceBean.updateSettlementReport(idSettlementReport, state, loggerUser.getUserName(), CommonsUtilities.currentDateTime(), newDate);
	}
	
	
	/**
	 * Metodo para actualizar el estado de las SEDIR
	 * @param idSettlementReport
	 */
	public void updateSettlementUnfulfillment(Long idSettlementUnfulfillment, Integer state) {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
				
		mcnOperationsServiceBean.updateSettlementUnfulfillment(idSettlementUnfulfillment, state, loggerUser.getUserName(), CommonsUtilities.currentDateTime());
	}
	
	/**
	 * Metodo para actualizar el estado de las SEDIR
	 * @param idSettlementReport
	 */
	public void updateUnfulfillmentMechOperaSett(Long idMechanismOperationPk, Long idSettlementOperationPk) {
		
		SettlementUnfulfillment settlementOperation =  mcnOperationsServiceBean.getSettlementOperation(idSettlementOperationPk);
				
		mcnOperationsServiceBean.updateUnfulfillmentMechOperaSett(settlementOperation.getOperationUnfulfillment().getSettlementOperation().getIdSettlementOperationPk(), idMechanismOperationPk);
	}
	
	/**
	 * Metodo para actualizar el estado de las SEDIR
	 * @param idSettlementReport
	 */
	@LoggerAuditWeb
	public void settledOperation(SettlementReportTO settlementReportTO) {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		Integer indBuyerSeller = 1;
		
		if (settlementReportTO.getBalanceDescription() == "VENDEDOR" ) { indBuyerSeller = 1; } else { indBuyerSeller = 0; }
		
		try {
			settlementsComponentSingleton.settleGrossMechanismOperationsUnfulfillment
					(null, 
					1L, 
					1L, 
					null,//CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1) , 
					settlementReportTO.getMechanismOperation().getCurrency(), 
					ComponentConstant.TERM_PART, 
					null, 
					SettlementSchemaType.GROSS.getCode(), 
					loggerUser, 
					null,
					settlementReportTO.getMechanismOperation().getIdMechanismOperationPk(),
					indBuyerSeller);
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Object getLstParticipantsAndCheck(Long mechanismId, Long modalityId, Integer saleRole, Integer purchaseRole, Long idParticipant) {
		/*
		 * Si la modalidad es colocacion primaria no externa, el vendedor obligatoriamente debe ser cliente de cavapy
		 */
		if((modalityId.equals(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode()) || modalityId.equals(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode()))
				&& (Validations.validateIsNotNull(saleRole) && saleRole.equals(ComponentConstant.ONE))) {
			return mcnOperationsServiceBean.getLstParticipants(
					mechanismId, 
					modalityId,
					saleRole, purchaseRole,
					idParticipant,
					ComponentConstant.ONE);
		} else {
			return mcnOperationsServiceBean.getLstParticipants(
					mechanismId, 
					modalityId,
					saleRole, purchaseRole,
					idParticipant,
					null);
		}
	}
	
	public ProgramInterestCoupon getLastCouponPayed(String idSecurityCodePk, Date operationDate) { 
		return this.securitiesQueryService.getMostRecentPayedCouponBySecurityPk(idSecurityCodePk, operationDate);
	}
	
	public ProgramInterestCoupon getNextNonPayedCoupon(String idSecurityCodePk, Date operationDate) { 
		return this.securitiesQueryService.getOldestNonPayedCouponBySecurityPk(idSecurityCodePk, operationDate);
	}	
	
	public Integer findDaysMaxAmp(SettlementDateOperation settlementDateOperation, RegisterPrepaidExtendedTO registerPrepaidExtendedTO) {
		return mcnOperationsServiceBean.findDaysMaxAmp(settlementDateOperation, registerPrepaidExtendedTO);
	}
	

	public void operationMCNconvertXLStoXML(ProcessFileTO processFileTO){		
 		
		int rowSize = 19;
		
 		try {
 			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
 	        DocumentBuilder builder = factory.newDocumentBuilder();
 	        DOMImplementation implementation = builder.getDOMImplementation();
 	 		File fileTemp = null;
 	 		Integer pos = 0;
 	 		
 			FileInputStream fileInputStream = new FileInputStream(processFileTO.getTempProcessFile().getPath());
 			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
 			XSSFSheet datatypeSheet = workbook.getSheetAt(0);
 			Iterator<Row> iterator = datatypeSheet.iterator();

			Document documento = implementation.createDocument(null, "ROWSET", null);
	        documento.setXmlVersion("1.0");
			

	        if(processFileTO.getValidationProcessTO() == null){
	        	processFileTO.setValidationProcessTO(new ValidationProcessTO());
	        }	
	        
	        Boolean cellValid = true;
			
 			while (iterator.hasNext()) {
 				Row row = iterator.next();
 				Element operation = documento.createElement("ROW");
                if(row.getRowNum() > 0) {
                    Iterator<Cell> cellIterator = row.iterator();
    				pos++;
        			cellValid = true;        			

                    String[] tagHeader =  {"NRO_REGISTRO", //0
                    			"COMPRADOR",
                    			"VENDEDOR",
                    			"SERIE",
                    			"CANTIDAD",
                    			"MERCADO_NEGOCIACION",	//5
                    			"SUBASTA",
                    			"MONEDA",
                    			"FECHA",
                    			"HORA",
                    			"DIAS_LIQUIDACION_CONTADO", //10
                    			"FECHA_LIQUIDACION_CONTADO",
                    			"DIAS_LIQUIDACION_PLAZO",
                    			"FECHA_LIQUIDACION_PLAZO",
                    			"TASA_INTERES_REPO",
                    			"PRECIO_UNITARIO_CONTADO_LIMPIO",	//15
                    			"PRECIO_PLAZO",
                    			"MONTO_CONTADO_SUCIO",
                    			"MONTO_FINAL",
                    			"AFORO"}; //19
                    
                    String registerNumber =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("A"), row);
            		String buyer =				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("B"), row);	
            		String seller =				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("C"), row);
            		String securityCode =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("D"), row);	
            		String quantity =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("E"), row);
            		String modality =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("F"), row);		
            		String sale =				CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("G"), row);
            		String currency =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("H"), row);
            		String operationDate =		CommonsUtilities.getCellForceStringValue(CellReference.convertColStringToIndex("I"), row);
            		String operationHour =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("J"), row);	
            		String cashSettlementDays =	CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("K"), row);
            		String cashSettlementDate =	CommonsUtilities.getCellForceStringValue(CellReference.convertColStringToIndex("L"), row);
            		String termSettlementDays =	CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("M"), row);
            		String termSettlementDate =	CommonsUtilities.getCellForceStringValue(CellReference.convertColStringToIndex("N"), row);
            		String termInterestRate =	CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("O"), row);
            		String cashPrice =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("P"), row);	
            		String termPrice =			CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("Q"), row);	
            		String realCashAmount =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("R"), row);	
            		String termCashAmount =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("S"), row);
            		String warrantyCause =		CommonsUtilities.getCellValue(CellReference.convertColStringToIndex("T"), row);
        			
            		if(Validations.validateIsNotNull(registerNumber) && !(registerNumber.isEmpty())){
	        			CommonsUtilities.createTagXML(operation, documento, registerNumber, tagHeader[0]);        			        			
	        			CommonsUtilities.createTagXML(operation, documento, buyer, tagHeader[1]); 
	        			CommonsUtilities.createTagXML(operation, documento, seller, tagHeader[2]);         			        			
	        			CommonsUtilities.createTagXML(operation, documento, securityCode, tagHeader[3]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, quantity, tagHeader[4]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, modality, tagHeader[5]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, sale, tagHeader[6]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, currency, tagHeader[7]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, operationDate, tagHeader[8]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, operationHour, tagHeader[9]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, cashSettlementDays, tagHeader[10]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, cashSettlementDate, tagHeader[11]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, Validations.validateIsNotNull(termSettlementDays)? termSettlementDays : GeneralConstants.EMPTY_STRING, tagHeader[12]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, Validations.validateIsNotNull(termSettlementDate)? termSettlementDate : GeneralConstants.EMPTY_STRING , tagHeader[13]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, Validations.validateIsNotNull(termInterestRate)? termInterestRate : GeneralConstants.EMPTY_STRING, tagHeader[14]);  			        			
	        			CommonsUtilities.createTagXML(operation, documento, cashPrice, tagHeader[15]);			        			
	        			CommonsUtilities.createTagXML(operation, documento, Validations.validateIsNotNull(termPrice)? termPrice : GeneralConstants.EMPTY_STRING, tagHeader[16]);			        			
	        			CommonsUtilities.createTagXML(operation, documento, realCashAmount, tagHeader[17]);			        			
	        			CommonsUtilities.createTagXML(operation, documento, Validations.validateIsNotNull(termCashAmount)? termCashAmount : GeneralConstants.EMPTY_STRING, tagHeader[18]);			        			
	        			CommonsUtilities.createTagXML(operation, documento, Validations.validateIsNotNull(warrantyCause)? warrantyCause : GeneralConstants.EMPTY_STRING, tagHeader[19]);
	        				                
	                    documento.getDocumentElement().appendChild(operation);    
	            	}
                }
 			}
 			
 			// Asocio el source con el Document
			Source source = new DOMSource(documento);
			// Creo el Result, indicado que fichero se va a crear
 			fileTemp = File.createTempFile("tempMcnfile", ".tmp");			
			Result result = new StreamResult(fileTemp);
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
	        transformer.transform(source, result);
	        
			processFileTO.setOriginalTempProcessFile(processFileTO.getTempProcessFile());
			processFileTO.setTempProcessFile(fileTemp);
 		} catch(IOException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch(TransformerException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch(ParserConfigurationException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch (TransformerFactoryConfigurationError e) {
 			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
		} 
 		
 	}
		
}