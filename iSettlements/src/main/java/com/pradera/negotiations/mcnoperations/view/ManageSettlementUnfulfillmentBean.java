package com.pradera.negotiations.mcnoperations.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.funds.fundoperations.util.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.negotiation.type.SettlementType;
import com.pradera.model.settlement.OperationUnfulfillment;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementUnfulfillment;
import com.pradera.model.settlement.type.SettlementDateRequestStateType;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.accountassignment.to.SettlementReportTO;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.negotiations.mcnoperations.to.SearchMCNOperationsTO;
import com.pradera.negotiations.operations.view.OperationBusinessBean;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.utils.NegotiationUtils;

@LoggerCreateBean
@DepositaryWebBean
public class ManageSettlementUnfulfillmentBean extends GenericBaseBean implements Serializable{


	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The mcn operation service facade. */
	@EJB
	McnOperationServiceFacade mcnOperationServiceFacade;
	
	/** The max days of custody request. */
	@Inject @Configurable
	Integer maxDaysOfCustodyRequest;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The search operation to. */
	private SearchMCNOperationsTO searchOperationTO;
	
	/** The mechanism user. */
	private boolean cevaldomUser, participantUser, mechanismUser;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The mechanism operation data model. */
	private GenericDataModel<MechanismOperationTO> mechanismOperationDataModel;
	
	/** The mechanism operation data model. */
	private GenericDataModel<SettlementReportTO> settlementReportDataModel;
	
	/** selected mechanism operation data model. */
	private List<MechanismOperationTO> selectedMechanismOperations;
	
	/** selected mechanism operation data model. */
	private List<String> lstReturnBalance;
	
	/** selected mechanism operation data model. */
	private List<SettlementReportTO> selectsettlementReportons;
	
	/** objeto lista para obtener el resumen de compras forzosas */
	private GenericDataModel<MechanismOperationTO> mechanismOperationDataModelForcedPurcharse;
	
	/** The Constant SEARCH_MAPPING. */
	private static final String SEARCH_MAPPING = "searchQueryOperation";

	/** The mp parameters. */
	private Map<Integer, String> mpParameters;
	
	/** The operation business comp. */
	@Inject
	private OperationBusinessBean operationBusinessComp;
	
	/** The security. */
	private Security security;
	
	/** The issuer name. */
	private String issuerName;
	
	/**  The issuer. */
	private List<Issuer> listIssuer;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The Constant REGISTER_MANUAL_MAPPING. */
	private final static String REGISTER_MANUAL_MAPPING = "registerSettlementUnfulfillment";
	
	/**  Variables estaticas *. */
	private static final String CONFIRM 	 = "confirm";
	
	/** The Constant REJECT. */
	private static final String ANNULATE 	 = "annulate";
	
	/** The Constant REJECT. */
	private static final String REJECT 	     = "reject";
	
	/** The Constant GENERATE. */
	private static final String APPROVE 	 = "approve";
	
	/** The Constant GENERATE. */
	private static final String REVIEW 		 = "review";
	
	/** The Constant GENERATE. */
	private static final String AUTHORIZE 		 = "authorize";
	
	String option = "";
	
	private Integer quantityReg = null;
	
	/** The bl participant. */
	private boolean blDepositary, blParticipant;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			searchOperationTO = new SearchMCNOperationsTO();	
			issuerName = null;
			security = new Security();
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				cevaldomUser = true;
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(1));
			}else if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())){
				participantUser = true;
				searchOperationTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				searchOperationTO.setLstParticipant(mcnOperationServiceFacade.getLstParticipants(null));
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanismByParticipant(searchOperationTO.getParticipantSelected()));
			}else {
				mechanismUser = true;
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(1));						
				if(InstitutionType.BOLSA.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					searchOperationTO.setNegotiationMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
				}
				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModality(searchOperationTO.getNegotiationMechanismSelected()));				
			}			
			searchOperationTO.setInitialDate(getCurrentSystemDate());
			searchOperationTO.setEndDate(getCurrentSystemDate());
			searchOperationTO.setDateTypeSelected(GeneralConstants.ZERO_VALUE_INTEGER);
			setListIssuer(mcnOperationServiceFacade.issuerList());
			fillCombos();
			fillParameters();
			loadPriviligies();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load priviligies.
	 */
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnReview(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		privilegeComponent.setBtnAuthorize(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Fill combos.
	 */
	private void fillCombos(){
			ParameterTable paramTab = new ParameterTable();
			paramTab.setParameterTablePk(GeneralConstants.ZERO_VALUE_INTEGER);
			paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.operationDate"));
			searchOperationTO.getLstDateType().add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(GeneralConstants.ONE_VALUE_INTEGER);
			paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.cashSettlementDate"));			
			searchOperationTO.getLstDateType().add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(GeneralConstants.TWO_VALUE_INTEGER);
			paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.termSettlementDate"));
			searchOperationTO.getLstDateType().add(paramTab);
			
			List<ParameterTable> lstSettlementType = new ArrayList<ParameterTable>();
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementType.DVP.getCode());
			paramTab.setParameterName(SettlementType.DVP.getValue());
			lstSettlementType.add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementType.FOP.getCode());
			paramTab.setParameterName(SettlementType.FOP.getValue());
			lstSettlementType.add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementType.ECE.getCode());
			paramTab.setParameterName(SettlementType.ECE.getValue());
			lstSettlementType.add(paramTab);
			searchOperationTO.setLstSettlementType(lstSettlementType);
			List<ParameterTable> lstSettlementSchema = new ArrayList<ParameterTable>();
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
			paramTab.setParameterName(SettlementSchemaType.NET.getValue());
			lstSettlementSchema.add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
			paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
			lstSettlementSchema.add(paramTab);
			paramTab = new ParameterTable();
			searchOperationTO.setLstSettlementSchema(lstSettlementSchema);
	}
	
	/**
	 * Fill parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	public void fillParameters() throws ServiceException{
		mpParameters = new HashMap<Integer,  String>();

		ParameterTableTO paramTO = new ParameterTableTO();
		paramTO.setLstMasterTableFk(new ArrayList<Integer>());
		paramTO.getLstMasterTableFk().add(MasterTableType.SETTLEMENT_REQUEST_STATE.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.INSTRUMENT_TYPE.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.CURRENCY.getCode());
		paramTO.getLstMasterTableFk().add(MasterTableType.SECURITIES_CLASS.getCode());
		
		List<ParameterTable> lstParams = generalPametersFacade.getListParameterTableServiceBean(paramTO);
		
		for(ParameterTable paramTab:lstParams){		
			if(MasterTableType.SETTLEMENT_REQUEST_STATE.getCode().equals(paramTab.getMasterTable().getMasterTablePk())){
				searchOperationTO.getLstStates().add(paramTab);
			}else if(MasterTableType.CURRENCY.getCode().equals(paramTab.getMasterTable().getMasterTablePk())){
				searchOperationTO.getLstCurrency().add(paramTab);
			} else if (MasterTableType.SECURITIES_CLASS.getCode().equals(paramTab.getMasterTable().getMasterTablePk()))
				if (paramTab.getText1() != null)
					searchOperationTO.getLstSecurityClass().add(paramTab);
			mpParameters.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
	}
	
	/**
	 * Change participant search.
	 */ 
	
	public void changeParticipantSearch(){
		searchOperationTO.setHolder(new Holder());
		searchOperationTO.setHolderAccount(new HolderAccount());

		blNoResult = false;
		mechanismOperationDataModel = null;
		mechanismOperationDataModelForcedPurcharse = null;
	}
	
	/**
	 * Change holder search.
	 */
	
	public void changeHolderSearch(){
		searchOperationTO.setHolderAccount(new HolderAccount());
	}
	
	/**
	 * On change negotiation mechanism.
	 */
	public void onChangeNegotiationMechanism(){
		try {
			searchOperationTO.setNegotiationModalitySelected(null);
			if(cevaldomUser){				
				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModalityReport(
																						searchOperationTO.getNegotiationMechanismSelected()));
				searchOperationTO.setParticipantSelected(null);
				//searchOperationTO.setLstParticipant(null);
			}else if(participantUser){
				
				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModalityReport(
						searchOperationTO.getNegotiationMechanismSelected()));
				
//				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModalityForParticipant(
//																						searchOperationTO.getNegotiationMechanismSelected(), 
//																						searchOperationTO.getParticipantSelected()));		
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo para limpiar la pantalla de administracion
	 */
	public void clean(){
		JSFUtilities.resetViewRoot();
		blNoResult = false;
		settlementReportDataModel = null;
		selectsettlementReportons = new ArrayList<SettlementReportTO>();
		init();
	}
	
	/**
	 * Metodo para limpiar la pantalla de registro
	 */
	public void cleanRegister(){
		JSFUtilities.resetViewRoot();
		blNoResult = false;
		mechanismOperationDataModel = null;
		mechanismOperationDataModelForcedPurcharse = null;
		manualRegister();
	}
	
	/**
	 * On change negotitation modality.
	 */
	public void onChangeNegotitationModality(){
		try {
			if(!participantUser){
				searchOperationTO.setParticipantSelected(null);
				searchOperationTO.setLstParticipant(mcnOperationServiceFacade.getListParticipants(searchOperationTO.getNegotiationMechanismSelected()
																					, searchOperationTO.getNegotiationModalitySelected(),null,null));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search operations.
	 */
	@LoggerAuditWeb
	public void searchOperations(){
		try {
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				searchOperationTO.setIdSecurityCodePk(security.getIdSecurityCodePk());
			}else{
				searchOperationTO.setIdSecurityCodePk(null);
			}
			
			List<MechanismOperationTO> lstResult = mcnOperationServiceFacade.searchOperationsUnfulfillment(searchOperationTO);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				blNoResult = false;
				mechanismOperationDataModel = new GenericDataModel<MechanismOperationTO>(lstResult);
			}else{
				mechanismOperationDataModel = null;
				blNoResult = true;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Search operations.
	 */
	@LoggerAuditWeb
	public void searchRequestReport(){
		try {
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				searchOperationTO.setIdSecurityCodePk(security.getIdSecurityCodePk());
			}else{
				searchOperationTO.setIdSecurityCodePk(null);
			}
			//limpiamos la seleccion
			selectsettlementReportons = new ArrayList<SettlementReportTO>();
			
			List<SettlementReportTO> lstResult = mcnOperationServiceFacade.searchOperationsUnfulfillmentSearch(searchOperationTO);
					
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				blNoResult = false;
				settlementReportDataModel = new GenericDataModel<SettlementReportTO>(lstResult);
			}else{
				settlementReportDataModel = null;
				blNoResult = true;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search operations.
	 */
	@LoggerAuditWeb
	public void searchRequestReportV(){
		try {
			if(Validations.validateIsNotNullAndNotEmpty(issuerName)){
				searchOperationTO.setIdIssuerPk(issuerName);
			}else{
				searchOperationTO.setIdIssuerPk(null);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				searchOperationTO.setIdSecurityCodePk(security.getIdSecurityCodePk());
			}else{
				searchOperationTO.setIdSecurityCodePk(null);
			}
			
			//limpiamos la seleccion
			selectsettlementReportons = new ArrayList<SettlementReportTO>();
			
			List<SettlementReportTO> lstResult = mcnOperationServiceFacade.searchOperationsUnfulfillmentV(searchOperationTO);
					
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				blNoResult = false;
				settlementReportDataModel = new GenericDataModel<SettlementReportTO>(lstResult);
			}else{
				settlementReportDataModel = null;
				blNoResult = true;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 
	 */
	public void searchOperationsIssue1141(){
		try {
			if(Validations.validateIsNotNullAndNotEmpty(issuerName)){
				searchOperationTO.setIdIssuerPk(issuerName);
			}else{
				searchOperationTO.setIdIssuerPk(null);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				searchOperationTO.setIdSecurityCodePk(security.getIdSecurityCodePk());
			}else{
				searchOperationTO.setIdSecurityCodePk(null);
			}
			
			List<MechanismOperationTO> lstResult = mcnOperationServiceFacade.searchOperationsIssue1141(searchOperationTO);
			
			List<MechanismOperationTO> lstResultForcedPurchase=new ArrayList<>();
			for (MechanismOperationTO moResult : lstResult)
				if (moResult.getForcedPurchase())
					lstResultForcedPurchase.add(moResult);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				blNoResult = false;
				mechanismOperationDataModel = new GenericDataModel<MechanismOperationTO>(lstResult);
				mechanismOperationDataModelForcedPurcharse = new GenericDataModel<MechanismOperationTO>(lstResultForcedPurchase);
			}else{
				mechanismOperationDataModel = null;
				mechanismOperationDataModelForcedPurcharse = null;
				blNoResult = true;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * View mechanism operation.
	 *
	 * @param event the event
	 */
	/**
	 * Check security transfer available.
	 *
	 * @param e the e
	 * @throws ServiceException 
	 */
	/*public void selectOperation(SelectEvent e) throws ServiceException{
		
		MechanismOperationTO securityTransferOperation = (MechanismOperationTO)e.getObject();
		
		Boolean result = mcnOperationServiceFacade.searchOperationExist(securityTransferOperation.getIdMechanismOperationPk());
		
		if(result.equals(true)) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("unfulfillment.operation.message.register"));
					JSFUtilities.showSimpleValidationDialog();
		}
		
		//for(MechanismOperationTO arrTransferOperations:mechanismOperationDataModel){		}
		Integer res = 0;
		for(MechanismOperationTO arrTransferOperation:selectedMechanismOperations){
			
			res++;
			
			if( arrTransferOperation.getIdMechanismOperationPk().equals(securityTransferOperation.getIdMechanismOperationPk()) ){
				selectedMechanismOperations.remove(0);
			}
		}
	}*/
	
	/**
	 * Un check security transfer available.
	 *
	 * @param e the e
	 */
	//public void unCheckSecurityTransferAvailable(UnselectEvent e){
		//MechanismOperationTO securityTransferOperation = (MechanismOperationTO)e.getObject();
		//securityTransferOperation.setSelected(false);
		/*for(SecurityTransferOperation arrTransferOperation:lstTransferSecurityAvailable){
			
			if( arrTransferOperation.getIdTransferOperationPk().equals(securityTransferOperation.getIdTransferOperationPk()) ){
				arrTransferOperation.setSelected(false);
				
				if(arrTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) &&
					arrTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
				){
					arrTransferOperation.setSourceHolderAccount(new HolderAccount());
				}
				if(arrTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) &&
					arrTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
				){
					arrTransferOperation.setTargetHolderAccount(new HolderAccount());
				}
			}
		}
		dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);*/
	//}
	
	
	/**
	 * View mechanism operation.
	 *
	 * @param event the event
	 */
	public void viewMechanismOperation(ActionEvent event){
		Long idMechanismOperation = (Long)event.getComponent().getAttributes().get("mechanismOperation");
		
		operationBusinessComp.setIdMechanismOperationPk(idMechanismOperation);
		operationBusinessComp.setShowAssignments(true);
		operationBusinessComp.setShowViewDetails(true);
		operationBusinessComp.searchMechanismOperation();
	}
	
	
	/** 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * issue 1141 apliacion plazo reporto */
	public void confirmReportExtended() {
		
		JSFUtilities.showComponent(":idCofirmExtended");
		JSFUtilities.showMessageOnDialog(PropertiesConstants.FUNDS_OPERATION_BEFORE_SAVE_MESSAGE, null,
				PropertiesConstants.MSG_CONFIRM_REPORT_EXTENDED, null);
		
	}	
	
	
	/**
	 * Metodo del boton Grabar en pantalla de registro de SEDIR
	 */
	public void registerRequestSettlementUnfulfillment() {
		
		List<Integer> concat = new ArrayList<Integer>();
		
		//Validamos que se seleccione al menos 1 registro
		if (selectedMechanismOperations.size() < GeneralConstants.ONE_VALUE_INTEGER) {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}else {
			
			for(MechanismOperationTO selectedOperation : selectedMechanismOperations) {
				
				//validamos que se seleccione una opcion de devolucion
				if(!Validations.validateIsNotNullAndNotEmpty(selectedOperation.getReturnBalance())) {
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("inegotiations.unfulfillment.message.option"));
							JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			
			//concatenamos los id para mostrar en el mensaje
			for(MechanismOperationTO selectedOperation : selectedMechanismOperations) {
							concat.add(Integer.valueOf(selectedOperation.getBallotNumber().toString()));
						
			}
						
			JSFUtilities.showMessageOnDialog((GeneralConstants.LBL_HEADER_ALERT), null,
						PropertiesConstants.UNFULFILLMENT_OPERATION_REGISTER, new Object[]{StringUtils.join(concat,", ")});
			JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
		}
	}	
	
	
	/**
	 * Before to detail.
	 *
	 * @param button the button
	 * @throws InterruptedException the interrupted exception
	 * @throws ServiceException the service exception
	 */
	public String beforeToDetail(String button) throws InterruptedException, ServiceException{
		
		//Verificamos que se seleccionen registros 
		if(selectsettlementReportons.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			
			Boolean message = false;
			
				//Segundo validar que todas las opciones tengan el mismo estado valido para ejecutar la accion
				for (SettlementReportTO listToValidate : selectsettlementReportons){
					if(button.equals(APPROVE) || button.equals(ANNULATE)){
						if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.REGISTERED.getCode())){
							
							message = true;
							
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
									JSFUtilities.showSimpleValidationDialog();
									return null;
							}
					}else{
						if(button.equals(REVIEW)){
							if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.APPROVED.getCode())){
								
								message = true;
								
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
										JSFUtilities.showSimpleValidationDialog();
										return null;
							}
						}else{
							if(button.equals(CONFIRM)){
								if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.REVIEW.getCode())){
									
									message = true;
									
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
											JSFUtilities.showSimpleValidationDialog();
											return null;
								}
							}else{
								if(button.equals(REJECT)){
									if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.APPROVED.getCode()) 
											&& !listToValidate.getRequestState().equals(SettlementDateRequestStateType.REVIEW.getCode())
											&& !listToValidate.getRequestState().equals(SettlementDateRequestStateType.CONFIRMED.getCode())){
										
										message = true;
										
										showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
												, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
												JSFUtilities.showSimpleValidationDialog();
												return null;
									}
								}else {
									if(button.equals(AUTHORIZE)){
										if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.CONFIRMED.getCode())){
											
//											JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
//											JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').hide()");
											
											message = true;
											
											showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
													, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
													JSFUtilities.showSimpleValidationDialog();
													return null;
										}
									}
								}
							}
						}
					}
				}
				
				if (message.equals(false)) {
					//Si pasa las validaciones enviamos mensaje de confirmacion
					String messageProperties = "";
					switch(button){
						case APPROVE:	messageProperties = PropertiesConstants.UNFULFILLMENT_OPERATION_APPROVE_CONFIRM_MULTIPLE;
										option = APPROVE; break;
						case REVIEW:	messageProperties = PropertiesConstants.UNFULFILLMENT_OPERATION_REVIEW_CONFIRM_MULTIPLE;
										option = REVIEW; break;
						case CONFIRM:	messageProperties = PropertiesConstants.UNFULFILLMENT_OPERATION_CONFIRM_CONFIRM_MULTIPLE;
										option = CONFIRM; break;
						case ANNULATE:	messageProperties = PropertiesConstants.UNFULFILLMENT_OPERATION_ANNULATE_CONFIRM_MULTIPLE;
										option = ANNULATE; break;
						case REJECT:	messageProperties = PropertiesConstants.UNFULFILLMENT_OPERATION_REJECT_CONFIRM_MULTIPLE;
										option = REJECT; break;
						case AUTHORIZE:	messageProperties = PropertiesConstants.UNFULFILLMENT_OPERATION_AUTHORIZE_CONFIRM_MULTIPLE;
										option = AUTHORIZE; break;				
						
					}
						//Concatenamos los ID para mostrar en pantalla
						List <Integer> idsConcat = new ArrayList<>();
						for(SettlementReportTO idConcat : selectsettlementReportons){
							idsConcat.add(idConcat.getIdSettlementReportPk().intValue());
						}
					//Mostramos mensaje	
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(messageProperties,new Object[]{StringUtils.join(idsConcat,",")}));
					JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
					
					/*JSFUtilities.showComponent(":idCofirmExtended");
					JSFUtilities.showMessageOnDialog(messageProperties, null,
							messageProperties, new Object[]{StringUtils.join(idsConcat,",")});*/
				}
		}else{
			//Si no se selecciona nungun registro enviamos mensaje
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		return null;
	}

	
	/**
	 * Metodo para registrar las solicitudes SEDIR
	 */
	@LoggerAuditWeb
	public void saveRequestSettlementUnfulfillment() {
		
		SettlementUnfulfillment settlementUnfulfillment = null;
		
		//Recorremos el listado de operaciones seleccionadas
		for (MechanismOperationTO listMOExtended : selectedMechanismOperations) {
			settlementUnfulfillment = new SettlementUnfulfillment();
			
			OperationUnfulfillment operationUnfulfillment = new OperationUnfulfillment();
			
			operationUnfulfillment = mcnOperationServiceFacade.getOperationUnfulfillment(listMOExtended.getIdOperationUnfulfillment());
			
			settlementUnfulfillment.setOperationUnfulfillment(operationUnfulfillment);
			settlementUnfulfillment.setRequestState(SettlementDateRequestStateType.REGISTERED.getCode());
			
			if(listMOExtended.getReturnBalance().equals("VENDEDOR")) {
				settlementUnfulfillment.setIndBuyerSeller(GeneralConstants.ONE_VALUE_INTEGER);
			}else {
				settlementUnfulfillment.setIndBuyerSeller(GeneralConstants.ZERO_VALUE_INTEGER);
			}

			try {
				mcnOperationServiceFacade.saveSettlementUnfulfillment(settlementUnfulfillment);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		
		List <Integer> concat = new ArrayList<Integer>();
		
//		JSFUtilities.showMessageOnDialog((GeneralConstants.LBL_HEADER_ALERT), null,
//				PropertiesConstants.SEDIR_OPERATION_REGISTER, new Object[]{StringUtils.join(concat,", ")});
//		
		
//		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
//				PropertiesUtilities.getMessage(PropertiesConstants.EXTENDED_REPORTED_TERM));
		
		clean();
		
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.UNFULFILLMENT_OPERATION_REGISTER_CORRECT, new Object[]{concat}));
		
//		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
//				PropertiesUtilities.getMessage(PropertiesConstants.EXTENDED_REPORTED_TERM));
//		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		
		
		
		//searchMCNOperations();
	}
	
	/**
	 * 
	 * @return
	 */
	public String validateReject(){
		JSFUtilities.hideGeneralDialogues();
		//return validateAction(SettlementDateRequestStateType.REJECTED.getCode());
		
		return "TEST";
	}
	
	//agregamos un dia
	/*Date settlementDateAdd = (Date) CommonsUtilities.addDate(listMOExtended.getTermSettlementDate(), 1);	*/
	/*//debe ser un dia habil
	while (holidayQueryServiceBean.isNonWorkingDayServiceBean(settlementDateAdd,true)) {
		settlementDateAdd = (Date) CommonsUtilities.addDate(settlementDateAdd,1);
		settlementDaysAdd = settlementDaysAdd + 1;
	}	*/
	
	
	/**
	 * Metodo para confirmar la ampliacion de 1 dia
	 */
	public void saveConfirmationExtended() {
		
			//for (MechanismOperationTO listMOExtended : mechanismOperationDataModel) {
		
		//Recorremos el listado de operaciones seleccionadas para ampliar
		for (MechanismOperationTO listMOExtended : selectedMechanismOperations) {
			
			//agregamos un dia
			Date settlementDateAdd = (Date) CommonsUtilities.addDate(listMOExtended.getTermSettlementDate(), 1);	
			
			Long settlementDaysAdd = listMOExtended.getTermSettlementDays() + 1;
			
			//debe ser un dia habil
			while (holidayQueryServiceBean.isNonWorkingDayServiceBean(settlementDateAdd,true)) {
				settlementDateAdd = (Date) CommonsUtilities.addDate(settlementDateAdd,1);
				settlementDaysAdd = settlementDaysAdd + 1;
			}		
			
			BigDecimal settlementPriceAdd, settlementAmountAdd;
			BigDecimal newTermPrice = listMOExtended.getCashSettlementPrice().setScale(2,RoundingMode.HALF_UP);
			
			//calculamos nuevo precio
			settlementPriceAdd = NegotiationUtils.getTermPrice(newTermPrice, listMOExtended.getAmountRate(), settlementDaysAdd);
			
			//p*q cantidad negociable settlement_operation
			SettlementOperation settlementOperationTerm = (SettlementOperation) settlementProcessService.getSettlementOperation(
							listMOExtended.getIdMechanismOperationPk(), 
							ComponentConstant.TERM_PART);
			
			settlementAmountAdd = settlementPriceAdd.multiply(settlementOperationTerm.getStockQuantity());

			mcnOperationServiceFacade.updateMechanismOperationSettlement(
					listMOExtended.getIdMechanismOperationPk(),
					settlementDateAdd,
					settlementDaysAdd,
					settlementPriceAdd,
					settlementAmountAdd);
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.EXTENDED_REPORTED_TERM));
		JSFUtilities.showSimpleValidationDialog();
		searchOperationsIssue1141();
		JSFUtilities.updateComponent("frmQueryOperations:opnlResult");
	}

	
		/*
		//Verificamos que se seleccionen registros 
		if(selectsettlementReportons.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			
				//Segundo validar que todas las opciones tengan el mismo estado valido para ejecutar la accion
				for (SettlementReportTO listToValidate : selectsettlementReportons){
					if(button.equals(APPROVE) || button.equals(ANNULATE)){
						if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.REGISTERED.getCode())){
							
							JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
							JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').hide()");
							
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
									JSFUtilities.showSimpleValidationDialog();
									return null;
							}
					}else{
						if(button.equals(REVIEW)){
							if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.APPROVED.getCode())){
								
								JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
								JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').hide()");
								
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
										JSFUtilities.showSimpleValidationDialog();
										return null;
							}
						}else{
							if(button.equals(CONFIRM)){
								if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.REVIEW.getCode())){
									
									JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
									JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').hide()");
									
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
											JSFUtilities.showSimpleValidationDialog();
											return null;
								}
							}else{
								if(button.equals(REJECT)){
									if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.APPROVED.getCode()) 
											&& !listToValidate.getRequestState().equals(SirtexOperationStateType.REVIEWED.getCode()) ){
										
										JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
										JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').hide()");
										
										showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
												, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
												JSFUtilities.showSimpleValidationDialog();
												return null;
									}
								}else {
									if(button.equals(AUTHORIZE)){
										if(!listToValidate.getRequestState().equals(SettlementDateRequestStateType.AUTHORIZE.getCode()) 
												&& !listToValidate.getRequestState().equals(SirtexOperationStateType.REVIEWED.getCode()) ){
											
											JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
											JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').hide()");
											
											showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
													, PropertiesUtilities.getMessage("msg.sirtex.multiple.action"));
													JSFUtilities.showSimpleValidationDialog();
													return null;
										}
									}
								}
							}
						}
					}
				}
											
				//Si pasa las validaciones enviamos mensaje de confirmacion
				String messageProperties = "";
				switch(button){
					case APPROVE:	messageProperties = PropertiesConstants.SEDIR_OPERATION_APPROVE_CONFIRM_MULTIPLE;
									option = APPROVE; break;
					case REVIEW:	messageProperties = PropertiesConstants.SEDIR_OPERATION_REVIEW_CONFIRM_MULTIPLE;
									option = REVIEW; break;
					case CONFIRM:	messageProperties = PropertiesConstants.SEDIR_OPERATION_CONFIRM_CONFIRM_MULTIPLE;
									option = CONFIRM; break;
					case ANNULATE:	messageProperties = PropertiesConstants.SEDIR_OPERATION_ANNULATE_CONFIRM_MULTIPLE;
									option = ANNULATE; break;
					case REJECT:	messageProperties = PropertiesConstants.SEDIR_OPERATION_REJECT_CONFIRM_MULTIPLE;
									option = REJECT; break;
					case AUTHORIZE:	messageProperties = PropertiesConstants.SEDIR_OPERATION_REJECT_CONFIRM_MULTIPLE;
									option = AUTHORIZE; break;				
					
				}
					//Concatenamos los ID para mostrar en pantalla
					List <Integer> idsConcat = new ArrayList<>();
					for(SettlementReportTO idConcat : selectsettlementReportons){
						idsConcat.add(idConcat.getIdSettlementReportPk().intValue());
					}
				//Mostramos mensaje	
				/*showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(messageProperties,new Object[]{StringUtils.join(idsConcat,",")}));
				JSFUtilities.executeJavascriptFunction("PF('idCofirmExtended').show()");
				
				JSFUtilities.showComponent(":idCofirmExtended");
				JSFUtilities.showMessageOnDialog(messageProperties, null,
						messageProperties, new Object[]{StringUtils.join(idsConcat,",")});
			
		}else{
			//Si no se selecciona nungun registro enviamos mensaje
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		return null;
	
	*/
	
	
	
	
	/**
	 * Metodo para cambiar el estado de la SEDIR
	 */
	@LoggerAuditWeb
	public void saveReportExtended(){
		
		Integer state = null;
		String messageProperties2 = "";
		List <Integer> idsConcat = new ArrayList<>();
		
		if(option.equals(APPROVE)) {
			messageProperties2 = PropertiesConstants.UNFULFILLMENT_OPERATION_APPROVE_OK;
			state = SettlementDateRequestStateType.APPROVED.getCode();
		}else if(option.equals(ANNULATE)) {
			messageProperties2 = PropertiesConstants.UNFULFILLMENT_OPERATION_ANNULATE_OK;
			state = SettlementDateRequestStateType.ANNULATE.getCode();
		}else if(option.equals(REVIEW)) {
			messageProperties2 = PropertiesConstants.UNFULFILLMENT_OPERATION_REVIEW_OK;
			state = SettlementDateRequestStateType.REVIEW.getCode();
		}else if(option.equals(REJECT)) {
			messageProperties2 = PropertiesConstants.UNFULFILLMENT_OPERATION_REJECT_OK;
			state = SettlementDateRequestStateType.REJECTED.getCode();
		}else if(option.equals(CONFIRM)) {
			messageProperties2 = PropertiesConstants.UNFULFILLMENT_OPERATION_CONFIRM_OK;
			state = SettlementDateRequestStateType.CONFIRMED.getCode();
		}else if(option.equals(AUTHORIZE)) {
			messageProperties2 = PropertiesConstants.UNFULFILLMENT_OPERATION_AUTHORIZE_OK;
			state = SettlementDateRequestStateType.AUTHORIZE.getCode();
		}
		
		//Actualizamos el estado de las operaciones segun el privilegio
		if (!state.equals(SettlementDateRequestStateType.AUTHORIZE.getCode())) {
			for (SettlementReportTO listMOExtended : selectsettlementReportons) {
				idsConcat.add(listMOExtended.getIdSettlementReportPk().intValue());
				mcnOperationServiceFacade.updateSettlementUnfulfillment(listMOExtended.getIdSettlementReportPk(),state);
			}
			
			//Solo se realizan los cambios de fecha en el caso de AUTORIZAR
		}else {
			//Recorremos el listado de operaciones seleccionadas para ampliar
			for (SettlementReportTO listMOExtended : selectsettlementReportons) {
				
				//Aqui liquidamos la operacion
				mcnOperationServiceFacade.settledOperation(listMOExtended);
				
				//actualizamos el estado de la solicitud
				idsConcat.add(listMOExtended.getIdSettlementReportPk().intValue());
				mcnOperationServiceFacade.updateSettlementUnfulfillment(listMOExtended.getIdSettlementReportPk(),state);
				
				//aca dejamos el estado de la operacion MO y SO en estado cancelada plazo
				mcnOperationServiceFacade.updateUnfulfillmentMechOperaSett(listMOExtended.getMechanismOperation().getIdMechanismOperationPk(),
						listMOExtended.getIdSettlementReportPk());
				
			}
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(messageProperties2,new Object[]{StringUtils.join(idsConcat,",")}));
		JSFUtilities.showSimpleValidationDialog();
		//searchOperationsIssue1141();
		searchRequestReportV();
		JSFUtilities.updateComponent("frmQueryOperations:opnlResult");
	}
	
	
	
	
	
	/**
	 * Metodo que se ejecuta al presionar el boton Registrar en pantalla de administracion
	 * @return
	 */
	public String manualRegister() {
		try {
			searchOperationTO = new SearchMCNOperationsTO();	
			quantityReg = null;
			mechanismOperationDataModel = null;
			selectedMechanismOperations = null;
			issuerName = null;
			security = new Security();
			lstReturnBalance = new ArrayList<String>();
			lstReturnBalance.add("COMPRADOR");
			lstReturnBalance.add("VENDEDOR");
			
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				cevaldomUser = true;
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(1));
				//searchOperationTO.setNegotiationMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
				searchOperationTO.setLstParticipant(mcnOperationServiceFacade.getLstParticipants(null));
			}else if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())){
				//searchOperationTO.setNegotiationMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
				participantUser = true;
				searchOperationTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				searchOperationTO.setLstParticipant(mcnOperationServiceFacade.getLstParticipants(null));
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(1));
				//searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanismByParticipant(searchOperationTO.getParticipantSelected()));
			}else {
				mechanismUser = true;
				searchOperationTO.setLstNegotiationMechanisms(mcnOperationServiceFacade.getLstNegotiationMechanism(1));						
				if(InstitutionType.BOLSA.getCode().equals(userInfo.getUserAccountSession().getInstitutionType())){
					searchOperationTO.setNegotiationMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
				}
				searchOperationTO.setLstNegotiationModalities(mcnOperationServiceFacade.getLstNegotiationModalityReport(searchOperationTO.getNegotiationMechanismSelected()));				
			}			
			searchOperationTO.setInitialDate(getCurrentSystemDate());
			searchOperationTO.setEndDate(getCurrentSystemDate());
			searchOperationTO.setDateTypeSelected(GeneralConstants.ZERO_VALUE_INTEGER);
			setListIssuer(mcnOperationServiceFacade.issuerList());
			fillCombos();
			fillParameters();
			
			return REGISTER_MANUAL_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		
		blNoResult = false;
		mechanismOperationDataModel = null;
		mechanismOperationDataModelForcedPurcharse = null;
		
	}
	
	/**
	 * Back to manage.
	 *
	 * @return the string
	 */
	public String backToManage(){
		return SEARCH_MAPPING;
	}
	
	/**
	 * Gets the search operation to.
	 *
	 * @return the search operation to
	 */
	public SearchMCNOperationsTO getSearchOperationTO() {
		return searchOperationTO;
	}

	/**
	 * Sets the search operation to.
	 *
	 * @param searchOperationTO the new search operation to
	 */
	public void setSearchOperationTO(SearchMCNOperationsTO searchOperationTO) {
		this.searchOperationTO = searchOperationTO;
	}

	/**
	 * Checks if is cevaldom user.
	 *
	 * @return true, if is cevaldom user
	 */
	public boolean isCevaldomUser() {
		return cevaldomUser;
	}
	
	/**
	 * Sets the cevaldom user.
	 *
	 * @param cevaldomUser the new cevaldom user
	 */
	public void setCevaldomUser(boolean cevaldomUser) {
		this.cevaldomUser = cevaldomUser;
	}
	
	/**
	 * Checks if is participant user.
	 *
	 * @return true, if is participant user
	 */
	public boolean isParticipantUser() {
		return participantUser;
	}
	
	/**
	 * Sets the participant user.
	 *
	 * @param participantUser the new participant user
	 */
	public void setParticipantUser(boolean participantUser) {
		this.participantUser = participantUser;
	}
	
	/**
	 * Checks if is mechanism user.
	 *
	 * @return true, if is mechanism user
	 */
	public boolean isMechanismUser() {
		return mechanismUser;
	}
	
	/**
	 * Sets the mechanism user.
	 *
	 * @param mechanismUser the new mechanism user
	 */
	public void setMechanismUser(boolean mechanismUser) {
		this.mechanismUser = mechanismUser;
	}
	
	/**
	 * Gets the mechanism operation data model.
	 *
	 * @return the mechanism operation data model
	 */
	public GenericDataModel<MechanismOperationTO> getMechanismOperationDataModel() {
		return mechanismOperationDataModel;
	}

	/**
	 * Sets the mechanism operation data model.
	 *
	 * @param mechanismOperationDataModel the new mechanism operation data model
	 */
	public void setMechanismOperationDataModel(
			GenericDataModel<MechanismOperationTO> mechanismOperationDataModel) {
		this.mechanismOperationDataModel = mechanismOperationDataModel;
	}

	
	public GenericDataModel<SettlementReportTO> getSettlementReportDataModel() {
		return settlementReportDataModel;
	}

	public void setSettlementReportDataModel(GenericDataModel<SettlementReportTO> settlementReportDataModel) {
		this.settlementReportDataModel = settlementReportDataModel;
	}

	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}

	/**
	 * Gets the mp parameters.
	 *
	 * @return the mp parameters
	 */
	public Map<Integer, String> getMpParameters() {
		return mpParameters;
	}

	/**
	 * Sets the mp parameters.
	 *
	 * @param mpParameters the mp parameters
	 */
	public void setMpParameters(Map<Integer, String> mpParameters) {
		this.mpParameters = mpParameters;
	}

	/**
	 * Sets the mp parameters.
	 *
	 * @param mpParameters the mp parameters
	 */
	public void setMpParameters(HashMap<Integer, String> mpParameters) {
		this.mpParameters = mpParameters;
	}
	//issue 825
	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public List<Issuer> getListIssuer() {
		return listIssuer;
	}

	public void setListIssuer(List<Issuer> listIssuer) {
		this.listIssuer = listIssuer;
	}

	public GenericDataModel<MechanismOperationTO> getMechanismOperationDataModelForcedPurcharse() {
		return mechanismOperationDataModelForcedPurcharse;
	}

	public void setMechanismOperationDataModelForcedPurcharse(GenericDataModel<MechanismOperationTO> mechanismOperationDataModelForcedPurcharse) {
		this.mechanismOperationDataModelForcedPurcharse = mechanismOperationDataModelForcedPurcharse;
	}

	public List<MechanismOperationTO> getSelectedMechanismOperations() {
		return selectedMechanismOperations;
	}
	
	public void setSelectedMechanismOperations(List<MechanismOperationTO> selectedMechanismOperations) {
		this.selectedMechanismOperations = selectedMechanismOperations;
	}

	public List<SettlementReportTO> getSelectsettlementReportons() {
		return selectsettlementReportons;
	}

	public void setSelectsettlementReportons(List<SettlementReportTO> selectsettlementReportons) {
		this.selectsettlementReportons = selectsettlementReportons;
	}

	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}

	public Integer getQuantityReg() {
		return quantityReg;
	}

	public void setQuantityReg(Integer quantityReg) {
		this.quantityReg = quantityReg;
	}

	public boolean isBlDepositary() {
		return blDepositary;
	}

	public void setBlDepositary(boolean blDepositary) {
		this.blDepositary = blDepositary;
	}

	public boolean isBlParticipant() {
		return blParticipant;
	}

	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}

	public List<String> getLstReturnBalance() {
		return lstReturnBalance;
	}

	public void setLstReturnBalance(List<String> lstReturnBalance) {
		this.lstReturnBalance = lstReturnBalance;
	}
	
	
	

}
