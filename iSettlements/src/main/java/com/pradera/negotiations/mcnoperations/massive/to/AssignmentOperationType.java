package com.pradera.negotiations.mcnoperations.massive.to;

import java.math.BigDecimal;
import java.util.Date;

import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.model.negotiation.HolderAccountOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AssignmentOperationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AssignmentOperationType extends ValidationOperationType {

	/** The reference number. */
	private String referenceNumber;
	
	/** The reference date. */
	private Date referenceDate;
	
	/** The operation class. */
	private String operationClass;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The assignment part mnemonic. */
	private String assignmentPartMnemonic;

	/** The operation type. */
	private String operationType;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The market date. */
	private Date marketDate;
	
	/** The market rate. */
	private BigDecimal marketRate;	// both optional
	
	/** The market price. */
	private BigDecimal marketPrice;	// both optional
	
	/** The security code. */
	private String securityCode;
	
	/** The source security code. */
	private String sourceSecurityCode; // optional
	
	/** The target security code. */
	private String targetSecurityCode; // optional

	private String securityClass;
	// added later
	
	/** The holder account operation. */
	private HolderAccountOperation holderAccountOperation;
	
	/**
	 * Gets the reference number.
	 *
	 * @return the reference number
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * Sets the reference number.
	 *
	 * @param referenceNumber the new reference number
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * Gets the reference date.
	 *
	 * @return the reference date
	 */
	public Date getReferenceDate() {
		return referenceDate;
	}

	/**
	 * Sets the reference date.
	 *
	 * @param referenceDate the new reference date
	 */
	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	/**
	 * Gets the operation class.
	 *
	 * @return the operation class
	 */
	public String getOperationClass() {
		return operationClass;
	}

	/**
	 * Sets the operation class.
	 *
	 * @param operationClass the new operation class
	 */
	public void setOperationClass(String operationClass) {
		this.operationClass = operationClass;
	}

	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}

	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}

	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}

	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}

	/**
	 * Gets the assignment part mnemonic.
	 *
	 * @return the assignment part mnemonic
	 */
	public String getAssignmentPartMnemonic() {
		return assignmentPartMnemonic;
	}

	/**
	 * Sets the assignment part mnemonic.
	 *
	 * @param assignmentPartMnemonic the new assignment part mnemonic
	 */
	public void setAssignmentPartMnemonic(String assignmentPartMnemonic) {
		this.assignmentPartMnemonic = assignmentPartMnemonic;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the source security code.
	 *
	 * @return the source security code
	 */
	public String getSourceSecurityCode() {
		return sourceSecurityCode;
	}

	/**
	 * Sets the source security code.
	 *
	 * @param sourceSecurityCode the new source security code
	 */
	public void setSourceSecurityCode(String sourceSecurityCode) {
		this.sourceSecurityCode = sourceSecurityCode;
	}

	/**
	 * Gets the target security code.
	 *
	 * @return the target security code
	 */
	public String getTargetSecurityCode() {
		return targetSecurityCode;
	}

	/**
	 * Sets the target security code.
	 *
	 * @param targetSecurityCode the new target security code
	 */
	public void setTargetSecurityCode(String targetSecurityCode) {
		this.targetSecurityCode = targetSecurityCode;
	}

	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	public String getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	
}
