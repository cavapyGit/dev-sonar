package com.pradera.negotiations.mcnoperations.to;

import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;

public class MCNPricesCalculatorTO {
	private String issuerName;
	private String idSecurityCodePk;
	private Integer currency;
	private BigDecimal nominalValue;
	private BigDecimal quantity;
	private BigDecimal pxq;
	private Date issuanceDate;
	private Date expirationDate;
	private Integer periodicity;
	private BigDecimal nominalRate;
	private BigDecimal dailyRate;
	private Date operationDate;
	private Date lastCoupon;
	private Integer elapsedDays;
	private BigDecimal accruedInterest;
	private BigDecimal cleanPrice;
	private BigDecimal cleanAmount;
	private BigDecimal dirtyPrice;
	private BigDecimal dirtyAmount;
	private String currencyDescription;
	private String periodicityDescription;
	private Integer nonPayedCouponDuration;
	
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
		this.currencyDescription = CurrencyType.get(currency).getValue();
	}
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	public BigDecimal getNominalValue() {
		return nominalValue;
	}
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getPxq() {
		return pxq;
	}
	public void setPxq(BigDecimal pxq) {
		this.pxq = pxq;
	}
	public Date getIssuanceDate() {
		return issuanceDate;
	}
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public Integer getPeriodicity() {
		return periodicity;
	}
	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
		this.periodicityDescription = InterestPeriodicityType.get(periodicity).getValue();
	}
	public BigDecimal getNominalRate() {
		return nominalRate;
	}
	public void setNominalRate(BigDecimal nominalRate) {
		this.nominalRate = nominalRate;
	}
	public BigDecimal getDailyRate() {
		return dailyRate;
	}
	public void setDailyRate(BigDecimal dailyRate) {
		this.dailyRate = dailyRate;
	}
	public Date getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	public Date getLastCoupon() {
		return lastCoupon;
	}
	public void setLastCoupon(Date lastCoupon) {
		this.lastCoupon = lastCoupon;
	}
	public Integer getElapsedDays() {
		return elapsedDays;
	}
	public void setElapsedDays(Integer elapsedDays) {
		this.elapsedDays = elapsedDays;
	}
	public BigDecimal getAccruedInterest() {
		return accruedInterest;
	}
	public void setAccruedInterest(BigDecimal accruedInterest) {
		this.accruedInterest = accruedInterest;
	}
	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}
	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}
	public BigDecimal getCleanAmount() {
		return cleanAmount;
	}
	public void setCleanAmount(BigDecimal cleanAmount) {
		this.cleanAmount = cleanAmount;
	}
	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}
	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}
	public String getPeriodicityDescription() {
		return periodicityDescription;
	}
	public void setPeriodicityDescription(String periodicityDescription) {
		this.periodicityDescription = periodicityDescription;
	}
	public Integer getNonPayedCouponDuration() {
		return nonPayedCouponDuration;
	}
	public void setNonPayedCouponDuration(Integer nonPayedCouponDuration) {
		this.nonPayedCouponDuration = nonPayedCouponDuration;
	}
	public BigDecimal getDirtyAmount() {
		return dirtyAmount;
	}
	public void setDirtyAmount(BigDecimal dirtyAmount) {
		this.dirtyAmount = dirtyAmount;
	}
	
}
