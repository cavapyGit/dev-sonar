package com.pradera.negotiations.positioncommission.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchPositionCommissionTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchPositionCommissionTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The participant selected. */
	private Long negoMechanismSelected, modalityGroupSelected, participantSelected;
	
	/** The settlement schema selected. */
	private Integer settlementSchemaSelected;
	
	/** The currency selected. */
	private Integer currencySelected;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism> lstNegotiationMechanism;
	
	/** The lst modality group. */
	private List<ModalityGroup> lstModalityGroup;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The bl no result by role. */
	private boolean blNoResult, blNoResultByRole;
	
	/** The lst position by participant. */
	private List<PositionParticipantTO> lstPositionByParticipant;
	
	/** The lst position by role. */
	private List<PositionRoleTO> lstPositionByRole;
	
	/** The lst position by modality group. */
	private List<PositionModalityGroupTO> lstPositionByModalityGroup;
	
	/** The total purchase. */
	private BigDecimal totalPurchase = BigDecimal.ZERO;
	
	/** The total sale. */
	private BigDecimal totalSale = BigDecimal.ZERO;
	
	/** The total debt. */
	private BigDecimal totalDebt = BigDecimal.ZERO;
	
	/** The total creditor. */
	private BigDecimal totalCreditor = BigDecimal.ZERO;
	
	/** The modality group header. */
	private Long participantHeader, modalityGroupHeader;
	
	/** The settlement schema hedaer. */
	private Integer settlementSchemaHedaer;
	
	/** The lst settlement schema. */
	private List<ParameterTable> lstSettlementSchema;
	
	/** The role. */
	private String role;
	
	/** The extended. */
	private boolean extended;
	
	/**
	 * Gets the nego mechanism selected.
	 *
	 * @return the nego mechanism selected
	 */
	public Long getNegoMechanismSelected() {
		return negoMechanismSelected;
	}
	
	/**
	 * Sets the nego mechanism selected.
	 *
	 * @param negoMechanismSelected the new nego mechanism selected
	 */
	public void setNegoMechanismSelected(Long negoMechanismSelected) {
		this.negoMechanismSelected = negoMechanismSelected;
	}
	
	/**
	 * Gets the modality group selected.
	 *
	 * @return the modality group selected
	 */
	public Long getModalityGroupSelected() {
		return modalityGroupSelected;
	}
	
	/**
	 * Sets the modality group selected.
	 *
	 * @param modalityGroupSelected the new modality group selected
	 */
	public void setModalityGroupSelected(Long modalityGroupSelected) {
		this.modalityGroupSelected = modalityGroupSelected;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public Integer getCurrencySelected() {
		return currencySelected;
	}
	
	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the lst modality group.
	 *
	 * @return the lst modality group
	 */
	public List<ModalityGroup> getLstModalityGroup() {
		return lstModalityGroup;
	}
	
	/**
	 * Sets the lst modality group.
	 *
	 * @param lstModalityGroup the new lst modality group
	 */
	public void setLstModalityGroup(List<ModalityGroup> lstModalityGroup) {
		this.lstModalityGroup = lstModalityGroup;
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Checks if is bl no result by role.
	 *
	 * @return true, if is bl no result by role
	 */
	public boolean isBlNoResultByRole() {
		return blNoResultByRole;
	}
	
	/**
	 * Sets the bl no result by role.
	 *
	 * @param blNoResultByRole the new bl no result by role
	 */
	public void setBlNoResultByRole(boolean blNoResultByRole) {
		this.blNoResultByRole = blNoResultByRole;
	}
	
	/**
	 * Gets the lst position by participant.
	 *
	 * @return the lst position by participant
	 */
	public List<PositionParticipantTO> getLstPositionByParticipant() {
		return lstPositionByParticipant;
	}
	
	/**
	 * Sets the lst position by participant.
	 *
	 * @param lstPositionByParticipant the new lst position by participant
	 */
	public void setLstPositionByParticipant(
			List<PositionParticipantTO> lstPositionByParticipant) {
		this.lstPositionByParticipant = lstPositionByParticipant;
	}
	
	/**
	 * Gets the lst position by role.
	 *
	 * @return the lst position by role
	 */
	public List<PositionRoleTO> getLstPositionByRole() {
		return lstPositionByRole;
	}
	
	/**
	 * Sets the lst position by role.
	 *
	 * @param lstPositionByRole the new lst position by role
	 */
	public void setLstPositionByRole(List<PositionRoleTO> lstPositionByRole) {
		this.lstPositionByRole = lstPositionByRole;
	}
	
	/**
	 * Gets the lst position by modality group.
	 *
	 * @return the lst position by modality group
	 */
	public List<PositionModalityGroupTO> getLstPositionByModalityGroup() {
		return lstPositionByModalityGroup;
	}
	
	/**
	 * Sets the lst position by modality group.
	 *
	 * @param lstPositionByModalityGroup the new lst position by modality group
	 */
	public void setLstPositionByModalityGroup(
			List<PositionModalityGroupTO> lstPositionByModalityGroup) {
		this.lstPositionByModalityGroup = lstPositionByModalityGroup;
	}
	
	/**
	 * Gets the total purchase.
	 *
	 * @return the total purchase
	 */
	public BigDecimal getTotalPurchase() {
		return totalPurchase;
	}
	
	/**
	 * Sets the total purchase.
	 *
	 * @param totalPurchase the new total purchase
	 */
	public void setTotalPurchase(BigDecimal totalPurchase) {
		this.totalPurchase = totalPurchase;
	}
	
	/**
	 * Gets the total sale.
	 *
	 * @return the total sale
	 */
	public BigDecimal getTotalSale() {
		return totalSale;
	}
	
	/**
	 * Sets the total sale.
	 *
	 * @param totalSale the new total sale
	 */
	public void setTotalSale(BigDecimal totalSale) {
		this.totalSale = totalSale;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the participant header.
	 *
	 * @return the participant header
	 */
	public Long getParticipantHeader() {
		return participantHeader;
	}
	
	/**
	 * Sets the participant header.
	 *
	 * @param participantHeader the new participant header
	 */
	public void setParticipantHeader(Long participantHeader) {
		this.participantHeader = participantHeader;
	}
	
	/**
	 * Gets the modality group header.
	 *
	 * @return the modality group header
	 */
	public Long getModalityGroupHeader() {
		return modalityGroupHeader;
	}
	
	/**
	 * Sets the modality group header.
	 *
	 * @param modalityGroupHeader the new modality group header
	 */
	public void setModalityGroupHeader(Long modalityGroupHeader) {
		this.modalityGroupHeader = modalityGroupHeader;
	}
	
	/**
	 * Gets the settlement schema hedaer.
	 *
	 * @return the settlement schema hedaer
	 */
	public Integer getSettlementSchemaHedaer() {
		return settlementSchemaHedaer;
	}
	
	/**
	 * Sets the settlement schema hedaer.
	 *
	 * @param settlementSchemaHedaer the new settlement schema hedaer
	 */
	public void setSettlementSchemaHedaer(Integer settlementSchemaHedaer) {
		this.settlementSchemaHedaer = settlementSchemaHedaer;
	}
	
	/**
	 * Gets the lst settlement schema.
	 *
	 * @return the lst settlement schema
	 */
	public List<ParameterTable> getLstSettlementSchema() {
		return lstSettlementSchema;
	}
	
	/**
	 * Sets the lst settlement schema.
	 *
	 * @param lstSettlementSchema the new lst settlement schema
	 */
	public void setLstSettlementSchema(List<ParameterTable> lstSettlementSchema) {
		this.lstSettlementSchema = lstSettlementSchema;
	}
	
	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
	/**
	 * Gets the settlement schema selected.
	 *
	 * @return the settlement schema selected
	 */
	public Integer getSettlementSchemaSelected() {
		return settlementSchemaSelected;
	}
	
	/**
	 * Sets the settlement schema selected.
	 *
	 * @param settlementSchemaSelected the new settlement schema selected
	 */
	public void setSettlementSchemaSelected(Integer settlementSchemaSelected) {
		this.settlementSchemaSelected = settlementSchemaSelected;
	}
	
	/**
	 * Gets the total debt.
	 *
	 * @return the total debt
	 */
	public BigDecimal getTotalDebt() {
		return totalDebt;
	}
	
	/**
	 * Sets the total debt.
	 *
	 * @param totalDebt the new total debt
	 */
	public void setTotalDebt(BigDecimal totalDebt) {
		this.totalDebt = totalDebt;
	}
	
	/**
	 * Gets the total creditor.
	 *
	 * @return the total creditor
	 */
	public BigDecimal getTotalCreditor() {
		return totalCreditor;
	}
	
	/**
	 * Sets the total creditor.
	 *
	 * @param totalCreditor the new total creditor
	 */
	public void setTotalCreditor(BigDecimal totalCreditor) {
		this.totalCreditor = totalCreditor;
	}
	
	/**
	 * Checks if is extended.
	 *
	 * @return true, if is extended
	 */
	public boolean isExtended() {
		return extended;
	}
	
	/**
	 * Sets the extended.
	 *
	 * @param extended the new extended
	 */
	public void setExtended(boolean extended) {
		this.extended = extended;
	}
}
