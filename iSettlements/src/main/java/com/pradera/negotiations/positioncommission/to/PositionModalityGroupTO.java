package com.pradera.negotiations.positioncommission.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PositionModalityGroupTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class PositionModalityGroupTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The position participant to. */
	private PositionParticipantTO positionParticipantTO;
	
	/** The deposited amount. */
	private BigDecimal depositedAmount;
	
	/** The paid amount. */
	private BigDecimal paidAmount;
	
	/** The net position. */
	private BigDecimal netPosition;
	
	/** The lst position by role buy. */
	private List<PositionRoleTO> lstPositionByRoleBuy;
	
	/** The lst position by role sell. */
	private List<PositionRoleTO> lstPositionByRoleSell;

	/**
	 * Gets the deposited amount.
	 *
	 * @return the deposited amount
	 */
	public BigDecimal getDepositedAmount() {
		return depositedAmount;
	}
	
	/**
	 * Sets the deposited amount.
	 *
	 * @param depositedAmount the new deposited amount
	 */
	public void setDepositedAmount(BigDecimal depositedAmount) {
		this.depositedAmount = depositedAmount;
	}
	
	/**
	 * Gets the paid amount.
	 *
	 * @return the paid amount
	 */
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	
	/**
	 * Sets the paid amount.
	 *
	 * @param paidAmount the new paid amount
	 */
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	
	/**
	 * Gets the net position.
	 *
	 * @return the net position
	 */
	public BigDecimal getNetPosition() {
		return netPosition;
	}
	
	/**
	 * Sets the net position.
	 *
	 * @param netPosition the new net position
	 */
	public void setNetPosition(BigDecimal netPosition) {
		this.netPosition = netPosition;
	}
	
	/**
	 * Gets the lst position by role buy.
	 *
	 * @return the lst position by role buy
	 */
	public List<PositionRoleTO> getLstPositionByRoleBuy() {
		return lstPositionByRoleBuy;
	}
	
	/**
	 * Sets the lst position by role buy.
	 *
	 * @param lstPositionByRoleBuy the new lst position by role buy
	 */
	public void setLstPositionByRoleBuy(List<PositionRoleTO> lstPositionByRoleBuy) {
		this.lstPositionByRoleBuy = lstPositionByRoleBuy;
	}
	
	/**
	 * Gets the lst position by role sell.
	 *
	 * @return the lst position by role sell
	 */
	public List<PositionRoleTO> getLstPositionByRoleSell() {
		return lstPositionByRoleSell;
	}
	
	/**
	 * Sets the lst position by role sell.
	 *
	 * @param lstPositionByRoleSell the new lst position by role sell
	 */
	public void setLstPositionByRoleSell(List<PositionRoleTO> lstPositionByRoleSell) {
		this.lstPositionByRoleSell = lstPositionByRoleSell;
	}
	
	/**
	 * Gets the position participant to.
	 *
	 * @return the position participant to
	 */
	public PositionParticipantTO getPositionParticipantTO() {
		return positionParticipantTO;
	}
	
	/**
	 * Sets the position participant to.
	 *
	 * @param positionParticipantTO the new position participant to
	 */
	public void setPositionParticipantTO(PositionParticipantTO positionParticipantTO) {
		this.positionParticipantTO = positionParticipantTO;
	}
}