package com.pradera.negotiations.positioncommission.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PositionRoleTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class PositionRoleTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The modality. */
	private String modality;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The security. */
	private String security;
	
	/** The operation quantity. */
	private BigDecimal operationQuantity;
	
	/** The operation price. */
	private BigDecimal operationPrice;
	
	/** The effective amount. */
	private BigDecimal effectiveAmount;
	
	/** The id participant buyer. */
	private Long idParticipantBuyer;
	
	/** The participant buyer. */
	private String participantBuyer;
	
	/** The participant buyer mnemonic. */
	private String participantBuyerMnemonic;
	
	/** The id participant seller. */
	private Long idParticipantSeller;
	
	/** The participant seller. */
	private String participantSeller;
	
	/** The participant seller mnemonic. */
	private String participantSellerMnemonic;
	
	/** The state. */
	private String state;
	
	/** The id mechanism operation pk. */
	private Long idMechanismOperationPk;
	
	/** The id settlement operation pk. */
	private Long idSettlementOperationPk;
	
	/** The id incharge stock participant pk. */
	private Long idInchargeStockParticipantPk;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/**
	 * Gets the modality.
	 *
	 * @return the modality
	 */
	public String getModality() {
		return modality;
	}
	
	/**
	 * Sets the modality.
	 *
	 * @param modality the new modality
	 */
	public void setModality(String modality) {
		this.modality = modality;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the participant buyer.
	 *
	 * @return the participant buyer
	 */
	public String getParticipantBuyer() {
		return participantBuyer;
	}
	
	/**
	 * Sets the participant buyer.
	 *
	 * @param participantBuyer the new participant buyer
	 */
	public void setParticipantBuyer(String participantBuyer) {
		this.participantBuyer = participantBuyer;
	}
	
	/**
	 * Gets the participant seller.
	 *
	 * @return the participant seller
	 */
	public String getParticipantSeller() {
		return participantSeller;
	}
	
	/**
	 * Sets the participant seller.
	 *
	 * @param participantSeller the new participant seller
	 */
	public void setParticipantSeller(String participantSeller) {
		this.participantSeller = participantSeller;
	}
	
	/**
	 * Gets the effective amount.
	 *
	 * @return the effective amount
	 */
	public BigDecimal getEffectiveAmount() {
		return effectiveAmount;
	}
	
	/**
	 * Sets the effective amount.
	 *
	 * @param effectiveAmount the new effective amount
	 */
	public void setEffectiveAmount(BigDecimal effectiveAmount) {
		this.effectiveAmount = effectiveAmount;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public String getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(String security) {
		this.security = security;
	}
	
	/**
	 * Gets the operation quantity.
	 *
	 * @return the operation quantity
	 */
	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}
	
	/**
	 * Sets the operation quantity.
	 *
	 * @param operationQuantity the new operation quantity
	 */
	public void setOperationQuantity(BigDecimal operationQuantity) {
		this.operationQuantity = operationQuantity;
	}
	
	/**
	 * Gets the operation price.
	 *
	 * @return the operation price
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}
	
	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the new operation price
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}
	
	/**
	 * Gets the id participant buyer.
	 *
	 * @return the id participant buyer
	 */
	public Long getIdParticipantBuyer() {
		return idParticipantBuyer;
	}
	
	/**
	 * Sets the id participant buyer.
	 *
	 * @param idParticipantBuyer the new id participant buyer
	 */
	public void setIdParticipantBuyer(Long idParticipantBuyer) {
		this.idParticipantBuyer = idParticipantBuyer;
	}
	
	/**
	 * Gets the participant buyer mnemonic.
	 *
	 * @return the participant buyer mnemonic
	 */
	public String getParticipantBuyerMnemonic() {
		return participantBuyerMnemonic;
	}
	
	/**
	 * Sets the participant buyer mnemonic.
	 *
	 * @param participantBuyerMnemonic the new participant buyer mnemonic
	 */
	public void setParticipantBuyerMnemonic(String participantBuyerMnemonic) {
		this.participantBuyerMnemonic = participantBuyerMnemonic;
	}
	
	/**
	 * Gets the id participant seller.
	 *
	 * @return the id participant seller
	 */
	public Long getIdParticipantSeller() {
		return idParticipantSeller;
	}
	
	/**
	 * Sets the id participant seller.
	 *
	 * @param idParticipantSeller the new id participant seller
	 */
	public void setIdParticipantSeller(Long idParticipantSeller) {
		this.idParticipantSeller = idParticipantSeller;
	}
	
	/**
	 * Gets the participant seller mnemonic.
	 *
	 * @return the participant seller mnemonic
	 */
	public String getParticipantSellerMnemonic() {
		return participantSellerMnemonic;
	}
	
	/**
	 * Sets the participant seller mnemonic.
	 *
	 * @param participantSellerMnemonic the new participant seller mnemonic
	 */
	public void setParticipantSellerMnemonic(String participantSellerMnemonic) {
		this.participantSellerMnemonic = participantSellerMnemonic;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the id mechanism operation pk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	
	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the new id mechanism operation pk
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}
	
	/**
	 * Gets the id incharge stock participant pk.
	 *
	 * @return the id incharge stock participant pk
	 */
	public Long getIdInchargeStockParticipantPk() {
		return idInchargeStockParticipantPk;
	}
	
	/**
	 * Sets the id incharge stock participant pk.
	 *
	 * @param idInchargeStockParticipantPk the new id incharge stock participant pk
	 */
	public void setIdInchargeStockParticipantPk(Long idInchargeStockParticipantPk) {
		this.idInchargeStockParticipantPk = idInchargeStockParticipantPk;
	}
	
	/**
	 * Gets the id settlement operation pk.
	 *
	 * @return the id settlement operation pk
	 */
	public Long getIdSettlementOperationPk() {
		return idSettlementOperationPk;
	}
	
	/**
	 * Sets the id settlement operation pk.
	 *
	 * @param idSettlementOperationPk the new id settlement operation pk
	 */
	public void setIdSettlementOperationPk(Long idSettlementOperationPk) {
		this.idSettlementOperationPk = idSettlementOperationPk;
	}
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
}