package com.pradera.negotiations.positioncommission.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PositionParticipantTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class PositionParticipantTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The participant. */
	private String participant;	
	
	/** The id modality group pk. */
	private Long idModalityGroupPk;
	
	/** The modality group. */
	private String modalityGroup;
	
	/** The settlement schema. */
	private Integer settlementSchema;
	
	/** The settlement schema desc. */
	private String settlementSchemaDesc;
	
	/** The currency. */
	private String currency;
	
	/** The buy position. */
	private BigDecimal buyPosition;
	
	/** The sell position. */
	private BigDecimal sellPosition;	
	
	/** The lst position by modality group. */
	private List<PositionModalityGroupTO> lstPositionByModalityGroup;
	
	/** The net position. */
	private BigDecimal netPosition;
	
	/** The is net position negative. */
	private Boolean isNetPositionNegative;
	
	/**
	 * Instantiates a new position participant to.
	 */
	public PositionParticipantTO() {
		buyPosition = BigDecimal.ZERO;
		sellPosition = BigDecimal.ZERO;
		netPosition = BigDecimal.ZERO;
	}
	
	/**
	 * Gets the checks if is net position negative.
	 *
	 * @return the checks if is net position negative
	 */
	public Boolean getIsNetPositionNegative() {
		return isNetPositionNegative;
	}
	
	/**
	 * Sets the checks if is net position negative.
	 *
	 * @param isNetPositionNegative the new checks if is net position negative
	 */
	public void setIsNetPositionNegative(Boolean isNetPositionNegative) {
		this.isNetPositionNegative = isNetPositionNegative;
	}
	
	/**
	 * Gets the net position.
	 *
	 * @return the net position
	 */
	public BigDecimal getNetPosition() {
		return netPosition;
	}
	
	/**
	 * Sets the net position.
	 *
	 * @param netPosition the new net position
	 */
	public void setNetPosition(BigDecimal netPosition) {
		this.netPosition = netPosition;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public String getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the modality group.
	 *
	 * @return the modality group
	 */
	public String getModalityGroup() {
		return modalityGroup;
	}
	
	/**
	 * Sets the modality group.
	 *
	 * @param modalityGroup the new modality group
	 */
	public void setModalityGroup(String modalityGroup) {
		this.modalityGroup = modalityGroup;
	}
	
	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public Integer getSettlementSchema() {
		return settlementSchema;
	}
	
	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the new settlement schema
	 */
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the buy position.
	 *
	 * @return the buy position
	 */
	public BigDecimal getBuyPosition() {
		return buyPosition;
	}
	
	/**
	 * Sets the buy position.
	 *
	 * @param buyPosition the new buy position
	 */
	public void setBuyPosition(BigDecimal buyPosition) {
		this.buyPosition = buyPosition;
	}
	
	/**
	 * Gets the sell position.
	 *
	 * @return the sell position
	 */
	public BigDecimal getSellPosition() {
		return sellPosition;
	}
	
	/**
	 * Sets the sell position.
	 *
	 * @param sellPosition the new sell position
	 */
	public void setSellPosition(BigDecimal sellPosition) {
		this.sellPosition = sellPosition;
	}
	
	/**
	 * Gets the id modality group pk.
	 *
	 * @return the id modality group pk
	 */
	public Long getIdModalityGroupPk() {
		return idModalityGroupPk;
	}
	
	/**
	 * Sets the id modality group pk.
	 *
	 * @param idModalityGroupPk the new id modality group pk
	 */
	public void setIdModalityGroupPk(Long idModalityGroupPk) {
		this.idModalityGroupPk = idModalityGroupPk;
	}
	
	/**
	 * Gets the lst position by modality group.
	 *
	 * @return the lst position by modality group
	 */
	public List<PositionModalityGroupTO> getLstPositionByModalityGroup() {
		return lstPositionByModalityGroup;
	}
	
	/**
	 * Sets the lst position by modality group.
	 *
	 * @param lstPositionByModalityGroup the new lst position by modality group
	 */
	public void setLstPositionByModalityGroup(
			List<PositionModalityGroupTO> lstPositionByModalityGroup) {
		this.lstPositionByModalityGroup = lstPositionByModalityGroup;
	}
	
	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}
	
	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the settlement schema desc.
	 *
	 * @return the settlement schema desc
	 */
	public String getSettlementSchemaDesc() {
		return settlementSchemaDesc;
	}
	
	/**
	 * Sets the settlement schema desc.
	 *
	 * @param settlementSchemaDesc the new settlement schema desc
	 */
	public void setSettlementSchemaDesc(String settlementSchemaDesc) {
		this.settlementSchemaDesc = settlementSchemaDesc;
	}
}