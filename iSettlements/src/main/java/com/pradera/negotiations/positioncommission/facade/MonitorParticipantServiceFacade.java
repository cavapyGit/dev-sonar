package com.pradera.negotiations.positioncommission.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.negotiations.mcnoperations.service.McnOperationServiceBean;
import com.pradera.negotiations.positioncommission.to.PositionModalityGroupTO;
import com.pradera.negotiations.positioncommission.to.PositionParticipantTO;
import com.pradera.negotiations.positioncommission.to.PositionRoleTO;
import com.pradera.negotiations.positioncommission.to.SearchPositionCommissionTO;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitorParticipantServiceFacade.
 *
 * @author PraderaTechnologies
 * The Class MonitorParticipantServiceFacade
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class MonitorParticipantServiceFacade {

	/** The mcn operations service bean. */
	@EJB
	McnOperationServiceBean mcnOperationsServiceBean;
	
	/** The parameter service bean. */
	@Inject
	ParameterServiceBean parameterServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;

	
	/**
	 * Search position by participant.
	 *
	 * @param searchPositionCommissionTO the search position commission to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PARTICIPANT_POSITION_MONITORING)
	public List<PositionParticipantTO> searchPositionByParticipant(SearchPositionCommissionTO searchPositionCommissionTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		List<Object[]> lstResults = mcnOperationsServiceBean.searchPositionByParticipant(searchPositionCommissionTO);
		Map<Long, PositionParticipantTO> mapParticipants = new LinkedHashMap<Long, PositionParticipantTO>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstResults)){
			for(Object[] objResult:lstResults){
				//CREATE THE FIRST FIELD
				Long idParticipant = (Long)objResult[0];
				PositionParticipantTO posByParti = mapParticipants.get(idParticipant);
				if(posByParti == null){
					posByParti = new PositionParticipantTO();
					posByParti.setIdParticipantPk(idParticipant);
					posByParti.setParticipant((String)objResult[1]);
					posByParti.setParticipantMnemonic((String)objResult[2]);
					posByParti.setIdModalityGroupPk((Long)objResult[3]);
					posByParti.setModalityGroup((String)objResult[4]);
					posByParti.setSettlementSchema((Integer)objResult[5]);
					posByParti.setCurrency((String)objResult[11]);
					if(SettlementSchemaType.GROSS.getCode().equals(posByParti.getSettlementSchema())){
						posByParti.setSettlementSchemaDesc(SettlementSchemaType.GROSS.getValue());
					}else{
						posByParti.setSettlementSchemaDesc(SettlementSchemaType.NET.getValue());
					}
					
					//CREATE THE SECOND FIELD
					List<PositionModalityGroupTO> lstPositionByModalityGroup = new ArrayList<PositionModalityGroupTO>();					
					PositionModalityGroupTO posByModaGroup = new PositionModalityGroupTO();
					posByModaGroup.setPositionParticipantTO(posByParti);
					posByModaGroup.setDepositedAmount(mcnOperationsServiceBean.getCashAmountMovementByRole(posByParti, searchPositionCommissionTO, OperationClassType.RECEPTION_FUND.getCode()));
					posByModaGroup.setPaidAmount(mcnOperationsServiceBean.getCashAmountMovementByRole(posByParti, searchPositionCommissionTO, OperationClassType.SEND_FUND.getCode()));
					posByModaGroup.setNetPosition(posByModaGroup.getDepositedAmount().subtract(posByModaGroup.getPaidAmount()));
					if(posByModaGroup.getNetPosition().compareTo(BigDecimal.ZERO) < 0){
						posByModaGroup.setNetPosition(BigDecimal.ZERO);
					}
					
					//CREATE THE LAST FIELD BUY
					List<PositionRoleTO> lstPositionByRole = new ArrayList<PositionRoleTO>();
					List<Object[]> lstResultsBuy = mcnOperationsServiceBean.searchPositionByRole(
							posByParti.getIdParticipantPk(), posByParti.getIdModalityGroupPk(), searchPositionCommissionTO, ComponentConstant.PURCHARSE_ROLE);
					if(Validations.validateListIsNotNullAndNotEmpty(lstResultsBuy)){
						for(Object[] objResultBuy:lstResultsBuy){
							PositionRoleTO posByRole = new PositionRoleTO();
							posByRole.setIdMechanismOperationPk((Long)objResultBuy[16]);
							posByRole.setModality((String)objResultBuy[0]);
							posByRole.setOperationNumber((Long)objResultBuy[1]);
							posByRole.setOperationDate((Date)objResultBuy[2]);
							posByRole.setOperationQuantity((BigDecimal)objResultBuy[3]);
							posByRole.setOperationPrice((BigDecimal)objResultBuy[6]);
							posByRole.setEffectiveAmount((BigDecimal)objResultBuy[4]);
							posByRole.setSecurity((String)objResultBuy[5]);
							posByRole.setIdParticipantBuyer((Long)objResultBuy[7]);
							posByRole.setParticipantBuyer((String)objResultBuy[8]);
							posByRole.setParticipantBuyerMnemonic((String)objResultBuy[9]);
							posByRole.setIdParticipantSeller((Long)objResultBuy[10]);
							posByRole.setParticipantSeller((String)objResultBuy[11]);
							posByRole.setParticipantSellerMnemonic((String)objResultBuy[12]);
							posByRole.setState((String)objResultBuy[15]);
							posByRole.setBallotNumber((Long)objResultBuy[18]);
							posByRole.setSequential((Long)objResultBuy[19]);
							posByRole.setIdInchargeStockParticipantPk(idParticipant);
							lstPositionByRole.add(posByRole);
						}
						posByModaGroup.setLstPositionByRoleBuy(lstPositionByRole);
					}
					
					//CREATE THE LAST FIELD BUY
					lstPositionByRole = new ArrayList<PositionRoleTO>();
					List<Object[]> lstResultsSell = mcnOperationsServiceBean.searchPositionByRole(
							posByParti.getIdParticipantPk(), posByParti.getIdModalityGroupPk(), searchPositionCommissionTO, ComponentConstant.SALE_ROLE);
					if(Validations.validateListIsNotNullAndNotEmpty(lstResultsSell)){
						for(Object[] objResultSell:lstResultsSell){
							PositionRoleTO posByRole = new PositionRoleTO();
							posByRole.setIdMechanismOperationPk((Long)objResultSell[16]);
							posByRole.setModality((String)objResultSell[0]);
							posByRole.setOperationNumber((Long)objResultSell[1]);
							posByRole.setOperationDate((Date)objResultSell[2]);
							posByRole.setOperationQuantity((BigDecimal)objResultSell[3]);
							posByRole.setOperationPrice((BigDecimal)objResultSell[6]);
							posByRole.setEffectiveAmount((BigDecimal)objResultSell[4]);
							posByRole.setSecurity((String)objResultSell[5]);
							posByRole.setIdParticipantBuyer((Long)objResultSell[7]);
							posByRole.setParticipantBuyer((String)objResultSell[8]);
							posByRole.setParticipantBuyerMnemonic((String)objResultSell[9]);
							posByRole.setIdParticipantSeller((Long)objResultSell[10]);
							posByRole.setParticipantSeller((String)objResultSell[11]);
							posByRole.setParticipantSellerMnemonic((String)objResultSell[12]);
							posByRole.setState((String)objResultSell[15]);
							posByRole.setBallotNumber((Long)objResultSell[18]);
							posByRole.setSequential((Long)objResultSell[19]);
							posByRole.setIdInchargeStockParticipantPk(idParticipant);
							lstPositionByRole.add(posByRole);
						}
						posByModaGroup.setLstPositionByRoleSell(lstPositionByRole);
					}
					lstPositionByModalityGroup.add(posByModaGroup);
					
					posByParti.setLstPositionByModalityGroup(lstPositionByModalityGroup);
					
					mapParticipants.put(idParticipant, posByParti);
				}
				
				BigDecimal settlementAmount = (BigDecimal)objResult[9];
				Integer role = (Integer)objResult[10];
				if(ComponentConstant.SALE_ROLE.equals(role)){
					posByParti.setSellPosition(posByParti.getSellPosition().add(settlementAmount));
				}else if(ComponentConstant.PURCHARSE_ROLE.equals(role)){
					posByParti.setBuyPosition(posByParti.getBuyPosition().add(settlementAmount));
				}
				
				if(posByParti.getSettlementSchema().equals(SettlementSchemaType.NET.getCode())){
					posByParti.setNetPosition(posByParti.getSellPosition().subtract(posByParti.getBuyPosition()));
					posByParti.setIsNetPositionNegative(posByParti.getNetPosition().signum()==-1?Boolean.TRUE:Boolean.FALSE);
 				}else{
					posByParti.setNetPosition(BigDecimal.ZERO);
				}
				 
			}			
		}
		return new ArrayList<PositionParticipantTO>(mapParticipants.values()) ;
	}
	
	/**
	 * Gets the list modality group.
	 *
	 * @param idNegotiationMechanimsPk the id negotiation mechanims pk
	 * @return the list modality group
	 * @throws ServiceException the service exception
	 */
	public List<ModalityGroup> getListModalityGroup(Long idNegotiationMechanimsPk) throws ServiceException{
		return mcnOperationsServiceBean.getListModalityGroup(idNegotiationMechanimsPk);
	}
	
	/**
	 * Start net settlement process.
	 *
	 * @param idCurrency the id currency
	 * @param idModalityGroup the id modality group
	 * @param idMechanism the id mechanism
	 * @param settlementDate the settlement date
	 * @param netSettlementAttributesTO the net settlement attributes to
	 * @throws ServiceException the service exception
	 */
	public void searchParticipantPosition(Integer idCurrency, Long idModalityGroup, Long idMechanism, 
			Date settlementDate, NetSettlementAttributesTO netSettlementAttributesTO) throws ServiceException{		        
		mcnOperationsServiceBean.calculateNetParticipantPositions(idMechanism, settlementDate, idModalityGroup, idCurrency, false, netSettlementAttributesTO);
	}
}
