package com.pradera.negotiations.positioncommission.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.settlement.OperationSettlement;
import com.pradera.model.settlement.ParticipantPosition;
import com.pradera.model.settlement.SettlementProcess;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.OperationSettlementSateType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.operations.view.OperationBusinessBean;
import com.pradera.negotiations.positioncommission.facade.MonitorParticipantServiceFacade;
import com.pradera.negotiations.positioncommission.to.PositionModalityGroupTO;
import com.pradera.negotiations.positioncommission.to.PositionParticipantTO;
import com.pradera.negotiations.positioncommission.to.SearchPositionCommissionTO;
import com.pradera.settlements.core.facade.SettlementMonitoringFacade;
import com.pradera.settlements.core.to.NetParticipantPositionTO;
import com.pradera.settlements.core.to.NetSettlementAttributesTO;
import com.pradera.settlements.core.to.NetSettlementOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class SimulationParticipantMgmtBean.
 *
 * @author : PraderaTechnologies.
 */
@DepositaryWebBean
@LoggerCreateBean
public class SimulationParticipantMgmtBean extends GenericBaseBean {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search position commission to. */
	private SearchPositionCommissionTO searchPositionCommissionTO;
	
	/** The bl participant. */
	private boolean blParticipant;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The general parameters facade. */
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;		
	
	/** The monitor participant service facade. */
	@EJB
	private MonitorParticipantServiceFacade monitorParticipantServiceFacade;
	
	/** The manage holder account assignment facade. */
	@Inject
	private AccountAssignmentFacade manageHolderAccountAssignmentFacade;
	
	/** The operation business comp. */
	@Inject
	private OperationBusinessBean operationBusinessComp;
	
	/** The id participant bc. */
	@Inject @Configurable String idParticipantBC;
	
	/** The net settlement attributes to. */
	private NetSettlementAttributesTO netSettlementAttributesTO;
	
	/** The monitor process facade. */
	@EJB
	private SettlementMonitoringFacade monitorProcessFacade;
	
	/** The lst id chained holder operation. */
	private List<Long> lstIdChainedHolderOperation;
	
	/** The net settlement operation to. */
	private List<NetSettlementOperationTO> netSettlementOperationTO = new ArrayList<NetSettlementOperationTO>();
	
	/** The settlement process net session. */
	private SettlementProcess settlementProcessNetSession;
	
	/** The participant selected. */
	private String participantSelected;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
		searchPositionCommissionTO = new SearchPositionCommissionTO();
		searchPositionCommissionTO.setSettlementDate(getCurrentSystemDate());
		netSettlementAttributesTO = new NetSettlementAttributesTO();
		Map<Long, NetParticipantPositionTO> mapParticipantPositions =  new HashMap<Long, NetParticipantPositionTO>();
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		settlementProcessNetSession = new SettlementProcess();
		try{
			fillCombos();
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				List<Participant> lstTemp = new ArrayList<Participant>();
				if(!userInfo.getUserAccountSession().getParticipantCode().equals(new Long(idParticipantBC))){
					Participant participant = manageHolderAccountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
					lstTemp.add(participant);
					searchPositionCommissionTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
					searchPositionCommissionTO.setLstParticipants(lstTemp);
					searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));
					blParticipant = true;
				}else{
					searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegotiationMechanism());
				}
			}else{
				searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegotiationMechanism());
			}
			searchPositionCommissionTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
			changeNegotiationMechanism();
			searchPositionCommissionTO.setSettlementSchemaSelected(SettlementSchemaType.NET.getCode());
			searchPositionCommissionTO.setCurrencySelected(CurrencyType.PYG.getCode());
		}catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change negotiation mechanism.
	 */
	public void changeNegotiationMechanism(){
		try {
			cleanResults();
			searchPositionCommissionTO.setLstModalityGroup(null);
			searchPositionCommissionTO.setModalityGroupSelected(null);
			if(!blParticipant){
				searchPositionCommissionTO.setLstParticipants(null);
				searchPositionCommissionTO.setParticipantSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchPositionCommissionTO.getNegoMechanismSelected())){				
				if(!blParticipant)
					searchPositionCommissionTO.setLstParticipants(manageHolderAccountAssignmentFacade.getListParticipantsForNegoMechanism(searchPositionCommissionTO.getNegoMechanismSelected(), null));
				searchPositionCommissionTO.setLstModalityGroup(monitorParticipantServiceFacade.getListModalityGroup(searchPositionCommissionTO.getNegoMechanismSelected()));
				List<ModalityGroup> lstModalityGroup = new ArrayList<ModalityGroup>();
				for(ModalityGroup objModalityGroup : searchPositionCommissionTO.getLstModalityGroup()){
					if(!objModalityGroup.getIdModalityGroupPk().equals(GeneralConstants.MODALITY_GROUP_FOP)){
						lstModalityGroup.add(objModalityGroup);
					}
				}
				searchPositionCommissionTO.setLstModalityGroup(lstModalityGroup);
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		searchPositionCommissionTO.setLstPositionByModalityGroup(null);
		searchPositionCommissionTO.setLstPositionByParticipant(null);
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResult(false);
		searchPositionCommissionTO.setBlNoResultByRole(false);
	}
	
	/**
	 * Load mechanism operation.
	 *
	 * @param mechanismOperationId the mechanism operation id
	 */
	public void loadMechanismOperation(Long mechanismOperationId){
		idMechanismOperation = mechanismOperationId;
		operationBusinessComp.setIdMechanismOperationPk(idMechanismOperation);
		operationBusinessComp.setShowAssignments(false);
		operationBusinessComp.searchMechanismOperation();
	}
		
//	public String showOperation(PositionRoleTO posiByRole){
//		try {
//			ParameterTable paramTab = new ParameterTable();
//			lstSettlementType = new ArrayList<ParameterTable>();
//			paramTab = new ParameterTable();
//			paramTab.setParameterTablePk(SettlementType.DVP.getCode());
//			paramTab.setParameterName(SettlementType.DVP.getValue());
//			lstSettlementType.add(paramTab);
//			paramTab = new ParameterTable();
//			paramTab.setParameterTablePk(SettlementType.FOP.getCode());
//			paramTab.setParameterName(SettlementType.FOP.getValue());
//			lstSettlementType.add(paramTab);
//			paramTab = new ParameterTable();
//			paramTab.setParameterTablePk(SettlementType.ECE.getCode());
//			paramTab.setParameterName(SettlementType.ECE.getValue());
//			lstSettlementType.add(paramTab);
//			mechanismOperation = monitorParticipantServiceFacade.getMechanismOperation(posiByRole);
//			mechanismOperation = monitorParticipantServiceFacade.loadMechanismOperation(mechanismOperation, posiByRole.getIdInchargeStockParticipantPk(), blParticipant);
//			if(Validations.validateListIsNotNullAndNotEmpty(mechanismOperation.getSwapOperations()))
//				swapOperation = mechanismOperation.getSwapOperations().get(0);//SOLO 1 SIEMPRE
//			return VIEW_MAPPING;
//		} catch (ServiceException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	/**
 * Clean search.
 */
public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		searchPositionCommissionTO.setCurrencySelected(null);
		searchPositionCommissionTO.setLstModalityGroup(null);
		searchPositionCommissionTO.setModalityGroupSelected(null);
		searchPositionCommissionTO.setNegoMechanismSelected(null);
		searchPositionCommissionTO.setSettlementSchemaSelected(null);		
		if(!blParticipant){
			searchPositionCommissionTO.setParticipantSelected(null);
			searchPositionCommissionTO.setLstParticipants(null);
		}
		searchPositionCommissionTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
		changeNegotiationMechanism();
		searchPositionCommissionTO.setSettlementSchemaSelected(SettlementSchemaType.NET.getCode());
		searchPositionCommissionTO.setCurrencySelected(CurrencyType.PYG.getCode());
		
		netSettlementAttributesTO = new NetSettlementAttributesTO();
		Map<Long, NetParticipantPositionTO> mapParticipantPositions =  new HashMap<Long, NetParticipantPositionTO>();
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		settlementProcessNetSession = new SettlementProcess();
	}
	
	/**
	 *  Gets  Positions by Participant list.
	 */
	@LoggerAuditWeb
	public void searchPositionsParticipant(){
		cleanResults();
		try {
			searchPositionCommissionTO.setTotalPurchase(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalSale(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalDebt(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalCreditor(BigDecimal.ZERO);
			searchPositionCommissionTO.setBlNoResult(true);
			
			Integer idCurrency =  searchPositionCommissionTO.getCurrencySelected();
			Long idModalityGroup = searchPositionCommissionTO.getModalityGroupSelected();
			Long idMechanism = searchPositionCommissionTO.getNegoMechanismSelected();
			Date settlementDate = searchPositionCommissionTO.getSettlementDate();
			
			netSettlementAttributesTO = new NetSettlementAttributesTO();
			netSettlementAttributesTO.setIdParticipantPk(searchPositionCommissionTO.getParticipantSelected());
			
			monitorParticipantServiceFacade.searchParticipantPosition(idCurrency, idModalityGroup, idMechanism, settlementDate, netSettlementAttributesTO);
			
			loadParticipantByModalities();
			
			if(Validations.validateListIsNotNullAndNotEmpty(netSettlementAttributesTO.getLstParticipantPosition())) {
				searchPositionCommissionTO.setBlNoResult(false);
			}
			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		
		}		
	}	
	
	/**
	 * Load participant by modalities.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipantByModalities() throws ServiceException{
		
		Map<String,NetParticipantPositionTO> mapParticipantModality = new HashMap<String, NetParticipantPositionTO>();
		
		for(NetSettlementOperationTO settlementOperationTO : netSettlementAttributesTO.getLstGuaranteeRemovedOperations()){
			String key = new StringBuffer().append(settlementOperationTO.getIdModality()).append(settlementOperationTO.getIdSaleParticipant()).toString();
			NetParticipantPositionTO netParticipantPositionTO = mapParticipantModality.get(key); 
			if(netParticipantPositionTO == null){
				netParticipantPositionTO = new NetParticipantPositionTO();
				mapParticipantModality.put(key, netParticipantPositionTO);
			}
			netParticipantPositionTO.setIdParticipant(settlementOperationTO.getIdSaleParticipant());
			netParticipantPositionTO.setParticipantDescription(settlementOperationTO.getSaleParticipantDescription());
			netParticipantPositionTO.setParticipantNemonic(settlementOperationTO.getSaleParticipantNemonic());
			netParticipantPositionTO.setMechanismName(settlementOperationTO.getMechanismName());
			netParticipantPositionTO.setModalityName(settlementOperationTO.getModalityName());
			netParticipantPositionTO.getLstSettlementOperation().add(settlementOperationTO);
		}
		netSettlementAttributesTO.setLstGuaranteeParticipantPosition(new ArrayList<NetParticipantPositionTO>(mapParticipantModality.values()));
		
		mapParticipantModality = new HashMap<String, NetParticipantPositionTO>();
		
		for(NetSettlementOperationTO settlementOperationTO : netSettlementAttributesTO.getLstStockRemovedOperations()){
			String key = new StringBuffer().append(settlementOperationTO.getIdModality()).append(settlementOperationTO.getIdSaleParticipant()).toString();
			NetParticipantPositionTO netParticipantPositionTO = mapParticipantModality.get(key); 
			if(netParticipantPositionTO == null){
				netParticipantPositionTO = new NetParticipantPositionTO();
				mapParticipantModality.put(key, netParticipantPositionTO);
			}
			netParticipantPositionTO.setIdParticipant(settlementOperationTO.getIdSaleParticipant());
			netParticipantPositionTO.setParticipantDescription(settlementOperationTO.getSaleParticipantDescription());
			netParticipantPositionTO.setParticipantNemonic(settlementOperationTO.getSaleParticipantNemonic());
			netParticipantPositionTO.setMechanismName(settlementOperationTO.getMechanismName());
			netParticipantPositionTO.setModalityName(settlementOperationTO.getModalityName());
			netParticipantPositionTO.getLstSettlementOperation().add(settlementOperationTO);
		}
		netSettlementAttributesTO.setLstStockParticipantPosition(new ArrayList<NetParticipantPositionTO>(mapParticipantModality.values()));
		
	}
	
	/**
	 * Begin edit net position.
	 *
	 * @param participantPosition the participant position
	 */
	public void beginEditNetPosition(NetParticipantPositionTO participantPosition) {
		
		netSettlementAttributesTO.setParticipantPositionToEdit(participantPosition);
		
		for(NetSettlementOperationTO settlementOperationTO : participantPosition.getLstSettlementOperation()){
			
			BigDecimal saleAmountOnOperation = NegotiationUtils.getAmountOnOperation(settlementOperationTO,participantPosition.getIdParticipant(),ComponentConstant.SALE_ROLE);
			BigDecimal purchaseAmountOnOperation = NegotiationUtils.getAmountOnOperation(settlementOperationTO,participantPosition.getIdParticipant(),ComponentConstant.PURCHARSE_ROLE);
			
			settlementOperationTO.setParticipantSaleAmount(saleAmountOnOperation);
			settlementOperationTO.setParticipantPurchaseAmount(purchaseAmountOnOperation);
			
			if(settlementOperationTO.getParticipantSaleAmount().compareTo(settlementOperationTO.getParticipantPurchaseAmount()) > 0){
				settlementOperationTO.setDisabled(Boolean.TRUE);
			}else{
				settlementOperationTO.setDisabled(Boolean.FALSE);
			}
			
			if(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode().equals(settlementOperationTO.getOperationSettlement().getOperationState())){
				settlementOperationTO.setSelected(Boolean.TRUE);
			}else{
				settlementOperationTO.setSelected(Boolean.FALSE);
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(participantPosition.getDisplayCodeDescription())){
			participantSelected = participantPosition.getDisplayCodeDescription();
		}
	}
	
	/**
	 * Verify chain on multiple operations.
	 *
	 * @param objNetSettlementOperationTO the obj net settlement operation to
	 */
	public void verifyChainOnMultipleOperations(NetSettlementOperationTO objNetSettlementOperationTO){
		if(Validations.validateIsNull(objNetSettlementOperationTO)){
			for (NetSettlementOperationTO objSettlementOperationTO : netSettlementAttributesTO.getParticipantPositionToEdit().getLstSettlementOperation()) {
				
				if(netSettlementAttributesTO.getParticipantPositionToEdit().isCheckAll()){
					if(!objSettlementOperationTO.isSelected() && !objSettlementOperationTO.isDisabled()){
						objSettlementOperationTO.setSelected(true);
						verifyChainOperation(objSettlementOperationTO,true);
						if(lstIdChainedHolderOperation!=null){
							List<String> ballotSequentials = new ArrayList<String>();
							for(NetSettlementOperationTO selectedSettlementOperationTO : netSettlementOperationTO){
								ballotSequentials.add(selectedSettlementOperationTO.getBallotSequential());
							}
							
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
									PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_CHAIN_OPERATION_CONFIRM, 
											new Object[]{CommonsUtilities.concatWithBreakLine(ballotSequentials.toArray(), 5)} ) );
							JSFUtilities.executeJavascriptFunction("PF('idVerifyChainOperationW').show()");
						}
					}
				} else {
					if(objSettlementOperationTO.isSelected() && !objSettlementOperationTO.isDisabled()){
						objSettlementOperationTO.setSelected(false);
						verifyChainOperation(objSettlementOperationTO,true);
					}
				}
			}
		} else {
			verifyChainOperation(objNetSettlementOperationTO,false);
		}
	}
	
	/**
	 * Verify chain operation.
	 *
	 * @param objNetSettlementOperationTO the obj net settlement operation to
	 * @param checkAll the check all
	 */
	public void verifyChainOperation(NetSettlementOperationTO objNetSettlementOperationTO, boolean checkAll) 
	{
		JSFUtilities.hideGeneralDialogues();
		try {
			if (objNetSettlementOperationTO.isSelected()) {
				if (StringUtils.equalsIgnoreCase(BooleanType.YES.getValue(), objNetSettlementOperationTO.getIndChainedHolderOperation())) {
					netSettlementOperationTO.add(objNetSettlementOperationTO);
					if(!checkAll){
						//this operation belongs to chain, the we get the chains to be removed
						lstIdChainedHolderOperation= monitorProcessFacade.getListChainSettlementOperation(objNetSettlementOperationTO.getIdSettlementOperation(),null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_CHAIN_OPERATION_CONFIRM, new Object[]{objNetSettlementOperationTO.getBallotSequential()}));
						JSFUtilities.executeJavascriptFunction("PF('idVerifyChainOperationW').show()");
					}else{
						List<Long> tmpLstIdChainedHolderOperation= monitorProcessFacade.getListChainSettlementOperation(objNetSettlementOperationTO.getIdSettlementOperation(),null);
						if(lstIdChainedHolderOperation!=null){
							lstIdChainedHolderOperation.addAll(tmpLstIdChainedHolderOperation);
						}else{
							lstIdChainedHolderOperation = tmpLstIdChainedHolderOperation;
						}
					}
				}
			} else {
				if (StringUtils.equalsIgnoreCase(BooleanType.YES.getValue(), objNetSettlementOperationTO.getIndChainedHolderOperation())) {
					//we get the chains inside the operation
					lstIdChainedHolderOperation= monitorProcessFacade.getListChainSettlementOperation(objNetSettlementOperationTO.getIdSettlementOperation(),null);
					for (Long idChainedHolderOperation: lstIdChainedHolderOperation) {
						//we update the selection of the operations for each chain
						updateChainedOperations(idChainedHolderOperation, null, netSettlementAttributesTO.getMapParticipantPosition(), false);
					}
				}
				lstIdChainedHolderOperation= null;
				netSettlementAttributesTO.getParticipantPositionToEdit().updateCheckAllSettlementEditions();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Update chained operations.
	 *
	 * @param idChainedHolderOperation the id chained holder operation
	 * @param idSettlementOperation the id settlement operation
	 * @param mapParticipantOperations the map participant operations
	 * @param selectState the select state
	 * @throws ServiceException the service exception
	 */
	private void updateChainedOperations(Long idChainedHolderOperation,	Long idSettlementOperation,	Map<Long, NetParticipantPositionTO> mapParticipantOperations,
			boolean selectState) throws ServiceException {

		List<Long> lstIdSettlementOperationToRemove = monitorProcessFacade.getListSettlementOperationByChain(idChainedHolderOperation,
						idSettlementOperation);
		// we removed all operation of this chain
		Iterator<Long> itPositions = mapParticipantOperations.keySet().iterator();
		// we look up for all operations in all participant positions
		while (itPositions.hasNext()) {
			Long idParticipant = itPositions.next();
			List<NetSettlementOperationTO> lstParticipantOperations = mapParticipantOperations.get(idParticipant).getLstSettlementOperation();
			// for each participant position we get the chained operation to be
			// removed
			for (NetSettlementOperationTO objNetSettlementOperationTO : lstParticipantOperations) {
				for (Long idSettlementOperationToRemove : lstIdSettlementOperationToRemove) {
					// we look up the chained operation inside the participant
					// position
					if (idSettlementOperationToRemove.equals(objNetSettlementOperationTO.getIdSettlementOperation())) {
						if (objNetSettlementOperationTO.isSelected() != selectState) {
							objNetSettlementOperationTO.setSelected(selectState);

							// now we verify if this operation belongs to others
							// different chain
							List<Long> lstIdChainedHolderOperation = monitorProcessFacade.getListChainSettlementOperation(idSettlementOperationToRemove, idChainedHolderOperation);
							if (Validations
									.validateListIsNotNullAndNotEmpty(lstIdChainedHolderOperation)) {
								for (Long idChainedHolderOperationToRemove : lstIdChainedHolderOperation) {
									// we remove the operation of these chains
									updateChainedOperations(
											idChainedHolderOperationToRemove,
											idSettlementOperationToRemove,
											mapParticipantOperations,
											selectState);
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Removes the chain operation selection.
	 */
	public void removeChainOperationSelection() {
		try {
			for (Long idChainedHolderOperation: lstIdChainedHolderOperation) {
				//we remove the operations for each chain
				updateChainedOperations(idChainedHolderOperation, null, netSettlementAttributesTO.getMapParticipantPosition(), true);
			}
			lstIdChainedHolderOperation= null;
			netSettlementOperationTO.clear();
			netSettlementAttributesTO.getParticipantPositionToEdit().updateCheckAllSettlementEditions();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before end net edition.
	 */
	public void beforeEndNetEdition(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_NET_EDITION_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('idConfNetEditionProcessW').show()");
	}
	
	/**
	 * End edit net position.
	 *
	 * @throws ServiceException the service exception
	 */
	public void endEditNetPosition() throws ServiceException{
		
		Map<Long, NetParticipantPositionTO> mapParticipantPosition= netSettlementAttributesTO.getMapParticipantPosition();
		Iterator<Long> it= mapParticipantPosition.keySet().iterator();
		while (it.hasNext()) {
			Long idParticipant= it.next();
			//we get the operation for each participant position
			List<NetSettlementOperationTO> lstParticipantOperations= mapParticipantPosition.get(idParticipant).getLstSettlementOperation();
			for(NetSettlementOperationTO settlementOperation: lstParticipantOperations){
				OperationSettlement operationSettlement = settlementOperation.getOperationSettlement();
				if(settlementOperation.isSelected()){
					operationSettlement.setOperationState(OperationSettlementSateType.WITHDRAWN_BY_FUNDS.getCode());
				}else{
					operationSettlement.setOperationState(OperationSettlementSateType.SETTLEMENT_PENDIENT.getCode());
				}
			}
		}
		
		Map<Long, NetParticipantPositionTO> mapParticipantPositions = NegotiationUtils.populateParticipantPositions(netSettlementAttributesTO.getLstPendingSettlementOperations(),false);
		netSettlementAttributesTO.setLstParticipantPosition(null);
		netSettlementAttributesTO.setMapParticipantPosition(mapParticipantPositions);
		
		List<ParticipantPosition> nwParticipantPositions =  monitorProcessFacade.endEditAndRecalculateNetPositions(netSettlementAttributesTO,false);
		settlementProcessNetSession.setParticipantPositions(nwParticipantPositions);
		
		netSettlementAttributesTO.setParticipantPositionToEdit(null);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_SETTLEMENT_NET_EDITION_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Load mec operations pendient by part.
	 *
	 * @param selectedPosition the selected position
	 */
	public void loadMecOperationsPendientByPart(NetParticipantPositionTO selectedPosition){
		netSettlementAttributesTO.setParticipantRemovedPosition(getAmountForView(selectedPosition));
	}
	
	/**
	 * Clean operations detail net process.
	 */
	public void cleanOperationsDetailNetProcess(){
		netSettlementAttributesTO.setParticipantRemovedPosition(null);
	}
	
	/**
	 * Gets the amount for view.
	 *
	 * @param selectedPosition the selected position
	 * @return the amount for view
	 */
	public NetParticipantPositionTO getAmountForView(NetParticipantPositionTO selectedPosition){		
		Long idParticipantAux = selectedPosition.getIdParticipant();
		for(NetSettlementOperationTO objNetSettlementOperationTO :selectedPosition.getLstSettlementOperation()){
			if(objNetSettlementOperationTO.getIdPurchaseParticipant().equals(idParticipantAux) && 
					objNetSettlementOperationTO.getIdSaleParticipant().equals(idParticipantAux)){
				objNetSettlementOperationTO.setParticipantPurchaseAmount(objNetSettlementOperationTO.getSettlementAmount());
				objNetSettlementOperationTO.setParticipantSaleAmount(objNetSettlementOperationTO.getSettlementAmount());
			}
			else if(objNetSettlementOperationTO.getIdPurchaseParticipant().equals(idParticipantAux)){
				if(objNetSettlementOperationTO.getOperationPart().equals(OperationPartType.CASH_PART.getCode())){
					objNetSettlementOperationTO.setParticipantPurchaseAmount(objNetSettlementOperationTO.getSettlementAmount());
					objNetSettlementOperationTO.setParticipantSaleAmount(BigDecimal.ZERO);
				}else{
					objNetSettlementOperationTO.setParticipantPurchaseAmount(BigDecimal.ZERO);
					objNetSettlementOperationTO.setParticipantSaleAmount(objNetSettlementOperationTO.getSettlementAmount());
				}				
			}else if(objNetSettlementOperationTO.getIdSaleParticipant().equals(idParticipantAux)){
				if(objNetSettlementOperationTO.getOperationPart().equals(OperationPartType.CASH_PART.getCode())){
					objNetSettlementOperationTO.setParticipantSaleAmount(objNetSettlementOperationTO.getSettlementAmount());
					objNetSettlementOperationTO.setParticipantPurchaseAmount(BigDecimal.ZERO);
				}else{
					objNetSettlementOperationTO.setParticipantSaleAmount(BigDecimal.ZERO);
					objNetSettlementOperationTO.setParticipantPurchaseAmount(objNetSettlementOperationTO.getSettlementAmount());
				}			
			}
		}
		return selectedPosition;		
	}
	
	/**
	 * Cancel end edition.
	 */
	public void cancelEditNetPosition(){
		netSettlementAttributesTO.setParticipantPositionToEdit(null);
	}
	
	/**
	 * Clean selected net settlement operation.
	 */
	public void cleanSelectedNetSettlementOperation() {
		for(NetSettlementOperationTO objSettlementOperationTO : netSettlementOperationTO){
			objSettlementOperationTO.setSelected(false);
		}
		lstIdChainedHolderOperation = null;
		netSettlementOperationTO.clear();
		netSettlementAttributesTO.getParticipantPositionToEdit().updateCheckAllSettlementEditions();
	}
	
	/**
	 * Show position modality group.
	 *
	 * @param posByParti the pos by parti
	 */
	public void showPositionModalityGroup(PositionParticipantTO posByParti){
		searchPositionCommissionTO.setBlNoResultByRole(false);
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setParticipantHeader(posByParti.getIdParticipantPk());
		searchPositionCommissionTO.setModalityGroupHeader(posByParti.getIdModalityGroupPk());
		searchPositionCommissionTO.setSettlementSchemaHedaer(posByParti.getSettlementSchema());
		searchPositionCommissionTO.setLstPositionByModalityGroup(posByParti.getLstPositionByModalityGroup());
	}
	
	/**
	 * Show position sale.
	 *
	 * @param posByModaGroup the pos by moda group
	 */
	public void showPositionSale(PositionModalityGroupTO posByModaGroup){
		searchPositionCommissionTO.setRole(ParticipantRoleType.SELL.getValue());
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResultByRole(true);
		if(Validations.validateListIsNotNullAndNotEmpty(posByModaGroup.getLstPositionByRoleSell())){
			searchPositionCommissionTO.setLstPositionByRole(posByModaGroup.getLstPositionByRoleSell());
			searchPositionCommissionTO.setBlNoResultByRole(false);
		}
	}
	
	/**
	 * Show position purchase.
	 *
	 * @param posByModaGroup the pos by moda group
	 */
	public void showPositionPurchase(PositionModalityGroupTO posByModaGroup){
		searchPositionCommissionTO.setRole(ParticipantRoleType.BUY.getValue());
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResultByRole(true);
		if(Validations.validateListIsNotNullAndNotEmpty(posByModaGroup.getLstPositionByRoleBuy())){
			searchPositionCommissionTO.setLstPositionByRole(posByModaGroup.getLstPositionByRoleBuy());
			searchPositionCommissionTO.setBlNoResultByRole(false);
		}		
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setIndicator2(GeneralConstants.ONE_VALUE_STRING);
		searchPositionCommissionTO.setLstCurrency(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
		paramTab.setParameterName(SettlementSchemaType.NET.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
		paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
		lstTemp.add(paramTab);
		searchPositionCommissionTO.setLstSettlementSchema(lstTemp);
	}
	
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Gets the search position commission to.
	 *
	 * @return the search position commission to
	 */
	public SearchPositionCommissionTO getSearchPositionCommissionTO() {
		return searchPositionCommissionTO;
	}
	
	/**
	 * Sets the search position commission to.
	 *
	 * @param searchPositionCommissionTO the new search position commission to
	 */
	public void setSearchPositionCommissionTO(
			SearchPositionCommissionTO searchPositionCommissionTO) {
		this.searchPositionCommissionTO = searchPositionCommissionTO;
	}

	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}

	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}

	/**
	 * Gets the id participant bc.
	 *
	 * @return the id participant bc
	 */
	public Long getIdParticipantBC() {
		return new Long(idParticipantBC);
	}

	/**
	 * Sets the id participant bc.
	 *
	 * @param idParticipantBC the new id participant bc
	 */
	public void setIdParticipantBC(String idParticipantBC) {
		this.idParticipantBC = idParticipantBC;
	}

	/**
	 * Gets the net settlement attributes to.
	 *
	 * @return the netSettlementAttributesTO
	 */
	public NetSettlementAttributesTO getNetSettlementAttributesTO() {
		return netSettlementAttributesTO;
	}

	/**
	 * Sets the net settlement attributes to.
	 *
	 * @param netSettlementAttributesTO the netSettlementAttributesTO to set
	 */
	public void setNetSettlementAttributesTO(
			NetSettlementAttributesTO netSettlementAttributesTO) {
		this.netSettlementAttributesTO = netSettlementAttributesTO;
	}

	/**
	 * Gets the lst id chained holder operation.
	 *
	 * @return the lstIdChainedHolderOperation
	 */
	public List<Long> getLstIdChainedHolderOperation() {
		return lstIdChainedHolderOperation;
	}

	/**
	 * Sets the lst id chained holder operation.
	 *
	 * @param lstIdChainedHolderOperation the lstIdChainedHolderOperation to set
	 */
	public void setLstIdChainedHolderOperation(
			List<Long> lstIdChainedHolderOperation) {
		this.lstIdChainedHolderOperation = lstIdChainedHolderOperation;
	}

	/**
	 * Gets the net settlement operation to.
	 *
	 * @return the netSettlementOperationTO
	 */
	public List<NetSettlementOperationTO> getNetSettlementOperationTO() {
		return netSettlementOperationTO;
	}

	/**
	 * Sets the net settlement operation to.
	 *
	 * @param netSettlementOperationTO the netSettlementOperationTO to set
	 */
	public void setNetSettlementOperationTO(
			List<NetSettlementOperationTO> netSettlementOperationTO) {
		this.netSettlementOperationTO = netSettlementOperationTO;
	}

	/**
	 * Gets the settlement process net session.
	 *
	 * @return the settlementProcessNetSession
	 */
	public SettlementProcess getSettlementProcessNetSession() {
		return settlementProcessNetSession;
	}

	/**
	 * Sets the settlement process net session.
	 *
	 * @param settlementProcessNetSession the settlementProcessNetSession to set
	 */
	public void setSettlementProcessNetSession(
			SettlementProcess settlementProcessNetSession) {
		this.settlementProcessNetSession = settlementProcessNetSession;
	}

	/**
	 * @return the participantSelected
	 */
	public String getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * @param participantSelected the participantSelected to set
	 */
	public void setParticipantSelected(String participantSelected) {
		this.participantSelected = participantSelected;
	}	
	
}