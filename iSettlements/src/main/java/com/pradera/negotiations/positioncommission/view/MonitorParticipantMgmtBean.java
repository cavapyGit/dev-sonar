package com.pradera.negotiations.positioncommission.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.operations.view.OperationBusinessBean;
import com.pradera.negotiations.positioncommission.facade.MonitorParticipantServiceFacade;
import com.pradera.negotiations.positioncommission.to.PositionModalityGroupTO;
import com.pradera.negotiations.positioncommission.to.PositionParticipantTO;
import com.pradera.negotiations.positioncommission.to.PositionRoleTO;
import com.pradera.negotiations.positioncommission.to.SearchPositionCommissionTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ManagePositionsCommissionsBean.
 *
 * @author : PraderaTechnologies.
 */
@DepositaryWebBean
@LoggerCreateBean
public class MonitorParticipantMgmtBean extends GenericBaseBean {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search position commission to. */
	private SearchPositionCommissionTO searchPositionCommissionTO;
	
	private SearchPositionCommissionTO searchPositionCommissionTOAux;
	
	private List<PositionRoleTO> selectionLstPositionByRole;
	
	/** The bl participant. */
	private boolean blParticipant;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The Constant VIEW_MAPPING. */
	private final static String VIEW_MAPPING = "viewMonitorOpe";
	
	/** The general pameters facade. */
	@Inject
	private GeneralParametersFacade generalPametersFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;		
	
	/** The monitor participant service facade. */
	@EJB
	private MonitorParticipantServiceFacade monitorParticipantServiceFacade;
	
	/** The manage holder account assignment facade. */
	@Inject
	private AccountAssignmentFacade manageHolderAccountAssignmentFacade;
	
	/** The operation business comp. */
	@Inject
	private OperationBusinessBean operationBusinessComp;
	
	@Inject @Configurable String idParticipantBC;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
		selectionLstPositionByRole=new ArrayList<PositionRoleTO>();
		searchPositionCommissionTO = new SearchPositionCommissionTO();
		searchPositionCommissionTO.setSettlementDate(getCurrentSystemDate());
		try{
			fillCombos();
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				List<Participant> lstTemp = new ArrayList<Participant>();
				if(!userInfo.getUserAccountSession().getParticipantCode().equals(new Long(idParticipantBC))){
					Participant participant = manageHolderAccountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
					lstTemp.add(participant);
					searchPositionCommissionTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
					searchPositionCommissionTO.setLstParticipants(lstTemp);
					searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));
					blParticipant = true;
				}else{
					searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegotiationMechanism());
				}
			}else{
				searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegotiationMechanism());
			}
			searchPositionCommissionTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
			changeNegotiationMechanism();
			searchPositionCommissionTO.setSettlementSchemaSelected(SettlementSchemaType.NET.getCode());
			searchPositionCommissionTO.setCurrencySelected(CurrencyType.PYG.getCode());
		}catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change negotiation mechanism.
	 */
	public void changeNegotiationMechanism(){
		try {
			cleanResults();
			searchPositionCommissionTO.setLstModalityGroup(null);
			searchPositionCommissionTO.setModalityGroupSelected(null);
			if(!blParticipant){
				searchPositionCommissionTO.setLstParticipants(null);
				searchPositionCommissionTO.setParticipantSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchPositionCommissionTO.getNegoMechanismSelected())){				
				if(!blParticipant)
					searchPositionCommissionTO.setLstParticipants(manageHolderAccountAssignmentFacade.getListParticipantsForNegoMechanism(searchPositionCommissionTO.getNegoMechanismSelected(), null));
				searchPositionCommissionTO.setLstModalityGroup(monitorParticipantServiceFacade.getListModalityGroup(searchPositionCommissionTO.getNegoMechanismSelected()));
				List<ModalityGroup> lstModalityGroup = new ArrayList<ModalityGroup>();
				for(ModalityGroup objModalityGroup : searchPositionCommissionTO.getLstModalityGroup()){
					if(!objModalityGroup.getIdModalityGroupPk().equals(GeneralConstants.MODALITY_GROUP_FOP)){
						lstModalityGroup.add(objModalityGroup);
					}
				}
				searchPositionCommissionTO.setLstModalityGroup(lstModalityGroup);
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		searchPositionCommissionTO.setLstPositionByModalityGroup(null);
		searchPositionCommissionTO.setLstPositionByParticipant(null);
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResult(false);
		searchPositionCommissionTO.setBlNoResultByRole(false);
	}
	
	/**
	 * Load mechanism operation.
	 *
	 * @param mechanismOperationId the mechanism operation id
	 */
	public void loadMechanismOperation(Long mechanismOperationId){
		idMechanismOperation = mechanismOperationId;
		operationBusinessComp.setIdMechanismOperationPk(idMechanismOperation);
		operationBusinessComp.setShowAssignments(false);
		operationBusinessComp.searchMechanismOperation();
	}
		
//	public String showOperation(PositionRoleTO posiByRole){
//		try {
//			ParameterTable paramTab = new ParameterTable();
//			lstSettlementType = new ArrayList<ParameterTable>();
//			paramTab = new ParameterTable();
//			paramTab.setParameterTablePk(SettlementType.DVP.getCode());
//			paramTab.setParameterName(SettlementType.DVP.getValue());
//			lstSettlementType.add(paramTab);
//			paramTab = new ParameterTable();
//			paramTab.setParameterTablePk(SettlementType.FOP.getCode());
//			paramTab.setParameterName(SettlementType.FOP.getValue());
//			lstSettlementType.add(paramTab);
//			paramTab = new ParameterTable();
//			paramTab.setParameterTablePk(SettlementType.ECE.getCode());
//			paramTab.setParameterName(SettlementType.ECE.getValue());
//			lstSettlementType.add(paramTab);
//			mechanismOperation = monitorParticipantServiceFacade.getMechanismOperation(posiByRole);
//			mechanismOperation = monitorParticipantServiceFacade.loadMechanismOperation(mechanismOperation, posiByRole.getIdInchargeStockParticipantPk(), blParticipant);
//			if(Validations.validateListIsNotNullAndNotEmpty(mechanismOperation.getSwapOperations()))
//				swapOperation = mechanismOperation.getSwapOperations().get(0);//SOLO 1 SIEMPRE
//			return VIEW_MAPPING;
//		} catch (ServiceException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	/**
 * Clean search.
 */
public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		searchPositionCommissionTO.setCurrencySelected(null);
		searchPositionCommissionTO.setLstModalityGroup(null);
		searchPositionCommissionTO.setModalityGroupSelected(null);
		searchPositionCommissionTO.setNegoMechanismSelected(null);
		searchPositionCommissionTO.setSettlementSchemaSelected(null);		
		if(!blParticipant){
			searchPositionCommissionTO.setParticipantSelected(null);
			searchPositionCommissionTO.setLstParticipants(null);
		}
		searchPositionCommissionTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
		changeNegotiationMechanism();
		searchPositionCommissionTO.setSettlementSchemaSelected(SettlementSchemaType.NET.getCode());
		searchPositionCommissionTO.setCurrencySelected(CurrencyType.PYG.getCode());
	}
	
	/**
	 *  Gets  Positions by Participant list.
	 */
	@LoggerAuditWeb
	public void searchPositionsParticipant(){
		cleanResults();
		try {
			searchPositionCommissionTO.setTotalPurchase(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalSale(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalDebt(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalCreditor(BigDecimal.ZERO);
			searchPositionCommissionTO.setBlNoResult(true);
			List<PositionParticipantTO> lstResult = monitorParticipantServiceFacade.searchPositionByParticipant(searchPositionCommissionTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(PositionParticipantTO posByParti:lstResult){
					searchPositionCommissionTO.setTotalPurchase(searchPositionCommissionTO.getTotalPurchase().add(posByParti.getBuyPosition()));
					searchPositionCommissionTO.setTotalSale(searchPositionCommissionTO.getTotalSale().add(posByParti.getSellPosition()));
					if(posByParti.getNetPosition().compareTo(BigDecimal.ZERO) < 0){
						searchPositionCommissionTO.setTotalDebt(searchPositionCommissionTO.getTotalDebt().add(posByParti.getNetPosition()));
					}else{
						searchPositionCommissionTO.setTotalCreditor(searchPositionCommissionTO.getTotalCreditor().add(posByParti.getNetPosition()));
					}
				}
				searchPositionCommissionTO.setLstPositionByParticipant(lstResult);
				searchPositionCommissionTO.setBlNoResult(false);
				searchPositionCommissionTOAux=searchPositionCommissionTO;
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		
		}		
	}	
	
	
	public void deleteParticipantInList(){}
	/**
	 * Show position modality group.
	 *
	 * @param posByParti the pos by parti
	 */
	public void showPositionModalityGroup(PositionParticipantTO posByParti){
		searchPositionCommissionTO.setBlNoResultByRole(false);
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setParticipantHeader(posByParti.getIdParticipantPk());
		searchPositionCommissionTO.setModalityGroupHeader(posByParti.getIdModalityGroupPk());
		searchPositionCommissionTO.setSettlementSchemaHedaer(posByParti.getSettlementSchema());
		searchPositionCommissionTO.setLstPositionByModalityGroup(posByParti.getLstPositionByModalityGroup());
	}
	
	/**
	 * Show position sale.
	 *
	 * @param posByModaGroup the pos by moda group
	 */
	public void showPositionSale(PositionModalityGroupTO posByModaGroup){
		searchPositionCommissionTO.setRole(ParticipantRoleType.SELL.getValue());
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResultByRole(true);
		if(Validations.validateListIsNotNullAndNotEmpty(posByModaGroup.getLstPositionByRoleSell())){
			searchPositionCommissionTO.setLstPositionByRole(posByModaGroup.getLstPositionByRoleSell());
			searchPositionCommissionTO.setBlNoResultByRole(false);
		}
	}
	
	/**
	 * Show position purchase.
	 *
	 * @param posByModaGroup the pos by moda group
	 */
	public void showPositionPurchase(PositionModalityGroupTO posByModaGroup){
		searchPositionCommissionTO.setRole(ParticipantRoleType.BUY.getValue());
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResultByRole(true);
		if(Validations.validateListIsNotNullAndNotEmpty(posByModaGroup.getLstPositionByRoleBuy())){
			searchPositionCommissionTO.setLstPositionByRole(posByModaGroup.getLstPositionByRoleBuy());
			searchPositionCommissionTO.setBlNoResultByRole(false);
		}		
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setIndicator2(GeneralConstants.ONE_VALUE_STRING);
		searchPositionCommissionTO.setLstCurrency(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
		paramTab.setParameterName(SettlementSchemaType.NET.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
		paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
		lstTemp.add(paramTab);
		searchPositionCommissionTO.setLstSettlementSchema(lstTemp);
	}
	
	public void onChangeSelectPosition(){
		System.out.println("Se ha seleccionado el :");
	}
	
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Gets the search position commission to.
	 *
	 * @return the search position commission to
	 */
	public SearchPositionCommissionTO getSearchPositionCommissionTO() {
		return searchPositionCommissionTO;
	}
	
	/**
	 * Sets the search position commission to.
	 *
	 * @param searchPositionCommissionTO the new search position commission to
	 */
	public void setSearchPositionCommissionTO(
			SearchPositionCommissionTO searchPositionCommissionTO) {
		this.searchPositionCommissionTO = searchPositionCommissionTO;
	}

	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}

	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}

	/**
	 * Gets the id participant bc.
	 *
	 * @return the id participant bc
	 */
	public Long getIdParticipantBC() {
		return new Long(idParticipantBC);
	}

	/**
	 * Sets the id participant bc.
	 *
	 * @param idParticipantBC the new id participant bc
	 */
	public void setIdParticipantBC(String idParticipantBC) {
		this.idParticipantBC = idParticipantBC;
	}

	public List<PositionRoleTO> getSelectionLstPositionByRole() {
		return selectionLstPositionByRole;
	}

	public void setSelectionLstPositionByRole(
			List<PositionRoleTO> selectionLstPositionByRole) {
		this.selectionLstPositionByRole = selectionLstPositionByRole;
	}
}