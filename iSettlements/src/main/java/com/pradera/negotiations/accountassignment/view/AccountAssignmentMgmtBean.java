package com.pradera.negotiations.accountassignment.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationGroup;
import org.primefaces.event.SelectEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryGroupConversation;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.InchargeAgreement;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.NegotiationTypeType;
import com.pradera.model.negotiation.type.ParticipantAssignmentStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.negotiations.accountassignment.exception.AccountAssignmentException;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.accountassignment.to.AssignmentProcessTO;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.accountassignment.to.OperationWithNoSecuritiesTO;
import com.pradera.negotiations.accountassignment.to.ParticipantAssignmentTO;
import com.pradera.negotiations.accountassignment.to.RegisterAccountAssignmentTO;
import com.pradera.negotiations.accountassignment.to.SearchAccountAssignmentTO;
import com.pradera.negotiations.inchargeagreement.facade.InchargeAgreementFacade;
import com.pradera.settlements.chainedoperation.facade.ChainedOperationsFacade;
import com.pradera.settlements.chainedoperation.to.RegisterChainedOperationTO;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAssignmentMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class AccountAssignmentMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search account assignment to. */
	private SearchAccountAssignmentTO searchAccountAssignmentTO;
	
	/** The register account assignment to. */
	private RegisterAccountAssignmentTO registerAccountAssignmentTO;
	
	/** The bl participant. */
	private boolean blDepositary, blParticipant;
	
	/** The bl confirmed. */
	private boolean blPending, blRegistered, blConfirmed;
	
	/** The bl assignment expired. */
	private boolean blAssignmentProcessClosed, blParticipantAssignmentClosed, blAssignmentExpired;
	
	/** The bl open participant assignment. */
	private boolean blCloseAssignmentProcess, blCloseParticipantAssignment, blOpenParticipantAssignment;
	
	/** The bl remove. */
	private boolean blSave, blRemove;
	
	/** The participant assignment. */
	private ParticipantAssignmentTO participantAssignment;
	
	private Boolean mandatoryInChargeParticipant;
	
	/** The Constant VIEW_MAPPING. */
	private static final String VIEW_MAPPING = "viewAccountAssignment";
	
	/** The Constant REGITER_MAPPING. */
	private static final String REGITER_MAPPING = "registerAccountAssignment";
	
	/** The map parameters. */
	private HashMap<Integer, String> mapParameters = new HashMap<Integer,String>();
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The manage holder account assignment facade. */
	@EJB
	AccountAssignmentFacade manageHolderAccountAssignmentFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;	
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The incharge agreement facade. */
	@EJB
	InchargeAgreementFacade inchargeAgreementFacade;
	
	/** The manage massive assignment bean. */
	@Inject
	@ConversationGroup(DepositaryGroupConversation.class)
	MassiveAssignmentMgmtBean manageMassiveAssignmentBean;  
	
	/** The market fact assignment mgmt bean. */
	@Inject
    private MarketFactAssignmentMgmtBean marketFactAssignmentMgmtBean;
	
	/** The chained operations facade. */
	@EJB
	ChainedOperationsFacade chainedOperationsFacade;
	
	/** The negotiation modalities chained. */
	private String negotiationModalitiesChained;
	
	/** The mechanism name chained. */
	private String mechanismNameChained;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try{
			searchAccountAssignmentTO = new SearchAccountAssignmentTO();
			searchAccountAssignmentTO.setDate(getCurrentSystemDate());
			searchAccountAssignmentTO.setDateType(GeneralConstants.ZERO_VALUE_INTEGER);
			fillCombos();			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				blParticipant = true;
				List<Participant> lstTemp = new ArrayList<Participant>();
				Participant participant = manageHolderAccountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				lstTemp.add(participant);
				searchAccountAssignmentTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				searchAccountAssignmentTO.setLstParticipants(lstTemp);
				searchAccountAssignmentTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));
			}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				blDepositary = true;
				searchAccountAssignmentTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegotiationMechanism());
			}
			loadHolidays();
			loadParameters();
			searchAccountAssignmentTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
			changeNegotiationMechanism();
		}catch (Exception e) {
			 excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParameters() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.INCHARGE_SETTLEMENT_STATE.getCode());
			masterPks.add(MasterTableType.ACCOUNT_OPERATION_STATE.getCode());
			masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());			
		parameterTableTO.setLstMasterTableFk(masterPks);
		
		List<ParameterTable> lst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt.getParameterName());
		}
		
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		lst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable paramTab: lst){
			mapParameters.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
	}
	
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
	   Holiday holidayFilter = new Holiday();
	   holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
	   GeographicLocation countryFilter = new GeographicLocation();
	   countryFilter.setIdGeographicLocationPk(countryResidence);
	   holidayFilter.setGeographicLocation(countryFilter);
	   try {
		   allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	   } catch (ServiceException e) {
		  e.printStackTrace();
	   }
	}
	

	/**
	 * Register massive assignment.
	 *
	 * @return the string
	 */
	public String registerMassiveAssignment(){
		manageMassiveAssignmentBean.cleanAll();
		return "registerMassiveAssignment";
	}
	
	/**
	 * Change negotiation mechanism.
	 */
	public void changeNegotiationMechanism(){
		try {
			cleanResults();
			searchAccountAssignmentTO.setLstNegotiationModality(null);
			searchAccountAssignmentTO.setNegoModalitySelected(null);
			if(blDepositary){
				searchAccountAssignmentTO.setLstParticipants(null);
				searchAccountAssignmentTO.setParticipantSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAssignmentTO.getNegoMechanismSelected())){
				if(blDepositary){
					searchAccountAssignmentTO.setLstParticipants(manageHolderAccountAssignmentFacade.getListParticipantsForNegoMechanism(searchAccountAssignmentTO.getNegoMechanismSelected(), null));
					searchAccountAssignmentTO.setLstNegotiationModality(manageHolderAccountAssignmentFacade.getListNegotiationModality(searchAccountAssignmentTO.getNegoMechanismSelected(), null,null));
				}
				if(blParticipant){
					searchAccountAssignmentTO.setLstNegotiationModality(manageHolderAccountAssignmentFacade.getListNegotiationModality(searchAccountAssignmentTO.getNegoMechanismSelected(),
							searchAccountAssignmentTO.getParticipantSelected(),null));
				}
					
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change negotiation modality.
	 */
	public void changeNegotiationModality(){
		try {
			cleanResults();			
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAssignmentTO.getNegoModalitySelected())){
				if(blDepositary)
					searchAccountAssignmentTO.setLstParticipants(manageHolderAccountAssignmentFacade.getListParticipantsForNegoModality(searchAccountAssignmentTO.getNegoMechanismSelected()
																															, searchAccountAssignmentTO.getNegoModalitySelected(), null,null));
			}else{
				if(blDepositary)
					searchAccountAssignmentTO.setLstParticipants(manageHolderAccountAssignmentFacade.getListParticipantsForNegoMechanism(searchAccountAssignmentTO.getNegoMechanismSelected(), null));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		if(blDepositary){
			searchAccountAssignmentTO.setParticipantSelected(null);
			searchAccountAssignmentTO.setLstParticipants(null);
		}
		searchAccountAssignmentTO.setNegoMechanismSelected(null);
		searchAccountAssignmentTO.setNegoModalitySelected(null);
		searchAccountAssignmentTO.setLstNegotiationModality(null);
		searchAccountAssignmentTO.setDateType(GeneralConstants.ZERO_VALUE_INTEGER);
		searchAccountAssignmentTO.setDate(getCurrentSystemDate());
		searchAccountAssignmentTO.setAssignmentProcessState(null);
		searchAccountAssignmentTO.setParticipantAssignmentState(null);
		searchAccountAssignmentTO.setSettlementSchemaType(null);
		searchAccountAssignmentTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
		changeNegotiationMechanism();
		cleanResults();
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		searchAccountAssignmentTO.setBlNoResult(false);
		searchAccountAssignmentTO.setAssignmentProcesssSelected(null);
		searchAccountAssignmentTO.setLstAssignmentProcess(null);
		searchAccountAssignmentTO.setLstParticipantAssignment(null);
		searchAccountAssignmentTO.setLstMechanismOperation(null);
		searchAccountAssignmentTO.setLstOperationWithNoSecurities(null);
	}
	
	/**
	 * Monitor assignment process.
	 */
	@LoggerAuditWeb
	public void monitorAssignmentProcess(){
		try{
			cleanResults();
			searchAccountAssignmentTO.setBlNoResult(true);
			List<AssignmentProcessTO> lstResult = manageHolderAccountAssignmentFacade.getAssignmentProcess(searchAccountAssignmentTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				searchAccountAssignmentTO.setLstAssignmentProcess(new GenericDataModel<AssignmentProcessTO>(lstResult));
				searchAccountAssignmentTO.setBlNoResult(false);
			}
		}catch (Exception e) {
			 excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Show participant assignment.
	 *
	 * @param assignmentProcessTO the assignment process to
	 */
	public void showParticipantAssignment(AssignmentProcessTO assignmentProcessTO){
		searchAccountAssignmentTO.setLstParticipantAssignment(null);
		searchAccountAssignmentTO.setLstMechanismOperation(null);
		blAssignmentProcessClosed = false;
		blAssignmentExpired = false;		
		searchAccountAssignmentTO.setNegoModalityHeader(assignmentProcessTO.getIdNegotiationModalityPk());
		searchAccountAssignmentTO.setModalityNameHeader(assignmentProcessTO.getModalityName());
		searchAccountAssignmentTO.setAssignmentProcessHeader(assignmentProcessTO.getIdAssignmentProcessPk());
		searchAccountAssignmentTO.setSettlementDate(assignmentProcessTO.getSettlementDate());
		searchAccountAssignmentTO.setSettlementSchemaName(assignmentProcessTO.getSettlementSchemaName());
		//searchAccountAssignmentTO.setAssignmentProcessState(assignmentProcessTO.getState());
		searchAccountAssignmentTO.setAssignmentPorcessStateDesc(assignmentProcessTO.getStateDescription());
		searchAccountAssignmentTO.setLstParticipantAssignment(new GenericDataModel<ParticipantAssignmentTO>(assignmentProcessTO.getLstParticipantAssignment()));
		searchAccountAssignmentTO.setLstOperationWithNoSecurities(null);
		
		// TODO issue 1105: cargando la grilla de Operaciones con valores faltantes para cada participante
		Long idParticipant;
		for(ParticipantAssignmentTO participantAssignment: searchAccountAssignmentTO.getLstParticipantAssignment()){
			idParticipant=participantAssignment.getIdParticipantPk();
			participantAssignment.setLstOperationWithNoSecurities(manageHolderAccountAssignmentFacade.getLstOperationsWithNoSecurities(CommonsUtilities.convertDateToString(searchAccountAssignmentTO.getDate(),"dd/MM/yyyy"),idParticipant,assignmentProcessTO.getIdNegotiationModalityPk()));
			if(participantAssignment.getLstOperationWithNoSecurities().isEmpty()){
				participantAssignment.setIndOperationsWithNoSecurities(false);
			}else{
				participantAssignment.setIndOperationsWithNoSecurities(true);
				participantAssignment.setCantOperationsWithNoSecurities(participantAssignment.getLstOperationWithNoSecurities().size());
			}
		}
		
		if(AssignmentProcessStateType.CLOSED.getCode().equals(assignmentProcessTO.getState())){
			blAssignmentProcessClosed = true;
		}
		if(CommonsUtilities.currentDate().after(assignmentProcessTO.getSettlementDate())){
			blAssignmentExpired = true;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_EXPIRED_SETTLEMENT_DATE, 
							new Object[]{assignmentProcessTO.getIdAssignmentProcessPk().toString()}));
			JSFUtilities.showSimpleValidationDialog();			
		}
	}
	
	/**
	 * issue 1105: metodo para mostrar las operaciones con valores faltantes
	 * @param participantAssignmentTO
	 */
	public void showOperationsWithNoSecurities(ParticipantAssignmentTO participantAssignmentTO){
		blPending = false;
		blConfirmed = false;
		blRegistered = false;
		blParticipantAssignmentClosed = false;
		searchAccountAssignmentTO.setAssignmentState(AssignmentRequestStateType.REGISTERED.getCode());
		searchAccountAssignmentTO.setAssignmentStateDesc(PropertiesUtilities.getMessage("msg.assignment.account.registered"));
		searchAccountAssignmentTO.setParticipantHeader(participantAssignmentTO.getIdParticipantPk());
		searchAccountAssignmentTO.setLstMechanismOperation(null);
		searchAccountAssignmentTO.setLstOperationWithNoSecurities(new GenericDataModel<OperationWithNoSecuritiesTO>(participantAssignmentTO.getLstOperationWithNoSecurities()));
		if(ParticipantAssignmentStateType.CLOSED.getCode().equals(participantAssignmentTO.getState()))
			blParticipantAssignmentClosed = true;
	}
	
	/**
	 * Show pending operations.
	 *
	 * @param participantAssignmentTO the participant assignment to
	 */
	public void showPendingOperations(ParticipantAssignmentTO participantAssignmentTO){
		blPending = true;
		blConfirmed = false;
		blRegistered = false;
		blParticipantAssignmentClosed = false;
		searchAccountAssignmentTO.setAssignmentState(AssignmentRequestStateType.PENDING.getCode());
		searchAccountAssignmentTO.setAssignmentStateDesc(PropertiesUtilities.getMessage("msg.assignment.account.pending"));
		searchAccountAssignmentTO.setParticipantHeader(participantAssignmentTO.getIdParticipantPk());
		searchAccountAssignmentTO.setParticipantAssignmentHeader(participantAssignmentTO.getIdParticipantAssignmentPk());
		searchAccountAssignmentTO.setParticipantAssignmentState(participantAssignmentTO.getState());
		searchAccountAssignmentTO.setParticipantAssignmentStateDesc(participantAssignmentTO.getStateDescription());
		searchAccountAssignmentTO.setLstMechanismOperation(new GenericDataModel<MechanismOperationTO>(participantAssignmentTO.getLstPendingMechanismOperation()));
		searchAccountAssignmentTO.setLstOperationWithNoSecurities(null);
		if(ParticipantAssignmentStateType.CLOSED.getCode().equals(participantAssignmentTO.getState()))
			blParticipantAssignmentClosed = true;
	}
	
	/**
	 * Show registered operations.
	 *
	 * @param participantAssignmentTO the participant assignment to
	 */
	public void showRegisteredOperations(ParticipantAssignmentTO participantAssignmentTO){
		blPending = false;
		blConfirmed = false;
		blRegistered = true;
		blParticipantAssignmentClosed = false;
		searchAccountAssignmentTO.setAssignmentState(AssignmentRequestStateType.REGISTERED.getCode());
		searchAccountAssignmentTO.setAssignmentStateDesc(PropertiesUtilities.getMessage("msg.assignment.account.registered"));
		searchAccountAssignmentTO.setParticipantHeader(participantAssignmentTO.getIdParticipantPk());
		searchAccountAssignmentTO.setParticipantAssignmentHeader(participantAssignmentTO.getIdParticipantAssignmentPk());
		searchAccountAssignmentTO.setParticipantAssignmentState(participantAssignmentTO.getState());
		searchAccountAssignmentTO.setParticipantAssignmentStateDesc(participantAssignmentTO.getStateDescription());
		searchAccountAssignmentTO.setLstMechanismOperation(new GenericDataModel<MechanismOperationTO>(participantAssignmentTO.getLstRegisteredMechanismOperation()));
		searchAccountAssignmentTO.setLstOperationWithNoSecurities(null);
		if(ParticipantAssignmentStateType.CLOSED.getCode().equals(participantAssignmentTO.getState()))
			blParticipantAssignmentClosed = true;
	}
	
	/**
	 * Show confirmed operations.
	 *
	 * @param participantAssignmentTO the participant assignment to
	 */
	public void showConfirmedOperations(ParticipantAssignmentTO participantAssignmentTO){
		blPending = false;
		blConfirmed = true;
		blRegistered = false;
		blParticipantAssignmentClosed = false;
		searchAccountAssignmentTO.setAssignmentState(AssignmentRequestStateType.CONFIRMED.getCode());
		searchAccountAssignmentTO.setAssignmentStateDesc(PropertiesUtilities.getMessage("msg.assignment.account.confirmed"));
		searchAccountAssignmentTO.setParticipantHeader(participantAssignmentTO.getIdParticipantPk());
		searchAccountAssignmentTO.setParticipantAssignmentHeader(participantAssignmentTO.getIdParticipantAssignmentPk());
		searchAccountAssignmentTO.setParticipantAssignmentState(participantAssignmentTO.getState());
		searchAccountAssignmentTO.setParticipantAssignmentStateDesc(participantAssignmentTO.getStateDescription());
		searchAccountAssignmentTO.setLstMechanismOperation(new GenericDataModel<MechanismOperationTO>(participantAssignmentTO.getLstConfirmedMechanismOperation()));
		searchAccountAssignmentTO.setLstOperationWithNoSecurities(null);
		if(ParticipantAssignmentStateType.CLOSED.getCode().equals(participantAssignmentTO.getState()))
			blParticipantAssignmentClosed = true;
	}
	
	/**
	 * Clean validations.
	 */
	public void cleanValidations() {
		for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getAssignmentProcesssSelected()){	
			assignmentProcessTO.setCheckedMarketFacts(false);
			assignmentProcessTO.setCheckedPurchasePendingAssgn(false);
			assignmentProcessTO.setCheckedSalePendingAssgn(false);
		}
	}
	
	/**
	 * Validate close assignment process.
	 *
	 * @param firstValidation the first validation
	 */
	@LoggerAuditWeb
	public void validateCloseAssignmentProcess(Integer firstValidation){		
		
		for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getAssignmentProcesssSelected()){	
			try {
				
				if(CommonsUtilities.currentDate().after(assignmentProcessTO.getSettlementDate())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_INVALID_CLOSE, 
									new Object[]{assignmentProcessTO.getIdAssignmentProcessPk().toString()}));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				if(!assignmentProcessTO.getState().equals(AssignmentProcessStateType.OPENED.getCode())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_CLOSED_STATE,
									new Object[]{assignmentProcessTO.getIdAssignmentProcessPk().toString()}));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				if(ComponentConstant.ONE.equals(firstValidation)){
					assignmentProcessTO.setCheckedMarketFacts(false);
					assignmentProcessTO.setCheckedPurchasePendingAssgn(false);
					assignmentProcessTO.setCheckedSalePendingAssgn(false);
				}
				
				if(!assignmentProcessTO.isCheckedSalePendingAssgn()){
					assignmentProcessTO.setCheckedSalePendingAssgn(true);
					manageHolderAccountAssignmentFacade.validatePendingAssignments(assignmentProcessTO.getIdAssignmentProcessPk(), null, ComponentConstant.SALE_ROLE);
				}
				
				if(!assignmentProcessTO.isCheckedPurchasePendingAssgn()){
					assignmentProcessTO.setCheckedPurchasePendingAssgn(true);
					manageHolderAccountAssignmentFacade.validatePendingAssignments(assignmentProcessTO.getIdAssignmentProcessPk(), null, ComponentConstant.PURCHARSE_ROLE);
				}
				
				if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){ 
					if(!assignmentProcessTO.isCheckedMarketFacts()){
						assignmentProcessTO.setCheckedMarketFacts(true);
						manageHolderAccountAssignmentFacade.validateRegisteredMarketFacts(assignmentProcessTO.getIdAssignmentProcessPk(),null);
					}
				}
			} catch (ServiceException e) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
				JSFUtilities.executeJavascriptFunction("PF('cnfWCloseAssignmentDlg').show();");
				return;
			}
		}
		
		JSFUtilities.executeJavascriptFunction("PF('cnfWCloseAssignmentDlg').hide();");
		
		beforeCloseAssignmentProcess();
	}
	
	
//	/**
//	 * Validate close assignment process.
//	 *
//	 * @param firstValidation the first validation
//	 */
//	@LoggerAuditWeb
//	public void validateCloseAssignmentProcess(Integer firstValidation){		
//		if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){ 
//			
//			String inconsistences="";
//			
//			for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getAssignmentProcesssSelected()){	
//				try {
//					
//					String inconsistencesPendingsAccount1="";
//					String inconsistencesPendingsAccount2="";
//					String inconsistencesMarketfact="";
//					
//					if(CommonsUtilities.currentDate().after(assignmentProcessTO.getSettlementDate())){
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//								PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_INVALID_CLOSE, 
//										new Object[]{assignmentProcessTO.getIdAssignmentProcessPk().toString()}));
//						JSFUtilities.showSimpleValidationDialog();
//						return;
//					}
//					
//					if(!assignmentProcessTO.getState().equals(AssignmentProcessStateType.OPENED.getCode())){
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//								PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_CLOSED_STATE,
//										new Object[]{assignmentProcessTO.getIdAssignmentProcessPk().toString()}));
//						JSFUtilities.showSimpleValidationDialog();
//						return;
//					}
//					
//					if(ComponentConstant.ONE.equals(firstValidation)){
//						assignmentProcessTO.setCheckedMarketFacts(false);
//						assignmentProcessTO.setCheckedPurchasePendingAssgn(false);
//						assignmentProcessTO.setCheckedSalePendingAssgn(false);
//					}
//					
//					if(!assignmentProcessTO.isCheckedSalePendingAssgn()){
//						assignmentProcessTO.setCheckedSalePendingAssgn(true);
//						inconsistencesPendingsAccount1=manageHolderAccountAssignmentFacade.validatePendingAssignments(assignmentProcessTO.getIdAssignmentProcessPk(), null, ComponentConstant.SALE_ROLE);
//					}
//					
//					if(!assignmentProcessTO.isCheckedPurchasePendingAssgn()){
//						assignmentProcessTO.setCheckedPurchasePendingAssgn(true);
//						inconsistencesPendingsAccount2=manageHolderAccountAssignmentFacade.validatePendingAssignments(assignmentProcessTO.getIdAssignmentProcessPk(), null, ComponentConstant.PURCHARSE_ROLE);
//					}
//					
////					if(!assignmentProcessTO.isCheckedMarketFacts()){
////						assignmentProcessTO.setCheckedMarketFacts(true);
////						manageHolderAccountAssignmentFacade.validateRegisteredMarketFacts(assignmentProcessTO.getIdAssignmentProcessPk(),null);
////					}
//					
//					/* Issue 112 Falta de Valores */
//					if(!assignmentProcessTO.isCheckedMarketFacts()){	
//						assignmentProcessTO.setCheckedMarketFacts(true);
//						inconsistencesMarketfact=
//						manageHolderAccountAssignmentFacade.validateIncompleteSecurities(
//								assignmentProcessTO.getIdNegotiationModalityPk(), null,null, ComponentConstant.SALE_ROLE,searchAccountAssignmentTO.getDate());
//
//					}
//					
//					inconsistences = inconsistences+inconsistencesPendingsAccount1+inconsistencesPendingsAccount2+inconsistencesMarketfact;	
//					
//				} catch (ServiceException e) {
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
//					JSFUtilities.executeJavascriptFunction("PF('cnfWCloseAssignmentDlg').show();");
//					return;
//				}
//			}
//			
//		//	inconsistences = inconsistencesPendingsAccount1+inconsistencesPendingsAccount2+inconsistencesMarketfact;	
//			if (Validations.validateIsNotEmpty(inconsistences) ){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						,(PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INCONSISTENCES_GENERAL,inconsistences)));
//				JSFUtilities.executeJavascriptFunction("PF('cnfWCloseAssignmentDlg').show();");
//				return;
//			}
//			
//		}
//		JSFUtilities.executeJavascriptFunction("PF('cnfWCloseAssignmentDlg').hide();");
//		
//		beforeCloseAssignmentProcess();
//	}
	
	/**
	 * Validate chained holder operations.
	 *
	 * @param settlementDate the settlement date
	 * @return the list
	 */
	public List<Object[]> validateChainedHolderOperations(Date settlementDate){
		Integer instrumentType= InstrumentType.FIXED_INCOME.getCode();
		List<Integer> lstIndExtended = new ArrayList<Integer>();
		lstIndExtended.add(BooleanType.YES.getCode());
		lstIndExtended.add(BooleanType.NO.getCode());
		List<Participant> lstParticipants;
		List<Object[]> lstIdParticipantsPks = new ArrayList<Object[]>();		
		try {
			lstParticipants = manageHolderAccountAssignmentFacade.getListParticipantsForNegoMechanism(searchAccountAssignmentTO.getNegoMechanismSelected(), null);						
			if(Validations.validateListIsNotNullAndNotEmpty(lstParticipants)){
				for(Participant objParticipant : lstParticipants){
					
					List<RegisterChainedOperationTO> lstRegisterChainedOperationTO = chainedOperationsFacade.validateChainedHolderOperations(settlementDate, 
							objParticipant.getIdParticipantPk(), lstIndExtended, instrumentType);
					
					if (Validations.validateListIsNotNullAndNotEmpty(lstRegisterChainedOperationTO)) {
						
						for (RegisterChainedOperationTO objRegisterChainedOperationTO: lstRegisterChainedOperationTO) {							
							Object[] objRegChainedOperation = {objRegisterChainedOperationTO.getParticipant().getIdParticipantPk(),
									objRegisterChainedOperationTO.getParticipant().getMnemonic()};
							lstIdParticipantsPks.add(objRegChainedOperation);
							break;
						}
						
					}
				}
			}			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}			
		return lstIdParticipantsPks;
	}
	
	/**
	 * to validate the Selected combo boxes related states are opened or not.
	 */
	public void beforeCloseAssignmentProcess(){
		if(Validations.validateIsNotNull(searchAccountAssignmentTO.getAssignmentProcesssSelected()) 
				&& searchAccountAssignmentTO.getAssignmentProcesssSelected().length>0){
			String negotiationModalities = null;
			boolean blFirstTime = true; 
			for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getAssignmentProcesssSelected()){				
				
				if(blFirstTime){
					negotiationModalities = assignmentProcessTO.getModalityName();
					blFirstTime = false;
				}else{
					negotiationModalities = negotiationModalities + GeneralConstants.STR_COMMA + assignmentProcessTO.getModalityName();
				}
				
			}
			negotiationModalitiesChained = negotiationModalities;						
			
			String mechanismName = null;
			for(NegotiationMechanism negoMechanism:searchAccountAssignmentTO.getLstNegotiationMechanism()){
				if(negoMechanism.getIdNegotiationMechanismPk().equals(searchAccountAssignmentTO.getNegoMechanismSelected())){
					mechanismName = negoMechanism.getMechanismName();
					break;
				}
			}
			mechanismNameChained = mechanismName;
			
			blCloseAssignmentProcess = true;
			blCloseParticipantAssignment = false;
			blOpenParticipantAssignment = false;
			
			// Agregado para la validacion de cadenas
			List<Object[]> lstIdParticipantsPks = validateChainedHolderOperations(CommonsUtilities.currentDate());
			if(Validations.validateListIsNotNullAndNotEmpty(lstIdParticipantsPks)){		
				String strMenmonicParticipant = GeneralConstants.EMPTY_STRING;			
				for(Object[] objDataParticipant : lstIdParticipantsPks){
					if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strMenmonicParticipant)) {
						strMenmonicParticipant += objDataParticipant[1].toString();
					} else {
						strMenmonicParticipant += GeneralConstants.STR_COMMA + objDataParticipant[1].toString();
					}
				}
				Object[] argObj = new Object[1];
				argObj[0] = strMenmonicParticipant;
				if(lstIdParticipantsPks.size() == 1){				
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_ASKCHAINED_ONE_EXITS, argObj));
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogChained').show();");
					return;
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_ASKCHAINED_MASSIVE_EXITS, argObj));
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogChained').show();");
					return;
				}														
			}
			
			Object[] argObj = new Object[2];
			argObj[0] = mechanismName;
			argObj[1] = negotiationModalities;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_ASK_CLOSE, argObj));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * to validate the Selected combo boxes related states are opened or not.
	 */
	public void beforeValidationsChained(){
		Object[] argObj = new Object[2];
		argObj[0] = mechanismNameChained;
		argObj[1] = negotiationModalitiesChained;
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_ASK_CLOSE, argObj));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Validate close participant assignment.
	 *
	 * @param participantAssignmentTO the participant assignment to
	 * @param firstValidation the first validation
	 */
	@LoggerAuditWeb
	public void validateCloseParticipantAssignment(ParticipantAssignmentTO participantAssignmentTO, Integer firstValidation){
		 
		try {
			
			if(ComponentConstant.ONE.equals(firstValidation)){
				participantAssignment  = participantAssignmentTO;
				participantAssignment.setCheckedMarketFacts(false);
				participantAssignment.setCheckedPurchasePendingAssgn(false);
				participantAssignment.setCheckedSalePendingAssgn(false);
			}
			Object[] argObj = new Object[1];
			argObj[0] = new StringBuffer().append(participantAssignment.getParticipantFullDesc()).append(GeneralConstants.DASH).append(participantAssignment.getIdParticipantPk()).toString();
			
			if (ParticipantAssignmentStateType.CLOSED.getCode().equals(participantAssignment.getState())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PA_CLOSED_STATE, argObj));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(ComponentConstant.ONE.equals(firstValidation)){
				participantAssignment.setCheckedMarketFacts(false);
				participantAssignment.setCheckedPurchasePendingAssgn(false);
				participantAssignment.setCheckedSalePendingAssgn(false);
			}
			
			if(!participantAssignment.isCheckedSalePendingAssgn()){
				participantAssignment.setCheckedSalePendingAssgn(true);
				manageHolderAccountAssignmentFacade.validatePendingAssignments(
						searchAccountAssignmentTO.getAssignmentProcessHeader(), participantAssignment.getIdParticipantAssignmentPk(), ComponentConstant.SALE_ROLE);
			}
			
			if(!participantAssignment.isCheckedPurchasePendingAssgn()){
				participantAssignment.setCheckedPurchasePendingAssgn(true);
				manageHolderAccountAssignmentFacade.validatePendingAssignments(
						searchAccountAssignmentTO.getAssignmentProcessHeader(), participantAssignment.getIdParticipantAssignmentPk(), ComponentConstant.PURCHARSE_ROLE);
			}
				
			Participant participant = searchAccountAssignmentTO.getLstParticipants()
										.stream().filter(p -> p.getIdParticipantPk().equals(participantAssignment.getIdParticipantPk()))
										.findFirst().get();
			
			if(participant.getIndDepositary().equals(BooleanType.YES.getCode())) {
				manageHolderAccountAssignmentFacade.checkHolderAccountInParticipant(
						null, participantAssignment.getIdParticipantPk(), Boolean.TRUE, Boolean.FALSE);
			} else {
				if(Validations.validateIsNotNullAndPositive(idepositarySetup.getIdInChargeParticipant())){
					manageHolderAccountAssignmentFacade.checkHolderAccountInParticipant(
							null, participantAssignment.getIdParticipantPk(), Boolean.FALSE, Boolean.TRUE);
				}
			}			
			
			if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){	
				if(!participantAssignment.isCheckedMarketFacts()){
					participantAssignment.setCheckedMarketFacts(true);
					manageHolderAccountAssignmentFacade.validateRegisteredMarketFacts(searchAccountAssignmentTO.getAssignmentProcessHeader(), 
																						participantAssignment.getIdParticipantAssignmentPk());
				}
			}			

			JSFUtilities.executeJavascriptFunction("PF('cnfWCloseParticipantAssignmentDlg').hide();");
			
			beforeCloseParticipantAssignment(participantAssignment);
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWCloseParticipantAssignmentDlg').show();");
			return;
		} catch (AccountAssignmentException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, e.getMessage());
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
				
	}
	
//	/**
//	 * Validate close participant assignment.
//	 *
//	 * @param participantAssignmentTO the participant assignment to
//	 * @param firstValidation the first validation
//	 */
//	@LoggerAuditWeb
//	public void validateCloseParticipantAssignment(ParticipantAssignmentTO participantAssignmentTO, Integer firstValidation){
//		if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){ 
//			try {
//				
//				String inconsistences="";
//				String inconsistencesPendingsAccount1="";
//				String inconsistencesPendingsAccount2="";
//				String inconsistencesMarketfact="";
//				
//				if(ComponentConstant.ONE.equals(firstValidation)){
//					participantAssignment  = participantAssignmentTO;
//					participantAssignment.setCheckedMarketFacts(false);
//					participantAssignment.setCheckedPurchasePendingAssgn(false);
//					participantAssignment.setCheckedSalePendingAssgn(false);
//				}
//					
//					Object[] argObj = new Object[1];
//					argObj[0] = new StringBuffer().append(participantAssignment.getParticipantFullDesc()).append(GeneralConstants.DASH).append(participantAssignment.getIdParticipantPk()).toString();
//					
//					if (ParticipantAssignmentStateType.CLOSED.getCode().equals(participantAssignment.getState())){
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//								PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PA_CLOSED_STATE, argObj));
//						JSFUtilities.showSimpleValidationDialog();
//						return;
//					}
//				
//				if(ComponentConstant.ONE.equals(firstValidation)){
//					participantAssignment.setCheckedMarketFacts(false);
//					participantAssignment.setCheckedPurchasePendingAssgn(false);
//					participantAssignment.setCheckedSalePendingAssgn(false);
//				}
//				
//				if(!participantAssignment.isCheckedSalePendingAssgn()){
//					participantAssignment.setCheckedSalePendingAssgn(true);
//					inconsistencesPendingsAccount1=
//					manageHolderAccountAssignmentFacade.validatePendingAssignments(
//							searchAccountAssignmentTO.getAssignmentProcessHeader(), participantAssignment.getIdParticipantAssignmentPk(), ComponentConstant.SALE_ROLE);
//				}
//				
//				if(!participantAssignment.isCheckedPurchasePendingAssgn()){
//					participantAssignment.setCheckedPurchasePendingAssgn(true);
//					inconsistencesPendingsAccount2=
//					manageHolderAccountAssignmentFacade.validatePendingAssignments(
//							searchAccountAssignmentTO.getAssignmentProcessHeader(), participantAssignment.getIdParticipantAssignmentPk(), ComponentConstant.PURCHARSE_ROLE);
//				}
//				
////				if(!participantAssignment.isCheckedMarketFacts()){
////					participantAssignment.setCheckedMarketFacts(true);
////					manageHolderAccountAssignmentFacade.validateRegisteredMarketFacts(searchAccountAssignmentTO.getAssignmentProcessHeader(), 
////																						participantAssignment.getIdParticipantAssignmentPk());
////				}
//				
//				/* Issue 112 Falta de Valores */
//				if(!participantAssignment.isCheckedMarketFacts()){
//					participantAssignment.setCheckedMarketFacts(true);
//					inconsistencesMarketfact=
//					manageHolderAccountAssignmentFacade.validateIncompleteSecurities(
//							searchAccountAssignmentTO.getNegoModalityHeader(), participantAssignment.getIdParticipantAssignmentPk(),participantAssignment.getIdParticipantPk(), ComponentConstant.SALE_ROLE,searchAccountAssignmentTO.getDate());
//				}
//				inconsistences = inconsistencesPendingsAccount1+inconsistencesPendingsAccount2+inconsistencesMarketfact;	
//				if (Validations.validateIsNotEmpty(inconsistences) ){
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							,(PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INCONSISTENCES,inconsistences)));
//					JSFUtilities.executeJavascriptFunction("PF('cnfWCloseParticipantAssignmentDlg').show();");
//					return;
//				}
//				
//			} catch (ServiceException e) {
////				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
////						, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
////				JSFUtilities.executeJavascriptFunction("PF('cnfWCloseParticipantAssignmentDlg').show();");
////				return;
//			}
//		}
//		
//		JSFUtilities.executeJavascriptFunction("PF('cnfWCloseParticipantAssignmentDlg').hide();");
//		
//		beforeCloseParticipantAssignment(participantAssignment);
//	}
	
	/**
	 * Before close participant assignment.
	 *
	 * @param participantAssignmentTO the participant assignment to
	 */
	public void beforeCloseParticipantAssignment(ParticipantAssignmentTO participantAssignmentTO){
		Object[] argObj = new Object[1];
		argObj[0] = new StringBuffer().append(participantAssignmentTO.getParticipantFullDesc()).append(GeneralConstants.DASH).append(participantAssignmentTO.getIdParticipantPk()).toString();
		
		blCloseAssignmentProcess = false;
		blCloseParticipantAssignment = true;
		blOpenParticipantAssignment = false;
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PA_ASK_CLOSE, argObj));			
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Before open participant assignment.
	 *
	 * @param participantAssignmentTO the participant assignment to
	 */
	public void beforeOpenParticipantAssignment(ParticipantAssignmentTO participantAssignmentTO){		
		Object[] argObj = new Object[1];
		argObj[0] = new StringBuffer().append(participantAssignmentTO.getParticipantFullDesc()).append(GeneralConstants.DASH).append(participantAssignmentTO.getIdParticipantPk()).toString();
		
		if (ParticipantAssignmentStateType.OPENED.getCode().equals(participantAssignmentTO.getState())){
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PA_OPENED_STATE, argObj));
			JSFUtilities.showSimpleValidationDialog();
		} else {
			participantAssignment  = participantAssignmentTO;
			blCloseAssignmentProcess = false;
			blCloseParticipantAssignment = false;
			blOpenParticipantAssignment = true;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PA_ASK_OPEN, argObj));			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		try {
			if(blCloseAssignmentProcess){
				closeAssignmentProcess();
			}else if(blCloseParticipantAssignment){
				closeParticipantAssignment();
			}else if(blOpenParticipantAssignment){
				openParticipantAssignment();
			}	
		} catch (ServiceException e) {
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
								, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
		}		
	}
	
	/**
	 * Close assignment process.
	 *
	 * @throws ServiceException the service exception
	 */
	private void closeAssignmentProcess() throws ServiceException{
		try {
			String negotiationModalities = null;
			boolean blFirstTime = true;
			String strIdAssignmentProcess= GeneralConstants.EMPTY_STRING;
		
			for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getAssignmentProcesssSelected()){
				if (AssignmentProcessStateType.OPENED.getCode().equals(assignmentProcessTO.getState())) {
					//manageHolderAccountAssignmentFacade.closeGeneralAssignmentProcess(assignmentProcessTO.getIdAssignmentProcessPk());
					if(blFirstTime){
						negotiationModalities = assignmentProcessTO.getModalityName();
						blFirstTime = false;
					}else{
						negotiationModalities = negotiationModalities + GeneralConstants.STR_COMMA + assignmentProcessTO.getModalityName();
					}
					strIdAssignmentProcess += assignmentProcessTO.getIdAssignmentProcessPk() + GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
			}
			Map<String, Object> processParameters=new HashMap<String, Object>();
			processParameters.put(SettlementConstant.CLOSSING_ASSIGNMENT_PROCESS, strIdAssignmentProcess);
			BusinessProcess objBusinessProcess= new BusinessProcess();
			objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GENERAL_ASSIGNMENT_CLOSE.getCode());
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
			String mechanismName = null;
			for(NegotiationMechanism negoMechanism:searchAccountAssignmentTO.getLstNegotiationMechanism()){
				if(negoMechanism.getIdNegotiationMechanismPk().equals(searchAccountAssignmentTO.getNegoMechanismSelected())){
					mechanismName = negoMechanism.getMechanismName();
					break;
				}
			}
			Object[] argObj = new Object[2];
			argObj[0] = mechanismName;
			argObj[1] = negotiationModalities;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_AP_SUCCESS_CLOSE, argObj));			
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			cleanResults();
		} catch (ServiceException e) {
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
								, PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
		}
	}
	
	/**
	 * Close participant assignment.
	 *
	 * @throws ServiceException the service exception
	 */
	private void closeParticipantAssignment() throws ServiceException{
		
		manageHolderAccountAssignmentFacade.updateParticipantAssignment(participantAssignment.getIdParticipantAssignmentPk(), ParticipantAssignmentStateType.CLOSED.getCode());
		monitorAssignmentProcess();
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAssignmentTO.getLstAssignmentProcess())
				&& Validations.validateListIsNotNullAndNotEmpty(searchAccountAssignmentTO.getLstAssignmentProcess().getDataList())){
			for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getLstAssignmentProcess()){
				if(searchAccountAssignmentTO.getAssignmentProcessHeader().equals(assignmentProcessTO.getIdAssignmentProcessPk())){
					showParticipantAssignment(assignmentProcessTO);
					break;
				}					
			}
		}
		Object[] argObj = new Object[1];
		argObj[0] = new StringBuffer().append(participantAssignment.getParticipantFullDesc()).append(GeneralConstants.DASH).append(participantAssignment.getIdParticipantPk()).toString();
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PA_SUCCESS_CLOSE, argObj));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Open participant assignment.
	 *
	 * @throws ServiceException the service exception
	 */
	private void openParticipantAssignment() throws ServiceException{
		manageHolderAccountAssignmentFacade.updateParticipantAssignment(participantAssignment.getIdParticipantAssignmentPk(), ParticipantAssignmentStateType.OPENED.getCode());
		monitorAssignmentProcess();
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAssignmentTO.getLstAssignmentProcess())
				&& Validations.validateListIsNotNullAndNotEmpty(searchAccountAssignmentTO.getLstAssignmentProcess().getDataList())){
			for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getLstAssignmentProcess()){
				if(searchAccountAssignmentTO.getAssignmentProcessHeader().equals(assignmentProcessTO.getIdAssignmentProcessPk())){
					showParticipantAssignment(assignmentProcessTO);
					break;
				}					
			}
		}
		Object[] argObj = new Object[1];
		argObj[0] = new StringBuffer().append(participantAssignment.getParticipantFullDesc()).append(GeneralConstants.DASH).append(participantAssignment.getIdParticipantPk()).toString();
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PA_SUCCESS_OPEN, argObj));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * View account assignment.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @return the string
	 */
	public String viewAccountAssignment(MechanismOperationTO mechanismOperationTO){
		try {
			registerAccountAssignmentTO = new RegisterAccountAssignmentTO();
			registerAccountAssignmentTO.setHolAccOpeMarketFact(new AccountOperationMarketFact());
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){				
				registerAccountAssignmentTO.setBlMarketFact(true);
			}
			registerAccountAssignmentTO.setAmountAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setQuantityAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setCrossAmountAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setCrossQuantityAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setParticipantOrigin(searchAccountAssignmentTO.getParticipantHeader());
			fillMechanismOperation(mechanismOperationTO);
			fillHolderAccountOperations(mechanismOperationTO);
			return VIEW_MAPPING;	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	/**
	 * Register account assignment.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @return the string
	 */
	public String registerAccountAssignment(MechanismOperationTO mechanismOperationTO){
		try {
			
			registerAccountAssignmentTO = new RegisterAccountAssignmentTO();
			registerAccountAssignmentTO.setHolAccOpeMarketFact(new AccountOperationMarketFact());
			registerAccountAssignmentTO.setHolder(new Holder());
			registerAccountAssignmentTO.setLstHolderAccountOperationRemoved(new ArrayList<HolderAccountOperation>());
			registerAccountAssignmentTO.setParticipantOrigin(searchAccountAssignmentTO.getParticipantHeader());
			registerAccountAssignmentTO.setAmountAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setQuantityAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setCrossAmountAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setCrossQuantityAssig(BigDecimal.ZERO);
			registerAccountAssignmentTO.setHolderAccountOperation(new HolderAccountOperation());
			registerAccountAssignmentTO.getHolderAccountOperation().setIsSelected(false);
			registerAccountAssignmentTO.setBlInchargeAgreementDisabled(false);
			registerAccountAssignmentTO.getHolderAccountOperation().setRole(mechanismOperationTO.getRole());			
			registerAccountAssignmentTO.getHolderAccountOperation().setInchargeFundsParticipant(new Participant(registerAccountAssignmentTO.getParticipantOrigin()));
			registerAccountAssignmentTO.getHolderAccountOperation().setInchargeStockParticipant(new Participant(registerAccountAssignmentTO.getParticipantOrigin()));
			registerAccountAssignmentTO.setIsDepositaryClient(Boolean.FALSE);
			registerAccountAssignmentTO.getHolderAccountOperation().setIndAutomatedIncharge(ComponentConstant.ZERO);
			
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){				
				registerAccountAssignmentTO.setBlMarketFact(true);
			}
			
			fillMechanismOperation(mechanismOperationTO);			
			fillHolderAccountOperations(mechanismOperationTO);

			if(Validations.validateIsNotNullAndPositive(idepositarySetup.getIdInChargeParticipant())) {
				this.mandatoryInChargeParticipant = this.manageHolderAccountAssignmentFacade.fillInChargeParticipant(registerAccountAssignmentTO, idepositarySetup.getIdInChargeParticipant(), mapParameters);
				if(this.mandatoryInChargeParticipant)
					this.assignHolderAccount();
			} else {
				this.mandatoryInChargeParticipant = Boolean.FALSE;
			}
			
			Long idModality = registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk();
			
			if(NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode().equals(idModality)
					|| NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode().equals(idModality)){
				registerAccountAssignmentTO.setBlOperationRepo(true);
				fillHolderAccountOperationRepo(registerAccountAssignmentTO.getMechanismOperation().getIdMechanismOperationPk());
			}
			return REGITER_MAPPING;	
		} catch (AccountAssignmentException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, e.getMessage());
			JSFUtilities.showSimpleValidationDialog();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Reset inchargers.
	 */
	public void resetInchargers(){
		if(!registerAccountAssignmentTO.getHolderAccountOperation().getIsSelected()){
			registerAccountAssignmentTO.getHolderAccountOperation().getInchargeFundsParticipant().setIdParticipantPk(registerAccountAssignmentTO.getParticipantOrigin());
			registerAccountAssignmentTO.getHolderAccountOperation().getInchargeStockParticipant().setIdParticipantPk(registerAccountAssignmentTO.getParticipantOrigin());
		}
	}
	
	/**
	 * Change role.
	 *
	 * @throws ServiceException the service exception
	 */
	public void changeRole() throws ServiceException{
		//registerAccountAssignmentTO.setBlOperationRepo(false);
		registerAccountAssignmentTO.setLstHolderAccountOperationRepo(null);
		registerAccountAssignmentTO.setHolderAccountOperationRepo(null);
		if(registerAccountAssignmentTO.isBlOperationRepo() && ParticipantRoleType.SELL.getCode().equals(registerAccountAssignmentTO.getHolderAccountOperation().getRole())){
			JSFUtilities.resetViewRoot();
			//registerAccountAssignmentTO.setBlOperationRepo(true);			
			fillHolderAccountOperationRepo(registerAccountAssignmentTO.getMechanismOperation().getIdMechanismOperationPk());
		}
		validateInchargeAgreement();
	}
	
	/**
	 * Un select holder account operation.
	 */
	public void unSelectHolderAccountOperation(){
		JSFUtilities.resetComponent(":frmAccountAssignment:quantity");
		JSFUtilities.resetComponent(":frmAccountAssignment:holderHelper:holderHelper");
		registerAccountAssignmentTO.setHolderAccountOperationRepo(null);
	}
	
	/**
	 * Reset holder account operation.
	 *
	 * @param evet the evet
	 */
	public void resetHolderAccountOperation(SelectEvent evet){
		JSFUtilities.resetComponent(":frmAccountAssignment:quantity");
		JSFUtilities.resetComponent(":frmAccountAssignment:holderHelper:holderHelper");
		registerAccountAssignmentTO.setHolder(new Holder());
		registerAccountAssignmentTO.setLstHolderAccount(null);
		registerAccountAssignmentTO.getHolderAccountOperation().setHolderAccount(null);
		registerAccountAssignmentTO.getHolderAccountOperation().setStockQuantity(null);
		registerAccountAssignmentTO.getHolderAccountOperation().setCashAmount(null);
		registerAccountAssignmentTO.getHolderAccountOperation().setSettlementAmount(null);
	}
	
	/**
	 * Validate holder.
	 */
	public void validateHolder(){
		try {
			registerAccountAssignmentTO.setLstHolderAccount(null);
			registerAccountAssignmentTO.getHolderAccountOperation().setHolderAccount(null);
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountAssignmentTO.getHolder().getIdHolderPk())){
				if(HolderStateType.BLOCKED.getCode().equals(registerAccountAssignmentTO.getHolder().getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_BLOCKED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAssignmentTO.setHolder(new Holder());
					updateInchargeAgreement();
					return;
				}					
				
				if(Validations.validateIsNullOrEmpty(registerAccountAssignmentTO.getHolderAccountOperation().getInchargeStockParticipant())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAssignmentTO.setHolder(new Holder());
					updateInchargeAgreement();
				}else{
					List<HolderAccount> lstResult = manageHolderAccountAssignmentFacade.findHolderAccounts(registerAccountAssignmentTO.getHolder().getIdHolderPk(), 
												registerAccountAssignmentTO.getParticipantOrigin());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						for(HolderAccount holderAccount:lstResult){
							holderAccount.setAccountDescription(mapParameters.get(holderAccount.getAccountType()));
						}
						registerAccountAssignmentTO.setLstHolderAccount(lstResult);
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						registerAccountAssignmentTO.setHolder(new Holder());	
						updateInchargeAgreement();
					}
				}
				//verificamos contrato de encargo
				validateInchargeAgreement();
			}else {
				updateInchargeAgreement();
			}				
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Validate incharge agreement.
	 */
	public void validateInchargeAgreement(){
		try{
		if(Validations.validateIsNotNullAndPositive(registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk())
				&& Validations.validateIsNotNullAndPositive(registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk())
				&& Validations.validateIsNotNullAndPositive(registerAccountAssignmentTO.getParticipantOrigin())
				&& Validations.validateIsNotNullAndPositive(registerAccountAssignmentTO.getHolderAccountOperation().getRole())){
			boolean rolBuy=false;
			boolean rolSell=false;
			if(registerAccountAssignmentTO.getHolderAccountOperation().getRole().equals(ParticipantRoleType.BUY.getCode()))
				rolBuy=true;
			if(registerAccountAssignmentTO.getHolderAccountOperation().getRole().equals(ParticipantRoleType.SELL.getCode()))
				rolSell=true;
			List<InchargeAgreement>  lstInchargeAgreement = (List<InchargeAgreement>)inchargeAgreementFacade.getInchargeAgreementForFilter
					(registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk(),
							registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk(),
							rolBuy,rolSell, 
							registerAccountAssignmentTO.getParticipantOrigin(),null, registerAccountAssignmentTO.getHolder().getIdHolderPk());
			if(Validations.validateIsNotNull(lstInchargeAgreement) && lstInchargeAgreement.size() == GeneralConstants.ONE_VALUE_INTEGER){
				InchargeAgreement inchargeAgreement = (InchargeAgreement)lstInchargeAgreement.get(GeneralConstants.ZERO_VALUE_INTEGER);
				List<HolderAccount> lstResultAux = manageHolderAccountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
						inchargeAgreement.getTraderParticipant().getIdParticipantPk());
				if(Validations.validateListIsNullOrEmpty(lstResultAux)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE_AGREEMENT));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAssignmentTO.setHolder(new Holder());	
					updateInchargeAgreement();
					return;
				}
				
				List<HolderAccount> lstResult = manageHolderAccountAssignmentFacade.findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
						inchargeAgreement.getInchargeParticipant().getIdParticipantPk());
				if(Validations.validateListIsNullOrEmpty(lstResult)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE_AGREEMENT));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAssignmentTO.setHolder(new Holder());	
					updateInchargeAgreement();
					return;
				}
				if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)
						&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					registerAccountAssignmentTO.getHolderAccountOperation().setInchargeFundsParticipant(inchargeAgreement.getInchargeParticipant());
					registerAccountAssignmentTO.getHolderAccountOperation().setInchargeStockParticipant(inchargeAgreement.getInchargeParticipant());
				}
				if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ZERO_VALUE_INTEGER)
						&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					registerAccountAssignmentTO.getHolderAccountOperation().setInchargeStockParticipant(inchargeAgreement.getInchargeParticipant());
					registerAccountAssignmentTO.getHolderAccountOperation().setInchargeFundsParticipant(inchargeAgreement.getTraderParticipant());
				}
				if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)
						&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
					registerAccountAssignmentTO.getHolderAccountOperation().setInchargeFundsParticipant(inchargeAgreement.getInchargeParticipant());
					registerAccountAssignmentTO.getHolderAccountOperation().setInchargeStockParticipant(inchargeAgreement.getTraderParticipant());
				}
				registerAccountAssignmentTO.getHolderAccountOperation().setIsSelected(true);
				registerAccountAssignmentTO.setBlInchargeAgreementDisabled(true);
			}
			else{
				updateInchargeAgreement();
			}
		}else{
			updateInchargeAgreement();
		}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Update incharge agreement.
	 */
	public void updateInchargeAgreement(){
		if(registerAccountAssignmentTO.isBlInchargeAgreementDisabled()){
			registerAccountAssignmentTO.getHolderAccountOperation().setIsSelected(false);
			registerAccountAssignmentTO.setBlInchargeAgreementDisabled(false);
			resetInchargers();
		}
	}
	
	/**
	 * Calculate amount.
	 */
	public void calculateAmount(){		
		try {
			this.manageHolderAccountAssignmentFacade.calculateAmount(registerAccountAssignmentTO);
		} catch (AccountAssignmentException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(e.getMessage()));
			JSFUtilities.showSimpleValidationDialog();			
		}
		
	}

	/**
	 * Assign holder account.
	 */
	public void assignHolderAccount(){
		try {
			registerAccountAssignmentTO.setHolderAccountOperationSelected(null);
			if(registerAccountAssignmentTO.isBlOperationRepo() && 
					Validations.validateIsNotNullAndNotEmpty(registerAccountAssignmentTO.getHolderAccountOperationRepo()) && 
					ComponentConstant.SALE_ROLE.equals(registerAccountAssignmentTO.getHolderAccountOperation().getRole())){
				
				registerAccountAssignmentTO.getHolderAccountOperation().setHolderAccount(registerAccountAssignmentTO.getHolderAccountOperationRepo().getHolderAccount());
				registerAccountAssignmentTO.getHolderAccountOperation().setAccountOperationMarketFacts(
						registerAccountAssignmentTO.getHolderAccountOperationRepo().getAccountOperationMarketFacts());
			}
			if(Validations.validateIsNullOrEmpty(registerAccountAssignmentTO.getHolderAccountOperation().getHolderAccount())
					|| Validations.validateIsNullOrEmpty(registerAccountAssignmentTO.getHolderAccountOperation().getHolderAccount().getIdHolderAccountPk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			// validate if its same participant
			if(registerAccountAssignmentTO.getHolderAccountOperation().getIsSelected()
					&& registerAccountAssignmentTO.getParticipantOrigin().equals(registerAccountAssignmentTO.getHolderAccountOperation().getInchargeStockParticipant().getIdParticipantPk())
					&& registerAccountAssignmentTO.getParticipantOrigin().equals(registerAccountAssignmentTO.getHolderAccountOperation().getInchargeFundsParticipant().getIdParticipantPk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_SAME_PARTICIPANTS));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			String validatePrimaryPlace = NegotiationUtils.validateQuantityNegotitation(
					registerAccountAssignmentTO.getMechanismOperation().getIndPrimaryPlacement(),
					registerAccountAssignmentTO.getMechanismOperation().getSecurities(),
					registerAccountAssignmentTO.getHolderAccountOperation().getStockQuantity());
			HolderAccountOperation holderAccountOperation = null;
			if(Validations.validateIsNotNullAndNotEmpty(validatePrimaryPlace)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(validatePrimaryPlace));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(registerAccountAssignmentTO.getLstHolderAccountOperation()!=null){
				manageHolderAccountAssignmentFacade.validateAssignmentConditions(registerAccountAssignmentTO.getHolderAccountOperation(), 
						registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList());
			}
			
			if(!validateQuantity(registerAccountAssignmentTO.getHolderAccountOperation().getStockQuantity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_EXCEEDED_QUANTITY));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}	
				
			String keyInchargeRule = new StringBuilder(ComponentConstant.ZERO).append(ComponentConstant.ZERO).append(ComponentConstant.ZERO).toString();
			if(registerAccountAssignmentTO.getHolderAccountOperation().getIsSelected()){
				Map<String, String> mpInchargeRule = manageHolderAccountAssignmentFacade.getInchargeRules(registerAccountAssignmentTO.getHolderAccountOperation().getRole());					
				if(registerAccountAssignmentTO.getParticipantOrigin().equals(registerAccountAssignmentTO.getHolderAccountOperation().getInchargeStockParticipant().getIdParticipantPk())){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ONE).append(ComponentConstant.ONE).append(ComponentConstant.ZERO).toString();
				}else if(registerAccountAssignmentTO.getParticipantOrigin().equals(registerAccountAssignmentTO.getHolderAccountOperation().getInchargeFundsParticipant().getIdParticipantPk())){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ONE).append(ComponentConstant.ZERO).append(ComponentConstant.ONE).toString();
				}else if(registerAccountAssignmentTO.getHolderAccountOperation().getInchargeStockParticipant().getIdParticipantPk().equals(registerAccountAssignmentTO.getHolderAccountOperation().getInchargeFundsParticipant().getIdParticipantPk())){
					keyInchargeRule = new StringBuilder().append(ComponentConstant.ZERO).append(ComponentConstant.ONE).append(ComponentConstant.ONE).toString();
				}
				if(Validations.validateIsNullOrEmpty(mpInchargeRule.get(keyInchargeRule))){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_INCHARGE_COMBINATION));
					updateInchargeAgreement();
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			
			try {
				holderAccountOperation = registerAccountAssignmentTO.getHolderAccountOperation().clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			holderAccountOperation.setHolderAccountStateDesc(AssignmentRequestStateType.PENDING.getDescripcion());
			if(holderAccountOperation.getIsSelected()){
				holderAccountOperation.setIndIncharge(BooleanType.YES.getCode());
			}else{
				holderAccountOperation.setIndIncharge(BooleanType.NO.getCode());
			}			
			holderAccountOperation.setIsSelected(false);
			registerAccountAssignmentTO.setBlInchargeAgreementDisabled(false);
			holderAccountOperation.setInchargeStockParticipant(manageHolderAccountAssignmentFacade.getParticipant(holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk()));
			holderAccountOperation.setInchargeFundsParticipant(manageHolderAccountAssignmentFacade.getParticipant(holderAccountOperation.getInchargeFundsParticipant().getIdParticipantPk()));					
			if(registerAccountAssignmentTO.isBlCrossOperation()){
				if(ParticipantRoleType.BUY.getCode().equals(holderAccountOperation.getRole())){
					updateQuantityAssig(true, holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount());
				}else{
					updateCrossQuantityAssig(true, holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount());
				}
			}else{
				updateQuantityAssig(true, holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount());
			}
			
			if(Validations.validateIsNull(registerAccountAssignmentTO.getLstHolderAccountOperation())){
				registerAccountAssignmentTO.setLstHolderAccountOperation(new GenericDataModel<HolderAccountOperation>());
			}
			
			registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList().add(holderAccountOperation);
			
			registerAccountAssignmentTO.setHolder(new Holder());
			registerAccountAssignmentTO.setLstHolderAccount(null);
			registerAccountAssignmentTO.getHolderAccountOperation().setHolderAccount(null);
			if(registerAccountAssignmentTO.isBlMarketFact() && ComponentConstant.SALE_ROLE.equals(registerAccountAssignmentTO.getHolderAccountOperation().getRole())){
				registerAccountAssignmentTO.setHolAccOpeMarketFact(new AccountOperationMarketFact());
			}
			registerAccountAssignmentTO.getHolderAccountOperation().setStockQuantity(null);
			registerAccountAssignmentTO.getHolderAccountOperation().setCashAmount(null);
			registerAccountAssignmentTO.getHolderAccountOperation().setSettlementAmount(null);
			registerAccountAssignmentTO.getHolderAccountOperation().setInchargeFundsParticipant(new Participant(registerAccountAssignmentTO.getParticipantOrigin()));
			registerAccountAssignmentTO.getHolderAccountOperation().setInchargeStockParticipant(new Participant(registerAccountAssignmentTO.getParticipantOrigin()));
			registerAccountAssignmentTO.getHolderAccountOperation().setIsSelected(false);
			registerAccountAssignmentTO.getHolderAccountOperation().setAccountOperationMarketFacts(null);
			registerAccountAssignmentTO.setBlInchargeAgreementDisabled(false);
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Show market fact.
	 *
	 * @param holderAccountOperation the holder account operation
	 */
	public void showMarketFact(HolderAccountOperation holderAccountOperation){
		marketFactAssignmentMgmtBean.setViewOnly(false);
		marketFactAssignmentMgmtBean.setMaxQuantity(holderAccountOperation.getStockQuantity());
		marketFactAssignmentMgmtBean.setRemoteCommand("onCloseMarketFact");
		
		if(holderAccountOperation.getIdHolderAccountOperationPk()!=null){
			marketFactAssignmentMgmtBean.setViewOnly(true);
		}
		
		marketFactAssignmentMgmtBean.setLstAccountMarketfacts(NegotiationUtils.convertToMarketFactAccounts(holderAccountOperation.getAccountOperationMarketFacts()));
		if(InstrumentType.FIXED_INCOME.getCode().equals(registerAccountAssignmentTO.getMechanismOperation().getSecurities().getInstrumentType())){
			marketFactAssignmentMgmtBean.setFixedIncome(true);
		}
		marketFactAssignmentMgmtBean.showMarketFact();
		registerAccountAssignmentTO.setSelectedHolderAccountOperation(holderAccountOperation);
	}
	
	/**
	 * On market fact close.
	 */
	public void onMarketFactClose(){
		registerAccountAssignmentTO.getSelectedHolderAccountOperation().setAccountOperationMarketFacts(
				NegotiationUtils.convertToSettlementAccountMarketFacts(marketFactAssignmentMgmtBean.getLstAccountMarketfacts(),registerAccountAssignmentTO.getSelectedHolderAccountOperation()));
		registerAccountAssignmentTO.setSelectedHolderAccountOperation(null);
	}

	/**
	 * Show edit market fact.
	 *
	 * @param holderAccountOperation the holder account operation
	 */
	public void showEditMarketFact(HolderAccountOperation holderAccountOperation){
		JSFUtilities.executeJavascriptFunction("PF('dlgWEditMarketFact').show();");
		registerAccountAssignmentTO.getHolAccOpeMarketFact().setHolderAccountOperation(holderAccountOperation);
		registerAccountAssignmentTO.setBlFixedIncome(false);
		if(InstrumentType.FIXED_INCOME.getCode().equals(registerAccountAssignmentTO.getMechanismOperation().getSecurities().getInstrumentType())){
			registerAccountAssignmentTO.setBlFixedIncome(true);
		}
	}

	/**
	 * Before remove account.
	 */
	public void beforeRemoveAccount(){
		HolderAccountOperation[] holderAccountOperations = registerAccountAssignmentTO.getHolderAccountOperationSelected();
 		if(Validations.validateIsNotNull(holderAccountOperations) && holderAccountOperations.length > 0){
 			List<String> accountsNumber = new ArrayList<String>();
 			for(int i=0;i<holderAccountOperations.length;i++){
 				
				Long inchargeState = manageHolderAccountAssignmentFacade.getInchargeState(holderAccountOperations[i].getIdHolderAccountOperationPk());
				if(Validations.validateIsNotNullAndNotEmpty(inchargeState) &&
						HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode().equals(new Integer(inchargeState.toString()))){
					Object[] bodyData = new Object[2];
					bodyData[0] = holderAccountOperations[i].getHolderAccount().getAccountNumber().toString();
					bodyData[1] = ParticipantRoleType.get(holderAccountOperations[i].getRole()).getValue();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INCHARGE_CONFIRMED, bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				String accNumber = holderAccountOperations[i].getHolderAccount().getAccountNumber().toString();
 				accountsNumber.add(accNumber);
			}
			blRemove = true;
			blSave = false;
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_ASK_REMOVE, 
							new Object[]{StringUtils.join(accountsNumber.toArray(),GeneralConstants.STR_COMMA_WITHOUT_SPACE)}));			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_REMOVE_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}		
	}
	
	/**
	 * Before save assignment.
	 */
	public void beforeSaveAssignment(){
		String headerMessage = null, bodyMessage = null;
		
		if(BigDecimal.ZERO.equals(registerAccountAssignmentTO.getQuantityAssig()) && BigDecimal.ZERO.equals(registerAccountAssignmentTO.getCrossQuantityAssig())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_INVALID_QUANTITY_OPERATION));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(registerAccountAssignmentTO.isBlCrossOperation()){
			if(registerAccountAssignmentTO.getQuantityAssig().compareTo(registerAccountAssignmentTO.getMechanismOperation().getStockQuantity()) != 0
					|| registerAccountAssignmentTO.getCrossQuantityAssig().compareTo(registerAccountAssignmentTO.getMechanismOperation().getStockQuantity()) != 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SAME_QUANTITY_OPERATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}else{
			if(registerAccountAssignmentTO.getQuantityAssig().compareTo(registerAccountAssignmentTO.getMechanismOperation().getStockQuantity()) != 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_NOT_SAME_QUANTITY_OPERATION));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		if(registerAccountAssignmentTO.isBlMarketFact()){
			for(HolderAccountOperation holderAccountOperation:registerAccountAssignmentTO.getLstHolderAccountOperation()){
				if(ComponentConstant.SALE_ROLE.equals(holderAccountOperation.getRole()) &&
						!HolderAccountOperationStateType.CONFIRMED.getCode().equals(holderAccountOperation.getHolderAccountState())){
					if(Validations.validateListIsNullOrEmpty(holderAccountOperation.getAccountOperationMarketFacts())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY, 
																holderAccountOperation.getHolderAccount().getAccountNumber().toString()));
						JSFUtilities.showSimpleValidationDialog();
						return;					
					}
					
					BigDecimal total = BigDecimal.ZERO;
					for(AccountOperationMarketFact accMktFact : holderAccountOperation.getAccountOperationMarketFacts()){
						total = total.add(accMktFact.getOperationQuantity() != null ? accMktFact.getOperationQuantity() : BigDecimal.ZERO);
					}
					if(total.compareTo(holderAccountOperation.getStockQuantity()) != 0){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_EMPTY, 
																holderAccountOperation.getHolderAccount().getAccountNumber().toString()));
						JSFUtilities.showSimpleValidationDialog();
						return;	
					}
				}
			}
		}
		
		blRemove = false;
		blSave = true;
		if(blPending){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REGISTER;
			bodyMessage = PropertiesConstants.ASSIGNMENT_ACCOUNT_ASK_REGISTER;
		}else if(blRegistered){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_MODIFY;
			bodyMessage = PropertiesConstants.ASSIGNMENT_ACCOUNT_ASK_MODIFY;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage),
				PropertiesUtilities.getMessage(bodyMessage));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");					
	}
	
	/**
	 * Do action assignment.
	 */
	@LoggerAuditWeb
	public void doActionAssignment(){
		try {
			if(blSave){
				saveAssignment();
			}else if(blRemove){
				removeAccount();
			}
		} catch (ServiceException e) {
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
								, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		} catch (AccountAssignmentException e) {
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, e.getMessage());
		}
	}
	
	/**
	 * Before confirm assignment.
	 */
	public void beforeConfirmAssignment(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_ASK_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	/**
	 * Confirm assignment.
	 */
	@LoggerAuditWeb
	public void confirmAssignment(){
		try {
			manageHolderAccountAssignmentFacade.confirmAssignment(searchAccountAssignmentTO.getAssignmentProcessHeader(), searchAccountAssignmentTO.getParticipantAssignmentHeader(),
													registerAccountAssignmentTO);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_SUCCESS_CONFIRM, 
										new Object[]{registerAccountAssignmentTO.getMechanismOperation().getOperationNumber().toString()}));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			monitorAssignmentProcess();
			for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getLstAssignmentProcess()){
				if(searchAccountAssignmentTO.getAssignmentProcessHeader().equals(assignmentProcessTO.getIdAssignmentProcessPk())){
					showParticipantAssignment(assignmentProcessTO);
					break;
				}					
			}
		} catch (ServiceException e) {
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
						, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}				
	}
	
	/**
	 * Removes the account.
	 */
	private void removeAccount(){		
		for(int i=0;i<registerAccountAssignmentTO.getHolderAccountOperationSelected().length;i++){
			HolderAccountOperation holderAccountOperationSel = registerAccountAssignmentTO.getHolderAccountOperationSelected()[i];
			if(registerAccountAssignmentTO.isBlCrossOperation()){
				if(ParticipantRoleType.BUY.getCode().equals(holderAccountOperationSel.getRole()))
					updateQuantityAssig(false, holderAccountOperationSel.getStockQuantity(), holderAccountOperationSel.getSettlementAmount());
				else
					updateCrossQuantityAssig(false, holderAccountOperationSel.getStockQuantity(), holderAccountOperationSel.getSettlementAmount());
			}else{
				updateQuantityAssig(false, holderAccountOperationSel.getStockQuantity(), holderAccountOperationSel.getSettlementAmount());
			}
			
			if(registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList().contains(holderAccountOperationSel)){
				boolean removed = registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList().remove(holderAccountOperationSel);
				
				if(removed && holderAccountOperationSel.getIdHolderAccountOperationPk() != null){
					registerAccountAssignmentTO.getLstHolderAccountOperationRemoved().add(holderAccountOperationSel);
				}
			}
		}
		
		if(registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList().isEmpty()){
			registerAccountAssignmentTO.setLstHolderAccountOperation(null);
		}
		
		if(registerAccountAssignmentTO.getHolderAccountOperationSelected().length > 1){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNTS));
		} else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_SUCCESS_REMOVE_ACCOUNT));
		}
			
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog2').show()");
	}

	/**
	 * Save assignment.
	 *
	 * @throws ServiceException the service exception
	 * @throws AccountAssignmentException 
	 */
	private void saveAssignment() throws ServiceException, AccountAssignmentException{
		registerAccountAssignmentTO.setHolderAccountOperationRepo(null);
		registerAccountAssignmentTO.setLstHolderAccountOperationRepo(null);
		manageHolderAccountAssignmentFacade.saveAssignment(searchAccountAssignmentTO.getAssignmentProcessHeader(),
															searchAccountAssignmentTO.getParticipantAssignmentHeader(),
															registerAccountAssignmentTO);
		String bodyMessage = null;
		if(blPending){
			bodyMessage = PropertiesConstants.ASSIGNMENT_ACCOUNT_SUCCESS_REGISTER;
		}else if(blRegistered){
			bodyMessage = PropertiesConstants.ASSIGNMENT_ACCOUNT_SUCCESS_MODIFY;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(bodyMessage, 
						new Object[]{registerAccountAssignmentTO.getMechanismOperation().getOperationNumber().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		monitorAssignmentProcess();
		for(AssignmentProcessTO assignmentProcessTO:searchAccountAssignmentTO.getLstAssignmentProcess()){
			if(searchAccountAssignmentTO.getAssignmentProcessHeader().equals(assignmentProcessTO.getIdAssignmentProcessPk())){
				showParticipantAssignment(assignmentProcessTO);
				break;
			}					
		}
	}
	
	/**
	 * Dialogo de confirmacion de bloqueo
	 * */
	public void showDialogConfimPurchaseProcess(){
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogBlock').show();");
	}

	/**
	 * Execute sale purchase process.
	 */
	@LoggerAuditWeb
	public void executeSalePurchaseProcess(){
		try {
			
			BusinessProcess objBusinessProcess = new BusinessProcess();
			objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode());
			Map<String, Object> processParameters=new HashMap<String, Object>();
			processParameters.put(ComponentConstant.SETTLEMENT_SCHEMA, SettlementSchemaType.NET.getCode());
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), objBusinessProcess, processParameters);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
							, PropertiesUtilities.getMessage(PropertiesConstants.EXECUTED_PROCESS_SUCCESFUL));
			JSFUtilities.showSimpleValidationDialog();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fill combos.
	 */
	private void fillCombos(){
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(GeneralConstants.ZERO_VALUE_INTEGER);
		paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.operationDate"));
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(GeneralConstants.ONE_VALUE_INTEGER);
		paramTab.setParameterName(PropertiesUtilities.getMessage("msg.operation.search.cashSettlementDate"));
		lstTemp.add(paramTab);
		searchAccountAssignmentTO.setLstDateType(lstTemp);
		
		lstTemp = new ArrayList<ParameterTable>();
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
		paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
		paramTab.setParameterName(SettlementSchemaType.NET.getValue());
		lstTemp.add(paramTab);
		searchAccountAssignmentTO.setLstSettlementSchemaType(lstTemp);
		
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ComponentConstant.ONE);
		try {
			parameterTableTO.setMasterTableFk(MasterTableType.ASSIGNMENT_PROCESS_STATE.getCode());
			searchAccountAssignmentTO.setLstAssignmentProcessState(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
			
			parameterTableTO.setMasterTableFk(MasterTableType.PARTICIPANT_ASSIGNMENT_PROCESS_STATE.getCode());
			searchAccountAssignmentTO.setLstParticipantAssignmentState(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Fill mechanism operation.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @throws ServiceException the service exception
	 */
	private void fillMechanismOperation(MechanismOperationTO mechanismOperationTO) throws ServiceException{
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(new Integer(NegotiationTypeType.CROSS_NEGOTIATION.getCode().toString()));
		paramTab.setParameterName(NegotiationTypeType.CROSS_NEGOTIATION.getDescripcion());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(new Integer(NegotiationTypeType.NORMAL_NEGOTIATION.getCode().toString()));
		paramTab.setParameterName(NegotiationTypeType.NORMAL_NEGOTIATION.getDescripcion());
		lstTemp.add(paramTab);
		registerAccountAssignmentTO.setLstOperationType(lstTemp);
		lstTemp = new ArrayList<ParameterTable>();
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(ParticipantRoleType.BUY.getCode());
		paramTab.setParameterName(ParticipantRoleType.BUY.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(ParticipantRoleType.SELL.getCode());
		paramTab.setParameterName(ParticipantRoleType.SELL.getValue());
		lstTemp.add(paramTab);
		registerAccountAssignmentTO.setLstRoles(lstTemp);
		lstTemp = new ArrayList<ParameterTable>();
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
		paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
		paramTab.setParameterName(SettlementSchemaType.NET.getValue());
		lstTemp.add(paramTab);
		registerAccountAssignmentTO.setLstSettlementSchema(lstTemp);
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		//MT DE ESTADOS
		paramTabTO.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
		registerAccountAssignmentTO.setLstOperationState(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));		
		registerAccountAssignmentTO.setBlInchargeDisabled(true);		
		registerAccountAssignmentTO.setMechanismOperation(manageHolderAccountAssignmentFacade.getMechanismOperation(mechanismOperationTO.getIdMechanismOperationPk()));
		
		boolean blExistTrader = false;
		
		Integer primaryPlacement = registerAccountAssignmentTO.getMechanismOperation().getIndPrimaryPlacement();
		if(ComponentConstant.ONE.equals(primaryPlacement)){
			registerAccountAssignmentTO.setBlPrimaryPlacement(true);
		}
		
		Long mechanismId = registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk();
		Long modalityId = registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk();
		
		List<Participant> lstResult = manageHolderAccountAssignmentFacade.getListParticipantsForNegoModality(mechanismId, modalityId, null ,ComponentConstant.ONE);
		if(!blConfirmed){		
			for(Participant participant:lstResult){
				if(registerAccountAssignmentTO.getParticipantOrigin().equals(participant.getIdParticipantPk())){
					blExistTrader = true;
					break;
				}				
			}
			if(!blExistTrader){
				lstResult.add(manageHolderAccountAssignmentFacade.getParticipant(registerAccountAssignmentTO.getParticipantOrigin()));
			}
		}
		registerAccountAssignmentTO.setLstInchargeParticipants(lstResult);		
		if(mechanismOperationTO.getIdPurchaseParticipant().equals(mechanismOperationTO.getIdSaleParticipant())){
			registerAccountAssignmentTO.setOperationTypeSelected(new Integer(NegotiationTypeType.CROSS_NEGOTIATION.getCode().toString()));
			registerAccountAssignmentTO.setBlCrossOperation(true);
		}else{
			registerAccountAssignmentTO.setOperationTypeSelected(new Integer(NegotiationTypeType.NORMAL_NEGOTIATION.getCode().toString()));
		}
		if(ComponentConstant.ONE.equals(registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getIndIncharge())){
			registerAccountAssignmentTO.setBlInchargeDisabled(false);
			registerAccountAssignmentTO.setBlInchargeAgreementDisabled(false);
		}
					
	}

	/**
	 * Fill holder account operations.
	 *
	 * @param mechanismOperationTO the mechanism operation to
	 * @throws ServiceException the service exception
	 */
	private void fillHolderAccountOperations(MechanismOperationTO mechanismOperationTO) throws ServiceException{
		List<Integer> lstRoles = new ArrayList<Integer>();
		if(registerAccountAssignmentTO.isBlCrossOperation()){
			lstRoles.add(ComponentConstant.PURCHARSE_ROLE);
			lstRoles.add(ComponentConstant.SALE_ROLE);
		}else{
			lstRoles.add(mechanismOperationTO.getRole());
		}
		List<Integer> lstStates = new ArrayList<Integer>();
		if(blConfirmed){
			lstStates.add(HolderAccountOperationStateType.CONFIRMED.getCode());
		}else{
			lstStates.add(HolderAccountOperationStateType.CONFIRMED.getCode());
			lstStates.add(HolderAccountOperationStateType.REGISTERED.getCode());
		}
		
		List<HolderAccountOperation> lstResult = manageHolderAccountAssignmentFacade.getHolderAccountOperations(
				mechanismOperationTO.getIdMechanismOperationPk(), searchAccountAssignmentTO.getParticipantHeader(), lstRoles, lstStates, blDepositary);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			for(HolderAccountOperation holderAccountOperation :lstResult){
				holderAccountOperation.setHolderAccountStateDesc(mapParameters.get(holderAccountOperation.getHolderAccountState()));	
				holderAccountOperation.getHolderAccount().setAccountDescription(mapParameters.get(holderAccountOperation.getHolderAccount().getAccountType()));
				if(registerAccountAssignmentTO.isBlCrossOperation()){
					if(ParticipantRoleType.BUY.getCode().equals(holderAccountOperation.getRole()))
						updateQuantityAssig(true, holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount());
					else
						updateCrossQuantityAssig(true, holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount());
				}else{
					updateQuantityAssig(true, holderAccountOperation.getStockQuantity(), holderAccountOperation.getSettlementAmount());
				}
				if(holderAccountOperation.getHolderAccountState().equals(HolderAccountOperationStateType.CONFIRMED.getCode())){
					holderAccountOperation.setIsSelected(true);
				}
			}
			registerAccountAssignmentTO.setLstHolderAccountOperation(new GenericDataModel<HolderAccountOperation>(lstResult));				
		}
	}
	
	/**
	 * Fill holder account operation repo.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @throws ServiceException the service exception
	 */
	private void fillHolderAccountOperationRepo(Long idMechanismOperationPk) throws ServiceException{
		registerAccountAssignmentTO.setHolderAccountOperationRepo(null);
		registerAccountAssignmentTO.setLstHolderAccountOperationRepo(new GenericDataModel<HolderAccountOperation>(manageHolderAccountAssignmentFacade.getHolderAccountOperationsRepo(idMechanismOperationPk)));
		for(HolderAccountOperation holderAccountOperation:registerAccountAssignmentTO.getLstHolderAccountOperationRepo()){
			if(holderAccountOperation.getAccountOperationMarketFacts()!=null){
				for(AccountOperationMarketFact accountOperationMarketFact : holderAccountOperation.getAccountOperationMarketFacts()){
					accountOperationMarketFact.setIdAccountOperMarketFactPk(null);
					accountOperationMarketFact.setOriginalQuantity(accountOperationMarketFact.getOriginalQuantity().add(accountOperationMarketFact.getOperationQuantity()));
					accountOperationMarketFact.setOperationQuantity(BigDecimal.ZERO);
					accountOperationMarketFact.setHolderAccountOperation(null);
				}
			}
			HolderAccount foundAccount = manageHolderAccountAssignmentFacade.findHolderAccount(holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
			foundAccount.setAccountDescription(mapParameters.get(foundAccount.getAccountType()));
			holderAccountOperation.setHolderAccount(foundAccount);
		}
	}
	
	/**
	 * Update quantity assig.
	 *
	 * @param blAction the bl action
	 * @param quantity the quantity
	 * @param amount the amount
	 */
	private void updateQuantityAssig(boolean blAction, BigDecimal quantity, BigDecimal amount){
		if(blAction){
			registerAccountAssignmentTO.setQuantityAssig(registerAccountAssignmentTO.getQuantityAssig().add(quantity).setScale(2, RoundingMode.HALF_UP));
			registerAccountAssignmentTO.setAmountAssig(registerAccountAssignmentTO.getAmountAssig().add(amount).setScale(2, RoundingMode.HALF_UP));
		}else{
			registerAccountAssignmentTO.setQuantityAssig(registerAccountAssignmentTO.getQuantityAssig().subtract(quantity).setScale(2, RoundingMode.HALF_UP));
			registerAccountAssignmentTO.setAmountAssig(registerAccountAssignmentTO.getAmountAssig().subtract(amount).setScale(2, RoundingMode.HALF_UP));
		}
	}
	
	/**
	 * Update cross quantity assig.
	 *
	 * @param blAction the bl action
	 * @param quantity the quantity
	 * @param amount the amount
	 */
	private void updateCrossQuantityAssig(boolean blAction, BigDecimal quantity, BigDecimal amount){
		if(blAction){
			registerAccountAssignmentTO.setCrossQuantityAssig(registerAccountAssignmentTO.getCrossQuantityAssig().add(quantity).setScale(2, RoundingMode.HALF_UP));
			registerAccountAssignmentTO.setCrossAmountAssig(registerAccountAssignmentTO.getCrossAmountAssig().add(amount).setScale(2, RoundingMode.HALF_UP));
		}else{
			registerAccountAssignmentTO.setCrossQuantityAssig(registerAccountAssignmentTO.getCrossQuantityAssig().subtract(quantity).setScale(2, RoundingMode.HALF_UP));
			registerAccountAssignmentTO.setCrossAmountAssig(registerAccountAssignmentTO.getCrossAmountAssig().subtract(amount).setScale(2, RoundingMode.HALF_UP));
		}
	}
	
	/**
	 * Validate quantity.
	 *
	 * @param stockQuantity the stock quantity
	 * @return true, if successful
	 */
	private boolean validateQuantity(BigDecimal stockQuantity){
		BigDecimal totalQuantity = BigDecimal.ZERO;
		if(registerAccountAssignmentTO.isBlCrossOperation()){
			if(ParticipantRoleType.BUY.getCode().equals(registerAccountAssignmentTO.getHolderAccountOperation().getRole()))
				totalQuantity = registerAccountAssignmentTO.getQuantityAssig().add(stockQuantity);
			else
				totalQuantity = registerAccountAssignmentTO.getCrossQuantityAssig().add(stockQuantity);
		}else{
			totalQuantity = registerAccountAssignmentTO.getQuantityAssig().add(stockQuantity);
		}
		if(totalQuantity.compareTo(registerAccountAssignmentTO.getMechanismOperation().getStockQuantity()) == 1)
			return false;
		else 
			return true;		
	}
	
	/**
	 * Gets the cui holder.
	 *
	 * @param holderAccount the holder account
	 * @return the cui holder
	 */
	public static String getCuiHolder(HolderAccount holderAccount){
		return NegotiationUtils.getCuiHolder(holderAccount);
	}
	
	/**
	 * Gets the search account assignment to.
	 *
	 * @return the search account assignment to
	 */
	public SearchAccountAssignmentTO getSearchAccountAssignmentTO() {
		return searchAccountAssignmentTO;
	}
	
	/**
	 * Sets the search account assignment to.
	 *
	 * @param searchAccountAssignmentTO the new search account assignment to
	 */
	public void setSearchAccountAssignmentTO(
			SearchAccountAssignmentTO searchAccountAssignmentTO) {
		this.searchAccountAssignmentTO = searchAccountAssignmentTO;
	}
	
	/**
	 * Checks if is bl depositary.
	 *
	 * @return true, if is bl depositary
	 */
	public boolean isBlDepositary() {
		return blDepositary;
	}
	
	/**
	 * Sets the bl depositary.
	 *
	 * @param blDepositary the new bl depositary
	 */
	public void setBlDepositary(boolean blDepositary) {
		this.blDepositary = blDepositary;
	}
	
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Checks if is bl pending.
	 *
	 * @return true, if is bl pending
	 */
	public boolean isBlPending() {
		return blPending;
	}
	
	/**
	 * Sets the bl pending.
	 *
	 * @param blPending the new bl pending
	 */
	public void setBlPending(boolean blPending) {
		this.blPending = blPending;
	}
	
	/**
	 * Checks if is bl registered.
	 *
	 * @return true, if is bl registered
	 */
	public boolean isBlRegistered() {
		return blRegistered;
	}
	
	/**
	 * Sets the bl registered.
	 *
	 * @param blRegistered the new bl registered
	 */
	public void setBlRegistered(boolean blRegistered) {
		this.blRegistered = blRegistered;
	}
	
	/**
	 * Checks if is bl confirmed.
	 *
	 * @return true, if is bl confirmed
	 */
	public boolean isBlConfirmed() {
		return blConfirmed;
	}
	
	/**
	 * Sets the bl confirmed.
	 *
	 * @param blConfirmed the new bl confirmed
	 */
	public void setBlConfirmed(boolean blConfirmed) {
		this.blConfirmed = blConfirmed;
	}
	
	/**
	 * Checks if is bl assignment process closed.
	 *
	 * @return true, if is bl assignment process closed
	 */
	public boolean isBlAssignmentProcessClosed() {
		return blAssignmentProcessClosed;
	}
	
	/**
	 * Sets the bl assignment process closed.
	 *
	 * @param blAssignmentProcessClosed the new bl assignment process closed
	 */
	public void setBlAssignmentProcessClosed(boolean blAssignmentProcessClosed) {
		this.blAssignmentProcessClosed = blAssignmentProcessClosed;
	}
	
	/**
	 * Checks if is bl participant assignment closed.
	 *
	 * @return true, if is bl participant assignment closed
	 */
	public boolean isBlParticipantAssignmentClosed() {
		return blParticipantAssignmentClosed;
	}
	
	/**
	 * Sets the bl participant assignment closed.
	 *
	 * @param blParticipantAssignmentClosed the new bl participant assignment closed
	 */
	public void setBlParticipantAssignmentClosed(
			boolean blParticipantAssignmentClosed) {
		this.blParticipantAssignmentClosed = blParticipantAssignmentClosed;
	}
	
	/**
	 * Checks if is bl close assignment process.
	 *
	 * @return true, if is bl close assignment process
	 */
	public boolean isBlCloseAssignmentProcess() {
		return blCloseAssignmentProcess;
	}
	
	/**
	 * Sets the bl close assignment process.
	 *
	 * @param blCloseAssignmentProcess the new bl close assignment process
	 */
	public void setBlCloseAssignmentProcess(boolean blCloseAssignmentProcess) {
		this.blCloseAssignmentProcess = blCloseAssignmentProcess;
	}
	
	/**
	 * Checks if is bl close participant assignment.
	 *
	 * @return true, if is bl close participant assignment
	 */
	public boolean isBlCloseParticipantAssignment() {
		return blCloseParticipantAssignment;
	}
	
	/**
	 * Sets the bl close participant assignment.
	 *
	 * @param blCloseParticipantAssignment the new bl close participant assignment
	 */
	public void setBlCloseParticipantAssignment(boolean blCloseParticipantAssignment) {
		this.blCloseParticipantAssignment = blCloseParticipantAssignment;
	}
	
	/**
	 * Checks if is bl open participant assignment.
	 *
	 * @return true, if is bl open participant assignment
	 */
	public boolean isBlOpenParticipantAssignment() {
		return blOpenParticipantAssignment;
	}
	
	/**
	 * Sets the bl open participant assignment.
	 *
	 * @param blOpenParticipantAssignment the new bl open participant assignment
	 */
	public void setBlOpenParticipantAssignment(boolean blOpenParticipantAssignment) {
		this.blOpenParticipantAssignment = blOpenParticipantAssignment;
	}

	/**
	 * Gets the register account assignment to.
	 *
	 * @return the register account assignment to
	 */
	public RegisterAccountAssignmentTO getRegisterAccountAssignmentTO() {
		return registerAccountAssignmentTO;
	}
	
	/**
	 * Sets the register account assignment to.
	 *
	 * @param registerAccountAssignmentTO the new register account assignment to
	 */
	public void setRegisterAccountAssignmentTO(
			RegisterAccountAssignmentTO registerAccountAssignmentTO) {
		this.registerAccountAssignmentTO = registerAccountAssignmentTO;
	}
	
	/**
	 * Checks if is bl save.
	 *
	 * @return true, if is bl save
	 */
	public boolean isBlSave() {
		return blSave;
	}
	
	/**
	 * Sets the bl save.
	 *
	 * @param blSave the new bl save
	 */
	public void setBlSave(boolean blSave) {
		this.blSave = blSave;
	}
	
	/**
	 * Checks if is bl remove.
	 *
	 * @return true, if is bl remove
	 */
	public boolean isBlRemove() {
		return blRemove;
	}
	
	/**
	 * Sets the bl remove.
	 *
	 * @param blRemove the new bl remove
	 */
	public void setBlRemove(boolean blRemove) {
		this.blRemove = blRemove;
	}
	
	/**
	 * Checks if is bl assignment expired.
	 *
	 * @return true, if is bl assignment expired
	 */
	public boolean isBlAssignmentExpired() {
		return blAssignmentExpired;
	}
	
	/**
	 * Sets the bl assignment expired.
	 *
	 * @param blAssignmentExpired the new bl assignment expired
	 */
	public void setBlAssignmentExpired(boolean blAssignmentExpired) {
		this.blAssignmentExpired = blAssignmentExpired;
	}

	/**
	 * Gets the negotiation modalities chained.
	 *
	 * @return the negotiationModalitiesChained
	 */
	public String getNegotiationModalitiesChained() {
		return negotiationModalitiesChained;
	}

	/**
	 * Sets the negotiation modalities chained.
	 *
	 * @param negotiationModalitiesChained the negotiationModalitiesChained to set
	 */
	public void setNegotiationModalitiesChained(String negotiationModalitiesChained) {
		this.negotiationModalitiesChained = negotiationModalitiesChained;
	}

	/**
	 * Gets the mechanism name chained.
	 *
	 * @return the mechanismNameChained
	 */
	public String getMechanismNameChained() {
		return mechanismNameChained;
	}

	/**
	 * Sets the mechanism name chained.
	 *
	 * @param mechanismNameChained the mechanismNameChained to set
	 */
	public void setMechanismNameChained(String mechanismNameChained) {
		this.mechanismNameChained = mechanismNameChained;
	}

	public Boolean getMandatoryInChargeParticipant() {
		return mandatoryInChargeParticipant;
	}

	public void setMandatoryInChargeParticipant(Boolean mandatoryInChargeParticipant) {
		this.mandatoryInChargeParticipant = mandatoryInChargeParticipant;
	}
	
	
	
}