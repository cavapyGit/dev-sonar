package com.pradera.negotiations.accountassignment.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParticipantAssignmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class ParticipantAssignmentTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The id participant assignment pk. */
	private Long idParticipantAssignmentPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant desc. */
	private String participantDesc;
	
	/** The participant full desc. */
	private String participantFullDesc;
	
	/** The confirmed operations. */
	private BigDecimal totalOperations, pendingOperations, registeredOperations, confirmedOperations;
	
	/** The state. */
	private Integer state;
	
	/** The state description. */
	private String stateDescription;
	
	/** The lst confirmed mechanism operation. */
	private List<MechanismOperationTO> lstPendingMechanismOperation, lstRegisteredMechanismOperation, lstConfirmedMechanismOperation;
	
	/** The checked market facts. */
	private boolean checkedMarketFacts;
	
	/** The checked sale pending assgn. */
	private boolean checkedSalePendingAssgn;
	
	/** The checked purchase pending assgn. */
	private boolean checkedPurchasePendingAssgn;
	
	private boolean indOperationsWithNoSecurities=false;
	
	private Integer cantOperationsWithNoSecurities=0;
	
	/** issue 1105: lista que almacena las operaciones con valores faltantes */
	private List<OperationWithNoSecuritiesTO> lstOperationWithNoSecurities;
	
	/**
	 * Checks if is checked market facts.
	 *
	 * @return true, if is checked market facts
	 */
	public boolean isCheckedMarketFacts() {
		return checkedMarketFacts;
	}
	
	/**
	 * Sets the checked market facts.
	 *
	 * @param checkedMarketFacts the new checked market facts
	 */
	public void setCheckedMarketFacts(boolean checkedMarketFacts) {
		this.checkedMarketFacts = checkedMarketFacts;
	}
	
	public List<OperationWithNoSecuritiesTO> getLstOperationWithNoSecurities() {
		return lstOperationWithNoSecurities;
	}

	public void setLstOperationWithNoSecurities(List<OperationWithNoSecuritiesTO> lstOperationWithNoSecurities) {
		this.lstOperationWithNoSecurities = lstOperationWithNoSecurities;
	}

	public boolean isIndOperationsWithNoSecurities() {
		return indOperationsWithNoSecurities;
	}

	public void setIndOperationsWithNoSecurities(boolean indOperationsWithNoSecurities) {
		this.indOperationsWithNoSecurities = indOperationsWithNoSecurities;
	}

	public Integer getCantOperationsWithNoSecurities() {
		return cantOperationsWithNoSecurities;
	}

	public void setCantOperationsWithNoSecurities(Integer cantOperationsWithNoSecurities) {
		this.cantOperationsWithNoSecurities = cantOperationsWithNoSecurities;
	}

	/**
	 * Gets the id participant assignment pk.
	 *
	 * @return the id participant assignment pk
	 */
	public Long getIdParticipantAssignmentPk() {
		return idParticipantAssignmentPk;
	}
	
	/**
	 * Sets the id participant assignment pk.
	 *
	 * @param idParticipantAssignmentPk the new id participant assignment pk
	 */
	public void setIdParticipantAssignmentPk(Long idParticipantAssignmentPk) {
		this.idParticipantAssignmentPk = idParticipantAssignmentPk;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the participant desc.
	 *
	 * @return the participant desc
	 */
	public String getParticipantDesc() {
		return participantDesc;
	}
	
	/**
	 * Sets the participant desc.
	 *
	 * @param participantDesc the new participant desc
	 */
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
	
	/**
	 * Gets the total operations.
	 *
	 * @return the total operations
	 */
	public BigDecimal getTotalOperations() {
		return totalOperations;
	}
	
	/**
	 * Sets the total operations.
	 *
	 * @param totalOperations the new total operations
	 */
	public void setTotalOperations(BigDecimal totalOperations) {
		this.totalOperations = totalOperations;
	}
	
	/**
	 * Gets the pending operations.
	 *
	 * @return the pending operations
	 */
	public BigDecimal getPendingOperations() {
		return pendingOperations;
	}
	
	/**
	 * Sets the pending operations.
	 *
	 * @param pendingOperations the new pending operations
	 */
	public void setPendingOperations(BigDecimal pendingOperations) {
		this.pendingOperations = pendingOperations;
	}
	
	/**
	 * Gets the registered operations.
	 *
	 * @return the registered operations
	 */
	public BigDecimal getRegisteredOperations() {
		return registeredOperations;
	}
	
	/**
	 * Sets the registered operations.
	 *
	 * @param registeredOperations the new registered operations
	 */
	public void setRegisteredOperations(BigDecimal registeredOperations) {
		this.registeredOperations = registeredOperations;
	}
	
	/**
	 * Gets the confirmed operations.
	 *
	 * @return the confirmed operations
	 */
	public BigDecimal getConfirmedOperations() {
		return confirmedOperations;
	}
	
	/**
	 * Sets the confirmed operations.
	 *
	 * @param confirmedOperations the new confirmed operations
	 */
	public void setConfirmedOperations(BigDecimal confirmedOperations) {
		this.confirmedOperations = confirmedOperations;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the lst pending mechanism operation.
	 *
	 * @return the lst pending mechanism operation
	 */
	public List<MechanismOperationTO> getLstPendingMechanismOperation() {
		return lstPendingMechanismOperation;
	}
	
	/**
	 * Sets the lst pending mechanism operation.
	 *
	 * @param lstPendingMechanismOperation the new lst pending mechanism operation
	 */
	public void setLstPendingMechanismOperation(
			List<MechanismOperationTO> lstPendingMechanismOperation) {
		this.lstPendingMechanismOperation = lstPendingMechanismOperation;
	}
	
	/**
	 * Gets the lst registered mechanism operation.
	 *
	 * @return the lst registered mechanism operation
	 */
	public List<MechanismOperationTO> getLstRegisteredMechanismOperation() {
		return lstRegisteredMechanismOperation;
	}
	
	/**
	 * Sets the lst registered mechanism operation.
	 *
	 * @param lstRegisteredMechanismOperation the new lst registered mechanism operation
	 */
	public void setLstRegisteredMechanismOperation(
			List<MechanismOperationTO> lstRegisteredMechanismOperation) {
		this.lstRegisteredMechanismOperation = lstRegisteredMechanismOperation;
	}
	
	/**
	 * Gets the lst confirmed mechanism operation.
	 *
	 * @return the lst confirmed mechanism operation
	 */
	public List<MechanismOperationTO> getLstConfirmedMechanismOperation() {
		return lstConfirmedMechanismOperation;
	}
	
	/**
	 * Sets the lst confirmed mechanism operation.
	 *
	 * @param lstConfirmedMechanismOperation the new lst confirmed mechanism operation
	 */
	public void setLstConfirmedMechanismOperation(
			List<MechanismOperationTO> lstConfirmedMechanismOperation) {
		this.lstConfirmedMechanismOperation = lstConfirmedMechanismOperation;
	}
	
	/**
	 * Gets the participant full desc.
	 *
	 * @return the participant full desc
	 */
	public String getParticipantFullDesc() {
		return participantFullDesc;
	}
	
	/**
	 * Sets the participant full desc.
	 *
	 * @param participantFullDesc the new participant full desc
	 */
	public void setParticipantFullDesc(String participantFullDesc) {
		this.participantFullDesc = participantFullDesc;
	}
	
	/**
	 * Checks if is checked sale pending assgn.
	 *
	 * @return true, if is checked sale pending assgn
	 */
	public boolean isCheckedSalePendingAssgn() {
		return checkedSalePendingAssgn;
	}
	
	/**
	 * Sets the checked sale pending assgn.
	 *
	 * @param checkedSalePendingAssgn the new checked sale pending assgn
	 */
	public void setCheckedSalePendingAssgn(boolean checkedSalePendingAssgn) {
		this.checkedSalePendingAssgn = checkedSalePendingAssgn;
	}
	
	/**
	 * Checks if is checked purchase pending assgn.
	 *
	 * @return true, if is checked purchase pending assgn
	 */
	public boolean isCheckedPurchasePendingAssgn() {
		return checkedPurchasePendingAssgn;
	}
	
	/**
	 * Sets the checked purchase pending assgn.
	 *
	 * @param checkedPurchasePendingAssgn the new checked purchase pending assgn
	 */
	public void setCheckedPurchasePendingAssgn(boolean checkedPurchasePendingAssgn) {
		this.checkedPurchasePendingAssgn = checkedPurchasePendingAssgn;
	}
}
