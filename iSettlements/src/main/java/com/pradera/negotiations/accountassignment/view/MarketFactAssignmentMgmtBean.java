package com.pradera.negotiations.accountassignment.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketFactAssignmentMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@LoggerCreateBean
@HelperBean
public class MarketFactAssignmentMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant DLG_WDG_ID. */
	private static final String DLG_WDG_ID = "dlgWMarketFact";
	
	/** The settlement account marketfact. */
	private MarketFactAccountTO settlementAccountMarketfact;
	
	/** The lst account marketfacts. */
	private List<MarketFactAccountTO> lstAccountMarketfacts;
	
	/** The fixed income. */
	private boolean fixedIncome;
	
	/** The view only. */
	private boolean viewOnly;
	
	/** The remote command. */
	private String remoteCommand;
	
	/** The max quantity. */
	private BigDecimal maxQuantity;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		settlementAccountMarketfact = new MarketFactAccountTO();
	}
	
	/**
	 * Validate market fact zero.
	 */
	public void validateMarketFactZero(){
		if(fixedIncome){
			BigDecimal enteredRate = settlementAccountMarketfact.getMarketRate();
			if(enteredRate!=null){
				/*
				if(enteredRate.compareTo(BigDecimal.ZERO) == 0){
					settlementAccountMarketfact.setMarketRate(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_RATE_ZERO));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}*/
			}
		}else{
			BigDecimal enteredPrice = settlementAccountMarketfact.getMarketPrice();
			if(enteredPrice!=null){
				if(enteredPrice.compareTo(BigDecimal.ZERO) == 0){
					settlementAccountMarketfact.setMarketPrice(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_PRICE_ZERO));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		}
	}
	
	/**
	 * Adds the market fact.
	 */
	public void addMarketFact(){
		BigDecimal totalQuantityMarketFact = BigDecimal.ZERO;
		if(Validations.validateListIsNullOrEmpty(lstAccountMarketfacts)){
			lstAccountMarketfacts = new ArrayList<MarketFactAccountTO>();
		}else{
			for(MarketFactAccountTO accountMarketFact:lstAccountMarketfacts){
				totalQuantityMarketFact = totalQuantityMarketFact.add(accountMarketFact.getQuantity());
			}	
		}
		
		totalQuantityMarketFact = totalQuantityMarketFact.add(settlementAccountMarketfact.getQuantity());
		if(totalQuantityMarketFact.compareTo(maxQuantity) == 1){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_QUANTITY_EXCEEDED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		lstAccountMarketfacts.add(settlementAccountMarketfact);
		
		settlementAccountMarketfact = new MarketFactAccountTO();
	}
	
	/**
	 * Show market fact.
	 */
	public void showMarketFact(){
		JSFUtilities.resetComponent("frmMarketFact");
		JSFUtilities.executeJavascriptFunction("PF('"+DLG_WDG_ID+"').show();");
	}
	
	/**
	 * Close market fact.
	 */
	public void closeMarketFact(){
		if(!viewOnly){
			BigDecimal total = BigDecimal.ZERO;
			if(Validations.validateListIsNotNullAndNotEmpty(lstAccountMarketfacts)){
				for(MarketFactAccountTO settlementAccountMarketfact:lstAccountMarketfacts){
					total = total.add(settlementAccountMarketfact.getQuantity());
				}
				if(total.compareTo(maxQuantity) != 0){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.ASSIGNMENT_ACCOUNT_MARKETFACT_QUANTITY_NOT_ENOUGH));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		
			JSFUtilities.executeJavascriptFunction(remoteCommand+"();");
		}
		
		JSFUtilities.executeJavascriptFunction("PF('"+DLG_WDG_ID+"').hide();");
		
	}
	
	/**
	 * Removes the market fact.
	 *
	 * @param settlementMarketFact the settlement market fact
	 */
	public void removeMarketFact(MarketFactAccountTO settlementMarketFact){
		lstAccountMarketfacts.remove(settlementMarketFact);
	}

	/**
	 * Gets the settlement account marketfact.
	 *
	 * @return the settlement account marketfact
	 */
	public MarketFactAccountTO getSettlementAccountMarketfact() {
		return settlementAccountMarketfact;
	}

	/**
	 * Sets the settlement account marketfact.
	 *
	 * @param settlementAccountMarketfact the new settlement account marketfact
	 */
	public void setSettlementAccountMarketfact(
			MarketFactAccountTO settlementAccountMarketfact) {
		this.settlementAccountMarketfact = settlementAccountMarketfact;
	}

	/**
	 * Gets the lst account marketfacts.
	 *
	 * @return the lst account marketfacts
	 */
	public List<MarketFactAccountTO> getLstAccountMarketfacts() {
		return lstAccountMarketfacts;
	}

	/**
	 * Sets the lst account marketfacts.
	 *
	 * @param lstAccountMarketfacts the new lst account marketfacts
	 */
	public void setLstAccountMarketfacts(
			List<MarketFactAccountTO> lstAccountMarketfacts) {
		this.lstAccountMarketfacts = lstAccountMarketfacts;
	}

	/**
	 * Checks if is fixed income.
	 *
	 * @return true, if is fixed income
	 */
	public boolean isFixedIncome() {
		return fixedIncome;
	}

	/**
	 * Sets the fixed income.
	 *
	 * @param fixedIncome the new fixed income
	 */
	public void setFixedIncome(boolean fixedIncome) {
		this.fixedIncome = fixedIncome;
	}

	/**
	 * Checks if is view only.
	 *
	 * @return true, if is view only
	 */
	public boolean isViewOnly() {
		return viewOnly;
	}

	/**
	 * Sets the view only.
	 *
	 * @param viewOnly the new view only
	 */
	public void setViewOnly(boolean viewOnly) {
		this.viewOnly = viewOnly;
	}

	/**
	 * Gets the max quantity.
	 *
	 * @return the max quantity
	 */
	public BigDecimal getMaxQuantity() {
		return maxQuantity;
	}

	/**
	 * Sets the max quantity.
	 *
	 * @param maxQuantity the new max quantity
	 */
	public void setMaxQuantity(BigDecimal maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	/**
	 * Gets the remote command.
	 *
	 * @return the remote command
	 */
	public String getRemoteCommand() {
		return remoteCommand;
	}

	/**
	 * Sets the remote command.
	 *
	 * @param remoteCommand the new remote command
	 */
	public void setRemoteCommand(String remoteCommand) {
		this.remoteCommand = remoteCommand;
	}
	
	

}
