package com.pradera.negotiations.accountassignment.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.settlement.SettlementReport;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementRequestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SettlementReportTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id settlement request. */
	private Long idSettlementReportPk;
	
	/** The state description. */
	private String stateDescription;
	
	/** The request state. */
	private Integer requestState;
	
	/** The register date. */
	private Date registerDate;
	
	/** The request state. */
	private Integer quantityDays;
	
	/** The register date. */
	private Date originalDate;
	
	/** The register date. */
	private Date originalExpirationDate;
	
	/** The register date. */
	private Date newExpirationDate;
	
	/** The state description. */
	private String requestParticipant;
	
	/** The state description. */
	private String counterParticipant;
	
	/** The state description. */
	private String idSecurityCodePk;
	
	private MechanismOperation mechanismOperation;
	
	private SettlementReport settlementReport;

	private String balanceDescription;
	
	public String getBalanceDescription() {
		return balanceDescription;
	}

	public void setBalanceDescription(String balanceDescription) {
		this.balanceDescription = balanceDescription;
	}

	
	public Long getIdSettlementReportPk() {
		return idSettlementReportPk;
	}

	public void setIdSettlementReportPk(Long idSettlementReportPk) {
		this.idSettlementReportPk = idSettlementReportPk;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Integer getQuantityDays() {
		return quantityDays;
	}

	public void setQuantityDays(Integer quantityDays) {
		this.quantityDays = quantityDays;
	}

	public Date getOriginalExpirationDate() {
		return originalExpirationDate;
	}

	public void setOriginalExpirationDate(Date originalExpirationDate) {
		this.originalExpirationDate = originalExpirationDate;
	}

	public Date getNewExpirationDate() {
		return newExpirationDate;
	}

	public void setNewExpirationDate(Date newExpirationDate) {
		this.newExpirationDate = newExpirationDate;
	}

	public String getRequestParticipant() {
		return requestParticipant;
	}

	public void setRequestParticipant(String requestParticipant) {
		this.requestParticipant = requestParticipant;
	}

	public String getCounterParticipant() {
		return counterParticipant;
	}

	public void setCounterParticipant(String counterParticipant) {
		this.counterParticipant = counterParticipant;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Date getOriginalDate() {
		return originalDate;
	}

	public void setOriginalDate(Date originalDate) {
		this.originalDate = originalDate;
	}

	public SettlementReport getSettlementReport() {
		return settlementReport;
	}

	public void setSettlementReport(SettlementReport settlementReport) {
		this.settlementReport = settlementReport;
	}
	
	

}
