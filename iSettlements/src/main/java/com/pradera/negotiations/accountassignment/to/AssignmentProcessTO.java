package com.pradera.negotiations.accountassignment.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AssignmentProcessTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class AssignmentProcessTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id assignment process pk. */
	private Long idAssignmentProcessPk;
	
	/** The id negotiation modality pk. */
	private Long idNegotiationModalityPk;
	
	/** The modality name. */
	private String modalityName;
	
	/** The settlement schema. */
	private Integer settlementSchema;
	
	/** The settlement schema name. */
	private String settlementSchemaName;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The confirmed assignment. */
	private BigDecimal totalAssignment, pendingAssignment, registeredAssignment, confirmedAssignment;
	
	/** The state. */
	private Integer state;
	
	/** The state description. */
	private String stateDescription;
	
	/** The lst participant assignment. */
	private List<ParticipantAssignmentTO> lstParticipantAssignment;
	
	/** The checked sale pending assgn. */
	private boolean checkedSalePendingAssgn;
	
	/** The checked purchase pending assgn. */
	private boolean checkedPurchasePendingAssgn;
	
	/** The checked market facts. */
	private boolean checkedMarketFacts;
	
	
	/**
	 * Checks if is checked sale pending assgn.
	 *
	 * @return true, if is checked sale pending assgn
	 */
	public boolean isCheckedSalePendingAssgn() {
		return checkedSalePendingAssgn;
	}
	
	/**
	 * Sets the checked sale pending assgn.
	 *
	 * @param checkedSalePendingAssgn the new checked sale pending assgn
	 */
	public void setCheckedSalePendingAssgn(boolean checkedSalePendingAssgn) {
		this.checkedSalePendingAssgn = checkedSalePendingAssgn;
	}
	
	/**
	 * Checks if is checked purchase pending assgn.
	 *
	 * @return true, if is checked purchase pending assgn
	 */
	public boolean isCheckedPurchasePendingAssgn() {
		return checkedPurchasePendingAssgn;
	}
	
	/**
	 * Sets the checked purchase pending assgn.
	 *
	 * @param checkedPurchasePendingAssgn the new checked purchase pending assgn
	 */
	public void setCheckedPurchasePendingAssgn(boolean checkedPurchasePendingAssgn) {
		this.checkedPurchasePendingAssgn = checkedPurchasePendingAssgn;
	}
	
	/**
	 * Checks if is checked market facts.
	 *
	 * @return true, if is checked market facts
	 */
	public boolean isCheckedMarketFacts() {
		return checkedMarketFacts;
	}
	
	/**
	 * Sets the checked market facts.
	 *
	 * @param checkedMarketFacts the new checked market facts
	 */
	public void setCheckedMarketFacts(boolean checkedMarketFacts) {
		this.checkedMarketFacts = checkedMarketFacts;
	}
	
	/**
	 * Gets the id assignment process pk.
	 *
	 * @return the id assignment process pk
	 */
	public Long getIdAssignmentProcessPk() {
		return idAssignmentProcessPk;
	}
	
	/**
	 * Sets the id assignment process pk.
	 *
	 * @param idAssignmentProcessPk the new id assignment process pk
	 */
	public void setIdAssignmentProcessPk(Long idAssignmentProcessPk) {
		this.idAssignmentProcessPk = idAssignmentProcessPk;
	}
	
	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public Integer getSettlementSchema() {
		return settlementSchema;
	}
	
	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the new settlement schema
	 */
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}
	
	/**
	 * Gets the settlement schema name.
	 *
	 * @return the settlement schema name
	 */
	public String getSettlementSchemaName() {
		return settlementSchemaName;
	}
	
	/**
	 * Sets the settlement schema name.
	 *
	 * @param settlementSchemaName the new settlement schema name
	 */
	public void setSettlementSchemaName(String settlementSchemaName) {
		this.settlementSchemaName = settlementSchemaName;
	}
	
	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}
	
	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the new id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}
	
	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}
	
	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the total assignment.
	 *
	 * @return the total assignment
	 */
	public BigDecimal getTotalAssignment() {
		return totalAssignment;
	}
	
	/**
	 * Sets the total assignment.
	 *
	 * @param totalAssignment the new total assignment
	 */
	public void setTotalAssignment(BigDecimal totalAssignment) {
		this.totalAssignment = totalAssignment;
	}
	
	/**
	 * Gets the pending assignment.
	 *
	 * @return the pending assignment
	 */
	public BigDecimal getPendingAssignment() {
		return pendingAssignment;
	}
	
	/**
	 * Sets the pending assignment.
	 *
	 * @param pendingAssignment the new pending assignment
	 */
	public void setPendingAssignment(BigDecimal pendingAssignment) {
		this.pendingAssignment = pendingAssignment;
	}
	
	/**
	 * Gets the registered assignment.
	 *
	 * @return the registered assignment
	 */
	public BigDecimal getRegisteredAssignment() {
		return registeredAssignment;
	}
	
	/**
	 * Sets the registered assignment.
	 *
	 * @param registeredAssignment the new registered assignment
	 */
	public void setRegisteredAssignment(BigDecimal registeredAssignment) {
		this.registeredAssignment = registeredAssignment;
	}
	
	/**
	 * Gets the confirmed assignment.
	 *
	 * @return the confirmed assignment
	 */
	public BigDecimal getConfirmedAssignment() {
		return confirmedAssignment;
	}
	
	/**
	 * Sets the confirmed assignment.
	 *
	 * @param confirmedAssignment the new confirmed assignment
	 */
	public void setConfirmedAssignment(BigDecimal confirmedAssignment) {
		this.confirmedAssignment = confirmedAssignment;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the lst participant assignment.
	 *
	 * @return the lst participant assignment
	 */
	public List<ParticipantAssignmentTO> getLstParticipantAssignment() {
		return lstParticipantAssignment;
	}
	
	/**
	 * Sets the lst participant assignment.
	 *
	 * @param lstParticipantAssignment the new lst participant assignment
	 */
	public void setLstParticipantAssignment(
			List<ParticipantAssignmentTO> lstParticipantAssignment) {
		this.lstParticipantAssignment = lstParticipantAssignment;
	}
	
}
