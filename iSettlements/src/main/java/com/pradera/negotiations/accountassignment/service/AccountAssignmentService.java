package com.pradera.negotiations.accountassignment.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.AssignmentProcessOpen;
import com.pradera.model.negotiation.AssignmentProcessOpenFile;
import com.pradera.model.negotiation.AssignmentRequest;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.InchargeRule;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationOpen;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.ReassignmentRequest;
import com.pradera.model.negotiation.ReassignmentRequestDetail;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.ParticipantAssignmentStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ChainedHolderOperation;
import com.pradera.model.settlement.ParticipantSettlement;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.SettlementAccountOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.type.ChainedOperationStateType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.settlement.type.ReassignmentRequestStateType;
import com.pradera.negotiations.accountassignment.exception.AccountAssignmentException;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.accountassignment.to.SearchAccountAssignmentTO;
import com.pradera.negotiations.assignmentrequest.to.AccountAssignmentTO;
import com.pradera.negotiations.assignmentrequest.to.AccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.AssignmentOpenModelTO;
import com.pradera.negotiations.assignmentrequest.to.MarckefactDataTO;
import com.pradera.negotiations.assignmentrequest.to.MechanismOperationForOpenTO;
import com.pradera.negotiations.assignmentrequest.to.ParameterToRevertBalances;
import com.pradera.negotiations.assignmentrequest.to.RegisterAccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAssignmentRequestTO;
import com.pradera.negotiations.operations.service.MechanismOperationService;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.settlements.chainedoperation.service.ChainedOperationsServiceBean;
import com.pradera.settlements.chainedoperation.to.SearchChainedOperationTO;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAssignmentService.
 *
 * @author PraderaTechnologies
 */
@Stateless
public class AccountAssignmentService extends CrudDaoServiceBean{
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(AccountAssignmentService.class);
		
	/** The negotiation operation service bean. */
	@EJB
	private NegotiationOperationServiceBean negotiationOperationServiceBean;	
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The mechanism operation service. */
	@EJB
	private MechanismOperationService mechanismOperationService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The chained operations service bean. */
	@EJB
	private ChainedOperationsServiceBean chainedOperationsServiceBean;
	
	/** The batch service bean. */
	@EJB
	private BatchServiceBean batchServiceBean;
	
	@EJB
	private HolderAccountComponentServiceBean holderAccountServiceBean;
	
	/**
	 * ejecuta un update o delete de una query nativa
	 */
	public void executeUpdateNativeQuery(String queryNative, Map<String, Object> parameters){
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = em.createNativeQuery(queryNative);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		int cantExcecute=query.executeUpdate();
		System.out.println("::: cantidad de registros afectados: "+cantExcecute);
	} 
	
	/**
	 * issue 1161: obtiene los id de las operaciones MCN que se revertira sus saldos de comrpa/venta
	 * @param idAssignmentProcesOpenPk
	 * @return
	 */
	private List<Long> lstIdMechanismOperationToRevertBalances(Long idAssignmentProcesOpenPk) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" select MOO.ID_MECHANISM_OPERATION_FK from MECHANISM_OPERATION_OPEN moo ");
		sd.append(" where MOO.ID_ASSIGNMENT_OPEN_FK = :idAssignmentProcesOpenPk");
		
		Query q=em.createNativeQuery(sd.toString());
		q.setParameter("idAssignmentProcesOpenPk", idAssignmentProcesOpenPk);
		List<Object> lstObj = q.getResultList();
		List<Long> lst=new ArrayList<>();
		for(Object obj : lstObj){
			lst.add(Long.parseLong(obj.toString()));
		}
		return lst;
	}
	
	/**
	 * issue 1161: obtine los parametros para la reversion de saldos (parte VENTA)
	 * @param idMechanismOperationPk
	 * @return
	 */
	private List<ParameterToRevertBalances> obtainParameterToRevertSaleBalance(Long idMechanismOperationPk){
		StringBuilder sd=new StringBuilder();
		sd.append(" select MO.ID_SELLER_PARTICIPANT_FK, HAO.ID_HOLDER_ACCOUNT_FK, MO.ID_SECURITY_CODE_FK, HAO.STOCK_QUANTITY, HAO.ID_HOLDER_ACCOUNT_OPERATION_PK, S.INSTRUMENT_TYPE from HOLDER_ACCOUNT_OPERATION hao                      ");
		sd.append(" inner join MECHANISM_OPERATION mo  on MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK and HAO.role = 2 and HAO.ID_REF_ACCOUNT_OPERATION_FK is null ");
		sd.append(" inner join SECURITY s on S.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK");
		sd.append(" where MO.ID_MECHANISM_OPERATION_PK = :idMechanismOperationPk																							        ");
		
		Query q=em.createNativeQuery(sd.toString());
		q.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		List<Object[]> lst=q.getResultList();
		if(lst==null || lst.isEmpty() || lst.size() == 0){
			return null;
		}else{
			List<ParameterToRevertBalances> lstParametersToRevertBalances=new ArrayList<>();
			ParameterToRevertBalances parameter;
			for(Object[] row : lst){
				parameter=new ParameterToRevertBalances();
				// verificando si tenemos los 4 parametros
				if(row[0]==null) {return null;}
				else {parameter.setIdParticipant(Long.parseLong(row[0].toString()));}
				if(row[1]==null) {return null;}
				else {parameter.setIdHolderAccount(Long.parseLong(row[1].toString()));}
				if(row[2]==null) {return null;}
				else {parameter.setIdSecurotyCodePk(row[2].toString());}
				if(row[3]==null) {return null;}
				else {parameter.setStockQuantity(Long.parseLong(row[3].toString()));}
				if(row[4]==null) {return null;}
				else {parameter.setIdHolderAccountOperationPk(Long.parseLong(row[4].toString()));}
				if(row[5]==null) {return null;}
				else {parameter.setInstrumentType(Integer.parseInt(row[5].toString()));}
				// role: venta
				parameter.setRole(ComponentConstant.SALE_ROLE);
				parameter.setIdMechanismOperation(idMechanismOperationPk);
				
				lstParametersToRevertBalances.add(parameter);
			}
			
			return lstParametersToRevertBalances;
		}
	}
	
	/**
	 * issue 1161: obtine los parametros para la reversion de saldos (parte COMPRA)
	 * @param idMechanismOperationPk
	 * @return
	 */
	private List<ParameterToRevertBalances> obtainParameterToRevertPurchaseBalance(Long idMechanismOperationPk){
		StringBuilder sd=new StringBuilder();
		sd.append(" select MO.ID_BUYER_PARTICIPANT_FK, HAO.ID_HOLDER_ACCOUNT_FK, MO.ID_SECURITY_CODE_FK, HAO.STOCK_QUANTITY, HAO.ID_HOLDER_ACCOUNT_OPERATION_PK, S.INSTRUMENT_TYPE from HOLDER_ACCOUNT_OPERATION hao                      ");
		sd.append(" inner join MECHANISM_OPERATION mo  on MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK and HAO.role = 1 and HAO.ID_REF_ACCOUNT_OPERATION_FK is null ");
		sd.append(" inner join SECURITY s on S.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK");
		sd.append(" where MO.ID_MECHANISM_OPERATION_PK = :idMechanismOperationPk																							        ");
		
		Query q=em.createNativeQuery(sd.toString());
		q.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		List<Object[]> lst=q.getResultList();
		if(lst==null || lst.isEmpty() || lst.size() == 0){
			return null;
		}else{
			List<ParameterToRevertBalances> lstParametersToRevertBalances=new ArrayList<>();
			ParameterToRevertBalances parameter;
			for(Object[] row : lst){
				parameter=new ParameterToRevertBalances();
				// verificando si tenemos los 4 parametros
				if(row[0]==null) {return null;}
				else {parameter.setIdParticipant(Long.parseLong(row[0].toString()));}
				if(row[1]==null) {return null;}
				else {parameter.setIdHolderAccount(Long.parseLong(row[1].toString()));}
				if(row[2]==null) {return null;}
				else {parameter.setIdSecurotyCodePk(row[2].toString());}
				if(row[3]==null) {return null;}
				else {parameter.setStockQuantity(Long.parseLong(row[3].toString()));}
				if(row[4]==null) {return null;}
				else {parameter.setIdHolderAccountOperationPk(Long.parseLong(row[4].toString()));}
				if(row[5]==null) {return null;}
				else {parameter.setInstrumentType(Integer.parseInt(row[5].toString()));}
				// role: compra
				parameter.setRole(ComponentConstant.PURCHARSE_ROLE);
				parameter.setIdMechanismOperation(idMechanismOperationPk);
				
				lstParametersToRevertBalances.add(parameter);
			}
			
			return lstParametersToRevertBalances;
		}
	}
	
	/**
	 * issue 1161: obtiene los HM de una cuanta titular
	 * @param idHolderAccountOperationPk
	 * @return
	 */
	private List<MarckefactDataTO> obtainHM(Long idHolderAccountOperationPk){
		StringBuilder sd=new StringBuilder();
		sd.append(" select trunc(SAM.MARKET_DATE) as fecha, nvl(SAM.MARKET_RATE,0) as tasa, nvl(SAM.MARKET_PRICE,0) as precio, nvl(SAM.MARKET_QUANTITY,0) as cant_neg_hm ");
		sd.append(" from SETTLEMENT_ACCOUNT_MARKETFACT sam");
		sd.append(" inner join SETTLEMENT_ACCOUNT_OPERATION sao on SAO.ID_SETTLEMENT_ACCOUNT_PK = SAM.ID_SETTLEMENT_ACCOUNT_FK");
		sd.append(" where SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = :idHolderAccountOperationPk  ");
		
		Query q=em.createNativeQuery(sd.toString());
		q.setParameter("idHolderAccountOperationPk", idHolderAccountOperationPk);
		List<Object[]> lst=q.getResultList();
		if(lst==null || lst.isEmpty() || lst.size() == 0){
			return null;
		}else{
			List<MarckefactDataTO> lstMarckefactDataTO=new ArrayList<>();
			MarckefactDataTO marckefactData;
			for(Object[] row : lst){
				marckefactData=new MarckefactDataTO();
				marckefactData.setDateHM((Date) row[0]);
				marckefactData.setRateHM(new BigDecimal(row[1].toString()));
				marckefactData.setPriceHM(new BigDecimal(row[2].toString()));
				// cantidad negociada a nivel HM (ayuda para la reversion de saldos en HMB)
				marckefactData.setStockQuantityHM(Long.parseLong(row[3].toString()));
				
				lstMarckefactDataTO.add(marckefactData);
			}
			
			return lstMarckefactDataTO;
		}
	}
	
	/**
	 * issue 1161: realiza la reversion de saldos entre comprador(PURCHASE_BALANCE) y Vendedor(SALE_BALANCE) 
	 * cuando se Apertura el Cierre de Asignacion General
	 * @param paramToRevert
	 */
	private void revertRevertBalance(ParameterToRevertBalances paramToRevert){
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sd=new StringBuilder();
		
		sd.append(" select HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK from HOLDER_ACCOUNT_MOVEMENT ham   ");
		sd.append(" where 0=0                                                                   ");
		sd.append(" and HAM.ID_PARTICIPANT_FK = :idParticipant                                  ");
		sd.append(" and HAM.ID_HOLDER_ACCOUNT_FK = :idHolderAccount                             ");
		sd.append(" and HAM.ID_SECURITY_CODE_FK = :idSecurityCodePk                             ");
		sd.append(" and HAM.ID_TRADE_OPERATION_FK = :idMechanismOperation                       ");
		sd.append(" and HAM.MOVEMENT_QUANTITY = :cantNeg										");
		sd.append(" and trunc(HAM.MOVEMENT_DATE) = :date									    ");
		
		Query q=em.createNativeQuery(sd.toString());
		q.setParameter("idParticipant", paramToRevert.getIdParticipant());
		q.setParameter("idHolderAccount", paramToRevert.getIdHolderAccount() );
		q.setParameter("idSecurityCodePk", paramToRevert.getIdSecurotyCodePk());
		q.setParameter("idMechanismOperation", paramToRevert.getIdMechanismOperation());
		q.setParameter("cantNeg", paramToRevert.getStockQuantity());
		q.setParameter("date", paramToRevert.getDate() );
		List<Object> lstHAM=q.getResultList();
		if(lstHAM == null || lstHAM.isEmpty() || lstHAM.size() == 0){
			System.out.println("::: No se ha encontrado identificador para HOLDER_ACCOUNT_MOVEMENT");
		}else{
			// con este id eliminamos a la tabla hija y a la misma tabla
			Long idHolderAccountMovementPk = Long.parseLong(lstHAM.get(0).toString());
			String queryNativeExecute="delete from HOLDER_MARKETFACT_MOVEMENT where ID_HOLDER_ACCOUNT_MOVEMENT_FK = :idHolderAccountMovementPk";
			parameters = new HashMap<String, Object>();
			parameters.put("idHolderAccountMovementPk", idHolderAccountMovementPk);
			executeUpdateNativeQuery(queryNativeExecute, parameters);
			queryNativeExecute="delete from HOLDER_ACCOUNT_MOVEMENT where ID_HOLDER_ACCOUNT_MOVEMENT_PK = :idHolderAccountMovementPk";
			executeUpdateNativeQuery(queryNativeExecute, parameters);
		}
		
		// quitando los INDICADORES DE LOS VALORES EN NEGOCIACION 
		// para cuando se vuelva a cerrar el Processo DE CIERRE DE ASIGNACION
		sd = new StringBuilder();
		sd.append(" update SETTLEMENT_ACCOUNT_OPERATION set STOCK_REFERENCE = null                    ");
		sd.append(" where ID_SETTLEMENT_OPERATION_FK in (select SO.ID_SETTLEMENT_OPERATION_PK         ");
		sd.append(" 									 from SETTLEMENT_OPERATION so                 ");
		sd.append(" 									 where SO.ID_MECHANISM_OPERATION_FK = :idMechanismOperation )");
		parameters = new HashMap<String, Object>();
		parameters.put("idMechanismOperation", paramToRevert.getIdMechanismOperation());
		executeUpdateNativeQuery(sd.toString(), parameters);
		
		// SI ES COMPRADOR QUITAMOS LA CANTIDAD COMPRADA EN  HMB Y HAB (Role 1)
		// SI ES VENDEDOR DEVOLVEMOS SALDOS CON UPDATE (Role 2)
		if(paramToRevert.getRole().equals(ComponentConstant.PURCHARSE_ROLE)){
			// ES COMPRADOR tenemos que devolver los saldos de acuerdo a la cantidad negociada
			// ===========================================================================
			if(paramToRevert.getLstMarckefactDataTO()!=null){
				// devolviendo saldos a nivel HM
				for(MarckefactDataTO hm: paramToRevert.getLstMarckefactDataTO()){
					
					// devolviendo saldos en HMB
					sd = new StringBuilder();
					sd.append(" update HOLDER_MARKETFACT_BALANCE set PURCHASE_BALANCE = PURCHASE_BALANCE - :cantNeg   ");
					sd.append(" where ID_PARTICIPANT_FK = :idParticipant                                              ");
					sd.append(" and ID_HOLDER_ACCOUNT_FK = :idHolderAccount                                           ");
					sd.append(" and ID_SECURITY_CODE_FK = :idSecurityCodePk                                           ");
					sd.append(" and PURCHASE_BALANCE >= :cantNeg                                                      ");
					sd.append(" and trunc(LAST_MODIFY_DATE) = :date 												  ");
					sd.append(" and MARKET_DATE = :dateHM														  ");
					sd.append(" and ((MARKET_RATE = :rateHM and :instrumentType = "+InstrumentType.FIXED_INCOME.getCode()+") ");
					sd.append(" 	or (MARKET_PRICE = :priceHM and :instrumentType = "+InstrumentType.VARIABLE_INCOME.getCode()+")) ");
					
					parameters = new HashMap<String, Object>();
					parameters.put("cantNeg", hm.getStockQuantityHM());
					parameters.put("idParticipant", paramToRevert.getIdParticipant());
					parameters.put("idHolderAccount", paramToRevert.getIdHolderAccount());
					parameters.put("idSecurityCodePk", paramToRevert.getIdSecurotyCodePk());
					parameters.put("date", paramToRevert.getDate());
					// parametros HM
					parameters.put("dateHM", CommonsUtilities.truncateDateTime(hm.getDateHM()));
					parameters.put("rateHM", hm.getRateHM());
					parameters.put("priceHM", hm.getPriceHM());
					parameters.put("instrumentType", paramToRevert.getInstrumentType());
					executeUpdateNativeQuery(sd.toString(), parameters);
				}
			}else{
				// devolviendo saldos en HMB
				sd = new StringBuilder();
				sd.append(" update HOLDER_MARKETFACT_BALANCE set PURCHASE_BALANCE = PURCHASE_BALANCE - :cantNeg   ");
				sd.append(" where ID_PARTICIPANT_FK = :idParticipant                                              ");
				sd.append(" and ID_HOLDER_ACCOUNT_FK = :idHolderAccount                                           ");
				sd.append(" and ID_SECURITY_CODE_FK = :idSecurityCodePk                                           ");
				sd.append(" and PURCHASE_BALANCE >= :cantNeg                                                      ");
				sd.append(" and trunc(LAST_MODIFY_DATE) = :date 												  ");
				
				parameters = new HashMap<String, Object>();
				parameters.put("cantNeg", paramToRevert.getStockQuantity());
				parameters.put("idParticipant", paramToRevert.getIdParticipant());
				parameters.put("idHolderAccount", paramToRevert.getIdHolderAccount());
				parameters.put("idSecurityCodePk", paramToRevert.getIdSecurotyCodePk());
				parameters.put("date", paramToRevert.getDate());
				
				executeUpdateNativeQuery(sd.toString(), parameters);
			}
			
			// devolviendo saldos en HAB
			sd = new StringBuilder();
			sd.append(" update HOLDER_ACCOUNT_BALANCE set PURCHASE_BALANCE = PURCHASE_BALANCE - :cantNeg   ");
			sd.append(" where ID_PARTICIPANT_PK = :idParticipant                                           ");
			sd.append(" and ID_HOLDER_ACCOUNT_PK = :idHolderAccount                                        ");
			sd.append(" and ID_SECURITY_CODE_PK = :idSecurityCodePk                                        ");
			sd.append(" and PURCHASE_BALANCE >= :cantNeg 												   ");
			parameters = new HashMap<String, Object>();
			parameters.put("cantNeg", paramToRevert.getStockQuantity());
			parameters.put("idParticipant", paramToRevert.getIdParticipant());
			parameters.put("idHolderAccount", paramToRevert.getIdHolderAccount() );
			parameters.put("idSecurityCodePk", paramToRevert.getIdSecurotyCodePk());
			executeUpdateNativeQuery(sd.toString(), parameters);
		}else{
			// ES VENDEDOR tenemos que devolver los saldos de acuerdo a la cantidad negociada
			// ===========================================================================
			if(paramToRevert.getLstMarckefactDataTO()!=null){
				// devolviendo saldos a nivel HM
				for(MarckefactDataTO hm: paramToRevert.getLstMarckefactDataTO()){
					
					// devolviendo saldos en HMB
					sd = new StringBuilder();
					sd.append(" update HOLDER_MARKETFACT_BALANCE set SALE_BALANCE = SALE_BALANCE - :cantNeg, AVAILABLE_BALANCE = AVAILABLE_BALANCE + :cantNeg   ");
					sd.append(" where ID_PARTICIPANT_FK = :idParticipant                                                                                        ");
					sd.append(" and ID_HOLDER_ACCOUNT_FK = :idHolderAccount                                                                                     ");
					sd.append(" and ID_SECURITY_CODE_FK = :idSecurityCodePk                                                                                     ");
					sd.append(" and SALE_BALANCE >= :cantNeg                                                                                                    ");
					sd.append(" and trunc(LAST_MODIFY_DATE) = :date 																						    ");
					sd.append(" and MARKET_DATE = :dateHM														  ");
					sd.append(" and ((MARKET_RATE = :rateHM and :instrumentType = "+InstrumentType.FIXED_INCOME.getCode()+") ");
					sd.append(" 	or (MARKET_PRICE = :priceHM and :instrumentType = "+InstrumentType.VARIABLE_INCOME.getCode()+")) ");
					
					parameters = new HashMap<String, Object>();
					parameters.put("cantNeg", hm.getStockQuantityHM());
					parameters.put("idParticipant", paramToRevert.getIdParticipant());
					parameters.put("idHolderAccount", paramToRevert.getIdHolderAccount() );
					parameters.put("idSecurityCodePk", paramToRevert.getIdSecurotyCodePk());
					parameters.put("date", paramToRevert.getDate() );
					// parametros de HM
					parameters.put("dateHM", CommonsUtilities.truncateDateTime(hm.getDateHM()));
					parameters.put("rateHM", hm.getRateHM());
					parameters.put("priceHM", hm.getPriceHM());
					parameters.put("instrumentType", paramToRevert.getInstrumentType());
					executeUpdateNativeQuery(sd.toString(), parameters);
				}
			}else{
				// devolviendo saldos en HMB
				sd = new StringBuilder();
				sd.append(" update HOLDER_MARKETFACT_BALANCE set SALE_BALANCE = SALE_BALANCE - :cantNeg, AVAILABLE_BALANCE = AVAILABLE_BALANCE + :cantNeg   ");
				sd.append(" where ID_PARTICIPANT_FK = :idParticipant                                                                                        ");
				sd.append(" and ID_HOLDER_ACCOUNT_FK = :idHolderAccount                                                                                     ");
				sd.append(" and ID_SECURITY_CODE_FK = :idSecurityCodePk                                                                                     ");
				sd.append(" and SALE_BALANCE >= :cantNeg                                                                                                    ");
				sd.append(" and trunc(LAST_MODIFY_DATE) = :date 																						    ");
				
				parameters = new HashMap<String, Object>();
				parameters.put("cantNeg", paramToRevert.getStockQuantity());
				parameters.put("idParticipant", paramToRevert.getIdParticipant());
				parameters.put("idHolderAccount", paramToRevert.getIdHolderAccount() );
				parameters.put("idSecurityCodePk", paramToRevert.getIdSecurotyCodePk());
				parameters.put("date", paramToRevert.getDate() );
				
				executeUpdateNativeQuery(sd.toString(), parameters);
			}
			
			// devolviendo saldos en HAB
			sd = new StringBuilder();
			sd.append(" update HOLDER_ACCOUNT_BALANCE set SALE_BALANCE = SALE_BALANCE - :cantNeg, AVAILABLE_BALANCE = AVAILABLE_BALANCE + :cantNeg   ");
			sd.append(" where ID_PARTICIPANT_PK = :idParticipant                                                                                     ");
			sd.append(" and ID_HOLDER_ACCOUNT_PK = :idHolderAccount                                                                                  ");
			sd.append(" and ID_SECURITY_CODE_PK = :idSecurityCodePk                                                                                  ");
			sd.append(" and SALE_BALANCE >= :cantNeg																								 ");
			parameters = new HashMap<String, Object>();
			parameters.put("cantNeg", paramToRevert.getStockQuantity());
			parameters.put("idParticipant", paramToRevert.getIdParticipant());
			parameters.put("idHolderAccount", paramToRevert.getIdHolderAccount() );
			parameters.put("idSecurityCodePk", paramToRevert.getIdSecurotyCodePk());
			executeUpdateNativeQuery(sd.toString(), parameters);
		}
	}
	
	/**
	 * iisue 1161: hace la reversion de saldos VENTA/COMPRA cuando se confirma una Apertura de Cierre de Asignacion General
	 * @param idAssignmentProcesOpenPk
	 * @param date
	 * @throws ServiceException
	 */
	public void revertSaleBalances(Long idAssignmentProcesOpenPk, Date date) throws ServiceException{
		//hallando una lista de los MechaOp que seran cancelados
		List<Long> lstIdOperationMCN=lstIdMechanismOperationToRevertBalances(idAssignmentProcesOpenPk);
		for(Long idMCN:lstIdOperationMCN){
			// obteniendo los parametrso para revertir saldos parte VENTA
			List<ParameterToRevertBalances> lstParametersRevertSaleBalance = obtainParameterToRevertSaleBalance(idMCN);
			if(lstParametersRevertSaleBalance!=null && !lstParametersRevertSaleBalance.isEmpty()){
				for(ParameterToRevertBalances parametersRevertSaleBalance : lstParametersRevertSaleBalance){
					parametersRevertSaleBalance.setDate(date);
					//obteniendo los HM de cada MCN a revertir
					List<MarckefactDataTO> lstMarckefactDataTO=obtainHM(parametersRevertSaleBalance.getIdHolderAccountOperationPk());
					if(lstMarckefactDataTO!=null && !lstMarckefactDataTO.isEmpty()){
						parametersRevertSaleBalance.setLstMarckefactDataTO(lstMarckefactDataTO);
					}else{
						parametersRevertSaleBalance.setLstMarckefactDataTO(null);
					}
					
					// reversion de saldos para la parte VENTA
					revertRevertBalance(parametersRevertSaleBalance);
				}
			}
			
			// obteniendo los parametrso para revertir saldos parte COMPRA
			List<ParameterToRevertBalances> lstParametersRevertPurchaseBalance = obtainParameterToRevertPurchaseBalance(idMCN);
			if(lstParametersRevertPurchaseBalance!=null && !lstParametersRevertPurchaseBalance.isEmpty()){
				for(ParameterToRevertBalances parametersRevertPurchaseBalance : lstParametersRevertPurchaseBalance){
					parametersRevertPurchaseBalance.setDate(date);
					//obteniendo los HM de cada MCN a revertir
					List<MarckefactDataTO> lstMarckefactDataTO=obtainHM(parametersRevertPurchaseBalance.getIdHolderAccountOperationPk());
					if(lstMarckefactDataTO!=null && !lstMarckefactDataTO.isEmpty()){
						parametersRevertPurchaseBalance.setLstMarckefactDataTO(lstMarckefactDataTO);
					}else{
						parametersRevertPurchaseBalance.setLstMarckefactDataTO(null);
					}
					// reversion de saldos para la parte COMPRA
					revertRevertBalance(parametersRevertPurchaseBalance);
				}
			}
			
		}
	}
	
	public List<AssignmentProcessOpenFile> lstAssignmentProcessOpenFiles(Long idAssignmentPorcessOpenPk) throws ServiceException {
		StringBuilder sd=new StringBuilder();
		sd.append(" select apoFile.idAssignmentOpenFilePk,");
		sd.append(" 	   apoFile.filename,");
		sd.append(" 	   apoFile.registryDate,");
		sd.append(" 	   apoFile.lastModifyUser,");
		sd.append(" 	   apoFile.lastModifyDate,");
		sd.append(" 	   apoFile.lastModifyIp,");
		sd.append(" 	   apoFile.lastModifyApp,");
		sd.append(" 	   apoFile.idAssignmentProcessOpenFk");
	    sd.append(" from AssignmentProcessOpenFile apoFile ");
		sd.append(" where apoFile.idAssignmentProcessOpenFk.idAssignmentProcessOpenPk = :idAssignmentPorcessOpenPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idAssignmentPorcessOpenPk", idAssignmentPorcessOpenPk);
		List<Object[]> objectLst = findListByQueryString(sd.toString(), parameters);
		List<AssignmentProcessOpenFile> res = null;
		
		if(Validations.validateIsNotNullAndNotEmpty(objectLst)) {
			res = new ArrayList<AssignmentProcessOpenFile>();
			for(Object[] obj : objectLst) {
				AssignmentProcessOpenFile apof = new AssignmentProcessOpenFile();
				
				apof.setIdAssignmentOpenFilePk(Long.valueOf(obj[0].toString()));
				apof.setFilename(obj[1].toString());
				apof.setRegistryDate((Date)obj[2]);
				apof.setLastModifyUser(obj[3].toString());
				apof.setLastModifyDate((Date)obj[4]);
				apof.setLastModifyIp(obj[5].toString());
				apof.setLastModifyIp(obj[6].toString());
				apof.setIdAssignmentProcessOpenFk((AssignmentProcessOpen)obj[7]);
				apof.setDocumentFile(this.getAssignmentProcessOpenFile(apof.getIdAssignmentOpenFilePk()));
				
				res.add(apof);
			}
		}
		
		return res;
	}
	
	public byte[] getAssignmentProcessOpenFile(Long idAssignmentOpenFilePk) throws ServiceException{
		String query = "Select apoFile.documentFile FROM AssignmentProcessOpenFile apoFile WHERE apoFile.idAssignmentOpenFilePk = :idAssignmentOpenFilePk";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("idAssignmentOpenFilePk", idAssignmentOpenFilePk);
		try{
			return (byte[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	public List<MechanismOperationOpen> lstMechanismOperationOpen(Long idAssignmentPorcessOpenPk) throws ServiceException {
		StringBuilder sd=new StringBuilder();
		sd.append(" select moo");
		sd.append(" from MechanismOperationOpen moo");
		sd.append(" inner join fetch moo.idMechanismOperationFk mo");
		sd.append(" inner join moo.idAssignmentOpenFk ao");
		sd.append(" where ao.idAssignmentProcessOpenPk = :idAssignmentPorcessOpenPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idAssignmentPorcessOpenPk", idAssignmentPorcessOpenPk);
		return findListByQueryString(sd.toString(), parameters);
	}
	
	public List<Object[]> lstAssignmentOpenModelTO(AssignmentOpenModelTO filter) throws ServiceException{
		List<Object[]> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append(" select                                                                                  ");
		sd.append("   APO.ID_ASSIGNMENT_PROCESS_OPEN_PK,                                                    ");
		sd.append("   APO.REGISTRY_DATE AS FECHA_APERTURA,                            ");
		sd.append("   APO.MOTIVE_OPEN,                                                                      ");
		sd.append("   (select                                                                               ");
		sd.append("     LISTAGG((                                                                           ");
		sd.append("       select MO.BALLOT_NUMBER||'/'||MO.sequential                                       ");
		sd.append("       from MECHANISM_OPERATION mo                                                       ");
		sd.append("       where MO.ID_MECHANISM_OPERATION_PK = MOO.ID_MECHANISM_OPERATION_FK) ||'<br/>')    ");
		sd.append("     WITHIN GROUP (order by MOO.ID_MECHANISM_OPERATION_FK)                               ");
		sd.append("   from MECHANISM_OPERATION_OPEN moo                                                     ");
		sd.append("   where MOO.ID_ASSIGNMENT_OPEN_FK = APO.ID_ASSIGNMENT_PROCESS_OPEN_PK) AS PAPELETAS,    ");
		sd.append("   (SELECT APS.LAST_MODIFY_USER FROM ASSIGNMENT_PROCESS_OPEN_STATE APS                   ");
		sd.append("     WHERE APS.ID_ASSIGNMENT_PROCESS_OPEN_FK = APO.ID_ASSIGNMENT_PROCESS_OPEN_PK         ");
		sd.append("     AND APS.STATE_REQUEST = 2521) AS USER_REG,                                          ");
		sd.append("   (SELECT APS.LAST_MODIFY_USER FROM ASSIGNMENT_PROCESS_OPEN_STATE APS                   ");
		sd.append("     WHERE APS.ID_ASSIGNMENT_PROCESS_OPEN_FK = APO.ID_ASSIGNMENT_PROCESS_OPEN_PK         ");
		sd.append("     AND APS.STATE_REQUEST = 2522) AS USER_CONFIRM,                                      ");
		sd.append("   (SELECT P.DESCRIPTION FROM PARAMETER_TABLE P                                          ");
		sd.append("     WHERE P.PARAMETER_TABLE_PK = APO.STATE_REQUEST) AS ESTADO,                          ");
		sd.append("	  APO.STATE_REQUEST as STATE_NUM");
		sd.append(" from ASSIGNMENT_PROCESS_OPEN apo                                                        ");
		sd.append(" where 0=0																				");
		if (Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getFinalDate())) {
			sd.append(" and APO.REGISTRY_DATE between :initialDate and :finalDate");
		}
		if (Validations.validateIsNotNull(filter.getId())) {
			sd.append(" and APO.ID_ASSIGNMENT_PROCESS_OPEN_PK = :numberRequest");
		}
		if (Validations.validateIsNotNull(filter.getStateRequest())) {
			sd.append(" and APO.STATE_REQUEST = :stateRequest");
		}

		try {
			Query q=em.createNativeQuery(sd.toString());
			if (Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getFinalDate())) {
				q.setParameter("initialDate", filter.getInitialDate());
				q.setParameter("finalDate", filter.getFinalDate());
			}
			if (Validations.validateIsNotNull(filter.getId())) {
				q.setParameter("numberRequest", filter.getId());
			}
			if (Validations.validateIsNotNull(filter.getStateRequest())) {
				q.setParameter("stateRequest", filter.getStateRequest());
			}
			lst=q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException();
		}
		return lst;
	}
	
	/**
	 * obtiene la cantidad de encadenamientos que tiene una operacio MCN
	 * @param idMechanismOperationPk
	 * @return
	 */
	public Integer quantityChainsMcn(Long idMechanismOperationPk){
		StringBuilder sd=new StringBuilder();
		sd.append(" select count(case when SAO.CHAINED_QUANTITY > 0 then 1 else null end) as cant_ecadenaminetos                        ");
		sd.append(" from SETTLEMENT_ACCOUNT_OPERATION sao                                                                               ");
		sd.append(" inner join HOLDER_ACCOUNT_OPERATION hao on HAO.ID_HOLDER_ACCOUNT_OPERATION_PK = SAO.ID_HOLDER_ACCOUNT_OPERATION_FK  ");
		sd.append(" inner join MECHANISM_OPERATION mo on MO.ID_MECHANISM_OPERATION_PK = HAO.ID_MECHANISM_OPERATION_FK                   ");
		sd.append(" where MO.ID_MECHANISM_OPERATION_PK = :idMechanismOperationPk														");
		try {
			Query q=em.createNativeQuery(sd.toString());
			q.setParameter("idMechanismOperationPk", idMechanismOperationPk);
			Object result=q.getSingleResult();
			if(Validations.validateIsNotNull(result)){
				return Integer.parseInt(result.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error en el metodo AccountAssignmentService.quantityChainsMcn "+e.getMessage());
		}
		return null;
	}
	
	/**
	 * obtiene las operaciones MCN que cambiaran de estado ASIGNADO -> REGISTRADO
	 * @param parameterDate
	 * @return
	 * @throws ServiceException
	 */
	public List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTO(AssignmentOpenModelTO filter) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" select                                                                                              ");
		sd.append("   MO.ID_MECHANISM_OPERATION_PK,                                                                     ");
		sd.append("   to_char(MO.OPERATION_DATE,'DD/MM/YYYY'),                                                          ");
		sd.append("   MO.BALLOT_NUMBER,                                                                                 ");
		sd.append("   MO.SEQUENTIAL,                                                                                    ");
		sd.append("   NM.MODALITY_NAME,                                                                                 ");
		sd.append("   MO.ID_SECURITY_CODE_FK,                                                                           ");
		sd.append("   CURRENCY.TEXT1 as CURENCY,                                                                        ");
		sd.append("   PSELL.MNEMONIC as PSELL,                                                                          ");
		sd.append("   PBUY.MNEMONIC as PBUY,                                                                            ");
		sd.append("   PSTATE.DESCRIPTION as STATE_TEXT,                                                                 ");
		sd.append("   MO.OPERATION_STATE as STATE_OP                                                                    ");
		sd.append(" from MECHANISM_OPERATION mo                                                                         ");
		sd.append(" inner join NEGOTIATION_MODALITY nm on NM.ID_NEGOTIATION_MODALITY_PK = MO.ID_NEGOTIATION_MODALITY_FK ");
		sd.append(" inner join PARAMETER_TABLE currency on CURRENCY.PARAMETER_TABLE_PK = MO.OPERATION_CURRENCY          ");
		sd.append(" inner join PARTICIPANT pbuy on PBUY.ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK                  ");
		sd.append(" inner join PARTICIPANT psell on PSELL.ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK               ");
		sd.append(" inner join PARAMETER_TABLE pstate on pstate.PARAMETER_TABLE_PK = MO.OPERATION_STATE                 ");
		sd.append(" where 0=0                                                                                           ");
		
		if(Validations.validateIsNotNull(filter.getRegistryDate()))
			sd.append(" and MO.ID_ASSIGNMENT_PROCESS_FK IN (:lstAssignmentProcess)											");
		
		if(Validations.validateIsNotNull(filter.getLstIdMechanismOperationToOpen())
				&& Validations.validateIsPositiveNumber(filter.getLstIdMechanismOperationToOpen().size()))
			sd.append(" and MO.ID_MECHANISM_OPERATION_PK in (:lstMechanismOperation)");
		
		sd.append(" order by MO.ID_MECHANISM_OPERATION_PK");
		
		List<MechanismOperationForOpenTO> lst=new ArrayList<>();
		try {
			Query q=em.createNativeQuery(sd.toString());
			if(Validations.validateIsNotNull(filter.getRegistryDate())){
				List<Long> lstIdAssignmentProcess= lstIdAssignmentProcessByDate(filter.getRegistryDate());
				if (lstIdAssignmentProcess == null || lstIdAssignmentProcess.isEmpty() || lstIdAssignmentProcess.size()==0)
					return lst;
				q.setParameter("lstAssignmentProcess", lstIdAssignmentProcess);
			}
			if(Validations.validateIsNotNull(filter.getLstIdMechanismOperationToOpen())
					&& Validations.validateIsPositiveNumber(filter.getLstIdMechanismOperationToOpen().size())){
				q.setParameter("lstMechanismOperation", filter.getLstIdMechanismOperationToOpen());
			}
			
			List<Object[]> lstObj=q.getResultList();
			MechanismOperationForOpenTO mcnOpen;
			for(Object[] row:lstObj){
				mcnOpen=new MechanismOperationForOpenTO();
				mcnOpen.setId(row[0] != null ? Long.parseLong(row[0].toString()) : 0L);
				mcnOpen.setOperationDate(row[1] != null ? row[1].toString() : "---");
				mcnOpen.setBallotNumber(row[2] != null ? Long.parseLong(row[2].toString()) : 0L);
				mcnOpen.setSequential(row[3] != null ? Long.parseLong(row[3].toString()) : 0L);
				mcnOpen.setModalityText(row[4] != null ? row[4].toString() : "---");
				mcnOpen.setIdSecurityCodePk(row[5] != null ? row[5].toString() : "---");
				mcnOpen.setCurrencyText(row[6] != null ? row[6].toString() : "---");
				mcnOpen.setParticipantSeller(row[7] != null ? row[7].toString() : "---");
				mcnOpen.setParticipantBuyer(row[8] != null ? row[8].toString() : "---");
				mcnOpen.setStateText(row[9] != null ? row[9].toString() : "---");
				mcnOpen.setStateOperation(row[10] != null ? Integer.parseInt(row[10].toString()) : 0);
				lst.add(mcnOpen);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException();
		}
		return lst;
	}
	
	/**
	 * obtener los id de los Procesos de asignacion por criterio de fecha de registro
	 * @param dateParameter
	 * @return
	 * @throws ServiceException
	 */
	public List<Long> lstIdAssignmentProcessByDate(Date dateParameter) throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" select ap ");
		sd.append(" from AssignmentProcess ap");
		sd.append(" where trunc(ap.settlementDate) >= :dateParameter");
		sd.append("   and trunc(ap.settlementDate) < ( :dateParameter +1 )");
		sd.append("	  and ap.assignmentState = ").append(AssignmentProcessStateType.CLOSED.getCode());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("dateParameter", dateParameter);
		List<AssignmentProcess> lstAssProcess = findListByQueryString(sd.toString(), parameters);
		List<Long> lst = new ArrayList<>();
		for (AssignmentProcess ap : lstAssProcess) {
			lst.add(ap.getIdAssignmentProcessPk());
		}
		return lst;
	}
	
	/**
	 * issue 1105: metodo para obtener los encadenamientos en estado registrado de un determinado paricipante y una fecha en concreto 
	 * @param searchChainedOperationTO
	 * @return
	 */
	public List<ChainedHolderOperation> getChainHolderRequest(SearchChainedOperationTO searchChainedOperationTO) {
		List<ChainedHolderOperation> lst = new ArrayList<>();
		StringBuilder sd =new StringBuilder();
		sd.append(" select cho");
		sd.append(" from ChainedHolderOperation cho");
		sd.append(" where 0=0");
		sd.append("	and cho.chaintState = "+ChainedOperationStateType.REGISTERED.getCode());
		sd.append(" and cho.participant.idParticipantPk = :idParticipantPk");
		sd.append("	and trunc(cho.registerDate) = :dateParameter");
		try {
			Query q=em.createQuery(sd.toString());
			q.setParameter("idParticipantPk", searchChainedOperationTO.getIdParticipantPk());
			q.setParameter("dateParameter", searchChainedOperationTO.getInitialDate());
			q.getResultList();	
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("::: Se ha producido un error en el metodo AccountAssignmentService.getLstOperationsWithNoSecurities "+e.getMessage());
		}
		return lst;
	}
	
	/**
	 * issue 1105: 
	 * metodo que obtiene las operaciones con falta de valores de una determinada fecha y un participante
	 * @param dateOperation
	 * @param idParticipantPk
	 * @param codeModality
	 * @return
	 */
	public List<Object[]> getLstOperationsWithNoSecurities(String dateOperation, Long idParticipantPk, Long codeModality){
		List<Object[]> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append(" select                                                                                                          ");
		sd.append("   MO.OPERATION_NUMBER,                                                                                          ");
		sd.append("   MO.OPERATION_DATE FECHA,                                                                                      ");
		sd.append("   MO.BALLOT_NUMBER ||'-'|| MO.sequential PAPELETA_SECUENCIAL,                                                   ");
		sd.append("   MO.ID_SECURITY_CODE_FK CLASE_CLAVE_VALOR,                                                                     ");
		sd.append("   SAM.MARKET_QUANTITY CANTIDAD_TITULAR,                                                                         ");
		
		sd.append("   case                                                                                                          ");
		sd.append("   when                                                                                                          ");
		// issue 1172: se adiciona SAO.CHAINED_QUANTITY para calcular la cantidad faltante
		sd.append("     NVL((select SUM(SAM.MARKET_QUANTITY - (HMB.AVAILABLE_BALANCE + SAO.CHAINED_QUANTITY )) from HOLDER_MARKETFACT_BALANCE HMB             ");
		sd.append("         where (HMB.MARKET_RATE = SAM.MARKET_RATE                                                                ");
		sd.append("           and SE.INSTRUMENT_TYPE=124 or HMB.MARKET_PRICE = SAM.MARKET_PRICE                                     ");
		sd.append("           and SE.INSTRUMENT_TYPE=399)                                                                           ");
		sd.append("           and HMB.ID_HOLDER_ACCOUNT_FK = HAO.ID_HOLDER_ACCOUNT_FK                                               ");
		sd.append("           and HMB.IND_ACTIVE=1                                                                                  ");
		sd.append("           and HMB.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK),SAM.MARKET_QUANTITY) < 0                        ");
		sd.append("   then 0                                                                                                        ");
		sd.append("   else                                                                                                          ");
		// issue 1172: se adiciona SAO.CHAINED_QUANTITY para calcular la cantidad faltante
		sd.append("     NVL((select SUM(SAM.MARKET_QUANTITY - (HMB.AVAILABLE_BALANCE + SAO.CHAINED_QUANTITY )) from HOLDER_MARKETFACT_BALANCE HMB             ");
		sd.append("         where (HMB.MARKET_RATE = SAM.MARKET_RATE                                                                ");
		sd.append("           and SE.INSTRUMENT_TYPE=124 or HMB.MARKET_PRICE = SAM.MARKET_PRICE                                     ");
		sd.append("           and SE.INSTRUMENT_TYPE=399)                                                                           ");
		sd.append("           and HMB.ID_HOLDER_ACCOUNT_FK = HAO.ID_HOLDER_ACCOUNT_FK                                               ");
		sd.append("           and HMB.IND_ACTIVE=1                                                                                  ");
		sd.append("           and HMB.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK),SAM.MARKET_QUANTITY)                            ");
		sd.append("   end CANTIDAD_FALTANTE,                                                                                        ");
		
		sd.append("   (select MNEMONIC from PARTICIPANT where ID_PARTICIPANT_PK = MO.ID_SELLER_PARTICIPANT_FK) as PARTICIPANT_SELL, ");
		sd.append("   (select MNEMONIC from PARTICIPANT where ID_PARTICIPANT_PK = MO.ID_BUYER_PARTICIPANT_FK) as PARTICIPANT_BUY,   ");
		
		sd.append("   SO.SETTLEMENT_AMOUNT as AMOUNT_LIQ,                                                                           ");
		
		sd.append("   (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = MO.OPERATION_STATE ) as ESTADO_OPERACION, ");
		sd.append("   SE.SECURITY_CLASS AS CLASE_VALOR,                                                        						");
		// esto detecta si se trata de inconsistencia de Hechos de Mercado (1=SI  0=NO)
		sd.append("   CASE                                                                         									");
		sd.append("     WHEN (SELECT SUM( HMB.AVAILABLE_BALANCE) FROM HOLDER_MARKETFACT_BALANCE HMB									");
		sd.append("      WHERE HMB.ID_HOLDER_ACCOUNT_FK = HAO.ID_HOLDER_ACCOUNT_FK                 									");
		sd.append("       AND HMB.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK                     									");
		sd.append("       AND HMB.IND_ACTIVE=1                                                     									");
		sd.append("       AND (HMB.MARKET_RATE <> sam.MARKET_RATE AND SE.INSTRUMENT_TYPE=124       									");
		sd.append("       OR HMB.MARKET_PRICE <> SAM.MARKET_PRICE AND SE.INSTRUMENT_TYPE=399       									");
		sd.append("       OR HMB.MARKET_DATE <> saM.MARKET_DATE)) IS NOT NULL                      									");
		sd.append("     THEN 1                                                                     									");
		sd.append("     ELSE 0 END ES_INCONSITENCIA_HM											   									");
		
		sd.append(" from  HOLDER_ACCOUNT_OPERATION HAO, MECHANISM_OPERATION MO, SETTLEMENT_ACCOUNT_OPERATION SAO,                   ");
		sd.append("       SETTLEMENT_ACCOUNT_MARKETFACT SAM, SETTLEMENT_OPERATION SO, security SE                                   ");
		sd.append(" where HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                              ");
		sd.append("   and SAO.ID_SETTLEMENT_ACCOUNT_PK =SAM.ID_SETTLEMENT_ACCOUNT_FK                                                ");
		sd.append("   and MO.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK                                                           ");
		sd.append("   and SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK                                   ");
		sd.append("   and MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK                                               ");
		sd.append("   and SO.ID_SETTLEMENT_OPERATION_PK = SAO.ID_SETTLEMENT_OPERATION_FK                                            ");
		sd.append("   and HAO.OPERATION_PART = SO.OPERATION_PART                                                                    ");
		sd.append("   and MO.OPERATION_STATE = 612                                                                                  ");
		sd.append("   and HAO.HOLDER_ACCOUNT_STATE = 623                                                                            ");
		sd.append("   and HAO.role=2                                                                                                ");
		sd.append("   and HAO.OPERATION_PART=1                                                                                      ");
		sd.append("   and MO.ID_NEGOTIATION_MECHANISM_FK=1                                                                          ");
		sd.append("   and SAO.STOCK_REFERENCE is null                                                                               ");
		sd.append("   and SAO.STOCK_QUANTITY>SAO.CHAINED_QUANTITY                                                                   ");
		sd.append("   and SAM.IND_ACTIVE=1                                                                                          ");
		sd.append("   and SO.SETTLEMENT_DATE = TO_DATE('"+dateOperation+"','DD/MM/YYYY')                                            ");
		sd.append("   and HAO.ID_INCHARGE_STOCK_PARTICIPANT = :idParticipantPk                                                      ");
		sd.append("   and MO.ID_NEGOTIATION_MODALITY_FK = :codeModality                                                      		");
		sd.append(" order by 1,2																									");
		
		try {
			Query q=em.createNativeQuery(sd.toString());
			q.setParameter("idParticipantPk", idParticipantPk);
			q.setParameter("codeModality", codeModality);
			lst=q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("::: Se ha producido un error en el metodo AccountAssignmentService.getLstOperationsWithNoSecurities "+e.getMessage());
		}
		return lst;
	}
	
	/**
	 * To get the HolderAccountOperations .
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param idParticipantPk the id participant pk
	 * @param lstRoles the lst roles
	 * @param lstStates the lst states
	 * @param operationPart the operation part
	 * @param blDepositary the bl depositary
	 * @return List
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getHolderAccountOperations(Long idMechanismOperationPk, Long idParticipantPk
																	, List<Integer> lstRoles, List<Integer> lstStates, Integer operationPart, 
																	boolean blDepositary) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT distinct hao , hao.holderAccount.idHolderAccountPk");
		sbQuery.append("	from HolderAccountOperation hao");
		sbQuery.append("	INNER JOIN FETCH hao.inchargeFundsParticipant");
		sbQuery.append("	INNER JOIN FETCH hao.inchargeStockParticipant");
		sbQuery.append("	WHERE hao.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk");
		if(idParticipantPk!=null){
			sbQuery.append("	AND hao.assignmentRequest.participantAssignment.participant.idParticipantPk = :idParticipantPk");
			parameters.put("idParticipantPk", idParticipantPk);
		}
		sbQuery.append("	AND hao.assignmentRequest.role in (:roles)");
		sbQuery.append("	AND hao.operationPart = :operationPart");
		sbQuery.append("	AND hao.holderAccountState in (:states) ");
		if(!blDepositary){
			sbQuery.append("	AND (hao.inchargeState is null OR hao.inchargeState <> :stateConfirm)");
			parameters.put("stateConfirm", HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
		}
			
		parameters.put("idMechanismOperationPk", idMechanismOperationPk);
		
		parameters.put("operationPart", operationPart);
		parameters.put("roles", lstRoles);
		parameters.put("states", lstStates);
		
		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
	}
	

	/**
	 * Search incharge requests.
	 *
	 * @param searchAssignmentRequestTO the search assignment request to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")	
	public List<AccountAssignmentTO> searchInchargeRequests(SearchAssignmentRequestTO searchAssignmentRequestTO){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT ");
		sbQuery.append("	 hao.idHolderAccountOperationPk, "); //0
		sbQuery.append("	 mo.idMechanismOperationPk, "); //1
		sbQuery.append("	 mo.operationDate, ");//2
		sbQuery.append("	 mo.operationNumber, ");//3
		sbQuery.append("	 mo.ballotNumber, mo.sequential, ");// 4 5 
		sbQuery.append("	 mo.securities.idSecurityCodePk,");//6
		sbQuery.append("	 mo.buyerParticipant.idParticipantPk,");//7
		sbQuery.append("	 mo.buyerParticipant.mnemonic,");//8
		sbQuery.append("	 mo.buyerParticipant.description,");
		sbQuery.append("	 mo.sellerParticipant.idParticipantPk,");
		sbQuery.append("	 mo.sellerParticipant.mnemonic,");
		sbQuery.append("	 mo.sellerParticipant.description,");//12
		sbQuery.append("	 hao.role,");//13
		sbQuery.append("	 (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = mo.cashSettlementCurrency), ");
		sbQuery.append("	 hao.settlementAmount, ");//15
		sbQuery.append("	 hao.inchargeState, ");//16
		sbQuery.append("	 (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = hao.inchargeState), ");//17
		sbQuery.append("	 hao.holderAccount.idHolderAccountPk, ");//18
		sbQuery.append("	 mo.securities.instrumentType, ");//19
		sbQuery.append("	 hao.stockQuantity, ");//20
		sbQuery.append("	 ho.idHolderPk ");//21 
		
		sbQuery.append("	FROM HolderAccountOperation hao ");
		sbQuery.append("	inner join hao.holderAccount ha");
		sbQuery.append("	inner join ha.holderAccountDetails had");
		sbQuery.append("	inner join had.holder ho");
		sbQuery.append("	inner join hao.mechanismOperation mo ");
		sbQuery.append("	inner join mo.securities se");				
		sbQuery.append("	inner join mo.buyerParticipant bp");
		sbQuery.append("	inner join mo.sellerParticipant sp");

		
		sbQuery.append("	WHERE mo.mechanisnModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	AND mo.mechanisnModality.id.idNegotiationModalityPk = :idNegotiationModalityPk");
		
		sbQuery.append("	AND hao.indIncharge = :indIncharge");
		sbQuery.append("	AND mo.operationState in (:states)");
		sbQuery.append("	AND hao.operationPart = :operationPart");

		if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getStateSelected())){
			sbQuery.append("	AND hao.inchargeState = :inchargeState");
			parameters.put("inchargeState", searchAssignmentRequestTO.getStateSelected());
			
			if(HolderAccountOperationStateType.REJECTED_INCHARGE_STATE.getCode().equals(searchAssignmentRequestTO.getStateSelected())){
				sbQuery.append("	AND hao.holderAccountState = :holAccState ");
				parameters.put("holAccState", HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode());
			}else{
				sbQuery.append("	AND hao.holderAccountState in (:holAccState, :holAccRegState)");
				parameters.put("holAccRegState", HolderAccountOperationStateType.REGISTERED.getCode());
				parameters.put("holAccState", HolderAccountOperationStateType.CONFIRMED.getCode());		
			}			
		}else{
			sbQuery.append("	AND (hao.holderAccountState in (:holAccConfiState, :holAccRegState)");
			sbQuery.append("	OR	(hao.holderAccountState = :holAccModiState AND hao.inchargeState = :inchargeReject) )");
			
			parameters.put("inchargeReject", HolderAccountOperationStateType.REJECTED_INCHARGE_STATE.getCode());
			parameters.put("holAccRegState", HolderAccountOperationStateType.REGISTERED.getCode());
			parameters.put("holAccModiState", HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode());
			parameters.put("holAccConfiState", HolderAccountOperationStateType.CONFIRMED.getCode());	
		}
		
		if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getRoleSelected()) &&
				ComponentConstant.PURCHARSE_ROLE.equals(searchAssignmentRequestTO.getRoleSelected())){
			sbQuery.append("	AND ( hao.role = :role");
			sbQuery.append("	AND (hao.inchargeFundsParticipant.idParticipantPk = :inchargeParticipant");
			sbQuery.append("	OR hao.inchargeStockParticipant.idParticipantPk = :inchargeParticipant) )");
			
			if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getParticipantTraderSelected())){
				sbQuery.append("	AND hao.mechanismOperation.buyerParticipant.idParticipantPk = :traderParticipant");
				parameters.put("traderParticipant", searchAssignmentRequestTO.getParticipantTraderSelected());
			}
			
		}else if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getRoleSelected()) &&
				ComponentConstant.SALE_ROLE.equals(searchAssignmentRequestTO.getRoleSelected())){
			sbQuery.append("	AND ( hao.role = :role");
			sbQuery.append("		AND (hao.inchargeFundsParticipant.idParticipantPk = :inchargeParticipant");
			sbQuery.append("		OR hao.inchargeStockParticipant.idParticipantPk = :inchargeParticipant) )");
			
			if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getParticipantTraderSelected())){
				sbQuery.append("	AND hao.mechanismOperation.sellerParticipant.idParticipantPk = :traderParticipant");
				parameters.put("traderParticipant", searchAssignmentRequestTO.getParticipantTraderSelected());
			}
			
		}else{
			sbQuery.append("	AND (hao.inchargeFundsParticipant.idParticipantPk = :inchargeParticipant");
			sbQuery.append("		OR hao.inchargeStockParticipant.idParticipantPk = :inchargeParticipant)");
			
			if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getParticipantTraderSelected())){
				sbQuery.append("	AND (hao.mechanismOperation.sellerParticipant.idParticipantPk = :traderParticipant");
				sbQuery.append("		OR	hao.mechanismOperation.buyerParticipant.idParticipantPk = :traderParticipant)");
				parameters.put("traderParticipant", searchAssignmentRequestTO.getParticipantTraderSelected());
			}
		}
		
		if(ComponentConstant.ZERO.equals(searchAssignmentRequestTO.getDateType())){
			sbQuery.append("	AND TRUNC(hao.mechanismOperation.operationDate) between :initialDate AND :endDate");
		} else {
			sbQuery.append("	AND TRUNC(hao.mechanismOperation.cashSettlementDate) between :initialDate AND :endDate");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAssignmentRequestTO.getSecurity().getIdSecurityCodePk())){
			sbQuery.append("	AND mo.securities.idSecurityCodePk = :idSecurityCodePk");
			parameters.put("idSecurityCodePk", searchAssignmentRequestTO.getSecurity().getIdSecurityCodePk());
		}
		
		if(Validations.validateIsNotNull(searchAssignmentRequestTO.getHolder())&&
				Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getHolder().getIdHolderPk())){
			sbQuery.append("	AND ho.idHolderPk = :idHolderPk");
			parameters.put("idHolderPk", searchAssignmentRequestTO.getHolder().getIdHolderPk());
		}
		
//		
		sbQuery.append("	ORDER BY hao.mechanismOperation.operationDate DESC"); 
		
		List<Integer> lstStates = new ArrayList<Integer>();
		lstStates.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		lstStates.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		lstStates.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		lstStates.add(MechanismOperationStateType.TERM_SETTLED.getCode());
		
		parameters.put("idNegotiationMechanismPk", searchAssignmentRequestTO.getNegoMechanismSelected());
		parameters.put("idNegotiationModalityPk", searchAssignmentRequestTO.getNegoModalitySelected());
		parameters.put("inchargeParticipant", searchAssignmentRequestTO.getParticipantChargedSelected());
		parameters.put("initialDate", searchAssignmentRequestTO.getInitialDate());
		parameters.put("endDate", searchAssignmentRequestTO.getEndDate());
		parameters.put("indIncharge", ComponentConstant.ONE);
		parameters.put("operationPart", ComponentConstant.CASH_PART);
		parameters.put("states", lstStates);
		
		if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getRoleSelected())){
			parameters.put("role", searchAssignmentRequestTO.getRoleSelected());
		}
		
		List<Object[]> arrObjs = findListByQueryString(sbQuery.toString(), parameters);
		List<AccountAssignmentTO> lstAccountAssignments = new ArrayList<AccountAssignmentTO>();
		for(Object[] arrObject : arrObjs){
			if(new Integer(arrObject[13].toString()).equals(ParticipantRoleType.BUY.getCode())){
				if(new Long(arrObject[7].toString()).equals(searchAssignmentRequestTO.getParticipantChargedSelected()))
					continue;
			}else if(new Integer(arrObject[13].toString()).equals(ParticipantRoleType.SELL.getCode())){
				if(new Long(arrObject[10].toString()).equals(searchAssignmentRequestTO.getParticipantChargedSelected()))
					continue;
			}
			if(Validations.validateIsNotNullAndPositive(searchAssignmentRequestTO.getParticipantTraderSelected())){
				if(new Integer(arrObject[13].toString()).equals(ParticipantRoleType.BUY.getCode())){
					if(!new Long(arrObject[7].toString()).equals(searchAssignmentRequestTO.getParticipantTraderSelected()))
						continue;
				}else if(new Integer(arrObject[13].toString()).equals(ParticipantRoleType.SELL.getCode())){
					if(!new Long(arrObject[10].toString()).equals(searchAssignmentRequestTO.getParticipantTraderSelected()))
						continue;
				}
			}
			AccountAssignmentTO accountAssignmentTO = new AccountAssignmentTO();
			accountAssignmentTO.setIdHolderAccountOperation((Long)arrObject[0]);
			accountAssignmentTO.setIdMechanismOperationPk((Long)arrObject[1]);
			accountAssignmentTO.setOperationDate((Date)arrObject[2]);
			accountAssignmentTO.setOperationNumber((Long)arrObject[3]);
			accountAssignmentTO.setBallotNumber((Long)arrObject[4]);
			accountAssignmentTO.setSequential((Long)arrObject[5]);
			accountAssignmentTO.setIdSecurityCodePk((String)arrObject[6]);
			accountAssignmentTO.setIdPurchaseParticipant((Long)arrObject[7]);
			accountAssignmentTO.setPurchaseParticipantNemonic((String)arrObject[8]);
			accountAssignmentTO.setPurchaseParticipantDescription((String)arrObject[9]);
			accountAssignmentTO.setIdSaleParticipant((Long)arrObject[10]);
			accountAssignmentTO.setSaleParticipantNemonic((String)arrObject[11]);
			accountAssignmentTO.setSaleParticipantDescription((String)arrObject[12]);
			accountAssignmentTO.setRole((Integer)arrObject[13]);
			accountAssignmentTO.setCurrencyDescription((String)arrObject[14]);
			accountAssignmentTO.setAccountAmount((BigDecimal)arrObject[15]);
			accountAssignmentTO.setInchargeState((Integer)arrObject[16]);
			accountAssignmentTO.setInchargeStateDescription((String)arrObject[17]);
			//accountAssignmentTO.setIdSettlementAccountOperation((Long)arrObject[18]);
			accountAssignmentTO.setIdHolderAccount((Long)arrObject[18]);
			accountAssignmentTO.setInstrumentType((Integer)arrObject[19]);
			accountAssignmentTO.setAccountStockQuantity((BigDecimal)arrObject[20]);
			accountAssignmentTO.setIdHolderPk((Long)arrObject[21]);
			lstAccountAssignments.add(accountAssignmentTO);
		}
		return lstAccountAssignments;
	}
	
	/**
	 * Gets the settlement account detail.
	 *
	 * @param idSettlementAccount the id settlement account
	 * @return the settlement account detail
	 */
	public Object[] getSettlementAccountDetail(Long idSettlementAccount) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hao, "); // 0
		sbQuery.append("	hao.inchargeFundsParticipant.idParticipantPk, ");//1
		sbQuery.append("	hao.inchargeStockParticipant.idParticipantPk, ");//2
		sbQuery.append("	hao.holderAccount.accountNumber, ");//3
		sbQuery.append("	mm.negotiationMechanism.idNegotiationMechanismPk, ");//4
		sbQuery.append("	mm.negotiationModality.idNegotiationModalityPk, ");//5
		sbQuery.append("	mo.indTermSettlement, ");//6
		sbQuery.append("	mo.cashSettlementDate, ");//7
		sbQuery.append("	mo.idMechanismOperationPk, ");//8
		sbQuery.append("	mo.buyerParticipant.idParticipantPk, ");//9
		sbQuery.append("	mo.sellerParticipant.idParticipantPk, ");//10
		sbQuery.append("	mo.securities.instrumentType ");//11
		sbQuery.append("	from HolderAccountOperation hao ");
		sbQuery.append("	INNER JOIN hao.mechanismOperation mo");
		sbQuery.append("	INNER JOIN mo.mechanisnModality mm");
		sbQuery.append("	WHERE hao.idHolderAccountOperationPk = :idSettlementAccount");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSettlementAccount", idSettlementAccount);
		return (Object[])query.getSingleResult();
	}
	
	/**
	 * To get AssignmentRequest objects list.
	 *
	 * @param idParticipantAssignmentPk the id participant assignment pk
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param lstRoles the lst roles
	 * @return List<AssignmentRequest>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AssignmentRequest> getListAssignmentRequest(Long idParticipantAssignmentPk, Long idMechanismOperationPk, List<Integer> lstRoles) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ar FROM AssignmentRequest ar");
		sbQuery.append("	WHERE ar.participantAssignment.idParticipantAssignmentPk = :idParticipantAssignmentPk");
		sbQuery.append("	AND ar.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk");
		sbQuery.append("	AND ar.role in (:roles)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantAssignmentPk", idParticipantAssignmentPk);
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		query.setParameter("roles", lstRoles);
		return (List<AssignmentRequest>)query.getResultList();
	}
	
	/**
	 * to get the HolderAccountoperations .
	 *
	 * @param idHolderAccountOperationRefPk the id holder account operation ref pk
	 * @return List<HolderAccountOperation>
	 * @throws ServiceException the service exception
	 */
	public HolderAccountOperation getHolderAccountOperationTermPart(Long idHolderAccountOperationRefPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hao FROM HolderAccountOperation hao");
			sbQuery.append("	WHERE hao.refAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperationPk");
			sbQuery.append("	and hao.operationPart = :operationPart");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountOperationPk", idHolderAccountOperationRefPk);
			query.setParameter("operationPart", OperationPartType.TERM_PART.getCode());
			return (HolderAccountOperation)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	/**
	 * Gets the holder account operations by role.
	 *
	 * @param operationId the operation id
	 * @param role the role
	 * @param part the part
	 * @return the holder account operations by role
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountOperation> getHolderAccountOperationsByRole(Long operationId, Integer role, Integer part) throws ServiceException{
		HashMap<String, Object> parameters = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hao FROM HolderAccountOperation hao");
		sbQuery.append("	inner join hao.holderAccount ha ");
		sbQuery.append("	WHERE hao.mechanismOperation.idMechanismOperationPk = :operationId ");
		sbQuery.append("	and hao.operationPart = :operationPart ");
		sbQuery.append("	and hao.holderAccountState in (:accountState) ");
		sbQuery.append("	and hao.role = :role order by ha.accountNumber desc ");
		parameters.put("operationId", operationId);
		parameters.put("operationPart", part);
		parameters.put("role", role);
		parameters.put("accountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		return (List<HolderAccountOperation>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * to closeParticipantAssignmentProcess.
	 *
	 * @param idParticipantAssignment the id participant assignment
	 * @param state the state
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateParticipantAssignment(Long idParticipantAssignment, Integer state, LoggerUser loggerUser) throws ServiceException{	
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" Update ParticipantAssignment set assignmentState = :assignmentState ");
		sbQuery.append(" , lastModifyApp = :lastModifyApp ");
		sbQuery.append(" , lastModifyDate = :lastModifyDate ");
		sbQuery.append(" , lastModifyIp = :lastModifyIp ");
		sbQuery.append(" , lastModifyUser = :lastModifyUser ");
		sbQuery.append(" where idParticipantAssignmentPk = :partiAssPK ");
		Query query2 = em.createQuery(sbQuery.toString());
		query2.setParameter("assignmentState", state);
		query2.setParameter("partiAssPK",  idParticipantAssignment);
		query2.setParameter("lastModifyApp",  loggerUser.getIdPrivilegeOfSystem());
		query2.setParameter("lastModifyDate",  loggerUser.getAuditTime());
		query2.setParameter("lastModifyIp",  loggerUser.getIpAddress());
		query2.setParameter("lastModifyUser",  loggerUser.getUserName());
		query2.executeUpdate();
	} 
	
		
	/**
	 * to closeGeneralAssignmentProcess.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void closeAssignmentProcess(Long assignmentProcessId, LoggerUser loggerUser) throws ServiceException{
		try {
			StringBuffer sbQuery = new StringBuffer();
			//Changing the state of AssignmentProcess
			sbQuery.append(" Update AssignmentProcess assProc set assProc.assignmentState = :assignmentState ");
			sbQuery.append(" , lastModifyApp = :lastModifyApp ");
			sbQuery.append(" , lastModifyDate = :lastModifyDate ");
			sbQuery.append(" , lastModifyIp = :lastModifyIp ");
			sbQuery.append(" , lastModifyUser = :lastModifyUser ");
			sbQuery.append(" where assProc.idAssignmentProcessPk = :assProcPK ");
			Query query1 = em.createQuery(sbQuery.toString());
			query1.setParameter("assProcPK",  assignmentProcessId);
			query1.setParameter("assignmentState", AssignmentProcessStateType.CLOSED.getCode());
			query1.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
			query1.setParameter("lastModifyDate", loggerUser.getAuditTime());
			query1.setParameter("lastModifyIp", loggerUser.getIpAddress());
			query1.setParameter("lastModifyUser", loggerUser.getUserName());
			query1.executeUpdate();
		} catch (Exception ex) {		
			throw new ServiceException(ErrorServiceType.CLOSE_ASSIGNMENT_PROCESS);						
		}
	}
	
	/**
	 * to close GeneralAssignmentProcess related ParticipantAssignments.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void closeParticipantAssignments(Long assignmentProcessId, LoggerUser loggerUser) throws ServiceException{
		try {
			StringBuffer sbQuery = new StringBuffer();
			//Changing the state of ParticipantAssignment
			sbQuery.append("Update ParticipantAssignment partiAss set partiAss.assignmentState = :assignmentState ");
			sbQuery.append(" , lastModifyApp = :lastModifyApp ");
			sbQuery.append(" , lastModifyDate = :lastModifyDate ");
			sbQuery.append(" , lastModifyIp = :lastModifyIp ");
			sbQuery.append(" , lastModifyUser = :lastModifyUser ");
			sbQuery.append(" where partiAss.assignmentProcess.idAssignmentProcessPk = :assignmentPK");
			
			Query query2 = em.createQuery(sbQuery.toString());
			query2.setParameter("assignmentPK",  assignmentProcessId);
			query2.setParameter("assignmentState", ParticipantAssignmentStateType.CLOSED.getCode());
			query2.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
			query2.setParameter("lastModifyDate", loggerUser.getAuditTime());
			query2.setParameter("lastModifyIp", loggerUser.getIpAddress());
			query2.setParameter("lastModifyUser", loggerUser.getUserName());
			query2.executeUpdate();
		} catch (Exception ex) {
			throw new ServiceException(ErrorServiceType.CLOSE_PARTICIPANT_ASSIGNMENTS);			
		}
	}
	
	/**
	 * Complete holder account assignment.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void completeHolderAccountAssignment(Long idAssignmentProcess, LoggerUser loggerUser) throws ServiceException, AccountAssignmentException{
		try {
			List<MechanismOperation> lstPendingOperations = getListPendingAssigmentRequest(idAssignmentProcess);
			if (Validations.validateListIsNotNullAndNotEmpty(lstPendingOperations)){
				
				for (MechanismOperation objMechanismOperation: lstPendingOperations){				 
					AssignmentRequest buyerAssignmentRequest = null, sellerAssignmentRequest = null;
					List<AssignmentRequest> lstAssignmentRequests  = objMechanismOperation.getAssignmentRequests();
					if (Validations.validateListIsNotNullAndNotEmpty(lstAssignmentRequests)){
						for (AssignmentRequest objAssignmentRequestTemp: lstAssignmentRequests){
							if (AssignmentRequestStateType.PENDING.getCode().equals(objAssignmentRequestTemp.getRequestState())) {
								if(ComponentConstant.SALE_ROLE.equals(objAssignmentRequestTemp.getRole())){
									sellerAssignmentRequest = objAssignmentRequestTemp;
									sellerAssignmentRequest.setRegistryDate(CommonsUtilities.currentDateTime());
									sellerAssignmentRequest.setConfirmDate(CommonsUtilities.currentDateTime());
									sellerAssignmentRequest.setConfirmUser(loggerUser.getUserName());
									sellerAssignmentRequest.setRequestState(AssignmentRequestStateType.REGISTERED.getCode());
									update(sellerAssignmentRequest,loggerUser);
								}else{
									buyerAssignmentRequest = objAssignmentRequestTemp;
									buyerAssignmentRequest.setRegistryDate(CommonsUtilities.currentDateTime());
									buyerAssignmentRequest.setConfirmDate(CommonsUtilities.currentDateTime());
									buyerAssignmentRequest.setConfirmUser(loggerUser.getUserName());
									buyerAssignmentRequest.setRequestState(AssignmentRequestStateType.REGISTERED.getCode());
									update(buyerAssignmentRequest,loggerUser);
								}
							}
						}
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(sellerAssignmentRequest)){
						HolderAccountOperation cashAccount = createHolderAccountOperation(objMechanismOperation, loggerUser, ComponentConstant.CASH_PART, sellerAssignmentRequest,null);
						
						if(cashAccount.getIndAutomatedIncharge().equals(ComponentConstant.ONE)) {

							List<HolderAccount> lstOriginHolderAccount = this.checkHolderAccountInParticipant(
									objMechanismOperation.getSellerParticipant(), null, Boolean.FALSE, Boolean.TRUE);
							
							HolderAccount holderAccountOrigin = lstOriginHolderAccount.get(0);
							
							this.confirmInchargeAssignment(cashAccount, holderAccountOrigin, loggerUser);
						}
						
						generateAccountOperationData(cashAccount,objMechanismOperation,false,loggerUser);
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(buyerAssignmentRequest)){
						HolderAccountOperation cashAccount = createHolderAccountOperation(objMechanismOperation, loggerUser, ComponentConstant.CASH_PART, buyerAssignmentRequest,null);
						
						if(cashAccount.getIndAutomatedIncharge().equals(ComponentConstant.ONE)) {
							List<HolderAccount> lstOriginHolderAccount = this.checkHolderAccountInParticipant(
									objMechanismOperation.getBuyerParticipant(), null, Boolean.FALSE, Boolean.TRUE);
							
							HolderAccount holderAccountOrigin = lstOriginHolderAccount.get(0);
							
							this.confirmInchargeAssignment(cashAccount, holderAccountOrigin, loggerUser);
						}

						generateAccountOperationData(cashAccount,objMechanismOperation,false,loggerUser);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof ServiceException || ex instanceof AccountAssignmentException) {
				throw ex;
			} 
			throw new ServiceException(ErrorServiceType.COMPLETE_HOLDER_ACCOUNT_ASSIGNMENT);
		}
	}
	

	/**
	 * Confirm recorded assigments.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void confirmRecordedAssigments(Long assignmentProcessId, LoggerUser loggerUser) throws ServiceException {
		try {
			List<Object[]> lstObjsAccountOperation = getHolderAccountOperationRegistered(assignmentProcessId);
//			Set<String> operationRoleSet = new LinkedHashSet<String>();
			Set<MechanismOperation> mechanismOperationList = new LinkedHashSet<MechanismOperation>();	
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstObjsAccountOperation)){
				
				for(Object[] objAccountOperation:lstObjsAccountOperation){
					HolderAccountOperation holderAccountOperation = (HolderAccountOperation) objAccountOperation[0];
					
//					String key = holderAccountOperation.getMechanismOperation().getIdMechanismOperationPk().toString() + GeneralConstants.TWO_POINTS + holderAccountOperation.getRole().toString();							
//					operationRoleSet.add(key);
					mechanismOperationList.add(holderAccountOperation.getMechanismOperation());
					
					settlementProcessService.updateHolderAccountOperationState(holderAccountOperation.getIdHolderAccountOperationPk(),
							HolderAccountOperationStateType.CONFIRMED.getCode(), null, 
							loggerUser, ComponentConstant.CASH_PART);
					
					settlementProcessService.updateSettlementAccountByHAOState(holderAccountOperation.getIdHolderAccountOperationPk(),
							HolderAccountOperationStateType.CONFIRMED.getCode(), loggerUser, ComponentConstant.CASH_PART);
					
					Integer indTermSettlement = holderAccountOperation.getMechanismOperation().getIndTermSettlement();
					
					if(ComponentConstant.ONE.equals(indTermSettlement)){
						
						settlementProcessService.updateHolderAccountOperationState(holderAccountOperation.getIdHolderAccountOperationPk(),
								HolderAccountOperationStateType.CONFIRMED.getCode(), null, 
								loggerUser, ComponentConstant.TERM_PART);
						
						settlementProcessService.updateSettlementAccountByHAOState(holderAccountOperation.getIdHolderAccountOperationPk(),
								HolderAccountOperationStateType.CONFIRMED.getCode(), loggerUser, ComponentConstant.TERM_PART);
					}
				}
				
				for(MechanismOperation objMechanismOperation:mechanismOperationList){
					
					Long idMechanismOperation = objMechanismOperation.getIdMechanismOperationPk();
					int updated = updateAssignmentRequestState(idMechanismOperation, AssignmentRequestStateType.CONFIRMED.getCode(), null, loggerUser);
					
					if(updated > 0){
						
						if(ComponentConstant.ONE.equals(objMechanismOperation.getIndMarginGuarantee())){
							calculateCoverageAmount(idMechanismOperation, loggerUser);
						}
						
						List<SettlementOperation> settlementOperations = (List<SettlementOperation>) settlementProcessService.getSettlementOperation(idMechanismOperation,null);
						
						for(SettlementOperation settlementOperation : settlementOperations){
							settlementProcessService.updateSettlementOperationState(settlementOperation.getIdSettlementOperationPk(), 
									MechanismOperationStateType.ASSIGNED_STATE.getCode(), null, loggerUser, false);
						}
						
						settlementProcessService.updateMechanismOperationState(idMechanismOperation, MechanismOperationStateType.ASSIGNED_STATE.getCode(), loggerUser);
					}
				}
			}
		} catch (Exception ex) {
			if (ex instanceof ServiceException) {
				throw ex;
			}
			throw new ServiceException(ErrorServiceType.CONFIRM_RECORDED_ASSIGNMENTS);
		}
	}

	/**
	 * Update assignment request state.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param requestState the request state
	 * @param idRole the id role
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateAssignmentRequestState(Long idMechanismOperation, Integer requestState, Integer idRole, LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE AssignmentRequest set requestState = :requestState ");
		stringBuffer.append(" , confirmDate = :lastModifyDate ");
		stringBuffer.append(" , confirmUser = :lastModifyUser ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		if(idRole!=null){
			stringBuffer.append(" AND   role = :idRole ");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		if(idRole!=null){
			query.setParameter("idRole", idRole);
		}
		query.setParameter("requestState", requestState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}


	/**
	 * Creates the holder account operation.
	 *
	 * @param objMechanismOperation the obj mechanism operation
	 * @param loggerUser the logger user
	 * @param operationPart the operation part
	 * @param objAssignmentRequest the obj assignment request
	 * @param holderAccountOperation the holder account operation
	 * @return the holder account operation
	 * @throws ServiceException the service exception
	 */
	private HolderAccountOperation createHolderAccountOperation(MechanismOperation objMechanismOperation, LoggerUser loggerUser, 
												Integer operationPart, AssignmentRequest objAssignmentRequest, 
												HolderAccountOperation holderAccountOperation) throws ServiceException{
		Object[] params= null;
		Long idParticipant = null;
		if(ComponentConstant.SALE_ROLE.equals(objAssignmentRequest.getRole())){
			params = new Object[] {objMechanismOperation.getSellerParticipant().getDisplayCodeMnemonic()};
			idParticipant = objMechanismOperation.getSellerParticipant().getIdParticipantPk();
		}else if(ComponentConstant.PURCHARSE_ROLE.equals(objAssignmentRequest.getRole())){
			params = new Object[] {objMechanismOperation.getBuyerParticipant().getDisplayCodeMnemonic()};
			idParticipant = objMechanismOperation.getBuyerParticipant().getIdParticipantPk();
		}
		HolderAccount objHolderAccount = getHolderAccountByParticipant(idParticipant);
		
		if (Validations.validateIsNull(objHolderAccount)) {
			throw new ServiceException(ErrorServiceType.PARTICIPANT_HOLDER_ACCOUNT, params);
		}
		
		HolderAccountOperation objHolderAccountOperation = new HolderAccountOperation();
		objHolderAccountOperation.setHolderAccount(objHolderAccount);
		
		HolderAccountOperation objHolderAccountOperationAux = new HolderAccountOperation();
		objHolderAccountOperationAux.setMechanismOperation(objMechanismOperation);
		objHolderAccountOperationAux.setRole(objAssignmentRequest.getRole());
		BigDecimal totalQuantityForHolderAccount = getTotalQuantity(objHolderAccountOperationAux);
		totalQuantityForHolderAccount = objMechanismOperation.getStockQuantity().subtract(totalQuantityForHolderAccount);
		
		objHolderAccountOperation.setStockQuantity(totalQuantityForHolderAccount);
		objHolderAccountOperation.setIndIncharge(BooleanType.NO.getCode());
		objHolderAccountOperation.setIndAutomaticAsignment(BooleanType.YES.getCode());
		if (ComponentConstant.CASH_PART.equals(operationPart)) {
			objHolderAccountOperation.setRole(objAssignmentRequest.getRole());	
			BigDecimal cashSettlementAmountAux = objHolderAccountOperation.getStockQuantity().multiply(objMechanismOperation.getCashPrice()).setScale(2,RoundingMode.HALF_UP);
			objHolderAccountOperation.setSettlementAmount(cashSettlementAmountAux);			
		} else if (ComponentConstant.TERM_PART.equals(operationPart)) {
			if(ComponentConstant.SALE_ROLE.equals(objAssignmentRequest.getRole())){
				objHolderAccountOperation.setRole(ComponentConstant.PURCHARSE_ROLE);
			} else {
				objHolderAccountOperation.setRole(ComponentConstant.SALE_ROLE);
			}
			objHolderAccountOperation.setSettlementAmount(objMechanismOperation.getTermSettlementAmount());
			objHolderAccountOperation.setRefAccountOperation(holderAccountOperation);
		}
		
		if(ComponentConstant.SALE_ROLE.equals(objAssignmentRequest.getRole())){
			if(Validations.validateIsNotNullAndPositive(idepositarySetup.getIdInChargeParticipant())
					&& (Validations.validateIsNull(objMechanismOperation.getSellerParticipant().getIndDepositary()) 
					|| objMechanismOperation.getSellerParticipant().getIndDepositary().equals(ComponentConstant.ZERO))){
				Participant inchargeParticipant = this.find(Participant.class, idepositarySetup.getIdInChargeParticipant());
				
				objHolderAccountOperation.setInchargeStockParticipant(inchargeParticipant);
				objHolderAccountOperation.setInchargeFundsParticipant(inchargeParticipant);
				
				objHolderAccountOperation.setIndAutomatedIncharge(ComponentConstant.ONE);
				objHolderAccountOperation.setIndIncharge(BooleanType.YES.getCode());
				objHolderAccountOperation.setInchargeState(HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode());
			} else {
				objHolderAccountOperation.setInchargeStockParticipant(objMechanismOperation.getSellerParticipant());
				objHolderAccountOperation.setInchargeFundsParticipant(objMechanismOperation.getSellerParticipant());
				
				objHolderAccountOperation.setIndAutomatedIncharge(ComponentConstant.ZERO);
			}
		}else{
			if(Validations.validateIsNotNullAndPositive(idepositarySetup.getIdInChargeParticipant())
					&& (Validations.validateIsNull(objMechanismOperation.getBuyerParticipant().getIndDepositary()) 
					|| objMechanismOperation.getBuyerParticipant().getIndDepositary().equals(ComponentConstant.ZERO))){
				Participant inchargeParticipant = this.find(Participant.class, idepositarySetup.getIdInChargeParticipant());

				objHolderAccountOperation.setInchargeStockParticipant(inchargeParticipant);
				objHolderAccountOperation.setInchargeFundsParticipant(inchargeParticipant);

				objHolderAccountOperation.setIndAutomatedIncharge(ComponentConstant.ONE);
				objHolderAccountOperation.setIndIncharge(BooleanType.YES.getCode());
				objHolderAccountOperation.setInchargeState(HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode());
			} else {
				objHolderAccountOperation.setInchargeStockParticipant(objMechanismOperation.getBuyerParticipant());
				objHolderAccountOperation.setInchargeFundsParticipant(objMechanismOperation.getBuyerParticipant());
				
				objHolderAccountOperation.setIndAutomatedIncharge(ComponentConstant.ZERO);
			}
		}			
		objHolderAccountOperation.setOperationPart(operationPart);
		objHolderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
		objHolderAccountOperation.setRegisterUser(loggerUser.getUserName());
		objHolderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		objHolderAccountOperation.setConfirmDate(CommonsUtilities.currentDateTime());		
		objHolderAccountOperation.setMechanismOperation(objMechanismOperation);
		objHolderAccountOperation.setAssignmentRequest(objAssignmentRequest);
		
		create(objHolderAccountOperation,loggerUser);
		
		if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact()) 
				&& ComponentConstant.CASH_PART.equals(operationPart)
				&& ComponentConstant.SALE_ROLE.equals(objHolderAccountOperation.getRole())){
			objHolderAccountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
			
			AccountOperationMarketFact holAccOpeMarketFact = new AccountOperationMarketFact();
			holAccOpeMarketFact.setHolderAccountOperation(objHolderAccountOperation);
			holAccOpeMarketFact.setOperationQuantity(objHolderAccountOperation.getStockQuantity());
			holAccOpeMarketFact.setMarketDate(CommonsUtilities.currentDate());
			holAccOpeMarketFact.setMarketPrice(BigDecimal.ZERO);
			holAccOpeMarketFact.setMarketRate(BigDecimal.ZERO);
			create(holAccOpeMarketFact,loggerUser);
			
			objHolderAccountOperation.getAccountOperationMarketFacts().add(holAccOpeMarketFact);
		}
		
		return objHolderAccountOperation;
	}	
	
	/**
	 * Gets the list pending assigment request.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @return the list pending assigment request
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	private List<MechanismOperation> getListPendingAssigmentRequest(Long idAssignmentProcess) throws ServiceException{
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT MO ");
		stringBuilderSql.append(" FROM MechanismOperation MO ");
		stringBuilderSql.append(" INNER JOIN FETCH MO.sellerParticipant");
		stringBuilderSql.append(" INNER JOIN FETCH MO.buyerParticipant");
		stringBuilderSql.append(" INNER JOIN FETCH MO.assignmentRequests");
		stringBuilderSql.append(" WHERE MO.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcess");
		stringBuilderSql.append(" AND MO.operationState = :operationState ");
		stringBuilderSql.append(" and exists (select AR from AssignmentRequest AR "); 
		stringBuilderSql.append(" 			where AR.mechanismOperation.idMechanismOperationPk = MO.idMechanismOperationPk ");
		stringBuilderSql.append("			and AR.requestState = :pending) ");		
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("operationState", MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.setParameter("pending", AssignmentRequestStateType.PENDING.getCode());
		query.setParameter("idAssignmentProcess", idAssignmentProcess);
		return (List<MechanismOperation>)query.getResultList();
	}
	
	/**
	 * Gets the holder account by participant.
	 *
	 * @param idParticipant the id participant
	 * @return the holder account by participant
	 */
	@SuppressWarnings("unchecked")
	private HolderAccount getHolderAccountByParticipant(Long idParticipant){
		HolderAccount objHolderAccount = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT HAC.holderAccount ");
		stringBuffer.append(" FROM HolderAccountDetail HAC, Participant PA ");
		stringBuffer.append(" WHERE HAC.holderAccount.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and HAC.holderAccount.participant.idParticipantPk = PA.idParticipantPk ");
		stringBuffer.append(" and HAC.holder.idHolderPk = PA.holder.idHolderPk ");
		stringBuffer.append(" and HAC.holderAccount.accountGroup = :accountGroup ");
		stringBuffer.append(" and HAC.holderAccount.stateAccount = :stateAccount ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
		query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
	
		List<HolderAccount> objList = (List<HolderAccount>)query.getResultList();
		if (Validations.validateListIsNotNullAndNotEmpty(objList))
			objHolderAccount = (HolderAccount) objList.get(0);
		
		return objHolderAccount;
	}
	
	/**
	 * Gets the assignment request.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param role the role
	 * @return the assignment request
	 */
	public AssignmentRequest getAssignmentRequest(Long idMechanismOperation, Integer role) {
		AssignmentRequest objAssignmentRequest= null;
		StringBuffer stringBuffer= new StringBuffer();
		try {
			stringBuffer.append(" SELECT AR ");
			stringBuffer.append(" FROM AssignmentRequest AR ");
			stringBuffer.append(" WHERE AR.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
			stringBuffer.append(" and AR.role= :role ");
			
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idMechanismOperation", idMechanismOperation);
			query.setParameter("role", role);
			objAssignmentRequest = (AssignmentRequest) query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return objAssignmentRequest;
	}
	
	/**
	 * Gets the assignment process.
	 *
	 * @param searchAccountAssignmentTO the search account assignment to
	 * @return the assignment process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getAssignmentProcess(SearchAccountAssignmentTO searchAccountAssignmentTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ap.idAssignmentProcessPk,");
		sbQuery.append("			ap.mechanisnModality.id.idNegotiationModalityPk,");
		sbQuery.append("			ap.mechanisnModality.negotiationModality.modalityName,");
		sbQuery.append("			ap.settlementDate,");
		sbQuery.append("   			ap.assignmentState,");
		sbQuery.append("			(SELECT pt.parameterName FROM ParameterTable pt");
		sbQuery.append("				WHERE pt.parameterTablePk = ap.assignmentState),");
		sbQuery.append("			ap.mechanisnModality.settlementSchema "); //6
		sbQuery.append("	FROM AssignmentProcess ap, MechanismOperation mo");							
		sbQuery.append("	WHERE ap.mechanisnModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		sbQuery.append("	AND mo.assignmentProcess.idAssignmentProcessPk = ap.idAssignmentProcessPk");							
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAssignmentTO.getNegoModalitySelected()))
			sbQuery.append("	AND ap.mechanisnModality.id.idNegotiationModalityPk = :idNegotiationModalityPk");
		if(Validations.validateIsNotNullAndPositive(searchAccountAssignmentTO.getAssignmentProcessState()))
			sbQuery.append("	AND ap.assignmentState = :idAssignmentState");
		if(GeneralConstants.ZERO_VALUE_INTEGER.equals(searchAccountAssignmentTO.getDateType()))
			sbQuery.append("	AND TRUNC(mo.operationDate) = TRUNC(:dateSelected)");
		else
			sbQuery.append("	AND TRUNC(mo.cashSettlementDate) = TRUNC(:dateSelected)");
		if(Validations.validateIsNotNullAndPositive(searchAccountAssignmentTO.getSettlementSchemaType()))
			sbQuery.append("	AND mo.settlementSchema = :settlementSchemaTypePrm");
		
		sbQuery.append("	GROUP BY ap.idAssignmentProcessPk, ap.mechanisnModality.id.idNegotiationModalityPk,");
		sbQuery.append("	ap.mechanisnModality.negotiationModality.modalityName, ap.settlementDate, ap.assignmentState, ap.mechanisnModality.settlementSchema ");
		sbQuery.append("	ORDER BY ap.idAssignmentProcessPk DESC");		
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idNegotiationMechanismPk", searchAccountAssignmentTO.getNegoMechanismSelected());
		query.setParameter("dateSelected", searchAccountAssignmentTO.getDate());		
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAssignmentTO.getNegoModalitySelected()))
			query.setParameter("idNegotiationModalityPk", searchAccountAssignmentTO.getNegoModalitySelected());	
		if(Validations.validateIsNotNullAndPositive(searchAccountAssignmentTO.getAssignmentProcessState()))
			query.setParameter("idAssignmentState",searchAccountAssignmentTO.getAssignmentProcessState());
		if(Validations.validateIsNotNullAndPositive(searchAccountAssignmentTO.getSettlementSchemaType()))
			query.setParameter("settlementSchemaTypePrm", searchAccountAssignmentTO.getSettlementSchemaType());
		return (List<Object[]>)query.getResultList();					
	}
	
	/**
	 * Gets the assignment requests by state.
	 *
	 * @param idAssignmentProcessPk the id assignment process pk
	 * @param idParticipantAssignmentPk the id participant assignment pk
	 * @param requestState the request state
	 * @param idParticipantPk the id participant pk
	 * @return the assignment requests by state
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getAssignmentRequestsByState(Long idAssignmentProcessPk, Long idParticipantAssignmentPk,
										Integer requestState, Long idParticipantPk) throws ServiceException{
		try {
			StringBuilder sbQuery =  new StringBuilder();
			sbQuery.append("	SELECT COUNT(*)");
			sbQuery.append("	FROM AssignmentRequest ar");								
			sbQuery.append("	WHERE ar.requestState = :requestState");			
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
				sbQuery.append("	AND ar.participantAssignment.participant.idParticipantPk = :idParticipantPk");
			if(Validations.validateIsNotNullAndNotEmpty(idAssignmentProcessPk))
				sbQuery.append("	AND ar.participantAssignment.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcessPk");
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantAssignmentPk))
				sbQuery.append("	AND ar.participantAssignment.idParticipantAssignmentPk = :idParticipantAssignmentPk");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("requestState", requestState);			
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
				query.setParameter("idParticipantPk", idParticipantPk);
			if(Validations.validateIsNotNullAndNotEmpty(idAssignmentProcessPk))
				query.setParameter("idAssignmentProcessPk", idAssignmentProcessPk);
			if(Validations.validateIsNotNullAndNotEmpty(idParticipantAssignmentPk))
				query.setParameter("idParticipantAssignmentPk", idParticipantAssignmentPk);
			
			return new BigDecimal(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}		
	}
	
	/**
	 * Gets the participant assignment.
	 *
	 * @param idAssignmentProcessPk the id assignment process pk
	 * @param idParticipantPk the id participant pk
	 * @param participantAssignmentState the participant assignment state
	 * @return the participant assignment
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getParticipantAssignment(Long idAssignmentProcessPk, Long idParticipantPk, Integer participantAssignmentState) throws ServiceException{
		StringBuilder sbQuery =  new StringBuilder();
		sbQuery.append("	SELECT pa.idParticipantAssignmentPk,");
		sbQuery.append("			pa.participant.idParticipantPk,");
		sbQuery.append("			pa.participant.mnemonic,");
		sbQuery.append("			pa.participant.description,");
		sbQuery.append("			pa.assignmentState,");
		sbQuery.append("			(SELECT pt.parameterName");
		sbQuery.append("				FROM ParameterTable pt WHERE pt.parameterTablePk = pa.assignmentState)");
		sbQuery.append("	FROM ParticipantAssignment pa");
		sbQuery.append("	WHERE pa.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcessPk");	
		
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
			sbQuery.append("	AND pa.participant.idParticipantPk = :idParticipantPk");
		if(Validations.validateIsNotNullAndPositive(participantAssignmentState))
			sbQuery.append("	AND pa.assignmentState = :assignmentStatePrm");
		
		sbQuery.append("	ORDER BY pa.participant.mnemonic ASC");
		
		Query query = em.createQuery(sbQuery.toString());				
		query.setParameter("idAssignmentProcessPk", idAssignmentProcessPk);		
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
			query.setParameter("idParticipantPk", idParticipantPk);	
		if(Validations.validateIsNotNullAndPositive(participantAssignmentState))
			query.setParameter("assignmentStatePrm", participantAssignmentState);
		
		return (List<Object[]>)query.getResultList();
	}
	
	/**
	 * Gets the operations.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param idAssignmentProcessPk the id assignment process pk
	 * @param requestState the request state
	 * @return the operations
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getOperations(Long idParticipantPk, Long idAssignmentProcessPk, Integer requestState) throws ServiceException{
		StringBuilder sbQuery =  new StringBuilder();
		sbQuery.append("	SELECT mo.operationNumber,");									//0
		sbQuery.append("			mo.securities.idSecurityCodePk,");						//1
		sbQuery.append("			mo.stockQuantity,");									//2
		sbQuery.append("			so.settlementAmount,");									//3
		sbQuery.append("			mo.buyerParticipant.mnemonic,");						//4
		sbQuery.append("			mo.sellerParticipant.mnemonic,");						//5
		sbQuery.append("	 		mo.buyerParticipant.idParticipantPk,");					//6
		sbQuery.append("			mo.sellerParticipant.idParticipantPk,");				//7
		sbQuery.append("			mo.operationDate,");									//8
		sbQuery.append("			mo.idMechanismOperationPk,");							//9
		sbQuery.append("			areq.role,");											//10
		sbQuery.append("			so.settlementPrice,");									//11
		sbQuery.append("			so.settlementDate,");									//12
		sbQuery.append("			mo.buyerParticipant.description,");						//13
		sbQuery.append("			mo.sellerParticipant.description,");					//14
		sbQuery.append("			mo.ballotNumber,");										//15
		sbQuery.append("			mo.sequential, ");										//16
		sbQuery.append("			(select pt.parameterName from ParameterTable pt where pt.parameterTablePk =  so.settlementCurrency) "); //17
		sbQuery.append("	FROM MechanismOperation mo, SettlementOperation SO, AssignmentRequest areq");
		sbQuery.append("	WHERE mo.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcessPk");
		sbQuery.append("	and mo.idMechanismOperationPk = SO.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append("	AND (mo.sellerParticipant.idParticipantPk = :idParticipantPk AND areq.role = :sell");
		sbQuery.append("			OR mo.buyerParticipant.idParticipantPk = :idParticipantPk AND areq.role = :buy)");
		sbQuery.append("	AND areq.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk");
		sbQuery.append("	AND areq.mechanismOperation.operationState not in (:excludedStates) ");
		sbQuery.append("	AND areq.requestState = :requestState");
		sbQuery.append("	AND SO.operationPart = :cashPart");
		sbQuery.append("	ORDER BY mo.operationDate DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAssignmentProcessPk", idAssignmentProcessPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("sell", ComponentConstant.SALE_ROLE);
		query.setParameter("buy", ComponentConstant.PURCHARSE_ROLE);
		query.setParameter("requestState", requestState);
		query.setParameter("cashPart", ComponentConstant.CASH_PART);
		List<Integer> states= new ArrayList<Integer>();
		states.add(MechanismOperationStateType.CANCELED_STATE.getCode());
		states.add(MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
		query.setParameter("excludedStates", states);
		return (List<Object[]>)query.getResultList();
	}
	
	/**
	 * Gets the operations.
	 *
	 * @param filter the filter
	 * @return the operations
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> getOperations(RegisterAccountReassignmentTO filter) throws ServiceException{
		StringBuilder sbQuery =  new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT mo.operationNumber,");								//0
		sbQuery.append("	mo.securities.idSecurityCodePk,");						//1
		sbQuery.append("	mo.stockQuantity,");									//2
		sbQuery.append("	mo.cashSettlementAmount,");								//3
		sbQuery.append("	mo.buyerParticipant.mnemonic,");						//4
		sbQuery.append("	mo.sellerParticipant.mnemonic,");						//5
		sbQuery.append("	mo.buyerParticipant.idParticipantPk,");					//6
		sbQuery.append("	mo.sellerParticipant.idParticipantPk,");				//7
		sbQuery.append("	mo.operationDate,");									//8
		sbQuery.append("	mo.idMechanismOperationPk,");							//9
		sbQuery.append("	0,");									//10
		sbQuery.append("	mo.cashSettlementPrice,");								//11
		sbQuery.append("	mo.cashSettlementDate,");								//12
		sbQuery.append("	mo.buyerParticipant.description,");						//13
		sbQuery.append("	mo.sellerParticipant.description,");					//14
		sbQuery.append("	mo.ballotNumber,");										//15
		sbQuery.append("	mo.sequential, ");										//16
		sbQuery.append("	(select pt.parameterName from ParameterTable pt where pt.parameterTablePk =  mo.cashSettlementCurrency), "); //17
		sbQuery.append("	mm.negotiationMechanism.idNegotiationMechanismPk, "); //18
		sbQuery.append("	mm.negotiationMechanism.mechanismName, "); //19
		sbQuery.append("	mm.negotiationModality.idNegotiationModalityPk, "); //20
		sbQuery.append("	mm.negotiationModality.modalityName, "); //21
		sbQuery.append("	mo.termSettlementDate, ");//22
		sbQuery.append("	mo.cashSettlementCurrency ");	//23
		sbQuery.append(" FROM MechanismOperation mo inner join mo.mechanisnModality mm ");
		sbQuery.append(" WHERE mo.operationState = :assignedState");
		
		if(Validations.validateIsNotNull(filter.getBallotNumber())){
			sbQuery.append("	and mo.ballotNumber  = :ballotNumber");
			parameters.put("ballotNumber", filter.getBallotNumber());
		}
		if(Validations.validateIsNotNull(filter.getSequential())){
			sbQuery.append("	and mo.sequential  = :sequential");
			parameters.put("sequential", filter.getSequential());
		}
		if(Validations.validateIsNotNull(filter.getIdMechanism())){
			sbQuery.append("	and mm.negotiationMechanism.idNegotiationMechanismPk  = :idMechanism ");
			parameters.put("idMechanism", filter.getIdMechanism());
		}
		if(Validations.validateIsNotNull(filter.getIdModality())){
			sbQuery.append("	and mm.negotiationModality.idNegotiationModalityPk  = :idModality ");
			parameters.put("idModality", filter.getIdModality());
		}
		if(Validations.validateIsNotNull(filter.getIdParticipant())){
			sbQuery.append("	and exists ( select hao.inchargeStockParticipant.idParticipantPk from HolderAccountOperation hao  ");
			sbQuery.append("		         where hao.mechanismOperation.idMechanismOperationPk = mo.idMechanismOperationPk  ");
			sbQuery.append("		         and   hao.inchargeStockParticipant.idParticipantPk = :idParticipant  ");
			sbQuery.append("		         and   hao.holderAccountState = :state ) ");
			parameters.put("idParticipant", filter.getIdParticipant());
			parameters.put("state", HolderAccountOperationStateType.CONFIRMED.getCode());
		}
		if(Validations.validateIsNotNull(filter.getSettlementDate())){
			sbQuery.append("	and mo.assignmentProcess.settlementDate = :settlementDate ");
			parameters.put("settlementDate", filter.getSettlementDate());
		}
		sbQuery.append(" ORDER BY mo.operationDate DESC");
		parameters.put("assignedState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		
		List<Object[]> lstResult =  findListByQueryString(sbQuery.toString(),parameters);
		List<MechanismOperationTO> lstMechanismOperation = new ArrayList<MechanismOperationTO>();
		for (Object[] objOperation : lstResult) {
			MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
			mechanismOperationTO.setOperationNumber((Long)objOperation[0]);
			mechanismOperationTO.setIdSecurityCodePk(objOperation[1].toString());
			mechanismOperationTO.setStockQuantity((BigDecimal)objOperation[2]);
			mechanismOperationTO.setCashSettlementAmount((BigDecimal)objOperation[3]);
			mechanismOperationTO.setIdPurchaseParticipant((Long)objOperation[6]);
			mechanismOperationTO.setPurchaseParticipantNemonic((String)objOperation[4]); 
			mechanismOperationTO.setIdSaleParticipant((Long)objOperation[7]);
			mechanismOperationTO.setSaleParticipantNemonic((String)objOperation[5]); 
			mechanismOperationTO.setOperationDate((Date)objOperation[8]);
			mechanismOperationTO.setIdMechanismOperationPk((Long)objOperation[9]);
			//mechanismOperationTO.setRoleDescription(ParticipantRoleType.get((Integer)arrResult[10]).getValue());
			//mechanismOperationTO.setRole((Integer)arrResult[10]);
			mechanismOperationTO.setCashSettlementPrice((BigDecimal)objOperation[11]);
			mechanismOperationTO.setCashSettlementDate((Date)objOperation[12]);		
			mechanismOperationTO.setBallotNumber((Long)objOperation[15]);
			mechanismOperationTO.setSequential((Long)objOperation[16]);
			mechanismOperationTO.setCurrencyDescription((String)objOperation[17]);
			mechanismOperationTO.setIdMechanism((Long)objOperation[18]);
			mechanismOperationTO.setMechanismName((String)objOperation[19]);
			mechanismOperationTO.setIdModality((Long)objOperation[20]);
			mechanismOperationTO.setModalityName((String)objOperation[21]);
			mechanismOperationTO.setTermSettlementDate((Date)objOperation[22]);
			mechanismOperationTO.setSettlementCurrency((Integer)objOperation[23]);
			
			lstMechanismOperation.add(mechanismOperationTO);
		}
		return lstMechanismOperation;
	}
	
	/**
	 * Reject incharges not confirmed.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void rejectInchargesNotConfirmed(Long assignmentProcessId, LoggerUser loggerUser) throws ServiceException{
		try {
			StringBuilder sbQuery =  new StringBuilder();
			sbQuery.append("	SELECT hao ");
			sbQuery.append("	from HolderAccountOperation hao ");
			sbQuery.append("	WHERE hao.mechanismOperation.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcessPk");
			sbQuery.append("	AND hao.inchargeState = :stateRegistered");
			sbQuery.append("	AND hao.mechanismOperation.operationState not in (:cancelState ) ");
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("idAssignmentProcessPk", assignmentProcessId);
			parameters.put("stateRegistered", HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode());
			parameters.put("cancelState", MechanismOperationStateType.CANCELED_STATE.getCode());
			List<HolderAccountOperation> lstResult = (List<HolderAccountOperation>)findListByQueryString(sbQuery.toString(), parameters);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(HolderAccountOperation holAccOperation:lstResult){
					rejectInchargeAssignment(holAccOperation,loggerUser);
				}
			}
		} catch (Exception ex) {
			if (ex instanceof ServiceException) {
				throw ex;
			}
			throw new ServiceException(ErrorServiceType.REJECT_INCHARGES_NOT_CONFIRMED);
		}
	}

	/**
	 * Calculate coverage amount.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void calculateCoverageAmount(Long idMechanismOperation,LoggerUser loggerUser) throws ServiceException {
		negotiationOperationServiceBean.calculateCoverageAmount(idMechanismOperation,loggerUser);
	}

	/**
	 * Gets the list id assignment process.
	 *
	 * @param processDate the process date
	 * @return the list id assignment process
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListIdAssignmentProcess(Date processDate){
		List<Long> lstIdAssignmentProcess= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT AP.idAssignmentProcessPk FROM AssignmentProcess AP ");
		stringBuffer.append(" WHERE AP.settlementDate = :processDate ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("processDate", processDate);
		lstIdAssignmentProcess= query.getResultList();
		return lstIdAssignmentProcess;
	}

	/**
	 * Gets the list nego mechanism for participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the list nego mechanism for participant
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getListNegoMechanismForParticipant(Long idParticipantPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT nm FROM NegotiationMechanism nm");
		sbQuery.append("	WHERE nm.idNegotiationMechanismPk IN (");
		sbQuery.append("	SELECT distinct pm.mechanismModality.negotiationMechanism.idNegotiationMechanismPk");
		sbQuery.append("	FROM ParticipantMechanism pm");
		sbQuery.append("	WHERE pm.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	AND nm.indMcnProcess = :indMcn ");
		sbQuery.append("	AND pm.stateParticipantMechanism = :state ");
		sbQuery.append("	AND pm.mechanismModality.negotiationMechanism.stateMechanism = :stateMechanism");
		sbQuery.append("	)");
		sbQuery.append("	ORDER BY nm.mechanismName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ParticipantMechanismStateType.REGISTERED.getCode());
		query.setParameter("indMcn", BooleanType.YES.getCode());
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("stateMechanism", NegotiationMechanismStateType.ACTIVE.getCode());
		return (List<NegotiationMechanism>)query.getResultList();
	}

	/**
	 * Gets the list participants for mechanism modality.
	 *
	 * @param mechanismId the mechanism id
	 * @param modalityId the modality id
	 * @param accountType the account type
	 * @param indIncharge the ind incharge
	 * @return the list participants for mechanism modality
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getListParticipantsForMechanismModality(Long mechanismId, Long modalityId, Integer accountType, Integer indIncharge) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT pa FROM Participant pa");
		sbQuery.append("	WHERE pa.idParticipantPk IN (");
		sbQuery.append("	SELECT distinct pm.participant.idParticipantPk");
		sbQuery.append("	FROM ParticipantMechanism pm");
		sbQuery.append("	WHERE pm.stateParticipantMechanism = :stateParticipantMechanism ");		
		sbQuery.append("	AND pm.mechanismModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		if(modalityId!=null){
			sbQuery.append("	AND pm.mechanismModality.id.idNegotiationModalityPk = :idNegotiationModalityPk ");
			parameters.put("idNegotiationModalityPk", modalityId);
		}
		sbQuery.append("	)");
		if(accountType!=null){
			sbQuery.append("	AND pa.accountType = :accountType ");
			parameters.put("accountType", accountType);
		}
		if(indIncharge!=null){
			sbQuery.append("	AND pa.indSettlementIncharge = :indIncharge ");
			parameters.put("indIncharge", indIncharge);
		}
		sbQuery.append("	AND pa.state = :state ");
		sbQuery.append("	ORDER BY pa.mnemonic ASC");
		
		parameters.put("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		parameters.put("state", ParticipantStateType.REGISTERED.getCode());
		parameters.put("idNegotiationMechanismPk", mechanismId);
			
		return (List<Participant>)findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation getMechanismOperation(Long idMechanismOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT mo FROM MechanismOperation mo");
			sbQuery.append("	INNER JOIN FETCH mo.buyerParticipant");
			sbQuery.append("	INNER JOIN FETCH mo.sellerParticipant");
			sbQuery.append("	INNER JOIN FETCH mo.mechanisnModality mm");
			sbQuery.append("	INNER JOIN FETCH mm.negotiationMechanism");
			sbQuery.append("	INNER JOIN FETCH mm.negotiationModality");
			sbQuery.append("	INNER JOIN FETCH mo.securities");
			sbQuery.append("	WHERE mo.idMechanismOperationPk = :idMechanismOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
			return (MechanismOperation)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	/**
	 * Find holder accounts.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccounts(Long idHolderPk, Long idParticipantPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct had.holderAccount.idHolderAccountPk");
		sbQuery.append("	FROM HolderAccountDetail had");
		sbQuery.append("	WHERE had.holder.idHolderPk = :idHolderPk");
		sbQuery.append("	AND had.holderAccount.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	AND had.holderAccount.stateAccount = :state");
		sbQuery.append("	AND had.holderAccount.accountGroup = :accGroup");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPk", idHolderPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("state", HolderAccountStatusType.ACTIVE.getCode());
		query.setParameter("accGroup", HolderAccountGroupType.INVERSTOR.getCode());
		
		List<Long> lstIdHolderAccountPk = (List<Long>)query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdHolderAccountPk)){
			sbQuery = new StringBuilder();
			sbQuery.append("	SELECT distinct ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	WHERE ha.idHolderAccountPk in (:lstIdHolderAccountPk)");
			Query queryTwo = em.createQuery(sbQuery.toString());
			queryTwo.setParameter("lstIdHolderAccountPk", lstIdHolderAccountPk);
			return (List<HolderAccount>)queryTwo.getResultList();
		}else
			return null;
	}
	
	/**
	 * Find holder accounts for incharge agreement.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccountsForInchargeAgreement(Long idHolderPk, Long idParticipantPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct had.holderAccount.idHolderAccountPk");
		sbQuery.append("	FROM HolderAccountDetail had");
		sbQuery.append("	WHERE had.holder.idHolderPk = :idHolderPk");
		sbQuery.append("	AND had.holderAccount.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	AND had.holderAccount.stateAccount = :state");
		sbQuery.append("	AND had.holderAccount.accountType in( :accountType1 , :accountType2 )");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPk", idHolderPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("state", HolderAccountStatusType.ACTIVE.getCode());
		query.setParameter("accountType1", HolderAccountType.NATURAL.getCode());
		query.setParameter("accountType2", HolderAccountType.JURIDIC.getCode());
		List<Long> lstIdHolderAccountPk = (List<Long>)query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdHolderAccountPk)){
			sbQuery = new StringBuilder();
			sbQuery.append("	SELECT distinct ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	WHERE ha.idHolderAccountPk in (:lstIdHolderAccountPk)");
			query = em.createQuery(sbQuery.toString());
			query.setParameter("lstIdHolderAccountPk", lstIdHolderAccountPk);
			return (List<HolderAccount>)query.getResultList();
		}else
			return null;
	}
	
	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	WHERE ha.idHolderAccountPk = :idHolderAccountPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			return (HolderAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Gets the incharge rule.
	 *
	 * @param role the role
	 * @return the incharge rule
	 */
	@SuppressWarnings("unchecked")
	public List<InchargeRule> getInchargeRule(Integer role) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ir");
		sbQuery.append("	FROM InchargeRule ir");
		sbQuery.append("	WHERE ir.inchargeRole = :role");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("role", role);
		return (List<InchargeRule>)query.getResultList();
	}

	/**
	 * Gets the holder account operation registered.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @return the holder account operation registered
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getHolderAccountOperationRegistered(Long assignmentProcessId){
		StringBuilder sbQuery =  new StringBuilder();
		sbQuery.append("	SELECT hao, hao.inchargeFundsParticipant.idParticipantPk, hao.inchargeStockParticipant.idParticipantPk ");
		sbQuery.append("	FROM HolderAccountOperation hao ");
		sbQuery.append("	INNER JOIN FETCH hao.mechanismOperation mo");
		sbQuery.append("	WHERE mo.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcessPk");
		sbQuery.append("	AND hao.holderAccountState = :stateRegistered");
		sbQuery.append("	AND mo.operationState = :operationState");
		sbQuery.append("	AND hao.operationPart = :cashPart");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAssignmentProcessPk", assignmentProcessId);
		query.setParameter("stateRegistered", HolderAccountOperationStateType.REGISTERED.getCode());
		query.setParameter("operationState", MechanismOperationStateType.REGISTERED_STATE.getCode());
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		return (List<Object[]>)query.getResultList();
	}

	/**
	 * Gets the ind guarantees.
	 *
	 * @param idMechamismOperation the id mechamism operation
	 * @return the ind guarantees
	 */
	public Integer getIndGuarantees(Long idMechamismOperation) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT mo.indMarginGuarantee");
			sbQuery.append("	FROM MechanismOperation mo");
			sbQuery.append("	WHERE mo.idMechanismOperationPk = :idMechanismOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idMechanismOperationPk", idMechamismOperation);
			return new Integer(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return BooleanType.NO.getCode();
		}
	}
	
	/**
	 * Gets the ind term settlement.
	 *
	 * @param idMechamismOperation the id mechamism operation
	 * @return the ind term settlement
	 */
	public Integer getIndTermSettlement(Long idMechamismOperation) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT mo.indTermSettlement");
			sbQuery.append("	FROM MechanismOperation mo");
			sbQuery.append("	WHERE mo.idMechanismOperationPk = :idMechanismOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idMechanismOperationPk", idMechamismOperation);
			return new Integer(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return BooleanType.NO.getCode();
		}
	}

	/**
	 * Gets the incharge state.
	 *
	 * @param idHolderAccountOperationPk the id holder account operation pk
	 * @return the incharge state
	 */
	public Long getInchargeState(Long idHolderAccountOperationPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hao.inchargeState");
			sbQuery.append("	FROM HolderAccountOperation hao");
			sbQuery.append("	WHERE hao.idHolderAccountOperationPk = :idHolderAccountOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountOperationPk", idHolderAccountOperationPk);
			return (Long)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the holder account operation detail.
	 *
	 * @param idSettlementAccount the id settlement account
	 * @return the holder account operation detail
	 * @throws ServiceException the service exception
	 */
	public HolderAccountOperation getHolderAccountOperationDetail(Long idSettlementAccount) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hao ");
			sbQuery.append("	from HolderAccountOperation hao");
			sbQuery.append("	INNER JOIN FETCH hao.holderAccount ");
			sbQuery.append("	INNER JOIN FETCH hao.inchargeFundsParticipant");
			sbQuery.append("	INNER JOIN FETCH hao.inchargeStockParticipant");
			sbQuery.append("	LEFT JOIN FETCH hao.accountOperationMarketFacts ");
			sbQuery.append("	WHERE hao.idHolderAccountOperationPk = :idSettlementAccount");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSettlementAccount", idSettlementAccount);
			return (HolderAccountOperation)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Gets the holder account operation.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @return the holder account operation
	 */
	@SuppressWarnings("unchecked")
	private List<HolderAccountOperation> getHolderAccountOperation(Long idMechanismOperation) {
		StringBuilder sbQuery =  new StringBuilder();
		sbQuery.append("	SELECT hao");
		sbQuery.append("	FROM HolderAccountOperation hao");
		sbQuery.append("	INNER JOIN FETCH hao.holderAccount ha");
		sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
		sbQuery.append("	WHERE hao.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk");
		sbQuery.append("	AND hao.holderAccountState in ( :stateRegistered ,:stateConfirmed )");
		sbQuery.append("	AND hao.operationPart = :cashPart");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperation);
		query.setParameter("stateRegistered", HolderAccountOperationStateType.REGISTERED.getCode());
		query.setParameter("stateConfirmed", HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		return (List<HolderAccountOperation>)query.getResultList();
	}

	/**
	 * Gets the total quantity.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @return the total quantity
	 */
	private BigDecimal getTotalQuantity(HolderAccountOperation holderAccountOperation) {
		StringBuilder sbQuery =  new StringBuilder();
		sbQuery.append("	SELECT NVL(SUM(hao.stockQuantity),0)");
		sbQuery.append("	FROM HolderAccountOperation hao");
		sbQuery.append("	WHERE hao.mechanismOperation.idMechanismOperationPk = :idMechanismOperationPk");
		sbQuery.append("	AND hao.holderAccountState = :stateRegistered");
		sbQuery.append("	AND hao.operationPart = :cashPart");
		sbQuery.append("	AND hao.role = :role");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", holderAccountOperation.getMechanismOperation().getIdMechanismOperationPk());
		query.setParameter("stateRegistered", HolderAccountOperationStateType.REGISTERED.getCode());
		query.setParameter("role", holderAccountOperation.getRole());
		query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());
		return new BigDecimal(query.getSingleResult().toString());
	}


	/**
	 * Gets the reference mechanism operation.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the reference mechanism operation
	 */
	public Long getReferenceMechanismOperation(Long idMechanismOperationPk) {
		StringBuilder sbQuery =  new StringBuilder();
		sbQuery.append("	SELECT mo.referenceOperation.idMechanismOperationPk");
		sbQuery.append("	FROM MechanismOperation mo");
		sbQuery.append("	WHERE mo.idMechanismOperationPk = :idMechanismOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idMechanismOperationPk", idMechanismOperationPk);
		return (Long)query.getSingleResult(); 
		
	}

	/**
	 * Save holder account assignment.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @param indMarketFact the ind market fact
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void saveHolderAccountAssignment(HolderAccountOperation holderAccountOperation, Integer indMarketFact, LoggerUser loggerUser) throws ServiceException {
		
		//1 - validate if holder/was previously assigned
		Long idMechanismOperation = holderAccountOperation.getMechanismOperation().getIdMechanismOperationPk();
		List<HolderAccountOperation> lstExistingHolderAccountOperation = getHolderAccountOperation(idMechanismOperation);
		
		validateAssignmentConditions(holderAccountOperation,lstExistingHolderAccountOperation);
		
		AssignmentRequest assignmentRequest = getAssignmentRequest(holderAccountOperation.getMechanismOperation().getIdMechanismOperationPk(), holderAccountOperation.getRole());
		holderAccountOperation.setAssignmentRequest(assignmentRequest);
		holderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		holderAccountOperation.setIndAutomaticAsignment(ComponentConstant.ZERO);
		
		if(ComponentConstant.ONE.equals(holderAccountOperation.getIndIncharge())){
			holderAccountOperation.setInchargeState(HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode());
		}
		
		//validamos cantidad total en cuentas contra cantidad de operacion
		BigDecimal operationQuantity = holderAccountOperation.getMechanismOperation().getStockQuantity();
		BigDecimal currentTotalQuantity = getTotalQuantity(holderAccountOperation);
		BigDecimal totalQuantity = currentTotalQuantity.add(holderAccountOperation.getStockQuantity());
		if(totalQuantity.compareTo(operationQuantity) == 1){
			throw new ServiceException(ErrorServiceType.ASSIGNMENT_EXCEEDED_QUANTITY, new Object[]{ParticipantRoleType.get(holderAccountOperation.getRole()).getValue()});
		}
		
		create(holderAccountOperation);
		
		generateAccountOperationData(holderAccountOperation, holderAccountOperation.getMechanismOperation(),false,loggerUser);
		
		// si la cantidad coincide con el total de la operacion, actualizamos AssignmentRequest
		if(totalQuantity.compareTo(operationQuantity) == 0){
			if(assignmentRequest.getRequestState().equals(AssignmentRequestStateType.CONFIRMED.getCode())){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_REQUEST_CONFIRMED, new Object[]{ParticipantRoleType.get(holderAccountOperation.getRole()).getValue()});
			}
			//assignmentRequest.setRequestState(AssignmentRequestStateType.REGISTERED.getCode());
			//update(assignmentRequest);			
			updateAssignmentRequestStateByPk(assignmentRequest.getIdAssignmentRequestPk(), AssignmentRequestStateType.REGISTERED.getCode(), loggerUser);
		}
	}
	
	/**
	 * Generate account operation data.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @param mechanismOperation the mechanism operation
	 * @param isIncharge the is incharge
	 * @param loggUser the logg user
	 * @return the settlement account operation
	 * @throws ServiceException the service exception
	 */
	public SettlementAccountOperation generateAccountOperationData(HolderAccountOperation holderAccountOperation, 
			MechanismOperation mechanismOperation, boolean isIncharge, LoggerUser loggUser) throws ServiceException {

		//create cash settlement_operation
		SettlementAccountOperation settlementAccountOperation = settlementProcessService.populateSettlementAccountOperation(
				mechanismOperation.getIdMechanismOperationPk(),holderAccountOperation,null);
//		create(holderAccountOperation,loggUser);
		create(settlementAccountOperation,loggUser);
		
		// create market facts for sale or purchase
		if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
			settlementAccountOperation.setSettlementAccountMarketfacts(new ArrayList<SettlementAccountMarketfact>());
			if(ComponentConstant.SALE_ROLE.equals(holderAccountOperation.getRole())){
				for(AccountOperationMarketFact holAccOpeMarketFact:holderAccountOperation.getAccountOperationMarketFacts()){
					if(holAccOpeMarketFact.getIdAccountOperMarketFactPk()==null){
						create(holAccOpeMarketFact,loggUser);
					}
					SettlementAccountMarketfact settAccMktFact = settlementProcessService.populateSettlementAccountMarketFact(holAccOpeMarketFact, settlementAccountOperation);
					settlementAccountOperation.getSettlementAccountMarketfacts().add(settAccMktFact);
				}
			}else if(ComponentConstant.PURCHARSE_ROLE.equals(holderAccountOperation.getRole())){
				SettlementAccountMarketfact settAccMktFact = settlementProcessService.
						populatePurchaseSettlementMarketFacts(settlementAccountOperation,mechanismOperation.getIdMechanismOperationPk());
				settlementAccountOperation.getSettlementAccountMarketfacts().add(settAccMktFact);
			}
			saveAll(settlementAccountOperation.getSettlementAccountMarketfacts(),loggUser);
		}
		
		//create HAO term part and term settlement_operation
		if(ComponentConstant.ONE.equals(mechanismOperation.getIndTermSettlement())){
			HolderAccountOperation termAccountOperation = createHolderAccountOperationTermPart(holderAccountOperation);
			create(termAccountOperation,loggUser);
			
			SettlementAccountOperation termSettlementAccountOperation = settlementProcessService.populateSettlementAccountOperation(
					mechanismOperation.getIdMechanismOperationPk(),termAccountOperation,settlementAccountOperation);
			create(termSettlementAccountOperation,loggUser);
			
			if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
				termSettlementAccountOperation.setSettlementAccountMarketfacts(new ArrayList<SettlementAccountMarketfact>());
				for(SettlementAccountMarketfact settlementAccountMarketFact : settlementAccountOperation.getSettlementAccountMarketfacts()){
					detach(settlementAccountMarketFact);
					settlementAccountMarketFact.setChainedQuantity(BigDecimal.ZERO);
					settlementAccountMarketFact.setIdSettAccountMarketfactPk(null);
					settlementAccountMarketFact.setSettlementAccountOperation(termSettlementAccountOperation);
					create(settlementAccountMarketFact,loggUser);
				}
			}
		}
		
		return settlementAccountOperation;
		
	}


	/**
	 * Validate assignment conditions.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @param existingAccountOperations the existing account operations
	 * @throws ServiceException the service exception
	 */
	public void validateAssignmentConditions(HolderAccountOperation holderAccountOperation, List<HolderAccountOperation> existingAccountOperations) throws ServiceException{

		if(holderAccountOperation != null &&  !holderAccountOperation.getHolderAccount().getAccountGroup().equals(HolderAccountGroupType.INVERSTOR.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_SECURITY_NOT_INVESTOR);
		}
		if(holderAccountOperation != null){
			chainedOperationsServiceBean.validateChainedHolderOperation(holderAccountOperation.getIdHolderAccountOperationPk(), 
					holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
		}
				
		for(HolderAccountOperation holAccOperation:existingAccountOperations){
									
			if(!holAccOperation.getRole().equals(holderAccountOperation.getRole())){								
				
				if(holAccOperation.getHolderAccount().getIdHolderAccountPk().equals(holderAccountOperation.getHolderAccount().getIdHolderAccountPk())){
					throw new ServiceException(ErrorServiceType.ASSIGNMENT_ACCOUNT_EXISTS,new Object[]{holderAccountOperation.getHolderAccount().getAccountNumber()});
				}
				
				Long existHolderPk = 0L;
				Long newHolderPk = 0L;
				for(HolderAccountDetail holderAccountDetail:holderAccountOperation.getHolderAccount().getHolderAccountDetails()){
					if(ComponentConstant.ONE.equals(holderAccountDetail.getIndRepresentative())){
						newHolderPk = holderAccountDetail.getHolder().getIdHolderPk();
						break;
					}
				}
				for(HolderAccountDetail holderAccountDetail:holAccOperation.getHolderAccount().getHolderAccountDetails()){
					if(ComponentConstant.ONE.equals(holderAccountDetail.getIndRepresentative())){
						existHolderPk = holderAccountDetail.getHolder().getIdHolderPk();
						break;
					}
				}
				if(existHolderPk.equals(newHolderPk)){
					throw new ServiceException(ErrorServiceType.ASSIGNMENT_HOLDER_EXIST);
				}
			} 
			
		}
		
		
	}

	/**
	 * Creates the holder account operation term part.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @return the holder account operation
	 * @throws ServiceException the service exception
	 */
	private HolderAccountOperation createHolderAccountOperationTermPart(HolderAccountOperation holderAccountOperation) throws ServiceException{
		HolderAccountOperation hAccountOperation = new HolderAccountOperation();
		hAccountOperation.setAssignmentRequest(holderAccountOperation.getAssignmentRequest());
		hAccountOperation.setStockQuantity(holderAccountOperation.getStockQuantity());
		BigDecimal terAmount = NegotiationUtils.getSettlementAmount(holderAccountOperation.getMechanismOperation().getTermPrice(), hAccountOperation.getStockQuantity());
		hAccountOperation.setCashAmount(terAmount);
		BigDecimal settlementAmount = NegotiationUtils.getSettlementAmount(holderAccountOperation.getMechanismOperation().getTermSettlementPrice(), hAccountOperation.getStockQuantity());
		hAccountOperation.setSettlementAmount(settlementAmount);
		hAccountOperation.setHolderAccount(holderAccountOperation.getHolderAccount());
		hAccountOperation.setHolderAccountState(holderAccountOperation.getHolderAccountState());
		hAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		hAccountOperation.setMechanismOperation(holderAccountOperation.getMechanismOperation());
		hAccountOperation.setOperationPart(OperationPartType.TERM_PART.getCode());
		hAccountOperation.setInchargeStockParticipant(holderAccountOperation.getInchargeStockParticipant());
		hAccountOperation.setInchargeFundsParticipant(holderAccountOperation.getInchargeFundsParticipant());
		//in Term part buyer is changed as seller and seller is changed as buyer
		if(ComponentConstant.SALE_ROLE.equals(holderAccountOperation.getRole())){
			hAccountOperation.setRole(ComponentConstant.PURCHARSE_ROLE); 
		}else{
			hAccountOperation.setRole(ComponentConstant.SALE_ROLE); 
		}						
		hAccountOperation.setIndIncharge(holderAccountOperation.getIndIncharge());
		hAccountOperation.setInchargeState(holderAccountOperation.getInchargeState());
		hAccountOperation.setRefAccountOperation(holderAccountOperation);
		hAccountOperation.setIndAutomaticAsignment(ComponentConstant.ZERO);
		hAccountOperation.setMcnProcessFile(holderAccountOperation.getMcnProcessFile());
		return hAccountOperation;
	}

	/**
	 * Gets the holder account operation registered.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param role the role
	 * @param idMcnProcessFilePk the id mcn process file pk
	 * @return the holder account operation registered
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountOperation> getHolderAccountOperationRegistered(Long idMechanismOperationPk, Integer role, Long idMcnProcessFilePk) throws ServiceException {
		
		StringBuilder sbQuery =  new StringBuilder();
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	select hao");
		sbQuery.append("	FROM HolderAccountOperation hao inner join hao.mechanismOperation mo ");
		sbQuery.append("	WHERE mo.idMechanismOperationPk = :idMechanismOperationPk");
		sbQuery.append("	AND hao.holderAccountState = :accountState");
		sbQuery.append("	AND hao.role = :role ");
		sbQuery.append("	AND hao.operationPart = :cashPart ");
		sbQuery.append("	AND ( hao.mcnProcessFile.idMcnProcessFilePk is null or hao.mcnProcessFile.idMcnProcessFilePk != :idMcnProcessFilePk) ");
		sbQuery.append("	AND (hao.inchargeState is null or hao.inchargeState <> :stateConfirm) ");

		parameters.put("idMechanismOperationPk", idMechanismOperationPk);
		parameters.put("accountState", HolderAccountOperationStateType.REGISTERED.getCode());
		parameters.put("role", role);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		parameters.put("idMcnProcessFilePk", idMcnProcessFilePk);
		parameters.put("stateConfirm", HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
		
		List<HolderAccountOperation> accountOperations = findListByQueryString(sbQuery.toString(), parameters);
		return accountOperations;
	}

	
	/**
	 * Removes the holder account operations.
	 *
	 * @param lstHolderAccountOperation the lst holder account operation
	 * @param mechanismOperation the mechanism operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void removeHolderAccountOperations(List<HolderAccountOperation> lstHolderAccountOperation, MechanismOperation mechanismOperation, LoggerUser loggerUser) throws ServiceException {
		//Note: All holder_Account_operation MUST belong to the same mechanismOperation.
		
		boolean updatePurchaseRequest = false;
		boolean updateSaleRequest = false;
		
		for(HolderAccountOperation acc:lstHolderAccountOperation){
			HolderAccountOperation holderAccountOperation = find(HolderAccountOperation.class, acc.getIdHolderAccountOperationPk());
			
			chainedOperationsServiceBean.validateChainedHolderOperation(holderAccountOperation.getIdHolderAccountOperationPk(), 
					acc.getInchargeStockParticipant().getIdParticipantPk());
			
			if(!HolderAccountOperationStateType.REGISTERED.getCode().equals(holderAccountOperation.getHolderAccountState())){
				Object[] bodyData = new Object[2];
				bodyData[0] = holderAccountOperation.getHolderAccount().getAccountNumber().toString();
				bodyData[1] = ParticipantRoleType.get(holderAccountOperation.getRole()).getValue();
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_HOLDER_ACCOUNT_MODIFIED, bodyData);
			}
			
			// Validate request in charge 
			Integer inchargeState = getInchargeStateMassive(holderAccountOperation.getIdHolderAccountOperationPk());
			if(Validations.validateIsNotNullAndNotEmpty(inchargeState)){
				if(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode().equals(new Integer(inchargeState.toString())) 
						|| HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode().equals(new Integer(inchargeState.toString()))) {
					Object[] bodyData = new Object[2];
					bodyData[0] = holderAccountOperation.getHolderAccount().getAccountNumber().toString();
					bodyData[1] = ParticipantRoleType.get(holderAccountOperation.getRole()).getValue();
					throw new ServiceException(ErrorServiceType.INCHARGE_ASSIGNMENT_ACCOUNT_MASSIVE, bodyData);
				}
			}
			
			SettlementAccountOperation settlementAccountOperation = settlementProcessService.getSettlementAccountOperation(acc.getIdHolderAccountOperationPk());
			
			/* Initial Update Settlement Account Market fact */
			List<Long> lstSettlementAccountMarketfact = new ArrayList<Long>();
			if(Validations.validateListIsNotNullAndNotEmpty(settlementAccountOperation.getSettlementAccountMarketfacts())){
				for(SettlementAccountMarketfact objSettlementAccountMarketfact : settlementAccountOperation.getSettlementAccountMarketfacts()) {
					lstSettlementAccountMarketfact.add(objSettlementAccountMarketfact.getIdSettAccountMarketfactPk());
				}
				settlementProcessService.updateSettlementAccountMarketfact(lstSettlementAccountMarketfact, loggerUser);
			}						
			/* End Update Settlement Account Market fact */
			
			settlementProcessService.updateSettlementAccountOperationState(settlementAccountOperation.getIdSettlementAccountPk(), HolderAccountOperationStateType.CANCELLED.getCode(), loggerUser);
			/** Modify  holderAccountOperation:  HolderAccountState, indIncharge, InchargeState*/
			
			//issue 0000067 
			
//			if((mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_FIXED_INCOME.getCode())) ||
//			(mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_EQUITIES.getCode()))){
//				updateHolderAccountOperation(holderAccountOperation, HolderAccountOperationStateType.CANCELLED.getCode(), BooleanType.NO.getCode(), null,mechanismOperation.getIdMechanismOperationPk(),loggerUser);
//			}else{
				updateHolderAccountOperation(holderAccountOperation, HolderAccountOperationStateType.CANCELLED.getCode(), BooleanType.NO.getCode(), null,null,loggerUser);
//			}
			
			if(BooleanType.YES.getCode().equals(holderAccountOperation.getMechanismOperation().getIndTermSettlement())){
				HolderAccountOperation hActOperation = getHolderAccountOperationTermPart(holderAccountOperation.getIdHolderAccountOperationPk());		
				
				SettlementAccountOperation termSettlementAccount = settlementProcessService.getSettlementAccountOperationRef(settlementAccountOperation.getIdSettlementAccountPk());
				
				/* Initial Update Settlement Account Market fact */
				List<Long> lstSettlementAccountMarketfactTerm = new ArrayList<Long>();
				if(Validations.validateListIsNotNullAndNotEmpty(termSettlementAccount.getSettlementAccountMarketfacts())){
					for(SettlementAccountMarketfact objSettlementAccountMarketfact : termSettlementAccount.getSettlementAccountMarketfacts()) {
						lstSettlementAccountMarketfactTerm.add(objSettlementAccountMarketfact.getIdSettAccountMarketfactPk());
					}
					settlementProcessService.updateSettlementAccountMarketfact(lstSettlementAccountMarketfactTerm, loggerUser);
				}								
				/* End Update Settlement Account Market fact */
				
				settlementProcessService.updateSettlementAccountOperationState(termSettlementAccount.getIdSettlementAccountPk(), HolderAccountOperationStateType.CANCELLED.getCode(), loggerUser);								
				
				//hActOperation.setHolderAccountState(HolderAccountOperationStateType.CANCELLED.getCode());
				//hActOperation.setIndIncharge(BooleanType.NO.getCode());
				//hActOperation.setInchargeState(null);
				
				/** Modify  holderAccountOperation:  HolderAccountState, indIncharge, InchargeState*/
				updateHolderAccountOperation(hActOperation, HolderAccountOperationStateType.CANCELLED.getCode(), BooleanType.NO.getCode(), null,null, loggerUser);				
			}
			
			if(ComponentConstant.PURCHARSE_ROLE.equals(holderAccountOperation.getRole())){
				updatePurchaseRequest = true;
			}
			if(ComponentConstant.SALE_ROLE.equals(holderAccountOperation.getRole())){
				updateSaleRequest = true;
			}
		}
		
		if(updatePurchaseRequest){
			AssignmentRequest assignmentRequest = getAssignmentRequest(mechanismOperation.getIdMechanismOperationPk(), ComponentConstant.PURCHARSE_ROLE);
			//assignmentRequest.setRequestState(AssignmentRequestStateType.PENDING.getCode());			
			//update(assignmentRequest);
			updateAssignmentRequestStateByPk(assignmentRequest.getIdAssignmentRequestPk(), AssignmentRequestStateType.PENDING.getCode(), loggerUser);
		}
		
		if(updateSaleRequest){
			AssignmentRequest assignmentRequest = getAssignmentRequest(mechanismOperation.getIdMechanismOperationPk(), ComponentConstant.SALE_ROLE);
			//assignmentRequest.setRequestState(AssignmentRequestStateType.PENDING.getCode());
			//update(assignmentRequest);
			updateAssignmentRequestStateByPk(assignmentRequest.getIdAssignmentRequestPk(), AssignmentRequestStateType.PENDING.getCode(), loggerUser);
		}
		
	}

	/**
	 * Validate registered market facts.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @throws ServiceException the service exception
	 */
	public void validateRegisteredMarketFacts(Long assignmentProcessId, Long participantAssignmentId) throws ServiceException {
		List<String> lstInconsistency =settlementProcessService.validateAccountMarketFactConsistency(assignmentProcessId,participantAssignmentId);
		if(lstInconsistency.size() > 0){
			StringBuffer sb = new StringBuffer();
			for(String object : lstInconsistency){
				sb.append(object).append(GeneralConstants.STR_BR);
			}
			
			if(participantAssignmentId==null){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_MARKET_FACT_INCONSISTENCIES,new Object[]{sb.toString()});
			}else {
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_MARKET_FACT_INCONSISTENCIES_CONTINUE,new Object[]{sb.toString()});
			}
		}
	}

	/**
	 * Validate pending assignments.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @param role the role
	 * @throws ServiceException the service exception
	 */
	public void validatePendingAssignments(Long assignmentProcessId, Long participantAssignmentId, Integer role) throws ServiceException {
		List<Object[]> lstInconsistency =settlementProcessService.getPendingAssignmentOperations(assignmentProcessId,participantAssignmentId, role);
		if(lstInconsistency.size() > 0){
			StringBuilder sb = new StringBuilder();
			for(Object[] object : lstInconsistency){
				Long ballotNumber= (Long)object[1];
				Long sequential= (Long)object[2];
				String modalityName= (String)object[3];
				Long operationNumber = (Long)object[0];
				sb.append(modalityName).append(GeneralConstants.BLANK_SPACE);
				if(ballotNumber != null){
					sb.append(ballotNumber.toString());
				}else{
					sb.append(operationNumber.toString());
				}
				sb.append(GeneralConstants.STR_BR);
			}
			if(participantAssignmentId==null){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_PENDING_OPERATIONS_REMAIN,new Object[]{sb.toString(),ParticipantRoleType.get(role).getValue()});
			}else {
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_PENDING_OPERATIONS_REMAIN_CONTINUE,new Object[]{sb.toString(),ParticipantRoleType.get(role).getValue()});
			}
			
		}
	}
	
//	/**
//	 * Validate pending assignments.
//	 *
//	 * @param assignmentProcessId the assignment process id
//	 * @param participantAssignmentId the participant assignment id
//	 * @param role the role
//	 * @throws ServiceException the service exception
//	 */
//	public String validatePendingAssignments(Long assignmentProcessId, Long participantAssignmentId, Integer role) throws ServiceException {
//		List<Object[]> lstInconsistency =settlementProcessService.getPendingAssignmentOperations(assignmentProcessId,participantAssignmentId, role);
//		String inconsistence="";
//		if(lstInconsistency.size() > 0){
////			StringBuilder sb = new StringBuilder();
////			for(Object[] object : lstInconsistency){
////				Long ballotNumber= (Long)object[1];
////				Long sequential= (Long)object[2];
////				String modalityName= (String)object[3];
////				Long operationNumber = (Long)object[0];
////				sb.append(modalityName).append(GeneralConstants.BLANK_SPACE);
////				if(ballotNumber != null && sequential != null){
////					sb.append(ballotNumber.toString()).append(GeneralConstants.SLASH).append(sequential.toString());
////				}else{
////					sb.append(operationNumber.toString());
////				}
////				sb.append(GeneralConstants.STR_BR);
////			}
////			if(participantAssignmentId==null){
////				throw new ServiceException(ErrorServiceType.ASSIGNMENT_PENDING_OPERATIONS_REMAIN,new Object[]{sb.toString(),ParticipantRoleType.get(role).getValue()});
////			}else {
////				throw new ServiceException(ErrorServiceType.ASSIGNMENT_PENDING_OPERATIONS_REMAIN_CONTINUE,new Object[]{sb.toString(),ParticipantRoleType.get(role).getValue()});
////			}
//			
//			for(Object[] object : lstInconsistency){
//				
//				inconsistence=inconsistence +(String)object[3] + " : " + object[1].toString() + " / " + object[2].toString() + " - CUENTA NO REGISTRADA" + "<br>";
//			}
//			
//		}
//		return inconsistence;
//	}
	
	/**
	 * Validate incomplete securities.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @param role the role
	 * @throws ServiceException the service exception
	 */
	public String validateIncompleteSecurities(Long negotiationModality, Long participantAssignmentId, Long idParticipant, Integer role, Date dateSelected) throws ServiceException {
		String messageDup="";
		HashMap<String, String> strInconsistences = new HashMap<>();
		List<Object[]> lstsSecuritiesAssignment = settlementProcessService.getIncompleteSecurities(negotiationModality,participantAssignmentId, ParticipantRoleType.SELL.getCode(),idParticipant,dateSelected);
		
		//  ballotNumber 0 - sequential 1 - modalityName 2 - security 3 - idHolderAccount 4
		//  marketDate 5 - marketRate 6 - marketQuantity 7 - participant 8 - chainedQuantity 9 - securityClass 10
		
		List<HolderMarketFactBalance> lstHolderMarketfactBalance = new ArrayList<HolderMarketFactBalance>();
		Boolean notInconsistenceSecurities = false;
		Boolean notInconsistenceHM = false;
		Boolean chainedOperation = false;
		Long longIdParticipantPk = null;
		
		if (lstsSecuritiesAssignment.size()>0){
			for (Object[] securitiesAssignment : lstsSecuritiesAssignment) {
				
				lstHolderMarketfactBalance = new ArrayList<HolderMarketFactBalance>();
				notInconsistenceSecurities = false;
				notInconsistenceHM = false;
				chainedOperation = false;
				longIdParticipantPk = null;
				
				if(idParticipant == null){
					longIdParticipantPk = (Long)securitiesAssignment[8];
				}else{
					longIdParticipantPk=idParticipant;
				}
				
				//encadenamientos
					if (Integer.valueOf(securitiesAssignment[9].toString())>0){
						chainedOperation = true;
					}	
					
					if (chainedOperation.equals(true)){
						if(Integer.valueOf(securitiesAssignment[9].toString()) >= Integer.valueOf(securitiesAssignment[7].toString())){
							notInconsistenceSecurities=true;
							notInconsistenceHM=true;
						}else{
							lstHolderMarketfactBalance = getHolderMarketfactBalance((Long)securitiesAssignment[4],longIdParticipantPk,(String)securitiesAssignment[3]);
						}
					}else{
						lstHolderMarketfactBalance = getHolderMarketfactBalance((Long)securitiesAssignment[4],longIdParticipantPk,(String)securitiesAssignment[3]);
					}
						
				if( (lstHolderMarketfactBalance.size() > 0  && notInconsistenceSecurities.equals(false)) || chainedOperation.equals(true)){
					Integer availableBalance = new Integer(0);
					for (HolderMarketFactBalance holderMarketfactBalance :lstHolderMarketfactBalance){
						availableBalance=availableBalance + holderMarketfactBalance.getAvailableBalance().intValue() + Integer.valueOf(securitiesAssignment[9].toString());
						if(availableBalance >= Integer.valueOf(securitiesAssignment[7].toString()) ){
										notInconsistenceSecurities = true;
									}
									
									if((chainedOperation.equals(false)) && 
											(!Integer.valueOf(securitiesAssignment[10].toString()).equals(SecurityClassType.DPF.getCode()))
													&& !Integer.valueOf(securitiesAssignment[10].toString()).equals(SecurityClassType.DPA.getCode()))
										{
										if((holderMarketfactBalance.getAvailableBalance().intValue() >= (Integer.valueOf(securitiesAssignment[7].toString())) )
													&& (holderMarketfactBalance.getMarketDate().equals((Date)securitiesAssignment[5]))
													&& (holderMarketfactBalance.getMarketRate().equals((BigDecimal)securitiesAssignment[6]))
													){
												notInconsistenceHM=true;
											}
									}else{
										notInconsistenceHM=true;
									}
								}
								
								if (notInconsistenceSecurities.equals(false)){
									notInconsistenceHM=true;
									strInconsistences.put(securitiesAssignment[2]+ " : " +securitiesAssignment[0].toString()+" / "+securitiesAssignment[1].toString()//+" - "+securitiesAssignment[3].toString()
											+" - FALTA DE VALORES","FV");
								}
								if(notInconsistenceHM.equals(false)){
									strInconsistences.put(securitiesAssignment[2]+ " : " +securitiesAssignment[0].toString()+" / "+securitiesAssignment[1].toString()//+" - "+securitiesAssignment[3].toString()
											+" - HECHOS DE MERCADO"," H M");
								}
							}else{
								if(notInconsistenceSecurities.equals(false)){
									strInconsistences.put(securitiesAssignment[2]+ " : " +securitiesAssignment[0].toString()+" / "+securitiesAssignment[1].toString()//+" - "+securitiesAssignment[3].toString()
											+" - FALTA DE VALORES","FV");
								}
							}
				
			}
			if (strInconsistences.size()>0){
				for (Entry keyHash: strInconsistences.entrySet()) {
					messageDup=messageDup + keyHash.getKey().toString() + " <br> ";
					}
//				throw new ServiceException(ErrorServiceType.ASSIGNMENT_PENDING_INCOMPLETE_SECURITIES,new Object[]{messageDup});
			}
		}		
		return messageDup;
	}
	
	/**
	 * Gets the holderMarketfactsBalance from operation assignment.
	 *
	 * @param idHolderAccount the id holderAccount
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id securityCOde
	 * @return holderMarketfactBalance
	 */
	@SuppressWarnings("unchecked")
	public List<HolderMarketFactBalance> getHolderMarketfactBalance(Long idHolderAccount,Long idParticipant, String idSecurityCode ) {
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select hmb "); // ID Mechanism operation
		stringBuffer.append(" from HolderMarketFactBalance hmb ");
		stringBuffer.append(" where 1=1 ");
		stringBuffer.append(" and hmb.holderAccount.idHolderAccountPk = :idHolderAccount "); 
		stringBuffer.append(" and hmb.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and hmb.security.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" and hmb.totalBalance > 0 ");
		stringBuffer.append(" and hmb.availableBalance > 0 "); // modificar por que debemos verificar los encadenamientos
		stringBuffer.append(" and hmb.indActive = 1 ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccount", idHolderAccount);
		parameters.put("idParticipant", idParticipant);
		parameters.put("idSecurityCode", idSecurityCode);
		
		return  findListByQueryString(stringBuffer.toString(), parameters);
	}
	
	
	/**
	 * Gets the account operation marketfacts.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @return the account operation marketfacts
	 */
	@SuppressWarnings("unchecked")
	public List<AccountOperationMarketFact> getAccountOperationMarketfacts(Long idHolderAccountOperation) {
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select aom "); // ID Mechanism operation
		stringBuffer.append(" from AccountOperationMarketFact aom where ");
		stringBuffer.append(" aom.holderAccountOperation.idHolderAccountOperationPk = :idHolderAccountOperation "); 
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		
		return  findListByQueryString(stringBuffer.toString(), parameters);
	}


	/**
	 * Gets the holder account operations.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param role the role
	 * @param idParticipant the id participant
	 * @return the holder account operations
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountOperation> getHolderAccountOperations(Long idMechanismOperation, List<Integer> role, Long idParticipant) {

		Map<Long,HolderAccount> accounts = new HashMap<Long, HolderAccount>();
		List<Object[]> accountOperations = null;
		List<HolderAccountOperation> settlementAccountOperations = new ArrayList<HolderAccountOperation>();
		Map<String,Object> parameters = new HashMap<String,Object> ();

		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT  hao, hao.holderAccount.idHolderAccountPk ");
		querySql.append("	FROM HolderAccountOperation hao ");
		querySql.append(" inner join fetch hao.inchargeFundsParticipant fundsP ");
		querySql.append(" inner join fetch hao.inchargeStockParticipant stockP ");
		querySql.append(" where hao.operationPart = :cashPart ");
		querySql.append(" and hao.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		querySql.append(" and hao.holderAccountState in (:confirmedState) ");
		
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());
		
		if(idParticipant!=null){
			querySql.append(" and hao.inchargeStockParticipant.idParticipantPk = :idParticipant  ");
			parameters.put("idParticipant", idParticipant);
		}
		if(role!=null){
			querySql.append(" and hao.role in (:idRole) ");
			parameters.put("idRole", role);
		}
		querySql.append(" order by hao.operationPart, hao.role");
		
		parameters.put("cashPart", ComponentConstant.CASH_PART);

		accountOperations = (List<Object[]>) findListByQueryString(querySql.toString(), parameters);
		
		for(Object[] accOperation: accountOperations){
			HolderAccountOperation hao = (HolderAccountOperation)accOperation[0];
			Long idHolderAccount = (Long) accOperation[1];
			HolderAccount holderAccount = accounts.get(idHolderAccount);
			if(holderAccount==null){
				holderAccount = mechanismOperationService.getHolderAccount(idHolderAccount);
				accounts.put(idHolderAccount, holderAccount);
			}
			hao.setHolderAccount(holderAccount);
			if(hao.getRole().equals(ComponentConstant.SALE_ROLE)){
				hao.setAccountOperationMarketFacts(getAccountOperationMarketfacts(hao.getIdHolderAccountOperationPk()));
			}
			settlementAccountOperations.add(hao);
		}
			
		return settlementAccountOperations;
	}
	
	/**
	 * Gets the reassignment details.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @return the reassignment details
	 */
	@SuppressWarnings("unchecked")
	public List<ReassignmentRequestDetail> getReassignmentDetails(Long idReassignmentRequest) {

		Map<Long,HolderAccount> accounts = new HashMap<Long, HolderAccount>();
		List<Object[]> accountOperations = null;
		List<ReassignmentRequestDetail> requestDetails = new ArrayList<ReassignmentRequestDetail>();
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		StringBuilder querySql = new StringBuilder();
		querySql.append(" SELECT  rrd, hao.holderAccount.idHolderAccountPk, 		");
		querySql.append("	(select pt.parameterName from ParameterTable pt where pt.parameterTablePk = rrd.holderAccountState ) ");
		querySql.append("	FROM ReassignmentRequestDetail rrd 						");
		querySql.append(" inner join fetch rrd.holderAccountOperation hao 			");
		querySql.append(" inner join fetch hao.inchargeFundsParticipant fundsP 		");
		querySql.append(" inner join fetch hao.inchargeStockParticipant stockP 		");
		
		querySql.append(" left join fetch rrd.settlementAccountMarketfact samf 		");
		
		querySql.append(" where hao.operationPart = :cashPart ");
		querySql.append(" and rrd.reassignmentRequest.idReassignmentRequestPk = :idReassignmentRequest  ");
		//querySql.append(" and rrd.indOld = :indOld  ");
		querySql.append(" order by hao.role , hao.holderAccountState ");
		
		parameters.put("idReassignmentRequest", idReassignmentRequest);
		parameters.put("cashPart", ComponentConstant.CASH_PART);
		//parameters.put("indOld", GeneralConstants.ONE_VALUE_INTEGER);

		accountOperations = (List<Object[]>) findListByQueryString(querySql.toString(), parameters);
		
		for(Object[] accOperation: accountOperations){
			ReassignmentRequestDetail rrd = (ReassignmentRequestDetail)accOperation[0];
			HolderAccountOperation hao = rrd.getHolderAccountOperation();
			Long idHolderAccount = (Long) accOperation[1];
			String stateDescription = (String) accOperation[2];
			rrd.setStateDescription(stateDescription);
			HolderAccount holderAccount = accounts.get(idHolderAccount);
			if(holderAccount==null){
				holderAccount = mechanismOperationService.getHolderAccount(idHolderAccount);
				accounts.put(idHolderAccount, holderAccount);
			}
			hao.setHolderAccount(holderAccount);
			if(hao.getRole().equals(ComponentConstant.SALE_ROLE)){
				hao.setAccountOperationMarketFacts(getAccountOperationMarketfacts(hao.getIdHolderAccountOperationPk()));
			}
			requestDetails.add(rrd);
		}
			
		return requestDetails;
	}


	/**
	 * Check previous reassignment request.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param idParticipantPk the id participant pk
	 * @throws ServiceException the service exception
	 */
	public void checkPreviousReassignmentRequest(Long idMechanismOperationPk, Long idParticipantPk) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT nvl(count(rr.idReassignmentRequestPk),0) ");
		sbQuery.append("	FROM ReassignmentRequest rr ");
		sbQuery.append("	WHERE rr.requestState in (:state)");
		sbQuery.append("	AND rr.mechanismOperation.idMechanismOperationPk = :idOperation ");
		sbQuery.append("	AND rr.participant.idParticipantPk = :idParticipant ");
		
		List<Integer> lstState = new ArrayList<Integer>();
		lstState.add(ReassignmentRequestStateType.REGISTERED.getCode());
		
		parameters.put("idOperation", idMechanismOperationPk);
		parameters.put("state", lstState);
		parameters.put("idParticipant", idParticipantPk);
		
		Long result = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
		if(result > 0l){
			throw new ServiceException(ErrorServiceType.REASSIGNMENT_REQUEST_EXISTS);
		}
	}
	
	/**
	 * Check exist chained account.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @throws ServiceException the service exception
	 */
	public void checkExistChainedAccount(Long idHolderAccountOperation) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT distinct mo.ballotNumber , mo.sequential , mo.operationNumber, mo.operationDate ");
		sbQuery.append("	from SettlementAccountOperation sao ");
		sbQuery.append("	inner join sao.settlementOperation so ");
		sbQuery.append("	inner join so.mechanismOperation mo ");
		sbQuery.append("	inner join sao.holderAccountOperation hao ");
		sbQuery.append("	inner join hao.holderAccount ha ");
		sbQuery.append("	where hao.idHolderAccountOperationPk = :idHolderAccountOperation  ");
		sbQuery.append("	and exists ( ");
		sbQuery.append("		select hcd.idHolderChainDetailPk from HolderChainDetail hcd inner join hcd.chainedHolderOperation cho ");
		sbQuery.append("		where hcd.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = sao.idSettlementAccountPk  ");
		sbQuery.append("		and cho.chaintState not in  (:xcludedStates)  ) ");
		
		parameters.put("idHolderAccountOperation", idHolderAccountOperation);
		List<Integer> xcludedStates= new ArrayList<Integer>();
		xcludedStates.add(ChainedOperationStateType.CANCELLED.getCode());
		xcludedStates.add(ChainedOperationStateType.REJECTED.getCode());
		parameters.put("xcludedStates",xcludedStates);
						
		Object[] result = (Object[])findObjectByQueryString(sbQuery.toString(), parameters);
		if(result != null){
			String ballotNumber = result[0].toString();
			String sequential = result[1].toString();
			StringBuffer operation = new StringBuffer();
			if(ballotNumber != null && sequential !=null){
				operation.append(ballotNumber).append(GeneralConstants.SLASH).append(sequential);
			}else{
				Long operationNumber = new Long(result[2].toString());
				Date date = (Date)result[3];
				operation.append(operationNumber).append(GeneralConstants.DASH).append(date);
			}
			throw new ServiceException(ErrorServiceType.CHAINED_OPERATIONS_OPERATION_EXISTS_AS_CHAIN, new Object[]{operation.toString()});
		}
		
	}

	/**
	 * Check assignment process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param expectedState the expected state
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void checkAssignmentProcess(Long idMechanism, Long idModality, Integer expectedState, Date settlementDate) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT ap.idAssignmentProcessPk,  ");
		sbQuery.append("	ap.mechanisnModality.negotiationMechanism.mechanismName,   ");
		sbQuery.append("	ap.mechanisnModality.negotiationModality.modalityName ,  ");
		sbQuery.append("	ap.assignmentState,  ");
		sbQuery.append("	( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ap.assignmentState ) ");
		sbQuery.append("	FROM AssignmentProcess ap ");
		sbQuery.append("	WHERE ap.settlementDate  = :settlementDate ");
		sbQuery.append("	AND ap.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk = :idMechanism ");
		sbQuery.append("	AND ap.mechanisnModality.negotiationModality.idNegotiationModalityPk = :idModality ");
		
		parameters.put("idMechanism", idMechanism);
		parameters.put("idModality", idModality);
		parameters.put("settlementDate", settlementDate);
		
		Object[] result = (Object[])findObjectByQueryString(sbQuery.toString(), parameters);
		if(result !=null){
			Integer state =  (Integer)result[3];
			if(!state.equals(expectedState)){
				String mechanismName =  (String)result[1];
				String modalityName =  (String)result[2];
				String stateDesc =  (String)result[4];
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_PROCESS_UNEXPECTED_STATE,new Object[] {mechanismName, modalityName, CommonsUtilities.convertDatetoString(settlementDate) , stateDesc});
			}
		}
	}
	
	/**
	 * Save reassignment request.
	 *
	 * @param reassignmentRequest the reassignment request
	 * @param lstAccountOperations the lst account operations
	 * @param lstDeletedAccounts the lst deleted accounts
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void saveReassignmentRequest(ReassignmentRequest reassignmentRequest,
			List<HolderAccountOperation> lstAccountOperations, List<HolderAccountOperation> lstDeletedAccounts, LoggerUser loggerUser) throws ServiceException {
		Boolean operationCruze=false;
		checkPreviousReassignmentRequest(reassignmentRequest.getMechanismOperation().getIdMechanismOperationPk(),
				reassignmentRequest.getParticipant().getIdParticipantPk());
		
		reassignmentRequest.setRegisterUser(loggerUser.getUserName());
		reassignmentRequest.setRegisterDate(CommonsUtilities.currentDateTime());
		
		create(reassignmentRequest);
		
		for (HolderAccountOperation holderAccountOperation : lstDeletedAccounts) {
			if(holderAccountOperation.getIdHolderAccountOperationPk()!=null){
				chainedOperationsServiceBean.validateChainedHolderOperation(holderAccountOperation.getIdHolderAccountOperationPk(), 
						holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
				ReassignmentRequestDetail reassignmentRequestDetail = new ReassignmentRequestDetail();
				reassignmentRequestDetail.setReassignmentRequest(reassignmentRequest);
				reassignmentRequestDetail.setHolderAccountState(HolderAccountOperationStateType.CANCELLED.getCode());
				reassignmentRequestDetail.setHolderAccountOperation(holderAccountOperation);
				reassignmentRequestDetail.setIndOld(GeneralConstants.ZERO_VALUE_INTEGER);
				create(reassignmentRequestDetail);
			}
		}
		//verificando que sea cruzada 
		if (reassignmentRequest.getMechanismOperation().getBuyerParticipant().getIdParticipantPk().equals
				(reassignmentRequest.getMechanismOperation().getSellerParticipant().getIdParticipantPk())){
			operationCruze=true;
		}else{ operationCruze=false;}
		
		for (HolderAccountOperation holderAccountOperation : lstAccountOperations) {
			
			ReassignmentRequestDetail reassignmentRequestDetail = new ReassignmentRequestDetail();
			reassignmentRequestDetail.setReassignmentRequest(reassignmentRequest);
			reassignmentRequestDetail.setHolderAccountOperation(holderAccountOperation);
			reassignmentRequestDetail.setHolderAccountState(holderAccountOperation.getHolderAccountState());
			reassignmentRequestDetail.setIndOld(GeneralConstants.ONE_VALUE_INTEGER);
			
			if(holderAccountOperation.getIdHolderAccountOperationPk()==null){
				
				List<HolderAccountOperation> lstExistingHolderAccountOperation = getHolderAccountOperation(holderAccountOperation.getMechanismOperation().getIdMechanismOperationPk());
			
				if (operationCruze.equals(false)){
					validateAssignmentConditions(holderAccountOperation,lstExistingHolderAccountOperation);
				}
				
				AssignmentRequest assignmentRequest = getAssignmentRequest(holderAccountOperation.getMechanismOperation().getIdMechanismOperationPk(), holderAccountOperation.getRole());
				holderAccountOperation.setAssignmentRequest(assignmentRequest);
				holderAccountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
				holderAccountOperation.setOperationPart(ComponentConstant.CASH_PART);
				create(holderAccountOperation);
				
				if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
					if(ComponentConstant.SALE_ROLE.equals(holderAccountOperation.getRole())){
						for (AccountOperationMarketFact accountOperationMarketFact : holderAccountOperation.getAccountOperationMarketFacts()) {
							accountOperationMarketFact.setHolderAccountOperation(holderAccountOperation);
							create(accountOperationMarketFact);
						}
					}
				}
				
				reassignmentRequestDetail.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
			}else{
				reassignmentRequestDetail.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
			}
			
			create(reassignmentRequestDetail);
			
		}
		
	}


	/**
	 * Search reassignment requests.
	 *
	 * @param searchAccountReassignmentTO the search account reassignment to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<AccountReassignmentTO> searchReassignmentRequests(SearchAccountReassignmentTO searchAccountReassignmentTO) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT ");
		sbQuery.append("	 mo.idMechanismOperationPk, "); //0
		sbQuery.append("	 mo.operationDate, ");//1
		sbQuery.append("	 mo.operationNumber, ");//2
		sbQuery.append("	 mo.ballotNumber, mo.sequential, ");// 3 4 
		sbQuery.append("	 mo.securities.idSecurityCodePk,");//5
		sbQuery.append("	 buyPart.idParticipantPk,");//6
		sbQuery.append("	 buyPart.mnemonic,");//7
		sbQuery.append("	 buyPart.description,"); 
		sbQuery.append("	 sellPart.idParticipantPk,");//9
		sbQuery.append("	 sellPart.mnemonic,");
		sbQuery.append("	 sellPart.description,");//11
		sbQuery.append("	 (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = mo.currency), ");
		sbQuery.append("	 rr.requestState, ");//13
		sbQuery.append("	 (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = rr.requestState), ");//14
		sbQuery.append("	 p.idParticipantPk,");//15
		sbQuery.append("	 p.mnemonic,");
		sbQuery.append("	 p.description,");//17
		sbQuery.append("	 rr.idReassignmentRequestPk,");//18
		sbQuery.append("	 mo.indTermSettlement, ");//19
		sbQuery.append("	 rr.role, ");//20
		
		sbQuery.append("	 rr.requestReason, ");//21
		sbQuery.append("	 ( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = rr.requestReason ) ");//22
		
		sbQuery.append("	FROM ReassignmentRequest rr ");
		sbQuery.append("	inner join rr.mechanismOperation mo ");
		sbQuery.append("	inner join rr.participant p ");
		sbQuery.append("	inner join mo.buyerParticipant buyPart ");
		sbQuery.append("	inner join mo.sellerParticipant sellPart ");
		sbQuery.append("	WHERE mo.mechanisnModality.id.idNegotiationMechanismPk = :idNegotiationMechanismPk");
		if(Validations.validateIsNotNullAndPositive(searchAccountReassignmentTO.getIdModality())){
			sbQuery.append("	AND mo.mechanisnModality.id.idNegotiationModalityPk = :idNegotiationModalityPk");
			parameters.put("idNegotiationModalityPk", searchAccountReassignmentTO.getIdModality());
		}
		if(Validations.validateIsNotNullAndPositive(searchAccountReassignmentTO.getIdParticipant())){
			sbQuery.append("	AND rr.participant.idParticipantPk = :idParticipant ");
			parameters.put("idParticipant", searchAccountReassignmentTO.getIdParticipant());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountReassignmentTO.getBallotNumber())){
			sbQuery.append("	AND mo.ballotNumber = :ballotNumber ");
			parameters.put("ballotNumber", searchAccountReassignmentTO.getBallotNumber());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountReassignmentTO.getSequential())){
			sbQuery.append("	AND mo.sequential = :sequential ");
			parameters.put("sequential", searchAccountReassignmentTO.getSequential());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountReassignmentTO.getIdState())){
			sbQuery.append("	AND rr.requestState = :state ");
			parameters.put("state", searchAccountReassignmentTO.getIdState());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountReassignmentTO.getIdCurrency())){
			sbQuery.append("	AND mo.currency = :idCurrency ");
			parameters.put("idCurrency", searchAccountReassignmentTO.getIdCurrency());
		}
		
		sbQuery.append("	AND TRUNC(rr.registerDate) between :initialDate AND :finalDate");	
		sbQuery.append("	ORDER BY rr.idReassignmentRequestPk DESC"); 
		
		parameters.put("idNegotiationMechanismPk", searchAccountReassignmentTO.getIdMechanism());
		parameters.put("initialDate", searchAccountReassignmentTO.getInitialDate());
		parameters.put("finalDate", searchAccountReassignmentTO.getFinalDate());
		
		List<Object[]> arrObjs = findListByQueryString(sbQuery.toString(), parameters);
		List<AccountReassignmentTO> lstAccountAssignments = new ArrayList<AccountReassignmentTO>();
		for(Object[] arrObject : arrObjs){
			AccountReassignmentTO accountAssignmentTO = new AccountReassignmentTO();
			accountAssignmentTO.setIdMechanismOperationPk((Long)arrObject[0]);
			accountAssignmentTO.setOperationDate((Date)arrObject[1]);
			accountAssignmentTO.setOperationNumber((Long)arrObject[2]);
			accountAssignmentTO.setBallotNumber((Long)arrObject[3]);
			accountAssignmentTO.setSequential((Long)arrObject[4]);
			accountAssignmentTO.setIdSecurityCodePk((String)arrObject[5]);
			accountAssignmentTO.setIdPurchaseParticipant((Long)arrObject[6]);
			accountAssignmentTO.setPurchaseParticipantNemonic((String)arrObject[7]);
			accountAssignmentTO.setPurchaseParticipantDescription((String)arrObject[8]);
			accountAssignmentTO.setIdSaleParticipant((Long)arrObject[9]);
			accountAssignmentTO.setSaleParticipantNemonic((String)arrObject[10]);
			accountAssignmentTO.setSaleParticipantDescription((String)arrObject[11]);
			accountAssignmentTO.setCurrencyDescription((String)arrObject[12]);
			accountAssignmentTO.setIdReassignmentState((Integer)arrObject[13]);
			accountAssignmentTO.setReassignmentStateDescription((String)arrObject[14]);
			accountAssignmentTO.setIdParticipant((Long)arrObject[15]);
			accountAssignmentTO.setParticipantMemonic((String)arrObject[16]);
			accountAssignmentTO.setParticipantDescription((String)arrObject[17]);
			accountAssignmentTO.setIdReassignmentRequest((Long)arrObject[18]);
			accountAssignmentTO.setIndTermSettlement((Integer)arrObject[19]);
			accountAssignmentTO.setRole((Integer)arrObject[20]);
			
			if(Validations.validateIsNotNullAndNotEmpty(arrObject[21])){
				accountAssignmentTO.setRequestRason((Integer)arrObject[21]);
				accountAssignmentTO.setDescRequestReason((String)arrObject[22]);
			}
						
			lstAccountAssignments.add(accountAssignmentTO);
		}
		return lstAccountAssignments;
	}
	
	/**
	 * Update state reassignment request.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @param mechanismOperation the mechanism operation
	 * @param state the state
	 * @param loggerUser the logger user
	 * @param details the details
	 * @throws ServiceException the service exception
	 */
	public void updateStateReassignmentRequest(Long idReassignmentRequest, MechanismOperation mechanismOperation, Integer state, 
			LoggerUser loggerUser, List<ReassignmentRequestDetail> details) throws ServiceException {
		ReassignmentRequest reassignmentRequest = find(ReassignmentRequest.class, idReassignmentRequest);
		
		if(ReassignmentRequestStateType.AUTHORIZED.getCode().equals(state)){
			
			if(!reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.CONFIRMED.getCode())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			reassignmentRequest.setAuthorizeDate(CommonsUtilities.currentDateTime());
			reassignmentRequest.setAuthorizeUser(loggerUser.getUserName());
			
			SettlementOperation settlementOperation = (SettlementOperation) settlementProcessService.getSettlementOperation(
					mechanismOperation.getIdMechanismOperationPk(), ComponentConstant.CASH_PART);
			
			AccountsComponentTO objAccountsComponentTO = new AccountsComponentTO();
			objAccountsComponentTO.setIdBusinessProcess(BusinessProcessType.UNFULFILL_OPERATIONS_CANCEL.getCode());
			objAccountsComponentTO.setOperationPart(settlementOperation.getOperationPart());
			objAccountsComponentTO.setIdSettlementOperation(settlementOperation.getIdSettlementOperationPk());
			objAccountsComponentTO.setIdOperationType(NegotiationModalityType.get(
					mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk()).getParameterOperationType());
			objAccountsComponentTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
			objAccountsComponentTO.setLstSourceAccounts(new ArrayList<HolderAccountBalanceTO>(0));
			objAccountsComponentTO.setLstTargetAccounts(new ArrayList<HolderAccountBalanceTO>(0));
			
			for (ReassignmentRequestDetail detail : details) {
				detail.getHolderAccountOperation().setMechanismOperation(mechanismOperation);
				if(HolderAccountOperationStateType.CANCELLED.getCode().equals(detail.getHolderAccountState())){
					
					chainedOperationsServiceBean.validateChainedHolderOperation(detail.getHolderAccountOperation().getIdHolderAccountOperationPk(), null);
					
					settlementProcessService.updateSettlementOperationState(settlementOperation.getIdSettlementOperationPk(), 
							MechanismOperationStateType.ASSIGNED_STATE.getCode() , null, loggerUser, false);
					
					SettlementAccountOperation settlementAccount = settlementProcessService.getSettlementAccountOperation(
							detail.getHolderAccountOperation().getIdHolderAccountOperationPk());
					
					if(settlementAccount!=null){
						boolean unblock = false;
						if(ComponentConstant.SALE_ROLE.equals(settlementAccount.getRole())){
							if(AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(settlementAccount.getStockReference())){
								unblock = true;
							}
						}else if(ComponentConstant.PURCHARSE_ROLE.equals(settlementAccount.getRole())){
							if(AccountOperationReferenceType.REFERENTIAL_SECURITIES.getCode().equals(settlementAccount.getStockReference())){
								unblock = true;
							}
						}
							
						if(unblock){
							HolderAccountBalanceTO objHolderAccountBalanceTO = populateAccountOperation(
									detail, mechanismOperation.getSecurities().getIdSecurityCodePk(), settlementAccount.getIdSettlementAccountPk());
							
							if(settlementAccount.getRole().equals(ComponentConstant.SALE_ROLE)){
								objAccountsComponentTO.getLstSourceAccounts().add(objHolderAccountBalanceTO);
							}else if(settlementAccount.getRole().equals(ComponentConstant.PURCHARSE_ROLE)){
								objAccountsComponentTO.getLstTargetAccounts().add(objHolderAccountBalanceTO);
							}
							
							if(Validations.validateListIsNullOrEmpty(objAccountsComponentTO.getLstSourceAccounts()) || 
									Validations.validateListIsNullOrEmpty(objAccountsComponentTO.getLstTargetAccounts())){
								accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
							}
						}
						
						settlementProcessService.updateSettlementAccountOperationState(
								settlementAccount.getIdSettlementAccountPk(), detail.getHolderAccountState(), loggerUser);
					}
					
					settlementProcessService.updateHolderAccountOperationState(
							detail.getHolderAccountOperation().getIdHolderAccountOperationPk(), 
							detail.getHolderAccountState(), null, 
							loggerUser, ComponentConstant.CASH_PART);
					
					if(ComponentConstant.ONE.equals(mechanismOperation.getIndTermSettlement())){
						
						settlementProcessService.updateHolderAccountOperationState(
								detail.getHolderAccountOperation().getIdHolderAccountOperationPk(), 
								HolderAccountOperationStateType.CANCELLED.getCode(), null,
								loggerUser, ComponentConstant.TERM_PART);

						settlementProcessService.updateSettlementAccountByHAOState(
								detail.getHolderAccountOperation().getIdHolderAccountOperationPk(), 
								HolderAccountOperationStateType.CANCELLED.getCode(), loggerUser, ComponentConstant.TERM_PART);

					}
					
				} else if (HolderAccountOperationStateType.REGISTERED.getCode().equals(detail.getHolderAccountState())){
					
					detail.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
					update(detail);
					
					HolderAccountOperation holderAccountOperation = detail.getHolderAccountOperation();
					holderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());
					update(holderAccountOperation);
					
					generateAccountOperationData(holderAccountOperation, mechanismOperation,false,loggerUser);
					
				}
			}
			
		}else if(ReassignmentRequestStateType.REJECTED.getCode().equals(state)){
			
			if(!reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.REGISTERED.getCode())
					&& !reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.CONFIRMED.getCode())	
					){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			reassignmentRequest.setRejectDate(CommonsUtilities.currentDateTime());
			reassignmentRequest.setRejectUser(loggerUser.getUserName());
			
			for (ReassignmentRequestDetail detail : details) {
				if(HolderAccountOperationStateType.REGISTERED.getCode().equals(detail.getHolderAccountState())){
					settlementProcessService.updateHolderAccountOperationState(
							detail.getHolderAccountOperation().getIdHolderAccountOperationPk(), 
							HolderAccountOperationStateType.CANCELLED.getCode(), null,
							loggerUser, ComponentConstant.CASH_PART);
				}
			}
		}else if(ReassignmentRequestStateType.CONFIRMED.getCode().equals(state)){
			
			if(!reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			reassignmentRequest.setConfirmDate(CommonsUtilities.currentDateTime());
			reassignmentRequest.setConfirmUser(loggerUser.getUserName());

		}
		
		reassignmentRequest.setRequestState(state);
		update(reassignmentRequest);
	}
	
	/**
	 * Populate account operation.
	 *
	 * @param detail the detail
	 * @param idSecurityCode the id security code
	 * @param idSettlementAccount the id settlement account
	 * @return the holder account balance to
	 */
	private HolderAccountBalanceTO populateAccountOperation(ReassignmentRequestDetail detail, String idSecurityCode, Long idSettlementAccount) {
		HolderAccountBalanceTO objHolderAccountBalanceTO= new HolderAccountBalanceTO();
		objHolderAccountBalanceTO.setIdHolderAccount(detail.getHolderAccountOperation().getHolderAccount().getIdHolderAccountPk());
		objHolderAccountBalanceTO.setStockQuantity(detail.getHolderAccountOperation().getStockQuantity());
		objHolderAccountBalanceTO.setIdSecurityCode(idSecurityCode);
		objHolderAccountBalanceTO.setIdParticipant(detail.getHolderAccountOperation().getInchargeStockParticipant().getIdParticipantPk());
		
		if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
			List<SettlementAccountMarketfact> settlementAccountMarketfacts = settlementProcessService.getSettlementAccountMarketfacts(idSettlementAccount);
			if(Validations.validateListIsNotNullAndNotEmpty(settlementAccountMarketfacts)){
				objHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for(SettlementAccountMarketfact settlementAccountMarketfact : settlementAccountMarketfacts){
					MarketFactAccountTO objMarketFactAccountTO = new MarketFactAccountTO();
					objMarketFactAccountTO.setMarketDate(settlementAccountMarketfact.getMarketDate());
					objMarketFactAccountTO.setMarketPrice(settlementAccountMarketfact.getMarketPrice());
					objMarketFactAccountTO.setMarketRate(settlementAccountMarketfact.getMarketRate());
					objMarketFactAccountTO.setQuantity(settlementAccountMarketfact.getMarketQuantity());
					objHolderAccountBalanceTO.getLstMarketFactAccounts().add(objMarketFactAccountTO);
				}
			}
		}
		return objHolderAccountBalanceTO;
	}


	/**
	 * Reject incharge assignment.
	 *
	 * @param tmpAccount the tmp account
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void rejectInchargeAssignment(HolderAccountOperation tmpAccount, LoggerUser loggerUser) throws ServiceException {

		Object[] arrAccountObject = getSettlementAccountDetail(tmpAccount.getIdHolderAccountOperationPk());
		HolderAccountOperation accountOperation = (HolderAccountOperation) arrAccountObject[0];
		
		Long idMechanism = (Long)arrAccountObject[4];
		Long idModality = (Long)arrAccountObject[5];
		Integer indTermSettlement = (Integer)arrAccountObject[6];
		Date cashSettlementDate = (Date)arrAccountObject[7];
		//Long idFundsParticipant = (Long)arrAccountObject[1];
		Integer accountNumber = (Integer)arrAccountObject[3];
		Long idPurchaseParticipant = (Long)arrAccountObject[9];
		Long idSaleParticipant = (Long)arrAccountObject[10];
		AssignmentProcess assigmentProcess = negotiationOperationServiceBean.findAssignmentProcess(idMechanism, idModality, cashSettlementDate);
		if(!AssignmentProcessStateType.OPENED.getCode().equals(assigmentProcess.getAssignmentState())){
			throw new ServiceException(ErrorServiceType.ASSIGNMENT_PROCESS_CLOSED);
		}
		
		if(!HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode().equals(accountOperation.getInchargeState())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_INCHARGE_STATE_MODIFY);
		}
				
		if(!HolderAccountOperationStateType.REGISTERED.getCode().equals(accountOperation.getHolderAccountState())){
			Object[] bodyData = new Object[2];
			bodyData[0] = accountNumber.toString();
			bodyData[1] = ParticipantRoleType.get(accountOperation.getRole()).getValue();
			throw new ServiceException(ErrorServiceType.ASSIGNMENT_HOLDER_ACCOUNT_MODIFIED, bodyData);
		}
		
		
		accountOperation.setHolderAccountState(HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode()); 
		accountOperation.setInchargeState(HolderAccountOperationStateType.REJECTED_INCHARGE_STATE.getCode());
		accountOperation.setMotiveReject(tmpAccount.getMotiveReject());
		accountOperation.setComments(tmpAccount.getComments());
		update(accountOperation);
		
		settlementProcessService.updateSettlementAccountByHAOState(accountOperation.getIdHolderAccountOperationPk(),
				HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), loggerUser, ComponentConstant.CASH_PART);
		
		if(ComponentConstant.ONE.equals(indTermSettlement)){
			
			settlementProcessService.updateHolderAccountOperationState(accountOperation.getIdHolderAccountOperationPk(),
					HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), 
					HolderAccountOperationStateType.REJECTED_INCHARGE_STATE.getCode(),
					loggerUser, ComponentConstant.TERM_PART);
			
			settlementProcessService.updateSettlementAccountByHAOState(accountOperation.getIdHolderAccountOperationPk(),
					HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), loggerUser, ComponentConstant.TERM_PART);
			
		}
		
		detach(accountOperation);
		Long idParticipant = null;
		if(ComponentConstant.PURCHARSE_ROLE.equals(accountOperation.getRole())){
			idParticipant = idPurchaseParticipant;
		}else if(ComponentConstant.SALE_ROLE.equals(accountOperation.getRole())){
			idParticipant = idSaleParticipant;
		}
		accountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		accountOperation.setRegisterUser(loggerUser.getUserName());
		accountOperation.setAccountOperationValorizations(null);
		accountOperation.setInchargeStockParticipant(new Participant(idParticipant));
		accountOperation.setInchargeFundsParticipant(new Participant(idParticipant));
		accountOperation.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
		accountOperation.setIndIncharge(ComponentConstant.ZERO);
		accountOperation.setInchargeState(null);
		
		if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact()) && ComponentConstant.SALE_ROLE.equals(accountOperation.getRole())){
			List<AccountOperationMarketFact> accountMarketFacts = settlementProcessService.getAccountOperationMarketfacts(accountOperation.getIdHolderAccountOperationPk());
			for (AccountOperationMarketFact accountOperationMarketFact : accountMarketFacts) {
				detach(accountOperationMarketFact);
				accountOperationMarketFact.setIdAccountOperMarketFactPk(null);
				accountOperationMarketFact.setHolderAccountOperation(accountOperation);
			}
		}
		
		accountOperation.setIdHolderAccountOperationPk(null);
		create(accountOperation);
		
		generateAccountOperationData(accountOperation, accountOperation.getMechanismOperation(), true,loggerUser);
		
	}


	/**
	 * Confirm incharge assignment.
	 *
	 * @param tmpAccountOperation the tmp account operation
	 * @param holderAccount the holder account
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void confirmInchargeAssignment(HolderAccountOperation tmpAccountOperation,
			HolderAccount holderAccount, LoggerUser loggerUser) throws ServiceException {
		
		Object[] arrAccountObject = getSettlementAccountDetail(tmpAccountOperation.getIdHolderAccountOperationPk());
		HolderAccountOperation accountOperation = (HolderAccountOperation) arrAccountObject[0];
		
		Long idMechanism = (Long)arrAccountObject[4];
		Long idModality = (Long)arrAccountObject[5];
		Integer indTermSettlement = (Integer)arrAccountObject[6];
		Date cashSettlementDate = (Date)arrAccountObject[7];
		Long idFundsParticipant = (Long)arrAccountObject[1];
		Integer accountNumber = (Integer)arrAccountObject[3];
		Long idNegotiatiorParticipant = null;
		if(ComponentConstant.PURCHARSE_ROLE.equals(tmpAccountOperation.getRole())){
			idNegotiatiorParticipant = (Long)arrAccountObject[9];
		}else if(ComponentConstant.SALE_ROLE.equals(tmpAccountOperation.getRole())){
			idNegotiatiorParticipant = (Long)arrAccountObject[10];
		}
		Integer instrumentType = (Integer)arrAccountObject[11];
		
		AssignmentProcess assigmentProcess = negotiationOperationServiceBean.findAssignmentProcess(idMechanism, idModality, cashSettlementDate);
		if(!AssignmentProcessStateType.OPENED.getCode().equals(assigmentProcess.getAssignmentState())){
			throw new ServiceException(ErrorServiceType.ASSIGNMENT_PROCESS_CLOSED);
		}
		
		if(!HolderAccountOperationStateType.REGISTERED_INCHARGE_STATE.getCode().equals(accountOperation.getInchargeState())){
			throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_INCHARGE_STATE_MODIFY);
		}
		
		if(!HolderAccountOperationStateType.REGISTERED.getCode().equals(accountOperation.getHolderAccountState())){
			Object[] bodyData = new Object[2];
			bodyData[0] = accountNumber.toString();
			bodyData[1] = ParticipantRoleType.get(accountOperation.getRole()).getValue();
			throw new ServiceException(ErrorServiceType.ASSIGNMENT_HOLDER_ACCOUNT_MODIFIED, bodyData);
		}
		
		settlementProcessService.updateHolderAccountOperationState(accountOperation.getIdHolderAccountOperationPk(),
				HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), 
				HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode(),
				loggerUser, ComponentConstant.CASH_PART);
		
		settlementProcessService.updateSettlementAccountByHAOState(accountOperation.getIdHolderAccountOperationPk(),
				HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), loggerUser, ComponentConstant.CASH_PART);
		
		if(ComponentConstant.ONE.equals(indTermSettlement)){
			
			settlementProcessService.updateHolderAccountOperationState(accountOperation.getIdHolderAccountOperationPk(),
					HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), 
					HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode(),
					loggerUser, ComponentConstant.TERM_PART);
			
			settlementProcessService.updateSettlementAccountByHAOState(accountOperation.getIdHolderAccountOperationPk(),
					HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode(), loggerUser, ComponentConstant.TERM_PART);
			
		}
		
		detach(accountOperation);
		accountOperation.setIdHolderAccountOperationPk(null);
		accountOperation.setAccountOperationMarketFacts(null);
		accountOperation.setAccountOperationValorizations(null);
		accountOperation.setRegisterDate(CommonsUtilities.currentDateTime());
		accountOperation.setRegisterUser(loggerUser.getUserName());
		accountOperation.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
		accountOperation.setInchargeState(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode());
		accountOperation.setHolderAccount(holderAccount);
		
		create(accountOperation);
		
		if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact()) && ComponentConstant.SALE_ROLE.equals(accountOperation.getRole())){
			
			accountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
			for(AccountOperationMarketFact accountOperationMarketFact : tmpAccountOperation.getAccountOperationMarketFacts()){
				accountOperationMarketFact.setHolderAccountOperation(accountOperation);
				accountOperation.getAccountOperationMarketFacts().add(accountOperationMarketFact);
			}
			
			boolean inconsistent = settlementProcessService.isInconsistentAccountOperation(
					accountOperation.getIdHolderAccountOperationPk(), accountOperation.getAccountOperationMarketFacts(), instrumentType);
			
			if(inconsistent){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_MARKET_FACT_INCONSISTENCIES_INCHARGE);
			}
		}
		
		SettlementAccountOperation settlementAccountOperation = generateAccountOperationData(accountOperation, accountOperation.getMechanismOperation(),true,loggerUser);
		
		// TODO ONLY CONSIDERING CASH PART, COMPLETE IF INCHARGE IS FOR CASH AND TERM PARTS.
		if(!idNegotiatiorParticipant.equals(idFundsParticipant)){
			updateParticipantSettlements(settlementAccountOperation,idNegotiatiorParticipant,idFundsParticipant);
		}

	}
	
	/**
	 * Update participant settlements.
	 *
	 * @param settlementAccountOperation the settlement account operation
	 * @param idInitialParticipant the id initial participant
	 * @param idFundsParticipant the id funds participant
	 * @throws ServiceException the service exception
	 */
	public void updateParticipantSettlements(
			SettlementAccountOperation settlementAccountOperation, Long idInitialParticipant, Long idFundsParticipant) throws ServiceException {
		SettlementOperation settlementOperation = settlementAccountOperation.getSettlementOperation();
		
		// create new (incharge) participant data 
		ParticipantSettlement participantSettlement = settlementProcessService.populateParticipantSettlement(
				settlementOperation, settlementAccountOperation.getStockQuantity(), idFundsParticipant, 
				settlementAccountOperation.getRole(), ComponentConstant.ONE);
		
		settlementProcessService.create(participantSettlement);
		
		// update initial (negotiation) participant data 
		ParticipantSettlement initialParticipantSettlement = (ParticipantSettlement)settlementProcessService.getParticipantSettlements(
				settlementOperation.getIdSettlementOperationPk(), settlementAccountOperation.getRole(), idInitialParticipant);
		
		initialParticipantSettlement.setStockQuantity(initialParticipantSettlement.getStockQuantity().subtract(settlementAccountOperation.getStockQuantity()));
		initialParticipantSettlement.setSettlementAmount(NegotiationUtils.getSettlementAmount(settlementOperation.getSettlementPrice(), initialParticipantSettlement.getStockQuantity()));
		if(initialParticipantSettlement.getStockQuantity().compareTo(BigDecimal.ZERO) <= 0){
			initialParticipantSettlement.setOperationState(HolderAccountOperationStateType.REJECTED_INCHARGE_STATE.getCode());
		}
		settlementProcessService.update(initialParticipantSettlement);
	}

	/**
	 * Synchronize settlement account market fact.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void synchronizeSettlementAccountMarketfact(Long idAssignmentProcess, LoggerUser loggerUser) throws ServiceException {
		try {
			//we synchronize the market facts about term settlements according reported balance 
			synchronizeMarketFact(idAssignmentProcess, ComponentConstant.REPORTED_BALANCE, BooleanType.YES.getCode(), loggerUser);
			//we synchronize the market facts about term settlements according purchase balance
			synchronizeMarketFact(idAssignmentProcess, ComponentConstant.PURCHASE_BALANCE, BooleanType.YES.getCode(), loggerUser);
			//we synchronize the market facts about cash settlements according purchase balance
			synchronizeMarketFact(idAssignmentProcess, ComponentConstant.PURCHASE_BALANCE, BooleanType.NO.getCode(), loggerUser);
			//we synchronize the market facts about cash settlements according reported balance
			synchronizeMarketFact(idAssignmentProcess, ComponentConstant.REPORTED_BALANCE, BooleanType.NO.getCode(), loggerUser);
		} catch (Exception ex) {
			logger.error(":::::::::::::: synchronizeSettlementAccountMarketfact ::::::::::::::");
			throw new ServiceException(ErrorServiceType.SYNCHRONIZE_SETTLEMENT_ACCOUNT_MARKETFACT);
		}
	}
	
	/**
	 * Synchronize market fact.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @param balanceType the balance type
	 * @param indTermSettlement the ind term settlement
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void synchronizeMarketFact(Long idAssignmentProcess, int balanceType, Integer indTermSettlement, LoggerUser loggerUser) throws ServiceException{
		try {
			//we get the list of account market fact to be updated
			List<Object[]> lstAccountMarkeftfactAssignment= getListSettlementAccountMarketfactAssignment(idAssignmentProcess, balanceType, indTermSettlement);
			if (Validations.validateListIsNotNullAndNotEmpty(lstAccountMarkeftfactAssignment)) {
				//we update the assigned market facts for each account inside operation
				for (Object[] arrAccountMarketfact: lstAccountMarkeftfactAssignment) {
					Long idSettAccountMarketfact= new Long(arrAccountMarketfact[0].toString());
					BigDecimal marketRate= new BigDecimal(arrAccountMarketfact[1].toString());
					Date marketDate= (Date) arrAccountMarketfact[2];
					updateSettlementAccountMarketfactAssignment(idSettAccountMarketfact, marketRate, marketDate, loggerUser);
					Integer indHasTermSettlement= new Integer(arrAccountMarketfact[4].toString());
					if (BooleanType.YES.getCode().equals(indHasTermSettlement)) {
						//this DPF operation has term part. So we must update the market fact term operation also
						Long idSettlementAccount= new Long(arrAccountMarketfact[3].toString());
						updateSettlementTermMarketFact(idSettlementAccount, marketRate, marketDate, loggerUser);
					}
				}
			}
		} catch (Exception ex) {
			logger.error(":::::::::::::: synchronizeMarketFact ::::::::::::::");
			logger.error("indTermSettlement: " +indTermSettlement + ", balanceType: " +balanceType);
			throw new ServiceException(ErrorServiceType.SYNCHRONIZE_SETTLEMENT_ACCOUNT_MARKETFACT);	
		}
	}
	
	
	/**
	 * Gets the list settlement account marketfact assignment.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @param balanceType the balance type
	 * @param indTermSettlement the ind term settlement
	 * @return the list settlement account marketfact assignment
	 */
	@SuppressWarnings("unchecked")
	private List<Object[]> getListSettlementAccountMarketfactAssignment(Long idAssignmentProcess, int balanceType, Integer indTermSettlement) {
		StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT SAM.idSettAccountMarketfactPk, ");
		stringBuffer.append(" HMB.marketRate, ");
		stringBuffer.append(" HMB.marketDate, ");
		stringBuffer.append(" SAM.settlementAccountOperation.idSettlementAccountPk, ");
		stringBuffer.append(" SAM.settlementAccountOperation.settlementOperation.mechanismOperation.indTermSettlement ");
		stringBuffer.append(" FROM SettlementAccountMarketfact SAM, HolderMarketFactBalance HMB ");
		stringBuffer.append(" WHERE SAM.settlementAccountOperation.settlementOperation.mechanismOperation.securities.securityClass in (:lstSecurityClass) ");
		if (Validations.validateIsNotNull(idAssignmentProcess)) {
			stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.mechanismOperation.assignmentProcess.idAssignmentProcessPk = :idAssignmentProcess ");
		}
		stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.operationPart = :operationPart ");
		stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.operationState = :operationState ");
		stringBuffer.append(" and SAM.settlementAccountOperation.operationState = :settlementAccountState ");
		stringBuffer.append(" and SAM.settlementAccountOperation.role = :role ");
		stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.operationPart = SAM.settlementAccountOperation.holderAccountOperation.operationPart ");
		stringBuffer.append(" and HMB.holderAccount.idHolderAccountPk = SAM.settlementAccountOperation.holderAccountOperation.holderAccount.idHolderAccountPk ");
		stringBuffer.append(" and HMB.security.idSecurityCodePk = SAM.settlementAccountOperation.settlementOperation.mechanismOperation.securities.idSecurityCodePk ");
		stringBuffer.append(" and HMB.indActive = :indActive ");
		stringBuffer.append(" and SAM.indActive = :indActive ");
		stringBuffer.append(" and SAM.settlementAccountOperation.stockReference is null ");
		stringBuffer.append(" and (HMB.totalBalance > 0 ");
		if (ComponentConstant.PURCHASE_BALANCE == balanceType) {
			stringBuffer.append(" or HMB.purchaseBalance > 0) ");
		} else if (ComponentConstant.REPORTED_BALANCE == balanceType) {
			stringBuffer.append(" or HMB.reportedBalance > 0) ");
		}
		stringBuffer.append(" and SAM.settlementAccountOperation.settlementOperation.mechanismOperation.indTermSettlement = :indTermSettlement ");
		stringBuffer.append(" and (SAM.marketRate != HMB.marketRate or SAM.marketDate != HMB.marketDate) ");
		stringBuffer.append(" and not exists ( ");
		stringBuffer.append("	select hcd.idHolderChainDetailPk from HolderChainDetail hcd ");
		stringBuffer.append("	inner join hcd.chainedHolderOperation cho ");
		stringBuffer.append("	where hcd.settlementAccountMarketfact.settlementAccountOperation.idSettlementAccountPk = SAM.settlementAccountOperation.idSettlementAccountPk ");
		stringBuffer.append("	and cho.chaintState in  (:lstChainStates)  ) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		List<Integer> lstSecurityClass= new ArrayList<Integer>();
		lstSecurityClass.add(SecurityClassType.DPA.getCode());
		lstSecurityClass.add(SecurityClassType.DPF.getCode());
		query.setParameter("lstSecurityClass", lstSecurityClass);
		if (Validations.validateIsNotNull(idAssignmentProcess)) {
			query.setParameter("idAssignmentProcess", idAssignmentProcess);
		}
		query.setParameter("operationPart", OperationPartType.CASH_PART.getCode());
		query.setParameter("operationState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("settlementAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("role", ParticipantRoleType.SELL.getCode());
		query.setParameter("indActive", BooleanType.YES.getCode());
		query.setParameter("indTermSettlement", indTermSettlement);
		List<Integer> lstChainStates= new ArrayList<Integer>();
		lstChainStates.add(ChainedOperationStateType.REGISTERED.getCode());
		lstChainStates.add(ChainedOperationStateType.CONFIRMED.getCode());
		query.setParameter("lstChainStates", lstChainStates);
		
		return query.getResultList();
	}
	
	/**
	 * Update settlement account marketfact assignment.
	 *
	 * @param idSettAccountMarketfact the id sett account marketfact
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param loggerUser the logger user
	 */
	private void updateSettlementAccountMarketfactAssignment(Long idSettAccountMarketfact, BigDecimal marketRate, Date marketDate, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE SettlementAccountMarketfact ");
		stringBuffer.append(" SET marketDate = :marketDate ");
		stringBuffer.append(" , marketRate = :marketRate ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idSettAccountMarketfactPk = :idSettAccountMarketfact ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettAccountMarketfact", idSettAccountMarketfact);
		query.setParameter("marketDate", marketDate);
		query.setParameter("marketRate", marketRate);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	
	
	/**
	 * Update settlement term market fact.
	 *
	 * @param idSettlementAccount the id settlement account
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateSettlementTermMarketFact(Long idSettlementAccount, BigDecimal marketRate, Date marketDate, LoggerUser loggerUser) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" UPDATE SettlementAccountMarketfact set marketRate = :marketRate ");
		stringBuffer.append(" , marketDate = :marketDate ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE settlementAccountOperation.idSettlementAccountPk = ( ");
		stringBuffer.append("           select saoref.idSettlementAccountPk from SettlementAccountOperation saoref ");
		stringBuffer.append("           where saoref.settlementAccountOperation.idSettlementAccountPk = :idSettlementAccount ) ");
				
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idSettlementAccount", idSettlementAccount);
		query.setParameter("marketRate", marketRate);
		query.setParameter("marketDate", marketDate);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		return query.executeUpdate();
	}
	
	/**
	 * Gets the list participant assignment.
	 *
	 * @param processDate the process date
	 * @return the list participant assignment
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListParticipantAssignment(Date processDate) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT  ");
		stringBuffer.append(" PAS.idParticipantAssignmentPk ");
		stringBuffer.append(" FROM ParticipantAssignment PAS ");
		stringBuffer.append(" WHERE PAS.assignmentProcess.settlementDate = :processDate ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("processDate", processDate);
		
		return query.getResultList();
	}
	
	/**
	 * Update participant assignment.
	 *
	 * @param lstParticipantAssignment the lst participant assignment
	 * @param processState the process state
	 * @param loggerUser the logger user
	 */
	public void updateParticipantAssignment(List<Long> lstParticipantAssignment, Integer processState, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ParticipantAssignment ");
		stringBuffer.append(" SET assignmentState = :processState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idParticipantAssignmentPk in (:lstParticipantAssignment) ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("lstParticipantAssignment", lstParticipantAssignment);
		query.setParameter("processState", processState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", CommonsUtilities.currentDateTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update participant assignment.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @param state the state
	 * @param indIncharge the ind incharge
	 * @param inchargeState the incharge state
	 * @param loggerUser the logger user
	 */
	public void updateHolderAccountOperation(HolderAccountOperation holderAccountOperation, Integer state, Integer indIncharge, Integer inchargeState,Long IdMechanismOperationPk, LoggerUser loggerUser) {
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE HolderAccountOperation ");
		stringBuffer.append(" SET holderAccountState = :holderAccountState ");
		stringBuffer.append(" , indIncharge = :indIncharge ");
		stringBuffer.append(" , inchargeState = :inchargeState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
//		if (IdMechanismOperationPk !=null){
//			stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk =:idMechanismOperationPk ");
//		}else{
			stringBuffer.append(" WHERE idHolderAccountOperationPk =:idHolderAccountOperationPk ");
//		}
		Query query = em.createQuery(stringBuffer.toString());
		
//		if (IdMechanismOperationPk !=null){
//			query.setParameter("idMechanismOperationPk", IdMechanismOperationPk);
//		}else{
			query.setParameter("idHolderAccountOperationPk", holderAccountOperation.getIdHolderAccountOperationPk());
//		}
		query.setParameter("holderAccountState", state);
		query.setParameter("indIncharge", indIncharge);
		query.setParameter("inchargeState", inchargeState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", CommonsUtilities.currentDateTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Send executed sale purchase update batch.
	 *
	 * @param loggerUser the logger user
	 * @throws Exception the exception
	 */
	public void sendExecutedSalePurchaseUpdateBatch(LoggerUser loggerUser) throws Exception{
		logger.info("SalePurchaseUpdateBatch: Start the process ...");
		try {
			//we send the update sale and purchase balance process
			BusinessProcess objBusinessProcess= new BusinessProcess();
			objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode());
			Map<String, Object> processParameters=new HashMap<String, Object>();
			processParameters.put(ComponentConstant.SETTLEMENT_SCHEMA, SettlementSchemaType.NET.getCode());
			batchServiceBean.registerBatch(loggerUser.getUserName(), objBusinessProcess, processParameters);
		} catch (Exception e) {
			logger.error("SalePurchaseUpdateBatch : Error in processing ... ");			
			throw e;
		}
		logger.info("SalePurchaseUpdateBatch: Finish the process ... ");	
	}	
	
	/**
	 * Gets the holder account operations market fact.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param role the role
	 * @param idParticipant the id participant
	 * @return the holder account operations market fact
	 */
	@SuppressWarnings("unchecked")
	public List<SettlementAccountMarketfact> getSettlementAccountMarketfactByReassignment(Long idMechanismOperation, 
			List<Integer> role, Long idParticipant) {
		Map<Long,HolderAccount> accounts = new HashMap<Long, HolderAccount>();
		List<Object[]> accountOperations = null;
		List<SettlementAccountMarketfact> lstSettlementAccountMarketfact = new ArrayList<SettlementAccountMarketfact>();
		Map<String,Object> parameters = new HashMap<String,Object> ();
		StringBuilder querySql = new StringBuilder();
		querySql.append(" SELECT sam, hao.holderAccount.idHolderAccountPk, hao						");
		querySql.append(" FROM SettlementAccountMarketfact sam										");
		querySql.append(" inner join fetch sam.settlementAccountOperation sao 						");
		querySql.append(" inner join sao.holderAccountOperation hao 								");
		querySql.append(" inner join fetch hao.inchargeFundsParticipant fundsP 						");
		querySql.append(" inner join fetch hao.inchargeStockParticipant stockP 						");		
		querySql.append(" where hao.operationPart = :cashPart 										");
		querySql.append(" and hao.mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		querySql.append(" and hao.holderAccountState in (:confirmedState) 							");	
		querySql.append(" and sam.indActive = :indicatorOne   										");
		parameters.put("idMechanismOperation", idMechanismOperation);
		parameters.put("confirmedState", HolderAccountOperationStateType.CONFIRMED.getCode());	
		parameters.put("indicatorOne", ComponentConstant.ONE);
		if(idParticipant!=null){
			querySql.append(" and hao.inchargeStockParticipant.idParticipantPk = :idParticipant  ");
			parameters.put("idParticipant", idParticipant);
		}
		if(role!=null){
			querySql.append(" and hao.role in (:idRole) ");
			parameters.put("idRole", role);
		}
		querySql.append(" order by hao.operationPart, hao.role");
		parameters.put("cashPart", ComponentConstant.CASH_PART);			
		
		accountOperations = (List<Object[]>) findListByQueryString(querySql.toString(), parameters);		
		for(Object[] accOperation: accountOperations){
			SettlementAccountMarketfact sam = (SettlementAccountMarketfact) accOperation[0];			
			Long idHolderAccount = (Long) accOperation[1];
			HolderAccountOperation hao = (HolderAccountOperation) accOperation[2];
			HolderAccount holderAccount = accounts.get(idHolderAccount);
			if(holderAccount==null){
				holderAccount = mechanismOperationService.getHolderAccount(idHolderAccount);
				accounts.put(idHolderAccount, holderAccount);
			}
			hao.setHolderAccount(holderAccount);
			if(hao.getRole().equals(ComponentConstant.SALE_ROLE)){
				hao.setAccountOperationMarketFacts(getAccountOperationMarketfacts(hao.getIdHolderAccountOperationPk()));
			}
			sam.getSettlementAccountOperation().setHolderAccountOperation(hao);							
			lstSettlementAccountMarketfact.add(sam);
		}			
		return lstSettlementAccountMarketfact;
	}
	
	/**
	 * Save reassignment request market fact.
	 *
	 * @param reassignmentRequest the reassignment request
	 * @param lstSettlementAccountMarketfact the lst settlement account marketfact
	 * @param lstSettlementAccountMarketfactInitial the lst settlement account marketfact initial
	 * @param isFixedIncome the is fixed income
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void saveReassignmentRequestMarketFact(ReassignmentRequest reassignmentRequest, List<SettlementAccountMarketfact> lstSettlementAccountMarketfact, 
			List<SettlementAccountMarketfact> lstSettlementAccountMarketfactInitial, boolean isFixedIncome, LoggerUser loggerUser) throws ServiceException {
		
		checkPreviousReassignmentRequest(reassignmentRequest.getMechanismOperation().getIdMechanismOperationPk(),
				reassignmentRequest.getParticipant().getIdParticipantPk());
		Boolean operationCruze=false;
		//issue 115
		int count = 0;
		String strOperation = GeneralConstants.EMPTY_STRING;
		for(SettlementAccountMarketfact objSettlementAccountMarketfact : lstSettlementAccountMarketfact) {
			if(objSettlementAccountMarketfact.isSelected()) {
				for(SettlementAccountMarketfact objSettlementAccountMarketfactInitial : lstSettlementAccountMarketfactInitial) {
		
				//	if(!isFixedIncome) { 
		
					if(isFixedIncome) {
						if(objSettlementAccountMarketfact.getMarketDate().compareTo(objSettlementAccountMarketfactInitial.getMarketDate()) == 0
								&& objSettlementAccountMarketfact.getMarketRate().compareTo(objSettlementAccountMarketfactInitial.getMarketRate()) == 0
								){	
							if(GeneralConstants.EMPTY_STRING.compareTo(strOperation) == 0){
								strOperation = GeneralConstants.EMPTY_STRING + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk();
							} else {
								strOperation = strOperation + GeneralConstants.STR_COMMA_WITHOUT_SPACE + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk()  ;
							}							
							//count++;
						}
					} else {
						if(objSettlementAccountMarketfact.getMarketDate().compareTo(objSettlementAccountMarketfactInitial.getMarketDate()) == 0
								&& objSettlementAccountMarketfact.getMarketPrice().compareTo(objSettlementAccountMarketfactInitial.getMarketPrice()) == 0){
							if(GeneralConstants.EMPTY_STRING.compareTo(strOperation) == 0){
								strOperation = GeneralConstants.EMPTY_STRING + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk();
							} else {
								strOperation = strOperation + GeneralConstants.STR_COMMA_WITHOUT_SPACE + objSettlementAccountMarketfact.getIdSettAccountMarketfactPk()  ;
							}							
							//count++;
						}
					}
				}
			}
		}
		if(count > 0){
			throw new ServiceException(ErrorServiceType.REASSIGNMENT_REQUEST_NOT_CANGES_MARKETFACT, new Object[] {strOperation});
		}
		
		reassignmentRequest.setRegisterUser(loggerUser.getUserName());
		reassignmentRequest.setRegisterDate(CommonsUtilities.currentDateTime());
		
		create(reassignmentRequest);
		
		//verificando que sea cruzada 
		if (reassignmentRequest.getMechanismOperation().getBuyerParticipant().getIdParticipantPk().equals
				(reassignmentRequest.getMechanismOperation().getSellerParticipant().getIdParticipantPk())){
			operationCruze=true;
		}else{ operationCruze=false;}
		
		HashMap<Long, Long> hmIdSettlementAccountMF = new HashMap<>();
		
		for(SettlementAccountMarketfact objSettlementAccountMarketfact : lstSettlementAccountMarketfact) {
		
			HolderAccountOperation holderAccountOperation = objSettlementAccountMarketfact.getSettlementAccountOperation().getHolderAccountOperation();			
			if(objSettlementAccountMarketfact.isSelected()) {
				ReassignmentRequestDetail reassignmentRequestDetail = new ReassignmentRequestDetail();
				reassignmentRequestDetail.setReassignmentRequest(reassignmentRequest);
				reassignmentRequestDetail.setHolderAccountOperation(holderAccountOperation);
				reassignmentRequestDetail.setSettlementAccountMarketfact(objSettlementAccountMarketfact);
				reassignmentRequestDetail.setHolderAccountState(holderAccountOperation.getHolderAccountState());
				List<HolderAccountOperation> lstExistingHolderAccountOperation = getHolderAccountOperation(reassignmentRequest.getMechanismOperation().getIdMechanismOperationPk());				
				if (operationCruze.equals(false)){
					validateAssignmentConditions(holderAccountOperation,lstExistingHolderAccountOperation);
				}
				reassignmentRequestDetail.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
				reassignmentRequestDetail.setMarketDate(objSettlementAccountMarketfact.getMarketDate());
				reassignmentRequestDetail.setMarketRate(objSettlementAccountMarketfact.getMarketRate());
				reassignmentRequestDetail.setMarketPrice(objSettlementAccountMarketfact.getMarketPrice());
				reassignmentRequestDetail.setMarketQuantity(objSettlementAccountMarketfact.getMarketQuantity());
				
				reassignmentRequestDetail.setIndOld(GeneralConstants.ONE_VALUE_INTEGER);
				
				hmIdSettlementAccountMF.put(objSettlementAccountMarketfact.getIdSettAccountMarketfactPk(),objSettlementAccountMarketfact.getIdSettAccountMarketfactPk());
				
				create(reassignmentRequestDetail);
			}
		}	
		
		for(SettlementAccountMarketfact objSettlementAccountMarketfactInitialHM : lstSettlementAccountMarketfactInitial) {
			if(Validations.validateIsNotNullAndNotEmpty(hmIdSettlementAccountMF.get(objSettlementAccountMarketfactInitialHM.getIdSettAccountMarketfactPk()))){
				
				HolderAccountOperation holderAccountOperation = objSettlementAccountMarketfactInitialHM.getSettlementAccountOperation().getHolderAccountOperation();		
				
				ReassignmentRequestDetail reassignmentRequestDetail = new ReassignmentRequestDetail();
				reassignmentRequestDetail.setReassignmentRequest(reassignmentRequest);
				reassignmentRequestDetail.setHolderAccountOperation(holderAccountOperation);
				reassignmentRequestDetail.setSettlementAccountMarketfact(objSettlementAccountMarketfactInitialHM);
				reassignmentRequestDetail.setHolderAccountState(holderAccountOperation.getHolderAccountState());
				List<HolderAccountOperation> lstExistingHolderAccountOperation = getHolderAccountOperation(reassignmentRequest.getMechanismOperation().getIdMechanismOperationPk());				
				if (operationCruze.equals(false)){
					validateAssignmentConditions(holderAccountOperation,lstExistingHolderAccountOperation);
				}
				reassignmentRequestDetail.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
				reassignmentRequestDetail.setMarketDate(objSettlementAccountMarketfactInitialHM.getMarketDate());
				reassignmentRequestDetail.setMarketRate(objSettlementAccountMarketfactInitialHM.getMarketRate());
				reassignmentRequestDetail.setMarketPrice(objSettlementAccountMarketfactInitialHM.getMarketPrice());
				reassignmentRequestDetail.setMarketQuantity(objSettlementAccountMarketfactInitialHM.getMarketQuantity());
				
				reassignmentRequestDetail.setIndOld(GeneralConstants.ZERO_VALUE_INTEGER);
				
				create(reassignmentRequestDetail);
			}
		}
		
	}
	
	/**
	 * Update state reassignment request.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @param mechanismOperation the mechanism operation
	 * @param state the state
	 * @param loggerUser the logger user
	 * @param details the details
	 * @param isFixedIncome the is fixed income
	 * @throws ServiceException the service exception
	 */
	public void updateStateReassignmentRequestMarketFact(Long idReassignmentRequest, MechanismOperation mechanismOperation, Integer state, 
			LoggerUser loggerUser, List<ReassignmentRequestDetail> details, boolean isFixedIncome) throws ServiceException {
		ReassignmentRequest reassignmentRequest = find(ReassignmentRequest.class, idReassignmentRequest);
		
		if(ReassignmentRequestStateType.AUTHORIZED.getCode().equals(state)){
			
			if(!reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.CONFIRMED.getCode())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			reassignmentRequest.setAuthorizeDate(CommonsUtilities.currentDateTime());
			reassignmentRequest.setAuthorizeUser(loggerUser.getUserName());			
			
			for (ReassignmentRequestDetail detail : details) {
				if(detail.getSettlementAccountMarketfact() != null && Validations.validateIsNotNullAndNotEmpty(detail.getMarketDate())){
					
					detail.getHolderAccountOperation().setMechanismOperation(mechanismOperation);
					
					chainedOperationsServiceBean.validateChainedHolderOperation(detail.getHolderAccountOperation().getIdHolderAccountOperationPk(), null);
					
					settlementProcessService.updateReassignmentRequestAccountOperationMarketFact(detail.getHolderAccountOperation().getIdHolderAccountOperationPk(),
							detail.getMarketDate(), detail.getMarketRate(), detail.getMarketPrice(), isFixedIncome, loggerUser);
					
					settlementProcessService.updateReassignmentRequestSettlementAccountMarketfact(detail.getSettlementAccountMarketfact().getIdSettAccountMarketfactPk(),
							detail.getMarketDate(), detail.getMarketRate(), detail.getMarketPrice(), isFixedIncome, loggerUser);
					
					if(ComponentConstant.ONE.equals(mechanismOperation.getIndTermSettlement())){
						
						SettlementAccountOperation settlementAccountOperation = settlementProcessService.getSettlementAccountOperation(detail.getHolderAccountOperation().getIdHolderAccountOperationPk());
						
						HolderAccountOperation hActOperation = getHolderAccountOperationTermPart(detail.getHolderAccountOperation().getIdHolderAccountOperationPk());						
						settlementProcessService.updateReassignmentRequestAccountOperationMarketFact(hActOperation.getIdHolderAccountOperationPk(),
								detail.getMarketDate(), detail.getMarketRate(), detail.getMarketPrice(), isFixedIncome, loggerUser);
						
						SettlementAccountOperation termSettlementAccount = settlementProcessService.getSettlementAccountOperationRef(settlementAccountOperation.getIdSettlementAccountPk());
						
						/* Initial Update Settlement Account Market fact */						
						if(Validations.validateListIsNotNullAndNotEmpty(termSettlementAccount.getSettlementAccountMarketfacts())){
							for(SettlementAccountMarketfact objSettlementAccountMarketfact : termSettlementAccount.getSettlementAccountMarketfacts()) {								
								settlementProcessService.updateReassignmentRequestSettlementAccountMarketfact(objSettlementAccountMarketfact.getIdSettAccountMarketfactPk(),
										detail.getMarketDate(), detail.getMarketRate(), detail.getMarketPrice(), isFixedIncome, loggerUser);										
							}							
						}								
						/* End Update Settlement Account Market fact */							
					}
				}								
			}
			
		} else if(ReassignmentRequestStateType.REJECTED.getCode().equals(state)){
			
			if(!reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.REGISTERED.getCode())
					&& !reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.CONFIRMED.getCode())
					){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			reassignmentRequest.setRejectDate(CommonsUtilities.currentDateTime());
			reassignmentRequest.setRejectUser(loggerUser.getUserName());			
			
		} else if(ReassignmentRequestStateType.CONFIRMED.getCode().equals(state)){
			
			if(!reassignmentRequest.getRequestState().equals(ReassignmentRequestStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
			}
			
			reassignmentRequest.setConfirmDate(CommonsUtilities.currentDateTime());
			reassignmentRequest.setConfirmUser(loggerUser.getUserName());			
		}
		
		reassignmentRequest.setRequestState(state);
		update(reassignmentRequest);
	}
	
	/**
	 * Update assignment request state by pk.
	 *
	 * @param idAssignmentRequestPk the id assignment request pk
	 * @param requestState the request state
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int updateAssignmentRequestStateByPk(Long idAssignmentRequestPk, Integer requestState,  LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" UPDATE AssignmentRequest set requestState = :requestState ");
		stringBuffer.append(" , confirmDate = :lastModifyDate ");
		stringBuffer.append(" , confirmUser = :lastModifyUser ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idAssignmentRequestPk = :idAssignmentRequestPk ");			
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idAssignmentRequestPk", idAssignmentRequestPk);		
		query.setParameter("requestState", requestState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());		
		return query.executeUpdate();
	}
	
	/**
	 * Gets the incharge state.
	 *
	 * @param idHolderAccountOperationPk the id holder account operation pk
	 * @return the incharge state
	 */
	public Integer getInchargeStateMassive(Long idHolderAccountOperationPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hao.inchargeState");
			sbQuery.append("	FROM HolderAccountOperation hao");
			sbQuery.append("	WHERE hao.idHolderAccountOperationPk = :idHolderAccountOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountOperationPk", idHolderAccountOperationPk);
			return (Integer) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the list mnemonic participant assignment.
	 *
	 * @param processDate the process date
	 * @return the list mnemonic participant assignment
	 */
	@SuppressWarnings("unchecked")
	public List<String> getListMnemonicParticipantAssignment(Date processDate) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT  									");
		stringBuffer.append(" DISTINCT PA.mnemonic 						");
		stringBuffer.append(" FROM ParticipantAssignment PAS 			");
		stringBuffer.append(" join PAS.assignmentProcess AP 			");
		stringBuffer.append(" join PAS.participant PA 					");
		stringBuffer.append(" WHERE AP.settlementDate = :processDate 	");		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("processDate", processDate);		
		return query.getResultList();
	}
	
	public List<HolderAccount> checkHolderAccountInParticipant(Participant originParticipant, Long idOriginParticipantPk, Boolean checkOriginParticipant, Boolean checkInChargeParticipant) throws ServiceException, AccountAssignmentException {
		if(Validations.validateIsNull(originParticipant)) {
			originParticipant = find(Participant.class, idOriginParticipantPk);
		}
		
		List<HolderAccount> res = new ArrayList<HolderAccount>();
		
		if(Validations.validateIsNull(originParticipant.getIndDepositary()) ||  originParticipant.getIndDepositary().equals(ComponentConstant.ZERO)){
			if(checkOriginParticipant) {
				
				List<HolderAccount> lstOriginHolderAccount = this.holderAccountServiceBean.getAccountsHolderAndParticipant(originParticipant.getHolder().getIdHolderPk(), idOriginParticipantPk);
				
				if(Validations.validateIsNullOrEmpty(lstOriginHolderAccount) || lstOriginHolderAccount.size() < 1) {
					throw new AccountAssignmentException(PropertiesConstants.ASSIGNMENT_ACCOUNT_IN_CHARGE_ORIGIN_HOLDER_ACCOUNT_NO_EXISTS);
				}
				
				res.addAll(lstOriginHolderAccount);
			}
			
			if(checkInChargeParticipant) {
				/**
				 * Ubicamos la cuenta titular del depositante, esta cuenta debe estar creada dentro del participante encargado.
				 * Por ejemplo, el depositante BANVAR debe tener una cuenta titular bajo el depositante encarga BOLSA DE VALORES DE ASUNCION
				 */						
				
				List<HolderAccount> lstOriginHolderAccount = this.holderAccountServiceBean.getAccountsHolderAndParticipant(originParticipant.getHolder().getIdHolderPk(), idepositarySetup.getIdInChargeParticipant());
				
				if(Validations.validateIsNullOrEmpty(lstOriginHolderAccount) || lstOriginHolderAccount.size() < 1) {
					throw new AccountAssignmentException(PropertiesConstants.ASSIGNMENT_ACCOUNT_IN_CHARGE_PARTICIPANT_HOLDER_ACCOUNT_NO_EXISTS);
				}
				
				res.addAll(lstOriginHolderAccount);
			}
		}
		
		return res;
	}
	
	public HolderAccount getHolderAccountComponentServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		return this.holderAccountServiceBean.getHolderAccountComponentServiceBean(holderAccountTO);
	}
	
	public Holder getHolderServiceBean(Long idHolderPk) {
		return this.holderAccountServiceBean.getHolderServiceBean(idHolderPk);
	}
}
