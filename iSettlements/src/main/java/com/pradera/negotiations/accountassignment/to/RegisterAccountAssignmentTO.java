package com.pradera.negotiations.accountassignment.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterAccountAssignmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class RegisterAccountAssignmentTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mechanism operation. */
	private MechanismOperation mechanismOperation;
	
	/** The lst operation state. */
	private List<ParameterTable> lstRoles, lstOperationType, lstSettlementSchema, lstOperationState;
	
	/** The operation type selected. */
	private Integer operationTypeSelected;
	
	/** The lst incharge participants. */
	private List<Participant> lstInchargeParticipants;
	
	/** The participant origin. */
	private Long participantOrigin;
	
	/** The lst holder account operation. */
	private GenericDataModel<HolderAccountOperation> lstHolderAccountOperation;
	
	/** The lst holder account operation removed. */
	private List<HolderAccountOperation> lstHolderAccountOperationRemoved;
	
	/** The holder account operation selected. */
	private HolderAccountOperation[] holderAccountOperationSelected;
	
	/** The holder account operation. */
	private HolderAccountOperation holderAccountOperation;
	
	/** The cross amount assig. */
	private BigDecimal quantityAssig, amountAssig, crossQuantityAssig, crossAmountAssig;
	
	/** The bl incharge agreement disabled. */
	private boolean blInchargeDisabled, blCrossOperation , blInchargeAgreementDisabled;
	
	/** The holder. */
	private Holder holder;
	
	/** The lst holder account. */
	private List<HolderAccount> lstHolderAccount;
	
	/** The hol acc ope market fact. */
	private AccountOperationMarketFact holAccOpeMarketFact;
	
	/** The bl fixed income. */
	private boolean blMarketFact, blFixedIncome;
	
	/** The bl operation repo. */
	private boolean blOperationRepo;
	
	/** The bl primary placement. */
	private boolean blPrimaryPlacement;
	
	/** The lst holder account operation repo. */
	private GenericDataModel<HolderAccountOperation> lstHolderAccountOperationRepo;
	
	/** The holder account operation repo. */
	private HolderAccountOperation holderAccountOperationRepo;
	
	/** The selected holder account operation. */
	private HolderAccountOperation selectedHolderAccountOperation;
	
	private Boolean isDepositaryClient;
	
	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}
	
	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}
	
	/**
	 * Gets the lst roles.
	 *
	 * @return the lst roles
	 */
	public List<ParameterTable> getLstRoles() {
		return lstRoles;
	}
	
	/**
	 * Sets the lst roles.
	 *
	 * @param lstRoles the new lst roles
	 */
	public void setLstRoles(List<ParameterTable> lstRoles) {
		this.lstRoles = lstRoles;
	}
	
	/**
	 * Gets the lst operation type.
	 *
	 * @return the lst operation type
	 */
	public List<ParameterTable> getLstOperationType() {
		return lstOperationType;
	}
	
	/**
	 * Sets the lst operation type.
	 *
	 * @param lstOperationType the new lst operation type
	 */
	public void setLstOperationType(List<ParameterTable> lstOperationType) {
		this.lstOperationType = lstOperationType;
	}
	
	/**
	 * Checks if is bl cross operation.
	 *
	 * @return true, if is bl cross operation
	 */
	public boolean isBlCrossOperation() {
		return blCrossOperation;
	}
	
	/**
	 * Sets the bl cross operation.
	 *
	 * @param blCrossOperation the new bl cross operation
	 */
	public void setBlCrossOperation(boolean blCrossOperation) {
		this.blCrossOperation = blCrossOperation;
	}
	
	/**
	 * Gets the operation type selected.
	 *
	 * @return the operation type selected
	 */
	public Integer getOperationTypeSelected() {
		return operationTypeSelected;
	}
	
	/**
	 * Sets the operation type selected.
	 *
	 * @param operationTypeSelected the new operation type selected
	 */
	public void setOperationTypeSelected(Integer operationTypeSelected) {
		this.operationTypeSelected = operationTypeSelected;
	}
	
	/**
	 * Gets the lst incharge participants.
	 *
	 * @return the lst incharge participants
	 */
	public List<Participant> getLstInchargeParticipants() {
		return lstInchargeParticipants;
	}
	
	/**
	 * Sets the lst incharge participants.
	 *
	 * @param lstInchargeParticipants the new lst incharge participants
	 */
	public void setLstInchargeParticipants(List<Participant> lstInchargeParticipants) {
		this.lstInchargeParticipants = lstInchargeParticipants;
	}
	
	/**
	 * Gets the participant origin.
	 *
	 * @return the participant origin
	 */
	public Long getParticipantOrigin() {
		return participantOrigin;
	}
	
	/**
	 * Sets the participant origin.
	 *
	 * @param participantOrigin the new participant origin
	 */
	public void setParticipantOrigin(Long participantOrigin) {
		this.participantOrigin = participantOrigin;
	}
	
	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}
	
	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}
	
	/**
	 * Gets the quantity assig.
	 *
	 * @return the quantity assig
	 */
	public BigDecimal getQuantityAssig() {
		return quantityAssig;
	}
	
	/**
	 * Sets the quantity assig.
	 *
	 * @param quantityAssig the new quantity assig
	 */
	public void setQuantityAssig(BigDecimal quantityAssig) {
		this.quantityAssig = quantityAssig;
	}
	
	/**
	 * Gets the amount assig.
	 *
	 * @return the amount assig
	 */
	public BigDecimal getAmountAssig() {
		return amountAssig;
	}
	
	/**
	 * Sets the amount assig.
	 *
	 * @param amountAssig the new amount assig
	 */
	public void setAmountAssig(BigDecimal amountAssig) {
		this.amountAssig = amountAssig;
	}
	
	/**
	 * Gets the cross quantity assig.
	 *
	 * @return the cross quantity assig
	 */
	public BigDecimal getCrossQuantityAssig() {
		return crossQuantityAssig;
	}
	
	/**
	 * Sets the cross quantity assig.
	 *
	 * @param crossQuantityAssig the new cross quantity assig
	 */
	public void setCrossQuantityAssig(BigDecimal crossQuantityAssig) {
		this.crossQuantityAssig = crossQuantityAssig;
	}
	
	/**
	 * Gets the cross amount assig.
	 *
	 * @return the cross amount assig
	 */
	public BigDecimal getCrossAmountAssig() {
		return crossAmountAssig;
	}
	
	/**
	 * Sets the cross amount assig.
	 *
	 * @param crossAmountAssig the new cross amount assig
	 */
	public void setCrossAmountAssig(BigDecimal crossAmountAssig) {
		this.crossAmountAssig = crossAmountAssig;
	}
	
	/**
	 * Gets the lst holder account operation.
	 *
	 * @return the lst holder account operation
	 */
	public GenericDataModel<HolderAccountOperation> getLstHolderAccountOperation() {
		return lstHolderAccountOperation;
	}
	
	/**
	 * Sets the lst holder account operation.
	 *
	 * @param lstHolderAccountOperation the new lst holder account operation
	 */
	public void setLstHolderAccountOperation(
			GenericDataModel<HolderAccountOperation> lstHolderAccountOperation) {
		this.lstHolderAccountOperation = lstHolderAccountOperation;
	}
	
	/**
	 * Gets the lst settlement schema.
	 *
	 * @return the lst settlement schema
	 */
	public List<ParameterTable> getLstSettlementSchema() {
		return lstSettlementSchema;
	}
	
	/**
	 * Sets the lst settlement schema.
	 *
	 * @param lstSettlementSchema the new lst settlement schema
	 */
	public void setLstSettlementSchema(List<ParameterTable> lstSettlementSchema) {
		this.lstSettlementSchema = lstSettlementSchema;
	}
	
	/**
	 * Gets the lst operation state.
	 *
	 * @return the lst operation state
	 */
	public List<ParameterTable> getLstOperationState() {
		return lstOperationState;
	}
	
	/**
	 * Sets the lst operation state.
	 *
	 * @param lstOperationState the new lst operation state
	 */
	public void setLstOperationState(List<ParameterTable> lstOperationState) {
		this.lstOperationState = lstOperationState;
	}
	
	/**
	 * Checks if is bl incharge disabled.
	 *
	 * @return true, if is bl incharge disabled
	 */
	public boolean isBlInchargeDisabled() {
		return blInchargeDisabled;
	}
	
	/**
	 * Sets the bl incharge disabled.
	 *
	 * @param blInchargeDisabled the new bl incharge disabled
	 */
	public void setBlInchargeDisabled(boolean blInchargeDisabled) {
		this.blInchargeDisabled = blInchargeDisabled;
	}
	
	/**
	 * Gets the holder account operation selected.
	 *
	 * @return the holder account operation selected
	 */
	public HolderAccountOperation[] getHolderAccountOperationSelected() {
		return holderAccountOperationSelected;
	}
	
	/**
	 * Sets the holder account operation selected.
	 *
	 * @param holderAccountOperationSelected the new holder account operation selected
	 */
	public void setHolderAccountOperationSelected(
			HolderAccountOperation[] holderAccountOperationSelected) {
		this.holderAccountOperationSelected = holderAccountOperationSelected;
	}
	
	/**
	 * Gets the lst holder account operation removed.
	 *
	 * @return the lst holder account operation removed
	 */
	public List<HolderAccountOperation> getLstHolderAccountOperationRemoved() {
		return lstHolderAccountOperationRemoved;
	}
	
	/**
	 * Sets the lst holder account operation removed.
	 *
	 * @param lstHolderAccountOperationRemoved the new lst holder account operation removed
	 */
	public void setLstHolderAccountOperationRemoved(
			List<HolderAccountOperation> lstHolderAccountOperationRemoved) {
		this.lstHolderAccountOperationRemoved = lstHolderAccountOperationRemoved;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}
	
	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	
	/**
	 * Checks if is bl market fact.
	 *
	 * @return true, if is bl market fact
	 */
	public boolean isBlMarketFact() {
		return blMarketFact;
	}
	
	/**
	 * Sets the bl market fact.
	 *
	 * @param blMarketFact the new bl market fact
	 */
	public void setBlMarketFact(boolean blMarketFact) {
		this.blMarketFact = blMarketFact;
	}
	
	/**
	 * Checks if is bl fixed income.
	 *
	 * @return true, if is bl fixed income
	 */
	public boolean isBlFixedIncome() {
		return blFixedIncome;
	}
	
	/**
	 * Sets the bl fixed income.
	 *
	 * @param blFixedIncome the new bl fixed income
	 */
	public void setBlFixedIncome(boolean blFixedIncome) {
		this.blFixedIncome = blFixedIncome;
	}
	
	/**
	 * Gets the lst holder account operation repo.
	 *
	 * @return the lst holder account operation repo
	 */
	public GenericDataModel<HolderAccountOperation> getLstHolderAccountOperationRepo() {
		return lstHolderAccountOperationRepo;
	}
	
	/**
	 * Sets the lst holder account operation repo.
	 *
	 * @param lstHolderAccountOperationRepo the new lst holder account operation repo
	 */
	public void setLstHolderAccountOperationRepo(
			GenericDataModel<HolderAccountOperation> lstHolderAccountOperationRepo) {
		this.lstHolderAccountOperationRepo = lstHolderAccountOperationRepo;
	}
	
	/**
	 * Gets the holder account operation repo.
	 *
	 * @return the holder account operation repo
	 */
	public HolderAccountOperation getHolderAccountOperationRepo() {
		return holderAccountOperationRepo;
	}
	
	/**
	 * Sets the holder account operation repo.
	 *
	 * @param holderAccountOperationRepo the new holder account operation repo
	 */
	public void setHolderAccountOperationRepo(
			HolderAccountOperation holderAccountOperationRepo) {
		this.holderAccountOperationRepo = holderAccountOperationRepo;
	}
	
	/**
	 * Checks if is bl operation repo.
	 *
	 * @return true, if is bl operation repo
	 */
	public boolean isBlOperationRepo() {
		return blOperationRepo;
	}
	
	/**
	 * Sets the bl operation repo.
	 *
	 * @param blOperationRepo the new bl operation repo
	 */
	public void setBlOperationRepo(boolean blOperationRepo) {
		this.blOperationRepo = blOperationRepo;
	}
	
	/**
	 * Gets the hol acc ope market fact.
	 *
	 * @return the hol acc ope market fact
	 */
	public AccountOperationMarketFact getHolAccOpeMarketFact() {
		return holAccOpeMarketFact;
	}
	
	/**
	 * Sets the hol acc ope market fact.
	 *
	 * @param holAccOpeMarketFact the new hol acc ope market fact
	 */
	public void setHolAccOpeMarketFact(
			AccountOperationMarketFact holAccOpeMarketFact) {
		this.holAccOpeMarketFact = holAccOpeMarketFact;
	}
	
	/**
	 * Checks if is bl primary placement.
	 *
	 * @return true, if is bl primary placement
	 */
	public boolean isBlPrimaryPlacement() {
		return blPrimaryPlacement;
	}
	
	/**
	 * Sets the bl primary placement.
	 *
	 * @param blPrimaryPlacement the new bl primary placement
	 */
	public void setBlPrimaryPlacement(boolean blPrimaryPlacement) {
		this.blPrimaryPlacement = blPrimaryPlacement;
	}
	
	/**
	 * Gets the selected holder account operation.
	 *
	 * @return the selected holder account operation
	 */
	public HolderAccountOperation getSelectedHolderAccountOperation() {
		return selectedHolderAccountOperation;
	}
	
	/**
	 * Sets the selected holder account operation.
	 *
	 * @param selectedHolderAccountOperation the new selected holder account operation
	 */
	public void setSelectedHolderAccountOperation(
			HolderAccountOperation selectedHolderAccountOperation) {
		this.selectedHolderAccountOperation = selectedHolderAccountOperation;
	}
	
	/**
	 * Checks if is bl incharge agreement disabled.
	 *
	 * @return true, if is bl incharge agreement disabled
	 */
	public boolean isBlInchargeAgreementDisabled() {
		return blInchargeAgreementDisabled;
	}
	
	/**
	 * Sets the bl incharge agreement disabled.
	 *
	 * @param blInchargeAgreementDisabled the new bl incharge agreement disabled
	 */
	public void setBlInchargeAgreementDisabled(boolean blInchargeAgreementDisabled) {
		this.blInchargeAgreementDisabled = blInchargeAgreementDisabled;
	}

	public Boolean getIsDepositaryClient() {
		return isDepositaryClient;
	}

	public void setIsDepositaryClient(Boolean isDepositaryClient) {
		this.isDepositaryClient = isDepositaryClient;
	}
	
}
