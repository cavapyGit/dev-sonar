package com.pradera.negotiations.accountassignment.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchAccountAssignmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class SearchAccountAssignmentTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism> lstNegotiationMechanism;	
	
	/** The lst negotiation modality. */
	private List<NegotiationModality> lstNegotiationModality;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst date type. */
	private List<ParameterTable> lstDateType;
	
	/** The participant selected. */
	private Long negoMechanismSelected, negoModalitySelected, participantSelected;
	
	/** The date type. */
	private Integer dateType;
	
	/** The date. */
	private Date date;
	
	/** The lst assignment process. */
	private GenericDataModel<AssignmentProcessTO> lstAssignmentProcess;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The assignment processs selected. */
	private AssignmentProcessTO[] assignmentProcesssSelected;
	
	/** The lst participant assignment. */
	private GenericDataModel<ParticipantAssignmentTO> lstParticipantAssignment;
	
	/** The lst mechanism operation. */
	private GenericDataModel<MechanismOperationTO> lstMechanismOperation;
	
	private GenericDataModel<OperationWithNoSecuritiesTO> lstOperationWithNoSecurities;
	
	/** The participant header. */
	private Long negoModalityHeader, assignmentProcessHeader, participantAssignmentHeader ,participantHeader;
	
	/** The modality name header. */
	private String modalityNameHeader;
	
	/** The settlement schema type. */
	private Integer assignmentState, assignmentProcessState, participantAssignmentState, settlementSchemaType;
	
	/** The participant assignment state desc. */
	private String assignmentStateDesc, assignmentPorcessStateDesc, participantAssignmentStateDesc;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The settlement schema name. */
	private String settlementSchemaName;
	
	/** The lst assignment process state. */
	private List<ParameterTable> lstAssignmentProcessState;
	
	/** The lst participant assignment state. */
	private List<ParameterTable> lstParticipantAssignmentState;
	
	/** The lst settlement schema type. */
	private List<ParameterTable> lstSettlementSchemaType;
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the lst negotiation modality.
	 *
	 * @return the lst negotiation modality
	 */
	public List<NegotiationModality> getLstNegotiationModality() {
		return lstNegotiationModality;
	}
	
	/**
	 * Sets the lst negotiation modality.
	 *
	 * @param lstNegotiationModality the new lst negotiation modality
	 */
	public void setLstNegotiationModality(
			List<NegotiationModality> lstNegotiationModality) {
		this.lstNegotiationModality = lstNegotiationModality;
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the lst date type.
	 *
	 * @return the lst date type
	 */
	public List<ParameterTable> getLstDateType() {
		return lstDateType;
	}
	
	/**
	 * Sets the lst date type.
	 *
	 * @param lstDateType the new lst date type
	 */
	public void setLstDateType(List<ParameterTable> lstDateType) {
		this.lstDateType = lstDateType;
	}
	
	/**
	 * Gets the nego mechanism selected.
	 *
	 * @return the nego mechanism selected
	 */
	public Long getNegoMechanismSelected() {
		return negoMechanismSelected;
	}
	
	/**
	 * Sets the nego mechanism selected.
	 *
	 * @param negoMechanismSelected the new nego mechanism selected
	 */
	public void setNegoMechanismSelected(Long negoMechanismSelected) {
		this.negoMechanismSelected = negoMechanismSelected;
	}
	
	/**
	 * Gets the nego modality selected.
	 *
	 * @return the nego modality selected
	 */
	public Long getNegoModalitySelected() {
		return negoModalitySelected;
	}
	
	/**
	 * Sets the nego modality selected.
	 *
	 * @param negoModalitySelected the new nego modality selected
	 */
	public void setNegoModalitySelected(Long negoModalitySelected) {
		this.negoModalitySelected = negoModalitySelected;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the date type.
	 *
	 * @return the date type
	 */
	public Integer getDateType() {
		return dateType;
	}
	
	/**
	 * Sets the date type.
	 *
	 * @param dateType the new date type
	 */
	public void setDateType(Integer dateType) {
		this.dateType = dateType;
	}
	
	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * Gets the lst assignment process.
	 *
	 * @return the lst assignment process
	 */
	public GenericDataModel<AssignmentProcessTO> getLstAssignmentProcess() {
		return lstAssignmentProcess;
	}
	
	/**
	 * Sets the lst assignment process.
	 *
	 * @param lstAssignmentProcess the new lst assignment process
	 */
	public void setLstAssignmentProcess(
			GenericDataModel<AssignmentProcessTO> lstAssignmentProcess) {
		this.lstAssignmentProcess = lstAssignmentProcess;
	}
	
	/**
	 * Gets the lst participant assignment.
	 *
	 * @return the lst participant assignment
	 */
	public GenericDataModel<ParticipantAssignmentTO> getLstParticipantAssignment() {
		return lstParticipantAssignment;
	}
	
	/**
	 * Sets the lst participant assignment.
	 *
	 * @param lstParticipantAssignment the new lst participant assignment
	 */
	public void setLstParticipantAssignment(
			GenericDataModel<ParticipantAssignmentTO> lstParticipantAssignment) {
		this.lstParticipantAssignment = lstParticipantAssignment;
	}
	
	
	
	public GenericDataModel<OperationWithNoSecuritiesTO> getLstOperationWithNoSecurities() {
		return lstOperationWithNoSecurities;
	}

	public void setLstOperationWithNoSecurities(GenericDataModel<OperationWithNoSecuritiesTO> lstOperationWithNoSecurities) {
		this.lstOperationWithNoSecurities = lstOperationWithNoSecurities;
	}

	/**
	 * Gets the lst mechanism operation.
	 *
	 * @return the lst mechanism operation
	 */
	public GenericDataModel<MechanismOperationTO> getLstMechanismOperation() {
		return lstMechanismOperation;
	}
	
	/**
	 * Sets the lst mechanism operation.
	 *
	 * @param lstMechanismOperation the new lst mechanism operation
	 */
	public void setLstMechanismOperation(
			GenericDataModel<MechanismOperationTO> lstMechanismOperation) {
		this.lstMechanismOperation = lstMechanismOperation;
	}
	
	/**
	 * Gets the nego modality header.
	 *
	 * @return the nego modality header
	 */
	public Long getNegoModalityHeader() {
		return negoModalityHeader;
	}
	
	/**
	 * Sets the nego modality header.
	 *
	 * @param negoModalityHeader the new nego modality header
	 */
	public void setNegoModalityHeader(Long negoModalityHeader) {
		this.negoModalityHeader = negoModalityHeader;
	}
	
	/**
	 * Gets the assignment process header.
	 *
	 * @return the assignment process header
	 */
	public Long getAssignmentProcessHeader() {
		return assignmentProcessHeader;
	}
	
	/**
	 * Sets the assignment process header.
	 *
	 * @param assignmentProcessHeader the new assignment process header
	 */
	public void setAssignmentProcessHeader(Long assignmentProcessHeader) {
		this.assignmentProcessHeader = assignmentProcessHeader;
	}
	
	/**
	 * Gets the participant header.
	 *
	 * @return the participant header
	 */
	public Long getParticipantHeader() {
		return participantHeader;
	}
	
	/**
	 * Sets the participant header.
	 *
	 * @param participantHeader the new participant header
	 */
	public void setParticipantHeader(Long participantHeader) {
		this.participantHeader = participantHeader;
	}
	
	/**
	 * Gets the assignment state.
	 *
	 * @return the assignment state
	 */
	public Integer getAssignmentState() {
		return assignmentState;
	}
	
	/**
	 * Sets the assignment state.
	 *
	 * @param assignmentState the new assignment state
	 */
	public void setAssignmentState(Integer assignmentState) {
		this.assignmentState = assignmentState;
	}
	
	/**
	 * Gets the assignment process state.
	 *
	 * @return the assignment process state
	 */
	public Integer getAssignmentProcessState() {
		return assignmentProcessState;
	}
	
	/**
	 * Sets the assignment process state.
	 *
	 * @param assignmentProcessState the new assignment process state
	 */
	public void setAssignmentProcessState(Integer assignmentProcessState) {
		this.assignmentProcessState = assignmentProcessState;
	}
	
	/**
	 * Gets the participant assignment state.
	 *
	 * @return the participant assignment state
	 */
	public Integer getParticipantAssignmentState() {
		return participantAssignmentState;
	}
	
	/**
	 * Sets the participant assignment state.
	 *
	 * @param participantAssignmentState the new participant assignment state
	 */
	public void setParticipantAssignmentState(Integer participantAssignmentState) {
		this.participantAssignmentState = participantAssignmentState;
	}
	
	/**
	 * Gets the assignment state desc.
	 *
	 * @return the assignment state desc
	 */
	public String getAssignmentStateDesc() {
		return assignmentStateDesc;
	}
	
	/**
	 * Sets the assignment state desc.
	 *
	 * @param assignmentStateDesc the new assignment state desc
	 */
	public void setAssignmentStateDesc(String assignmentStateDesc) {
		this.assignmentStateDesc = assignmentStateDesc;
	}
	
	/**
	 * Gets the assignment porcess state desc.
	 *
	 * @return the assignment porcess state desc
	 */
	public String getAssignmentPorcessStateDesc() {
		return assignmentPorcessStateDesc;
	}
	
	/**
	 * Sets the assignment porcess state desc.
	 *
	 * @param assignmentPorcessStateDesc the new assignment porcess state desc
	 */
	public void setAssignmentPorcessStateDesc(String assignmentPorcessStateDesc) {
		this.assignmentPorcessStateDesc = assignmentPorcessStateDesc;
	}
	
	/**
	 * Gets the participant assignment state desc.
	 *
	 * @return the participant assignment state desc
	 */
	public String getParticipantAssignmentStateDesc() {
		return participantAssignmentStateDesc;
	}
	
	/**
	 * Sets the participant assignment state desc.
	 *
	 * @param participantAssignmentStateDesc the new participant assignment state desc
	 */
	public void setParticipantAssignmentStateDesc(
			String participantAssignmentStateDesc) {
		this.participantAssignmentStateDesc = participantAssignmentStateDesc;
	}
	
	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	
	/**
	 * Gets the assignment processs selected.
	 *
	 * @return the assignment processs selected
	 */
	public AssignmentProcessTO[] getAssignmentProcesssSelected() {
		return assignmentProcesssSelected;
	}
	
	/**
	 * Sets the assignment processs selected.
	 *
	 * @param assignmentProcesssSelected the new assignment processs selected
	 */
	public void setAssignmentProcesssSelected(
			AssignmentProcessTO[] assignmentProcesssSelected) {
		this.assignmentProcesssSelected = assignmentProcesssSelected;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the participant assignment header.
	 *
	 * @return the participant assignment header
	 */
	public Long getParticipantAssignmentHeader() {
		return participantAssignmentHeader;
	}
	
	/**
	 * Sets the participant assignment header.
	 *
	 * @param participantAssignmentHeader the new participant assignment header
	 */
	public void setParticipantAssignmentHeader(Long participantAssignmentHeader) {
		this.participantAssignmentHeader = participantAssignmentHeader;
	}
	
	/**
	 * Gets the lst participant assignment state.
	 *
	 * @return the lst participant assignment state
	 */
	public List<ParameterTable> getLstParticipantAssignmentState() {
		return lstParticipantAssignmentState;
	}
	
	/**
	 * Sets the lst participant assignment state.
	 *
	 * @param lstParticipantAssignmentState the new lst participant assignment state
	 */
	public void setLstParticipantAssignmentState(
			List<ParameterTable> lstParticipantAssignmentState) {
		this.lstParticipantAssignmentState = lstParticipantAssignmentState;
	}
	
	/**
	 * Gets the lst assignment process state.
	 *
	 * @return the lst assignment process state
	 */
	public List<ParameterTable> getLstAssignmentProcessState() {
		return lstAssignmentProcessState;
	}
	
	/**
	 * Sets the lst assignment process state.
	 *
	 * @param lstAssignmentProcessState the new lst assignment process state
	 */
	public void setLstAssignmentProcessState(
			List<ParameterTable> lstAssignmentProcessState) {
		this.lstAssignmentProcessState = lstAssignmentProcessState;
	}
	
	/**
	 * Gets the lst settlement schema type.
	 *
	 * @return the lst settlement schema type
	 */
	public List<ParameterTable> getLstSettlementSchemaType() {
		return lstSettlementSchemaType;
	}
	
	/**
	 * Sets the lst settlement schema type.
	 *
	 * @param lstSettlementSchemaType the new lst settlement schema type
	 */
	public void setLstSettlementSchemaType(
			List<ParameterTable> lstSettlementSchemaType) {
		this.lstSettlementSchemaType = lstSettlementSchemaType;
	}
	
	/**
	 * Gets the settlement schema type.
	 *
	 * @return the settlement schema type
	 */
	public Integer getSettlementSchemaType() {
		return settlementSchemaType;
	}
	
	/**
	 * Sets the settlement schema type.
	 *
	 * @param settlementSchemaType the new settlement schema type
	 */
	public void setSettlementSchemaType(Integer settlementSchemaType) {
		this.settlementSchemaType = settlementSchemaType;
	}
	
	/**
	 * Gets the settlement schema name.
	 *
	 * @return the settlement schema name
	 */
	public String getSettlementSchemaName() {
		return settlementSchemaName;
	}
	
	/**
	 * Sets the settlement schema name.
	 *
	 * @param settlementSchemaName the new settlement schema name
	 */
	public void setSettlementSchemaName(String settlementSchemaName) {
		this.settlementSchemaName = settlementSchemaName;
	}
	
	/**
	 * Gets the modality name header.
	 *
	 * @return the modality name header
	 */
	public String getModalityNameHeader() {
		return modalityNameHeader;
	}
	
	/**
	 * Sets the modality name header.
	 *
	 * @param modalityNameHeader the new modality name header
	 */
	public void setModalityNameHeader(String modalityNameHeader) {
		this.modalityNameHeader = modalityNameHeader;
	}
	
}
