package com.pradera.negotiations.accountassignment.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.constants.Constants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MechanismOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
public class MechanismOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id mechanism operation pk. */
	private Long idMechanismOperationPk;
	
	/** The id mechanism. */
	private Long idMechanism;
	
	/** The id modality. */
	private Long idModality;
	
	/** The modality name. */
	private String modalityName;
	
	/** The mechanism name. */
	private String mechanismName;
	
	/** The id process file. */
	private Long idProcessFile;
	
	/** The role. */
	private Integer role;
	
	/** The role description. */
	private String roleDescription;
	
	/** The id sale participant. */
	private Long idSaleParticipant;
	
	/** The sale participant description. */
	private String saleParticipantDescription;
	
	/** The sale participant nemonic. */
	private String saleParticipantNemonic;
	
	/** The seller participant code nemonic. */
	private String sellerParticipantCodeNemonic;
	
	/** The id purchase participant. */
	private Long idPurchaseParticipant;
	
	/** The purchase participant description. */
	private String purchaseParticipantDescription;
	
	/** The purchase participant nemonic. */
	private String purchaseParticipantNemonic;
	
	/** The buyer participant code nemonic. */
	private String buyerParticipantCodeNemonic;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The ballot number. */
	private Long ballotNumber;
	
	/** The sequential. */
	private Long sequential;
	
	/** The ballot sequential. */
	private String ballotSequential;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/** The cash settlement date. */
	private Date cashSettlementDate;
	
	/** The cash settlement amount. */
	private BigDecimal cashSettlementAmount;
	
	/** The cash settlement price. */
	private BigDecimal cashSettlementPrice;
	
	/** The term settlement date. */
	private Date termSettlementDate;
	
	private Long termSettlementDays;	
	
	private String returnBalance;
	
	public String getReturnBalance() {
		return returnBalance;
	}
	public void setReturnBalance(String returnBalance) {
		this.returnBalance = returnBalance;
	}
	public Long getTermSettlementDays() {
		return termSettlementDays;
	}
	public void setTermSettlementDays(Long termSettlementDays) {
		this.termSettlementDays = termSettlementDays;
	}
	
	private BigDecimal amountRate;
	public BigDecimal getAmountRate() {
		return amountRate;
	}
	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}

	/** The term settlement amount. */
	private BigDecimal termSettlementAmount;
	
	/** The term settlement price. */
	private BigDecimal termSettlementPrice;
	
	/** The settlement date. */
	private Date settlementDate;
	
	/** The settlement amount. */
	private BigDecimal settlementAmount;
	
	/** The settlement currency. */
	private Integer settlementCurrency;
	
	/** The operation state. */
	private Integer operationState;
	
	/** The state description. */
	private String stateDescription;
	
	/** The currency description. */
	private String currencyDescription;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The remove settlement motive. */
	private String removeSettlementMotive;
	
	/** The id remove settlement motive. */
	private Integer idRemoveSettlementMotive;
	
	/** The id settlement operation. */
	private Long idSettlementOperation;
	
	/** The ind term settlement. */
	private Integer indTermSettlement;
	
	/** The forced purchase. */
	private Boolean forcedPurchase;
	
	/** indica si es o no una anticipacion parcial */
	private Boolean isIndPrepaid;
	
	/** Cantidad restante por liquidar */
	private BigDecimal stockQuantityRemmaing;
	
	private Boolean indBuyerSeller;
	
	/** The id mechanism operation pk. */
	private Long idOperationUnfulfillment;
	
	/**
	 * Gets the seller participant code nemonic.
	 *
	 * @return the seller participant code nemonic
	 */
	public String getSellerParticipantCodeNemonic(){
    	if(Validations.validateIsNullOrEmpty(sellerParticipantCodeNemonic)){
    		if(Validations.validateIsNotNullAndPositive(idSaleParticipant) && Validations.validateIsNotNull(saleParticipantNemonic)){
    			StringBuffer sb = new StringBuffer();
        		sb.append(saleParticipantNemonic);
        		sb.append(Constants.DASH);
        		sb.append(idSaleParticipant);    		
        		sellerParticipantCodeNemonic = sb.toString();
        	}
    	}
    	return sellerParticipantCodeNemonic;
    }
	
	/**
	 * Gets the buyer participant code nemonic.
	 *
	 * @return the buyer participant code nemonic
	 */
	public String getBuyerParticipantCodeNemonic(){
    	if(Validations.validateIsNullOrEmpty(buyerParticipantCodeNemonic)){
    		if(Validations.validateIsNotNullAndPositive(idPurchaseParticipant) && Validations.validateIsNotNullAndNotEmpty(purchaseParticipantNemonic)){
    			StringBuffer sb = new StringBuffer();
        		sb.append(purchaseParticipantNemonic);
        		sb.append(Constants.DASH);
        		sb.append(idPurchaseParticipant);    		
        		buyerParticipantCodeNemonic = sb.toString();
        	}
    	}
    	return buyerParticipantCodeNemonic;
    }
	
	/**
	 * Gets the ballot sequential.
	 *
	 * @return the ballot sequential
	 */
	public String getBallotSequential(){
		if(ballotSequential == null){
			StringBuilder sbOpNumber = new StringBuilder();
			if(ballotNumber!=null && sequential!=null){
				sbOpNumber.append(this.ballotNumber).append(GeneralConstants.SLASH).append(this.sequential);
			}
			ballotSequential = sbOpNumber.toString();
		}
		return ballotSequential;
	}
	
	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the id mechanism operation pk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	
	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the new id mechanism operation pk
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}
	
	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}
	
	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}
	
	/**
	 * Gets the role description.
	 *
	 * @return the role description
	 */
	public String getRoleDescription() {
		return roleDescription;
	}
	
	/**
	 * Sets the role description.
	 *
	 * @param roleDescription the new role description
	 */
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
	
	/**
	 * Gets the id sale participant.
	 *
	 * @return the id sale participant
	 */
	public Long getIdSaleParticipant() {
		return idSaleParticipant;
	}

	/**
	 * Sets the id sale participant.
	 *
	 * @param idSaleParticipant the new id sale participant
	 */
	public void setIdSaleParticipant(Long idSaleParticipant) {
		this.idSaleParticipant = idSaleParticipant;
	}

	/**
	 * Gets the sale participant description.
	 *
	 * @return the sale participant description
	 */
	public String getSaleParticipantDescription() {
		return saleParticipantDescription;
	}

	/**
	 * Sets the sale participant description.
	 *
	 * @param saleParticipantDescription the new sale participant description
	 */
	public void setSaleParticipantDescription(String saleParticipantDescription) {
		this.saleParticipantDescription = saleParticipantDescription;
	}

	/**
	 * Gets the sale participant nemonic.
	 *
	 * @return the sale participant nemonic
	 */
	public String getSaleParticipantNemonic() {
		return saleParticipantNemonic;
	}

	/**
	 * Sets the sale participant nemonic.
	 *
	 * @param saleParticipantNemonic the new sale participant nemonic
	 */
	public void setSaleParticipantNemonic(String saleParticipantNemonic) {
		this.saleParticipantNemonic = saleParticipantNemonic;
	}

	/**
	 * Gets the id purchase participant.
	 *
	 * @return the id purchase participant
	 */
	public Long getIdPurchaseParticipant() {
		return idPurchaseParticipant;
	}

	/**
	 * Sets the id purchase participant.
	 *
	 * @param idPurchaseParticipant the new id purchase participant
	 */
	public void setIdPurchaseParticipant(Long idPurchaseParticipant) {
		this.idPurchaseParticipant = idPurchaseParticipant;
	}

	/**
	 * Gets the purchase participant description.
	 *
	 * @return the purchase participant description
	 */
	public String getPurchaseParticipantDescription() {
		return purchaseParticipantDescription;
	}

	/**
	 * Sets the purchase participant description.
	 *
	 * @param purchaseParticipantDescription the new purchase participant description
	 */
	public void setPurchaseParticipantDescription(
			String purchaseParticipantDescription) {
		this.purchaseParticipantDescription = purchaseParticipantDescription;
	}

	/**
	 * Gets the purchase participant nemonic.
	 *
	 * @return the purchase participant nemonic
	 */
	public String getPurchaseParticipantNemonic() {
		return purchaseParticipantNemonic;
	}

	/**
	 * Sets the purchase participant nemonic.
	 *
	 * @param purchaseParticipantNemonic the new purchase participant nemonic
	 */
	public void setPurchaseParticipantNemonic(String purchaseParticipantNemonic) {
		this.purchaseParticipantNemonic = purchaseParticipantNemonic;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the cash settlement date.
	 *
	 * @return the cash settlement date
	 */
	public Date getCashSettlementDate() {
		return cashSettlementDate;
	}
	
	/**
	 * Sets the cash settlement date.
	 *
	 * @param cashSettlementDate the new cash settlement date
	 */
	public void setCashSettlementDate(Date cashSettlementDate) {
		this.cashSettlementDate = cashSettlementDate;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	
	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	
	/**
	 * Gets the ballot number.
	 *
	 * @return the ballot number
	 */
	public Long getBallotNumber() {
		return ballotNumber;
	}
	
	/**
	 * Sets the ballot number.
	 *
	 * @param ballotNumber the new ballot number
	 */
	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	
	/**
	 * Gets the sequential.
	 *
	 * @return the sequential
	 */
	public Long getSequential() {
		return sequential;
	}
	
	/**
	 * Sets the sequential.
	 *
	 * @param sequential the new sequential
	 */
	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}
	
	/**
	 * Gets the cash settlement amount.
	 *
	 * @return the cash settlement amount
	 */
	public BigDecimal getCashSettlementAmount() {
		return cashSettlementAmount;
	}

	/**
	 * Sets the cash settlement amount.
	 *
	 * @param cashSettlementAmount the new cash settlement amount
	 */
	public void setCashSettlementAmount(BigDecimal cashSettlementAmount) {
		this.cashSettlementAmount = cashSettlementAmount;
	}

	/**
	 * Gets the cash settlement price.
	 *
	 * @return the cash settlement price
	 */
	public BigDecimal getCashSettlementPrice() {
		return cashSettlementPrice;
	}

	/**
	 * Sets the cash settlement price.
	 *
	 * @param cashSettlementPrice the new cash settlement price
	 */
	public void setCashSettlementPrice(BigDecimal cashSettlementPrice) {
		this.cashSettlementPrice = cashSettlementPrice;
	}

	/**
	 * Gets the term settlement amount.
	 *
	 * @return the term settlement amount
	 */
	public BigDecimal getTermSettlementAmount() {
		return termSettlementAmount;
	}

	/**
	 * Sets the term settlement amount.
	 *
	 * @param termSettlementAmount the new term settlement amount
	 */
	public void setTermSettlementAmount(BigDecimal termSettlementAmount) {
		this.termSettlementAmount = termSettlementAmount;
	}

	/**
	 * Gets the term settlement price.
	 *
	 * @return the term settlement price
	 */
	public BigDecimal getTermSettlementPrice() {
		return termSettlementPrice;
	}

	/**
	 * Sets the term settlement price.
	 *
	 * @param termSettlementPrice the new term settlement price
	 */
	public void setTermSettlementPrice(BigDecimal termSettlementPrice) {
		this.termSettlementPrice = termSettlementPrice;
	}

	/**
	 * Sets the seller participant code nemonic.
	 *
	 * @param sellerParticipantCodeNemonic the new seller participant code nemonic
	 */
	public void setSellerParticipantCodeNemonic(String sellerParticipantCodeNemonic) {
		this.sellerParticipantCodeNemonic = sellerParticipantCodeNemonic;
	}

	/**
	 * Sets the buyer participant code nemonic.
	 *
	 * @param buyerParticipantCodeNemonic the new buyer participant code nemonic
	 */
	public void setBuyerParticipantCodeNemonic(String buyerParticipantCodeNemonic) {
		this.buyerParticipantCodeNemonic = buyerParticipantCodeNemonic;
	}

	/**
	 * Sets the ballot sequential.
	 *
	 * @param ballotSequential the new ballot sequential
	 */
	public void setBallotSequential(String ballotSequential) {
		this.ballotSequential = ballotSequential;
	}

	/**
	 * Gets the id mechanism.
	 *
	 * @return the id mechanism
	 */
	public Long getIdMechanism() {
		return idMechanism;
	}

	/**
	 * Sets the id mechanism.
	 *
	 * @param idMechanism the new id mechanism
	 */
	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}

	/**
	 * Gets the id modality.
	 *
	 * @return the id modality
	 */
	public Long getIdModality() {
		return idModality;
	}

	/**
	 * Sets the id modality.
	 *
	 * @param idModality the new id modality
	 */
	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}

	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}

	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the new modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}

	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}

	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}

	/**
	 * Gets the id process file.
	 *
	 * @return the id process file
	 */
	public Long getIdProcessFile() {
		return idProcessFile;
	}

	/**
	 * Gets the term settlement date.
	 *
	 * @return the term settlement date
	 */
	public Date getTermSettlementDate() {
		return termSettlementDate;
	}

	/**
	 * Sets the term settlement date.
	 *
	 * @param termSettlementDate the new term settlement date
	 */
	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}

	/**
	 * Sets the id process file.
	 *
	 * @param idProcessFile the new id process file
	 */
	public void setIdProcessFile(Long idProcessFile) {
		this.idProcessFile = idProcessFile;
	}

	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}

	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the removes the settlement motive.
	 *
	 * @return the removes the settlement motive
	 */
	public String getRemoveSettlementMotive() {
		return removeSettlementMotive;
	}

	/**
	 * Sets the removes the settlement motive.
	 *
	 * @param removeSettlementMotive the new removes the settlement motive
	 */
	public void setRemoveSettlementMotive(String removeSettlementMotive) {
		this.removeSettlementMotive = removeSettlementMotive;
	}

	/**
	 * Gets the id settlement operation.
	 *
	 * @return the id settlement operation
	 */
	public Long getIdSettlementOperation() {
		return idSettlementOperation;
	}

	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	/**
	 * Sets the id settlement operation.
	 *
	 * @param idSettlementOperation the new id settlement operation
	 */
	public void setIdSettlementOperation(Long idSettlementOperation) {
		this.idSettlementOperation = idSettlementOperation;
	}

	/**
	 * Gets the id remove settlement motive.
	 *
	 * @return the id remove settlement motive
	 */
	public Integer getIdRemoveSettlementMotive() {
		return idRemoveSettlementMotive;
	}

	/**
	 * Sets the id remove settlement motive.
	 *
	 * @param idRemoveSettlementMotive the new id remove settlement motive
	 */
	public void setIdRemoveSettlementMotive(Integer idRemoveSettlementMotive) {
		this.idRemoveSettlementMotive = idRemoveSettlementMotive;
	}

	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}

	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	/**
	 * Gets the ind term settlement.
	 *
	 * @return the ind term settlement
	 */
	public Integer getIndTermSettlement() {
		return indTermSettlement;
	}

	/**
	 * Sets the ind term settlement.
	 *
	 * @param indTermSettlement the new ind term settlement
	 */
	public void setIndTermSettlement(Integer indTermSettlement) {
		this.indTermSettlement = indTermSettlement;
	}

	/**
	 * Gets the settlement currency.
	 *
	 * @return the settlement currency
	 */
	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}

	/**
	 * Sets the settlement currency.
	 *
	 * @param settlementCurrency the new settlement currency
	 */
	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	/**
	 * Gets the forced purchase.
	 *
	 * @return the forced purchase
	 */
	public Boolean getForcedPurchase() {
		return forcedPurchase;
	}

	/**
	 * Sets the forced purchase.
	 *
	 * @param forcedPurchase the new forced purchase
	 */
	public void setForcedPurchase(Boolean forcedPurchase) {
		this.forcedPurchase = forcedPurchase;
	}

	public Boolean getIsIndPrepaid() {
		return isIndPrepaid;
	}

	public void setIsIndPrepaid(Boolean isIndPrepaid) {
		this.isIndPrepaid = isIndPrepaid;
	}

	public BigDecimal getStockQuantityRemmaing() {
		return stockQuantityRemmaing;
	}

	public void setStockQuantityRemmaing(BigDecimal stockQuantityRemmaing) {
		this.stockQuantityRemmaing = stockQuantityRemmaing;
	}
	public Boolean getIndBuyerSeller() {
		return indBuyerSeller;
	}
	public void setIndBuyerSeller(Boolean indBuyerSeller) {
		this.indBuyerSeller = indBuyerSeller;
	}
	public Long getIdOperationUnfulfillment() {
		return idOperationUnfulfillment;
	}
	public void setIdOperationUnfulfillment(Long idOperationUnfulfillment) {
		this.idOperationUnfulfillment = idOperationUnfulfillment;
	}
	
	
	
	
}


