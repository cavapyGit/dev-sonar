package com.pradera.negotiations.accountassignment.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.beanio.BeanIOConfigurationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.massive.type.MechanismFileProcessType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.negotiations.mcnoperations.facade.McnOperationServiceFacade;
import com.pradera.settlements.utils.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveAssignmentMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MassiveAssignmentMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The upload file types. */
	private String fUploadFileTypes = "XLSX|xlsx|xls|XLS|xml|dat";
	
	/** The upload file types pattern. */
	private Pattern fUploadFileTypesPattern=Pattern.compile("([^\\s]+(\\.(?i)("+fUploadFileTypes+"))$)");
	
	/** The upload file types display. */
	private String fUploadFileTypesDisplay="*.xlsx., *.xml, *.dat";

	/** The lst negotiation mechanisms. */
	List<NegotiationMechanism> lstNegotiationMechanisms;
	
	/** The mcn process files data model. */
	private GenericDataModel<ProcessFileTO> mcnProcessFilesDataModel;
	
	/** The errors data model. */
	private GenericDataModel<RecordValidationType> errorsDataModel;
	
	/** The mcn process file filter. */
	//private List<BeanType> lstNegotiationMechanisms;
	private ProcessFileTO mcnProcessFileFilter;  // for searches
	
	/** The mcn process file. */
	private ProcessFileTO mcnProcessFile; // bean sent to Registration Batch 
	
	/** The tmp mcn process file. */
	private ProcessFileTO tmpMcnProcessFile; // temporal bean on upload
	
	/** The disabled mechanism. */
	private boolean disabledMechanism = false; // if the combo is disabled
	
	/** The mechanism id. */
	private Long mechanismId; //used only to initialize MechanismProcessFileTO
	
	/** The session state is active. */
	private boolean sessionStateIsActive;
	
	/** The participant in session. */
	private Participant participantInSession;
	
	/** The mcn operation service facade. */
	@EJB
	private McnOperationServiceFacade mcnOperationServiceFacade;

	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {

		try {
			Integer insType = userInfo.getUserAccountSession().getInstitutionType();
	
			if(insType!=null){
				
				if(userInfo.getUserAccountSession().isParticipantInstitucion()){
					participantInSession = accountsFacade.getParticipantStateServiceFacade(
								userInfo.getUserAccountSession().getParticipantCode());
					if(!participantInSession.getState().equals(ParticipantStateType.REGISTERED.getCode())){
						setSessionStateIsActive(Boolean.FALSE);
					}else{
						setSessionStateIsActive(Boolean.TRUE);
					}
					
				}else if (userInfo.getUserAccountSession().isDepositaryInstitution()){
					setSessionStateIsActive(Boolean.TRUE);
					mechanismId = null;
					setDisabledMechanism(false);
				}else{
					setSessionStateIsActive(Boolean.TRUE);
					if(insType.equals(InstitutionType.BCR.getCode())){
						setDisabledMechanism(true);
						mechanismId = NegotiationMechanismType.BC.getCode();
					}else if(insType.equals(InstitutionType.BOLSA.getCode())){
						setDisabledMechanism(true);
						mechanismId = NegotiationMechanismType.BOLSA.getCode();
					}
				}
				
				lstNegotiationMechanisms = mcnOperationServiceFacade.getLstNegotiationMechanism(ComponentConstant.ONE);
			}
		
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		mcnProcessFileFilter = new ProcessFileTO(mechanismId);
		mcnProcessFileFilter.setProcessDate(CommonsUtilities.currentDate());
		tmpMcnProcessFile = new ProcessFileTO(mechanismId); 
		mcnProcessFileFilter.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
		tmpMcnProcessFile.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
		mcnProcessFile = null;
	}

	/**
	 * Change mechanism search.
	 */
	public void changeMechanismSearch(){
		mechanismId = mcnProcessFileFilter.getIdNegotiationMechanismPk();
	}
	
	/**
	 * On change process date.
	 */
	public void onChangeProcessDate() {
		mcnProcessFilesDataModel = null;
		tmpMcnProcessFile = new ProcessFileTO(mechanismId);	
		mcnProcessFile = null;
	}
	
	/**
	 * Clean all.
	 */
	public void cleanAll() {
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveAssignment");
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveAssignmentSearch");
		mcnProcessFileFilter = new ProcessFileTO(mechanismId);
		mcnProcessFileFilter.setProcessDate(CommonsUtilities.currentDate());
		mcnProcessFileFilter.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
		tmpMcnProcessFile = new ProcessFileTO(mechanismId); 
		tmpMcnProcessFile.setIdNegotiationMechanismPk(NegotiationMechanismType.BOLSA.getCode());
		mcnProcessFile = null;
		mcnProcessFilesDataModel = null;
	}

	/**
	 * Search processed files.
	 */
	public void searchProcessedFiles() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idNegotiationMechanismPk", mcnProcessFileFilter.getIdNegotiationMechanismPk());
		parameters.put("processDate", mcnProcessFileFilter.getProcessDate());
		parameters.put("processType",MechanismFileProcessType.MCN_ASSIGNMENT_UPLOAD.getCode());
		if(participantInSession!=null){
			parameters.put("idParticipantPk", participantInSession.getIdParticipantPk());
		}
		List<ProcessFileTO> mcnProcessFiles = mcnOperationServiceFacade
				.getMcnProcessedFilesInformation(parameters);
		mcnProcessFilesDataModel = new GenericDataModel<ProcessFileTO>(
				mcnProcessFiles);
	}

	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
			if (fDisplayName != null) {
				tmpMcnProcessFile.setFileName(fDisplayName);
				tmpMcnProcessFile.setProcessFile(event.getFile().getContents());
				tmpMcnProcessFile.setTempProcessFile(File.createTempFile("tempMcnfile", ".tmp"));
				FileUtils.writeByteArrayToFile(tmpMcnProcessFile.getTempProcessFile(), tmpMcnProcessFile.getProcessFile());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the mcn stream content.
	 *
	 * @param mcnFile the mcn file
	 * @return the mcn stream content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getMcnStreamContent(ProcessFileTO mcnFile) throws IOException{
		if(mcnFile!=null){
			
			InputStream inputStream = new ByteArrayInputStream(mcnFile.getProcessFile());
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream,null, mcnFile.getFileName());
			inputStream.close();

			return streamedContentFile;
		}
		return null;
	}

	/**
	 * Removefile handler.
	 *
	 * @param actionEvent the action event
	 */
	public void removefileHandler(ActionEvent actionEvent) {
		tmpMcnProcessFile.setProcessFile(null);
		tmpMcnProcessFile.setFileName(null);
		if(tmpMcnProcessFile.getTempProcessFile()!=null){
			tmpMcnProcessFile.getTempProcessFile().delete();
			tmpMcnProcessFile.setTempProcessFile(null);
		}
	}
	
	/**
	 * Gets the processed file content.
	 *
	 * @param mcnfile the mcnfile
	 * @return the processed file content
	 */
	public StreamedContent getProcessedFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = mcnOperationServiceFacade.getProcessedMcnFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,mcnfile.getFileName());
		} catch (Exception e) {
			showMessageOnDialog(null,null,PropertiesConstants.MCN_MASSIVE_ERROR_DOWNLOADING_FILE,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	
	/**
	 * Gets the accepted file content.
	 *
	 * @param mcnfile the mcnfile
	 * @return the accepted file content
	 */
	public StreamedContent getAcceptedFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = mcnOperationServiceFacade.getAcceptedMcnFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,
					"accepted_files"+mcnfile.getFileName()+".txt");
		} catch (Exception e) {
			showMessageOnDialog(null,null,PropertiesConstants.MCN_MASSIVE_ERROR_DOWNLOADING_FILE,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	

	/**
	 * Gets the rejected file content.
	 *
	 * @param mcnfile the mcnfile
	 * @return the rejected file content
	 */
	public StreamedContent getRejectedFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = mcnOperationServiceFacade.getRejectedMcnFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,
					"rejected_files"+mcnfile.getFileName()+".txt");
		} catch (Exception e) {
			showMessageOnDialog(null,null,PropertiesConstants.MCN_MASSIVE_ERROR_DOWNLOADING_FILE,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}

	/**
	 * Validate mcn file.
	 *
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServiceException the service exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void validateMcnFile() 
			throws BeanIOConfigurationException, IllegalArgumentException, IOException, ServiceException, ParserConfigurationException {
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveAssignment");
		int countValidationErrors = 0;
		
		if(Validations.validateIsNullOrNotPositive(tmpMcnProcessFile.getIdNegotiationMechanismPk())){
    		JSFUtilities.addContextMessage("frmMassiveAssignment:idMechanism",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MECHANISM_NULL), 
    				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MECHANISM_NULL));
			countValidationErrors++;
    	}
		
		if(Validations.validateIsNull(tmpMcnProcessFile.getTempProcessFile())){
    		JSFUtilities.addContextMessage("frmMassiveAssignment:fuplMcnFile",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MCNFILE_NULL), 
    				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MCNFILE_NULL));
			countValidationErrors++;
    	}
		
		
		if(countValidationErrors > 0) {
    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
    		return;
    	}
		
		for (NegotiationMechanism mechanism : lstNegotiationMechanisms) {
			if (mechanism.getIdNegotiationMechanismPk().equals(tmpMcnProcessFile.getIdNegotiationMechanismPk())) {
				tmpMcnProcessFile.setDescription(mechanism.getDescription());
				break;
			}
		}
		
		tmpMcnProcessFile.setRootTag(NegotiationConstant.MASSIVE_XML_BBV_ROOT_TAG);
		tmpMcnProcessFile.setStreamFileDir(NegotiationConstant.BBV_ASSIGNMENT_STREAM);
		tmpMcnProcessFile.setInterfaceName(ComponentConstant.INTERFACE_BBV_ASSIGNMENTS);
		if(participantInSession!=null){
			tmpMcnProcessFile.setIdParticipantPk(participantInSession.getIdParticipantPk());
		}
		tmpMcnProcessFile.setValidationProcessTO(new ValidationProcessTO());
		if(CommonsUtilities.validateFileOperationName(tmpMcnProcessFile, ComponentConstant.MASSIVE_ASSIGNMENT_PREFIX, ComponentConstant.MASSIVE_ASSIGNMENT_SUFFIX)) {
			CommonsUtilities.operationAssignmentconvertXLStoXML(tmpMcnProcessFile);
			CommonsUtilities.validateFileOperation(tmpMcnProcessFile, ComponentConstant.MASSIVE_ASSIGNMENT_PREFIX, ComponentConstant.MASSIVE_ASSIGNMENT_SUFFIX);
		}
		
		ValidationProcessTO mcnValidationTO = tmpMcnProcessFile.getValidationProcessTO();
		
		errorsDataModel = new GenericDataModel<RecordValidationType>(mcnValidationTO==null?new ArrayList<RecordValidationType>():mcnValidationTO.getLstValidations());
		mcnProcessFile = tmpMcnProcessFile;
		tmpMcnProcessFile = new ProcessFileTO(mechanismId);
	}
	
	/**
	 * Before process mcn file.
	 * Metodo que se utiliza para verificar si existe una carga de archivo de asignacion previa y que se encuentra en proceso //issue 502
	 * @throws ServiceException 
	 */
	@LoggerAuditWeb
	public void beforeProcessMcnFile() throws ServiceException{	
		
		//Validamos que no existan operaciones pendientes de cancelar
		if(mcnOperationServiceFacade.existPendingCancelOperations()) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("mcn.msg.massive.pending.cancel.operations"));
					JSFUtilities.showSimpleValidationDialog();
		}else {
			if(mcnOperationServiceFacade.verifyFileAssignmentProcess(mcnProcessFile)){	//issue 502
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
						PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_ASSIGN_FILE_IN_PROCESS));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_REGISTER_CONFIRM));
				JSFUtilities.executeJavascriptFunction("PF('cnfwProcessMcnFile').show();");
			}
		}
	}
	
	/**
	 * Process mcn file.
	 */
	@LoggerAuditWeb
	public void processMcnFile(){	
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.HOLDER_ACCOUNT_ASSIGN_MASSIVE_REGISTER.getCode());
		
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PARAMETER_FILE, mcnProcessFile.getTempProcessFile().getAbsolutePath());
			details.put(GeneralConstants.PARAMETER_STREAM, mcnProcessFile.getStreamFileDir());
			details.put(GeneralConstants.PARAMETER_MECHANISM, mcnProcessFile.getIdNegotiationMechanismPk());
			details.put(GeneralConstants.PARAMETER_LOCALE, "es"); //JSFUtilities.getCurrentLocale().toString());
			details.put(GeneralConstants.PARAMETER_FILE_NAME, mcnProcessFile.getFileName());
			if(mcnProcessFile.getIdParticipantPk()!=null){
				details.put(GeneralConstants.PARAMETER_OTC_PART_ID,mcnProcessFile.getIdParticipantPk());
			}
			
			batchProcessServiceFacade.registerBatch(
					userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.MCN_MASSIVE_ASSIGN_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();
		cleanAll();
	}
	

	/**
	 * Gets the mcn process files data model.
	 *
	 * @return the mcnProcessFilesDataModel
	 */
	public GenericDataModel<ProcessFileTO> getMcnProcessFilesDataModel() {
		return mcnProcessFilesDataModel;
	}

	/**
	 * Sets the mcn process files data model.
	 *
	 * @param mcnProcessFilesDataModel            the mcnProcessFilesDataModel to set
	 */
	public void setMcnProcessFilesDataModel(
			GenericDataModel<ProcessFileTO> mcnProcessFilesDataModel) {
		this.mcnProcessFilesDataModel = mcnProcessFilesDataModel;
	}

	/**
	 * Gets the mcn process file filter.
	 *
	 * @return the mcnProcessFileFilter
	 */
	public ProcessFileTO getMcnProcessFileFilter() {
		return mcnProcessFileFilter;
	}

	/**
	 * Sets the mcn process file filter.
	 *
	 * @param mcnProcessFileFilter            the mcnProcessFileFilter to set
	 */
	public void setMcnProcessFileFilter(ProcessFileTO mcnProcessFileFilter) {
		this.mcnProcessFileFilter = mcnProcessFileFilter;
	}

	/**
	 * Gets the mcn process file.
	 *
	 * @return the mcnProcessFile
	 */
	public ProcessFileTO getMcnProcessFile() {
		return mcnProcessFile;
	}

	/**
	 * Sets the mcn process file.
	 *
	 * @param mcnProcessFile            the mcnProcessFile to set
	 */
	public void setMcnProcessFile(ProcessFileTO mcnProcessFile) {
		this.mcnProcessFile = mcnProcessFile;
	}

	/**
	 * Gets the errors data model.
	 *
	 * @return the errorsDataModel
	 */
	public GenericDataModel<RecordValidationType> getErrorsDataModel() {
		return errorsDataModel;
	}

	/**
	 * Sets the errors data model.
	 *
	 * @param errorsDataModel            the errorsDataModel to set
	 */
	public void setErrorsDataModel(GenericDataModel<RecordValidationType> errorsDataModel) {
		this.errorsDataModel = errorsDataModel;
	}
	
	/**
	 * Gets the f upload file types.
	 *
	 * @return the fUploadFileTypes
	 */
	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	/**
	 * Sets the f upload file types.
	 *
	 * @param fUploadFileTypes the fUploadFileTypes to set
	 */
	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	/**
	 * Gets the f upload file types display.
	 *
	 * @return the fUploadFileTypesDisplay
	 */
	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	/**
	 * Sets the f upload file types display.
	 *
	 * @param fUploadFileTypesDisplay the fUploadFileTypesDisplay to set
	 */
	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}
	
	/**
	 * Gets the f upload file types pattern.
	 *
	 * @return the fUploadFileTypesPattern
	 */
	public Pattern getfUploadFileTypesPattern() {
		return fUploadFileTypesPattern;
	}

	/**
	 * Sets the f upload file types pattern.
	 *
	 * @param fUploadFileTypesPattern the fUploadFileTypesPattern to set
	 */
	public void setfUploadFileTypesPattern(Pattern fUploadFileTypesPattern) {
		this.fUploadFileTypesPattern = fUploadFileTypesPattern;
	}
	
	/**
	 * Gets the tmp mcn process file.
	 *
	 * @return the tmpMcnProcessFile
	 */
	public ProcessFileTO getTmpMcnProcessFile() {
		return tmpMcnProcessFile;
	}

	/**
	 * Sets the tmp mcn process file.
	 *
	 * @param tmpMcnProcessFile the tmpMcnProcessFile to set
	 */
	public void setTmpMcnProcessFile(ProcessFileTO tmpMcnProcessFile) {
		this.tmpMcnProcessFile = tmpMcnProcessFile;
	}

	/**
	 * Checks if is disabled mechanism.
	 *
	 * @return the disabledMechanism
	 */
	public boolean isDisabledMechanism() {
		return disabledMechanism;
	}

	/**
	 * Sets the disabled mechanism.
	 *
	 * @param disabledMechanism the disabledMechanism to set
	 */
	public void setDisabledMechanism(boolean disabledMechanism) {
		this.disabledMechanism = disabledMechanism;
	}

	/**
	 * Gets the lst negotiation mechanisms.
	 *
	 * @return the lst negotiation mechanisms
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanisms() {
		return lstNegotiationMechanisms;
	}

	/**
	 * Sets the lst negotiation mechanisms.
	 *
	 * @param lstNegotiationMechanisms the new lst negotiation mechanisms
	 */
	public void setLstNegotiationMechanisms(
			List<NegotiationMechanism> lstNegotiationMechanisms) {
		this.lstNegotiationMechanisms = lstNegotiationMechanisms;
	}

	/**
	 * Checks if is session state is active.
	 *
	 * @return true, if is session state is active
	 */
	public boolean isSessionStateIsActive() {
		return sessionStateIsActive;
	}

	/**
	 * Sets the session state is active.
	 *
	 * @param sessionStateIsActive the new session state is active
	 */
	public void setSessionStateIsActive(boolean sessionStateIsActive) {
		this.sessionStateIsActive = sessionStateIsActive;
	}

	/**
	 * Gets the participant in session.
	 *
	 * @return the participant in session
	 */
	public Participant getParticipantInSession() {
		return participantInSession;
	}

	/**
	 * Sets the participant in session.
	 *
	 * @param participantInSession the new participant in session
	 */
	public void setParticipantInSession(Participant participantInSession) {
		this.participantInSession = participantInSession;
	}

	

	
}
