package com.pradera.negotiations.accountassignment.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * issue 1105: pojo que almacena las operaciones con valores faltantes
 * @author rlarico
 *
 */
public class OperationWithNoSecuritiesTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer operationNumber;
	private String dateOperation;
	private String sequentialBallot;
	private String idSecurityCodePk;
	private Integer quantityHolder;
	private Integer quantityMissing;
	private String mnemonicParticipantSell;
	private String mnemonicParticipantBuy;
	private BigDecimal amountSettlements;
	private String stateOperation;
	
	// constructor
	public OperationWithNoSecuritiesTO(){}

	//TODO getter and setter
	public Integer getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(Integer operationNumber) {
		this.operationNumber = operationNumber;
	}

	public String getDateOperation() {
		return dateOperation;
	}

	public void setDateOperation(String dateOperation) {
		this.dateOperation = dateOperation;
	}

	public String getSequentialBallot() {
		return sequentialBallot;
	}

	public void setSequentialBallot(String sequentialBallot) {
		this.sequentialBallot = sequentialBallot;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public Integer getQuantityHolder() {
		return quantityHolder;
	}

	public void setQuantityHolder(Integer quantityHolder) {
		this.quantityHolder = quantityHolder;
	}

	public Integer getQuantityMissing() {
		return quantityMissing;
	}

	public void setQuantityMissing(Integer quantityMissing) {
		this.quantityMissing = quantityMissing;
	}

	public String getMnemonicParticipantSell() {
		return mnemonicParticipantSell;
	}

	public void setMnemonicParticipantSell(String mnemonicParticipantSell) {
		this.mnemonicParticipantSell = mnemonicParticipantSell;
	}

	public String getMnemonicParticipantBuy() {
		return mnemonicParticipantBuy;
	}

	public void setMnemonicParticipantBuy(String mnemonicParticipantBuy) {
		this.mnemonicParticipantBuy = mnemonicParticipantBuy;
	}

	public BigDecimal getAmountSettlements() {
		return amountSettlements;
	}

	public void setAmountSettlements(BigDecimal amountSettlements) {
		this.amountSettlements = amountSettlements;
	}

	public String getStateOperation() {
		return stateOperation;
	}

	public void setStateOperation(String stateOperation) {
		this.stateOperation = stateOperation;
	}
}
